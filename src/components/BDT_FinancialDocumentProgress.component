<apex:component controller="BDT_FinancialDocumentProgressController" allowDML="true">
<apex:attribute name="financialDocumentIDAttr" assignTo="{!financialDocumentId}" 	description="Financial Document ID."  type="String" required="true"/>
<apex:attribute name="documentNameAttr" assignTo="{!documentName}" description="documentName" type="string" required="true"/>
<apex:attribute name="priceCheckAttr" assignTo="{!priceCheck}" description="priceCheck" type="boolean" required="true"/>
<apex:attribute name="paymentCheckAttr" assignTo="{!paymentCheck}" description="paymentCheck" type="boolean" required="true"/>
<apex:attribute name="contentCheckAttr" assignTo="{!contentCheck}" description="contentCheck" type="boolean" required="true"/>
<apex:attribute name="approvalCheckAttr" assignTo="{!approvalCheck}" description="approvalCheck" type="boolean" required="true"/>
<apex:attribute name="showOnlyStudySelectionAttr" assignTo="{!showOnlyStudySelection}" description="showOnlyStudySelection" type="boolean"/>
<apex:attribute name="currentPageAttr" assignTo="{!currentPage}" description="currentPage" type="string" required="true"/>
<apex:attribute name="showAllOptionsAttr" assignTo="{!showAllOptions}" description="showAllOptions" type="boolean"/>
<apex:attribute name="approvalsUpToDateAttr" assignTo="{!approvalsUpToDate}" description="approvalsUpToDate" type="boolean" required="true"/>
<style>

	.Progress {
		text-align: center;
	}
	.Progress .bubble {
		border: 1px solid #5588bb;
		border-radius: 30px 30px 30px 30px;
		height: 60px;
		width: 60px;
		background-color: #6699cc;
		margin: 12px 0;
		display: inline-block;
	}
	.Progress .bubble .innerBubble{
		width: 100%; 
  		height: 100%; 
  		display: table; 
	}
	.Progress .bubble .innerBubbleCheck{
		height:20px;
		width:20px;
		background: url({!URLFOR($Resource.BDT_nav_files,'img/check_on_progress.png')});
		float:right;
		margin-top:-15px;
		
	}
	.Progress .bubble .innerBubbleWarningCheck{
		height:20px;
		width:20px;
		/*background: url({!URLFOR($Resource.BDT_nav_files, 'img/warning.gif')});*/
		float:right;
		margin-top:-17px;
		
	}
	.Progress .bubble .innerBubble a {
		display: table-cell; 
		vertical-align: middle; 
		text-align: center; 
		text-decoration: none;
		color:white;
		font-weight: bold;
		text-shadow: 0 1px 0 #5784AE;
	}
	.Progress .noBubble {
		height: 60px;
		margin: 12px 0;
		display: inline-block;
	}
	.Progress .noBubble .innerBubble{
		width: 100%; 
  		height: 100%; 
  		display: table; 
	}
	.Progress .noBubble .innerBubble span.history {
		display: table-cell; 
		text-align: right; 
		color:lightgray;
		font-weight: normal;
		font-size: 50px;
		width: 60px;
		
	}
	.Progress .noBubble .innerBubble span.arrow {
		color: gray;
		display: table-cell;
		font-size: 30px;
		font-weight: normal;
		letter-spacing: -16px;
		text-align: left;
		vertical-align: middle;
		width: 38px;
    }
	.Progress .noBubble .innerBubble span.spacedLeftArrow {
		color: lightgray;
		display: table-cell;
		font-size: 30px;
		font-weight: normal;
		letter-spacing: -16px;
		text-align: middle;
		vertical-align: middle;
		width: 70px;
    }
	.Progress .noBubble .innerBubble span.spacedRightArrow {
		color: lightgray;
		display: table-cell;
		font-size: 30px;
		font-weight: normal;
		letter-spacing: -16px;
		text-align: left;
		vertical-align: middle;
		width: 60px;
    }
</style>

	<apex:form >
		<div class="BDT_Container">
			<c:BDT_PageBlockHeader sectionHeading="Progress of {!IF(showAllOptionsAttr,'Proposal','Change Order')} {!documentName}" buttonValue="Back" buttonName="Back to Financial Documents"  goToPageName="BDT_FinancialDocOverview" />
			
			
			<div class="Progress">
				<!-- Study Selection -->
				<apex:outputPanel rendered="{!currentPageAttr=='bdt_financialdocstudysel'}">
					<div class="bubble" style="background-color:orange">
						<div class="innerBubble">
							<apex:commandLink value="Study Selection" action="{!GotoStudySelection}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
						</div>
					</div>
				</apex:outputPanel>
				<apex:outputPanel rendered="{!currentPageAttr!='bdt_financialdocstudysel'}">
					<div class="bubble">
						<div class="innerBubble">
							<apex:commandLink value="Study Selection" action="{!GotoStudySelection}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
						</div>
					</div>
				</apex:outputPanel>
				<apex:outputPanel rendered="{!not(showOnlyStudySelection)}">
					<div class="noBubble"><div class="innerBubble"><span class="spacedRightArrow">>>>>>>>>>>>>>>></span></div></div>
					<!-- Price -->
					<apex:outputPanel rendered="{!currentPageAttr=='bdt_financialdocpricing'}">
						<div class="bubble" style="background-color:orange">
							<div class="innerBubble">
								<apex:commandLink value="Price" action="{!GotoPrice}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
							</div>
							<apex:outputPanel styleclass="innerBubbleCheck" layout="block" rendered="{!priceCheck}"/>
						</div>
					</apex:outputPanel>
					<apex:outputPanel rendered="{!currentPageAttr!='bdt_financialdocpricing'}">
						<div class="bubble">
							<div class="innerBubble">
								<apex:commandLink value="Price" action="{!GotoPrice}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
							</div>
							<apex:outputPanel styleclass="innerBubbleCheck" layout="block" rendered="{!priceCheck}"/>
						</div>
					</apex:outputPanel>
					<div class="noBubble"><div class="innerBubble"><span class="arrow">>>>>>>>>>>>>>>></span></div></div>
					<!-- Payment -->
					<apex:outputPanel rendered="{!currentPageAttr=='bdt_financialdocpayment' && showAllOptionsAttr}">
						<div class="bubble" style="background-color:orange">
							<div class="innerBubble">
								<apex:commandLink value="Payment" action="{!GotoPayment}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
							</div>
							<apex:outputPanel styleclass="innerBubbleCheck" layout="block" rendered="{!paymentCheck}"/>
						</div>
					</apex:outputPanel>
					<apex:outputPanel rendered="{!currentPageAttr!='bdt_financialdocpayment' && showAllOptionsAttr}">
						<div class="bubble">
							<div class="innerBubble">
								<apex:commandLink value="Payment" action="{!GotoPayment}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
							</div>
							<apex:outputPanel styleclass="innerBubbleCheck" layout="block" rendered="{!paymentCheck}"/>
						</div>
					</apex:outputPanel>
					<apex:outputPanel rendered="{!showAllOptionsAttr}">
						<div class="noBubble"><div class="innerBubble"><span class="arrow">>>>>>>>>>>>>>>></span></div></div>
					</apex:outputPanel>
					<!-- Content -->
					<apex:outputPanel rendered="{!currentPageAttr=='bdt_financialdoccontent' && showAllOptionsAttr}">
						<div class="bubble" style="background-color:orange">
							<div class="innerBubble">
								<apex:commandLink value="Content" action="{!GotoContent}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
							</div>
							<apex:outputPanel styleclass="innerBubbleCheck" layout="block" rendered="{!contentCheck}"/>
						</div>
					</apex:outputPanel>
					<apex:outputPanel rendered="{!currentPageAttr!='bdt_financialdoccontent' && showAllOptionsAttr}">
						<div class="bubble">
							<div class="innerBubble">
								<apex:commandLink value="Content" action="{!GotoContent}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
							</div>
							<apex:outputPanel styleclass="innerBubbleCheck" layout="block" rendered="{!contentCheck}"/>
						</div>
					</apex:outputPanel> 
					<apex:outputPanel rendered="{!showAllOptionsAttr}">
						<div class="noBubble"><div class="innerBubble"><span class="arrow">>>>>>>>>>>>>>>></span></div></div>
					</apex:outputPanel>
					<!-- Approval -->
					<apex:outputPanel rendered="{!currentPageAttr=='bdt_financialdocapproval'}">
						<div class="bubble" style="background-color:orange">
							<div class="innerBubble">
								<apex:commandLink value="Approval" action="{!GotoApproval}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
							</div>
							<apex:outputPanel styleclass="innerBubbleCheck" layout="block" rendered="{!approvalCheck}"/>
							<apex:outputPanel styleclass="innerBubbleWarningCheck" layout="block" rendered="{!NOT(approvalsUpToDate)}">
								<img src="{!URLFOR($Resource.BDT_nav_files, '/img/warning.gif')}" style="width:20px;height:20px;"/>
							</apex:outputPanel>
						</div>
					</apex:outputPanel>
					<apex:outputPanel rendered="{!currentPageAttr!='bdt_financialdocapproval'}">
						<div class="bubble">
							<div class="innerBubble">
								<apex:commandLink value="Approval" action="{!GotoApproval}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
							</div>
							<apex:outputPanel styleclass="innerBubbleCheck" layout="block" rendered="{!approvalCheck}"/>
							<apex:outputPanel styleclass="innerBubbleWarningCheck" layout="block" rendered="{!NOT(approvalsUpToDate)}">
								<img src="{!URLFOR($Resource.BDT_nav_files, '/img/warning.gif')}" style="width:20px;height:20px;"/>
							</apex:outputPanel>
						</div>
					</apex:outputPanel>
					<div class="noBubble"><div class="innerBubble"><span class="spacedLeftArrow">>>>>>>>>>>>>>>></span></div></div>
					<!-- Output -->
					<apex:outputPanel rendered="{!currentPageAttr=='bdt_financialdocoutput'}">
						<div class="bubble" style="background-color:orange">
							<div class="innerBubble">
								<apex:commandLink value="Output" action="{!GotoOutput}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
							</div>
						</div>
					</apex:outputPanel>
					<apex:outputPanel rendered="{!currentPageAttr!='bdt_financialdocoutput'}">
						<div class="bubble">
							<div class="innerBubble">
								<apex:commandLink value="Output" action="{!GotoOutput}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
							</div>
						</div>
					</apex:outputPanel>
					<div class="noBubble"><div class="innerBubble"><span class="history">}</span></div></div>
					<!-- History -->
					<apex:outputPanel rendered="{!currentPageAttr=='bdt_financialdochistory'}">
						<div class="bubble" style="background-color:orange">
							<div class="innerBubble">
								<apex:commandLink value="History" action="{!GotoHistory}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
							</div>
						</div>
					</apex:outputPanel>
					<apex:outputPanel rendered="{!currentPageAttr!='bdt_financialdochistory'}">
						<div class="bubble">
							<div class="innerBubble">
								<apex:commandLink value="History" action="{!GotoHistory}"><apex:param name="financialDocumentId" value=""/></apex:commandLink>
							</div>
						</div>
					</apex:outputPanel>
				</apex:outputPanel>
				
				



				<div></div>
				
			
			</div>
			
			
		</div>
	</apex:form>

</apex:component>