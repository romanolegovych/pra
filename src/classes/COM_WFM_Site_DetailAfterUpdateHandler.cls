public with sharing class COM_WFM_Site_DetailAfterUpdateHandler extends TriggerHandlerBase {
	
	private static final String CLASSNAME = 'COM_WFM_Site_DetailAfterUpdateHandler';
    private static final COM_Logger LOGGER = new COM_Logger(CLASSNAME);

	public override void mainEntry(TriggerParameters tp) {
		process((List<WFM_Site_Detail__c>)tp.newList);
	}
	
	private void process(List<WFM_Site_Detail__c> sites) {
		LOGGER.info('Processing updated sites:'+sites.size());
		for(WFM_Site_Detail__c site : sites) {
			site.Name += site.Country__c;
			//sObjectsToUpdate.put(site.Id, site);
		}
	}
	
	public override void inProgressEntry(TriggerParameters tp) {
		LOGGER.debug('This is an example for reentrant code...');
	}
	
	public override void updateObjects() {
		// for demonstration purposes, don't do anything here...
	}

}