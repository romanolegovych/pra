public with sharing class RM_MyProjectRoleChartController {
	public string selectedCountry{get; set;}
	public string nView{get; set;}
	public String selectedBusinessUnit{ get; set; }

	public Integer n{get; set;}
	public Boolean bPrev{get; private set;}
	public Boolean bNext{get; private set;}
	
	private string projectRID;
	private string strProjectRoleData;
	private string roleCat;
	
	private Integer nMax;
	private Integer roleSize;
	private Integer defaultView = 10;
	
	public RM_MyProjectRoleChartController(){
		projectRID = ApexPages.currentPage().getParameters().get('id');
		system.debug('-----projectRID------'+projectRID); 
		n = 1;
		selectedCountry = 'All';
		getRoleData();
		bPrev = false;
		bNext = false;
		if (selectedBusinessUnit == null)
		{
	     	  set<string> stManagedBU = RM_OtherService.GetUserGroupBU(UserInfo.getUserId());  
	   		  system.debug('---------ManagedBU---------'+stManagedBU);
		      if (stManagedBU != null && stManagedBU.size() > 0){
	                list<string>lstManagedBU = new list<string>();                
	                lstManagedBU.addAll(stManagedBU);
	                 selectedBusinessUnit = lstManagedBU[0];
		      }
	         else	         
	            selectedBusinessUnit = '';	         
		}
		
		getRoleData();
		if (roleSize > defaultView){		
   			nView = string.valueOf(defaultView);
   			bNext = true;
   			
   			nMax = Math.round(roleSize/decimal.valueOf(nView)+0.5);
   			system.debug('-----nMax------'+nMax + '--' +roleSize) ;
		}
   		else
   			nView = 'All';
   		
	}
	public List<SelectOption> getProjCountryItems(){
		List<SelectOption> country = RM_LookUpDataAccess.GetProjectCountry(ApexPages.currentPage().getParameters().get('id'),  -3, -1);
       system.debug('-----ProjCountryItems------'+country); 
       return country; 
   	}
   	public List<SelectOption>getBusinessUnit(){ 
        //transient List<SelectOption> options = RM_LookUpDataAccess.getBU(true);
        transient List<SelectOption> options = RM_LookUpDataAccess.getBUinGroup(false); 
        return options;
    } 
     public PageReference BusinessUnitChange(){
    	getRoleData();
   	  	return null;
    	
    }
   	 public PageReference ViewChange(){
    	if (nView != 'All'){
    		bNext = true;
    		n = 1;
    		
    		
    	}
    	else{
    		bPrev = false;
			bNext = false;
    	}
    	getRoleData();
    	
    	return null;
    }
    public PageReference Previous(){
    	if (n > 1)
    		n -= 1;
    	if (n > 1)
    		bPrev = true;
    	else
    		bPrev = false;
    	if (n < nMax)
    		bNext = true;
    	else
    		bNext = false;
    	getRoleData();
    	return null;
    }
    public PageReference Next(){
    	if (n < nMax)
    		n += 1;
    	if (n > 1)
    		bPrev = true;
    	else
    		bPrev = false;
    	if (n < nMax)
    		bNext = true;
    	else
    		bNext = false;
    	system.debug('-----Next---n---'+n + 'nMax' + nMax);
    	getRoleData();
    	return null;
    }
     public list<SelectOption> getViewItems(){
   	
   	
   		return  RM_LookupDataAccess.getViewNumberItems(roleSize, 10);
   	}
   	public  PageReference CountryChange(){
   		getRoleData();
   	  	return null;
   	} 
   	public string GetProjectRoleData(){
   		return strProjectRoleData;
   	}
   	public string  getJobClass(){
    	return roleCat;
    }
   	private void getRoleData(){
   		roleCat = '';
   		strProjectRoleData = '';
   		list<string> JobClassList = RM_AssignmentsService.GetJobClasswithAssignmentandWorkedByProjectCountry(projectRID, selectedCountry, selectedBusinessUnit, -3, -1);  
   		if (JobClassList.size() > 0){
	   		
	   		for (string s: jobClassList)
				roleCat +='\''+ s + '\',';
	    	List<AggregateResult> arAssignedList = RM_AssignmentsService.GetAssignmentByProjectCountry(projectRID, selectedCountry, selectedBusinessUnit, -3, -1);
			roleSize = JobClassList.size();
			
			system.debug('-----arAssignedList------'+arAssignedList);
			 
			if (roleSize > 0){
				integer nStart = 0;
		 	 	integer nEnd = roleSize-1;
	    	 	 	
		 	 	if (nView!= 'All' && nView != null){
	    	 	 	nStart = Integer.valueof(nView)*(n-1);
	    	 	 	
	    	 	 	nEnd = nStart + Integer.valueOf(nView)-1;
	    	 	 	if (nEnd > roleSize-1)
	    	 	 		nEnd = roleSize-1;
		 	 	}	
		 	 	else if (nView == null){
		 	 		if (roleSize > defaultView ){
			 	 		nView = string.ValueOf(defaultView);
			 	 		nEnd = nStart + Integer.valueOf(nView)-1;
		 	 		}
		 	 	} 
				strProjectRoleData='';
				for (string m: RM_Tools.GetMonths(-3, -1)){
					strProjectRoleData += '{ name: \'Assigned ' + m + '\', data:[';
					for  (integer j = nStart; j <= nEnd; j++) {
					
						boolean bFind = false;
						for(AggregateResult ar:arAssignedList){
							if ((string)ar.get('Job_Class_Desc__c') == jobClassList[j] && (string)ar.get('Year_Month__c') == m){
				
						
								bFind = true;
								if (ar.get('assignedFTE') != null)
									strProjectRoleData += string.valueOf((decimal)ar.get('assignedFTE')) + ',';
							}
						}
						
						if (!bFind)
							strProjectRoleData +=  '0,';
					}
					strProjectRoleData = strProjectRoleData.substring(0, strProjectRoleData.length()-1) + '], stack: \'Assigned\'},';
					
				}
				List<AggregateResult> arHistList = RM_EmployeeService.GetActuralWorkHrByProjectIDCountry(projectRID, selectedCountry, selectedBusinessUnit, -3, -1);
				for (string m: RM_Tools.GetMonths(-3, -1)){
					strProjectRoleData += '{ name: \'Actuals ' + m + '\', data:[';
					for  (integer j = nStart; j <= nEnd; j++) {
					
						boolean bFind = false;
						for(AggregateResult ar:arHistList){
						
							if ((string)ar.get('Job_Class_Desc__c') == jobClassList[j] && (string)ar.get('Year_Month__c') == m){
								bFind = true;
								strProjectRoleData += string.valueOf((decimal)ar.get('fte')) + ',';
							}
						}
						if (!bFind)
							strProjectRoleData +=  '0,';
					}
					strProjectRoleData = strProjectRoleData.substring(0, strProjectRoleData.length()-1) + '], stack: \'Actuals\'},';
					
				}
				system.debug('-----strProjectRoleData------'+strProjectRoleData);
				system.debug('-----roleCat------'+roleCat);
				//remove last ,
				if (roleCat.length() > 0){
					roleCat = roleCat.substring(0, roleCat.length()-1);
					strProjectRoleData = strProjectRoleData.substring(0, strProjectRoleData.length()-1);
				}
			}
   		}
   		
   			
   	}
}