/**
 * Test to cover CourseToProject controller
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestCourseToProjectController {

    static List<Job_Class_Desc__c> projectRoles;
    static List<WFM_Client__c> clients;
    static List<WFM_Project__c> projects;
    static List<WFM_Contract__c> contracts;
    static List<Role_Type__c> roleTypes;
    static List<LMS_Role__c> roles;
    static List<Country__c> countries;
    static List<Course_Domain__c> courseDomains;
    static List<LMS_Course__c> courses;
    static List<Employee_Details__c> employees;
    static List<LMS_Role_Course__c> roleCourses;
    static List<LMS_Role_Employee__c> roleEmployees;
    static List<LMSConstantSettings__c> constants;
    static List<CourseDomainSettings__c> domains;
    static List<RoleAdminServiceSettings__c> settings;

    static void init() {

        constants = new LMSConstantSettings__c[] {
            new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
            new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
            new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
            new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D'),
            new LMSConstantSettings__c(Name = 'typePRA', Value__c = 'PRA'),
            new LMSConstantSettings__c(Name = 'typeProject', Value__c = 'Project Specific'),
            new LMSConstantSettings__c(Name = 'typeAdhoc', Value__c = 'Additional Role'),
            new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
            new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
            new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
            new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
            new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
            new LMSConstantSettings__c(Name = 'statusToDelete', Value__c = 'Delete'),
            new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended'),
            new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
            new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
            new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
            new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
            new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
            new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete')
        };
        insert constants;

        domains = new CourseDomainSettings__c[] {
            new CourseDomainSettings__c(Name = 'Internal', Domain_Id__c = 'Internal'),
            new CourseDomainSettings__c(Name = 'PST', Domain_Id__c = 'Project Specific'),
            new CourseDomainSettings__c(Name = 'Archive', Domain_Id__c = 'Archive')
        };
        insert domains;

        settings = new RoleAdminServiceSettings__c[] {
            new RoleAdminServiceSettings__c(Name = 'DOMAIN', Value__c = 'DOMAIN'),
            new RoleAdminServiceSettings__c(Name = 'ENDPOINT', Value__c = 'ENDPOINT'),
            new RoleAdminServiceSettings__c(Name = 'TIMEOUT', Value__c = '50000') 
        };
        insert settings;

        projectRoles = new Job_Class_Desc__c[] {
            new Job_Class_Desc__c(Name = 'class1', Job_Class_ExtID__c = 'class1', Job_Class_Code__c = 'code1', Is_Assign__c = true),
            new Job_Class_Desc__c(Name = 'class2', Job_Class_ExtID__c = 'class2', Job_Class_Code__c = 'code2', Is_Assign__c = true),
            new Job_Class_Desc__c(Name = 'class3', Job_Class_ExtID__c = 'class3', Job_Class_Code__c = 'code3', Is_Assign__c = true),
            new Job_Class_Desc__c(Name = 'class4', Job_Class_ExtID__c = 'class4', Job_Class_Code__c = 'code4', Is_Assign__c = true)
        };
        insert projectRoles;

        clients = new WFM_Client__c[] {
            new WFM_Client__c(Name = 'Client1', Client_Unique_Key__c = 'Client1'),
            new WFM_Client__c(Name = 'Client2', Client_Unique_Key__c = 'Client2'),
            new WFM_Client__c(Name = 'c', Client_Unique_Key__c = 'c')
        };
        insert clients;

        contracts = new WFM_Contract__c[] {
            new WFM_Contract__c(Name = 'contract1', Contract_Unique_Key__c = 'contract1', Client_Id__c = clients[0].Id),
            new WFM_Contract__c(Name = 'contract2', Contract_Unique_Key__c = 'contract2', Client_Id__c = clients[1].Id),
            new WFM_Contract__c(Name = 'contract3', Contract_Unique_Key__c = 'contract3', Client_Id__c = clients[2].Id)
        };
        insert contracts;

        projects = new WFM_Project__c[] {
            new WFM_Project__c(Name = 'Project1', Project_Unique_Key__c = 'Project1', Contract_Id__c = contracts[0].Id),
            new WFM_Project__c(Name = 'Project2', Project_Unique_Key__c = 'Project2', Contract_Id__c = contracts[1].Id, Status_Desc__c = 'Lost'),
            new WFM_Project__c(Name = 'p', Project_Unique_Key__c = 'p', Contract_Id__c = contracts[2].Id, Status_Desc__c = 'Closed')
        };
        insert projects;

        roleTypes = new Role_Type__c[] {
            new Role_Type__c(Name = 'PRA'),
            new Role_Type__c(Name = 'Project Specific'),
            new Role_Type__c(Name = 'Adhoc')
        };
        insert roleTypes;

        courseDomains = new Course_Domain__c[] {
            new Course_Domain__c(Domain__c = 'Internal'),
            new Course_Domain__c(Domain__c = 'Archive'),
            new Course_Domain__c(Domain__c = 'Project Specific')
        };
        insert courseDomains;   

        courses = new LMS_Course__c[] {
            new LMS_Course__c(Offering_Template_No__c = 'course123', Course_Code__c = 'C123', Title__c = 'Course 123', Domain_Id__c = courseDomains[0].Domain__c,
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse1',
                              Discontinued_From__c = Date.today()+1, Duration__c = 60),
            new LMS_Course__c(Offering_Template_No__c = 'course321', Course_Code__c = 'C124', Title__c = 'Course 321', Domain_Id__c = courseDomains[1].Domain__c,
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse2',
                              Discontinued_From__c = Date.today()+1, Project_Id__c = 'Project1', Client_Id__c = 'Client1', Duration__c = 60),
            new LMS_Course__c(Offering_Template_No__c = 'course124', Course_Code__c = 'C234', Title__c = 'Course 234', Domain_Id__c = courseDomains[2].Domain__c,
                              Type__c = 'Project Specific', Status__c = 'Retired', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse3',
                              Discontinued_From__c = Date.today()+1, Project_Id__c = 'Project1', Client_Id__c = 'Client1', Duration__c = 60),
            new LMS_Course__c(Offering_Template_No__c = 'course243', Course_Code__c = 'C243', Title__c = 'Course 243', Domain_Id__c = courseDomains[2].Domain__c,
                              Type__c = 'Project Specific', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse4',
                              Discontinued_From__c = Date.today()+1, Project_Id__c = 'Project1', Client_Id__c = 'Client1', Duration__c = 60)
        };
        insert courses;

        countries = new Country__c[] {
            new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='US/Canada', 
                               Daily_Business_Hrs__c=8.0)
        };
        insert countries;
    }

    static void initManyCourses() {
        Integer i = 1000;
        courses = new List<LMS_Course__c>();
        while(i < 2001) {
            courses.add(new LMS_Course__c(Offering_Template_No__c = 'course' + i, Course_Code__c = 'C' + i, Title__c = 'Course ' + i, Domain_Id__c = 'Internal',
                Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                Updated_On__c = Datetime.now(), Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse' + i,
                Discontinued_From__c = Date.today()+1));
            i++;
        }
        insert courses;
    }

    static void initRoles() {
        roles = new LMS_Role__c[] {
            new LMS_Role__c(Job_Class_Desc__c = projectRoles[0].Id, Project_Id__c = projects[0].Id, Client_Id__c = clients[0].Id, Role_Type__c = roleTypes[1].Id, Status__c = 'Active', Sync_Status__c = 'N'),
            new LMS_Role__c(Job_Class_Desc__c = projectRoles[1].Id, Project_Id__c = projects[0].Id, Client_Id__c = clients[0].Id, Role_Type__c = roleTypes[1].Id, Status__c = 'Active', Sync_Status__c = 'N'),
            new LMS_Role__c(Job_Class_Desc__c = projectRoles[0].Id, Project_Id__c = null, Client_Id__c = clients[0].Id, Role_Type__c = roleTypes[1].Id, Status__c = 'Active', Sync_Status__c = 'N'),
            new LMS_Role__c(Job_Class_Desc__c = projectRoles[3].Id, Project_Id__c = projects[0].Id, Client_Id__c = clients[0].Id, Role_Type__c = roleTypes[1].Id, Status__c = 'Active', Sync_Status__c = 'N'),
            new LMS_Role__c(Job_Class_Desc__c = projectRoles[1].Id, Project_Id__c = projects[1].Id, Client_Id__c = clients[1].Id, Role_Type__c = roleTypes[1].Id, Status__c = 'Inactive', Sync_Status__c = 'N'),
            new LMS_Role__c(Job_Class_Desc__c = projectRoles[2].Id, Project_Id__c = projects[1].Id, Client_Id__c = clients[1].Id, Role_Type__c = roleTypes[1].Id, Status__c = 'Active', Sync_Status__c = 'N'),
            new LMS_Role__c(Job_Class_Desc__c = projectRoles[3].Id, Project_Id__c = projects[2].Id, Client_Id__c = clients[2].Id, Role_Type__c = roleTypes[1].Id, Status__c = 'Active', Sync_Status__c = 'Y')
        };
        insert roles;
    }

    static void initMappings() {
        roleCourses = new LMS_Role_Course__c[]{
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Draft Delete', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Committed', Assigned_By__c = 'ASA')
        };
        insert roleCourses;
    }

    static void initEmployees() {
        employees = new Employee_Details__c[] {
            new Employee_Details__c(Name = 'employee1', Employee_Unique_Key__c = 'employee1', First_Name__c = 'Andrew', Last_Name__c = 'Allen', 
                Email_Address__c = 'a@a.com', Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', 
                Department__c = 'Analysis and Reporting', Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', 
                Date_Hired__c = date.parse('12/27/2009'), Business_Unit__c = 'RDU')
        };
        insert employees;

        roleEmployees = new LMS_Role_Employee__c[] {
            new LMS_Role_Employee__c(Role_Id__c = roles[0].Id, Employee_Id__c = employees[0].Id, Status__c = 'Draft', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today())
        };
        insert roleEmployees;
    }

    static LMS_CourseToProjectController initSuccessRoleSearch() {
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = projectRoles[0].Name;
        return c;
    }

    static LMS_CourseToProjectController initSuccessRoleSearchNoProject() {
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = '';
        c.projectRole = projectRoles[0].Name;
        return c;
    }

    static LMS_CourseToProjectController initSuccessRoleSearchNoClient() {
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = '';
        c.projectText = projects[0].Name;
        c.projectRole = projectRoles[0].Name;
        return c;
    }

    static testMethod void testInit() {
        init();
        String pstDomain = domains[1].Domain_Id__c;
        String internalDomain = domains[0].Domain_Id__c;
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        System.assert(c.clientText == '' && c.projectText == '' && c.courseText == 'Enter course title' &&
            c.courseTextStyle == 'WaterMarkedTextBox' && c.searchFilter == 'and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
			    'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
			    'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')');
    }

    static testMethod void testInitWithParams(){
        init();
        Pagereference pr = new PageReference('/apex/LMS_CourseToProject?client=c&project=p&class=c');
        Test.setCurrentPage(pr);
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        System.assert(c.clientText == 'c' && c.projectText == 'p' && c.projectRole == 'c');
    }

    static testMethod void testInitWithParamsRoleId() {
        init();
        initRoles();
        Pagereference pr = new PageReference('/apex/LMS_CourseToProject?roleId='+roles[0].Id);
        Test.setCurrentPage(pr);
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        System.assert(c.clientText == clients[0].Name && c.projectText == projects[0].Name &&
            c.projectRole == projectRoles[0].Name);
    }

    static testMethod void testResetMethods(){
        init();
        String pstDomain = domains[1].Domain_Id__c;
        String internalDomain = domains[0].Domain_Id__c;
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.closeCourseSearch();
        System.assert(c.selCourses == null);

        c.resetCourseSearch();
        System.assert(c.courseText == 'Enter course title' && c.courseTextStyle == 'WaterMarkedTextBox' && c.selCourses == null);

        c.resetPage();
        System.assert(c.courses == null && c.selCourses == null && c.clientText == '' && c.projectText == '' && 
            c.projectFilter == 'and Client_Id__c != \'\' and Client_Id__c != null and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')' && 
            c.searchFilter == 'and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
    			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')');
    }

    static testMethod void testProjectSearchType(){
        init();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = 'client1';
        c.projectText = '';
        c.projSearchType();
        System.assert(c.projectFilter == ' and Client_Id__c != \'\' and Client_Id__c != null and Client_Id__c = \'' + c.clientText + '\''
        	+ ' and Status_Desc__c not in (\'Bid\',\'Lost\',\'Closed\')');

        c.clientText = '';
        c.projectText = 'project1';
        c.projSearchType();
        System.debug('------------------------project filter------------------------------'+c.projectFilter);
        System.assert(c.projectFilter == ' and Client_Id__c != \'\' and Client_Id__c != null and Client_Id__c = \'\''
        	+ ' and Status_Desc__c not in (\'Bid\',\'Lost\',\'Closed\')');
    }

    static testMethod void testClientSearchType(){
        init();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = '';
        c.projectText = 'project1';
        c.projSearchType();
        System.debug('------------------------project filter------------------------------'+c.projectFilter);
        System.assert(c.projectFilter == ' and Client_Id__c != \'\' and Client_Id__c != null and Client_Id__c = \'\''
        	+ ' and Status_Desc__c not in (\'Bid\',\'Lost\',\'Closed\')');
    }

    static testMethod void testSearchType(){
        init();
        initRoles();
        String pstDomain = domains[1].Domain_Id__c;
        String internalDomain = domains[0].Domain_Id__c;
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.courseType = 'Select Course Type';
        c.searchType();
        System.assert(c.searchFilter == 'and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' and ' + 
    		'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')');

        c.courseType = 'PRA Courses Only';
        c.searchType();
        System.assert(c.searchFilter == 'and Type__c NOT IN (\'PST\',\'SOP\') and Domain_Id__c = \'' + internalDomain + '\' and ' + 
    		'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')');

        c.courseType = 'PRA SOPs Only';
        c.searchType();
        System.assert(c.searchFilter == 'and Type__c = \'SOP\' and Domain_Id__c = \'' + internalDomain + '\' and ' + 
    		'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')');

        c.courseType = 'Project Specific Only';
        c.clientText = clients[0].Name;
        c.projectText = '';
        c.projectRole = projectRoles[0].Name;
        c.search();
        c.searchType();
        System.assert(c.searchFilter == 'and Client_Id__c = \'' + c.clientText + '\' and Project_Id__c = null and ' + 
		    'Domain_Id__c IN (\'' + pstDomain + '\',\'' + internalDomain + '\') and ' + 
		    '(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')');

        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = projectRoles[0].Name;
        c.search();
        c.searchType();
        System.assert(c.searchFilter == 'and (Project_Id__c = \'' + c.projectText + '\' or ' + 
		    '(Client_Id__c = \'' + c.clientText + '\' and Project_Id__c = null)) and ' + 
		    'Domain_Id__c IN (\'' + pstDomain + '\',\'' + internalDomain + '\') and ' + 
		    '(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')');
    }

    static testMethod void testProjectRoleLOV(){
        init();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        List<SelectOption> options = c.getProjectRoles();
        System.assert(options.size() == 5 && options[1].getValue() == 'class1');
    }

    static testMethod void testCourseTypes(){
        init();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        List<SelectOption> options = c.getCourseTypes();
        System.assert(options.size() == 5 && options[0].getValue() == 'Select Course Type' &&
                      options[1].getValue() == 'PRA Courses Only' && options[2].getValue() == 'PRA SOPs Only' && 
                      options[3].getValue() == 'Project Specific Only' && options[4].getValue() == 'Sponsor Training');
    }

    static testMethod void testRoleSearchNoCourses() {
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = clients[2].Name;
        c.projectText = projects[2].Name;
        c.projectRole = projectRoles[3].Name;
        c.search();
        System.assert(c.courses.size() == 0);
    }

    static testMethod void testRoleSearchSuccess() {
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = projectRoles[0].Name;
        c.search();
        System.assert(c.courses.size() == 2);
    }

    static testMethod void testRoleSearchSuccessInactive() {
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = clients[1].Name;
        c.projectText = projects[1].Name;
        c.projectRole = projectRoles[1].Name;
        c.search();
        System.assert(c.courses.size() == 0);
    }

    static testMethod void testRoleSearchSuccessLost() {
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = clients[1].Name;
        c.projectText = projects[1].Name;
        c.projectRole = projectRoles[2].Name;
        c.search();
        System.assert(c.courses.size() == 0);
    }

    static testMethod void testRoleSearchSuccessWithDraftDelete() {
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = projectRoles[0].Name;
        c.search();
        System.assert(c.courses[0].assignmentStatus == 'Draft' && c.courses[1].assignmentStatus == 'Draft Delete');
    }

    static testMethod void testRoleSearchFail(){
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = 'Client5';
        c.projectText = '';
        c.projectRole = 'class5';
        c.search();
        System.assert(c.courses == null);
    }

    static testMethod void testRoleSearchFailRoleName(){
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.roleName = 'CPRole Zero';
        c.search();
        System.assert(c.courses == null && c.createRoleError == 'Role not found, please enter a different role name');
    }

    static testMethod void testCourseSearchSuccessProjectID(){
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = initSuccessRoleSearch();
        c.search();
        c.courseType = 'Project Specific Only';
        c.courseText = '';
        c.blnUpdated = true;
        c.courseSearch();
        System.assert(c.selCourses.size() == 1);
    }

    static testMethod void testCourseSearchSuccessClientID(){
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = initSuccessRoleSearchNoProject();
        c.search();
        c.courseType = 'Project Specific Only';
        c.courseText = '';
        c.blnUpdated = true;
        c.courseSearch();
        System.assert(c.selCourses.size() == 0);
    }

    static testMethod void testCourseSearchSuccessCourseCode(){
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = initSuccessRoleSearch();
        c.search();
        c.courseType = 'SOPs Only';
        c.courseText = 'Course 1';
        c.blnUpdated = false;
        c.courseSearch();
        System.assert(c.selCourses.size() == 1);
    }

    static testMethod void testCourseSearchSuccessWithUpdatedDates() {
        init();
        initRoles();
        initMappings();
        Datetime dtLess = Datetime.now().addDays(-2);
        Datetime dtMore = Datetime.now().addDays(2);
        LMS_CourseToProjectController c = initSuccessRoleSearch();
        c.search();
        c.courseType = 'SOPs Only';
        c.courseText = 'Course 1';
        c.courseUpdateStart = dtLess.month() + '/' + dtLess.day() +  '/' + dtLess.year();
        c.courseUpdateEnd = dtMore.month() + '/' + dtMore.day() +  '/' + dtMore.year();
        c.blnUpdated = false;
        c.courseSearch();
        System.assert(c.selCourses.size() == 1);
    }

    static testMethod void testCourseSearchFailure(){
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = initSuccessRoleSearch();
        c.search();
        c.courseType = 'PRA Courses Only';
        c.courseText = '';
        c.blnUpdated = false;
        c.courseSearch();
        System.assert(c.selCourses.size() == 0);
    }

    static testMethod void testCourseSearchManyError() {
        init();
        initRoles();
        initMappings();
        initManyCourses();
        LMS_CourseToProjectController c = initSuccessRoleSearch();
        c.search();
        c.courseType = 'SOPs Only';
        c.blnUpdated = false;
        c.courseSearch();
        System.assert(c.selCourses.size() == 0);
    }

    static testMethod void testCreateRole(){
        init();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = projectRoles[0].Name;
        PageReference pr = c.createRole();
        System.assert(pr == null);
    }

    static testMethod void testCancelCreateRole() {
        init();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        PageReference pr = c.cancelCreateRole();
        System.assert(pr == null && c.roles == null);
    }

    static testMethod void testSendRoleData() {
        init();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = clients[1].Name;
        c.projectText = projects[1].Name;
        c.projectRole = projectRoles[3].Name;
        c.search();
        c.createRole();
        PageReference pr = c.sendRoleData();
        System.assert(pr == null);
    }

    static testMethod void testAddCourse(){
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = initSuccessRoleSearch();
        c.search();
        c.courseType = 'SOPs Only';
        c.courseText = 'Course 1';
        c.courseSearch();
        PageReference pr = c.addCourse();
        System.assert(pr == null);
    }

    static testMethod void testAddCourse2(){
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = initSuccessRoleSearch();
        c.search();
        c.courseType = 'SOPs Only';
        c.courseText = 'Course 1';
        c.courseSearch();
        c.selCourses[0].selected = false;
        PageReference pr = c.addCourse();
        System.assert(pr == null);
    }

    static testMethod void testCancel(){
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = initSuccessRoleSearch();
        c.search();
        PageReference pr = c.cancel();
        System.assert(pr == null);
    }

    static testMethod void testAddCommit(){
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = initSuccessRoleSearch();
        c.search();
        PageReference pr = c.commitCourses();
        System.assert(pr == null);
    }

    static testMethod void testApplyCommitDate(){
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = initSuccessRoleSearch();
        c.search();

        c.courses[0].selected = true;
        c.commitDate = '12/12/2102';
        PageReference pr = c.setCommitDate();
        System.assert(pr == null);
    }

    static testMethod void testRemoveCourse(){
        init();
        initRoles();
        initMappings();
        LMS_CourseToProjectController c = initSuccessRoleSearch();
        c.search();
        for(LMS_CourseAssignments ca : c.courses) {
            ca.selected = true;
        }
        PageReference pr = c.removeCourses();
        System.assert(pr == null);
    }

    static testMethod void testIsValidClientId() {
        init();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = clients[0].Name;
        Boolean isValid = LMS_CourseToProjectController.isValidClientId(c.clientText);
        System.assert(isValid == true);

        c.clientText = 'invalidClient';
        isValid = LMS_CourseToProjectController.isValidClientId(c.clientText);
        System.assert(isValid == false);
    }

    static testMethod void testIsValidProjectId() {
        init();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.projectText = projects[0].Name;
        Boolean isValid = LMS_CourseToProjectController.isValidProjectId(c.projectText);
        System.assert(isValid == true);

        c.projectText = 'invalidProject';
        isValid = LMS_CourseToProjectController.isValidProjectId(c.projectText);
        System.assert(isValid == false);
    }

    static testMethod void testIsValidClientProjectPair() {
        init();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        Boolean isValid = LMS_CourseToProjectController.isValidClientProjectPair(c.clientText, c.projectText);
        System.assert(isValid == true);

        c.clientText = 'invalidClient';
        c.projectText = projects[0].Name;
        isValid = LMS_CourseToProjectController.isValidClientProjectPair(c.clientText, c.projectText);
        System.assert(isValid == false);

        c.clientText = clients[0].Name;
        c.projectText = 'invalidProject';
        isValid = LMS_CourseToProjectController.isValidClientProjectPair(c.clientText, c.projectText);
        System.assert(isValid == false);
    }

    static testMethod void testShowErrors() {
        init();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        PageReference newPage = c.showErrors();
        System.assert(newPage.getUrl() == '/apex/LMS_CourseProjectError');
    }

    static testMethod void testViewImpact() {
        init();
        initRoles();
        initMappings();
        initEmployees();
        LMS_CourseToProjectController c = new LMS_CourseToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = projectRoles[0].Name;
        c.search();
        c.courseType = 'Project Specific Only';
        c.courseText = 'Course 2';
        c.blnUpdated = false;
        c.courseSearch();
        System.debug(c.selCourses);
        c.selCourses[0].selected = true;
        c.viewImpact();
        System.assert(c.selCourses.size() == 1);
    }
}