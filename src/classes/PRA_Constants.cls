/* 
*  Constants for use in PRA (PnF)
*/
public with sharing class PRA_Constants {
    /*
    *   Object: Client Project
    *    Field: Status
    *   Picklist values
    */
    public static final String CP_STATUS_NOT_STARTED = 'Not Started';
    public static final String CP_STATUS_IN_PROGRESS = 'In Progress';
    public static final String CP_STATUS_COMPLETE    = 'Complete';
    public static final String CP_STATUS_ON_HOLD     = 'On Hold';
    public static final String CP_STATUS_IN_REVIEW   = 'In Review';
    public static final String CP_STATUS_RM          = 'RM';
    
    /*
    *   Object: Client Task
    *    Field: Status
    *   These values are used to keep track of the flow of WBS data through P&F
    */
    public static final String CU_STATUS_NEW         = 'New';
    public static final String CU_STATUS_READY       = 'Ready';
    public static final String CU_STATUS_SENT        = 'Sent';
    public static final String CU_STATUS_ACK         = 'Ack';
    
    //used this ALL constant to get for all the projects status
    public static final String CP_ALL                = 'ALL';
    
    /*  
    *   These values are used to keep track of the flow of Pge Modes in different visualforce pages.
    */
    public static final String PAGE_MODE_VIEW                         = 'View'; 
    public static final String PAGE_MODE_EDIT                         = 'Edit';
    public static final String ST_NONE                                = 'None';
    public static final String ST_DEFAULT_PROJECT_LABEL               = 'Home';
    
    /*  
    *   Migrated Worked Hour client task.
    */
    public static final String MIGRATED_CLIENT_TASK                   = 'Migrated Worked Hours';
    
    /*  
    *   The value is used to filter out the task group used for on PF projects like (full service).
    */
    public static final String TG_PF                                  = 'Full Service'; 
    
    
    public PRA_Constants(){
    }
}