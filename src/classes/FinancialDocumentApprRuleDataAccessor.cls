/** Implements the Selector Layer of the object FinancialDocumentApprRule__c
 * @author	Dimitrios Sgourdos
 * @version	16-Dec-2013
 */
public with sharing class FinancialDocumentApprRuleDataAccessor {
	
	// Global Constants
	public static final String	PROPOSAL_VALUE_MORE = 'Value of proposal more than';
	public static final String	TOTAL_VALUE_MORE	= 'Value of total proposal more than';
	public static final String	DELTA_MORE			= 'Delta more than';
	public static final String	NUM_OF_CURRENCIES	= 'Number of currencies more than';
	public static final String	PAYMENT_TERMS		= 'Payment terms exceed';
	
	
	/** Object definition for fields used in application for FinancialDocumentApprRule
	 * @author	Dimitrios Sgourdos
	 * @version 10-Dec-2013
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('FinancialDocumentApprRule__c');
	}
	
	
	/** Object definition for fields used in application for FinancialDocumentApprRule with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 16-Dec-2013
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'Description__c,';
		result += referenceName + 'ListOfServiceCategories__c,';
		result += referenceName + 'RuleFirstParameter__c,';
		result += referenceName + 'RuleSecondParameter__c,';
		result += referenceName + 'SelectedRule__c,';
		result += referenceName + 'SequenceNumber__c';
		return result;
	}
	
	
	/** Retrieve the all the Financial Document Approval Rules that have been set in the system.
	 * @author	Dimitrios Sgourdos
	 * @version 10-Dec-2013
	 * @return	The Financial Document Approval Rules list.
	 */
	public static List<FinancialDocumentApprRule__c> getFinancialDocumentApprRules() {
		String query = String.format(
								'SELECT {0} ' +
								'FROM FinancialDocumentApprRule__c ' +
								'ORDER BY SequenceNumber__c',
								new List<String> {
									getSObjectFieldString()
								}
							);
		return (List<FinancialDocumentApprRule__c>) Database.query(query);
	}
	
}