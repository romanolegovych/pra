public with sharing class BDT_FinancialDocPaymentController {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} //Financial Documents Payment
	public Boolean 									showPage 						{get;set;}
	public Boolean									showSavePage					{get;set;}
	public	Boolean									invalidDocFlag					{get;set;}
	public	Boolean									approvedDocFlag					{get;set;}
	Boolean											previousPaymentTermsComplete;
	public Boolean									completeFlag					{get;set;}
	public FinancialDocument__c 					financialDocument				{get;set;}
	public List<mainTableRow>						mainTableRowsList				{get;set;}
	List<Study__c>									selProposalStudies;
	public List<studySelectionRow>					stSelRowPropStudies				{get;set;}
	public String									delRowPaymentTerm				{get;set;}
	public String									columnPaymentTerm				{get;set;}
	public String									linkedPaymentIndex				{get;set;}
	public String									commentValue					{get;set;}
	List<FinancialDocumentPaymentTerm__c> 			DeletionPaymentTermList;
	public List<SelectOption>						mileStonesSelectList			{get;set;}
	
	
	// The constructor of the class
	public BDT_FinancialDocPaymentController() {
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Financial Documents Payment');
		String tmpfinancialDocumentId;
		List<Study__c> tmpProposalStudies = new List<Study__c>();
		studySelectionRow stSelRowItem 	  = new studySelectionRow();
		List<FinancialDocumentPaymentTerm__c> tmpFinDocPaymentTermList = new List<FinancialDocumentPaymentTerm__c>();
		initializeGlobalVariables();
		// Find the current financial Document
		tmpfinancialDocumentID = system.currentPageReference().getParameters().get('financialDocumentID');
		if( String.isEmpty(tmpfinancialDocumentID) ) {
			showPage = false;
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected proposal.Please, select a proposal and reload the page.');
			ApexPages.addMessage(msg);
			return;
		} else {
			// Find the financial document and studies
			financialDocument = FinancialDocumentService.getFinancialDocumentById(tmpfinancialDocumentID);
			if( String.isBlank(financialDocument.Name) ) {
				showPage = false;
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected proposal.Please, select a proposal and reload the page.');
				ApexPages.addMessage(msg);
				return;
			}
			tmpProposalStudies = BDT_DC_FinancialDocument.readProjectStudies(financialDocument.Client_Project__c);
			previousPaymentTermsComplete = financialDocument.PaymentTermsComplete__c;
			completeFlag = financialDocument.PaymentTermsComplete__c;
			invalidDocFlag = (financialDocument.DocumentStatus__c == FinancialDocumentDataAccessor.DOCUMENT_STATUS_INVALID);
			approvedDocFlag = (financialDocument.DocumentStatus__c == FinancialDocumentDataAccessor.DOCUMENT_STATUS_APPROVED);
			// Find the selected studies for this financial document
			if( ! String.isEmpty(financialDocument.listOfStudyIds__c) ) {
				for(Study__c tmpStudy : tmpProposalStudies) {
					stSelRowItem = new studySelectionRow();
					if(financialDocument.listOfStudyIds__c.contains(tmpStudy.id)) {
						stSelRowItem.proposalStudy = tmpStudy;
						stSelRowItem.studyLbl = tmpStudy.Code__c.substringAfterLast('-');
						stSelRowPropStudies.add(stSelRowItem);
						selProposalStudies.add(tmpStudy);
					}
				}  
			}
			// Read the FinancialDocumentPaymentTerm and create the main rows of the table
			tmpFinDocPaymentTermList =  [SELECT Id,
												Description__c,
												StudyIdPercentageJSON__c,
												PaymentTerm_Number__c,
												LinkedToMileStone__c
										 FROM	FinancialDocumentPaymentTerm__c
										 WHERE	FinancialDocument__c = :tmpfinancialDocumentID
										 ORDER BY PaymentTerm_Number__c];
			List<String> tmpSelStudiesIds = new List<String>();
			if(! String.isEmpty(financialDocument.listOfStudyIds__c)) {
				tmpSelStudiesIds = financialDocument.listOfStudyIds__c.split(':');
			}
			mileStonesSelectList = BDT_StudyMileStoneService.getMileStonesOptionsForPaymentTerms(tmpSelStudiesIds);
			createMainTableRows(tmpFinDocPaymentTermList);
		}
	}
	
	
	// It initializes the global variables of the controller
	void initializeGlobalVariables() {
		showPage 	 = true;
		showSavePage = false;
		mainTableRowsList 		= new List<mainTableRow>();
		stSelRowPropStudies   	= new List<studySelectionRow>();
		selProposalStudies    	= new List<Study__c>();
		DeletionPaymentTermList = new List<FinancialDocumentPaymentTerm__c>();
		mileStonesSelectList	= new List<SelectOption>();
	}
	
	
	// It creates the rows of the table
	void createMainTableRows(List<FinancialDocumentPaymentTerm__c> tmpFinDocPaymentTermList) {
		Integer tmpCnt = 0;
		mainTableRow tmpTableRow = new mainTableRow();
		List<StudyPercentJson> tmpStPercentJsonList = new List<StudyPercentJson>();
		// Create a table row for each payment term
		for(FinancialDocumentPaymentTerm__c tmpObject : tmpFinDocPaymentTermList) {
			tmpCnt++;
			tmpObject.PaymentTerm_Number__c = tmpCnt;
			tmpTableRow = new mainTableRow();
			tmpTableRow.finDocPaymentTermObject = tmpObject;
			tmpTableRow.notLinkedToMileStone = ! tmpObject.LinkedToMileStone__c;
			if (tmpObject.StudyIdPercentageJSON__c == null) {
				tmpObject.StudyIdPercentageJSON__c = '[{ "studyId":"-" , "percentage":"-" }]';
			}
			tmpStPercentJsonList = (List<StudyPercentJson>) JSON.deserialize(tmpObject.StudyIdPercentageJSON__c, List<StudyPercentJson>.class);
			tmpTableRow.stPercentTableRow = adjustWithCurrentSelectedStudies(tmpStPercentJsonList); 
			mainTableRowsList.add(tmpTableRow);
		}
		if(mainTableRowsList.size()==0) {
			addRow(); 
		}
		calculateSums();
	}
	
	
	// It adjusts the study percentages
	List<StudyPercentJson> adjustWithCurrentSelectedStudies(List<StudyPercentJson> previousList) {
		List<StudyPercentJson> finalList  = new List<StudyPercentJson>();
		StudyPercentJson	   tmpRowData = new StudyPercentJson();
		Map<String, String>	   tmpJsonMap = new Map<String, String>();
		// create the map
		for(StudyPercentJson tmpObject : previousList) {
			tmpJsonMap.put(tmpObject.studyId, tmpObject.percentage);
		}
		// For each selected study
		for(Study__c tmpStudy : selProposalStudies) {
			tmpRowData = new StudyPercentJson();
			tmpRowData.studyId	  = tmpStudy.id;
			tmpRowData.percentage = tmpJsonMap.get(tmpStudy.id);
			tmpRowData.percentage = ( String.isBlank(tmpRowdata.percentage)? '-' : tmpRowdata.percentage);
			finalList.add(tmpRowData);
		}
		return finalList;
	}
	
	
	// It adds a row in the table
	public void addRow() {
		mainTableRow tmpTableRow = new mainTableRow();
		List<StudyPercentJson> tmpStPercentJsonList = new List<StudyPercentJson>();
		// Create FinancialDocumentPaymentTerm__c object
		FinancialDocumentPaymentTerm__c tmpObject = new FinancialDocumentPaymentTerm__c();
		tmpObject.FinancialDocument__c = financialDocument.Id;
		tmpObject.Description__c 	   = '';
		tmpObject.StudyIdPercentageJSON__c = '[{ "studyId":"-" , "percentage":"-" }]';
		tmpObject.PaymentTerm_Number__c = mainTableRowsList.Size() + 1;
		// Create table row
		tmpTableRow.finDocPaymentTermObject = tmpObject;
		tmpStPercentJsonList = (List<StudyPercentJson>) JSON.deserialize(tmpObject.StudyIdPercentageJSON__c, List<StudyPercentJson>.class);
		tmpTableRow.stPercentTableRow  = adjustWithCurrentSelectedStudies(tmpStPercentJsonList);
		mainTableRowsList.add(tmpTableRow);
		calculateSums();
	}
	
	
	// It calculates the inserted sums
	void calculateSums() {
		Decimal sumResult;
		String  tmpStr;
		for(Integer stCnt=0; stCnt<stSelRowPropStudies.size(); stCnt++) {
			sumResult = 0;
			for(Integer rowCnt=0; rowCnt<mainTableRowsList.size(); rowCnt++) {
				tmpStr = mainTableRowsList[rowCnt].stPercentTableRow[stCnt].percentage;
				if(tmpStr!='-' && (!String.isEmpty(tmpStr)) ) {
					sumResult += Decimal.valueOf(tmpStr);
				}
			}
			stSelRowPropStudies[stCnt].totalPercentage = BDT_Utils.removeTailingZerosFromDecimals(String.valueOf(sumResult));
		}
	}
	
	
	// It deletes a row from the table
	public void DeletePaymentTerm() {
		List<FinancialDocumentPaymentTerm__c> tmpList;
		Integer tmpInt = Integer.valueOf(delRowPaymentTerm);
		// If the record already exists in the Object, add it to the deletion list
		if(mainTableRowsList[tmpInt].finDocPaymentTermObject.id != NULL) {
			DeletionPaymentTermList.add(mainTableRowsList[tmpInt].finDocPaymentTermObject);
		}
		mainTableRowsList.remove(tmpInt);
		if(mainTableRowsList.isEmpty()) {
			addRow();
		} else { // Cause in addRow, the function calculateSums is called 
			calculateSums();	
		} 
	}
	
	
	// It calculates the sum for the column that has been edited
	public void calculateColumnSums() {
		Integer stCnt  = Integer.valueOf(columnPaymentTerm);
		Decimal sumResult = 0;
		String  tmpStr;
		for(Integer rowCnt=0; rowCnt<mainTableRowsList.size(); rowCnt++) {
			tmpStr = mainTableRowsList[rowCnt].stPercentTableRow[stCnt].percentage; 
			if(tmpStr!='-' && (!String.isEmpty(tmpStr)) ) {
				sumResult += Decimal.valueOf(tmpStr);
			}
		}
		stSelRowPropStudies[stCnt].totalPercentage = BDT_Utils.removeTailingZerosFromDecimals(String.valueOf(sumResult));
	}
	
	
	// It saves the Payment Terms
	public void save() {
		calculateSums();
		if(validateData()) {
			if(completeFlag) {
				commentValue = '';
				showPage 	 = false;
				showSavePage = true;
			} else {
				actualSave();
				showPage 	 = false;
			}
		}else {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The percentage sum of each study must be equal to 100. Please, correct the entries and try again.');
			ApexPages.addMessage(msg);
		}
	}	
	
	
	// It validates if the inserted data are correct
	boolean validateData() {
		for(studySelectionRow tmpItem : stSelRowPropStudies) {
			tmpItem.totalPercentage = (tmpItem.totalPercentage == null)? '' : tmpItem.totalPercentage;
			if( ! tmpItem.totalPercentage.equals('100')) {
				return false;
			}
		}
		return true;
	}
	
	
	// It actually saves the changes in Payment Terms
	void actualSave() {
		List<FinancialDocumentPaymentTerm__c> UpdatePaymentTermList = new List<FinancialDocumentPaymentTerm__c>();
		try {
			financialDocument.PaymentTermsComplete__c = completeFlag;
			
			// Firstly Delete the Payment terms that were deleted
			if( DeletionPaymentTermList.size() > 0) {
				delete DeletionPaymentTermList;
			}
			
			// Update the payment terms
			UpdatePaymentTermList = createTheUpdates();
			if( UpdatePaymentTermList.size() > 0 ) {
				UpdatePaymentTermList = renumberPaymentTermList(UpdatePaymentTermList);
				upsert UpdatePaymentTermList;
			}
			
			// Update the FinancialDocument, if needed
			if( previousPaymentTermsComplete != financialDocument.PaymentTermsComplete__c) {
				update financialDocument;
			}
			
			// Send e-mail notifications for adjustments
			FinancialDocumentApprovalService.sendAdjustmentsEmailNotifications(financialDocument,
																FinancialDocumentHistoryDataAccessor.PAYMENT_PROCESS,
																userInfo.getName());
			
			showPage     = false;
			showSavePage = false;
		} catch (Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to save the changes. Please, reload the page and try again.');
			ApexPages.addMessage(msg);
		}
	}
	
	
	// It creates the records that must be inserted/updated in the Payment Terms Object
	List<FinancialDocumentPaymentTerm__c> createTheUpdates() {
		List<FinancialDocumentPaymentTerm__c> tmpUpdatePaymentTermList = new List<FinancialDocumentPaymentTerm__c>();
		for(mainTableRow tmpTableRow : mainTableRowsList) {
			if( tmpTableRow.finDocPaymentTermObject.Description__c != null  && 
				tmpTableRow.finDocPaymentTermObject.Description__c!='') {
				for(studyPercentJson tmpItem : tmpTableRow.stPercentTableRow) {
					if( String.isEmpty(tmpItem.Percentage) ) { // check for empty values
						tmpItem.Percentage = '-';
					}
				}
				tmpTableRow.finDocPaymentTermObject.StudyIdPercentageJSON__c = JSON.serialize(tmpTableRow.stPercentTableRow);		
				tmpTableRow.finDocPaymentTermObject.LinkedToMileStone__c = ! tmpTableRow.notLinkedToMileStone;
				tmpUpdatePaymentTermList.add(tmpTableRow.finDocPaymentTermObject);
			}
		}
		return tmpUpdatePaymentTermList;
	}
	
	
	// It renumber the given Payment Terms List
	List<FinancialDocumentPaymentTerm__c> renumberPaymentTermList(List<FinancialDocumentPaymentTerm__c> tmpUpdatePaymentTermList){
		tmpUpdatePaymentTermList = BDT_Utils.sortListNumeric(tmpUpdatePaymentTermList, 'PaymentTerm_Number__c'); 
		Decimal val=1;
		for (FinancialDocumentPaymentTerm__c tmpItem : tmpUpdatePaymentTermList) {
			tmpItem.PaymentTerm_Number__c = val;
			val++; 
		}
		return tmpUpdatePaymentTermList;
	}		
	
	
	
	// It saves the Payment Terms and the comment for the changes for the History
	public void commentSave() {
		if( commentValue!=null && commentValue!='' ) {
			// Update the history of the financial document
			String changeDescription = '';
			String totalValueString = '';
			if(financialDocument.TotalValue__c != null) {
				totalValueString = financialDocument.TotalValue__c;
			} 
			FinancialDocumentHistoryService.saveHistoryObject(financialDocument.id, commentValue, changeDescription, totalValueString, 'Payment');
			actualSave();
		} else {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'A comment is mandatory. Please, enter a comment.');
			ApexPages.addMessage(msg);
		}
	}
	
	
	// It closes the Payment Term
	public void closePage() {
		showPage = false;
	}
	
	
	// It closes the comment section
	public void closeComment() {
		showPage 	 = true;
		showSavePage = false;
	}
	
	
	/** Link a payment term to a milestone or not.
	 * @author	Dimitrios Sgourdos
	 * @version 28-Feb-2014
	 */
	public void linkPaymentToMileStone() {
		Integer tmpInt = Integer.valueOf(linkedPaymentIndex);
		mainTableRowsList[tmpInt].finDocPaymentTermObject.Description__c = NULL;
	}
	
	
	// usefull classes for the json object for keeping the table data
	public class studySelectionRow {
		public Study__c	proposalStudy{get;set;}
		public String	studyLbl{get;set;}
		public String	totalPercentage{get;set;}
	}
	
	public class StudyPercentJson {
		public String studyId{get;set;}
		public String percentage{get;set;}
	}
	
	public class mainTableRow {
		public FinancialDocumentPaymentTerm__c  finDocPaymentTermObject	{get;set;}
		public Boolean							notLinkedToMileStone	{get;set;}
		public List<StudyPercentJson>			stPercentTableRow		{get;set;}
	}
		
}