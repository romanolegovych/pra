@isTest
private class BDT_LabMethodsControllerTest {

    static testMethod void myUnitTest() { 
    	// Create the default records for the method lists
		List<LaboratorySelectionLists__c> tmpList = new List<LaboratorySelectionLists__c>();
		tmpList.add(new LaboratorySelectionLists__c(Name='Amount/Volume Units'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Amount/Volume Units', listFirstValue__c='g'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Analytical Techniques'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Analytical Techniques', listFirstValue__c='ELISA'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Analytical Techniques', listFirstValue__c='LC'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Anti-coagulants'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Anti-coagulants', listFirstValue__c='N/A'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Anti-coagulants', listFirstValue__c='K2-EDTA'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Compound Classes'));  
		tmpList.add(new LaboratorySelectionLists__c(Name='Compound Classes', listFirstValue__c='PD'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Concentration Units'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Concentration Units', listFirstValue__c='ng/mL'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Detection Techniques'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Detection Techniques', listFirstValue__c='MS/MS'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Detection Techniques', listFirstValue__c='N/A'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Detection Types'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Detection Types', listFirstValue__c='API3000'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Matrices'));  
		tmpList.add(new LaboratorySelectionLists__c(Name='Matrices', listFirstValue__c='Bile'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Matrices', listFirstValue__c='Blood'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Sample Preparation Techniques'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Sample Preparation Techniques', listFirstValue__c='Dilution', listSecondValue__c='Dil'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Species'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Species', listFirstValue__c='Beagle'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Storage Temperatures'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Storage Temperatures', listFirstValue__c='room temperature'));
		insert tmpList;
		// Create Therapeutic Area
		TherapeuticArea__c tmpTherapeuticItem = new TherapeuticArea__c(Name='Cardiology/Vascular Diseases');
		insert tmpTherapeuticItem;
		// Create the controller
		BDT_LabMethodsController p = new BDT_LabMethodsController();
		// Check add/delete compound from search criteria
		p.addCompoundOption();
		system.assertEquals(p.CompoundItemsList.size(), 2);
		p.delRowCompound = '0';
		p.deleteSearchCompound();
		system.assertEquals(p.CompoundItemsList.size(), 1);
		p.deleteSearchCompound();
		system.assertEquals(p.CompoundItemsList.size(), 1); // cause the list is empty so it automatically adds a compound
		// check Matrix and anti-coagulant 
		p.labMethod.Matrix__c = 'Blood';
		p.labMethod.AntiCoagulant__c = 'K2-EDTA';
		p.assignMatrixAndAntiCoagulant();
		p.labMethod.Matrix__c = 'Bile';
		p.assignMatrixAndAntiCoagulant();
		system.assertEquals(p.labMethod.AntiCoagulant__c, 'N/A');
		// check Analytical Technique and Detection
		p.labMethod.AnalyticalTechnique__c = 'LC';
		p.labMethod.Detection__c = 'MS/MS';
		p.assignAnalyticalTechAndDetectionTech();
		p.labMethod.AnalyticalTechnique__c = 'ELISA';
		p.assignAnalyticalTechAndDetectionTech();
		system.assertEquals(p.labMethod.Detection__c, 'N/A');
		// check clear
		p.clearSearchOptions();
		system.assertEquals(p.labMethod.Matrix__c, NULL);
		// check create new
		for(Integer i=0; i<22; i++) {
			p.addCompoundOption();
		}
		p.createNewMethod(); 
		system.assertEquals(p.resultsLabMethodList.size(), 0);  // check the limitation of 20 compounds
		p.clearSearchOptions();
		p.createNewMethod(); 
		system.assertEquals(p.resultsLabMethodList.size(), 0);  // check that no all values have been inserted
		p.CompoundItemsList[0].Name = '0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789 0123456789';
		p.labMethod.Species__c = 'Beagle';
		p.labMethod.Matrix__c = 'Blood';
		p.assignMatrixAndAntiCoagulant();
		p.labMethod.AntiCoagulant__c = 'K2-EDTA';
		p.labMethod.AnalyticalTechnique__c = 'LC';
		p.assignAnalyticalTechAndDetectionTech();
		p.labMethod.Detection__c = 'MS/MS';
		p.labMethod.Location__c = 'PRA-NL';
		p.labMethod.Department__c = 'SML';
		p.labMethod.Proprietary__c = 'Y';
		p.createNewMethod();
		system.assertEquals(p.resultsLabMethodList.size(), 0);  // check the limitation of 80 characters for the name per compound
		p.CompoundItemsList[0].Name = 'test compound 1';
		p.addCompoundOption();
		p.CompoundItemsList[1].Name = 'test compound 2';
		p.createNewMethod();
		system.assertEquals(p.resultsLabMethodList.size(), 1);  // check that the creation and automatically search were successfull
		// Test the editing of a method
		p.selectedMethodId = p.resultsLabMethodList[0].Id;
		p.editSelectedMethod();
		p.cancelEdit();
		p.selectedMethodId = p.resultsLabMethodList[0].Id;
		p.editSelectedMethod();
		p.selLabMethodItem.Matrix__c = 'Bile'; 
		p.assignMatrixAndAntiCoagulant();
		p.selLabMethodItem.AnalyticalTechnique__c = 'ELISA';
		p.assignAnalyticalTechAndDetectionTech();
		// Add the comedication
		p.addCoMedicationOption();
		system.assertEquals(p.labComedWrapItemList.size(), 2);
		p.delRowCoMedic = '0';
		p.deleteCoMedicationOption();
		system.assertEquals(p.labComedWrapItemList.size(), 1);
		p.deleteCoMedicationOption();
		system.assertEquals(p.labComedWrapItemList.size(), 1); // cause the list is empty so it automatically adds a compound
		p.addCoMedicationOption();
		p.saveEdit();
		p.labComedWrapItemList[0].coMedicationItem.Assessment__c = 'Assessment test 1';
		p.selLabMethodItem.Matrix__c = NULL;
		p.saveEdit();
		p.selLabMethodItem.Matrix__c = 'Bile';
		p.saveEdit();
		// Edit again but now there is one co-medications assigned
		p.editSelectedMethod();
		// Export to Excel
		p.showExportOptions();
		p.cancelExportToExcel();
		p.showExportOptions();
		p.exportToExcel();
    }
}