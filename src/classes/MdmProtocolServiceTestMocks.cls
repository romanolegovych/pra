/**
 * Mock for Mdm Protocol Service class
 */
@isTest
public class MdmProtocolServiceTestMocks implements WebServiceMock {
	
	public boolean shouldCreateError {get;set;}
	
	private static final String GET_PROTOCOLS_LIKE_TYPE = 'MdmProtocolService.getProtocolVOsLikeClientProtNumResponse';
	private static final String GET_PROTOCOL_FROM_CP_TYPE = 'MdmProtocolService.getProtocolVOByClientProtNumResponse';
	private static final String GET_PROTOOCL_FROM_SYSID_TYPE = 'MdmProtocolService.getProtocolVOBySystemIdResponse';
	private static final String GET_PROTOCOLS_NOT_MAPPED_TYPE = 'MdmProtocolService.getProtocolsNotMappedToSiteResponse';
	private static final String GET_PROTOCOLS_NOT_MAPPED_BY_SYSID_TYPE = 'MdmProtocolService.getProtocolsNotMappedToSiteBySystemIdResponse';
	private static final String GET_EDOCS_URL_FROM_CP_TYPE = 'MdmProtocolService.getEDocsUrlByClientProtNumResponseResponse';
	private static final String GET_EDOCS_URL_FROM_PROT_TYPE = 'MdmProtocolService.getEDocsUrlForProtocolResponse';
	private static final String SAVE_PROTOCOL_FROM_VO_TYPE = 'MdmProtocolService.saveProtocolFromVOResponse';
	private static final String DELETE_PROTOCOL_FROM_VO_TYPE = 'MdmProtocolService.deleteProtocolFromVOResponse';
	private static final String SAVE_REGION_MAPPING_FOR_PROT_TYPE = 'MdmProtocolService.saveRegionMappingForProtocolResponseResponse';
	
	public MdmProtocolServiceTestMocks(boolean doError) {
		this.shouldCreateError = doError;
		system.debug('----------- Mocking a MdmProtocolService method request -----------------');
	}
	
	public void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
		String requestName, String responseNS, String responseName, String responseType) {	
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}	
		if (responseType.equals(GET_PROTOCOLS_LIKE_TYPE)) {
			MdmProtocolService.getProtocolVOsLikeClientProtNumResponse resp = createMockProtocolLikeResponse();
			response.put('response_x', resp);
		} else if (responseType.equals(GET_PROTOCOL_FROM_CP_TYPE)) {
			MdmProtocolService.getProtocolVOByClientProtNumResponse resp = createMockProtocolVOFromClientProtNumResponse();
			response.put('response_x', resp);
		} else if (responseType.equals(GET_PROTOOCL_FROM_SYSID_TYPE)) {
			MdmProtocolService.getProtocolVOBySystemIdResponse resp = createMockProtocolVOFromSystemIdResponse();
			response.put('response_x', resp);
		} else if (responseType.equals(GET_PROTOCOLS_NOT_MAPPED_TYPE)) {
			MdmProtocolService.getProtocolsNotMappedToSiteResponse resp = createMockProtocolsNotMappedToSiteResponse();
			response.put('response_x', resp);
		} else if (responseType.equals(GET_PROTOCOLS_NOT_MAPPED_BY_SYSID_TYPE)) {
			MdmProtocolService.getProtocolsNotMappedToSiteBySystemIdResponse resp = createMockProtocolsNotMappedToSiteBySystemIdResponse();
			response.put('response_x', resp);
		} else if (responseType.equals(GET_EDOCS_URL_FROM_CP_TYPE)) {
			MdmProtocolService.getEDocsUrlForProtocolResponse resp = createMockGetEDocsUrlForProtocolResponse();
			response.put('response_x', resp);
		} else if (responseType.equals(GET_EDOCS_URL_FROM_PROT_TYPE)) {
			MdmProtocolService.getEDocsUrlByClientProtNumResponseResponse resp = createMockGetEDocsUrlForClientProtNumResponse();
			response.put('response_x', resp);
		} else if (responseType.equals(SAVE_PROTOCOL_FROM_VO_TYPE)) {
			MdmProtocolService.saveProtocolFromVOResponse resp = createMockSaveProtocolFromVOResponse();
			response.put('response_x', resp);
		} else if (responseType.equals(DELETE_PROTOCOL_FROM_VO_TYPE)) {
			MdmProtocolService.deleteProtocolFromVOResponse resp = createMockDeleteProtocolFromVOResponse();
			response.put('response_x', resp);
		} else if (responseType.equals(SAVE_REGION_MAPPING_FOR_PROT_TYPE)) {
			MdmProtocolService.saveRegionMappingForProtocolResponseResponse resp = createMockSaveRegionMappingFromVOResponse();
			response.put('response_x', resp);
		}
		system.debug('----------------------------request------------------------' + request);
		system.debug('----------------------------response------------------------' + response);
	}
	
	/********************* Methods to build main response objects ********************************/
	
	private MdmProtocolService.getProtocolVOsLikeClientProtNumResponse createMockProtocolLikeResponse() {
		MdmProtocolService.getProtocolVOsLikeClientProtNumResponse resp =  new MdmProtocolService.getProtocolVOsLikeClientProtNumResponse();
		MdmProtocolService.protocolListResponse protListResponse = new MdmProtocolService.protocolListResponse();
		protListResponse.errors = null;
		protListResponse.statusCode = '0';
		protListResponse.protocolVOs = createMockProtocolVOList(5);
		resp.GetProtocolVOsLikeClientProtNumResponse = protListResponse;
		return resp;
	}
	
	private MdmProtocolService.getProtocolVOByClientProtNumResponse createMockProtocolVOFromClientProtNumResponse() {
		MdmProtocolService.getProtocolVOByClientProtNumResponse resp =  new MdmProtocolService.getProtocolVOByClientProtNumResponse();
		MdmProtocolService.protocolResponse protocolResponse = new MdmProtocolService.protocolResponse();
		protocolResponse.errors = null;
		protocolResponse.statusCode = '0';
		protocolResponse.protocolVO = createMockProtocolVO(1);
		resp.GetProtocolVOProtNumResponse = protocolResponse;
		return resp;
	}
	
	private MdmProtocolService.getProtocolVOBySystemIdResponse createMockProtocolVOFromSystemIdResponse() {
		MdmProtocolService.getProtocolVOBySystemIdResponse resp =  new MdmProtocolService.getProtocolVOBySystemIdResponse();
		MdmProtocolService.protocolResponse protocolResponse = new MdmProtocolService.protocolResponse();
		protocolResponse.errors = null;
		protocolResponse.statusCode = '0';
		protocolResponse.protocolVO = createMockProtocolVO(1);
		resp.GetProtocolVOSystemIdResponse = protocolResponse;
		return resp;
	}
	
	private MdmProtocolService.getProtocolsNotMappedToSiteResponse createMockProtocolsNotMappedToSiteResponse() {
		MdmProtocolService.getProtocolsNotMappedToSiteResponse resp =  new MdmProtocolService.getProtocolsNotMappedToSiteResponse();
		MdmProtocolService.protocolListResponse protocolListResponse = new MdmProtocolService.protocolListResponse();
		protocolListResponse.errors = null;
		protocolListResponse.statusCode = '0';
		protocolListResponse.protocolVOs = createMockProtocolVOList(5);
		resp.GetProtooclsNotMappedToSiteResponse = protocolListResponse;
		return resp;
	}
	
	private MdmProtocolService.getProtocolsNotMappedToSiteBySystemIdResponse createMockProtocolsNotMappedToSiteBySystemIdResponse() {
		MdmProtocolService.getProtocolsNotMappedToSiteBySystemIdResponse resp =  new MdmProtocolService.getProtocolsNotMappedToSiteBySystemIdResponse();
		MdmProtocolService.protocolListResponse protocolListResponse = new MdmProtocolService.protocolListResponse();
		protocolListResponse.errors = null;
		protocolListResponse.statusCode = '0';
		protocolListResponse.protocolVOs = createMockProtocolVOList(5);
		resp.GetProtocolsNotMappedToSiteBySystemId = protocolListResponse;
		return resp;
	}
	
	private MdmProtocolService.getEDocsUrlForProtocolResponse createMockGetEDocsUrlForProtocolResponse() {
		MdmProtocolService.getEDocsUrlForProtocolResponse resp =  new MdmProtocolService.getEDocsUrlForProtocolResponse();
		MdmProtocolService.valueResponse valueResponse = new MdmProtocolService.valueResponse();
		valueResponse.errors = null;
		valueResponse.statusCode = '0';
		valueResponse.value = 'value';
		resp.GetEDocsUrlForProtocolResponse = valueResponse;
		return resp;
	}
	
	private MdmProtocolService.getEDocsUrlByClientProtNumResponseResponse createMockGetEDocsUrlForClientProtNumResponse() {
		MdmProtocolService.getEDocsUrlByClientProtNumResponseResponse resp =  new MdmProtocolService.getEDocsUrlByClientProtNumResponseResponse();
		MdmProtocolService.valueResponse valueResponse = new MdmProtocolService.valueResponse();
		valueResponse.errors = null;
		valueResponse.statusCode = '0';
		valueResponse.value = 'value';
		resp.GetEDocsUrlByClientProtNumResponse = valueResponse;
		return resp;
	}
	
	private MdmProtocolService.saveProtocolFromVOResponse createMockSaveProtocolFromVOResponse() {
		MdmProtocolService.saveProtocolFromVOResponse resp =  new MdmProtocolService.saveProtocolFromVOResponse();
		MdmProtocolService.mdmCrudResponse mdmCrudResponse = new MdmProtocolService.mdmCrudResponse();
		mdmCrudResponse.errors = null;
		mdmCrudResponse.actionType = 'I';
		mdmCrudResponse.operationStatus = '0';
		resp.SaveProtocolResponse = mdmCrudResponse;
		return resp;
	}
	
	private MdmProtocolService.deleteProtocolFromVOResponse createMockDeleteProtocolFromVOResponse() {
		MdmProtocolService.deleteProtocolFromVOResponse resp =  new MdmProtocolService.deleteProtocolFromVOResponse();
		MdmProtocolService.mdmCrudResponse mdmCrudResponse = new MdmProtocolService.mdmCrudResponse();
		mdmCrudResponse.errors = null;
		mdmCrudResponse.actionType = 'D';
		mdmCrudResponse.operationStatus = '0';
		resp.DeleteProtocolResponse = mdmCrudResponse;
		return resp;
	}
	
	private MdmProtocolService.saveRegionMappingForProtocolResponseResponse createMockSaveRegionMappingFromVOResponse() {
		MdmProtocolService.saveRegionMappingForProtocolResponseResponse resp =  new MdmProtocolService.saveRegionMappingForProtocolResponseResponse();
		MdmProtocolService.mdmCrudResponse mdmCrudResponse = new MdmProtocolService.mdmCrudResponse();
		mdmCrudResponse.errors = null;
		mdmCrudResponse.actionType = 'I';
		mdmCrudResponse.operationStatus = '0';
		resp.SaveRegionMappingForProtocolResponse = mdmCrudResponse;
		return resp;
	}
	
	/********************** Methods to build objects in responses ******************************/
	
	private MdmProtocolService.protocolVO createMockProtocolVO(Integer index) {
		MdmProtocolService.protocolVO protocolVO = new MdmProtocolService.protocolVO();
		protocolVO.protocolId = index;
		protocolVO.systemId = 'prot-' + index;
		protocolVO.clientProtocolNum = 'clientprot-' + index;
		protocolVO.disableEdcAutomation = false;
		protocolVO.regionVOs = createMockRegionVOList(5);
		protocolVO.studyVOs = createMockStudyVOList(5);
		protocolVO.source = 'SF_TEST_MOCK';
		return protocolVO;
	}
	
	private list<MdmProtocolService.protocolVO> createMockProtocolVOList(Integer listSize) {
		list<MdmProtocolService.protocolVO> protocolVOs = new list<MdmProtocolService.protocolVO>();
		for (Integer i = 1; i <= listSize; i++) {
			protocolVOs.add(createMockProtocolVO(i));
		}
		return protocolVOs;
	}
	
	private MdmProtocolService.studyVO createMockStudyVO(Integer index) {
		MdmProtocolService.studyVO studyVO = new MdmProtocolService.studyVO();
		studyVO.praId = 'PRA ID ' + index;
		studyVO.systemId = 'SYS ID ' + index;
		studyVO.studyId = index;
		return studyVO;
	}
	
	private list<MdmProtocolService.studyVO> createMockStudyVOList(Integer listSize) {
		list<MdmProtocolService.studyVO> studyVOs = new list<MdmProtocolService.studyVO>();
		for (Integer i = 1; i <= listSize; i++) {
			studyVOs.add(createMockStudyVO(i));
		}
		return studyVOs;
	}
	
	private MdmProtocolService.regionVO createMockRegionVO(Integer index) {
		MdmProtocolService.regionVO regionVO = new MdmProtocolService.regionVO();
		regionVO.countryCode = 'CC ' + index;
		regionVO.countryName = 'NAME ' + index;
		regionVO.systemAId = 'SYS A ' + index;
		regionVO.systemBId = 'SYS B ' + index;
		return regionVO;
	}
	
	private list<MdmProtocolService.regionVO> createMockRegionVOList(Integer listSize) {
		list<MdmProtocolService.regionVO> regionVOs = new list<MdmProtocolService.regionVO>();
		for (Integer i = 1; i <= listSize; i++) {
			regionVOs.add(createMockRegionVO(i));
		}
		return regionVOs;
	}
	
}