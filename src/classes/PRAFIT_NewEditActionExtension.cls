/*
 * PRAFIT_NewEditActionExtension supports the VF page PRAFIT_NewEditAction as an extension to the standard PRAFIT_Action__c controller
 *  
 *  Adds logic for setting page mode between new record and edit record. Also provides remote action method for fetching lists of 
 *  users related to the “Functional Team” the record is currently assigned to.
 *  IMPORTANT: Uses the “with sharing” keyword to enforce Sharing Rules and ensure limited PRAFIT_Action sObect records 
 *  are returned to User for editing based on their Public Group memberships.
*/

public with sharing class PRAFIT_NewEditActionExtension{
    
    //member variables
        private boolean newActionFlag;
        public string actionId {get; private set;}
        public PRAFIT_Action__c actionObj {get; private set;}
        public string teamMembers {get; private set;}
    
    //constructor
        public PRAFIT_NewEditActionExtension(ApexPages.standardController stdCtr) {
            actionId = Apexpages.currentPage().getparameters().get('id');
            newActionFlag = (String.isBlank(actionId));
            actionObj = new PRAFIT_Action__c();
            if(!newActionFlag){
                try{
                    String soqlCriteria = 'ID = \'' + actionID + '\'';
                    List <PRAFIT_Action__c> actionList = PRAFIT_Utils.getSObjects('PRAFIT_Action__c', soqlCriteria, new List<String>(),1);
                    actionObj = actionList[0];
                    teamMembers = actionObj.Team_Members__c;
                }catch(Exception e){
                    System.debug('>>>>>>>>>>>>>>>>>>>>  Exception in PRAFIT_NewEditActionController : ' + e.getMessage());
                    ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected PRAFIT Action. ' + e.getMessage());
                    ApexPages.addMessage(msg);  
                }
            }
        }
    
    //manage page tile for page re-use adding a New record or Editing an exiting one
        public string getPageTitle(){
            string pageTitle;
            if(newActionFlag){
                pageTitle = 'New PRAFIT Action';
            } else {
                pageTitle = 'Editing: ' + actionObj.Name + ' - ' + actionObj.Title__c; 
            }
            return pageTitle;
        }
    
    //remote action for allowing jQuery on PRAFIT_NewEditActionPage.page to fetch lists of users based on Functional Team selected on page
        @RemoteAction
        public static List<String> getFunctionalTeamMembers(String functionalTeamName) {
            PRAFIT_Config__c nameSpace = PRAFIT_Config__c.getInstance('nameSpace');
            String nameSpaceString = nameSpace.Value__c;
            String publicGroupName = nameSpaceString + ' ' + functionalTeamName;
            List<Group> groupList = [SELECT Id FROM Group WHERE Name = :publicGroupName LIMIT 1];
            List<String> userNameList = new List<String>();
            if(!groupList.isEmpty()){
                string groupId = groupList[0].Id;
                set<id> userIdSet = PRAFIT_Utils.getUserIdsFromGroup(groupId);
                if(!userIdSet.isEmpty()){
                    List<User> userList = new List<User>();
                    userList = [SELECT id, name
                                FROM User
                                WHERE Id = :userIdSet
                                ORDER BY name
                                LIMIT 100];
                    for(User userObj : userList){
                        userNameList.add(userObj.name);
                    }
                } else {
                    userNameList.add('No Users found for ' + publicGroupName);
                }
            } else {
                userNameList.add('No Record found for ' + publicGroupName);
            }
            return userNameList;
        }
}