public with sharing class BDT_FinancialDocumentProgressController {
	
	public string financialDocumentId 		{get;set;}
	public string documentName 				{get;set;}
	public boolean priceCheck 				{get;set;}
	public boolean paymentCheck 			{get;set;}
	public boolean contentCheck 			{get;set;}
	public boolean approvalCheck 			{get;set;}
	public boolean showOnlyStudySelection	{get;set;}
	public string  currentPage 				{get;set;}
	public boolean showAllOptions			{get;set;}
	public boolean approvalsUpToDate		{get;set;}
	
	public BDT_FinancialDocumentProgressController(){}
	
	public pageReference GotoStudySelection(){
		PageReference pr = new PageReference(System.Page.bdt_financialdocstudysel.getUrl());
		pr.getParameters().put('financialDocumentId',financialDocumentId); 		
		pr.setRedirect(true);
		return pr;
	}
	public pageReference GotoPrice(){
		PageReference pr = new PageReference(System.Page.BDT_FinancialDocPricing.getUrl());
		pr.getParameters().put('financialDocumentId',financialDocumentId); 		
		pr.setRedirect(true);
		return pr;
	}
	public pageReference GotoPayment(){
		PageReference pr = new PageReference(System.Page.bdt_financialdocpayment.getUrl());
		pr.getParameters().put('financialDocumentId',financialDocumentId); 		
		pr.setRedirect(true);
		return pr;
	}
	public pageReference GotoContent() {
		PageReference pr = new PageReference(System.Page.bdt_financialdoccontent.getUrl());
		pr.getParameters().put('financialDocumentId',financialDocumentId); 		
		pr.setRedirect(true);
		return pr;
	}
	public pageReference GotoApproval(){
		PageReference pr = new PageReference(System.Page.bdt_financialdocapproval.getUrl());
		pr.getParameters().put('financialDocumentId',financialDocumentId); 		
		pr.setRedirect(true);
		return pr;
	}
	public pageReference GotoOutput(){
		PageReference pr = new PageReference(System.Page.bdt_financialdocoutput.getUrl());
		pr.getParameters().put('financialDocumentId',financialDocumentId); 		
		pr.setRedirect(true);
		return pr;
	}
	public pageReference GotoHistory(){
		PageReference pr = new PageReference(System.Page.bdt_financialdochistory.getUrl());
		pr.getParameters().put('financialDocumentId',financialDocumentId); 		
		pr.setRedirect(true);
		return pr;
	}

}