/**
 * This class will test the very complicated export to revenue module
 * This ofcourse means I'll have to simulate all of the data required for testing 
 */
@isTest(seeAllData=false)
private class PRA_ExportToRevenueBatchTest {

    static final String testValue = 'Remove after Migration';
	
	static testMethod void shouldTestOldClasses() {
    	PRA_ExportToRevenueBatch con = new PRA_ExportToRevenueBatch();
    	system.assert(con.testValue == testValue);
    } 
}