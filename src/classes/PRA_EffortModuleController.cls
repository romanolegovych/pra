public with sharing class PRA_EffortModuleController {
    
	// Expose PRA_ClientTaskSearchService properties as
	// belonging to the controller class.
	public String selectedProject {
		get { return ctSearch.selectedProject; }
		set { ctSearch.selectedProject = value;}
	}
	public List<String> selectedOperationalArea {
		get { return ctSearch.selectedOperationalAreas; }
		set { ctSearch.selectedOperationalAreas = value;}
	}
	public List<String> selectedGroup {
		get { return ctSearch.selectedGroups; }
		set { ctSearch.selectedGroups = value;}
	}
	public List<String> selectedTasks {
		get { return ctSearch.selectedTasks; }
		set { ctSearch.selectedTasks = value;}
	}
	public List<String> selectedRegion {
		get { return ctSearch.selectedRegions; }
		set { ctSearch.selectedRegions = value;}
	}
	public List<SelectOption> regionOptions {
		get { return ctSearch.regionOptions; }
		set { ctSearch.regionOptions = value;}
	}
	public List<SelectOption> operationalAreaOptions {
		get { return ctSearch.operationalAreaOptions; }
		set { ctSearch.operationalAreaOptions = value;}
	}
	public List<SelectOption> groupOptions {
		get { return ctSearch.groupOptions; }
		set { ctSearch.groupOptions = value;}
	}
	public List<SelectOption> taskOptions {
		get { return ctSearch.taskOptions; }
		set { ctSearch.taskOptions = value;}
	}
	
    // static text varaibles
    public static String EFFORT_TYPE_PROPOSED {get{ return 'Proposed';}}
    public static String EFFORT_TYPE_CONTRACTED {get{ return 'Contracted';}}
    public static String EFFORT_TYPE_WORKED_TO_DATE {get{ return 'Worked to Date';}}
    public static String EFFORT_TYPE_LAST_APPROVED {get{ return 'Last Approved';}}
    public static String EFFORT_TYPE_ETC_LAST_APPROVED {get{ return 'ETC Effort Last Approved';}}
    public static String OFFICIAL_FORECAST {get{ return 'Official Forecast';}}
    public static String PREVIOUS_OFFICIAL_FORECAST {get{ return 'Previous Official Forecast';}}
    public static String WORKED {get{ return 'Worked';}}
    //constant representing the select value option in the picklist
    public final String ST_PLACEHOLDER = 'Enter name or project ID....';
    //value selected by the user in the picklist
    public String selectedProjectLabel  {get;set;}
    public Date selectedProjectMigratedDate {get;set;}
    //Value to store ProjectID for VF
    public String selectedProjectName   {get;set;}
    public Boolean blocked   {get;set;}    //block the edit button
    // The task selected
    public List<SelectOption> optionTasks   {get;set;}
    public String selectedTask;
    // Date selectors
    public List<SelectOption> startDateOptions      {get;set;}
    public List<SelectOption> endDateOptions        {get;set;}
    // Checkbox for unconfirmed units
    public Boolean unconfirmedOnly {get;set;}
    public Boolean confirmedOnly {get;set;}
    // Unit List, Task wrapper
    public List<Unit_Effort__c> liUnit {get;set;}
    public Map<Id, TaskWrapper> taskWrapperMap = new Map<Id, TaskWrapper>();
    public List<TaskWrapper> liWrappers {get;set;}
    // Collapsed
    public Boolean isCollapsed {get;set;}
    public Boolean isFirstTime {get;set;}
    // Void = a selectList in VF is always bound to a variable but we only use some Client side
    public String voidSelection {get;set;}
    //Selected Task wraper
    public List<TaskWrapper> selectedTaskWraper {get;set;}
    // flag for action if user preference should be deleted - workaround for forbitten DML operations on constructor
    private Boolean shouldDeleteUserPreference = false;
    //Effort types
    public Map<String, Decimal> effortTypesMap;
    // Task id
    public String taskId {get;set;}
    private Client_Task__c task;
    // Passing the month labels
    public String jsonMonthLabels   {get;set;}
    // Unit Values
    public String jsonUnitValues {get;set;} 
    public String jsonHourValues {get;set;} 
    // Effort Values
    public String jsonEffortValues {get;set;} 
    // Cumulative worked hours
    public Decimal cumulativeWorkedHours {get;set;}
    
    public PRA_ClientTaskSearchService ctSearch = new PRA_ClientTaskSearchService();
    public PRA_EffortModuleController() {
        selectedTaskWraper = new List<TaskWrapper>();
        // Get details from User Preference
        PRA_Utils.UserPreference userPref = PRA_Utils.retrieveUserPreference();
        if(userPref != null){
            // check if user prefferences are correct (if project was not removed)
            selectedProject         = userPref.projectId;
            List<Client_Project__c> clientProjectsList = 
                [select Name, Is_Project_Locked__c, Migrated_Date__c from Client_Project__c where Id = :selectedProject and Load_Status__c != 'New'];
            system.debug('--------clientProjectsList.size = ' + clientProjectsList.size());
            if(clientProjectsList.size() == 1) {
                selectedProjectLabel        = clientProjectsList.get(0).name;
                selectedProjectName         = clientProjectsList.get(0).name;//Assigning for display in VF page
                blocked                     = clientProjectsList.get(0).Is_Project_Locked__c;
                selectedProjectMigratedDate = clientProjectsList.get(0).migrated_date__c;
                selectedOperationalArea     = userPref.operationalAreaId;
                selectedRegion              = userPref.regionId;
                selectedTasks                = userPref.clientUnitId;
                selectedGroup               = userPref.clientUnitGroupId;
                unconfirmedOnly             = userPref.unconfirmedOnly;
                confirmedOnly               = userPref.confirmedOnly;
                selectedTask                = userPref.crctId;
                selectProject();
                isCollapsed = true;
                // EXECUTE THE SEARCH
                isFirstTime = true;
                searchTasks();
            } else {
                // Init of Unconfirmed
                unconfirmedOnly = false;
                confirmedOnly = false;
                isFirstTime = true;
                // Reset the filters
                reset();
                
                // Set the default project to the first project in the list
                selectedProject = ctSearch.ST_NONE;
                isCollapsed = false;
            }
            
        } else {
            // Init of Unconfirmed
            unconfirmedOnly = false;
            confirmedOnly = false;
            isFirstTime = true;
            // Reset the filters
            reset();
            
            // Set the default project to the first project in the list
            selectedProject = ctSearch.ST_NONE;
            isCollapsed = false;
        }
    }
            
    // Reset every filters
    public void reset() {
        selectedProject = ctSearch.ST_NONE;
        selectedProjectName = 'Home';
        selectedProjectLabel = null;
        selectedProjectMigratedDate = null;
        selectProject();
        liWrappers = null;
        confirmedOnly = false;
        unConfirmedOnly = false;
        optionTasks = null;
                if(!isFirstTime)
        PRA_Utils.deleteUserPreference();
    }
    
    // Selecting a Project updates OA and Regions
    public void selectProject() {
    	ctSearch.initOnSelectProject();
    }
    
    // Init of the Target Month picklist
    public void initDateOptions(Date taskEndDate) {
        startDateOptions = new List<SelectOption>();
        for(Integer i = 0; i < (Date.today().monthsBetween(taskEndDate)+1); i++) {
            Date monthIndex = Date.today().addMonths(i);
            DateTime t = Datetime.newInstance(monthIndex.year(), monthIndex.month(), 1);
            String stLabel  = t.format('MMM yyyy');
            // We pass the index in the effort table as the value => selecting this date will give the index of the effort
            startDateOptions.add(new SelectOption('' + (i + 3), stLabel));
        }
        endDateOptions = new List<SelectOption>(startDateOptions);
        endDateOptions.remove(0);
        startDateOptions.remove(startDateOptions.size() - 1);
    }
    
    // Init the Effort Types picklist
    public List<SelectOption> getEffortTypes() {
        List<SelectOption> retVal = new List<SelectOption>();
        retVal.add(new SelectOption('', 'Select Effort'));
        
        //if crctid is selected get calcucalted picklist values
        if(effortTypesMap == null || effortTypesMap.size() == 0) {
            initEffortTypes();
        }
    
        retVal.add(new SelectOption('' + effortTypesMap.get(EFFORT_TYPE_CONTRACTED), EFFORT_TYPE_CONTRACTED));
        retVal.add(new SelectOption('' + effortTypesMap.get(EFFORT_TYPE_WORKED_TO_DATE), EFFORT_TYPE_WORKED_TO_DATE));
        retVal.add(new SelectOption('' + effortTypesMap.get(EFFORT_TYPE_LAST_APPROVED), EFFORT_TYPE_LAST_APPROVED));
        retVal.add(new SelectOption('' + effortTypesMap.get(EFFORT_TYPE_ETC_LAST_APPROVED), EFFORT_TYPE_ETC_LAST_APPROVED));
        retVal.add(new SelectOption('0', EFFORT_TYPE_PROPOSED));
        
        return retVal;
    }
    
    public List<EffortTypeWrapper> getEffortTypeList() {
        List<EffortTypeWrapper> retVal = new List<EffortTypeWrapper>();
        
        if(effortTypesMap == null || effortTypesMap.size() == 0) {
            initEffortTypes();
        }
        EffortTypeWrapper etw = new EffortTypeWrapper();
        etw.contracted = effortTypesMap.get(EFFORT_TYPE_CONTRACTED) == null ? 0 : effortTypesMap.get(EFFORT_TYPE_CONTRACTED).setScale(3);
        etw.workedToDate = effortTypesMap.get(EFFORT_TYPE_WORKED_TO_DATE) == null ? 0 : effortTypesMap.get(EFFORT_TYPE_WORKED_TO_DATE).setScale(3);
        etw.lastApproved = effortTypesMap.get(EFFORT_TYPE_ETC_LAST_APPROVED) == null ? 0 : effortTypesMap.get(EFFORT_TYPE_ETC_LAST_APPROVED).setScale(3);
        retVal.add(etw);
        
        return retVal;
    }
    
    public void searchTasksVF() {
        for(Client_Project__c c : [SELECT Name, Is_Project_Locked__c FROM Client_Project__c WHERE Id = :selectedProject limit 1]) {  
           selectedProjectName  = c.Name;
           selectedProjectLabel = c.Name;
           blocked              = c.Is_Project_Locked__c;
        }
        isFirstTime = false;
        searchTasks();
    }
    
    // Event Handlers
    public void initGroupOptions() {
		ctSearch.initGroupOptions();    	
    }
    
    public void initTaskOptions() {
    	ctSearch.initTaskOptions();
    }
    
    // Retrieve the list of CRCT tasks which form the basis of the rows created and populate the picklist
    public void searchTasks() {
        optionTasks = new List<SelectOption>();

        ctSearch.orderBy = ctSearch.CT_ORDERBY_CLIENT_UNIT;
        ctSearch.filterClosedUnits = true;
        List<Client_Task__c> availableTasks = ctSearch.getTasks();
        
        if(availableTasks.size() > 0) {
            for(Client_Task__c task : availableTasks) {
                Boolean includeThisTask = true;
                if(task.Description__c.contains('Migrated Worked Hours')) {
                    includeThisTask = false;
                }
                // Init
                if(includeThisTask == true) {
                    selectedProjectName = task.Project__r.Name;
                    selectedProjectMigratedDate = task.Project__r.Migrated_Date__c;
                    optionTasks.add(new SelectOption(task.Id, task.Client_Unit_Number__c + ' - ' + task.Description__c + ' - ' + task.Project_Region__r.Name));
                }
            }
            
            System.debug('*** SIZE returned: ' + optionTasks.size());
            
            // We select the 1st returned task by default
            if(selectedTask == null || selectedTask == '') {
                selectedTask = optionTasks[0].getValue();
            } else {
                // check if selected task is available
                Boolean bTaskFound = false;
                for(Integer i = 0; i < optionTasks.size() && !bTaskFound; i++) {
                    if(selectedTask == optionTasks[i].getValue()) {
                        bTaskFound = true;
                    }
                }
                if(!bTaskFound) {
                    // task not found - select first avaiable
                    selectedTask = optionTasks[0].getValue();
                }
            }
            // Launch the selection of the Task
            selectTask();
        }
        if(!isFirstTime) { 
            // Save user prefs
            PRA_Utils.savePrefs(selectedProject, selectedRegion, selectedOperationalArea, selectedTasks, 
                                 selectedGroup, unconfirmedOnly, confirmedOnly, selectedTask);
        } else {
                isFirstTime = false;
        }
    }

    public void setSelectedTask(String task) {
        selectedTask = task;
    }
    
    public String getSelectedTask() {
        return selectedTask;
    }
    
    public void selectTaskAndSavePreferences() {
        // Save user prefs
        selectTask();
        PRA_Utils.savePrefs(selectedProject, selectedRegion, selectedOperationalArea, selectedTasks, 
                                 selectedGroup, unconfirmedOnly, confirmedOnly, selectedTask);
    }
    
    // When user selects a Task in the picklist
    public void selectTask() {
        try {
            // Getting values from CRCT
            task = [SELECT Id, Total_Worked_Units__c, Client_Unit_Number__c, Total_Worked_Hours__c, Total_Units__c, 
                        Original_End_Date__c, Start_Date__c, Description__c, Project_Region__r.Name, Unit_of_Measurement__c, Project__r.Estimated_End_Date__c,
                        Average_Contract_Effort__c, Average_Last_Approved_Effort__c, Average_Worked_Effort__c, Total_Contract_Hours__c, Total_Contract_Units__c,
                        LA_Total_Forecast_Hours__c, LA_Total_Worked_Hours__c, LA_Total_Forecast_Units__c, LA_Total_Worked_Unit__c 
                    FROM Client_Task__c WHERE Id = :selectedTask];
            
            taskId = task.Id;
            // pra-825 - changing the end date to be project estimated end date
            initDateOptions(task.Project__r.Estimated_End_Date__c);
            
            // initialize Effort Types
            initEffortTypes();
            
            // Declaring the list of 1 Wrapper
            liWrappers = new List<TaskWrapper>{new TaskWrapper(), new TaskWrapper(), new TaskWrapper()};
            
            // Init of the common values
            liWrappers[0].unitnumber = task.Client_Unit_Number__c;
            liWrappers[0].taskName = task.Description__c;
            liWrappers[0].region = task.Project_Region__r.Name;
            liWrappers[0].unitOfMeasurement = task.Unit_of_Measurement__c;
            liWrappers[0].ETC = task.Total_Units__c.setScale(3);
            liWrappers[0].entity = 'Units';
            liWrappers[1].entity = 'Effort';
            liWrappers[2].entity = 'Hours';
            
            selectedTaskWraper = new List<TaskWrapper>();
            selectedTaskWraper.add(liWrappers[0]);
            
            // Getting the total number of months to return INCLUDING 1st and last mont => +1 at the end AND 3 last months
            Integer nbMonths = Date.today().monthsBetween(task.Project__r.Estimated_End_Date__c) + 1 +3;
            System.debug('nbMonths before: '+nbMonths);
            
            // Getting the next multiple of 9
            nbMonths = (nbMonths + 9 - 1) / 9 * 9;
            System.debug('nbMonths after: ' + nbMonths);
            
            List<String> liMonthLabels = new List<String>();
            for(Integer i = -3; i < nbMonths-3; i++) {
                Date monthInContext = Date.today().addMonths(i).toStartOfMonth();
                liMonthLabels.add(Datetime.newInstance(monthInContext.year(), monthInContext.month(), 1).format('MMM yyyy'));
            }
            jsonMonthLabels = JSON.serialize(liMonthLabels);
            
            // Month Forecast created - proposed
            Date monthForecastCreatedCurrentSOQL = Date.today().toStartOfMonth();
            
            // Getting the Worked Units
            List<Unit_Effort__c> workedUnits = [Select Id, Client_Task__c, Worked_Unit__c,Total_Worked_Hours__c, Month_Applies_To__c FROM Unit_Effort__c 
                                                WHERE Client_Task__c = :selectedTask
                                                AND (Month_Applies_To__c >= :Date.today().toStartOfMonth().addMonths(-3) AND Month_Applies_To__c <= :Date.today().addMonths(-1))
                                                ORDER BY Month_Applies_To__c];
            // Get the current months forecast units
            List<Unit_Effort__c> currentMonthForecastUnits = [Select Id, Client_Task__c, Forecast_Unit__c, Month_Applies_To__c, Client_Task__r.Close_Date__c 
            						   FROM Unit_Effort__c WHERE Client_Task__c = :selectedTask
                                       AND (Month_Applies_To__c >= :Date.today().toStartOfMonth() AND Month_Applies_To__c <= :task.Project__r.Estimated_End_Date__c)
                                       ORDER BY Month_Applies_To__c];
                                       
            System.debug('----------------worked units------------------' + workedUnits.size());
            System.debug('----------------current month forecast unit-------------------------' + currentMonthForecastUnits.size());
            
            List<ValueWrapper> liUnitValues = new List<ValueWrapper>();
            List<ValueWrapper> liHourValues = new List<ValueWrapper>();
            // Preparing for worked units = always 3
            for(Unit_Effort__c un : workedUnits) {
                Integer iAddEmptyRecord = 0;
                Integer monthIdx =  Date.today().toStartOfMonth().addMonths(-3).monthsBetween(un.Month_Applies_To__c);
                if(liUnitValues.size() < monthIdx) {
                    while(liUnitValues.size() < monthIdx) {
                        liUnitValues.add(new ValueWrapper(null, null));
                        liHourValues.add(new ValueWrapper(null, null));
                    }
                }
                liUnitValues.add(new ValueWrapper(un.Id, un.Worked_Unit__c));
                liHourValues.add(new ValueWrapper(un.Id, un.Total_Worked_Hours__c));
            }
            while(liUnitValues.size() < 3) {
                liUnitValues.add(new ValueWrapper(null, null));
                liHourValues.add(new ValueWrapper(null, null));
            }
            for(Unit_Effort__c un : currentMonthForecastUnits) {
                if (un.Client_Task__r.Close_Date__c == null ||
                	un.Month_Applies_To__c < un.Client_Task__r.Close_Date__c) {
		                liUnitValues.add(new ValueWrapper(un.Id, un.Forecast_Unit__c));
                	} else {
		                liUnitValues.add(new ValueWrapper(null, null));
                	}
            }
            
            /* Effort*/
            // Getting the Worked Efforts
            List<Unit_Effort__c> workedEfforts = [Select Id, Client_Task__c, Worked_Effort__c, Month_Applies_To__c FROM Unit_Effort__c WHERE Client_Task__c = :selectedTask
                                                AND (Month_Applies_To__c >= :Date.today().toStartOfMonth().addMonths(-3) AND  Month_Applies_To__c <= :Date.today().addMonths(-1))
                                                ORDER BY Month_Applies_To__c];
            // Get the current months forecast Effort
            List<Unit_Effort__c> currentMonthEfforts = [Select Id, Client_Task__c, Forecast_Effort__c, Month_Applies_To__c, Client_Task__r.Close_Date__c 
            						   FROM Unit_Effort__c WHERE Client_Task__c = :selectedTask
                                       AND (Month_Applies_To__c >= :Date.today().toStartOfMonth() AND Month_Applies_To__c <= :task.Project__r.Estimated_End_Date__c)
                                       ORDER BY Month_Applies_To__c];
            
            List<ValueWrapper> liEffortValues = new List<ValueWrapper> ();
            // Preparing for worked Efforts = always 3
            for(Unit_Effort__c ef : workedEfforts) {
                Integer iAddEmptyRecord = 0;
                Integer monthIdx =  Date.today().toStartOfMonth().addMonths(-3).monthsBetween(ef.Month_Applies_To__c);
                if(liEffortValues.size() < monthIdx) {
                    while(liEffortValues.size() < monthIdx) {
                        liEffortValues.add(new ValueWrapper(null, null));
                    }
                }
                liEffortValues.add(new ValueWrapper(ef.Id, ef.Worked_Effort__c));
            }
            while(liEffortValues.size() < 3) {
                liEffortValues.add(new ValueWrapper(null, null));
            }
            
            System.debug(currentMonthEfforts.size());
            
            for(Unit_Effort__c un : currentMonthEfforts) {
                if (un.Client_Task__r.Close_Date__c == null ||
                	un.Month_Applies_To__c < un.Client_Task__r.Close_Date__c) {
		                liEffortValues.add(new ValueWrapper(un.Id, un.Forecast_Effort__c));
                	} else {
		                liEffortValues.add(new ValueWrapper(null, null));
                	}
            }
            
            /* Hours */
            cumulativeWorkedHours = task.Total_Worked_Hours__c;
            liWrappers[2].varPrevMonth = 0.0;
            
            jsonUnitValues = JSON.serialize(liUnitValues);
            jsonEffortValues = JSON.serialize(liEffortValues);
            jsonHourValues = JSON.serialize(liHourValues);
            
            System.debug('--------------------------------------END');
        } catch(Exception e) {
            System.debug(LoggingLevel.INFO, e);
            System.debug(LoggingLevel.INFO, e.getLineNumber());
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }

    // Called when clicking on Save
    @RemoteAction
    public static String saveEfforts(String JSONString) {
        // Deserializing the JSON
        List<ValueWrapper> deserializedEfforts = 
          (List<ValueWrapper>)JSON.deserialize(JSONString, List<ValueWrapper>.class);
        Set<Id> updatedEffortsIds = new Set<Id>();
        // Map to hold values
        Map<String, Decimal> effortNewVal = new Map<String, Decimal>();
        for(ValueWrapper vw : deserializedEfforts) {
            effortNewVal.put(vw.id, vw.value);
        }
        
        // List to update efforts
        List<Unit_Effort__c> unitEfforts = new List<Unit_Effort__c>();
        List<Unit_Effort__c> unitEffortCosts = new List<Unit_Effort__c>();
        Map<Id, Unit_Effort__c> unitEffortMap = new Map<Id, Unit_Effort__c>();
        List<Unit_Effort__c> efforts = [SELECT Id, Client_Task__c, Forecast_Effort__c, Total_Capped_Cost__c, Total_Forecast_Cost__c, 
            Total_Contracted_BDG_Cost__c, Month_Applies_To__c FROM Unit_Effort__c 
            WHERE Id IN :effortNewVal.keySet()];
        
        for(Unit_Effort__c effort : efforts) {
            if(effort.Forecast_Effort__c != effortNewVal.get(effort.Id)) {
                effort.Forecast_Effort__c = effortNewVal.get(effort.Id);
                updatedEffortsIds.add(effort.Id);
                unitEfforts.add(effort);
            }
            unitEffortMap.put(effort.Id, effort);
        } 
        if(unitEfforts.size() > 0) {
            update unitEfforts;
        }    
        if(updatedEffortsIds.size() > 0) {
                for(AggregateResult ar : [SELECT Unit_Effort__c ue, sum(Capped_Cost__c) capped, sum(Forecast_Cost__c) forecast, sum(Contracted_BDG_Cost__c) contracted 
                    from Hour_EffortRatio__c WHERE Unit_Effort__c IN :updatedEffortsIds GROUP BY Unit_Effort__c ORDER BY Unit_Effort__c]) {
                    Unit_Effort__c unitEffort = unitEffortMap.get(String.valueOf(ar.get('ue')));
                    if(ar.get('capped') != null && 
                        Decimal.valueOf(Double.valueOf(ar.get('capped'))) != unitEffortMap.get(String.valueOf(ar.get('ue'))).Total_Capped_Cost__c)
                                unitEffort.Total_Capped_Cost__c = Decimal.valueOf(Double.valueOf(ar.get('capped')));
                        if(ar.get('forecast') != null && 
                                Decimal.valueOf(Double.valueOf(ar.get('forecast'))) != unitEffortMap.get(String.valueOf(ar.get('ue'))).Total_Forecast_Cost__c)
                                unitEffort.Total_Forecast_Cost__c = Decimal.valueOf(Double.valueOf(ar.get('forecast')));
                        if(ar.get('contracted') != null && 
                                Decimal.valueOf(Double.valueOf(ar.get('contracted'))) != unitEffortMap.get(String.valueOf(ar.get('ue'))).Total_Contracted_BDG_Cost__c)
                                unitEffort.Total_Contracted_BDG_Cost__c = Decimal.valueOf(Double.valueOf(ar.get('contracted')));
                        
                        if((ar.get('capped') != null && Decimal.valueOf(Double.valueOf(ar.get('capped'))) == unitEffortMap.get(String.valueOf(ar.get('ue'))).Total_Capped_Cost__c) ||
                        (ar.get('forecast') != null && Decimal.valueOf(Double.valueOf(ar.get('forecast'))) == unitEffortMap.get(String.valueOf(ar.get('ue'))).Total_Forecast_Cost__c) ||
                                (ar.get('contracted') != null && Decimal.valueOf(Double.valueOf(ar.get('contracted'))) == unitEffortMap.get(String.valueOf(ar.get('ue'))).Total_Contracted_BDG_Cost__c)) {
                                unitEffortCosts.add(unitEffort);
                    }
                } 
        }
        if(unitEffortCosts.size() > 0) {
            update unitEffortCosts;
        }
        
        // Check for missing effort ratios and/or for missing effort
        String strReturn = '';
        if ((efforts.size() > 0) &&
            (efforts[0] != null)) {
            ID taskID = efforts[0].Client_Task__c;
            Client_Task__c ct = [SELECT Project__r.Estimated_End_Date__c 
                                 FROM Client_Task__c
                                 WHERE id = :taskID ];

	        List<Unit_Effort__c> ue;
	        
	        ue = PRA_DataAccessor.getMissingEffortRatiosByTask(new List<Id>{taskID}, System.today(), ct.Project__r.Estimated_End_Date__c);
	        
	        if (!ue.isEmpty()) {
	        	strReturn += 'Effort Ratios are missing for this Client Unit. Remember to update the Effort Ratios.\n';
	        }

            ue = PRA_DataAccessor.getMissingEffortByTask(new List<Id>{taskID}, System.today(), ct.Project__r.Estimated_End_Date__c);

            if (!ue.isEmpty()) {
            	strReturn += '\n';
                strReturn += 'This Client Unit has forecasted units. Please forecast the effort if this is a labor unit.\n';
            }
        }
        return strReturn;
    }
    
    
    // JS Remoting action called when looking for Project
    @RemoteAction
    public static List<Client_Project__c> searchProject(String projectName) {
        System.debug('projectName: ' + projectName );
        List<Client_Project__c> liProj = Database.query('SELECT Id, Name FROM Client_Project__c WHERE Name LIKE \'%' 
        + String.escapeSingleQuotes(projectName) + '%\' and Load_Status__c != \'New\'');
        return liProj;
    }
    
    private void initEffortTypes() {        
        Map<String, Decimal> retVal = new Map<String, Decimal>();
        if(taskId != null && taskId <> '' && task != null) {        
            // contracted = Sum of (Bid Contract.Total HOURS) DIVIDED BY Sum of (Bid Contract.Total Units)
            Decimal val = 0;
            if(task.Average_Contract_Effort__c <> 0) {
                val = task.Average_Contract_Effort__c;
            }
            retVal.put(EFFORT_TYPE_CONTRACTED, val);
            
            // work to date = Sum of (HOURS.[Number] where [Type] = Worked) DIVIDED BY Sum of (Units.[Number] where [Type] = Worked) 
            val = 0;                
            if(task.Average_Worked_Effort__c <> 0) {
                val = task.Average_Worked_Effort__c;
            }
            retVal.put(EFFORT_TYPE_WORKED_TO_DATE,val);
            
            // Last Approved = Sum of (HOURS.[Number] where [Type] = Historical) DIVIDED BY Sum of (Units.[Number] where [Type] = Historical) and are last historical
            val = 0;
            if(task.Average_Last_Approved_Effort__c <> 0) {
                val = task.Average_Last_Approved_Effort__c;
            }
            retVal.put(EFFORT_TYPE_LAST_APPROVED,val);
            
            val = 0;
			if (task.LA_Total_Forecast_Units__c <> null && task.LA_Total_Worked_Unit__c <> null &&
				task.LA_Total_Forecast_Units__c <> null && task.LA_Total_Worked_Unit__c <> null) {

	            if (task.LA_Total_Forecast_Units__c + task.LA_Total_Worked_Unit__c <> 0) {
	            	val = (task.LA_Total_Forecast_Hours__c + task.LA_Total_Worked_Hours__c) / 
	            		(task.LA_Total_Forecast_Units__c + task.LA_Total_Worked_Unit__c); 
	            }
			}
            retVal.put(EFFORT_TYPE_ETC_LAST_APPROVED,val);
        }
        effortTypesMap = retVal;
    }
    
    // Value Wrapper
    public class ValueWrapper {
        public String id                {get;set;} 
        public Decimal value            {get;set;}
        public ValueWrapper(String id, Decimal value) {
            this.id = id;
            this.value = value;
        }
    }
    
    // Wrapper to hold the data to display
    public class TaskWrapper {
        //Unit Number
        public String unitnumber            {get;set;}  
        // reg
        public String region                {get;set;} 
        // unit name
        public String taskName              {get;set;} 
        // Unit Driver
        public String unitOfMeasurement     {get;set;}
        public Decimal ETC                  {get;set;}
        public Decimal varPrevMonth         {get;set;}
        public String entity                {get;set;} // type of entity - hours, Unit, effort
    }
    
    // Wrapper for Effort Type
    public class EffortTypeWrapper {
        public Decimal contracted       {get;set;}
        public Decimal workedToDate     {get;set;}
        public Decimal lastApproved     {get;set;}
    }
}