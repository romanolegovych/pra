/* 
@author Kondal Reddy
@date 2014  
@description test class for Listing SRM Scenarios on PBB Scenario SRM tab
*/

@isTest

public class PBB_SRMListControllerTest {
    
    static testMethod void testConstructor() {
        test.startTest();
        PBB_DataAccessor da = new PBB_DataAccessor();
        test.stopTest();
    }
     static testMethod void testPBB_SRMListController() {
         PBB_TestUtils tu = new PBB_TestUtils();
         test.startTest();
         
         WFM_Project__c wp = new WFM_Project__c();
         wp = tu.createwfmproject();
         Bid_Project__c bp = new Bid_Project__c();
         bp = tu.createbidproject();
         
         SRM_Scenario__c srm = new SRM_Scenario__c();
         srm.bid_project__c = bp.id;
         srm.Description__c = 'Description11';
         insert srm;
         PBB_Scenario__c ps = new PBB_Scenario__c();
         SRM_Model__c smm = tu.createSRMModelAttributes();
         SRM_Scenario__c  srmScen= tu.createSRMScenario();
         ps = tu.createscenarioAttributes();
         system.assertequals(ps.Description__c,'Description01T');
         ps.SRM_Scenario__c = srm.id;
         upsert ps;
         ApexPages.currentPage().getParameters().put('PBBScenarioID',ps.id);
         system.assert(ps.id != null,'Record inserted');
         
         /** 
         * This tests PBBSdcenarioID and Bidproject not null check, if statements
         */
          ApexPages.StandardController p = new ApexPages.StandardController(ps);
          PBB_SRMListController pbbs = new PBB_SRMListController(p);
         
         /** 
         * This tests detachSRMScenario() method
         */
         pageReference pr = pbbs.detachSRMScenario(); 
         
         /**
         * This tests the newSrmScenario()
         */
         PBB_Scenario__c psc = new PBB_Scenario__c();
         psc = tu.createscenarioAttributes();
         psc = [select id,bid_project__c from PBB_Scenario__c LIMIT 1];
         ApexPages.currentPage().getParameters().put('/apex/SRM_Scenario?bid=',psc.bid_project__c);
         pageReference ss = pbbs.newSrmScenario();
         
         /**
         * This tests srmEdit()
         */
         ApexPages.currentPage().getParameters().put('/apex/SRM_Scenario?id=',ps.SRM_Scenario__c);
         pageReference sp = pbbs.srmEdit();
         
         /**
         * This is to test constructor with no parameter
         */
         ApexPages.StandardController p1 = new ApexPages.StandardController(ps);
         PBB_SRMListController pbbs1 = new PBB_SRMListController();
         
         test.stopTest();
     }
}