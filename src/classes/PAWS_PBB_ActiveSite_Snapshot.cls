global class PAWS_PBB_ActiveSite_Snapshot implements PAWS_IScheduler, Database.Batchable<sObject>
{	
	public PAWS_PBB_ActiveSite_Snapshot()
	{
	}
	
	public void schedulerExecute(Object ctx)
	{
		PAWS_PBB_ActiveSite_Snapshot.startBatch();
	}

	/******************** BATCH SECTION *************************/

	//*********************** STATIC ******************************/
	
	public static void startBatch()
	{
		if(canStartBatch() == false)
		{
			return;
		}
    	Database.executeBatch(new PAWS_PBB_ActiveSite_Snapshot(), 100);
    	PAWS_Scheduler.startScheduler('PAWS_PBB_ActiveSite_Snapshot');
	}
	
	private static Boolean canStartBatch()
   	{   		
   		Integer total = [select count() from AsyncApexJob where ApexClass.Name = 'PAWS_PBB_ActiveSite_Snapshot' and Status in ('Processing', 'Queued', 'Preparing') and JobType = 'BatchApex' limit 1];
   		return total == 0;
   	}

   	//*********************** PUBLIC ******************************/
	global Database.QueryLocator start(Database.BatchableContext BC)
    {
		return Database.getQueryLocator([
			select
				Number_Of_Sites_Completed__c, Protocol_ID__c, SRM_Scenario_Name__c,
				(
					select
						Study_Start_Up_Expected_Date_BI_0__c, Study_Start_Up_Expected_Date_BI_05__c, Study_Start_Up_Expected_Date_BI_1__c, Study_Start_Up_Complete_Date__c
					from
						PAWS_Project_Flow_Sites__r
				)
			from
				ecrf__c]);
    }

	global void execute(Database.BatchableContext info, List<sObject> scope)
	{	
		List<PAWS_Project_Flow_Site_Snapshot__c> snapshotList = new List<PAWS_Project_Flow_Site_Snapshot__c>();
		Datetime monday = getCurrentWeekMondayDateTime();
		DAtetime sunday = getCurrentWeekSundayDateTime();

		for(ecrf__c eachProject : (List<ecrf__c>)scope)
		{
			PAWS_Project_Flow_Site_Snapshot__c snapshot = new PAWS_Project_Flow_Site_Snapshot__c (
					Week_Start__c=getCurrentWeekMondayDateTime().date(),
					Project_ID__c=eachProject.ID,
					Protocol__c=eachProject.Protocol_ID__c,
					Scenario__c=eachProject.SRM_Scenario_Name__c
			);

			for(PAWS_Project_Flow_Site__c eachSite : eachProject.PAWS_Project_Flow_Sites__r)
			{
				if(eachSite.Study_Start_Up_Complete_Date__c >= monday && eachSite.Study_Start_Up_Complete_Date__c <= sunday)
				{
					snapshot.Active__c = snapshot.Active__c == null ? 1 : ++snapshot.Active__c;
				}
				else if(eachSite.Study_Start_Up_Expected_Date_BI_0__c >= monday && eachSite.Study_Start_Up_Expected_Date_BI_0__c <= sunday)
				{
					snapshot.Active_BI_0__c = snapshot.Active_BI_0__c == null ? 1 : ++snapshot.Active_BI_0__c;
				}
				else if(eachSite.Study_Start_Up_Expected_Date_BI_05__c >= monday && eachSite.Study_Start_Up_Expected_Date_BI_05__c <= sunday)
				{
					snapshot.Active_BI_05__c = snapshot.Active_BI_05__c == null ? 1 : ++snapshot.Active_BI_05__c;
				}
				else if(eachSite.Study_Start_Up_Expected_Date_BI_1__c >= monday && eachSite.Study_Start_Up_Expected_Date_BI_1__c <= sunday)
				{
					snapshot.Active_BI_1__c = snapshot.Active_BI_1__c == null ? 1 : ++snapshot.Active_BI_1__c;
				}
			}

			snapshotList.add(snapshot);
		}
		insert snapshotList;
   	}     
   	
   	global void finish(Database.BatchableContext info)
   	{   
   		
   	} 

   	private Datetime getCurrentWeekMondayDateTime()
   	{
   		Datetime dt = DateTime.now();
		while(dt.format('E') != 'Mon')
		{
		    dt = dt.addDays(-1);
		}
		return dt;
   	}

   	private Datetime getCurrentWeekSundayDateTime()
   	{
   		Datetime dt = DateTime.now();
		while(dt.format('E') != 'Sun')
		{
		    dt = dt.addDays(1);
		}
		return dt;
   	}
}