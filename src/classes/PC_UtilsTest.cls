@isTest
private class PC_UtilsTest {
	
	private static final String ACCOUNT_ID_VALUE = '001000000000000AAA';
	private static final String SUBJECT = 'SUBJECT';
	
	@isTest
	private static void getIdFromStringTest(){
		Id accountId = PC_Utils.getIdFromString(ACCOUNT_ID_VALUE, SUBJECT);
		System.assert(accountId != null);
	}
	
	@isTest
	private static void getIdFromStringEmptyValue(){
		try{
			PC_Utils.getIdFromString(null, SUBJECT);
			System.assert(false);
		}catch(PC_Utils.PC_Exception e){
			
		}
	}
	
	@isTest
	private static void getIdFromStringIncorrectValue(){
		try{
			PC_Utils.getIdFromString('Some incorrect value', SUBJECT);
			System.assert(false);
		}catch(PC_Utils.PC_Exception e){
			
		}
	}
	
	@isTest
	private static void getSObjectNameFromIdTest(){
		System.assertEquals( 'Account', PC_Utils.getSObjectNameFromId(ACCOUNT_ID_VALUE, SUBJECT) );
	}
	
	@isTest
	private static void getSObjectNameFromIdEmptyValue(){
		try{
			PC_Utils.getSObjectNameFromId(null, SUBJECT);
			System.assert(false);
		}catch(PC_Utils.PC_Exception e){
			
		}
	}
	
	@isTest
	private static void getSObjectNameFromIdIncorrectValue(){
		try{
			PC_Utils.getSObjectNameFromId('Some incorrect value', SUBJECT);
			System.assert(false);
		}catch(PC_Utils.PC_Exception e){
			
		}
	}
	
	
}