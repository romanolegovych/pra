public with sharing class BDT_UserPreferenceService {

	public map<string,BDT_UserPreference__c> 	userPreferences {get;set;}
	
	public BDT_UserPreferenceService() {
		initialize();
	}
	
	public void initialize() {
		userPreferences = new map<string,BDT_UserPreference__c>();
		For (BDT_UserPreference__c up:BDT_UserPreferenceDataAccessor.getAllUsersPreferences()) {
			userPreferences.put(up.name,up);
		}
	}

	public void saveUserPreferences() {
		try {
			upsert userPreferences.values();
		} catch (Exception e) {
			system.debug(e);
		}
	}
	
	public String getPreference(String PreferenceName){
		if (userPreferences.containsKey(PreferenceName)) {
			return userPreferences.get(PreferenceName).text__c;
		}
		return '';
	}

	public void setPreference(String PreferenceName,String Value){
		
		BDT_UserPreference__c pref;
		
		if (userPreferences.containsKey(PreferenceName)) {
			pref = userPreferences.get(PreferenceName);
		} else {
			pref = new BDT_UserPreference__c(name = PreferenceName);
		}
		
		pref.Text__c = Value;
		
		userPreferences.put(PreferenceName,pref);
		
	}

/******* MAINTAIN USERPREFERENCE PROJECT ***********/

	/**
	 * Set users current project, will also try to set last studies used from quickList.
	 * @author Maurice Kremer
	 * @version 27-Dec-13
	 * @param ProjectId
	 */
	public static void staticSetProject (String ProjectId) {
		// read preferences
		BDT_UserPreferenceService up = new BDT_UserPreferenceService();
		up.setProject(ProjectId);
		up.saveUserPreferences();
	}

	/**
	 * Set users current project, will also try to set last studies used from quickList.
	 * @author Maurice Kremer
	 * @version 27-Dec-13
	 * @param ProjectId
	 */		
	public void setProject (String ProjectId) {
		
		// check the quickList if it contains a set of studies for this project
		Set<String> StudyIdSet = new Set<String>();
		for (quickListWrapper ql:getQuickList()) {
			if (ql.projectId == ProjectId) {
				StudyIdSet = ql.studySet;
				break;
			}
		}
		
		Client_Project__c cp = [select id,code__c from client_project__c where id = :ProjectId];
		setProjectAndStudies (cp, StudyIdSet);
		saveUserPreferences();
	}


	public void setProjectAndStudies (Client_Project__c project, Set<String> StudyIdSet) {
		
		BDT_UserPreference__c up;
		
		if (userPreferences.containsKey(BDT_UserPreferenceDataAccessor.PROJECT)) {
			up = userPreferences.get(BDT_UserPreferenceDataAccessor.PROJECT);
		} else {
			up = new BDT_UserPreference__c(name = BDT_UserPreferenceDataAccessor.PROJECT);
		}
		up.text__c = project.id;
		
		userPreferences.put(BDT_UserPreferenceDataAccessor.PROJECT,up);
		
		// clean existing study selection
		if (userPreferences.containsKey(BDT_UserPreferenceDataAccessor.STUDIES)) {
			userPreferences.get(BDT_UserPreferenceDataAccessor.STUDIES).text__c = '';
		}
		
		// add project to quicklist
		addProjectToQuickList (project.id,project.code__c, '');  /* TO-DO sponsor name */
		
		// set studies (will also set studies in quicklist)
		addStudyCodes(StudyIdSet);
		
	}

/******* MAINTAIN USERPREFERENCE STUDY ***********/

	/**
	 * Add set of study id's to users study list
	 * @author Maurice Kremer
	 * @version 23-Dec-13
	 * @param Study__c
	 */
	public void addStudyCodes (Set<String> StudyIdSet) {
		
		String value = getPreference(BDT_UserPreferenceDataAccessor.STUDIES);
		value = (value==null)?'':value;
		for (String studyId : StudyIdSet) {
			// add unique study ids
			if (!value.contains(studyId)){
				value += ':' + studyId;
			}
		}
		setPreference(BDT_UserPreferenceDataAccessor.STUDIES,value.removeStart(':'));
		
		/** UPDATE QUICKLIST **/
		addStudyToQuickList (StudyIdSet);

	}

	/**
	 * Remove studies from users study list
	 * @author Maurice Kremer
	 * @version 23-Dec-13
	 * @param Study__c
	 */
	public void removeStudyCodes (Set<String> StudyIdSet) {
		
		String key = BDT_UserPreferenceDataAccessor.STUDIES;
		
		// find existing or create new
		BDT_UserPreference__c up;
		if (userPreferences.containsKey(key)) {
			up = userPreferences.get(key);
		} else {
			up = new BDT_UserPreference__c(name = key);
		}
		
		String value = '';
		if (userPreferences.containsKey(key)) {
			value = up.text__c + ':';
		} else {
			// no values, no change needed
			return;
		}
		
		For (String studyId : StudyIdSet) {
			value = value.replace(studyId, '');
			value = value.replace('::',':');
		}
		value = value.removeStart(':');
		value = value.removeEnd(':');
		up.Text__c = value;
		
		userPreferences.put(key,up);

		/** UPDATE QUICKLIST **/
		removeStudyFromQuickList (StudyIdSet);
		
	}

/******* MAINTAIN USERPREFERENCE QUICKLIST ***********/

	/**
	 * Add a project to the quick list
	 * @author Maurice Kremer
	 * @version 23-Dec-13
	 * @param String ProjectID, String ProjectCode, String ProjectSponsor
	 */	
	public void addProjectToQuickList (String ProjectID, String ProjectCode, String ProjectSponsor) {
		
		List<quickListWrapper> quickList= getQuickList ();
		
		List<quickListWrapper> newQuickList = new List<quickListWrapper>();
		quickListWrapper newItem = new quickListWrapper();
		newItem.projectCode = ProjectCode;
		newItem.projectId = ProjectID;
		newItem.sponsorCode = ProjectSponsor;
		newQuickList.add(newItem);
		
		// append other items to a maximum of 6
		For (Integer i = 0; i < BDT_UserPreferenceDataAccessor.QUICKLISTSIZE && i < quickList.size(); i++ ) {
		
			if (ProjectId == quickList[i].projectId) {
				// same project, skip to next iteration
				continue;
			}
			
			newQuickList.add(quickList[i]);
		}
		
		setQuickList(newQuickList);
		
	}
	
	/**
	 * Remove a project from the quick list
	 * @author Maurice Kremer
	 * @version 24-Dec-13
	 * @param Study__c
	 */
	public void removeProjectFromQuickList (String ProjectID) {

		List<quickListWrapper> quickList= getQuickList ();
		
		For (integer i = 0 ; i < quickList.size() ; i++) {
			if (ProjectID == quickList[i].projectId) {
				// reduce study list for this project
				quickList.remove(i);
				// finished
				break;
			}
		}
		
		setQuickList (quickList);

	}
	
	/**
	 * Add a study id set to its project in the quick list
	 * @author Maurice Kremer
	 * @version 23-Dec-13
	 * @param Study__c
	 */	
	public void addStudyToQuickList (Set<String> StudyIdSet) {
		
		List<quickListWrapper> quickList= getQuickList ();
		String ProjectId = getPreference(BDT_UserPreferenceDataAccessor.PROJECT);
		
		For (quickListWrapper q:quickList) {
			if (ProjectId == q.projectId) {
				// extend study list for this project
				if (q.studySet == null) {
					q.studySet = StudyIdSet;
				} else {
					q.studySet.addAll(StudyIdSet);
				}
				// finished
				break;
			}
		}
		
		setQuickList (quickList);
		
	}

	/**
	 * Remove a study id set from its project in the quick list
	 * @author Maurice Kremer
	 * @version 23-Dec-13
	 * @param Study__c
	 */
	public void removeStudyFromQuickList (Set<String> StudyIdSet) {

		List<quickListWrapper> quickList= getQuickList ();
		String ProjectId = getPreference(BDT_UserPreferenceDataAccessor.PROJECT);
		
		For (quickListWrapper q:quickList) {
			if (ProjectId == q.projectId) {
				// reduce study list for this project
				q.studySet.removeAll(StudyIdSet);
				// finished
				break;
			}
		}
		setQuickList (quickList);

	}
	
	/**
	 * Deserialize quicklist from user preferences
	 * @author Maurice Kremer
	 * @version 23-Dec-13
	 * @return list<quickListWrapper>
	 */
	public list<quickListWrapper> getQuickList () {
		list<quickListWrapper> result = new list<quickListWrapper>();
		
		try {
			result = (list<quickListWrapper>)JSON.deserialize(userPreferences.get(BDT_UserPreferenceDataAccessor.QUICKLIST).LongText__c,list<quickListWrapper>.class);
		} catch (Exception e) {}
		
		return result;
	}
	
	/**
	 * Serialize quicklist to user preferences
	 * @author Maurice Kremer
	 * @version 23-Dec-13
	 * @param list<quickListWrapper>
	 */
	public void setQuickList (list<quickListWrapper> quickList) {
		
		list<quickListWrapper> result = new list<quickListWrapper>();
		
		String key = BDT_UserPreferenceDataAccessor.QUICKLIST;
		
		// find existing or create new
		BDT_UserPreference__c up;
		if (userPreferences.containsKey(key)) {
			up = userPreferences.get(key);
		} else {
			up = new BDT_UserPreference__c(name = key);
		}
		
		up.LongText__c = JSON.serialize(quickList);
		userPreferences.put(key,up);
	}

	/**
	 * Activate an item from the quicklist
	 * @author Maurice Kremer
	 * @version 23-Dec-13
	 */
	public void reloadQuickListProject (Client_Project__c project) {
		list<quickListWrapper> quickList = getQuickList ();
		
		For (Integer i = 0; i < quickList.size(); i++) {
			quickListWrapper q = quickList[i];
			if (q.projectId == project.Id) {
				// set the selected project and studies
				setProjectAndStudies (project, q.studySet);
				
				// ensure the project is now at top of the list
				setQuickList(moveProjectToTopOfQuickList(quickList,i));
				
				// exit loop after project was found
				break;
			}
		}
		
	}

	public list<quickListWrapper> moveProjectToTopOfQuickList (list<quickListWrapper> quickList, Integer i) {
		list<quickListWrapper> newList = new list<quickListWrapper>();
		newList.add(quickList.remove(i));
		newList.addAll(quickList);
		return newList;
	}


	/**
	 * Wrapper for the quickList
	 * @author Maurice Kremer
	 * @version 23-Dec-13
	 */
	public class quickListWrapper {
		
		public string 		projectId 	{get;set;}
		public string 		projectCode {get;set;}
		public string		sponsorCode {get;set;}
		public set<String> 	studySet	{get;set;}

	}
}