/**
*   'PRA_ClientTaskService' class is to access all the client task related services.
*   @author   Devaram Bhargav
*   @version  20-Nov-2013
*   @since    20-Nov-2013
*/
public with sharing class PRA_ClientTaskService {
    
    /*
    *   This class is to capture the custom exceptions.
    */
    public class ClientTaskServiceException extends Exception{}
    
    /*
    *   These variables are used to get the client tasks.
    */
    public static final String TEXT_DISPLAY_ALL_CLIENT_TASKS   = 'Display All Client Units';
    public static final String TEXT_OPEN_CLIENT_TASKS          = 'Re-Open Client Units';
    public static final String TEXT_CLOSE_CLIENT_TASKS         = 'Close Client Units';
    
    public static final String TO_DISPLAY_ALL_CLIENT_TASKS     = 'Display All Client Units';
    public static final String TO_RE_OPEN_CLIENT_TASKS         = 'Re-Open Client Units';
    public static final String TO_CLOSE_CLIENT_TASKS           = 'Close Client Units';
    
    /**
    *   This Method converts all the client tasks list with predefined attributes value . 
    *   @return   the list of ClientTaskWrapper .
    */
    public static List<ClientTaskWrapper> getClientTaskWrapperListbyAction(String ProjectID,String SelectedAction){
        //Create a List that needs to return.
        List<ClientTaskWrapper>  ClientTaskWrapperList = new List<ClientTaskWrapper>();
        
        //make the list of client tasks
        List<Client_Task__c> clienttasks = new List<Client_Task__c>(); 
        
        //Loop over the results make a list of wrapper.
        for(Client_Task__c ct:PRA_DataAccessor.getClientTaskbyProjectOpenClosedStatus(ProjectID,SelectedAction)){
            
            Boolean bselect;
            
            //This is very tricky, when you display all client tasks, you should verify which has close date or not,
            //but if not in this mode the rest of all client tasks will display as unselected.
            if(ct.Close_Date__c!=null && SelectedAction==TO_DISPLAY_ALL_CLIENT_TASKS)
                bselect = true;
            else
                bselect = false;
            
            ClientTaskWrapperList.add(new ClientTaskWrapper(ct.Id, ct.Client_Unit_Number__c, ct.Description__c, ct.Combo_Code__c,ct.close_date__c,bselect));
            
        } 
        system.debug('-----ClientTaskWrapperList --------'+ClientTaskWrapperList);
        return ClientTaskWrapperList;
    }
    
    /**
    *   This Method converts all the client tasks list with predefined attributes value . 
    *   @return   ErrorMsg.
    */
    public static String updateClientTaskwithCloseDate(List<string> ctIDList,Date closedate){
        
        //make the list of client tasks
        List<Client_Task__c> clienttasks = new List<Client_Task__c>(); 
        
        //Loop over the results make a list of wrapper.
        for(Client_Task__c ct:PRA_DataAccessor.getClientTaskbyctIDs(ctIDList)){            
            //updating the date the close date.
            ct.Close_Date__c=closedate;
            //add the updated tasks to a list.
            clienttasks.add(ct);            
        }        
        String ErrorMsg=PRA_DataAccessor.upsertClientTaskList(clienttasks);
        if((ErrorMsg==null || ErrorMsg=='') && closedate!=null )
            ErrorMsg=updateUniteffortbyclosedate(ctIDList,closedate);
        system.debug('-----ErrorMsg --------'+ErrorMsg+ctIDList+closedate);
        return ErrorMsg;
    }
    
    /**
    *   This Method updates the unit_effort__c object records of forecast units, forecast effort and contract budgeted units as '0'. 
    *   it only updates the records after the predefined attribute as Month_applies_to__c>=closedate for list of client tasks.
    *   @return   the ErrorMsg .
    */
    public static String updateUniteffortbyclosedate(List<string> ctIDList,Date closedate){
        
        //make the list of Unit_effort__c
        List<Unit_effort__c> UnitEffortList = new List<Unit_effort__c>(); 
        
        //Loop over the results make a list to update.
        for(Unit_effort__c ut:PRA_DataAccessor.getUnitEffortByTaskIDsandMonth(ctIDList,closedate)){            
            
            //updating the forecast units, forecast effort and contract budgeted units as '0'.
            ut.Forecast_Unit__c       = 0;
            ut.Forecast_Effort__c     = 0;
            ut.Contracted_BDG_Unit__c = 0;
            
            //add the updated units to a list.
            UnitEffortList.add(ut);            
        }        
        String ErrorMsg = PRA_DataAccessor.upsertUnitEffortList(UnitEffortList);
        system.debug('-----ErrorMsg --------'+ErrorMsg+UnitEffortList+closedate);
        return ErrorMsg;
    }
    
    /**
    *   This Method returns error message if they found any failed CO status.     
    *   @return   the ErrorMsg if found any failed CO.
    */
    public static String ValidatefailedCO(string ProjectID, string Status){        
        String ErrorMsg;
        //make the list of Change_Order__c
        List<Change_Order__c> COList = PRA_DataAccessor.getCObyProjectIDandStatus(ProjectID,Status); 
        
        //check if any faile dCO's exist then assign to the error message 
        if (COList.size()>0) {
            ErrorMsg  = 'Cannot Close/Re-open the client units, as you have failed Change Order. Fix the Change order '+COList[0].name;
            ErrorMsg += 'and then try to Close/Re-open the client units';
        }
        system.debug('-----ErrorMsg --------'+ErrorMsg+ProjectID+Status);
        return ErrorMsg;
    }
    
    /*
    *   ClientTaskWrapper is used to make a wrapper list to display all the client tasks for a project with option even to select/deselect.
    */
    public class ClientTaskWrapper {
        public Id ClienttaskId          {get;set;}
        public String clientUnitNumber  {get;set;}
        public String description       {get;set;}
        public String combocode         {get;set;}         
        public Date closedate           {get;set;} 
        public Boolean selected         {get;set;} 
        
        
        public ClientTaskWrapper(Id clientTaskId, String unitNumber, String unitName, String combocode,Date closedate,Boolean checked) {
            this.ClienttaskId       = clientTaskId;
            this.clientUnitNumber   = unitNumber;
            this.description        = unitName;
            this.combocode          = combocode;
            this.closedate          = closedate;
            this.selected           = checked;
        }
    } 
}