/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PRA_ProjectHealthIndicatorControllerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
         Test.startTest();
         Client_Project__c clientProjectObj=new Client_Project__c();
         clientProjectObj.Name='ABC Project';
         clientProjectObj.PRA_Project_Id__c='ABC 001';
         clientProjectObj.Migrated_date__c=Date.today();
         Insert clientProjectObj;
         
         PHI_Project__c projectObj=new PHI_Project__c();
         projectObj.Project_ID__c=clientProjectObj.Id;
         projectObj.First_Site_Activated__c=Date.today()-1;
         projectObj.Last_Site_Activated__c=Date.today();
         Insert projectObj;
         
         Country__c countryObj=new Country__c();
         countryObj.Name='USA';
         Insert countryObj;
         
         PHI_Monthly__c monthlyObj1=new PHI_Monthly__c();
         monthlyObj1.Project_ID__c=clientProjectObj.Id;
         monthlyObj1.Country_ID__c=countryObj.Id;
         monthlyObj1.Month_Applies_To__c=Date.today();
         monthlyObj1.Total_Number_of_Sites_Activated__c=10;
         monthlyObj1.Total_Number_of_Sites_Planned__c=10;
         monthlyObj1.Total_Number_of_Subjects_Screened__c=10;
         monthlyObj1.Total_Number_of_Subjects_Enrolled__c=10;
         Insert monthlyObj1;
         
         PHI_Monthly__c monthlyObj2=new PHI_Monthly__c();
         monthlyObj2.Project_ID__c=clientProjectObj.Id;
         monthlyObj2.Country_ID__c=countryObj.Id;
         monthlyObj2.Month_Applies_To__c=Date.today()+1;
         monthlyObj2.Total_Number_of_Sites_Activated__c=20;
         monthlyObj2.Total_Number_of_Sites_Planned__c=20;
         monthlyObj2.Total_Number_of_Subjects_Screened__c=20;
         monthlyObj2.Total_Number_of_Subjects_Enrolled__c=20;
         Insert monthlyObj2;
         
         PRA_ProjectHealthIndicatorController obj=new PRA_ProjectHealthIndicatorController();
         //obj.searchprojectText='ABC Project';
         obj.projectText='ABC Project';
         obj.getSearchOnSites();
         obj.getSearchOnSubjects(); 
         
         obj.selectedSite='Cumulative View';
         obj.selectedSubject='Cumulative View';
         obj.getgraphs();
         obj.getgraphs1();
         obj.searchproject();
         obj.SearchSiteBasedOnCountry();
         obj.SearchSites();
         obj.SearchSubjects();
         obj.SearchSubjectsBasedOnCountry();
         //obj.tabularVisibility();
         //obj.SubjectstabularVisibility();
         //obj.tabularVisibility();
         //obj.SubjectstabularVisibility();
         
         obj.selectedSite='Country';
         obj.selectedSubject='Country';
         obj.getgraphs();
         obj.getgraphs1();
         obj.searchproject();
         obj.SearchSiteBasedOnCountry();
         obj.SearchSites();
         obj.SearchSubjects();
         obj.SearchSubjectsBasedOnCountry();
         
         
         obj.selectedSite='Month';
         obj.selectedSubject='Month';
         obj.getgraphs();
         obj.getgraphs1();
         obj.searchproject();
         obj.SearchSiteBasedOnCountry();
         obj.SearchSites();
         obj.SearchSubjects();
         obj.SearchSubjectsBasedOnCountry();
         
         PRA_AutoCompleteController.findSObjects('Client_Project__c', 'Name', 'test', 'Country', '');
         PRA_AutoCompleteController.findSObjects('', '', '', '', '');
                  
         Test.stopTest();
    }
}