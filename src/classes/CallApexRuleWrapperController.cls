public class CallApexRuleWrapperController {
   
    public CallApexRuleWrapperController() {
        String jsonInput = '{"condition":">=:50","statuses":"In Progress;Pending","step":"Initial Database Set Up","flow":"P-Clinical Informatics Rollup Milestones","object":"ecrf__c","method":"checkStraightCrossFlowRule"}';
        try {
            prepareData(jsonInput);
        } catch (Exception e) {
            
        }
    }
    
    private void prepareData(String jsonInput) {
        
        Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(jsonInput);
        
        String conditionsStr = (String)m.get('condition');
        if (conditionsStr != null && conditionsStr.length() > 0) {
            String[] conditionsList = conditionsStr.split(':', 2);
            this.condition = conditionsList[0];
            if (conditionsList.size() > 1) {
                this.condition_value = conditionsList[1];
            }
        }
        if (m.get('statuses') != null) {
            this.statuses = ((String)m.get('statuses')).split(';');
        }
        
        String currentStep = ((String)m.get('stepid'));
        String currentStepName = ((String)m.get('step'));
        String currentFlow = ((String)m.get('flowid'));
        String currentFlowName = ((String)m.get('flow'));
        String currentDataobject = ((String)m.get('object'));
        
        if (Schema.getGlobalDescribe().get(currentDataobject) != null) {
            this.dataobject = Schema.getGlobalDescribe().get(currentDataobject).getDescribe().getLabel();
            
            STSWR1__Flow_Step_Junction__c[] stepsList = [SELECT Id, Name, STSWR1__Flow__c, STSWR1__Flow__r.Name 
                FROM STSWR1__Flow_Step_Junction__c 
                WHERE Id = :currentStep 
                    AND STSWR1__Flow__c = :currentFlow
                    AND STSWR1__Flow__r.STSWR1__Object__c = :this.dataobject
                ORDER BY STSWR1__Flow__c
                LIMIT 1];
            if (stepsList.size() > 0) {
                this.step = stepsList[0].Id;
                this.flow = stepsList[0].STSWR1__Flow__c;
            } else {
                stepsList = [SELECT Id, Name, STSWR1__Flow__c, STSWR1__Flow__r.Name 
                    FROM STSWR1__Flow_Step_Junction__c 
                    WHERE Name = :currentStepName 
                        AND STSWR1__Flow__r.Name = :currentFlowName
                        AND STSWR1__Flow__r.STSWR1__Object__c = :this.dataobject
                    ORDER BY STSWR1__Flow__c
                    LIMIT 1];
                if (stepsList.size() > 0) {
                    this.step = stepsList[0].Id;
                    this.flow = stepsList[0].STSWR1__Flow__c;
                }
            }
            
            if (this.flow != null) {
                STSWR1__Item__c parentItem = [
                        Select STSWR1__Parent__c
                            From STSWR1__Item__c 
                            Where STSWR1__Source_Flow__c = :this.flow
                            LIMIT 1];
                
                if (parentItem != null) {
                    FolderId = parentItem.STSWR1__Parent__c;
                }
            }
        }
    }
    
    public String dataobject {get;set;}
    private Map<String, String> dataObjectMap = new Map<String, String>{};
    public List<SelectOption> ObjectTypes {
        get {
            if(ObjectTypes == null) {
                dataObjectMap = new Map<String, String>{};
                ObjectTypes = new List<SelectOption>();
                ObjectTypes.add(new SelectOption('', '--None--'));
                for (String objectName : PAWS_APIHelper.PAWS_OBJECTS)  {
                    Schema.DescribeSObjectResult describe = Schema.getGlobalDescribe().get(objectName).getDescribe();
                    ObjectTypes.add(new SelectOption(describe.getLabel(), describe.getLabel()));
                    dataObjectMap.put(describe.getLabel(), objectName);
                }
            }
            return ObjectTypes;
        }
        set {
            flows = null;
            flow = null;
        }
    }
    
    public String flow {get;set;}
    private Map<String, String> flowsMap = new Map<String, String>{};
    public String FolderId {get; set;}
 
    public List<SelectOption> flows {
        get {
            if (flows == null) {
                flows = new List<SelectOption>();
                flowsMap = new Map<String, String>{};
                flows.add(new SelectOption('-1', '--Select Flow--'));
                if(dataobject == null || dataobject == '') {
                    return flows;
                }
                
                try {
                  if (FolderId != null) {
                    STSWR1__Item__c parentItem = [
                        Select STSWR1__Parent__c, STSWR1__Parent__r.STSWR1__Name_Value__c, STSWR1__Name_Value__c 
                            From STSWR1__Item__c 
                            Where Id = :FolderId];
                    flows.add(new SelectOption('folder-' + (parentItem.STSWR1__Parent__c == null ? 'root' : parentItem.STSWR1__Parent__c), '[..] - go up to "' + (parentItem.STSWR1__Parent__c == null ? 'root' : parentItem.STSWR1__Parent__r.STSWR1__Name_Value__c) + '" (current folder: "' + parentItem.STSWR1__Name_Value__c + '")'));
                  }
                  
                  List<STSWR1__Item__c> currentItems = [
                      Select STSWR1__Name_Value__c, STSWR1__Source_Flow__c, STSWR1__Type__c
                          From STSWR1__Item__c 
                          Where 
                              (STSWR1__Type__c = 'Folder' 
                                  And STSWR1__Parent__c = :FolderId)
                              OR (
                                  STSWR1__Type__c = 'File' 
                                  And STSWR1__Parent__c = :FolderId 
                                  And STSWR1__Source_Flow__r.STSWR1__Object__c = : dataobject
                                  AND STSWR1__Source_Flow__r.STSWR1__Is_Template__c = TRUE)
                          Order By STSWR1__Type__c Desc, STSWR1__Name_Value__c];
                          
                  for (STSWR1__Item__c currentItem : currentItems) {
                      if (currentItem.STSWR1__Type__c == 'Folder') {
                          flows.add(new SelectOption('folder-' + currentItem.Id, '[' + currentItem.STSWR1__Name_Value__c + ']'));
                      } else {
                          flows.add(new SelectOption(currentItem.STSWR1__Source_Flow__c, currentItem.STSWR1__Name_Value__c));
                          flowsMap.put(currentItem.STSWR1__Source_Flow__c, currentItem.STSWR1__Name_Value__c);
                      }
                  }
                }
                catch(Exception ex) {
                  ApexPages.addMessages(ex);
                }
            }
            return flows;
        }
        set;
    }
    
    public void SelectedItemOnChange() {
        try {
          if (flow.startsWith('folder-')) {
            FolderId = flow.split('-')[1];
            FolderId = (FolderId == 'root' ? null : FolderId);
            flows = null;
            flow = null;
          } else {
              
          }
        } catch(Exception ex) {
          ApexPages.addMessages(ex);
        }
    }   
        
    
    public String step {get;set;}
    private Map<String, String> stepsMap = new Map<String, String>{};
    public List<SelectOption> steps {
        get {
            steps = new List<SelectOption>();
            stepsMap = new Map<String, String>{};
            steps.add(new SelectOption('', '--None--'));
            if(flow == null || flow == '') {
                return steps;
            }
            STSWR1__Flow_Step_Junction__c[] stepsList = [SELECT Id, Name FROM STSWR1__Flow_Step_Junction__c 
                WHERE STSWR1__Flow__c = : flow
                ORDER BY STSWR1__Index__c];
            for (STSWR1__Flow_Step_Junction__c currentStep : stepsList) {
                steps.add(new SelectOption(currentStep.Id, currentStep.Name));
                stepsMap.put(currentStep.Id, currentStep.Name);
            }
            return steps;
        }
        set;
    }
   
    String[] statuses = new String[]{};
    public List<SelectOption> getItems() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Not Started','Not Started'));
        options.add(new SelectOption('In Progress','In Progress'));
        options.add(new SelectOption('Pending','Pending'));
        return options;
    }
    
    public String[] getStatuses() {
        return statuses;
    }

    public void setStatuses(String[] statuses) {
        this.statuses = statuses;
    }

    public String condition {get;set;}
    public List<SelectOption> getConditions() {
        List<SelectOption> conditions = new List<SelectOption>();
        conditions.add(new SelectOption('--None--','--None--'));
        conditions.add(new SelectOption('<','<'));
        conditions.add(new SelectOption('<=','<='));
        conditions.add(new SelectOption('=','='));
        conditions.add(new SelectOption('>=','>='));
        conditions.add(new SelectOption('>','>'));
        return conditions;
    }
    
    public String condition_value {get;set;}
    
    public PageReference save() {
    
        Map<String, String> resultMap = new Map<String, String>{};
        
        resultMap.put('method', 'checkStraightCrossFlowRule');
        resultMap.put('object', dataObjectMap.get(dataobject));
        resultMap.put('flow', flowsMap.get(flow));
        resultMap.put('flowid', flow);
        resultMap.put('step', stepsMap.get(step));
        resultMap.put('stepid', step);
        resultMap.put('statuses', String.join(statuses, ';'));
        if (condition != '--None--') {
            resultMap.put('condition', condition + ':' + condition_value);
        }
        
        result_json = JSON.serialize(resultMap);
        return null;
    }
    
    public String result_json {get;set;}

}