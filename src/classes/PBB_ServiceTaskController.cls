/** 
@Author Grace Guo
@Date 2015
@Description this class is inserting the Service Task for a service function.
*/
public with sharing class PBB_ServiceTaskController {

    public  ID  stID{get;set;}                     //to get the Service Task ID
    public  Service_Task__c st{get;set;}           //to get the Service Task Sobject for edit or new
    public  string[] SelectBSs{get;set;}           //to get the selected Business Segments
    public  Integer QuestionCount{get;set;}        //to get the selected Business Segments
    
    //Standard Constructor
    public PBB_ServiceTaskController(ApexPages.StandardController controller) {   
             
        SelectBSs = new list<String>();
        
        //check if the ST is for edit
        st= (Service_Task__c )controller.getRecord();
        
        //check if the ST is for edit
        stID = ApexPages.currentPage().getParameters().get('id');
        string clone =ApexPages.currentPage().getParameters().get('clone');
        System.debug('--clone --'+clone );
        if(stID !=null){
            st  = PBB_DataAccessor.getServiceTaskbyIDForBS(stID); 
             
            if(clone !=null)
                st=st.clone(false, true);  
     
            if(st.Business_Segment__c!=NULL){            
                String BS=st.Business_Segment__c; 
                System.debug('--BS--'+BS+st.Business_Segment__c);
                //get the selected BS from string to list for picklist
                SelectBSs = BS.split(';'); 
                System.debug('--SelectBSs --'+SelectBSs );
            }
        }
        //check if any Question names are attached to service tasks or not.
        QuestionCount =0;
        if(stID !=null && clone ==null){
           QuestionCount = [select count() from Service_Task_To_Service_Impact__c where Service_Task__c =:stID ]; 
        } 
    }  
    
    /** 
    @Author Bhargav Devaram
    @Date 2015
    @Description get the types of service tasks
    */
    public List<selectOption> getBusinessSegmentTypes(){
        List<SelectOption> options = new List<SelectOption>(); 
        for(PRA_Business_Unit__c bs:PBB_DataAccessor.getPRABusinessUnit()){
            system.debug('-bs.name--'+bs.name);
            options.add(new SelectOption(bs.name,bs.Business_Unit_Code__c+'-'+bs.name));            
        }  
        return options;
    }   
    
    /** 
    @Author Bhargav Devaram
    @Date 2015
    @Description save the service TASK for service function
    */
    public PageReference saveST () {          
        PageReference pageRef =null; 
                  
        try{                      
            //check if the Business segment is selected 
            if(SelectBSs.size()>0){    
                //convert the list of selected Business Segments to string
                String BS='';             
                for(String b:SelectBSs){
                    BS+=b+';';
                    System.debug('--bs--'+bs);
                }
                //save as a string to the multipicklist           
                st.Business_Segment__c =BS;
                System.debug('--st--'+st); 
                upsert st;
                pageRef=new PageReference('/'+st.id);
                pageRef.setRedirect(true);  
                System.debug('--pageRef--'+pageRef);                                         
            }
            else{
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''+'Business Segment is required'));               
            } 
        }
        catch (DMLException e) {
            ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR,e.getdmlMessage(0) );
            ApexPages.addMessage(msg);        
        }
        return pageRef;        
    } 
}