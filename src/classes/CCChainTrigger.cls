public with sharing class CCChainTrigger extends STSWR1.AbstractTrigger {
    private static final Map<String, String> KEY_PREFIX_TO_LOOKUP_FIELD_MAP = new Map<String, String>{
        SObjectType.PAWS_Project_Flow_Agreement__c.keyPrefix => 'PAWS_Project_Flow_Agreement__c',
        SObjectType.PAWS_Project_Flow_Country__c.keyPrefix => 'PAWS_Project_Flow_Country__c',
        SObjectType.PAWS_Project_Flow_Document__c.keyPrefix => 'PAWS_Project_Flow_Document__c',
        SObjectType.PAWS_Project_Flow_Site__c.keyPrefix => 'PAWS_Project_Flow_Site__c',
        SObjectType.PAWS_Project_Flow_Submission__c.keyPrefix => 'PAWS_Project_Flow_Submission__c',
        SObjectType.ecrf__c.keyPrefix => 'PAWS_Project_Flow__c'
    };
    
    private static final Map<String, String> LOOKUP_FIELD_TO_SOBJECT_NAME_MAP = new Map<String, String>{
        'PAWS_Project_Flow__c' => ecrf__c.class.getName()
    };
    
    private Map<ID, STSWR1__Flow__c> associatedFlows = new Map<ID, STSWR1__Flow__c>();
    private Map<ID, Critical_Chain__c> sourceChains = new Map<ID, Critical_Chain__c>();
    private Map<ID, SObject> sourceMap = new Map<ID, SObject>();
    
    public override void beforeInsert(List<SObject> records) {
        associatedFlows = selectFlows((List<Critical_Chain__c>) records);
        populatePAWSLookups((List<Critical_Chain__c>) records);
        populatePAWSProjects((List<Critical_Chain__c>) records);
        updateNames((List<Critical_Chain__c>) records);
    }
    
    public override void afterInsert(List<SObject> records)
    {
        createAggregatedMilestones((List<Critical_Chain__c>) records);
        createAggregatedMilestoneJunctions((List<Critical_Chain__c>) records);

        maintainEWCategories((List<Critical_Chain__c>) records);
    }
    
    public override void beforeUpdate(List<SObject> records, List<SObject> priors) {
        associatedFlows = selectFlows((List<Critical_Chain__c>) records);
        populatePAWSLookups((List<Critical_Chain__c>) records);
        populatePAWSProjects((List<Critical_Chain__c>) records);
        updateNames((List<Critical_Chain__c>) records);
    }

    public override void afterUpdate(List<SObject> records, List<SObject> priors)
    {
        maintainEWCategories((List<Critical_Chain__c>) records);
    }
    
    private Map<ID, STSWR1__Flow__c> selectFlows(List<Critical_Chain__c> chains) {
        List<ID> flowIDs = new List<ID>();
        for (Critical_Chain__c chain : chains) {
            flowIDs.add(chain.Flow__c);
        }
        
        return new Map<ID, STSWR1__Flow__c>([select STSWR1__Source_ID__c from STSWR1__Flow__c where Id in :flowIDs]);
    }
    
    private void populatePAWSLookups(List<Critical_Chain__c> chains) {
        for (Critical_Chain__c chain : chains) {
            STSWR1__Flow__c associatedFlow = associatedFlows.get(chain.Flow__c);
            /*
            if (chain.Flow__c == null || associatedFlow.STSWR1__Source_ID__c == null) {
                continue;
            }
            /**/
            if (associatedFlow == null)
            {
                continue;
            }
            else if (associatedFlow.STSWR1__Source_ID__c == null)//a template Flow
            {
                for (String lookupField : KEY_PREFIX_TO_LOOKUP_FIELD_MAP.values())
                {
                    chain.put(lookupField, null);
                }
            }
            else
            {
                String prefix = associatedFlow.STSWR1__Source_ID__c.substring(0, 3);
                if (KEY_PREFIX_TO_LOOKUP_FIELD_MAP.get(prefix) != null) {
                    chain.put(KEY_PREFIX_TO_LOOKUP_FIELD_MAP.get(prefix), associatedFlow.STSWR1__Source_ID__c);
                }
            }
        }
    }
    
    private void populatePAWSProjects(List<Critical_Chain__c> chains) {
        List<String> projectIDs = new List<String>();
        List<String> objectNames = new List<String>();
        List<ID> pawsProjectIDs = new List<ID>();
        for (Critical_Chain__c chain : chains) {
            pawsProjectIDs.add(chain.PAWS_Project_Flow__c);
            if (chain.PAWS_Project_Flow__c != null) {
                continue;
            }
            
            for (String field : KEY_PREFIX_TO_LOOKUP_FIELD_MAP.values()) {
                if (chain.get(field) != null) {
                    projectIDs.add((String) chain.get(field));
                    objectNames.add(LOOKUP_FIELD_TO_SOBJECT_NAME_MAP.containsKey(field) ? LOOKUP_FIELD_TO_SOBJECT_NAME_MAP.get(field) : field);
                    //objectNames.add(field);
                }
            }
        }
        
        Map<ID, ID> childIdToProjectIdMap = new Map<ID, ID>();
        for (String objectName : objectNames) {
            
            for (SObject each : Database.query('select PAWS_Project__c from ' + objectName + ' where Id in :projectIDs')) {
                childIdToProjectIdMap.put(each.Id, (ID) each.get('PAWS_Project__c'));
            }
        }
        
        for (Critical_Chain__c chain : chains) {
            STSWR1__Flow__c associatedFlow = associatedFlows.get(chain.Flow__c);
            if (chain.Flow__c == null || associatedFlow.STSWR1__Source_ID__c == null) {
                continue;
            }
            
            String preffix = associatedFlow.STSWR1__Source_ID__c.substring(0, 3);
            if (KEY_PREFIX_TO_LOOKUP_FIELD_MAP.get(preffix) != null) {
                String childID = (String) chain.get(KEY_PREFIX_TO_LOOKUP_FIELD_MAP.get(preffix));
                if (childIdToProjectIdMap.get(childID) != null) {
                    chain.put('PAWS_Project_Flow__c', childIdToProjectIdMap.get(childID));
                }
            }
        }
        
        Map<ID, ecrf__c> pawsProjectIdToProjectManager = new Map<ID, ecrf__c>([SELECT Name, ID, Project_Manager__c FROM ecrf__c WHERE ID IN :pawsProjectIDs]);        
        for (Critical_Chain__c chain : chains) {
            if (chain.PAWS_Project_Flow__c != null
                    && chain.Chain_Owner__c == null
                    && pawsProjectIdToProjectManager.get(chain.Paws_Project_Flow__c) != null)
            {
                chain.Chain_Owner__c = pawsProjectIdToProjectManager.get(chain.Paws_Project_Flow__c).Project_Manager__c;
            }
        }    
    }
    
    private List<EW_Aggregated_Milestone__c> selectExistingMilestones(List<Critical_Chain__c> chains)
    {
        List<ID> projectIDs = new List<ID>();
        for (Critical_Chain__c each : chains)
        {
            if (each.PAWS_Project_Flow__c != null)
            {
                projectIDs.add(each.PAWS_Project_Flow__c);
            }
        }
        
        return [select Name, PAWS_Project_Flow__c, Cloned_From_Milestone__c from EW_Aggregated_Milestone__c where PAWS_Project_Flow__c in :projectIDs];
    }
    
    private List<EW_Aggregated_Milestone__c> selectTemplateMilestones(List<Critical_Chain__c> chains)
    {
        List<ID> ewIDs = new List<ID>();
        for (Critical_Chain__c each : chains)
        {
            ewIDs.add(each.Cloned_From_Early_Warning__c);
        }
        
        return [select Name, Milestone_Target_Date__c, PAWS_Project_Flow__c, Cloned_From_Milestone__c, (select Early_Warning__c from Aggregated_Milestone_EW_Junctons__r) from EW_Aggregated_Milestone__c
            where Id in (select Aggregated_Milestone__c from EW_Aggregated_Milestone_EW_Junction__c where Early_Warning__c in :ewIDs)
            and PAWS_Project_Flow__c = null];
    }
    
    private List<EW_Aggregated_Milestone__c> matchTemplateMilestones(Critical_Chain__c chain, List<EW_Aggregated_Milestone__c> aggregatedMilestones)
    {
        List<EW_Aggregated_Milestone__c> templateMilestones = new List<EW_Aggregated_Milestone__c>();
        for (EW_Aggregated_Milestone__c aggregatedMilestone : aggregatedMilestones)
        {
            for (EW_Aggregated_Milestone_EW_Junction__c milestoneJunction : aggregatedMilestone.Aggregated_Milestone_EW_Junctons__r)
            {
                if (milestoneJunction.Early_Warning__c == chain.Cloned_From_Early_Warning__c)
                {
                    templateMilestones.add(aggregatedMilestone);
                }
            }
        }
        
        return templateMilestones;
    }
    
    private EW_Aggregated_Milestone__c matchMilestone(Critical_Chain__c chain, List<EW_Aggregated_Milestone__c> aggregatedMilestones)
    {
        for (EW_Aggregated_Milestone__c aggregatedMilestone : aggregatedMilestones)
        {
            if (chain.PAWS_Project_Flow__c != null && chain.PAWS_Project_Flow__c == aggregatedMilestone.PAWS_Project_Flow__c)
            {
                return aggregatedMilestone;
            }
            
            for (EW_Aggregated_Milestone_EW_Junction__c milestoneJunction : aggregatedMilestone.Aggregated_Milestone_EW_Junctons__r)
            {
                if (milestoneJunction.Early_Warning__c == chain.Cloned_From_Early_Warning__c)
                {
                    return aggregatedMilestone;
                }
            }
        }
        
        return null;
    }
    
    private EW_Aggregated_Milestone__c matchExistingMilestone(Critical_Chain__c chain, EW_Aggregated_Milestone__c templateMilestone, List<EW_Aggregated_Milestone__c> aggregatedMilestones)
    {
        for (EW_Aggregated_Milestone__c aggregatedMilestone : aggregatedMilestones)
        {
            if (chain.PAWS_Project_Flow__c != null
                    && chain.PAWS_Project_Flow__c == aggregatedMilestone.PAWS_Project_Flow__c
                    && templateMilestone.Id == aggregatedMilestone.Cloned_From_Milestone__c)
            {
                System.debug(System.LoggingLevel.ERROR, 'Existing Milestones found: ' + System.Json.serialize(aggregatedMilestone));//@TODO: Delete this line!
                return aggregatedMilestone;
            }
        }
        
        return null;
    }
    
    private void createAggregatedMilestones(List<Critical_Chain__c> chains)
    {
        List<EW_Aggregated_Milestone__c> templateMilestones = selectTemplateMilestones(chains);
        List<EW_Aggregated_Milestone__c> existingMilestones = selectExistingMilestones(chains);
        System.debug(System.LoggingLevel.ERROR, 'The following template milestones have been selected: ' + System.Json.serialize(templateMilestones));//@TODO: Delete this line!
        System.debug(System.LoggingLevel.ERROR, 'The following existing milestones have been selected: ' + System.Json.serialize(existingMilestones));//@TODO: Delete this line!
        
        List<EW_Aggregated_Milestone__c> newMilestones = new List<EW_Aggregated_Milestone__c>();
        for (Critical_Chain__c chain : chains)
        {
            for (EW_Aggregated_Milestone__c templateMilestone : matchTemplateMilestones(chain, templateMilestones))
            {
                System.debug(System.LoggingLevel.ERROR, 'Chain: ' + System.Json.serialize(chain) + ' Template Milestones found: ' + System.Json.serialize(templateMilestone));//@TODO: Delete this line!
                if (matchExistingMilestone(chain, templateMilestone, existingMilestones) != null)
                {
                    continue;
                }
                
                EW_Aggregated_Milestone__c newMilestone = templateMilestone.clone(false, true);
                newMilestone.Cloned_From_Milestone__c = templateMilestone.Id;
                newMilestone.PAWS_Project_Flow__c = chain.PAWS_Project_Flow__c;
                
                newMilestones.add(newMilestone);
                existingMilestones.add(newMilestone);
            }
        }
        
        insert newMilestones;
        //upsert existingMilestones;
    }
    
    private void createAggregatedMilestoneJunctions(List<Critical_Chain__c> chains)
    {
        List<EW_Aggregated_Milestone_EW_Junction__c> existingJunctions = selectAggregatedMilestoneJunctions(chains);
        List<EW_Aggregated_Milestone__c> targetMilestones = selectAggregatedMilestones(chains);
        
        List<EW_Aggregated_Milestone_EW_Junction__c> newJunctions = new List<EW_Aggregated_Milestone_EW_Junction__c>();
        
        for (Critical_Chain__c eachEW : chains)
        {
            List<EW_Aggregated_Milestone_EW_Junction__c> templateJunctions = matchTemplateAggregatedMilestoneJunctions(eachEW, existingJunctions);
            List<EW_Aggregated_Milestone_EW_Junction__c> builtJunctions = buildNewAggregatedMilestoneJunctions(eachEW, templateJunctions, existingJunctions, targetMilestones);
            newJunctions.addAll(builtJunctions);
            existingJunctions.addAll(builtJunctions);
        }
        
        insert newJunctions;
    }
    
    private List<EW_Aggregated_Milestone_EW_Junction__c> matchTemplateAggregatedMilestoneJunctions(Critical_Chain__c chain,
            List<EW_Aggregated_Milestone_EW_Junction__c> existingJunctions)
    {
        List<EW_Aggregated_Milestone_EW_Junction__c> templateAggregatedMilestoneJunctions = new List<EW_Aggregated_Milestone_EW_Junction__c>();
        for (Integer i = 0, n = existingJunctions.size(); i < n && chain.Cloned_From_Early_Warning__c != null; i++)
        {
            EW_Aggregated_Milestone_EW_Junction__c existingJunction = existingJunctions.get(i);
            if (existingJunction.Early_Warning__c == chain.Cloned_From_Early_Warning__c
                    && existingJunction.Aggregated_Milestone__r.PAWS_Project_Flow__c == null)
            {
                templateAggregatedMilestoneJunctions.add(existingJunction);
            }
        }
        
        return templateAggregatedMilestoneJunctions;
    }
    
    private List<EW_Aggregated_Milestone_EW_Junction__c> buildNewAggregatedMilestoneJunctions(Critical_Chain__c chain,
            List<EW_Aggregated_Milestone_EW_Junction__c> templateJunctions,
            List<EW_Aggregated_Milestone_EW_Junction__c> existingJunctions,
            List<EW_Aggregated_Milestone__c> aggregatedMilestones)
    {
        List<EW_Aggregated_Milestone_EW_Junction__c> newJunctions = new List<EW_Aggregated_Milestone_EW_Junction__c>();
        for (Integer j = 0, m = templateJunctions.size(); j < m && chain.Cloned_From_Early_Warning__c != null; j++)
        {
            EW_Aggregated_Milestone_EW_Junction__c templateJunction = templateJunctions.get(j);
            EW_Aggregated_Milestone_EW_Junction__c existingJunction;
            Boolean junctionExists = false;
            for (Integer i = 0, n = existingJunctions.size(); i < n; i++)
            {
                existingJunction = existingJunctions.get(i);
                if (existingJunction.Aggregated_Milestone__r.Cloned_From_Milestone__c == templateJunction.Aggregated_Milestone__c
                        && existingJunction.Aggregated_Milestone__r.PAWS_Project_Flow__c == chain.PAWS_Project_Flow__c
                        && existingJunction.Early_Warning__r.Cloned_From_Early_Warning__c == templateJunction.Early_Warning__c)
                {
                    junctionExists = true;
                    break;
                }
            }
            
            if (junctionExists)
            {
                continue;
            }
            
            for (EW_Aggregated_Milestone__c targetMilestone : aggregatedMilestones)
            {
                if (templateJunction.Aggregated_Milestone__c != targetMilestone.Cloned_From_Milestone__c)
                {
                    continue;
                }
                
                EW_Aggregated_Milestone_EW_Junction__c newJunction = new EW_Aggregated_Milestone_EW_Junction__c(
                    Aggregated_Milestone__c = targetMilestone.Id,
                    Early_Warning__c = chain.Id,
                    Aggregated_Milestone__r = targetMilestone,
                    Early_Warning__r = existingJunction.Early_Warning__r
                );
                
                newJunctions.add(newJunction);
                existingJunctions.add(newJunction);
            }
        }
        
        return newJunctions;
    }
    
    private List<EW_Aggregated_Milestone__c> selectAggregatedMilestones(List<Critical_Chain__c> chains)
    {
        List<ID> projectIDs = new List<ID>();
        for (Critical_Chain__c eachChain : chains)
        {
            if (eachChain.PAWS_Project_Flow__c != null)
            {
                projectIDs.add(eachChain.PAWS_Project_Flow__c);
            }
        }
        
        return [select PAWS_Project_Flow__c, Cloned_From_Milestone__c from EW_Aggregated_Milestone__c where PAWS_Project_Flow__c in :projectIDs];
    }
    
    private List<EW_Aggregated_Milestone_EW_Junction__c> selectAggregatedMilestoneJunctions(List<Critical_Chain__c> chains)
    {
        List<ID> ewIDs = new List<ID>();
        for (Critical_Chain__c eachChain : chains)
        {
            ewIDs.addAll(new List<ID>{eachChain.Id, eachChain.Cloned_From_Early_Warning__c});
        }
        
        return [select Aggregated_Milestone__c, Early_Warning__c, Early_Warning__r.Cloned_From_Early_Warning__c, Aggregated_Milestone__r.PAWS_Project_Flow__c, Aggregated_Milestone__r.Cloned_From_Milestone__c from EW_Aggregated_Milestone_EW_Junction__c where Early_Warning__c in :ewIDs];
    }
    
    private void updateNames(List<Critical_Chain__c> chains)
    {
        selectSources(chains);
        selectSourceChains(chains);
        for (Critical_Chain__c chain : chains)
        {
            updateName(chain);
        }
    }
    
    
    private Map<ID, SObject> selectSources(List<Critical_Chain__c> chains)
    {
        List<String> sourceIDs = new List<String>();
        List<String> sourceObjects = new List<String>();
        
        for (STSWR1__Flow__c associatedFlow : associatedFlows.values())
        {
            sourceIDs.add(associatedFlow.STSWR1__Source_ID__c);
            try
            {
                sourceObjects.add('' + ((ID) associatedFlow.STSWR1__Source_ID__c).getSObjectType());
            }
            catch (Exception e)
            {
                System.debug(System.LoggingLevel.ERROR, 'CCChainTrigger.selectSources: ' + e.getMessage());
            }
        }
        
        sourceMap = new Map<ID, SObject>();
        for (String sourceObject : sourceObjects)
        {
            for (SObject sourceRecord : Database.query('select Name from ' + sourceObject + ' where Id in :sourceIDs'))
            {
                sourceMap.put(sourceRecord.Id, sourceRecord);
            }
        }
        
        return sourceMap;
    }
    
    private void selectSourceChains(List<Critical_Chain__c> chains)
    {
        List<ID> sourceChainIDs = new List<ID>();
        for (Critical_Chain__c chain : chains)
        {
            sourceChainIDs.add(chain.Cloned_From_Early_Warning__c);
        }
        
        sourceChains = new Map<ID, Critical_Chain__c>([select Name from Critical_Chain__c where Id in :sourceChainIDs]);
    }
    
    private void updateName(Critical_Chain__c chain)
    {
        STSWR1__Flow__c associatedFlow = associatedFlows.get(chain.Flow__c);
        Critical_Chain__c sourceChain = sourceChains.get(chain.Cloned_From_Early_Warning__c);
        //System.debug(System.LoggingLevel.ERROR, 'Associated Flow: ' + associatedFlow + ' Source Chain: ' + sourceChain + ' Source Record: ' + sourceMap.get(associatedFlow.STSWR1__Source_ID__c));//@TODO: Delete this line!
        if (associatedFlow != null && sourceChain != null)// && chain.Name == sourceChain.Name)
        {
            try
            {
                SObject sourceRecord = sourceMap.get(associatedFlow.STSWR1__Source_ID__c);
                String name = sourceChain.Name + ' - ' + sourceRecord.get('Name');
                
                chain.Name = name.substring(0, Math.min(80, name.length()));
            }
            catch (Exception e)
            {
                System.debug(System.LoggingLevel.ERROR, 'CCChainTrigger.updateName: ' + e.getMessage());
            }
        }
        
    }

    private void maintainEWCategories(List<Critical_Chain__c> chains)
    {
        new EWCategoryHelper().cloneCategories(chains);

        //Update EW data in junction records
        update [select Id from EW_Category_EW_Junction__c where Early_Warning__c in :chains];
    }
}