/**
@author Bhargav Devaram
@date 2015
@description this is PBB_CountryServicesController's test class
**/

@isTest   
private class PBB_CountryServicesCntrlTest {
    static testMethod void testPBB_CountryServicesController()    {
        
        test.startTest();   
            
        //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils ();
        tu.wfmp = tu.createwfmproject();
        tu.bid =tu.createbidproject();        
        tu.scen =tu.createscenarioAttributes();
        tu.country =tu.createCountryAttributes();
        tu.psc =tu.createPBBScenarioCountry();   
        tu.smm = tu.createServiceModelAttributes();
        tu.sa = tu.createServiceAreaAttributes();
        tu.sf = tu.createServiceFunctionAttributes();
        tu.St = tu.createServiceTaskAttributes();
        tu.smm = tu.approveServicemodel(tu.smm);
        tu.Wrm = tu.createWRModelAttributes();
        tu.scen.WR_Model__c  = tu.Wrm.id;
        update tu.scen;
        
        //set the pbb scenario id.
        Apexpages.currentPage().getParameters().put('PBBScenarioID', tu.scen.id);
        PBB_CountryServicesController csc = new PBB_CountryServicesController();        
        System.assertEquals(csc.CountrySelect,PBB_Constants.PROJECT_LEVEL);
        csc.getServiceTaskType();
        csc.getCountriesSelectedforPBBS();
        System.assertEquals(csc.selectedCountriesList.size(),1);
        csc.saveSelectedCountries();
        Set<String> TaskIds = new Set<String>{tu.st.id+'=PRA'};
        csc.selectedServicetasks = JSON.serialize(TaskIds);        
        csc.save();
        System.assertEquals(PBB_DataAccessor.GetSelectedServiceTask(tu.scen.id,tu.country.id).size(),0);
        
        test.stopTest();
    }   
}