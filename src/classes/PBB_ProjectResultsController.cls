/**
@author Bhargav Devaram
@date 2014
@description this controller class is to display the sub tab menus related to a scenario results
**/
public class PBB_ProjectResultsController {
   
   /** 
    * to hide/show the main tabs in the component.
    */
    public  List<String> HiddenHomeTabsList{get;set;}
   
   /**
    * to hide/show the subtabs in the component.
    */
    public  List<String> HiddenSubTabsList{get;set;} 
   
   /** 
   * Scenario Id for a project
   */
   public String ScenarioID{get;set;}
   
   //PBB Scenario Id for a project
   public String PBBScenarioID{get;set;}
   
   //Default it to project info tab
   public static final string Defaulttoprojectplantab  = '1';
   public string screenState   {get;private set;}
   
   /** 
   @author Bhargav Devaram
   @date 2014 
   @description Constructor  
   */ 
   Public PBB_ProjectResultsController(){ 
       HiddenHomeTabsList = new List<String>();
       HiddenSubTabsList = new List<String>(); 
       
//       HiddenSubTabsList.add('CUGpage');
       HiddenSubTabsList.add('RDpage');
       HiddenSubTabsList.add('Ppage');
        
       PBBScenarioID = ApexPages.currentPage().getParameters().get('PBBScenarioID');  
         screenState = PBB_ProjectResultsController.Defaulttoprojectplantab;
       if(PBBScenarioID!=null && PBBScenarioID!=''){
       
       }   
   }
}