public class MdmMappingServiceWrapper {
	public static final String UNIQUE_CRS_INSTANCE_NAME_CONSTRAINT = 'UNIQUE_CRS_INSTANCE_NAME';
	
	private static String ENDPOINT = '';
	private static String SERVICENAME = '';
	private static Integer TIMEOUT = 0;
	private static List<String> errors;
	
	static {
		initSettings();
	}   
	
	public static MdmMappingService.valueListResposne getAttributeValues() {
		MdmMappingService.valueListResposne response = null;
		MdmMappingService.MdmMappingServicePort service = getService();
		try {
			response = service.getAttributeKeyValues();
			system.debug('--------------- Attribute value retreival successful -------------------');
		} catch (Exception e) {
			errors = new List<String>();
			errors.add(e.getMessage());
			system.debug('---------------- Attribute value retrieval failed -----------------');
			system.debug('----- Error: ' + e.getMessage());
			system.debug('----- Stack trace: ' + e.getStackTraceString());
			system.debug('----- Cause: ' + e.getCause());
		}
		system.debug('----- Response is: ' + response);
		return response;
	}
	
	public static MdmMappingService.valueListResposne getAllCrsInstances() {
		MdmMappingService.valueListResposne response = null;
		MdmMappingService.MdmMappingServicePort service = getService();
		try {
			response = service.getAllInstances();
			system.debug('--------------- Crs Instance - All retreival successful -------------------');
		} catch (Exception e) {
			errors = new List<String>();
			errors.add(e.getMessage());
			system.debug('---------------- Crs Instance - All retrieval failed -----------------');
			system.debug('----- Error: ' + e.getMessage());
			system.debug('----- Stack trace: ' + e.getStackTraceString());
			system.debug('----- Cause: ' + e.getCause());
		}
		system.debug('----- Response is: ' + response);
		return response;
	}
	
	public static MdmMappingService.valueListResposne getCTMSCrsInstances() {
		MdmMappingService.valueListResposne response = null;
		MdmMappingService.MdmMappingServicePort service = getService();
		try {
			response = service.getCTMSInstances();
			system.debug('--------------- Crs Instance - CTMS retreival successful -------------------');
		} catch (Exception e) {
			errors = new List<String>();
			errors.add(e.getMessage());
			system.debug('---------------- Crs Instance - CTMS retrieval failed -----------------');
			system.debug('----- Error: ' + e.getMessage());
			system.debug('----- Stack trace: ' + e.getStackTraceString());
			system.debug('----- Cause: ' + e.getCause());
		}
		system.debug('----- Response is: ' + response);
		return response;
	}
	
	public static MdmMappingService.valueListResposne getETMFCrsInstances() {
		MdmMappingService.valueListResposne response = null;
		MdmMappingService.MdmMappingServicePort service = getService();
		try {
			response = service.getETMFInstances();
			system.debug('--------------- Crs Instance - ETMF retreival successful -------------------');
		} catch (Exception e) {
			errors = new List<String>();
			errors.add(e.getMessage());
			system.debug('---------------- Crs Instance - ETMF retrieval failed -----------------');
			system.debug('----- Error: ' + e.getMessage());
			system.debug('----- Stack trace: ' + e.getStackTraceString());
			system.debug('----- Cause: ' + e.getCause());
		}
		system.debug('----- Response is: ' + response);
		return response;
	}
	
	public static MdmMappingService.instanceListResponse getInstancesLikeName(String instanceName) {
		MdmMappingService.instanceListResponse response = null;
		if (instanceName != null) {
			MdmMappingService.MdmMappingServicePort service = getService();
			try {
				response = service.getInstancesLikeName(instanceName);
				system.debug('--------------- InstanceVO retreival successful -------------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- InstanceVO retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response ;
	}
	
	public static MdmMappingService.protocolSiteListResponse getProtocolMappingSitesSearch(Long protProjId, String praStudyId, 
		String clientProtNum, String instanceName) {
		MdmMappingService.protocolSiteListResponse response = null;
		if (protProjId != null && praStudyId != null && clientProtNum != null && instanceName != null) {
			MdmMappingService.MdmMappingServicePort service = getService();
			try {
				response = service.getProtocolSiteVOsForProtocolMapping(protProjId, praStudyId, clientProtNum, instanceName);
				system.debug('--------------- ProtocolSiteVO retreival successful -------------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- ProtocolSiteVO retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response ;
	}
	
	public static MdmMappingService.etmfSiteListResponse getEtmfSitesSearch(Long protocolId, String clientProtNum, 
		String ctmsInstance, String etmfInstance) {
		MdmMappingService.etmfSiteListResponse response = null;
		if (protocolId != null && clientProtNum != null && ctmsInstance != null && etmfInstance != null) {
			MdmMappingService.MdmMappingServicePort service = getService();
			try {
				response = service.getEtmfSiteVOsForProtocolMapping(protocolId, clientProtNum, ctmsInstance, etmfInstance);
				system.debug('--------------- EtmfSiteVO retreival successful -------------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- EtmfSiteVO retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response ;
	}
	
	public static MdmMappingService.mdmCrudResponse saveInstanceFromVO(MdmMappingService.instanceVO instanceVO) {
		MdmMappingService.mdmCrudResponse response = null;
		if (instanceVO != null) {
			MdmMappingService.MdmMappingServicePort service = getService();
			try {
				response = service.saveInstanceFromVO(instanceVO);
				system.debug('---------------- Crs Instance save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Crs Instance save failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmMappingService.mdmCrudResponse deleteCrsInstanceFromVO(MdmMappingService.instanceVO instanceVO) {
		MdmMappingService.mdmCrudResponse response = null;
		if (instanceVO != null) {
			MdmMappingService.MdmMappingServicePort service = getService();
			try {
				response = service.deleteInstanceFromVO(instanceVO);
				system.debug('---------------- CrsInstance delete successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- CrsInstance delete failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmMappingService.mdmCrudResponse saveCrsInstClientProjFromVO(MdmMappingService.protocolSiteVO protocolSiteVO) {
		MdmMappingService.mdmCrudResponse response = null;
		if (protocolSiteVO != null) {
			MdmMappingService.MdmMappingServicePort service = getService();
			try {
				response = service.saveCrsInstClientProjFromProtSiteVO(protocolSiteVO);
				system.debug('---------------- CrsInstClientProj save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- CrsInstClientProj save failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmMappingService.mdmCrudResponse deleteCrsInstClientProjFromVO(MdmMappingService.protocolSiteVO protocolSiteVO) {
		MdmMappingService.mdmCrudResponse response = null;
		if (protocolSiteVO != null) {
			MdmMappingService.MdmMappingServicePort service = getService();
			try {
				response = service.deleteCrsInstClientProjFromProtSiteVO(protocolSiteVO);
				system.debug('---------------- CrsInstClientProj delete successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- CrsInstClientProj delete failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmMappingService.mdmCrudResponse saveEtmfProtocolMappingFromVO(MdmMappingService.etmfSiteVO etmfSiteVO) {
		MdmMappingService.mdmCrudResponse response = null;
		if (etmfSiteVO != null) {
			MdmMappingService.MdmMappingServicePort service = getService();
			try {
				response = service.saveCrsInstProtFromSiteVO(etmfSiteVO);
				system.debug('---------------- CrsInstProt save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- CrsInstProt save failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmMappingService.mdmCrudResponse deleteEtmfProtocolMappingFromVO(MdmMappingService.etmfSiteVO etmfSiteVO) {
		MdmMappingService.mdmCrudResponse response = null;
		if (etmfSiteVO != null) {
			MdmMappingService.MdmMappingServicePort service = getService();
			try {
				response = service.deleteCrsInstProtFromSiteVO(etmfSiteVO);
				system.debug('---------------- CrsInstProt delete successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- CrsInstProt delete failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static List<String> getErrors() {
		return errors;
	}
	
	private static MdmMappingService.MdmMappingServicePort getService() {
		MdmMappingService.MdmMappingServicePort service = new MdmMappingService.MdmMappingServicePort();
		service.endpoint_x = ENDPOINT + SERVICENAME;
		service.timeout_x = TIMEOUT;
		return service;
	}
	
	private static void initSettings() {
		Map<String, MuleServicesCS__c> settings = MuleServicesCS__c.getAll();
		ENDPOINT = settings.get('ENDPOINT').Value__c;
		SERVICENAME = settings.get('MdmMappingService').Value__c;
		Integer to = Integer.valueOf(settings.get('TIMEOUT').Value__c);
		if (to != null && to > 0) {
			TIMEOUT = to;
		} 
		system.debug('--------- Using following Custom Settings for MuleServiceCS ---------');
		system.debug('ENDPOINT:'+ENDPOINT);
		system.debug('TIMEOUT:'+TIMEOUT);
		system.debug('----------------------- MuleServiceCS END ----------------------------');
	}
}