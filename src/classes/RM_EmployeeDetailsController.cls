public with sharing class RM_EmployeeDetailsController {
	public EMPLOYEE_DETAILS__c empl {get; private set;}
	public boolean bNewAssignment{get; private set;}
	
	
	
	public RM_EmployeeDetailsController(){
		string emplID = ApexPages.currentPage().getParameters().get('ID');
		empl = RM_EmployeeService.GetEmployeeDetailByEmployee(emplID);
		bNewAssignment = false; 
		if (RM_OtherService.IsRMAdmin(UserInfo.getUserId()))
			bNewAssignment = true;    
	   	else{
	     	set<string> stManagedBU = RM_OtherService.GetUserGroupBU(UserInfo.getUserId());  
	   		system.debug('---------ManagedBU---------'+stManagedBU);
	   		if (stManagedBU != null){
	   			if (stManagedBU.contains(empl.Business_unit_desc__c))
	   				bNewAssignment = true;
	   		} 
	   	}
	}
	
	 
}