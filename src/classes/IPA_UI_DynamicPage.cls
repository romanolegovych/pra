public class IPA_UI_DynamicPage
{
    private IPA_DynamicPageComponentFactory factory;
    public String pageId;
    public String articleId {get; private set;}
    
    //Constructor
    public IPA_UI_DynamicPage()
    {
        factory = new IPA_DynamicPageComponentFactory();
        pageId = '';
        articleId = '';
        
        try
        {
            pageId = ApexPages.currentPage().getParameters().get('Id');
            articleId = ApexPages.currentPage().getParameters().get('ArticleId');
        }catch(Exception ex) {}
    }
    
    public Component.Apex.OutputPanel getHeaderWidgets() {
        return factory.createOutputPanel(pageId, 'Header');
    }
    
    public Component.Apex.OutputPanel getFullWidgets() {
        return factory.createOutputPanel(pageId, 'Full');
    }
    
    public Component.Apex.OutputPanel getSplitLeftWidgets() {
        return factory.createOutputPanel(pageId, 'Split Left');
    }
    
    public Component.Apex.OutputPanel getSplitRightWidgets() {
        return factory.createOutputPanel(pageId, 'Split Right');
    }
    
    public Component.Apex.OutputPanel getFloatLeftWidgets() {
        return factory.createOutputPanel(pageId, 'Float Left');
    }
    
    public Component.Apex.OutputPanel getFloatRightWidgets() {
        return factory.createOutputPanel(pageId, 'Float Right');
    }
    
    public Component.Apex.OutputPanel getSidebarWidgets() {
        return factory.createOutputPanel(pageId, 'Sidebar');
    }
    
    public Component.Apex.OutputPanel getArticleDetailWidget() {
        return factory.createArticleDetailOutputPanel(articleId);
    }
    
    public Component.Apex.OutputPanel getTabGroupWidgets() {
        return factory.createOutputPanel(pageId, 'Tab Group');
    }
}