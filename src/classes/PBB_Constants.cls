/**
 @author     Niharika Reddy
 @date       2015
 @description    Defines all the constants used in this app
 */
 public class PBB_Constants {
     
         public static final String PROJECT_LEVEL = 'GLOBAL SERVICES';
         public static final String  INVALID_SCENARIO_ID = 'Invalid ScenarioID';
         
         public PBB_Constants(){
         }
         
         public List<selectOption> ParameterList=new List<selectOption>();
         public List<selectOption> getParameterList(){
         	ParameterList.add( new selectOption('', '--None--') );
        	ParameterList.add( new selectOption('PBB_Scenario__c', 'PBBScenario') );
        	return ParameterList;
    	 }
     
 }