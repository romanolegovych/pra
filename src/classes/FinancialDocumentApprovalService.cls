/** Implements the Service Layer of the object FinancialDocumentApproval__c
 * @author	Dimitrios Sgourdos
 * @version	23-Jan-2014
 */
public with sharing class FinancialDocumentApprovalService {
	
	/** Retrieve the list of Financial Document Approvals for the given financial document.
	 * @author	Dimitrios Sgourdos
	 * @version 17-Jan-2014
	 * @param	financialDocumentId		The id of the financial document
	 * @param	orderByField			The field that the Financial Category Total Prices will be ordered by
	 * @return	The list of Financial Document Approvals.
	 */
	public static List<FinancialDocumentApproval__c> getApprovalsByDocumentId(String financialDocumentId,
																			String orderByField) {
		// Retrieve from data accessor
		return FinancialDocumentApprovalDataAccessor.getFinancialDocumentApprovals(
														'FinancialDocument__c = ' + '\'' + financialDocumentId + '\'',
														orderByField
				);
	}
	
	
	/** Retrieve the list of Financial Document Approvals that are assigned with an approval rule for the given 
	 *	financial document.
	 * @author	Dimitrios Sgourdos
	 * @version 17-Jan-2014
	 * @param	financialDocumentId		The id of the financial document
	 * @param	orderByField			The field that the Financial Category Total Prices will be ordered by
	 * @return	The list of Financial Document Approvals.
	 */
	public static List<FinancialDocumentApproval__c> getApprovalsAssignedWithRulesByDocumentId(
																						String financialDocumentId,
																						String orderByField) {
		// Create where clause
		String whereClause = 'FinancialDocument__c = ' + '\'' + financialDocumentId + '\'';
		whereClause += ' AND IsAssignedToApprRule__c = true ';
		
		return FinancialDocumentApprovalDataAccessor.getFinancialDocumentApprovals(whereClause, orderByField);
	}
	
	
	/** Retrieve the list of Financial Document Approvals for the given financial document that the adjustments
	 *	notification is enabled.
	 * @author	Dimitrios Sgourdos
	 * @version 22-Jan-2014
	 * @param	financialDocumentId		The id of the financial document
	 * @param	orderByField			The field that the Financial Category Total Prices will be ordered by
	 * @return	The list of Financial Document Approvals.
	 */
	public static List<FinancialDocumentApproval__c> getNotifyEnabledApprovalsByDocumentId(String financialDocumentId,
																			String orderByField) {
		// Create where clause
		String whereClause = 'FinancialDocument__c = ' + '\'' + financialDocumentId + '\'';
		whereClause += ' AND AdjustmentsNotification__c = true ';
		
		return FinancialDocumentApprovalDataAccessor.getFinancialDocumentApprovals(whereClause, orderByField);
	}
	
	
	/** Retrieve the list of Financial Document Approvals for the given financial document that they are not approved
	 *	or rejected and they are not assigned with the 'Creator' rule. 
	 * @author	Dimitrios Sgourdos
	 * @version 23-Jan-2014
	 * @param	financialDocumentId		The id of the financial document
	 * @param	orderByField			The field that the Financial Category Total Prices will be ordered by
	 * @return	The list of Financial Document Approvals.
	 */
	public static List<FinancialDocumentApproval__c> getNotVotedApprovalsByDocumentId(String financialDocumentId,
																					String orderByField) {
		// Create where clause
		String whereClause = 'FinancialDocument__c = ' + '\'' + financialDocumentId + '\'';
		whereClause += ' AND Status__c = NULL ';
		whereClause += ' AND SequenceNumber__c != 1 ';
		
		return FinancialDocumentApprovalDataAccessor.getFinancialDocumentApprovals(whereClause, orderByField);
	}
	
	
	/** Create a Financial Document Approval instance.
	 * @author	Dimitrios Sgourdos
	 * @version 10-Jan-2014
	 * @param	financialDocumentId		The id of the financial document
	 * @param	emp						The employee that the approval will be assigned with
	 * @param	notifyFlag				If the approver will be notified when adjustments done
	 * @param	ruleDescription			The Description for the approval
	 * @param	sequenceNumber			The sequence number for the approval
	 * @return	The Financial Document Approval instance.
	 */
	public static FinancialDocumentApproval__c createApprovalInstance(String financialDocumentId,
																	Employee_Details__c emp,
																	String ruleId,
																	Boolean notifyFlag,
																	String ruleDescription,
																	Decimal sequenceNumber) {
		// Check the given employee
		if(emp == NULL) {
			return NULL;
		}
		
		// Create the approval instance
		FinancialDocumentApproval__c result = new FinancialDocumentApproval__c(
													AdjustmentsNotification__c = notifyFlag,
													ApproverEmail__c		= emp.Email_Address__c,
													ApproverJobCode__c		= emp.Job_Class_Desc__c,
													ApproverLocationCode__c = emp.Location_Code__c,
													ApproverName__c 		= getEmployeeFullName(emp),
													FinancialDocument__c	= financialDocumentId,
													FinancialDocumentApprRule__c = ruleId,
													IsAssignedToApprRule__c = (ruleId != NULL),
													RuleDescription__c		= ruleDescription,
													SequenceNumber__c		= sequenceNumber
												);
		return result;
	}
	
	
	/** Create Financial Document Approvals assigned with the given financial document for the given approval rules.
	 * @author	Dimitrios Sgourdos
	 * @version 14-Jan-2014
	 * @param	financialDocumentId		The id of the financial document
	 * @param	rules					The financial document approval rules
	 * @return	The Financial Document Approvals.
	 */
	public static List<FinancialDocumentApproval__c> cloneApprovalRulesToDocumentApprovals(String financialDocumentId,
																			List<FinancialDocumentApprRule__c> rules) {
		// Declare variables
		List<FinancialDocumentApproval__c> results = new List<FinancialDocumentApproval__c>();
		
		// Check if the financial document is invalid
		if(financialDocumentId==NULL) {
			return results;
		}
		
		// Create one approval for each rule
		Integer counter = 2; // (start from 2 cause 1 is for the document owner)
		
		for(FinancialDocumentApprRule__c tmpRule : rules) {
			results.add( new FinancialDocumentApproval__c(SequenceNumber__c = counter,
														RuleDescription__c = tmpRule.Description__c,
														FinancialDocument__c = financialDocumentId,
														FinancialDocumentApprRule__c = tmpRule.Id,
														IsAssignedToApprRule__c = true
						) );
			counter++;
		}
		
		return results;
	}
	
	
	/** Recalculate the Financial Document Approvals assigned with the given financial document for the approval rules
	 *	that exist in the system.
	 * @author	Dimitrios Sgourdos
	 * @version 16-Jan-2014
	 * @param	approvals				The initial financial document approvals
	 * @param	deletedApprovals		The approvals that are holding for deletion
	 * @param	financialDocumentId		The id of the financial document
	 * @return	The updated Financial Document Approvals. Indirectly also the updated list of approvals that are 
	 *			holding for deletion.
	 */
	public static List<FinancialDocumentApproval__c> recalculateApprovalsFromApplicableRules(
													List<FinancialDocumentApproval__c> approvals,
													List<FinancialDocumentApproval__c> deletedApprovals,
													String financialDocumentId) {
		// Declare Variables and check for valid data
		List<FinancialDocumentApproval__c> results = new List<FinancialDocumentApproval__c>();
		
		if(financialDocumentId==NULL 
				|| approvals==NULL 
				|| approvals.size()==0 
				|| approvals[0].SequenceNumber__c!=1
				|| deletedApprovals==NULL) {
			return approvals;
		}
		
		// Find the applicable approval rules and clone them to new approvals
		List<FinancialDocumentApprRule__c> applicableRules =
					FinancialDocumentApprRuleService.getApplicableApprovalRulesForFinancialDocument(financialDocumentId);
							
		List<FinancialDocumentApproval__c> newRulesApprovals = cloneApprovalRulesToDocumentApprovals(
																								financialDocumentId,
																								applicableRules);
		
		// Split the old approvals in the ones that are originated from admin rules and the ones that are additionals.
		// Starting from index 1 cause the first approval is the Creator approval
		List<FinancialDocumentApproval__c> oldAdminApprovals = new List<FinancialDocumentApproval__c>();
		List<FinancialDocumentApproval__c> oldAddedApprovals = new List<FinancialDocumentApproval__c>();
		
		for(Integer i=1; i<approvals.size(); i++) {
			if(approvals[i].IsAssignedToApprRule__c) {
				// If the associated rules is deleted, then delete the approval, else check if it is still applicable
				if(approvals[i].FinancialDocumentApprRule__c == NULL) {
					deletedApprovals.add(approvals[i]);
				} else {
					oldAdminApprovals.add(approvals[i]);
				}
			} else {
				oldAddedApprovals.add(approvals[i]);
			}
		}
		
		// Map the old "admin" approvals on the rule id
		Map<String,SObject> approvalsOnRulesMap = COM_SObjectUtils.mapSObjectOnKey (
																		oldAdminApprovals,
																		new List<String>{'FinancialDocumentApprRule__c'}
																	);
		
		// Add the creator approval and the approvals that are cloned from rules tested against the old "admin" approvals
		results.add(approvals[0]);
		
		Integer counter = 2;
		for(FinancialDocumentApproval__c tmpApproval : newRulesApprovals) {
			FinancialDocumentApproval__c newItem = new FinancialDocumentApproval__c();
			
			// If there is already approval for this rule keep it, else add the new one
			String key = tmpApproval.FinancialDocumentApprRule__c;
			if( approvalsOnRulesMap.containsKey(key) ) {
				newItem = (FinancialDocumentApproval__c) approvalsOnRulesMap.remove(key);
				newItem.RuleDescription__c = tmpApproval.RuleDescription__c;
			} else {
				newItem = tmpApproval;
			}
			
			newItem.SequenceNumber__c = counter;
			results.add(newItem);
			counter++;
		}
		
		// Add the old approvals that are not originated from rules to the results. 
		for(FinancialDocumentApproval__c tmpApproval : oldAddedApprovals) {
			tmpApproval.SequenceNumber__c = counter;
			results.add(tmpApproval);
			counter++;
		}
		
		// Add to the deleted approvals the ones that are remaining in the map of old approvals
		for(sObject tmpItem : approvalsOnRulesMap.values()) {
			deletedApprovals.add( (FinancialDocumentApproval__c) tmpItem );
		}
		
		return results;
	}
	
	
	/** Reset the approve/reject action from the given existing approvals and reset the release for approvals selection
	 *	of the financial document.
	 * @author	Dimitrios Sgourdos
	 * @version 16-Jan-2014
	 * @param	sourceList			The given Financial Document Approvals
	 * @param	financialDocument	The financial document that the approvals are assigned with
	 * @return	The given financial document with the released for approvals selection reset.
	 */
	public static FinancialDocument__c resetApprovals(List<FinancialDocumentApproval__c> sourceList,
													FinancialDocument__c financialDocument) {
		List<FinancialDocumentApproval__c> updateList = new List<FinancialDocumentApproval__c>();
		
		// Reset all the approvals
		for(FinancialDocumentApproval__c tmpApproval : sourceList) {
			Boolean approvalActionFlag = (tmpApproval.Status__c==FinancialDocumentApprovalDataAccessor.APPROVAL_APPROVED
										|| tmpApproval.Status__c==FinancialDocumentApprovalDataAccessor.APPROVAL_REJECTED);
			// If there is meaning for update
			if(tmpApproval.Id!=NULL	&& approvalActionFlag) {
				tmpApproval.Status__c = NULL;
				tmpApproval.ApprovalDate__c = NULL;
				tmpApproval.ApprovalNotes__c = NULL;
				updateList.add(tmpApproval);
			}
		}
		update updateList;
		
		// Update the financial document in case it is released for approvals
		if(financialDocument.IsApprovalReleased__c && updateList.size()>0) {
			financialDocument.IsApprovalReleased__c = false;
			update financialDocument;
		}
		return financialDocument;
	}
	
	
	/** Save the given financial document approvals.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Dec-2013
	 * @param	approvals			The initial list of approvals
	 * @param	financialDocument	The financial document that the approval are assigned with
	 * @return	If the save was successful or not.
	 */
	public static Boolean saveApprovals(List<FinancialDocumentApproval__c> approvals,
										FinancialDocument__c financialDocument) {
		// Check if the approvals are valid
		if( financialDocument == NULL
				|| approvals == NULL
				|| (financialDocument.IsApprovalReleased__c &&  (! isApprovalsValidForRelease(approvals)) )
				|| approvals.size()==0) {
			return false;
		}
		
		// Save the approvals
		try{
			upsert approvals;
		} catch(Exception e) {
			return false;
		}
		return true;
	}
	
	
	/** Determine if the Financial Document Approvals are valid for the document to be released for approve.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Jan-2014
	 * @param	sourceList			The given Financial Document Approvals
	 * @return	if the Financial Document Approvals are valid.
	 */
	public static Boolean isApprovalsValidForRelease(List<FinancialDocumentApproval__c> sourceList) {
		for(FinancialDocumentApproval__c tmpApproval : sourceList) {
			if( String.isBlank(tmpApproval.RuleDescription__c)
					|| String.isBlank(tmpApproval.ApproverName__c)
					|| String.isBlank(tmpApproval.ApproverEmail__c)
					|| (tmpApproval.DeadLineDate__c == NULL && tmpApproval.SequenceNumber__c!=1) ) {
				return false;
			}
		}
		
		return true;
	}
	
	
	/** Delete the given Financial Document Approvals in case they have id.
	 * @author	Dimitrios Sgourdos
	 * @version 09-Jan-2014
	 * @param	sourceList			The initial list of Approvals
	 * @return	If the deletion was successful or not.
	 */
	public static Boolean deleteApprovals(List<FinancialDocumentApproval__c> sourceList) {
		List<FinancialDocumentApproval__c> deletedList = new List<FinancialDocumentApproval__c>();
		
		// delete only the samples that have id
		for(FinancialDocumentApproval__c tmpApproval : sourceList) {
			if(tmpApproval.Id != NULL) {
				deletedList.add(tmpApproval);
			}
		}
		
		try{
			delete deletedList;
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}
	
	
	/** Calculates the full name of the given employee.
	 * @author	Dimitrios Sgourdos
	 * @version 10-Jan-2014
	 * @param	emp					The given employee
	 * @return	The full name (FirstName LastName) of the Employee.
	 */
	public static String getEmployeeFullName(Employee_Details__c emp) {
		if(emp == NULL) {
			return NULL;
		}
		
		String fullName = (String.isNotBlank(emp.First_Name__c) )? emp.First_Name__c+' ' : '';
		fullName += (String.isNotBlank(emp.Last_Name__c) )? emp.Last_Name__c : '';
		fullName = fullName.removeEnd(' ');
		
		return fullName;
	}
	
	
	/** Maps the Employees on their full name(FirstName LastName).
	 * @author	Dimitrios Sgourdos
	 * @version 10-Jan-2014
	 * @param	sourceList			The employees
	 * @return	The created map.
	 */
	public static Map<String, List<Employee_Details__c>> mapEmployeesOnFullName(List<Employee_Details__c> sourceList) {
		Map<String, List<Employee_Details__c>> results = new Map<String, List<Employee_Details__c>>();
		
		for(Employee_Details__c emp : sourceList) {
			String key = getEmployeeFullName(emp);
			List<Employee_Details__c> newList = new List<Employee_Details__c>();
			if( results.containsKey(key) ) {
				newList = results.remove(key);
			}
			newList.add(emp);
			results.put(key, newList);
		}
		
		return results;
	}
	
	
	/** Retrieves all the plenbox known Employees mapped on their full name(FirstName LastName).
	 * @author	Dimitrios Sgourdos
	 * @version 10-Jan-2014
	 * @param	plenboxUsers		The known plenbox users
	 * @return	The created map.
	 */
	public static Map<String, List<Employee_Details__c>> getEmployeesMappedOnFullName(List<User> plenboxUsers) {
		List<Employee_Details__c> empList = FinancialDocumentApprovalDataAccessor.getPlenboxEmployees(plenboxUsers,
																								'Email_Address__c');
		return mapEmployeesOnFullName(empList);
	}
	
	
	/** Retrieve the Employee with the given full name(FirstName LastName) and email from the map of Employees on their
	 *	full name.
	 * @author	Dimitrios Sgourdos
	 * @version 10-Jan-2014
	 * @param	userFullName		The full name of the Employee
	 * @param	userEmail			The e-mail of the Employee
	 * @param	sourceMap			The map of Employees on their full name
	 * @return	The Employee with the given full name and e-mail.
	 */
	public static Employee_Details__c getEmployeeByEmailAndFullNameFromMap(String userFullName,
																	String userEmail,
																	Map<String, List<Employee_Details__c>> sourceMap) {
		// Find the Employee
		if( sourceMap.containsKey(userFullName) ) {
			List<Employee_Details__c> empList = sourceMap.get(userFullName);
			// Retrieve the one with the same e-mail
			for(Employee_Details__c tmpEmp : empList) {
				if( userEmail.equalsIgnoreCase(tmpEmp.Email_Address__c) ) {
					return tmpEmp;
				}
			}
		}
		
		// The employee was not found
		return NULL;
	}
	
	
	/** Finds an employee with the given credentials or in case the employee doesn't exist initialize a new one with
	 *	the given credentials.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Jan-2014
	 * @param	empfirstName		The first name of the employee
	 * @param	empLastName			The last name of the employee
	 * @param	empEmail			The email of the employee
	 * @return	The found or the new Employee with the given credentials.
	 */
	public static Employee_Details__c getEmployeeByNameAndEmail(String empfirstName, String empLastName, String empEmail){
		return FinancialDocumentApprovalDataAccessor.getEmployeeByNameAndEmail(empfirstName, empLastName, empEmail);
	}
	
	
	/** Update the sequence number of the approvals to be solid.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Jan-2014
	 * @param	sourceList			The given approvals
	 * @return	The updated approvals.
	 */
	public static List<FinancialDocumentApproval__c> updateApprovalsSequence(List<FinancialDocumentApproval__c> sourceList) {
		Integer counter = 1;
		for(FinancialDocumentApproval__c tmpApproval : sourceList) {
			tmpApproval.SequenceNumber__c = counter;
			counter++;
		}
		return sourceList;
	}
	
	
	/** Approve/Reject the given approval depends on the given approveFlag.
	 * @author	Dimitrios Sgourdos
	 * @version 15-Jan-2014
	 * @param	approval			The given approval
	 * @param	approveFlag			This flag is true for approval and false for rejection
	 * @param	approvalNotes		The comments for approval/rejection
	 * @return	The updated approval.
	 */
	public static FinancialDocumentApproval__c approveRejectDocument(FinancialDocumentApproval__c approval,
																	Boolean approveFlag,
																	String	approvalNotes) {
		// Update approval
		approval.Status__c        = (approveFlag?
										FinancialDocumentApprovalDataAccessor.APPROVAL_APPROVED :
										FinancialDocumentApprovalDataAccessor.APPROVAL_REJECTED
									);
		approval.ApprovalDate__c  = Date.today();
		approval.ApprovalNotes__c = approvalNotes;
		
		try { // just in case
			upsert approval;
		} catch(Exception e){}
		
		return approval;
	}
	
	
	/** Determine if the document approvals that are assigned with a rule are up to date or not.
	 * @author	Dimitrios Sgourdos
	 * @version 17-Jan-2014
	 * @param	financialDocumentId		The id of the financial document
	 * @return	The updated approval.
	 */
	public static Boolean areDocumentApprovalsUpToDate(String financialDocumentId) {
		Set<String> applicableRulesSet = new Set<String>();
		Set<String> storedRulesSet 	   = new Set<String>();
		
		// Find the assigned approval rules and check if there is a deleted rule
		List<FinancialDocumentApproval__c> storedApprovalsList = getApprovalsAssignedWithRulesByDocumentId(
																								financialDocumentId,
																								'SequenceNumber__c');
		
		storedRulesSet = BDT_Utils.getStringSetFromList(storedApprovalsList, 'FinancialDocumentApprRule__c');
		
		// Find the applicable approval rules
		List<FinancialDocumentApprRule__c> applicableRulesList =
					FinancialDocumentApprRuleService.getApplicableApprovalRulesForFinancialDocument(financialDocumentId);
		applicableRulesSet = BDT_Utils.getStringSetFromList(applicableRulesList, 'Id');
		

		// Check if the two sets are equal
		if( applicableRulesSet.containsAll(storedRulesSet) && storedRulesSet.containsAll(applicableRulesSet) ) {
			return true;
		}
		
		// The sets are not equal
		return false;
	}
	
	
	/** Determine if an assigned rule from the given approvals is deleted or not.
	 * @author	Dimitrios Sgourdos
	 * @version 17-Jan-2014
	 * @param	sourceList			The financial document approvals
	 * @return	If an assigned rule from the given approvals is deleted or not.
	 */
	public static Boolean isAssignedRuleDeleted(List<FinancialDocumentApproval__c> sourceList) {
		// Try to find a NULL value
		for(FinancialDocumentApproval__c tmpApproval : sourceList) {
			if(tmpApproval.FinancialDocumentApprRule__c == NULL) {
				return true;
			}
		}
		
		return false;
	}
	
	
	/** Determine if at least one approval from the given ones is rejected.
	 * @author	Dimitrios Sgourdos
	 * @version 20-Jan-2014
	 * @param	sourceList			The financial document approvals
	 * @return	If at least one approval from the given ones is rejected.
	 */
	public static Boolean isAnApprovalRejected(List<FinancialDocumentApproval__c> sourceList) {
		for(FinancialDocumentApproval__c tmpItem : sourceList) {
			if(tmpItem.Status__c == FinancialDocumentApprovalDataAccessor.APPROVAL_REJECTED) {
				return true;
			}
		}
		
		// Then nobody has rejected it
		return false;
	}
	
	
	/** Determine if all the given approvals are approved.
	 * @author	Dimitrios Sgourdos
	 * @version 20-Jan-2014
	 * @param	sourceList			The financial document approvals
	 * @return	If all the given approvals are approved.
	 */
	public static Boolean isAllApprovalsApproved(List<FinancialDocumentApproval__c> sourceList) {
		for(FinancialDocumentApproval__c tmpItem : sourceList) {
			if(tmpItem.Status__c != FinancialDocumentApprovalDataAccessor.APPROVAL_APPROVED) {
				return false;
			}
		}
		
		// Then it is approved from everybody
		return true; 
	}
	
	
	/** Retrieve the emails of the users that are assigned with a document approval that have the notify flag enabled.
	 * @author	Dimitrios Sgourdos
	 * @version 22-Jan-2014
	 * @param	financialDocumentId		The id of the financial document
	 * @return	The emails of the users.
	 */
	public static Set<String> getUserEmailsForSendingNotification(String financialDocumentId) {
		// Retrieve the document approvals that have the notify flag enabled
		List<FinancialDocumentApproval__c> notifiedApprovals =
												FinancialDocumentApprovalService.getNotifyEnabledApprovalsByDocumentId(
																								financialDocumentId,
																								'SequenceNumber__c');
		
		// Create the set of the user emails
		Set<String> results = BDT_Utils.getStringSetFromList(notifiedApprovals, 'ApproverEmail__c');
		results.remove(NULL);
		
		return results;
	}
	
	
	/** Send email notifications to the users that are assigned with a document approval that 
	 *	the notify flag is enabled.
	 * @author	Dimitrios Sgourdos
	 * @version 22-Jan-2014
	 * @param	financialDocumentId		The id of the financial document
	 * @param	processStep				The process step that the change happened
	 * @param	plenboxUserName			The user that changed the financial document
	 */
	public static void sendAdjustmentsEmailNotifications(FinancialDocument__c financialDocument,
														String processStep,
														String plenboxUserName) {
		// Check parameters
		if(financialDocument == NULL || financialDocument.Id == NULL || processStep == NULL || plenboxUserName == NULL) {
			return;
		}
		
		// Initialize variables
		String subject = FinancialDocumentDataAccessor.ADJUSTMENTS_NOTIFICATION_EMAIL_SUBJECT;
		
		String HTMLBody = FinancialDocumentService.createHtmlBodyForEmailNotifications(financialDocument,
																					processStep,
																					plenboxUserName,
																					DateTime.Now(),
																					UserInfo.getTimeZone());
		
		Set<String> userEmailsSet = getUserEmailsForSendingNotification(financialDocument.Id);
		List<String> receiptTo = new List<String>();
		receiptTo.addAll(userEmailsSet);
		
		// Send email notification
		if( ! receiptTo.isEmpty() ) {
			Messaging.SingleEmailMessage notifyEmail = BDT_Utils.createEmailInstance(receiptTo, subject, HTMLBody, NULL);
			try {
				Messaging.sendEmail(new Messaging.SingleEmailMessage[] { notifyEmail });
			} catch(Exception e) {}
		}
	}
	
	
	/** Send email appointments to the users that are assigned with a document approval that it is not yet
	 *	approved or rejected.
	 * @author	Dimitrios Sgourdos
	 * @version 23-Jan-2014
	 * @param	financialDocumentId		The id of the financial document
	 * @param	sendFlag				A flag to be false in test classes to avoid trying sending emails
	 */
	public static void sendApprovalAppointments(FinancialDocument__c financialDocument, Boolean sendFlag) {
		// Check parameters
		if(financialDocument == NULL || financialDocument.Id == NULL) {
			return;
		}
		
		// Initialize variables
		List<Messaging.SingleEmailMessage> approvalEmailsList = new List<Messaging.SingleEmailMessage>();
		
		String subject = FinancialDocumentDataAccessor.RELEASE_FOR_APPROVAL_APPOINTMENT_SUBJECT;
		
		// Create emails for each not approved/rejected approval
		List<FinancialDocumentApproval__c> approvalList = getNotVotedApprovalsByDocumentId(financialDocument.Id,
																						'SequenceNumber__c');
		
		for(FinancialDocumentApproval__c tmpApproval : approvalList) {
			if(tmpApproval.DeadLineDate__c == NULL 
				|| String.isBlank(tmpApproval.ApproverName__c)
				|| String.isBlank(tmpApproval.ApproverEmail__c) ) {
					continue;
			}
			
			
			DateTime tmpDeadLineDate = tmpApproval.DeadLineDate__c;
			tmpDeadLineDate = tmpDeadLineDate.addDays(1);
			
			String HTMLBody = FinancialDocumentService.createHtmlBodyForApprovalEmailAppointment(financialDocument,
																						tmpApproval.RuleDescription__c,
																						tmpDeadLineDate);
			
			List<String> receiptTo = new List<String> { tmpApproval.ApproverEmail__c };
			
			blob tmpBlob = FinancialDocumentService.createDocumentApprovalAppointmentInstance(
															financialDocument.DocumentOwnerName__c,
															financialDocument.DocumentOwnerEmail__c,
															UserInfo.getTimeZone().getDisplayName(),
															tmpApproval.ApproverName__c,
															tmpApproval.ApproverEmail__c,
															tmpDeadLineDate);
			
			Messaging.EmailFileAttachment attachFile = new Messaging.EmailFileAttachment();
			attachFile.setFileName('invite.ics');
			attachFile.setBody(tmpBlob);
			attachFile.setContentType('text/calendar');
			
			Messaging.SingleEmailMessage approvalEmail = BDT_Utils.createEmailInstance(receiptTo,
																					subject,
																					HTMLBody,
																					attachFile);
			approvalEmailsList.add(approvalEmail);
		}
		
		// Send email appointments
		if( ! approvalEmailsList.isEmpty() && sendFlag) {
			try {
				Messaging.sendEmail(approvalEmailsList);
			} catch(Exception e) {}
		}
	}
}