/** Implements the test for the Selector Layer of the object StudyService__c
 * @author	Dimitrios Sgourdos
 * @version	28-Jan-2014
 */
@isTest
private class StudyServiceDataAccessorTest {
	
	/** Test the function getStudyServiceList
	 * @author	Dimitrios Sgourdos
	 * @version	27-Jan-2014
	 */
	static testMethod void getStudyServiceListTest() {
		// Create data
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001',
																				'BDT Project First Experiments');
		insert currentProject;
		
		ServiceCategory__c srvCat = new ServiceCategory__c(code__c='001',name='Service Category (1)');
		insert srvCat;
		
		List<Service__c> srvList = new List<Service__c>();
		srvList.add( new Service__c(Name = 'Srv 1', ServiceCategory__c=srvCat.Id) );
		srvList.add( new Service__c(Name = 'Srv 2', ServiceCategory__c=srvCat.Id) );
		insert srvList;
		
		Study__c study = new Study__c(Project__c = currentProject.Id);
		insert study;
		
		List<ProjectService__c> prSrvList = new List<ProjectService__c>();
		prSrvList.add(new ProjectService__c(Service__c = srvList[0].id, Client_Project__c = currentProject.id) );
		prSrvList.add(new ProjectService__c(Service__c = srvList[1].id, Client_Project__c = currentProject.id) );
		insert prSrvList;
		
		List<StudyService__c> stSrvList = new List<StudyService__c>();
		stSrvList.add( new StudyService__c(ProjectService__c = prSrvList[0].Id, NumberOfUnits__c=2, Study__c=study.Id));
		stSrvList.add( new StudyService__c(ProjectService__c = prSrvList[1].Id, NumberOfUnits__c=1, Study__c=study.Id));
		insert stSrvList;
		
		String errorMessage = 'Error in retrieving the Study Services from the system';
		
		// Check the function without where clause but order by field
		List<StudyService__c> results = StudyServiceDataAccessor.getStudyServiceList(NULL, 'NumberOfUnits__c');
		system.assertEquals(results.size(), 2, errorMessage);
		system.assertEquals(results[0].Id, stSrvList[1].Id, errorMessage);
		system.assertEquals(results[1].Id, stSrvList[0].Id, errorMessage);
		
		// Check the function with where clause
		results = StudyServiceDataAccessor.getStudyServiceList('NumberOfUnits__c=2', 'NumberOfUnits__c');
		system.assertEquals(results.size(), 1, errorMessage);
		system.assertEquals(results[0].Id, stSrvList[0].Id, errorMessage);
	}
	
	
	/** Test the function getStudyServiceById
	 * @author	Dimitrios Sgourdos
	 * @version	28-Jan-2014
	 */
	static testMethod void getStudyServiceByIdTest() {
		// Create data
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001',
																				'BDT Project First Experiments');
		insert currentProject;
		
		ServiceCategory__c srvCat = new ServiceCategory__c(code__c='001',name='Service Category (1)');
		insert srvCat;
		
		Service__c srv = new Service__c(Name = 'Srv 1', ServiceCategory__c=srvCat.Id);
		insert srv;
		
		Study__c study = new Study__c(Project__c = currentProject.Id);
		insert study;
		
		ProjectService__c prSrv = new ProjectService__c(Service__c = srv.id, Client_Project__c = currentProject.id);
		insert prSrv;
		
		StudyService__c stSrv = new StudyService__c(ProjectService__c = prSrv.Id,
													NumberOfUnits__c=2,
													Study__c=study.Id);
		insert stSrv;
		
		String errorMessage = 'Error in retrieving the Study Services from the system';
		
		// check the function with a not valid id to create an exception for code coverage
		StudyService__c result = StudyServiceDataAccessor.getStudyServiceById(NULL);
		system.assertEquals(result, NULL, errorMessage);
		
		// check the function with valid id
		result = StudyServiceDataAccessor.getStudyServiceById(stSrv.Id);
		system.assertEquals(result.Id, stSrv.Id, errorMessage);
	}
}