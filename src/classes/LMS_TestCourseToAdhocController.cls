/**
 * Test class for CourseToAdhoc controller
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestCourseToAdhocController {
	
	static List<Role_Type__c> roleTypes;
	static List<LMS_Role__c> roles;
	static List<Course_Domain__c> courseDomains;
	static List<LMS_Course__c> courses;
	static List<Country__c> countries;
    static List<Employee_Details__c> employees;
	static List<LMS_Role_Course__c> roleCourses;
	static List<LMS_Role_Employee__c> roleEmployees;
	static List<LMSConstantSettings__c> constants;
	static List<CourseDomainSettings__c> domains;
	static List<RoleAdminServiceSettings__c> settings;
	
	static void init() {
    	
    	constants = new LMSConstantSettings__c[] {
    		new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
    		new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
    		new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
    		new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D'),
    		new LMSConstantSettings__c(Name = 'typePRA', Value__c = 'PRA'),
    		new LMSConstantSettings__c(Name = 'typeProject', Value__c = 'Project Specific'),
    		new LMSConstantSettings__c(Name = 'typeAdhoc', Value__c = 'Additional Role'),
    		new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
    		new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
    		new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
    		new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
    		new LMSConstantSettings__c(Name = 'statusToDelete', Value__c = 'Delete'),
    		new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
    		new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended'),
    		new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
    		new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
    		new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
    		new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
    		new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
    		new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete')
    	};
    	insert constants;
    	
    	domains = new CourseDomainSettings__c[] {
    		new CourseDomainSettings__c(Name = 'Internal', Domain_Id__c = 'Internal'),
    		new CourseDomainSettings__c(Name = 'PST', Domain_Id__c = 'Project Specific'),
    		new CourseDomainSettings__c(Name = 'Archive', Domain_Id__c = 'Archive')
    	};
    	insert domains;
    	
    	settings = new RoleAdminServiceSettings__c[] {
    		new RoleAdminServiceSettings__c(Name = 'DOMAIN', Value__c = 'DOMAIN'),
    		new RoleAdminServiceSettings__c(Name = 'ENDPOINT', Value__c = 'ENDPOINT'),
    		new RoleAdminServiceSettings__c(Name = 'TIMEOUT', Value__c = '50000')
    	};
    	insert settings;
    	
		roleTypes = new Role_Type__c[] {
			new Role_Type__c(Name = 'PRA'),
			new Role_Type__c(Name = 'Project Specific'),
			new Role_Type__c(Name = 'Additional Role')
		};
		insert roleTypes;
		
		courseDomains = new Course_Domain__c[] {
			new Course_Domain__c(Domain__c = 'Internal'),
			new Course_Domain__c(Domain__c = 'Archive'),
			new Course_Domain__c(Domain__c = 'Project Specific')
		};
		insert courseDomains;
				
		courses = new LMS_Course__c[] {
    		new LMS_Course__c(Offering_Template_No__c = 'course123', Course_Code__c = 'C123', Title__c = 'Course 123', Domain_Id__c = 'Internal',
    						  Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
    						  Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse1',
    						  Discontinued_From__c = Date.today()+1, Duration__c = 60),
    		new LMS_Course__c(Offering_Template_No__c = 'course321', Course_Code__c = 'C124', Title__c = 'Course 132', Domain_Id__c = 'Archive',
    						  Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
    						  Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse2',
    						  Discontinued_From__c = Date.today()+1, Project_Id__c = 'Project1', Client_Id__c = 'Client1', Duration__c = 60),
    		new LMS_Course__c(Offering_Template_No__c = 'course124', Course_Code__c = 'C234', Title__c = 'Course 234', Domain_Id__c = 'Internal',
    						  Type__c = 'SOP', Status__c = 'Retired', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
    						  Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse3',
    						  Discontinued_From__c = Date.today()+1, Project_Id__c = 'Project1', Client_Id__c = 'Client1', Duration__c = 60),
    		new LMS_Course__c(Offering_Template_No__c = 'course341', Course_Code__c = 'C231', Title__c = 'Course 243', Domain_Id__c = 'Internal',
    						  Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
    						  Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse4',
    						  Discontinued_From__c = Date.today()+1, Project_Id__c = 'Project1', Client_Id__c = 'Client1', Duration__c = 60)
    	};
    	insert courses;
    	
    	countries = new Country__c[] {
    		new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='US/Canada', 
                               Daily_Business_Hrs__c=8.0)
    	};
    	insert countries;
	}
	
	static void initManyCourses() {
		Integer i = 1000;
		courses = new List<LMS_Course__c>();
		while(i < 2001)	{
			courses.add(new LMS_Course__c(Offering_Template_No__c = 'course' + i, Course_Code__c = 'C' + i, Title__c = 'Course ' + i, Domain_Id__c = 'Internal',
			  	Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
			 	Updated_On__c = Datetime.now(), Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse' + i,
			    Discontinued_From__c = Date.today()+1));
			i++;
		}
    	insert courses;
	}
	
	static void initRoles() {
		roles = new LMS_Role__c[] {
			new LMS_Role__c(Adhoc_Role_Name__c = 'TestRole1', Role_Type__c = roleTypes[2].Id, Status__c = 'Active', Sync_Status__c = 'Y'),
			new LMS_Role__c(Adhoc_Role_Name__c = 'TestRole2', Role_Type__c = roleTypes[2].Id, Status__c = 'Active', Sync_Status__c = 'N'),
			new LMS_Role__c(Adhoc_Role_Name__c = 'TestRole3', Role_Type__c = roleTypes[2].Id, Status__c = 'Inactive', Sync_Status__c = 'N')
		};
		insert roles;
	}
	
	static void initMappings() {
		roleCourses = new LMS_Role_Course__c[] {
			new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
			new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Draft Delete', Assigned_By__c = 'ASA'),
			new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
			new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Committed', Assigned_By__c = 'ASA')
		};
		insert roleCourses;
	}
	
	static void initEmployees() {
        employees = new Employee_Details__c[] {
            new Employee_Details__c(Name = 'employee1', Employee_Unique_Key__c = 'employee1', First_Name__c = 'Andrew', Last_Name__c = 'Allen', 
            	Email_Address__c = 'a@a.com', Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', 
            	Department__c = 'Analysis and Reporting', Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', 
            	Date_Hired__c = date.parse('12/27/2009'), Business_Unit__c = 'RDU')
        };
        insert employees;
        
        roleEmployees = new LMS_Role_Employee__c[] {
        	new LMS_Role_Employee__c(Role_Id__c = roles[1].Id, Employee_Id__c = employees[0].Id, Status__c = 'Draft', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today())
        };
        insert roleEmployees;
	}
	
	static LMS_CourseToAdhocController initSuccessRoleSearch() { 
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		c.roleText = roles[0].Adhoc_Role_Name__c;
		c.allAdhoc = false;
		return c;
	}
	
	static testMethod void testInit() {
		init();
		String internalDomain = domains[0].Domain_Id__c;
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		System.debug('-----------------------roleText------------------'+c.roleText);
		System.assert(c.courseText == 'Enter course title' && c.courseTextStyle == 'WaterMarkedTextBox' && 
		    c.roleText == 'Enter part of the role name' && c.roleTextStyle == 'WaterMarkedTextBox' && c.impactChange == '0' &&
		    c.employeeCount == 0 && c.impactStart == 0 && c.impactEnd == 0 &&
		    c.searchFilter == 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
        	'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
        	'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')');
	}
	
	static testMethod void testInitWithParams() {
		init();
		Pagereference pr = new PageReference('/apex/LMS_CourseToAdhoc?adhocName=name');
		Test.setCurrentPage(pr);
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		System.assert(c.roleText == 'name');
	}
	
	static testMethod void testInitWithParamsRoleId() {
		init();
		initRoles();
		Pagereference pr = new PageReference('/apex/LMS_CourseToAdhoc?roleId='+roles[0].Id);
		Test.setCurrentPage(pr);
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		System.assert(c.roleText == roles[0].Adhoc_Role_Name__c && c.allAdhoc == false);
	}
	
	static testMethod void testResetMethods() {
		init();
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		c.closeCourseSearch();
		System.assert(c.selCourses == null);
		
		c.roleReset();
		System.assert(c.courses == null && c.selCourses == null);
	}
	
	static testMethod void testSearchType() {
		init();
		String internalDomain = domains[0].Domain_Id__c;
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		c.courseType = 'Select Course Type';
		c.searchType();
		System.assert(c.searchFilter == 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
        	'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')');
		
		c.courseType = 'All';
		c.searchType();
		System.assert(c.searchFilter == 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
            'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')');
		
		c.courseType = 'Courses Only';
		c.searchType();
		System.assert(c.searchFilter == 'and Type__c NOT IN (\'PST\',\'SOP\') and Domain_Id__c = \'' + internalDomain + '\' ' + 
                'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')');
		
		c.courseType = 'SOPs Only';
		c.searchType();
		System.assert(c.searchFilter == 'and Type__c = \'SOP\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
                'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')');
	}
	
	static testMethod void testCourseTypes() {
		init();
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		List<SelectOption> options = c.getCourseTypes();
		System.assert(options.size() == 4 && options[0].getValue() == 'Select Course Type' && options[1].getValue() == 'All' &&
					  options[2].getValue() == 'Courses Only' && options[3].getValue() == 'SOPs Only');
	}
	
	static testMethod void testCourseReset() {
		init();
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		c.courseReset();
		System.assert(c.courses == null && c.selCourses == null && c.courseText == 'Enter course title');
	}
	
	static testMethod void testRoleSearchAllAdhoc() {
    	init();
    	initRoles();
    	initMappings();
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		c.allAdhoc = true;
		c.search();
		system.debug(c.roles.size());
		system.debug(c.courses);
		System.assert(c.roles.size() == 3 && c.courses == null);
    }
	
	static testMethod void testRoleSearchNoCourses() {
    	init();
    	initRoles();
    	initMappings();
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		c.roleText = roles[2].Adhoc_Role_Name__c;
		c.allAdhoc = false;
		c.search();
		System.debug(c.roles.size());
		System.debug(c.roles[0].Status__c);
		System.debug(c.courses);
		System.assert(c.roles.size() == 1 && c.roles[0].Status__c == 'Inactive' && c.courses.size() == 0);
    }
    
    static testMethod void testRoleSearchSuccess() {
    	init();
    	initRoles();
    	initMappings();
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		c.roleText = roles[1].Adhoc_Role_Name__c;
		c.allAdhoc = false;
		c.search();
		System.assert(c.courses.size() == 2);
    }
    
    static testMethod void testRoleSearchSuccessWithDraftDelete() {
    	init();
    	initRoles();
    	initMappings();
		LMS_CourseToAdhocController c = initSuccessRoleSearch();
		c.search();
		System.assert(c.courses[1].assignmentStatus == 'Draft Delete');
    }
    
    static testMethod void testRoleSearchFail() {
    	init();
    	initRoles();
    	initMappings();
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		c.roleText = 'New Role';
		c.search();
		System.assert(c.courses == null);
    }
    
    static testMethod void testCourseSearchSuccessCourseCode() {
    	init();
    	initRoles();
    	initMappings();
    	LMS_CourseToAdhocController c = initSuccessRoleSearch();
    	c.search();
    	c.courseType = 'SOPs Only';
    	c.courseText = 'Course 1';
    	c.blnUpdated = false;
    	c.courseSearch();
    	System.assert(c.selCourses.size() == 1);
    	
    	c.courseType = 'All';
    	c.courseText = 'Course 1';
    	c.blnUpdated = false;
    	c.courseSearch();
    	System.assert(c.selCourses.size() == 1);
    }
    
    static testMethod void testCourseSearchSuccessWithUpdatedDates() {
    	init();
    	initRoles();
    	initMappings();
    	LMS_CourseToAdhocController c = initSuccessRoleSearch();
    	c.search();
    	c.courseType = 'SOPs Only';
    	c.courseText = 'Course 1';
    	c.blnUpdated = false;
    	c.courseSearch();
    	System.assert(c.selCourses.size() == 1);
    	
    	c.courseType = 'All';
    	c.courseText = 'Course 1';
    	c.courseUpdateStart = '11/20/2012';
    	c.courseUpdateEnd = '12/18/2012';
    	c.blnUpdated = false;
    	c.courseSearch();
    	System.assert(c.selCourses.size() == 1);
    }
    
    static testMethod void testCourseSearchFailure() {
    	init();
    	initRoles();
    	initMappings();
    	LMS_CourseToAdhocController c = initSuccessRoleSearch();
    	c.search();
    	c.courseType = 'Courses Only';
    	c.blnUpdated = false;
    	c.courseSearch();
    	System.assert(c.selCourses == null);
    }
    
    static testMethod void testCourseSearchManyError() {
    	init();
    	initRoles();
    	initMappings();
    	initManyCourses();
    	LMS_CourseToAdhocController c = initSuccessRoleSearch();
    	c.search();
    	c.courseType = 'SOPs Only';
    	c.blnUpdated = false;
    	c.courseSearch();
    	System.assert(c.selCourses == null);
    }
    
    static testMethod void testCreateRole() {
    	init();
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		c.roleText = 'New Role';
    	PageReference pr = c.createRole();
    	System.assert(pr == null);
    }
    
    static testMethod void testCancelCreateRole() {
    	init();
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
    	PageReference pr = c.cancelCreateRole();
    	System.assert(pr == null && c.roles == null);
    }
    
    static testMethod void testSendRoleData() {
    	init();
		LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		c.roleText = 'New Role';
		c.allAdhoc = false;
		c.search();
		c.createRole();
    	PageReference pr = c.sendRoleData();
    	System.assert(pr == null);
    }
    
    static testMethod void testAddCourse() {
    	init();
    	initRoles();
    	initMappings();
    	LMS_CourseToAdhocController c = initSuccessRoleSearch();
		c.allAdhoc = false;
    	c.search();
    	c.courseType = 'SOPs Only';
		c.courseText = 'Course 2';
		c.blnUpdated = false;
    	c.courseSearch();
    	c.selCourses[0].selected = true;
    	PageReference pr = c.addCourse();
    	System.assert(pr == null);
    }
    
    static testMethod void testAddCourse2() {
    	init();
    	initRoles();
    	initMappings();
    	LMS_CourseToAdhocController c = initSuccessRoleSearch();
		c.allAdhoc = false;
    	c.search();
    	c.courseType = 'SOPs Only';
		c.courseText = 'Course 2';
		c.blnUpdated = false;
    	c.courseSearch();
    	c.selCourses[0].selected = false;
    	PageReference pr = c.addCourse();
    	System.assert(pr == null);
    }
    
    static testMethod void testCancel() {
    	init();
    	initRoles();
    	initMappings();
    	LMS_CourseToAdhocController c = initSuccessRoleSearch();
    	c.search();
    	PageReference pr = c.cancel();
    	System.assert(pr == null);
    }
    
    static testMethod void testAddCommit() {
    	init();
    	initRoles();
    	initMappings();
    	LMS_CourseToAdhocController c = initSuccessRoleSearch();
    	c.search();
    	PageReference pr = c.commitCourses();
    	System.assert(pr == null);
    }
	
	static testMethod void testApplyCommitDate(){
		init();
		initRoles();
		initMappings();
		LMS_CourseToAdhocController c = initSuccessRoleSearch();
		c.search();
		
		c.courses[0].selected = true;
		c.commitDate = '12/12/2102';
		PageReference pr = c.setCommitDate();
		System.assert(pr == null);
	}
    
    static testMethod void testRemoveCourse() {
    	init();
    	initRoles();
    	initMappings();
    	LMS_CourseToAdhocController c = initSuccessRoleSearch();
		c.allAdhoc = false;
    	c.search();
    	PageReference pr = c.removeCourses();
    	System.assert(pr == null);
    }
    
    static testMethod void testShowErrors() {
    	init();
    	LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		PageReference newPage = c.showErrors();
		System.assert(newPage.getUrl() == '/apex/LMS_CourseAdhocError');
    }
    
    static testMethod void testViewImpact() {
    	init();
    	initRoles();
    	initMappings();
    	initEmployees();
    	LMS_CourseToAdhocController c = new LMS_CourseToAdhocController();
		c.roleText = roles[1].Adhoc_Role_Name__c;
		c.allAdhoc = false;
    	c.search();
    	c.courseType = 'SOPs Only';
    	c.courseText = 'Course 2';
    	c.blnUpdated = false;
    	c.courseSearch();
    	c.selCourses[0].selected = true;
    	c.viewImpact();
    	System.assert(c.selCourses.size() == 1);
    }
}