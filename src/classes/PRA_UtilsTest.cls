/**
 * Test class to make sure that the flippin' PRA_Utils class is behaving itself.
 * Ai, it might not slay any dragons(like Ser Knightathon), but it will chop the heads of them nasty bugs that be troubling my dreams of late.
 * Enough folksy talk, now, onto coding and glory. 
 */
@isTest
private class PRA_UtilsTest {
    
    // This class will prepare the arsenal required for teh bug destruction gun
    /*ASA static PRA_Utils.UserPreference getMeSomePrefsMate() {
        PRA_Utils.UserPreference userPref = new PRA_Utils.UserPreference();
        userPref.userId = UserInfo.getUserId();
        userPref.projectId          = 'Project123456';
        userPref.regionId           = new List<String>{'America/Canada'};
        userPref.operationalAreaId  = new List<String>{'Project Management'};
        userPref.clientUnitId       = new List<String>{'Killing Gremlins'};
        userPref.clientUnitGroupId  = new List<String>{'Gremlinators'};
        userPref.unconfirmedOnly    = false;
        userPref.confirmedOnly      = false;
        return userPref;
    }
    
    // We be testing whether the user preference can be saved to the database
    static testMethod void saveAndRetrieveUserPreferenceTest() {
        // Init some friggin test data
        PRA_Utils.UserPreference up = getMeSomePrefsMate();
        
        // Save the darn gremlinators in the database
        PRA_Utils.saveUserPreference(up);
        List<User_Preference__c> userPrefList = [Select id, unconfirmedOnly__c, confirmedOnly__c, user_id__c, region_id__c, Operational_Area_Id__c, 
                                                    country_id__c, project_id__c, client_unit_id__c, client_unit_group_id__c 
                                                    from User_Preference__c];
        // Check if we have somethink in the dAtAbase
        System.assertNotEquals(null,userPrefList);
        // Check if it only returns 1 user pref
        System.assertEquals(1,userPrefList.size());
                
        // Change the confirmed flag
        up.confirmedOnly = true;
        PRA_Utils.saveUserPreference(up);
        
        // Get the data again
        userPrefList = [Select id, unconfirmedOnly__c, confirmedOnly__c, user_id__c, region_id__c, Operational_Area_Id__c, 
                                                    country_id__c, project_id__c, client_unit_id__c, client_unit_group_id__c 
                                                    from User_Preference__c];
        
        
        // Check all the stuff again
        System.assertNotEquals(null,userPrefList);
        System.assertEquals(1,userPrefList.size());
        // Check the status of the confirmed flag
        System.assertEquals(true,userPrefList[0].ConfirmedOnly__c);   
        
        // Test retrieve functionality
        up = PRA_Utils.retrieveUserPreference();   
        // check if user pref is not null
        System.assertNotEquals(null,up);
        // Check the status of the confirmed flag
        System.assertEquals(true,up.confirmedOnly);  
        
        // Test alternate save prefs method
        PRA_Utils.savePrefs('Project3456', up.regionId, up.OperationalAreaId, up.clientUnitId, up.clientUnitGroupId, up.unconfirmedOnly, up.confirmedOnly);
        
        // Get the data again
        userPrefList = [Select id, unconfirmedOnly__c, confirmedOnly__c, user_id__c, region_id__c, Operational_Area_Id__c, 
                                                    country_id__c, project_id__c, client_unit_id__c, client_unit_group_id__c 
                                                    from User_Preference__c];
        
        // Check all the stuff again
        System.assertNotEquals(null,userPrefList);
        System.assertEquals(1,userPrefList.size());
        // Test the new project id
        System.assertEquals('Project3456',userPrefList[0].Project_Id__c); 
         
    }
    
    
    // Now we must test the delightful delete feature
    static testMethod void destroyUserPreferenceTest() {
        // Init some friggin test data
        PRA_Utils.UserPreference up = getMeSomePrefsMate();
        
        // Save the darn gremlinators in the database
        PRA_Utils.saveUserPreference(up);
        List<User_Preference__c> userPrefList = [Select id, unconfirmedOnly__c, confirmedOnly__c, user_id__c, region_id__c, Operational_Area_Id__c, 
                                                    country_id__c, project_id__c, client_unit_id__c, client_unit_group_id__c 
                                                    from User_Preference__c];
         // Check that the u pref exists
        System.assertNotEquals(null,userPrefList);
        System.assertEquals(1,userPrefList.size());
        
        // Stamp the user pref out of existence
        PRA_Utils.deleteUserPreference();
        
        // Get the record again
        userPrefList = [Select id, unconfirmedOnly__c, confirmedOnly__c, user_id__c, region_id__c, Operational_Area_Id__c, 
                                                    country_id__c, project_id__c, client_unit_id__c, client_unit_group_id__c 
                                                    from User_Preference__c];
        
        // Check that the data is deleted
        System.assertNotEquals(null,userPrefList);
        
    }ASA*/
}