/** Implements the Selector Layer of the object StudyServicePricing__c
 * @author	Dimitrios Sgourdos
 * @version	20-Jan-2014
 */
public with sharing class StudyServicePricingDataAccessor {
	
	/** Object definition for fields used in application for StudyServicePricing
	 * @author	Dimitrios Sgourdos
	 * @version 20-Jan-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('StudyServicePricing__c');
	}
	
	
	/** Object definition for fields used in application for StudyServicePricing with the parameter referenceName 
	 *	as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 20-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'ApprovedFinancialDocument__c,';
		result += referenceName + 'CalculatedDiffToRateCard__c,';
		result += referenceName + 'CalculatedTotalPrice__c,';
		result += referenceName + 'DiscountOnTotal__c,';
		result += referenceName + 'FinancialDocumentIDs__c,';
		result += referenceName + 'NumberOfUnits__c,';
		result += referenceName + 'RateCardClass__c,';
		result += referenceName + 'RateCardCurrency__c,';
		result += referenceName + 'RateCardPrice__c,';
		result += referenceName + 'Service__c,';
		result += referenceName + 'StudyService__c,';
		result += referenceName + 'UnitPriceNegotiated__c,';
		result += referenceName + 'UnitPricePRAProposal__c';
		return result;
	}
	
	
	/** Retrieve the list of Study Service Pricings that meets the given criteria.
	 * @author	Dimitrios Sgourdos
	 * @version 20-Jan-2014
	 * @param	whereClause			The criteria that the Study Service Pricings must meet
	 * @param	orderByField		The field that the Study Service Pricings will be ordered by
	 * @return	The list of Study Service Pricings.
	 */
	public static List<StudyServicePricing__c> getStudyServicePricings(String whereClause, String orderByField) {
		// Query
		String query = String.format(
								'SELECT {0} ' +
								'FROM StudyServicePricing__c ' +
								'WHERE {1} ' +
								'ORDER BY {2}',
								new List<String> {
									getSObjectFieldString(),
									whereClause,
									orderByField
								}
							);
		
		return (List<StudyServicePricing__c>) Database.query(query);
	}
}