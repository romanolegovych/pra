public with sharing class BDT_NewEditGroupArmAssignment {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;}
	public ClinicalDesignStudyAssignment__c ClinicalDesignStudyAssignment   {get;set;}
	public List<ArmWrapper>		ArmWList {get;set;}
	List<String>					defaultGroupNames = new List<String>{'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
	Set<ID>							deleteIDs;
	public String					StudyLbl {get;set;}

	public BDT_NewEditGroupArmAssignment() {
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Study Population');
		String ID = ApexPages.currentPage().getParameters().get('ClinicalDesignStudyAssignmentID');

		If (ID == null || ID == '') {ID = ClinicalDesignStudyAssignment.ID;}
		readData(ID);
		deleteIDs = new Set<ID>();

	}

	public void readData (String ClinicalDesignStudyAssignmentID) {

		// read data from clinicalDesignStudyAssignment this will return one clinicalDesignStudyAssignment and
		// all it's child studyDesignPopulationAssignments (Populations assigned to the study design)
		ClinicalDesignStudyAssignment = [Select Study__r.Code__c
											  , Study__r.Id
											  , Study__c
											  , Id
											  , ClinicalDesign__r.Name
											  , ClinicalDesign__r.Id
											  , ClinicalDesign__c
											  ,  (Select Id, PopulationStudyAssignment__c From StudyDesignPopulationAssignments__r)
										   From ClinicalDesignStudyAssignment__c
										  where id = :ClinicalDesignStudyAssignmentID];

		String tmpString =  ClinicalDesignStudyAssignment.Study__r.Code__c;
		StudyLbl = tmpString.substringAfterLast('-');

		Set<ID> psaIDs = new Set<ID>();
		Set<ID> sdpaIDs = new Set<ID>();

		// put the StudyDesignPopulationAssignments and
		For (StudyDesignPopulationAssignment__c sdpa:ClinicalDesignStudyAssignment.StudyDesignPopulationAssignments__r) {
			psaIDs.add(sdpa.PopulationStudyAssignment__c);
			sdpaIDs.add(sdpa.id);

		}

		List<PopulationStudyAssignment__c> psaList = [Select Population__r.Name,
													Population__c,
													Id,
													(Select Id,
															ClinicalDesignStudyAssignment__c
														From StudyDesignPopulationAssignments__r
														where id in :sdpaIDs)
													From PopulationStudyAssignment__c
												   where id in :psaIDs];

		Map<String,List<GroupArmAssignment__c>> gaaMap = buildGAAmap(sdpaIDs);



		ArmWList = new List<ArmWrapper>();
		ArmWrapper ArmWItem;
		List<StudyDesignPopulationAssignmentWrapper> sdpaWList;
		StudyDesignPopulationAssignmentWrapper sdpaWItem;
		List<GroupArmAssignment__c> gaaList;
		GroupArmAssignment__c gaaItem;

		Integer groupNameIndex = 0;

		For (Arm__c arm:[select id, name, description__c from arm__c where clinicaldesign__c = :ClinicalDesignStudyAssignment.ClinicalDesign__r.Id]) {

			ArmWItem = new ArmWrapper();
			ArmWItem.arm = arm;
			sdpaWList = new List<StudyDesignPopulationAssignmentWrapper>();
			For (PopulationStudyAssignment__c psa : psaList) {

				sdpaWItem = new StudyDesignPopulationAssignmentWrapper();
				sdpaWItem.PopulationName = psa.Population__r.Name;
				For (StudyDesignPopulationAssignment__c sdpa : psa.StudyDesignPopulationAssignments__r) {
					sdpaWItem.StudyDesignPopulationAssignment = sdpa;

					if (gaaMap.containsKey(''+sdpa.id+':'+arm.id)) {
						sdpaWItem.GroupArmAssignment =  gaaMap.get(''+sdpa.id+':'+arm.id);
					} else {
						gaaItem = new GroupArmAssignment__c();
						gaaItem.name = defaultGroupNames[0];
						gaaItem.Arm__c = arm.id;
						gaaItem.StudyDesignPopulationAssignment__c = sdpa.id;
						gaaList = new List<GroupArmAssignment__c>();
						gaaList.add(gaaItem);
						sdpaWItem.GroupArmAssignment = gaaList;
					}
				}
				sdpaWList.add(sdpaWItem);
			}
			ArmWItem.StudyDesignPopulationAssignmentWL = sdpaWList;
			ArmWList.add(ArmWItem);
		}
	}

	public Map<String,List<GroupArmAssignment__c>> buildGAAmap (Set<ID> StudyDesignPopulationAssignmentIDs){

		Map<String,List<GroupArmAssignment__c>> gaaMap = new Map<String,List<GroupArmAssignment__c>>();
		List<GroupArmAssignment__c> gaaUseList;

		List<GroupArmAssignment__c> gaaList = [Select StudyDesignPopulationAssignment__c
													, NumberOfSubjects__c
													, NumberOfBackups__c
													, Name
													, Id
													, Arm__c
												 From GroupArmAssignment__c
												where StudyDesignPopulationAssignment__c in :StudyDesignPopulationAssignmentIDs
												order by name];

		for (GroupArmAssignment__c gaa:gaaList) {
			if (gaaMap.containsKey(''+gaa.StudyDesignPopulationAssignment__c+':'+gaa.arm__c)) {
				// add item to list
				gaaUseList = gaaMap.get(''+gaa.StudyDesignPopulationAssignment__c+':'+gaa.arm__c);
			} else {
				gaaUseList = new List<GroupArmAssignment__c>();
			}
			gaaUseList.add(gaa);
			gaaMap.put(''+gaa.StudyDesignPopulationAssignment__c+':'+gaa.arm__c,gaaUseList);
		}
		return gaaMap;

	}

	public void ExtendList () {
		String sdpaID = ApexPages.currentPage().getParameters().get('SDPAID');
		String armID = ApexPages.currentPage().getParameters().get('ARMID');

		Boolean complete = false;
		GroupArmAssignment__c gaaItem;
		for (ArmWrapper ArmW : ArmWList) {
			if (ArmW.arm.id == armID) {
				for (StudyDesignPopulationAssignmentWrapper sdpaW : ArmW.StudyDesignPopulationAssignmentWL) {
					if (sdpaW.StudyDesignPopulationAssignment.id == sdpaID) {
						gaaItem = new GroupArmAssignment__c();
						gaaItem.name = defaultGroupNames[sdpaW.GroupArmAssignment.size()];
						gaaItem.Arm__c = armID;
						gaaItem.StudyDesignPopulationAssignment__c = sdpaID;
						sdpaW.GroupArmAssignment.add(gaaItem);
						complete = true;
					}
					if (complete) {break;}
				}
			}
			if (complete) {break;}
		}
	}

	public void ReduceList () {
		String  sdpaID  = ApexPages.currentPage().getParameters().get('SDPAID');
		String  armID   = ApexPages.currentPage().getParameters().get('ARMID');
		Integer INDEX   = Integer.valueOf(ApexPages.currentPage().getParameters().get('INDEX'));

		Boolean complete = false;
		GroupArmAssignment__c gaaItem;
		for (ArmWrapper ArmW : ArmWList) {
			if (ArmW.arm.id == armID) {
				for (StudyDesignPopulationAssignmentWrapper sdpaW : ArmW.StudyDesignPopulationAssignmentWL) {
					if (sdpaW.StudyDesignPopulationAssignment.id == sdpaID) {
						deleteIDs.add(sdpaW.GroupArmAssignment[INDEX].id);

						sdpaW.GroupArmAssignment.remove(INDEX);
						complete = true;
					}
					if (complete) {break;}
				}
			}
			if (complete) {break;}
		}
	}

	public PageReference save() {
		try {
			list<GroupArmAssignment__c> upsertList = new List<GroupArmAssignment__c>();
			for (ArmWrapper ArmW : ArmWList) {
				for (StudyDesignPopulationAssignmentWrapper sdpaW : ArmW.StudyDesignPopulationAssignmentWL) {
					upsertList.addAll(sdpaW.GroupArmAssignment);
				}
			}
			if (upsertList.size()>0) {upsert upsertList;}
		} catch (Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Saving failed. Please check numbers for validity. (' +e+')');
			ApexPages.addMessage(msg);
			return null;
		}

		try {
			if (deleteIDs.size()>0) {
				delete [select id from GroupArmAssignment__c where id in :deleteIDs];
			}
		} catch (Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.INFO, 'Deleting failed. Please try again.');
			ApexPages.addMessage(msg);
			return null;
		}

		return new PageReference(System.Page.bdt_design.getUrl());
	}

	public PageReference cancel() {
		return new PageReference(System.Page.bdt_design.getUrl());
	}


	public class ArmWrapper {
		public Arm__c arm {get;set;}
		public List<StudyDesignPopulationAssignmentWrapper> StudyDesignPopulationAssignmentWL {get;set;}
	}
	public class StudyDesignPopulationAssignmentWrapper {
		public StudyDesignPopulationAssignment__c StudyDesignPopulationAssignment {get;set;}
		public String PopulationName {get;set;}
		public List<GroupArmAssignment__c> GroupArmAssignment {get;set;}
	}


}