public with sharing class BDT_PopulationDataAccessor {

	public static String getSObjectFieldString() {
		return getSObjectFieldString('Population__c');
	}

	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = '';
		result += referenceName + 'Age_Max__c,';
		result += referenceName + 'Age_Min__c,';
		result += referenceName + 'BMI_Max__c,';
		result += referenceName + 'BMI_Min__c,';
		result += referenceName + 'BloodPD_Max__c,';
		result += referenceName + 'BloodPD_Min__c,';
		result += referenceName + 'BloodPS_Max__c,';
		result += referenceName + 'BloodPS_Min__c,';
		result += referenceName + 'Smoking_Max__c,';
		result += referenceName + 'Client_Project__c,';
		result += referenceName + 'Female__c,';
		result += referenceName + 'Male__c,';
		result += referenceName + 'Population_Code__c,';
		result += referenceName + 'Name,';
		result += referenceName + 'Id,';
		result += referenceName + 'ScreeningEfficiency__c,';
		result += referenceName + 'Screening_Efficiency__c,';
		result += referenceName + 'Smoking__c,';
		result += referenceName + 'Smoking_No__c,';
		result += referenceName + 'Smoking_Yes__c,';
		result += referenceName + 'Total_Assigned_Active_Subjects__c,';
		result += referenceName + 'Total_Assigned_Backup_subjects__c,';
		result += referenceName + 'Total_Backup_in_Project__c,';
		result += referenceName + 'Total_Subjects_in_Project__c';
		return result;
	}

	public static List<Population__c> getList(String whereClause) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM Population__c ' +
								'WHERE  {1}',
								new List<String> {
									getSObjectFieldString(),
									whereClause
								}
							);
		return (List<Population__c>) Database.query(query);
	}

}