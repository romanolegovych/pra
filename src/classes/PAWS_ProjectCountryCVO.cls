public with sharing class PAWS_ProjectCountryCVO
{
	public ID RecordId { get; private set; }
	public String Name { get; private set; }

	public Decimal NumberOfSitesOnTrack { get; private set; }
	public Decimal NumberOfSitesDelayed { get; private set; }
	public Decimal NumberOfSitesCompleted { get; private set; }
	public Decimal NumberOfSitesAtRisk { get; private set; }
	public Decimal NumberOfSites { get; private set; }

	public List<PAWS_ProjectSiteCVO> Sites { get; private set; }

	public PAWS_ProjectCountryCVO()
	{
		Sites = new List<PAWS_ProjectSiteCVO>();
	}

	public PAWS_ProjectCountryCVO(PAWS_Project_Flow_Country__c inProjectCountry)
	{
		this();

		RecordId = inProjectCountry.ID;
		Name = inProjectCountry.Name;

		NumberOfSitesOnTrack = inProjectCountry.Number_Of_Sites_On_Track__c;
		NumberOfSitesDelayed = inProjectCountry.Number_Of_Sites_Delayed__c;
		NumberOfSitesCompleted = inProjectCountry.Number_Of_Sites_Completed__c;
		NumberOfSitesAtRisk = inProjectCountry.Number_Of_Sites_At_Risk__c;
		NumberOfSites = inProjectCountry.Number_Of_Sites__c;
	}

	public PAWS_ProjectSiteCVO addSite(PAWS_Project_Flow_Site__c inProjectSite)
	{
		PAWS_ProjectSiteCVO newProjectSite = new PAWS_ProjectSiteCVO(inProjectSite);
		Sites.add(newProjectSite);
		return newProjectSite;
	}
}