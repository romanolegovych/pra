@istest public class CCApiTest
{
    public static Map<String, SObject> setup()
    {
        PAWS_ApexTestsEnvironment.init();
        //System.debug(PAWS_ApexTestsEnvironment.Flow);
        System.debug(PAWS_ApexTestsEnvironment.Project);
        System.debug(PAWS_ApexTestsEnvironment.FlowInstance);
        System.debug(PAWS_ApexTestsEnvironment.FlowStep);
        PAWS_ApexTestsEnvironment.GanttStepProperty = new STSWR1__Gantt_Step_Property__c(
            STSWR1__Step__c = PAWS_ApexTestsEnvironment.FlowStep.Id,
            STSWR1__Revised_End_Date__c = Date.today(),
            STSWR1__Planned_Start_Date__c = Date.today(),
            STSWR1__Planned_End_Date__c = Date.today().addDays(10),
            Project__c = PAWS_ApexTestsEnvironment.Project.Id,
            STSWR1__Revised_Comment__c = 'revised comment');
        
        //System.debug(PAWS_ApexTestsEnvironment.GanttStepProperty);
        //System.debug(PAWS_ApexTestsEnvironment.FlowInstanceHistory);
        
        PAWS_ApexTestsEnvironment.GanttStepProperty.STSWR1__Planned_Start_Date__c = System.today().addDays(-1);
        PAWS_ApexTestsEnvironment.GanttStepProperty.STSWR1__Revised_Comment__c = 'Revised Comment';
        upsert PAWS_ApexTestsEnvironment.GanttStepProperty;
        
        Critical_Chain__c chain = new Critical_Chain__c();
        for (STSWR1__Flow_Instance__c instance : [select STSWR1__Flow__c, STSWR1__Flow__r.STSWR1__Source_ID__c from STSWR1__Flow_Instance__c where Id = :PAWS_ApexTestsEnvironment.FlowInstance.Id])
        {
            instance.STSWR1__Flow__r.STSWR1__Source_ID__c = PAWS_ApexTestsEnvironment.Project.Id;
            update instance.STSWR1__Flow__r;
            chain = new Critical_Chain__c(
                Name = 'test',
                Flow__c = instance.STSWR1__Flow__c,
                //Flow_Instance__c = PAWS_ApexTestsEnvironment.FlowInstance.Id,
                PAWS_Project_Flow__c = PAWS_ApexTestsEnvironment.Project.Id
            );
            
            insert chain;
        }
        
        
        Critical_Chain_Step_Junction__c chainJunction = new Critical_Chain_Step_Junction__c(
            Flow_Step__c = PAWS_ApexTestsEnvironment.FlowStep.Id,
            Critical_Chain__c = chain.Id
        );
        
        insert chainJunction;
        
        PAWS_ApexTestsEnvironment.FlowInstance.PAWS_Project_Flow__c = PAWS_ApexTestsEnvironment.Project.Id;
        update PAWS_ApexTestsEnvironment.FlowInstance;
        
        EW_Aggregated_Milestone__c aggregatedMilesone = new EW_Aggregated_Milestone__c(
            Name = 'TEST',
            PAWS_Project_Flow__c = PAWS_ApexTestsEnvironment.Project.Id
        );
        
        insert aggregatedMilesone;
        for (Critical_Chain__c each : [select Name, PAWS_Project_Flow__c from Critical_Chain__c])
        {
            insert new EW_Aggregated_Milestone_EW_Junction__c(Aggregated_Milestone__c = aggregatedMilesone.Id,
                Early_Warning__c = each.Id
            );
        }
        
        return new Map<String, SObject>{
            'Flow' => PAWS_ApexTestsEnvironment.Flow,
            'Flow Instance' => PAWS_ApexTestsEnvironment.FlowInstance,
            'Flow Step' => PAWS_ApexTestsEnvironment.FlowStep,
            'Cursor' => PAWS_ApexTestsEnvironment.FlowInstanceCursor,
            'Critical Chain' => chain,
            'Critical Chain Step Junction' => chainJunction,
            'PAWS Project' => PAWS_ApexTestsEnvironment.Project,
            'Country' => PAWS_ApexTestsEnvironment.ProjectCountry,
            'Site' => PAWS_ApexTestsEnvironment.ProjectSite,
            'Aggregated Milestone' => aggregatedMilesone
        };
        
    }
    
    @istest private static void testLoadDashboard()
    {
        Map<String, SObject> sobjectMap = setup();
        for (Critical_Chain__c each : [select Name, PAWS_Project_Flow__c from Critical_Chain__c])
        {
            System.assert(CCApi.loadDashboard('' + each.PAWS_Project_Flow__c, sobjectMap.get('Aggregated Milestone').Id, new Map<String, Object>()) != null);
        }
        //System.assert(CCApi.loadDashboard('' + sobjectMap.get('Flow Instance').get('PAWS_Project_Flow__c'), null, new Map<String, Object>()) != null);
    }
    
    @istest private static void testLoadChain()
    {
        Map<String, SObject> sobjectMap = setup();
        for (Critical_Chain__c each : [select Name from Critical_Chain__c])
        {
            System.assert(CCApi.loadChain(each.Id) != null);
        }
    }
    
    @istest private static void testLoadCCData()
    {
        Map<String, SObject> sobjectMap = setup();
        for (Critical_Chain__c each : [select Name from Critical_Chain__c])
        {
            System.assert(CCApi.loadCCData(new List<ID>{each.Id}) != null);
        }
    }
    
    @istest private static void testSaveChain()
    {
        Map<String, SObject> sobjectMap = setup();
        //System.assert(CCApi.saveChain((CCApi.AsyncResult) CCApi.loadChain(sobjectMap.get('Critical Chain').Id)) != null);
        
        
        for (Critical_Chain__c each : [select Name, (select Remaining_Time__c from Step_Junctions__r) from Critical_Chain__c])
        {
            CCApi.AsyncResult result = new CCApi.AsyncResult();
            result.ccData = System.Json.serialize(new Map<ID, Critical_Chain_Step_Junction__c>(each.Step_Junctions__r));
            System.assert(CCApi.saveChain(result) != null);
            //System.assert(CCApi.saveChain((CCApi.AsyncResult) CCApi.loadChain(each.Id)) != null);
        }
    }
    
    @istest private static void testGetters()
    {
        Map<String, SObject> sobjectMap = setup();
        CCApi api = new CCApi(new ApexPages.StandardController(sobjectMap.get('Critical Chain')));
        System.assert(api.biFilterOptions != null);
        System.debug(CCApi.biFilter);
        System.debug(api.chain);
        
        api = new CCApi(new ApexPages.StandardController(sobjectMap.get('PAWS Project')));
        api.milestoneID = sobjectMap.get('Aggregated Milestone').Id;
        api.onMilestoneChange();
        
        api.selectedCountry = api.getAvailableCountries().get(1).getValue();
        api.onCountryChange();
        api.onFilter();
        
        api.selectedSite = api.getAvailableSites().get(1).getValue();
        api.onFilter();
        
        api.changeMilestoneTargetDate();
    }
    
    @istest private static void testSelectFilers()
    {
        Map<String, SObject> sobjectMap = setup();
        Test.setCurrentPage(Page.CC_Dashboard);
        ApexPages.currentPage().getParameters().put('instanceID', sobjectMap.get('Site').Id);
        CCApi api = new CCApi(new ApexPages.StandardController(sobjectMap.get('PAWS Project')));
        
        api.selectFilters();
        System.assert(api.selectedSite != null);
    }
    
    @istest private static void testExecute()
    {
        Map<String, SObject> sobjectMap = setup();
        CCApi api = new CCApi();
        Test.setCurrentPage(Page.CC_Request);
        ApexPages.currentPage().getParameters().put('flowID', sobjectMap.get('Flow').Id);
        api.execute();
        
        CCApi.loadFlows(new List<String>{sobjectMap.get('Flow').Id});
    }
    
    @istest private static void testInstance()
    {
        Map<String, SObject> sobjectMap = setup();
        Test.setCurrentPage(Page.CC_Dashboard);
        ApexPages.currentPage().getParameters().put('instanceID', sobjectMap.get('PAWS Project').Id);
        CCApi api = new CCApi(new ApexPages.StandardController(sobjectMap.get('PAWS Project')));
        
        System.assert(api.getAvailableCountries() != null);
        api.selectedCountry = api.getAvailableCountries().get(1).getValue();
        System.assert(api.getAvailableSites() != null);
        System.assert(api.aggregatedMilestoneOptions.size() > 1);
        api.milestoneID = api.aggregatedMilestoneOptions.get(1).getValue();
        System.assertEquals(api.aggregatedMilestone.Id, api.aggregatedMilestoneOptions.get(1).getValue());
        System.assert(api.instanceID != null);
    }
    
    @istest private static void testEvents()
    {
        Map<String, SObject> sobjectMap = setup();
        Test.setCurrentPage(Page.CC_Dashboard);
        ApexPages.currentPage().getParameters().put('instanceID', sobjectMap.get('PAWS Project').Id);
        CCApi api = new CCApi(new ApexPages.StandardController(sobjectMap.get('PAWS Project')));
        
        api.onMilestoneChange();
        api.onCountryChange();
        api.onFilter();
    }
    
    @istest private static void testLoadFlowHierarchy()
    {
        Map<String, SObject> sobjectMap = setup();
        CCApi.loadFlowHierarchy(sobjectMap.get('Flow').Id);
    }
    
    
    @istest private static void testLoadFlowHierarchyUp()
    {
        Map<String, SObject> sobjectMap = setup();
        for (STSWR1__Item__c each : [select Name, STSWR1__Source_Flow__c from STSWR1__Item__c])
        {
        //  loadFlowsHierarchyUp(Map<ID, ID> ids, Set<ID> usedIds, Map<ID, Map<ID, ID>> hierarchyUp, Set<ID> allFlowsIds)
            CCApi.loadFlowsHierarchyUp(new Map<ID, ID>{each.STSWR1__Source_Flow__c => each.STSWR1__Source_Flow__c}, new Set<ID>(), new Map<ID, Map<ID, ID>>(), new Set<ID>{each.STSWR1__Source_Flow__c});
        }
    }
    
    @istest private static void testLoadFlowHierarchyDown()
    {
        Map<String, SObject> sobjectMap = setup();
        for (STSWR1__Item__c each : [select Name, STSWR1__Source_Flow__c from STSWR1__Item__c])
        {
        //  loadFlowsHierarchyUp(Map<ID, ID> ids, Set<ID> usedIds, Map<ID, Map<ID, ID>> hierarchyUp, Set<ID> allFlowsIds)
            CCApi.loadFlowsHierarchyDown(new Map<ID, ID>{each.STSWR1__Source_Flow__c => each.STSWR1__Source_Flow__c}, new Set<ID>(), new Map<ID, Map<ID, ID>>(), new Set<ID>{each.STSWR1__Source_Flow__c});
        }
    }
    
    @istest private static void testGetParentLevelFlow()
    {
        Map<String, SObject> sobjectMap = setup();
        CCApi.getParentLevelFlow(new STSWR1__Flow__c(STSWR1__Source_Id__c = sobjectMap.get('Site').Id));
        CCApi.getParentLevelFlow(new STSWR1__Flow__c(STSWR1__Source_Id__c = sobjectMap.get('Country').Id));
    }
    
    @istest private static void testLoadFlowByConnectionPoint()
    {
        Map<String, SObject> sobjectMap = setup();
        
        STSWR1__Item__c item = new STSWR1__Item__c(Name='Test Item', STSWR1__Source_Flow__c = PAWS_ApexTestsEnvironment.Flow.Id);
        insert item;
        
        STSWR1__Flow__c flow = PAWS_ApexTestsEnvironment.Flow.clone();
        insert flow;
        
        STSWR1__Item__c descendantItem = new STSWR1__Item__c(Name='Test Item D', STSWR1__Source_Flow__c = flow.Id);
        insert descendantItem;
        
        //STSWR1__Item__c item = [select Name, STSWR1__Source_Flow__c from STSWR1__Item__c where STSWR1__Source_Flow__c = :PAWS_ApexTestsEnvironment.Flow.Id];
        //item.STSWR1__Parent__c = [select Name, STSWR1__Source_Flow__c from STSWR1__Item__c where STSWR1__Source_Flow__c != :PAWS_ApexTestsEnvironment.ProjectFlow.Id].get(0).Id;
        //update item;
        
        for (STSWR1__Item__c each : [select Name, STSWR1__Source_Flow__c from STSWR1__Item__c where Id = :descendantItem.Id])
        {
            //CCApi.loadFlowHierarchyByConnectionPoint(each.STSWR1__Source_Flow__c, new Set<ID>{each.STSWR1__Source_Flow__c}, new Map<ID, Map<ID, ID>>(), new Map<ID, Map<ID, ID>>());
        }
    }
}