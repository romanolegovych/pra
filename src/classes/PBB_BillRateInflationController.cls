public class PBB_BillRateInflationController {
    Public String Id {get;set;}
    Public String scheduledYear{get;set;}
    public String country     {get;set;}
    public String jobPosition{get;set;}
    Public List<BillRateInflationWrapper> result {get;set;}
    Public String InflationRate{get;set;}
    public String modelId{ get; set; }
    Public Boolean inflation_applied =False;
    
    public PBB_BillRateInflationController () {
        country     = System.currentPagereference().getParameters().get('country');
        jobPosition = System.currentPagereference().getParameters().get('jobPosition');
        scheduledYear = System.currentPagereference().getParameters().get('scheduledYear');
        modelId = System.currentPagereference().getParameters().get('modelId');
        getresult();
    }
    
    Public List<BillRateInflationWrapper> getresult(){
        result =  new List<BillRateInflationWrapper>();
        if(!inflation_applied){
          List<Bill_Rate__c> costrates = [SELECT Id,
                                                 Year_Rate_Applies_To__c,
                                                 Schedule_Year__c,
                                                 Bill_Rate__c,
                                                 Currency__c,
                                                 Currency__r.Name,
                                                 CreatedBy.Name,
                                                 CreatedDate,
                                                 LastModifiedBy.Name,
                                                 LastModifiedDate,
                                                 IsActive__c,
                                                 country__r.Name,
                                                 Job_Position__r.Name
                                                 
                                          FROM   Bill_Rate__c 
                                          WHERE  Model_Id__c =: modelId AND Schedule_Year__c =: scheduledYear AND Job_Position__c =: jobPosition AND country__c =: country AND IsActive__c = true  ORDER BY Year_Rate_Applies_To__c];
          system.debug('------costrates ------'+costrates );
          for(Bill_Rate__c r : costrates){
              system.debug('------r------'+r);
              BillRateInflationWrapper cost = new BillRateInflationWrapper(r,false);
              result.add(cost);
          }
              
        }
        return result;
    }
    
    Public Pagereference apply(){
    inflation_applied = true;
        List<BillRateInflationWrapper> tempresult = result; 
        for(Integer i = 1 ; i < tempresult.size(); i ++){        
            Decimal unScaled = (Decimal) tempresult[i-1].costrate.Bill_Rate__c + (tempresult[i-1].costrate.Bill_Rate__c)*(Decimal.valueOf(InflationRate))/100;
            tempresult[i].costrate.Bill_Rate__c = unScaled.setScale(5);
        }
        result = tempresult;
        return null;
    }
    
    Public Pagereference save(){
        List<Bill_Rate__c> updatelist = new List<Bill_Rate__c>();
        for(BillRateInflationWrapper c : result){
            updatelist.add(c.costrate);
        }
        update updatelist;
        return null;
    }
        
    Public Pagereference deleterecords(){
        List<Bill_Rate__c> deletelist= new List<Bill_Rate__c>();
        for(BillRateInflationWrapper c : result){
                c.costrate.IsActive__c  = false;
                deletelist.add(c.costrate);
        }
        update deletelist;
        getresult();
        return null;
    }
    // public PageReference back() {
      //   PageReference pageRef = Page.PBB_BillRate;
     //    return pageRef;
   // }
    
    public class BillRateInflationWrapper {
        public Bill_Rate__c costrate {get;set;}
        public Boolean selected {get;set;}
        public BillRateInflationWrapper (Bill_Rate__c c, Boolean sel) {
            this.costrate = c;
            this.selected = sel;
        }
    }    
}