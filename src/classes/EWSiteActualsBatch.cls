global with sharing class EWSiteActualsBatch implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts, Schedulable
{
	public static final String BASE_FLOW_INSTANCE_QUERY =
		!Test.isRunningTest()
		? 'Select STSWR1__Object_Id__c From STSWR1__Flow_Instance__c Where ((STSWR1__Is_Active__c = true And STSWR1__Is_Completed__c = \'No\') or LastModifiedDate >= LAST_WEEK) And STSWR1__Object_Id__c like \'' + Schema.SObjectType.ecrf__c.getKeyPrefix() + '%\''
		: 'Select STSWR1__Object_Id__c From STSWR1__Flow_Instance__c Where STSWR1__Object_Id__c like \'' + Schema.SObjectType.ecrf__c.getKeyPrefix() + '%\'';
	
	public static void scheduleMe()
	{
		System.schedule('EW Site Actuals Batch', '0 0 0 * * ?', new EWSiteActualsBatch(BASE_FLOW_INSTANCE_QUERY));
	}
	
	global void execute(SchedulableContext context)
	{
		executeBatch();
	}
	
	public String query;
	
	public static void executeBatch()
	{
		Database.executeBatch(new EWSiteActualsBatch(BASE_FLOW_INSTANCE_QUERY), 1);
	}
	
	public EWSiteActualsBatch(String query)
	{
		this.query = query;
	}
	
	global virtual Database.QueryLocator start(Database.BatchableContext bc)
	{
		return Database.getQueryLocator(query);
	}
	
	global virtual void execute(Database.BatchableContext BC, List<SObject> scope)
	{
		
		try
		{
			for (SObject each : scope)
			{
				for (ecrf__c eachProject : [select Project_ID__c, Protocol_ID__r.Protocal_Unique_Key__c, Protocol_ID__r.SRM_LPI_Date__c, Protocol_ID__r.SRM_Final_Protocol_Date__c from ecrf__c where Id = :((STSWR1__Flow_Instance__c) each).STSWR1__Object_Id__c])
				{
					//new SiteActivationService().setActualSiteActivationInfo(eachProject.Project_ID__c);
					new SiteActivationService().setActualSiteActivationInfo(eachProject.Protocol_ID__r);
				}
			}
		}
		catch (Exception e)
		{
			System.debug(System.LoggingLevel.ERROR, 'EWSiteActualsBatch.execute: ' + e.getMessage() + '\nStacktrace: ' + e.getStacktraceString());
			//do nothing
		}
	}
	
	global virtual void finish(Database.BatchableContext BC)
	{
		/**
		* Added due to request from Edara, Ramya Shree
		*/
		String paramsJSONString = Json.serialize(new List<String>{'WFM_Protocol__c', 'Country_Weekly_Event__c'});
		PRA_Batch_Queue__c PBBPatientEnrollementActualstbatchQueue = new PRA_Batch_Queue__c(Batch_Class_Name__c = 'PBB_GetPatientEnrolledActualsBatch',Parameters_JSON__c = paramsJSONString,
			Status__c ='Waiting', Priority__c = 5, Scope__c = 1);
		
		insert PBBPatientEnrollementActualstbatchQueue;
	}
	
	public class SiteActivationService
	{
		/*
		* values should be cumulative 
		* do not provide actuals for future weeks
		* provide values for all weeks
		* actuals should also be cumulative
		* add a flag to show whether the Wrapper Object is for the last week
		*/
		public List<String> activationDateFields = new List<String>{
			'Study_Start_Up_Complete_Date__c',
			'Study_Start_Up_Expected_Date_BI_0__c',
			'Study_Start_Up_Expected_Date_BI_05__c',
			'Study_Start_Up_Expected_Date_BI_1__c'
		};
		
		private List<PAWS_Project_Flow_Site__c> pawsSites = new List<PAWS_Project_Flow_Site__c>();
		private Map<String, Country__c> countryMap = new Map<String, Country__c>();
		
		public void setActualSiteActivationInfo(WFM_Protocol__c wfmProtocol)
		{
			PBB_WR_APIs.setWeeklyEventsByProtocolUniqKey(wfmProtocol.Protocal_Unique_Key__c, getActualSiteActivationInfo(wfmProtocol));
		}
		
		public List<PBB_WR_APIs.WrapperObj> getActualSiteActivationInfo(WFM_Protocol__c wfmProtocol)
		{
			Date currentWeek = new EWDatetime(System.today()).toMonday(System.today());
			initializePAWSSites(wfmProtocol.Protocal_Unique_Key__c);
			initializeCountries(pawsSites);
			
			List<PBB_WR_APIs.WrapperObj> activationDetails = new List<PBB_WR_APIs.WrapperObj>();
			Set<Date> processedDates = new Set<Date>();
			List<Date> weekdates = getWeekdates(wfmProtocol);
			for (Integer i = 0, n = weekdates.size(); i < n; i++)
			{
				Date weekdate = weekdates.get(i);
				if (!processedDates.add(weekdate))
				{
					continue;
				}
				
				for (String countryName : countryMap.keyset())
				{
					Integer activatedNumber = getActivatedSites(weekdate, countryName, 'Study_Start_Up_Complete_Date__c', null);
					PBB_WR_APIs.WrapperObj activationDetail = new PBB_WR_APIs.WrapperObj(
						countryMap.get(countryName).Id,
						weekdate,
						(weekDate > currentWeek ? null : activatedNumber),
						(weekDate > currentWeek ? activatedNumber + getActivatedSites(weekdate, countryName, 'Study_Start_Up_Expected_Date_BI_1__c', 'Study_Start_Up_Status__c') : null),
						(weekDate > currentWeek ? activatedNumber + getActivatedSites(weekdate, countryName, 'Study_Start_Up_Expected_Date_BI_0__c', 'Study_Start_Up_Status__c') : null),
						(weekDate > currentWeek ? activatedNumber + getActivatedSites(weekdate, countryName, 'Study_Start_Up_Expected_Date_BI_05__c', 'Study_Start_Up_Status__c') : null),
						i == n - 1
					);
					
					activationDetails.add(activationDetail);
				}
			}
			
			
			return activationDetails;
		}
		
		private void initializePAWSSites(String protocolUniqueKey)
		{
			pawsSites = selectPAWSSites(protocolUniqueKey);
		}
		
		private List<PAWS_Project_Flow_Site__c> selectPAWSSites(String protocolUniqueKey)
		{
			return Database.query('select '
				+ String.join(new List<String>(Schema.SObjectType.PAWS_Project_Flow_Site__c.Fields.getMap().keyset()), ', ')
				+ ', PAWS_Country__r.Name '
				+ ' from ' + PAWS_Project_Flow_Site__c.class.getName() + ' where PAWS_Project__r.Protocol_ID__r.Protocal_Unique_Key__c = :protocolUniqueKey');
		}
		
		private void initializeCountries(List<PAWS_Project_Flow_Site__c> pawsSites)
		{
			for (Country__c actualCountry : selectActualCountries(pawsSites))
			{
				countryMap.put(actualCountry.Name, actualCountry);
			}
		}
		
		private List<Country__c> selectActualCountries(List<PAWS_Project_Flow_Site__c> pawsSites)
		{
			List<String> countryNames = new List<String>();
			for (PAWS_Project_Flow_Site__c pawsSite : pawsSites)
			{
				countryNames.add(pawsSite.PAWS_Country__r.Name);
			}
			
			return [select Name from Country__c where Name in :countryNames];
		}
		
		private Integer getActivatedSites(Date dateWeek, String countryName, String activationField, String statusField)
		{
			Integer i = 0;
			for (PAWS_Project_Flow_Site__c pawsSite : pawsSites)
			{
				Datetime activationDate = (Datetime) pawsSite.get(activationField);
				Boolean isCompleted = (statusField != null ? pawsSite.get(statusField) == 'Completed' : false);
				
				if (pawsSite.PAWS_Country__r.Name == countryName
						&& !isCompleted
						&& activationDate != null
						&& new EWDatetime(activationDate).toMonday(activationDate.date()) <= dateWeek)
				{
					i++;
				}
			}
			
			return i;
		}
		
		private List<Date> getWeekdates(WFM_Protocol__c wfmProtocol)
		{
			Date startDate = wfmProtocol.SRM_Final_Protocol_Date__c;
			Date endDate = wfmProtocol.SRM_LPI_Date__c;
			List<Date> weekdates = new List<Date>();
			for (PAWS_Project_Flow_Site__c pawsSite : pawsSites)
			{
				for (String activationDateField : activationDateFields)
				{
					Datetime activationDate = (Datetime) pawsSite.get(activationDateField);
					if (activationDate != null)
					{
						//weekdates.add(new EWDatetime(activationDate).toMonday(activationDate.date()));
						
						//Commented out startDate update due to request from Kondal since it leads to reprojections for weeks prior to week 1
						//if (startDate == null || startDate > new EWDatetime(activationDate).toMonday(activationDate.date()))
						//{
						//	startDate = new EWDatetime(activationDate).toMonday(activationDate.date());
						//}
						
						if (endDate == null || endDate < new EWDatetime(activationDate).toMonday(activationDate.date()))
						{
							endDate = new EWDatetime(activationDate).toMonday(activationDate.date());
						}
					}
				}
			}
			
			for (Integer i = 0; startDate != null && endDate != null && startDate.addDays(i * 7) <= endDate; i++)
			{
				weekdates.add(startDate.addDays(i * 7));
			}
			
			System.debug('Weekdates: ' + System.Json.serialize(weekdates));
			
			return weekdates;
		}
	}
}