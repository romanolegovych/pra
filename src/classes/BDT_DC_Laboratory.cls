/**
*	'Data Controller' for the Laboratory part of the project
*	@author   Dimitrios Sgourdos
*	@version  05-Sep-2013
*/
public with sharing class BDT_DC_Laboratory {
	
	public static Integer standardMethodLines = 44;	// The standard lines that are necessary per method in the excel file 
	public static Integer maxFieldsForMethod  =  8; // The fields that are included in the basic info for a method 
	
	
	/**
	*	Schedule a deletion of an attachment with id 'attachId'. 
	*	The deletion is scheduled after 10 minutes that the function is called.
	*	@author   Dimitrios Sgourdos
	*	@version  05-Sep-2013
	*	@param    attachdId				The Id of the attachemnt that will be deleted
	*/
	public static void scheduleTheAttachDeletion(String attachId){
		Datetime currentDateTime   = system.now();
		Datetime scheduledDateTime = currentDateTime.addMinutes(10); // let the download available for 10 minutes
		// Create the chron string for the schedule job
		String   chronString = '' + scheduledDateTime.second() + ' ' +
									scheduledDateTime.minute() + ' ' +
									scheduledDateTime.hour()   + ' ' +
									scheduledDateTime.day()    + ' ' +
									scheduledDateTime.month()  + ' ' +
									'? ' +
									scheduledDateTime.year();
		// Schedule the job		
		BDT_DeleteAttachments delAttach = new BDT_DeleteAttachments(attachId);
		system.schedule('Deletion of excel attachment:'+attachId, chronString, delAttach); 
	} 	
	
	
	/**
	*	Create the basic tags info that must be included in an excel file.
	*	@author   Dimitrios Sgourdos
	*	@version  05-Sep-2013
	*	@return   The basic tags info as a string.
	*/
	public static String createBasicInfoForExcel() {
		String txt = '<?xml version="1.0"?>\n';
		txt += '<?mso-application progid="Excel.Sheet"?>\n';
		txt += '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet"\n';
		txt += ' xmlns:o="urn:schemas-microsoft-com:office:office"\n';
 		txt += 'xmlns:x="urn:schemas-microsoft-com:office:excel"\n';
 		txt += 'xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet"\n';
 		txt += 'xmlns:html="http://www.w3.org/TR/REC-html40">\n';
 		txt += '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office">\n';
 		txt += ' <Company>PRA International</Company>\n';
 		txt += ' <Version>1.00</Version>\n';
		txt += ' </DocumentProperties>\n';
		txt += ' <OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office">\n';
		txt += ' <AllowPNG/>\n';
		txt += ' </OfficeDocumentSettings>\n';
		txt += ' <ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">\n';
		txt += '  <ProtectStructure>False</ProtectStructure>\n';
		txt += '  <ProtectWindows>False</ProtectWindows>\n';
		txt += ' </ExcelWorkbook>\n';
		return txt;
	}
	
	
	/**
	*	Create the styles info to be used from an excel file.
	*	@author   Dimitrios Sgourdos
	*	@version  05-Sep-2013
	*	@return   The styles info as a string.
	*/
	public static String createStylesForExcel(){ 
		String txt = '<Styles>\n';
		txt += '  <Style ss:ID="Default" ss:Name="Normal">\n';
		txt += '   <Alignment ss:Vertical="Bottom"/>\n';
		txt += '   <Borders/>\n';
		txt += '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>\n';
		txt += '   <Interior/>\n';
		txt += '   <NumberFormat/>\n';
		txt += '   <Protection/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="s70">\n';
		txt += '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="studyDataStl">\n';
		txt += '   <Borders/>\n';
		txt += '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#333399" ss:Bold="1"/>\n';
		txt += '   <Interior ss:Color="#C5D9F1" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="studyDataDoubleStl">\n';
		txt += '   <Borders>\n';
		txt += '    <Border ss:Position="Right" ss:LineStyle="Double" ss:Weight="3"/>\n';
		txt += '   </Borders>\n';
		txt += '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#000000"/>\n';
		txt += '   <Interior ss:Color="#C5D9F1" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="tableHeadersStl">\n';
		txt += '   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>\n';
		txt += '   <Borders>\n';
		txt += '    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
		txt += '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
		txt += '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
		txt += '   </Borders>\n';
		txt += '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#FFFFFF" ss:Bold="1"/>\n';
		txt += '   <Interior ss:Color="#1F497D" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="methodHeaders">\n';
		txt += '   <Alignment ss:Horizontal="Center" ss:Vertical="Center"/>\n';
		txt += '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#FFFFFF" ss:Bold="1"/>\n';
		txt += '   <Interior ss:Color="#1F497D" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="methodTableHeaders">\n';
		txt += '   <Alignment ss:Vertical="Center"/>\n';
		txt += '   <Borders>\n';
		txt += '    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
		txt += '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
		txt += '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
		txt += '   </Borders>\n';
		txt += '   <Interior ss:Color="#C5D9F1" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="tableHeadersDoubleStl">\n';
		txt += '   <Borders>\n';
		txt += '    <Border ss:Position="Bottom" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
		txt += '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
		txt += '    <Border ss:Position="Right" ss:LineStyle="Double" ss:Weight="3"/>\n';
		txt += '   </Borders>\n';
		txt += '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#FFFFFF"/>\n';
		txt += '   <Interior ss:Color="#C5D9F1" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="FirstLevelSrvCatStl">\n';
		txt += '   <Borders/>\n';
		txt += '   <Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="11" ss:Color="#FFFFFF" ss:Bold="1"/>\n';
		txt += '   <Interior ss:Color="#4F81BD" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="FirstLevelSrvCatStlDouble">\n';
		txt += '   <Borders>\n';
		txt += '    <Border ss:Position="Right" ss:LineStyle="Double" ss:Weight="3"/>\n';
		txt += '   </Borders>\n';
		txt += '   <Interior ss:Color="#4F81BD" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="SecondLevelSrvCatStl">\n';
		txt += '   <Borders/>\n';
		txt += '   <Interior ss:Color="#C5D9F1" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="SecondLevelSrvCatStlDouble">\n';
		txt += '   <Borders>\n';
		txt += '    <Border ss:Position="Right" ss:LineStyle="Double" ss:Weight="3"/>\n';
		txt += '   </Borders>\n';
		txt += '   <Interior ss:Color="#C5D9F1" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="ThirdLevelSrvCatStl">\n';
		txt += '   <Borders/>\n';
		txt += '   <Interior ss:Color="#E1EAF3" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="ThirdLevelSrvCatStlDouble">\n';
		txt += '   <Borders>\n';
		txt += '    <Border ss:Position="Right" ss:LineStyle="Double" ss:Weight="3"/>\n';
		txt += '   </Borders>\n';
		txt += '   <Interior ss:Color="#E1EAF3" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="servicesOddStl">\n';
		txt += '   <Borders>\n';
		txt += '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
		txt += '   </Borders>\n';
		txt += '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="servicesOddStlDouble">\n';
		txt += '   <Borders>\n';
		txt += '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
		txt += '    <Border ss:Position="Right" ss:LineStyle="Double" ss:Weight="3"/>\n';
		txt += '   </Borders>\n';
		txt += '   <Interior ss:Color="#FFFFFF" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="servicesEvenStl">\n';
		txt += '   <Borders>\n';
		txt += '    <Border ss:Position="Right" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
		txt += '   </Borders>\n';
		txt += '   <Interior ss:Color="#F3F2E9" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="servicesEvenStlDouble">\n';
		txt += '   <Borders>\n';
		txt += '    <Border ss:Position="Left" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
		txt += '    <Border ss:Position="Right" ss:LineStyle="Double" ss:Weight="3"/>\n';
		txt += '   </Borders>\n';
		txt += '   <Interior ss:Color="#F3F2E9" ss:Pattern="Solid"/>\n';
		txt += '  </Style>\n';
		txt += '  <Style ss:ID="lastRowStl">\n';
		txt += '   <Borders>\n';
		txt += '    <Border ss:Position="Top" ss:LineStyle="Continuous" ss:Weight="1"/>\n';
		txt += '   </Borders>\n';
		txt += '  </Style>\n';	
		txt += ' </Styles>\n';
		return txt;
	}
	
}