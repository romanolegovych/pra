@isTest
public class IPA_test
{
    static testmethod void ipa_BO_DAL_method()
    {   
        //***************** Public Declartions ******************        
        Date TodayDate = system.today();        
        String RecCategory = [Select Id From RecordType Where SobjectType = 'IPA_Category__c' and Name = 'Category'].Id;
        //String RecSubCategory = [Select Id From RecordType Where SobjectType = 'IPA_Category__c' and Name = ' Sub Category'].Id;
        String RecLandingPage = [Select Id From RecordType Where SobjectType = 'IPA_Page__c' and Name = 'Landing Page'].Id;
        String RecSubPage = [Select Id From RecordType Where SobjectType = 'IPA_Page__c' and Name = 'Sub Page'].Id;
        String RecMainMenu = [Select Id From RecordType Where SobjectType = 'IPA_Links__c' and Name = 'Main Menu Item'].Id;
        String RecSubMenu = [Select Id From RecordType Where SobjectType = 'IPA_Links__c' and Name = 'Sub Menu Item'].Id;
        String RecQuickLink = [Select Id From RecordType Where SobjectType = 'IPA_Links__c' and Name = 'Quick Link'].Id;
        String RecResourceCabinet = [Select Id From RecordType Where SobjectType = 'IPA_Links__c' and Name = 'Resource Cabinet Item'].Id;
        String RecCorpoUpdate = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Corporate Update'].Id;
        String RecHoliday = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Holiday'].Id;
        String RecIndWatch = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Industry Watch'].Id;
        String RecDeptContact = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Department Contact'].Id;
        String RecDeptContent = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Department Content'].Id;
        String RecDeptNews = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Department News'].Id;
        String RecEvent = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Event'].Id;        
        String RecHero = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Hero Graphic'].Id;
        
        // ********************* Records for Dynamic Page ****************
        IPA_Links__c LQuickLinkObj;
        IPA_Links__c LResourceCabinetObj;
        IPA_Articles__c ADeptContactObj;
        IPA_Articles__c ADeptContentObj;
        IPA_Articles__c AEventObj;
        IPA_Articles__c ADeptNewsObj;
        IPA_Articles__c AHero1Obj;
        IPA_Articles__c AHero2Obj;     
        IPA_Articles__c AHero3Obj; 
        IPA_Page_Widget_Link__c PageWidgetQuickLinkObj;
        IPA_Page_Widget_Link__c PageWidgetResourceCabinetObj;
        IPA_Page_Widget_Content__c PageWidgetContactObj;
        IPA_Page_Widget_Content__c PageWidgetContentObj;
        IPA_Page_Widget_Content__c PageWidgetEventObj;
        IPA_Page_Widget_Content__c PageWidgetDeptNewsObj;
        IPA_Page_Widget_Content__c PageWidgetHero1Obj;
        IPA_Page_Widget_Content__c PageWidgetHero2Obj;
        IPA_Page_Widget_Content__c PageWidgetHero3Obj;
        IPA_Link_Topic__c QuickLinkTopicObj;
        IPA_Link_Topic__c ResourceCabinetTopicObj;
        IPA_Content_Topic__c ContentTopicContactObj;
        IPA_Content_Topic__c ContentTopicContentObj;
        IPA_Content_Topic__c ContentTopicDeptNewsObj;
        IPA_Content_Topic__c ContentTopicEventObj;
        IPA_Content_Topic__c ContentTopicHero1Obj;
        IPA_Content_Topic__c ContentTopicHero2Obj;
        IPA_Content_Topic__c ContentTopicHero3Obj;
            
        IPA_Category__c CatObj = new IPA_category__c();        
        CatObj.RecordTypeId = RecCategory;
        CatObj.Name = 'Test Category';
        CatObj.Active__c = true;
        insert CatObj;
        
        IPA_Page__c PageObj = new IPA_Page__c();        
        PageObj.RecordTypeId = RecLandingPage;
        PageObj.Name = 'Test Page';
        PageObj.Category__c = CatObj.Id;
        PageObj.Published__c = true;
        insert PageObj;
        
        String[] WidgetNames = new List<String>{'Tab Group', 'Quick Links', 'Resource Cabinet Item', 'Department Contacts', 'Department Content', 'Department News', 'Events', 'Hero Graphic 1', 'Hero Graphic 2',  'Hero Graphic 3',  'Event Holidays', 'My  apps', 'Tme Zone'};
        String[] VFComponents = new List<String>{'IPA_TabGroupComponent', 'IPA_QuickLinksComponent', 'IPA_ResourceCabinetComponent', 'IPA_DepartmentContactsComponent', 'IPA_DepartmentContentsComponent', 'IPA_DepartmentNewsComponent', 'IPA_EventsComponent', 'IPA_HeroGraphic1Component', 'IPA_HeroGraphic2Component', 'IPA_HeroGraphic3Component', 'IPA_EventsHolidaysComponent', 'IPA_MyAppsComponent', 'IPA_TimeZoneComponent'};
        for(Integer i=0; i<WidgetNames.size(); i++)
        {
            IPA_Widget__c widgetObj = new IPA_Widget__c();
            widgetObj.Name = WidgetNames[i];
            widgetObj.VisualForce_Component_Name__c = VFComponents[i];
            WidgetObj.Active__c = true;
            widgetObj.Description__c = WidgetNames[i];
            insert widgetObj;
        }
        
        IPA_Topic__c TopicObj = new IPA_Topic__c();
        TopicObj.Parent_Category__c = CatObj.Id;
        TopicObj.Name = 'Test Topic';
        TopicObj.Active__c = true;
        insert TopicObj;
        
        String[] DisplayColumns = new List<String>{'Full', 'Tab Group', 'Tab Group', 'Split Left', 'Full', 'Split Right', 'Float Right', 'Header', 'full', 'SideBar', 'Split Left', 'Sidebar', 'SideBar'};
        for(Integer condition=1; condition<=3; condition++)
        {
            if(condition==1)
            {
                for(Integer i = 0; i < WidgetNames.size(); i++)
                {
                    List<IPA_Widget__c> WidgetIds = [Select Id from IPA_widget__c order by CreatedDate];
                    IPA_Page_Widget__c PageWidgetObj = new IPA_Page_Widget__c();
                    PageWidgetObj.Page__c = PageObj.Id;
                    PageWidgetObj.Widget__c = WidgetIds[i].Id;
                    PageWidgetObj.Display_Column__c = DisplayColumns[i];
                    if(i==1 || i==2){
                    PageWidgetObj.Display_Label__c = 'Test';    
                    PageWidgetObj.Parent_Tab_Group_Widget__c = [select Id From IPA_Page_Widget__c where Widget__c =: WidgetIds[0].Id].Id;
                    }
                    PageWidgetObj.Weight__c = i;
                    PageWidgetObj.Active__c = true;
                    insert PageWidgetObj;
                    
                    //If i==0, we dont need to create any IPA Link/Content record.
                    
                    If(i == 1) {
                        LQuickLinkObj = new IPA_Links__c();
                        LQuickLinkObj.RecordTypeId = RecQuickLink;
                        LQuickLinkObj.Category__c = CatObj.Id;
                        LQuickLinkObj.Name = 'Dynamic Test Page Link';
                        LQuickLinkObj.URL__c = 'http://www.google.com';
                        LQuickLinkObj.Weight__c = 0;
                        LQuickLinkObj.CSS_Class__c = '';
                        LQuickLinkObj.External_Link__c = true;
                        LQuickLinkObj.Restricted_Link__c = true;
                        insert LQuickLinkObj;
                  }
                    
                    If(i == 2) {
                        LResourceCabinetObj = new IPA_Links__c();
                        LResourceCabinetObj.RecordTypeId = RecResourceCabinet;
                        LResourceCabinetObj.Category__c = CatObj.Id;
                        LResourceCabinetObj.Name = 'Dynamic Test Page ResourceCabinet';
                        LResourceCabinetObj.URL__c = 'http://www.google.com';
                        LResourceCabinetObj.Weight__c = 0;
                        LResourceCabinetObj.CSS_Class__c = '';                        
                        insert LResourceCabinetObj;
                    }
                    
                    If(i == 3) {
                        ADeptContactObj = new IPA_Articles__c();                        
                        ADeptContactObj.RecordTypeId = RecDeptContact;
                        ADeptContactObj.Category__c = CatObj.Id;
                        ADeptContactObj.Contact_Name__c = 'Test Contact Name';
                        ADeptContactObj.Title__c = 'Test Contact';
                        ADeptContactObj.Weight__c = 5;
                        ADeptContactObj.Phone__c = '123456';
                        ADeptContactObj.Email__c = 'test@mail.com';
                        ADeptContactObj.Office_Location__c = 'Raleigh';
                        ADeptContactObj.Published__c = true;        
                        insert ADeptContactObj;
                    }
                    
                    If(i == 4) {
                        ADeptContentObj = new IPA_Articles__c();                        
                        ADeptContentObj.RecordTypeId = RecDeptContent;
                        ADeptContentObj.Category__c = CatObj.Id;
                        ADeptContentObj.Title__c = 'Test Content Title';
                        ADeptContentObj.Weight__c = 0;
                        ADeptContentObj.Display_Date__c = system.Today();
                        ADeptContentObj.Teaser__c = 'Test Content Teaser';
                        insert ADeptContentObj;
                    }
                    
                    If(i == 5) {
                        ADeptNewsObj = new IPA_Articles__c();                        
                        ADeptNewsObj.RecordTypeId = RecDeptNews;
                        ADeptNewsObj.Published__c = true;
                        ADeptNewsObj.Category__c = CatObj.Id;
                        ADeptNewsObj.Title__c = 'Test News Title';
                        ADeptNewsObj.Weight__c = 5;
                        ADeptNewsObj.Display_Date__c = system.Today();
                        ADeptNewsObj.Teaser__c = 'Test News Teaser';
                        insert ADeptNewsObj;
                    }
                    
                    If(i == 6) {
                        AEventObj = new IPA_Articles__c();                        
                        AEventObj.RecordTypeId = RecEvent;
                        AEventObj.Published__c = true;
                        AEventObj.Category__c = CatObj.Id;
                        AEventObj.Title__c = 'Test Event title';
                        AEventObj.Article_Type__c = 'Event';
                        AEventObj.Display_Area__c = 'Event & Holidays';
                        AEventObj.Weight__c = 0;
                        AEventObj.Display_Date__c = system.today();
                        insert AEventObj;                        
                    }
                    
                    If(i == 7) {
                        AHero1Obj = new IPA_Articles__c();                        
                        AHero1Obj.RecordTypeId = RecHero;
                        AHero1Obj.Published__c = true;
                        AHero1Obj.Category__c = CatObj.Id;
                        AHero1Obj.Title__c = 'Test Hero1 Title';
                        AHero1Obj.Weight__c = 5;
                        insert AHero1Obj;                        
                    }
                    
                    If(i == 8) {
                        AHero2Obj = new IPA_Articles__c();                         
                        AHero2Obj.RecordTypeId = RecHero;
                        AHero2Obj.Published__c = true;
                        AHero2Obj.Category__c = CatObj.Id;
                        AHero2Obj.Title__c = 'Test Hero2 Title';
                        AHero2Obj.Weight__c = 5;
                        insert AHero2Obj;                               
                    }
                    
                    If(i == 9) {
                        AHero3Obj = new IPA_Articles__c();                         
                        AHero3Obj.RecordTypeId = RecHero;
                        AHero3Obj.Published__c = true;
                        AHero3Obj.Category__c = CatObj.Id;
                        AHero3Obj.Title__c = 'Test Hero3 Title';
                        AHero3Obj.Weight__c = 5;
                        insert AHero3Obj;                               
                    }
                }
            }
            	
            if(condition==2)
            {				
                List<IPA_Page_Widget__c> PageWidgetIds = [Select Id from IPA_Page_widget__c order by CreatedDate];                
                QuickLinkTopicObj = new IPA_Link_Topic__c();
                QuickLinkTopicObj.Link__c = LQuickLinkObj.Id;
                QuickLinkTopicObj.Topic__c = TopicObj.Id;
                insert QuickLinkTopicObj;
                
                IPA_Page_Widget_Topic__c PWTQuickLinkObj = new IPA_Page_Widget_Topic__c();
                PWTQuickLinkObj.Topic__c = TopicObj.id;
                PWTQuickLinkObj.Page_Widget__c = PageWidgetIds[1].Id;
                insert PWTQuickLinkObj;

                ResourceCabinetTopicObj = new IPA_Link_Topic__c();
                ResourceCabinetTopicObj.Link__c = LResourceCabinetObj.Id;
                ResourceCabinetTopicObj.Topic__c = TopicObj.Id;
                insert ResourceCabinetTopicObj;
                ResourceCabinetTopicObj.Link__c = LQuickLinkObj.id;
                upsert ResourceCabinetTopicObj;
                ResourceCabinetTopicObj.Link__c = LResourceCabinetObj.id;
                update LResourceCabinetObj;
                IPA_Page_Widget_Topic__c PWTResourceCabinetObj = new IPA_Page_Widget_Topic__c();
                PWTResourceCabinetObj.Topic__c = TopicObj.id;
                PWTResourceCabinetObj.Page_Widget__c = PageWidgetIds[2].Id;
                insert PWTResourceCabinetObj;
                
                ContentTopicContactObj = new IPA_Content_Topic__c();
                ContentTopicContactObj.Content__c = ADeptContactObj.Id;
                ContentTopicContactObj.Topic__c = TopicObj.Id;
                insert ContentTopicContactObj;
                IPA_Page_Widget_Topic__c PWTContentObj = new IPA_Page_Widget_Topic__c();
                PWTContentObj.Topic__c = TopicObj.id;
                PWTContentObj.Page_Widget__c = PageWidgetIds[3].Id;
                insert PWTContentObj;
                
                ContentTopicContentObj = new IPA_Content_Topic__c();
                ContentTopicContentObj.Content__c = ADeptContentObj.Id;
                ContentTopicContentObj.Topic__c = TopicObj.Id;
                insert ContentTopicContentObj;
                PWTContentObj = new IPA_Page_Widget_Topic__c();
                PWTContentObj.Topic__c = TopicObj.id;
                PWTContentObj.Page_Widget__c = PageWidgetIds[4].Id;
                insert PWTContentObj;
                
                ContentTopicDeptNewsObj = new IPA_Content_Topic__c();
                ContentTopicDeptNewsObj.Content__c = ADeptNewsObj.Id;
                ContentTopicDeptNewsObj.Topic__c = TopicObj.Id;
                insert ContentTopicDeptNewsObj;
                PWTContentObj = new IPA_Page_Widget_Topic__c();
                PWTContentObj.Topic__c = TopicObj.id;
                PWTContentObj.Page_Widget__c = PageWidgetIds[5].Id;
                insert PWTContentObj;
                
                ContentTopicEventObj = new IPA_Content_Topic__c();
                ContentTopicEventObj.Content__c = AEventObj.Id;
                ContentTopicEventObj.Topic__c = TopicObj.Id;
                insert ContentTopicEventObj;
                PWTContentObj = new IPA_Page_Widget_Topic__c();
                PWTContentObj.Topic__c = TopicObj.id;
                PWTContentObj.Page_Widget__c = PageWidgetIds[6].Id;
                insert PWTContentObj;
                
                ContentTopicHero1Obj = new IPA_Content_Topic__c();
                ContentTopicHero1Obj.Content__c = AHero1Obj.Id;
                ContentTopicHero1Obj.Topic__c = TopicObj.Id;
                insert ContentTopicHero1Obj;
                PWTContentObj = new IPA_Page_Widget_Topic__c();
                PWTContentObj.Topic__c = TopicObj.id;
                PWTContentObj.Page_Widget__c = PageWidgetIds[7].Id;
                insert PWTContentObj;
                
                ContentTopicHero2Obj = new IPA_Content_Topic__c();
                ContentTopicHero2Obj.Content__c = AHero2Obj.Id;
                ContentTopicHero2Obj.Topic__c = TopicObj.Id;
                insert ContentTopicHero2Obj;                
                PWTContentObj = new IPA_Page_Widget_Topic__c();
                PWTContentObj.Topic__c = TopicObj.id;
                PWTContentObj.Page_Widget__c = PageWidgetIds[8].Id;
                insert PWTContentObj;
                
                ContentTopicHero3Obj = new IPA_Content_Topic__c();
                ContentTopicHero3Obj.Content__c = AHero3Obj.Id;
                ContentTopicHero3Obj.Topic__c = TopicObj.Id;
                insert ContentTopicHero3Obj;
                Test.StartTest();
                PWTContentObj = new IPA_Page_Widget_Topic__c();
                PWTContentObj.Topic__c = TopicObj.id;
                PWTContentObj.Page_Widget__c = PageWidgetIds[9].Id;
                insert PWTContentObj;
            }    
            
            if(condition == 3)
            {   
                    system.debug('Condition :' +condition);
                    List<IPA_Page_Widget__c> pagewidgetIDs = [Select Id from IPA_Page_Widget__c order by CreatedDate];                	
                    PageWidgetQuickLinkObj = new IPA_Page_Widget_link__c();          
                    PageWidgetQuickLinkObj.Page_Widget__c = pagewidgetIDs[1].Id;
                    PageWidgetQuickLinkObj.Link__c = LQuickLinkObj.Id; 
                    insert PageWidgetQuickLinkObj;
                	PageWidgetResourceCabinetObj = new IPA_Page_Widget_link__c();          
                    PageWidgetResourceCabinetObj.Page_Widget__c = pagewidgetIDs[2].Id;
                    PageWidgetResourceCabinetObj.Link__c = LResourceCabinetObj.Id;
                    insert PageWidgetResourceCabinetObj;
                    PageWidgetContactObj = new IPA_Page_Widget_Content__c();      
                    PageWidgetContactObj.Page_Widget__c = pagewidgetIDs[3].Id;
                    PageWidgetContactObj.Content__c = ADeptContactObj.Id;
                    insert PageWidgetContactObj;
                    PageWidgetContentObj = new IPA_Page_Widget_Content__c();
                    PageWidgetContentObj.Page_Widget__c = pagewidgetIDs[4].Id;
                    PageWidgetContentObj.Content__c = ADeptContentObj.Id;
                    insert PageWidgetContentObj;
                    PageWidgetDeptNewsObj = new IPA_Page_Widget_Content__c();
                    PageWidgetDeptNewsObj.Page_Widget__c = pagewidgetIDs[5].Id;
                    PageWidgetDeptNewsObj.Content__c = ADeptNewsObj.Id;
                    insert PageWidgetDeptNewsObj;
                    PageWidgetEventObj = new IPA_Page_Widget_Content__c();
                    PageWidgetEventObj.Page_Widget__c = pagewidgetIDs[6].Id;
                    PageWidgetEventObj.Content__c = AEventObj.Id;
                    insert PageWidgetEventObj;                                      
                    PageWidgetHero1Obj = new IPA_Page_Widget_Content__c();
                    PageWidgetHero1Obj.Page_Widget__c = pagewidgetIDs[7].Id;
                    PageWidgetHero1Obj.Content__c = AHero1Obj.Id;
                    insert PageWidgetHero1Obj;
                    PageWidgetHero2Obj = new IPA_Page_Widget_Content__c();
                    PageWidgetHero2Obj.Page_Widget__c = pagewidgetIDs[8].Id;
                    PageWidgetHero2Obj.Content__c = AHero2Obj.Id;
                    insert PageWidgetHero2Obj;                  
                	PageWidgetHero3Obj = new IPA_Page_Widget_Content__c();
                    PageWidgetHero3Obj.Page_Widget__c = pagewidgetIDs[9].Id;
                    PageWidgetHero3Obj.Content__c = AHero3Obj.Id;
                    insert PageWidgetHero3Obj;         
            
                    //apexpages.currentpage().getparameters().put('ArticleId',ACorpoUpdateObj.Id);
            }
            
            // ******************** Apex Class Testing ***************** // 
            
            // Apex Classes for Dynamic Page
            apexpages.currentpage().getparameters().put('Id',PageObj.Id);            
            IPA_UI_DynamicPage DynPageclsObj = new IPA_UI_DynamicPage();
            DynPageclsObj.getHeaderWidgets();     
            DynPageclsObj.getFullWidgets();
            DynPageclsObj.getFloatLeftWidgets();
            DynPageclsObj.getFloatRightWidgets();
            DynPageclsObj.getSidebarWidgets();
            DynPageclsObj.getSplitLeftWidgets();
            DynPageclsObj.getSplitRightWidgets();
            if(condition == 2 ) DynPageclsObj.getArticleDetailWidget();
            IPA_TabGroupComponentController TabGroupclsObj = new IPA_TabGroupComponentController();
            TabGroupclsObj.getlstWidgets();                                                                                     
            IPA_QuickLinksComponentController QuickLinkclsobj = new IPA_QuickLinksComponentController();
            QuickLinkclsobj.getLstQuickLinks();
            IPA_QuickLinksComponentController QuickLinkclsobj2 = new IPA_QuickLinksComponentController();
            QuickLinkclsobj2.pageWidgetObj = [Select Id, Page__c, Page__r.Category__c, Widget__c from IPA_Page_Widget__c where Page__r.Name = 'Test Page'  AND Widget__r.Name = 'Quick Links'];
            QuickLinkclsobj2.getLstQuickLinks();
            IPA_ResourceCabinetComponentController ResourceCabinetclsobj = new IPA_ResourceCabinetComponentController();
            ResourceCabinetclsobj.getLstMediaLibraries();
            IPA_ResourceCabinetComponentController ResourceCabinetclsobj2 = new IPA_ResourceCabinetComponentController();
            ResourceCabinetclsobj2.pageWidgetObj = [Select Id, Page__c, Page__r.Category__c, Widget__c from IPA_Page_Widget__c where Page__r.Name = 'Test Page'  AND Widget__r.Name = 'Resource Cabinet Item'];
            ResourceCabinetclsobj2.getLstMediaLibraries();
            IPA_DepContactsComponentController DeptContactclsObj = new IPA_DepContactsComponentController();
            DeptContactclsObj.getLstDepContacts();
            IPA_DepContactsComponentController DeptContactclsObj2 = new IPA_DepContactsComponentController();                   
            DeptContactclsObj2.pageWidgetObj = [Select Id, Page__c, Page__r.Category__c, Widget__c from IPA_Page_Widget__c where Page__r.Name = 'Test Page'  AND Widget__r.Name = 'Department Contacts'];
            DeptContactclsObj2.getLstDepContacts();
            IPA_DepContentsComponentController DeptContentclsObj = new IPA_DepContentsComponentController();
            DeptContentclsObj.getfetchContents();
            IPA_DepContentsComponentController DeptContentclsObj2 = new IPA_DepContentsComponentController();
            DeptContentclsObj2.pageWidgetObj = [Select Id, Page__c, Page__r.Category__c, Widget__c from IPA_Page_Widget__c where Page__r.Name = 'Test Page'  AND Widget__r.Name = 'Department Content'];
            DeptContentclsObj2.getfetchContents();  
            IPA_DepNewsComponentController DeptNewsclsObj = new IPA_DepNewsComponentController();
            DeptNewsclsObj.getfetchContents();
            IPA_DepNewsComponentController DeptNewsclsObj2 = new IPA_DepNewsComponentController();
            DeptNewsclsObj2.pageWidgetObj = [Select Id, Page__c, Page__r.Category__c, Widget__c from IPA_Page_Widget__c where Page__r.Name = 'Test Page'  AND Widget__r.Name = 'Department News'];
            DeptNewsclsObj2.getfetchContents(); 
            IPA_EventsHolidaysComponentController EventclsObj = new IPA_EventsHolidaysComponentController();
            EventclsObj.getFetchContent();
            IPA_EventsHolidaysComponentController EventclsObj2 = new IPA_EventsHolidaysComponentController();
            EventclsObj2.pageWidgetObj = [Select Id, Page__c, Page__r.Category__c, Widget__c from IPA_Page_Widget__c where Page__r.Name = 'Test Page'  AND Widget__r.Name = 'Events'];
            EventclsObj2.getFetchContent();
            IPA_HeroGraphicComponentController Hero1clsObj = new IPA_HeroGraphicComponentController();
            Hero1clsObj.pageWidgetObj = [Select Id, Page__c, Page__r.Category__c, Widget__c from IPA_Page_Widget__c where Page__r.Name = 'Test Page'  AND Widget__r.Name = 'Hero Graphic 1'];
            Hero1clsObj.getFetchContent();
            IPA_TimeZoneComponentController TimeZoneclsObj = new IPA_TimeZoneComponentController();    
            
            IPA_DynamicPageComponentFactory actortclsObj = new IPA_DynamicPageComponentFactory();
            actortclsObj.returnPageCategory(pageObj.Id);
        }
        Test.StopTest();        
    }
}