public with sharing class BDT_NewEditPassThroughServiceController {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Administration Pass-through
	public String PassThroughServiceId;
	public string ServiceCategoryId;
	public PassThroughService__c PassThroughService{get;set;}

	public BDT_NewEditPassThroughServiceController(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Pass-through');
		loadPassThroughService();
	}

    public void loadPassThroughService(){
    	PassThroughServiceId = System.currentPageReference().getParameters().get('PassThroughServiceId');
    	
    	ServiceCategoryId = System.currentPageReference().getParameters().get('ServiceCategoryId');
    	 
    	try{
    	   PassThroughService = [Select Name, InformationalText__c, DisplaySequence__c, ServiceCategory__c
    	   						 from PassThroughService__c
    	   						 where PassThroughService__c.Id = :PassThroughServiceId];
       	} 
 		catch(QueryException e){
 			PassThroughService = new PassThroughService__c();
 			PassThroughService.ServiceCategory__c = ServiceCategoryId;
 		}   
    }
    		
	public pageReference save(){
		try{
			upsert PassThroughService;
		}
		catch(DMLException e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Pass-Through Service could not be saved !');
        	ApexPages.addMessage(msg);
		}
		PageReference passThroughServicesOverview = new PageReference(System.Page.BDT_PassThroughServices.getUrl());
		passThroughServicesOverview.getParameters().put('ScId',PassThroughService.ServiceCategory__c);
		return passThroughServicesOverview;
	}
	
	public pageReference deleteSoft(){
		PassThroughService.BDTDeleted__c = true;
		
		try{
			upsert PassThroughService;
		}
		catch(DMLException e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Pass-Through Service could not be deleted !');
        	ApexPages.addMessage( msg );
		}
		PageReference passThroughServicesOverview = new PageReference(System.Page.BDT_PassThroughServices.getUrl());
		passThroughServicesOverview.getParameters().put('ScId',PassThroughService.ServiceCategory__c);
	
		return passThroughServicesOverview;
	}
	
	public pageReference cancel(){
		PageReference passThroughServicesOverview = new PageReference(System.Page.BDT_PassThroughServices.getUrl());
		passThroughServicesOverview.getParameters().put('ScId',PassThroughService.ServiceCategory__c);
	
		return passThroughServicesOverview;
	}
	
}