@RestResource(urlMapping='/CV_ConnectStatusBySite/*')
global with sharing class CV_ConnectStatusBySite{
	
    @HttpPost
    global static StatusBySiteResult getStatusBySite(String protocolUniqKey){
    	try{
    		return new StatusBySiteResult( PC_Utils.getStatusBySite(protocolUniqKey) );
        }catch(Exception e){
            return new StatusBySiteResult( e.getMessage() );
        }
    }
        
	global class StatusBySiteResult{
	    
	        global String errorMessage { get; private set; }
	        
	        global List<PRA_WrappersForMobile.WrapperSite> returnedValues { get; private set; }
	        
	        public StatusBySiteResult(List<PRA_WrappersForMobile.WrapperSite> returnedValues ){
	            this.returnedValues = returnedValues ;
	        }
	        
	        public StatusBySiteResult(String errorMessage){
	            this.errorMessage = errorMessage;
	        }
	}
}