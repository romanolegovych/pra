public with sharing class BDT_NewEditRateCardController {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} //Administration Rate Cards
	public List<RateCardClass__c> 	ratecardclassList	{get;set;}
	public List<RateCardClass__c> 	deletedRatecardclassList	{get;set;}
	public RateCard__c 				ratecard 			{get;set;}
	public Service__c				service				{get;set;}
	public Business_Unit__c 		businessUnit 		{get;set;}
	
	public Boolean					showDeletedRCC 		{get;set;}
	
	
	public BDT_NewEditRateCardController() {
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Rate Cards');
		showDeletedRCC = false;
		
		String serviceID      =  System.CurrentPageReference().getParameters().get('serviceID');
		String businessunitID =  System.CurrentPageReference().getParameters().get('businessunitID');
		
		deletedRatecardclassList = new list<RateCardClass__c>();
		ratecardclassList		 = new list<RateCardClass__c>();
		
		service = 		[select id, name, ServiceCategory__c from Service__c where id = :serviceID];
		businessUnit = 	[select id, currency__r.name, name from Business_Unit__c where id = :businessunitID];
		
		try {ratecard = [select id
						 from 	ratecard__c 
						 where 	Service__c 			= :serviceID
						 and    Business_Unit__c  	= :businessunitID
						 limit	1];
		} catch (Exception e) {
			/* no ratecard present, create one */
			ratecard 					= new ratecard__c();
			ratecard.service__c 		= serviceid;
			ratecard.business_unit__c 	= businessunitid;
		}	
	
		if (ratecard.id != null) {
			try {ratecardclassList = [Select 	r.UnitPrice__c, r.RateCard__c, r.Name, r.MaximumUnits__c, r.Id , r.NumericClass__c
									  From 		RateCardClass__c r
						 			  WHERE		((BDTDeleted__c  = :showDeletedRCC) or (BDTDeleted__c  = false))
						 			  AND       RateCard__c = :ratecard.id];
			} catch (Exception e) {
				/* no ratecardclass present, create one */
				ratecardclassList 		= new List<RateCardClass__c>();
				ratecardclassList.add(new RateCardClass__c(name='default',ratecard__c=ratecard.id));
			}		
		}
	}
	
	
	public void updateList(){}
	
	public void addRateCardClass(){
		ratecardclassList.add(new RateCardClass__c(name='Enter name...'));
	}
	
	public void deleteRateCardClass() {
		String RateCardClassId = System.currentPageReference().getParameters().get('RateCardClassId');
		Integer counter = 0;
		Integer listindex;
		for (RateCardClass__c rcc:ratecardclassList) {
			if (rcc.id == RateCardClassId) {
				listindex = counter;
			}
			counter++;
		}
		
		
		ratecardclassList[listindex].BDTDeleted__c = true;
		deletedRatecardclassList.add(ratecardclassList[listindex]);
		
		ratecardclassList.remove(listindex);
	}
	
	public PageReference save(){
		
		/* verify if ratecard exists */
		
		if (ratecard.id == null) {
			
			upsert ratecard;
		}		
		/* parse ratecardlist */
		for (RateCardClass__c rcc:ratecardclassList) {
			if (rcc.RateCard__c == null) {
				rcc.RateCard__c = ratecard.id;
			}
			
		}
		
		if (isSingleClass) {
				ratecardclassList[0].NumericClass__c = false;
				ratecardclassList[0].MaximumUnits__c = null;
				ratecardclassList[0].Name = 'default';
		}
		/* update updated records */
		if (!ratecardclassList.isEmpty()) {
			upsert ratecardclassList;
		}
		
		/* update deleted records */
		if (!deletedRatecardclassList.isEmpty()) {
			update deletedRatecardclassList;
		}
				
		PageReference ratecardPage = new PageReference(System.Page.BDT_RateCards.getUrl());
		if(service.ServiceCategory__c != null){
			ratecardPage.getParameters().put('ServiceCategoryId', service.ServiceCategory__c);
		}
		if(businessUnit.id!=null){
			ratecardPage.getParameters().put('BusinessUnit', businessUnit.id);
		}
		ratecardPage.setRedirect(true);
		return ratecardPage;
	}
	
	public PageReference cancel(){
	
		PageReference ratecardPage = new PageReference(System.Page.BDT_RateCards.getUrl());
		if(service.ServiceCategory__c != null){
			ratecardPage.getParameters().put('ServiceCategoryId', service.ServiceCategory__c);
		}
		if(businessUnit.id!=null){
			ratecardPage.getParameters().put('BusinessUnit', businessUnit.id);
		}
		ratecardPage.setRedirect(true);
		return ratecardPage;
	}
	
	public boolean isSingleClass {
		get{
			try {return (ratecardclassList.size()==1);} catch (Exception e) {return false;}
			}
		}
	
}