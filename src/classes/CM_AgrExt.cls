public class CM_AgrExt{

    private static final RecordType RT_Template = [SELECT Id from RecordType WHERE  SObjectType = 'Apttus__APTS_Agreement__c' and DeveloperName = 'Template'];
    public Apttus__APTS_Agreement__c agreement{ get; set; }
    
    public CM_AgrExt(ApexPages.StandardSetController controller) {
        agreement = new Apttus__APTS_Agreement__c();
        String pid = ApexPages.CurrentPage().getParameters().get('id');
        if(!String.isEmpty(RT_Template.Id)){
            agreement.RecordTypeId = RT_Template.Id;
            agreement.Name = 'TemplateP__'+DateTime.Now().format('yyyy-MM-dd_hh_mm_ss');
            agreement.Protocol__c = pid;
        }
    }
    
    /* 
    public CM_AgrExt(ApexPages.StandardController controller) {
        Apttus__APTS_Agreement__c agr = (Apttus__APTS_Agreement__c) controller.getRecord();        
        WFM_Protocol__c prot = [SELECT Id FROM WFM_Protocol__c WHERE Id =: Id.valueOf(ApexPages.currentPage().getParameters().get('pid'))];     
        if (RT_Template <> null){
            agr.RecordTypeId = RT_Template.Id;
        }        
        if (prot <> null){
            agr.Protocol__c = prot.Id;
        }               
    }
    */
    /**
     * @author  Prashant Wayal
     * @date    23-July-2014 
     * @description This method is used to save the agreement for Protocol Object
     *              with record type as 'Template'.
    */
    
    //Method to save agrement
    public pageReference saveAgreement(){
        if(agreement != null){
            try{
                upsert agreement;
                PageReference redirect = new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+agreement.Protocol__c);
                redirect.setRedirect(true);
                return redirect;
            }
            catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error while saving agreement: '+e));
                return null;
            }
        }
        return null;
    }
}