public with sharing class RM_OpportunityTriggerHandler {
	public static final string CLOSED = 'Closed Lost';
	public static final string CLOSEDSIGNED = 'Closed Signed Contract';
	/**
    * @author   Min Chen
    * @date     August 2014   
    * @description  After update trigger
    **/
	public void OpportunityOnAfterUpdate(Map<Id, Opportunity> mapOldOpp, Map<Id, Opportunity> mapNewOpp) {
    // If PRA_Project_ID is not null and type = 'New Business' and Name Not end with 'REBID' or 'REBID2',
     // Monitoring field: PRA_Project_ID; Therapeutic_Area, Indication_Group, Primary_indication, Phase, 
     //         Bid_Defense_Date, StageName, Study_Start_Date, study_End_Date
     // new list: new pra_project ID but old is null
     // exit list: both new or old with pra_project_id, 
     //     if no change in project ID and check other fields
     // 
     //     
        system.debug('------------OpportunityOnAfterUpdate------------' + mapOldOpp + '---' + mapNewOpp);
        Set<Id> oppNewID = new Set<Id>();
        Set<ID> oppUpdateIDs = new Set<ID>();
        Set<ID> oppUpdateBidProjectIDs = new Set<ID>();       
        list<Opportunity> lstUpdateOpp = new list<Opportunity>();
     
        for(Id key : mapNewOpp.keySet()) {
            if(mapOldOpp.containsKey(key)){
                Opportunity newOpp = mapNewOpp.get(key);    
                Opportunity oldOpp = mapOldOpp.get(key);
                system.debug('------------Opportunity-----------' + oldOpp + '---' + newOpp);
                if (newOpp.PRA_Project_ID__c != null && newOpp.Type == 'New Business' && newOpp.Name.indexOf('REBID')== -1){
                    if (oldOpp.PRA_Project_ID__c == null && newOpp.StageName != CLOSED && newOpp.StageName != CLOSEDSIGNED){
                        oppNewID.add(newOpp.id);                       
                    }
                    else{ 
                        if (newOpp.PRA_Project_ID__c != oldOpp.PRA_Project_ID__c
                            ||newOpp.Therapeutic_Area__c != oldOpp.Therapeutic_Area__c
                            ||newOpp.Indication_Group__c != oldOpp.Indication_Group__c
                            ||newOpp.Primary_Indication__c != oldOpp.Primary_Indication__c
                            ||newOpp.Phase__c != oldOpp.Phase__c
                            ||newOpp.Bid_Defense_Date__c != oldOpp.Bid_Defense_Date__c
                            ||newOpp.Study_Start_Date__c!= oldOpp.Study_Start_Date__c
                            ||newOpp.Study_End_Date__c!= oldOpp.Study_End_Date__c
                            ||newOpp.Native_Currency__c!=oldOpp.Native_Currency__c
                            ||newOpp.Native_Contract_Amount__c!= oldOpp.Native_Contract_Amount__c
                            ||newOpp.Business_Unit__c != oldOpp.Business_Unit__c
                            ||newOpp.Operational_Owner__c != oldOpp.Operational_Owner__c
                            ||(newOpp.StageName != oldOpp.StageName && newOpp.StageName == CLOSED)
                             ){
                                oppUpdateIDs.add(newOpp.id);
                             }
                        if ( newOpp.Business_Unit__c.indexOf('PR') > -1){
                            if (newOpp.PRA_Project_ID__c != oldOpp.PRA_Project_ID__c
                            ||newOpp.StageName != oldOpp.StageName
                            ||newOpp.Type != oldOpp.Type
                            ||newOpp.split_Sites_EAPA__c != oldOpp.split_Sites_EAPA__c
                            ||newOpp.split_Sites_NA__c != oldOpp.split_Sites_NA__c
                            ||newOpp.Expected_Patients__c != oldOpp.Expected_Patients__c
                            ||newOpp.Expected_Sites__c != oldOpp.Expected_Sites__c
                            ||newOpp.RFP_1st_Patient_In_Date__c != oldOpp.RFP_1st_Patient_In_Date__c
                            ||newOpp.RFP_Last_Patient_In_Date__c != oldOpp.RFP_Last_Patient_In_Date__c
                            ||newOpp.Proposal_Due_Date__c != oldOpp.Proposal_Due_Date__c
                            ||newOpp.RFP_Expected_Date__c != oldOpp.RFP_Expected_Date__c
                            ||newOpp.RFP_Received_Date__c != oldOpp.RFP_Received_Date__c
                            ||newOpp.OwnerId !=  oldOpp.OwnerId
                            ||newOpp.Operational_Owner__c != oldOpp.Operational_Owner__c
                            ||newOpp.Protocol_Number__c !=  oldOpp.Protocol_Number__c
                            ) {
                            	oppUpdateBidProjectIDs.add(newOpp.id);
                            }   
                        }
                    }
                    if (newOpp.StageName != oldOpp.StageName &&
                         (newOpp.StageName == CLOSED || oldOpp.StageName == CLOSED)) {//need update
                            oppUpdateIDs.add(newOpp.id);
                                                
                    }   
                    
                }
                system.debug('------------oppUpdateIDs-----------' + oppUpdateIDs);
                system.debug('------------oppNewID-----------' + oppNewID);            
            }           
        }
       

         
        if(!oppNewID.IsEmpty()){
            system.debug('------------before Update Async for insert------------'+oppNewID);
            OpportunityOnAfterInsertAsync(oppNewID);
        }
        if (!oppUpdateIDs.IsEmpty()){
            system.debug('------------before Update Async------------'+oppUpdateIDs);
            OpportunityOnAfterUpdateAsync(oppUpdateIDs);
        }
         if (!oppUpdateBidProjectIDs.IsEmpty()){
            system.debug('------------before Update Bid Project Async------------'+oppUpdateBidProjectIDs);
            OpportunityOnAfterUpdateBidProjectAsync(oppUpdateBidProjectIDs);
        }
	 }
	/**
    * @author   Min Chen
    * @date     August 2014   
    * @description  After insert trigger
    **/ 
	public void OpportunityOnAfterInsert(List<Opportunity> lstNewOpp) {
     // Only if PRA_Project_ID is not null and type = 'New Business' and Name Not end with 'REBID', 
     //create new WFM_Project and new WFM_Client
        system.debug('------------OpportunityOnAfterInsert------------');
        Set<Id> oppID = new Set<Id>();
     
        system.debug('------------lstNewOpp------------'+lstNewOpp);
        for(Opportunity o : lstNewOpp) {
            if (o.PRA_Project_ID__c != null && o.Type == 'New Business' && o.Name.indexOf('REBID')==-1
            && o.StageName != CLOSED && o.StageName != CLOSEDSIGNED){
                oppID.add(o.id);            
            }
        }
        
        if(!oppID.IsEmpty())            
            OpportunityOnAfterInsertAsync(oppID);
       
     }
     /**
    * @author   Min Chen
    * @date     August 2014   
    * @description  before delete trigger
    **/ 
     public void OpportunityOnBeforeDelete(List<Opportunity> lstOpp, Map<ID, Opportunity> oppMap) {
     // If PRA_Project_ID is not null and it is in WFM_Project
     // then update status_desc will be deleted
        
        Set<string> projectNames = new set<string>();
        for(Opportunity o : lstOpp) {
            if (o.PRA_Project_ID__c != null && o.Type == 'New Business' && o.Name.indexOf('REBID')== -1){
                projectNames.add(o.PRA_Project_ID__c);
            }
        }
        system.debug('------------projectNames------------'+projectNames);
        if (!projectNames.IsEmpty()){
            OpportunityOnBeforeDeleteAsync(projectNames);
        }       
        
     }
    
    /**
    * @author   Min Chen
    * @date     August 2014   
    * @description  future method after update
    **/  
	@future public static void OpportunityOnAfterUpdateAsync(set<Id> oppUpdateIDs){
	
		OpportunityOnAfterUpdate(oppUpdateIDs);
	 }
	
	 private static void OpportunityOnAfterUpdate(set<ID> updateOppID){
		system.debug('------------after Update Async------------'+updateOppID);
    	
    	list<Opportunity> lstOpp = RM_ProjectsDataAccessor.getOpportunityByOppRID(updateOppID);
    	map<string, sObject> mapUpdateOpp = RM_Tools.GetMapfromListObject('id', lstOpp);
    	Set<string> oppOwnerID = RM_Tools.GetSetfromListObject('Operational_Owner__c', lstOpp);
    	system.debug('------------oppOwnerID------------'+oppOwnerID);
    	map<string,SObject> mapUser = RM_Tools.GetMapfromListObject('id', RM_DataAccessor.GetUsersInfo(oppOwnerID)) ;
    	system.debug('------------mapUser------------'+mapUser);
    	
    	list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByOpportunityID(updateOppID);
    	
    	map<string, SObject> mapPorject = RM_Tools.GetMapfromListObject('Opportunity__c', lstProj);
        
        list<Opportunity> lstInsert = new list<Opportunity>();
        for (Opportunity o :lstOpp){
            if (!mapPorject.containsKey(o.id) && o.StageName != CLOSED && o.StageName != CLOSEDSIGNED){
            	
               lstInsert.add(o);
            }
        }
        try{
	    	if (!lstProj.IsEmpty()){
	    		
		    	for (WFM_Project__c p: lstProj){
		    		
		    		if ((p.Status_Desc__c == 'Bid' || p.Status_Desc__c == 'Lost') && mapUpdateOpp.containsKey(p.Opportunity__c)){
		    			Opportunity opp = (Opportunity)mapUpdateOpp.get(p.Opportunity__c);
		    			if (p.name != opp.PRA_Project_ID__c){
		    				p.name = opp.PRA_Project_ID__c;
		    				p.Project_Unique_Key__c = opp.PRA_Project_ID__c;
		    			}
		    			
		    			p.Therapeutic_Area__c = opp.Therapeutic_Area__c;        			
			        	p.Indication_Group__c = opp.Indication_Group__c;
			        	p.Primary_Indication__c = opp.Primary_Indication__c;
			        	if (opp.Phase__c.indexOf('Phase') > -1)			        	
			        		p.Phase__c = opp.Phase__c.replace('Phase ', '');
			        	p.Bid_Defense_Date__c = opp.Bid_Defense_Date__c; 
			        	p.Project_Start_Date__c = opp.Study_Start_Date__c;			        	
			        	p.Project_End_Date__c = opp.Study_End_Date__c;
			        		
			        	p.Currency_Code__c = opp.Native_Currency__c;
			        	p.Total_Budget__c = opp.Native_Contract_Amount__c;
			        	p.Owning_Business_Unit__c = opp.Business_Unit__c;
			        	if (opp.StageName == CLOSED)
			        		p.Status_Desc__c = 'Lost';
			        	else
			        		p.Status_Desc__c = 'Bid';
			        	system.debug('------------opp.Operational_Owner__c------------'+opp.Operational_Owner__c);
			        	if (mapUser.ContainsKey(opp.Operational_Owner__c)){
			        		
			        		user u = (user)mapUser.get(opp.Operational_Owner__c);
			        		string DPD = u.lastName + ', ' + u.FirstName;
			        		if (DPD != p.Director_Project_Delivery__c){
			        			p.Director_Project_Delivery__c = DPD;
			            		p.DPD_Employee_ID__c= u.EmployeeNumber;
			        		}
		        		}
		        		system.debug('------------update project------------'+p);
		    		}
		    		
		    	}
		    	update lstProj;
	    		
	    		
	    	}
	    	if (!lstInsert.IsEmpty()){
	    		OpportunityOnAfterInsertWFMProject(lstInsert);
	    	}
        }
    	catch (exception e){
                //RM_Tools.CreateErrorLog('OpportunityOnAfterUpdate', e);
        }
	 }
	  /**
    * @author   Min Chen
    * @date     August 2014   
    * @description  future method after update to update bid project
    **/  
    @future public static void OpportunityOnAfterUpdateBidProjectAsync(set<Id> oppUpdateIDs){
    
        OpportunityOnAfterUpdateBidProject(oppUpdateIDs);
     }
    
     private static void OpportunityOnAfterUpdateBidProject(set<ID> updateOppID){
        system.debug('------------after Update Async------------'+updateOppID);
        
        list<Opportunity> lstOpp = RM_ProjectsDataAccessor.getOpportunityByOppRID(updateOppID);
        map<string, sObject> mapUpdateOpp = RM_Tools.GetMapfromListObject('id', lstOpp); 
        
        list<BID_Project__c> lstProj = RM_ProjectsDataAccessor.getBidProjectByOpportunityID(updateOppID);
        //create map not sobject
         map<string, BID_Project__c> mapBidProject = new map<string, BID_Project__c>();
        for (BID_Project__c b: lstProj){
        	mapBidProject.put(b.PRA_Project_ID__r.Opportunity__c, b);
        }
        
        list<Opportunity> lstInsert = new list<Opportunity>();
        for (Opportunity o :lstOpp){
        	if (!mapBidProject.containsKey(o.id) && o.StageName != CLOSED && o.StageName != CLOSEDSIGNED)
        	   lstInsert.add(o);
        }
        try{
            if (!lstProj.IsEmpty()){
           
                for (BID_Project__c p: lstProj){
                    
                    if ((p.PRA_Project_ID__r.Status_Desc__c == 'Bid' || p.PRA_Project_ID__r.Status_Desc__c == 'Lost') 
                        && mapUpdateOpp.containsKey(p.PRA_Project_ID__r.Opportunity__c)){
                        Opportunity opp = (Opportunity)mapUpdateOpp.get(p.PRA_Project_ID__r.Opportunity__c);
                        if (p.name != opp.PRA_Project_ID__c){
                            p.name = opp.PRA_Project_ID__c;
                            p.Unique_Project__c = opp.PRA_Project_ID__c + opp.Type;
                        }
                        
                        p.BID_Stage__c = opp.StageName;                
                        p.Opportunity_Type__c = opp.Type;
                        p.split_Sites_EAPA__c = opp.split_Sites_EAPA__c;
                        p.Split_Sites_NA__c = opp.split_Sites_NA__c;
                        p.Expected_Patients__c = opp.Expected_Patients__c;
                        p.Expected_Sites__c = opp.Expected_Sites__c;                    
                        p.First_Patient_In__c = opp.RFP_1st_Patient_In_Date__c;    
                            
                        p.Last_Patient_In__c = opp.RFP_Last_Patient_In_Date__c;    
                        p.Proposal_Due_Date__c = opp.Proposal_Due_Date__c;
                        p.RFP_Expected__c = opp.RFP_Expected_Date__c;
                        p.RFP_Received__c = opp.RFP_Received_Date__c;
                        p.Opportunity_Owner__c = opp.OwnerId;
                        p.Project_Director__c = opp.Operational_Owner__c;
                        p.Protocol_Number__c = opp.Protocol_Number__c;
                        p.Account__c = opp.accountID;
                        system.debug('------------update bid project------------'+p);
                    }
                    
                }
                update lstProj;
               
                    
            
          
                
	        }
	        if (!lstInsert.IsEmpty()){
	        	OpportunityOnAfterInsertBid(updateOppID, lstInsert);
	        	
	        	
	        }
        }
            catch (exception e){
               // RM_Tools.CreateErrorLog('OpportunityOnAfterUpdateBidProject', e);
        }  
     }
	 /**
    * @author   Min Chen
    * @date     August 2014   
    * @description  future method after Insert
    **/ 
	@future public static void OpportunityOnAfterInsertAsync(Set<ID> newOppID){
		list<Opportunity> lstOpp = RM_ProjectsDataAccessor.getOpportunityByOppRID(newOppID);  
		OpportunityOnAfterInsertWFMProject(lstOpp);
		OpportunityOnAfterInsertBid(newOppID, lstOpp);
	}
	
	private static void OpportunityOnAfterInsertWFMProject(list<Opportunity> lstOpp){
		
	  	
	  	Set<Id> oppUpdateID = new Set<Id>();
	  	Set<string> setProject = new set<string>();
       // map<ID, Opportunity> mapOpp = new map<ID, Opportunity>();
        system.debug('------------lstOpp------------'+lstOpp);
        for(Opportunity o : lstOpp) {
        	if (o.PRA_Project_ID__c != null && o.Type == 'New Business' && o.Name.indexOf('REBID')==-1 && o.StageName != CLOSED && o.StageName != CLOSEDSIGNED){            	
            	setProject.add(o.pra_project_id__c);            	
        	}
        }
        system.debug('------------setProject------------'+setProject);
        if (!setProject.IsEmpty()){
        	set<string> setExit = new set<string>();
        	setExit = RM_Tools.GetSetfromListObject('name', RM_ProjectsDataAccessor.getProjectBySetProjectName(setProject));
        	system.debug('------------Set Exit------------'+setExit);
        	for (integer i = 0; i < lstOpp.size(); i++)	{
        		if (setExit.contains(lstOpp[i].PRA_Project_ID__c)){
        			lstOpp.Remove(i); 
        			oppUpdateID.add(lstOpp[i].id); 
        			system.debug('------------oppUpdateID---------'+oppUpdateID);   		
        		}
        		
        	}
        }
        if (!oppUpdateID.IsEmpty()){
        	OpportunityOnAfterUpdate(oppUpdateID);
        	
        }
        if(!lstOpp.IsEmpty()){ // new need insert
		
			map<ID, Opportunity> mapOpp = new map<ID, Opportunity>();
			
			system.debug('------------lstOpp------------'+lstOpp);	
			Set<string> OppOwnerID = RM_Tools.GetSetfromListObject('Operational_Owner__c', lstOpp);
			
			Set<string> accountID = RM_Tools.GetSetfromListObject('accountID', lstOpp);
			//create new wfm_client
	        try{
	        	map<string,SObject> mapUser = RM_Tools.GetMapfromListObject('id', RM_DataAccessor.GetUsersInfo(OppOwnerID)) ;
	            map<string, sObject> mapClient = creatAllNewClient( accountID); // map key is accountRID
	            
	            
	            list<WFM_Project__c> lstNewProj = new list<WFM_Project__c>();
			 	for (Opportunity opp: lstOpp){	        	
		        	system.debug('------------opp------------'+opp);	
	        		WFM_Project__c p = new WFM_Project__c(name=opp.PRA_Project_ID__c,  
	        		Opportunity__c=opp.id,
	        		Project_Unique_Key__c = opp.PRA_Project_ID__c,
	        		Therapeutic_Area__c = opp.Therapeutic_Area__c,
	        		Indication_Group__c = opp.Indication_Group__c,			        		
	        				        		
	        		Project_Start_Date__c = opp.Study_Start_Date__c,			        		
	        		Status_Desc__c = 'Bid',	
	        		Owning_Business_Unit__c = opp.Business_Unit__c);
	        		
	        		if (opp.Phase__c.indexOf('Phase') > -1)
	        			p.Phase__c = opp.Phase__c.replace('Phase ', '');	
	        		if (opp.Study_End_Date__c != null)
	        			p.Project_End_Date__c = opp.Study_End_Date__c;
	        		if (opp.Primary_Indication__c != null)
	        			p.Primary_Indication__c = opp.Primary_Indication__c;
	        		if (opp.Bid_Defense_Date__c != null)
	        			p.Bid_Defense_Date__c = opp.Bid_Defense_Date__c;
	        		if (opp.Native_Currency__c != null)
	        			p.Currency_Code__c = opp.Native_Currency__c;
	        		if (opp.Native_Contract_Amount__c != null)
	        			p.Total_Budget__c = opp.Native_Contract_Amount__c;
	        		if (opp.StageName == CLOSED)
	        			p.Status_Desc__c = 'Lost';
	        		if (mapClient != null && mapClient.containsKey(opp.accountID)){
	        			WFM_Client__c cli = (WFM_Client__c)mapClient.get(opp.accountID);
	        			p.WFM_Client__c = cli.id;
	        		}
	        		if (mapUser.ContainsKey(opp.Operational_Owner__c)){
		        		user u = (user)mapUser.get(opp.Operational_Owner__c);
		        		p.Director_Project_Delivery__c = u.lastName + ', ' + u.FirstName;
		            	p.DPD_Employee_ID__c= u.EmployeeNumber;
	        		}
	        		system.debug('------------p------------'+p);	
	        		lstNewProj.add(p);	
		        	
		        }
		        system.debug('------------lstNewProj------------'+lstNewProj);
		        if (!lstNewProj.IsEmpty()){
		        	insert lstNewProj;
		        }
	        }
	        catch(exception e){
	        	system.debug('------------error------------'+e.getMessage());
	        	//RM_Tools.CreateErrorLog('OpportunityOnAfterInsertWFMProject', e);
	        }
        }
	}
	private static void OpportunityOnAfterInsertBid(Set<ID> newOppID, list<Opportunity> lstOpp){
		Set<Id> oppUpdateID = new Set<Id>();
        Set<string> setProject = new set<string>();
       
        system.debug('------------lstOpp------------'+lstOpp);
        for(Opportunity o : lstOpp) {
            if (o.PRA_Project_ID__c != null && o.Type == 'New Business' && o.Name.indexOf('REBID')==-1 && o.StageName != CLOSED && o.StageName != CLOSEDSIGNED){                
                setProject.add(o.pra_project_id__c);                
            }
        }
        system.debug('------------setProject------------'+setProject);
        if (!setProject.IsEmpty()){
            set<string> setExit = new set<string>();
            setExit = RM_Tools.GetSetfromListObject('name', RM_ProjectsDataAccessor.getBidProjectByProjectName(setProject)); 
            system.debug('------------Set Exit------------'+setExit);
            for (integer i = 0; i < lstOpp.size(); i++) {
                if (setExit.contains(lstOpp[i].PRA_Project_ID__c)){
                    lstOpp.Remove(i); 
                    oppUpdateID.add(lstOpp[i].id); 
                    system.debug('------------oppUpdateID---------'+oppUpdateID);           
                }
                
            }
        }
        if (!oppUpdateID.IsEmpty()){
           
            OpportunityOnAfterUpdateBidProject(oppUpdateID);
        }
        if(!lstOpp.IsEmpty()){ // new need insert
        
            map<ID, Opportunity> mapOpp = new map<ID, Opportunity>();            
          
           
            //create new wfm_client
            try{
               
                
                map<string, SObject> mapWFMBasedOppID = RM_Tools.GetMapfromListObject('Opportunity__c', RM_ProjectsDataAccessor.getProjectByOpportunityID(newOppID));
                list<Bid_Project__c> lstBidProject = new list<Bid_Project__c>();
                for (Opportunity opp: lstOpp){
                    if (mapWFMBasedOppID.containsKey(opp.id))
                    {
                         WFM_Project__c wfm =  (WFM_Project__c)mapWFMBasedOppID.get(opp.id);
                         if (opp.Business_unit__c.indexOf('PR') > -1){
                             Bid_Project__c bProj = createNewBidProject(opp, wfm.id);
                             lstBidProject.add(bProj);
                         }
                    }
                }
                
                if (!lstBidProject.IsEmpty())
                  insert lstBidProject;
                   
               
            }
            catch(exception e){
                system.debug('------------error------------'+e.getMessage());
                //RM_Tools.CreateErrorLog('OpportunityOnAfterInsertBid', e);
            }
        }
	}	 
	 private static Bid_Project__c createNewBidProject(Opportunity opp, id wfmProjectID){
	 	 Bid_Project__c bidProject = new Bid_Project__c(name = opp.PRA_Project_ID__c,
	 	     Unique_Project__c = opp.PRA_Project_ID__c + ':' + opp.Type,
	 	     PRA_Project_ID__c = wfmProjectID,
	 	     BID_Stage__c = opp.StageName,
	 	     Opportunity_Type__c = opp.Type,
	 	     Opportunity_Owner__c = opp.OwnerId,
	 	     Account__c = opp.accountID);
	 	     
	 	     if (opp.split_Sites_EAPA__c != null)
	 	         bidProject.split_Sites_EAPA__c = opp.split_Sites_EAPA__c;
	 	     if (opp.split_Sites_NA__c != null)
	 	         bidProject.Split_Sites_NA__c = opp.split_Sites_NA__c;
	 	     if (opp.Expected_Patients__c != null)
	 	         bidProject.Expected_Patients__c = opp.Expected_Patients__c;
	 	     if (opp.Expected_Sites__c != null)
	 	         bidProject.Expected_Sites__c = opp.Expected_Sites__c;
	 	     if (opp.RFP_1st_Patient_In_Date__c != null)
	 	         bidProject.First_Patient_In__c = opp.RFP_1st_Patient_In_Date__c;    
	 	     if (opp.RFP_Last_Patient_In_Date__c != null)
	 	         bidProject.Last_Patient_In__c = opp.RFP_Last_Patient_In_Date__c;    
	 	     if (opp.Proposal_Due_Date__c != null)
	 	         bidProject.Proposal_Due_Date__c = opp.Proposal_Due_Date__c;
	 	     if (opp.RFP_Expected_Date__c != null)
	 	         bidProject.RFP_Expected__c = opp.RFP_Expected_Date__c;
	 	     if (opp.RFP_Received_Date__c != null)
	 	         bidProject.RFP_Received__c = opp.RFP_Received_Date__c;
	 	     if (opp.Operational_Owner__c != null)
	 	         bidProject.Project_Director__c = opp.Operational_Owner__c;
	 	     if (opp.Protocol_Number__c != null) 
	 	         bidProject.Protocol_Number__c = opp.Protocol_Number__c;
	
	 	 
	 	 return bidProject;
	 }
	 private static map<string, sObject> creatAllNewClient(set<string> accountIDs){
	 	try{
		 	system.debug('------------creatAllNewClient------------'+accountIDs);
		 	list<Account> lstAccount = RM_ProjectsDataAccessor.getAccountByaccountID( accountIDs);
		 	
		 	
	   		map<string, sObject> mapAccount = RM_Tools.GetMapfromListObject('name', lstAccount) ;
	   		set<string> nameSet = RM_Tools.GetSetfromListObject('name', lstAccount);
	   		system.debug('------------nameSet------------'+nameSet);
	   		list<WFM_Client__c> lstClientNeedUpdate = RM_ProjectsDataAccessor.getClientByclientUniqueKeyNoAccount(nameSet);
	   		if (!lstClientNeedUpdate.IsEmpty()){
	   		
		   		for (WFM_Client__c c: lstClientNeedUpdate){
		   			if (mapAccount.containsKey(c.name)){
		   				Account ac = (Account)mapAccount.get(c.name);
		   			    c.account__c = ac.id; 
		   			}
		   		}
		   		update lstClientNeedUpdate;
	   		}
	   		list<WFM_Client__c> lstClient = RM_ProjectsDataAccessor.getClientByclientUniqueKey(nameSet);
	   		system.debug('------------lstClient------------'+lstClient);
	   		map<string, sObject> mapClient = new map<string, sObject>();
	   		if (!lstClient.IsEmpty()){ //
	   			 mapClient = RM_Tools.GetMapfromListObject('client_unique_key__c', lstClient);
	   		}
	   		
		 	
		 	list<WFM_Client__c> lstNewClient = new list<WFM_Client__c>();
		 	list<string> lstName = new list<string>();
		 	lstName.addAll(nameSet);
		 	for(string n: lstName){		 		
		 		if (!mapClient.containsKey(n)){		 			
		 			Account a = (Account)mapAccount.get(n);
		 			WFM_Client__c client = new WFM_Client__c(name = a.name.toUpperCase(), client_name__c=a.name, client_unique_key__c=a.name, account__c = a.id, Source__c='CRM');
		 			lstNewClient.add(client);
		 		}		 		
		 	}
		 	system.debug('------------lstNewClient------------'+lstNewClient);
		 	if (!lstNewClient.IsEmpty())
		 		upsert lstNewClient;
		 		
		 	return RM_Tools.GetMapfromListObject('account__c', RM_ProjectsDataAccessor.getClientByclientUniqueKey(nameSet));
	 	}
	 	catch(exception e){
	        	system.debug('------------error------------'+e.getMessage());
	        	//RM_Tools.CreateErrorLog('creatAllNewClient', e);
	        	
	        	return null;
	    }
	 }
	  /**
    * @author   Min Chen
    * @date     August 2014   
    * @description  future method before delete
    **/ 
	 @future public static void OpportunityOnBeforeDeleteAsync(set<string> projectNames){
	 	system.debug('------------delete after Async------------'+projectNames);
	 	list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectBySetProjectNameStatus(projectNames, 'Bid');
	 	system.debug('------------lstProj after Async------------'+lstProj);
	 	list<WFM_Project__c> lstUpdate = new list<WFM_Project__c>();
 		for (WFM_Project__c p: lstProj){
 			if (p.Status_Desc__c == 'Bid' || p.Status_Desc__c == 'Lost'){
 				p.Status_Desc__c = 'Deleted';
 				lstUpdate.add(p);
 			}
 		}
 		system.debug('------------lstUpdate------------'+lstUpdate);
 		try{
 			if (!lstUpdate.IsEmpty())
 				update lstUpdate;
 		}
 		 catch(exception e){
        	//RM_Tools.CreateErrorLog('OpportunityOnBeforeDeleteAsync', e);
        }
	 }
}