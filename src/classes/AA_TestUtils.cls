/**
*   'AA_TestUtils' is the TestUtil class used to create the data for AA.
*   @author   Abhishek Prasad
*   @version  19-Nov-2014
*   @since    19-Nov-2014
*/
@isTest
public with sharing class AA_TestUtils {
    /** CountryList variable is to hold the list of Country records*/  
    public List<Country__c> CountryList;
    /** Business Unit variable is to hold the list of BU records*/  
    public List<Business_Unit__c> BusinessUnitList;
    /** Function Code variable is to hold the list of F records*/  
    public List<Function_Code__c> FunctionCodeList;
    /** BUF Code variable is to hold the list of BUF records*/  
    public List<BUF_Code__c> BufCodeList;
    /** Currency variable is to hold the list of Currency  records*/  
    public List<Currency__c> currencyList;
    /** Cost Rate variable is to hold the list of Cost Rate records*/  
    public List<Cost_Rate__c> costrateList;
    /** Revenue Allocation Rate variable is to hold the list of Revenue Allocation Rate records*/  
    public List<Revenue_Allocation_Rate__c> revenueList;
    /** billModelList holds all Bill Rate Card Model*/
    public Bill_Rate_Card_Model__c billModel;
    /** billRateList is the hold of Bill rate records */
    public Bill_Rate__c billRate;
    /** jobPosition holds Job Position records */
    public Job_Class_Desc__c jobPosition;
    public Country__c countryy;
    public Currency__c currencyy;
    /** billModelList holds all Bill Rate Card Model*/
    public List<Bill_Rate_Card_Model__c> billRateModelList;
    /** billRateList is the hold of Bill rate records */
    public List<Bill_Rate__c> billRatesList ;
    /**
    *   This Method is to create the test data.          
    */
    public void AA_TestUtils(){
        
    }
    public Bill_Rate_Card_Model__c createBillModel(){
        billModel = new Bill_Rate_Card_Model__c (Name = 'BillRateModel1',Year__c='2004');
        System.debug('----billRateModel-----'+billModel);
        insert billModel;
        return billModel;
    }
    
    public Bill_Rate__c createBillRate(){
        Bill_Rate_Card_Model__c billModel1 = createBillModel();
        billRate = new Bill_Rate__c (Model_Id__c = billModel1.Id, currency__c=createCurrency().Id, country__c=createCountry().Id, Job_Position__c=createJobPosition().Id, Schedule_Year__c=billModel1.Year__c, Bill_Rate__c=100.00, isActive__c = true);
        System.debug('----BillRate----'+billRate);
        insert billRate;
        return billRate;
    }
    
    public Job_Class_Desc__c createJobPosition(){
        jobPosition = new Job_Class_Desc__c(Name='Job1', Job_Position_Long__c='job Position', Job_Class_Code__c='TEST123', Job_Code__c='TEST123');
        insert jobPosition;
        return jobPosition;
    
    }
    
    public country__c createCountry(){
        countryy = new Country__c(Name='Country1');
        insert countryy;
        return countryy;
    }
    
    public currency__c createCurrency(){
        currencyy = new currency__c(Name='currency1');
        insert currencyy;
        return currencyy;
    }
    
    
    public Business_Unit__c getBUCode(){
        //Country data
        CountryList = new list<Country__c>();
        Country__c country = new Country__c(name='TestCountry133',PRA_Country_ID__c='10019', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0);
        CountryList.add(country);
        Country__c country1 = new Country__c(name='TestCountry233',PRA_Country_ID__c='10119', Country_Code__c='T2',Region_Name__c='North America2', 
        daily_Business_Hrs__c=7.5);
        CountryList.add(country1);
        insert CountryList;     
        
        //BU Data
        BusinessUnitList = new list<Business_Unit__c>();
        Business_Unit__c businessUnit1 = new Business_Unit__c(name='BUY',Default_Country__c=country.Id);
        BusinessUnitList.add(businessUnit1);
        Business_Unit__c businessUnit2 = new Business_Unit__c(name='BUZ',Default_Country__c=country1.Id);
        BusinessUnitList.add(businessUnit2);
        insert BusinessUnitList;

        
        return businessUnit1;
    }
    
    public Function_Code__c getFCode(){
        //Country data
        CountryList = new list<Country__c>();
        Country__c country = new Country__c(name='TestCountry144',PRA_Country_ID__c='10009', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0);
        CountryList.add(country);
        Country__c country1 = new Country__c(name='TestCountry244',PRA_Country_ID__c='10109', Country_Code__c='T2',Region_Name__c='North America2', 
        daily_Business_Hrs__c=7.5);
        CountryList.add(country1);
        insert CountryList;     

        //FC Data
        FunctionCodeList= new list<Function_Code__c>();
        Function_Code__c functionCode1 = new Function_Code__c(name='FY');
        FunctionCodeList.add(functionCode1);
        Function_Code__c functionCode2 = new Function_Code__c(name='FZ');
        FunctionCodeList.add(functionCode2);
        insert FunctionCodeList;

        
        return functionCode1;
    }
       
    public BUF_Code__c getBUFCode(){
        //Country data
        CountryList = new list<Country__c>();
        Country__c country = new Country__c(name='TestCountry155',PRA_Country_ID__c='10055', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0);
        CountryList.add(country);
        Country__c country1 = new Country__c(name='TestCountry255',PRA_Country_ID__c='10155', Country_Code__c='T2',Region_Name__c='North America2', 
        daily_Business_Hrs__c=7.5);
        CountryList.add(country1);
        insert CountryList;     
        
        //BU Data
        BusinessUnitList = new list<Business_Unit__c>();
        Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
        BusinessUnitList.add(businessUnit1);
        Business_Unit__c businessUnit2 = new Business_Unit__c(name='BU2',Default_Country__c=country1.Id);
        BusinessUnitList.add(businessUnit2);
        insert BusinessUnitList;
        
        //FC Data
        FunctionCodeList= new list<Function_Code__c>();
        Function_Code__c functionCode1 = new Function_Code__c(name='F1');
        FunctionCodeList.add(functionCode1);
        Function_Code__c functionCode2 = new Function_Code__c(name='F2');
        FunctionCodeList.add(functionCode2);
        insert FunctionCodeList;
        
        //BUF Code Data
        BufCodeList= new list<BUF_Code__c>();
        BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
        BufCodeList.add(BufCode1);
        BUF_Code__c BufCode2 = new BUF_Code__c(name='BU2F2',Business_Unit__c=businessUnit2.Id,Function_Code__c=functionCode2.Id);
        BufCodeList.add(BufCode2);
        insert BufCodeList;
        
        return BufCode1;
    }
    
    public Revenue_Allocation_Rate__c getRevRate(){      
            //Country data
            CountryList = new list<Country__c>();
            Country__c country = new Country__c(name='TestCountry22',PRA_Country_ID__c='1000', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
            CountryList.add(country);
            insert CountryList;     
            
            //BU Data
            BusinessUnitList = new list<Business_Unit__c>();
            Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU122',Default_Country__c=country.Id);
            BusinessUnitList.add(businessUnit1);
            insert BusinessUnitList;
            
            //FC Data
            FunctionCodeList= new list<Function_Code__c>();
            Function_Code__c functionCode1 = new Function_Code__c(name='F122');
            FunctionCodeList.add(functionCode1);
            insert FunctionCodeList;
            
            //BUF Code Data
            BufCodeList= new list<BUF_Code__c>();
            BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F122',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
            BufCodeList.add(BufCode1);
            insert BufCodeList;
            
            //Currency Data
            currencyList= new list<Currency__c>();
            Currency__c currency1 = new Currency__c(name='USD');
            currencyList.add(currency1);
            Currency__c currency2 = new Currency__c(name='EUR');
            currencyList.add(currency2);
            insert currencyList;
            
            //Revenue Allocation Rate Data
            revenueList= new list<Revenue_Allocation_Rate__c>();
            Revenue_Allocation_Rate__c revenue1= new Revenue_Allocation_Rate__c(Function__c=bufcode1.Id,Rate__c=123,Year__c='2011',Currency__c=currency1.Id);
            revenueList.add(revenue1);
            insert revenueList;
                 
            return revenue1;
    }
    
    public Cost_Rate__c getCostRate(){      
            //Country data
            CountryList = new list<Country__c>();
            Country__c country = new Country__c(name='TestCountry22',PRA_Country_ID__c='1000', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
            CountryList.add(country);
            insert CountryList;     
            
            //BU Data
            BusinessUnitList = new list<Business_Unit__c>();
            Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU122',Default_Country__c=country.Id);
            BusinessUnitList.add(businessUnit1);
            insert BusinessUnitList;
            
            //FC Data
            FunctionCodeList= new list<Function_Code__c>();
            Function_Code__c functionCode1 = new Function_Code__c(name='F122');
            FunctionCodeList.add(functionCode1);
            insert FunctionCodeList;
            
            //BUF Code Data
            BufCodeList= new list<BUF_Code__c>();
            BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F122',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
            BufCodeList.add(BufCode1);
            insert BufCodeList;
            
            //Currency Data
            currencyList= new list<Currency__c>();
            Currency__c currency1 = new Currency__c(name='USD');
            currencyList.add(currency1);
            Currency__c currency2 = new Currency__c(name='EUR');
            currencyList.add(currency2);
            insert currencyList;
            
            //Cost Rate Data
            costrateList= new list<Cost_Rate__c>();
            Cost_Rate__c costrate1= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2011');
            costrateList.add(costrate1);
            insert costrateList;
                 
            return costrate1;
    }
    
    public void initAll(){
        
            
            //Country data
            CountryList = new list<Country__c>();
            Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
            CountryList.add(country);
            Country__c country1 = new Country__c(name='TestCountry2',PRA_Country_ID__c='101', Country_Code__c='T2',Region_Name__c='North America2', 
            daily_Business_Hrs__c=7.5);
            CountryList.add(country1);
            insert CountryList;     
            
            //BU Data
            BusinessUnitList = new list<Business_Unit__c>();
            Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
            BusinessUnitList.add(businessUnit1);
            Business_Unit__c businessUnit2 = new Business_Unit__c(name='BU2',Default_Country__c=country1.Id);
            BusinessUnitList.add(businessUnit2);
            insert BusinessUnitList;
            
            //FC Data
            FunctionCodeList= new list<Function_Code__c>();
            Function_Code__c functionCode1 = new Function_Code__c(name='F1');
            FunctionCodeList.add(functionCode1);
            Function_Code__c functionCode2 = new Function_Code__c(name='F2');
            FunctionCodeList.add(functionCode2);
            insert FunctionCodeList;
            
            //BUF Code Data
            BufCodeList= new list<BUF_Code__c>();
            BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
            BufCodeList.add(BufCode1);
            BUF_Code__c BufCode2 = new BUF_Code__c(name='BU2F2',Business_Unit__c=businessUnit2.Id,Function_Code__c=functionCode2.Id);
            BufCodeList.add(BufCode2);
            insert BufCodeList;
            
            //Currency Data
            currencyList= new list<Currency__c>();
            Currency__c currency1 = new Currency__c(name='USD');
            currencyList.add(currency1);
            Currency__c currency2 = new Currency__c(name='EUR');
            currencyList.add(currency2);
            insert currencyList;

            
            //Cost Rate Data
            costrateList= new list<Cost_Rate__c>();
            Cost_Rate__c costrate1= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2011');
            costrateList.add(costrate1);
            Cost_Rate__c costrate1a= new Cost_Rate__c(BUF_Code__c=bufcode2.Id,Cost_Rate__c=122,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2011');
            costrateList.add(costrate1a);
            Cost_Rate__c costrate2= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2012');
            costrateList.add(costrate2);
            Cost_Rate__c costrate3= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2013');
            costrateList.add(costrate3);
            Cost_Rate__c costrate4= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2014');
            costrateList.add(costrate4);
            Cost_Rate__c costrate5= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2015');
            costrateList.add(costrate5);
            Cost_Rate__c costrate6= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2016');
            costrateList.add(costrate6);
            Cost_Rate__c costrate7= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2017');
            costrateList.add(costrate7);
            Cost_Rate__c costrate8= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2018');
            costrateList.add(costrate8);
            Cost_Rate__c costrate9= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2019');
            costrateList.add(costrate9);
            Cost_Rate__c costrate10= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2020');
            costrateList.add(costrate10);
            Cost_Rate__c costrate11= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2021');
            costrateList.add(costrate11);
            Cost_Rate__c costrate12= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2022');
            costrateList.add(costrate12);
            Cost_Rate__c costrate13= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2023');
            costrateList.add(costrate13);
            Cost_Rate__c costrate14= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2024');
            costrateList.add(costrate14);
            Cost_Rate__c costrate15= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2025');
            costrateList.add(costrate15);
            Cost_Rate__c costrate16= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency_lookup__c=currency1.Id,Year_Rate_Applies_To__c='2026');
            costrateList.add(costrate16);
            insert costrateList;
            
            //Revenue Allocation Rate Data
            revenueList= new list<Revenue_Allocation_Rate__c>();
            Revenue_Allocation_Rate__c revenue1= new Revenue_Allocation_Rate__c(Function__c=bufcode1.Id,Rate__c=123,Year__c='2011',Currency__c=currency1.Id);
            revenueList.add(revenue1);
            Revenue_Allocation_Rate__c revenue2= new Revenue_Allocation_Rate__c(Function__c=bufcode2.Id,Rate__c=123,Year__c='2012',Currency__c=currency1.Id);
            revenueList.add(revenue2);
            insert revenueList;
            
            // JOb positions
            Job_Class_Desc__c position = new Job_Class_Desc__c(Job_Position_Long__c='position',Job_Code__c='position',Job_Class_Code__c='position',Job_Class_ExtID__c='position');
            insert position;
            
            //Bill Rate Model
            billRateModelList = new List<Bill_Rate_Card_Model__c>();
            Bill_Rate_Card_Model__c model1= new Bill_Rate_Card_Model__c(Name='Model1',Year__c='2015');
            billRateModelList.add(model1);
            Bill_Rate_Card_Model__c model2= new Bill_Rate_Card_Model__c(Name='Model1',Year__c='2015');
            billRateModelList.add(model2);
            insert billRateModelList;
            
            //Bill Rates
            billRatesList = new List<Bill_Rate__c>();
            Bill_Rate__c bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2015');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2016');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2017');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2018');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2019');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2020');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2021');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2022');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2023');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2024');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2025');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2026');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2027');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2028');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2029');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2030');
            billRatesList.add(bill1);
            bill1 = new Bill_Rate__c(Model_Id__c=model1.Id,Bill_Rate__c=100.00,Currency__c=currency1.Id,Job_Position__c=position.Id,Country__c = country1.Id,Schedule_Year__c='2015',Year_Rate_Applies_To__c='2031');
            billRatesList.add(bill1);
            
            insert billRatesList;
            
        }
        
        
}