public with sharing class BDT_RoleService {

	/**
	 * Get the displaywrapper that shows roles and a checkbox boolean
	 * @author Maurice Kremer
	 * @version 17Oct13
	 * @return list<displayRoleSelector>
	 */
	public static list<displayRoleSelector> getDisplayRoles () {
		list<displayRoleSelector>  display = new  list<displayRoleSelector>();
		for (BDT_Role__c role: BDT_RoleDataAccessor.getAllRoles('Name')) {
			display.add(new displayRoleSelector(role));
		}
		return display;
	}

	/**
	 * Get specific role
	 * @author Maurice Kremer
	 * @version 17Oct13
	 * @param String RoleId Role Id
	 * @return BDT_Role__c
	 */
	public static BDT_Role__c getRole(String RoleId) {
		return BDT_RoleDataAccessor.getRole(RoleId);
	}

	/**
	 * Save list of roles and calculate any missing rolenumbers
	 * @author Maurice Kremer
	 * @version 17Oct13
	 * @param list<BDT_Role__c> saveList List of roles to save
	 */
	public static void saveRoles(list<BDT_Role__c> saveList) {

		list<BDT_Role__c> updateList = new list<BDT_Role__c>();
		list<BDT_Role__c> insertList = new list<BDT_Role__c>();
		for (BDT_Role__c role : saveList) {
			if (role.id == null) {
				insertList.add(role);
			} else {
				updateList.add(role);
			}
		}

		//determine rolenumber if there are new records
		if (insertList.size() > 0) {
			// read used numbers
			Set<Object> usedNumbers = COM_SObjectUtils.getSetFromList (BDT_RoleDataAccessor.getAllRoles(), 'Role_Number__c');
			// generate a new set
			List<Decimal> availableNumbers = new List<Decimal>();
			// get numbers needed for new records
			for (Integer i = 1; (i < 100 && availableNumbers.size() < insertList.size() ) ; i++) {
				if (!usedNumbers.contains(Decimal.valueOf(i))) {
					availableNumbers.add(i);
				}
			}

			// assign one of free numbers to roles that need to be created
			for (Integer i = 0; i < insertList.size() ; i++) {
				insertList[i].Role_Number__c = availableNumbers[i];
			}
		}

		// combine lists for single upsert statement
		list<BDT_Role__c> upsertList = new list<BDT_Role__c>();
		upsertList.addAll(insertList);
		upsertList.addAll(updateList);

		upsert upsertList;
	}

	/**
	 * Delete specific role number
	 * @author Maurice Kremer
	 * @version 17Oct13
	 * @param Decimal RoleNumber Rolenumber to be deleted
	 */
	public static void deleteRole (Decimal RoleNumber) {
		
		// delete role for users
		BDT_UserRolesService.removeRoleFromAllUsers (roleNumber);

		delete [select id from BDT_Role__c where Role_Number__c = :RoleNumber];
		
	}
	
	public class displayRoleSelector {
		
		public displayRoleSelector(BDT_Role__c role) {
			roleSelected = false;
			this.role = role;
		}
		
		public Boolean 		roleSelected 	{get;set;}
		public BDT_Role__c 	role	 		{get;set;}
	}

}