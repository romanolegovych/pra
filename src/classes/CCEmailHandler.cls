global class CCEmailHandler implements Messaging.InboundEmailHandler
{
	global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope)
	{
		Messaging.InboundEmailResult result = new Messaging.InboundEmailresult();
		
		Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
		message.setReplyTo('cc_email_handler@m-1nsngf3r3f5q4tmxoclkrxchld0stehy8ou98s2dc7x22tqaln.l-2a0g1mac.cs8.apex.sandbox.salesforce.com');
		
		message.setToAddresses(new List<String>{'ilya@silvertreesystems.com'});
		message.setSubject('Some subject');
		
		try
		{
			updateDB(parseSubject(email.subject).get('ID'), parseRemainingTime(email.plainTextBody));
			
			message.setHtmlBody('Success! ' + parseSubject(email.subject).get('ID') + ' ' + parseRemainingTime(email.plainTextBody));
		}
		catch (Exception e)
		{
			result.success = false;
			result.message = e.getMessage();
			
			message.setHtmlBody('ERROR: ' + e.getMessage());
		}
		
		Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{message});
		
		
		return result;
		
		/**
		* 1. Locate associated Critical Chain Step data
		* 2. Parse user's response: find out how many working days between today and response
		* 3. Update data from step #1
		* 4. Notify a user in case of an error
		*/
	}
	
	private void updateDB(String recordID, Integer remainingTime)
	{
		for (Critical_Chain_Step_Junction__c each : [select Remaining_Time__c from Critical_Chain_Step_Junction__c where Id = :recordID])
		{
			each.Remaining_Time__c = remainingTime;
			update each;
		}
	}
	
	private Map<String, String> parseSubject(String subject)
	{
		Map<String, String> result = new Map<String, String>();
		System.Matcher subjectMatcher = System.Pattern.compile('(?:Re: )?(?:Sandbox: )?How many time is remaining for critical step "(.+?)"\\((.*)\\)').matcher(subject);
		if (subjectMatcher.lookingAt())
		{
			result.putAll(new Map<String, String>{
				'Name' => subjectMatcher.group(1),
				'ID' => subjectMatcher.group(2)
			});
		}
		
		return result;
	}
	
	public static Integer parseRemainingTime(String body)
	{
		Integer remainingTime;
		System.Matcher eolMatcher = System.Pattern.compile('(?:\\s*)(([\\d]{2})-([\\w]{3})-([\\d]{4}))').matcher(body);
		try
		{
			if (eolMatcher.lookingAt())
			{
				remainingTime = new EWDatetime(System.today()).workdaysBetween(parseDate(eolMatcher.group(1)));
			}
			else if (eolMatcher.usePattern(Pattern.compile('(?:\\s*)([\\d]+)')).lookingAt())
			{
				remainingTime = Integer.valueOf(eolMatcher.group(1));
			}
		}
		catch (Exception e)
		{
			//do nothing
		}
		
		return remainingTime;
	}
	
	private static Map<String, String> MONTH_MAP = new Map<String, String>{
		'JAN' => '01',
		'FEB' => '02',
		'MAR' => '03',
		'APR' => '04',
		'MAY' => '05',
		'JUN' => '06',
		'JUL' => '07',
		'AUG' => '08',
		'SEP' => '09',
		'OCT' => '10',
		'NOV' => '11',
		'DEC' => '12'
	};
	
	public static Date parseDate(String dateString)
	{
		System.Matcher dateMatcher = System.Pattern.compile('([\\d]{2})-([\\w]{3})-([\\d]{4})').matcher(dateString);
		if (dateMatcher.lookingAt())
		{
			return Date.valueOf(dateMatcher.group(3) + '-' + MONTH_MAP.get(dateMatcher.group(2).toUppercase()) + '-' + dateMatcher.group(1));
		}
		
		return null;
	}
}