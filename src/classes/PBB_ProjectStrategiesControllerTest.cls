/**
@author Bhargav Devaram
@date 2015
@description this test class is to display the Project Strategies tab and its sub tab menus related to a scenario results
**/
@isTest
private class PBB_ProjectStrategiesControllerTest{
    static testMethod void testClassdata(){
        test.startTest();
        PBB_TestUtils tu = new PBB_TestUtils ();        
        //Prepare the test data        
        tu.wfmp = tu.createWfmProject();        
        tu.bid =tu.createBidProject(); 
        tu.protocol = tu.createProtocol();     
         
        ApexPages.currentPage().getParameters().put('ProtocolID',tu.protocol.id);
        PBB_ProjectStrategiesController pc = new PBB_ProjectStrategiesController();  
        test.stopTest();
    }    
}