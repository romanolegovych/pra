@isTest
public with sharing class BDT_SecuritiesServiceTest {
	

	static list<BDT_Role__c> createBaseData () {
		// create a couple of roles
		list<BDT_Role__c> roleList = new list<BDT_Role__c>();
		roleList.add(new BDT_Role__c(Name='b',Role_Number__c=2));
		roleList.add(new BDT_Role__c(Name='a',Role_Number__c=1));
		insert roleList;
		
		// create a couple of securities
		list<BDT_Securities__c> securityList = new list<BDT_Securities__c>();
		securityList.add(new BDT_Securities__c(Name='s2'));
		securityList.add(new BDT_Securities__c(Name='s1'));
		insert securityList;

		return roleList;
	}
	
	static testMethod void getSecuritiesTest() {
		
		list<BDT_Role__c> roleList = createBaseData();
		
		// get the display wrapper
		list<BDT_SecuritiesService.SecuritiesDisplay> sdList = BDT_SecuritiesService.getSecurities(roleList);
		
		// check security names in wrapper --> names should be alphabetically ordered
		system.assertEquals('s1', sdList[0].securityName);
		system.assertEquals('s2', sdList[1].securityName);
			
		// check role order within wrapper --> roles should be ordered like list order
		system.assertEquals(2, sdList[0].roles[0].roleNumber);
		system.assertEquals(1, sdList[0].roles[1].roleNumber);
		system.assertEquals(2, sdList[1].roles[0].roleNumber);
		system.assertEquals(1, sdList[1].roles[1].roleNumber);
		
	}
	
	static testMethod void saveSecuritiesTest() {
		
		list<BDT_Role__c> roleList = createBaseData();
		
		// get the display wrapper
		list<BDT_SecuritiesService.SecuritiesDisplay> sdList = BDT_SecuritiesService.getSecurities(roleList);

		// set all roles in s1 but not s2
		sdList[0].roles[0].createPriv = true;
		sdList[0].roles[1].readPriv = true;
		sdList[0].roles[0].updatePriv = true;
		sdList[0].roles[1].deletePriv = true;
		sdList[0].roles[0].privWasAdjusted = true;
		sdList[0].roles[1].privWasAdjusted = true;
		
		BDT_SecuritiesService.saveSecurities(sdList);
		
		// test if data was stored correctly
		BDT_Securities__c sec;
		sec = BDT_Securities__c.getValues('s1');
		system.assertEquals('2',sec.CreateRoleNumbers__c);	//role b assigned
		system.assertEquals('1',sec.ReadRoleNumbers__c);	//role a assigned
		system.assertEquals('2',sec.UpdateRoleNumbers__c);	//role b assigned
		system.assertEquals('1',sec.DeleteRoleNumbers__c);	//role a assigned
		
		sec = BDT_Securities__c.getValues('s2');
		system.assertEquals(null,sec.CreateRoleNumbers__c);	//no roles assigned
		system.assertEquals(null,sec.ReadRoleNumbers__c);	//no roles assigned
		system.assertEquals(null,sec.UpdateRoleNumbers__c);	//no roles assigned
		system.assertEquals(null,sec.DeleteRoleNumbers__c);	//no roles assigned
	}
	
	static testMethod void updatePrivilegeTest() {
		//1
		system.assertEquals('1:2:3',BDT_SecuritiesService.updatePrivilege('1:2:3',1,true)); //keep role
		system.assertEquals('2:3',BDT_SecuritiesService.updatePrivilege('1:2:3',1,false)); 	//remove role
		system.assertEquals('2:3',BDT_SecuritiesService.updatePrivilege('2:3',1,false)); 	//do not add role
		system.assertEquals('2:3:1',BDT_SecuritiesService.updatePrivilege('2:3',1,true)); 	//add role
		//2
		system.assertEquals('1:2:3',BDT_SecuritiesService.updatePrivilege('1:2:3',2,true));
		system.assertEquals('1:3',BDT_SecuritiesService.updatePrivilege('1:2:3',2,false));
		system.assertEquals('1:3',BDT_SecuritiesService.updatePrivilege('1:3',2,false));
		system.assertEquals('1:3:2',BDT_SecuritiesService.updatePrivilege('1:3',2,true));
		//3
		system.assertEquals('1:2:3',BDT_SecuritiesService.updatePrivilege('1:2:3',3,true));
		system.assertEquals('1:2',BDT_SecuritiesService.updatePrivilege('1:2:3',3,false));
		system.assertEquals('1:2',BDT_SecuritiesService.updatePrivilege('1:2',3,false));
		system.assertEquals('1:2:3',BDT_SecuritiesService.updatePrivilege('1:2',3,true));
	}
	
	static testMethod void hasRoleTest() {
		String roleSet = '1:2:3';
		Decimal a = 1;
		Decimal b = 2;
		Decimal c = 3;
		Decimal d = 4;
		
		system.assertEquals(true,BDT_SecuritiesService.hasRole(roleSet,a));
		system.assertEquals(true,BDT_SecuritiesService.hasRole(roleSet,b));
		system.assertEquals(true,BDT_SecuritiesService.hasRole(roleSet,c));
		system.assertEquals(false,BDT_SecuritiesService.hasRole(roleSet,d));
	}
	
	static testMethod void wrapperCoverage() {
		BDT_SecuritiesService.pageSecurityWrapper a= new BDT_SecuritiesService.pageSecurityWrapper('test');
		system.assert(a.getCreatePriv());
		system.assert(a.getUpdatePriv());
		system.assert(a.getDeletePriv());
		system.assert(a.getReadPriv());
	} 

}