public with sharing class BDT_LaboratoryListsController {
	public BDT_SecuritiesService.pageSecurityWrapper 		securitiesA 		{get;set;} // Administration Lab Parameters
	public List<String>										listsNames 		 	{get;set;}
	public Boolean											noSelection		 	{get;set;}
	public Boolean											addEditFlag		 	{get;set;}
	public Boolean											addFlag			 	{get;set;}
	public String											firstColName	 	{get;set;}
	public String											secondColName	 	{get;set;}
	public Boolean											secondColFlag	 	{get;set;}
	public String											selectedListName 	{get;set;}
	public String											selectedValueIndex	{get;set;}
	public List<LaboratorySelectionLists__c>				selectedLabList	 	{get;set;}
	public LaboratorySelectionLists__c						labSelListItem	 	{get;set;}
	
	
	// Initialize variables in class constructor
	public BDT_LaboratoryListsController() {
		securitiesA 	= new BDT_SecuritiesService.pageSecurityWrapper('Administration Lab Parameters');
		listsNames	 	= new List<String>();
		selectedLabList = new List<LaboratorySelectionLists__c>(); 
		labSelListItem	= new LaboratorySelectionLists__c();
		noSelection		= true;
		addEditFlag		= false;
		addFlag			= false;
		findLabLists();
	} 
	
	
	// It finds the distinct names of the lists and save them sorted in listsNames list
	void findLabLists() {
		Set<String> tmpSet = new Set<String>();
		List<LaboratorySelectionLists__c> tmpList = new List<LaboratorySelectionLists__c>();
		// Retrieve all the records that they don't have listFirstValue__c. 
		// These are the default records and they are unique per List. so no necessity to create firstly a Set.  
		tmpList = [SELECT Name 
				   FROM   LaboratorySelectionLists__c 
				   WHERE  listFirstValue__c = null 	OR 
						  listFirstValue__c = ''];
		for(LaboratorySelectionLists__c tmpItem : tmpList) {
			listsNames.add(tmpItem.Name);
		}
		listsNames.sort(); 
	}
	
	
	// It finds the selected list values
	public void getListValues() {
		noSelection = false;
		decideColumnsLabel(selectedListName);
		selectedLabList = [SELECT 	Id,
									Name,
									listFirstValue__c, 
									listSecondValue__c  
						   FROM 	LaboratorySelectionLists__c 
						   WHERE 	Name =: selectedListName	AND
									listFirstValue__c != null 	AND 
									listFirstValue__c != ''];
	}
	
	
	// Used to find the label of the results table columns
	void decideColumnsLabel(String listName) {
		secondColName = '';
		secondColFlag = false; 
		if(listName == 'Amount/Volume Units') {
			firstColName  = 'Amount/Volume unit';
		} else if(listName == 'Analytical Techniques') {
			firstColName  = 'Analytical Technique';
		} else if(listName == 'Anti-coagulants') {
			firstColName  = 'Anti-coagulant';
		} else if(listName == 'Compound Classes') {
			firstColName  = 'Class';
			secondColName = 'Additional Information';
			secondColFlag = true; 
		} else if(listName == 'Concentration Units') {
			firstColName  = 'Range unit'; 
		} else if(listName == 'Detection Techniques') {
			firstColName  = 'Detection';
		} else if(listName == 'Detection Types') {
			firstColName  = 'Type'; 
		} else if(listName == 'Matrices') {
			firstColName  = 'Matrix';
		} else if(listName == 'Sample Preparation Techniques') {
			firstColName  = 'Sample Preparation';
			secondColName = 'Abbreviation';
			secondColFlag = true; 
		} else if(listName == 'Species') {
			firstColName  = 'Species';
		} else if(listName == 'Storage Temperatures') {
			firstColName  = 'Temperature';
		} 
	} 
	
	
	// It enables the add/edit panel in add mode
	public void addSelectedValue() {
		labSelListItem	= new LaboratorySelectionLists__c();	
		labSelListItem.Name = selectedListName;
		addEditFlag = true;
		addFlag 	= true; 
	}
	
	
	// It enables the add/edit panel in edit mode
	public void editSelectedValue() {
		labSelListItem = new LaboratorySelectionLists__c();
		labSelListItem = selectedLabList[Integer.valueOf(selectedValueIndex)];
		addEditFlag = true;
		addFlag		= false;
	}
	
	
	// It cancels the add/edit value procedure
	public void cancelValue() {
		addEditFlag = false;
	}
	
	
	// It deletes the selected value from the selected list
	public void deleteValue() {
		delete labselListItem;
		getListValues();
		addEditFlag = false;
	}
	
	
	// It saves the value in the selected list
	public void saveValue() {
		if(labselListItem.listFirstValue__c==null || labselListItem.listFirstValue__c=='') {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The ' + firstColName + ' cannot be empty. Please, fill in a value');
			ApexPages.addMessage(msg);
			return;
		}
		upsert labselListItem;
		if(addFlag) {
			selectedLabList.add(labselListItem);
		} else {
			selectedLabList[Integer.valueOf(selectedValueIndex)] = labselListItem;
		}
		addEditFlag = false;
	}
	
}