public with sharing class PIT_customLookupController {
    
    transient public List<Employee_Details__c> lstEmployees{get;set;}
    public List<UserWrapper> lstUsers{get;set;}
    public String searchString {get;set;}
    //public User dummyUser {get;set;}
    public Employee_Details__c objEmployee {get;set;}
    public List<SelectOption> BU_OptionsList     {get;set;}
    public List<SelectOption> JobsOptionsList    {get;set;}
    public List<SelectOption> CountryOptionsList {get;set;}
    public List<SelectOption> DepOptionsList     {get;set;}
    public Map<String, Set<String>> BU_Dep_Map   {get;set;}
    
    public PIT_customLookupController() {
        // Read the selected user and create lists
        //dummyUser = new User();
        objEmployee = new Employee_Details__c();
        lstEmployees = new List<Employee_Details__c>();
        lstUsers = new List<UserWrapper>();
        BU_Dep_Map = new Map<String, Set<String>>();
        searchString = System.currentPageReference().getParameters().get('lksrch');
        // If there is already a selected user, find him
        if(String.isNotBlank(searchString)) {
            try {
               
                                objEmployee = [SELECT id,
                                    Name, 
                                    Business_Unit_Desc__c, 
                                    Department__c,
                                    Job_Class_Desc__c, 
                                    Country_Name__c 
                            FROM    Employee_Details__c 
                            WHERE   name =: searchString 
                            LIMIT   1];
            } catch (exception e) {
                //dummyUser = new User();
                 objEmployee = new Employee_Details__c();
            }
            //if(searchString!=null && searchString!=''){
            //list<Country__c> lstCountries = [SELECT id, Name FROM Country__c ORDER BY Name limit 1];
            //if ( lstCountries != null && lstCountries.size ()>0 && objEmployee.Country_Name__c == null  )
                    //objEmployee.Country_Name__c =lstCountries[0].Id;
            //}
        }
        // Search the results
        //String tmpStr = dummyUser.Department;
        createOptionsList();  
        buildDepartmentList();      
        if(searchString!=null && searchString!='')
        search();
    }
    
    
    // used to create the dropdown lists
    public void createOptionsList() {
        //List<Employee_Details__c> tmpEmployeeList = new List<Employee_Details__c>();
        Set<String> tmpBUSet = new Set<String>();
        Set<String> tmpCountriesSet = new Set<String>();
        Set<String> tmpJobsSet = new Set<String>();
        
        // Read available values
        Set<String> tmpSet = new Set<String>();
       
        for(PRA_Business_Unit__c bulist:[SELECT Name FROM PRA_Business_Unit__c]){
            tmpBUSet.add(bulist.Name);          
        }        
        for(Job_Class_Desc__c jobcls:[SELECT Name FROM Job_Class_Desc__c]){
            tmpJobsSet.add(jobcls.Name);
        }       
        // Build the Businness Unit list
        BU_OptionsList = new List<SelectOption>();
        BU_OptionsList.add(new SelectOption('0','--None--'));
        for(String tmpStr : tmpBUSet) {
            BU_OptionsList.add(new SelectOption(tmpStr, tmpStr));
        }   
        BU_OptionsList.sort();
        // Build the Job Family list
        JobsOptionsList = new List<SelectOption>();
        JobsOptionsList.add(new SelectOption('0','--None--'));
        for(String tmpStr : tmpJobsSet) {
            JobsOptionsList.add(new SelectOption(tmpStr, tmpStr));
        }   
        JobsOptionsList.sort();
        
        // Build the countries list
        CountryOptionsList = new List<SelectOption>();
        CountryOptionsList.add(new SelectOption('','--None--'));
        for(Country__c objCountry : [SELECT id, Name FROM Country__c ORDER BY Name ]) {
            CountryOptionsList.add(new SelectOption(objCountry.Id, objCountry.Name));
        }
       // CountryOptionsList.sort();
    }
    
    
    // used to build the Department list
    public void buildDepartmentList() {
        Set<String> tmpSet = new Set<String>();
        for(Department__c dept:[SELECT Name FROM Department__c]){
            tmpSet.add(dept.Name);
        }
        DepOptionsList = new List<SelectOption>();
        DepOptionsList.add(new SelectOption('0','--None--'));
        for(String tmpStr : tmpSet) {
            DepOptionsList.add(new SelectOption(tmpStr, tmpStr));
        }
        DepOptionsList.sort();
        //search();
    } 
    
    
    // used to clear all the search criteria
    public void clearCriteria() {
        objEmployee = new Employee_Details__c();        
        lstusers.clear();
        searchString = '';
    }
    
    // used to search the persons that can be associated with the search attributes
    public void search() {
        list<User> lstUsersSearch = new list<User>();
        if ( searchString == null || searchString==''){
            lstUsersSearch =[select Id,Name,EmployeeNumber from User where isactive=true and (employeenumber!=null or employeenumber!='') order by FirstName  limit 1000];
        }        
        else if ( searchString != null && searchString != '' ){
            string soqluser = 'select Id,Name,EmployeeNumber from User where isactive=true and  Name LIKE \''+String.escapeSingleQuotes(searchString)+'%\' Order by FirstName limit 1000';
            lstUsersSearch = database.query(soqluser);
        }
        
        lstUsers = new List<UserWrapper>();
        map<string,User> mapUsers= new map<string,User>();
        for (User objUser:lstUsersSearch)
        {
            mapUsers.put(objUser.EmployeeNumber,objUser);
        }
        system.debug('*********mapUsers************'+mapUsers);
        
        set<string> setNames =new  set<string> ();
        setNames =mapUsers.keyset();
        system.debug('*********setNames************'+setNames);
         String soql = 'SELECT id, Name,First_Name__c, Last_Name__c, Business_Unit_Desc__c, Job_Class_Desc__c, Country_Name__c,Country_Name__r.Name, Department__c FROM Employee_Details__c WHERE';
        // Add Bussiness Unit
        if (objEmployee.Business_Unit_Desc__c != null && objEmployee.Business_Unit_Desc__c != '0') {
            soql += ' Business_Unit_Desc__c = \'' + objEmployee.Business_Unit_Desc__c + '\' AND';
        }   
        // Add Department
        if (objEmployee.Department__c != null && objEmployee.Department__c != '0') {
            soql += ' Department__c = \'' + objEmployee.Department__c + '\' AND';  
        }
        // Add Job family
        if (objEmployee.Job_Class_Desc__c != null && objEmployee.Job_Class_Desc__c != '0') {
            soql += ' Job_Class_Desc__c = \'' + objEmployee.Job_Class_Desc__c + '\' AND';
        }
        system.debug('test String '+objEmployee.Country_Name__c);
        // Add country
         //system.debug('*********************'+objEmployee.Country_Name__c);
        if (objEmployee.Country_Name__c != null) {
            soql += ' Country_Name__c = \'' + objEmployee.Country_Name__c + '\' AND';
        }
       /* if ( searchString != null && searchString != '' ){
            soql += ' ( Last_Name__c LIKE \''+String.escapeSingleQuotes(objEmployee.Last_Name__c)+'%\' OR First_Name__c LIKE \''+String.escapeSingleQuotes(objEmployee.First_Name__c)+'%\') AND';
        }*/
        // Add name
        soql += ' name IN : setNames ORDER BY First_Name__c limit 1000';
        try {
                //system.debug('test mapUsers '+mapUsers);
            //lstEmployees.clear();
            lstEmployees = database.query(soql);
            
            for(Employee_Details__c objEmp: lstEmployees)
            {                
                system.debug('test emp name val '+objEmp.Name );
                if ( mapUsers != null && mapUsers.get(objEmp.Name) != null )
                {   UserWrapper objWrapper = new UserWrapper();
                    //system.debug('test emp name '+mapUsers.get(objEmp.Name) );
                    objWrapper.objEmployee = objEmp;
                    objWrapper.objUser = mapUsers.get(objEmp.Name);
                    lstUsers.add(objWrapper);
                }
            }
        } catch (exception e) {}
    }
    
    
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    
    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
    public class UserWrapper{
        public Employee_Details__c objEmployee{ get; set; }
        public User objUser{ get; set; }
    }
}