/** Implements the test for the Selector Layer of the object LaboratoryAnalysisSlots__c
 * @author	Dimitrios Sgourdos
 * @version	11-Nov-2013
 */
@isTest
private class LaboratoryAnalysisSlotsDataAccessorTest {
	
	// Global variables
	private static List<Study__c>	studiesList;
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	06-Nov-2013
	 */
	static void init(){
		// Create project
		Client_Project__c firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		
		// Create studies
		studiesList = BDT_TestDataUtils.buildStudies(firstProject,3);
		insert studiesList;
	}
	
	
	/** Test the function getSlotsByMdValFlagAndStudies
	 * @author	Dimitrios Sgourdos
	 * @version	11-Nov-2013
	 */
	static testMethod void getSlotsByMdValFlagAndStudiesTest() {
		// Create data
		init();
		
		LaboratoryMethod__c labMethod = new LaboratoryMethod__c (
												Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
												Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood',
												Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle');
		insert labMethod;
		
		List<LaboratoryAnalysisSlots__c> initialList = new List<LaboratoryAnalysisSlots__c>();
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=1, SequenceNumber__c=2, MdValSlot__c=false, Study__c=studiesList[0].Id, LaboratoryMethod__c=labMethod.Id));
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=1, SequenceNumber__c=1, MdValSlot__c=true, Study__c=studiesList[1].Id, LaboratoryMethod__c=labMethod.Id));
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=2, SequenceNumber__c=1, MdValSlot__c=true, Study__c=studiesList[0].Id, LaboratoryMethod__c=labMethod.Id));
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=1, SequenceNumber__c=3, MdValSlot__c=true, Study__c=studiesList[2].Id, LaboratoryMethod__c=labMethod.Id));
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=1, SequenceNumber__c=4, MdValSlot__c=false, Study__c=studiesList[2].Id, LaboratoryMethod__c=labMethod.Id));
		insert initialList;
		
		// Query for the first two studies
		List<Study__c> tmpStudiesList = new List<Study__c>();
		tmpStudiesList.add(studiesList[0]);
		tmpStudiesList.add(studiesList[1]);
		
		List<LaboratoryAnalysisSlots__c> results = LaboratoryAnalysisSlotsDataAccessor.getSlotsByMdValFlagAndStudies(tmpStudiesList, true);
		system.assertEquals(results.size(), 2);
		system.assertequals(results[0].Id, initialList[1].Id);
		system.assertequals(results[1].Id, initialList[2].Id);
		results = LaboratoryAnalysisSlotsDataAccessor.getSlotsByMdValFlagAndStudies(tmpStudiesList, false);
		system.assertEquals(results.size(), 1);
		system.assertequals(results[0].Id, initialList[0].Id);
	}
}