@isTest
private class PRA_ChangeOrderEndDateControllerTest{
	
	static testMethod void PRA_ChangeOrderEndDateControllerTest() { 
        test.startTest();  
        
        PRA_TestUtils tu = new PRA_TestUtils();
        // tu.initAll();      
        insert tu.getAccount('Test Account');
        Date curentDate = System.today();
        Client_Project__c pr1 = tu.getClientProject(tu.client.id, 'Test project1', 'praProjectId1', curentDate.addMonths(-6), curentDate.addMonths(6));
        Client_Project__c pr2 = tu.getClientProject(tu.client.id, 'Test 2', 'praProjectId2', curentDate.addMonths(-6), curentDate.addMonths(6));
        insert new List<Client_Project__c>{pr1,pr2};      
        Operational_Area__c op=tu.getOperationalArea('Therapeutic Expertise', 'HC');
        insert op;      
        Task_Group__c tg=tu.getTaskGroup('Wild coyote', op.id, 'WC');
        insert tg;        
        Business_Unit__c  bu=tu.getBusinessUnit('Road runner hunters', 'RRH');
        insert bu;      
        list<Country__c> c=tu.getCountries(bu.id).values();
        insert c;      
        list<Project_Client_Region__c> pcr=tu.getProjectRegions().values();
        insert pcr;      
        Client_Task__c ctb=tu.getClientTask(tg.id, pr1.id, 'Rocket power', 'Rocket power rolls',  curentDate.addMonths(-4), curentDate.addMonths(4), c[0].Id, pcr[0].Id);
        //ct.Revised_End_Date__c=date.today().addMonths(4);
        insert ctb;      
        Client_Task__c ct1=tu.getClientTask(tg.id, pr1.id, 'Rocket1 power', 'Rocket1 power rolls',  curentDate.addMonths(-4), curentDate.addMonths(4), c[0].Id, pcr[0].Id);
        //ct1.Revised_End_Date__c=date.today();
        insert ct1;
        Change_Order__c coObj=tu.createChangeOrder(pr1.Id);
        //coObj.Proposed_Estimated_End_Date__c=date.today();
        //update coObj;  
        
        Change_Order_Item__c coitem = new Change_Order_Item__c(Change_Order__C=coObj.id);
        coitem.Description__c='Test';
        coitem.Client_Unit_Number__c='123';
        coitem.Project_Region__c=pcr[0].id;
        coitem.End_Date__c=curentDate.addMonths(0).toStartOfMonth();
        coitem.Type__c='New';
        insert coitem;
        
        Change_Order_Item__c coitem1 = new Change_Order_Item__c(Change_Order__C=coObj.id);
        coitem1.Description__c='Test';
        coitem1.Client_Unit_Number__c='123';
        coitem1.Project_Region__c=pcr[0].id;
        coitem1.End_Date__c=curentDate.addMonths(1).toStartOfMonth();
        coitem1.Type__c='New';
        insert coitem1;
        
       // Change_Order__c coObj1=tu.createChangeOrder(pr1.Id);
        coObj.Proposed_Estimated_End_Date__c=date.today();
        update coObj;
        
        PageReference oPage = new PageReference('/apex/PRA_Change_Order_EndDate?id='+coObj.Id);
           Test.setCurrentPage(oPage);
        
        PRA_ChangeOrderEndDateController coController=new PRA_ChangeOrderEndDateController();
       
           coController.selectedProjectid=tu.clientProject.Id;
           PRA_ChangeOrderEndDateController coControllerObj=new PRA_ChangeOrderEndDateController();
           coControllerObj.ProposedProjectEndDate=string.valueof(date.today());
           coController.searchbypropose();
           //coController.GetDateFromString('08/01/2012','mm/dd/yyyy');
           coController.save();
           coController.selectedRecord=coObj.Id;
           coController.viewRecord();
       
        test.stopTest(); 
    }
}