/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class SRM_ValidateCalendarTriggerTest {

    static testMethod void myUnitTest() {
        //Create SRM Model
        SRM_Model__c model = new SRM_MOdel__c();
        model.Status__c = 'In Progress';
        model.Comments__c = 'Test Comments';
        insert model;
        
        //Create Country
        Country__c country = new Country__c();
        country.Name = 'Test India';
        country.Country_Code__c = 'TI';
        country.Daily_Business_Hrs__c = 8;
        country.Description__c = 'Test India';
        country.PRA_Country_Id__c = 'TI1';
        insert country;
        
        //Create SRM Calendar Adjustment Record
        SRM_Calender_Adjustments__c cal = new SRM_Calender_Adjustments__c();
        cal.SRM_Model__c = model.Id;
        cal.Country__c = country.Id;
        cal.From_Date__c = SRM_Utils.getWeekStartDate(Date.today());
        cal.To_Date__c = SRM_Utils.getWeekStartDate(Date.today()+7);
        cal.Site_Act_Adjustment__c = 100;
        cal.Subject_Enroll_Adjustment__c = 100;
        insert cal;
        System.debug('<<<<'+cal);
        cal.From_Date__c = SRM_Utils.getWeekStartDate(Date.today()+20);
        cal.To_Date__c = SRM_Utils.getWeekStartDate(Date.today()+27);
        update cal;
        System.debug('<<<<'+cal);
        try{
            cal.To_Date__c = SRM_Utils.getWeekStartDate(Date.today()+20);
            cal.From_Date__c = SRM_Utils.getWeekStartDate(Date.today()+27);
            update cal;
        }
        catch(Exception e){}
        try{
            cal.From_Date__c = Date.newInstance(2014,11,15);
            cal.To_Date__c = Date.newInstance(2014,11,16);
            update cal;
        }
        catch(Exception e){}
        SRM_Calender_Adjustments__c cal1 = new SRM_Calender_Adjustments__c();
        cal1.SRM_Model__c = model.Id;
        cal1.Country__c = country.Id;
        cal1.From_Date__c = SRM_Utils.getWeekStartDate(Date.today()+40);
        cal1.To_Date__c = SRM_Utils.getWeekStartDate(Date.today()+47);
        cal1.Site_Act_Adjustment__c = 100;
        cal1.Subject_Enroll_Adjustment__c = 100;
        insert cal1;

    }
}