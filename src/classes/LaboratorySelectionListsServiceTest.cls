/** Implements the test for the Service Layer of the object LaboratorySelectionLists__c
 * @author	Dimitrios Sgourdos
 * @version	23-Oct-2013
 */
@isTest
private class LaboratorySelectionListsServiceTest {
	
	// Global variables
	private static List<LaboratorySelectionLists__c>	lslList;
	
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	21-Oct-2013
	 */
	static void init(){
		lslList = new List<LaboratorySelectionLists__c>();
		// Create Laboratory Selection Lists  'Values' Records 
		lslList.add(new LaboratorySelectionLists__c(Name='Category A', listFirstValue__c = 'Value A 3'));
		lslList.add(new LaboratorySelectionLists__c(Name='Category A', listFirstValue__c = 'Value A 2'));
		lslList.add(new LaboratorySelectionLists__c(Name='Category A', listFirstValue__c = 'Value A 1'));
		lslList.add(new LaboratorySelectionLists__c(Name='Category B', listFirstValue__c = 'Value B 1'));
	}
	
	
	/** Test the function splitLaboratorySelectionListsValuesPerCategoryInMap
	 * @author	Dimitrios Sgourdos
	 * @version	23-Oct-2013
	 */
	static testMethod void splitLaboratorySelectionListsValuesPerCategoryInMapTest() {
		init();
		
		Map<String, List<LaboratorySelectionLists__c>> lslMap = LaboratorySelectionListsService.splitLaboratorySelectionListsValuesPerCategoryInMap(lslList);
		
		system.assertEquals(lslMap.keySet().size(), 2);
		system.assertEquals(lslMap.containsKey('Category A'), true);
		system.assertEquals(lslMap.containsKey('Category B'), true);
	}
	
	
	/** Test the function retrieveValuesForCategoryFromMap
	 * @author	Dimitrios Sgourdos
	 * @version	23-Oct-2013
	 */
	static testMethod void retrieveValuesForCategoryFromMapTest() {
		// Initialize
		init();
		
		Map<String, List<LaboratorySelectionLists__c>> lslMap =
										LaboratorySelectionListsService.splitLaboratorySelectionListsValuesPerCategoryInMap(lslList);
		
		// Check the method
		List<LaboratorySelectionLists__c> myList = lslMap.get('Category A');
		system.assertEquals(myList.size(), 3);
		
		// Retrieve 'Category A' without sorting
		List<SelectOption> valuesList = LaboratorySelectionListsService.retrieveValuesForCategoryFromMap(lslMap, 'Category A', false, false);
		system.assertEquals(valuesList.size(), 3);
		system.assertEquals(valuesList[0].getValue(), 'Value A 3');
		system.assertEquals(valuesList[1].getValue(), 'Value A 2');
		system.assertEquals(valuesList[2].getValue(), 'Value A 1');
		
		// Retrieve 'Category A' with sorting
		valuesList = LaboratorySelectionListsService.retrieveValuesForCategoryFromMap(lslMap, 'Category A', false, true);
		system.assertEquals(valuesList.size(), 3);
		system.assertEquals(valuesList[0].getValue(), 'Value A 1');
		system.assertEquals(valuesList[1].getValue(), 'Value A 2');
		system.assertEquals(valuesList[2].getValue(), 'Value A 3');
		
		// Retrieve 'Category B' without none value
		valuesList = LaboratorySelectionListsService.retrieveValuesForCategoryFromMap(lslMap, 'Category B', false, true);
		system.assertEquals(valuesList.size(), 1);
		system.assertEquals(valuesList[0].getValue(), 'Value B 1');
		
		// Retrieve 'Category B' with none value
		valuesList = LaboratorySelectionListsService.retrieveValuesForCategoryFromMap(lslMap, 'Category B', true, true);
		system.assertEquals(valuesList.size(), 2);
		system.assertEquals(valuesList[0].getValue(), '');
		system.assertEquals(valuesList[1].getValue(), 'Value B 1'); 
	}
	
	
	/** Test the function enableAssociatedSelectOption
	 * @author	Dimitrios Sgourdos
	 * @version	17-Oct-2013
	 */
	static testMethod void enableAssociatedSelectOptionTest() {
		Set<String> enablingValuesSet = new Set<String>();
		enablingValuesSet.add('Example A');
		enablingValuesSet.add('Example B');
		
		Boolean enablingFlag = LaboratorySelectionListsService.enableAssociatedSelectOption(enablingValuesSet, 'Example A');
		system.assertEquals(enablingFlag, true);
		
		enablingFlag = LaboratorySelectionListsService.enableAssociatedSelectOption(enablingValuesSet, 'Example C');
		system.assertEquals(enablingFlag, false);
	}
	
	
	/** Test the function readMatrixValuesEnablingAnticoagulant
	 * @author	Dimitrios Sgourdos
	 * @version	21-Oct-2013
	 */
	static testMethod void readMatrixValuesEnablingAnticoagulantTest() {
		Set<String> valuesSet = LaboratorySelectionListsService.readMatrixValuesEnablingAnticoagulant();
		system.assertEquals(valuesSet.size(), 3);
		system.assertEquals(valuesSet.contains('Plasma'), 	   true);
		system.assertEquals(valuesSet.contains('Whole blood'), true);
		system.assertEquals(valuesSet.contains('Blood'), 	   true);
	}
	
	
	/** Test the function readAnalyticalTechniqueValuesEnablingDetection
	 * @author	Dimitrios Sgourdos
	 * @version	21-Oct-2013
	 */
	static testMethod void readAnalyticalTechniqueValuesEnablingDetectionTest() {
		Set<String> valuesSet = LaboratorySelectionListsService.readAnalyticalTechniqueValuesEnablingDetection();
		system.assertEquals(valuesSet.size(), 1);
		system.assertEquals(valuesSet.contains('LC'), true);
	}
	
	
	/** Test the function getAllSelectionListValues.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Oct-2013
	 */
	static testMethod void getAllSelectionListValuesTest() {
		List<LaboratorySelectionLists__c> results = LaboratorySelectionListsService.getAllSelectionListValues();
	}
}