@isTest
private class BDT_FlowchartServiceTotalTest {

	public static Client_Project__c 						firstProject;
	public static List<Study__c> 							studiesList;
	public static List<ClinicalDesign__c> 					designList;
	public static List<Business_Unit__c> 					myBUList;
	public static list<ClinicalDesignStudyAssignment__c> 	cdsaList;
	public static List<Arm__c>								armList;
	public static List<Population__c> 						populationList;
	public static List<PopulationStudyAssignment__c> 		spaList;
	public static list<StudyDesignPopulationAssignment__c>  sdpaList;
	public static list<GroupArmAssignment__c>				gaaList;
	public static List<ServiceCategory__c> 					ServiceCategoryList;
	public static List<Service__c> 							ServiceList;
	public static List<RateCard__c> 						RateCardList;
	public static List<RateCardClass__c> 					RateCardClassList;
	public static List<ProjectService__c> 					ProjectServiceList;
	public static List<Epoch__c> 							EpochList;
	public static List<Flowcharts__c> 						FlowchartList;
	public static List<flowchartassignment__c> 				FlowchartAssignmentList;
	public static List<TimePoint__c>						TimepointList;
	public static List<ProjectServiceTimePoint__c>			ProjectServiceTimePointList;
	
	static void init(){
		//create project
		firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		
		//create business units
		myBUList = BDT_TestDataUtils.buildBusinessUnit(2);
		insert myBUList;
		
		//create studies
		studiesList = BDT_TestDataUtils.buildStudies(firstProject,2);
		studiesList[0].Business_Unit__c = myBUList[0].Id; 
		studiesList[1].Business_Unit__c = myBUList[1].Id;  
		insert studiesList;
		
		//create design
		designList = BDT_TestDataUtils.buildClinicDesign(firstProject,1); 
		insert designList;
		
		//create population
		populationList = BDT_TestDataUtils.buildPopulation(firstProject.id, 2);
		insert populationList;

		// add populations to study
		spaList = BDT_TestDataUtils.buildPopulationStudyAssignment(studiesList, populationList);
		insert spaList;		
		
		// add design to study
		cdsaList = BDT_TestDataUtils.buildDesignAssignment(designList,studiesList); 
		insert cdsaList;
		
		// add populations to study design	
		sdpaList = BDT_TestDataUtils.buildStudyDesignPopulationAssignment(cdsaList, spaList );
		insert sdpaList;	
		
		// add arms to clinical design
		armList = BDT_TestDataUtils.buildArms(designList,1);			 				  
		insert armList;
		
		//add arms to groups
		gaaList = new list<GroupArmAssignment__c>();
		for (Arm__c a:armList) {
			for (StudyDesignPopulationAssignment__c sdpa: sdpaList){
				gaaList.add(new GroupArmAssignment__c(arm__c = a.id,
													  studydesignpopulationassignment__c = sdpa.id,
													  NumberOfSubjects__c = 2));
			}
		}
		insert gaaList;
		
		// add epochs
		EpochList = BDT_TestDataUtils.buildEpochs(designList,1);
		insert EpochList;
		
		// add flowcharts
		FlowchartList = BDT_TestDataUtils.buildFlowcharts(designList,1);
		insert FlowchartList;
		
		//add flowchartassignments
		FlowchartAssignmentList = BDT_TestDataUtils.buildFlowchartAssignment (designList,FlowchartList,armList,EpochList);
		insert FlowchartAssignmentList;	
		
		//add timepoints 																		
		TimepointList = BDT_TestDataUtils.buildTimePoints(FlowchartList, 2);
		insert TimepointList;
		
		// create service categories
		ServiceCategoryList = BDT_TestDataUtils.buildServiceCategory(2, null);
		ServiceCategoryList = BDT_TestDataUtils.buildServiceCategory(1, ServiceCategoryList);
		insert ServiceCategoryList;
		
		// create services that are design objects
		ServiceList = BDT_TestDataUtils.buildService(ServiceCategoryList, 2, 'TEST', true);
		insert ServiceList;
		
		// create ratecards
		RateCardList = BDT_TestDataUtils.buildRateCard(ServiceList, myBUList);
		insert RateCardList;
		
		// create ratecard classes
		RateCardClassList = BDT_TestDataUtils.buildRateCardClass(RateCardList);
		insert RateCardClassList;
		
		// create project services
		ProjectServiceList = BDT_TestDataUtils.buildProjectService(ServiceList, firstProject.id);
		insert ProjectServiceList;	
		
		// add projectservice timepoints
		List<ProjectServiceTimePoint__c> ProjectServiceTimePointNullList = new List<ProjectServiceTimePoint__c>();
		ProjectServiceTimePointList = new List<ProjectServiceTimePoint__c>();
		for (TimePoint__c tp:TimepointList){
			for (ProjectService__c ps:ProjectServiceList) {
				ProjectServiceTimePointNullList.add(new ProjectServiceTimePoint__c(ProjectService__c = ps.id,
																				Flowchart__c = tp.flowchart__c,
																				TimePoint__c = null,
																				TimePointCount__c = null,
																				listOfTimeMinutes__c = null));
				ProjectServiceTimePointList.add(new ProjectServiceTimePoint__c(ProjectService__c = ps.id,
																			   Flowchart__c = tp.flowchart__c,
																			   TimePoint__c = tp.id));
			}
		}
		insert ProjectServiceTimePointNullList;
		insert ProjectServiceTimePointList;	

		// remove groups so it can be redone to trigger the calculation from the arm side
		try{delete [select id from GroupArmAssignment__c];}catch(Exception e){}
		//add arms to groups
		gaaList = new list<GroupArmAssignment__c>();
		for (Arm__c a:armList) {
			for (StudyDesignPopulationAssignment__c sdpa: sdpaList){
				gaaList.add(new GroupArmAssignment__c(arm__c = a.id,
													  studydesignpopulationassignment__c = sdpa.id,
													  NumberOfSubjects__c = 1));
			}
		}
		insert gaaList;

		// test trigger routines that clear orphaned records
		sdpaList[0].ClinicalDesignStudyAssignment__c = null;
		upsert sdpaList[0];
		// test trigger routines that clear orphaned records
		spaList[0].Study__c = null;
		upsert spaList[0];

	}

	static testMethod void myUnitTest() {
		init();
	}
	
	
	static testMethod void determineArmDataTest() {
		
		// create some test data
		list<Population__c> p = new list<Population__c>();
		p.add(new Population__c(name='popA'));
		p.add(new Population__c(name='popB'));
		insert p;
		
		Client_Project__c cp = new Client_Project__c();
		insert cp;
		
		list<Study__c> s = new list<Study__c>();
		s.add(new Study__c(project__c = cp.id));
		s.add(new Study__c(project__c = cp.id));
		insert s;
		
		list<PopulationStudyAssignment__c> psa = new list<PopulationStudyAssignment__c>();
		psa.add(new PopulationStudyAssignment__c(population__c = p[0].id, study__c = s[0].id));
		psa.add(new PopulationStudyAssignment__c(population__c = p[1].id, study__c = s[1].id));
		insert psa;
		
		ClinicalDesign__c cd = new ClinicalDesign__c(Design_Name__c = 'D');
		insert cd;
		
		list<ClinicalDesignStudyAssignment__c> cdsa = new list<ClinicalDesignStudyAssignment__c> ();
		cdsa.add(new ClinicalDesignStudyAssignment__c(study__c = s[0].id,ClinicalDesign__c = cd.id));
		cdsa.add(new ClinicalDesignStudyAssignment__c(study__c = s[1].id,ClinicalDesign__c = cd.id));
		insert cdsa;
		
		list<StudyDesignPopulationAssignment__c> sdpa = new list<StudyDesignPopulationAssignment__c> ();
		sdpa.add(new StudyDesignPopulationAssignment__c(populationstudyassignment__c = psa[0].id,
				clinicaldesignstudyassignment__c = cdsa[0].id
		));
		sdpa.add(new StudyDesignPopulationAssignment__c(populationstudyassignment__c = psa[1].id,
				clinicaldesignstudyassignment__c = cdsa[1].id
		));
		insert sdpa;
		
		Arm__c a = new Arm__c(Description__c = 'The Arm', ClinicalDesign__c = cd.id);
		insert a;
		
		list<GroupArmAssignment__c> gaa = new list<GroupArmAssignment__c>();
		gaa.add(new GroupArmAssignment__c(name = 'anull', numberofsubjects__c = 0, StudyDesignPopulationAssignment__c = sdpa[0].id, arm__c = a.id));
		gaa.add(new GroupArmAssignment__c(name = 'a', numberofsubjects__c = 1, StudyDesignPopulationAssignment__c = sdpa[0].id, arm__c = a.id));
		gaa.add(new GroupArmAssignment__c(name = 'b', numberofsubjects__c = 2, StudyDesignPopulationAssignment__c = sdpa[1].id, arm__c = a.id));
		gaa.add(new GroupArmAssignment__c(name = 'a', numberofsubjects__c = 3, StudyDesignPopulationAssignment__c = sdpa[1].id, arm__c = a.id));
		insert gaa;
		
		Set<ID> armIDs = new Set<ID>();
		armIDs.add(a.id);
		
		list<BDT_FlowchartServiceTotal.ArmGroupDetailsWrapper> result = BDT_FlowchartServiceTotal.determineArmData(armIDs);
		
		system.assertEquals('popA',result[0].populationName);
		system.assertEquals(1,result[0].groupSize);
		system.assertEquals('a',result[0].groupName);
		
		system.assertEquals('popB',result[1].populationName);
		system.assertEquals(3,result[1].groupSize);
		system.assertEquals('a',result[1].groupName);

		system.assertEquals('popB',result[2].populationName);
		system.assertEquals(2,result[2].groupSize);
		system.assertEquals('b',result[2].groupName);
		
		system.debug(result);
		
	}
	
	static testMethod void determineFlowchartDataTest() {
		// Create some necessary data
		// Create projectservice records
		ProjectService__c projectService =new ProjectService__c();
		insert projectService;
		// Create a clinical design
		ClinicalDesign__c design = new ClinicalDesign__c(Design_Name__c='Dummy design');
		insert design;
		// Create flowchart records
		List<Flowcharts__c> fcList = new List<Flowcharts__c>();
		fcList.add(new Flowcharts__c(Description__c='FC A', ClinicalDesign__c=design.Id));
		fcList.add(new Flowcharts__c(Description__c='FC B', ClinicalDesign__c=design.Id));
		fcList.add(new Flowcharts__c(Description__c='FC C', ClinicalDesign__c=design.Id));
		fcList.add(new Flowcharts__c(Description__c='FC D', ClinicalDesign__c=design.Id));
		insert fcList;
		// Create ProjectServiceTimePoint records
		List<ProjectServiceTimePoint__c> pstpList = new List<ProjectServiceTimePoint__c>();
		pstpList.add(new ProjectServiceTimePoint__c(Flowchart__c=fcList[0].Id, ProjectService__c=projectService.Id, listOfTimeMinutes__c='10', TimePointCount__c=1) );
		pstpList.add(new ProjectServiceTimePoint__c(Flowchart__c=fcList[1].Id, ProjectService__c=projectService.Id, listOfTimeMinutes__c='10:20', TimePointCount__c=2) );
		pstpList.add(new ProjectServiceTimePoint__c(Flowchart__c=fcList[2].Id, ProjectService__c=projectService.Id, TimePointCount__c=0) );
		pstpList.add(new ProjectServiceTimePoint__c(Flowchart__c=fcList[3].Id, ProjectService__c=projectService.Id, listOfTimeMinutes__c='10', TimePointCount__c=1) );
		insert pstpList;

		// Create a set with the first three flowcharts
		Set<Id> flowchartIds = new Set<Id>();
		for(Integer i=0; i<3; i++) {
			flowchartIds.add(fcList[i].Id);
		}

		// Check the methods (Expected two records as we query for the first three flowcharts and the third has 0 timepointcoun)
		List<BDT_FlowchartServiceTotal.FlowchartDataWrapper> result = BDT_FlowchartServiceTotal.determineFlowchartData(flowchartIds);
		system.assertEquals(result.size(), 2);
	}
	
	static testMethod void createBaseJSONtest() {

		// create fake data
		
		list<BDT_FlowchartServiceTotal.ArmGroupDetailsWrapper> armDetailList = new list<BDT_FlowchartServiceTotal.ArmGroupDetailsWrapper>();
		BDT_FlowchartServiceTotal.ArmGroupDetailsWrapper a;
		a = new BDT_FlowchartServiceTotal.ArmGroupDetailsWrapper();
		a.populationName = 'Healthy_a';
		a.groupName = 'A';
		a.groupSize = 10;
		a.studyId = null;
		a.armId = null;
		a.armName = 'ARM';
		armDetailList.add(a);
		
		a = new BDT_FlowchartServiceTotal.ArmGroupDetailsWrapper();
		a.populationName = 'Healthy_b';
		a.groupName = 'B';
		a.groupSize = 5;
		a.studyId = null;
		a.armId = null;
		a.armName = 'ARM';
		armDetailList.add(a);
		
		a = new BDT_FlowchartServiceTotal.ArmGroupDetailsWrapper();
		a.populationName = 'Patient_b';
		a.groupName = 'B';
		a.groupSize = 5;
		a.studyId = null;
		a.armId = null;
		a.armName = 'ARM';
		armDetailList.add(a);
		
		list<BDT_FlowchartServiceTotal.FlowchartDataWrapper> flowchartDetailList = new list<BDT_FlowchartServiceTotal.FlowchartDataWrapper>();
		BDT_FlowchartServiceTotal.FlowchartDataWrapper b;
		b = new BDT_FlowchartServiceTotal.FlowchartDataWrapper();
		b.flowchartId = null;
		b.flowchartName = 'M';
		b.designName = 'Design';
		b.projectServiceId = null;
		b.timePointCount = 10;
		flowchartDetailList.add(b);
		
		b = new BDT_FlowchartServiceTotal.FlowchartDataWrapper();
		b.flowchartId = null;
		b.flowchartName = 'O';
		b.designName = 'Design';
		b.projectServiceId = null;
		b.timePointCount = 2;
		flowchartDetailList.add(b);
		
		b = new BDT_FlowchartServiceTotal.FlowchartDataWrapper();
		b.flowchartId = null;
		b.flowchartName = 'N';
		b.designName = 'Design';
		b.projectServiceId = null;
		b.timePointCount = 10;
		flowchartDetailList.add(b);
		
		list<BDT_FlowchartServiceTotal.BaseJsonWrapper> result = BDT_FlowchartServiceTotal.createBaseJSON (armDetailList,flowchartDetailList,new Map<String,String>());
		
		system.assertEquals(9,result.size());
		
		system.assertEquals(100,result[0].details.totalunits);
		system.assertEquals(50,result[3].details.totalunits);
		system.assertEquals(10,result[7].details.totalunits);
		
	}
	
	static testMethod void createListOfStudyVolunteerWrapperTest() {
		
		// create dummy ID
		list<account> accList = new list<account>();
		accList.add(new account(name='dummy'));
		accList.add(new account(name='dummy'));
		insert accList;
		
		list<BDT_FlowchartServiceTotal.BaseJsonWrapper> sourceDataList = new list<BDT_FlowchartServiceTotal.BaseJsonWrapper>();
		
		BDT_FlowchartServiceTotal.BaseJsonWrapper bjw;
		bjw = new BDT_FlowchartServiceTotal.BaseJsonWrapper();
		bjw.armId = accList[0].Id;
		bjw.flowchartId = accList[0].Id;
		bjw.studyId = accList[0].Id;
		bjw.projectServiceId = accList[0].Id;
		bjw.details = new BDT_FlowchartServiceTotal.SVWrapper();
		sourceDataList.add(bjw);
		
		bjw = new BDT_FlowchartServiceTotal.BaseJsonWrapper();
		bjw.armId = accList[0].Id;
		bjw.flowchartId = accList[1].Id;
		bjw.studyId = accList[0].Id;
		bjw.projectServiceId = accList[0].Id;
		bjw.details = new BDT_FlowchartServiceTotal.SVWrapper();
		sourceDataList.add(bjw);
		
		bjw = new BDT_FlowchartServiceTotal.BaseJsonWrapper();
		bjw.armId = accList[0].Id;
		bjw.flowchartId = accList[0].Id;
		bjw.studyId = accList[0].Id;
		bjw.projectServiceId = accList[0].Id;
		bjw.details = new BDT_FlowchartServiceTotal.SVWrapper();
		sourceDataList.add(bjw);
		
		bjw = new BDT_FlowchartServiceTotal.BaseJsonWrapper();
		bjw.armId = accList[0].Id;
		bjw.flowchartId = accList[0].Id;
		bjw.studyId = accList[0].Id;
		bjw.projectServiceId = accList[0].Id;
		bjw.details = new BDT_FlowchartServiceTotal.SVWrapper();
		sourceDataList.add(bjw);
		
		//map<string,list<BaseJsonWrapper>> result = BDT_FlowchartServiceTotal.createMapFromBaseJsonWrapper (sourceDataList);
		map<string,map<id,map<id,list<BDT_FlowchartServiceTotal.SVWrapper>>>> result = BDT_FlowchartServiceTotal.createMapOfStudyVolunteerWrapper (sourceDataList);
		
		system.assertEquals(3,result.get(accList[0].Id + ':' + accList[0].Id).get(accList[0].Id).get(accList[0].Id).size());
		system.assertEquals(1,result.get(accList[0].Id + ':' + accList[1].Id).get(accList[0].Id).get(accList[0].Id).size());
		
		
	}	
	
	
}