/** The Controller of the page BDT_ApprovalRules
 * @author	Dimitrios Sgourdos
 * @version	16-Dec-2013
 */
public with sharing class BDT_ApprovalRulesController {
	
	// Global variables
	public BDT_SecuritiesService.pageSecurityWrapper 					securitiesA 				{get;set;} // Administration Approval Rules
	public  List<SelectOption>											ruleConfigSelectList		{get;set;}
	public  List<SelectOption>											serviceCategorySelectList	{get;set;}
	public  List<FinancialDocumentApprRuleService.approvalRuleWrapper>	rulesWrapperList			{get;set;}
	public  String														changedRuleRowIndex			{get;set;}
	public  String														deletedRuleRowIndex			{get;set;}
	private List<FinancialDocumentApprRule__c>							rulesDeletedList;
	// Variables for transfer global Constants to page
	public  String	ProposalValueMore	{get;set;}
	public  String	TotalValueMore		{get;set;}
	public  String	DeltaMore			{get;set;}
	public  String	NumOfCurrencies		{get;set;}
	public  String	PaymentTerms		{get;set;}
	
	
	/** The constructor of the class.
	 * @author	Dimitrios Sgourdos
	 * @version	10-Dec-2013
	 */
	public BDT_ApprovalRulesController(){
		init();
		readConstants();
		readSelectionLists();
		readFinancialDocumentApprovalRuleData();
	}
	
	
	/** Initialize the global variables of the controller.
	 * @author	Dimitrios Sgourdos
	 * @version	11-Dec-2013
	 */
	private void init() {
		securitiesA 			  = new BDT_SecuritiesService.pageSecurityWrapper('Administration Approval Rules');
		ruleConfigSelectList 	  = new List<SelectOption>();
		serviceCategorySelectList = new List<SelectOption>();
		rulesWrapperList 		  = new List<FinancialDocumentApprRuleService.approvalRuleWrapper>();
		rulesDeletedList		  = new List<FinancialDocumentApprRule__c>();
	}
	
	
	/** Read the global constants for the rule configuration.
	 * @author	Dimitrios Sgourdos
	 * @version	16-Dec-2013
	 */
	private void readConstants() {
		ProposalValueMore = FinancialDocumentApprRuleDataAccessor.PROPOSAL_VALUE_MORE;
		TotalValueMore	  = FinancialDocumentApprRuleDataAccessor.TOTAL_VALUE_MORE;
		DeltaMore		  = FinancialDocumentApprRuleDataAccessor.DELTA_MORE;
		NumOfCurrencies	  = FinancialDocumentApprRuleDataAccessor.NUM_OF_CURRENCIES;
		PaymentTerms	  = FinancialDocumentApprRuleDataAccessor.PAYMENT_TERMS;
	}
	
	
	/** Initialize the selection lists used in the page.
	 * @author	Dimitrios Sgourdos
	 * @version	11-Dec-2013
	 */
	private void readSelectionLists() {
		ruleConfigSelectList = BDT_Utils.getSelectOptionsFromPickList(new FinancialDocumentApprRule__c(),
																'SelectedRule__c',
																'Please select...',
																false );
		
		List<ServiceCategory__c> tmpSrvCatList = ServiceCategoryService.getFirstLevelServiceCategories();
		serviceCategorySelectList = ServiceCategoryService.createSelectOptionsFromServiceCategories(tmpSrvCatList,
																					'Please select one or more...');
	}
	
	
	/** Read the Financial Document Approval Rule data.
	 * @author	Dimitrios Sgourdos
	 * @version	10-Dec-2013
	 */
	private void readFinancialDocumentApprovalRuleData() {
		rulesWrapperList = FinancialDocumentApprRuleService.getApprovalRuleWrapperData();
	}
	
	
	/** Add a financial document approval rule to the rules list.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Dec-2013
	 */
	public void addApprovalRule() {
		rulesWrapperList = FinancialDocumentApprRuleService.addRuleToApprovalRuleWrapperList(rulesWrapperList);
	}
	
	
	/** Clear the current financial document approval rule parameters.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Dec-2013
	 */
	public void clearParameters() {
		Integer index = Integer.valueOf(changedRuleRowIndex);
		rulesWrapperList[index] = FinancialDocumentApprRuleService.resetApprovalRuleParameters(rulesWrapperList[index]);
	}
	
	
	/** Remove a financial document approval rule from the rules list.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Dec-2013
	 */
	public void removeApprovalRule() {
		Integer index = Integer.valueOf(deletedRuleRowIndex);
		rulesDeletedList.add(rulesWrapperList[index].rule);
		rulesWrapperList = FinancialDocumentApprRuleService.removeRuleFromApprovalRuleWrapperList(rulesWrapperList,
																								index);
	}
	
	
	/** Reset the financial document approval rules.
	 * @author	Dimitrios Sgourdos
	 * @version 11-Dec-2013
	 */
	public void reset() {
		rulesDeletedList = new List<FinancialDocumentApprRule__c>();
		readFinancialDocumentApprovalRuleData();
	}
	
	
	/** Reset the financial document approval rules.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Dec-2013
	 */
	public void save() {
		// Validate and retrieve the data
		if( FinancialDocumentApprRuleService.validateApprovalRules(rulesWrapperList) ) {
			// Adjust data
			List<FinancialDocumentApprRule__c> updateList = new List<FinancialDocumentApprRule__c>();
			updateList = FinancialDocumentApprRuleService.extractApprovalRules(rulesWrapperList);
			
			// Save  approval rules
			Boolean successFlag = FinancialDocumentApprRuleService.saveApprovalRules(updateList);
			if( ! successFlag ) {
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The save was unsuccessful! Please, try again!');
				ApexPages.addMessage(msg);
				return;
			} else {
				// Delete approval rules
				successFlag = FinancialDocumentApprRuleService.deleteApprovalRules(rulesDeletedList);
				if( ! successFlag ) {
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The deletion of approval rules was unsuccessful! Please, try again!');
					ApexPages.addMessage(msg);
					return;
				}
			}
		} else {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please, fill all the fields for all the approval rules and try to save again!');
			ApexPages.addMessage(msg);
			return;
		}
		
		reset();
	}
}