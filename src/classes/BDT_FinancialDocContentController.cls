public with sharing class BDT_FinancialDocContentController {
	
	public BDT_SecuritiesService.pageSecurityWrapper 	securitiesA 		{get;set;} // Financial Documents Content
	public FinancialDocumentContent__c					finDocContent		{get;set;}
	public FinancialDocument__c 						financialDocument	{get;set;}
	public Boolean										invalidDocFlag		{get;set;}
	public Boolean										showPage 			{get;set;}
	public Boolean										showSavePage		{get;set;}
	public Boolean										showRichEditor		{get;set;}
	public Boolean										showContactDetEditor{get;set;}
	public Boolean										showPrConsidEditor	{get;set;}
	public Boolean										showStratInfoEditor	{get;set;}
	public String										richTextOption		{get;set;}
	public String										commentValue		{get;set;}
	public Boolean										completeFlag		{get;set;}
	public Boolean										previousContentComplete;
	public list<selectionChecksJson>					contentForSaving;
	public map<String, list<BDT_FinancialDocContentConfiguration.contentChapterWrapper> > contentData {get;set;}
	
	
	public BDT_FinancialDocContentController () {
		init();
		
		// Read financial Document
		String tmpFinDocId = system.currentPageReference().getParameters().get('financialDocumentID');
		financialDocument = FinancialDocumentService.getFinancialDocumentById(tmpFinDocId);
		if(financialDocument == NULL) {
			errorOnReadingFinancialDocument();
			return;
		}
		
		invalidDocFlag = (financialDocument.DocumentStatus__c == FinancialDocumentDataAccessor.DOCUMENT_STATUS_INVALID);
		previousContentComplete = financialDocument.ContentSetUpComplete__c;
		completeFlag = financialDocument.ContentSetUpComplete__c;
		
		buildContent();
	}
	
	
	/**	Initialize the global variables.
	 * @author   Dimitrios Sgourdos
	 * @version  11-Feb-2014
	 */
	private void init() {
		securitiesA		 = new BDT_SecuritiesService.pageSecurityWrapper('Financial Documents Content');
		showPage		 = true;
		showSavePage	 = false;
		contentForSaving = new list<selectionChecksJson>();
	}
	
	
	/**	Give an error message in case the financial document is not found.
	 * @author   Dimitrios Sgourdos
	 * @version  11-Feb-2014
	 */
	private void errorOnReadingFinancialDocument() {
		showPage = false;
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected financial document.Please, select a document and reload the page.');
		ApexPages.addMessage(msg);
	}
	
	
	/**	Build the content for the selected financial document.
	 * @author   Dimitrios Sgourdos
	 * @version  11-Feb-2014
	 */
	private void buildContent(){
		// Read Content Configuration
		BDT_FinancialDocContentConfiguration c = new BDT_FinancialDocContentConfiguration();
		contentData = c.content;
		
		// Read Financial Document Content
		finDocContent = BDT_FinancialDocumentContentService.getContentByDocumentId(financialDocument.Id);
		if(finDocContent == NULL) {
			finDocContent = new FinancialDocumentContent__c();
			finDocContent.FinancialDocument__c = financialDocument.Id;
		}
		
		// Assign Content to the Content Configuration
		assignContentToConfiguration();
	}
	
	
	/**	Assign Content to the Content Configuration.
	 * @author   Dimitrios Sgourdos
	 * @version  11-Feb-2014
	 */
	private void assignContentToConfiguration() {
		if( String.isNotBlank(finDocContent.SelectCheckJSON__c) ) {
			Map<String, selectionChecksJson> tmpContentMap  = new Map<String, selectionChecksJson>();
			List<selectionChecksJson> tmpJSONList = new List<selectionChecksJson>();
			
			// Assign all the options to a map with key the code of the option
			tmpJSONList = (List<selectionChecksJson>) JSON.deserialize(finDocContent.SelectCheckJSON__c, List<selectionChecksJson>.class);
			for(selectionChecksJson tmpItem : tmpJSONList) {
				tmpContentMap.put(tmpItem.checkCode, tmpItem);
			}
			
			readSelectedValuesToConfiguration(tmpContentMap);
		}
	}
	
	
	public void readValuesPerTag(String tagName, Map<String, selectionChecksJson> selectedContent) {
		Attachment       	tmpAttach;
		List<Attachment> 	tmpAttachList;
		selectionChecksJson tmpObject;
		String 				tmpString;
		// Read the Configuration
		list<BDT_FinancialDocContentConfiguration.contentChapterWrapper> tmpContentTMPList = new list<BDT_FinancialDocContentConfiguration.contentChapterWrapper>();
		tmpContentTMPList = contentData.get(tagName);
		for(BDT_FinancialDocContentConfiguration.contentChapterWrapper tmpItem : tmpContentTMPList) {
			// Read the general check box
			tmpObject = new selectionChecksJson();
			tmpObject = selectedContent.get(tmpItem.ChapterCode);
			tmpItem.selected = (tmpObject != null)? tmpObject.checkFlag : tmpItem.selected;
			// Read the interior check boxes
			for(BDT_FinancialDocContentConfiguration.contentSubChapterWrapper tmpSubItem : tmpItem.subContent) {
				for(BDT_FinancialDocContentConfiguration.contentItemWrapper tmpCheckItem : tmpSubItem.checkItems) {
					tmpObject = new selectionChecksJson();
					tmpObject = selectedContent.get(tmpCheckItem.itemCode);
					tmpCheckItem.selected = (tmpObject != null)? tmpObject.checkFlag : tmpCheckItem.selected;
					if(tmpCheckItem.inputReqChk) {
						tmpCheckItem.itemInput.defaultText = (tmpObject != null)? tmpObject.inputValue : tmpCheckItem.itemInput.defaultText;
					}
				}
			}	
		}
	}
	
	public void readSelectedValuesToConfiguration(Map<String, selectionChecksJson> selectedContent) {
		if( ! selectedContent.isEmpty() ) {
			readValuesPerTag('Front Page and Summary', selectedContent);
			readValuesPerTag('General', selectedContent);
			readValuesPerTag('Project Info (general)', selectedContent);
			readValuesPerTag('Project Details', selectedContent);
			readValuesPerTag('Financial Info', selectedContent);
			readValuesPerTag('Appendix', selectedContent);
		}
	}
	
	
	public void writeValuesPerTag(String tagName) {
		selectionChecksJson tmpObject;
		list<BDT_FinancialDocContentConfiguration.contentChapterWrapper> tmpContentTMPList = new list<BDT_FinancialDocContentConfiguration.contentChapterWrapper>();
		tmpContentTMPList = contentData.get(tagName);
		for(BDT_FinancialDocContentConfiguration.contentChapterWrapper tmpItem : tmpContentTMPList) {
			// Read the general check box
			tmpObject = new selectionChecksJson();
			tmpObject.checkCode  = tmpItem.ChapterCode;
			tmpObject.checkFlag  = tmpItem.selected;
			tmpObject.inputValue = '';
			contentForSaving.add(tmpObject);
			// Read the interior check boxes
			for(BDT_FinancialDocContentConfiguration.contentSubChapterWrapper tmpSubItem : tmpItem.subContent) {
				for(BDT_FinancialDocContentConfiguration.contentItemWrapper tmpCheckItem : tmpSubItem.checkItems) {
					tmpObject = new selectionChecksJson();
					tmpObject.checkCode  = tmpCheckItem.itemCode;
					tmpObject.checkflag  = tmpCheckItem.selected;
					tmpObject.inputValue = (tmpCheckItem.inputReqChk)? tmpCheckItem.itemInput.defaultText : '';
					contentForSaving.add(tmpObject);
				}
			}
		}
	}

	public void writeValuesFromConfiguration() {
		writeValuesPerTag('Front Page and Summary');
		writeValuesPerTag('General');
		writeValuesPerTag('Project Info (general)');
		writeValuesPerTag('Project Details');
		writeValuesPerTag('Financial Info');
		writeValuesPerTag('Appendix');
	}
	
	
	/**	Close the rich editor and return back to the content options.
	 * @author   Dimitrios Sgourdos
	 * @version  11-Feb-2014
	 */
	public void closeRichEditors() {
		showRichEditor		 = false;
		showContactDetEditor = false;
		showPrConsidEditor	 = false;
		showStratInfoEditor	 = false;
		showPage = true;
	}
	
	
	/**	Open the rich editor for Strategic Information.
	 * @author   Dimitrios Sgourdos
	 * @version  12-Feb-2014
	 */
	public void openStrategicInfoEditor() {
		showPage			= false;
		showRichEditor		= true;
		showStratInfoEditor = true;
		richTextOption		= 'Strategic Information';
	}
	
	
	/**	Open the rich editor for Contact details.
	 * @author   Dimitrios Sgourdos
	 * @version  12-Feb-2014
	 */
	public void openContactDetailsEditor() {
		showPage			 = false;
		showRichEditor		 = true;
		showContactDetEditor = true;
		richTextOption		 = 'Contact Details';
	}
	
	
	/**	Open the rich editor for Project Considerations.
	 * @author   Dimitrios Sgourdos
	 * @version  12-Feb-2014
	 */
	public void openProjectConsiderationsEditor() {
		showPage		   = false;
		showRichEditor	   = true;
		showPrConsidEditor = true;
		richTextOption	   = 'Project Considerations';
	}
	
	
	
	public void actualSave() {
		try {
			financialDocument.ContentSetUpComplete__c = completeFlag;
			// Read and save the selections
			writeValuesFromConfiguration();
			finDocContent.SelectCheckJSON__c = JSON.serialize(contentForSaving);
			upsert finDocContent;
			// Update the FinancialDocument, if needed
			if( previousContentComplete != financialDocument.ContentSetUpComplete__c) {
				update financialDocument;
			}
			showPage     = false;
			showSavePage = false;
		} catch (Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to save the changes. Please, reload the page and try again.');
			ApexPages.addMessage(msg);
		}
	}
	
	
	public void commentSave() {
		if( commentValue!=null && commentValue!='' ) {
			// Update the history of the financial document
			String changeDescription = 'Updating financial document Content';
			String totalValueString = '';
			if(financialDocument.TotalValue__c != null) {
				totalValueString = financialDocument.TotalValue__c;
			} 
			FinancialDocumentHistoryService.saveHistoryObject(financialDocument.id, commentValue, changeDescription, totalValueString, 'Content');
			actualSave();
		} else {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'A comment is mandatory. Please, enter a comment.');
			ApexPages.addMessage(msg);
		}
	}
	
	
	public void save() {
		if(completeFlag) {
			commentValue = '';
			showPage 	 = false;
			showSavePage = true;
		} else {
			actualSave();
			showPage 	 = false;
		}
	}
	
	
	public void closeComment() {
		showPage 	 = true;
		showSavePage = false;
	}
	
	
	public void closePage() {
		showpage = false;
	}
	
	
	public class selectionChecksJson {
		public String 	checkCode	{get;set;}
		public Boolean 	checkFlag	{get;set;}
		public String 	inputValue	{get;set;}
	}
}