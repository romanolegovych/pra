/** Implements the Selector Layer of the object FinancialDocumentApproval__c
 * @author	Dimitrios Sgourdos
 * @version	17-Jan-2014
 */
public with sharing class FinancialDocumentApprovalDataAccessor {
	
	// Global Constants
	public static final String	APPROVAL_APPROVED = 'Approved';
	public static final String	APPROVAL_REJECTED = 'Rejected';
	public static final String	APPROVAL_CREATOR_REASON = 'Creator';
	public static final String	RELEASED_APPROVAL_COMMENT = 'Released for approval';
	public static final String	RECALCULATE_RULES_COMMENT = 'Recalculated preset rules';
	public static final String	RESET_APPROVALS_COMMENT = 'Reset approvals';
	
	
	/** Object definition for fields used in application for FinancialDocumentApproval
	 * @author	Dimitrios Sgourdos
	 * @version 06-Jan-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('FinancialDocumentApproval__c');
	}
	
	
	/** Object definition for fields used in application for FinancialDocumentApproval with the parameter referenceName 
	 *	as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 08-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'AdjustmentsNotification__c,';
		result += referenceName + 'ApprovalDate__c,';
		result += referenceName + 'ApprovalNotes__c,';
		result += referenceName + 'ApproverEmail__c,';
		result += referenceName + 'ApproverJobCode__c,';
		result += referenceName + 'ApproverLocationCode__c,';
		result += referenceName + 'ApproverName__c,';
		result += referenceName + 'DeadLineDate__c,';
		result += referenceName + 'FinancialDocument__c,';
		result += referenceName + 'FinancialDocumentApprRule__c,';
		result += referenceName + 'IsAssignedToApprRule__c,';
		result += referenceName + 'RuleDescription__c,';
		result += referenceName + 'SequenceNumber__c,';
		result += referenceName + 'Status__c';
		return result;
	}
	
	
	/** Retrieve the list of Financial Document Approvals that meets the given criteria.
	 * @author	Dimitrios Sgourdos
	 * @version 17-Jan-2014
	 * @param	whereClause			The criteria that the approvals must meet
	 * @param	orderByField		The field that the approvals will be ordered by
	 * @return	The list of Financial Document Approvals.
	 */
	public static List<FinancialDocumentApproval__c> getFinancialDocumentApprovals(String whereClause,
																				String orderByField) {
		// Query
		String query = String.format(
								'SELECT {0} ' +
								'FROM FinancialDocumentApproval__c ' +
								'WHERE {1} ' +
								'ORDER BY {2}',
								new List<String> {
									getSObjectFieldString(),
									whereClause,
									orderByField
								}
							);
		
		return (List<FinancialDocumentApproval__c>) Database.query(query);
	}
	
	
	/** Retrieve the list of Employees that are known as plenbox users.
	 * @author	Dimitrios Sgourdos
	 * @version 10-Jan-2014
	 * @param	plenboxUsers		The known plenbox users
	 * @param	orderByField		The field that the employees will be ordered by
	 * @return	The list of Employees.
	 */
	public static List<Employee_Details__c> getPlenboxEmployees(List<User> plenboxUsers, String orderByField) {
		// Read only the employees that have e-mail similar with one of the plenbox users
		Set<String> plenboxEmailsSet = BDT_Utils.getStringSetFromList(plenboxUsers, 'Email');
		
		String query = String.format(
						'SELECT Id, First_Name__c, Last_Name__c, Email_Address__c, Job_Class_Desc__c, Location_Code__c ' +
						'FROM Employee_Details__c ' +
						'WHERE Email_Address__c in :plenboxEmailsSet ' +
						'ORDER BY {0}',
						new List<String> {
							orderByField
						}
					);
		
		return (List<Employee_Details__c>) Database.query(query);
	}
	
	
	/** Finds an employee with the given credentials or in case the employee doesn't exist initialize a new one with
	 *	the given credentials.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Jan-2014
	 * @param	empfirstName		The first name of the employee
	 * @param	empLastName			The last name of the employee
	 * @param	empEmail			The email of the employee
	 * @return	The found or the new Employee with the given credentials.
	 */
	public static Employee_Details__c getEmployeeByNameAndEmail(String empfirstName, String empLastName, String empEmail){
		// Find the employee or initialize a new one
		Employee_Details__c employee = new Employee_Details__c();
		
		try{
			employee = [SELECT	Id, First_Name__c, Last_Name__c, Email_Address__c, Job_Class_Desc__c, Location_Code__c
						FROM	Employee_Details__c
						WHERE	First_Name__c =: empfirstName
						AND		Last_Name__c = : empLastName
						AND		Email_Address__c =: empEmail
						LIMIT	1];
		} catch (Exception e) {
			employee = new Employee_Details__c(First_Name__c = empfirstName,
											Last_Name__c = empLastName,
											Email_Address__c = empEmail);
		}
		
		return employee;
	}
}