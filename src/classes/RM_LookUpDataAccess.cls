public with sharing class RM_LookUpDataAccess {

    public RM_LookUpDataAccess(){        
    } 
     public static List<SelectOption> getStatus(Boolean bBlank){
         transient set<String> setItems=new set<string>();
         transient List<SelectOption> options = new List<SelectOption>();
         if(bBlank)
                 options.add(new SelectOption('',''));
         for(Employee_Status__c temp:[select Status_Desc__c from Employee_Status__c where status_desc__c != null and status_desc__c != 'Terminated' order by Status_Desc__c asc])
         if(!setItems.contains(temp.Status_Desc__c)){
                 options.add(new SelectOption(temp.Status_Desc__c,temp.Status_Desc__c));
                 setItems.add(temp.Status_Desc__c);
                 }
         return options;
    }        
     public static List<SelectOption> getBU(Boolean bBlank){
         transient set<String> setItems=new set<string>();
         transient List<SelectOption> options = new List<SelectOption>();
         if(bBlank)
                 options.add(new SelectOption('',''));
         for(PRA_Business_Unit__c temp:[select name from PRA_Business_Unit__c order by name asc])
         if(!setItems.contains(temp.name)){
                 options.add(new SelectOption(temp.name,temp.name));
                 setItems.add(temp.name);
         }
         return options;
    } 
     public static List<SelectOption> getBUinGroup(Boolean bBlank){
         transient set<String> setItems=new set<string>();
         transient List<SelectOption> options = new List<SelectOption>();
         if(bBlank)
                 options.add(new SelectOption('',''));
         for(PRA_Business_Unit__c temp:[select name from PRA_Business_Unit__c where id in (select PRA_Business_Unit__c from WFM_BU_Group_Mapping__c) order by name asc])
         if(!setItems.contains(temp.name)){
                 options.add(new SelectOption(temp.name,temp.name));
                 setItems.add(temp.name);
         }
         return options;
    }   
    public static List<SelectOption> getUserEditableBU(Boolean bBlank, Boolean bEdit, string userID){
         transient set<String> setItems=new set<string>();
         transient List<SelectOption> options = new List<SelectOption>();
         if(bBlank)
                 options.add(new SelectOption('',''));
         if (bEdit){
         	if (RM_OtherService.IsRMAdmin(userID)){
         		return getBU(bBlank);
         	}
         	else{
	         	list<string>lstGrpName = RM_DataAccessor.GetRMGroupfromUserID(userID);
	        	
		        if (lstGrpName != null){
		            list<WFM_BU_Group_Mapping__c> lstBU= RM_DataAccessor.getBUGroupMapping(lstGrpName);
		            if (lstBU.size()> 0)
		            {
		                for (WFM_BU_Group_Mapping__c bu : lstBU){
		                    if(!setItems.contains(bu.PRA_Business_Unit__r.name)){              
		                        options.add(new SelectOption(bu.PRA_Business_Unit__r.name,bu.PRA_Business_Unit__r.name));
		                        setItems.add(bu.PRA_Business_Unit__r.name);
		                    }
		                }
		            }
		        }
         	}
         }
         else{
	        return getBU(bBlank);
         }
         return options;
    }     
     public static List<SelectOption> getFuncCode(Boolean bBlank){
         transient set<String> setItems=new set<string>();
         transient List<SelectOption> options = new List<SelectOption>();
         if(bBlank)
                 options.add(new SelectOption('',''));
         for(Function_Code__c temp:[select name from Function_Code__c where Is_RM__C = true order by name asc])
         if(!setItems.contains(temp.name)){
                 options.add(new SelectOption(temp.name,temp.name));
                 setItems.add(temp.name);
         }
         return options;
    } 
     public static List<SelectOption> getBufCodeBUCode(Boolean bBlank){
         transient set<String> setItems=new set<string>();
         transient List<SelectOption> options = new List<SelectOption>();
         if(bBlank)
                 options.add(new SelectOption('',''));
         for(BUF_Code__c temp:[SELECT Business_Unit__r.name,Function_Code__r.name,Name FROM BUF_Code__c where function_code__r.Is_RM__c = true order by Business_Unit__r.name asc]){
	         if (temp.Business_Unit__r.name.length() == 3){
		         if(!setItems.contains(temp.Business_Unit__r.name)){
		                 options.add(new SelectOption(temp.Business_Unit__r.name,temp.Business_Unit__r.name));
		                 setItems.add(temp.Business_Unit__r.name);
		                 system.debug('********temp.Business_Unit__r.name,temp.Business_Unit__r.name'+temp.Business_Unit__r.name+'&&&'+temp.Business_Unit__r.name);
		         }
	         }
         }
         system.debug('list id options'+options);
         return options;
    }
    /* method By yatish*/
     public static List<SelectOption> getAllocationBUCode(Boolean bBlank){
         transient set<String> setItems=new set<string>();
         transient List<SelectOption> options = new List<SelectOption>();
         if(bBlank)
                 options.add(new SelectOption('',''));
         for(WFM_Employee_Allocations__c temp:[SELECT BU_Code__c FROM WFM_employee_Allocations__c ORDER BY BU_Code__c ASC NULLS LAST])
         if(!setItems.contains(temp.BU_Code__c)){
                 options.add(new SelectOption(temp.BU_Code__c,temp.BU_Code__c));
                 setItems.add(temp.BU_Code__c);
                 system.debug('********temp.Business_Unit__r.name,temp.Business_Unit__r.name'+temp.BU_Code__c);
         }
         system.debug('list id options'+options);
         return options;
    }     
    public static List<SelectOption> getRoles(Boolean bAssign, Boolean bClinical, Boolean bBlank){
        string strSQL = '';
        transient set<String> setItems=new set<string>();        
        transient List<SelectOption> options = new List<SelectOption>(); 
        if (bBlank)       
            options.add(new SelectOption('',''));
        if (bAssign)
            strSQL = 'select name from  Job_Class_Desc__c where Is_Assign__c = true order by name asc';
        else if (bClinical)
             strSQL = 'select name from  Job_Class_Desc__c where IsClinical__c = true order by name asc';
        else 
            strSQL = 'select name from  Job_Class_Desc__c order by name asc';
        system.debug('---------strSQL---------' + strSQL);
        for(Job_Class_Desc__c temp:database.query(strSQL))
            if(!setItems.contains(temp.name)){              
                options.add(new SelectOption(temp.name,temp.name));
               setItems.add(temp.name);
            }
        return options; 
    }
  
    /*public static List<SelectOption> getLocBUwithID(Boolean bBlank, string CountryRID){ 
        transient set<String> setItems=new set<string>();
       
         transient List<SelectOption> options = new List<SelectOption>();
         
            if (bBlank)       
                options.add(new SelectOption('',''));
            if (CountryRID == ''){
	            for(Business_Unit__c temp:[select id, name from Business_Unit__c order by name asc])
	            if(!setItems.contains(temp.name)){
	               options.add(new SelectOption(temp.id,temp.name));
	               setItems.add(temp.name);
	            } 
            }
            else{
            	for(Country_BusinessUnit__c temp:[select Business_Unit__c, Business_Unit_Name__c from Country_BusinessUnit__c where Country__c = :CountryRID order by name asc])
	            if(!setItems.contains(temp.Business_Unit_Name__c)){
	               options.add(new SelectOption(temp.Business_Unit__c,temp.Business_Unit_Name__c));
	               setItems.add(temp.Business_Unit_Name__c);
	            } 
            }
            
        return options;
    }*/
    public static List<SelectOption> getAllLocBU(Boolean bBlank){ 
        transient set<String> setItems=new set<string>();
       
         transient List<SelectOption> options = new List<SelectOption>();
         list<Business_Unit__c> lstBU = new list<Business_Unit__c>();
            if (bBlank)       
                options.add(new SelectOption('',''));
            
	            lstBU = [select name from Business_Unit__c order by name asc];
	            
            
            if (lstBU.size() > 0){
            	for (Business_Unit__c temp :lstBU){
            		if (temp.name.length() == 3){
			            if(!setItems.contains(temp.name)){
			               options.add(new SelectOption(temp.name,temp.name));
			               setItems.add(temp.name);
			            } 
            		}
            	}
            }
            
        return options;
    }
     public static List<SelectOption> getLocBU(Boolean bBlank, string Country){ 
        transient set<String> setItems=new set<string>();
       
         transient List<SelectOption> options = new List<SelectOption>();
         list<Country_BusinessUnit__c> lstCountryBU = new list<Country_BusinessUnit__c>();
            if (bBlank)       
                options.add(new SelectOption('',''));
            if (Country == null ||Country == ''){
	            lstCountryBU = [select Business_Unit__c, Business_Unit_Name__c from Country_BusinessUnit__c order by Business_Unit_Name__c asc];
	            
            }
            else{
            	lstCountryBU = [select Business_Unit__c, Business_Unit_Name__c from Country_BusinessUnit__c where Country_Name__c = :Country order by Business_Unit_Name__c asc];
            }
            if (lstCountryBU.size() > 0){
            	for (Country_BusinessUnit__c temp :lstCountryBU)
	            if(!setItems.contains(temp.Business_Unit_Name__c)){
	               options.add(new SelectOption(temp.Business_Unit_Name__c,temp.Business_Unit_Name__c));
	               setItems.add(temp.Business_Unit_Name__c);
	            } 
            }
            
        return options;
    }
    public static List<SelectOption> getLocBUwithListContry(Boolean bBlank, list<string> lstCountry){ 
        transient set<String> setItems=new set<string>();
       
         transient List<SelectOption> options = new List<SelectOption>();
         list<Country_BusinessUnit__c> lstCountryBU = new list<Country_BusinessUnit__c>();
            if (bBlank)       
                options.add(new SelectOption('',''));
            if (lstCountry == null )
	            lstCountryBU = [select Business_Unit__c, Business_Unit_Name__c from Country_BusinessUnit__c order by name asc];
	            
            
            else
            	lstCountryBU = [select Business_Unit__c, Business_Unit_Name__c from Country_BusinessUnit__c where Country_Name__c in :lstCountry order by Business_Unit_Name__c asc];
         	if (lstCountryBU.size() > 0){
            	for (Country_BusinessUnit__c temp :lstCountryBU){ 
		            if(!setItems.contains(temp.Business_Unit_Name__c)){
		               options.add(new SelectOption(temp.Business_Unit_Name__c,temp.Business_Unit_Name__c));
		               setItems.add(temp.Business_Unit_Name__c);
		            } 
            	}
            }
            
        return options;
    }
    public static List<SelectOption> getCountry(Boolean bBlank){ 
        transient set<String> setItems=new set<string>();
       
         transient List<SelectOption> options = new List<SelectOption>();
         
            if (bBlank)       
                options.add(new SelectOption('',''));
            for(Country__c temp:[select name from Country__c order by name asc])
            if(!setItems.contains(temp.name)){
               options.add(new SelectOption(temp.name,temp.name));
               setItems.add(temp.name);
            } 
        return options;
    }
    public static List<SelectOption> getCountrywithID(Boolean bBlank, string BULocRID){ 
        transient set<String> setItems=new set<string>();
       
         transient List<SelectOption> options = new List<SelectOption>();
         
            if (bBlank)       
                options.add(new SelectOption('',''));
            if (BULocRID == ''){
	            for(Country__c temp:[select id, name from Country__c order by name asc])
	            if(!setItems.contains(temp.name)){
	               options.add(new SelectOption(temp.id,temp.name));
	               setItems.add(temp.name);
	            } 
            }
            else{
            	for(Country_BusinessUnit__c temp:[select Country__c, Country_Name__c from Country_BusinessUnit__c where Business_Unit__c = :BULocRID order by name asc])
	            if(!setItems.contains(temp.Country_Name__c)){
	               options.add(new SelectOption(temp.Country__c,temp.Country_Name__c));
	               setItems.add(temp.Country_Name__c);
	            } 
            }
            
        return options;
    }
    public static List<SelectOption> getCountry(Boolean bBlank, string BULoc){ 
        transient set<String> setItems=new set<string>();
       
         transient List<SelectOption> options = new List<SelectOption>();
         
            if (bBlank)       
                options.add(new SelectOption('',''));
            if (BULoc == null || BULoc == ''){
	            for(Country__c temp:[select id, name from Country__c order by name asc])
	            if(!setItems.contains(temp.name)){
	               options.add(new SelectOption(temp.name,temp.name));
	               setItems.add(temp.name);
	            } 
            }
            else{
            	for(Country_BusinessUnit__c temp:[select Country__c, Country_Name__c from Country_BusinessUnit__c where Business_Unit_Name__c = :BULoc order by Country_Name__c asc])
	            if(!setItems.contains(temp.Country_Name__c)){
	               options.add(new SelectOption(temp.Country_Name__c,temp.Country_Name__c));
	               setItems.add(temp.Country_Name__c);
	            } 
            }
            
        return options;
    }
    public static List<SelectOption> getRMRegion(Boolean bBlank){ 
        transient set<String> setItems=new set<string>();
       
         transient List<SelectOption> options = new List<SelectOption>();
         
            if (bBlank)       
                options.add(new SelectOption('',''));
            for(Country__c temp:[select RM_Region__c from Country__c order by RM_Region__c asc]){
                if (temp.RM_Region__c != null && temp.RM_Region__c != ''){
                    if(!setItems.contains(temp.RM_Region__c)){
                       options.add(new SelectOption(temp.RM_Region__c,temp.RM_Region__c));
                       setItems.add(temp.RM_Region__c);
                    } 
                }
            }
        return options;
    }
    public static List<SelectOption> getCountryByRegion(Boolean bBlank, list<string> lstRegion){ 
        transient set<String> setItems=new set<string>();
       
         transient List<SelectOption> options = new List<SelectOption>();
         
            if (bBlank)       
                options.add(new SelectOption('',''));
            if (lstRegion != null && lstRegion.size() > 0 && lstRegion[0]!= ''){
                for(Country__c temp:[select name from Country__c where RM_Region__c in :lstRegion order by name asc]){
                    if (temp.name != null && temp.name != ''){
                        if(!setItems.contains(temp.name)){
                           options.add(new SelectOption(temp.name,temp.name));
                           setItems.add(temp.name);
                        } 
                    }
                }
            }
            else{
                 return getCountry(true);
            }
        return options;
    }
    public static List<SelectOption> getSubRegion(Boolean bBlank){ 
        transient set<String> setItems=new set<string>();
       
         transient List<SelectOption> options = new List<SelectOption>();
         
            if (bBlank)       
                options.add(new SelectOption('',''));
            for(WFM_Sub_Region__c temp:[select Sub_Region__c from WFM_Sub_Region__c order by Sub_Region__c asc])
            if(!setItems.contains(temp.Sub_Region__c)){
               options.add(new SelectOption(temp.Sub_Region__c,temp.Sub_Region__c));
               setItems.add(temp.Sub_Region__c);
            } 
        return options;
    }
     public static List<SelectOption> getStateBySubRegion(Boolean bBlank, list<string> lstSubRegion){ 
        transient set<String> setItems=new set<string>();
       
         transient List<SelectOption> options = new List<SelectOption>();
         
            if (bBlank)       
                options.add(new SelectOption('',''));
            if (lstSubRegion != null && lstSubRegion.size() > 0 && lstSubRegion[0] != ''){
                for(WFM_Sub_Region__c temp:[select State_Provience__c from WFM_Sub_Region__c where Sub_Region__c in :lstSubRegion  order by State_Provience__c asc])
                if(!setItems.contains(temp.State_Provience__c)){
                   options.add(new SelectOption(temp.State_Provience__c,temp.State_Provience__c));
                   setItems.add(temp.State_Provience__c);
                } 
            }
            else{
                for(WFM_Sub_Region__c temp:[select State_Provience__c from WFM_Sub_Region__c  order by State_Provience__c asc])
                if(!setItems.contains(temp.State_Provience__c)){
                   options.add(new SelectOption(temp.State_Provience__c,temp.State_Provience__c));
                   setItems.add(temp.State_Provience__c);
                } 
            }
            
        return options;
    }
    public static List<SelectOption> getTheropeticAreas(Boolean bBlank){
        transient set<String> setItems=new set<string>();
        
        transient List<SelectOption> options = new List<SelectOption>();
        
        if (bBlank)
            options.add(new SelectOption('',''));
        for(WFM_Therapeutic__c temp:[select name from  WFM_Therapeutic__c order by name asc])
        if(!setItems.contains(temp.name)){
           options.add(new SelectOption((temp.name).trim(),(temp.name).trim()));
           setItems.add(temp.name);
        }
        return options;
    } 
    public static list<SelectOption> getIndicationByTheropeticArea(string strTheropeticArea){
        list <SelectOption>Indications=new list<SelectOption>();
        for(WFM_Therapeutic__c taObject:[select Name,Indication_Group__c,Primary_Indication__c from  WFM_Therapeutic__c where Name!=null and Primary_Indication__c!=null and Indication_Group__c!=null and Name=:strTheropeticArea order by Indication_Group__c, Primary_Indication__c]){
                Indications.add(new SelectOption(taObject.Indication_Group__c+'/'+taObject.Primary_Indication__c,taObject.Indication_Group__c+'/'+taObject.Primary_Indication__c));
        }
        return Indications;
    }
    public static List<SelectOption> getPhase(Boolean bBlank){
        transient set<String> setItems=new set<string>();
        
        transient List<SelectOption> options = new List<SelectOption>();
        
           if (bBlank)
                options.add(new SelectOption('',''));
            for(WFM_Phase__c temp:[select name from  WFM_Phase__c order by name asc])
            if(!setItems.contains(temp.name)){
               options.add(new SelectOption((temp.name).trim(),(temp.name).trim()));
               setItems.add(temp.name);
            }
        return options;
    } 
    public static List<SelectOption>getLocation(Boolean bBlank){
        transient set<String> setItems=new set<string>();
       
         transient List<SelectOption> options = new List<SelectOption>();
         if (bBlank)
            options.add(new SelectOption('',''));
        for(WFM_Location__c temp:[select name from WFM_Location__c  order by name asc])
        if(!setItems.contains(temp.name)){
           options.add(new SelectOption(temp.name,temp.name));
           setItems.add(temp.name);
        }
        return options;
    }
    public static List<SelectOption>GetLangauges(Boolean bBlank){
        transient set<String> setItems=new set<string>();       
      
        transient List<SelectOption> options = new List<SelectOption>();
        if (bBlank) 
            options.add(new SelectOption('',''));
        for(WFM_LANGUAGE__c temp:[select Name from WFM_LANGUAGE__c order by Name asc])
        if(!setItems.contains(temp.Name)){
           options.add(new SelectOption(temp.Name,temp.Name));
           setItems.add(temp.Name);
        }
        return options;
    }
    public static List<SelectOption>getproficiencyItems(Boolean bBlank){
        transient List<SelectOption> options = new List<SelectOption>();
        if (bBlank)
            options.add(new SelectOption('',''));
        Schema.DescribeFieldResult pickListValues = WFM_EE_LANGUAGE__c.Level_of_Speak__c.getDescribe();
        for(Schema.PicklistEntry pickListValue:pickListValues.getPicklistValues()){
                options.add(new SelectOption(pickListValue.getLabel(),pickListValue.getValue()));
        }
        return options;
    }
    public static list<SelectOption> GetSavedFolder(string RootType){
        string userId = UserInfo.getUserId();
       // set<string> setItems = new set<string>();
        // Get root id and display Resource Folders //
        list<SelectOption> options = new list<SelectOption>();   
        list<Root_Folder__c> rootIdLookup = [select Id, name from Root_Folder__c where Root_Folder__c.Name = :RootType];
        if (rootIdLookup.size() > 0)
        {
            string rootId = string.valueof(rootIdLookup[0].Id);
            list<Sub_Folder__c> subDefaultList = RM_OtherService.GetDefaultSubFolderList(rootId, UserInfo.getUserID());
            if (subDefaultList.size() == 0){ // need make one for interface -- cannot insert at this stage since DML not allowed
                if (RootType== 'Resources')
                    options.add(new SelectOption('', 'My Resource Searches' )); 
                else if (RootType == 'Projects')
                    options.add(new SelectOption('', 'My Project Searches' ));
                else if (RootType == 'Assignments')
                    options.add(new SelectOption('', 'My Assignment Searches' ));
            }
            else{
            
                for(Sub_Folder__c temp : subDefaultList)       
                    options.add(new SelectOption(temp.id, temp.Name));
          
            }
            
            for(Sub_Folder__c temp : RM_OtherService.GetCustomSubFolderList(rootId,UserInfo.getUserID() ))     
                options.add(new SelectOption(temp.id, temp.Name));
            
            
        }
        else{
            if (RootType== 'Resources')
                options.add(new SelectOption('', 'My Resource Searches' )); 
            else if (RootType == 'Projects')
                options.add(new SelectOption('', 'My Project Searches' ));
            else if (RootType == 'Assignments')
                options.add(new SelectOption('', 'My Assignment Searches' ));
        }
        
      
    
        return options;
    }
     public static List<SelectOption>GetModificationReason(Boolean bBlank){
        transient set<String> setItems=new set<string>();       
      
        transient List<SelectOption> options = new List<SelectOption>();
        if (bBlank) 
            options.add(new SelectOption('',''));
            
        for (RM_AMRReasonLookupSetting__c temp: [SELECT lookup_Type__c,Name,Value__c FROM RM_AMRReasonLookupSetting__c WHERE lookup_Type__c = 'AMR_Reason' order by value__c])
        //for(WFM_Lookup__c temp:[select Name from WFM_Lookup__c where Lookup_Type__c='Mod_Reason' order by Display_Order__c asc])
        if(!setItems.contains(temp.Name)){
           options.add(new SelectOption(temp.Name,temp.Name));
           setItems.add(temp.Name);
        }
        return options;
    }
    public static List<SelectOption>GetRejectReason(Boolean bBlank){
        transient set<String> setItems=new set<string>();       
      
        transient List<SelectOption> options = new List<SelectOption>();
        if (bBlank) 
            options.add(new SelectOption('',''));
            
        for (RM_AMRReasonLookupSetting__c temp: [SELECT lookup_Type__c,Name,Value__c FROM RM_AMRReasonLookupSetting__c WHERE lookup_Type__c = 'Reject_Reason' order by value__c])
        //for(WFM_Lookup__c temp:[select Name from WFM_Lookup__c where Lookup_Type__c='Mod_Reason' order by Display_Order__c asc])
        if(!setItems.contains(temp.Name)){
           options.add(new SelectOption(temp.Name,temp.Name));
           setItems.add(temp.Name);
        }
        return options;
    }
     public static List<SelectOption>GetProjectCountry(string projectRID, integer iStartMonth, Integer iEndMonth){
        list <string> jobClass = RM_DataAccessor.getClinicalJobClass();
        transient set<String> setItems=new set<string>();       
      
        transient List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('All','All'));
        for (AggregateResult ar :[SELECT  employee_id__r.country_name__r.name country FROM WFM_employee_Allocations__c 
            where project_id__c = :projectRID and (Allocation_end_Date__c > :RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(iStartMonth)) or allocation_start_Date__c < :RM_Tools.GetLastDayDatefromYearMonth(RM_Tools.GetYearMonth(iEndMonth))) 
            and employee_id__r.job_class_desc__c in :jobClass
            group by employee_id__r.country_name__r.name order by employee_id__r.country_name__r.name]) 
        
           options.add(new SelectOption((string)ar.get('country'),(string)ar.get('country')));
        return options;
    }
    
    public static list<SelectOption> getViewNumberItems(Integer iTotal, Integer iIncrement ){
        List<SelectOption> options = new List<SelectOption>();
        if (iTotal > iIncrement){
            for (integer i = 0; i < Math.round(iTotal/iIncrement); i++){
                string nV = string.valueOf((i+1)*iIncrement);
                options.add(new SelectOption(nV, nV));
            }
        }
        options.add(new SelectOption('All', 'All'));
    
        return options;
    }
    public static list<SelectOption> getGroupJobClassDesc(Boolean bBlank){
        Boolean bGroup = false;
        string userId = UserInfo.getUserId();
        string profID = UserInfo.getProfileId();
        transient set<String> setItems=new set<string>();    
        List<SelectOption> options = new List<SelectOption>();
         if (bBlank) 
            options.add(new SelectOption('',''));
        Profile p = RM_DataAccessor.GetUserProfileInfo(profID);
         system.debug('---------p.name---------' + p.name);
        //need to check profile with group or not
        string strSQL = '';
        if (RM_OtherService.IsRMAdmin(UserInfo.getUserId()) ){
            
            return getRoles(false, false,  bBlank);
        }
        else{
            list<string> lstGroupName = RM_DataAccessor.GetRMGroupfromUserID(userId);  
            if (lstGroupName != null){
            
                if (lstGroupName.size() > 0){
                    
                    strSQL = 'SELECT Group_Name__c,Job_Class_Desc__r.name FROM WFM_JC_Group_Mapping__c WHERE Group_Name__c  in: lstGroupName order by Job_Class_Desc__r.name asc';
               
                
                    for(WFM_JC_Group_Mapping__c temp:database.query(strSQL))
                        if(!setItems.contains(temp.Job_Class_Desc__r.name)){              
                            options.add(new SelectOption(temp.Job_Class_Desc__r.name,temp.Job_Class_Desc__r.name));
                         setItems.add(temp.Job_Class_Desc__r.name);
                        }
                    return options;
                
                }
                else
                    return options;
            }
            else
                return options;
       }
    }
    public static list<SelectOption> getJobClassDescByBU(Boolean bBlank, list<string> lstBU){
        system.debug('---------lstBU---------' + lstBU);
        
        transient set<String> setItems=new set<string>();    
        List<SelectOption> options = new List<SelectOption>();
        if (bBlank) 
            options.add(new SelectOption('',''));
        if (lstBU.size() > 0 && lstBU[0] != ''){
            List<string> lstGroup = new List<string>();
            for (WFM_BU_Group_Mapping__c m : [select group_name__c from WFM_BU_Group_Mapping__c where PRA_Business_Unit__r.name in :lstBU]){
                lstGroup.add(m.group_name__c);
            }
            if (lstGroup.size() > 0 && lstGroup[0] != ''){
                for(WFM_JC_Group_Mapping__c temp:[SELECT Group_Name__c,Job_Class_Desc__r.name FROM WFM_JC_Group_Mapping__c WHERE Group_Name__c  in: lstGroup order by Job_Class_Desc__r.name asc])
                    if(!setItems.contains(temp.Job_Class_Desc__r.name)){              
                        options.add(new SelectOption(temp.Job_Class_Desc__r.name,temp.Job_Class_Desc__r.name));
                     setItems.add(temp.Job_Class_Desc__r.name);
                    }
                return options;
            
            }
            else
                return options;
        }
        else
        {   system.debug('---------lstBU---------' + lstBU+bBlank);
            return getRoles(false, false,  bBlank);
        }   
    }  
}