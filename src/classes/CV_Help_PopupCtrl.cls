/**
* @author Kostya Hladkyi
* @date 03/11/2015
* @description Controller for CV_Help_Popup page
*/
public with sharing class CV_Help_PopupCtrl {
	
	private static final String MENU_ID_PARAM = 'menuId';
	private static final String CHANGED_MENU_ID_PARAM = 'changedMenuId';
	
	public String menuRecordId{
		get; 
		private set{
			menuRecordId = value;
			record = CV_Utils.getHelpRecordById(value);
		} 
	}
	
	private Help_Record__c record{
		get; set;
	}
	
	public CV_Help_PopupCtrl(){
		setMenuRecordIdFromPageParameters(MENU_ID_PARAM);
	}
	
	public List<SelectOption> getHelpItems(){
		return CV_Utils.getHelpItems( CV_Utils.selectHelpRecords(false), false );
	}
	
	public String getContent(){
		return record != null ? record.Rich_Html_Content__c : '';
	}
	
	public String getHeader(){
		return record != null ? record.Label__c : '';
	}
	
	public void reloadContent(){
		setMenuRecordIdFromPageParameters(CHANGED_MENU_ID_PARAM);
	}
	
	private void setMenuRecordIdFromPageParameters(String menuIdParameterName){
		menuRecordId = ApexPages.currentPage().getParameters().get(menuIdParameterName);
	}
	
}