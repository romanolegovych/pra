/**
 * @description	Implements the test for the functions of the NCM_Utils class
 * @author		Dimitrios Sgourdos
 * @version		Created: 02-Sep-2015, Edited: 14-Sep-2015
 */
@isTest
private class NCM_UtilsTest {
	
	/**
	 * @description	Test the function createSetForDynamicQuery
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 02-Sep-2015
	*/
	static testMethod void createSetForDynamicQueryTest() {
		String errorMessage = 'Error in analyzing the given parameters';
		
		String result = NCM_Utils.createSetForDynamicQuery(new List<String>{'TestA','TestB'});
		
		system.assertEquals(true, result.contains('TestA'), errorMessage);
		system.assertEquals(true, result.contains('TestB'), errorMessage);
		system.assertEquals('\'TestA\',\'TestB\'', result, errorMessage);
	}
	
	
	/**
	 * @description	Test the function buildTestUserList
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 02-Sep-2015
	*/
	static testMethod void buildTestUserListTest() {
		List<User> results = NCM_Utils.buildTestUserList(2);
		
		String errorMessage = 'Error in creating user instances';
		
		system.assertEquals(2, results.size(), errorMessage); 
		system.assertNotEquals(NULL, results[0].Id, errorMessage);
		system.assertNotEquals(NULL, results[1].Id, errorMessage);
	}
}