/**
 * @description	Implements the test for the functions of the NCM_SrvLayer_Email class
 * @author		Dimitrios Sgourdos
 * @version		Created: 01-Oct-2015, Edited: 02-Oct-2015
 */
@isTest
private class NCM_SrvLayer_EmailTest {
	
	/**
	 * @description	Test the function getOrgWideEmailAddressIdFromAddressName
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 02-Oct-2015
	*/
	static testMethod void getOrgWideEmailAddressIdFromAddressNameTest() {
		// Retrieve one org wide email address
		List<OrgWideEmailAddress> orgMailList = [SELECT Id, Address FROM OrgWideEmailAddress LIMIT 1];
		
		String errorMEssage = 'Error in retrieving the id of the org wide email address';
		
		// Check the function with invalid parameter
		Id result = NCM_SrvLayer_Email.getOrgWideEmailAddressIdFromAddressName(
															'Invalid Exapmle getOrgWideEmailAddressIdFromAddressName');
		system.assertEquals(NULL, result, errorMessage);
		
		if(orgMailList.size() > 0) {
			result = NCM_SrvLayer_Email.getOrgWideEmailAddressIdFromAddressName(orgMailList[0].Address);
			system.assertNotEquals(NULL, result, errorMessage);
		}
	}
	
	
	/**
	 * @description	Test the function createEmailInstance
	 * @author		Jegadheesan Muthusamy
	 * @date		Created: 11-Sep-2015
	*/
	static testMethod void createEmailInstanceTest() {
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		String errorMessage = 'Error in creating e-mail instance';
		
		Messaging.SingleEmailMessage result = NCM_SrvLayer_Email.createEmailInstance(testUser.Id,
																					NULL,
																					'Test Subject',
																					'Test mail body',
																					NULL);
		
		system.assertNotEquals(result, NULL, errorMessage );
	}
	
	
	/**
	 * @description	Test the function sendEmailInstances
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015
	*/
	static testMethod void sendEmailInstancesTest() {
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		Messaging.SingleEmailMessage emailInstance = NCM_SrvLayer_Email.createEmailInstance(testUser.Id,
																							NULL,
																							'Test Subject',
																							'Test mail body',
																							NULL);
		List<Messaging.SingleEmailMessage> source = new List<Messaging.SingleEmailMessage>();
		source.add(emailInstance);
		
		NCM_SrvLayer_Email.sendEmailInstances(NULL);
		NCM_SrvLayer_Email.sendEmailInstances(source);
	}
}