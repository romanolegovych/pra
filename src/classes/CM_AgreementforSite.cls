public with sharing class CM_AgreementforSite {

   

    
    public Apttus__APTS_Agreement__c agreement{ get; set; }
    public String RecordTypeID{ get; set; }
    public String RecordType{ get; set; }
    public WFM_Protocol__c protocol { get; set; }
    public Protocol_Country__c protocolcntry{ get; set; }
    public WFM_Site_Detail__c site { get; set; }
    public ID PID{ get; set; }
    public String ObjName { get; set; }
    
    public CM_AgreementforSite(ApexPages.StandardSetController controller) {
        
        
        
        agreement = new Apttus__APTS_Agreement__c();
        PID=(ApexPages.CurrentPage().getParameters().get('id'));
        ObjName = PID.getSObjectType().getDescribe().getName();
        system.debug('--------ObjName ------'+ObjName );
        if(ObjName == 'WFM_Protocol__c'){
            RecordType = 'Template';
            RecordTypeID = [SELECT Id from RecordType WHERE Name =:RecordType ].id;  
            agreement.RecordTypeId = RecordTypeID ;  
            protocol = [select id,name,Project_ID__r.Project_Unique_Key__c,Project_ID__r.Client_ID__c
                                    from WFM_Protocol__c where id=:PID];  
            agreement.Protocol__c  = protocol.id;
            agreement.Name = protocol.Project_ID__r.Client_ID__c+ '_' + protocol.Project_ID__r.Project_Unique_Key__c+ '_'+ agreement.Contract_Type__c + '_'+RecordType ;  
            agreement.Project_id__c =  protocol.Project_ID__r.Project_Unique_Key__c;   
                                 
            
        }
        if(ObjName =='Protocol_Country__c'){
            RecordType = 'Template';
            RecordTypeID = [SELECT Id from RecordType WHERE Name =:RecordType ].id;
            protocolcntry =[select id,name,Client_Protocol__r.Project_ID__r.Project_Unique_Key__c,Client_Protocol__r.Project_ID__r.Client_ID__c
                                    from Protocol_Country__c where id=:PID]; 
            agreement.RecordTypeId = RecordTypeID ;
            agreement.Protocol_Country__c  = protocolcntry.id;
            agreement.Name = protocolcntry.Client_Protocol__r.Project_ID__r.Client_ID__c+ '_' + protocolcntry.Client_Protocol__r.Project_ID__r.Project_Unique_Key__c+ '_'+ agreement.Contract_Type__c + '_'+RecordType ;  
            agreement.Project_id__c =  protocolcntry.Client_Protocol__r.Project_ID__r.Project_Unique_Key__c;   
        }
        if(ObjName =='WFM_Site_Detail__c'){
            RecordType = 'Agreement';
            RecordTypeID = [SELECT Id from RecordType WHERE Name =:RecordType ].id;
            agreement.RecordTypeId = RecordTypeID ;
            site = [select id,name,Project_Protocol__r.Project_ID__r.Project_Unique_Key__c,Project_Protocol__r.Project_ID__r.Client_ID__c
                                    from WFM_Site_Detail__c where id=:PID];    
            system.debug('--------site ------'+site );
            agreement.site__c=site.id;
            agreement.Name = site.Project_Protocol__r.Project_ID__r.Client_ID__c+ '_' + site.Project_Protocol__r.Project_ID__r.Project_Unique_Key__c+ '_'+ agreement.Contract_Type__c + '_'+RecordType ;  
            agreement.Project_id__c =  site.Project_Protocol__r.Project_ID__r.Project_Unique_Key__c;      
        }
        
                    
        /*agreement =[select id,name,Contract_Type__c ,PRA_Status__c,Apttus__Status__c,Apttus__Status_Category__c,Protocol_Country__c,Team__c,Protocol__c,site__c,
                           Comments__c,Contract_Holder__c,RecordTypeId, Owner.FirstName, Owner.LastName,
                           Site__r.Project_Protocol__r.Project_ID__r.Project_Unique_Key__c,
                           Protocol__r.Project_ID__r.Project_Unique_Key__c,
                           Protocol_Country__r.Client_Protocol__r.Project_ID__r.Project_Unique_Key__c,
                           Sponsor__c,Study_ID__c,Project_ID__c
                    from Apttus__APTS_Agreement__c 
                    where site__c=:sid ];
           */         
         
         system.debug('--------agreement ------'+agreement+RecordTypeID  );
    }   
    
    //Method to save agrement   
    public PageReference saveAgreement(){   
        try{           
            system.debug('--------agreement ------'+agreement);
            if(ObjName =='WFM_Protocol__c'){
                agreement.Name = protocol.Project_ID__r.Client_ID__c+ '_' + protocol.Project_ID__r.Project_Unique_Key__c+ '_'+ agreement.Contract_Type__c + '_'+RecordType ;  
                agreement.Project_ID__c=protocol.Project_ID__r.Project_Unique_Key__c;   
                agreement.Protocol__c = protocol.id;  
            }
            if(ObjName =='Protocol_Country__c'){
                agreement.Name = protocolcntry.Client_Protocol__r.Project_ID__r.Client_ID__c+ '_' + protocolcntry.Client_Protocol__r.Project_ID__r.Project_Unique_Key__c+ '_'+ agreement.Contract_Type__c + '_'+RecordType ;  
                agreement.Project_ID__c = protocolcntry.Client_Protocol__r.Project_ID__r.Project_Unique_Key__c;   
                agreement.Protocol_Country__c  = protocolcntry.id;
            }
            if(ObjName =='WFM_Site_Detail__c'){
                agreement.Name = site.Project_Protocol__r.Project_ID__r.Client_ID__c+ '_' + site.Project_Protocol__r.Project_ID__r.Project_Unique_Key__c+ '_'+ agreement.Contract_Type__c + '_'+RecordType ;  
                agreement.Project_ID__c=site.Project_Protocol__r.Project_ID__r.Project_Unique_Key__c;
                agreement.site__c = site.id;
            }
           
            system.debug('--------agreement 1------'+agreement);
            upsert agreement;
            system.debug('--------agreement 11------'+agreement);
            
           
            system.debug('--------agreement ------'+agreement.site__c);
            PageReference pageRef=new PageReference('/'+PID);
            pageRef.setRedirect(false);
            return pageRef; 
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
    }   
}