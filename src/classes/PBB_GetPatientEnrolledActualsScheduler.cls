/**
 * @author Ramya
 * @Date 2015
 * @Description This class sheduler job that is scheduled nightly for upserting Actual subjects from site object.
 */
global class PBB_GetPatientEnrolledActualsScheduler implements Schedulable{

    global void execute(SchedulableContext sc) {
       
        String className = 'PBB_GetPatientEnrolledActualsBatch';
    
        String query = 'Remove';
        String jsonParams = Json.serialize(new list<String>{'WFM_Protocol__c','Country_Weekly_Event__c'});
   
        PRA_Batch_Queue__c mappingBatch =
            new PRA_Batch_Queue__c(Batch_Class_Name__c = className, Parameters_JSON__c = jsonParams, 
                Status__c = 'Waiting', Priority__c = 5, Scope__c = 1);
        
        insert mappingBatch;
  }
}