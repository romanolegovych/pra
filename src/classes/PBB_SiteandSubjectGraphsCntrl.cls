/** 
@Author Devaram Bhargav
@Date 2015
@Description this class is for showing the results of Site Activation and Subject Enrollment graphs and its Grids.
*/
public class PBB_SiteandSubjectGraphsCntrl {
    
    public String ProtocolUniqueKey {get;set;}
    public WFM_Protocol__c protocol {get;set;}
    //PBB Scenario object ID
    public String PBBScenarioID{ get; set; }  
    
    
    //PBB Scenario object             
    public PBB_Scenario__c PBBScenario{ get; set; }         
    
    //to hide/show the main tabs in the component.
    public  List<String> HiddenHomeTabsList{get;set;}
    
    //to hide/show the subtabs in the component.
    public  List<String> HiddenSubTabsList{get;set;} 
    
    
    /** 
    @Author Devaram Bhargav
    @Date 2015
    @Description this is the constructor for  Site Activation and Subject Enrollment graphs and its Grids.
    */
    public PBB_SiteandSubjectGraphsCntrl () {    
        HiddenHomeTabsList = new List<String>();
        HiddenSubTabsList = new List<String>();        
        
        ProtocolUniqueKey = ApexPages.currentPage().getParameters().get('ProtocolUniqueKey'); 
        
        if(ProtocolUniqueKey!=null && ProtocolUniqueKey!=''){
            System.debug('%%%%%% PROTOCOL ID %%%%%%%%%'+ProtocolUniqueKey);
            
            protocol = new WFM_Protocol__c ();
            protocol = [select Id, name, Protocal_Unique_Key__c, Project_ID__c from WFM_Protocol__c where  Protocal_Unique_Key__c =: ProtocolUniqueKey]; 
            
            PBBScenario = new pbb_scenario__c();
            PBBScenario = [select name,Bid_Project__c,bid_project__r.name from Pbb_scenario__c where protocol_name__c =: protocol.name AND bid_project__r.PRA_Project_ID__c =: protocol.Project_ID__c];
            PBBScenarioID = PBBScenario.id ;
        }      
        
    }   
}