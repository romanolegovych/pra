/**
*   'PAWS_FloatingProcessesControllerTests' is the test class for PAWS_FloatingProcessesController
*   Test classes are used to write the unit test and cover the code coverage of the code.
*   @author   Work Relay
*/
@isTest 
public with sharing class PAWS_FloatingProcessesControllerTests {
	
	static STSWR1__Flow_Instance__c flowInstance;
	static ecrf__c PAWS_Project;
	static List <STSWR1__Flow__c> flows;
		
	static void init() {
		PAWS_ApexTestsEnvironment.init();
		PAWS_Project = PAWS_ApexTestsEnvironment.Project;
		flowInstance = PAWS_ApexTestsEnvironment.FlowInstance;
		STSWR1__Flow__c flowToDecline = PAWS_ApexTestsEnvironment.Flow;
		STSWR1__Flow__c flowToShow = flowToDecline.clone(false,true,false,false);		
		flows = new List <STSWR1__Flow__c>();		
		flows.add(flowToDecline);
		flows.add(flowToShow);
		for (Integer i=0; i < flows.size(); i++) {
			flows[i].STSWR1__Type__c = 'Recurrent';
			flows[i].STSWR1__Start_Type__c = 'Manual';
		}
		upsert flows;		
	}
	
	
	static testMethod void test1() {
		init();
		
		PageReference ref = Page.PAWS_FloatingProcesses;
    	ref.getParameters().put('sourceId', PAWS_Project.Id);
    	Test.setCurrentPage(ref);
		
		PAWS_FloatingProcessesController controller = new PAWS_FloatingProcessesController(); 
		controller.selectedFlowId = flows[0].id;
		controller.init();
		controller.apply();
		System.assert(controller.availableFlows.size() == 3);		
		System.assert(controller.leaveThePage != null); 		 
	}
	
	static testMethod void test2() {
		init();
		
		PageReference ref = Page.PAWS_FloatingProcesses;
    	ref.getParameters().put('sourceId', PAWS_Project.Id);
    	Test.setCurrentPage(ref);
		
		PAWS_FloatingProcessesController controller = new PAWS_FloatingProcessesController(); 
		controller.selectedFlowId = '';
		controller.init();
		controller.apply();	
		System.assert(controller.leaveThePage == null); 		 
	}
	
}