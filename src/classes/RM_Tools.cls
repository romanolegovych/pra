public class RM_Tools {
	static final string CONFIRMED = 'Confirmed';
	static final string PROPOSED = 'Proposed';
	static final string RESERVATION = 'Reservation';
	static final string ASSIGNMENT = 'Assignment';
    public RM_Tools(){
        
    }
    
    
    public static  set<string> GetMonths(Integer bMonth, Integer fMonth){
    
        Set<String> months = new Set<String>();     
        Date myDate = System.Today();           
        for (Integer i = bMonth; i <= fMonth; i++)
        {
                Date newDate =myDate.addMonths(i);
                string yearMonth = String.valueOf(newDate);
                months.add(yearMonth.SubString(0, yearMonth.lastIndexOf('-')));
        } 
        return months;  
    }
   public static integer GetMonthNumFromNow(string strMonth){
   	  	Date aDate = GetDatefromYearMonth(strMonth);
   	  	system.debug('------------avaStartMonth--------------'+aDate.monthsBetween(System.Today()));
   	  	return System.Today().monthsBetween(aDate);
   }
   public static integer GetMonthNumFromNow(Date aDate){
   	  	
   	  	system.debug('------------avaStartMonth--------------'+aDate.monthsBetween(System.Today()));
   	  	return System.Today().monthsBetween(aDate);
   }
   public static string GetYearMonth(Integer NMonthFromNow){
        Date myDate = System.Today().addMonths(NMonthFromNow); 
        string yearMonth = String.valueOf(myDate);
        return yearMonth.SubString(0, yearMonth.lastIndexOf('-'));
   }
   
   public static List<string> GetMonthsList(Integer bMonth, Integer fMonth) {
   
        List<string> months = new List<String>();     
        Date myDate = System.Today();           
        for (Integer i = bMonth; i <= fMonth; i++)
        {
            Date newDate =myDate.addMonths(i);
            string yearMonth = String.valueOf(newDate);
            months.add(yearMonth.SubString(0, yearMonth.lastIndexOf('-')));
        } 
        return months;   
    }
    public static List<string> GetMonthsListFromDate(Date dMonth, Date dEndMonth){
    
        List<string> months = new List<String>();
        Date aMonth = date.newinstance(dMonth.year(), dMonth.month(), 1);     
        
        while(aMonth<=dEndMonth)    
        {                               
            string yearMonth = String.valueOf(aMonth);
            months.add(yearMonth.SubString(0, yearMonth.lastIndexOf('-')));
            aMonth =aMonth.addMonths(1);
        } 
        return months;   
    }
    
    public static Date GetDatefromYearMonth(string yMonth){
        List<string> parts = yMonth.split('-');
        return date.newinstance(Integer.valueOf(parts[0]), Integer.valueOf(parts[1]), 1);
    }
    public static Date GetFirstDayDatefromDate(Date aDate){        
        return date.newinstance(aDate.Year(), aDate.Month(), 1);
    }
    public static Date GetLastDayinMonthfromDate(Date aDate){  
    	Date firstDay = date.newinstance(aDate.Year(), aDate.Month(), 1);      
        return firstDay.addMonths(1).addDays(-1);
    }
    public static Date GetLastDayDatefromYearMonth(string yMonth){ 
    	List<string> parts = yMonth.split('-');
    	Date nextMonth=date.newinstance(Integer.valueOf(parts[0]), Integer.valueOf(parts[1])+1, 1);        
        return nextMonth.addDays(-1) ;
    }
   
    public static string GetYearMonthFromDate(Date dMonth){
        string yearMonth = String.valueOf(dMonth);
        return  yearMonth.SubString(0, yearMonth.lastIndexOf('-'));
    }
    public static decimal FormatDouble(double dData, Integer iDecimal){
        decimal d = dData;
        return d.divide(1, iDecimal);   
    }
    
    public static string GetStringfromDate(Date aDate, string format){
        if (aDate != null){
            string year = string.valueOf(aDate.year());
            string month = string.valueOf(aDate.month());
            string day = string.valueOf(aDate.day());
            if (month.length() == 1)
                month = '0' + month;
            if(day.length() == 1)
                day = '0' + day;
            
            if (format=='mm/dd/yyyy')
                return month+'/'+day+'/'+year;
            
        }   
        
        return '';
    }
    public static date GetDateFromString(string strDate, string format){
        
        if (format == 'mm/dd/yyyy' && strDate.length() > 7){  
            List <string> datePart = strDate.split('/');
            return date.newInstance(Integer.valueOf(datePart[2]), Integer.valueOf(datePart[0]), Integer.valueOf(datePart[1]));
        
        }
        return null;
    }
    public static String getFormattedDate(Datetime value, string format){
 
        String formattedDate = '';
        if(value!=null)
        {
            value = value.addHours(value.hour());
            if(format == 'mm/dd/yyyy')        
                formattedDate = value.format('MM/dd/yyyy');
            
        }    
        return formattedDate;
    }
    public static Date getDateFromStringwithTimeCorrection(string strDate, string format){
    	 if (format == 'mm/dd/yyyy' && strDate.length() > 7){  
            List <string> datePart = strDate.split('/');
            integer hour = DateTime.now().hour();
            integer minute = DateTime.now().minute();
            integer sec = DateTime.now().second();
            DateTime aDateTime = DateTime.newInstance(Integer.valueOf(datePart[2]), Integer.valueOf(datePart[0]), Integer.valueOf(datePart[1]), hour, minute, sec);
        	return aDateTime.date();
        }
        return null;
    }
    
    public static string get2DecimalinStr(string value){
        if (value.indexOf('.') < 0) // no decimal
            value += '.00';
        else if (value.indexOf('.') == value.length() - 2)
            value += '0';
        return value;
    }
    public static set<string> getSetwithougEmptyValuefromMultiSel(string[] selectedOption){ 
        set<string> lstValue = new set<string>();   
    
        if(selectedOption!=null && selectedOption.size()>0){
           for(integer i=0;i<selectedOption.size();i++){
              if(selectedOption[i]!='' && selectedOption[i]!=null)
                    lstValue.add(selectedOption[i]);
           }
        }
        return lstValue;
    }
    public static string escapeStringforJson(string inputStr){
		/*\b  Backspace (ascii code 08)
		\f  Form feed (ascii code 0C)
		\n  New line
		\r  Carriage return
		\t  Tab
		\v  Vertical tab
		\'  Apostrophe or single quote
		\"  Double quote
		\\  Backslash caracter*/
		if (inputStr.contains('\b'))
			inputStr = inputStr.replaceAll('\b', '\\b');
		if (inputStr.contains('\f'))
			inputStr = inputStr.replaceAll('\f', '\\f');
		if (inputStr.contains('\r\n'))
			inputStr = inputStr.replaceAll('\r\n', '\n');
		if (inputStr.contains('\r'))
			inputStr = inputStr.replaceAll('\r', '\n');	
		if (inputStr.contains('\n'))
			inputStr = inputStr.replaceAll('\n', ' ');	
			
		if (inputStr.contains('\t'))
			inputStr = inputStr.replaceAll('\t', '\\t');	
		
		if (inputStr.contains('"'))
			inputStr = inputStr.replaceAll('"', ' ');	
			
		return String.escapeSingleQuotes(inputStr);
	}
    /*---------------other common function------------------*/
   public static map<string, sObject> GetMapfromListObject(string field, list<sObject> lstObject){
   		map<string, sObject> aMap = new map<string, sObject>();
   		for(sObject aObject : lstObject){
   			string f = string.valueOf(aObject.get(field));   
   			system.debug('------------map field--------------'+f);			
   			aMap.put(f, aObject);   			
   		}
   		return aMap;
   }
   public static set<string> GetSetfromListObject(string field, list<sObject> lstObject){
   		set<string> aSet = new set<string>();
   		for(sObject aObject : lstObject){
   			string f = string.valueOf(aObject.get(field));   			
   			aSet.add(f);   			
   		}
   		return aSet;
   }
   public static list<string> GetListStrfromListObject(string field, list<sObject> lstObject){
   		list<string> alist = new list<string>();
   		for(sObject aObject : lstObject){
   			string f = string.valueOf(aObject.get(field));   			
   			alist.add(f);   			
   		}
   		return alist;
   }
   public static string GetAssignmentStatusString(boolean bStatus ){
        if (bStatus)
            return CONFIRMED; 
        else
            return PROPOSED;
        
   }
   public static string GetAssignmentType(string projectStatus){
        if (projectStatus == 'Bid')
            return RESERVATION;
        else
            return ASSIGNMENT;
    }
   public static Boolean GetAssignmentBooleanStatusfromString(string strStatus ){
        if (strStatus == CONFIRMED)
            return true; 
        else
            return false;
        
    }
}