public with sharing class PAWS_FloatingProcessExtension
{
	public PAWS_Floating_Process__c record {get; set;}
	public Boolean isError {get; set;}
	
	public PAWS_FloatingProcessExtension(ApexPages.StandardController stdController)
	{
		stdController.addFields(new List<String>{
			'Flow__c',
			'Status__c'
		});
		
		isError = false;
		record = (PAWS_Floating_Process__c) stdController.getRecord();
	}
	
	public void StartProcess()
	{
		if (record.Flow__c == null)
		{
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Cannot start Process: reference to WR Flow is missing.'));
			isError = true;
			return;
		}
		
		List<STSWR1__Flow__c> flows = [
			Select
				Name,
				STSWR1__Object_Type__c,
				STSWR1__Type__c,
				STSWR1__Conditions__c,
				STSWR1__Conditions_Logic__c, 
				STSWR1__Number_Of_Instances__c,
				STSWR1__Enable_User_Chatter_Notifications__c,
				STSWR1__Enable_Object_Chatter_Notifications__c, 
				(
					Select
						STSWR1__Flow__r.Name,
						STSWR1__Object_Id__c
					From
						STSWR1__Flow_Instances__r
					Where
						STSWR1__Object_Id__c = :record.Id
				)
			From
				STSWR1__Flow__c
			Where
				Id = :record.Flow__c And
				STSWR1__Status__c = 'Active'
		];
		
		sObject sourceObject = PAWS_Utilities.makeQuery('PAWS_Floating_Process__c', 'Id =\'' + record.Id + '\'' , null)[0];
		Map<sObject, List<STSWR1__Flow__c>> allowedObjectFlowMap = new Map<sObject, List<STSWR1__Flow__c>>{sourceObject => flows};
		
		try
		{
			STSWR1.WorkflowActivator.getInstance().processObjects(allowedObjectFlowMap, true);
			
			record.Status__c = 'In Progress';
			update record;
		}
		catch(Exception ex){
			ApexPages.addMessages(ex);
			isError = true;
		}
	}
}