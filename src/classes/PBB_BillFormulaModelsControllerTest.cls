@isTest
private class PBB_BillFormulaModelsControllerTest {
	private static PBB_BillFormulaModelsController cont;
	
	@isTest
	static void BillFormulaModelsControllerTest() {
		Test.startTest();
    		cont = new PBB_BillFormulaModelsController();
    	Test.stopTest();
    	System.assert(cont.billFormulaList.size() == 0);
	}

	@isTest
	static void getNewPageReferenceTest() {
		cont = new PBB_BillFormulaModelsController();
		Test.startTest();
    		PageReference pr = cont.getNewPageReference();
    	Test.stopTest();
    	Schema.DescribeSObjectResult  r = Formula_Name__c.SObjectType.getDescribe();
    	System.assertEquals(pr.getUrl(), '/'+r.getKeyPrefix()+'/e');
	}
}