public without sharing class PAWS_WorkflowProgressController 
{
    public ID FlowInstanceId 
    {
        get;
        set
        {
            if(FlowInstanceId != value) refresh();
            FlowInstanceId = value;
        }
    }
    
    public DateTime ReinitFlag 
    {
        get;
        set
        {
            if(ReinitFlag != value) refresh();
            ReinitFlag = value;
        }
    }
    
    public String StepFilter {get;set;}
    public String TagFilter {get;set;}
    
    public List<InstanceWrapper> Instances
    {
        get
        {
            if(Instances == null) 
            {
                Instances = new List<InstanceWrapper>();
                Instances.add(new InstanceWrapper(FlowInstanceId, StepFilter, TagFilter));
            }
            return Instances;
        }
        set;
    }
    
    private transient STSWR1__Flow_Instance__c FlowInstanceLocal;
    public STSWR1__Flow_Instance__c FlowInstance
    {
        get
        {
            if(FlowInstanceLocal == null && !String.isEmpty(FlowInstanceId))
            {
                FlowInstanceLocal = (STSWR1__Flow_Instance__c)new STSWR1.API().call('WorkflowService', 'getFlowInstanceById', FlowInstanceId);
            }
            return FlowInstanceLocal;
        }
        private set { FlowInstanceLocal = value; }
    }

    public ID SeletedInstanceId {get;set;}
    public ID SeletedSubInstanceId {get;set;}

    /****************************** METHODS SECTION *********************************/
    
    public void refresh()
    {
        FlowInstance = null;
        Instances = null;
        SeletedInstanceId = null;
    }
    
    public void loadStepSubFlows()
    {
        Integer index = -1;
        for(Integer i = 0; i < Instances.size(); i++)
        {
            InstanceWrapper instance = Instances[i];

            if(instance.FlowInstanceId == SeletedInstanceId)
            {
                index = i;break;
            }
        }

        if(index != -1)
        {
            for(Integer i = index + 1; i < Instances.size(); i++)
            {
                Instances.remove(i);i--;
            }
        }
    
        Instances.add(new InstanceWrapper(SeletedSubInstanceId, StepFilter, TagFilter));
        SeletedInstanceId = null;
        SeletedSubInstanceId = null;
    }

    /****************************** OTHER SECTION *********************************/
    
    public class InstanceWrapper
    {
        public InstanceWrapper(ID flowInstanceId)
        {
            this.FlowInstanceId = flowInstanceId;
        }
        
        public InstanceWrapper(ID flowInstanceId, String stepFilter, String tagFilter)
        {
            this.FlowInstanceId = flowInstanceId;
            this.StepFilter = stepFilter;
            this.TagFilter = tagFilter;
        }
        
        public ID FlowInstanceId {get;set;}
        public String StepFilter {get;set;}
        public String TagFilter {get;set;}

        private transient STSWR1__Flow_Instance__c FlowInstanceLocal;
        public STSWR1__Flow_Instance__c FlowInstance
        {
            get
            {
                if(FlowInstanceLocal == null && !String.isEmpty(FlowInstanceId))
                {
                    FlowInstanceLocal = (STSWR1__Flow_Instance__c)new STSWR1.API().call('WorkflowService', 'getFlowInstanceById', FlowInstanceId);
                }
                
                return FlowInstanceLocal;
            }
            private set {FlowInstanceLocal = value;}
        }

        private transient List<Step> StepsLocal;
        public List<Step> Steps
        {
            get
            {
                if(StepsLocal == null && FlowInstance != null)
                {
                    Map<ID, Map<String, List<STSWR1__Flow_Step_Junction__c>>> flowProgressMap = (Map<ID, Map<String, List<STSWR1__Flow_Step_Junction__c>>>)new STSWR1.API().call('WorkflowService', 'getFlowProgress', new List<STSWR1__Flow_Instance__c>{FlowInstance});
                    Map<String, List<STSWR1__Flow_Step_Junction__c>> progress = flowProgressMap.get(FlowInstance.Id);
                    
                    List<STSWR1__Flow_Step_Junction__c> allSteps = new List<STSWR1__Flow_Step_Junction__c>();
                    allSteps.addAll(progress.get('past'));
                    allSteps.addAll(progress.get('pending'));
                    allSteps.addAll(progress.get('current'));
                    allSteps.addAll(progress.get('future'));
     
                    Map<ID, List<STSWR1__Flow_Instance_Cursor__c>> subCursors = loadSubCursors(FlowInstance);
                    Map<ID, STSWR1__Gantt_Step_Property__c> stepProperties = loadStepProperties(FlowInstance);
                    Map<ID, Set<String>> stepTags = loadStepTags(FlowInstance);
                    Map<ID, STSWR1__Flow_Instance_History__c> stepsHistory = loadStepHistory(allSteps);
      
                    StepsLocal = new List<Step>();
                    for(STSWR1__Flow_Step_Junction__c step : progress.get('past'))
                    {
                        Step stepWrapper = new Step(step, stepsHistory.get(step.Id), subCursors.get(step.Id), stepProperties.get(step.Id), stepTags.get(step.Id));
                        stepWrapper.Status = 'Completed';
                        
                        if(isStepAllowed(stepWrapper)) StepsLocal.add(stepWrapper);
                    }
                    
                    for(STSWR1__Flow_Step_Junction__c step : progress.get('pending'))
                    {
                        Step stepWrapper = new Step(step, stepsHistory.get(step.Id), subCursors.get(step.Id), stepProperties.get(step.Id), stepTags.get(step.Id));
                        stepWrapper.Status = 'Pending';
                        
                        if(isStepAllowed(stepWrapper)) StepsLocal.add(stepWrapper);
                    }
                    
                    for(STSWR1__Flow_Step_Junction__c step : progress.get('current'))
                    {
                        Step stepWrapper = new Step(step, stepsHistory.get(step.Id), subCursors.get(step.Id), stepProperties.get(step.Id), stepTags.get(step.Id));
                        stepWrapper.Status = 'In Progress';
                        
                        if(isStepAllowed(stepWrapper)) StepsLocal.add(stepWrapper);
                    }
                    
                    for(STSWR1__Flow_Step_Junction__c step : progress.get('future'))
                    {
                        Step stepWrapper = new Step(step, null, null, stepProperties.get(step.Id), stepTags.get(step.Id));
                        stepWrapper.Status = 'Future';
                        
                        if(isStepAllowed(stepWrapper)) StepsLocal.add(stepWrapper);
                    }
                }
                return StepsLocal;
            }
            private set {StepsLocal = value;}
        }
        
        //****************************************************************************************************
        
        public void refresh()
        {
            FlowInstance = null;
            Steps = null;
        }

        private Map<ID, STSWR1__Gantt_Step_Property__c> loadStepProperties(STSWR1__Flow_Instance__c flowInstance)
        {   
        	return (Map<ID, STSWR1__Gantt_Step_Property__c>)new STSWR1.API().call('GanttStepPropertyService', 'getFlowPropertiesMap', new Map<String, Object> {'flowId' => flowInstance.STSWR1__Flow__c});	
        }
        
        private Map<ID, Set<String>> loadStepTags(STSWR1__Flow_Instance__c flowInstance)
        {   
            Map<ID, Set<String>> properties = new Map<ID, Set<String>>();
            for(STSWR1__Flow_Step_Property__c property : [select Name, STSWR1__Flow_Step__c from STSWR1__Flow_Step_Property__c where STSWR1__Flow_Step__r.STSWR1__Flow__c = :flowInstance.STSWR1__Flow__c and STSWR1__Type__c = 'Tag' order by CreatedDate])
            {
                if(!properties.containsKey(property.STSWR1__Flow_Step__c)) properties.put(property.STSWR1__Flow_Step__c, new Set<String>());
                properties.get(property.STSWR1__Flow_Step__c).add(property.Name);
            }
            
            return properties;
        }
        
        private Map<ID, STSWR1__Flow_Instance_History__c> loadStepHistory(List<STSWR1__Flow_Step_Junction__c> steps)
        {   
            Map<ID, STSWR1__Flow_Instance_History__c> result = new Map<ID, STSWR1__Flow_Instance_History__c>();
            List<STSWR1__Flow_Instance_History__c> history = [select STSWR1__Step__c, STSWR1__Step__r.Name, STSWR1__Step__r.STSWR1__Is_System_Value__c, STSWR1__Step__r.STSWR1__Flow_Branch__c, STSWR1__Step__r.STSWR1__Flow_Milestone__r.Name, STSWR1__Step__r.STSWR1__Flow__r.Name, STSWR1__Actual_Start_Date__c, STSWR1__Actual_Start_Date_Value__c, STSWR1__Actual_Complete_Date__c, STSWR1__Actual_Complete_Date_Value__c, STSWR1__Status__c, CreatedDate, STSWR1__Completed_Date__c, STSWR1__Comments__c, STSWR1__Cursor__r.STSWR1__Step__c, STSWR1__Cursor__r.STSWR1__Is_Skipped__c, STSWR1__Cursor__r.STSWR1__Flow_Branch__c from STSWR1__Flow_Instance_History__c where STSWR1__Step__c in :steps order by CreatedDate, Name];
            for(Integer i = 0; i < history.size(); i++)
            {
                if(!result.containsKey(history[i].STSWR1__Step__c) || history[i].STSWR1__Step__r.STSWR1__Flow_Branch__c == (ID)history[i].STSWR1__Cursor__r.STSWR1__Flow_Branch__c)
               		result.put(history[i].STSWR1__Step__c, history[i]);
            }
                
            return result;
        }
        
        private Map<ID, List<STSWR1__Flow_Instance_Cursor__c>> loadSubCursors(STSWR1__Flow_Instance__c flowInstance)
        {
            Map<ID, List<STSWR1__Flow_Instance_Cursor__c>> result = new Map<ID, List<STSWR1__Flow_Instance_Cursor__c>>();
    
            Map<ID, Set<ID>> stepFlowMap = new Map<ID, Set<ID>>();
            Map<ID, List<STSWR1__Flow_Instance_Cursor__c>> flowCursorMap = new Map<ID, List<STSWR1__Flow_Instance_Cursor__c>>();
            for(STSWR1__Flow_Step_Action__c action : [select STSWR1__Status__c, STSWR1__Type__c, STSWR1__Flow_Step__c, STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Flow_Step__r.STSWR1__Flow__c = :flowInstance.STSWR1__Flow__c])
            {
                if(action.STSWR1__Status__c == 'Active' && action.STSWR1__Type__c == 'Start Sub Flow') 
                {
                	try
                	{
	                    Map<String, Object> data = (Map<String, Object>)JSON.deserializeUntyped(action.STSWR1__Config__c);
	                    ID flowId = (ID)data.get('flowId');
	      
	                    if(!stepFlowMap.containsKey(action.STSWR1__Flow_Step__c)) stepFlowMap.put(action.STSWR1__Flow_Step__c, new Set<ID>()); 
	                    stepFlowMap.get(action.STSWR1__Flow_Step__c).add(flowId);
	                    
	                    flowCursorMap.put(flowId, new List<STSWR1__Flow_Instance_Cursor__c>());
                	}catch(Exception ex){}
                }
            }
    
            if(flowCursorMap.size() == 0) return result;
                
            Set<ID> instanceCursorMap = new Set<ID>();
            List<STSWR1__Flow_Instance_Cursor__c> cursors = [select STSWR1__Flow_Instance__r.STSWR1__Object_Type__c, STSWR1__Flow_Instance__r.STSWR1__Object_Id__c, STSWR1__Flow_Instance__r.STSWR1__Flow__c, STSWR1__Flow_Instance__r.STSWR1__Flow__r.Name, STSWR1__Flow_Instance__r.STSWR1__Parent_Step__c, STSWR1__Flow_Instance__r.STSWR1__Variables__c, STSWR1__Step__r.Name, STSWR1__Step__r.STSWR1__Layout_Type__c, STSWR1__Step__r.STSWR1__Layout__c, STSWR1__Step__r.STSWR1__Layout_URL_Params__c, STSWR1__Step_Changed_Date__c, STSWR1__Step_Assign_Type__c, STSWR1__Step_Assigned_To__c, STSWR1__Is_Completed__c from STSWR1__Flow_Instance_Cursor__c where STSWR1__Flow_Instance__r.STSWR1__Flow__c in :flowCursorMap.keySet() and STSWR1__Flow_Instance__r.STSWR1__Parent__c = :flowInstance.Id];
            for(STSWR1__Flow_Instance_Cursor__c cursor : cursors)
            {
                if(instanceCursorMap.contains(cursor.STSWR1__Flow_Instance__c)) continue;
            
                instanceCursorMap.add(cursor.STSWR1__Flow_Instance__c);
                flowCursorMap.get(cursor.STSWR1__Flow_Instance__r.STSWR1__Flow__c).add(cursor);
            }
                
            for(ID stepId : stepFlowMap.keySet())
            {
                if(!result.containsKey(stepId)) result.put(stepId, new List<STSWR1__Flow_Instance_Cursor__c>()); 
                List<STSWR1__Flow_Instance_Cursor__c> stepCursors = result.get(stepId);
                        
                for(ID flowId : stepFlowMap.get(stepId))
                {
                    stepCursors.addAll(flowCursorMap.get(flowId));
                }
            }
        
            return result;
        }
        
        public Boolean isStepAllowed(Step stepWrapper)
        {
            if(stepWrapper.ActualStatus == 'Skipped') return false;
            
            if(!String.isEmpty(StepFilter))
            {
                Set<String> stepsFilterItems = new Set<String>(StepFilter.toLowerCase().split(';'));
                if(!stepsFilterItems.contains(stepWrapper.Instance.Name.toLowerCase())) return false;
            }
            
            if(!String.isEmpty(TagFilter) && stepWrapper.SubCursors.size() == 0)
            {
                if(stepWrapper.Tags == null) return false;
                
                Boolean flag = false;
                List<String> tagsFilterItems = TagFilter.split(';');
                for(String tagsFilterItem : tagsFilterItems)
                {
                    flag |= stepWrapper.Tags.contains(tagsFilterItem);
                }
                
                if(!flag) return false;
            }
            
            return true;
        }
    }
    
    public class SubFlow
    {
        public SubFlow(STSWR1__Flow_Instance_Cursor__c instance)
        {
            this.Instance = instance;
            this.Url = (String)new STSWR1.API().call('WorkflowService', 'buildGatewayTaskUrl', instance);
        }
        
        public STSWR1__Flow_Instance_Cursor__c Instance {get;set;}
        public String Url {get;set;}
    }
    
    public class Step
    {
        public Step(STSWR1__Flow_Step_Junction__c step, STSWR1__Flow_Instance_History__c history, List<STSWR1__Flow_Instance_Cursor__c> subCursors, STSWR1__Gantt_Step_Property__c inputProperty, Set<String> tags)
        {
            this.Property = inputProperty;
            this.Instance = step;
            this.History = history;
            this.Tags = tags;
            this.SubCursors = new List<SubFlow>();
            
            if(subCursors != null)
            {
                for(STSWR1__Flow_Instance_Cursor__c subCursor : subCursors)
                {
                    this.SubCursors.add(new SubFlow(subCursor));
                }
            }
        }

        public STSWR1__Gantt_Step_Property__c Property {get;set;}
        public STSWR1__Flow_Step_Junction__c Instance {get;set;}
        public STSWR1__Flow_Instance_History__c History {get;set;}
        public Set<String> Tags {get;set;}
        public List<SubFlow> SubCursors {get;set;}
        
        public Boolean CanShowRevisedStartDate { get { return Property != null && Property.STSWR1__Revised_Start_Date__c != null && Property.STSWR1__Planned_Start_Date__c != Property.STSWR1__Revised_Start_Date__c; } }
        public Boolean CanShowRevisedEndDate { get { return Property != null && Property.STSWR1__Revised_End_Date__c != null && Property.STSWR1__Planned_End_Date__c != Property.STSWR1__Revised_End_Date__c; } }
        public String Status {get;set;}
        
        public String ActualStatus 
        {
            get
            {
                Boolean isSkipped = (Property != null && Property.STSWR1__Is_Skipped__c == true) || (History != null && History.STSWR1__Comments__c == 'Skipped based on conditions.') || (History != null && History.STSWR1__Step__c == History.STSWR1__Cursor__r.STSWR1__Step__c && History.STSWR1__Cursor__r.STSWR1__Is_Skipped__c == true);
                return isSkipped ? 'Skipped' : Status;
            }
        }
        
        public Boolean IsPast
        {
            get
            {
                return Status == 'Completed';
            }
        }
        
        public Boolean IsLate
        {
            get
            {
                if(Property == null || IsPast == true) return false;
                
                return Property.STSWR1__Is_Late__c;
            }
        }
    }   
}