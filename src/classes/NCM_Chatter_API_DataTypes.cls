/**
 * @description	This class holds the wrapper data types that are used in the 'Chatter' API
 * @author		Dimitrios Sgourdos
 * @date		Created: 08-Sep-2015
 */
public with sharing class NCM_Chatter_API_DataTypes {
	
	/**
	 * @description	This sub-class holds the wrapper data that are used for posting a feed or comment in chatter
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 08-Sep-2015
	 */
	public class ChatterPostWrapper {
		public Id		objectToPostToId	{get;set;}
		public String	postText			{get;set;}
		
		// Standard constructor
		public ChatterPostWrapper() {
			
		}
		
		// Constructor with filling the sub-class parameters
		public ChatterPostWrapper(Id pObjectToPostToId, String pPostText) {
			objectToPostToId = pObjectToPostToId;
			postText		 = pPostText;
		}
	}
	
	
	/**
	 * @description	This sub-class holds the wrapper data that are used for subscribe or unsubscride a user to a feed
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 08-Sep-2015
	 */
	public class SubscriptionToByUserWrapper {
		public Id	feedId			{get;set;}
		public Id 	subscriberId	{get;set;}
		
		// Standard constructor
		public SubscriptionToByUserWrapper() {
			
		}
		
		// Constructor with filling the sub-class parameters
		public SubscriptionToByUserWrapper(ID pFeedId, ID pSubscriberId) {
			feedId	 	 = pFeedId;
			subscriberId = pSubscriberId;
		}
	}
}