/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_SiteInfoControllerUTest {

    static list<WFM_Client__c> lstClient;
    static list<WFM_Contract__c>  lstContract;
    static list<WFM_Project__c> lstProject;
    static WFM_Protocol__c aProt;
    static list<WFM_Site_Detail__c> lstSite;
    static RM_ConstantSetting__c emailGroup;
    
    static RM_UnitTestData init(){
    	RM_UnitTestData testData = new RM_UnitTestData();
 
    	lstClient = testData.lstClient;
    	lstContract = testData.lstContract;
    	lstProject = testData.lstProject;
 
    	aProt = testData.insertProt(lstProject[0].ID);
    	lstSite = testData.insertSite(aProt.ID); 
 
    	//reset email to tester's email
    	user u = RM_OtherService.GetUserInfo(UserInfo.getUserId());
 
    	return testData;
    }
    
    

    
     static testMethod void TestSiteDetail() {
        init();
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/RM_siteInfo?ID='+ lstProject[0].id );
        Test.setCurrentPage(pageRef);
        RM_SiteInfoController detailExt = new RM_SiteInfoController();   
        List<RM_SiteInfoController.siteDetail> sites = detailExt.getSiteDetail();
        system.assert(sites[0].ActSubjEnroll == 0);
        system.assert(sites[1].ActSubjEnroll==5);
        Test.stopTest();
        
      }
      static testMethod void TestLocation() {
        init();
        Test.startTest();
       	PageReference pageRef = new PageReference('/apex/RM_siteInfo?ID='+ lstProject[0].id );
        Test.setCurrentPage(pageRef);
        RM_SiteInfoController detailExt = new RM_SiteInfoController();   
        
        List<RM_SiteInfoController.LocationSummary> location = detailExt.GetSiteLocation();
        system.assert(location[0].country == 'United State');
        system.assert(location[0].num_site==5);
        Test.stopTest();       
        
      }
      static testMethod void TestMileStone() {
        init();
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/RM_siteInfo?ID='+ lstProject[0].id );
        Test.setCurrentPage(pageRef);
        RM_SiteInfoController detailExt = new RM_SiteInfoController();   
        List<RM_SiteInfoController.MileStone> stone = detailExt.GetMilestones();
        system.assert(stone[0].acturalDate == '12/27/2009');
       
        Test.stopTest();
      }
}