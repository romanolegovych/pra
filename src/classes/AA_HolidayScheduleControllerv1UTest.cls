/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AA_HolidayScheduleControllerv1UTest{
    
    static Country__c countryT;
    static State_Province__c stateT;
    static Holiday_Country_State__c hcsT;
    static Holiday_Schedule__c holidayScheduleT;
    static List<Holiday_Schedule__c> holiday;
    
    
    static void init(){
        
        countryT = new Country__c(Name = 'country1');
        insert countryT;
        
        stateT = new State_Province__c(Name = 'state1', Country__c = countryT.Id, State_Province_UniqueKey__c = countryT.Id +':' + 'State1');
        insert stateT;
        
        hcsT = new Holiday_Country_State__c(Country__c = countryT.Id, State_Province__c = stateT.Id, Holiday_Country_State_UniqueKey__c = CountryT.Id + ':' + StateT.Id);
        insert hcsT;
           
    }
    
    
    
    static testMethod void testConstructorHolidaySchedule() {
    
        init(); 
        test.startTest();
    
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        system.assertEquals(testObj.holidayDuration, 1.0);
        system.assertEquals(testObj.isSaveEdit, false);
        system.assertEquals(testObj.blnSave, true);
        system.assertEquals(testObj.isEdit, false);  
        system.assertEquals(testObj.addOrEdit, false);
        system.assertEquals(testObj.renderPanel, true);   
        
        test.stopTest();        
    
    }
    
   
    static testMethod void testHolidayAdd() {
        
        test.startTest();
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        testObj.AllowAdd();
        
        system.assertEquals(testObj.isSaveEdit, true);
        system.assertEquals(testObj.isEdit, false);
        system.assertEquals(testObj.holidayName, null);  
        system.assertEquals(testObj.holidayDate, null);
        
        test.stopTest();
        
    }
    
   
    static testMethod void testHolidayEditFunc() {
        
        init();
      
        test.startTest();
         
        Holiday_Schedule__c EditTest = new Holiday_Schedule__c(Holiday_Name__c = 'EditName',  Holiday_Country_State__c = hcsT.Id,  Holiday_Date__c = RM_Tools.GetDateFromString('08/14/2014', 'mm/dd/yyyy'), Duration_of_Day__c = 2, Status__c = 'Approved');
        insert EditTest;
        
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        testObj.paramEdit = EditTest.Id;
        testObj.Edit(); 
        
        system.assertEquals(testObj.isSaveEdit, true);
        system.assertEquals(testObj.isEdit, true);
        system.assertEquals(testObj.enableDetails, true);  
        system.assertEquals(testObj.addOrEdit, true);
        
        system.assertEquals(testObj.holidayName, 'EditName');
        system.assertEquals(testObj.holidayDate, '08/14/2014');
        system.assertEquals(testObj.holidayDuration, 2);
             
        test.stopTest();
    
    }
    
    
    static testMethod void testDelFunc() {
    
        init();
        
        test.startTest();
        
        Holiday_Schedule__c DeleteTest = new Holiday_Schedule__c(Holiday_Name__c = 'DeleteName',  Holiday_Country_State__c = hcsT.Id,  Holiday_Date__c = RM_Tools.GetDateFromString('08/14/2014', 'mm/dd/yyyy'), Duration_of_Day__c = 2, Status__c = 'Approved');
        insert DeleteTest;
        
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        testObj.paramDel = DeleteTest.Id;
        testObj.Del(); 
        
        List<ApexPages.Message> msgs = ApexPages.getMessages();
        system.assert(msgs.size() == 1);
        
        system.assertEquals('Deleted', msgs.get(0).getSummary());
        
        test.stopTest();
      }
    
    
    static testMethod void testCloneAll() { 
    
        init();
        
        test.startTest();
        
        List<Holiday_Schedule__c> cloneHolidayList = new List<Holiday_Schedule__c>();
        
        for(Integer i=0; i<2; i++) {
            Holiday_Schedule__c cloneTest = new Holiday_Schedule__c(Holiday_Name__c = 'cloneName' + i,  Holiday_Country_State__c = hcsT.Id,  Holiday_Date__c = RM_Tools.GetDateFromString('08/14/2014', 'mm/dd/yyyy'), Duration_of_Day__c = 2, Status__c = 'Draft');
            cloneHolidayList.add(cloneTest);
        }
                
        insert cloneHolidayList;
        
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        testObj.SelectedCountry = countryT.Name;
        testObj.SelectedYear = cloneHolidayList[0].Holiday_Date__c.year();
        testObj.CloneAll(); 
               
        List<Holiday_Schedule__c> retrieveHList = [Select Holiday_Date__c, Holiday_Name__c, Holiday_Country_State__c, Status__c, Duration_of_Day__c FROM Holiday_Schedule__c where holiday_name__c = :cloneHolidayList[0].holiday_name__c];
        system.assertEquals(retrieveHList[0].Holiday_Name__c, cloneHolidayList[0].holiday_name__c);  
        system.assertEquals(retrieveHList.size(),2);     
                
        test.stopTest();
        
    }
    
    
    static testMethod void testCloneAllForLeap() {
    
        init();
        test.startTest();
        
        Holiday_Schedule__c cloneTest1 = new Holiday_Schedule__c(Holiday_Name__c = 'cloneName3',  Holiday_Country_State__c = hcsT.Id,  Holiday_Date__c = RM_Tools.GetDateFromString('08/14/2016', 'mm/dd/yyyy'), Duration_of_Day__c = 3, Status__c = 'Draft');
        insert cloneTest1;   
    
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        testObj.SelectedCountry = countryT.Name;
        testObj.SelectedYear = 2016;
        testObj.CloneAll();
        
        List<Holiday_Schedule__c> retrieveHList1 = [Select Holiday_Date__c, Holiday_Name__c, Holiday_Country_State__c, Status__c, Duration_of_Day__c FROM Holiday_Schedule__c];
        system.assertEquals(retrieveHList1.size(), 2);
        
        test.stopTest();
    
    }
    
    static testMethod void testupdateNewEntryForNull() {
    
        test.startTest();
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
       
        
        testObj.holidayDate = null;
        testObj.holidayName = null;
        
        testObj.updateNewEntry();
        
        List<ApexPages.Message> msgs = ApexPages.getMessages();
        system.assert(msgs.size() == 1);
        system.assertEquals('Field cannot be left empty', msgs.get(0).getSummary());
        
        test.stopTest();
    
    }
    
    
    static testMethod void testupdateNewEntry() {
    
        init();
       
        test.startTest();
        
        Holiday_Schedule__c updateTest = new Holiday_Schedule__c(Holiday_Name__c = 'updateName',  Holiday_Country_State__c = hcsT.Id,  Holiday_Date__c = RM_Tools.GetDateFromString('08/14/2016', 'mm/dd/yyyy'), Duration_of_Day__c = 3, Status__c = 'Draft');
        insert updateTest; 
                
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
       
        testObj.isEdit = true;
        testObj.paramEdit = updateTest.Id;
        testObj.holidayName = 'EditedName';
        testObj.holidayDate = '09/14/2015';
        
        testObj.updateNewEntry();
        
        system.assertEquals(testObj.isEdit, true);
        system.assertEquals(testObj.enableDetails, false);  
                
        system.assertEquals(testObj.holidayName, null);
        system.assertEquals(testObj.holidayDate, null);
        system.assertEquals(testObj.paramEdit, null);
        
        List<ApexPages.Message> msgs = ApexPages.getMessages();
        system.assert(msgs.size() == 1);
        system.assertEquals('Entry Updated.', msgs.get(0).getSummary());
        
        test.stopTest();
    
    }
    
    static testMethod void testdisplayOutputForNull() {
    
        test.startTest();
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        
        
        testObj.holidayDate = null;
        testObj.holidayName = null;
        
        testObj.displayOutput();
        
        List<ApexPages.Message> msgs = ApexPages.getMessages();
        system.assert(msgs.size() == 1);
        system.assertEquals('Field cannot be left empty', msgs.get(0).getSummary());
  
        test.stopTest();
    }
    
    
    static testMethod void testdisplayOutputAdd() {
    
        init();
        
        test.startTest();
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();

        
        testObj.holidayName = 'testHoliday';
        testObj.holidayDate =  '06/12/2015';
        testObj.holidayDuration = 0.5;
        List<String> stateNames = new List<String>();
        stateNames.add(hcsT.Id);
        testObj.selectedState = stateNames;            // Needs reviewing
        
        testObj.displayOutput();
        
        List<Holiday_Schedule__c> retrieveHList2 = [Select Holiday_Date__c, Holiday_Name__c, Holiday_Country_State__c, Status__c, Duration_of_Day__c FROM Holiday_Schedule__c];
        system.assertEquals(retrieveHList2.size(), 1);
        
        List<ApexPages.Message> msgs = ApexPages.getMessages();  
        system.assert(msgs.size() == 1);       
        system.assertEquals('New Holiday added', msgs.get(0).getSummary());
        
        system.assertEquals(testObj.holidayName, null);
        system.assertEquals(testObj.holidayDate, null);
        test.stopTest();
        
    }
    
    
    static testMethod void testdisplayOutputAddForDuration() {
    
        init();
        
        test.startTest();
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();

        
        testObj.holidayName = 'testHoliday';
        testObj.holidayDate =  '06/12/2015';
        testObj.holidayDuration = 2;
        List<String> stateNames = new List<String>();
        stateNames.add(hcsT.Id);
        testObj.selectedState = stateNames;            // Needs reviewing
        
        testObj.displayOutput();
        
        List<Holiday_Schedule__c> retrieveHList2 = [Select Holiday_Date__c, Holiday_Name__c, Holiday_Country_State__c, Status__c, Duration_of_Day__c FROM Holiday_Schedule__c];
        system.assertEquals(retrieveHList2.size(), 2);
        
        List<ApexPages.Message> msgs = ApexPages.getMessages();  
        system.assert(msgs.size() == 1);       
        system.assertEquals('New Holiday added', msgs.get(0).getSummary());
        
        system.assertEquals(testObj.holidayName, null);
        system.assertEquals(testObj.holidayDate, null);
        test.stopTest();
        
    }
    
    
    static testMethod void testResetFunc() {
    
        test.startTest();
        
        PageReference pageRef = Page.AA_HolidaySchedulev1;
        Test.setCurrentPage(pageRef);
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        testObj.resetCountryHoliday();
        
        system.assertEquals(testObj.SelectedCountry, null);
        system.assertEquals(testObj.SelectedYear, Date.today().Year());
        
        test.stopTest();
    }
      
    
    static testMethod void testHolidaySaveNewCall() {
        
        test.startTest();
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        testObj.saveAndNewFunc();
        
        system.assertEquals(testObj.isSaveEdit, true);
        
        test.stopTest();
    }
    
    
    static testMethod void testHolidaySaveOrEditCall() {
    
        test.startTest();
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        testObj.saveOrEditFunc();
        
        system.assertEquals(testObj.addOrEdit, false);
        system.assertEquals(testObj.isSaveEdit, false);
        
        test.stopTest();
    }
    
    
    static testMethod void testHolidaySaveOrEditConditionCheck() {
    
        test.startTest();
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        testObj.addOrEdit = true;
        testObj.saveOrEditFunc();
        
        system.assertEquals(testObj.addOrEdit, true);
        system.assertEquals(testObj.isSaveEdit, false);
        
        test.stopTest();
    }
    
    static testMethod void testCancelFunc() {
    
        test.startTest();
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        testObj.cancelFunc();
        
        system.assertEquals(testObj.isSaveEdit, false);
    }
    
    
    static testMethod void testApproveAll() {
    
        init();  
        List<Holiday_Schedule__c> HSList = new List<Holiday_Schedule__c>();
        
        for(Integer i=0; i<3; i++) {
        Holiday_Schedule__c HSTest = new Holiday_Schedule__c(Holiday_Name__c = 'ApproveHol' + i,  Holiday_Country_State__c = hcsT.Id,  Holiday_Date__c = RM_Tools.GetDateFromString('08/14/2014', 'mm/dd/yyyy'), Duration_of_Day__c = 2, Status__c = 'Draft');
        HSList.add(HSTest);
        }
        
        insert HSList;
        
        test.startTest();
        
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
    
        testObj.SelectedCountry = countryT.name;
        testObj.SelectedYear = HSList[0].Holiday_Date__c.Year();
        testObj.stat ='Draft';
        
        testObj.ApproveAll();
        List<Holiday_Schedule__c> retrieveHolidayList = [Select Holiday_Date__c, Holiday_Name__c, Holiday_Country_State__c, Status__c, Duration_of_Day__c FROM Holiday_Schedule__c];
        for(Holiday_Schedule__c c: retrieveHolidayList) {
        system.assertEquals('Approved',c.Status__c);
        }        
    
        test.stopTest();    
    
    }
    

    static testMethod void testCountryOptionList() {
    
        PageReference pageRef = Page.AA_HolidaySchedulev1;
        Test.setCurrentPage(pageRef);
        
        List<Country__c> countryTestList = new List<Country__c>();
        
        for(Integer i=0; i<3; i++) {
        Country__c countryTest = new Country__c(Name = 'CountryS'+i, Is_PRA_Office__c = true);
        countryTestList.add(countryTest);
        }
        
        insert countryTestList;
        
        test.startTest();
        
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        for(Country__c c : countryTestList) {
        testObj.boolm = c.is_PRA_Office__c;
        }
        List<SelectOption> countryOptions=testObj.CountryList;
        
        test.stopTest();
        
        system.assertEquals(4,countryOptions.size());
        }
  
        
    static testMethod void testYearOptionList() {
    
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        test.startTest();
        List<SelectOption> yearOptions = testObj.YearList;
        test.stopTest();
        
        system.assertEquals(3,yearOptions.size());  
     }
 
    
    static testMethod void testDurationOptionList() {
    
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
        test.startTest();
 
        List<SelectOption> durOptions = testObj.DurationList;
 
        test.stopTest();
        
        system.assertEquals(8,durOptions.size());  
    }
  
    
    static testMethod void testStateOptionList() {
    
        PageReference pageRef = Page.AA_HolidaySchedulev1;
        Test.setCurrentPage(pageRef);
        AA_HolidayScheduleControllerv1 testObj = new AA_HolidayScheduleControllerv1();
       
        init();
        
        test.startTest();
        
        testObj.SelectedCountry = CountryT.Name;
       
        List<SelectOption> stateOptions=testObj.StateList;
        
        test.stopTest();
        
        system.assertEquals(1,stateOptions.size());
    }
    
}