/**
 * @author Konstantin Ermolenko
 * @Date 12/24/2014
 * @Description VF page controller for page that renders Linked Step activity. Tests are in PAWS_StepProperty_LinkedStepCntrlTests class, 
 */
public with sharing class PAWS_StepProperty_LinkedStepActCntrl
{
	public List<PAWS_Queue__c> PAWSQueues
	{
		get
		{
			if (PAWSQueues == null)
			{
				Datetime dt = Datetime.now() - 1;
				PAWSQueues = [
					Select
						Type__c,
						Status__c,
						Error_Details__c,
						Data__c,
						CreatedDate
					From
						PAWS_Queue__c
					Where
						Type__c in ('Linked Step Proceed', 'Linked Step Sync Dates') And
						CreatedDate >= :dt
					Order By
						CreatedDate desc
				];
			}
			return PAWSQueues;
		}
		set;
	}
	
	public List<PAWSQueueWrapper> PAWSQueueWrappers
	{
		get
		{
			if (PAWSQueueWrappers == null)
			{
				PAWSQueueWrappers = new List<PAWSQueueWrapper>();
				
				Set<ID> stepIds = new Set<ID>();
				Map<ID, ID> pqStepId = new Map<ID, ID>();
				for (PAWS_Queue__c pq : PAWSQueues)
				{
					Map<String, String> dataMap = (Map<String, String>) JSON.deserialize(pq.Data__c, Map<String, String>.class);
					String stepId = dataMap.get('sourceStepId') != null ? dataMap.get('sourceStepId') : dataMap.get('stepId');
					if (!String.isEmpty(stepId))
					{
						stepIds.add(stepId);
						pqStepId.put(pq.Id, stepId);
					}
				}
				
				Map<ID, STSWR1__Flow_Step_Junction__c> stepsMap = new Map<ID, STSWR1__Flow_Step_Junction__c>([Select Name, STSWR1__Flow__r.Name From STSWR1__Flow_Step_Junction__c Where Id in :stepIds]);
				
				for (PAWS_Queue__c pq : PAWSQueues)
				{
					PAWSQueueWrappers.add(new PAWSQueueWrapper(pq, stepsMap.get(pqStepId.get(pq.Id))));
				}
			}
			return PAWSQueueWrappers;
		}
		set;
	}
	
	public void Refresh()
	{
		PAWSQueues = null;
		PAWSQueueWrappers = null;
	}
	
	public class PAWSQueueWrapper
	{
		public PAWS_Queue__c PQ {get; set;}
		public STSWR1__Flow_Step_Junction__c Step {get; set;}
		
		public PAWSQueueWrapper(PAWS_Queue__c pq, STSWR1__Flow_Step_Junction__c step)
		{
			this.PQ = pq;
			this.Step = step;
		}
	}
}