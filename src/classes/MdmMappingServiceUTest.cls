/**
 * Test class for MDM Mapping Service classes
 */
@isTest
private class MdmMappingServiceUTest {
	
	static list<MuleServicesCS__c> serviceSettings;
	
	static void init() {
		serviceSettings = new MuleServicesCS__c[] {
			new MuleServicesCS__c(name = 'ENDPOINT', Value__c = 'ENDPOINT'),
			new MuleServicesCS__c(name = 'TIMEOUT', Value__c = '60000'),
			new MuleServicesCS__c(name = 'MdmMappingService', Value__c = 'MdmMappingService')
		};
		insert serviceSettings;
	}
	
	// These testmethod is just to check that the mocking is working
	// Assertions will be made for classes that use the service wrapper
	static testmethod void testGenerateResponses_POSITIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		MdmMappingService.valueListResposne valueListResp = MdmMappingServiceWrapper.getAllCrsInstances();
		valueListResp = MdmMappingServiceWrapper.getAttributeValues();
		valueListResp = MdmMappingServiceWrapper.getCTMSCrsInstances();
		valueListResp = MdmMappingServiceWrapper.getETMFCrsInstances();
		MdmMappingService.etmfSiteListResponse etmfSiteListResp = MdmMappingServiceWrapper.getEtmfSitesSearch(0, '', '', '');
		MdmMappingService.instanceListResponse instanceListResp = MdmMappingServiceWrapper.getInstancesLikeName('');
		MdmMappingService.protocolSiteListResponse protocolSiteListResp = MdmMappingServiceWrapper.getProtocolMappingSitesSearch(0, '', '', '');
		Test.stopTest();
	}
	
	static testmethod void testGenerateResponses2_POSITIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		MdmMappingService.instanceVO instanceVO = new MdmMappingService.instanceVO();
		MdmMappingService.etmfSiteVO etmfSiteVO = new MdmMappingService.etmfSiteVO();
		MdmMappingService.protocolSiteVO protocolSiteVO = new MdmMappingService.protocolSiteVO();
		MdmMappingService.mdmCrudResponse mdmCrudResp = MdmMappingServiceWrapper.saveInstanceFromVO(instanceVO);
		mdmCrudResp = MdmMappingServiceWrapper.saveCrsInstClientProjFromVO(protocolSiteVO);
		mdmCrudResp = MdmMappingServiceWrapper.saveEtmfProtocolMappingFromVO(etmfSiteVO);
		mdmCrudResp = MdmMappingServiceWrapper.deleteCrsInstanceFromVO(instanceVO);
		mdmCrudResp = MdmMappingServiceWrapper.deleteCrsInstClientProjFromVO(protocolSiteVO);
		mdmCrudResp = MdmMappingServiceWrapper.deleteEtmfProtocolMappingFromVO(etmfSiteVO);
		MdmMappingServiceWrapper.getErrors();
		Test.stopTest();
	}
	
	static testmethod void testGenerateResponses_NEGATIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(true));
		MdmMappingService.valueListResposne valueListResp = MdmMappingServiceWrapper.getAllCrsInstances();
		valueListResp = MdmMappingServiceWrapper.getAttributeValues();
		valueListResp = MdmMappingServiceWrapper.getCTMSCrsInstances();
		valueListResp = MdmMappingServiceWrapper.getETMFCrsInstances();
		MdmMappingService.etmfSiteListResponse etmfSiteListResp = MdmMappingServiceWrapper.getEtmfSitesSearch(0, '', '', '');
		MdmMappingService.instanceListResponse instanceListResp = MdmMappingServiceWrapper.getInstancesLikeName('');
		MdmMappingService.protocolSiteListResponse protocolSiteListResp = MdmMappingServiceWrapper.getProtocolMappingSitesSearch(0, '', '', '');
		Test.stopTest();
	}
	
	static testmethod void testGenerateResponses2_NEGATIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(true));
		MdmMappingService.instanceVO instanceVO = new MdmMappingService.instanceVO();
		MdmMappingService.etmfSiteVO etmfSiteVO = new MdmMappingService.etmfSiteVO();
		MdmMappingService.protocolSiteVO protocolSiteVO = new MdmMappingService.protocolSiteVO();
		MdmMappingService.mdmCrudResponse mdmCrudResp = MdmMappingServiceWrapper.saveInstanceFromVO(instanceVO);
		mdmCrudResp = MdmMappingServiceWrapper.saveCrsInstClientProjFromVO(protocolSiteVO);
		mdmCrudResp = MdmMappingServiceWrapper.saveEtmfProtocolMappingFromVO(etmfSiteVO);
		mdmCrudResp = MdmMappingServiceWrapper.deleteCrsInstanceFromVO(instanceVO);
		mdmCrudResp = MdmMappingServiceWrapper.deleteCrsInstClientProjFromVO(protocolSiteVO);
		mdmCrudResp = MdmMappingServiceWrapper.deleteEtmfProtocolMappingFromVO(etmfSiteVO);
		MdmMappingServiceWrapper.getErrors();
		Test.stopTest();
	}
	
}