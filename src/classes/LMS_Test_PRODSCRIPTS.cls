/**
 * Test class for prod scripts data loader class
 * Author: Andrew Allen
 */
@isTest
private class LMS_Test_PRODSCRIPTS {
    
    static List<LMS_Role__c> roles;
    static List<Role_Type__c> roleTypes;
    static List<Job_Class_Desc__c> jobClasses;
    static List<Job_Title__c> jobTitles;
    static List<PRA_Business_Unit__c> businessUnits;
    static List<Department__c> departments;
    static List<Region__c> regions;
    static List<Country__c> countries;
    static List<Employee_Details__c> employees;
    static List<Employee_Type__c> empTypes;
    static List<Employee_Status__c> empStatuses;
    static List<Course_Domain__c> courseDomains;
    static List<LMS_Course__c> courses;
    static List<LMS_Role_Course__c> courseMappings;
    static List<LMSConstantSettings__c> constants;
    static List<CourseDomainSettings__c> domains;
    static List<RoleAdminServiceSettings__c> settings;
    
    static void init() {
        
        constants = new LMSConstantSettings__c[] {
        	new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
    		new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
    		new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
    		new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D'),
    		new LMSConstantSettings__c(Name = 'typePRA', Value__c = 'PRA'),
    		new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
    		new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
    		new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
    		new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
    		new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
    		new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
    		new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
    		new LMSConstantSettings__c(Name = 'statusToDelete', Value__c = 'Delete'),
    		new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended'),
    		new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
    		new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
    		new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
    		new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete')
        };
        insert constants;
        
        domains = new CourseDomainSettings__c[] {
            new CourseDomainSettings__c(Name = 'Internal', Domain_Id__c = 'Internal'),
            new CourseDomainSettings__c(Name = 'PST', Domain_Id__c = 'Project Specific'),
            new CourseDomainSettings__c(Name = 'Archive', Domain_Id__c = 'Archive')
        };
        insert domains;
        
        settings = new RoleAdminServiceSettings__c[] {
            new RoleAdminServiceSettings__c(Name = 'DOMAIN', Value__c = 'DOMAIN'),
            new RoleAdminServiceSettings__c(Name = 'ENDPOINT', Value__c = 'ENDPOINT'),
            new RoleAdminServiceSettings__c(Name = 'TIMEOUT', Value__c = '50000')
        };
        insert settings;
    
        jobClasses = new Job_Class_Desc__c[] {
            new Job_Class_Desc__c(Name = 'Accountant', Job_Class_Code__c = 'code1', Job_Class_ExtID__c = 'Accountant'),
            new Job_Class_Desc__c(Name = 'Account Manager', Job_Class_Code__c = 'code2', Job_Class_ExtID__c = 'Account Manager')
        };
        insert jobClasses;
        
        jobTitles = new Job_Title__c[] {
            new Job_Title__c(Job_Code__c = 'Code1', Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = 'Accountant 1', Status__c = 'A'),
            new Job_Title__c(Job_Code__c = 'Code2', Job_Class_Desc__c = jobClasses[1].Id, Job_Title__c = 'Account Manager 1', Status__c = 'A')
        };
        insert jobTitles;
        
        businessUnits = new PRA_Business_Unit__c[] {
            new PRA_Business_Unit__c(Name = 'Analysis and Reporting', Business_Unit_Code__c = 'BUA', Status__c = 'A'),
            new PRA_Business_Unit__c(Name = 'CEO', Business_Unit_Code__c = 'BUB', Status__c = 'A'),
            new PRA_Business_Unit__c(Name = 'Accounting', Business_Unit_Code__c = 'BUC', Status__c = 'A')
        };
        insert businessUnits;
        
        departments = new Department__c[] {
            new Department__c(Name = 'Analysis and Reporting', Department_Code__c = 'DEPTA', Status__c = 'A'),
            new Department__c(Name = 'Executive Office', Department_Code__c = 'DEPTB', Status__c = 'A'),
            new Department__c(Name = 'Analysis and Reporting', Department_Code__c = 'DEPTC', Status__c = 'A')
        };
        insert departments;
        
        regions = new Region__c[] {
            new Region__c(Region_Name__c = 'US/Canada', Region_Id__c = 1, Status__c = 'A'),
            new Region__c(Region_Name__c = 'Latin America', Region_Id__c = 2, Status__c = 'A'),
            new Region__c(Region_Name__c = 'Europe/Asia', Region_Id__c = 3, Status__c = 'A'),
            new Region__c(Region_Name__c = 'Asia/Pacific', Region_Id__c = 4, Status__c = 'A')    
        };
        insert regions;
        
        countries = new Country__c[] {
            new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='US/Canada', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry2',PRA_Country_ID__c='200', Country_Code__c='JU',Region_Name__c='Latin America', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry3',PRA_Country_ID__c='300', Country_Code__c='TB',Region_Name__c='Asia/Pacific', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry4',PRA_Country_ID__c='400', Country_Code__c='AB',Region_Name__c='Europe/Africa', 
                               Daily_Business_Hrs__c=8.0)
        };
        insert countries;
        
        employees = new Employee_Details__c[] {
            new Employee_Details__c(Name = 'employee1', Employee_Unique_Key__c = 'employee1', First_Name__c = 'Andrew', Last_Name__c = 'Allen', Email_Address__c = 'a@a.com', 
                                    Job_Class_Desc__c = jobClasses[0].Name, Job_Code__c = jobTitles[0].Job_Code__c, Business_Unit_Desc__c = businessUnits[0].Name, Department__c = departments[0].Name,
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU'),
            new Employee_Details__c(Name = 'employee2', Employee_Unique_Key__c = 'employee2', First_Name__c = 'Allen', Last_Name__c = 'Stephens', Email_Address__c = 's@a.com', 
                                    Job_Class_Desc__c = jobClasses[0].Name, Job_Code__c = jobTitles[0].Job_Code__c, Business_Unit_Desc__c = businessUnits[0].Name, Department__c = departments[0].Name,
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU'),
            new Employee_Details__c(Name = 'employee3', Employee_Unique_Key__c = 'employee3', First_Name__c = 'Allen', Last_Name__c = 'Richard', Email_Address__c = 'r@a.com', 
                                    Job_Class_Desc__c = jobClasses[0].Name, Job_Code__c = jobTitles[0].Job_Code__c, Business_Unit_Desc__c = businessUnits[0].Name, Department__c = departments[0].Name,
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU')
        };
        insert employees;
        
        empTypes = new Employee_Type__c[] {
            new Employee_Type__c(Name = 'Employees'),
            new Employee_Type__c(Name = 'Contractor'),
            new Employee_Type__c(Name = 'Terminated')
        };
        insert empTypes;
        
        empStatuses = new Employee_Status__c[] {
            new Employee_Status__c(Employee_Status__c = 'AA', Employee_Type__c = empTypes[0].Name),
            new Employee_Status__c(Employee_Status__c = 'ZZ', Employee_Type__c = empTypes[1].Name),
            new Employee_Status__c(Employee_Status__c = 'XX', Employee_Type__c = empTypes[2].Name)
        };
        insert empStatuses;
        
        roleTypes = new Role_Type__c[] {
            new Role_Type__c(Name = 'PRA'),
            new Role_Type__c(Name = 'Project Specific')
        };
        insert roleTypes;
        
        roles = new LMS_Role__c[] {
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = jobTitles[0].Id, Business_Unit__c = businessUnits[0].Id,
                            Department__c = departments[0].Id, Region__c = regions[0].Id, Country__c = countries[0].Id, Employee_Type__c = empTypes[0].Id,
                            Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '1'),
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = null, Business_Unit__c = null,
                            Department__c = null, Region__c = null, Country__c = null, Employee_Type__c = null,
                            Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '2'),
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = jobTitles[0].Id, Business_Unit__c = null,
                            Department__c = null, Region__c = null, Country__c = null, Employee_Type__c = null,
                            Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '3'),
            new LMS_Role__c(Job_Class_Desc__c = null, Job_Title__c = null, Business_Unit__c = businessUnits[0].Id,
                            Department__c = null, Region__c = null, Country__c = null, Employee_Type__c = null,
                            Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '4'),
            new LMS_Role__c(Job_Class_Desc__c = null, Job_Title__c = null, Business_Unit__c = businessUnits[0].Id,
                            Department__c = departments[0].Id, Region__c = null, Country__c = null, Employee_Type__c = null,
                            Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '5'),
            new LMS_Role__c(Job_Class_Desc__c = null, Job_Title__c = null, Business_Unit__c = null,
                            Department__c = departments[0].Id, Region__c = null, Country__c = null, Employee_Type__c = null,
                            Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '6'),
            new LMS_Role__c(Job_Class_Desc__c = null, Job_Title__c = null, Business_Unit__c = null,
                            Department__c = null, Region__c = regions[0].Id, Country__c = null, Employee_Type__c = null,
                            Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '7'),
            new LMS_Role__c(Job_Class_Desc__c = null, Job_Title__c = null, Business_Unit__c = null,
                            Department__c = null, Region__c = regions[0].Id, Country__c = countries[0].Id, Employee_Type__c = null,
                            Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '8'),
            new LMS_Role__c(Job_Class_Desc__c = null, Job_Title__c = null, Business_Unit__c = null,
                            Department__c = null, Region__c = null, Country__c = null, Employee_Type__c = empTypes[0].Id,
                            Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '9')
        };
        insert roles;
        
        
        courseDomains = new Course_Domain__c[] {
            new Course_Domain__c(Domain__c = 'Internal'),
            new Course_Domain__c(Domain__c = 'Archive'),
            new Course_Domain__c(Domain__c = 'Project Specific')
        };
        insert courseDomains;
        
        courses = new LMS_Course__c[] {
            new LMS_Course__c(Offering_Template_No__c = 'course123', Course_Code__c = 'C123', Title__c = 'Course 123', Domain_Id__c = courseDomains[0].Domain__c,
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse1',
                              Discontinued_From__c = Date.today()-30),
            new LMS_Course__c(Offering_Template_No__c = 'course321', Course_Code__c = 'C321', Title__c = 'Course 321', Domain_Id__c = courseDomains[0].Domain__c,
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today()-1, SABA_ID_PK__c = 'sabaCourse2',
                              Discontinued_From__c = Date.today()+30),
            new LMS_Course__c(Offering_Template_No__c = 'course234', Course_Code__c = 'C234', Title__c = 'Course 234', Domain_Id__c = courseDomains[0].Domain__c,
                              Type__c = 'SOP', Status__c = 'Retired', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse3',
                              Discontinued_From__c = Date.today()-30),
            new LMS_Course__c(Offering_Template_No__c = 'course432', Course_Code__c = 'C432', Title__c = 'Course 432', Domain_Id__c = courseDomains[1].Domain__c,
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse4',
                              Discontinued_From__c = Date.today()-30)
        };
        insert courses;
        
        courseMappings = new LMS_Role_Course__c[] {
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[0].Id, Status__c = 'Draft', Sync_Status__c = 'N',
                                   Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[1].Id, Status__c = 'Pending', Sync_Status__c = 'Y',
                                   Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[2].Id, Status__c = 'Committed', Sync_Status__c = 'Y',
                                   Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[3].Id, Status__c = 'Committed', Sync_Status__c = 'Y',
                                   Assigned_By__c = 'ASA')
        };
        insert courseMappings;
    }
    
    static testMethod void testSendRoleData() {
        init();
        LMS_PROD_SCRIPTS.sendRoleData(roles);
    }
    
    static testMethod void testPopulateRoleEmployees() {
        init();
        LMS_PROD_SCRIPTS.populateRoleEmployees(roles);
    }
    
    static testMethod void testSendRoleCourses() {
        init();
        LMS_PROD_SCRIPTS.sendRoleCourseMappings(courseMappings);
    }
    
    static testMethod void testSendRoleEmployees() {
        init();
        LMS_PROD_SCRIPTS.populateRoleEmployees(roles);
        List<LMS_Role_Employee__c> roleEmployees = 
        [SELECT  Role_Id__c, Status__c, Employee_Id__r.Name, Employee_Id__r.Date_Hired__c, Role_Id__r.SABA_Role_PK__c 
            FROM LMS_Role_Employee__c 
            WHERE Sync__c = 'N' ORDER BY Role_Id__c LIMIT 1000];
        LMS_PROD_SCRIPTS.sendRoleEmployeeMappings(roleEmployees);
    }
    
    /*static testMethod void testPRODScheduler() {
        init();
        LMS_UpdatePRODValues c = new LMS_UpdatePRODValues();
        SchedulableContext cxt;
        c.execute(cxt);
    }*/
    
}