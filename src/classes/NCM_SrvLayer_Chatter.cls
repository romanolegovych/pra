/**
 * @description	The Service Layer for the Chatter
 * @author		Dimitrios Sgourdos
 * @date		Created: 09-Sep-2015, Edited: 10-Sep-2015
 */
public with sharing class NCM_SrvLayer_Chatter {
	
	/**
	 * @description	Post a feed under the given object
	 * @author		Maurice Kremer, Dimitrios Sgourdos
	 * @date		Created: 03-Jul-2014, Edited: 08-Sept-2015
	 * @param		objectToPostToId    The object to post to
	 * @param		postText            The text of the post
	*/
	static public void postFeedItem(Id objectToPostToId, String postText) {
		FeedItem postFeed = new FeedItem(	parentId = objectToPostToId,
											Body	 = postText
										);
		insert postFeed;
	}
	
	
	/**
	 * @description	Post the feeds under the given corresponding objects
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 08-Sept-2015
	 * @param		source    The wrapper data that holds the combination of feed and related object
	*/
	static public void postFeedItem(List<NCM_Chatter_API_DataTypes.ChatterPostWrapper> source) {
		List<FeedItem> postFeeds = new List<FeedItem>();
		
		for(NCM_Chatter_API_DataTypes.ChatterPostWrapper tmpItem : source) {
			postFeeds.add( new FeedItem(parentId = tmpItem.objectToPostToId, Body = tmpItem.postText) );
		}
		
		insert postFeeds;
	}
	
	
	/**
	 * @description	Create a comment under a feed post
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 10-Jul-2014, Edited: 08-Sep-2015
	 * @param		postComment       The text of the comment
	 * @param		feedToPostToId    The feed post that the comment will be related with
	*/
	static public void postComment(Id feedToPostToId, String postComment) {
		FeedComment comment = new FeedComment(	FeedItemId  = feedToPostToId,
												CommentBody = postComment
											);
		insert comment;
	}
	
	
	/**
	 * @description	Create comments under the given corresponding feed posts
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 10-Jul-2014, Edited: 08-Sep-2015
	 * @param		source    The wrapper data that holds the combination of comment and related feed
	*/
	static public void postComment(List<NCM_Chatter_API_DataTypes.ChatterPostWrapper> source) {
		List<FeedComment> commentsList = new List<FeedComment>();
		
		for(NCM_Chatter_API_DataTypes.ChatterPostWrapper tmpItem : source) {
			commentsList.add( new FeedComment(FeedItemId = tmpItem.objectToPostToId, CommentBody = tmpItem.postText) );
		}
		
		insert commentsList;
	}
	
	
	/**
	 * @description	Check if a user is subscribed to a feed
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Jul-2014, Edited: 08-Sep-2015
	 * @param		objectFollowedId    The object to check if the user is subscribed to
	 * @param		userId              The user to check if he is subscribed to the given feed
	 * @return		Flag if a user is subscribed to a feed or not
	*/
	static public Boolean isUserSubscribedToFeed(Id objectToFollowId, Id userId) {
		List<EntitySubscription> subscriptionList = NCM_Chatter_DataAccessor.getSubscriptionByFeedAndUser(
																									objectToFollowId,
																									userId);
		return ( ! subscriptionList.isEmpty() );
	}
	
	
	/**
	 * @description	Get the feed posts that are related with the given parent object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Jul-2014
	 * @param		feedParentId    The id of the parent object
	 * @return		The posts related with the given object
	*/
	static public List<FeedItem> getFeedItems(Id feedParentId) {
		return NCM_Chatter_DataAccessor.getFeedItemsByParentFeed(feedParentId);
	}
	
	
	/**
	 * @description	Get the posts that are related with the given parent object and topic
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Jul-2014, Edited: 08-Sep-2015
	 * @param		feedParentId    	The id of the parent object
	 * @param		relatedTopicName    The related topic of the posts
	 * @return		The posts related with the given object and topic
	*/
	static public List<FeedItem> getFeedItems(Id feedParentId, String relatedTopicName) {
		return NCM_Chatter_DataAccessor.getFeedItemsByParentFeedAndRelatedTopic(feedParentId, relatedTopicName);
	}
	
	
	/**
	 * @description	Unsubscribe the given user from the given feed
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Jul-2014, Edited: 09-Sep-2015
	 * @param		objectFollowedId    The object the user is unsubscribed from
	 * @param		userId              The user that will be unsubscribed from the feed
	*/
	static public void unsubscribeUserFromFeed(Id objectFollowedId, Id userId) {
		List<EntitySubscription> subscription = NCM_Chatter_DataAccessor.getSubscriptionByFeedAndUser(
																									objectFollowedId,
																									userId);
		delete subscription;
		
	}
	
	
	/**
	 * @description	Unsubscribe the given combinations of users and feeds
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 08-Sep-2015, Edited: 09-Sep-2015
	 * @param		source    The wrapper data that keeps which subscriptions must be deleted
	*/
	static public void unsubscribeUserFromFeed(List<NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper> source) {
		// Check for invalid parameters
		if(source == NULL || source.isEmpty()) {
			return;
		}
		
		// Find the possible subscriptions and map them in the combination of feed id and subscriber id
		Map<String, EntitySubscription> subscriptionMap = mapSubscriptionsOnSubscriberAndFeed(source);
		
		// Delete the corresponding subscriptions
		List<EntitySubscription> results = new List<EntitySubscription>();
		
		for(NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper tmpWrapperItem : source) {
			String key = '' + tmpWrapperItem.feedId + ':' + tmpWrapperItem.subscriberId;
			if( subscriptionMap.containsKey(key) ) {
				results.add( subscriptionMap.remove(key) );
			}
		}
		
		delete results;
	}
	
	
	/**
	 * @description	Subscribe given user to the given feed
	 * @author 		Maurice Kremer, Dimitrios Sgourdos
	 * @date		Created: 03-Jul-2014, Edited: 08-Sept-2015
	 * @param		objectToFollowId    The object the user is subscribed to
	 * @param		userId              The user that will be subscribed to the feed
	*/
	static public void subscribeUserToFeed(Id objectToFollowId, Id userId) {
		// Check if the user is already subscribed
		if( isUserSubscribedToFeed(objectToFollowId, userId) ) {
			return;
		}
		
		// Subscribe the user
		EntitySubscription subscription = new EntitySubscription(	parentId = objectToFollowId,
																	subscriberId = userId
																);
		insert subscription;
	}
	
	
	/**
	 * @description	Subscribe the given combinations of users and feeds
	 * @author 		Dimitrios Sgourdos
	 * @date		Created: 08-Sept-2015, Edited: 09-Sep-2015
	 * @param		source    The wrapper data that keeps which subscriptions must be inserted
	*/
	static public void subscribeUserToFeed(List<NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper> source) {
		// Check for invalid parameters
		if(source == NULL || source.isEmpty()) {
			return;
		}
		
		// Find the possible existing subscriptions and map them in the combination of feed id and subscriber id
		Map<String, EntitySubscription> subscriptionMap = mapSubscriptionsOnSubscriberAndFeed(source);
		
		// Delete the corresponding subscriptions
		List<EntitySubscription> results = new List<EntitySubscription>();
		
		for(NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper tmpWrapperItem : source) {
			String key = '' + tmpWrapperItem.feedId + ':' + tmpWrapperItem.subscriberId;
			if( ! subscriptionMap.containsKey(key) ) {
				results.add( new EntitySubscription(parentId = tmpWrapperItem.feedId,
													subscriberId = tmpWrapperItem.subscriberId)
							);
			}
		}
		
		insert results;
	}
	
	
	/**
	 * @description	Find the subscriptions that belong to the given feeds and subscribers and map them in the
	 *				combination of feed id and subscriber id
	 * @author 		Dimitrios Sgourdos
	 * @date		Created: 09-Sept-2015
	 * @param		source    The wrapper data that keeps possible subscriptions
	 * @return		The mapped subscriptions
	*/
	@testVisible static private Map<String, EntitySubscription> mapSubscriptionsOnSubscriberAndFeed(
													List<NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper> source) {
		// Check for invalid parameters
		Map<String, EntitySubscription> results = new Map<String, EntitySubscription>();
		
		if(source == NULL || source.isEmpty()) {
			return results;
		}
		
		// Find the possible existing subscriptions and map them in the combination of feed id and subscriber id
		Set<Id> feedIds = new Set<Id>();
		Set<Id> userIds = new Set<Id>();
		
		for(NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper tmpWrapperItem : source) {
			feedIds.add(tmpWrapperItem.feedId);
			userIds.add(tmpWrapperItem.subscriberId);
		}
		
		List<EntitySubscription> subscriptionsList = NCM_Chatter_DataAccessor.getSubscriptionsByFeedSetAndUserSet(
																											feedIds,
																											userIds);
		
		for(EntitySubscription tmpItem : subscriptionsList) {
			results.put( '' + tmpItem.ParentId + ':' + tmpItem.SubscriberId, tmpItem);
		}
		
		return results;
	}
}