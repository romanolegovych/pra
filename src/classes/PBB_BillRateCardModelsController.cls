public with sharing class PBB_BillRateCardModelsController {

	/**
	* to display the list of Bill Rate Card Models 
	*/
	public List<Bill_Rate_Card_Model__c> BRModelsList    { get; set; }

	/** 
       @description Constructor
       @author Tkachenko Oleksiy
       @date 2015   
   */
	public PBB_BillRateCardModelsController() {
		BRModelsList = PBB_DataAccessor.getAllBillModels();
	}

	/** 
        @description Function to call New Page of required Model
        @author Tkachenko Oleksiy
        @date 2015  
   */
   public PageReference getNewPageReference() {
       Schema.DescribeSObjectResult R;
       R = Bill_Rate_Card_Model__c.SObjectType.getDescribe();
       return new PageReference('/' + R.getKeyPrefix() + '/e');
   }
}