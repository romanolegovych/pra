public with sharing class CC_Clone implements STSWR1.FlowPostImportInterface//Uncommend prior to deployment
{
	private Map<String, String> oldNewFlowIDMap;
	private Map<ID, ID> oldNewChainIDMap = new Map<ID, ID>();
	
	private Map<ID, Critical_Chain__c> oldChainIDNewChainMap = new Map<ID, Critical_Chain__c>();
	
	private Map<String, STSWR1__Flow_Step_Junction__c> stepsMap = new Map<String, STSWR1__Flow_Step_Junction__c>();
	private List<String> affectedFlowIDs = new List<String>();
	
	public void execute(Map<String, String> oldNewFlowIdMap)
	{
		//futureExecute(oldNewFlowIdMap);
		this.oldNewFlowIDMap = oldNewFlowIdMap;
		cloneChains();
	}
	
	public void executeLater(Map<String, String> oldNewFlowIdMap)
	{
		
		this.oldNewFlowIDMap = oldNewFlowIdMap;
		cloneChains();
	}
	
	@future
	public static void futureExecute(Map<String, String> oldNewFlowIdMap)
	{
		new CC_Clone().executeLater(oldNewFlowIdMap);
	}
	
	public void cloneChains()
	{
		System.Savepoint transactionSavepoint = Database.setSavepoint();
		try
		{
			affectedFlowIDs.addAll(oldNewFlowIDMap.values());
			for (Critical_Chain__c oldChain : [select Flow__c, Flow_Instance__c, Id, Milestone__c, Name from Critical_Chain__c where Flow__c in :oldNewFlowIDMap.keyset() and Flow__c != null])
			{
				Critical_Chain__c newChain = oldChain.clone(false, true);
				newChain.Flow__c = oldNewFlowIDMap.get(oldChain.Flow__c);
				newChain.Cloned_From_Early_Warning__c = oldChain.Id;
				
				oldChainIDNewChainMap.put(oldChain.Id, newChain);
				//affectedFlowIDs.add(newChain.Flow__c);
			}
			
			insert oldChainIDNewChainMap.values();
			cloneSteps();
		}
		catch (Exception e)
		{
			System.debug(System.LoggingLevel.ERROR, 'Exception caught: ' + e.getMessage() + '\n' + e.getStacktraceString());
			Database.rollback(transactionSavepoint);
		}
	}
	
	
	//private List<ID> buildRecordIDs(List<Critical_Chain__c> chains)
	//{
	private List<ID> buildRecordIDs(List<String> flowIDs)
	{
		List<ID> result = new List<ID>();
		/*
		for (Critical_Chain__c each : [select Flow__r.STSWR1__Source_ID__c from Critical_Chain__c where Id in :chains])
		{
			result.add(each.Flow__r.STSWR1__Source_ID__c);
		}
		/**/
		
		for (STSWR1__Flow__c each : [select STSWR1__Source_ID__c from STSWR1__Flow__c where Id in :flowIDs])
		{
			result.add((ID) each.STSWR1__Source_ID__c);
		}
		
		for (PAWS_Project_Flow_Site__c each : [select PAWS_Country__c from PAWS_Project_Flow_Site__c where Id in :result])
		{
			result.add(each.PAWS_Country__c);
		}
		
		for (PAWS_Project_Flow_Country__c each : [select PAWS_Project__c from PAWS_Project_Flow_Country__c where Id in :result])
		{
			result.add(each.PAWS_Project__c);
		}
		
		return result;
	}
	
	private Map<String, String> buildOldNewFlowIdMap(List<ID> recordIDs)
	{
		Map<String, String> oldNewFlowIdMap = new Map<String, String>();
		
		for (STSWR1__Flow__c each : [select STSWR1__Parent__c from STSWR1__Flow__c where STSWR1__Source_ID__c in :recordIDs])
		{
			oldNewFlowIdMap.put(each.STSWR1__Parent__c, each.Id);
		}
		
		return oldNewFlowIdMap;
	}
	
	/*
	private Map<ID, ID> buildOldNewChainIdMap(List<ID> recordIDs)
	{
		Map<ID, ID> oldNewChainIDMap = new Map<ID, ID>();
		for (Critical_Chain__c each : [select Cloned_From_Early_Warning__c from Critical_Chain__c where STSWR1_Flow__r.STSWR1__Source_ID__c in :])
		
		return oldNewChainIDMap;
	}/**/
	
	private void cloneSteps()
	{
		List<ID> chainFlowIDs = new List<ID>();
		//oldNewFlowIdMap.putAll(buildOldNewFlowIdMap(buildRecordIDs(oldChainIDNewChainMap.values())));
		oldNewFlowIdMap.putAll(buildOldNewFlowIdMap(buildRecordIDs(oldNewFlowIdMap.values())));
		
		//for (STSWR1__Flow_Step_Junction__c step : [select STSWR1__Flow__c, STSWR1__Index__c from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__c in :affectedFlowIDs])
		for (STSWR1__Flow_Step_Junction__c step : [select STSWR1__Flow__c, STSWR1__Index__c from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__c in :oldNewFlowIdMap.values()])
		{
			stepsMap.put(buildHash(step), step);
		}
		
		//Adding EWs which has been created before and we only need to add steps to existing EW
		for (Critical_Chain__c each : [select Cloned_From_Early_Warning__c, (select Flow_Step__c from Step_Junctions__r) from Critical_Chain__c where Flow__c in :oldNewFlowIdMap.values()])
		{
			oldChainIDNewChainMap.put(each.Cloned_From_Early_Warning__c, each);
		}
		
		List<Critical_Chain_Step_Junction__c> newCCSteps = new List<Critical_Chain_Step_Junction__c>();
		for (Critical_Chain_Step_Junction__c ccStep : [select Buffer_Percentage__c, Buffer_Time__c, Challenging_Percentage__c, Challenging_Time__c, Critical_Chain__c, Duration__c,
				Flow_Step__r.STSWR1__Flow__c, Flow_Step__r.STSWR1__Index__c,
				Flow_Step__c, Remaining_Time__c, Used_Time__c from Critical_Chain_Step_Junction__c
				//where Critical_Chain__c in :oldChainIDNewChainMap.keyset()])
				where Critical_Chain__r.Flow__c in :oldNewFlowIdMap.keyset()])
		{
			if (stepsMap.get(oldNewFlowIDMap.get(ccStep.Flow_Step__r.STSWR1__Flow__c) + '' + ccStep.Flow_Step__r.STSWR1__Index__c) == null)
			{
				continue;
			}
			
			
			Critical_Chain__c newChain = oldChainIDNewChainMap.get(ccStep.Critical_Chain__c);
			ID stepID = stepsMap.get(oldNewFlowIDMap.get(ccStep.Flow_Step__r.STSWR1__Flow__c) + '' + ccStep.Flow_Step__r.STSWR1__Index__c).Id;
			
			Boolean junctionExists = false;
			for (Critical_Chain_Step_Junction__c existingStep : newChain.Step_Junctions__r)
			{
				if (existingStep.Flow_Step__c == stepID)
				{
					junctionExists = true;
					break;
				}
			}
			
			if (junctionExists)
			{
				continue;
			}
			
			Critical_Chain_Step_Junction__c newCCStep = ccStep.clone(false, true);
			newCCStep.Critical_Chain__c = newChain.Id;
			newCCStep.Flow_Step__c = stepID;
			
			//newCCStep.Critical_Chain__c = oldChainIDNewChainMap.get(ccStep.Critical_Chain__c).Id;
			//newCCStep.Flow_Step__c = stepsMap.get(oldNewFlowIDMap.get(ccStep.Flow_Step__r.STSWR1__Flow__c) + '' + ccStep.Flow_Step__r.STSWR1__Index__c).Id;
			
			newCCSteps.add(newCCStep);
		}
		
		insert newCCSteps;
	}
	
	private String buildHash(STSWR1__Flow_Step_Junction__c step)
	{
		return step.STSWR1__Flow__c + '' + step.STSWR1__Index__c;
	}
}