public class LMS_PersonToRoleController {
	
	public String val{get;private set;}
    public Boolean renderPSTTab{get;private set;}
    public Boolean renderAdhocTab{get;private set;}
	
	public LMS_PersonToRoleController(){
		val='PRA';
		checkPermissions();
	}
    
    private void checkPermissions() {
		renderPSTTab = true;
		renderAdhocTab = true;
		
		String profileName = LMS_ToolsDataAccessor.getProfileNameFromId(UserInfo.getProfileId());
		Boolean isPSTAdmin = LMS_ToolsDataAccessor.isPSTAdmin(profileName);
		Boolean isAdhocAdmin = LMS_ToolsDataAccessor.isAdhocAdmin(profileName);
		Boolean isPRAAdhocAdmin = LMS_ToolsDataAccessor.isPRAAdhocAdmin(profileName);
		
		if(isPRAAdhocAdmin) {
			renderPSTTab = false;
		} else if(isPSTAdmin) {
			renderAdhocTab = false;
		} else if(isAdhocAdmin) {
			renderPSTTab = false;
		}
    }
}