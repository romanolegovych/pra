/** Implements the test for the Selector Layer of the object Study__c
 * @author	Dimitrios Sgourdos
 * @version	18-Nov-2013
 */
@isTest
private class StudyDataAccessorTest {
	
	// Global variables
	private static List<Study__c> 					 studiesList;
	private static List<LaboratoryMethodCompound__c> compoundList;
	
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	09-Oct-2013
	 */
	static void init() {
		// Create project
		Client_Project__c firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		
		// Create studies
		studiesList = BDT_TestDataUtils.buildStudies(firstProject,3);
		insert studiesList;
	}
	
	
	/** Initialize laboratory compounds
	 * @author	Dimitrios Sgourdos
	 * @version	18-Nov-2013
	 */
	static void initCompounds() {
		// Create Laboratory Methods
		List<LaboratoryMethod__c> lmList = new  List<LaboratoryMethod__c>();
		lmList.add(new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
											Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood', 
											Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle'));
		lmList.add(new LaboratoryMethod__c (Name='PRA-NL-LML-0002', AnalyticalTechnique__c='ELISA', AntiCoagulant__c='N/A',
											Department__c='SML', Detection__c='N/A', Location__c='PRA-US', Matrix__c='Breast milk', 
											Proprietary__c='N', ShowOnMethodList__c='Y', Species__c='Human'));
		insert lmList; 
		
		// Create Laboratory Method Compounds
		compoundList = new List<LaboratoryMethodCompound__c>();
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[0], 'midazolam') );
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[0], '4-OH') );
		insert compoundList;
	}
	
	
	/** Test function getByStudyIds
	 * @author	Dimitrios Sgourdos
	 * @version	23-Oct-2013
	 */
	static testMethod void getByStudyIds() {
		init();
		
		// Add all the studies except the last one from studiesList to the listOfStudiesIds
		List<Study__c> tmpStudiesList = new List<Study__c>();
		tmpStudiesList.addAll(studiesList);
		tmpStudiesList.remove(tmpStudiesList.size()-1); 
		String listOfStudiesIds = BDT_Utils.createConcatinatedStringFromSobject(tmpStudiesList, 'Id');
		
		// Check function
		List<Study__c> retrievedStudiesList = StudyDataAccessor.getByStudyIds(listOfStudiesIds);
		system.assertEquals(retrievedStudiesList.size(), studiesList.size() - 1);
	}
	
	
	/** Test function getByUserPreference
	 * @author	Dimitrios Sgourdos
	 * @version	23-Oct-2013
	 */
	static testMethod void getByUserPreferenceTest() {
		init();
		
		// Add all the studies except the last one from studiesList to the listOfStudiesIds
		List<Study__c> tmpStudiesList = new List<Study__c>();
		tmpStudiesList.addAll(studiesList);
		tmpStudiesList.remove(tmpStudiesList.size()-1); 
		String listOfStudiesIds = BDT_Utils.createConcatinatedStringFromSobject(tmpStudiesList, 'Id');
		
		// Check function
		BDT_Utils.setPreference('SelectedStudies', listOfStudiesIds);
		List<Study__c> retrievedStudiesList = StudyDataAccessor.getByUserPreference();
		system.assertEquals(retrievedStudiesList.size(), studiesList.size() - 1);
	}
	
	
	/** Test function getByProjectId
	 * @author	Dimitrios Sgourdos
	 * @version	05-Nov-2013
	 */
	static testMethod void getByProjectIdTest() {
		init();
		String projectId = studiesList[0].Project__c;
		
		// Check function
		List<Study__c> retrievedStudiesList = StudyDataAccessor.getByProjectId(projectId);
		system.assertEquals(retrievedStudiesList.size(), studiesList.size());
		
		// Disable first study
		studiesList[0].BDTDeleted__c = true;
		upsert studiesList[0];
		
		retrievedStudiesList = StudyDataAccessor.getByProjectId(projectId);
		system.assertEquals(retrievedStudiesList.size(), studiesList.size()-1);
	}
	
	
	/** Test function getByUsedInAN
	 * @author	Dimitrios Sgourdos
	 * @version	18-Nov-2013
	 */
	static testMethod void getByUsedInANTest() {
		init();
		initCompounds();
		
		// Create analysis
		List<LaboratoryAnalysis__c> analysisList = new List<LaboratoryAnalysis__c>();
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[0], false,  false, false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[1], false, false, false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[1], true,  false, false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[2], true,  false, false));
		insert analysisList;
		
		// Query for the first two studies
		List<Study__c> tmpStudiesList = new List<Study__c>();
		tmpStudiesList.add(studiesList[0]);
		tmpStudiesList.add(studiesList[1]);
		String listOfStudiesIds = BDT_Utils.createConcatinatedStringFromSobject(tmpStudiesList, 'Id');
		List<Study__c> results = StudyDataAccessor.getByUsedInAN(listOfStudiesIds);
		
		// Only the second of the first two studies has a LaboratoryAnalysis__c record with Analysis__c = true
		system.assertEquals(results.size(), 1);
		system.assertEquals(results[0].Id, tmpStudiesList[1].Id);
	}
	
	
	/** Test function getByUsedInMdOrVal
	 * @author	Dimitrios Sgourdos
	 * @version	18-Nov-2013
	 */
	static testMethod void getByUsedInMdOrValTest() {
		init();
		initCompounds();
		
		// Create analysis
		List<LaboratoryAnalysis__c> analysisList = new List<LaboratoryAnalysis__c>();
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[0], false, false, false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[1], false, false, false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[1], false, true,  false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[2], false, false, true));
		insert analysisList;
		
		// Query 
		String listOfStudiesIds = BDT_Utils.createConcatinatedStringFromSobject(studiesList, 'Id');
		List<Study__c> results = StudyDataAccessor.getByUsedInMdOrVal(listOfStudiesIds);
		
		// Only the last two studies have a LaboratoryAnalysis__c record with MethodDevelopment__c or MethodValidation__c equal to true
		system.assertEquals(results.size(), 2);
		system.assertEquals(results[0].Id, studiesList[1].Id);
		system.assertEquals(results[1].Id, studiesList[2].Id);
	}
	
	
	/** Test function getByUsedInMd
	 * @author	Dimitrios Sgourdos
	 * @version	18-Nov-2013
	 */
	static testMethod void getByUsedInMdTest() {
		init();
		initCompounds();
		
		// Create analysis
		List<LaboratoryAnalysis__c> analysisList = new List<LaboratoryAnalysis__c>();
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[0], false, false, false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[1], false, false, false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[1], false, true,  false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[2], false, true, false));
		insert analysisList;
		
		// Query 
		String listOfStudiesIds = BDT_Utils.createConcatinatedStringFromSobject(studiesList, 'Id');
		List<Study__c> results = StudyDataAccessor.getByUsedInMd(listOfStudiesIds);
		
		// Only the last two studies have a LaboratoryAnalysis__c record with MethodDevelopment__c equal to true
		system.assertEquals(results.size(), 2);
		system.assertEquals(results[0].Id, studiesList[1].Id);
		system.assertEquals(results[1].Id, studiesList[2].Id);
	}
	
	
	/** Test function getByUsedInVal
	 * @author	Dimitrios Sgourdos
	 * @version	18-Nov-2013
	 */
	static testMethod void getByUsedInValTest() {
		init();
		initCompounds();
		
		// Create analysis
		List<LaboratoryAnalysis__c> analysisList = new List<LaboratoryAnalysis__c>();
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[0], false, false, false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[1], false, false, false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[1], false, false, true));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[2], false, false, true));
		insert analysisList;
		
		// Query 
		String listOfStudiesIds = BDT_Utils.createConcatinatedStringFromSobject(studiesList, 'Id');
		List<Study__c> results = StudyDataAccessor.getByUsedInVal(listOfStudiesIds);
		
		// Only the last two studies have a LaboratoryAnalysis__c record with MethodValidation__c equal to true
		system.assertEquals(results.size(), 2);
		system.assertEquals(results[0].Id, studiesList[1].Id);
		system.assertEquals(results[1].Id, studiesList[2].Id);
	}
}