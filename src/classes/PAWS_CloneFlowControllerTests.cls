/**
*   'PAWS_CloneFlowControllerTests' is the test class for PAWS_CloneFlowController
*   Test classes are used to write the unit test and cover the code coverage of the code.
*   @author   Work Relay
*/
@isTest 
public with sharing class PAWS_CloneFlowControllerTests 
{
	static testmethod void run()
    {
    	PAWS_ApexTestsEnvironment.init();
    	
    	PageReference ref = Page.PAWS_CloneFlow;
    	ref.getParameters().put('sourceId', PAWS_ApexTestsEnvironment.Project.Id);
    	ref.getParameters().put('folderId', PAWS_ApexTestsEnvironment.ProjectFlow.Folder__c);
    	Test.setCurrentPage(ref);
    	
    	STSWR1__Flow__c temp = new STSWR1__Flow__c(Name='Test Flow', STSWR1__Object_Type__c='Stub__c', STSWR1__Status__c = 'Active');
    	insert temp;
    	
    	Map<String, Object> config = new Map<String, Object>();
        config.put('flowId', temp.Id);
    	config.put('flowObjectName', 'Stub__c');  	
    	
    	PAWS_ApexTestsEnvironment.FlowStepAction.STSWR1__Type__c = 'Start Sub Flow';
    	PAWS_ApexTestsEnvironment.FlowStepAction.STSWR1__Status__c = 'Active';
    	PAWS_ApexTestsEnvironment.FlowStepAction.STSWR1__Config__c = JSON.serialize(config);
    	update PAWS_ApexTestsEnvironment.FlowStepAction;
    	
    	PAWS_CloneFlowController controller = new PAWS_CloneFlowController();
    	controller.TemplateFlowId = PAWS_ApexTestsEnvironment.Flow.Id;
    	
    	System.assert(controller.TemplateFlows != null);
    	System.assert(controller.ParentFolderId != null);
    	
    	controller.beforeClone();
    	controller.cloneFlow();
    	controller.afterClone();
    	controller.rollBack();
    	
    	controller.fillRolesAndSwimlaneLookups();
    }
}