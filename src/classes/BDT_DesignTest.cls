@isTest
private class BDT_DesignTest {
	
	public static Client_Project__c firstProject {get;set;}
	public static List<Study__c> studiesList {get;set;}
	public static List<ClinicalDesign__c> designList {get;set;}
	public static List<Business_Unit__c> myBUList  {get;set;}
	
	static void init(){
		firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		
		myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert myBUList;
		
		studiesList = BDT_TestDataUtils.buildStudies(firstProject);
		studiesList[0].Business_Unit__c = myBUList[0].Id; 
		studiesList[1].Business_Unit__c = myBUList[1].Id; 
		studiesList[2].Business_Unit__c = myBUList[2].Id;
		studiesList[3].Business_Unit__c = myBUList[3].Id; 
		studiesList[4].Business_Unit__c = myBUList[4].Id;
		insert studiesList;
		
		designList = BDT_TestDataUtils.buildClinicDesign(firstProject); 
		insert designList;
		
		//create population
		List<Population__c> population = BDT_TestDataUtils.buildPopulation(firstProject.id, 4);
		insert population;

		// add populations to study
		List<PopulationStudyAssignment__c> spa = new List<PopulationStudyAssignment__c>();
		spa.add(new PopulationStudyAssignment__c(Population__c = population[0].id,
												 Study__c = studiesList[0].Id));
        spa.add(new PopulationStudyAssignment__c(Population__c = population[1].id,
												 Study__c = studiesList[0].Id));
		upsert spa;										 				  
		
		
		BDT_Utils.setPreference('SelectedProject', String.valueOf(firstProject.id));
		String studyList;
		For (Study__c study:studiesList) {
			studyList = studyList + ':' + String.valueOf(study.id);
		}
		studyList = studyList.substringAfter(':');
		BDT_Utils.setPreference('SelectedStudies',studyList);
	}
	
	static testMethod void createDesignPath() { 
		init();
		
		BDT_Design Design = new BDT_Design();
		
		// select and deselect designs for studies
		Design.designWrapperList[0].studyWrapperList[0].selected = true;
		Design.designWrapperList[0].studyWrapperList[1].selected = true;
		Design.saveDesignWrapper();
		Design.designWrapperList[0].studyWrapperList[1].selected = false;
		Design.designWrapperList[0].studyWrapperList[3].selected = true;	
		Design.saveDesignWrapper();			
		
		
		// display the design
		PageReference b = new Pagereference( system.Page.BDT_design.getUrl());
		b.getParameters().put('designId',Design.designWrapperList[0].designRecord.id);
		Test.setCurrentPage(b);
		Design.showClinicDesign();
		Design.hideDesignContainer();
		
		// Add population
		Design.saBody[0].saBodyInt[0].checkboxChecked = true;		
		Design.saveStudyPopulation();
		Design.saBody[0].saBodyInt[0].checkboxChecked = false;		
		Design.saveStudyPopulation();
		
		
		// fire off some links
		Design.EditGroupArmAssignment();
		Design.editClinicDesign();
	}
	
	
	/** The test is only for code coverage for the Lab design functions as in controller there are 
	 *	only calls to functions	from other classes that are already tested.
	 * @author	Dimitrios Sgourdos
	 * @version	13-Nov-2013
	 */
	static testMethod void labDesignsFunctionsTest() {
		// Create project
		Client_Project__c tmpProject = BDT_TestDataUtils.buildProject(); 
		insert tmpProject;
		// Create studies
		List<Study__c> tmpStudiesList = BDT_TestDataUtils.buildStudies(tmpProject,1);
		insert tmpStudiesList;
		BDT_Utils.setPreference('SelectedProject', String.valueOf(tmpProject.id));
		String listOfStudies = '';
		For (Study__c study : tmpStudiesList) {
			listOfStudies += ':' + String.valueOf(study.id);
		}
		listOfStudies = listOfStudies.removeStart(':');
		BDT_Utils.setPreference('SelectedStudies',listOfStudies);
		// Create the controller
		BDT_Design p = new BDT_Design();
		// Lab design Ia
		p.addShipmentToLabDesign();
		p.addSampleIndex = '0';
		p.addSampleToLabDesign();
		p.removeSampleIndex = '0';
		p.removeSampleFromLabDesign();
		p.resetSampleDesign();
		p.saveSampleDesign();
		// Lab design Ib
		p.addSlotAnDesign();
		p.addAnSlotLineIndex = '0';
		p.addSlotCombinationAnDesign();
		p.selectedSlotIndex = '0';
		p.selectShipments();
		p.saveSelectShipments();
		p.cancelSelectShipments();
		p.removeAnSlotLineIndex = '0';
		p.removeSlotCombinationAnDesign();
		p.calculateSummary();
		p.resetSlotAnDesign();
		p.saveSlotAnDesign();
		// Lab design II
		p.addSlotMdAndValDesign();
		p.addMdAndValSlotLineIndex = '0';
		p.addSlotCombinationMdAndValDesign();
		p.removeMdAndValSlotLineIndex = '0';
		p.removeSlotCombinationMdAndValDesign();
		p.resetSlotMdAndValDesign();
		p.saveSlotMdAndValDesign();
	}
}