/** Implements the test for the Service Layer of the object FlowchartServiceTotal__c
 * @author	Dimitrios Sgourdos
 * @version	03-Feb-2014
 */
@isTest
private class FlowchartServiceTotalServiceTest {
	
	/** Test the function deserializeFromStringToExplanationJSONWrapperList.
	 * @author	Dimitrios Sgourdos
	 * @version	03-Feb-2014
	 */
	static testMethod void deserializeFromStringToExplanationJSONWrapperListTest() {
		// Create data
		String jsonValue = '[{"totalunits":8,"timepoints":1,"populationName":"Diabetic Patients","groupsize":8,';
		jsonValue += '"groupname":"A","flowchartname":"Fc 1","epochname":"Epoch 1","designname":"Test design","armname":"Arm 1"},';
		
		jsonValue += '{"totalunits":16,"timepoints":2,"populationName":"Diabetic Patients","groupsize":8,';
		jsonValue += '"groupname":"A","flowchartname":"Fc 1","epochname":"Epoch 2","designname":"Test design","armname":"Arm 2"}]';
		
		String errorMessage = 'Error in deserializing the json';
		
		// Check the function with valid data
		List<FlowchartServiceTotalService.ExplanationJSONWrapper> results =
					FlowchartServiceTotalService.deserializeFromStringToExplanationJSONWrapperList(jsonValue);
		
		system.assertEquals(results.size(), 2, errorMessage);
		system.assertEquals(results[0].timepoints, 1, errorMessage);
		system.assertEquals(results[1].timepoints, 2, errorMessage);
		
		// Check the function with invalid data
		jsonValue = 'testing';
		
		results = FlowchartServiceTotalService.deserializeFromStringToExplanationJSONWrapperList(jsonValue);
		
		system.assertEquals(results.size(), 0, errorMessage);
	}
	
	
	/** ExplanationJSONWrapper Class.
	 * This function is only for the code coverage
	 * @author	Dimitrios Sgourdos
	 * @version	03-Feb-2014
	 */
	static testMethod void ExplanationJSONWrapperTest() {
		FlowchartServiceTotalService.ExplanationJSONWrapper result = new FlowchartServiceTotalService.ExplanationJSONWrapper();
	}
}