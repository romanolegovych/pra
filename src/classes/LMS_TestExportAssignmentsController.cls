/**
 * Test for ExportAssignments controller
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestExportAssignmentsController {
    
    static List<LMSConstantSettings__c> constants;
    static List<CourseDomainSettings__c> domains;
    static List<Job_Class_Desc__c> jobClasses;
    static List<Job_Title__c> jobTitles;
    static List<PRA_Business_Unit__c> businessUnits;
    static List<Department__c> departments;
    static List<Region__c> regions;
    static List<Country__c> countries;
    static List<Employee_Type__c> empTypes;
    static List<Employee_Status__c> empStatus;
    static List<Job_Class_Desc__c> projectRoles;
    static List<WFM_Client__c> clients;
    static List<WFM_Project__c> projects;
    static List<WFM_Contract__c> contracts;
    static List<Role_Type__c> roleTypes;
    static List<Employee_Details__c> employees;
    static List<LMS_Role__c> roles;
    static List<Course_Domain__c> courseDomains;
    static List<LMS_Course__c> courses;
    static List<LMS_Role_Course__c> roleCourses;
    static List<LMS_Role_Employee__c> roleEmps;
    
    static void init(){
        
        constants = new LMSConstantSettings__c[] {
        	new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
    		new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
    		new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
    		new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D'),
    		new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
    		new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
    		new LMSConstantSettings__c(Name = 'typePRA', Value__c = 'PRA'),
    		new LMSConstantSettings__c(Name = 'typeProject', Value__c = 'Project Specific'),
    		new LMSConstantSettings__c(Name = 'typeAdhoc', Value__c = 'Additional Role'),
    		new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
    		new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
    		new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
    		new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
    		new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
    		new LMSConstantSettings__c(Name = 'statusToDelete', Value__c = 'Delete'),
    		new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended'),
    		new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
    		new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
    		new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
    		new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete')
        };
        insert constants;
        
        domains = new CourseDomainSettings__c[] {
        	new CourseDomainSettings__c(Name='Archive', Domain_Id__c = 'Archive')
        };
        insert domains;
        
        jobClasses = new Job_Class_Desc__c[]{
            new Job_Class_Desc__c(Name = 'class1', Job_Class_Code__c = 'code1', Job_Class_ExtID__c = 'class1')
        };
        insert jobClasses;
        
        jobTitles = new Job_Title__c[]{
            new Job_Title__c(Job_Code__c = 'Code1', Job_Title__c = 'title1', Job_Class_Desc__c = jobClasses[0].Id, Status__c = 'A')
        };
        insert jobTitles;
        
        businessUnits = new PRA_Business_Unit__c[]{
            new PRA_Business_Unit__c(Name = 'unit', Business_Unit_Code__c = 'BUC', Status__c = 'A')
        };
        insert businessUnits;
        
        departments = new Department__c[]{
            new Department__c(Name = 'department', Department_Code__c = 'DEPTC', Status__c = 'A')
        };
        insert departments;
        
        regions = new Region__c[]{
            new Region__c(Region_Name__c = 'region1', Region_Id__c = 1, Status__c = 'A')
        };
        insert regions;
        
        countries = new Country__c[]{
            new Country__c(Name = 'country1', Region_Name__c = 'region1', Country_Code__c = 'C1', PRA_Country_ID__c = '100')
        };
        insert countries;
        
        empTypes = new Employee_Type__c[]{
            new Employee_Type__c(Name = 'type1')
        };
        insert empTypes;
        
        empStatus = new Employee_Status__c[]{
            new Employee_Status__c(Employee_Status__c = 'AA', Employee_Type__c = empTypes[0].Id)
        };
        insert empStatus;
        
        projectRoles = new Job_Class_Desc__c[]{
            new Job_Class_Desc__c(Name = 'projectRole', Job_Class_Code__c = 'pcod1', Job_Class_ExtID__c = 'projectRole', Is_Assign__c = true)
        };
        insert projectRoles;
        
        clients = new WFM_Client__c[]{
            new WFM_Client__c(Name = 'Client1', Client_Unique_Key__c = 'Client1')
        };
        insert clients;
        
        contracts = new WFM_Contract__c[]{
            new WFM_Contract__c(Name = 'contract1', Contract_Unique_Key__c = 'contract1', Client_Id__c = clients[0].Id)
        };
        insert contracts;
        
        projects = new WFM_Project__c[]{
            new WFM_Project__c(Name = 'Project1', Project_Unique_Key__c = 'Project1', Contract_Id__c = contracts[0].Id)
        };
        insert projects;
        
        roleTypes = new Role_Type__c[]{
            new Role_Type__c(Name = 'PRA'),
            new Role_Type__c(Name = 'Project'),
            new Role_Type__c(Name = 'Adhoc')
        };
        insert roleTypes;
        
        roles = new LMS_Role__c[]{
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = jobTitles[0].Id, Business_Unit__c = businessUnits[0].Id,
                            Department__c = departments[0].Id, Region__c = regions[0].Id, Country__c = countries[0].Id,
                            Employee_Type__c = empTypes[0].Id, Status__c = 'Active', Sync_Status__c = 'N'),
            new LMS_Role__c(Project_Role__c = projectRoles[0].Id, Project_Id__c = projects[0].Id, Client_Id__c = clients[0].Id, Status__c = 'Active', Sync_Status__c = 'N'),
            new LMS_Role__c(Adhoc_Role_Name__c = 'TestRole1', Status__c = 'Active', Sync_Status__c = 'N')
        };
        insert roles;
        
        courseDomains = new Course_Domain__c[]{
            new Course_Domain__c(Domain__c = 'Internal'),
            new Course_Domain__c(Domain__c = 'Archive'),
            new Course_Domain__c(Domain__c = 'Project Specific')
        };
        insert courseDomains;
        
        courses = new LMS_Course__c[]{
            new LMS_Course__c(Offering_Template_No__c = 'course123', Course_Code__c = 'C123', Title__c = 'Course 123', Domain_Id__c = courseDomains[0].Id,
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse1',
                              Discontinued_From__c = Date.today()-1),
            new LMS_Course__c(Offering_Template_No__c = 'course321', Course_Code__c = 'C124', Title__c = 'Course 321', Domain_Id__c = courseDomains[1].Id,
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse2',
                              Discontinued_From__c = Date.today()+1, Project_Id__c = 'Project1', Client_Id__c = 'Client1'),
            new LMS_Course__c(Offering_Template_No__c = 'course231', Course_Code__c = 'C231', Title__c = 'Course 231', Domain_Id__c = courseDomains[0].Id,
                              Type__c = 'WBT', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse3',
                              Discontinued_From__c = Date.today()-1)
        };
        insert courses;
        
        employees = new Employee_Details__c[]{
            new Employee_Details__c(Name = 'employee1', Employee_Unique_Key__c = 'employee1', First_Name__c = 'Andrew', Last_Name__c = 'Allen', Email_Address__c = 'a@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU'),
            new Employee_Details__c(Name = 'employee2', Employee_Unique_Key__c = 'employee2', First_Name__c = 'Andrew', Last_Name__c = 'Allen', Email_Address__c = 'a@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU'),
            new Employee_Details__c(Name = 'employee3', Employee_Unique_Key__c = 'employee3', First_Name__c = 'Andrew', Last_Name__c = 'Allen', Email_Address__c = 'a@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU')
        };
        insert employees;
        
        roleCourses = new LMS_Role_Course__c[]{
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Draft Delete', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[2].Id, Course_Id__c = courses[2].Id, Sync_Status__c = 'N', Status__c = 'Committed', Assigned_By__c = 'ASA')
        };
        insert roleCourses;
        
        roleEmps = new LMS_Role_Employee__c[]{
            new LMS_Role_Employee__c(Role_Id__c = roles[0].Id, Employee_Id__c = employees[0].Id, Status__c = 'Draft Delete', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today()),
            new LMS_Role_Employee__c(Role_Id__c = roles[1].Id, Employee_Id__c = employees[1].Id, Status__c = 'Committed', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today()),
            new LMS_Role_Employee__c(Role_Id__c = roles[2].Id, Employee_Id__c = employees[2].Id, Status__c = 'Draft', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today())
        };
        insert roleEmps;
    }
    
    static testMethod void testCourseListing(){
        init();
        PageReference pr = new PageReference('/apex/LMS_ExcelCourseAssignments?rCid=' + roles[0].Id);
        Test.setCurrentPage(pr);
        
        LMS_ExportAssignmentsController c = new LMS_ExportAssignmentsController();
        LMS_Role__c role = [SELECT Role_Name__c FROM LMS_Role__c where Id =:roles[0].Id];
        String roleName;
		roleName = role.Role_Name__c;
            
        System.debug('------------------------'+c.title);
        System.assert(c.title == '"Course Listing for Role ' + roleName + '"');
        System.assert(c.courses.size() == 1 && c.typeCourse == true && c.typeEmployee == false && c.typeRole == false);
    }
    
    static testMethod void testRoleListingPRA(){
        init();
        PageReference pr = new PageReference('/apex/LMS_ExcelCourseAssignments?cRid=' + roleCourses[0].Course_Id__c+'&type=1');
        Test.setCurrentPage(pr);
        
        LMS_ExportAssignmentsController c = new LMS_ExportAssignmentsController();
        LMS_Course__c course = [SELECT Title__c FROM LMS_Course__c where Id =:roleCourses[0].Course_Id__c];
        String courseName;
        courseName = course.Title__c;
            
        System.assert(c.title == '"PRA Role Listing for Course ' + courseName + '"');
        System.assert(c.typeCourse == false && c.typeEmployee == false && c.typeRole == true && c.isPRA == true);
    }
    
    static testMethod void testRoleListingPST(){
        init();
        PageReference pr = new PageReference('/apex/LMS_ExcelCourseAssignments?cRid=' + roleCourses[1].Course_Id__c+'&type=2');
        Test.setCurrentPage(pr);
        
        LMS_ExportAssignmentsController c = new LMS_ExportAssignmentsController();
        LMS_Course__c course = [SELECT Title__c FROM LMS_Course__c where Id =:roleCourses[1].Course_Id__c];
        String courseName;
        courseName = course.Title__c;
            
        System.assert(c.title == '"Project Specific Role Listing for Course ' + courseName + '"');
        System.assert(c.typeCourse == false && c.typeEmployee == false && c.typeRole == true && c.isPST == true);
    }
    
    static testMethod void testRoleListingAdhoc(){
        init();
        PageReference pr = new PageReference('/apex/LMS_ExcelCourseAssignments?cRid=' + roleCourses[2].Course_Id__c+'&type=3');
        Test.setCurrentPage(pr);
        
        LMS_ExportAssignmentsController c = new LMS_ExportAssignmentsController();
        LMS_Course__c course = [SELECT Title__c FROM LMS_Course__c where Id =:roleCourses[2].Course_Id__c];
        String courseName;
        courseName = course.Title__c;
            
        System.assert(c.title == '"Additional Role Listing for Course ' + courseName + '"');
        System.assert(c.typeCourse == false && c.typeEmployee == false && c.typeRole == true && c.isAdhoc == true);
    } 
    
    static testMethod void testEmployeeListing(){
        init();
        PageReference pr = new PageReference('/apex/LMS_ExcelCourseAssignments?rEid=' + roleEmps[2].Role_Id__c);
        Test.setCurrentPage(pr);
        
        LMS_ExportAssignmentsController c = new LMS_ExportAssignmentsController();
        LMS_Role__c role = [SELECT Role_Name__c FROM LMS_Role__c where Id =:roleEmps[2].Role_Id__c];
        String roleName;
        roleName = role.Role_Name__c;
        
        System.assert(c.title == '"Employee Listing for Role ' + roleName + '"');
        System.assert(c.typeCourse == false && c.typeEmployee == true && c.typeRole == false);
    }
    
    static testMethod void testCourseToEmployeeListingPRA(){
        init();
        PageReference pr = new PageReference('/apex/LMS_ExcelCourseAssignments?eCid=' + roleCourses[0].Course_Id__c + '&type=1');
        Test.setCurrentPage(pr);
        
        LMS_ExportAssignmentsController c = new LMS_ExportAssignmentsController();
        LMS_Course__c course = [SELECT Title__c FROM LMS_Course__c where Id =:roleCourses[0].Course_Id__c];
        String title = course.Title__c;
        
        System.assert(c.title == '"Employee Listing for Course ' + title + '"');
        System.assert(c.typeCourse == false && c.typeEmployee == true && c.typeRole == false);
    }
    
    static testMethod void testCourseToEmployeeListingPST(){
        init();
        PageReference pr = new PageReference('/apex/LMS_ExcelCourseAssignments?eCid=' + roleCourses[1].Course_Id__c + '&type=2');
        Test.setCurrentPage(pr);
        
        LMS_ExportAssignmentsController c = new LMS_ExportAssignmentsController();
        LMS_Course__c course = [SELECT Title__c FROM LMS_Course__c where Id =:roleCourses[1].Course_Id__c];
        String title = course.Title__c;
        
        System.assert(c.title == '"Employee Listing for Course ' + title + '"');
        System.assert(c.typeCourse == false && c.typeEmployee == true && c.typeRole == false);
    }
    
    static testMethod void testCourseToEmployeeListingAdhoc(){
        init();
        PageReference pr = new PageReference('/apex/LMS_ExcelCourseAssignments?eCid=' + roleCourses[2].Course_Id__c + '&type=3');
        Test.setCurrentPage(pr);
        
        LMS_ExportAssignmentsController c = new LMS_ExportAssignmentsController();
        LMS_Course__c course = [SELECT Title__c FROM LMS_Course__c where Id =:roleCourses[2].Course_Id__c];
        String title = course.Title__c;
        
        System.assert(c.title == '"Employee Listing for Course ' + title + '"');
        System.assert(c.typeCourse == false && c.typeEmployee == true && c.typeRole == false);
    }
    
}