@isTest
private class BDT_NewEditSubcontractorActivityTest {
	
	static testmethod void NewEditSubcontractorActivityTest(){
		Subcontractor__c sc = new Subcontractor__c(Name='Test');
		upsert sc;
		
		BDT_NewEditSubcontractorActivity scac = new BDT_NewEditSubcontractorActivity();
		
		SubcontractorActivity__c sca = new SubcontractorActivity__c(Name='TestTest', Subcontractor__c=sc.Id);
		upsert sca;
		
		scac.SubcontractorActivityId = sca.Id;
		
		system.assertEquals(scac.SubcontractorActivityId, sca.Id);
		
		scac.loadSubcontractorActivity();
		
		scac.save();
		scac.deleteSoft();
		scac.cancel();
		
	}
}