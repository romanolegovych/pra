public with sharing class PAWS_CloneFlowController 
{
    public class CloneFlowControllerException extends Exception {}
     
    /********************** PROPERTIES SECTION *************************/
    public String SourceId
    {
        get
        {
            if (SourceId == null)
            {
                SourceId = ApexPages.currentPage().getParameters().get('sourceId');
            }
            return SourceId;
        }
        set;
    }
    
    public ecrf__c pawsProject
    {
        get
        {
            if (pawsProject == null)
            {
                pawsProject = new ecrf__c(Type__c = 'Regular');
                for (ecrf__c p : [Select Type__c From ecrf__c Where Id = :SourceId])
                {
                    pawsProject = p;
                }
            }
            return pawsProject;
        }
        set;
    }
    
    public String ParentFolderId
    {
        get
        {
            if (ParentFolderId == null)
            {
                ParentFolderId = ApexPages.currentPage().getParameters().get('folderId');
                if (ParentFolderId == null)
                {
                    List<PAWS_Settings__c> settings = PAWS_Settings__c.getall().values();
                    if (settings.size() > 0)
                    {
                        String projectFolderName = (pawsProject.Type__c == 'Proposal' ? 'Proposal Projects' : settings.get(0).Project_Folder_Name__c);
                        if (!String.isEmpty(projectFolderName))
                        {
                            List<STSWR1__Item__c> items = [Select Id From STSWR1__Item__c s Where Name = :projectFolderName And STSWR1__Type__c = 'Folder' And STSWR1__Parent__c = null Order By CreatedDate limit 1];
                            ParentFolderId = (items.isEmpty() ? null : items.get(0).Id);
                            
                            if (items.isEmpty())
                            {
                                STSWR1__Item__c item = new STSWR1__Item__c(
                                    Name = projectFolderName,
                                    STSWR1__Parent__c = null
                                );
                                insert item;
                                ParentFolderId = item.Id;
                            }
                        }
                    }
                    
                }
            }
            return ParentFolderId;
        }
        set;
    }
    
    public List<SelectOption> TemplateFlows
    {
        get
        {
            if(TemplateFlows == null && SourceObject != null)
            {
                TemplateFlows = new List<SelectOption>();
                TemplateFlows.add(new SelectOption('', '--None--'));

                Schema.DescribeSObjectResult describe = SourceObject.getSObjectType().getDescribe();
                for(STSWR1__Flow__c record : [select Name from STSWR1__Flow__c where STSWR1__Type__c = 'Template' and STSWR1__Start_Type__c = 'Manual' and STSWR1__Object_Type__c = :describe.getLocalName().toLowerCase() order by Name])
                {
                    TemplateFlows.add(new SelectOption(record.Id, record.Name));
                }
            }
            
            return TemplateFlows;
        }
        set;
    }
    
    public sObject SourceObject
    {
        get
        {
            if(SourceObject == null && SourceId != null)
            {
                Schema.DescribeSObjectResult describe = getSObjectTypeById(SourceId).getDescribe();
                if(describe == null) return null;

                List<sObject> records = Database.query('select Id, Name from ' + describe.getLocalName() + ' where Id=\'' + SourceId + '\'');
                if(records.size() > 0) SourceObject = records[0];
            }
            return SourceObject;
        }
        set;
    }

     
    public String TemplateFlowId {get;set;}
    public String CloneFlowId {get;set;}
    
    public Boolean CanContinue {get;set;}
    public Map<ID, ID> OldNewFlowIdMap {get;set;}
    public List<ID> FlowIds {get;set;}
    
    public ID FolderId {get;set;}
    
    public String RetURL { get { return ApexPages.currentPage().getParameters().get('retURL');}}
     
    /********************** METHODS SECTION *************************/
    
    public void beforeClone()
    {
        try
        {
            CanContinue = false;
            system.debug('-----beforeClone, TemplateFlowId = ' + TemplateFlowId);
            if(String.isEmpty(TemplateFlowId)) return;
            
            OldNewFlowIdMap = new Map<ID, ID>{TemplateFlowId => null};
            FlowIds = new List<ID>{TemplateFlowId};
            
            Set<ID> ids = new Set<ID>{TemplateFlowId};
            system.debug('-----beforeClone, ids = ' + ids);
            while(ids.size() > 0)
            {
                List<STSWR1__Flow_Step_Action__c> actions = [select STSWR1__Flow_Step__c, STSWR1__Flow_Step__r.STSWR1__Flow__c, STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active' and STSWR1__Flow_Step__r.STSWR1__Flow__c = :ids];
                
                ids.clear();
                for(STSWR1__Flow_Step_Action__c action : actions)
                {
                    Map<String, Object> config = (Map<String, Object>)JSON.deserializeUntyped(action.STSWR1__Config__c);
                    if(config != null && !String.isEmpty((String)config.get('flowId')))
                    {
                        ids.add((String)config.get('flowId'));
                        OldNewFlowIdMap.put((String)config.get('flowId'), null);
                        FlowIds.add((String)config.get('flowId'));
                    }
                }
            }
            
            FolderId = createFolder();
            system.debug('-----beforeClone, FolderId = ' + FolderId);
            
            CanContinue = true;
        }catch(Exception ex)
        {
            if (Test.isRunningTest()) throw ex;
            rollBack();
            ApexPages.addMessages(ex);
        }
    }
     
    public void cloneFlow()
    {
        try
        {
            CanContinue = false;
            system.debug('-----cloneFlow, FlowIds.size() = ' + FlowIds.size());
            if(FlowIds.size() == 0) return;
            
            ID flowId = FlowIds[0];
            List<STSWR1__Flow__c> flows = [select Name from STSWR1__Flow__c where Id = :flowId];
            system.debug('-----cloneFlow, flows.size() = ' + flows.size());
            if(flows.size() > 0) 
            {
                STSWR1__Flow__c flow = flows[0];
            
                STSWR1.ItemsService.FolderID = FolderId;
                flow = STSWR1.FlowImportExportService.getInstance().cloneFlow(flow.Id, flow.Name);
                
                flow.STSWR1__Type__c = 'One Time';
                flow.STSWR1__Source_Id__c = SourceObject.Id;
                update flow;

                OldNewFlowIdMap.put(flowId, flow.Id);
            }

            FlowIds.remove(0);
            CanContinue = true;
        }catch(Exception ex)
        {
            if (Test.isRunningTest()) throw ex;
            rollBack();
            ApexPages.addMessages(ex);
        }
    }
    
    public void afterClone()
    {
        try
        {
            //new CC_Clone(OldNewFlowIdMap).cloneChains();
            CloneFlowId = null;
            
            //Replace SubFlows ids
            {
                List<STSWR1__Flow_Step_Action__c> actions = [select STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active' and STSWR1__Flow_Step__r.STSWR1__Flow__c = :OldNewFlowIdMap.values()];
                for(STSWR1__Flow_Step_Action__c action : actions)
                {
                    for(ID oldId : OldNewFlowIdMap.keySet())
                    {
                        ID newId = OldNewFlowIdMap.get(oldId);
                        if(String.isEmpty(newId)) continue;
                        
                        action.STSWR1__Config__c = action.STSWR1__Config__c.replace(oldId, newId);
                    }
                }
                
                update actions;
            }
            
            createRelationshipObject(OldNewFlowIdMap.get(TemplateFlowId));
            callPostImportHandler(OldNewFlowIdMap);
            
            CloneFlowId = OldNewFlowIdMap.get(TemplateFlowId);
        }catch(Exception ex)
        {
            if (Test.isRunningTest()) throw ex;
            rollBack();
            ApexPages.addMessages(ex);
        }
    }
    
    public void rollBack()
    {
        try
        {
            CanContinue = false;
            
            Set<ID> ids = new Set<ID>();
            for(ID oldId : OldNewFlowIdMap.keySet())
            {
                ID newId = OldNewFlowIdMap.get(oldId);
                if(!String.isEmpty(newId)) ids.add(newId);
            }
            
            if(ids.size() > 0) delete [select Id from STSWR1__Flow__c where Id in :ids];
            if(FolderId != null) delete [select Id from STSWR1__Item__c where Id = :FolderId];
        }catch(Exception ex)
        {
            ApexPages.addMessages(ex);
        }
    }

    private ID createFolder()
    { 
        if(SourceObject == null || String.isEmpty(TemplateFlowId)) return null;
        
        STSWR1__Item__c parentFolder = null;
        List<STSWR1__Item__c> folders = [select Id from STSWR1__Item__c where Name = :(String)SourceObject.get('Name') and STSWR1__Parent__c = :ParentFolderId];
        if(folders.size() > 0) 
        {
            parentFolder = folders[0];
        }else{
            parentFolder = new STSWR1__Item__c(Name = (String)SourceObject.get('Name'), STSWR1__Parent__c = ParentFolderId);
            insert parentFolder;
        }
        
        STSWR1__Flow__c flow = [select Name from STSWR1__Flow__c where Id = :TemplateFlowId];

        STSWR1__Item__c folder = new STSWR1__Item__c(Name = flow.Name, STSWR1__Parent__c = parentFolder.Id);
        insert folder;
        return folder.Id;
    }
    
    private void createRelationshipObject(ID flowId)
    {
        if(SourceObject == null) return;
        
        PAWS_Project_Flow_Junction__c record = new PAWS_Project_Flow_Junction__c();
        record.Flow__c = flowId;    
        record.Project__c = SourceObject.Id;    
        record.Folder__c = FolderId;    
        insert record;
    }

    public void callPostImportHandler(Map<ID, ID> flowsMap)
    {
        if(String.isEmpty(STSWR1__Work_Relay_Config__c.getInstance().STSWR1__Flow_Post_Import_Handler__c)) return;
        
        Map<String, String> temp = new Map<String, String>();
        for(ID key : flowsMap.keySet())
            temp.put(key, (String)flowsMap.get(key));
        
        Type classType = Type.forName(STSWR1__Work_Relay_Config__c.getInstance().STSWR1__Flow_Post_Import_Handler__c);
        STSWR1.FlowPostImportInterface classObject = (STSWR1.FlowPostImportInterface)classType.newInstance();
        classObject.execute(temp);
    }
    
    public sObjectType getSObjectTypeById(String id)
    {
        String prefix = id.substring(0,3);      
        for(sObjectType objectType : Schema.getGlobalDescribe().values())
        {
            Schema.DescribeSObjectResult describe = objectType.getDescribe();
            if(prefix.equals(describe.getKeyPrefix())) return objectType;
        }
        return null;
    }
    
    public void fillRolesAndSwimlaneLookups() {
        if (pawsProject.Type__c != 'Proposal')
        {
            String projectName = [SELECT Project_ID__r.Name FROM ecrf__c WHERE id = :SourceId LIMIT 1].Project_ID__r.Name;
            PAWS_API.updateLookupIDsForPAWSProjectFlows(projectName, SourceId);
            PAWS_API.updateLookupIDsForPAWSProjectFlowsSwimlanes(projectName, OldNewFlowIdMap.values());
        }
    }
}