@isTest
public class IPA_DashboardTest
{
    static testmethod void ipa_BO_DAL_method()
    {   
        // Page__c Trigger Testing //
        string profileNameSysAdmin = 'System Administrator';
        Profile defaultProfile = [Select Id from Profile where Name = : profileNameSysAdmin LIMIT 1];
        String randomWord = 'TestData' + math.rint(math.random() * 100000);
        User testUser = new User();
        testUser.Email = 'email' + randomWord + '@testmyapex.com';
        testUser.Username = 'user' + randomWord + '@testmyapex.com';
        testUser.FirstName = 'First Name ' + randomWord;
        testUser.LastName = 'Last Name' + randomWord;
        testUser.Alias = 'test';
        testUser.ProfileId = defaultProfile.Id;
        testUser.LanguageLocaleKey = 'en_US';
        testUser.LocaleSidKey = 'en_US';
        testUser.TimeZoneSidKey = 'America/Chicago';
        testUser.EmailEncodingKey = 'UTF-8';
        insert testUser;
        
        system.runAs(testUser){
            IPA_Category__c catObj = new IPA_Category__c();
            catObj.Active__c = true;
            catObj.Name = 'Test Category';
            Map <String,Schema.RecordTypeInfo> categoryRecordTypes = IPA_Category__c.sObjectType.getDescribe().getRecordTypeInfosByName();
            Id categoryRecordTypeId = categoryRecordTypes.values()[1].getRecordTypeId();
            catObj.RecordTypeId = categoryRecordTypeId;
            insert catObj;
            
            IPA_Page__c pageObj = new IPA_Page__c();
            pageObj.Category__c = catObj.Id;
            pageObj.Master_Landing_Page__c = false;
            pageObj.Published__c = true;
            pageObj.Name = 'Test Page';
            insert pageObj;
            pageObj.Master_Landing_Page__c = true;
            update pageObj;
        }
        
        //***************** Public Declartions ******************
     
        Date TodayDate = system.today();        
        String RecCategory = [Select Id From RecordType Where SobjectType = 'IPA_Category__c' and Name = 'Category'].Id;
        //String RecSubCategory = [Select Id From RecordType Where SobjectType = 'IPA_Category__c' and Name = ' Sub Category'].Id;
        String RecLandingPage = [Select Id From RecordType Where SobjectType = 'IPA_Page__c' and Name = 'Landing Page'].Id;
        String RecSubPage = [Select Id From RecordType Where SobjectType = 'IPA_Page__c' and Name = 'Sub Page'].Id;
        String RecMainMenu = [Select Id From RecordType Where SobjectType = 'IPA_Links__c' and Name = 'Main Menu Item'].Id;
        String RecSubMenu = [Select Id From RecordType Where SobjectType = 'IPA_Links__c' and Name = 'Sub Menu Item'].Id;
        String RecQuickLink = [Select Id From RecordType Where SobjectType = 'IPA_Links__c' and Name = 'Quick Link'].Id;
        String RecResourceCabinet = [Select Id From RecordType Where SobjectType = 'IPA_Links__c' and Name = 'Resource Cabinet Item'].Id;
        String RecCorpoUpdate = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Corporate Update'].Id;
        String RecHoliday = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Holiday'].Id;
        String RecIndWatch = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Industry Watch'].Id;
        String RecDeptContact = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Department Contact'].Id;
        String RecDeptContent = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Department Content'].Id;
        String RecDeptNews = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Department News'].Id;
        String RecEvent = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Event'].Id;        
        String RecHero = [Select Id From RecordType Where SobjectType = 'IPA_Articles__c' and Name = 'Hero Graphic'].Id;
        String sampleImage = '<img alt="User-added image" height="375" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOwAAACoCAYAAAD'+
            'jGgJuAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAEJVJREFUeNrsnb1y48gVhXumNhedOhls5FB06Ig'+
            'Yv8AwcJUjlzBPsJzAsajM2VJPMFA5dZWpF9ilog0NhnZEJZtazLdqjCtdlLEcEehu3P4DzqlCaWtHIoFGf33P7d83X758URAEpaG3KAIIArAQBAFYCAKwEAQBWAi'+
            'CACwEAVgIglLQNyiC+PXDTz/P6x+z+jr88Q+/PaBEpqs3mDgRLaRZ/aPg693JPz8QvPVVNVcN8hNKDcBCfiGdtSC9NPzzxxbAO0AMYCF3oC4Z0g/CH70/AbhCaQNY'+
            'yD4vXdUXwXrh6WuPDO/zBYABLNSfly4Z1HcR3BIABrCQR8srrccWwFvkwAB2atF0xaBeJPoYlANvGV5EXwA7SlALhnQxskd7bEXeLd40gEU0TUdHjrywzgAW0TRB3'+
            'besM+AFsNFF04Ij6gVK5NXIC9sMYIODmjOkH1AaRvBu0GEFYH3b3rWKY9w0VVGH1aa+SlhmAAvbm16+W8IyA1gpUCmaXqE0vETdki0zoi6ANc5PCdQFqkQQ3SHXBb'+
            'AANT09sF0uURQAtg1qodCRFLtdpveDcd0pAwtQkxMNDW2Q504MWLa+JUAFuAAWOSoEcAHsQFAzfrGYlQRwAWzEoNImZjTZ4Rp1ejrg1tCuAWx6sC45T8XMpOnpuVd5'+
            'CsNByQPL9rdEngqpl3FcAncHYOOElezvGlEVOhHNnFqNMb9NEljOVbeIqlBPfkvRdgNgw8KaM6yIqpCOaCO5YizzlJMClmcqfUYdhCx0o0YwDJQMsDWsZG2+Q72DBu'+
            'iRo+0u1Qd4C1ihCYmmpv5Y16c1IixsMITcdtrA8hgrFSg6mCAXop7kVUoTLmK3xGvACjkU1a3PdWAoeagQwA4U9laCfNWzHR8BCmAt7XCOegR51CVDWyCHtYcWO8RB'+
            'IXQT6wqg2C3xLeoOFEDXsea1Y99xIqt/ZB2/8prtpjym/aLo77G1zDRFQz95TLOjsC/xsNy6DXfz/2ecD0HjgTaa8VoA6xbuBugm0s9akAPqdHTkSFsB2GkD3QDcAN'+
            '3AjGWDgBbAJgpzA3GOfBrQAti0QW6iMiLyRKAFsOPKl9sXIB4htAB2GhDn/BMdXYlDC2CnZ6fzFsSIwsNEC+LnPsdpASwgzhleAGwnr5MrACz0WgRuLlhoPd3XwC4B'+
            'LBQa4IzBXfJPrE0+r9sa2hWAhWICeNmCF+PBX+uj690rACxkCy91XBUMMOB9kfOeYwALAV5ZOe2EArCQK3iLCee8zvJZAAv5yHmnuDfXexcblgNYyAe4M464qwlZZs'+
            'pnM2lrDGAh3/DmDO8Uoq64NQawUMiou2J4xxx1Ra0xgIVigLdgcMc4NXJfAzsHsNBY7TJF3Q8je7RPUgdLA1goRnAz9XJMy1jyXLEOqLeoHlBsqiv2ob7IIn9bX3cje'+
            'KQLdg6IsBAi7pSiLCIslFrEfZhylEWEhVKMuHn9gzpxUluvOzjKIsJCKUbcHQ+VfGQIUoqyBSwxNFVwS/Wy1WtKh6YNssWwxBBssn/93nbNLCIsNDabfJPA7VrbYgAL'+
            'jQ3cNUUw9bKQPFYtASwE/R9aspt5xLntO17oD2AhiKF94qVtsfYk5wAWgr4Gt2Q4HscA7Kh7iX/zl3+0T0h/1n///qcdqnGw95HxOzj4/m5ef0vvPppe5LoxeZMUsPU'+
            'L7GplmmMVdf6fzUsgm1S1rm1dkZ48Prvu8zXK+GpE9/zE1/Mz+Lz/V56h3Ti2/5vuub1A/a6+zyJEfWNoKeLGsnzPeHF7aGCpgsW0s959fW1Mo3D9HJQrLXsaHx8tO/'+
            'WMbnw1PvVzby0q/0N9b3nIl1yDS9DGsJDghnu1k8lhY7OnVPl+rCvirrFvmiKLt+i4fNkw+p7PdD/ciLh2RzaRKviuEryQIIZle8Y9xaGBrVScokpVcQ7cqzpibCO7f'+
            '3It33PDM3P0HesBsGeA1g7YbxLsu7jjiNZEtnYHxlMNT9WqFM01V+a70lOlpwqfN5+pYUdtI+nxpPHanbzUJle8sGh4mmcQs8gcXYdEyuzkvQWDtrbHKqA9pvHYmcnq'+
            'nRSBXev0MvLvtH9vxRVtbVDZCJAtRVqNCm8KxHuLXLlpeFYG8F5yXlsIvoNy4N/nsaRDEUA7NymL1MZhH4cMCRAg3OHxyaQVVELbe5zei8XfUE/wmiOUyULuq54eeZN'+
            'GY62Gb0s6i6lSsT3eBwQ2mRzWNJfZCcFCEcdk2tpKuBI+DLz/J2549sLPoJN7rnxXUk/KA0GbAVi9Sk8VT3f2y0VdWfsmbJvkr1L5m8kkcomxx/KMFT+mDiznkYXyP4'+
            '0xqQi7CAUsaytRsBY9sSK945we3BtESGtbzMNEi47oZOIaLhz2Xg+BthLO9cXTg2DA6g6ZSOWvLoG1iBiSw1kmn5VZviv6u/WZf/7IveimjWmMtpig3Sq/q3wukwDW4'+
            'oXtHNyDSWWfST2L8HzmnWtgO6wwwVpavp9Mxau1im+xQHBg89DACo5NmgCb1DadHVb4rgUr6WkswLbyWS8yWRs7aWADuYWQs7t2hrDOz1hhij6rk8avcvz+fUNLZeVr'+
            'JtQsamA5JzIZy3ORv5p2wuyE8pCQwJpGwXNWuBBwJzFb4rY1jmrxe6gIa9q6uqrkJj10BwHoXTyLdnQ3iYI8QeK1hui2Iwc3sfvRnwlLJw6ol1liADYSOyxhZU07nKS'+
            'B1Y1UDwaw0vu5PmOF14EcTihtYoqyUwdW9z6OHaCZPIuLDifdBmOrCVGzyFtZWGHTxmgWO63cAVXGwoN3YC3y16ODqNRoIVDZg3U4MVwLSWA5orwztMK2OfJcpaFobH'+
            'GICBtFdNWYathb2S0aH+ln0X2GB51OOy6T11at7DWt8GGMwHIue+/wKw4AVq6yP3YsUA85w+nZokpFCG58bK2wLbCZSkdbABsIWK6cVwKV3QTYo+TQlMEi8r3mjhgE6'+
            '2tDODcOU5JLABuxJY4of9WNTI+8FE+i8ZF+Dt28aqXxXtZn4N/z+lst2Uy5jGG7GE1bTA7jYVLARhRdV0KVfRHiWTrGSE/V21HEs5muBzZsQ5SSLd4BWP8FdM76neq+'+
            'y0parDaqhGAtOgD7VXRUPR1F3Mt87hltrfDecZ0IqSr0504K2J41nb+y4hrRxTuwDOtn3fvX6Cg6N4RjZIVPNJpFAL6ANdmEzRuwofNXruzfa1Z2nV0GTRqfwXOh6/v'+
            'fGMDau9Mjl8eVZWPVpcNYgeXhHWkZLePzuWuiaXSVhlWssltE2GrAvedK/2RxevlLDVgzdb7Taj2woTSt1KlMnmjXEcnTKg5jAXYnAOqMK+aVJKz8uU5X6HAjUyj9jq1'+
            '7pT9euj1T6R56esVdWOLn7WJ8nws00BYvhD8PwHKF32i2hnuu7LqFJ75bBkfSOZdTbtCKU0OzOllM3metLx1Y4SFuYq7iXu8cTV7sBViL/NV2TG/Ola4wqPC3bANNWnj'+
            'TxofO62ksa9sCDTkk68gN0kb33rlR+K7DCh8CVVoAG1mENa3gDxoNQPtqotOF4XesLfdXss273qnh60Dv2dIanVDXM4QjYYWHRNjZRGFtdmpMHthdq6JVSnYK2xBQbZ/'+
            'HVk1E3jEIuwG53rajQZvRwVkB622eEGML4bqoYgR2aQtsXUHnnI+uB0QnylFLjkqDbB9Hd19n2lLkWQ7tkOmYetgo9JzebKIBdhcdsJxXGlXw0+jHHSolL/9a8gte9LR'+
            'aElFJwg6/Vy89p80JdLnS71S64IZmObD8ryOvuO9SoOuHn36WdgLb6ICVzF95qmDoVRNGz3PS+NB/bxikQun1Yn+ghsrmDNqevDUq8ZGYu8hvU9IJHE3z11iBjf2lmUT'+
            'YfQfI5BiaBqgvL6LfzSycQtkRvW6V+ZipiUyjegodT5IR1qohBbBuOx2qnuhLwOQ1jATWVY813pqUJc+bPncA1s2AucK6328K7DwBNyAJbGnzR28dv7TB+Wtsts3wTyr'+
            'NZy5U/yqXBUOoW+7n5k0/uIa1z10I9Q34zl8zwVz7kTcqjwtYJTz+mpgd1ga2VVZ922mu+xZ89+St9PlLT2U1tlU7kuVW2v5hbMBWYwLWxC2wPe6rFBcatrErb116nLN'+
            'rGkFi3y5mBWDTy19NnsfYLTDgfUcdXvK4qk3e6rN8jRuGWLeL4eEcKTt8N2SZnjNgbfLXmIFlq2ny0mzdAsHYt0by+nTHi46Dq3zmrUOfP1ZbLBldB70HlxHWNLruI19'+
            'i5WWHCS6DQsdWNaeYt3brvziTtxYByuvgoc74iK5Zh2vxGl1jA3ZMdnhQPs7Wte+ow8tWa921wL0IsQrH8jtjjLDrmD4LwDp6HoHtbVaqv9f4uzq6/k2dH8O9tZkhJah'+
            '9ysBy7nol9HG3ElvMOAF2bPmrhSUePDxlYI3/2gHLOnCZmaY4i8jeuVT5HaU+y1WEHVX+atEAiQxPcXS8t3yHRQRlurMo6yiibB1dV4INSGGyM2IKwI4puko/j441PtU'+
            'nh8druIywUdhi7miSiq73NaxiaQmAdQOsGCzceWNSeSR3jxiqykNZu1CpZNY8i/fQiwM70vzVpAE6SvfKMoA6efEvKswQzjnZlEPQCCtshZdSVthlhB1V/spyuqWpgTX'+
            'u0zcxAWvZcM0Dwkp193uhj/tkO8E/dmCjnj9ssUJn56jyUzn9W+NXry3u2aWSWLXDeatUrkkTJJykJTEAi/xVr+GgCPs7zV/fRjQv12pjcc+wNiucJPLWfQ2rM5fzVrh'+
            'STT1/dQJsz/rWVys9QxvDLg4279d3lN0qmdVCe+V4eqV0hDVdM/gYcPNqF5VH/HkYOptKf6kS2c8pJLB1dC2VTCfTM6zSnUyugR2VHbY4scCFHR5i1Ra8/UxqETbzCOt'+
            'VKrC6AHYxJmBD568a+wn/R+NjrgJD++Sh3CcBqyiwlj2T1ciA3QmWJ6UXXRuZfaqvPyu9WVDBoLWccTVPBFbqDZ77glU6wpoCe4xk+pzkM1VCsJIl7ALseTYTl1+h+bE'+
            'hI+2j4e876Smm3uD6qoRg/eSyNzhGYGO3w6YW/ygxAaS1idq5vPVXG6nxAoGPJtAG6D0+hI6yPM5KdW5obzCV/3tX46w+gTXNX2OfMBEqf+07bf2rVTh8lMmdLrRUcT2'+
            'P09qUTS4I65zvYSisND00czGDySuwlvlr7BHWe/7KkyO67NrZBem8t/GN5ldRxa04T/ahYKt2alipXP6lhk2KOLIFzn3mq69Jauf/TBku2k7gHBXTRugwENaMre65cqz'+
            'qMlv1lOmaj+csNSoo/fs/69+n9bYrx+PhW4vyfBoI6ozdytB8ld5HIbFbhITefPnyRUFnLbFJrlfFsoiBc1QTh3BIYAKLqQUuB1pg6ihbSa5lBbAQ9DWs5EKGrLg5cmT'+
            'ehLa/Li0xBIUGNeOoOmSaIW3ivo4RVAALjQnWtbI/tPrIoG9iyVMBLDRWUHPVfZZQX45axmp9ASwE+/si6vUta0jLFJ8dwEKpgUr213SohqLpNhXbC2ChKYLaQErRtBp'+
            'LWQBYKGZQC/WyuEHX+tJSt93YIAWwUOzRdMWg9s3WOjKgFEl3qdtdAAulGFE/99hcArRiQKuplRGAhWLS5xM4qwZO+pnS8AuAhaagb1Xg5WuxC3OJISghvUURQBCAhSA'+
            'IwEIQgIUgCMBCEARgIQjAQhCUgv4nwAAEnlBacfrUqgAAAABJRU5ErkJggg==" width="500"></img>';
        // ********************* Records for Dashboard ******************
        
        IPA_Category__c DashCatObj;
        IPA_Links__c LMainMenuObj;
        IPA_Links__c LSubMenuObj;
        IPA_Articles__c ACorpoUpdateObj;
        IPA_Articles__c AEventObj;
        IPA_Articles__c AHolidayObj;
        IPA_Articles__c AIndNewsObj;
        IPA_Articles__c ARegNewsObj;
        IPA_Articles__c ATechNewsObj;
        IPA_Articles__c AMedNewsObj;
        IPA_Articles__c AGovtNewsObj;
        IPA_Articles__c APRANewsObj;
        
        DashCatObj = new IPA_Category__c();        
        DashCatObj.Active__c = true;
        DashCatObj.Name = 'Dashboard';
        DashCatObj.RecordTypeId = RecCategory;
        insert DashCatObj;
        
        LMainMenuObj = new IPA_Links__c();        
        LMainMenuObj.RecordTypeId = RecMainMenu;
        LMainMenuObj.Category__c = DashCatObj.id;
        LMainMenuObj.Name = 'Test Main Menu';
        LMainMenuObj.URL__c = 'http://www.google.com';
        LMainMenuObj.Weight__c = 1;
        LMainMenuObj.CSS_Class__c = 'pra-icon-dashboard';
        insert LMainMenuObj;
        
        LMainMenuObj.RecordTypeId = RecQuickLink;
        update LMainMenuObj;
        
        LMainMenuObj.RecordTypeId = RecMainMenu;
        update LMainMenuObj;
        
        LSubMenuObj = new IPA_Links__c();
        LSubMenuObj.RecordTypeId = RecSubMenu;
        LSubMenuObj.Category__c = DashCatObj.id;
        LSubMenuObj.Name = 'Test Sub Menu';
        LSubMenuObj.URL__c = 'http://www.google.com';
        LSubMenuObj.Weight__c = 1;
        LSubMenuObj.Parent_Link__c = LMainMenuObj.Id;
        insert LSubMenuObj;
        
        Date CorpoUpdateDate = TodayDate;
        for(Integer loopvar = 0; loopvar < 6; loopvar++)
        {
        ACorpoUpdateObj = new IPA_Articles__c();        
        ACorpoUpdateObj.RecordTypeId = RecCorpoUpdate;
        ACorpoUpdateObj.Published__c = true;
        ACorpoUpdateObj.Category__c = DashCatObj.Id;
        ACorpoUpdateObj.Article_Type__c = 'Corporate Update';
        ACorpoUpdateObj.Display_Area__c = 'Corporate Updates';
        ACorpoUpdateObj.Title__c = 'Test Corporate Update Title';
        ACorpoUpdateObj.Weight__c = 0;
        ACorpoUpdateObj.Display_Date__c = CorpoUpdateDate;
        ACorpoUpdateObj.Featured_Picture__c = sampleImage;
        ACorpoUpdateObj.Pictures__c =  sampleImage + sampleImage + sampleImage + sampleImage + sampleImage;      
        ACorpoUpdateObj.Content__c = 'Test Corporate Update Content';
        insert ACorpoUpdateObj;
        CorpoUpdateDate -= 20;
        }
        
        AEventObj = new IPA_Articles__c();                        
        AEventObj.RecordTypeId = RecEvent;
        AEventObj.Published__c = true;
        AEventObj.Category__c = DashCatObj.Id;
        AEventObj.Title__c = 'Test Event title';
        AEventObj.Article_Type__c = 'Event';
        AEventObj.Display_Area__c = 'Event & Holidays';
        AEventObj.Weight__c = 0;
        AEventObj.Display_Date__c = system.today();
        insert AEventObj; 
                        
        AHolidayObj = new IPA_Articles__c();        
        AHolidayObj.RecordTypeId = RecHoliday;
        AHolidayObj.Published__c = true;
        AHolidayObj.Category__c = DashCatObj.Id;
        AHolidayObj.Article_Type__c = 'Holiday';
        AHolidayObj.Display_Area__c = 'Events & Holidays';
        AHolidayObj.Title__c = 'Test Holiday Title';
        AHolidayObj.Weight__c = 0;
        AHolidayObj.Display_Date__c = system.today();
        insert AHolidayObj;
        
        AIndNewsObj = new IPA_Articles__c();        
        AIndNewsObj.RecordTypeId = RecIndWatch;
        AIndNewsObj.Published__c = true;
        AIndNewsObj.Category__c = DashcatObj.Id;
        AIndNewsObj.Title__c = 'Test Industry News Title';
        AIndNewsObj.Weight__c = 0;
        AIndNewsObj.Article_Type__c = 'Industry Watch';
        AIndNewsObj.Display_Area__c = 'Industry News';
        AIndNewsObj.Display_Date__c = system.today();
        AIndNewsObj.URL__c = 'http://www.google.com';
        insert AIndNewsObj;
        
        ARegNewsObj = new IPA_Articles__c();
        ARegNewsObj.RecordTypeId = RecIndWatch;
        ARegNewsObj.Published__c = true;
        ARegNewsObj.Category__c = DashcatObj.Id;
        ARegNewsObj.Title__c = 'Test Industry News Title';
        ARegNewsObj.Weight__c = 0;
        ARegNewsObj.Article_Type__c = 'Industry Watch';
        ARegNewsObj.Display_Area__c = 'Regulatory News';
        ARegNewsObj.Display_Date__c = system.today();
        ARegNewsObj.URL__c = 'http://www.google.com';
        insert ARegNewsObj;
        
        ATechNewsObj = new IPA_Articles__c();
        ATechNewsObj.RecordTypeId = RecIndWatch;
        ATechNewsObj.Published__c = true;
        ATechNewsObj.Category__c = DashcatObj.Id;
        ATechNewsObj.Title__c = 'Test Industry News Title';
        ATechNewsObj.Weight__c = 0;
        ATechNewsObj.Article_Type__c = 'Industry Watch';
        ATechNewsObj.Display_Area__c = 'Technology News';
        ATechNewsObj.Display_Date__c = system.today();
        ATechNewsObj.URL__c = 'http://www.google.com';
        insert ATechNewsObj;
        
        AMedNewsObj = new IPA_Articles__c();
        AMedNewsObj.RecordTypeId = RecIndWatch;
        AMedNewsObj.Published__c = true;
        AMedNewsObj.Category__c = DashcatObj.Id;
        AMedNewsObj.Title__c = 'Test Industry News Title';
        AMedNewsObj.Weight__c = 0;
        AMedNewsObj.Article_Type__c = 'Industry Watch';
        AMedNewsObj.Display_Area__c = 'Medical News';
        AMedNewsObj.Display_Date__c = system.today();
        AMedNewsObj.URL__c = 'http://www.google.com';
        insert AMedNewsObj;
        
        AGovtNewsObj = new IPA_Articles__c();
        AGovtNewsObj.RecordTypeId = RecIndWatch;
        AGovtNewsObj.Published__c = true;
        AGovtNewsObj.Category__c = DashcatObj.Id;
        AGovtNewsObj.Title__c = 'Test Industry News Title';
        AGovtNewsObj.Weight__c = 0;
        AGovtNewsObj.Article_Type__c = 'Industry Watch';
        AGovtNewsObj.Display_Area__c = 'Government News';
        AGovtNewsObj.Display_Date__c = system.today();
        AGovtNewsObj.URL__c = 'http://www.google.com';
        insert AGovtNewsObj;
        
        Test.StartTest();
        	system.runAs(testUser){
                // Apex Classes for Dashboard
                IPA_ArticleDetailComponentController ArticleDetailCompclsObj = new IPA_ArticleDetailComponentController();
                ArticleDetailCompclsObj.articleId = ACorpoUpdateObj.Id;
                ArticleDetailCompclsObj.getFetchContent();
                IPA_CorporateUpdatesComponentController CorpoUpdateCompclsObj = new IPA_CorporateUpdatesComponentController();
                IPA_IndustryWatchComponentController IndWatchCompclsObj = new IPA_IndustryWatchComponentController();
                IPA_IntegrationUpdateComponentController IntUpdateCompclsObj = new IPA_IntegrationUpdateComponentController();
                IPA_MyAppsComponentController MyappsCompclsObj = new IPA_MyAppsComponentController ();
                IPA_NewsSliderComponentController NewsSliderCompclsObj = new IPA_NewsSliderComponentController();
                IPA_PRANewsComponentController PRANewsCompclsObj = new IPA_PRANewsComponentController();        
                
                IPA_UI_LeftPanel ui_leftpan = new IPA_UI_LeftPanel();
                ui_leftpan.UserName = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
                ui_leftpan.loadLeftPanel();
                ui_leftpan.saveCustomLinks();
                
                apexpages.currentpage().getparameters().put('Type','Industry Watch'); 
                IPA_UI_NewsArchive ui_NewsArc = new IPA_UI_NewsArchive();
                //ui_NewsArc.NextWatch();
                //ui_NewsArc.PreWatch();
                
                apexpages.currentpage().getparameters().put('Id',ACorpoUpdateObj.Id); 
                IPA_UI_ArticleDetails ui_ArtiDet = new IPA_UI_ArticleDetails();        
                apexpages.currentpage().getparameters().put('Id',AEventObj.Id); 
                IPA_UI_ArticleDetails ui_ArtiDet2 = new IPA_UI_ArticleDetails();
                apexpages.currentpage().getparameters().put('Id',''); 
                IPA_UI_ArticleDetails ui_ArtiDet3 = new IPA_UI_ArticleDetails();
                
                apexpages.currentpage().getparameters().put('Type','Policy'); 
                apexPages.currentPage().getParameters().put('P','6');
                apexPages.currentPage().getParameters().put('A','Next'); 
                IPA_UI_NewsArchivePaging ui_newsArcPage = new IPA_UI_NewsArchivePaging();
                apexpages.currentpage().getparameters().put('Type','Policy'); 
                apexPages.currentPage().getParameters().put('P','6');
                apexPages.currentPage().getParameters().put('A','Prev'); 
                IPA_UI_NewsArchivePaging ui_newsArcPage2 = new IPA_UI_NewsArchivePaging();
                apexpages.currentpage().getparameters().put('Type','News'); 
                apexPages.currentPage().getParameters().put('P','6');
                apexPages.currentPage().getParameters().put('A','Prev'); 
                IPA_UI_NewsArchivePaging ui_newsArcPage3 = new IPA_UI_NewsArchivePaging();
                apexpages.currentpage().getparameters().put('Type','News'); 
                apexPages.currentPage().getParameters().put('P','2');
                apexPages.currentPage().getParameters().put('A','Next'); 
                IPA_UI_NewsArchivePaging ui_newsArcPage4 = new IPA_UI_NewsArchivePaging(); 
                
                IPA_Calendar calendar = new IPA_Calendar();
                calendar.Next();
                calendar.Previous();
            }
        Test.StopTest();        
    }
}