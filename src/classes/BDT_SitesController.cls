public with sharing class BDT_SitesController {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Administration Sites
	public List<Site__c> siteList{get;set;}
	public List<Business_Unit__c> businessUnitList{get;set;}
	public String goToPageName {get;set;}
	public String buName {get;set;}
	public String siteId {get;set;}
	public Boolean showDeleted{
		get {if (showDeleted == null) {
				showDeleted = false;
			}
			return showDeleted;
		}
		set;
	}
  	
  	public String buId {get;set;}
  	
	public BDT_SitesController(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Sites');
		businessUnitList = new List<Business_Unit__c>();
		siteList = new List<Site__c>();
		buId = System.currentPageReference().getParameters().get('buId');  		
		try{
			
			businessUnitList = [Select b.PRA_Business_Unit_Id__c, b.Name, b.Id, b.Description__c, b.Currency__c, b.currency__r.Name, b.Abbreviation__c 
								From Business_Unit__c b
								Where ((BDTDeleted__c  = :showDeleted) or (BDTDeleted__c  = false))
								order by Name
								];
								
		}catch(QueryException e){
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, 'No Business Unit could be found. ');
        		ApexPages.addMessage(msg); 
		}
		
		getSitesPerBUSelected();
	}
	
	public void getSitesPerBU(){
		buId = System.currentPageReference().getParameters().get('selectedbuId');
		
		getSitesPerBUSelected();
		
	}
	
	/*If returns from NewEditSite and buID has a value then we need to populate the BU and previously populated site list*/
	public void getSitesPerBUSelected(){	
		
		try{
			
			Business_Unit__c selectedBU = [Select b.PRA_Business_Unit_Id__c, b.Name, b.Id, b.Description__c, b.Currency__c, b.currency__r.Name, b.Abbreviation__c 
								          	From Business_Unit__c b
											Where ((BDTDeleted__c  = :showDeleted) or (BDTDeleted__c  = false))and id = : buId];
			buName = selectedBU.Name;
								
		}catch(QueryException e){
				
		}
			
		siteList.clear();
		try{
		    siteList = [Select s.Name, s.Id, s.City__c, s.Business_Unit__c, s.Business_Unit__r.id, s.Address_Line_1__c, s.Abbreviation__c,s.ZipCode__c, s.country__c, s.country__r.Name
						From Site__c s 
						Where s.Business_Unit__c = : buId
						and ((BDTDeleted__c  = :showDeleted) or (BDTDeleted__c  = false))
						];
		}catch(QueryException e){
				
		}
	}
	
	public PageReference EditBU(){
		String EditbuId = System.currentPageReference().getParameters().get('selectedBU');
		
		PageReference newEditBUPage = new PageReference(System.Page.BDT_NewEditBusinessUnit.getUrl());
		newEditBUPage.getParameters().put('buId',EditbuId);
		return newEditBUPage;
	}
	
	public PageReference EditSite(){
		siteId = System.currentPageReference().getParameters().get('siteId');
		String EditbuId = System.currentPageReference().getParameters().get('selectedBU');
				
		PageReference newEditSitePage = new PageReference(System.Page.BDT_NewEditSite.getUrl());		
		newEditSitePage.getParameters().put('buId',EditbuId);
		if(siteId!=null && siteId!=''){
			newEditSitePage.getParameters().put('siteId',siteId);
		}
		
		return newEditSitePage;
	}
	
}