/**
@author Bhargav Devaram
@date 2015
@description this test component class to display the graph of Sites and Subject Enrollment.
**/
@isTest
private class PBB_ViewSitesandSubjectgraphsCntrlTest{
    static testMethod void testPBB_ViewSitesandSubjectgraphsCntrl() {        
        /**
        * Init Test Utils
        */
        test.startTest();
         PBB_TestUtils tu = new PBB_TestUtils();
         WFM_Project__c wf = tu.createWfmProject();
         System.debug('********CREATED WFM PROJECT*******'+wf);
         SRM_Model__c smm = tu.createSRMModelAttributes();
         Bid_Project__c b = tu.createBidProject();
         Country__c  c1 = tu.createCountryAttributes();
         PBB_Scenario__c pb = tu.createScenarioAttributes();
         pb.protocol_name__c = 'ProtoRal' ;
         pb.status__c = 'Approved';
         upsert pb;
         pb.bid_status__c = 'Awarded';
         Wfm_protocol__c protocol = tu.createProtocol();
         protocol_country__c pc = tu.createProtocolCountry();
         country_weekly_event__c cw = tu.createCountryWeeklyEvents();
         
         PBB_ViewSitesandSubjectgraphsCntrl  vs = new PBB_ViewSitesandSubjectgraphsCntrl ();
         vs.protocolUniqueKey = protocol.protocal_unique_key__c;
       
         vs.chartOption ='Project Level';
         vs.chartType ='spline';  
         vs.GraphType ='SitesActivated';
         vs.cntry ='Spain';
         vs.generateGraphandTableData();
         vs.GraphType ='SubjectsEnrollment';
         vs.generateGraphandTableData();
         vs.getCntryList();
         
       
         test.stopTest();    
    } 
}