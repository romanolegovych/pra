@isTest
private class BDT_GeneralTemplateTest {
	
	public static User myUser {get;set;}
	
	static void init(){
		Profile p = [select id from profile where name='System Administrator'];
        myUser = new User(alias = 'standt', email='sysadmin@testorg.com',
            emailencodingkey='UTF-8', lastname='Antigels', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='charlies.antigels@testorg.com');
        insert myUser;
			
	}

    static testMethod void myUnitTest() {
    	init();
    	System.runAs(myUser) {
			BDT_GeneralTemplate a = new BDT_GeneralTemplate();
			a.setRightBarPreference('true');
			
			BDT_GeneralTemplate.setRightNavigationPreferences('true');
			
			BDT_GeneralTemplate b = new BDT_GeneralTemplate();
			System.assertEquals(b.rightBarPreference, 'true');
    	}
    }
}