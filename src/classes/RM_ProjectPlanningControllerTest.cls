/**

 */
@isTest
private class RM_ProjectPlanningControllerTest {
    static list<Country__c> lstCountry;
    static List<Business_Unit__c> lstLocation;       
    static list<Employee_Details__c> lstEmpl; 
    static list<Employee_Status__c> lstEmplStatus;    
    static list<WFM_Client__c> lstClient;
    static list<WFM_Contract__c>  lstContract;
    static list<WFM_Project__c> lstProject;
    static list<WFM_Country_Hours__c> lstCountryHrs;
    static List<WFM_Location_Hours__c> lstLocationHrs;
    static list<WFM_JC_Group_Mapping__c> lstJCGmap;
    static list<WFM_BU_Group_Mapping__c> lstBUGmap;
    static list<PRA_Business_Unit__c> lstpraBU;
    static list<Group> lstGroup;
    static list<Job_Class_Desc__c> lstJobs;
    static WFM_employee_Allocations__c alloc; 
    static WFM_employee_Allocations__c alloc1;   
    static List<WFM_Employee_Assignment__c> lstAssigns;
    static List<WFM_Employee_Assignment__c> lstAssigns1;
    static List<WFM_Employee_Availability__c> lstAvas;
    static List<WFM_Project_demand__c>lstProjectDemand;
    static RM_UnitTestData init(){
        
        RM_UnitTestData testData = new RM_UnitTestData();
        lstCountry = testData.lstCountry;
        lstLocation = testData.lstLocation;
        lstEmpl = testData.lstEmpl;
        lstClient = testData.lstClient;
        lstContract = testData.lstContract;
        lstProject = testData.lstProject;
        lstLocationHrs = testData.insertLocationHr(1,12,'test');
        lstpraBU = testData.lstpraBU ;
        lstJCGmap = testData.lstJCGmap;
        lstBUGmap = testData.lstBUGmap;
        lstAvas = testData.insertAvailability(lstEmpl[0],lstLocationHrs);
        lstJobs = testdata.lstJobs;
        //WFM_Project_demand__c wfmProjectDemand = lstProjectDemand[0];
        
        system.debug('***********lstJobs'+lstJobs);
        alloc = testData.insertAllocation(lstEmpl[0].id, lstProject[0].id, RM_Tools.GetStringfromDate(RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(-4)), 'mm/dd/yyyy'), RM_Tools.GetStringfromDate(RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(5)), 'mm/dd/yyyy'),  'Project Manager', lstCountry[0].Name);
        alloc1 = testData.insertAllocation(lstEmpl[0].id, lstProject[0].id, RM_Tools.GetStringfromDate(RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(-4)), 'mm/dd/yyyy'), RM_Tools.GetStringfromDate(RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(5)), 'mm/dd/yyyy'),  'Clinical Research Associate',lstCountry[0].Name);
        lstAssigns = testData.insertAssignment(lstEmpl[0].id, alloc, lstAvas, 0.2);
        lstAssigns1 = testData.insertAssignment(lstEmpl[0].id, alloc1, lstAvas, 0.5);
        return testData;
    }
    
    static testMethod void initialLoadUnitTest() {        
        RM_UnitTestData testData = init();
        testData.insertProjDemand();
        test.startTest();
        apexpages.currentpage().getparameters().put('Id',lstProject[0].Id);
        RM_ProjectPlanningController RPPC = new RM_ProjectPlanningController();         
        List<SelectOption> allCountries = RPPC.allCountries;
        List<SelectOption> allLocations = RPPC.allLocations;
        RPPC.getBusinessUnit();
        //RPPC.getLocations();
        //RPPC.getGeography();
        string allCountriesJSON = RPPC.allCountriesJSON;
        string allLocationsJSON = RPPC.allLocationsJSON;
        RPPC.SelectedRole = lstEmpl[0].Job_Class_Desc__c;
        RPPC.modeChange = 'true';
        RPPC.ishours = 'true';
        RPPC.type = 'FTE';
        
        RPPC.monthList = RM_Tools.GetMonthsList(-6, 24);
        RPPC.graphMonthList = RM_Tools.GetMonthsList(0,12);        
        RPPC.getRoleData();               
               
    }
    
    static testMethod void plannedvaluesMapUnitTest() {        
        RM_UnitTestData testData = init();
        testData.insertProjDemand();
        test.startTest();
        apexpages.currentpage().getparameters().put('Id',lstProject[0].Id);
        RM_ProjectPlanningController RPPC = new RM_ProjectPlanningController();         
        List<SelectOption> allCountries = RPPC.allCountries;
        List<SelectOption> allLocations = RPPC.allLocations;
        RPPC.getBusinessUnit();
        //RPPC.getLocations();
        //RPPC.getGeography();
        string allCountriesJSON = RPPC.allCountriesJSON;
        string allLocationsJSON = RPPC.allLocationsJSON;
        RPPC.selectedGraphYrMonth ='2014-12';
        RPPC.SelectedRole = lstEmpl[0].Job_Class_Desc__c;
        RPPC.modeChange = 'true';
        RPPC.ishours = 'true';
        RPPC.type = 'FTE';
        RPPC.monthList = RM_Tools.GetMonthsList(-6, 24);
        RPPC.graphMonthList = RM_Tools.GetMonthsList(0,12);        
        RPPC.getRoleData();               
               
    }
     static testMethod void intialLoadNoRoleUnitTest() {        
        RM_UnitTestData testData = init();
        testData.insertProjDemand();
        test.startTest();
        apexpages.currentpage().getparameters().put('Id',lstProject[0].Id);
        RM_ProjectPlanningController RPPC = new RM_ProjectPlanningController();         
        List<SelectOption> allCountries = RPPC.allCountries;
        List<SelectOption> allLocations = RPPC.allLocations;
        RPPC.getBusinessUnit();
        //RPPC.getLocations();
        //RPPC.getGeography();
        string allCountriesJSON = RPPC.allCountriesJSON;
        string allLocationsJSON = RPPC.allLocationsJSON;
        RPPC.SelectedRole = '';
        RPPC.modeChange = 'true';
        RPPC.ishours = 'true';
        RPPC.type = 'FTE';
        RPPC.monthList = RM_Tools.GetMonthsList(-6, 24);
        RPPC.graphMonthList = RM_Tools.GetMonthsList(0,12);        
        RPPC.getRoleData();               
               
    }
   static testMethod void saveUnitTest() {       
        RM_UnitTestData testData = init();
        testData.insertProjDemand();
        test.startTest();
        apexpages.currentpage().getparameters().put('Id',lstProject[0].Id);
        RM_ProjectPlanningController RPPCSave = new RM_ProjectPlanningController();
        RPPCSave.selectedBU=lstpraBU[0].Name;
        RPPCSave.previewPlanned='1.89,,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89';
        RPPCSave.previewIsHour = 'Hours'  ;
        RPPCSave.previewValue =1.0;
        RPPCSave.previewCntry =lstcountry[0].name;
        RPPCSave.previewLocation=lstLocation[0].Name;     
        RPPCSave.previewStartMonth ='Jan';
        RPPCSave.previewEndMonth ='Feb';
        RPPCSave.previewStartYear ='2013';
        RPPCSave.previewEndYear= '2015';
        RPPCSave.previewRole=lstJobs[0].Name;        
        RPPCSave.saveRolePlannedValues();
        test.stopTest(); 
    
    }
    static testMethod void method1UnitTest() {
        RM_UnitTestData testData = init();
        testData.insertProjDemand();
        test.startTest();
        apexpages.currentpage().getparameters().put('Id',lstProject[0].Id);
        RM_ProjectPlanningController RPPCSave = new RM_ProjectPlanningController();
        RPPCSave.ishours='true';
        RPPCSave.previewPlanned='1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89';
        RPPCSave.previewIsHour = 'FTE'  ;
        RPPCSave.previewValue =1.0;
        RPPCSave.previewCntry =lstcountry[0].name;
        RPPCSave.previewLocation=lstLocation[0].Name;     
        RPPCSave.previewStartMonth ='Mar';
        RPPCSave.previewEndMonth ='Apr';
        RPPCSave.previewStartYear ='2013';
        RPPCSave.previewEndYear= '2015';
        RPPCSave.previewRole=lstJobs[0].Name;        
        RPPCSave.saveRolePlannedValues();
        test.stopTest();
    }
    static testMethod void method2UnitTest() {
        RM_UnitTestData testData = init();
        testData.insertProjDemand();
        test.startTest();
        apexpages.currentpage().getparameters().put('Id',lstProject[0].Id);
        RM_ProjectPlanningController RPPCSave = new RM_ProjectPlanningController();
        RPPCSave.previewPlanned='1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89';
        RPPCSave.previewIsHour = 'FTE'  ;
        RPPCSave.previewValue =1.0;
        RPPCSave.previewCntry =lstcountry[0].name;
        RPPCSave.previewLocation=lstLocation[0].Name;     
        RPPCSave.previewStartMonth ='May';
        RPPCSave.previewEndMonth ='Jun';
        RPPCSave.previewStartYear ='2013';
        RPPCSave.previewEndYear= '2015';
        RPPCSave.previewRole=lstJobs[0].Name;        
        RPPCSave.saveRolePlannedValues();
        test.stopTest();
    }
    static testMethod void projectNullUnitTest() {
            
        RM_UnitTestData testData = init();
        testData.InsertCountry(2);
        testData.insertCountryHr(1,12,lstcountry[0].Id);
        test.startTest();
        apexpages.currentpage().getparameters().put('Id',lstProject[0].Id);
        RM_ProjectPlanningController RPPCSave = new RM_ProjectPlanningController();
        RPPCSave.previewPlanned='1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89';
        RPPCSave.previewIsHour = 'Hours'  ;
        RPPCSave.previewValue =1.0;
        RPPCSave.previewCntry =lstcountry[0].name;
        RPPCSave.previewLocation=lstLocation[0].Name;     
        RPPCSave.previewStartMonth ='Jul';
        RPPCSave.previewEndMonth ='Aug';
        RPPCSave.previewStartYear ='2013';
        RPPCSave.previewEndYear= '2015';
        RPPCSave.previewRole=lstJobs[0].Name;        
        RPPCSave.saveRolePlannedValues();
        test.stopTest();
    }
    static testMethod void method4UnitTest() {
        RM_UnitTestData testData = init();
        testData.insertProjDemand();
        test.startTest();
        apexpages.currentpage().getparameters().put('Id',lstProject[0].Id);
        RM_ProjectPlanningController RPPCSave = new RM_ProjectPlanningController();
        RPPCSave.previewPlanned='1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89';
        RPPCSave.previewIsHour = 'Hours'  ;
        RPPCSave.previewValue =1.0;
        RPPCSave.previewCntry =lstcountry[0].name;
        RPPCSave.previewLocation=lstLocation[0].Name;     
        RPPCSave.previewStartMonth ='Oct';
        RPPCSave.previewEndMonth ='Sep';
        RPPCSave.previewStartYear ='2013';
        RPPCSave.previewEndYear= '2013';
        RPPCSave.previewRole=lstJobs[0].Name;        
        RPPCSave.saveRolePlannedValues();
        test.stopTest();
    }
    static testMethod void method5UnitTest() {
        RM_UnitTestData testData = init();
        testData.insertProjDemand();
        test.startTest();
        apexpages.currentpage().getparameters().put('Id',lstProject[0].Id);
        RM_ProjectPlanningController RPPCSave = new RM_ProjectPlanningController();
        RPPCSave.previewPlanned='1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89,1.89';
        RPPCSave.previewIsHour = 'Hours'  ;
        RPPCSave.previewValue =1.0;
        RPPCSave.previewCntry =lstcountry[0].name;
        RPPCSave.previewLocation=lstLocation[0].Name;     
        RPPCSave.previewStartMonth ='Nov';
        RPPCSave.previewEndMonth ='Dec';
        RPPCSave.previewStartYear ='2013';
        RPPCSave.previewEndYear= '2015';
        RPPCSave.previewRole=lstJobs[0].Name;        
        RPPCSave.saveRolePlannedValues();
        test.stopTest();
    }
  
}