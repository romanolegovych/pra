/** Implements the test for the Service Layer of the object FinancialDocumentHistory__c
 * @author	Dimitrios Sgourdos
 * @version	15-Jan-2014
 */
@isTest
private class FinancialDocumentHistoryServiceTest {
	
	// Global variables
	private static FinancialDocument__c financialDocument;
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	15-Jan-2014
	 */
	static void init(){
		// Create project
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001', 'BDT Project First Experiments');
		insert currentProject;
		
		// Create financial document
		financialDocument = new FinancialDocument__c();
		financialDocument.DocumentType__c	= 'Proposal';
		financialDocument.Client_Project__c	= currentProject.Id;
		insert financialDocument;
	}
	
	
	/** Test the function saveHistoryObject
	 * @author	Dimitrios Sgourdos
	 * @version	15-Jan-2014
	 */
	static testMethod void saveHistoryObjectTest() {
		// Create data
		init();
		
		// Check the function
		Boolean result = FinancialDocumentHistoryService.saveHistoryObject( financialDocument.Id,
																			'Test comment',
																			'Test change',
																			'100 EUR',
																			'Test process');
		
		String errorMessage = 'Error in creating and saving  Financial Document History instance';
		system.assertEquals(result, true, errorMessage);
		
		// Check the function by creating an exception (for code coverage)
		String bigComment = 'This comment will be more than 255 characters. Currently, the comment is 87 characters.';
		for(Integer i=0; i<2; i++) {
			bigComment += bigComment;
		}
		
		result = FinancialDocumentHistoryService.saveHistoryObject( financialDocument.Id,
																			bigComment,
																			'Test change',
																			'100 EUR',
																			'Test process');
		system.assertEquals(result, false, errorMessage);
	}
}