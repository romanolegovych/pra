/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PBB_GetPatientEnrolledActualsBatchTest {
    
     private static WFM_Protocol__c protocol;
     private static WFM_Site_Detail__c site;
     private static Protocol_Country__c protocolCountry ;
     private static List<Country_Weekly_Event__c> countryWeeklylist;
     private static WFM_Project__c wfmp;
     private static Country__c country;
     
      static void createTestData() {
        PBB_TestUtils testUtils = new PBB_TestUtils();
        country = testUtils.createCountryAttributes();
        wfmp = testUtils.createWfmProject();
        protocol = testUtils.createProtocol();
        site = testUtils.createSite();
        protocolCountry = testUtils.createProtocolCountry();
        countryWeeklylist = testUtils.createPlannedWeeklyEvents();
        testUtils.updateWithPrevWeeks(countryWeeklylist);
        testUtils.updateWithActuals(countryWeeklylist);
       }
       
      static testMethod  void myUnitTest() {
        createTestData();
              
        Test.startTest();
        Datetime ProcessstartDate = system.now();
        String paramsJSONStringUnit = Json.serialize(new List<String>{'WFM_Protocol__c', 'Country_Weekly_Event__c'});
        String unitBatchQueueId = PRA_BatchQueue.addJob('PBB_GetPatientEnrolledActualsBatch',paramsJSONStringUnit, null);
        
        Test.stopTest();

        system.assertNotEquals(null, unitBatchQueueId); 
        
    }

}