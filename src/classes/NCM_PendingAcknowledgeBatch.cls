/**
 * @description	The Batch class for the Pending Acknowledgement Schedule
 * @author		Kalaiyarasan Karunanithi
 * @date		Created: 04-Sep-2015
 */
global class NCM_PendingAcknowledgeBatch implements Database.Batchable<SObject>, Database.Stateful  {

	/**
	* @description Constructor of the class
	* @author      Kalaiyarasan Karunanithi
	* @date        Created: 04-Sep-2015
	**/
	global NCM_PendingAcknowledgeBatch() {
		
	}
	
	/**
	* @description  The class implementing Database.Batchable must implement the start method.
					Use the start method to collect the records or objects to be passed to the interface method execute.
					This start method populates the QueryLocator object with all notifications that the assigned user
					should be reinformed.
	* @author       Kalaiyarasan Karunanithi  
	* @date         Created: 04-Sep-2015
	**/
	global Database.queryLocator start(Database.BatchableContext ctx){
		return NCM_DataAccessor.getNotificationAcknowledgingforSchedule(); 
	}
	
	/**
	 * @description	This class must also implement the execute method. 
	 *				The execute method is called for each batch of records passed to the method. 
	 *				Use this method to do all required processing for each chunk of data. This execute method reinform
	 *				the users of the notifications.
	 * @author		Kalaiyarasan Karunanithi, Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015, Edited: 14-Sep-2015
	**/
	global void execute(Database.BatchableContext ctx, List<Notification__c> notificationsList) {
		// Iterate through results (Notification object) and
		// create a new record in the 'Notification History' based on the condition
		List<NotificationHistory__c> notificationsHistoryList = new List<NotificationHistory__c>();
		
		List<Messaging.SingleEmailMessage> notificationEmailsList = new List<Messaging.SingleEmailMessage>();
		
		// Find the org wide email id for setting up the from address in the emails
		Id orgWideFromAddressId = NCM_Email_API.getOrgWideEmailAddressIdFromAddressName(
																			NCM_API_DataTypes.ORG_WIDE_EMAIL_ADDRESS);
		
		for( Notification__c notification : notificationsList ) {
			Decimal maxNum = (notification.NotificationRegistration__r.MaxNumberReminders__c == NULL)?
														0 :
														notification.NotificationRegistration__r.MaxNumberReminders__c;
			
			// Create notification history entry
			if( notification.NotificationsHistory__r.size()	<= maxNum) {// <= because there is also record from the
																		// initial communication
				// Add notification history instance
				Boolean smsSentFlag = notification.NotificationRegistration__r.NotifyBySMS__c
										&& notification.NotificationRegistration__r.NotificationCatalog__r.SMSallowed__c;
				
				NotificationHistory__c newHistory = new NotificationHistory__c();
				newHistory.Notification__c = notification.Id;
				newHistory.EmailSent__c = notification.NotificationRegistration__r.NotifyByEmail__c;
				newHistory.SMSsent__c = smsSentFlag;
				notificationsHistoryList.add( newHistory );
				
				// Add email instance
				if(newHistory.EmailSent__c) {
					notificationEmailsList.add( NCM_Email_API.createEmailInstance(
														notification.NotificationRegistration__r.UserId__c,
														orgWideFromAddressId,
														'REMINDEER: ' + notification.NotificationEvent__r.Subject__c,
														notification.NotificationEvent__r.EmailBody__c,
														NULL)
											);
				}
			}
			
			// Shift the reminder
			Decimal tmpReminderPeriodDec = (notification.NotificationRegistration__r.ReminderPeriod__c == NULL)?
															0 :
															notification.NotificationRegistration__r.ReminderPeriod__c;
			Integer reminderPeriodInt = Integer.valueOf(tmpReminderPeriodDec);
			
			notification.Reminder__c = notification.Reminder__c.addHours( reminderPeriodInt );
		}
		
		insert notificationsHistoryList;
		update notificationsList;
		NCM_Email_API.sendEmailInstances(notificationEmailsList);
	}
	
	
	/**
	* @description  This class must also implement the finish method.
	 				The finish method is called after all batches are processed.
	* @author       Kalaiyarasan Karunanithi  
	* @date         Created: 04-Sep-2015
	**/
	global void finish(Database.BatchableContext ctx) {
		
	}

}