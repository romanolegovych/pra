/**
*   'HR_TestUtils' is the TestUtil class used to create the data for all HR related test classes.
*   @author   Devaram Bhargav
*   @version  26-Sep-2013
*   @since    17-Sep-2013
*/
public with sharing class HR_TestUtils {
    /** CountryList variable is to hold the list of Country records*/  
    public List<Country__c> CountryList;
    
    /** EmplList variable is to hold the list of Employee_Details__c records*/ 
    public list<Employee_Details__c> EmplList;
    
    /** Hrm variable is to hold the HR_Manager_Mapping__c record*/ 
    public HR_Manager_Mapping__c  Hrm;
    
    /** hrempl variable is to hold the list of HR_Employee_Details__c record*/
    public HR_Employee_Details__c  hrempl;
    
    /** Inserting Custom Setting Record */
    public HRLawsonReasonMapping__c HrLawson;
    /** HREI variable is to hold the list of HR_Exit_Interview__c record*/ 
    public HR_Exit_Interview__c HREI;
    
    /** QuesLst variable is to hold the list of Questionnaire__c record*/ 
    public List<Questionnaire__c> QuesLst;
    
    /**
    *   This Method is to create the test data.          
    */
    public void HR_TestUtils(){
        //Country data
        CountryList = new list<Country__c>();
        Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0);
        CountryList.add(country);
        Country__c country1 = new Country__c(name='TestCountry2',PRA_Country_ID__c='101', Country_Code__c='T2',Region_Name__c='North America2', 
        daily_Business_Hrs__c=7.5);
        CountryList.add(country1);
        insert CountryList;     
        
        //Employee Detail      
        EmplList = new list<Employee_Details__c>();        
        Employee_Details__c empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='1000', Employee_Unique_Key__c='1000', First_Name__c='John', Last_Name__c='TestSmith', email_Address__c='JSmith@gmail.com', Job_Class_Desc__c='Clinical Research Associate',
        Function_code__c='TT',Buf_Code__c='TSTTT', State_Province__c = 'TestState', Business_Unit__c='TST', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), Manager_Last_Name__c='TestManager',  Manager_First_Name__c='TestFirst', 
        FTE_Equivalent__c = 0.8, NBR_PRA_Monitoring_Yrs__c = 5.0, NBR_Non_PRA_Monitoring_Yrs__c = 6.0, NBR_AT_PRA_Yrs__c=5.0, supervisor_id__c = '1020');        
        EmplList.add(empl);
        
        Employee_Details__c emplManager = new Employee_Details__c(COUNTRY_name__c = country.id, name='1020', Employee_Unique_Key__c='1020', First_Name__c='John', Last_Name__c='Manager', email_Address__c='JManager@gmail.com', Job_Class_Desc__c='Clinical Research Associate',
        Function_code__c='TT',Buf_Code__c='TSTTT', State_Province__c = 'TestState', Business_Unit__c='TST', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), Manager_Last_Name__c='TestManager',  Manager_First_Name__c='TestFirst', 
        FTE_Equivalent__c = 0.8, NBR_PRA_Monitoring_Yrs__c = 5.0, NBR_Non_PRA_Monitoring_Yrs__c = 6.0, NBR_AT_PRA_Yrs__c=5.0);
        EmplList.add(emplManager);
        
        Employee_Details__c emplMove = new Employee_Details__c(COUNTRY_name__c = country1.id, name='1010', Employee_Unique_Key__c='1010', First_Name__c='Cathrine', Last_Name__c='Smith',email_Address__c='CSmith@gmail.com', 
        FTE_Equivalent__c = 1.0, Function_code__c='CY',Buf_Code__c='KCICY', State_Province__c = 'TestState', Job_Class_Desc__c='Clinical Research Associate', Business_Unit__c='KCI', Status__c='AA', Date_Hired__c=date.parse('12/27/2011'));
        EmplList.add(emplMove);
        insert EmplList;   
        
        //HR Manager Mapping Detail data
        Hrm=new HR_Manager_Mapping__c();
        hrm.HR_Manager_ID__c=emplManager.id;
        insert hrm;
        
        // Custom Setting Mapping
        HrLawson=new HRLawsonReasonMapping__c(Name='1',HR_Reason__c='Other',Lawson_Code__c='T-OTHER');
        insert HrLawson;
        //HR Employee Details data
        hrempl= new HR_Employee_Details__c(Employee_ID__c=empl.id,Reason_Code_1__c='T-OTHER',Reason_Code_2__c='T-OTHER',HR_Manager_ID__c=hrm.id,Employee_Unique_Key__c='1000');
        insert hrempl;
        
        hrei=new HR_Exit_Interview__c(HR_Employee_Details__c=hrempl.id,Interviewed__c='Interview Scheduled',Compensation_Benefits__c=true);
        hrei.Exit_Interview_Date__c=date.today();
        hrei.HR_Representative_ID__c=emplManager.id;
        hrei.Compensation_Benefits__c=true;
        hrei.PrimarySecondary_for_CompBenefit__c='Primary reason';
        hrei.CompensationBenefits_SubCategories__c='Salary';
        hrei.Personal__c=true;
        hrei.PrimarySecondary_Personal__c='Secondary reason';
        hrei.Personal_Subcategories__c='Family reasons';
        insert hrei;
        
        hrei=new HR_Exit_Interview__c(HR_Employee_Details__c=hrempl.id,Interviewed__c='Interview Scheduled',Compensation_Benefits__c=true);
        hrei.Exit_Interview_Date__c=date.today();
        hrei.HR_Representative_ID__c=emplManager.id;
        hrei.Compensation_Benefits__c=true;
        hrei.PrimarySecondary_for_CompBenefit__c='Secondary reason';
        hrei.CompensationBenefits_SubCategories__c='Salary';
        hrei.Personal__c=true;
        hrei.PrimarySecondary_Personal__c='Primary reason';
        hrei.Personal_Subcategories__c='Family reasons';
        insert hrei;
        
        hrei=new HR_Exit_Interview__c(HR_Employee_Details__c=hrempl.id,Interviewed__c='Interview Scheduled',New_Career_Opportunity__c=true);
        hrei.Exit_Interview_Date__c=date.today();
        hrei.HR_Representative_ID__c=emplManager.id;
        hrei.Compensation_Benefits__c=true;
        hrei.PrimarySecondary_Newcareeropporunity__c='Secondary reason';
        hrei.NewCareer_Opportunity_SubCategories__c='Salary';
        hrei.Relocation__c=true;
        hrei.PrimarySecondary_Relocation__c='Primary reason';
        hrei.Relocation_Subcategories__c='Family reasons';
        insert hrei;
        
        hrei=new HR_Exit_Interview__c(HR_Employee_Details__c=hrempl.id,Interviewed__c='Interview Scheduled',New_Career_Opportunity__c=true);
        hrei.Exit_Interview_Date__c=date.today();
        hrei.HR_Representative_ID__c=emplManager.id;
        hrei.Compensation_Benefits__c=true;
        hrei.PrimarySecondary_Newcareeropporunity__c='Primary reason';
        hrei.NewCareer_Opportunity_SubCategories__c='Salary';
        hrei.Relocation__c=true;
        hrei.PrimarySecondary_Relocation__c='Secondary reason';
        hrei.Relocation_Subcategories__c='Family reasons';
        insert hrei;
        
        hrei=new HR_Exit_Interview__c(HR_Employee_Details__c=hrempl.id,Interviewed__c='Interview Scheduled',Result_of_Leadership__c=true);
        hrei.Exit_Interview_Date__c=date.today();
        hrei.HR_Representative_ID__c=emplManager.id;
        hrei.Compensation_Benefits__c=true;
        hrei.Primary_Secondary_Result_of_Leadership__c='Primary reason';
        hrei.Result_of_Leadership_Subcategories__c='Salary';
        hrei.Others__c=true;
        hrei.Primary_Secondary_Other__c='Secondary reason';
        hrei.Others_Subcategories__c='Family reasons';
        insert hrei;
        
        hrei=new HR_Exit_Interview__c(HR_Employee_Details__c=hrempl.id,Interviewed__c='Interview Scheduled',Result_of_Leadership__c=true);
        hrei.Exit_Interview_Date__c=date.today();
        hrei.HR_Representative_ID__c=emplManager.id;
        hrei.Compensation_Benefits__c=true;
        hrei.Primary_Secondary_Result_of_Leadership__c='Secondary reason';
        hrei.Result_of_Leadership_Subcategories__c='Salary';
        hrei.Others__c=true;
        hrei.Primary_Secondary_Other__c='Primary reason';
        hrei.Others_Subcategories__c='Family reasons';
        insert hrei;
        
        //Questionnaires list data
        QuesLst=new List<Questionnaire__c>();
        Questionnaire__c qn1=new Questionnaire__c();
        qn1.Form_Type__c='Exit Interview';
        qn1.Question_Text__c='How is PRA?';
        qn1.Question_Type__c='Multi-picklist';
        qn1.Question_Choices__c='good;bad;better;';
        qn1.Question_Order__c=1;
        qn1.Required__c=true;
        qn1.Section__c='Company Information';
        qn1.Section_Order__c=1;
        qn1.Active__c=true;
        QuesLst.add(qn1);       
        
        Questionnaire__c qn2=new Questionnaire__c();
        qn2.Form_Type__c='Exit Interview';
        qn2.Question_Text__c='How are colleagues in PRA?';
        qn2.Question_Type__c='picklist';
        qn2.Question_Choices__c='good;bad;better;';
        qn2.Question_Order__c=2;
        qn2.Required__c=true;
        qn2.Section__c='Persoanl Information';
        qn2.Section_Order__c=2;
        qn2.Active__c=true;
        QuesLst.add(qn2);        
        insert QuesLst;    
    }
}