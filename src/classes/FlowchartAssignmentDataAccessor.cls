public with sharing class FlowchartAssignmentDataAccessor {

	/** Object definition for fields used in application for FlowchartAssignment__c
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('FlowchartAssignment__c');		
	}
	
	
	/** Object definition for fields used in application for FlowchartAssignment__c with the parameter referenceName as a prefix
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Arm__c,';
		result += referenceName + 'epoch__c,';
		result += referenceName + 'Flowchart__c';
		return result;
	}
	
	/** Retrieve list of flowchartassignments.
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @param	whereClause			The criteria that FlowchartAssignment Lists must meet
	 * @return	List of arms
	 */
	public static List<FlowchartAssignment__c> getFlowchartAssignmentList(String whereClause) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM FlowchartAssignment__c ' +
								'WHERE  {1} ' +
								'ORDER BY Arm__r.Arm_Number__c, Epoch__r.Epoch_Number__c',
								new List<String> {
									getSObjectFieldString(),
									whereClause
								}
							);
		return (List<FlowchartAssignment__c>) Database.query(query);
	}
}