/** The Controller of the page BDT_FinancialDocApproval
 * @author	Dimitrios Sgourdos
 * @version	24-Jan-2014
 */
public with sharing class BDT_FinancialDocApprovalController {
	
	// Global Variables
	public BDT_SecuritiesService.pageSecurityWrapper 	securitiesA 		{get;set;} // Financial Documents Approval
	public FinancialDocument__c 						financialDocument	{get;set;}
	public String 										financialDocumentId	{get;set;}
	public Boolean										showMainSection		{get;set;}
	public Boolean 										showNotesSection	{get;set;}
	public Boolean										showOwnerNotesOption{get;set;}
	public Boolean										documentOwnerFlag	{get;set;}
	public List<FinancialDocumentApproval__c>			documentApprovals	{get;set;}
	public String										currentUserEmail	{get;set;}
	public String										currentUserFullName	{get;set;}
	public String										updatedApprovalIndex{get;set;}
	public String										deletedApprovalIndex{get;set;}
	public String										updateJobAndLocIndex{get;set;}
	public String										approveRejectIndex	{get;set;}
	public  String 										approveRejectComment{get;set;}
	private List<FinancialDocumentApproval__c>			approvalsDeletedList;
	private List<User>									plenboxUsers;
	private Map<String, List<Employee_Details__c>> 		plenboxEmployeesMap;
	// Variables for transfer global Constants to page
	public String	ApprovalApproved		{get;set;}
	public String	ApprovalRejected		{get;set;}
	public String	DocumentStatusInvalid	{get;set;}
	public String	DocumentAccepted		{get;set;}
	
	
	/** The constructor of the class.
	 * @author	Dimitrios Sgourdos
	 * @version	21-Jan-2014
	 */
	public BDT_FinancialDocApprovalController() {
		init();
		readConstants();
		if( readFinancialDocument() ) {
			readCurrentUserInfo();
			plenboxUsers = BDT_UserRolesService.getBDTusers();
			readPlenboxEmployeesMappedOnFullName(plenboxUsers);
			documentOwnerFlag = isCurrentUserTheDocumentOwner();
			documentApprovals = FinancialDocumentApprovalService.getApprovalsByDocumentId(financialDocumentId,
																						'SequenceNumber__c');
			// If the user is the owner recalculate the approvals
			if(documentOwnerFlag
					&& financialDocument.ApprovalsUpToDate__c != true
					&& financialDocument.ApprovalStatus__c != DocumentAccepted
					&& financialDocument.DocumentStatus__c != DocumentStatusInvalid) {
				recalculateApprovals();
			}
			showMainSection = true;
		} else {
			errorOnReadingFinancialDocument();
		}
	}
	
	
	/** Initialize the global variables of the controller.
	 * @author	Dimitrios Sgourdos
	 * @version	09-Jan-2014
	 */
	private void init() {
		securitiesA 		 = new BDT_SecuritiesService.pageSecurityWrapper('Financial Documents Approval');
		documentApprovals	 = new List<FinancialDocumentApproval__c>();
		approvalsDeletedList = new List<FinancialDocumentApproval__c>();
		plenboxUsers		 = new List<User>();
		plenboxEmployeesMap	 = new Map<String, List<Employee_Details__c>>();
	}
	
	
	/** Read the global constants for the approvals.
	 * @author	Dimitrios Sgourdos
	 * @version	20-Jan-2014
	 */
	private void readConstants() {
		ApprovalApproved = FinancialDocumentApprovalDataAccessor.APPROVAL_APPROVED;
		ApprovalRejected = FinancialDocumentApprovalDataAccessor.APPROVAL_REJECTED;
		DocumentAccepted = FinancialDocumentDataAccessor.DOCUMENT_ACCEPTED;
		DocumentStatusInvalid = FinancialDocumentDataAccessor.DOCUMENT_STATUS_INVALID;
	}
	
	
	/** Read the current user name and email.
	 * @author	Dimitrios Sgourdos
	 * @version	09-Jan-2014
	 */
	private void readCurrentUserInfo() {
		// Current User E-mail
		currentUserEmail = UserInfo.getUserEmail();
		
		// Current User Name
		String firstName = UserInfo.getFirstName();
		currentUserFullName = (String.isNotBlank(firstName) )? firstName + ' ' : '';
		String LastName = UserInfo.getLastName();
		currentUserFullName += (String.isNotBlank(LastName) )? LastName : '';
		currentUserFullName = currentUserFullName.removeEnd(' ');
	}
	
	
	/** Retrieves all the plenbox known Employees mapped on their full name(FirstName LastName).
	 * @author	Dimitrios Sgourdos
	 * @version 10-Jan-2014
	 */
	private void readPlenboxEmployeesMappedOnFullName(List<User> usersList) {
		plenboxEmployeesMap = FinancialDocumentApprovalService.getEmployeesMappedOnFullName(usersList);
	}
	
	
	/** Checks if the user with the given e-mail end fullname(FirstName LastName) is the owner of the given financial 
	 *	document.
	 * @author	Dimitrios Sgourdos
	 * @version 09-Jan-2014
	 * @return	If the user is the financial document owner.
	 */
	private Boolean isCurrentUserTheDocumentOwner() {
		return FinancialDocumentService.isTheFinancialDocumentOwner(financialDocument,
																	currentUserFullName,
																	currentUserEmail);
	}
	
	
	/**	Read the financial document.
	 * @author   Dimitrios Sgourdos
	 * @version  07-Jan-2013
	 */
	private Boolean readFinancialDocument() {
		financialDocumentID = system.currentPageReference().getParameters().get('financialDocumentID');
		if(String.isBlank(financialDocumentID)) {
			return false;
		} else {
			financialDocument = financialDocumentService.getFinancialDocumentById(financialDocumentId);
			if(financialDocument == NULL) {
				return false;
			}
		}
		return true;
	}
	
	
	/**	Give an error message in case the financial document is not found.
	 * @author   Dimitrios Sgourdos
	 * @version  06-Jan-2013
	 */
	private void errorOnReadingFinancialDocument() {
		showMainSection = false;
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected financial document.Please, select a document and reload the page.');
		ApexPages.addMessage(msg);
	}
	
	
	/**	Update the job and location fields depend on the user selection.
	 * @author   Dimitrios Sgourdos
	 * @version  14-Jan-2013
	 */
	public void updateJobAndLocation() {
		Integer tmpIndex = Integer.valueOf(updateJobAndLocIndex);
		
		// Retrieve from existing employees or initialize a new one
		Employee_Details__c emp = FinancialDocumentApprovalService.getEmployeeByEmailAndFullNameFromMap(
																		documentApprovals[tmpIndex].ApproverName__c,
																		documentApprovals[tmpIndex].ApproverEmail__c,
																		plenboxEmployeesMap);
		if(emp == NULL) {
			emp = new Employee_Details__c();
		}
		
		documentApprovals[tmpIndex].ApproverJobCode__c = emp.Job_Class_Desc__c;
		documentApprovals[tmpIndex].ApproverLocationCode__c = emp.Location_Code__c;
	}
	
	
	/** Show the section of adding a comment while taking ownership.
	 * @author	Dimitrios Sgourdos
	 * @version 15-Jan-2014
	 */
	public void showOwnershipComments(){
		showMainSection		 = false;
		showOwnerNotesOption = true;
		showNotesSection	 = true;
	}
	
	
	/** Give the ownership of the financial document to the current user.
	 * @author	Dimitrios Sgourdos
	 * @version 15-Jan-2014
	 */
	public void takeDocumentOwnership() {
		String oldOwnerName = financialDocument.DocumentOwnerName__c;
		
		// Check if a comment has been added
		if( String.isBlank(approveRejectComment) ) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'A comment is mandatory! Please, add a comment and try again!');
			ApexPages.addMessage(msg);
			return;
		}
		
		// Retrieve from existing employees or initialize a new one
		Employee_Details__c emp = FinancialDocumentApprovalService.getEmployeeByEmailAndFullNameFromMap(
																								currentUserFullName,
																								currentUserEmail,
																								plenboxEmployeesMap);
		if(emp == NULL) {
			emp = new Employee_Details__c(First_Name__c = currentUserFullName, Email_Address__c = currentUserEmail);
		}
		
		// Take document ownership
		Boolean successFlag = FinancialDocumentService.takeDocumentOwnership(financialDocument, documentApprovals[0], emp);
		if( ! successFlag ) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Error in taking the financial document ownership! Please, try again');
			ApexPages.addMessage(msg);
		} else {
			documentOwnerFlag = true;
			// Capture action to history
			String changeDescription = 'Old owner: ' + oldOwnerName + ', New owner: ' + financialDocument.DocumentOwnerName__c;
			FinancialDocumentHistoryService.saveHistoryObject(financialDocument.id,
															approveRejectComment,
															changeDescription,
															financialDocument.TotalValue__c,
															FinancialDocumentHistoryDataAccessor.APPROVAL_PROCESS);
			closeComments();
		}
	}
	
	
	/** Release the financial document for approvals in case the approvals are valid.
	 * @author	Dimitrios Sgourdos
	 * @version 23-Jan-2014
	 */
	public void releaseDocumentForApprovals() {
		Boolean successFlag = FinancialDocumentService.releaseFinancialDocumentForApprovals(financialDocument,
																						documentApprovals);
		if( ! successFlag ) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please, fill "Reason for Approval", "Name" and "Dead-line" for all the approvals and try to release the document again!');
			ApexPages.addMessage(msg);
		} else {
			saveDocumentApprovals();
			
			// Capture action to history
			FinancialDocumentHistoryService.saveHistoryObject(financialDocument.id,
														FinancialDocumentApprovalDataAccessor.RELEASED_APPROVAL_COMMENT,
														FinancialDocumentApprovalDataAccessor.RELEASED_APPROVAL_COMMENT,
														financialDocument.TotalValue__c,
														FinancialDocumentHistoryDataAccessor.APPROVAL_PROCESS);
		}
	}
	
	
	/** Save the current user desire to be informed or not when adjustments happened.
	 * @author	Dimitrios Sgourdos
	 * @version 08-Jan-2014
	 */
	public void saveAdjustmentNotificationChange() {
		upsert documentApprovals[Integer.valueOf(updatedApprovalIndex)];
	}
	
	
	/** Recalculate the Financial Document Approvals for the approval rules that exist in the system.
	 * @author	Dimitrios Sgourdos
	 * @version 21-Jan-2014
	 */
	public void recalculateApprovals() {
		documentApprovals = FinancialDocumentApprovalService.recalculateApprovalsFromApplicableRules(documentApprovals,
																								approvalsDeletedList,
																								financialDocumentId);
	}
	
	
	/** Reset the approve/reject action from the existing approvals except from the document creator's initial approval.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Jan-2014
	 */
	public void resetDocumentApprovals() {
		documentApprovals = FinancialDocumentApprovalService.updateApprovalsSequence(documentApprovals);
		upsert documentApprovals;
		
		financialDocument = FinancialDocumentApprovalService.resetApprovals(documentApprovals, financialDocument);
		
		FinancialDocumentApprovalService.deleteApprovals(approvalsDeletedList);
		approvalsDeletedList = new List<FinancialDocumentApproval__c>();
		
		documentApprovals = FinancialDocumentApprovalService.getApprovalsByDocumentId(financialDocumentId,
																						'SequenceNumber__c');
		
		// Capture action to history
		FinancialDocumentHistoryService.saveHistoryObject(financialDocument.id,
														FinancialDocumentApprovalDataAccessor.RESET_APPROVALS_COMMENT,
														FinancialDocumentApprovalDataAccessor.RESET_APPROVALS_COMMENT,
														financialDocument.TotalValue__c,
														FinancialDocumentHistoryDataAccessor.APPROVAL_PROCESS);
	}
	
	
	/** Save the financial document approvals.
	 * @author	Dimitrios Sgourdos
	 * @version 23-Jan-2014
	 */
	public void saveDocumentApprovals() {
		documentApprovals = FinancialDocumentApprovalService.updateApprovalsSequence(documentApprovals);
		Boolean successFlag = FinancialDocumentApprovalService.saveApprovals(documentApprovals,financialDocument);
		
		if( ! successFlag ) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please, fill "Reason for Approval", "Name" and "Dead-line" for all the approvals and try to release the document again!');
			ApexPages.addMessage(msg);
		} else {
			FinancialDocumentApprovalService.deleteApprovals(approvalsDeletedList);
			
			// Check for warning
			if( financialDocument.IsApprovalReleased__c == false
					&& (!FinancialDocumentApprovalService.isApprovalsValidForRelease(documentApprovals)) ) {
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, '"Reason for Approval", "Name" or "Dead-line" are not filled for all the approvals!');
				ApexPages.addMessage(msg);
			}
			
			// Check if the approvals are up to date
			Boolean oldValueFlag = financialDocument.ApprovalsUpToDate__c;
			Boolean upToDateFlag = FinancialDocumentApprovalService.areDocumentApprovalsUpToDate(financialDocumentId);
			if(oldValueFlag != upToDateFlag) {
				financialDocument.ApprovalsUpToDate__c = upToDateFlag;
				// Update approval and document statuses
				financialDocument = FinancialDocumentService.updateDocumentApprovalStatus(financialDocument);
				update financialDocument;
			} else if(approvalsDeletedList.size() > 0) {
				// Update approval and document statuses
				financialDocument = FinancialDocumentService.updateDocumentApprovalStatus(financialDocument);
				update financialDocument;
			}
			
			// Clear deleted list
			approvalsDeletedList = new List<FinancialDocumentApproval__c>();
			
			// Send approval appointments
			if(financialDocument.IsApprovalReleased__c ) {
				FinancialDocumentApprovalService.sendApprovalAppointments(financialDocument, true);
			}
		}
	}
	
	
	/** Add an approval in the Financial Document Approvals list.
	 * @author	Dimitrios Sgourdos
	 * @version 08-Jan-2014
	 */
	public void addApprover() {
		Integer sequenceNum = documentApprovals.size() +1;
		documentApprovals.add(	FinancialDocumentApprovalService.createApprovalInstance(financialDocumentId,
																						new Employee_Details__c(),
																						NULL,
																						false,
																						NULL,
																						sequenceNum)
							);
	}
	
	
	/** Remove a financial document approval from the approvals list.
	 * @author	Dimitrios Sgourdos
	 * @version 09-Jan-2014
	 */
	public void removeApproval() {
		Integer index = Integer.valueOf(deletedApprovalIndex);
		approvalsDeletedList.add(documentApprovals[index]);
		documentApprovals.remove(index);
	}
	
	
	/** Cancel the procedure of approve or reject a financial document.
	 * @author	Dimitrios Sgourdos
	 * @version 15-Jan-2014
	 */
	public void closeComments(){
		showOwnerNotesOption = false;
		showNotesSection 	 = false;
		showMainSection	 	 = true;
	}
	
	
	/** Show the procedure of approve or reject a financial document.
	 * @author	Dimitrios Sgourdos
	 * @version 15-Jan-2014
	 */
	public void showComments(){
		approveRejectComment = documentApprovals[Integer.valueOf(approveRejectIndex)].ApprovalNotes__c;
		showMainSection		 = false;
		showOwnerNotesOption = false;
		showNotesSection 	 = true;
	}
	
	
	/** Approve the financial document for the current approval.
	 * @author	Dimitrios Sgourdos
	 * @version 20-Jan-2014
	 */
	public void approveDocument(){
		Integer tmpIndex = Integer.valueOf(approveRejectIndex);
		documentApprovals[tmpIndex] = FinancialDocumentApprovalService.approveRejectDocument(
																						documentApprovals[tmpIndex],
																						true,
																						approveRejectComment);
		
		// Update approval and document statuses
		financialDocument = FinancialDocumentService.updateDocumentApprovalStatus(financialDocument);
		
		// Capture action to history
		String changeDescription = documentApprovals[tmpIndex].RuleDescription__c + ' (approved) ';
		FinancialDocumentHistoryService.saveHistoryObject(financialDocument.id,
														approveRejectComment,
														changeDescription,
														financialDocument.TotalValue__c,
														FinancialDocumentHistoryDataAccessor.APPROVAL_PROCESS);
		
		// Send e-mail notifications for adjustments
		FinancialDocumentApprovalService.sendAdjustmentsEmailNotifications(financialDocument,
																FinancialDocumentHistoryDataAccessor.APPROVAL_PROCESS,
																userInfo.getName());
		
		closeComments();
	}
	
	
	/** Reject the financial document for the current approval.
	 * @author	Dimitrios Sgourdos
	 * @version 20-Jan-2014
	 */
	public void rejectDocument(){
		if( String.isNotBlank(approveRejectComment) ) {
			Integer tmpIndex = Integer.valueOf(approveRejectIndex);
			documentApprovals[tmpIndex] = FinancialDocumentApprovalService.approveRejectDocument(
																						documentApprovals[tmpIndex],
																						false,
																						approveRejectComment);
			
			// Update approval and document statuses
			financialDocument = FinancialDocumentService.updateDocumentApprovalStatus(financialDocument);
			
			// Capture action to history
			String changeDescription = documentApprovals[tmpIndex].RuleDescription__c + ' (rejected) ';
			FinancialDocumentHistoryService.saveHistoryObject(financialDocument.id,
															approveRejectComment,
															changeDescription,
															financialDocument.TotalValue__c,
															FinancialDocumentHistoryDataAccessor.APPROVAL_PROCESS);
			
			// Send e-mail notifications for adjustments
			FinancialDocumentApprovalService.sendAdjustmentsEmailNotifications(financialDocument,
																FinancialDocumentHistoryDataAccessor.APPROVAL_PROCESS,
																userInfo.getName());
			
			closeComments();
		} else {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'A comment for the rejection is mandatory! Please, add a comment and try again!');
			ApexPages.addMessage(msg);
		}
	}
}