/**
 * This test class covers the following Apex Classes:
 *	-DWF_WhiteListHomeController
 *	-DWF_WhiteListNewEditController
 *
 * The class DWF_FilterServiceTestMocks provides mock responses
 * to webservice requests.
 */
@isTest
private class DWF_WhiteListControllersUTest {

	static list<MuleServicesCS__c> serviceSettings;
	static list<DWF_System__c> targetSystems;
	static DWF_Whitelist__c whitelist;

	static void init() {
		serviceSettings = new MuleServicesCS__c[] {
			new MuleServicesCS__c(name = 'ENDPOINT', Value__c = 'ENDPOINT'),
			new MuleServicesCS__c(name = 'TIMEOUT', Value__c = '60000'),
			new MuleServicesCS__c(name = 'DwFilterService', Value__c = 'DwFilterService')
		};
		insert serviceSettings;
		
		targetSystems = new DWF_System__c[] {
			new DWF_System__c(name = 'Salesforce'),
			new DWF_System__c(name = 'Success Factors'),
			new DWF_System__c(name = 'Databasics'),
			new DWF_System__c(name = 'AON'),
			new DWF_System__c(name = 'CSD')
		};
		insert targetSystems;
		
		whitelist = new DWF_Whitelist__c(business_unit_name__c = 'bu name 1', business_unit_no__c = 1, company_name__c = 'company name 1',
			company_no__c = 1, DWF_System__c = targetSystems.get(0).Id);
		insert whitelist;
	}
	
	/******** Start DWF_WhiteListHomeController Tests ********/
	static testmethod void testHomeConstructor() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_ALL_COMPANY_VO_RESPONSE));
		DWF_WhiteListHomeController con = new DWF_WhiteListHomeController();
		system.assert(con.selectedView.equals('RecentWL'));
		system.assertNotEquals(con.companyNames, null);
		system.assertNotEquals(con.businessUnits, null);
		con.selectedView = 'MyWL';
		con.searchWhitelists();
		system.debug('----------------------------------------' + con.whiteLists);
		//system.assert(con.whiteLists == null);
		Test.stopTest();
	}
	static testmethod void testDropdownLists() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_ALL_COMPANY_VO_RESPONSE));
		DWF_WhiteListHomeController con = new DWF_WhiteListHomeController();
		list<selectoption> filterTypes = con.getFilterTypes();
		list<selectoption> systems = con.getSystemNames();
		system.assertNotEquals(filterTypes, null);
		system.assert(filterTypes.size() == 3);
		system.assertNotEquals(systems, null);
		system.assert(systems.size() == targetSystems.size() + 1);
		Test.stopTest();
	}
	static testmethod void testGetCompanyAndBUFromValue() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_ALL_COMPANY_VO_RESPONSE));
		DWF_WhiteListHomeController con = new DWF_WhiteListHomeController();
		// set company no, call get from value
		con.selectedCompanyNo = null;
		con.getCompanyFromSelectedValue();
		con.selectedCompanyNo = '1';
		con.getCompanyFromSelectedValue();
		// set bu name, call get from value
		con.selectedBUName = 'MOCK BU NAME 1';
		con.getBusinessUnitFromSelectedValue();
		Test.stopTest();
	}
	static testmethod void testDeleteWhitelist() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_ALL_COMPANY_VO_RESPONSE));
		DWF_WhiteListHomeController con = new DWF_WhiteListHomeController();
		DWF_Whitelist__c wlRec = [select Id from DWF_Whitelist__c where company_no__c = 1];
		// set id and delete
		con.deleteWhitelistID = wlRec.Id;
		con.deleteWhitelist();
		// re-query and assert record was deleted
		try {
			wlRec = [select Id from DWF_Whitelist__c where company_no__c = 1];
		} catch (Exception e) {
			system.assert(e != null);	
		}
		Test.stopTest();
	}
	/********* End DWF_WhiteListHomeController Tests *********/
	
	
	/******** Start DWF_WhiteListNewEditController Tests ********/
	static testmethod void testNewEditConstructor() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_ALL_COMPANY_VO_RESPONSE));
		Apexpages.Standardcontroller stdCon = new Apexpages.Standardcontroller(whitelist);
		// Test edit constructor
		DWF_WhiteListNewEditController con = new DWF_WhiteListNewEditController(stdCon);
		// Test new constructor
		stdCon = new Apexpages.Standardcontroller(new DWF_Whitelist__c());
		con = new DWF_WhiteListNewEditController(stdCon);
		Test.stopTest();
	}
	static testmethod void testSystemsLists() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_ALL_COMPANY_VO_RESPONSE));
		Apexpages.Standardcontroller stdCon = new Apexpages.Standardcontroller(whitelist);
		DWF_WhiteListNewEditController con = new DWF_WhiteListNewEditController(stdCon);
		list<selectoption> systems = con.getSystemNames();
		system.assertNotEquals(systems, null);
		system.assert(systems.size() == targetSystems.size() + 1);
		Test.stopTest();
	}
	static testmethod void testGetCompanyFromValue() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_ALL_COMPANY_VO_RESPONSE));
		Apexpages.Standardcontroller stdCon = new Apexpages.Standardcontroller(whitelist);
		DWF_WhiteListNewEditController con = new DWF_WhiteListNewEditController(stdCon);
		con.getCompanyFromSelectedValue();
		system.assertNotEquals(con.companyVO, null);
		Test.stopTest();
	}
	static testmethod void testSaveWhitelist() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_ALL_COMPANY_VO_RESPONSE));
		Apexpages.Standardcontroller stdCon = new Apexpages.Standardcontroller(whitelist);
		DWF_WhiteListNewEditController con = new DWF_WhiteListNewEditController(stdCon);
		con.saveWhiteList();
		Test.stopTest();
	}
	/********* End DWF_WhiteListNewEditController Tests *********/

}