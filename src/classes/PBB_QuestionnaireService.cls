/** 
@Author Bhargav Devaram
@Date 2015
@Description this service class used for Questionnaires for selected countries and service tasks
*/
public with sharing class PBB_QuestionnaireService {
    
    public static String PROJECT_LEVEL{get {return PBB_Constants.PROJECT_LEVEL;}}
    
    /** 
    @Author Bhargav Devaram
    @Date 2015
    @Description this method is to show error Message
    */
    public static void showErrorMessage(String errorMessage) {
        if(ApexPages.CurrentPage() != null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
        }
        else{
            System.debug('>>> '+errorMessage+' >>>');    
        } 
    }
    
    /** 
    @Author Bhargav Devaram
    @Date 2015
    @Description this method is to get all service areas for PBB Scenario
    */
    public static List<Service_Area__c> getServiceAreasbyPBBScenarioID(ID scenarioId) {
        List<Service_Area__c> SAList = new List<Service_Area__c>();
        AggregateResult[] agSAList= PBB_DataAccessor.getAggregateSA(scenarioId);
        
        Set<String> SAIDList= new Set<String>();
        for (AggregateResult ar : agSAList) {
            SAIDList.add(String.valueOf(ar.get('said')));
        }        
        SAList=PBB_DataAccessor.getServiceAreaByids(SAIDList);
        system.debug('--agSAList--'+agSAList+'==SAIDList='+SAIDList+'=SAList=='+SAList);
        return SAList;
    }
    /** 
    @Author Bhargav Devaram
    @Date 2015
    @Description this method is to get all Questionnaire by Service Areas for PBB Scenario
    */
    public static Map<String,QuestionWrapper> getQuestionnairebyServiceArea(String ServiceAreaID,String PBBScenarioId) { 
        //Questionnaire return list for Service Areas for PBB Scenario ID
        List<QuestionWrapper>  QWList = new List<QuestionWrapper>();
        Map<String,QuestionWrapper> QWMap = new Map<String,QuestionWrapper>();
        //prepare a map with Question Name with Question version with country and its response associated
        Map<String,Map<String,Map<String,String>>> QtoQvtoCntrytoRspMap = new Map<String,Map<String,Map<String,String>>>();
        
        //get all the question names that are answered for the service task for PBB Scenario
        Set<String> AssquestionsIDs = new Set<String>();
        
        for(AggregateResult ags: PBB_DataAccessor.getImpactqtnsversion(ServiceAreaID,PBBScenarioId)){
                                      system.debug('--ags--'+ags);
                //Map for Question version with Country to response map
                Map<String,Map<String,String>> QtoCtoRMap = new Map<String,Map<String,String>>();
                //check if question name exists in map 
                if(!QtoQvtoCntrytoRspMap.containskey(String.valueOf(ags.get('qn')))){                                  
                    QtoQvtoCntrytoRspMap.put(String.valueOf(ags.get('qn')),QtoCtoRMap);                                   
                }
                //get Question version with Country to response map
                QtoCtoRMap = QtoQvtoCntrytoRspMap.get(String.valueOf(ags.get('qn'))); 
                
                //Map for Country to response 
                Map<String,String> CtoRMap = new Map<String,String>();
                //check if question version exists in map 
                if(!QtoCtoRMap.containskey(String.valueOf(ags.get('q')))){                                  
                    QtoCtoRMap.put(String.valueOf(ags.get('q')),CtoRMap);                                   
                }
                //get Country to response map for question version
                CtoRMap = QtoCtoRMap.get(String.valueOf(ags.get('q'))); 
                
                string cntry =PROJECT_LEVEL;  //default if country not found
                if(String.valueOf(ags.get('c'))!=null) 
                    cntry = String.valueOf(ags.get('c'));
                
                //assign the country and its response to the map
                if(!CtoRMap.containskey(cntry)){                                  
                    CtoRMap.put(cntry,String.valueOf(ags.get('r')));                                   
                }                                                       
                CtoRMap.put(cntry, String.valueOf(ags.get('r')))  ;  
                //assign the question version to country and its response map                                  
                QtoCtoRMap.put(String.valueOf(ags.get('q')),CtoRMap); 
                //assign thequestion Name with  question version to country and its response map
                QtoQvtoCntrytoRspMap.put(String.valueOf(ags.get('qn')),QtoCtoRMap);
                
                //get list of question version ids assigned to the service area
                AssquestionsIDs.add(String.valueOf(ags.get('q')));                                                     
            }
            system.debug('--QtoQvtoCntrytoRspMap--'+QtoQvtoCntrytoRspMap);
            //get all questionlist ( assigned ) for service area.
            for(Service_Impact_Questions__c si:PBB_DataAccessor.getqtnlistSA(AssquestionsIDs)){
                                                   
                //prepare the question
                QuestionWrapper qw = new QuestionWrapper (); 
                
                //question to wrapper
                qw.siq = si; 
                
                String DefaultResponse='';    
                SelectOption empty = new SelectOption('Null','');
                qw.ResponseOptions.add(empty);
                //get the response options for the question              
                for(Service_Impact_Response__c sir:si.Service_Impact_Responses__r){
                    SelectOption s = new SelectOption(String.valueof(sir.id),String.valueof(sir.name));
                    if(sir.Default__c==true)
                        DefaultResponse = String.valueof(sir.id);
                    qw.ResponseOptions.add(s);
                }                
                system.debug('--si--'+si.Service_Impact_Question__c);
                
                system.debug('--QtoQvtoCntrytoRspMap--'+QtoQvtoCntrytoRspMap);
                Map<String,Map<String,String>> QtoCtoRMap = new Map<String,Map<String,String>>();                                                
                QtoCtoRMap = QtoQvtoCntrytoRspMap.get(String.valueOf(si.Service_Impact_Question__c));                                   
                
                system.debug('--QtoCtoRMap--'+QtoCtoRMap);
                Map<String,String> CtoRMap = new Map<String,String>();
                CtoRMap = QtoCtoRMap.get(String.valueOf(si.id));
                                                   
                system.debug('--CtoRMap--'+CtoRMap);
                for(String country:CtoRMap.keySet()){
                    String Response = CtoRMap.get(country);
                    system.debug('--Response--'+Response+'--'+DefaultResponse);    
                    if(Response==null)
                        Response = '';
                        //Response = DefaultResponse; 
                    if(si.Question_Type__c!='Different response per country')
                        qw.stResp = Response; 
                    qw.CntrytoRsp.put(country,Response);                              
                }
                system.debug('--qw--'+qw);  
                system.debug('--qw--'+qw.ResponseOptions); 
                qw.ResponseOptions.sort();
                QWList.add(qw);   
                QWMap.put(String.valueof(si.id), qw);                                   
            }
    
        system.debug('--QWList--'+QWList+QWMap);
        return QWMap;
    }
    
    /** 
    @Author Bhargav Devaram
    @Date 2015
    @Description this method is to save all Questionnaire by Service Areas for PBB Scenario
    */
    public static void saveQuestionnairebyServiceArea(String ServiceAreaID,String PBBScenarioId,QuestionWrapper QW,String Country) {
        try{    
            system.debug('--ServiceAreaID--'+ServiceAreaID+PBBScenarioId+Country+QW);
            String SIQId = QW.siq.id ;
            system.debug('--SIQId--'+SIQId);
            
            //prepare query to get all the service impact question for a scenario by country/not
            String query ='select id,name,Service_Impact_Question__c, Service_Impact_Questions__c,Service_Impact_Questions__r.Service_Impact_Question__c,';                                                     
                   query+=' Service_Impact_Response__c ,Countries_Service_Tasks__c, Countries_Service_Tasks__r.PBB_Scenario_Country__r.Country__r.name ';
                   query+=' from  Service_Task_Responses__c ';
                   query+=' where Service_Impact_Questions__c=:SIQId';                                        
                   query+=' and Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__c=:PBBScenarioId';                            
            if(Country!=null && Country!=''){
                if(Country==PBB_Constants.PROJECT_LEVEL)
                    query+=' and Countries_Service_Tasks__r.PBB_Scenario_Country__c=null';
                else
                   query+=' and Countries_Service_Tasks__r.PBB_Scenario_Country__r.Country__r.name=:Country';
            }
            system.debug('--query--'+query);   
            
            //get the response that is selected for the question if by single response or by different response by country 
            String Response;
            if(Country!=null && Country!=''){
                Map<String,String> CntrytoResp = new Map<String,String>();
                Response = (QW.CntrytoRsp).get(Country);
                if(Response=='Null')
                    Response =null;
            }else{
                Response = QW.stResp;
            }
            system.debug('--Response--'+Response);  
            
            //prepare the list of responses answers for the question to update
            List<Service_Task_Responses__c> STRList = new List<Service_Task_Responses__c>();
            for(Service_Task_Responses__c str:Database.query(query)){
                str.Service_Impact_Response__c = (Response=='Null'?null:Response);
                STRList.add(str);
            }
            system.debug('--Response--'+Response+STRList); 
            upsert STRList;
            
            // Update current Scenario Budget with impacts
            PBB_BudgetEngineService.updateImpact(STRList);
        }    
        catch (Exception e) {
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR,e.getMessage()));  
        }
    }    
    
    /** 
    @Author Bhargav Devaram
    @Date 2015
    @Description Wrapper class to hold all the Questions for all Service Areas for PBB Scenario
    */
    public class QuestionWrapper {      
      
        public Service_Impact_Questions__c siq{ get; set; }    //Service impact Question
        public List<SelectOption> ResponseOptions{ get; set; } //Response options for Service Impact Question         
        public String stResp{ get; set; }                      //Response for Service Impact Question  for type Project Level or Single Response for all countries 
        Public Map<String,String> CntrytoRsp{get;set;}         //Map for country to response Service Impact Question  for different response for all countries 
             
        public QuestionWrapper(){
            siq = new Service_Impact_Questions__c();
            ResponseOptions = new List<SelectOption>();
            CntrytoRsp = new Map<String,String>();            
        }
    }      
}