/*
* Create Apex Trigger on Flow Instance History to populates parent step with actuals dates from sub-flow.
* There should be the following logic:
*	IF the a sub-flow locks the same step it was run from THEN we populate Parent Step Dates
*/
public with sharing class PAWS_FlowInstanceHistoryTrigger extends STSWR1.AbstractTrigger
{
	private Map<ID, STSWR1__Flow_Instance_History__c> parentsInstances = new Map<ID, STSWR1__Flow_Instance_History__c>();
	private Map<ID, STSWR1__Flow_Step_Junction__c> parentSteps = new Map<ID, STSWR1__Flow_Step_Junction__c>();
	private Map<ID, STSWR1__Flow_Instance_History__c> extendedInstances = new Map<ID, STSWR1__Flow_Instance_History__c>();
	
	private Map<ID, STSWR1__Flow_Instance_History__c> historyToUpdate = new Map<ID, STSWR1__Flow_Instance_History__c>();
	
	private Map<ID, STSWR1__Flow_Instance_Cursor__c> startHistories = new Map<ID, STSWR1__Flow_Instance_Cursor__c>();
	
	public override void afterInsert(List<SObject> records)
	{
		System.debug('IGNORE_PAWS_TRIGGERS: ' + PAWS_Utilities.IGNORE_PAWS_TRIGGERS);
		if(PAWS_Utilities.IGNORE_PAWS_TRIGGERS == true) return;
		
		try
		{
			new PAWS_StepProperty_LinkedStepHelper().HistoryAfterInsert((List<STSWR1__Flow_Instance_History__c>) records);
			updateParentHistory((List<STSWR1__Flow_Instance_History__c>) records);
		}catch(Exception ex)
		{
			onError(records, ex.getMessage());
		}
	}
	
	//drop me
	/*public override void beforeUpdate(List<SObject> records, List<SObject> oldRecords)
	{
		system.debug('----history trigger: BEFORE UPDATE');
		Map<ID, List<STSWR1__Flow_Instance_History__c>> stepHistory = new Map<ID, List<STSWR1__Flow_Instance_History__c>>();
		for (STSWR1__Flow_Instance_History__c h : (List<STSWR1__Flow_Instance_History__c>) records)
		{
			if (!stepHistory.containsKey(h.STSWR1__Step__c)) stepHistory.put(h.STSWR1__Step__c, new List<STSWR1__Flow_Instance_History__c>());
			stepHistory.get(h.STSWR1__Step__c).add(h);
		}
		
		for (STSWR1__Flow_Step_Junction__c step : [Select Name, STSWR1__Flow__r.Name From STSWR1__Flow_Step_Junction__c Where Id = :stepHistory.keySet()])
		{
			system.debug('----trigger step: Name = ' + step.Name + ', Flow Name = ' + step.STSWR1__Flow__r.Name);
			for (STSWR1__Flow_Instance_History__c h : stepHistory.get(step.Id))
			{
				system.debug('----trigger history: STSWR1__Actual_Start_Date__c = ' + h.STSWR1__Actual_Start_Date__c);
			}
		}
	}*/
	
	public override void afterUpdate(List<SObject> records, List<SObject> oldRecords)
	{
		//System.debug('IGNORE_PAWS_TRIGGERS: ' + PAWS_Utilities.IGNORE_PAWS_TRIGGERS);
		if(PAWS_Utilities.IGNORE_PAWS_TRIGGERS == true) return;

		try
		{
			new PAWS_StepProperty_LinkedStepHelper().HistoryAfterUpdate((List<STSWR1__Flow_Instance_History__c>) records, (List<STSWR1__Flow_Instance_History__c>) oldRecords);
			updateParentHistory((List<STSWR1__Flow_Instance_History__c>) records);
		}catch(Exception ex)
		{
			onError(records, ex.getMessage());
		}
		
		/*system.debug('----history trigger: AFTER UPDATE');
		List<STSWR1__Flow_Instance_History__c> refreshedRecords = [Select STSWR1__Step__c, STSWR1__Actual_Start_Date__c From STSWR1__Flow_Instance_History__c where id in :records];
		
		Map<ID, List<STSWR1__Flow_Instance_History__c>> stepHistory = new Map<ID, List<STSWR1__Flow_Instance_History__c>>();
		for (STSWR1__Flow_Instance_History__c h : (List<STSWR1__Flow_Instance_History__c>) refreshedRecords)
		{
			if (!stepHistory.containsKey(h.STSWR1__Step__c)) stepHistory.put(h.STSWR1__Step__c, new List<STSWR1__Flow_Instance_History__c>());
			stepHistory.get(h.STSWR1__Step__c).add(h);
		}
		
		for (STSWR1__Flow_Step_Junction__c step : [Select Name, STSWR1__Flow__r.Name From STSWR1__Flow_Step_Junction__c Where Id = :stepHistory.keySet()])
		{
			system.debug('----trigger step: Name = ' + step.Name + ', Flow Name = ' + step.STSWR1__Flow__r.Name);
			for (STSWR1__Flow_Instance_History__c h : stepHistory.get(step.Id))
			{
				system.debug('----trigger history: STSWR1__Actual_Start_Date__c = ' + h.STSWR1__Actual_Start_Date__c);
			}
		}*/
	}
	
	/**
	* Selects "extended" history records with parent instances and step infromation - steps which kick-off sub-flows.
	*/
	/*
	private void selectExtendedInstances(List<STSWR1__Flow_Instance_History__c> historyRecords)
	{
		PAWS_Utilities.checkLimit('Queries', 1);
		extendedInstances = new Map<ID, STSWR1__Flow_Instance_History__c>([select
			STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Parent_Step__c,
			STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c
			from STSWR1__Flow_Instance_History__c where Id = :historyRecords]);
	}
	/**/
	
	/**
	* Selects sibling history records
	*/
	/*
	private void selectSiblingHistories(List<STSWR1__Flow_Instance_History__c> historyRecords)
	{
		List<ID> cursorIDs = new List<ID>();
		for (STSWR1__Flow_Instance_History__c each : historyRecords)
		{
			cursorIDs.add(each.STSWR1__Cursor__c);
		}
		
		PAWS_Utilities.checkLimit('Queries', 1);
		startHistories = new Map<ID, STSWR1__Flow_Instance_Cursor__c>(
			[select Name, (select STSWR1__Actual_Start_Date__c,
				STSWR1__Actual_Complete_Date__c from STSWR1__Flow_Instance_History__r order by STSWR1__Actual_Start_Date__c asc limit 1)
			from STSWR1__Flow_Instance_Cursor__c where Id in :cursorIDs]
		);
	}
	/**/
	
	/*
	private STSWR1__Flow_Instance_History__c matchStartHistory(ID cursorID)
	{
		STSWR1__Flow_Instance_Cursor__c cursorRecord = startHistories.get(cursorID);
		return cursorRecord != null && cursorRecord.STSWR1__Flow_Instance_History__r.size() > 0
			? cursorRecord.STSWR1__Flow_Instance_History__r.get(0)
			: null;
	}
	/**/
	
	/**
	* Selects parent Flow Steps 
	*/
	/*
	private void selectParentInstances()
	{
		List<ID> parentStepIDs = new List<ID>();
		for (STSWR1__Flow_Instance_History__c each : extendedInstances.values())
		{
			//@TODO: Uncomment the code below once Roma fixes the issue when Parent Step set to "prior" to lock step.
			if (each.STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Parent_Step__c
					== each.STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c)
			{
				parentStepIDs.add(each.STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c);
			}
		}
		
		System.debug(System.LoggingLevel.ERROR, 'Parent Step IDs: ' + parentStepIDs);//@TODO: remove this line!
		
		PAWS_Utilities.checkLimit('Queries', 1);
		parentSteps = new Map<ID, STSWR1__Flow_Step_Junction__c>([select Id, (select STSWR1__Actual_Start_Date__c,
				STSWR1__Actual_Complete_Date__c from STSWR1__Flow_Instance_History__r
				order by CreatedDate desc limit 1)
			from STSWR1__Flow_Step_Junction__c where Id in :parentStepIDs]);
	}
	/**/
	
	/**
	* Matches Parent Step if any
	*/
	/*
	private STSWR1__Flow_Step_Junction__c matchParentStep(STSWR1__Flow_Instance_History__c historyRecord)
	{
		STSWR1__Flow_Instance_History__c extendedInstance = extendedInstances.get(historyRecord.Id);
		
		return parentSteps.get(extendedInstance.STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c);
	}
	/**/
	
	/**
	* Matches Parent Step history record to provided Flow Instance History record
	*/
	/*
	private STSWR1__Flow_Instance_History__c matchParentHistory(STSWR1__Flow_Instance_History__c historyRecord)
	{
		STSWR1__Flow_Instance_History__c result;
		STSWR1__Flow_Step_Junction__c parentStep = matchParentStep(historyRecord);
		System.debug(System.LoggingLevel.ERROR, 'Parent Step: ' + parentStep);//@TODO: Remove this line!
		if (parentStep != null && parentStep.STSWR1__Flow_Instance_History__r.size() > 0)
		{
			result = parentStep.STSWR1__Flow_Instance_History__r.get(0);
		}
		
		return result;
	}
	/**/
	
	private void updateParentHistory(List<STSWR1__Flow_Instance_History__c> historyRecords)
	{
		new PAWS_FlowInstanceTrigger().updateStartDates(historyRecords);
		/*
		selectExtendedInstances(historyRecords);
		selectParentInstances();
		selectSiblingHistories(historyRecords);
		for (STSWR1__Flow_Instance_History__c each : historyRecords)
		{
			updateParentHistory(each);
		}
		
		PAWS_Utilities.checkLimit('DMLStatements', 1);
		update historyToUpdate.values();
		/**/
	}
	
	/*
	private void updateParentHistory(STSWR1__Flow_Instance_History__c historyRecord)
	{
		STSWR1__Flow_Instance_History__c parentHistory = matchParentHistory(historyRecord);
		STSWR1__Flow_Instance_History__c extendedHistory = extendedInstances.get(historyRecord.Id);
		System.debug(System.LoggingLevel.ERROR, 'Parent History: ' + parentHistory);//@TODO: Remove this line!
		if (parentHistory == null)
		{
			return;
		}
		
		STSWR1__Flow_Instance_History__c startHistory = matchStartHistory(historyRecord.STSWR1__Cursor__c);
		if (startHistory != null && (parentHistory.STSWR1__Actual_Start_Date__c == null
				|| startHistory.STSWR1__Actual_Start_Date__c < parentHistory.STSWR1__Actual_Start_Date__c))
		{
			parentHistory.STSWR1__Actual_Start_Date__c = startHistory.STSWR1__Actual_Start_Date__c;
			historyToUpdate.put(parentHistory.Id, parentHistory);
		}
	}
	/**/
	
	public void onError(List<SObject> records, String message)
	{
		for (SObject each : records)
		{
			each.addError(message);
		}
	}
}