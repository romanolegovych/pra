@isTest
private class PAWS_Utilities_Batch_Tests
{
	static testMethod void testBatch()
	{
		PAWS_ApexTestsEnvironment.init();
		
		STSWR1__Flow_Step_Junction__c flowStep = PAWS_ApexTestsEnvironment.FlowStep;
		STSWR1__Flow_Step_Property__c prop = new STSWR1__Flow_Step_Property__c(Name = 'Test', STSWR1__Value__c = 'test', STSWR1__Flow_Step__c = flowStep.Id, STSWR1__Type__c = 'Text');
		insert prop;
		
		Test.StartTest();
		
		PAWS_Utilities_Batch b = new PAWS_Utilities_Batch();
		b.Query = 'Select Flow__c, Flow_Id__c From STSWR1__Flow_Step_Property__c';
		Database.executeBatch(b);
		
		b.handleException(new List<STSWR1__Flow_Step_Property__c>{prop}, 'test');
		
		Test.StopTest();
	}
}