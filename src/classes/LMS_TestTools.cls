/**
 * Test for Tools class
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestTools {
    
    static List<Job_Class_Desc__c> jobClasses;
    static List<Job_Title__c> jobTitles;
    static List<PRA_Business_Unit__c> businessUnits;
    static List<Department__c> departments;
    static List<Region__c> regions;
    static List<Country__c> countries;
    static List<Employee_Type__c> empTypes;
    static List<Employee_Status__c> empStatus;
    static List<Role_Type__c> roleTypes;
    static List<WFM_Client__c> clients;
    static List<WFM_Project__c> projects;
    static List<WFM_Contract__c> contracts;
    static List<LMS_Role__c> roles;
    static List<Course_Domain__c> courseDomains;
    static List<LMS_Course__c> courses;
    static List<Employee_Details__c> employees;
    static List<LMS_Role_Course__c> roleCourses;
    static List<LMS_Role_Employee__c> roleEmployees;
    static List<LMSConstantSettings__c> constants;
    static List<CourseDomainSettings__c> domains;
    static List<RoleAdminServiceSettings__c> settings;
    
    static void init() {
        jobClasses = new Job_Class_Desc__c[] {
            new Job_Class_Desc__c(Name = 'class1', Job_Class_ExtID__c = 'class1', Job_Class_Code__c = 'code1'),
            new Job_Class_Desc__c(Name = 'class2', Job_Class_ExtID__c = 'class2', Job_Class_Code__c = 'code2')
        };
        insert jobClasses;
        
        jobTitles = new Job_Title__c[] {
            new Job_Title__c(Job_Code__c = 'Code1', Job_Title__c = 'title1', Job_Class_Desc__c = jobClasses[0].Id, Status__c = 'A'),
            new Job_Title__c(Job_Code__c = 'Code2', Job_Title__c = 'title2', Job_Class_Desc__c = jobClasses[1].Id, Status__c = 'A'),
            new Job_Title__c(Job_Code__c = 'Code3', Job_Title__c = 'title3', Job_Class_Desc__c = jobClasses[1].Id, Status__c = 'A')
        };
        insert jobTitles;
        
        businessUnits = new PRA_Business_Unit__c[] {
            new PRA_Business_Unit__c(Name = 'unit', Business_Unit_Code__c = 'BUC', Status__c = 'A')
        };
        insert businessUnits;
        
        departments = new Department__c[] {
            new Department__c(Name = 'department', Department_Code__c = 'DEPTC', Status__c = 'A')
        };
        insert departments;
        
        regions = new Region__c[] {
            new Region__c(Region_Name__c = 'region1', Region_Id__c = 1, Status__c = 'A'),
            new Region__c(Region_Name__c = 'region2', Region_Id__c = 2, Status__c = 'A')
        };
        insert regions;
        
        countries = new Country__c[] {
            new Country__c(Name = 'country1', Region_Name__c = 'region1', Country_Code__c = 'C1', PRA_Country_ID__c = '100'),
            new Country__c(Name = 'country2', Region_Name__c = 'region2', Country_Code__c = 'C2', PRA_Country_ID__c = '200')
        };
        insert countries;
        
        empTypes = new Employee_Type__c[] {
            new Employee_Type__c(Name = 'Employee'),
            new Employee_Type__c(Name = 'Contractor'),
            new Employee_Type__c(Name = 'Terminated')
        };
        insert empTypes;
        
        empStatus = new Employee_Status__c[] {
            new Employee_Status__c(Employee_Status__c = 'AA', Employee_Type__c = empTypes[0].Name),
            new Employee_Status__c(Employee_Status__c = 'XX', Employee_Type__c = empTypes[2].Name)
        };
        insert empStatus;
        
        roleTypes = new Role_Type__c[] {
            new Role_Type__c(Name = 'PRA'),
            new Role_Type__c(Name = 'Project Specific'),
            new Role_Type__c(Name = 'Additional Role')
        };
        insert roleTypes;
        
        clients = new WFM_Client__c[] {
            new WFM_Client__c(Name = 'Client1', Client_Unique_Key__c = 'Client1'),
            new WFM_Client__c(Name = 'Client2', Client_Unique_Key__c = 'Client2')
        };
        insert clients;
        
        contracts = new WFM_Contract__c[] {
            new WFM_Contract__c(Name = 'contract1', Contract_Unique_Key__c = 'contract1', Client_Id__c = clients[0].Id),
            new WFM_Contract__c(Name = 'contract2', Contract_Unique_Key__c = 'contract2', Client_Id__c = clients[1].Id)
        };
        insert contracts;
        
        projects = new WFM_Project__c[] {
            new WFM_Project__c(Name = 'Project1', Project_Unique_Key__c = 'Project1', Contract_Id__c = contracts[0].Id),
            new WFM_Project__c(Name = 'Project2', Project_Unique_Key__c = 'Project2', Contract_Id__c = contracts[1].Id)
        };
        insert projects;
        
        courseDomains = new Course_Domain__c[] {
            new Course_Domain__c(Domain__c = 'Internal'),
            new Course_Domain__c(Domain__c = 'Archive'),
            new Course_Domain__c(Domain__c = 'Project Specific')
        };
        insert courseDomains;
        
        
        constants = new LMSConstantSettings__c[] {
            new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
            new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
            new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
            new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D'),
            new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
            new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
            new LMSConstantSettings__c(Name = 'typePRA', Value__c = 'PRA'),
            new LMSConstantSettings__c(Name = 'typeProject', Value__c = 'Project Specific'),
            new LMSConstantSettings__c(Name = 'typeAdhoc', Value__c = 'Additional Role'),
            new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
            new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
            new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
            new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
            new LMSConstantSettings__c(Name = 'statusToDelete', Value__c = 'Delete'),
            new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
            new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
            new LMSConstantSettings__c(Name = 'statusPending', Value__c = 'Pending'),
            new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
            new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete'),
            new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
            new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended')
        };
        insert constants;
        
        domains = new CourseDomainSettings__c[] {
            new CourseDomainSettings__c(Name = 'Internal', Domain_Id__c = 'Internal'),
            new CourseDomainSettings__c(Name = 'PST', Domain_Id__c = 'Project Specific'),
            new CourseDomainSettings__c(Name = 'Archive', Domain_Id__c = 'Archive')
        };
        insert domains;
        
        settings = new RoleAdminServiceSettings__c[] {
            new RoleAdminServiceSettings__c(Name = 'DOMAIN', Value__c = 'DOMAIN'),
            new RoleAdminServiceSettings__c(Name = 'ENDPOINT', Value__c = 'ENDPOINT'),
            new RoleAdminServiceSettings__c(Name = 'TIMEOUT', Value__c = '50000')
        };
        insert settings;
                
        courses = new LMS_Course__c[] {
            new LMS_Course__c(Offering_Template_No__c = 'course123', Course_Code__c = 'C123', Title__c = 'Course 123', Domain_Id__c = 'Internal',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse1',
                              Discontinued_From__c = Date.today()-1),
            new LMS_Course__c(Offering_Template_No__c = 'course321', Course_Code__c = 'C124', Title__c = 'Course 132', Domain_Id__c = 'Internal',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse2',
                              Discontinued_From__c = Date.today()+1),
            new LMS_Course__c(Offering_Template_No__c = 'course124', Course_Code__c = 'C234', Title__c = 'Course 234', Domain_Id__c = 'Internal',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse3',
                              Discontinued_From__c = Date.today()+1),
            new LMS_Course__c(Offering_Template_No__c = 'course341', Course_Code__c = 'C231', Title__c = 'Course 243', Domain_Id__c = 'Archive',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse4',
                              Discontinued_From__c = Date.today()+1),
            new LMS_Course__c(Offering_Template_No__c = 'course555', Course_Code__c = 'C555', Title__c = 'Course 555', Domain_Id__c = 'Internal',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse5',
                              Discontinued_From__c = Date.today()+1)
        };
        insert courses;
        
        employees = new Employee_Details__c[] {
            new Employee_Details__c(Name = 'employee1', Employee_Unique_Key__c = 'employee1', First_Name__c = 'Andrew', Last_Name__c = 'Allen', Email_Address__c = 'a@a.com', 
                                    Job_Class_Desc__c = 'class1', Job_Code__c = 'title1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU')
        };
        insert employees;       
    }
    
    static void initRoles() {
        roles = new LMS_Role__c[] {
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = jobTitles[0].Id, Business_Unit__c = businessUnits[0].Id,
                            Department__c = departments[0].Id, Region__c = regions[0].Id, Country__c = countries[0].Id,
                            Employee_Type__c = empTypes[0].Id, Status__c = 'Active', Sync_Status__c = 'N'),
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[1].Id, Job_Title__c = jobTitles[2].Id, Business_Unit__c = businessUnits[0].Id,
                            Department__c = departments[0].Id, Region__c = regions[1].Id, Country__c = countries[1].Id,
                            Employee_Type__c = empTypes[1].Id, Status__c = 'Active', Sync_Status__c = 'N'),
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = jobTitles[0].Id, Business_Unit__c = businessUnits[0].Id,
                            Department__c = departments[0].Id, Region__c = regions[0].Id, Country__c = countries[0].Id,
                            Employee_Type__c = empTypes[1].Id, Status__c = 'Active', Sync_Status__c = 'N'),
            new LMS_Role__c(Job_Class_Desc__c = null, Job_Title__c = null, Business_Unit__c = null,
                            Department__c = null, Region__c = null, Country__c = null,
                            Employee_Type__c = empTypes[0].Id, Status__c = 'Active', Sync_Status__c = 'N'),
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = null, Business_Unit__c = businessUnits[0].Id,
                            Department__c = null, Region__c = regions[0].Id, Country__c = null,
                            Employee_Type__c = empTypes[0].Id, Status__c = 'Active', Sync_Status__c = 'N'),
            new LMS_Role__c(Job_Class_Desc__c = null, Job_Title__c = null, Business_Unit__c = null,
                            Department__c = departments[0].Id, Region__c = null, Country__c = null,
                            Employee_Type__c = null, Status__c = 'Active', Sync_Status__c = 'Y', SABA_Role_PK__c = 'roles000000000001')
        };
        insert roles;
    }
    
    static void initMappings() {
        roleCourses = new LMS_Role_Course__c[] {
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft Delete', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Committed', Assigned_By__c = 'ASA')
        };
        insert roleCourses;
        
        roleEmployees = new LMS_Role_Employee__c[] {
            new LMS_Role_Employee__c(Role_Id__c = roles[5].Id, Employee_Id__c = employees[0].Id, Sync__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA',
                Created_On__c = Date.today(), Updated_On__c = Date.today())
        };
        insert roleEmployees;
    }
    
    static void initMappingsSingleRole() {
        roleCourses = new LMS_Role_Course__c[] {
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[2].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[3].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA')
        };
        insert roleCourses;
    }
    
    static void initDeleteRCMore() {
        roleCourses = new List<LMS_Role_Course__c>();
        for(Integer i = 0; i < 6; i++) {
            for(Integer j = 0; j < 4; j++) {
                roleCourses.add(new LMS_Role_Course__c(Role_Id__c = roles[i].Id, Course_Id__c = courses[j].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'));
            }
        }
        insert roleCourses;
    }
    
    static void initDeleteRCLess() {
        roleCourses = new List<LMS_Role_Course__c>();
        for(Integer i = 0; i < 3; i++) {
            for(Integer j = 0; j < 4; j++) {
                roleCourses.add(new LMS_Role_Course__c(Role_Id__c = roles[i].Id, Course_Id__c = courses[j].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'));
            }
        }
        insert roleCourses;
    }
    
    static void initDeleteRCLess2() {
        roleCourses = new List<LMS_Role_Course__c>();
        for(Integer i = 0; i < 6; i++) {
            for(Integer j = 0; j < 5; j++) {
                roleCourses.add(new LMS_Role_Course__c(Role_Id__c = roles[i].Id, Course_Id__c = courses[j].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'));
            }
        }
        insert roleCourses;
    }
    
    static testMethod void testCreateRole() {
        init();
        List<LMS_Role__c> role = null;
        List<String> values = new String[]{jobClasses[0].Name,null,null,null,null,null,null};
        Integer i = 1;
        LMS_Tools.createRole(values, i);
        role = [SELECT Name FROM LMS_Role__c WHERE Role_Type__r.Name = 'PRA'];
        System.assert(role.size() == 1);
        
        values = new String[]{clients[0].Name,'',jobClasses[0].Name};
        i = 2;
        LMS_Tools.createRole(values, i);
        role = [SELECT Name FROM LMS_Role__c WHERE Role_Type__r.Name = 'Project Specific'];
        System.assert(role.size() == 1);
        
        values = new String[]{'Adhoc Role'};
        i = 3;
        LMS_Tools.createRole(values, i);
        role = [SELECT Name FROM LMS_Role__c WHERE Role_Type__r.Name = 'Additional Role'];
        System.assert(role.size() == 1);
    }
    
    static testMethod void testSendRoleData() {
        init();
        initRoles();
        List<LMS_Role__c> role = null;
        LMS_Tools.sendRoleData(roles[0]);
        role = [SELECT Name FROM LMS_Role__c WHERE Role_Type__r.Name = 'PRA'];
        System.assert(role.size() == 6);
    }
    
    static testMethod void testEmployeeMapping() {
        init();
        List<LMS_Role__c> role = null;
        List<String> values = new String[]{jobClasses[0].Name,null,null,null,null,null,null};
        Integer i = 1;
        LMS_Tools.createRole(values, i);
        role = [SELECT Id FROM LMS_Role__c WHERE Role_Type__r.Name = 'PRA'];
        LMS_Tools.createEmployeeMapping(values, role[0].Id);
        List<LMS_Role_Employee__c> re = [SELECT Name FROM LMS_Role_Employee__c];
        System.assert(re.size() == 1);
        
        values = new String[]{jobClasses[0].Name,jobTitles[0].Job_Title__c,null,null,null,null,empTypes[0].Name};
        i = 1;
        LMS_Tools.createRole(values, i);
        role = [SELECT Id FROM LMS_Role__c WHERE Role_Type__r.Name = 'PRA' AND Job_Title__c != null];
        LMS_Tools.createEmployeeMapping(values, role[0].Id);
        re = [SELECT Name FROM LMS_Role_Employee__c WHERE Role_Id__c = :role[0].Id];
        System.assert(re.size() == 0);
        
    }
    
    static testMethod void testSendInitialRoleEmployees() {
        init();
        initRoles();
        initMappings();
        
        LMS_Tools.sendInitialRoleEmployeeMappings(roles[5].Id);
    }
    
    static testMethod void testRcBatchDeleteLess() {
        init();
        initRoles();
        initDeleteRCLess();
        
        LMS_Tools.rcBatchDelete(roleCourses);
    }
    
    static testMethod void testRcBatchDeleteLess2() {
        init();
        initRoles();
        initDeleteRCLess2();
        
        LMS_Tools.rcBatchDelete(roleCourses);
    }
    
    static testMethod void testRcBatchDeleteMore() {
        init();
        initRoles();
        initDeleteRCMore();
        
        LMS_Tools.rcBatchDelete(roleCourses);
    }
    
    static testMethod void testExecuteCourseToRoleWS() {
        init();
        initRoles();
        initMappings();
        
        Map<String, LMS_Role_Course__c> objs = new Map<String, LMS_Role_Course__c>();
        Map<String, List<LMS_Role_Course__c>> maps = new Map<String, List<LMS_Role_Course__c>>();
        maps.put('A', roleCourses);
        for(LMS_Role_Course__c rc : roleCourses) {
            objs.put(rc.Course_Id__r.SABA_ID_PK__c, rc);
        }
        
        LMS_Tools.executeCoursesToRoleWS(maps, objs);
    }
    
    
}