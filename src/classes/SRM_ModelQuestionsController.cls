/**
@author 
@date 2014
@description this controller class is for srm model questions
**/
public with sharing class SRM_ModelQuestionsController {
    
    //Member Variables
    public SelectOption[] selectedCountries     { get; set; }
    public SelectOption[] allCountries          { get; set; }
    public SRM_Model_Questions__c srmQuestions  { get;set;} 
    public String returnUrl                     { get; set;}
    public String parentId                      {get;set;}
    
    public SRM_ModelQuestionsController(ApexPages.StandardController controller){
        
        selectedCountries = new List<SelectOption>();
        List<Country__c> countryList= SRM_ModelCloneDataAccessor.countries();    
        allCountries = new List<SelectOption>();
        for ( Country__c countryObj : countryList) {
            allCountries.add(new SelectOption(countryObj.Name, countryObj.Name));
        }
        String id = ApexPages.CurrentPage().getParameters().get('id');
        returnUrl = ApexPages.CurrentPage().getParameters().get('returl');
        String clone = ApexPages.CurrentPage().getParameters().get('clone');
        if(ApexPages.currentPage().getParameters().get('parentId')!=null){
            parentId = ApexPages.currentPage().getParameters().get('parentId');
        }
        else{
            parentId = SRM_ModelSiteActivationSevice.getParentId(URL.getCurrentRequestUrl().toExternalForm());
        }   
        if(String.isEmpty(id)){
            srmQuestions = new SRM_Model_Questions__c();
            if(!String.isEmpty(parentId)){
                srmQuestions.SRM_Model__c = parentId;   
            } 
            else if(ApexPages.currentPage().getParameters().get('parentId')!=null){
                srmQuestions.SRM_Model__c = ApexPages.currentPage().getParameters().get('parentId'); 
            }              
        }
        else if(!String.isEmpty(clone)){                         
            SRM_Model_Questions__c temp = SRM_ModelCloneDataAccessor.modelQuestions(id);
            SRM_Model_Questions__c clonerecord = temp.clone(false,true);
            srmQuestions = clonerecord;
            if(srmQuestions.Group__c != 'Project' && srmQuestions.Group__c != 'Contract' && srmQuestions.Group__c != 'Sponsor' ){         
                if(srmQuestions.Country__c!=null){                     
                    String[] splitArr = srmQuestions.Country__c.split(';');                    
                    Set<SelectOption> allCountriesSet = new Set<SelectOption>();
                    allCountriesSet.addAll(allCountries);
                    for(String countryList1:splitArr){
                        selectedCountries.add(new SelectOption(countryList1,countryList1));
                        allCountriesSet.remove(new SelectOption(countryList1,countryList1));
                    }                      
                    allCountries.clear();
                    allCountries.addAll(allCountriesSet);
                }            
                System.debug('<<<'+selectedCountries);   
            }
            else{
                srmQuestions.Selecting_Country__c = '';
                srmQuestions.country__c='';                 
            }
        }
        
        else{
            SRM_Model_Questions__c tempObj = SRM_ModelCloneDataAccessor.modelQuestions(id);
            if(tempobj!=null){                  
                srmQuestions = tempObj;
                if(srmQuestions.Group__c != 'Project' && srmQuestions.Group__c != 'Contract' && srmQuestions.Group__c != 'Sponsor' ){                                            
                    if(srmQuestions.Country__c!=null){                      
                        String[] splitArr = srmQuestions.Country__c.split(';');                     
                        Set<SelectOption> allCountriesSet = new Set<SelectOption>();
                        allCountriesSet.addAll(allCountries);
                        for(String countryList1:splitArr){
                            selectedCountries.add(new SelectOption(countryList1,countryList1));
                            allCountriesSet.remove(new SelectOption(countryList1,countryList1));
                        }                       
                        allCountries.clear();
                        allCountries.addAll(allCountriesSet);
                        allCountries.sort();
                    }                   
                    System.debug('<<<'+selectedCountries);   
                }
                else{
                    srmQuestions.Selecting_Country__c = '';
                    srmQuestions.country__c='';  
                }
            }    
        }
    }
    
    /*@author Fahad Arif
	@date 2015
	@description this method to generate the picklist from WFM_Therapeutics
	**/
    public List<selectOption> gettherptic(){    
       List<selectOption> therapticlist = new List<selectOption>();    
       Set<string> uniqueNames = new set<string>(); 
       //to add blank value as default
       therapticlist.add(new SelectOption('','  '));
       for(WFM_Therapeutic__c ta: [select id,name,Therapeutic_Area__c,Therapeutic_Area__r.Name  from WFM_Therapeutic__c 
                                          where Status__c = 'Approved' and Therapeutic_Area__c != null order by Therapeutic_Area__r.Name asc]) {
           if(!uniqueNames.contains(ta.Therapeutic_Area__r.name))                                   
           		therapticlist.add(new selectOption(ta.Therapeutic_Area__c,ta.Therapeutic_Area__r.name));
           uniqueNames.add(ta.Therapeutic_Area__r.name);                                   
           
       } 
       return therapticlist;
    }
    
//Method to Save
    /**
@author 
@date 2014
@description this method to save srm questions
**/
    public PageReference saveModelQuestions(){
        String  countries='';
        if(selectedCountries!=null && selectedCountries.size()>0){
            for(SelectOption so : selectedCountries){
                countries+=so.getValue()+';';
            }
            if(countries!=''){
                countries=countries.substring(0,countries.length()-1);
            }
        } 
        
        if(srmQuestions.Group__c == 'Country'){
            if(srmQuestions.Selecting_Country__c == null || srmQuestions.Selecting_Country__c == '' || srmQuestions.Selecting_Country__c == '--None--'){
                system.debug('@@srmQuestions.Selecting_Country__c'+srmQuestions.Selecting_Country__c);
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Options for Selecting Country:    You must enter a value.');
                ApexPages.addMessage(myMsg);
                return null; 
            }
        }      
        if(srmQuestions != null){
            if(srmQuestions.Group__c != 'Project' && srmQuestions.Group__c != 'Contract' && srmQuestions.Group__c != 'Sponsor' ){
                
                if(countries!='' && srmQuestions.Selecting_Country__c == 'Specific Countries'){
                    srmQuestions.country__c=countries;
                }
                else if (srmQuestions.Selecting_Country__c == 'All Countries for Scenario'){
                    srmQuestions.country__c='';
                }
                else if(srmQuestions.Selecting_Country__c == 'Specific Countries'){                     
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select the countries');
                    ApexPages.addMessage(myMsg);
                    return null;  
                }
            }
            else{
                srmQuestions.Selecting_Country__c = '';
                srmQuestions.country__c='';
            }           
        }   
        try{    
            upsert srmQuestions;
            PageReference p = new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+srmQuestions.Id);           
            p.setRedirect(true);               
            return p;
        }
        catch(DMLException ex){
            if(ex.getDmlType(0) == StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION){
                SRM_Utils.showErrorMessage('You do not have permission to create or edit a SRM Model Questions record once the model is Approved or Retired.');
            }
        }        
        return null;   
    }
    
    
//Method to Save&New
        /**
@author 
@date 2014
@description this method to save and generate new srm questions
**/
    public PageReference saveandnewQuestions(){
        String  countries='';
        if(selectedCountries!=null && selectedCountries.size()>0){
            for(SelectOption so : selectedCountries){
                countries+=so.getValue()+';';
            }
            if(countries!=''){
                countries=countries.substring(0,countries.length()-1);
            }
        } 
        if(srmQuestions.Group__c == 'Country'){
        	if(srmQuestions.Selecting_Country__c == null || srmQuestions.Selecting_Country__c == '' || srmQuestions.Selecting_Country__c == '--None--'){
            	system.debug('@@srmQuestions.Selecting_Country__c'+srmQuestions.Selecting_Country__c);
            	ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Options for Selecting Country:    You must enter a value.');
            	ApexPages.addMessage(myMsg);
           		return null; 
            
        	}
        }    
        if(srmQuestions != null){
            if(srmQuestions.Group__c != 'Project' && srmQuestions.Group__c != 'Contract' && srmQuestions.Group__c != 'Sponsor' ){
                if(countries!='' && srmQuestions.Selecting_Country__c == 'Specific Countries'){
                    srmQuestions.country__c=countries;
                }
                else if (srmQuestions.Selecting_Country__c == 'All Countries for Scenario'){
                    srmQuestions.country__c='';
                }
                else if(srmQuestions.Selecting_Country__c == 'Specific Countries'){               
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select the countries');
                    ApexPages.addMessage(myMsg);
                    return null;  
                }
            }
            else{
                srmQuestions.Selecting_Country__c = '';
                srmQuestions.country__c='';             
            }
            try{    
                upsert srmQuestions;
                return new pagereference('/apex/SRM_Model_Questions?parentid='+srmQuestions.SRM_Model__c).setRedirect(true);                
            }
            catch(DMLException ex){
                if(ex.getDmlType(0) == StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION){
                    SRM_Utils.showErrorMessage('You do not have permission to create or edit a SRM Model Questions record once the model is Approved or Retired.');
                }
            }
        }
        return null; 
    }  
    
    
//Method to Cancel 
        /**
@author 
@date 2014
@description this method to cancel srm questions
**/     
    public PageReference cancel(){
        PageReference p = new PageReference('/'+srmQuestions.SRM_Model__c);
        p.setRedirect(true);
        return p;
    }          
}