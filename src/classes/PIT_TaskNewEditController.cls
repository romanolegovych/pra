public with sharing class PIT_TaskNewEditController {
    Public Task                   ta                {get;set;}
    public Issue__c               issue             {get;set;}
    public boolean                newTaskFlag       {get;set;}
    public String                 parentIssueStr;
    public boolean                showUploadPage    {get;set;}
    public List<Attachment>       attachResults     {get;set;} 
    public boolean                attachExist       {get;set;} 
    public string                 AttachId          {get;set;} 
    public Attachment             newAttach         {get;set;}
    public String                 delRowIndex       {get;set;}
    ApexPages.standardController  globalStdCtr;
    
    
    
    public PIT_TaskNewEditController(ApexPages.standardController stdCtr) {
        globalStdCtr    = stdCtr;
        showUploadPage  = false;
        newAttach       = new Attachment(); 
        attachResults   = new List<Attachment>(); 
        attachExist     = false; 
        String tmpId    = system.currentPageReference().getParameters().get('id');
        String tmpStr   = system.currentPageReference().getParameters().get('retURL');
        String closeStr = system.currentPageReference().getParameters().get('close');
        newTaskFlag = (String.isBlank(tmpId));
        if(newTaskFlag) { // new task
            parentIssueStr = (String.isBlank(tmpStr))? NULL : tmpStr.substring(1); // to avoid the '/' character
            if(parentIssueStr.length() > 18) {
                parentIssueStr = parentIssueStr.substring(0,18);
            }
            ta = new Task();
            try {
                ta.WhatId = parentIssueStr;
            } catch (exception e) {
                ta = new Task();
                //ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Test.' + parentIssueStr);
                //ApexPages.addMessage(msg);  
            }
        } else { // edit issue
            try {
                ta = [SELECT id, 
                             ownerid, 
                             Subject, 
                             ActivityDate, 
                             WhatId, 
                             Description, 
                             Status, 
                             Priority, 
                             ReminderDateTime,
                             IsReminderSet 
                     FROM    Task 
                     WHERE   id =: tmpId];
                if((!String.isBlank(closeStr)) && closeStr=='1') {
                    ta.Status = 'Completed';
                }
                attachExist = findAttachments(); 
            } catch (exception e){
                ta = new Task();
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected task. Please, try again.');
                ApexPages.addMessage(msg);  
            }
        } // end of else
    }   
    
    
    // used to find the attachments 
    public boolean findAttachments() {
        try{
            attachResults = [SELECT Id, 
                                    ParentId, 
                                    Name, 
                                    BodyLength, 
                                    LastModifiedDate
                            FROM    Attachment 
                            WHERE   ParentId =: ta.id 
                            ORDER BY LastModifiedDate];
            return (attachResults.size() > 0);
        } catch(Exception e){
            return false;
        }
    }
    
    
    // used to open a new tab to show the attachment
    public PageReference viewAttach(){
        return new PageReference('/servlet/servlet.FileDownload?file='+ AttachId);
    }   
    
    
    // used to remove an attachment from the attachments list
    public void removeAttach() {
        Attachment tmpAttach = new Attachment();
        try{
            tmpAttach = [SELECT id FROM Attachment WHERE id =: AttachId];
            delete tmpAttach;
            attachResults.remove(Integer.valueOf(delRowIndex));
            attachExist = (attachResults.size() > 0);
        } catch (Exception e) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the delete the selected attachment. Please, try again.');
            ApexPages.addMessage(msg);  
        } 
    }
    
    
    // used to insert an attachment to the task(edit mode) or temporary to whatId(save mode)
    public void insertAttachment() {
        try {
            if(newAttach.Body != NULL) { // if there is an attachment
                newAttach.parentId = (newTaskFlag)? ta.WhatId : ta.id;
                insert newAttach;
            }
            newAttach.Body = null; // to avoid limitations
            attachResults.add(newAttach);
            newAttach = new Attachment();
            attachExist = true;
        } catch(Exception e) {
            newAttach.Body = null; // to avoid limitations
            newAttach = new Attachment();
        }
        hideAttachSelPanel();
    }
    
    
    // used to show the attachment selection panel
    public void showAttachSelPanel() {
        showUploadPage = true;
    }
    
    
    // used to hide the attachment selection panel
    public void hideAttachSelPanel() {
        showUploadPage = false;
    }
    
    
    // used to overwrite the cancel functionality with some additional functionality
    public PageReference Cancel() {
        // In case we are in new mode. delete the temporary attachments
        Set<Id> attachIds = new Set<Id>();
        List<Attachment> tmpAttachList    = new List<Attachment>(); 
        try {
            if(newTaskFlag) { // just in case
                // Find the ids of all attachments
                for(Attachment tmpObj : attachResults) {
                    attachIds.add(tmpObj.id);
                }
                // Retrieve and delete the attachments
                if(! attachIds.isEmpty()) {
                    tmpAttachList = [SELECT Id, Name, Body FROM Attachment WHERE ID IN :attachIds];
                    delete tmpAttachList;
                }
            } 
        } catch(Exception e) {}
        // Cancel functionality
        try {
            return globalStdCtr.Cancel();
        } catch (Exception e) {
            return ApexPages.currentPage();
        } 
    } 
    
    
    // used to save the task
    public PageReference Save() {
        try {
            upsert ta;
            if(newTaskFlag) { // if we are in New Task mode, then associate the attachments with the new saved task
                associateAttachments();
            }
            PageReference returnPage = new PageReference(ApexPages.currentPage().getParameters().get('retURL'));  
            return returnPage;//new PageReference('/'+ta.id);
        } catch (exception e) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to save the task. Please, try again.');
            ApexPages.addMessage(msg); 
            return ApexPages.currentPage();  
        } 
    }
    
    
    // used to associated the attachments with the new saved task. It us used only under the 'New Task' mode
    public void associateAttachments() {
        Set<Id> attachIds = new Set<Id>();
        List<Attachment> tmpAttachList    = new List<Attachment>(); 
        List<Attachment> tmpNewAttachList = new List<Attachment>(); 
        if(newTaskFlag) { // just in case
            // Find the ids of all attachments
            for(Attachment tmpObj : attachResults) {
                attachIds.add(tmpObj.id);
            }
            // Retrieve the attachments
            tmpAttachList = [SELECT Id, Name, Body FROM Attachment WHERE ID IN :attachIds];
            // delete the tmp attachments and save them again under the new saved task
            for(Attachment tmpObj : tmpAttachList) {
                Attachment tmpAttach = new Attachment();
                tmpAttach.Name = tmpObj.Name;
                tmpAttach.Body = tmpObj.Body;
                tmpAttach.parentId = ta.id; // cause after saving the new task, the task has an id
                tmpNewAttachList.add(tmpAttach);
            }
            delete tmpAttachList;
            // Update the attachments
            insert tmpNewAttachList;
        } 
    }
    
    // used to save the new task and reset the form for a new task to be inserted
    public PageReference SaveNewTask() { 
        try {
            insert ta;
            if(newTaskFlag) { // if we are in New Task mode, then associate the attachments with the new saved task 
                associateAttachments();
            }
            // reset the ta
            ta = new Task();
            ta.WhatId   = parentIssueStr;
            ta.Status   = 'Not Started';
            ta.Priority = 'Normal';
            // reset the global variables
            newAttach = new Attachment(); 
            attachResults = new List<Attachment>(); 
            attachExist   = false; 
            return ApexPages.currentPage();
        } catch (exception e) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to save the task. Please, try again.');
            ApexPages.addMessage(msg); 
            return null;  
        }            
    } 
    
}