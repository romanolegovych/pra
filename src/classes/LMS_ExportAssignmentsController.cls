public class LMS_ExportAssignmentsController {
    
    //Variables
    public String title{get;set;}
    public Boolean typeCourse{get;set;}
    public Boolean typeRole{get;set;}
    public Boolean typeEmployee{get;set;}
    public Boolean isPRA{get;set;}
    public Boolean isPST{get;set;}
    public Boolean isAdhoc{get;set;}
    //Collections
    public List<LMS_Role_Course__c> courses{get;set;}
    public List<LMS_Role_Course__c> roles{get;set;}
    public List<LMS_PersonAssignment> employeeWrapper{get;set;}
    
    public LMS_ExportAssignmentsController(){
        getAssignments();
    }
    
    public void getAssignments() {
        String query;
        String rCid = ApexPages.currentPage().getParameters().get('rCid');
        String cRid = ApexPages.currentPage().getParameters().get('cRid');
        String rEid = ApexPages.currentPage().getParameters().get('rEid');
        String eCid = ApexPages.currentPage().getParameters().get('eCid');
		String operation = ApexPages.currentPage().getParameters().get('type');
		String roleType = ApexPages.currentPage().getParameters().get('roleType');
        
        if(rCid != null) {
        	LMS_Role__c role = [Select Role_Name__c from LMS_Role__c where Id = :rCid];
    		query = 'select Course_Id__r.Course_Code__c,Course_Id__r.Title__c,Course_Id__r.Duration_Display__c,Course_Id__r.Available_From__c,' +
		        'Course_Id__r.Discontinued_From__c,Course_Id__r.Target_Days__c,Status__c,Sync_Status__c,Assigned_On__c,Assigned_By__c,Commit_Date__c ' +
		        'from LMS_Role_Course__c where Role_Id__c = :rCid order by Course_Id__r.Course_Code__c';
            courses = Database.query(query);
                              
            String roleName = role.Role_Name__c;
            title = '"Course Listing for Role ' + roleName + '"';
            typeCourse = true;
            typeRole = !typeCourse;
            typeEmployee = !typeCourse;
        }
        
        if(cRid != null) {
        	LMS_Course__c course = [select Title__c from LMS_Course__c where Id = :cRid];
            String courseName = course.Title__c;
        	if(operation == '1') {
        		isPRA = true;
        		isAdhoc = !isPRA;
        		isPST = !isPRA;
        		title = '"PRA Role Listing for Course ' + courseName + '"';
        		query = 'select Role_Id__r.Role_Name__c,Role_Id__r.Job_Class_Desc__r.Name,Role_Id__r.Job_Title__r.Job_Title__c,' + 
        			'Role_Id__r.Business_Unit__r.Name,Role_Id__r.Department__r.Name,Role_Id__r.Region__r.Region_Name__c,' + 
        			'Role_Id__r.Country__r.Name,Role_Id__r.Employee_Type__r.Name, Role_Id__r.Status__c,Course_Id__r.Title__c,' + 
        			'Assigned_On__c,Assigned_By__c,Status__c,Sync_Status__c,Commit_Date__c ' +
	                'from LMS_Role_Course__c where Course_Id__c = :cRid and Role_Id__r.Role_Type__r.Name = \'PRA\' order by Role_Id__r.Role_Name__c';
        	} else if(operation == '2') {
        		isPST = true;
        		isAdhoc = !isPST;
        		isPRA = !isPST;
        		title = '"Project Specific Role Listing for Course ' + courseName + '"';
        		query = 'select Role_Id__r.Role_Name__c,Role_Id__r.Client_Id__r.Name,Role_Id__r.Project_Id__r.Name,Role_Id__r.Job_Class_Desc__r.Name,' + 
        			'Role_Id__r.PST_Adhoc_Role__r.PST_Adhoc_Role__c,Role_Id__r.Status__c,Course_Id__r.Title__c,Assigned_On__c,' + 
        			'Assigned_By__c,Status__c,Sync_Status__c,Commit_Date__c ' +
	                'from LMS_Role_Course__c where Course_Id__c = :cRid and Role_Id__r.Role_Type__r.Name = \'Project Specific\' ' + 
	                'order by Role_Id__r.Role_Name__c';
        	} else if(operation == '3') {
        		isAdhoc = true;
        		isPRA = !isAdhoc;
        		isPST = !isAdhoc;
        		title = '"Additional Role Listing for Course ' + courseName + '"';
        		query = 'select Role_Id__r.Role_Name__c,Role_Id__r.Status__c,Course_Id__r.Title__c,' + 
        			'Assigned_On__c,Assigned_By__c,Status__c,Sync_Status__c,Commit_Date__c ' +
	                'from LMS_Role_Course__c where Course_Id__c = :cRid and Role_Id__r.Role_Type__r.Name = \'Additional Role\' order by Role_Id__r.Role_Name__c';
        	}
            roles = Database.query(query);
                            
            typeRole = true;
            typeCourse = !typeRole;
            typeEmployee = !typeRole;
        }
        
        if(rEid != null) {
        	Map<String, String> jobTitleMap = getJobTitleMap();
        	LMS_Role__c role = [select Role_Name__c from LMS_Role__c where Id = :rEid];
            List<LMS_Role_Employee__c> employees = 
            	[select Employee_Id__r.Name, Employee_Id__r.First_Name__c,Employee_Id__r.Last_Name__c,
            	Employee_Id__r.Job_Class_Desc__c,Employee_Id__r.Job_Code__c,Employee_Id__r.Business_Unit_Desc__c,
            	Employee_Id__r.Department__c,Employee_Id__r.Country_Name__r.Region_Name__c,Employee_Id__r.Country_Name__r.Name,
                Employee_Id__r.Status_Desc__c,Role_Id__r.Role_Name__c,Assigned_On__c,Status__c,Sync__c,Commit_Date__c
                from LMS_Role_Employee__c where Role_Id__c=:rEid order by Employee_Id__r.Last_Name__c];
            
            employeeWrapper = new List<LMS_PersonAssignment>();
            for(LMS_Role_Employee__c re : employees) {
            	employeeWrapper.add(new LMS_PersonAssignment(re, jobTitleMap));
            } 
                                
            String roleName = role.Role_Name__c;
            title = '"Employee Listing for Role ' + roleName + '"';
            typeEmployee = true;
            typeCourse = !typeEmployee;
            typeRole = !typeEmployee;
        }
        
        if(eCid != null) {
        	String filter = '';
        	List<String> roleIds = new List<String>();
        	Map<String, String> jobTitleMap = getJobTitleMap();
        	LMS_Course__c course = [select Title__c from LMS_Course__c where Id = :eCid];
        	
        	if(roleType == 'PRA') {
        		filter = 'and Role_Id__r.Role_Type__r.Name = \'PRA\'';
        	} else if(roleType == 'PST') {
        		filter = 'and Role_Id__r.Role_Type__r.Name = \'Project Specific\'';
        	} else if(roleType == 'Adhoc') {
        		filter = 'and Role_Id__r.Role_Type__r.Name = \'Additional Role\'';
        	}
        	
        	if(operation == '1') {
        		query = 'select Role_Id__c,Course_Id__r.Title__c from LMS_Role_Course__c' +  
    ' where Course_Id__c = :eCid and Status__c IN (\'Committed\',\'Draft Delete\',\'Pending - Delete\') ' + 
    filter + ' order by Role_Id__r.Role_Name__c';
        	} else if(operation == '2') {
	        	query = 'select Role_Id__c,Course_Id__r.Title__c from LMS_Role_Course__c' +  
    ' where Course_Id__c = :eCid and Status__c IN (\'Draft\',\'Pending - Add\') ' + filter + 
	        		' order by Role_Id__r.Role_Name__c';
        	} else if(operation == '3') {
	        	query = 'select Role_Id__c,Course_Id__r.Title__c from LMS_Role_Course__c' +  
    ' where Course_Id__c = :eCid and Status__c IN (\'Committed\',\'Draft\',\'Pending - Add\') ' + filter + 
	        		' order by Role_Id__r.Role_Name__c';
        	}
        	roles = Database.query(query);
        	for(LMS_Role_Course__c rc : roles) {
        		roleIds.add(rc.Role_Id__c);
        	}
        	
            List<LMS_Role_Employee__c> employees = 
	            [select Employee_Id__r.Name,Employee_Id__r.First_Name__c,Employee_Id__r.Last_Name__c,Employee_Id__r.Job_Class_Desc__c,
		        Employee_Id__r.Job_Code__c,Employee_Id__r.Business_Unit_Desc__c,Employee_Id__r.Department__c,
		        Employee_Id__r.Country_Name__r.Region_Name__c,Employee_Id__r.Country_Name__r.Name,
		        Employee_Id__r.Status_Desc__c,Role_Id__r.Role_Name__c,Assigned_On__c,Status__c,Sync__c,Commit_Date__c 
		        from LMS_Role_Employee__c where Role_Id__c IN :roleIds order by Employee_Id__r.Last_Name__c];    
            
            employeeWrapper = new List<LMS_PersonAssignment>();
            for(LMS_Role_Employee__c re : employees) {
            	employeeWrapper.add(new LMS_PersonAssignment(re, jobTitleMap));
            } 
                                
            String courseName = course.Title__c;
            title = '"Employee Listing for Course ' + courseName + '"';
            typeEmployee = true;
            typeCourse = !typeEmployee;
            typeRole = !typeEmployee;
        }
        
    }
    
    private Map<String, String> getJobTitleMap() {
    	Map<String, String> titleMap = new Map<String, String>();
    	List<Job_Title__c> jt = [select Job_Code__c, Job_Title__c from Job_Title__c];
    	for(Job_Title__c j : jt) {
    		titleMap.put(j.Job_Code__c, j.Job_Title__c);
    	}
    	
    	return titleMap;
    }
    
}