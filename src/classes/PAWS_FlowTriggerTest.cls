@istest private class PAWS_FlowTriggerTest
{
	@istest private static void coverAll()
	{
		PAWS_FlowTrigger testee = new PAWS_FlowTrigger();
		String sourceID = SObjectType.PAWS_Project_Flow_Country__c.keyPrefix + '000000000000000';
		
		STSWR1__Flow__c flow = new STSWR1__Flow__c(STSWR1__Source_ID__c = sourceID);
		testee.beforeUpdate(new List<STSWR1__Flow__c>{flow, new STSWR1__Flow__c()}, new List<STSWR1__Flow__c>{flow, new STSWR1__Flow__c()});
		
		System.assertEquals(flow.STSWR1__Source_ID__c, flow.PAWS_Project_Flow_Country__c);
		
		//Covering Flow.trigger
		try
		{
			insert new STSWR1__Flow__c();
		}
		catch (Exception e)
		{
			//do nothing
		}
		
	}
	
	@istest private static void coverDelete()
	{
		delete PAWS_ApexTestsEnvironment.Flow;
	}
}