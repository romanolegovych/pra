public with sharing class BDT_SecuritiesDataAccessor {

	/**
	 * Add standard reference name to field list
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @return String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('BDT_Securities__c');		
	}

	/**
	 * Add custom reference name to field list
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @return String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Name,';
		result += referenceName + 'CreateRoleNumbers__c,';
		result += referenceName + 'ReadRoleNumbers__c,';
		result += referenceName + 'UpdateRoleNumbers__c,';
		result += referenceName + 'DeleteRoleNumbers__c';
		return result;
	}

	/**
	 * Get all securities order default by name
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @return List<BDT_Securities__c>
	 */
	public static List<BDT_Securities__c> getAllSecurities () {
		String query = String.format('select {0} from BDT_Securities__c order by {1}',
								new List<String> {
									getSObjectFieldString(),
									'Name'
								}
							);
		return Database.query(query);
	}

}