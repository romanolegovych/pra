@isTest
private class AA_CostRateControllerTest{   
    
    /* Test Year Select options */
    static testMethod void getyearSelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_CostRateController controller = new AA_CostRateController();
         List < SelectOption > selection = controller.getyearSelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
     /* Test BU Select options */
    static testMethod void getbuSelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_CostRateController controller = new AA_CostRateController();
         List < SelectOption > selection = controller.getbuSelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
    /* Test FC Select options */
    static testMethod void getfcSelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_CostRateController controller = new AA_CostRateController();
         List < SelectOption > selection = controller.getfcSelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
    /* Test Country Select options */
    static testMethod void getcountrySelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_CostRateController controller = new AA_CostRateController();
         List < SelectOption > selection = controller.getcountrySelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
     /* Test Is Active Select options */
    static testMethod void getisactiveSelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_CostRateController controller = new AA_CostRateController();
         List < SelectOption > selection = controller.getisactiveSelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
    /* Test Rate Detail Page redirection */
    static testMethod void goToRateDetailTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_CostRateController controller = new AA_CostRateController();
         PageReference pageRef= controller.goToRateDetail();
         
         System.assertEquals(pageRef.getUrl(),Page.AA_Inflation_Override.getUrl());
    }
    
    /* Test Create Page redirection */
    static testMethod void createTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_CostRateController controller = new AA_CostRateController();
         PageReference pageRef= controller.create();
         
         System.assertEquals(pageRef.getUrl(),Page.AA_New_Cost_Rate.getUrl());
    }
    
    /* Test Create Page redirection */
    static testMethod void uploadTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_CostRateController controller = new AA_CostRateController();
         PageReference pageRef= controller.upload();
         
         System.assertEquals(pageRef.getUrl(),Page.AA_Cost_Rate_Upload.getUrl());
    }
     
    /* Test Submit Button */
    static testMethod void submitTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_CostRateController controller = new AA_CostRateController();
         PageReference pageRef= controller.submit();
         
         System.assertEquals(null,null);
    }
    
    
    /* Test Get Year Function */
    static testMethod void getYearTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_CostRateController controller = new AA_CostRateController();
         List < Integer > years = controller.getyear();
         
         System.assertNotEquals(years.size(),0);
    }
    
    /* Test Get Result Function */
    static testMethod void getresultTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_CostRateController controller = new AA_CostRateController();
         controller.selectedYear = '2011';
         PageReference pageRef= controller.submit();
         List < AA_CostRateController.CostRateWrapper > results = controller.getresult();
         
         System.assertNotEquals(results.size(),0);
    }
    
    /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }   
       
 }