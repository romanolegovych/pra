public with sharing class UserServices {
	
	public static String getUserPreferrences(String projectId, String applicationName){
		List<User_Preferences__c> results = selectUserPreferrences(projectId, applicationName);
		return results.isEmpty() ? null : results[0].Value__c;
	}
	
	public static void saveUserPreferrences(String projectId, String applicationName, String value){
		System.debug('--- applicationName: ' + applicationName);
		System.debug('--- value: ' + value);
		List<User_Preferences__c> results = selectUserPreferrences(projectId, applicationName);
		User_Preferences__c instance = results.isEmpty() 
				? new User_Preferences__c(
						Related_Id__c = projectId, 
						Application_Name__c = applicationName,
						Related_Object_Name__c = PC_Utils.getSObjectNameFromId(projectId, 'Protocol Id'),
						User__c = UserInfo.getUserId()
				)
				: results[0];
		instance.Value__c = value;
		upsert instance;
	}
	
	private static List<User_Preferences__c> selectUserPreferrences(String projectId, String applicationName){
		return [Select Value__c From User_Preferences__c Where Related_Id__c = :projectId And Application_Name__c = :applicationName And User__c = :UserInfo.getUserId()];
	}
	
	public class UserServicesException extends Exception {}
}