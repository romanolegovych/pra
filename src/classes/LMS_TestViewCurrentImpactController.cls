/** 
 * Test for view current impact controller
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestViewCurrentImpactController {
    static List<Job_Title__c> jobTitles;
    static List<Country__c> countries;
    static List<LMS_Role__c> roles;
    static List<Course_Domain__c> courseDomains;
    static List<LMS_Course__c> courses;
    static List<Employee_Details__c> employees;
    static List<LMS_Role_Course__c> roleCourses;
    static List<LMS_Role_Employee__c> roleEmps;
    static List<LMSConstantSettings__c> constants;
    static List<CourseDomainSettings__c> domains;
    static List<RoleAdminServiceSettings__c> settings;
    
    static void init() {
        
        constants = new LMSConstantSettings__c[] {
        	new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
    		new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
    		new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
    		new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D'),
    		new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
    		new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
    		new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
    		new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
    		new LMSConstantSettings__c(Name = 'typePRA', Value__c = 'PRA'),
    		new LMSConstantSettings__c(Name = 'typeProject', Value__c = 'Draft'),
    		new LMSConstantSettings__c(Name = 'typeAdhoc', Value__c = 'Draft'),
    		new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
    		new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
    		new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
    		new LMSConstantSettings__c(Name = 'statusToDelete', Value__c = 'Delete'),
    		new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended'),
    		new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
    		new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
    		new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
    		new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete')
        };
        insert constants;
        
        domains = new CourseDomainSettings__c[] {
            new CourseDomainSettings__c(Name = 'Archive', Domain_Id__c = 'Archive')
        };
        insert domains;
        
        settings =  new RoleAdminServiceSettings__c[] {
            new RoleAdminServiceSettings__c(Name = 'DOMAIN', Value__c = 'domain'),
            new RoleAdminServiceSettings__c(Name = 'ENDPOINT', Value__c = 'endpoint'),
            new RoleAdminServiceSettings__c(Name = 'TIMEOUT', Value__c = '50000')
        };
        insert settings;
    	
        jobTitles = new Job_Title__c[]{
            new Job_Title__c(Job_Code__c = 'Code1', Job_Title__c = 'Title1', Status__c = 'A'),
            new Job_Title__c(Job_Code__c = 'Code2', Job_Title__c = 'Title2', Status__c = 'A')
        };
        insert jobTitles;
        
        countries = new Country__c[]{
            new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='US/Canada', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry2',PRA_Country_ID__c='200', Country_Code__c='JU',Region_Name__c='Latin America', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry3',PRA_Country_ID__c='300', Country_Code__c='TB',Region_Name__c='Asia/Pacific', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry4',PRA_Country_ID__c='400', Country_Code__c='AB',Region_Name__c='Europe/Africa', 
                               Daily_Business_Hrs__c=8.0)
        };
        insert countries;
        
        roles = new LMS_Role__c[]{
            new LMS_Role__c(Adhoc_Role_Name__c = 'TestRole1', Status__c = 'Active', SABA_Role_PK__c = 'role001', Sync_Status__c = 'Y'),
            new LMS_Role__c(Adhoc_Role_Name__c = 'TestRole2', Status__c = 'Active', SABA_Role_PK__c = 'role002', Sync_Status__c = 'Y')
        };
        insert roles;
        
        courseDomains = new Course_Domain__c[]{
            new Course_Domain__c(Domain__c = 'Internal'),
            new Course_Domain__c(Domain__c = 'Archive'),
            new Course_Domain__c(Domain__c = 'Project Specific')
        };
        insert courseDomains;
        
        courses = new LMS_Course__c[]{
            new LMS_Course__c(Offering_Template_No__c = 'course123', Course_Code__c = 'C123', Title__c = 'Course 123', Domain_Id__c = 'Internal',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse1',
                              Discontinued_From__c = Date.today()-1),
            new LMS_Course__c(Offering_Template_No__c = 'course321', Course_Code__c = 'C124', Title__c = 'Course 321', Domain_Id__c = 'Archive',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse2',
                              Discontinued_From__c = Date.today()+1, Project_Id__c = 'Project1', Client_Id__c = 'Client1')
        };
        insert courses;
        
        employees = new Employee_Details__c[]{
            new Employee_Details__c(Name = 'employee1', Employee_Unique_Key__c = 'employee1', First_Name__c = 'Andrew', Last_Name__c = 'Allen', Email_Address__c = 'a@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU'),
            new Employee_Details__c(Name = 'employee2', Employee_Unique_Key__c = 'employee2', First_Name__c = 'Allen', Last_Name__c = 'Stephens', Email_Address__c = 's@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU')
        };
        insert employees;
        
        roleCourses = new LMS_Role_Course__c[]{
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft Delete', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Committed', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft Delete', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Committed', Assigned_By__c = 'ASA')
        };
        insert roleCourses;
        
        roleEmps = new LMS_Role_Employee__c[]{
            new LMS_Role_Employee__c(Role_Id__c = roles[0].Id, Employee_Id__c = employees[0].Id, Status__c = 'Draft', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today()),
            new LMS_Role_Employee__c(Role_Id__c = roles[0].Id, Employee_Id__c = employees[1].Id, Status__c = 'Draft', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today()),
            new LMS_Role_Employee__c(Role_Id__c = roles[1].Id, Employee_Id__c = employees[0].Id, Status__c = 'Draft', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today()),
            new LMS_Role_Employee__c(Role_Id__c = roles[1].Id, Employee_Id__c = employees[1].Id, Status__c = 'Draft', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today())
        };
        insert roleEmps;
    }
    
    static testMethod void testConstructorRoleId() {
        init();
        PageReference pr = new PageReference('/apex/LMS_ViewCurrentImpact?id='+roles[0].Id);
        Test.setCurrentPage(pr);
        
        LMS_ViewCurrentImpactController c = new LMS_ViewCurrentImpactController();
        System.assert(c.pageTitle == 'Employee Listing for Role: ' + roles[0].Adhoc_Role_Name__c &&
                      c.empList.size() == 2);
                      
        pr = new PageReference('/apex/LMS_ViewCurrentImpact?id='+roles[1].Id);
        Test.setCurrentPage(pr);
        
        c = new LMS_ViewCurrentImpactController();
        System.assert(c.pageTitle == 'Employee Listing for Role: ' + roles[1].Adhoc_Role_Name__c &&
                      c.empList.size() == 2);
    }
    
    static testMethod void testConstructorCourseId() {
        init();
        PageReference pr = new PageReference('/apex/LMS_ViewCurrentImpact?cid='+courses[0].Id);
        Test.setCurrentPage(pr);
        
        LMS_ViewCurrentImpactController c = new LMS_ViewCurrentImpactController();
        System.assert(c.pageTitle == 'Employee Listing for Course: ' + courses[0].Title__c &&
                      c.empList.size() == 4);
        
        pr = new PageReference('/apex/LMS_ViewCurrentImpact?cid='+courses[1].Id);
        Test.setCurrentPage(pr);
        
        c = new LMS_ViewCurrentImpactController();
        System.assert(c.pageTitle == 'Employee Listing for Course: ' + courses[1].Title__c &&
                      c.empList.size() == 4);
    }
}