public with sharing class WRModelsController {
	/**
	* to display the list of WR models   
	*/
	public List <WR_Model__c> WRModelsList    { get; set; }

	/** 
       @description Constructor
       @author Tkachenko Oleksiy
       @date 2015   
   */
	public WRModelsController() {
		WRModelsList =  PBB_DataAccessor.GetAllWrModels();
	}

	/** 
        @description Function to call New Page of required Model
        @author Tkachenko Oleksiy
        @date 2015  
   */
   public PageReference getNewPageReference() {
       Schema.DescribeSObjectResult R;
       R = WR_Model__c.SObjectType.getDescribe();
       return new PageReference('/' + R.getKeyPrefix() + '/e');
   }
}