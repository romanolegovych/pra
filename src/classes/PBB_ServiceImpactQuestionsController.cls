/** 
@Author Guo Grace 
@Date 2015
@Description this class is inserting the Service Impact Questions.
*/
public class PBB_ServiceImpactQuestionsController {

    public  ID  siqID                           {get;set;}                            //to get the Service Impact Questions ID
    public  Service_Impact_Questions__c siq     {get;set;}      //to get the Service Impact Questions Sobject for edit or new   
    
    //Selected & Available countries list
    public SelectOption[] selectedCountries     { get; set; }
    public SelectOption[] allCountries          { get; set; }
    public String parentId                      { get;set;}
    
    /**
    @author Bhargav Devaram
    @date 2015
    @description This is a constructor get the Service Impact Questions.  
    **/
    public PBB_ServiceImpactQuestionsController(ApexPages.StandardController controller){
        
        //check if the SIQ is for edit
        siq = (Service_Impact_Questions__c)controller.getRecord();
        siqID = ApexPages.currentPage().getParameters().get('id');
        
        allCountries = new List<SelectOption>();
        selectedCountries = new List<SelectOption>();
        
        //get all available countries
        for ( Country__c countryObj :PBB_DataAccessor.getAllCountriesList()) {
            allCountries.add(new SelectOption(countryObj.Name, countryObj.Name));
        }   
        string clone =ApexPages.currentPage().getParameters().get('clone');
        
        //check if record exists or not
        if(siqID !=null){
            list<Service_Impact_Questions__c> siqList  = PBB_DataAccessor.getServiceImpactQuestionsbyID(siqID);
            if(siqList.size()>0)
                siq  = siqList[0];
            
            parentId = siq.Service_Impact_Question__c; 
            System.debug('--Parent ID--'+parentId );    
            
            //Cloning Service Impact Questions
            if(clone !=null){
                siq  =siq.clone(false, true);
                siq.Status__c='In-Process';
            }
                       
            if(siq.Countries__c!=null){                     
                String[] splitArr = siq.Countries__c.split(';');                    
                Set<SelectOption> allCountriesSet = new Set<SelectOption>();
                allCountriesSet.addAll(allCountries);
                for(String country:splitArr){
                    selectedCountries.add(new SelectOption(country,country));
                    allCountriesSet.remove(new SelectOption(country,country));
                }                      
                allCountries.clear();
                allCountries.addAll(allCountriesSet);
            }            
            System.debug('<<<'+selectedCountries+'==='+allCountries);         
        }
        else{          
            siq.Question_Type__c='Single response for All Countries';  
            siq.Status__c='In-Process';   
        }
    }  
    
    /**
    @author Grace Guo
    @date 2015
    @description This method is to set the pre-approved questions to 'Retired'.
    **/    
    public void updateStatusToRetired(String name){    
        if(siqID !=null){             
            list<Service_Impact_Questions__c> siqList =new list<Service_Impact_Questions__c> ();            
            siqList = PBB_DataAccessor.getqtnsbyqnsName (parentId);            
            
            System.debug('--siqList--'+siqList);    
            System.debug('--parentID--'+parentId); 
            for(Service_Impact_Questions__c siqs: siqList){
                if ( siqs.Status__c == 'Approved' && siqs.id!= siq.ID && siqs.Service_Impact_Question__c == parentId) 
                     siqs.Status__c = 'Retired';
            }
            upsert siqList;
        }
    }    
    
    /**
    @author Bhargav Devaram
    @date 2015
    @description This method is to save the records of the Service Impact Questions.
    **/
    public PageReference saveSIQuestions(){
        try{             
            list<Service_Impact_Questions__c> siqList = PBB_DataAccessor.getqtnsbyqnsName(siq.Service_Impact_Question__c);
            System.debug('--siqList --'+siqList +siq.Question_Applies_to__c+parentId  );  
            if(siqList.size()>0) {
                if(!siq.Question_Applies_to__c.contains(siqList[0].Question_Applies_to__c)  ){
                    ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Previous version of this question is changed, please use the same version of Question Applies to');
                    ApexPages.addMessage(myMsg);
                    return null;  
                }                
            }                 
            String  countries='';
            if(selectedCountries!=null && selectedCountries.size()>0 && siq.Question_Applies_to__c == 'Country - Specific'){
                for(SelectOption so : selectedCountries){
                    countries+=so.getValue()+';';
                }
                if(countries!=''){
                    countries=countries.substring(0,countries.length()-1);
                }
            }             
            //Countries are required for Country - Specific 
            if(siq.Question_Applies_to__c == 'Country - Specific' && selectedCountries.size()==0){                     
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please select the countries');
                ApexPages.addMessage(myMsg);
                return null;  
            }
            if(siq.Question_Applies_to__c == 'Project' ){                     
                siq.Question_Type__c='';       
            }
            siq.countries__c=countries;         
            upsert siq ;
            
            //Cloning Service Impact Responses while saving 
            string clone =ApexPages.currentPage().getParameters().get('clone');
            if(!String.isEmpty(clone)){                
                List<Service_Impact_Response__c> serviceimpres = new List<Service_Impact_Response__c>();
                List<Service_Impact_Response__c> ImpactresList = PBB_DataAccessor.getAllServiceImpactresp(siqID);                                  
                for(Service_Impact_Response__c res : ImpactresList){
                    Service_Impact_Response__c newres = res.clone(false, true);  //do a deep clone
                    newres.Service_Impact_Questions__c = siq.ID;
                    serviceimpres.add(newres);
                    system.debug('****serviceimpres'+serviceimpres);
                }
                upsert serviceimpres;
            }
            
            PageReference p = new PageReference('/'+siq.Id);           
            p.setRedirect(true);               
            if(siq.Status__c == 'Approved' )
                updateStatusToRetired(siq.name);                         
            return p;
        }
        catch(Exception ex){
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''+ex.getmessage()));
        }
        return null;    
    }
    
    /**
    @author Bhargav Devaram
    @date 2015
    @description This method is get back to the parent record.
    **/      
    public PageReference cancel(){
        PageReference p = new PageReference('/'+siq.Service_Impact_Question__c);
        p.setRedirect(true);
        return p;
    }     
}