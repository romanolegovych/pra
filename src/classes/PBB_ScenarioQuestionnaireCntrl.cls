/** 
@Author Bhargav Devaram
@Date 2015
@Description this service class used for Questionnaires for selected countries and service tasks
*/
public class PBB_ScenarioQuestionnaireCntrl {

    public String PBBScenarioID{ get; set; }              //PBB Scenario object ID 
    Public PBB_Scenario__c  PBBScenario  {get;set;}       //PBB scenarios   
    Public Bid_Project__c BidProject    {get;set;}        //Bid Project      
    public String ServiceAreaID{get;set;}                 //Service Area ID
    public String SIQid{get;set;}                         //Service Impact Question ID  
    public String Cntry{get;set;}                         //Country ID  
   
    //to hide/show the main tabs in the component.
    public  List<String> HiddenHomeTabsList{get;set;}
    
    //to hide/show the subtabs in the component.
    public  List<String> HiddenSubTabsList{get;set;}
    
    //to display all service areas for PBB Scenario
    public  List<Service_Area__c> SAList{get;set;}
    
    //to hold project related questionnaire    
    public  Map<String,PBB_QuestionnaireService.QuestionWrapper> ProjectLevelQuestionMap{get;set;}
    public integer QMapSize { get { return ProjectLevelQuestionMap.size( ); } }
    
    //Constructor
    public PBB_ScenarioQuestionnaireCntrl() {
        
        //get PBB Scenario ID
        PBBScenarioID= ApexPages.currentPage().getParameters().get('PBBScenarioID');
        PBBScenario = PBB_DataAccessor.getScenarioByID(PBBScenarioID);
        
        //get Service Areas by PBB Scenario
        SAList = new List<Service_Area__c>();
        SAList = PBB_QuestionnaireService.getServiceAreasbyPBBScenarioID(PBBScenarioID);
        
        //home tabs and sub tabs
        HiddenHomeTabsList = new List<String>();
        HiddenSubTabsList = new List<String>();        
        
        ProjectLevelQuestionMap = new Map<String,PBB_QuestionnaireService.QuestionWrapper>();
    }
    
    /** 
    @Author Bhargav Devaram
    @Date 2015
    @Description this method is to get all Questionnaire by Service Areas for PBB Scenario
    */
    public void getQuestionnairebyServiceArea() {        
        
        SIQid='';
        Cntry='';         
        ProjectLevelQuestionMap = new Map<String,PBB_QuestionnaireService.QuestionWrapper>();
        ProjectLevelQuestionMap = PBB_QuestionnaireService.getQuestionnairebyServiceArea(ServiceAreaID,PBBScenarioID);         
        system.debug('--ServiceAreaID-'+ServiceAreaID+ProjectLevelQuestionMap  );
    }    

    /** 
    @Author Bhargav Devaram
    @Date 2015
    @Description this method is to save all Questionnaire by Service Areas for PBB Scenario
    */
    public void SaveQuestionnairebyServiceArea() {
        system.debug('--ServiceAreaID-'+ServiceAreaID+ProjectLevelQuestionMap+SIQid+Cntry);       
        PBB_QuestionnaireService.SaveQuestionnairebyServiceArea(ServiceAreaID,PBBScenarioID,ProjectLevelQuestionMap.get(SIQid),Cntry);  
        //check if you are on still same question
        if(Cntry=='')
            SIQid='';
        Cntry='';   
        system.debug('--ServiceAreaID-'+ServiceAreaID+SIQid);
    }
}