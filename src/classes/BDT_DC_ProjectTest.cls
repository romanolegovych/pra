@isTest
private class BDT_DC_ProjectTest {

    static testMethod void createEmptyProject() {
        Client_Project__c cp;
        cp = BDT_DC_Project.createEmptyProject();
        // test if first creation has id
        system.assert(cp.id == null);
        system.assert(cp.code__c == null);
    }
    
    static testMethod void getNewProjectCode() {
    	
    	String firstCode = BDT_DC_Project.getNewProjectCode();
    	
    	Client_Project__c cp;
    	cp = BDT_DC_Project.createEmptyProject();
    	cp.code__c = firstCode;
        upsert cp;
        
        String secondCode = BDT_DC_Project.getNewProjectCode();
    	system.assertNotEquals(secondCode, firstCode);
    	
    	system.assert(firstCode.endsWith('001'));
    	system.assert(secondCode.endsWith('002'));
    	
    }
}