@isTest
private class BDT_NewEditGroupArmAssignmentTest {
	public static Client_Project__c 						firstProject;
	public static List<Study__c> 							studiesList;
	public static List<ClinicalDesign__c> 					designList;
	public static List<Business_Unit__c> 					myBUList;
	public static list<ClinicalDesignStudyAssignment__c> 	cdsaList;
	public static List<Arm__c>								armList;
	public static List<Population__c> 						populationList;
	public static List<PopulationStudyAssignment__c> 		spaList;
	public static list<StudyDesignPopulationAssignment__c>  sdpaList;
	
	static void init(){
		firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		
		myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert myBUList;
		
		studiesList = BDT_TestDataUtils.buildStudies(firstProject);
		studiesList[0].Business_Unit__c = myBUList[0].Id; 
		studiesList[1].Business_Unit__c = myBUList[1].Id; 
		studiesList[2].Business_Unit__c = myBUList[2].Id;
		studiesList[3].Business_Unit__c = myBUList[3].Id; 
		studiesList[4].Business_Unit__c = myBUList[4].Id;
		insert studiesList;
		
		designList = BDT_TestDataUtils.buildClinicDesign(firstProject); 
		insert designList;
		
		//create population
		populationList = BDT_TestDataUtils.buildPopulation(firstProject.id, 4);
		insert populationList;

		// add populations to study
		spaList = BDT_TestDataUtils.buildPopulationStudyAssignment(studiesList, populationList);
		upsert spaList;		
		
		// add design to study
		cdsaList = BDT_TestDataUtils.buildDesignAssignment(designList,studiesList); 
		upsert cdsaList;
		
		// add populations to study design	
		sdpaList = BDT_TestDataUtils.buildStudyDesignPopulationAssignment(cdsaList, spaList );
		upsert sdpaList;	
		
		// add arms to clinical design
		armList = BDT_TestDataUtils.buildArms(designList,3);			 				  
		upsert armList;
		
		BDT_Utils.setPreference('SelectedProject', String.valueOf(firstProject.id));
		String studyList;
		For (Study__c study:studiesList) {
			studyList = studyList + ':' + String.valueOf(study.id);
		}
		studyList = studyList.substringAfter(':');
		BDT_Utils.setPreference('SelectedStudies',studyList);
		
	}
	
	static testMethod void createDesignPath() { 
		init();
		
		PageReference b = new Pagereference( system.Page.BDT_NewEditGroupArmAssignment.getUrl());
		b.getParameters().put('ClinicalDesignStudyAssignmentID',String.valueOf(cdsaList[0].id));
		Test.setCurrentPage(b);
		
		BDT_NewEditGroupArmAssignment GroupArmAssignment = new BDT_NewEditGroupArmAssignment();
		
		// extend an item
		String armID = GroupArmAssignment.ArmWList[0].arm.id;
		String sdpaID  = GroupArmAssignment.ArmWList[0].StudyDesignPopulationAssignmentWL[0].StudyDesignPopulationAssignment.id;
		b = new Pagereference( system.Page.BDT_NewEditGroupArmAssignment.getUrl());
		b.getParameters().put('SDPAID',sdpaID);
		b.getParameters().put('ARMID',armID);
		Test.setCurrentPage(b);
		GroupArmAssignment.ExtendList();
		
		// save
		GroupArmAssignment.save();
		
		// reduce list
		b = new Pagereference( system.Page.BDT_NewEditGroupArmAssignment.getUrl());
		b.getParameters().put('SDPAID',sdpaID);
		b.getParameters().put('ARMID',armID);
		b.getParameters().put('INDEX','0');
		Test.setCurrentPage(b);
		GroupArmAssignment.ReduceList();
		
		// save
		GroupArmAssignment.save();
		
		// cancel
		GroupArmAssignment.cancel();
		
		// reload page as now data was saved
		b = new Pagereference( system.Page.BDT_NewEditGroupArmAssignment.getUrl());
		b.getParameters().put('ClinicalDesignStudyAssignmentID',String.valueOf(cdsaList[0].id));
		Test.setCurrentPage(b);
		GroupArmAssignment = new BDT_NewEditGroupArmAssignment();
		
		
		
	}
}