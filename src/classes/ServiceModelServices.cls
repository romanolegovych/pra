public class ServiceModelServices {

    public static List<Service_Model__c> getServiceModels() {
        return [SELECT Id, Name, CreatedBy.Id, CreatedBy.LastName, CreatedBy.FirstName, 
                    LastModifiedDate, CreatedDate, Description__c, Service_Area_Count__c, Service_Model_Unique_ID__c,
                    LastModifiedById, LastModifiedBy.Id, LastModifiedBy.LastName, LastModifiedBy.FirstName, SM_AN__c, Status__c
                FROM Service_Model__c 
                ORDER BY Name
                LIMIT 1000];
    }

    public static List<Service_Area__c> getServiceAreaByServiceModelId(Id serviceModelId) {
        return [SELECT Id, Name, CreatedDate, CreatedById, Description__c, Service_Model__c, Service_Area_Unique_ID__c, SA_AN__c,  
                    LastModifiedById, LastModifiedBy.Id, LastModifiedBy.LastName, LastModifiedBy.FirstName, LastModifiedDate,
                    CreatedBy.Id, CreatedBy.LastName, CreatedBy.FirstName
                FROM Service_Area__c 
                WHERE Service_Model__c = : serviceModelId 
                ORDER BY Name
                LIMIT 1000];
    }

    public static List<Service_Function__c> getserviceFunctionsByServiceAreaId(Id serviceArealId) {
        return [SELECT Id, Name, CreatedDate, CreatedById, Description__c, Service_Area__c, Service_Function_Unique_ID__c, SF_AN__c,
                    LastModifiedById, LastModifiedBy.Id, LastModifiedBy.LastName, LastModifiedBy.FirstName, LastModifiedDate,
                    CreatedBy.Id, CreatedBy.LastName, CreatedBy.FirstName
                FROM Service_Function__c
                WHERE Service_Area__c = : serviceArealId 
                ORDER BY Name
                LIMIT 1000];
    }

    public static List<Service_Task__c> getServiceTasksByServiceFunctionId(Id serviceFunctionId) {
        return [SELECT Id, Name, CreatedDate, CreatedById, Service_Function__c, Description__c, Service_Task_Unique_ID__c, 
                    ST_AN__c, Service_Model__c, Global__c, Business_Segment__c,
                    LastModifiedById, LastModifiedBy.Id, LastModifiedBy.LastName, LastModifiedBy.FirstName, LastModifiedDate,
                    CreatedBy.Id, CreatedBy.LastName, CreatedBy.FirstName
                FROM Service_Task__c
                WHERE Service_Function__c = : serviceFunctionId 
                ORDER BY Name
                LIMIT 1000];
    }

    public static Service_Task__c getServiceTaskById(Id serviceTaskId) {
        return [SELECT Id, Name, CreatedDate, CreatedById, Service_Function__c, Description__c, Service_Task_Unique_ID__c, 
                    ST_AN__c, Service_Model__c, Global__c, Business_Segment__c,
                    LastModifiedById, LastModifiedBy.Id, LastModifiedBy.LastName, LastModifiedBy.FirstName, LastModifiedDate,
                    CreatedBy.Id, CreatedBy.LastName, CreatedBy.FirstName
                FROM Service_Task__c
                WHERE Id = : serviceTaskId];
    }

    public static List<Service_Task_To_Service_Impact__c> getServiceTaskToServiceImpactsByServiceTaskId(Id serviceTaskId) {
        return [SELECT Id, OwnerId, Name, CreatedDate, CreatedById, Service_Impact_Question_Name__c, Service_Task__c,
                    LastModifiedById, LastModifiedBy.Id, LastModifiedBy.LastName, LastModifiedBy.FirstName, LastModifiedDate,
                    CreatedBy.Id, CreatedBy.LastName, CreatedBy.FirstName, Service_Mod_Status__c
                FROM Service_Task_To_Service_Impact__c
                WHERE Service_Task__c = : serviceTaskId 
                ORDER BY Name
                LIMIT 1000];
    }



    public static List<Service_Task_To_Drivers__c> getServiceTaskToDriversListByServiceTaskId(Id serviceTaskId) {
        return [SELECT Cost__c, Countries__c, Countries_Option__c, Country_Fact_Sheet__c, Description__c, Drivers__c, Driver_Type__c,
                    Fee_Expense_Service__c, Formula_Name__c, Frequency__c, Parameter_Value__c, Per_Frequency__c, Price__c, Service_Model_Status__c,
                    Service_Task__c, Service_Task__r.Global__c, Status__c, Times_Frequency__c, Name, Rounding_Methods__c
                FROM Service_Task_To_Drivers__c 
                WHERE Service_Task__c = : serviceTaskId
                ORDER BY Name
                LIMIT 1000];
    }

    public static Service_Task_To_Drivers__c getServiceTaskToDriversById(Id serviceTaskToDriversId) {
        return [SELECT Cost__c, Countries__c, Countries_Option__c, Country_Fact_Sheet__c, Description__c, Drivers__c, Driver_Type__c,
                    Fee_Expense_Service__c, Formula_Name__c, Frequency__c, Parameter_Value__c, Per_Frequency__c, Price__c, Service_Model_Status__c,
                    Service_Task__c, Status__c, Times_Frequency__c, Name, Rounding_Methods__c
                FROM Service_Task_To_Drivers__c 
                WHERE Id = : serviceTaskToDriversId];
    }

    public static List<Country__c> getCountry() {
        return [SELECT Id, Name, Country_Code__c, Description__c, Currency__c, Is_PRA_Office__c FROM Country__c ORDER BY Name LIMIT 1000];
    }
}