@isTest(SeeAllData=false)
private class PRA_CompletedUnitsModuleControllerTest {
    static String ST_NONE;
    static Client_Project__c pro;
    static Project_Client_Region__c reg;
    static Country__c coun;
    static Operational_Area__c oa;
    static Unit_Effort__c un;
    static Unit_Effort__c un2;
    static Task_Group__c tg;
    static Client_Task__c ct;
    
    static void init() {
        // Cleaning user prefereneces
        List<User_Preference__c> userPrefList = [Select Id, UnconfirmedOnly__c, ConfirmedOnly__c, User_Id__c, Region_Id__c, Operational_Area_Id__c, Country_Id__c, Project_Id__c, Client_Unit_Id__c, Client_Unit_Group_Id__c FROM User_Preference__c ];
        delete userPrefList;
        
        PRA_CompletedUnitsModuleController classToTest = new PRA_CompletedUnitsModuleController();
        ST_NONE = classToTest.ctSearch.ST_NONE;
        
        Account client = new Account(Name = 'test2');
        insert client;
        
        pro = new Client_Project__c(Client__c = client.Id, Name='pro1', PRA_Project_Id__c = 'bcv', Contracted_Start_Date__c = Date.today().addMonths(-8),
                                Contracted_End_Date__c = Date.today().addMonths(8), Estimated_End_Date__c = Date.today().addMonths(8));
        insert pro;
        
        coun = new Country__c(Name = 'test', PRA_Country_Id__c = 'rid');
        insert coun;
        
        reg = new Project_Client_Region__c (Name = 'na', PRA_Region_Id__c = 'prid');
        insert reg;
        
        oa = new Operational_Area__c(Name = 'Therapeutic Expertise', PRA_Operational_Area_Id__c = 'bcv');
        insert oa;
        
        tg = new Task_Group__c(Name = 'IMV', Operational_Area__c = oa.Id, PRA_Task_Group_Id__c = 'bcv');
        insert tg;
        
        ct = new Client_Task__c(Task_Group__c = tg.Id, Project__c = pro.Id, PRA_Client_Task_Id__c = 'ybcvy', Description__c = 'desc', Start_Date__c = Date.today().addMonths(-8),
                 Original_End_Date__c = Date.today().addMonths(-8), Forecast_Curve__c = 'Therapeutic Expertise', Country__c = coun.Id, Project_Region__c = reg.Id,
                 Total_Contract_Units__c = 60, Total_Contract_Hours__c = 120);
        insert ct;
        
        un = new Unit_Effort__c(Month_Applies_To__c = Date.newinstance(DateTime.now().year(), DateTime.now().month()-1, 1), Client_Task__c = ct.Id, 
        	Worked_Unit__c = 10, Is_Confirmed_Worked_Unit__c = true, Unit_Effort_Id__c = ct.Id + '-' + Date.newinstance(DateTime.now().year(), DateTime.now().month()-1, 1));
        insert un;
        
        // 2nd Operational Area
        Operational_Area__c oa2 = new Operational_Area__c(Name = 'Project Management', PRA_Operational_Area_Id__c = 'irt');
        insert oa2;
        
        Task_Group__c tg2 = new Task_Group__c(Name = 'IMV', Operational_Area__c = oa2.Id, PRA_Task_Group_Id__c = 'irt');
        insert tg2;
        
        Client_Task__c ct2 = new Client_Task__c(Task_Group__c = tg2.Id, Project__c = pro.Id, PRA_Client_Task_Id__c = 'irt', Description__c = 'desc', 
                                Start_Date__c = Date.today().addMonths(-8), Original_End_Date__c = Date.today().addMonths(-8),
                                Forecast_Curve__c = 'Project Management', Country__c = coun.Id, Project_Region__c = reg.Id,
                                Total_Contract_Units__c = 60, Total_Contract_Hours__c = 120);
        insert ct2;
        
        un2 = new Unit_Effort__c(Month_Applies_To__c = Date.newinstance(DateTime.now().year(), DateTime.now().month()-1, 1), Client_Task__c = ct2.Id, 
        	Source_Worked_Unit__c = 10, Is_Confirmed_Worked_Unit__c = false, Unit_Effort_Id__c = ct2.Id + '-' + Date.newinstance(DateTime.now().year(), DateTime.now().month()-1, 1));
        insert un2;
    }
    
    static testMethod void testSearch_POSITIVE() {
        init();
        PRA_CompletedUnitsModuleController classToTest = new PRA_CompletedUnitsModuleController();
        classToTest.selectedProject = pro.Id;
        classToTest.selectProject();
        classToTest.searchTasks();
        System.assertNotEquals(classToTest.liWrappers, null);
        System.assertNotEquals(classToTest.liWrappers.size(), 0);
        System.assertEquals(classToTest.liWrappers.size(), 1);
        
        classToTest.selectedRegion = new List<String>{reg.Id};
        classToTest.selectedOperationalArea = new List<String>{oa.Id};
        classToTest.selectedGroup = new List<String>{tg.Id};
        classToTest.selectedTask = new List<String>{ct.Id};
        classToTest.confirmedOnly = true;
        classToTest.searchTasks();
        System.assertNotEquals(classToTest.liWrappers, null);
        System.assertNotEquals(classToTest.liWrappers.size(), 0);
        
        // Testing unconfirmed
        classToTest.unconfirmedOnly = true;
        classToTest.searchTasksVF();
        System.assertEquals(classToTest.liWrappers.size(), 1);
    }
    
    static testMethod void testSearch_OA_POSITIVE() {
        init();
        PRA_CompletedUnitsModuleController classToTest = new PRA_CompletedUnitsModuleController();
        classToTest.selectedProject = pro.Id;
        classToTest.selectedOperationalArea = new List<String>{oa.Id};
        //classToTest.isFirstTime = false;
        classToTest.selectProject();
        classToTest.searchTasks();
        System.assertNotEquals(classToTest.liWrappers, null);
        System.assertEquals(classToTest.liWrappers.size(), 1);
    }
    
    static testMethod void testSearch_Unconfirmed() {
        init();
        PRA_CompletedUnitsModuleController classToTest = new PRA_CompletedUnitsModuleController();
        classToTest.selectedProject = pro.Id;
        classToTest.unconfirmedOnly = true;
        classToTest.selectProject();
        classToTest.searchTasks();
        System.assertNotEquals(classToTest.liWrappers, null);
        System.assertEquals(classToTest.liWrappers.size(), 0);
    }
    
    static testMethod void testPrepareDates_POS() {
        init();
        PRA_CompletedUnitsModuleController classToTest = new PRA_CompletedUnitsModuleController();
        classToTest.selectedMonthStringSOQL = '2012-03-01';
        classToTest.prepareDates();
        System.assertEquals(classToTest.selectedMonthStringMinusOneSOQL, '2012-02-01');
        System.assertEquals(classToTest.selectedMonthStringMinusTwoSOQL, '2012-01-01');
    }
    
    static testMethod void testVFHelpers() {
        init();
        PRA_CompletedUnitsModuleController classToTest = new PRA_CompletedUnitsModuleController();
        classToTest.selectedProject = 'None';
        classToTest.selectProject();
        System.assertEquals(classToTest.selectedRegion, new String[]{ST_NONE});
        
        classToTest.selectedProject = pro.Id;
        classToTest.selectProject();
        System.assertEquals(classToTest.selectedOperationalArea, new String[]{ST_NONE});
        
        
        classToTest.selectedOperationalArea = new List<String>{oa.Id};
        classToTest.ctSearch.initOperationalAreaOptions();
        
        System.assertNotEquals(null, classToTest.exportToExcel());

    }
    
    static testMethod void testclickInclAdj() {
        init();
        PRA_CompletedUnitsModuleController.clickInclAdj(un2.Id, 'true');
        Unit_Effort__c uni = [SELECT include_adjustment__c FROM Unit_Effort__c WHERE Id = :un2.Id];
        System.assert(uni.include_adjustment__c);
    }
    
    static testMethod void testclickConfirmed() {
        init();
        PRA_CompletedUnitsModuleController.clickConfirmed(un2.Id, 'true');
        Unit_Effort__c uni = [SELECT Is_Confirmed_Worked_Unit__c FROM Unit_Effort__c WHERE Id = :un2.Id];
        System.assert(uni.Is_Confirmed_Worked_Unit__c);
    }
    
    // Test save User prefs
    static testMethod void testsavePrefs() {
        init();
        Profile prID = [Select Id From Profile WHERE Name Like 'System Administrator' LIMIT 1];
        list<User> us1 = [Select Id From User WHERE ProfileID = :prID.Id and IsActive=true limit 1];//take 1 user which will always exist 
        
        if (us1.size() > 0){
            System.runAs(us1[0]){
                PRA_CompletedUnitsModuleController cont = new PRA_CompletedUnitsModuleController();
                cont.selectedProject = pro.Id;
		        cont.selectProject();
                cont.selectedRegion = new List<String>{reg.Id};
                PRA_Utils.savePrefs(cont.selectedProject, cont.selectedRegion, cont.selectedOperationalArea, cont.selectedTask, 
                                 cont.selectedGroup, cont.unconfirmedOnly, cont.confirmedOnly);
                String regionId =  [Select Region_Id__c FROM User_Preference__c WHERE User_Id__c = :UserInfo.getUserId()].Region_Id__c.split(',').get(0);
                System.assertEquals(regionId, reg.Id);
                
                cont = new PRA_CompletedUnitsModuleController();
                System.assertEquals(cont.selectedRegion[0], reg.Id);
            }
        }
    }
    
    // Test save User prefs
    static testMethod void testSearchProject() {
        init();
        // We update to 15 = we add 5
        List<Client_Project__c> liPro = PRA_CompletedUnitsModuleController.searchProject('pro');
        System.assert(liPro.size() > 0);
        
        liPro = PRA_CompletedUnitsModuleController.searchProject('xxx');
        System.assertEquals(0 ,liPro.size() );
    }
    
    // test remoteaction updating list of Units
    static testMethod void testShouldUpdateWorkedUnits() {
        //given
        init();
        un.Worked_Unit__c += 2;
        un.Include_Adjustment__c = true;
        un.Is_Confirmed_Worked_Unit__c = false;
        
        un2 = new Unit_Effort__c(Month_Applies_To__c = Date.newinstance(DateTime.now().year(), DateTime.now().month()-2, 1), Client_Task__c = un2.Client_Task__c, 
        	Worked_Unit__c = 10, Is_Confirmed_Worked_Unit__c = true, Unit_Effort_Id__c = un2.Client_Task__c + '-' + Date.newinstance(DateTime.now().year(), DateTime.now().month()-2, 1));
        insert un2;
        
       
        un2.Worked_Unit__c += 20;
        un2.Include_Adjustment__c = false;
        un2.Is_Confirmed_Worked_Unit__c = true;
        
        List<Unit_Effort__c> units = new List<Unit_Effort__c>{un, un2};
        
        //when
        test.startTest();
               
        PRA_CompletedUnitsModuleController.updateWorkedUnitList(units);
         
        //then
        test.stopTest();
        Map<Id, Unit_Effort__c> updatedUnits = new Map<Id, Unit_Effort__c> ([SELECT Id, Is_Confirmed_Worked_Unit__c, Include_Adjustment__c, Forecast_Unit__c, Baseline_Unit__c, Worked_Unit__c,
       																		Last_Approved_Unit__c, Client_Task__r.Total_Worked_Units__c FROM Unit_Effort__c WHERE Id IN :units]);
        
        system.assertEquals(2, updatedUnits.size());
        Unit_Effort__c updateUn = updatedUnits.get(un.Id);
        Unit_Effort__c updateUn2 = updatedUnits.get(un2.Id);
        system.assertEquals(un.Worked_Unit__c, updateUn.Worked_Unit__c);
        system.assertEquals(un.Include_Adjustment__c, updateUn.Include_Adjustment__c);
        system.assertEquals(un.Is_Confirmed_Worked_Unit__c, updateUn.Is_Confirmed_Worked_Unit__c);
        
        system.assertEquals(un2.Worked_Unit__c, updateUn2.Worked_Unit__c);
        system.assertEquals(un2.Include_Adjustment__c, updateUn2.Include_Adjustment__c);
        system.assertEquals(un2.Is_Confirmed_Worked_Unit__c, updateUn2.Is_Confirmed_Worked_Unit__c);
    }
    
    static testMethod void testBlockedProperty() {
    	init();
        
        Profile pr = [select id, name from Profile where name = 'Generic Employee'];
        User testUser = new User(alias='TestUser',Email='testPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='PZ', 
                            LocaleSidKey='en_US', ProfileId=pr.id, TimeZoneSidKey='America/New_York', Username='Test.PZ@praintl.com.unitTestPMConsole');
        insert testUser;
        
        system.runas(testUser){
	        PRA_CompletedUnitsModuleController classToTest = new PRA_CompletedUnitsModuleController();
	        classToTest.selectedProject = pro.Id;
	    	
	    	test.startTest();
	    	classToTest.searchTasksVF();
	    	
	    	system.assertEquals(false, classToTest.blocked);
	
	    	Date monthAppliesTo = Date.today().toStartOfMonth();
	    	Monthly_Approval__c ma = [select Month_Approval_Applies_To__c, Is_Approved_PM_Worked_Units__c
	                                  from Monthly_Approval__c
	                                  where Month_Approval_Applies_To__c = :monthAppliesTo and Client_Project__r.id = :pro.id];
	    	
	    	if (ma != null){
	   	    	ma.Is_Approved_PM_Worked_Units__c = true;
				update ma;
	    	}
	    	
	    	classToTest.searchTasksVF();
	    	test.stopTest();
	
	    	system.assertEquals(true, classToTest.blocked);
        }
    }
}