/**
* This class provides a temporary notification solution for PAWS Project flows if some of the steps is
* more then 1 day overdue. Works together with PAWS Overdue Monitoring flow.
*
* @author illia leshchuk (ilya@silvertreesystems.com)
*/
public with sharing class PAWS_OverdueAction implements STSWR1.WorkflowCallApexActionInterface
{
	/**
	* This method is invoked from WR core.
	*/
	public void run(SObject context, STSWR1__Flow_Instance_Cursor__c cursor, String operationType, String parameter)
	{
		sendNotification((PAWS_Project_Flow_Junction__c) context);
		clearHistory(cursor);
	}
	
	/**
	* Sends notification in case if there is at least one day overdue!
	*/
	private void sendNotification(PAWS_Project_Flow_Junction__c junction)
	{
		PAWS_Utilities.checkLimit('Queries', 2);
		
		String body = '';
		List<ID> stepIDs = new List<ID>();
		for (STSWR1__Flow_Instance_Cursor__c each : [select STSWR1__Step__c from STSWR1__Flow_Instance_Cursor__c
				where STSWR1__Flow_Instance__r.STSWR1__Flow__c = :junction.Flow__c])
		{
			stepIDs.add(each.STSWR1__Step__c);
		}
		
		Date deadline = System.today();
		List<String> toAddresses = new List<String>();
		for (STSWR1__Gantt_Step_Property__c each : [select STSWR1__Step__r.Id, Overdue_Notification_Time__c, STSWR1__Step__r.Name, Project__r.CreatedBy.Email, Project__r.CreatedBy.Name, Project__r.Name, Project__r.Clinical_Informatics_Manager__r.Name, Project__r.Clinical_Informatics_Manager__r.Email from STSWR1__Gantt_Step_Property__c
				where STSWR1__Flow__c = :junction.Flow__c
				and STSWR1__Level__c = 'First'
				and STSWR1__Step__c in :stepIDs
				and Overdue_Notification_Time__c = null//this field is used to determine whether we notified CI Manager already
				and (
					(STSWR1__Revised_End_Date__c = null and STSWR1__Planned_End_Date__c < :deadline)
					or (STSWR1__Revised_End_Date__c != null and STSWR1__Revised_End_Date__c < :deadline)
				)
				])
		{
			String name = each.Project__r.Clinical_Informatics_Manager__r.Name != null
				? each.Project__r.Clinical_Informatics_Manager__r.Name
				: each.Project__r.CreatedBy.Name;
			
			body += 'Dear, ' + name + '!<br/>'
				+ 'This message has been sent to notify that the step <b>"' + each.STSWR1__Step__r.Name + '"</b> '
				+ 'for project <b><a href=' + Url.getSalesforceBaseUrl().toExternalForm() + '/' + junction.Project__c + '>' + each.Project__r.Name + '</a></b> is overdue!<br/>';
			
			if (each.Project__r.Clinical_Informatics_Manager__r.Email != null)
			{
				toAddresses.add(each.Project__r.Clinical_Informatics_Manager__r.Email);
			}
			else
			{
				toAddresses.add(each.Project__r.CreatedBy.Email);
			}
			
			each.Overdue_Notification_Time__c = System.now();
			
			PAWS_Utilities.checkLimit('DMLStatements', 1);
			update each;
			sendEmail(body, toAddresses);
		}
	}
	
	/**
	* Clears the step history, since this step is constantly running in backgound.
	*/
	public void clearHistory(STSWR1__Flow_Instance_Cursor__c cursor)
	{
		PAWS_Utilities.checkLimits(new Map<String, Integer> 
		{
			'Queries' => 1,
			'DMLStatements' => 1
		});
		
		List<STSWR1__Flow_Instance_History__c> history = [select Id from STSWR1__Flow_Instance_History__c where 
			STSWR1__Status__c = 'Completed'
				and STSWR1__Cursor__c = :cursor.Id
				order by STSWR1__Cursor__c, CreatedDate, Name
			];
				
		if (!history.isEmpty())
		{
			history.remove(0);
		}
		
		delete history;
	}
	
	/**
	* Sends actual email with the <code>body</code> html body to <code>toAddresses</code> email addresses.
	*/
	public void sendEmail(String body, List<String> toAddresses)
	{
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setSubject('Step Overdue Notification!');
		mail.setToAddresses(toAddresses);
		mail.setUseSignature(false);
		mail.saveAsActivity = false;
		mail.setHtmlBody(body);
		
		Messaging.sendEmail(new List<Messaging.SingleEmailMessage>{mail});
	}
}