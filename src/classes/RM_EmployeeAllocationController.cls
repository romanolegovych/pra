public with sharing class RM_EmployeeAllocationController {
	private final list<allocationReview>allocationData;
    private final integer startMonth = -4;
    private final integer endMonth = 0;
    public string displayType{get;private set;}
    public List<string> monthLabel{get;private set;}
    public string emplID {get; set;}
    public boolean  bVisHr{get; set;}
    public List<WFM_Employee_Availability__c> lstPost{set; get;}  //lst for post PTO grid
    public boolean isPostSwitch{get; set;}
     
    public RM_EmployeeAllocationController(){
    	emplID = ApexPages.currentPage().getParameters().get('id'); 
    	monthLabel = RM_Tools.GetMonthsList(startMonth, endMonth);
    	allocationData = getAllocationData();
    	string unit = RM_OtherService.getUserDefaultUnit(UserInfo.getUserId());
    	if (unit == 'FTE')
    		 bVisHr = false;
    	else
    		 bVisHr = true;
    	 lstPost = RM_AssignmentsService.getAvailabilityByEmployee(emplID, RM_Tools.GetMonthsList(-4, 0));
    }
	public list<allocationReview> getAllocation(){
        return allocationData;
    }
    public allocationReview UtilizationRate{get;set;}
  
    
    
    
    public class allocationReview{
        public string project_rid{get; private set;}    
        public string project_id {get; private set;} 
        public string client_id {get; private set;} 
       
        public string status {get; private set;}
        public string assign_type {get; set;}
        public string startDate {get; private set;} 
        public string endDate {get; private set;}  
        public Boolean bAssign{get; private set;}
       
        public list<string> assHr{get;private set;}
        public list<string> assFTE{get;private set;}
        public list<string> actHr{get;private set;}
        public list<string> actFTE{get;private set;}       
        public integer iStart{get; private set;}
        public integer iEnd{get; private set;}
       
        public allocationReview(WFM_Project__c proj, integer startMonth, integer endMonth){
                
             project_rid = proj.id; 
             project_id =proj.name;          
             client_id = proj.Client_ID__c;
             bAssign = false;
             
             iStart = startMonth;
             iEnd = endMonth;
           
             init(startMonth, endMonth);
        }
         public allocationReview(String p,integer startMonth, integer endMonth){
                
            project_id = p;
            
             iStart = startMonth;
             iEnd = endMonth;
           
             init(startMonth, endMonth);
        }
        
        
        private void init(integer startMonth, integer endMonth){
             //initialize month
            Integer iMonth = endMonth-startMonth + 1;
            assHr = new string[iMonth];
            assFTE = new string[iMonth];
            actHr = new string[iMonth];
            actFTE = new string[iMonth];
           
            for (Integer i = 0; i < iMonth; i++) {
                assHr[i] = ''; 
                assFTE[i]='';
                actHr[i] = ''; 
                actFTE[i]='';
                 
            }
        }        
    }
     private list<allocationReview> getAllocationData(){ 
        system.debug('------getAllocationData------');
        list<allocationReview> allos = new list<allocationReview>();
        Set<ID> projectRIDS = RM_ProjectService.GetProjectSetByEmplRIDYearMonth(emplID, startMonth, EndMonth);
        list<WFM_Project__c> projList = RM_ProjectService.GetProjectListBySetProject(projectRIDS);
        system.debug('-----projList------'+projList); 
        map<ID, AggregateResult> mapHist = getMapWorkHistory(projectRIDS, emplID);
        system.debug('-----mapHist------'+mapHist); 
        map<string, AggregateResult> mapWorkFTE = getMapWorkFTE(projectRIDS, emplID, startMonth, EndMonth);
        system.debug('-----mapWorkFTE------'+mapWorkFTE);
        map<string, AggregateResult> mapAssignedFTE = getMapAssignFTE(projectRIDS, emplID, startMonth, EndMonth);
        system.debug('-----mapAssignedFTE------'+mapAssignedFTE);
        Decimal FTEEq=0;
                       
        UtilizationRate = new allocationReview('Utilization Rate', startMonth, EndMonth);
       
        Map<String,sObject> Gross=new Map<String,sObject>();
        
       
        Gross = RM_Tools.GetMapfromListObject('Year_Month__c', RM_AssignmentsService.GetAvailabilityByEmployee(emplID,RM_Tools.GetMonthsList(startMonth,endMonth)));
         system.debug('----Gross----'+Gross);            
        for (WFM_Project__c e: projList){       
                        
            allocationReview pA;                
            pA = new allocationReview(e, startMonth, EndMonth);                     
            pA = getAssignmentinAR(pA, mapAssignedFTE, 'Confirmed');
            if (pA.bAssign)   
                pA = getHistoryAR(pA,mapWorkFTE);
            else{
                pA = getAssignmentinAR(pA, mapAssignedFTE, 'Proposed');
                if (pA.bAssign){   
                    pA = getHistoryAR(pA,mapWorkFTE);
                    system.debug('-----pA2------'+pA);}
                else{
                    AggregateResult ar = mapHist.get(e.id);
                    
                    if (ar!= null){
                        system.debug('-----ar------'+RM_Tools.GetLastDayDatefromYearMonth((string)ar.get('endMonth'))); 
                        pA.startDate = RM_Tools.GetStringfromDate(RM_Tools.GetDatefromYearMonth((string)ar.get('startMonth')), 'mm/dd/yyyy');                   
                        pA.endDate = RM_Tools.GetStringfromDate(RM_Tools.GetLastDayDatefromYearMonth((string)ar.get('endMonth')), 'mm/dd/yyyy');
                        system.debug('-----pA------'+pA); 
                    }
                    pA = getHistoryAR(pA,mapWorkFTE);
                    system.debug('-----pA1------'+pA);
                }                    
            }            
                            
            allos.add(pA);          
        }  
        integer i = 0; 
        for(string m: monthlabel){
            	
          if(gross.containsKey(m)){
              WFM_Employee_Availability__c we =(WFM_Employee_Availability__c)gross.get(m);
              if (we != null){
              
	              UtilizationRate.AssFTE[i]=string.valueOf((we.Utilization__c * 100).setscale(0)) + '%';    
	              UtilizationRate.Asshr[i]=string.valueOf((we.Utilization__c * 100).setscale(0)) + '%'; 
	              
	            
	              UtilizationRate.ActHr[i]=string.valueOf((we.Actual_Utilization__c * 100).setscale(0)) + '%';    
	              UtilizationRate.ActFTE[i]=string.valueOf((we.Actual_Utilization__c * 100).setscale(0)) + '%';     
	              system.debug('-----we-----' + we); 	
	              system.debug('-----i-----' + i);    
	              system.debug('-----UtilizationRate.AssHrssss-----'+UtilizationRate.AssFTE[i]+UtilizationRate.Asshr[i]+we); 
              }
           } 
           i++;                  
        }         
        //allos.add(UtilizationRate);        
        system.debug('-----UtilizationRate------'+UtilizationRate+allos);        
        return allos;
    }
    private map<ID, AggregateResult> getMapWorkHistory(Set<ID> projectRIDs, string employeeRID){
        map<ID, AggregateResult> mapHist = new map<ID, AggregateResult>();
        
        for (AggregateResult ar :RM_EmployeeService.GetEmployeeWorkHistrySummarybyEmployeeProjectIDSet(projectRIDs, employeeRID)){
            mapHist.put((ID)ar.get('Project_id__c'), ar);
        }
        return mapHist;
    }
    private map<string, AggregateResult> getMapWorkFTE(Set<ID> projectRIDs, string employeeRID, integer startMonth, integer endMonth){
         map<string, AggregateResult> mapWorkFTE = new map<string, AggregateResult>();
         for (AggregateResult ar :RM_EmployeeService.GetSumEmployeeWorkHistByEmployeeProjectRIDsYearMonth(projectRIDs,  employeeRID,  startMonth,  endMonth)){
            mapWorkFTE.put((string)ar.get('Project_ID__c') + (string)ar.get('year_month__c'), ar);
         }
         return mapWorkFTE;
    }
    private map<string, AggregateResult> getMapAssignFTE(Set<ID> projectRIDs, string employeeRID, integer startMonth, integer endMonth){
        map<string, AggregateResult> mapAssign = new map<string, AggregateResult>();
        for (AggregateResult ar :RM_AssignmentsService.GetEmployeeAllocationSummaryByEmployeeProjectRIDsYearMonth(projectRIDs,  employeeRID,  startMonth,  endMonth)){
            mapAssign.put((string)ar.get('Project_ID__c') + (string)ar.get('year_month__c') + (string)ar.get('status__c'), ar);
         }
        return mapAssign;
    }
     private allocationReview getAssignmentinAR(allocationReview p, map<string, AggregateResult> mapAssignedFTE, string status ){
        integer iIndex = 0;
        for (integer i = p.iStart; i<=p.iEnd; i++){
            string mapKey = p.project_rid + RM_Tools.GetYearMonth(i)+status;
            system.debug('-----mapKey------'+mapKey); 
            AggregateResult arAssigned =  mapAssignedFTE.get(mapKey);   
            system.debug('-----arAssigned------'+arAssigned); 
            if (arAssigned != null){
                p.bAssign = true;
                decimal FTE = 0.0;
                decimal Hr = 0.0;
                if (arAssigned.get('assignFTE') != null )
                    FTE = (decimal)arAssigned.get('assignFTE');
                    
                if (arAssigned.get('assignHour') != null )
                    Hr = (decimal)arAssigned.get('assignHour');
               
                
                p.assFTE[iIndex] = string.valueOf(FTE.divide(1, 2));
                p.assHr[iIndex] = string.valueOf(Hr.divide(1, 2));
                p.startDate = RM_Tools.GetStringfromDate((Date)arAssigned.get('startDate'), 'mm/dd/yyyy');
                p.endDate = RM_AssignmentsService.getAllocationEndDate((Date)arAssigned.get('endDate'), (Date)arAssigned.get('lastAssignDate'));
                //p.endDate = RM_Tools.GetStringfromDate((Date)arAssigned.get('endDate'), 'mm/dd/yyyy');
                p.status = (string)arAssigned.get('Status__c');
                p.assign_type = (string)arAssigned.get('type__c');
                
            }
            iIndex++;   
        }   
        
        return p;
    }
    private static  allocationReview getHistoryAR(allocationReview p, map<string, AggregateResult> mapWorkFTE){
        integer iIndex = 0;
        for (integer i = p.iStart; i<=0; i++){
            string mapKey = p.project_rid + RM_Tools.GetYearMonth(i);
            AggregateResult histAr =  mapWorkFTE.get(mapKey);   
            if (histAr != null){
                p.actHr[iIndex] = string.valueOf(((decimal)histAr.get('totalHr')).divide(1,1));
                p.actFTE[iIndex] = string.valueOf(((decimal)histAr.get('totalFTE')).divide(1,2));
               // UtilizationRate.actHr[iIndex]+=p.actHr[iIndex] ;
               // UtilizationRate.actFTE[iIndex]+=p.actFTE[iIndex] ;
            }
            iIndex++;           
        
        }
    return p;
    }
}