public class AA_RevenueRateController {

    private final List < Business_Unit__c > businessunits;
    private final List < Function_Code__c > functions;
    private final List < Country__c > countries;
    private String id3Digit;
    
    public List < Revenue_Allocation_Rate__c > result {
        get;
        set;
    }
    public String selectedBusinessUnit {
        get;
        set;
    }
    public String selectedFunction {
        get;
        set;
    }
    public String selectedCountry {
        get;
        set;
    }
    public String selectedActiveFlag {
        get;
        set;
    }
    public String selectedYear {
        get;
        set;
    }
    public List < Integer > year {
        get;
        set;
    }
    public String functionId{get; set;}
    public String scheduledYear{get; set;}

    
    public List < BUF_Code__c > bufcodeDetails = new List < BUF_Code__c > ();

    /* Constructor to initalize Business Unit Codes and Function Codes */
    public AA_RevenueRateController () {
        businessunits = AA_RevenueAllocationService.getBusinessUnitCodes();
        functions = AA_RevenueAllocationService.getFunctionCodes();
        countries = AA_RevenueAllocationService.getCountries();
        getresult();
        getyear();             
        /* Get object Id */
        //Revenue_Allocation_Rate__c revId = AA_RevenueAllocationService.getRarId();
        //id3Digit= String.valueOf(revId.Id).substring(0, 3);
    }

    /* Populate Year in Select List */
    public List < SelectOption > getyearSelectoption() {
            List < SelectOption > yearselectoption = new List < SelectOption > ();
            List < AggregateResult > yearList = AA_RevenueAllocationService.getValidYears();
            for (AggregateResult c: yearList)
            for (Integer i = 1999; i<  Date.Today().Year()+ 20;i++)
                yearselectoption.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
            return yearselectoption;
        }
        /* Populate Business Unit Codes in Select List */
    public List < SelectOption > getbuSelectoption() {
            List < SelectOption > buselectoption = new List < SelectOption > ();
            buselectoption.add(new SelectOption('', '- All Business Units -'));
            for (Business_Unit__c entry: businessunits) {
                buselectoption.add(new SelectOption(entry.Id, entry.Name));
            }
            return buselectoption;
        }
        /* Populate Function Codes in Select List */
    public List < SelectOption > getfcSelectoption() {
        List < SelectOption > fcselectoption = new List < SelectOption > ();
        fcselectoption.add(new SelectOption('', '- All Functions -'));
        for (Function_Code__c entry: functions) {
            fcselectoption.add(new SelectOption(entry.Id, entry.Name));
        }
        return fcselectoption;
    }

    /* Populate Countries in Select List */
    public List < SelectOption > getcountrySelectoption() {
            List < SelectOption > countryselectoption = new List < SelectOption > ();
            countryselectoption.add(new SelectOption('', '- All Countries -'));
            for (Country__c entry: countries) {
                countryselectoption.add(new SelectOption(entry.Id, entry.Name));
            }
            return countryselectoption;
        }
        /* Populate Active States in Select List */
    public List < SelectOption > getisactiveSelectoption() {
        List < SelectOption > activeselectoption = new List < SelectOption > ();
        activeselectoption.add(new SelectOption('A', '- Both -'));
        activeselectoption.add(new SelectOption('Y', 'Yes'));
        activeselectoption.add(new SelectOption('N', 'No'));
        return activeselectoption;
    }

     public PageReference goToRateDetail() {
         PageReference pageRef = Page.AA_Edit_Revenue_Allocation_Rate;
         pageRef.getParameters().put('functionId',functionId);
         pageRef.getParameters().put('scheduledYear',selectedYear);
         return pageRef;
    }
    public PageReference create() {
         PageReference pageRef = Page.AA_New_Revenue_Allocation_Rate;
         return pageRef;
    }
    public PageReference upload() {
         PageReference pageRef = Page.AA_Revenue_Allocation_Rate_Upload;
         return pageRef;
    }
    public Pagereference submit() {
        result = new List < Revenue_Allocation_Rate__c > ();
        String year = selectedYear;
        String bucode = selectedBusinessUnit;
        String functioncode = selectedFunction;
        String country = selectedCountry;
        String active = selectedActiveFlag;
        result  = AA_RevenueAllocationService.getRecordsByFilter(year, bucode, functioncode, country, active);
        return null;
    }

    public List < Integer > getyear() {
        List < Cost_Rate__c > yearList;
        List < Integer > year = new List < Integer > ();
        if (selectedYear == null) {
            Integer tempYear = 1999;
            for (Integer i = 0; i < 16; i++) {
                year.add(tempYear);
                tempYear++;
            }
        } else {
            yearList = [SELECT Schedule_Year__c FROM Cost_Rate__c WHERE Schedule_Year__c = : selectedYear ORDER BY Schedule_Year__c];
            Integer tempYear = Integer.valueOf(yearList[0].Schedule_Year__c);
            for (Integer i = 0; i < 16; i++) {
                year.add(tempYear);
                tempYear++;
            }
        }
        
        return year;
    }
    
    public List < Revenue_Allocation_Rate__c > getresult() {
        return result;
    }


}