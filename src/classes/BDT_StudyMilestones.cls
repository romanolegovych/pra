public with sharing class BDT_StudyMilestones {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;}
	public Client_Project__c clientProject		{get;set;}
	public list<MileStoneDefinition__c> msdList {get;set;}
	public list<StudyMileStone__c> smsList		{get;set;}
	public List<SelectOption> studyOptions		{get;set;}
	public boolean editMode						{get;set;}
	public string studyId						{get;set;}
	public String projectId 					{get;set;}
	public Boolean showDeleted{
		get {if (showDeleted == null) {
				showDeleted = false;
			}
			return showDeleted;
		}
		set;
	}
	public string copyFromStudyId				{get;set;}
	public list<SelectOption> copyToStudyIDs	{get;set;}
	public string studiesToCopyTo			    {get;set;} 
		
	public class MileStoneWrapper implements comparable{
		public StudyMileStone__c sms		{get;set;}
		public boolean applicable			{get;set;}
		public integer sequence				{get;set;}
		public string sortfield				{get;set;}
		
		public MileStoneWrapper (Boolean pactiveMS, StudyMileStone__c psms, integer pSequence, string pSortfield ) {
			applicable=pactiveMS;
			sms=psms;
			sequence = pSequence;
			sortfield = psortfield;
		}
		public Integer compareto(Object compareTo){
			MileStoneWrapper msw = (MileStoneWrapper)compareTo;
			if(sortfield==msw.sortfield) return 0;
			if(sortfield > msw.sortfield) return 1;
			return -1;
		}
	}
	
	public list<MileStoneWrapper> mileStoneTable {get;set;}
	
	public BDT_StudyMileStones(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Milestones');
		editMode = false;
		projectId = BDT_Utils.getPreference('SelectedProject');
				
		if(projectId!=null && projectId!=''){
			clientProject = BDT_Utils.getProject(projectId, showDeleted);
			buildStudyOptions();	
		}
	}
	
	public void addRow(){
		StudyMileStone__c sms = new StudyMileStone__c( Study__c = Id.ValueOf(studyId) );
		integer listSize = milestoneTable.size();
		
		MileStoneWrapper msw = new MileStoneWrapper(false, sms,listsize+1,''+(listsize+1) );
					
		mileStoneTable.add( msw );
	}
	
	public void showCopyMileStones(){
		editMode=true;		
	}
	
	public pagereference doCopy(){
		
		list<StudyMileStone__c> sourceList =[select id, name, MileStonedefinition__c, dueDate__c, study__c from studyMilestone__c where study__c = :id.valueOf(copyFromStudyId)];
		if(sourceList.isEmpty() ){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'The source study does not contain any milestones');
         	ApexPages.addMessage(msg);
         	return null;
		}
		
		list<StudyMileStone__c> targetList = new list<StudyMileStone__c>();
		
		list<StudyMileStone__c> check =[select id, study__c from StudyMileStone__c where study__c = :Id.valueOf(studiesToCopyTo) ];
		if( !check.isEmpty() ){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'The target study has milestones');
        	ApexPages.addMessage(msg);
		}
		else{
			for(StudyMileStone__c sms: sourceList){
				StudyMileStone__c smsCopy = new StudyMileStone__c();
				smsCopy.Study__c = Id.valueof(studiesToCopyTo);
				if(sms.MileStoneDefinition__c != null ){
					smsCopy.MileStoneDefinition__c = sms.MileStoneDefinition__c; 
				}
				smsCopy.Name = sms.Name;
				smsCopy.DueDate__c = sms.DueDate__c;
				targetList.add(smsCopy);
			}
		}		
		
			
		if(ApexPages.hasMessages()){
			return null;
		}else {
			insert targetList;
			editMode=false;
			return new PageReference(System.page.bdt_StudyMilestones.getURL() );
		} 
	}
	
	public void buildCopyToList(){ 
		List<Study__c> stl = [select id, code__c from study__c where bdtdeleted__c = false and Project__c = :clientProject.id and id!=:id.valueof(copyFromStudyId) order by code__c];
		
		copyToStudyIDs = new list<SelectOption>();
		for(Study__c s: stl){
			SelectOption so = new SelectOption(s.Id, s.Code__c);
			copyToStudyIDs.add(so); 
		}
	}
	
	public PageReference cancel(){
		editMode=false;
		
		return new PageReference(System.page.bdt_StudyMilestones.getURL() ); 
	}
	
	public PageReference save(){
		list<StudyMileStone__c> smsToSave = new list<StudyMileStone__c>();
		list<StudyMileStone__c> smsToDelete = new list<StudyMileStone__c>();
		
		for(MileStoneWrapper msw: mileStoneTable){
			if(msw.applicable && msw.sms.DueDate__c == null ){
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Please enter a date for the due date');
         		ApexPages.addMessage(msg);
			}else{
				if(msw.applicable){
					smsToSave.add ( msw.sms );
				}else{
					if(msw.sms.id!= null ){
						smsToDelete.add(msw.sms);	
					}
				}
			}
		}
		
		if(apexPages.hasMessages() ){
			return null;
		}else{
			if(!smsToSave.isEmpty() ){
				upsert smsToSave;
			}
			if(!smsToDelete.isEmpty() ){
				delete smsToDelete;
			}
			buildMileStoneWrapper(); 
			PageReference ref  = new PageReference(System.page.bdt_StudyMilestones.getURL() );
			return ref;
		}
	}
		
	public void buildMileStoneWrapper(){
		milestoneTable = new List<MileStoneWrapper>();
		
		map<string,StudyMileStone__c> smsMap = new map<string,StudyMileStone__c>();
		
		if(studyId!='0'){
			list<studyMileStone__c> smsList = [select id, name, dueDate__c, MileStoneDefinition__c, MileStoneDefinition__r.SequenceNumber__c, study__c 
												from studyMileStone__c where study__c = :studyId];
			
			for(Studymilestone__c sms: smslist){
				if(sms.MileStoneDefinition__r.Id != null ){
					smsMap.put(sms.MileStoneDefinition__r.Id, sms);
				}
			}		
			msdList = [Select m.Name, m.Id, m.Category__c, m.BDTDeleted__c, m.SequenceNumber__c  
						From MileStoneDefinition__c m  where m.BDTDeleted__c = false
						order by m.SequenceNumber__c];
			
			for(MileStoneDefinition__c msd: msdList ){
				if( smsMap.get(msd.id) == null ){
					StudyMileStone__c tmpSMS = new StudyMileStone__c(MileStoneDefinition__c = msd.id, study__c=studyId, Name= msd.Name);
					string sortfield = string.valueOf(Date.newInstance(2200, 1, 1))+BDT_Utils.lPad(''+integer.valueOf(msd.SequenceNumber__c), 4, '0');
					milestoneTable.add( new MileStoneWrapper( false, tmpSMS, Integer.valueOf(msd.SequenceNumber__c ), sortfield ));
				}else{
					StudyMileStone__c tmpSMS = smsMap.get(msd.Id);
					tmpSMS.Name = msd.Name;
					string sortfield = string.valueOf(tmpSMS.DueDate__c)+BDT_Utils.lpad(''+integer.valueOf(msd.SequenceNumber__c), 4,'0'); 
					milestoneTable.add(new Milestonewrapper(true, tmpSMS,Integer.valueOf(msd.SequenceNumber__c ), sortfield  ));
				}
			}
			integer i=1;
			for(StudyMileStone__c sms: smsList){
				if(sms.MileStoneDefinition__r.Id == null){
					string sortfield = string.valueOf(sms.DueDate__c)+BDT_Utils.lpad(''+msdList.size()+i, 4,'0'); 
					milestoneTable.add(new Milestonewrapper(true, sms,msdList.size()+i,sortfield  ));
					i++;
				}
			}
			milestonetable.sort();
		}
	} 
	
	public void buildStudyOptions(){
		studyOptions = new List<SelectOption>();
		list<Study__c> studyList =  [select id, code__c from study__c where bdtdeleted__c = false and Project__c = :clientProject.Id ];
		studylist.sort();
		studyOptions.add(new SelectOption('0','Select a study'));
		
		for(Study__c st: studyList){
			studyOptions.add(New SelectOption(st.Id, st.Code__c ));	
		}
	}
}