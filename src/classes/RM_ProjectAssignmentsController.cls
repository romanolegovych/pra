public with sharing class RM_ProjectAssignmentsController {
	public string projectID{get; private set;}
	public WFM_Project__c project{get; private set;}
	//public string protocol{get; private set;}
	//public string theraArea{get; private set;}
   // public string phase{get; private set;}
    public Boolean bBid{get;private set;}
    public string assignType{get; private set;}
   
    public String searchOn{get;set;} 
    public string selSearch{get;set;}  
    public String strSearchText{get;set;} 
    public String searchTextStyle{get;set;}
    public string searchObject{get; set;}
    public string searchfield{get; set;}
    public string additionalfield{get; set;}
    public string additionalfilter{get; set;}
    //public list<string>lActive{get; private set;}
    public set<string> stManagedBU{get;set;}
     
    public String copyName{get; set;}
    public String[] selEmployeeName{ get; set; }
   
   // public String selRole{get; set;}
   // public String bufCode{get; set;}
   // public String selBuf{get; set;}
   // public String selCountry{get; set;}
   // public Boolean bAssignStatus{get;set;} 
   // public String assigndValue{get; set;}
  //  public string assStartDate{get; set;}
  //  public string assEndDate{get; set;}
  //  public string RequestID{get; set;}
  //  public String strNote{get; set;}
    public String selectedUnit{get;set;}
    public list<ProjectBatchAssignment> ProAssigns{get;set;}
    public Integer iCursor{get; private set;}
    
    public boolean bAssigninHour{get;set;}
    public string SelectedAdd{get; set;}
    //control view
    public boolean bInsert{get; private set;}
    public boolean bEdit{get; private set;}
    public boolean bCopy{get; private set;}
    public boolean bAdd{get; private set;}
    public boolean bResourceList{get; private set;}
    public string resouceListSize{get; private set;}
    public transient boolean bShowValidateMessage{get; private set;}
    public boolean bRquireReqID{get; set;}
    public boolean bSystemAdmin{get; set;}  
  
    public transient string addMessage{get; private set;}
 
    //Edit portion
    public string newValue{get; set;}
    public string assNewStartDate{get; set;}
    public string assNewEndDate{get; set;}
    
    private list<selectoption> lstEmployeeOptions = new list<selectoption>();
    
    static final string EMPLOYEE = 'Employee';
    static final string MANAGER = 'Manager';
    static final string COPY = 'Copy/Paste';
    static final string INIT_AUTOCOMPLETE_NAME = 'Enter Last Name';
    static final string INIT_AUTOCOMPLETE_TYPE = 'WaterMarkedTextBox';
    
	public RM_ProjectAssignmentsController(){
		projectID = ApexPages.currentPage().getParameters().get('ID');
		//getProjectfromID();
		List<WFM_Project__c> lstProj = RM_ProjectService.GetProjectbyrID(projectID);
		if (lstProj.size() > 0)
			project = lstProj[0]; 
		assignType = RM_Tools.GetAssignmentType(project.Status_Desc__c);
		system.debug('----assignType----' + assignType);
		stManagedBU = RM_OtherService.GetUserGroupBU(UserInfo.getUserId());   	
      	system.debug('----stManagedBU----' + stManagedBU);
    	bSystemAdmin = RM_OtherService.IsRMAdmin(UserInfo.getUserId());
    	system.debug('----bSystemAdmin----' + bSystemAdmin);
    	if (!bSystemAdmin && stManagedBU == null)
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please contact your system administrator to assign you to a group'));
    			 
		//need error for no group
		searchOn='Employee';
     	setSearchType(stManagedBU); 
     	bInsert = true;
     	bEdit = false;
     	bCopy = false;
     	
     	bResourceList = false;
     	bShowValidateMessage = false;
      	bRquireReqID = RM_OtherService.IsRquireReqID(UserInfo.getUserId());
     	iCursor = 0;
     	//assStartDate = RM_Tools.GetStringfromDate(Date.today(), 'mm/dd/yyyy');
     //	if (project.Project_End_Date__c >= Date.today())
     //		assEndDate = RM_Tools.GetStringfromDate(project.Project_End_Date__c, 'mm/dd/yyyy');
        ProAssigns = new List<ProjectBatchAssignment>();
        selEmployeeName = new List<string>();
        lstEmployeeOptions = getOptionEmployeeList();
        string unit = RM_OtherService.getUserDefaultUnit(UserInfo.getUserId());
        if (unit == 'FTE')
        	bAssigninHour = false;
        else
        	bAssigninHour = true;
	}
	
	public List<SelectOption> getSearchOnItems(){
        List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption(EMPLOYEE,EMPLOYEE));
            options.add(new SelectOption(MANAGER,MANAGER)); 
            options.add(new SelectOption(COPY,COPY));            
       return options;
    }
    
    public  PageReference searchType(){
    		//clean multi select employee list
    		lstEmployeeOptions.clear();
            setSearchType( stManagedBU);
        return null;       
        
    }
    
    public list<selectoption> getEmployeeNameList(){
    	bShowValidateMessage = false;
        return lstEmployeeOptions;
    }
    
    public List<SelectOption> getRoles(){     
        
        transient List<SelectOption> options = RM_LookUpDataAccess.getGroupJobClassDesc(true);
        return options;
    }
    
    public List<SelectOption> getCountry(){        
       
        transient List<SelectOption> options = RM_LookUpDataAccess.getCountry(false); 
        return options;
    }
    public List<SelectOption> getBUCode(){
    	transient List<SelectOption> options = RM_LookUpDataAccess.getAllLocBU(false); 
        return options;
    }
    
    public PageReference enableInsert(){
    	bInsert = true;
    	bEdit = false;
    	return null;
    }
    
    public PageReference enableEdit(){
    	bInsert = false;
    	bEdit = true;
    	
    	return null;
    }
    
    public PageReference enableViewOnly(){
    	bInsert = false;
    	bEdit = false;
    	return null;
    }
    
   
    public void addAssignment(){    	
    	set<string> uniqAssigns = new set<string>();
    	//add employee to the grid
    	if (ProAssigns.size() > 0){    		
    		for(ProjectBatchAssignment oneA : ProAssigns){
    			uniqAssigns.add(oneA.getUniqueKey(project.name));    			
    		}
    		   		
    	}
    	system.debug('----uniqAssigns----' + uniqAssigns); 
    	system.debug('----selEmployeeName----' + selEmployeeName);
    	Set<string> setEmplWithoutEmptyValue = RM_Tools.getSetwithougEmptyValuefromMultiSel(selEmployeeName); 
    	system.debug('----setEmplWithoutEmptyValue----' + setEmplWithoutEmptyValue);
    	List<Employee_Details__c> lstEmpl = RM_EmployeeService.GetEmployeeDetailByEmployeeRIDs(setEmplWithoutEmptyValue);
    	system.debug('----lstEmpl----' + lstEmpl);
    	for(Employee_Details__c e: lstEmpl){   		
    		
    		ProjectBatchAssignment one = new ProjectBatchAssignment(e, project.name,RM_Tools.GetStringfromDate(Date.today(), 'mm/dd/yyyy') );
    		//need to get from interface
    		//one.status = RM_Tools.GetAssignmentStatusString(bAssignStatus);
    		
    		//one.startDate = RM_Tools.GetStringfromDate(Date.today(), 'mm/dd/yyyy');
    		//if (assEndDate != '')
    		//	one.EndDate = assEndDate;
    		if (project.Project_End_Date__c >= Date.today())
	 			one.EndDate = RM_Tools.GetStringfromDate(project.Project_End_Date__c, 'mm/dd/yyyy');
	 		one.sequenceID = iCursor;
	 		//if (RequestID != '')
	 		//	one.reqID = RequestID;
	 		//if (strNote!= '')
	 		//	one.note = strNote;
	 		//check if unique key is not there
	 		system.debug('----uniqAssigns----' + uniqAssigns); 
	 		if (!uniqAssigns.contains(one.UniqueKey)){
    			ProAssigns.add(one);
    			uniqAssigns.add(one.UniqueKey);
    			iCursor++;
	 		}
    	}  	
    }
    
    public void getEmployee(){
    	
		system.debug('----strSearchText----' + strSearchText);
    	lstEmployeeOptions = getOptionEmployeeList();
    	system.debug('----lstEmployeeOptions----' + lstEmployeeOptions);
    	//setSearchType(lActive, stManagedBU); 
    	 searchTextStyle=INIT_AUTOCOMPLETE_TYPE;        
         strSearchText = INIT_AUTOCOMPLETE_NAME;
         selSearch = '';
    	
    }
    public void DisableAdd(){
    	lstEmployeeOptions.clear();
    }  
    public PageReference validateCopy(){
    	system.debug('----copyName----' + copyName);
    	lstEmployeeOptions = getOptionEmployeeList();
    	return null;
    }
    
    public list<ProjectBatchAssignment> getAddAssignments(){
    	return ProAssigns;
    }
    
    public void remove(){
    	if (ProAssigns.size() > 0){
    		//remove from list, but need to find out which one
    		Integer iIndex = 0;
    		for (ProjectBatchAssignment p : ProAssigns){
    			if (p.sequenceID == Integer.valueOf(SelectedAdd)){
					ProAssigns.Remove(iIndex);
					break;
    			}
				iIndex++;					
    		}
    	}
    }
    
    public PageReference cancelAssignment(){
    	if (ProAssigns.size() > 0){
    		ProAssigns.Clear();
    	}
    	ApexPages.getMessages().clear();
    	return null;
    }
    
    public PageReference insertAssignment(){
    	//check allocation/assignment
    	//if assignment in the same month range, then it would not allowed
    	//verify the availability, may be need to insert availability
    	//insert allocation
    	//insert assignmnet
    	list<ProjectBatchAssignment> lstValidBatch =getValidBachAssignment();
    	system.debug('---------lstValidBatch---------' + lstValidBatch);
    	set<string> stPrimaryKey = new set<string>();
    	for(ProjectBatchAssignment one : lstValidBatch)
    		stPrimaryKey.add(one.getUniqueKey(project.name));
    		
    	map<string, sObject> mapRequest = insertRequest(lstValidBatch);
    	system.debug('---------mapRequest---------' + mapRequest);
    	map<string, sObject> mapAvail =  insertAvalability(lstValidBatch);
    	system.debug('---------mapAvail---------' + mapAvail);
    	//insert/update allocation
    	map<string, sObject> mapAlloc =  insertAllocation(lstValidBatch, mapRequest);
    	system.debug('---------mapAlloc---------' + mapAlloc);
    	//now need to do assignment
    	//upsert
    	insertBatchAssignment(lstValidBatch,mapAlloc,mapAvail);
    	return null;
    }
    
    private void insertBatchAssignment(list<ProjectBatchAssignment> lstValidBach, map<string, sObject> mapAlloc, map<string, sObject> mapAvail){
    	//upsert don't work for some reason
    	list<WFM_Employee_Assignment__c> lstAssign = new list<WFM_Employee_Assignment__c>();
    	
    	
    	for(ProjectBatchAssignment one : lstValidBach){
    		string allocID = '';
    		if (mapAlloc.containsKey(one.getUniqueKey(project.name))){ 
				WFM_employee_Allocations__c aAlloc = (WFM_employee_Allocations__c)mapAlloc.get(one.getUniqueKey(project.name));    				
				allocID = aAlloc.id; 
			}
			system.debug('---------allocID---------' + allocID);
    		list<string> lstMonth = RM_Tools.getMonthsListFromDate(RM_Tools.GetDateFromString(one.startDate, 'MM/DD/YYYY'), RM_Tools.GetDateFromString(one.endDate, 'MM/DD/YYYY'));
    		for(string aMonth :lstMonth){
    			string availID = '';
    			if (mapAvail.containsKey(one.employeeID+':'+aMonth)){ 
    				WFM_Employee_Availability__c aAvail = (WFM_Employee_Availability__c)mapAvail.get(one.employeeID+':'+aMonth);    				
    				availID = aAvail.id; 
    			}
    			system.debug('---------availID---------' + availID);
    			if (availID != '' && allocID != ''){
    				system.debug('---------Insert availID---------' + availID);
    		  		if (one.assignValue != ''){                  	
                    	system.debug('----assignType----' + assignType);
    		  			WFM_Employee_Assignment__c aAssign = new WFM_Employee_Assignment__c(Year_Month__c=aMonth, Assignment_Unique_Key__c=allocID+availID, Availability_FK__c= availID,Allocation_Key__c=allocID, 
                                          type__c = assignType,  Status__c=one.status, EMPLOYEE_ID__c =one.employeeRID);
                        if (bAssigninHour) 
                        	aAssign.Assigned_unit__c = 'Hours';
                        else
                        	aAssign.Assigned_unit__c = 'FTE';   
                        aAssign.Assigned_value__c = Decimal.valueOf(one.assignValue);
                        //need to get partial month  
                        Date endDate = RM_Tools.getDateFromString(one.endDate, 'mm/dd/yyyy'); 
                        aAssign.Act_End_Date__c = RM_Tools.GetLastDayDatefromYearMonth(aMonth);
                        if (endDate.monthsBetween(RM_Tools.GetDatefromYearMonth(aMonth)) == 0){
                        	aAssign.Act_End_Date__c = endDate;
                        }
                        system.debug('----aAssign----' + aAssign);
                        lstAssign.add(aAssign);  
    		  		}
    			} 
    		}
    	}
    	try{
    		system.debug('----lstAssign----' + lstAssign);
    		if (lstAssign.size() > 0)
    			RM_AssignmentsService.actionOnListObject(lstAssign, 'Upsert');
    	}
    	catch (DmlException e){
    		//need error message
    		for (Integer i = 0; i < e.getNumDml(); i++) {		        
		        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i))); 
		    }
    	}
    }
    
    private list<ProjectBatchAssignment> getValidBachAssignment(){
    	//There is no allocation and assignment not in the start date and end date range
    	
    	list<ProjectBatchAssignment> lstValidBach = new list<ProjectBatchAssignment>();
    	
    	list<ProjectBatchAssignment> lstBadBatch = new list<ProjectBatchAssignment>();
    	lstBadBatch = getBadRole(lstBadBatch);
    	//lstBadBatch = getBadBufCode(lstBadBatch);
    	lstBadBatch = getBadStartEndDate(lstBadBatch);
    	lstBadBatch = getBadAssignValue(lstBadBatch);
    	
    	
    	set<string> stPrimaryKey = new set<string>();
    	for(ProjectBatchAssignment one : ProAssigns)
    		stPrimaryKey.add(one.getUniqueKey(project.name));
    	//cannot insert
    	List<WFM_employee_Allocations__c> lstExitAllocation = RM_AssignmentsService.GetAllocationByPrimaryKeySet(stPrimaryKey);
     	//remove any record not in lstExitAllocation from ProAssigns
    	//create valid batch not in the list
    	set<string> stBadPrimaryKey = new set<string>();
    	for (WFM_employee_Allocations__c oneAllo : lstExitAllocation)
    		stBadPrimaryKey.add(oneAllo.Allocation_Unique_Key__c);
    	
    	for(integer i = ProAssigns.size() -1; i >= 0; i--){
    		if (!stBadPrimaryKey.contains(ProAssigns[i].getUniqueKey(project.name))){
    			//assignment format if we are not doing in front
				lstValidBach.add(ProAssigns[i]);
    		}
    		else{
    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Duplicate Assignment : '+ ProAssigns[i].getUniqueKey(project.name)));
    			lstBadBatch.add(ProAssigns[i]);
    		}
    	}
    	ProAssigns = lstBadBatch;
    	integer n= 0;
    	for (ProjectBatchAssignment a : ProAssigns){
    		a.sequenceID = n;
    		n++; 
    	}
    	return lstValidBach;
    }
    
    /*private list<ProjectBatchAssignment> getBadBufCode(list<ProjectBatchAssignment> lstBadBatch ){//good one in ProAssignd
    	//need to verify with country
    	set<string> stCountryBu = new set<string>();
    	for(ProjectBatchAssignment one : ProAssigns)
    		stCountryBu.add(one.country + ':' + one.buCode);
    	list<Country_BusinessUnit__c> lstCountryBufObj = RM_OtherService.validBUCountryCombo(stCountryBu);
    	set<string> stGoodCountryBuf = RM_Tools.GetSetfromListObject('Country_BU_ID__c', lstCountryBufObj);
    	
    	for(integer i = ProAssigns.size() -1; i >= 0; i--){
    		if (!stGoodCountryBuf.contains(ProAssigns[i].country + ':' + ProAssigns[i].buCode)){
    			lstBadBatch.add(ProAssigns[i]);    			
    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Incorrect Country and location combination: '+ ProAssigns[i].buCode));
    			ProAssigns.Remove(i);
    		}
    	}
    	return lstBadBatch;
    }*/
    
    private list<ProjectBatchAssignment> getBadStartEndDate(list<ProjectBatchAssignment> lstBadBatch ){//good one in ProAssignd
    	//validate start date and end date (end date cannot less than start date)
    	//list<ProjectBatchAssignment> lstBadBatch = new list<ProjectBatchAssignment>();
    	for(integer i = ProAssigns.size() -1; i >= 0; i--){
    		
    		if (RM_Tools.GetDateFromString(ProAssigns[i].endDate, 'mm/dd/yyyy') == null || RM_Tools.GetDateFromString(ProAssigns[i].startDate, 'mm/dd/yyyy') == null){
    			lstBadBatch.add(ProAssigns[i]);
	    			
	    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Incorrect date'));
	    		ProAssigns.Remove(i);
    		}
    		else{
    			
    			Date startA = RM_Tools.GetDateFromString(ProAssigns[i].startDate, 'mm/dd/yyyy');
    			Date endA = RM_Tools.GetDateFromString(ProAssigns[i].endDate, 'mm/dd/yyyy');
    			
	    		if (endA < startA){
	    			lstBadBatch.add(ProAssigns[i]);
	    			
	    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Incorrect end date: '+ ProAssigns[i].endDate));
	    			ProAssigns.Remove(i);
	    		}
	    		else if (endA.month() - startA.Month() > 539){
	    			lstBadBatch.add(ProAssigns[i]);
	    			
	    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Assignments can not exceed 10 years. Please correct the end date to continue :  '+ ProAssigns[i].endDate));
	    			ProAssigns.Remove(i);
	    		}
	    		
    		}
    		
    	}
    	return lstBadBatch;
    }
    
    private list<ProjectBatchAssignment> getBadAssignValue(list<ProjectBatchAssignment> lstBadBatch ){//good one in ProAssignd
    	//validate AssignedValue,it has be number and range
    	//list<ProjectBatchAssignment> lstBadBatch = new list<ProjectBatchAssignment>();
    	for(integer i = ProAssigns.size() -1; i >= 0; i--){   		
    		try{
	    		Decimal d = Decimal.valueOf(ProAssigns[i].assignValue);
	    		//need to set range
	    		//if 0<FTE<1 and 0<Hr<200	
	    		if (bAssigninHour){
	    			if (d < 0 || d > 9999){
	    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Assigned value should be between 0 and 9999 : '+ ProAssigns[i].assignValue));
	    				lstBadBatch.add(ProAssigns[i]);
	    				ProAssigns.Remove(i);
	    			}
	    		}    
	    		else{
	    			if (d < 0 || d > 1){
	    				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Assigned value should be between 0 and 1 : '+ ProAssigns[i].assignValue));
	    				lstBadBatch.add(ProAssigns[i]);
	    				ProAssigns.Remove(i);
	    			}
	    		}
    		}
    		catch (TypeException e){
    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Incorrect assigned value: '+ ProAssigns[i].assignValue));
    			lstBadBatch.add(ProAssigns[i]);
	    		ProAssigns.Remove(i);
    		}
    	}
    	return lstBadBatch;
    
    }
    
    private list<ProjectBatchAssignment> getBadRole(list<ProjectBatchAssignment> lstBadBatch ){//good one in ProAssignd
    	//validate AssignedValue,it has be number and range
    	//list<ProjectBatchAssignment> lstBadBatch = new list<ProjectBatchAssignment>();
    	for(integer i = ProAssigns.size() -1; i >= 0; i--){   		
    		
    		if (ProAssigns[i].role == null || ProAssigns[i].role.trim() == ''){ 
    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please select a role before continuing'));
    			lstBadBatch.add(ProAssigns[i]);
	    		ProAssigns.Remove(i);
    		
    		}
    	}
    	return lstBadBatch;    
    }    
    
    private Map<string, sObject> insertRequest(list<ProjectBatchAssignment> lstValidBach){
    	//find any request ID in there
    	
    	set<string> stRequestID = new set<string>();
    	for(ProjectBatchAssignment one : lstValidBach){
    		if (one.reqID != null && one.reqID != ''){
    			stRequestID.add(one.reqID);
    		}
    	}
    	
    	set<string> stExitRequestID = new set<string>();
    	for(WFM_Request__c r : RM_AssignmentsService.GetRequestBySetRequest(stRequestID)){    		
    			stExitRequestID.add(r.Request_ID__c);    		
    	}
    	
    	list<WFM_Request__c> lstNewRequest = new list<WFM_Request__c>();
    	for (string r : stRequestID){
    		if(!stExitRequestID.contains(r)){ // insert    			
    			lstNewRequest.add(new WFM_Request__c( Request_ID__c = r));
    		}  
    	}
    	try{
    	
    		RM_AssignmentsService.ActionOnListObject(lstNewRequest, 'Insert');
    	}
    	catch (DMLException e){
    		//need error message
    		for (Integer i = 0; i < e.getNumDml(); i++) {		        
		        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i))); 
		    }    		
    	} 
    	return RM_Tools.GetMapfromListObject('Request_ID__c', RM_AssignmentsService.GetRequestBySetRequest(stRequestID));
    }
    
    private map<string, sObject>  insertAvalability(list<ProjectBatchAssignment> lstValidBach){
    	boolean bInsert = false;
    	list<ID> lstEmployeeRID = new list<ID>();
    	Date maxDate = Date.Today();
    	for (ProjectBatchAssignment one : lstValidBach){
    		lstEmployeeRID.add(one.employeeRID);
    		if (RM_Tools.GetDateFromString(one.enddate, 'mm/dd/yyyy') > maxDate)
    			maxDate = RM_Tools.GetDateFromString(one.enddate, 'mm/dd/yyyy');
    	}
    	system.debug('---------maxDate---------' + maxDate);
    	system.debug('---------maxDateYearMonth---------' + RM_Tools.GetYearMonthFromDate(maxDate));
    	/*map<string, string> mapAvaMonth =  RM_AssignmentsService.GetMaxAvailabilityNeedMore(lstEmployeeRID, RM_Tools.GetYearMonthFromDate(maxDate));
   		system.debug('---------lstAva---------' + mapAvaMonth);
   		//get availability 
    	
    	system.debug('---------lstMonths---------' + lstMonths);
    	if (mapAvaMonth.size() == 0){
    		list<WFM_Employee_Availability__c> lst = RM_AssignmentsService.getAvailabilityIDByEmployeeList(lstEmployeeRID, lstMonths);
    		if (lst.size() == 0)
    			bInsert = true;
    	}
    	else
    		bInsert = true;
    	system.debug('---------bInsert---------' + bInsert);
	    if(bInsert) {*/
	    	list<string> lstMonths = RM_Tools.GetMonthsListFromDate(Date.today(), maxDate);
	    	try{ 
	    		
	     		RM_AssignmentsService.InsertAvailabilityByEmployees(lstEmployeeRID, RM_Tools.getMonthNumFromNow(RM_Tools.GetYearMonthFromDate(maxDate)));
	    	}
	     	catch (DMLException e){
	    		for (Integer i = 0; i < e.getNumDml(); i++) {		        
			        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i))); 
			    }
    		}	     	
	   // } 
    	return RM_Tools.getMapfromListObject('Availability_Unique_Key__c',RM_AssignmentsService.getAvailabilityIDByEmployeeList(lstEmployeeRID, lstMonths));
    }
    
    private map<string, sObject> insertAllocation(list<ProjectBatchAssignment> lstValidBach, map<string, sObject> mapRequest){
    	//need to find which one is insert 
    	 //insert request first
    	 
    	set<string> stPrimaryKey = new set<string>();
    	for(ProjectBatchAssignment one : lstValidBach)
    		stPrimaryKey.add(one.getUniqueKey(project.name));
    	system.debug('---------stPrimaryKey---------' + stPrimaryKey);
		Map<string,sObject > mapPrimaryExitAlloc = RM_Tools.getMapfromListObject('Allocation_Unique_Key__c', RM_AssignmentsService.GetAllocationByPrimaryKeySet(stPrimaryKey));
		system.debug('---------mapPrimaryExitAlloc---------' + mapPrimaryExitAlloc);
    	
    	List<WFM_employee_Allocations__c> lstNewAlloc = new List<WFM_employee_Allocations__c>();
    	List<WFM_employee_Allocations__c> lstUpdateAlloc = new List<WFM_employee_Allocations__c>();
    	for(ProjectBatchAssignment one :lstValidBach){
    		system.debug('---------one.getUniqueKey(project.name)---------' + one.getUniqueKey(project.name));
    		if (mapPrimaryExitAlloc.containsKey(one.getUniqueKey(project.name))){ // update
    			
    			WFM_employee_Allocations__c updateA = (WFM_employee_Allocations__c)mapPrimaryExitAlloc.get(one.getUniqueKey(project.name));
    			updateA.status__c = one.status;
    			updateA.Is_Lead__c = one.bLead;                
                updateA.Is_IEDR__c = one.bPSSV;                
                updateA.Is_Unblinded__c = one.bUnBL; 
                updateA.Allocation_End_Date__c = RM_Tools.getDateFromString(one.enddate, 'mm/dd/yyyy');
                if (one.reqID != null && one.reqID !=''){ 
                	if (mapRequest.containsKey(one.reqID)){
                		WFM_Request__c r = (WFM_Request__c)mapRequest.get(one.reqID);
                		updateA.Request__c = r.ID;
                	}
                }
                system.debug('---------Update---------' + updateA);
                lstUpdateAlloc.add(updateA);
    		}
    		else{ //insert
    			WFM_employee_Allocations__c newA = one.createNewAlloc(one.empl, project.name, project.id);
    			system.debug('---------one.bLead---------' + one.bLead);
    			newA.Is_Lead__c = one.bLead;                
                newA.Is_IEDR__c = one.bPSSV;                
                newA.Is_Unblinded__c = one.bUnBL;
                if (one.reqID != null && one.reqID !=''){
                	if (mapRequest.containsKey(one.reqID)){
                		WFM_Request__c r = (WFM_Request__c)mapRequest.get(one.reqID);
                		newA.Request__c = r.ID;
                	}
                }
                system.debug('---------Insert---------' + newA);
                lstNewAlloc.add(newA );
    		}
    	}
    	try{
	    	if (lstNewAlloc.size() > 0)
	    		RM_AssignmentsService.ActionOnListObject(lstNewAlloc, 'Insert');
	    		//insert lstNewAlloc;
	    	if (lstUpdateAlloc.size() > 0)
	    		RM_AssignmentsService.ActionOnListObject(lstUpdateAlloc, 'Update');
	    		//update lstUpdateAlloc;
	    }
    	catch (DMLException e){
    		//need error message
    		for (Integer i = 0; i < e.getNumDml(); i++) {		        
		        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, e.getDmlMessage(i))); 
		    }
    	}
    	//need to retrieve all allocation
    	return RM_Tools.getMapfromListObject('Allocation_Unique_Key__c', RM_AssignmentsService.getAllocationByPrimaryKeySet(stPrimaryKey));
    }
	
      private void setSearchType( set<string> stManagedBU ){
      	bShowValidateMessage = false;
        system.debug('-----searchOn---------' + searchOn); 
         system.debug('-----stManagedBU---------' + stManagedBU);
        string active = '';
               
        active = RM_OtherService.getActiveEmployeeStatusStr();
        system.debug('-----active---------' + active);
        string bu = '';
        if (stManagedBU != null && stManagedBU.size() > 0){
        	for (string b: stManagedBU){
        		bu += '\'' + b + '\',';
        	}
        	bu = bu.substring(0, bu.length()-1);
        }
        system.debug('---------bu---------' + bu);	
        searchTextStyle=INIT_AUTOCOMPLETE_TYPE;
        if (searchOn==EMPLOYEE){
             strSearchText = INIT_AUTOCOMPLETE_NAME;
             searchObject = 'Employee_Details__c';
             searchfield='FullName_NoAccent__c';           
             if (bSystemAdmin)
             	additionalfilter=' and Status__c in (' + active + ')';
             else
             	additionalfilter=' and Status__c in (' + active + ') and Business_Unit_Desc__c in (' + bu + ')';
            
             bCopy = false;
             copyName = '';
             resouceListSize = '1';
        }
        else if (searchOn==MANAGER){
             strSearchText = INIT_AUTOCOMPLETE_NAME;
             searchObject = 'Employee_Details__c';
             searchfield='FullName_NoAccent__c';             
             if (bSystemAdmin)
             	additionalfilter=' and Status__c in (' + active + ') and Manager__c = true ';
             else
             	additionalfilter=' and Status__c in (' + active + ') and Manager__c = true and Business_Unit_Desc__c in (' + bu + ')';
             
             bCopy = false;
             copyName = '';
        }
        else {
        	bCopy = true;
        	
        	resouceListSize = '10';
        }
    }
    
    private List<selectoption> getOptionEmployeeList(){
    	list<selectoption> options = new list<selectoption>();
    	
    	system.debug('---------selSearch---------' + selSearch);
    	system.debug('---------strSearchText---------' + strSearchText);
    	if (searchOn == EMPLOYEE){
    		if (selSearch != '' && selSearch != null){  
	    		options.add(new SelectOption(selSearch, strSearchText));
	    		selEmployeeName.add(selSearch);
    		}
    		//else
    		//	options.add(new SelectOption('',''));
    	}
    	else if (searchOn == MANAGER ){
    		//to get resource under this manager
    		system.debug('---------selSearch---------' + selSearch);
    		if (selSearch != '' && selSearch != null){
    			options = RM_EmployeeService.getResourceByManager(selSearch);
	    		
	    		if (options.size() < 10)
	    			resouceListSize = string.ValueOf(options.size());
	    		else
	    			resouceListSize = '10';
    		}
    		//else 
    		//	options.add(new SelectOption('',''));
    		
    	} 
    	else if (searchOn == COPY){// copy/paste
    		if (copyName != null&& copyName != ''){
    			addMessage = '';
    			List<String> parts = copyName.split('\\n');
				system.debug('---------parts---------' + parts);
				List<string> lstFullName = new List<string>();
				for (string s: parts){
					if (s.indexOf(',') < 0){
						
						
						list<string> nameParts = s.split('\\t');
						system.debug('---------nameParts---------' + nameParts);
						if (nameParts.size() > 1){ //tab
							string fullName = nameParts[0].trim()+', ' + nameParts[1].trim();
							lstFullName.add(fullName); 
						}
						
						
						else //space
						{
							string lastName = s.substringBefore(' ');
							string firstName = s.substringAfterLast(' ');
						
							string fullName = lastName+', ' + firstName;
						
							system.debug('---------fullName---------' + fullName);
							lstFullName.add(fullName);
						}
					}
					else
						lstFullName.add(s);
				}	
				List<Employee_Details__c> lstEmpl = RM_EmployeeService.getEmployeeDetailByEmployeeFullName(lstFullName, stManagedBU);
				Set<string> setEmpl = new Set<string>();
				for(Employee_Details__c e : lstEmpl){
					options.add(new SelectOption(e.id,e.full_name__c));
					setEmpl.add(e.full_name__c);
				}
    			//check if there is any record din't find
    			//options.add(new SelectOption(copyName,copyName));
    			string missing = '';
    			for(string n : lstFullName){
    				if (!setEmpl.contains(n))
    					missing += n + '; ';
    			}
    			if (missing.length() > 1){
    				addMessage = 'Cannot find resource or not a member of the business group for which your user account is allowed to manage assignments: ' + missing;
    				bShowValidateMessage = true;
    			}
    		}
    		//else
    		//	options.add(new SelectOption('',''));
    	}
    	return options;
    }
    
    
    public class ProjectBatchAssignment{
    	public integer sequenceID{get; private set;}        
        public string primaryString{get; private set;}
        public string employeeName{get; private set;}
        public string employeeID{get; private set;}
        
        Employee_Details__c empl{get; private set;}
        public string employeeRID{get; private set;}
        public string UniqueKey{get; private set;}
        public string role{get; set;}
        public boolean bLead{get; set;}
        public boolean bPSSV{get; set;}
        public boolean bUnBL{get; set;}
        public string country{get; set;}
        public string buCode{get; set;}
        public string status{get; set;}        
        public string reqID{get; set;}
        public string assignValue{get; set;}            
        public string startDate{get;  set;}
        public string endDate{get;  set;}     
        public string note{get; private set;}   
        
        public ProjectBatchAssignment(Employee_Details__c e, string projectName, string start){
        	empl = e;
           employeeName = e.full_name__c;
           employeeID = e.name;
           employeeRID = e.ID;
           role = e.job_class_desc__c;
           buCode = e.Business_Unit__c;
           country = e.country_name__r.name;
           startDate = start;		
           status = RM_Tools.getAssignmentStatusString(false); 
           UniqueKey = getUniqueKey(projectName);
        }
        
        public string getUniqueKey(string projectName){
        	return RM_AssignmentsService.getAllocationUniqueKey(employeeID, projectName,role, country, buCode, startDate);
        	
        }
        public WFM_employee_Allocations__c createNewAlloc(Employee_Details__c empl, string projectName, string projectRID){
        	return new WFM_employee_Allocations__c(EMPLOYEE_ID__c=employeeRID, Project_Buf_Code__c=buCode,Project_Country__c=country, Project_ID__c=projectRID, Project_Function__c=role, 
        	employee_buf_code__c = empl.buf_code__c, employee_country__c = empl.country_name__r.name, employee_role__c = empl.job_class_desc__c,
            Status__c=status, allocation_start_date__c = RM_Tools.getDateFromString(startDate, 'mm/dd/yyyy'), allocation_end_date__c = RM_Tools.getDateFromString(endDate, 'mm/dd/yyyy'), Allocation_Unique_Key__c=getUniqueKey(projectName));
        }
    }
  /*  @RemoteAction
    global static List<SelectOption> getLocCodes(string country){
       
        transient List<SelectOption> options = RM_LookUpDataAccess.getLocBU(false, country);       
       
        return options;
    }
    @RemoteAction
    global static List<SelectOption> getCountry(string BU){        
       
        transient List<SelectOption> options = RM_LookUpDataAccess.getCountry(false, BU); 
        return options;
    }*/
}