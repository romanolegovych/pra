public class IPA_Utils
{
	//get all picklist option values using the type of sobject and specific picklist field
    //TODO: move this into platform wide sobject utils class as it is not domain specific
    public static List<String> getPickListValues(String objectName, String fieldName) 
    {
        try
        {
            List<String> pickListValuesList = new List<String>();
            sObject sObj = Schema.getGlobalDescribe().get(objectName).newSObject();
            Schema.sObjectType sObjType = sObj.getSObjectType();
            Schema.DescribeSObjectResult sObjDescribe = sObjType.getDescribe();
            Map<String, Schema.SObjectField> sObjFieldMap = sObjDescribe.fields.getMap();
            List<Schema.PicklistEntry> sObjPickListEntryList = sObjFieldMap.get(fieldName).getDescribe().getPickListValues();
            System.debug('>>>>>>>>>>>>>>>>>>>>  sObjPickListEntryList for ' + objectName + ':' + fieldName + ' = ' + sObjPickListEntryList);
            for(Schema.PicklistEntry sObjPickListEntry : sObjPickListEntryList) 
            {
                pickListValuesList.add(sObjPickListEntry.getLabel());
            }
            return pickListValuesList;
        }
        catch(exception e)
        {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
            	'Unable to find setup information for the field ' + objectName + '.' + fieldName + 
				'. Please contact your system administrator.');
            ApexPages.addMessage(msg);
            System.Debug('>>>>>>>>>>>>>>>>>>>>  Exception in IPA_Utils.class:' + msg + '(' + e.getMessage() + ')');
        	return null;
        }
        
    }
    
	//get all fields from a given sobject, generate the needed soql, add optional where conditions and return results
    //TODO: move this into platform wide sobject utils class as it is not domain specific
    public static List<sObject> getSObjects(String objName, String criteria, List<String> customFields, Integer limitClause)
    {
		List<sObject> sObjList = new List<sObject>();		
		// build the base soqlQuery
        String soqlQuery = getBaseQuery(objName, customFields);		
		System.debug('>>>>>>>>>>>>>>>>>>>>  base soqlQuery = ' + soqlQuery);
		// add the where clause to the soqlQuery
		if((criteria != null) && (criteria.length() > 0))
		{
			soqlQuery += ' WHERE  ' + criteria;
			System.debug('>>>>>>>>>>>>>>>>>>>>  base with criteria soqlQuery = ' + soqlQuery);
		}
        if((limitClause != null) && (limitClause > 0))
        {
			soqlQuery += ' LIMIT  ' + limitClause;
			System.debug('>>>>>>>>>>>>>>>>>>>>  base with limit soqlQuery = ' + soqlQuery);
		}
		// make the dynamic call to get the records
		sObjList = Database.query(soqlQuery);
		return sObjList;
	}
    
	//return a dynamic Query string that includes all the direct sObject fields and the related Name fields from the parent objects
    //dependent method of getSobjects
    //TODO: move this into platform wide sobject utils class as it is not domain specific
    public static String getBaseQuery(String objName, List<String> customFields)
    {		
		// get the describe info for the org, object and its fields
		Map<String,Schema.SObjectType> orgMap = Schema.getGlobalDescribe();
		Schema.SObjectType sObjType = orgMap.get(objName);
		Schema.DescribeSObjectResult sObjDescribe = sObjType.getDescribe();
		Map<String, Schema.SObjectField> sObjFieldMap = sObjDescribe.fields.getMap();
		List<Schema.SObjectField> sObjFieldList = sObjFieldMap.values();
        //build out the base soql query using the describe info
		String soqlQuery = 'SELECT ';
		for(Schema.SObjectField sObjField : sObjFieldList)
		{
			String fieldLabel = sObjField.getDescribe().getLabel(); 
			String fieldName = sObjField.getDescribe().getName();
			String fieldType = String.valueOf(sObjField.getDescribe().getType()); 
			String relationshipName = sObjField.getDescribe().getRelationshipName();
			System.debug('>>>>>>>>>>>>>>>>>>>>  Label = ' + fieldLabel + '    Name = ' + fieldName + '     Type = ' + 
                         fieldType + '    relationshipName = ' + relationshipName);			
			// add each field to the base soql query
			soqlQuery += ' ' + fieldName + ',';
			// if this is a lookup field, also include the name field of the parent
			if(('REFERENCE'.equalsIgnoreCase(fieldType) == true) && ((relationshipName != null) && (relationshipName.length() > 0)))
			{
				// add the name field from the parent object to the base soql query
				soqlQuery += ' ' + relationshipName + '.Name,';
			}
		}
		// add additional custom fields specified in the call to the base soql query
		if((customFields != null) && (customFields.size() > 0))
		{
			for(String customField : customFields)
			{
				soqlQuery += ' ' + String.escapeSingleQuotes(customField) + ',';
			}
		}		
		// remove the last comma from the base query
		soqlQuery = soqlQuery.subString(0, soqlQuery.length() - 1);
		// add the from clause to the base query
		soqlQuery += ' FROM ' + objName + ' ';
		//return the dynamic base soql query string
		return soqlQuery;
	}
	
}