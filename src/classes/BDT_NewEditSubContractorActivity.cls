public with sharing class BDT_NewEditSubContractorActivity {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} //Administration Subcontractors
	public SubcontractorActivity__c SubcontractorActivity {get;set;}
	public String SubcontractorId;
	public String SubcontractorActivityId;

	public BDT_NewEditSubcontractorActivity(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Subcontractors');
		loadSubcontractorActivity(); 
	}

	public void loadSubcontractorActivity(){
		SubcontractorActivityId = System.currentPageReference().getParameters().get('SubcontractorActivityId');
		SubcontractorId = System.currentPageReference().getParameters().get('SubcontractorId');

		try{
			SubcontractorActivity = [Select Name, QualificationStatus__c, QualificationStartDate__c, QualificationEndDate__c, Subcontractor__r.Name
											from SubcontractorActivity__c
											where SubcontractorActivity__c.Id = :SubcontractorActivityId];	
		}
		catch(QueryException e){
			SubcontractorActivity = new SubcontractorActivity__c();
			SubcontractorActivity.Subcontractor__c=SubcontractorId;
		}
	}
	
	public PageReference save(){
		try{
			upsert SubcontractorActivity;	
		}
		catch( DMLException e ){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Subcontractor Activity could not be saved !');
        	ApexPages.addMessage(msg);
        	return null;
		}
		PageReference subcontractorsPage = new PageReference(System.Page.BDT_Subcontractors.getUrl());
		subcontractorsPage.getParameters().put('ScId',SubcontractorId);
	
		
		return subcontractorsPage;
	}
	
	public PageReference cancel(){
		SubcontractorId = system.currentPageReference().getParameters().get('SubcontractorId');
		PageReference subcontractorsPage = new PageReference(System.Page.BDT_Subcontractors.getUrl());
		subcontractorsPage.getParameters().put('ScId',SubcontractorId);
		
		return subcontractorsPage;
	}
	
	public PageReference deleteSoft(){
		SubcontractorActivity.BDTDeleted__c = true;
		
		try{
			update SubcontractorActivity;	
		}
		catch( DMLException e ){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Subcontractor Activity could not be deleted!');
        	ApexPages.addMessage(msg);
        	return null;
		}
		PageReference subcontractorsPage = new PageReference(System.Page.BDT_Subcontractors.getUrl());
		subcontractorsPage.getParameters().put('ScId',SubcontractorId);
		
		return subcontractorsPage;
	}
}