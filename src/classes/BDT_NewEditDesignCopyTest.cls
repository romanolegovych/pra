/**
 */
@isTest
private class BDT_NewEditDesignCopyTest {
	public static Client_Project__c firstProject {get;set;}
	public static List<Study__c> stydiesList {get;set;}	
	public static List<ClinicalDesign__c> designList {get;set;}
	public static List<Flowcharts__c> flowchartList  {get;set;}
	public static List<Arm__c> armList				 {get;set;}
	public static List<Epoch__c> epochList				 {get;set;}
	public static List<flowchartassignment__c> flowchartAssList {get;set;}
	
	static void init(){

		firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;
		
		stydiesList = BDT_TestDataUtils.buildStudies(firstProject);
		insert stydiesList;
		
		designList = BDT_TestDataUtils.buildClinicDesign(firstProject); 
		insert designList;		
				 
		flowchartList = BDT_TestDataUtils.buildFlowchart(designList[0].id);
		insert flowchartList;
		
		armList = BDT_TestDataUtils.buildArm(designList[0].id);
		insert armList;
		
		epochList = BDT_TestDataUtils.buildEpoch(designList[0].id);
		insert epochList;
		
		flowchartAssList = new List<flowchartassignment__c>();
		
		BDT_Utils.setPreference('SelectedProject', firstProject.id);
		BDT_Utils.setPreference('SelectedStudies',stydiesList[0].id);
	    
	    system.assertEquals(5,  [select count() from Epoch__c where ClinicalDesign__c =: designList[0].id]);
	    system.assertEquals(5,  [select count() from Arm__c where ClinicalDesign__c =: designList[0].id]);
	    system.assertEquals(5,  [select count() from Flowcharts__c where ClinicalDesign__c =: designList[0].id]);
	    
	    flowchartAssList.add(new flowchartassignment__c(arm__c = armList[0].id, epoch__c = epochList[0].id, flowchart__c = flowchartList[0].id));
    	insert flowchartAssList;
    	
    	system.assertEquals(1,  [select count() from flowchartassignment__c where id =: flowchartAssList[0].id]);
	}
	
	static testMethod void copyTimePOintandAllHappyPath() {
    	init();
    	//copy to flowchart 2
    	System.currentPageReference().getParameters().put('designId', designList[1].id);
        BDT_NewEditDesignCopy copyMe = new BDT_NewEditDesignCopy();
       
        copyMe.getCopyFromOptions();
        copyMe.setCopyFromId(designList[0].id);
        copyMe.getCopyFromId();
        copyMe.Copy();
        
       	system.assertEquals(5,  [select count() from Epoch__c where ClinicalDesign__c =: designList[1].id]);
	    system.assertEquals(5,  [select count() from Arm__c where ClinicalDesign__c =: designList[1].id]);
	    system.assertEquals(5,  [select count() from Flowcharts__c where ClinicalDesign__c =: designList[1].id]);
	}
}