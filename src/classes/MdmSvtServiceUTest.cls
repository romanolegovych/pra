/**
 * Unit tests for MDM Svt Service classes
 */
@isTest
private class MdmSvtServiceUTest {
	
	static list<MuleServicesCS__c> serviceSettings;
	
	static void init() {
		serviceSettings = new MuleServicesCS__c[] {
			new MuleServicesCS__c(name = 'ENDPOINT', Value__c = 'ENDPOINT'),
			new MuleServicesCS__c(name = 'TIMEOUT', Value__c = '60000'),
			new MuleServicesCS__c(name = 'MdmSvtService', Value__c = 'MdmSvtService')
		};
		insert serviceSettings;
	}
	
	// This testmethod is just to check that the mocking is working
	// Assertions will be made for classes that use the service wrapper
	static testMethod void testGenerateResponses_POSITIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmSvtServiceTestMocks(false));
		performTests();
		Test.stopTest();
	}
	
	static testMethod void testGenerateMoreResponses_POSITIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmSvtServiceTestMocks(false));
		performMoreTests();
		Test.stopTest();
	}
	
	static testMethod void testGenerateResponses_NEGATIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmSvtServiceTestMocks(true));
		performTests();
		Test.stopTest();
	}
	
	static testMethod void testGenerateMoreResponses_NEGATIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmSvtServiceTestMocks(true));
		performMoreTests();
		Test.stopTest();
	}
	
	private static void performTests() {
		MdmProtocolService.protocolVO protocolVO = new MdmProtocolService.protocolVO();
		MdmSvtService.svtVO svtVO = new MdmSvtService.svtVO();
		MdmSvtService.visitMappingResponse visitMappingResp = MdmSvtServiceWrapper.getSvtEcrfMappingByProtocolPraId(protocolVO, '');
		MdmSvtService.svtEcrfResponse svtEcfResp = MdmSvtServiceWrapper.getSvtsEcrfsFromProtocolPraId(protocolVO, '');
		MdmSvtService.svtEcrfMappingResponse svtEcrfMappingResp = MdmSvtServiceWrapper.getSvtsEcrfsMappingsFromProtocolPraId(protocolVO, '');
		MdmSvtService.svtEcrfUsvFormMappingResponse svtEcrfUsvFormMappingResp = MdmSvtServiceWrapper.getSvtsEcrfsUsvFormsMappingsFromProtocolPraId(protocolVO, '');
		MdmSvtService.svtEcrfResponse svtEcrfResp = MdmSvtServiceWrapper.getSvtsEcrfsFromProtocolPraId(protocolVO, '');
		MdmSvtService.svtListResponse svtListResp = MdmSvtServiceWrapper.getSvtVOsByProtocol(protocolVO);
		MdmSvtServiceWrapper.getErrors();
	}
	
	private static void performMoreTests() {
		MdmSvtService.ecrfMappingVO ecrfMappingVO = new MdmSvtService.ecrfMappingVO();
		list<MdmSvtService.ecrfMappingVO> ecrfMappingVOs = new list<MdmSvtService.ecrfMappingVO>();
		ecrfMappingVOs.add(ecrfMappingVO);
		MdmSvtService.ecrfFormMappingVO ecrfFormMappingVO = new MdmSvtService.ecrfFormMappingVO();
		list<MdmSvtService.ecrfFormMappingVO> ecrfFormMappingVOs = new list<MdmSvtService.ecrfFormMappingVO>();
		ecrfFormMappingVOs.add(ecrfFormMappingVO);
		MdmSvtService.mdmCrudResponse mdmCrudResp = MdmSvtServiceWrapper.saveSvtEcrfMappingFromVO(ecrfMappingVO);
		mdmCrudResp = MdmSvtServiceWrapper.saveSvtEcrfMappingFromVOs(ecrfMappingVOs);
		mdmCrudResp = MdmSvtServiceWrapper.deleteSvtEcrfMappingFromVOs(ecrfMappingVOs);
		mdmCrudResp = MdmSvtServiceWrapper.saveSvtUsvFormMappingFromVOs(ecrfFormMappingVOs);
		mdmCrudResp = MdmSvtServiceWrapper.deleteSvtUsvFormMappingFromVOs(ecrfFormMappingVOs);
	}
}