/**
@author Bhargav Devaram
@date 2015
@description this is PBB_ScenarioDetailsController test class
**/

@isTest   
private class PBB_ScenarioDetailsCntrlTest{        
    static testMethod void testPBB_ScenarioDetailsController(){        
        
        test.startTest();  
         //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils (); 
        tu.country = tu.createCountryAttributes();
        tu.acc= tu.createAccountAttributes();
        tu.opp = tu.createOpportunityAttributes();
        test.stopTest();
        tu.wfmp = [select id,name from wfm_project__c];
        tu.bid =  [select id,name from Bid_project__c];
        system.debug('-------tu.wfmp'+tu.wfmp+tu.bid );
		  
                 
		//tu.wfmp = tu.createWfmProject();
        //tu.bid = tu.createBidProject();
        
        tu.sm= tu.createSRMModelAttributes();
        tu.MSA = tu.createSRMModelSiteActAttributes();
		tu.MSE= tu.createSRMModelSubjEnrol();        
        tu.SCA= tu.createSRMCalendarAdj();
        tu.SWA= tu.createSRMWeekAdj();
        tu.SMQ= tu.createSRMModelQues();
        tu.SMR= tu.createSRMModelResp();
        tu.sm= tu.approveSRMmodel(tu.sm);
        
        PBB_Scenario__c scen = new PBB_Scenario__c(Bid_Project__c = tu.bid.Id);
        
        Scen.Status__c = 'Approved'; 
        Scen.SRM_model__c= tu.sm.id;
        //upsert scen;
        ApexPages.CurrentPage().getParameters().put('bid',tu.bid.Id);
        ApexPages.StandardController cnt = new ApexPages.StandardController(scen);
        PBB_ScenarioDetailsController scenario = new PBB_ScenarioDetailsController(cnt);
        PageReference ref = Page.PBB_ScenarioDetails;
        Test.setCurrentPage(ref); 
        System.assertEquals(1 , scenario.countryMap.size());
        System.assertEquals(1, scenario.countryMapSubEnroll.size());     
        scenario.addRow();
        scenario.scenario.Status__c = 'Approved';
        scenario.saveScenario(); 
        
        
        tu.bid.Proposal_Director__c = UserInfo.getUserId();
        tu.bid.Regulatory_Expert__c = UserInfo.getUserId();
        tu.bid.Proposal_Manager__c = UserInfo.getUserId(); 
        tu.bid.Therapeutic_Expert__c = UserInfo.getUserId();
        tu.bid.Project_Manager__c = UserInfo.getUserId(); 
        tu.bid.Business_Dev_Expert__c = UserInfo.getUserId();
        tu.bid.Contracts_Expert__c = UserInfo.getUserId();
        tu.bid.Startup_Expert__c = UserInfo.getUserId();
        tu.bid.Project_Director__c = UserInfo.getUserId();        
        update  tu.bid;      
        scenario.scenario.SRM_model__c= tu.sm.id;
        
        scenario.projectStartDate = DateTime.Now().addDays(1).format('dd-MMM-yyyy');
        scenario.projectEndDate = DateTime.Now().addDays(365).format('dd-MMM-yyyy');
           
        
        scenario.firstPatientRandomized = DateTime.Now().addDays(70).format('dd-MMM-yyyy');
        scenario.finalProtocolExpected = DateTime.Now().addDays(50).format('dd-MMM-yyyy');
        
        scenario.lastPatientRandomized = DateTime.Now().addDays(350).format('dd-MMM-yyyy'); //('MM/dd/yyyy'); //'10/15/2015';
        scenario.lastPatientOut = DateTime.Now().addDays(351).format('dd-MMM-yyyy'); //('MM/dd/yyyy'); //'10/15/2015'; 
        
        scen.Expected_Patients__c = 50;
        scen.Expected_Sites__c = 50;       
        system.debug('-------scenario.scenario'+scenario.scenario );
        scenario.inrList.clear();
        PBB_ScenarioDetailsController.InnerClass cls = new PBB_ScenarioDetailsController.InnerClass();
        cls.countryName = tu.country.Name;
        cls.noOfSites = '100';
        cls.expSubEnrollRate = 2;
        cls.height = 2;
        cls.subEnrollment = '2';
        cls.subEnrollRate = 2;
        cls.noOfSubjects = '200';
        cls.siteActId = tu.wfmp.id;
        scenario.inrList.add(cls);
        scenario.generateCountryChartData(); 
        scenario.saveAndExitScenario(); 
        scenario.chartOptionSub = 'Summary';
        scenario.generateCountryChartDataSubject();
        
        system.debug('-------scenario.scenario.Id'+scenario.scenario.Id );
        ApexPages.CurrentPage().getParameters().put('id',scenario.scenario.Id);
        ApexPages.StandardController cnt1 = new ApexPages.StandardController(scenario.scenario);
        PBB_ScenarioDetailsController scenario1 = new PBB_ScenarioDetailsController(cnt1);
        PageReference ref1 = Page.PBB_ScenarioDetails;
        Test.setCurrentPage(ref1);  
        
        
    }   
    
    static testMethod void testPBB_ScenarioDetailsController1(){        
        test.startTest();  
         //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils (); 
        tu.country = tu.createCountryAttributes();
        tu.acc= tu.createAccountAttributes();
        tu.opp = tu.createOpportunityAttributes();
        test.stopTest();
        tu.wfmp = [select id,name from wfm_project__c];
        tu.bid =  [select id,name from Bid_project__c];
        
        tu.sm= tu.createSRMModelAttributes();
        tu.MSA = tu.createSRMModelSiteActAttributes();
        system.debug('-------tu.MSA'+tu.MSA+tu.sm );
		tu.MSE= tu.createSRMModelSubjEnrol();        
        tu.SCA= tu.createSRMCalendarAdj();
        tu.SWA= tu.createSRMWeekAdj();
        tu.SMQ= tu.createSRMModelQues();
        tu.SMR= tu.createSRMModelResp();
        //tu.sm= tu.approveSRMmodel(tu.sm);
        tu.sm.status__c = 'Approved';
        update tu.sm;
        
        tu.bid.Proposal_Director__c = UserInfo.getUserId();
        tu.bid.Regulatory_Expert__c = UserInfo.getUserId();
        tu.bid.Proposal_Manager__c = UserInfo.getUserId(); 
        tu.bid.Therapeutic_Expert__c = UserInfo.getUserId();
        tu.bid.Project_Manager__c = UserInfo.getUserId(); 
        tu.bid.Business_Dev_Expert__c = UserInfo.getUserId();
        tu.bid.Contracts_Expert__c = UserInfo.getUserId();
        tu.bid.Startup_Expert__c = UserInfo.getUserId();
        tu.bid.Project_Director__c = UserInfo.getUserId();        
        update  tu.bid;      
        
        system.debug('-------tu.wfmp'+tu.wfmp+tu.bid );
	    tu.scen =tu.createScenarioAttributes();
        tu.scen.Status__c = 'Approved';
        tu.scen.SRM_MODEL__c = tu.sm.id;
        upsert tu.scen;
        system.debug('-------tu.scen.SRM_MODEL__c '+tu.scen.SRM_MODEL__c +tu.sm.id+ tu.MSA.SRM_MODEL__c);
       
        tu.psc =tu.createPBBScenarioCountry();
        tu.scecont=tu.createSrmCountryAttributes();
        tu.scecont.SRM_Model_Site_Activation__c = tu.MSA.id;
        tu.scecont.SRM_Model_Subject_Enrollment__c = tu.MSE.id;
        update tu.scecont;
        ApexPages.CurrentPage().getParameters().put('bid',tu.bid.Id);
        ApexPages.StandardController cnt = new ApexPages.StandardController(tu.scen);
        PBB_ScenarioDetailsController scenario = new PBB_ScenarioDetailsController(cnt);
        PageReference ref = Page.PBB_ScenarioDetails;
        Test.setCurrentPage(ref); 
        
        scenario.chartOption = 'Summary';     
        System.debug('>>> scenario.inrList >>>'+scenario.inrList);
        scenario.generateSiteActivationData();
        
        scenario.chartOptionSub = 'Summary';        
        System.debug('>>> scenario.inrList >>>'+scenario.inrList);
        scenario.generateCountryChartDataSubject();
        
        System.debug('>>> scenario.inrList >>>'+scenario.inrList);
        
        scenario.IQSummaryExport();
        scenario.EnrollmentExport();
        scenario.SiteActivationExport();
        scenario.NoofsitesExport();
        
        scenario.cloneScenarioUsingOldModel(); 
        scenario.cloneScenarioUsingNewModel();
               
    }
    
    static testMethod void testPBB_ScenarioDetailsController3(){        
       
         test.startTest();  
         //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils (); 
        tu.country = tu.createCountryAttributes();
        tu.acc= tu.createAccountAttributes();
        tu.opp = tu.createOpportunityAttributes();
        test.stopTest();
        tu.wfmp = [select id,name from wfm_project__c];
        tu.bid =  [select id,name from Bid_project__c];
        
        tu.sm= tu.createSRMModelAttributes();
        tu.MSA = tu.createSRMModelSiteActAttributes();
        system.debug('-------tu.MSA'+tu.MSA+tu.sm );
		tu.MSE= tu.createSRMModelSubjEnrol();        
        tu.SCA= tu.createSRMCalendarAdj();
        tu.SWA= tu.createSRMWeekAdj();
        tu.SMQ= tu.createSRMModelQues();
        tu.SMR= tu.createSRMModelResp();
        tu.sm= tu.approveSRMmodel(tu.sm);
        
        tu.bid.Proposal_Director__c = UserInfo.getUserId();
        tu.bid.Regulatory_Expert__c = UserInfo.getUserId();
        tu.bid.Proposal_Manager__c = UserInfo.getUserId(); 
        tu.bid.Therapeutic_Expert__c = UserInfo.getUserId();
        tu.bid.Project_Manager__c = UserInfo.getUserId(); 
        tu.bid.Business_Dev_Expert__c = UserInfo.getUserId();
        tu.bid.Contracts_Expert__c = UserInfo.getUserId();
        tu.bid.Startup_Expert__c = UserInfo.getUserId();
        tu.bid.Project_Director__c = UserInfo.getUserId();        
        update  tu.bid;      
        
        system.debug('-------tu.wfmp'+tu.wfmp+tu.bid );
	    tu.scen =tu.createScenarioAttributes();
        tu.scen.Status__c = 'Approved';
        tu.scen.SRM_MODEL__c = tu.sm.id;
        upsert tu.scen;
        tu.psc =tu.createPBBScenarioCountry();
        tu.scecont=tu.createSrmCountryAttributes();
        tu.scecont.SRM_Model_Site_Activation__c = tu.MSA.id;
        tu.scecont.SRM_Model_Subject_Enrollment__c = tu.MSE.id;
        update tu.scecont;
        ApexPages.CurrentPage().getParameters().put('bid',tu.bid.Id);
        ApexPages.StandardController cnt = new ApexPages.StandardController(tu.scen);
        PBB_ScenarioDetailsController scenario = new PBB_ScenarioDetailsController(cnt);
        PageReference ref = Page.PBB_ScenarioDetails;
        Test.setCurrentPage(ref); 
      
        
        scenario.selectedRole ='Therapeutic Expert';
        scenario.displayQuestions();
        
        scenario.selectedRole ='Project Manager';
        scenario.displayQuestions();
        
        scenario.selectedRole ='Contracts Expert';
        scenario.displayQuestions(); 
        
        scenario.selectedRole ='Business Development Expert';
        scenario.displayQuestions(); 
        
        scenario.selectedRole ='Study Startup Expert';
        scenario.displayQuestions(); 
        
        scenario.selectedRole ='Regulatory Expert';
        scenario.displayQuestions(); 
        
        scenario.selectedRole ='Proposal Director';
        scenario.displayQuestions();  
        
        scenario.selectedRole ='DPD';
        scenario.displayQuestions(); 
        
        scenario.selectedRole ='Proposal Manager';
        scenario.displayQuestions();                 
                     
        
        scenario.selectedRole ='All';
        scenario.displayQuestions(); 
        ApexPages.CurrentPage().getParameters().put('id',tu.scen.Id);
        ApexPages.CurrentPage().getParameters().put('clone','1');
        PBB_ScenarioDetailsController scenario1 = new PBB_ScenarioDetailsController(cnt);
        PageReference ref1 = Page.PBB_ScenarioDetails;
        Test.setCurrentPage(ref1); 

        
    }
    
    static testMethod void testPBB_ScenarioDetailsController4(){        
       
         test.startTest();  
         //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils (); 
        tu.country = tu.createCountryAttributes();
        tu.acc= tu.createAccountAttributes();
        tu.opp = tu.createOpportunityAttributes();
        test.stopTest();
        tu.wfmp = [select id,name from wfm_project__c];
        tu.bid =  [select id,name from Bid_project__c];
        
        tu.sm= tu.createSRMModelAttributes();
        tu.MSA = tu.createSRMModelSiteActAttributes();
        system.debug('-------tu.MSA'+tu.MSA+tu.sm );
		tu.MSE= tu.createSRMModelSubjEnrol();        
        tu.SCA= tu.createSRMCalendarAdj();
        tu.SWA= tu.createSRMWeekAdj();
        tu.SMQ= tu.createSRMModelQues();
        tu.SMR= tu.createSRMModelResp();
        tu.sm= tu.approveSRMmodel(tu.sm);
        
        tu.bid.Proposal_Director__c = UserInfo.getUserId();
        tu.bid.Regulatory_Expert__c = UserInfo.getUserId();
        tu.bid.Proposal_Manager__c = UserInfo.getUserId(); 
        tu.bid.Therapeutic_Expert__c = UserInfo.getUserId();
        tu.bid.Project_Manager__c = UserInfo.getUserId(); 
        tu.bid.Business_Dev_Expert__c = UserInfo.getUserId();
        tu.bid.Contracts_Expert__c = UserInfo.getUserId();
        tu.bid.Startup_Expert__c = UserInfo.getUserId();
        tu.bid.Project_Director__c = UserInfo.getUserId();        
        update  tu.bid;      
        
        system.debug('-------tu.wfmp'+tu.wfmp+tu.bid );
	    tu.scen =tu.createScenarioAttributes();
        tu.scen.Status__c = 'Approved';
        tu.scen.SRM_MODEL__c = tu.sm.id;
        upsert tu.scen;
        tu.psc =tu.createPBBScenarioCountry();
        tu.scecont=tu.createSrmCountryAttributes();
        tu.scecont.SRM_Model_Site_Activation__c = tu.MSA.id;
        tu.scecont.SRM_Model_Subject_Enrollment__c = tu.MSE.id;
        update tu.scecont;
        ApexPages.CurrentPage().getParameters().put('bid',tu.bid.Id);
        ApexPages.CurrentPage().getParameters().put('id', tu.scen.Id);
        ApexPages.StandardController cnt = new ApexPages.StandardController(tu.scen);
        PBB_ScenarioDetailsController scenario = new PBB_ScenarioDetailsController(cnt);
        PageReference ref = Page.PBB_ScenarioDetails;
        Test.setCurrentPage(ref); 
         
        scenario.inrList.clear();
        PBB_ScenarioDetailsController.InnerClass cls = new PBB_ScenarioDetailsController.InnerClass();
        cls.countryName = tu.country.Name;
        cls.noOfSites = '100';
        cls.expSubEnrollRate = 2;
        cls.subEnrollRate = 2;
        cls.noOfSubjects = '200';
        cls.siteActId = tu.wfmp.Id;
        scenario.inrList.add(cls);
        
        scenario.generateCountryChartData();
        scenario.scenario.Status__c = 'Approved';
        scenario.saveAndExitScenario();
        System.assertNotEquals(null, scenario.arrayString);
        scenario.addRow();
        scenario.rowNum = scenario.inrList.size();
        scenario.displayQuestions();
        
        
        
        List<SelectOption> selOpt = new List<SelectOption>();
        selOpt.add(new SelectOption(tu.country.Name, tu.country.Name));
        PBB_ScenarioDetailsController.InnerClass cls1 = new PBB_ScenarioDetailsController.InnerClass(tu.country.Name, '100', tu.wfmp.Id, selOpt);
        scenario.inrList.add(cls1);
        
        ApexPages.CurrentPage().getParameters().put('index', 0+'');
        scenario.indexNo = 0;
		scenario.selectedRole ='All';
        scenario.displayQuestions();
        List<PBB_ScenarioDetailsController.questionWrapper> countrySiteActivationList = new List<PBB_ScenarioDetailsController.questionWrapper>();
        PBB_ScenarioDetailsController.questionWrapper qw = new PBB_ScenarioDetailsController.questionWrapper();
        qw.questionobj = tu.SMQ;
        PBB_ScenarioDetailsController.countryresponsewrapper crw = new PBB_ScenarioDetailsController.countryresponsewrapper();
        crw.questionobj = tu.SMQ;
        crw.isCountrySelected = true;
        crw.selectedValue='none';
        qw.country1queresponseList.add(crw);
        countrySiteActivationList.add(qw);
        
        scenario.countrySiteActivationList = countrySiteActivationList;
        scenario.countrySubEnrolmntList = countrySiteActivationList;
        system.debug('------scenario.countrySiteActivationList'+scenario.countrySiteActivationList );
        scenario.indexQuesToCopy = 0;
        scenario.saveQuestionResponses(tu.scen.id);
        scenario.countrySubEnrlChange();
        scenario.countrySiteActivationSelected();
        
        scenario.countrySiteActivationChange();
        scenario.countrySubEnrlSelected();
        
        scenario.delRow();
        scenario.cancel();
        scenario.EditCUGData();
        scenario.selectedModelId = tu.sm.id;
        scenario.selectModel();
        scenario.continuePopup();
        scenario.callsummarychart();
        
        scenario.scenarioModelMethod();       
        scenario.generateSummaryChartForSiteAndSubject();
        scenario.createWeeklyEvents();
        scenario.selectModel();
        scenario.indexNo = 0;
        scenario.cancelSiteActivationSelection();
        scenario.displaySiteActivations();
        scenario.CalendarAdjustments(); 
    }
}