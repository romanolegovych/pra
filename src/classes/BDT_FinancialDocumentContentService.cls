/** Implements the Service Layer of the object FinancialDocumentContent__c
 * @author	Dimitrios Sgourdos
 * @version	11-Feb-2014
 */
public with sharing class BDT_FinancialDocumentContentService {
	
	/** Retrieve the FinancialDocumentContent with the given financial document id..
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 11-Feb-2014
	 * @param	documentId			The id of the financial document
	 * @return	The Financial Document Content that is assigned to the given financial document
	 */
	public static FinancialDocumentContent__c getContentByDocumentId(String documentId) {
		return BDT_FinancialDocumentContentDataAccessor.getContentByDocumentId(documentId);
	}
}