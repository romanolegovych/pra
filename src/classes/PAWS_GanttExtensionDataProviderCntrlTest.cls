@isTest
private class PAWS_GanttExtensionDataProviderCntrlTest
{
	static testMethod void testPAWS_GanttExtensionDataProviderCntrl()
	{
		PAWS_ApexTestsEnvironment.init();
		PAWS_Project_Flow_Site__c projectSite = PAWS_ApexTestsEnvironment.ProjectSite;
		
		Test.startTest();
		
		PageReference pageRef = Page.PAWS_GanttExtensionDataProvider;
		pageRef.getParameters().put('pawsProjectId', (String) PAWS_ApexTestsEnvironment.Project.Id);
		pageRef.getParameters().put('actions', 'getCountries,getSites,test');
		Test.setCurrentPage(pageRef);
		
		PAWS_GanttExtensionDataProviderCntrl controller = new PAWS_GanttExtensionDataProviderCntrl();
		system.assert(controller.GetOutputJSON() != null);
		
		Test.stopTest();
	}
}