/**
* @author 	Sukrut Wagh
* @date 	06/24/2014
* @description Register common error codes here
*/
public enum COM_ErrorCode {
	
	/**
	* @author 	Sukrut Wagh
	* @date 	06/24/2014
	* @description Used when an exception occures while instantiating the integration service.
	*/
	CANNOT_CREATE_INTEGRATION_SERVICE,
	/**
	* @author 	Sukrut Wagh
	* @date 	06/24/2014
	* @description Integraton service implementation Apex class not found for integration service.
	*/
	INTEGRATION_SERVICE_NOT_FOUND,
	/**
	* @author 	Sukrut Wagh
	* @date 	06/24/2014
	* @description A Trigger Dispatcher Class is not registered / not found.
	*/
	TRIGGER_DISPATCHER_NOT_FOUND,
	/**
	* @author 	Sukrut Wagh
	* @date 	06/24/2014
	* @description soType is passed as null
	*/
	TRIGGER_DISPATCHER_SO_TYPE_NULL

}