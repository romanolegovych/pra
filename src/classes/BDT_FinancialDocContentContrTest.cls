@isTest
private class BDT_FinancialDocContentContrTest {
	
	static testMethod void myUnitTest() {
		// Create data
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001',
																				'BDT Project First Experiments');
		insert currentProject;
		
		FinancialDocument__c financialDocument = BDT_BuildTestData.buildFinancialDocument(currentProject.Id,
																							new List<Study__c>());
		insert financialDocument;
		
		FinancialDocumentContent__c source = new FinancialDocumentContent__c(
														FinancialDocument__c=financialDocument.Id,
														SelectCheckJSON__c   = '[{"inputValue":"","checkFlag":false,"checkCode":"FrontPageChk"},{"inputValue":"","checkFlag":true,"checkCode":"SummaryChk"},{"inputValue":"","checkFlag":false,"checkCode":"GeneralChk"}]');
		insert source;
		
		// Create the controller without a financial document as parameter
		BDT_FinancialDocContentController p = new BDT_FinancialDocContentController();
		
		// Create the controller with a financial document as a parameter
		PageReference pageRef = New PageReference(System.Page.BDT_FinancialDocContent.getUrl());
		pageref.getParameters().put('financialDocumentId' , financialDocument.id);
		Test.setCurrentPage(pageRef); 
		p = new BDT_FinancialDocContentController();
		
		p.writeValuesFromConfiguration();
		p.openStrategicInfoEditor();
		p.openContactDetailsEditor();
		p.openProjectConsiderationsEditor();
		p.closeRichEditors();
		p.save();
		p.completeFlag = true;
		p.save();
		p.commentSave();
		p.commentValue = 'Test comment';
		p.commentSave();
		p.closeComment();
		p.closePage();
	}
}