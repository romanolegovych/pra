public class IPA_UI_Dashboard
{   
    //Record Sets linked to Visualforce page
    public List<IPA_Links__c> lstQuickLinks {get; set;}
    public List<IPA_Articles__c> lstPolicyUpdates {get; set;}
    public List<IPA_Articles__c> lstEvents {get; set;}
    public List<IPA_Articles__c> lstHolidays {get; set;}
    public IPA_Articles__c featuredPRANews {get; set;}
    public IPA_Articles__c integrationUpdate {get; set;}
    public List<IPA_Articles__c> lstPRANews {get; set;}
    public List<IPA_Articles__c> lsindustryWatch {get; set;}
    public List<IPA_Articles__c> lstNewsSlider {get; set;}
    
    //Constructor
    //- Loads all required data to show in dashboard
    public IPA_UI_Dashboard()
    {
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        
        lstQuickLinks = ipa_bo.returnQuickLinks();
        lstPolicyUpdates = ipa_bo.returnPolicyUpdates();
        lstEvents = ipa_bo.returnTop3Events();
        lstHolidays = ipa_bo.returnTop3Holidays();
        featuredPRANews = ipa_bo.returnFeaturedPRANews();
        lstPRANews = ipa_bo.returnPRANews();
        integrationUpdate = ipa_bo.returnIntegrationUpdate();
        lsindustryWatch = ipa_bo.returnIndustryWatch();
        lstNewsSlider = ipa_bo.returnEmpNewsForSlider();
    }    
}