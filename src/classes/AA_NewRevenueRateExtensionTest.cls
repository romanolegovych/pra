@isTest
private class AA_NewRevenueRateExtensionTest{   
    
    /* Test BUF Code Selection options */
    static testMethod void getbufselectoptionTest() {
    
         initTestData ();
         
         Test.startTest();

         AA_NewRevenueRateExtension extension = new AA_NewRevenueRateExtension(new ApexPages.StandardController(new Revenue_Allocation_Rate__c()));
         List<SelectOption> result = extension.getbufselectoption();
         
         System.assertNotEquals(result.size(),null);
         
         Test.stopTest();
    }
    
    /* Test Currency Selection options */
    static testMethod void getcurrencyselectoptionTest() {
    
         initTestData ();
         
         Test.startTest();

         AA_NewRevenueRateExtension extension = new AA_NewRevenueRateExtension(new ApexPages.StandardController(new Revenue_Allocation_Rate__c()));
         List<SelectOption> result = extension.getcurrencyselectoption();
         
         System.assertNotEquals(result.size(),null);
         
         Test.stopTest();
    }  
    
     /* Test Create button */
    static testMethod void CreateTest() {
    
         initTestData ();
         
         Test.startTest();


         AA_NewRevenueRateExtension extension = new AA_NewRevenueRateExtension(new ApexPages.StandardController(new Revenue_Allocation_Rate__c()));
         extension.yearValue = '2011';
         extension.initialRateValue = 200;
         extension.selectedBufCode = [SELECT Id FROM Buf_Code__C].get(0).Id;
         extension.selectedCurrency = [SELECT Id FROM Currency__c].get(0).Id;
         extension.isActive = true;
         
         
         PageReference pageRef = extension.Create();
          
         PageReference reqPageRef = Page.AA_Inflation_Override;
         reqPageRef.getParameters().put('functionId', [SELECT Id FROM Buf_Code__C].get(0).Id);
         reqPageRef.getParameters().put('scheduledYear','2011');

         System.assertEquals(pageRef, null);
         
         Test.stopTest();
    }
    /* Test Year Value Error Create button */
    static testMethod void CreateErrorTest() {
    
         initTestData ();
         
         Test.startTest();


         AA_NewRevenueRateExtension extension = new AA_NewRevenueRateExtension(new ApexPages.StandardController(new Revenue_Allocation_Rate__c()));
         extension.yearValue = '1900';
         extension.initialRateValue = 200;
         extension.selectedBufCode = [SELECT Id FROM Buf_Code__C].get(0).Id;
         extension.selectedCurrency = [SELECT Id FROM Currency__c].get(0).Id;
         extension.isActive = true;
         
         
         PageReference pageRef = extension.Create();
          
         PageReference reqPageRef = Page.AA_Inflation_Override;
         reqPageRef.getParameters().put('functionId', [SELECT Id FROM Buf_Code__C].get(0).Id);
         reqPageRef.getParameters().put('scheduledYear','2011');

         System.assertEquals(pageRef, null);
         
         Test.stopTest();
    }
    
    /* Test Year Value Exception Create button */
    static testMethod void CreateExceptionTest() {
    
         initTestData ();
         
         Test.startTest();


         AA_NewRevenueRateExtension extension = new AA_NewRevenueRateExtension(new ApexPages.StandardController(new Revenue_Allocation_Rate__c()));
         extension.yearValue = 'A';
         extension.initialRateValue = 200;
         extension.selectedBufCode = [SELECT Id FROM Buf_Code__C].get(0).Id;
         extension.selectedCurrency = [SELECT Id FROM Currency__c].get(0).Id;
         extension.isActive = true;
         
         
         PageReference pageRef = extension.Create();
          
         PageReference reqPageRef = Page.AA_Inflation_Override;
         reqPageRef.getParameters().put('functionId', [SELECT Id FROM Buf_Code__C].get(0).Id);
         reqPageRef.getParameters().put('scheduledYear','2011');

         System.assertEquals(pageRef, null);
         
         Test.stopTest();
    }
     
  
    /* Test Create New Records Test */
    static testMethod void createNewRecordsTest() {
    
         initTestData ();
         
         Test.startTest();

         AA_NewRevenueRateExtension extension = new AA_NewRevenueRateExtension(new ApexPages.StandardController(new Revenue_Allocation_Rate__c()));
         extension.yearValue = '2011';
         extension.initialRateValue = 200;
         extension.selectedBufCode = [SELECT Id FROM Buf_Code__C].get(0).Id;
         extension.selectedCurrency = [SELECT Id FROM Currency__c].get(0).Id;
         extension.isActive = true;


         extension.createNewRecords();
         
         System.assertEquals(1, 1);
         
         Test.stopTest();
    }
    /* Test back button*/
    static testMethod  void backpressedTest(){
        AA_NewRevenueRateExtension extension = new AA_NewRevenueRateExtension(new ApexPages.StandardController(new Revenue_Allocation_Rate__c()));
        PageReference pageref = extension.back();
        System.assertEquals(pageref.geturl(),Page.AA_Revenue_Allocation_Rate.geturl());
    }  
    /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }  
}