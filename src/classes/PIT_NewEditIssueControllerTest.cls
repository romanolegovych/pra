@isTest
private class PIT_NewEditIssueControllerTest {

    static testMethod void myUnitTest() {
        // create the controller with New version
        WFM_Client__c client = new WFM_Client__c();
        client.Name ='test clt';
        client.Client_Name__c ='test clt';
        client.Client_Unique_Key__c='test clt';
        insert client;
         
        WFM_Contract__c objContract =new WFM_Contract__c();
        objContract.Name ='test';
        objContract.Client_ID__c =client.Id;
        objContract.Contract_Status__c='12';
        objContract.Contract_Unique_Key__c='test clt';
        insert objContract;
        WFM_Project__c objProj = new WFM_Project__c();
        objProj.Name = 'test';
        objProj.Contract_ID__c =objContract.Id;
        objProj.Project_Unique_Key__c ='tesfsdf';
        insert objProj;
        Issue__c objIssue = new Issue__c();
        objIssue.Project__c = objProj.Id;
        
        insert objIssue;
        
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=objIssue.id;
        insert attach;
        
        delete attach;       
       
        
        ApexPages.CurrentPage().GetParameters().put('Id',objIssue.Id);
        ApexPages.StandardController stdCtr = new ApexPages.StandardController(objIssue);
        //ApexPages.standardController stdCtr;
        PIT_NewEditIssueController p = new PIT_NewEditIssueController(stdCtr);       
        p.FindAttachments();
        p.insertAttachment();
        p.save();
        p.getctmsSubjectIds();
        p.getSiteIds();
        p.cancelNew();
        
        // Edit version
 //       Client_Project__c objproject = new Client_Project__c();
 //       objproject.Name= 'Test';
 //       objproject.Discount_Type__c ='%';
 //       objproject.Volume_Discount_Type__c ='%';
 //       insert objproject;
  //      Issue__c tmpIssue = new Issue__c(Category__c = 'Data Quality',Project__c=objproject.Id); 
 //       insert tmpIssue;
       // Issue__c tmpIssue = [SELECT id FROM Issue__c LIMIT 1];
        PageReference pageRef = New PageReference(System.Page.PIT_NewEditIssue.getUrl());
        //pageref.getParameters().put('id' , objIssue.id);
       // Test.setCurrentPage(pageRef);
        p = new PIT_NewEditIssueController(stdCtr);
        //System.assertEquals(p.issue.id, objIssue.id);
        p.showAttachSelPanel();
        p.hideAttachSelPanel();
        p.showAttachSelPanel();
        p.insertAttachment();
        p.removeAttach();
        p.viewAttach();
        p.cancelNew();
       // PIT_NewEditIssueController.getSites(objProj.name);
    }
}