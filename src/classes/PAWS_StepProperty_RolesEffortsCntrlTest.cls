@isTest
public class PAWS_StepProperty_RolesEffortsCntrlTest
{
    @isTest(SeeAllData=true)
    public static void instanceTest()
    {
        PAWS_StepProperty_RolesEffortsController ctrl = new PAWS_StepProperty_RolesEffortsController();
        ctrl.AddRecord();
        System.assert(ctrl.Roles.size() > 0);
        System.assert(ctrl.Records.size() == 1);
        
        ctrl.save();
        
        System.assert(ctrl.RecordsJson.length() > 0);
    }
}