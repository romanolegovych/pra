/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

@isTest
private class PBB_ScenarioCompareControllerTest{
    static testMethod void testPBB_ScenarioCompareController(){
        test.startTest();        
        //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils ();
        tu.wfmp = tu.createwfmproject();
        tu.bid =tu.createbidproject();        
        tu.scen =tu.createscenarioAttributes();
        tu.country =tu.createCountryAttributes();
        tu.psc =tu.createPBBScenarioCountry();
        tu.scecont= tu.createSrmCountryAttributes();   
        tu.pweList =tu.createPBBWeeklyEvents(tu.scecont,10);
        
        ApexPages.currentPage().getParameters().put('id',tu.bid.id);
        system.debug('-tu.scen --'+tu.scen);
        PBB_ScenarioCompareController scenCmpr = new PBB_ScenarioCompareController();
        scenCmpr.compareScenarios(); 
        List<SelectOption> selectedScenarios1 = new List<SelectOption>(); 
        selectedScenarios1.add(new SelectOption(tu.scen.id, tu.scen.id)); 
        System.assertEquals(scenCmpr.selectedScenarios.size(), 0);
        scenCmpr.selectedScenarios = selectedScenarios1;
        System.assertEquals(scenCmpr.selectedScenarios.size(), scenCmpr.selScenarioSize);
        scenCmpr.compareScenarios();        
        test.stopTest();
    }   
}