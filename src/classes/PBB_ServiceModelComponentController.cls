public with sharing class PBB_ServiceModelComponentController extends PBB_ComponentControllerBase {

    public PBB_ServiceModel serviceModel {get;set;}
    public PBB_ServiceArea serviceArea {get;set;}
    public PBB_ServiceFunction serviceFunction {get;set;}
    public PBB_ServiceTask serviceTask {get;set;}
    public PBB_ServiceTaskToServiceImpact serviceTaskToServiceImpact {get;set;}
    public PBB_ServiceTaskToDriver serviceTaskToDrivers {get;set;}
    public  Integer QuestionCount{get;set;}  

    @TestVisible private PBB_ServiceBase service;
    public String serviceId {get;set;}
    public String createObjectAPIName {get;set;}
    public Boolean isValid {get;set;}
    public Boolean flag {get;set;}

    public static final String SERVICE_MODEL = 'Service_Model__c';
    public static final String SERVICE_AREA = 'Service_Area__c';
    public static final String SERVICE_FUNCTION = 'Service_Function__c';
    public static final String SERVICE_TASK = 'Service_Task__c';
    public static final String SERVICE_TASK_TO_SERVICE_IMPACT = 'Service_Task_To_Service_Impact__c';
    public static final String SERVICE_TASK_TO_DRIVERS = 'Service_Task_To_Drivers__c';

    public String getServiceModelObjectApiName {
        get {
            return SERVICE_MODEL;
        }
    }

    public String getServiceAreaObjectApiName {
        get {
            return SERVICE_AREA;
        }
    }

    public String getServiceFunctionObjectApiName {
        get {
            return SERVICE_FUNCTION;
        }
    }

    public String getServiceTaskObjectApiName {
        get {
            return SERVICE_TASK;
        }
    }

    public String getServiceTaskToServiceImpactObjectApiName {
        get {
            return SERVICE_TASK_TO_SERVICE_IMPACT;
        }
    }

    public String getServiceTaskToDriversObjectApiName {
        get {
            return SERVICE_TASK_TO_DRIVERS;
        }
    }

    public Boolean isReadOnly {
        get {
            isReadOnly = false;
            if(serviceModel != null && serviceModel.serviceModel != null && (serviceModel.serviceModel.Status__c == 'Approved' || serviceModel.serviceModel.Status__c == 'Retired')) {
                isReadOnly = true;
            }
            return isReadOnly;
        }
        set;
    }

    private Map<String, PBB_ServiceBase> getObjectApiNameToService(){ 
        return new Map<String, PBB_ServiceBase> {
                SERVICE_MODEL => serviceModel,
                SERVICE_AREA => serviceArea,
                SERVICE_FUNCTION => serviceFunction,
                SERVICE_TASK => serviceTask,
                SERVICE_TASK_TO_SERVICE_IMPACT => serviceTaskToServiceImpact,
                SERVICE_TASK_TO_DRIVERS => serviceTaskToDrivers
        };
    }

    public PBB_ServiceModelComponentController() {
        init();
        flag = false;
    }

    private void init() {
        serviceModel = new PBB_ServiceModel();
    }
    
    public String getSelectServiceObject() {
        String objectAPIName = SERVICE_MODEL;
        service = serviceModel;
        if(!String.isBlank(serviceId)) {
            Id selectedServiceId = (Id)serviceId;
            objectAPIName = selectedServiceId.getSObjectType().getDescribe().getName();
            service = getObjectApiNameToService().get(objectAPIName);
            service.serviceId = serviceId;
        }
        cleansServices(objectAPIName);
        return objectAPIName;
    }

    private void cleansServices(String objectAPIName) {
        if(objectAPIName == SERVICE_AREA) {
            serviceTask = null;
        } else if(objectAPIName == SERVICE_MODEL) {
            serviceFunction = null;
            serviceTask = null;
        }
        if(String.isBlank(serviceId) && objectAPIName == SERVICE_MODEL) {
            serviceArea = null;
        }
    }
    
    public void refreshServiceObject() {
        if(!String.isBlank(serviceId)) {
            Id selectedServiceId = (Id)serviceId;
            String objectAPIName = selectedServiceId.getSObjectType().getDescribe().getName();  
            service.serviceId = serviceId;
            if(objectAPIName == SERVICE_AREA) {
                serviceFunction = new PBB_ServiceFunction(service.serviceId);
            } else if(objectAPIName == SERVICE_FUNCTION) {
                serviceTask = new PBB_ServiceTask(service.serviceId);
            } else if(objectAPIName == SERVICE_TASK) {
                serviceTaskToServiceImpact = new PBB_ServiceTaskToServiceImpact(service.serviceId);
                serviceTaskToDrivers = new PBB_ServiceTaskToDriver(service.serviceId);
            } else if(objectAPIName == SERVICE_MODEL) {
                serviceArea = new PBB_ServiceArea(service.serviceId);
            }else{
                ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'Object API name for refresh was not determined.') );
            }
        }
    }

    public PBB_ServiceModelComponentController getThis() {
        return this;
    }
    
    public void preparationCreateOrEditService() {
        isValid = false;
        getObjectApiNameToService().get(createObjectAPIName).preparationCreateOrEditService();
        If(createObjectAPIName =='Service_Task__c')
           QuestionCount = [select count() from Service_Task_To_Service_Impact__c where Service_Task__c =:service.serviceId];
    }
    
    public void clonePreparationCreateOrEditService() {
        isValid = false;
        system.debug('****flag 1 ='+flag);
        if(flag == true)
            service.flag = true;
        getObjectApiNameToService().get(createObjectAPIName).preparationCreateOrEditService();
    }
    
    public void getDetailService() {
        getSelectServiceObject();
        service.serviceId = serviceId;
        service.getDetailService();
        refreshServiceObject();
    }
    
    public void createService() {
        isValid = false;
        PBB_ServiceBase serviceToCreate = getObjectApiNameToService().get(createObjectAPIName);
        if (serviceToCreate != null){
            serviceToCreate.createService();
            isValid = serviceToCreate.isValid;
        }else{
            ApexPages.addMessage( new ApexPages.Message( ApexPages.Severity.Error, 'Object API name for creation was not determined.') );
        }
    }

    public void cancelService() {
        if(service != null) {
            service.cancelService();
        }
    }
    
    public void updateService() {
        isValid = false;
        getObjectApiNameToService().get(createObjectAPIName).createService();
        isValid = getObjectApiNameToService().get(createObjectAPIName).isValid;
    }
    
    public void removeService() {
        getObjectApiNameToService().get(createObjectAPIName).removeService();
        serviceId = getObjectApiNameToService().get(createObjectAPIName).previousServiceId;
        getObjectApiNameToService().get(createObjectAPIName).serviceId = serviceId;
        getDetailService();
    }
}