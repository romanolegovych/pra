/**
*   'PRA_WBSProjectController' is to display the work breakdown structure(WBS) for all the projects in P&F
*   @author   Devaram Bhargav
*   @version  18-Oct-2013
*   @since    18-Oct-2013
*/
public class PRA_WBSProjectController {
	
	/** ProjectsWBSList List is to hold the WBS of all the projects.*/
    public List<PRA_ProjectWBSCVO.ProjectWBS> ProjectsWBSList{get;set;}
    
    /** ProjectName variable is to hold the project name.*/
    public String ProjectName{get;set;} 
    
    /**
    *  Constructor that return the WBS of all the projects.          
    */
    public PRA_WBSProjectController () {
    	
    	//Initiating the list.
        ProjectsWBSList=new List<PRA_ProjectWBSCVO.ProjectWBS>();
                
        //Initiate the map to collect the Project and its WBS.        
        Map<String,PRA_ProjectWBSCVO.ProjectWBS> projectWBSMap=new Map<String,PRA_ProjectWBSCVO.ProjectWBS> ();
        
        //get the Map for all the projects with the client tasks statuses.
        projectWBSMap=PRA_WBSService.getMapfromAllProjectsbyClientTaskStatus();        
        system.debug('----------projectWBSMap----------'+projectWBSMap); 
        
        //adding the values from the MAP to a list to dispaly on the Pageblocktable.
        ProjectsWBSList=projectWBSMap.values();
        system.debug('-------ProjectsWBSList----------'+ProjectsWBSList);         
    }
       
    /**
    *  Projectworkflow method is to get the wbs_project__c data for particiular project and navigate to the
    *  PRA_ProjectWorkflow page.          
    */
    public Pagereference  Projectworkflow(){
    	  	
	    //Make a pagereference. 
	    PageReference pageRef=null;
	    system.debug('-----ProjectName-------'+ProjectName);
	    
	    //Get the WBS Project record of the project.	  
	    WBS_Project__c wbsproject=new WBS_Project__c();
	    try{
		    wbsproject=PRA_WBSSErvice.getWBSProjectbyProjectName(ProjectName);		   
		    system.debug('------wbsproject------'+wbsproject);
		    
		    //Parse to the wbsproject ID to PRA_ProjectWorkflow page once the record is returned.
		    pageRef=new PageReference('/apex/PRA_ProjectWorkflow?id='+wbsproject.id);
		    pageRef.setRedirect(true);		           
	    }catch (Exception ex) {   
	        Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, ex.getMessage()));
	    }
    	return pageRef;
    } 
}