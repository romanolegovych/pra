public class SYS_TaskRedirectController{
    public String currentPage = null;
    private String retURL = null;
    private Id objectId = null;
    private Id parentId = null;
  //constructor
    public SYS_TaskRedirectController(ApexPages.StandardController controller){
        currentPage = ApexPages.currentPage().getUrl();
        retURL = ApexPages.currentPage().getParameters().get('retURL');
        objectId = Apexpages.currentPage().getparameters().get('id');
        parentId = Apexpages.currentPage().getparameters().get('what_id');
        parentId = Apexpages.currentPage().getparameters().get('who_id');
        system.debug('------retURL --------'+retURL );
        system.debug('------objectId --------'+objectId );
        system.debug('------parentId --------'+parentId  );
    }
    //return appropriate page redirect based on current page and sObjectType
    public PageReference taskRedirect(){
        PageReference taskPageReference;
        String sObjType = null;
        if(objectId != null){
            Task taskObj = [SELECT Id, WhatId
                       FROM Task
                       WHERE Id = :objectId
                       LIMIT 1];
            sObjType = String.ValueOf(taskObj.WhatId.getSObjectType());
            if(currentPage.contains('SYS_TaskRedirectViewPage')){
                if(sObjType == 'Issue__c'){
                    taskPageReference = new PageReference('/apex/PIT_TaskDetail?id=' + objectId +'&retURL=' + retURL + '&sfdc.override=1');
                }else{
                    taskPageReference = new PageReference('/'+ objectId + '?retURL=' + retURL + '&nooverride=1');
                }
            }else if(currentPage.contains('SYS_TaskRedirectEditPage')){
                if(sObjType == 'Issue__c'){
                    taskPageReference = new PageReference('/apex/PIT_TaskNewEdit?id=' + objectId +'&retURL=' + retURL + '&sfdc.override=1');
                }else{
                    taskPageReference = new PageReference('/'+ objectId + '/e?retURL=' + retURL + '&nooverride=1');
                }
            }else if(currentPage.contains('SYS_TaskRedirectClosePage')){
                if(sObjType == 'Issue__c'){
                    taskPageReference = new PageReference('/apex/PIT_TaskNewEdit?id=' + objectId +'&close=1&retURL=' + retURL + '&sfdc.override=1');
                }else{
                    taskPageReference = new PageReference('/'+ objectId + '/e?close=1&retURL=' + retURL + '&nooverride=1');
                }
            }
        }else if(parentId != null){
            sObjType = String.ValueOf(parentId.getSObjectType());
            system.debug('------retURL 1--------'+retURL );
            system.debug('------objectId 2--------'+objectId );
           system.debug('------parentId3 --------'+parentId  );
           system.debug('------sObjType --------'+sObjType );
            if(sObjType == 'Issue__c'){
                taskPageReference = new PageReference('/apex/PIT_TaskNewEdit?what_id=' + parentId +'&retURL=' + retURL + '&sfdc.override=1');
            }else{
                 system.debug('------parentId5--------'+parentId);
                taskPageReference = new PageReference('/00T/e?who_id=' + parentId +'&retURL=' + retURL + '&nooverride=1');
            }
        }else{
        system.debug('------parentId6--------'+parentId);
      taskPageReference = new PageReference('/00T/e?retURL=' + retURL + '&nooverride=1');
        }
        taskPageReference.setredirect(true);
        return taskPageReference;
    }
}