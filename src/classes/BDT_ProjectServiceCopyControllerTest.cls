@isTest
private class BDT_ProjectServiceCopyControllerTest {

    static testMethod void myUnitTest() {
    	List<Study__c> 			StudiesList = new List<Study__c>();
    	List<ProjectService__c> PrSrvList = new List<ProjectService__c>();
    	// Create test data
        BDT_CreateTestData.createall();
		Client_Project__c project = [select id from Client_Project__c limit 1];
		BDT_Utils.setPreference('SelectedProject', String.valueOf(project.id));
		String StudyIDs = '';
		for (Study__c st : [select id from Study__c where Project__c = :project.id]) {
			StudyIDs = StudyIDs + ':' + st.id;
			StudiesList.add(st);
		}
    	BDT_Utils.setPreference('SelectedStudies', StudyIDs.removeStart(':'));
		// Create the controller
		BDT_ProjectServiceCopyController p = new BDT_ProjectServiceCopyController();
		// Test run button with no selections to create no valid process
		p.getCopyOptions();
		p.CancelBack();
		p.ProjectSearchTerm = ''; 
		p.ButtonSearchProject(); 
		p.testCopyStudyServices();
		if(StudiesList.size()>0) {
			p.SelectedSourceStudy = StudiesList[0].id;
		}
		p.fillTargetStudySelectOption();
		p.testCopyStudyServices();
		// Test run button with selections
		if(StudiesList.size()>1)
		{
			if(StudiesList.size()>2) {
				p.SelectedTargetStudy = StudiesList[1].id;
				PrSrvList = [SELECT id FROM ProjectService__c LIMIT 2];
				if(PrSrvList.size() > 1)
				{
					StudyService__c newSource   = new StudyService__c();
					newSource.NumberOfUnits__c  = 4;
					newSource.ProjectService__c = PrSrvList[0].id;
					newSource.Study__c		 	= p.SelectedSourceStudy;
					insert newSource;
					StudyService__c newTarget   = new StudyService__c();
					newTarget.NumberOfUnits__c  = 1;
					newTarget.ProjectService__c = PrSrvList[0].id;
					newTarget.Study__c		 	= p.SelectedTargetStudy;
					insert newTarget;
					newSource   = new StudyService__c();
					newSource.NumberOfUnits__c  = 5;
					newSource.ProjectService__c = PrSrvList[1].id;
					newSource.Study__c		 	= p.SelectedSourceStudy;
					insert newSource;
					p.SelectedCopyMethod = 'a';
					p.testCopyStudyServices();
					p.cancel();
					p.SelectedCopyMethod = 'b';
					p.testCopyStudyServices();
					p.cancel();
					p.SelectedCopyMethod = 'c';
					p.testCopyStudyServices();
					p.ApplyTestRun();	
				}	
			}
		}
		p.ProjectSearchTerm = '001'; 
		p.ButtonSearchProject(); 
    }
}