/**
*   'PAWS_WorkflowProgressControllerTests' is the test class for PAWS_WorkflowProgressController
*   Test classes are used to write the unit test and cover the code coverage of the code.
*   @author   Work Relay
*/
@isTest 
public with sharing class PAWS_WorkflowProgressControllerTests 
{
    static testmethod void run()
    {
        PAWS_ApexTestsEnvironment.init();
        
        STSWR1__Flow_Step_Connection__c connection = PAWS_ApexTestsEnvironment.FlowStepConnection; 
        STSWR1__Flow_Instance_Cursor__c cursor = PAWS_ApexTestsEnvironment.FlowInstanceCursor;   
        
        STSWR1__Flow__c subFlow = PAWS_ApexTestsEnvironment.Flow.clone(false, false, false);
        subFlow.STSWR1__Start_Type__c = 'Triggered by another flow';
        insert subFlow;
        
        STSWR1__Flow_Swimlane__c subFlowSwimlane = PAWS_ApexTestsEnvironment.FlowSwimlane.clone(false, false, false);
        subFlowSwimlane.STSWR1__Flow__c = subFlow.Id;
        insert subFlowSwimlane;
        
        STSWR1__Flow_Milestone__c subFlowMilestone = PAWS_ApexTestsEnvironment.FlowMilestone.clone(false, false, false);
        subFlowMilestone.STSWR1__Flow__c = subFlow.Id;
        insert subFlowMilestone;
        
        STSWR1__Flow_Branch__c subFlowBranch = PAWS_ApexTestsEnvironment.FlowBranch.clone(false, false, false);
        subFlowBranch.STSWR1__Flow__c = subFlow.Id;
        insert subFlowBranch;
        
        STSWR1__Flow_Step_Junction__c subFlowStep = PAWS_ApexTestsEnvironment.FlowStep.clone(false, false, false);
        subFlowStep.STSWR1__Flow__c = subFlow.Id;
        subFlowStep.STSWR1__Flow_Branch__c = subFlowBranch.Id;
        subFlowStep.STSWR1__Flow_Swimlane__c = subFlowSwimlane.Id;
        subFlowStep.STSWR1__Flow_Milestone__c = subFlowMilestone.Id;
        insert subFlowStep;
        
        STSWR1__Flow_Instance__c subFlowInstance = PAWS_ApexTestsEnvironment.FlowInstance.clone(false, false, false);
        subFlowInstance.STSWR1__Flow__c = subFlow.Id;
        subFlowInstance.STSWR1__Parent__c = cursor.STSWR1__Flow_Instance__c;
        subFlowInstance.STSWR1__Parent_Step__c = cursor.STSWR1__Step__c;
        insert subFlowInstance;
        
        STSWR1__Flow_Instance_Cursor__c subFlowCursor = PAWS_ApexTestsEnvironment.FlowInstanceCursor.clone(false, false, false);
        subFlowCursor.STSWR1__Flow_Instance__c = subFlowInstance.Id;
        subFlowCursor.STSWR1__Step__c = subFlowStep.Id;
        insert subFlowCursor;
        
        
        STSWR1__Flow_Step_Action__c action = new STSWR1__Flow_Step_Action__c();
        action.STSWR1__Flow_Step__c = PAWS_ApexTestsEnvironment.FlowStep.Id;
        action.STSWR1__Config__c = JSON.serialize(new Map<String, Object>{'flowId' => subFlow.Id});
        action.STSWR1__Type__c = 'Start Sub Flow';
        insert action;
        
        PAWS_WorkflowProgressController controller = new PAWS_WorkflowProgressController();
        controller.FlowInstanceID = PAWS_ApexTestsEnvironment.FlowInstanceCursor.STSWR1__Flow_Instance__c;
        controller.ReinitFlag = DateTime.now();
        
        System.assert(controller.Instances != null);
        System.assert(controller.FlowInstance != null);
        
        System.assert(controller.Instances[0].FlowInstance != null);
        System.assert(controller.Instances[0].Steps != null);
        controller.Instances[0].refresh();
    }
}