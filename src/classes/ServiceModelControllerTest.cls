@isTest
private class ServiceModelControllerTest {
    
    private static Service_Model__c serviceModel;
    private static Service_Area__c serviceArea;
    private static Service_Function__c serviceFunction;
    private static Service_Task__c serviceTask;
    private static Service_Task_To_Drivers__c serviceTaskToDrivers;
    private static Service_Task_To_Service_Impact__c serviceTaskToServiceImpact;
    private static ServiceModelController cont;

    static void createTestData() {
        PBB_TestUtils testUtils = new PBB_TestUtils();
        serviceModel = testUtils.CreateServiceModelAttributes();
        serviceArea = testUtils.createServiceAreaAttributes();
        serviceFunction = testUtils.createServiceFunctionAttributes();
        serviceTask = testUtils.createServiceTaskAttributes();
        Formula_Name__c billFormula = testUtils.createBillFormulaNames(); 
        serviceTaskToDrivers = testUtils.createSTtoBF(serviceTask, billFormula);
        Service_Impact_Question_Name__c siqn = testUtils.createSIQNAttributes();
        serviceTaskToServiceImpact = testUtils.createSTtoSI(serviceTask, siqn); 
        
        cont = new ServiceModelController();
    }
    
    @isTest
    static void constructorTest(){
        Test.startTest();
            cont = new ServiceModelController();
            ServiceModelController thisCont = cont.getThis();
        Test.stopTest();
        System.assertEquals(thisCont, cont);

    }

    @isTest
    static void getSelectServiceObjectServiceModelTest() {
    	createTestData();
        cont.serviceId = serviceModel.Id;
        Test.startTest();
            String objectAPIName = cont.getSelectServiceObject();
        Test.stopTest();
        System.assertEquals(cont.serviceModel.serviceId, serviceModel.Id);
        System.assertEquals(objectAPIName, cont.getServiceModelObjectApiName);
        System.assert(cont.serviceModel.serviceModels.size() > 0);
        System.assertEquals(cont.serviceFunction, null);
        System.assertEquals(cont.serviceTask, null);
    }

	@isTest
    static void getSelectServiceObjectServiceIdNullTest() {
    	createTestData();
        cont.serviceId = null;
        Test.startTest();
            String objectAPIName = cont.getSelectServiceObject();
        Test.stopTest();
        System.assert(cont.serviceModel.serviceModels.size() > 0);
        System.assertEquals(objectAPIName, cont.getServiceModelObjectApiName);
        System.assertEquals(cont.serviceArea, null);
    }

    @isTest
    static void getSelectServiceObjectServiceAreaTest() {
    	createTestData();
        cont.serviceId = serviceArea.Id;
        cont.serviceArea = new ServiceArea(serviceModel.Id);
        Test.startTest();
            String objectAPIName = cont.getSelectServiceObject();
        Test.stopTest();
        System.assertEquals(cont.serviceArea.serviceId, serviceArea.Id);
        System.assertEquals(objectAPIName, cont.getServiceAreaObjectApiName);
        System.assert(cont.serviceArea.serviceAreas.size() > 0);
        System.assertEquals(cont.serviceTask, null);
    }

    @isTest
    static void refreshServiceObjectServiceModelTest() {
    	createTestData();
        cont.serviceId = serviceModel.Id;
        cont.getSelectServiceObject();
        Test.startTest();
            cont.refreshServiceObject();
        Test.stopTest();
        System.assert(cont.serviceArea.serviceAreas.size() > 0);
    }

    @isTest
    static void refreshServiceObjectServiceAreaTest() {
    	createTestData();
        cont.serviceId = serviceModel.Id;
        cont.getSelectServiceObject();
        cont.refreshServiceObject();
        cont.serviceId = serviceArea.Id;
        Test.startTest();
            cont.refreshServiceObject();
        Test.stopTest();
        System.assert(cont.serviceFunction.serviceFunctions.size() > 0);
    }

    @isTest
    static void refreshServiceObjectServiceFunctionTest() {
    	createTestData();
        cont.serviceId = serviceModel.Id;
        cont.getSelectServiceObject();
        cont.refreshServiceObject();

        cont.serviceId = serviceArea.Id;
        cont.getSelectServiceObject();
        cont.refreshServiceObject();

        cont.serviceId = serviceFunction.Id;
        cont.getSelectServiceObject();
        Test.startTest();
            cont.refreshServiceObject();
        Test.stopTest();
        System.assert(cont.serviceTask.serviceTasks.size() > 0);
    }

    @isTest
    static void refreshServiceObjectServiceTaskTest() {
    	createTestData();
        cont.serviceId = serviceModel.Id;
        cont.getSelectServiceObject();
        cont.refreshServiceObject();

        cont.serviceId = serviceArea.Id;
        cont.getSelectServiceObject();
        cont.refreshServiceObject();

        cont.serviceId = serviceFunction.Id;
        cont.getSelectServiceObject();
        cont.refreshServiceObject();

        cont.serviceId = serviceTask.Id;
        cont.getSelectServiceObject();
        Test.startTest();
            cont.refreshServiceObject();
        Test.stopTest();
        System.assert(cont.serviceTaskToServiceImpact.serviceTaskToServiceImpacts.size() > 0);
        System.assert(cont.serviceTaskToDrivers.serviceTaskToDriversList.size() > 0);
    }

    @isTest
    static void refreshServiceObjectServiceIdNullTest() {
    	createTestData();
        cont.serviceId = null;
        cont.getSelectServiceObject();
        Test.startTest();
            cont.refreshServiceObject();
        Test.stopTest();
    }

    @isTest
    static void getDetailServiceTest() {
    	createTestData();
        cont.serviceId = serviceModel.Id;
        Test.startTest();
            cont.getDetailService();
        Test.stopTest();
        System.assertEquals(cont.serviceModel.serviceId, serviceModel.Id);
    }
	
    @isTest
    static void preparationCreateOrEditServiceTest() {
    	createTestData();
    	ApexPages.currentPage().getParameters().put('editServiceModelId', serviceModel.Id);
        cont.serviceId = serviceModel.Id;
        cont.createObjectAPIName = cont.getServiceModelObjectApiName;
        cont.getDetailService();
        Test.startTest();
            cont.preparationCreateOrEditService();
        Test.stopTest();
        System.assertEquals(cont.serviceModel.serviceModelForUpdate.Id, serviceModel.Id);
    }

	@isTest
    static void createServiceTest() {
    	createTestData();
        cont.serviceId = serviceModel.Id;
        cont.getDetailService();
        cont.createObjectAPIName = cont.getServiceModelObjectApiName;
        cont.preparationCreateOrEditService();
        cont.serviceModel.serviceModelForUpdate.Name = 'new Name';
        Test.startTest();
            cont.createService();
        Test.stopTest();
        System.assertNotEquals(cont.serviceModel.serviceModel.Name, 'new Name');
        Service_Model__c newServiceModel = [SELECT Name FROM Service_Model__c WHERE Name = 'new Name' LIMIT 1];
        System.assertEquals(newServiceModel.Name, 'new Name');
    }

    @isTest
    static void updateServiceTest() {
    	createTestData();
    	ApexPages.currentPage().getParameters().put('editServiceModelId', serviceModel.Id);
        cont.serviceId = serviceModel.Id;
        cont.getDetailService();
        cont.createObjectAPIName = cont.getServiceModelObjectApiName;
        cont.preparationCreateOrEditService();
        cont.serviceModel.serviceModelForUpdate.Name = 'new Name';
        Test.startTest();
            cont.updateService();
        Test.stopTest();
        System.assertEquals(cont.serviceModel.serviceModel.Name, 'new Name');
    }

	@isTest
    static void cancelServiceTest() {
    	createTestData();
        cont.serviceId = serviceModel.Id;
        cont.getDetailService();
        cont.createObjectAPIName = cont.getServiceModelObjectApiName;
        cont.preparationCreateOrEditService();
        cont.serviceModel.serviceModelForUpdate.Name = 'new Name';
        Test.startTest();
            cont.cancelService();
        Test.stopTest();
        System.assertNotEquals(cont.serviceModel.serviceModel.Name, 'new Name');
    }

    @isTest
    static void removeServiceTest() {
    	createTestData();
        cont.serviceId = serviceModel.Id;
        cont.createObjectAPIName = cont.getServiceModelObjectApiName;
        cont.getDetailService();
        Test.startTest();
            cont.removeService();
        Test.stopTest();
        System.assertEquals(cont.serviceId, null);
    }

    @isTest
    static void getObjectApiNameTest() {
    	createTestData();
    	cont.serviceId = serviceModel.Id;
    	System.assertEquals(cont.isReadOnly, false);
    	System.assertEquals(cont.getServiceModelObjectApiName, ServiceModelController.SERVICE_MODEL);
    	System.assertEquals(cont.getServiceAreaObjectApiName, ServiceModelController.SERVICE_AREA);
    	System.assertEquals(cont.getServiceFunctionObjectApiName, ServiceModelController.SERVICE_FUNCTION);
    	System.assertEquals(cont.getServiceTaskObjectApiName, ServiceModelController.SERVICE_TASK);
    	System.assertEquals(cont.getServiceTaskToServiceImpactObjectApiName, ServiceModelController.SERVICE_TASK_TO_SERVICE_IMPACT);
    	System.assertEquals(cont.getServiceTaskToDriversObjectApiName, ServiceModelController.SERVICE_TASK_TO_DRIVERS);
    }
}