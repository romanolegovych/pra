public class LMS_ToolsDataAccessor {

	private static String draft{get;set;}
	private static String draftDelete{get;set;}
	private static String committed{get;set;}
	private static String pendingAdd{get;set;}
	private static String pendingDelete{get;set;}

	static {
		Map<String, LMSConstantSettings__c> constants = LMSConstantSettings__c.getAll();
        draft = constants.get('statusDraft').Value__c;
        committed = constants.get('statusCommitted').Value__c;
        draftDelete = constants.get('statusDraftDelete').Value__c;
        pendingAdd = constants.get('statusPendingAdd').Value__c;
        pendingDelete = constants.get('statusPendingDelete').Value__c;
	}

	/**** Methods for calculating impact of adding courses/people to roles ****/

	public static Decimal getTotalCourseDurationForCommittedMappings(String roleId) {
		Decimal duration;
		List<AggregateResult> totalDuration = 
		[SELECT SUM(Course_Id__r.Duration__c) FROM LMS_Role_Course__c WHERE Role_Id__r.Id = :roleId AND (Status__c = :committed or Status__c = :draftDelete)];
		if(totalDuration.size() > 0 && totalDuration[0].get('expr0') != null) {
			duration = Decimal.valueOf(Double.valueOf(totalDuration[0].get('expr0')));
		}
		return duration;
	}
	public static Decimal getTotalCourseDurationForAddMappings(String roleId) {
		Decimal duration;
		List<AggregateResult> totalDuration = 
		[SELECT SUM(Course_Id__r.Duration__c) FROM LMS_Role_Course__c WHERE Role_Id__r.Id = :roleId AND (Status__c = :draft or Status__c = :pendingAdd)];
		if(totalDuration.size() > 0 && totalDuration[0].get('expr0') != null) {
			duration = Decimal.valueOf(Double.valueOf(totalDuration[0].get('expr0')));
		}
		return duration;
	}
	public static Decimal getTotalCourseDurationForDeleteMappings(String roleId) {
		Decimal duration;
		List<AggregateResult> totalDuration = 
		[SELECT SUM(Course_Id__r.Duration__c) FROM LMS_Role_Course__c WHERE Role_Id__r.Id = :roleId AND (Status__c = :draftDelete or Status__c = :pendingDelete)];
		if(totalDuration.size() > 0 && totalDuration[0].get('expr0') != null) {
			duration = Decimal.valueOf(Double.valueOf(totalDuration[0].get('expr0')));
		}
		return duration;
	}
	public static Decimal getTotalCourseDurationFromCourseList(List<String> courseIds) {
		Decimal duration;
		List<AggregateResult> totalDuration = 
		[SELECT SUM(Duration__c) FROM LMS_Course__c WHERE Id IN :courseIds];
		if(totalDuration.size() > 0 && totalDuration[0].get('expr0') != null) {
			duration = Decimal.valueOf(Double.valueOf(totalDuration[0].get('expr0')));
		}
		return duration;
	}
	public static Decimal getTotalCourseDurationFromCourseId(String courseId) {
		Decimal duration = 0;
		List<AggregateResult> totalDuration = 
		[SELECT SUM(Duration__c) FROM LMS_Course__c WHERE Id = :courseId];
		if(totalDuration != null && totalDuration[0].get('expr0') != null && totalDuration.size() > 0) {
			duration = Decimal.valueOf(Double.valueOf(totalDuration[0].get('expr0')));
		}
		return duration;
	}
	public static Map<String, String> getCommittedMapFromRoleIds(Set<String> roleIds) {
		Map<String, String> commEmps = new Map<String, String>();
        List<LMS_Role_Employee__c> commEmp = [SELECT Employee_Id__c,Status__c from LMS_Role_Employee__c WHERE Role_Id__c IN :roleIds];
        for(LMS_Role_Employee__c re : commEmp) {
        	if(!commEmps.containsKey(re.Employee_Id__c)) {
        		commEmps.put(re.Employee_Id__c, re.Status__c);
        	}
        }
		return commEmps;
	}
	public static Map<String, String> getNoDraftMapFromRoleIds(Set<String> roleIds) {
		Map<String, String> noDraft = new Map<String, String>();
        List<LMS_Role_Employee__c> commEmp = [SELECT Employee_Id__c,Status__c from LMS_Role_Employee__c WHERE Role_Id__c IN :roleIds];
        for(LMS_Role_Employee__c re : commEmp) {
        	if(!noDraft.containsKey(re.Employee_Id__c)) {
        		noDraft.put(re.Employee_Id__c, re.Status__c);
        	}
        }
		return noDraft;
	}
	public static Integer getTotalEmployeeCountForAddMappings(Set<String> roleIds, Map<String, String> employeeMap) {
		Integer employeeCount;
		List<LMS_Role_Employee__c> employees = [SELECT Employee_Id__c, Status__c FROM LMS_Role_Employee__c WHERE Role_Id__c = :roleIds];
		Set<Id> draftIds = new Set<Id>();
		for(LMS_Role_Employee__c re : employees) {
			draftIds.add(re.Employee_Id__c);
		}
		List<LMS_Role_Employee__c> draftAdds = [SELECT Employee_Id__c FROM LMS_Role_Employee__c WHERE Role_Id__c IN :roleIds AND Employee_Id__c IN :draftIds];
		Set<Id> excludeEmps = new Set<Id>();
        for(LMS_Role_Employee__c re : draftAdds)
            if(!employeeMap.containsKey(re.Employee_Id__c))
                excludeEmps.add(re.Employee_Id__c);

        List<AggregateResult> empAggregate = [SELECT COUNT(Name) FROM Employee_Details__c WHERE Id IN :excludeEmps];
        if(empAggregate.size() > 0 && empAggregate[0].get('expr0') != null) {
			employeeCount = Integer.valueOf(empAggregate[0].get('expr0'));
		}
		return employeeCount;
	}
	public static Integer getTotalEmployeeCountForDeleteMappings(String courseId, Set<String> roleIds) {
		Integer employeeCount;
		List<String> statusToCheck = new List<String>{draftDelete, pendingDelete};
		List<LMS_Role_Course__c> draftDeleteList = [SELECT Role_Id__c FROM LMS_Role_Course__c WHERE Course_Id__c = :courseId AND Status__c IN :statusToCheck];
		Set<Id> draftDeletes = new Set<Id>();
		for(LMS_Role_Course__c rc : draftDeleteList) {
			draftDeletes.add(rc.Role_Id__c);
		}

		List<LMS_Role_Employee__c> employees = [SELECT Employee_Id__c FROM LMS_Role_Employee__c WHERE Role_Id__c = :draftDeletes];
		Set<Id> deleteEmps = new Set<Id>();
        for(LMS_Role_Employee__c re : employees)
        	deleteEmps.add(re.Employee_Id__c);

        List<AggregateResult> empAggregate = [SELECT COUNT(Name) FROM Employee_Details__c WHERE Id IN :deleteEmps];
        if(empAggregate.size() > 0 && empAggregate[0].get('expr0') != null) {
			employeeCount = Integer.valueOf(empAggregate[0].get('expr0'));
		}
		return employeeCount;
	}

	/**** Methods to check role validity/uniqueness ****/
	public static Boolean isRoleValid(String roleType, String[] parameters) {
		Boolean isValid = false;
		if(roleType == 'PRA') {
			isValid = isPraRoleValid(parameters);
		} else if(roleType == 'PST') {
			isValid = isPstRoleValid(parameters);
		} else if(roleType == 'Additional Role') {
			isValid = isAdditionalRoleValid(parameters);
		}
		return isValid;
	}

	private static Boolean isPraRoleValid(String[] parameters) {
		Boolean isValid = false;
		String jobClass = parameters.get(0);
		String jobTitle = parameters.get(1);
		String businessUnit = parameters.get(2);
		String department = parameters.get(3);
		String region = parameters.get(4);
		String country = parameters.get(5);
		String employeeType = parameters.get(6);

		// Query database to determine if role already exists
		String query = 'select Id from LMS_Role__c where Id != null ';
		query+= jobClass != null ? 'and Job_Class_Desc__r.Name = :jobClass ' : jobTitle != null ? '' : 'and Job_Class_Desc__c = null ';
		query+= jobTitle != null ? 'and Job_Title__r.Job_Title__c = :jobTitle ' : 'and Job_Title__c = null ';
		query+= businessUnit != null ? 'and Business_Unit__r.Name = :businessUnit ' : 'and Business_Unit__c = null ';
		query+= department != null ? 'and Department__r.Name = :department ' : 'and Department__c = null ';
		query+= region != null ? 'and Region__r.Region_Name__c = :region ' : country != null ? '' : 'and Region__c = null ';
		query+= country != null ? 'and Country__r.Name = :country ' : 'and Country__c = null ';
		query+= employeeType != null ? 'and Employee_Type__r.Name = :employeeType' : 'and Employee_Type__c = null ';

		List<LMS_Role__c> roles = Database.query(query);
		system.debug('--------------------role scan size---------------------' + roles.size());
		if(roles != null && roles.size() > 0) {
			return isValid;
		} else {
			isValid = true;
		}

		return isValid;
	}

	private static Boolean isPstRoleValid(String[] parameters) {
		Boolean isValid = false;
		String client = parameters.get(0);
		String project = parameters.get(1);
		String role = parameters.get(2);

		// Query database to determine if role already exists
		String query = 'select Id from LMS_Role__c where Id != null ';
		query+= client != '' ? 'and Client_Id__r.Name = :client ' : '';
		query+= project != '' ? 'and Project_Id__r.Name = :project ' : '';
		query+= role != null ? 'and (Job_Class_Desc__r.Name = :role or PST_Adhoc_Role__r.PST_Adhoc_Role__c = :role)' : '';

		List<LMS_Role__c> roles = Database.query(query);
		system.debug('--------------------role scan size---------------------' + roles.size());
		if(roles != null && roles.size() > 0) {
			return isValid;
		} else {
			isValid = true;
		}

		return isValid;
	}

	private static Boolean isAdditionalRoleValid(String[] parameters) {
		Boolean isValid = false;
		String adhocName = parameters.get(0);

		// Query database to determine if role already exists
		String query = 'select Id from LMS_Role__c where Id != null ';
		query+= adhocName != '' ? 'and Adhoc_Role_Name__c = :adhocName' : '';

		List<LMS_Role__c> roles = Database.query(query);
		system.debug('--------------------role scan size---------------------' + roles.size());
		if(roles != null && roles.size() > 0) {
			return isValid;
		} else {
			isValid = true;
		}

		return isValid;
	}
	
	/**** Methods for threshold alert queries *****/
	public static Integer getPendingRoleCourseUpdates() {
		list<LMS_Role_Course__c> updates = 
			[select Id from LMS_Role_Course__c where 
				(Status__c = 'Pending - Add' AND Course_Id__r.Available_From__c <= tomorrow AND Commit_Date__c = null) OR 
				(Status__c = 'Pending - Add' AND Commit_Date__c <= tomorrow) OR 
				(Status__c = 'Removed') OR 
				(Status__c = 'Pending - Delete' AND Commit_Date__c <= tomorrow)];
		if(updates != null)
			return updates.size();
		return 0;
	}
	public static Integer getPendingRoleEmployeeUpdates() {
		list<LMS_Role_Employee__c> updates = 
			[select Id from LMS_Role_Employee__c where 
				(Sync__c = 'N' AND Status__c = 'Draft' AND Role_Id__r.Role_Type__r.Name = 'PRA') OR 
	    		(Sync__c = 'N' AND Status__c = 'Pending - Add' AND Commit_Date__c <= tomorrow) OR
	    		(Sync__c = 'N' AND Status__c = 'Transit - Avail') OR
	    		(Sync__c = 'Y' AND Status__c = 'Removed') OR
        		(Sync__c = 'Y' AND Status__c = 'Pending - Delete' AND Commit_Date__c <= tomorrow)];
		if(updates != null)
			return updates.size();
		return 0;
	}
	public static Integer getTransitAddRoleEmployees() {
		list<LMS_Role_Employee__c> transitAdds = [select Id from LMS_Role_Employee__c where Status__c = 'Transit - Add'];
		if(transitAdds != null) 
			return transitAdds.size();
		return 0;
	}
	public static Integer getTransitDeleteRoleEmployees() {
		list<LMS_Role_Employee__c> transitDeletes = [select Id from LMS_Role_Employee__c where Status__c = 'Transit - Delete'];
		if(transitDeletes != null) 
			return transitDeletes.size();
		return 0;
	}

	/**** Methods to find profile and determine permissions ****/

	public static String getProfileNameFromId(String id) {
		Profile p = [SELECT Name FROM Profile WHERE Id = :id];
		return p.Name;
	}
	public static Boolean isClearAdmin(String profile) {
		if(LMSClearAdminGroup__c.getValues(profile) != null) {
			return true;
		} else {
			return false;
		}
	}
	public static Boolean isRoleAdmin(String profile) {
		if(LMSRoleAdminGroup__c.getValues(profile) != null) {
			return true;
		} else {
			return false;
		}
	}
	public static Boolean isPRAAdmin(String profile) {
		if(LMSPRARoleAdminGroup__c.getValues(profile) != null) {
			return true;
		} else {
			return false;
		}
	}
	public static Boolean isPRAAdhocAdmin(String profile) {
		if(LMSPRAAdhocAdminGroup__c.getValues(profile) != null) {
			return true;
		} else {
			return false;
		}
	}
	public static Boolean isPSTAdmin(String profile) {

		if(LMSPSTRoleAdminGroup__c.getValues(profile) != null) {
			return true;
		} else {
			return false;
		}
	}
	public static Boolean isAdhocAdmin(String profile) {	
		if(LMSAdhocRoleAdminGroup__c.getValues(profile) != null) {
			return true;
		} else {
			return false;
		}
	}
}