public with sharing class RM_EmployeeWorkHistoryController {
	private final list<WorkHistory> histDate;
	public string emplID {get; set;}
	
	public RM_EmployeeWorkHistoryController(){
		 emplID = ApexPages.currentPage().getParameters().get('id');
		 histDate = GetHistoryDate(emplID);
	}
	public class WorkHistory
    {
        public string Project_id {get; private set;}
        public double Total_work_hr {get; private set;}
        public string Start_date {get; private set;}
        public string End_date {get; private set;}
        public string Sponsor {get; set;}
        public string Buf_code {get; private set;}
        
        public WorkHistory(WFM_EE_Work_History_Summary__c his)
        {
            Project_id = his.project__r.name;
            Total_work_hr = his.Sum_Worked_Hours__c;
            Start_date = his.Start_Year_Month__c;
            End_date=his.End_Year_Month__c;
            Buf_code=his.Buf_Code__c;
            Sponsor= his.project__r.Contract_id__r.Client_ID__r.name;
        }               
    }
    
    private list<WorkHistory> GetHistoryDate(string emplID){
        list<WFM_EE_Work_History_Summary__c> HistSummary = RM_EmployeeService.GetWorkHistorySummarybyEmployeeRID(emplID);
        list<WorkHistory> hist = new List<WorkHistory>();      
        
        for (WFM_EE_Work_History_Summary__c hisSumm : HistSummary)        
                hist.add(new WorkHistory(hisSumm)); 
        return hist;       
    }
     public list<WorkHistory> GetHistory(){
        return histDate;
     }
}