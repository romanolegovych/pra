public virtual with sharing class PRA_BaseDataAccessor {
    public class DataAccessorException extends Exception {}

    public PRA_BaseDataAccessor() {
    }
}