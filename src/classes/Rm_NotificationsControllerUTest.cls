@isTest 
private class Rm_NotificationsControllerUTest {

	static list<WFM_Notification__c> lstNotification;
	static list<WFM_Notification_view_log__c> lstLog;
	static WFM_Client__c wfcClientObj;
	static WFM_Contract__c wfmContractObj;
	static List<WFM_Project__c> wfmProjectObjList;
	
	static void init(){
		wfcClientObj = new WFM_Client__c();
        wfcClientObj.Name='tst';
        wfcClientObj.Client_Unique_Key__c = 'ssesss';
        insert wfcClientObj;              //insert wfcClientObj
            
        wfmContractObj = new WFM_Contract__c ();
        wfmContractObj.Name='sess';
        wfmContractObj.Contract_Unique_Key__c = 'sr123222';
        wfmContractObj.Client_ID__c = wfcClientObj.id;  
        insert wfmContractObj;                          //insert  wfmContractObj    
            
        //for bulk operation
        wfmProjectObjList = new List<WFM_Project__c>();
        String prjName = 'Test New Project' ;      
        for(Integer i = 0; i < 5; i++) 
        {
            WFM_Project__c wfmProjectObj = new WFM_Project__c ();
            wfmProjectObj.name = prjName+i;
            wfmProjectObj.Contract_ID__c = wfmContractObj.id;
            wfmProjectObj.Project_Unique_Key__c = 'seees'+i;
            wfmProjectObj.Status_Desc__c = 'oldStatus';      
            wfmProjectObj.Project_End_Date__c = System.Today();
            wfmProjectObjList.add(wfmProjectObj); 
            insert wfmProjectObj;
            //insert wfmProjectObjList;             //insert wfmProjectObj
                
            List<WFM_Project__c> insertedwfmProjectObjList= [select Id, Name,Status_Desc__c,Project_End_Date__c From WFM_Project__c where Name like: prjName + '%'];
        
            for(i = 0; i < insertedwfmProjectObjList.size(); i++) {
            insertedwfmProjectObjList[i].Status_Desc__c = 'newStatus'+i;      
            insertedwfmProjectObjList[i].Project_End_Date__c = System.Today()+i;
            }
            
            update insertedwfmProjectObjList; 
        }
	}
    static testMethod void validateUpdatenotification() {
   		init();
        Test.startTest();
            
        lstNotification= RM_OtherService.getAllNotification();
        system.assert(lstNotification.size() > 0);    
        RM_Notificationscontroller rmNotificationclsObj = new RM_Notificationscontroller();
        list<RM_Notificationscontroller.PgNotificationRecords> lstPgNotification = new list<RM_Notificationscontroller.PgNotificationRecords>();
        rmNotificationclsObj.getwfmnotifications();
        lstPgNotification = rmNotificationclsObj.ListOfPageNotifications;
        system.assert(lstPgNotification.size() > 0); 
        Test.stopTest(); 
  	}
    static testMethod void testShowAll(){
     	init();
        Test.startTest();
        RM_Notificationscontroller rmNotificationclsObj = new RM_Notificationscontroller();
        list<RM_Notificationscontroller.PgNotificationRecords> lstPgNotification = new list<RM_Notificationscontroller.PgNotificationRecords>();
        rmNotificationclsObj.ShowAll();
        lstPgNotification = rmNotificationclsObj.ListOfPageNotifications;
        system.assert(lstPgNotification.size() > 0); 
        Test.stopTest(); 
    }
    static testMethod void testMarkAsRead(){
     	init();
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         	Test.startTest();
         	myFunc2();
         	RM_Notificationscontroller rmNotificationclsObj = new RM_Notificationscontroller();
        	list<RM_Notificationscontroller.PgNotificationRecords> lstPgNotification = new list<RM_Notificationscontroller.PgNotificationRecords>();
        	rmNotificationclsObj.ShowAll();
        	lstPgNotification = rmNotificationclsObj.ListOfPageNotifications;
        	lstPgNotification[0].isChecked = true;
        
        	rmNotificationclsObj.MarkedAsRead();
        	string UserId = UserInfo.getUserId();    
  
        	list<string> ListOfGroupNames = RM_OtherService.GetRMGroupfromUserID(UserId);
        	lstLog = RM_OtherService.getListNotificationViewLog(ListOfGroupNames, UserID);
       
        	system.assert(lstLog.size() > 0); 
         	Test.stopTest();
        }     
            
        
       
        
        
    }
     @future
    private static void myFunc2()
    {  Group g1 = new Group(name = 'RM_Resource_group',DeveloperName='RM_Resource_group');        
        insert g1;
        GroupMember gm1 = new GroupMember();
        gm1.GroupId=g1.id;
        gm1.UserOrGroupId=UserInfo.getUserId();
        database.insert(gm1); 
        system.debug('----g1-----'+gm1);
        
        PRA_Business_Unit__c PBU = new PRA_Business_Unit__c(name = 'Clinical Informatics',Business_Unit_Code__c='CI');        
        insert PBU; 
        
        WFM_BU_Group_Mapping__c bug=new WFM_BU_Group_Mapping__c();
        bug.PRA_Business_Unit__c=pbu.id;
        bug.Group_Name__c='RM_Resource_group';
        insert bug;
        
    }
       
    
}