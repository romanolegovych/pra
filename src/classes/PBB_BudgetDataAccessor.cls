/**
    @description  This class ecapsulates all data access methods for the PBB Budget data model
       
                     Objects: Scenario_Budget__c
                              Scenario_Budget_Task__c
                              Scenario_Budget_RoleCountry__c
*/
public with sharing class PBB_BudgetDataAccessor extends PRA_BaseDataAccessor {

    public PBB_BudgetDataAccessor()
    {
        super();
        
    }

    /**
     @description This method creates the initial set of records for a PBB scenario budget
    */
    public void createBudgetDataStruct(Scenario_Budget__c budget, 
                                       List<Scenario_Budget_Task__c> budgetTasks,
                                       List<Scenario_Budget_RoleCountry__c> budgetRoleCountries)
    {
        Database.SaveResult[] results;
        Database.SaveResult result;
        String theKey;
        List<String> keyParts;
        
        ID budgetID;
        
        Boolean isError;
        String strErr;
        
        try 
        {
            // Insert top-level Scenario_Budget__c record
            result = Database.insert(budget, false);
                    
            if (result <> null) {
                if (result.isSuccess()) {
                    budgetID = result.getID();
                }
                else {
                    strErr = 'Error inserting Scenario_Budget__c object: ' + result.getErrors();
                    throw new DataAccessorException(strErr);
                }
            }

            // Add Scenario_Budget__c ID to detail Scenario_Budget_Task__c records
            for (Scenario_Budget_Task__c sbt : budgetTasks)
            {
                sbt.Scenario_Budget__c = budgetID;
            }
            
            // Insert second-level Scenario_Budget_Task__c records
            results = Database.insert(budgetTasks ,false);
            
            if (results.size() <> budgetTasks.size()) {
                throw new DataAccessorException('Size mismatch between Budget Tasks loaded and Results');
            }
            
            // Check for successful insert
            isError = false;
            for (Database.SaveResult sr : results) {
                if (!sr.isSuccess()) {
                    strErr += sr.getErrors() + '<br>'; 
                    isError = true;
                }
            }
            if (isError) {
                throw new DataAccessorException('Error inserting Scenario_Budget_Task__c records: ' + strErr);
            }

            // Create map of external Scenario_Budget_Task__c ID to new SF IDs
            map<String, ID> mapBudgetTaskIDs = new map<String, ID>();
            for (Integer i = 0; i < results.size(); i++)
            {
                mapBudgetTaskIDs.put(budgetTasks[i].Budget_Task_ID__c, results[i].getID());
            }
            
            // Add Scenario_Budget_Task__c ID to Scenario_Budget_RoleCountry__c records
            for (Scenario_Budget_RoleCountry__c roleCountry : budgetRoleCountries)
            {
                keyParts = roleCountry.Budget_RoleCountry_ID__c.split(':');
                system.debug('---- keyParts ' + keyparts);
                theKey = keyParts[0] + ':' + keyParts[1] + ':' + keyParts[2];
                system.debug('---- theKey ' + theKey);
                roleCountry.Scenario_Budget_Task__c = mapBudgetTaskIDs.get(theKey);
            }
            
            // insert third level Scenario_Budget_RoleCountry__c records
            results = Database.insert(budgetRoleCountries ,false);
            
            if (results.size() <> budgetRoleCountries.size()) {
                throw new DataAccessorException('Size mismatch between Budget RoleCountries loaded and Results');
            }
            
            // Check for successful insert
            isError = false;
            for (Database.SaveResult sr : results) {
                if (!sr.isSuccess()) {
                    strErr += sr.getErrors() + '<br>'; 
                    isError = true;
                }
            }
            if (isError) {
                throw new DataAccessorException('Error inserting Scenario_Budget_RoleCountry__c records: ' + strErr);
            }
        }
        catch (exception ex)
        {
            throw ex;
        }       
    }

    /**
      @Description Method returns a list of Scenario_Budget__c records that are flagged
                 as active given a PBB Scenario ID
    */
    public static list<Scenario_Budget__c> getActiveBudgets(ID pbbScenarioId) {
        return [SELECT id, active__c
                FROM Scenario_Budget__c
                WHERE PBB_Scenario__r.ID = :pbbScenarioID
                AND active__c = true];
    }
    
    /**
    @ Desc Method returns a map of IDs and Job Class Codes based on a set of IDs
    */
    public static map<ID, Job_Class_Desc__c> getJobCodes(set<ID> jobClassIDs) {
        map<ID, Job_Class_Desc__c> rtnMap =  new map<ID, Job_Class_Desc__c>(
            [SELECT ID, Job_Class_Code__c
             FROM Job_Class_Desc__c
             WHERE id IN :jobClassIDs]);
        return rtnMap;
    }
    
    public static map<String, Job_Class_Desc__c> getJobIds(set<String> aJobCodes) {
        map<String, Job_Class_Desc__c> rtnMap = new map<String, Job_Class_Desc__c>();
        
        for (Job_Class_Desc__c jcd : 
            [SELECT ID, Job_Class_Code__c
             FROM Job_Class_Desc__c
             WHERE Job_Class_Code__c IN :aJobCodes]) {

            rtnMap.put(jcd.Job_Class_Code__c, jcd);
        }
        return rtnMap;
    }
    
    public static void getMappedJobs(set<String> aJobCodes, map<ID, Job_Class_Desc__c> aMapIdToJob, map<String, Job_Class_Desc__c> aMapJobcodeToJob) {
        for (Job_Class_Desc__c jcd : 
            [SELECT ID, Job_Class_Code__c, Name
             FROM Job_Class_Desc__c
             WHERE Job_Class_Code__c IN :aJobCodes]) {

             aMapIdToJob.put(jcd.id, jcd);
             aMapJobcodeToJob.put(jcd.Job_Class_Code__c, jcd);
        }
    }

    /**
    @ Desc Method returns a map of IDs and Country Codes based on a set of IDs
    */
    public static map<ID, Country__c> getCountryCodes(set<ID> countryIDs)
    {
        map<ID, Country__c> rtnMap = new map<ID, Country__c>(
            [SELECT ID, Country_Code__c
             FROM Country__c
             WHERE id IN :countryIDs]);
        return rtnMap;
    }
    
    /**
    @description Method returns a list of Scenario Budget RoleCountry records and
                 their related Countries Service Tasks records based on a set of
                 countries_service_tasks__c IDs
    */
    public static list<Scenario_Budget_RoleCountry__c> getRoleCountriesForCountryTasks(Set<ID> aSetCountryServiceTasks)
    {
        list<Scenario_Budget_RoleCountry__c> listRtn =
            [SELECT Effort_Impact_Hours__c, Effort_Impact_Percent__c,
                    Countries_Service_Tasks__r.Total_Effort_Impact_by_Hours__c,
                    Countries_Service_Tasks__r.Total_Effort_Impact_by_Percentage__c
             FROM   Scenario_Budget_RoleCountry__c
             WHERE  Countries_Service_Tasks__r.ID IN :aSetCountryServiceTasks
             AND    Scenario_Budget_Task__r.Scenario_Budget__r.Active__c = true];
                
        return listRtn;
    }

    /**
    @author Ramya
    @date 2015
    @description Method to get all data related to grids from Scenario_Budget_Task__c level
    */
    public static List<Scenario_Budget_Task__c> getclientunitgriddata (ID PBBScenarioID) {
           
                return[  select    id,
                                   Budgeted_Effort__c,
                                   BA_Bill_Amount__c,
                                   Bill_Amount_per_Unit__c,
                                   Effort_per_Unit__c,
                                   Number_of_Units__c,
                                   Scenario_Budget__r.BA_Total_Amount__c,
                                   Scenario_Budget__r.Active__c,
                                   Expenses_Price__c,
                                   Fee_Price__c,
                                   Price__c,
                                   Total_Direct_Unit_Price__c,
                                   Scenario_Budget__r.PBB_Scenario__c,
                                   Service_Task__r.Name,
                                   Service_Task__r.Service_Function__r.Service_Area__r.Name,
                                   Service_Task__r.Id                                     
                         FROM      Scenario_Budget_Task__c 
                         WHERE     Scenario_Budget__r.PBB_Scenario__c =:PBBScenarioID
                         ORDER BY  Service_Task__r.Name ASC];
   }
   
         /**
    @author Ramya
    @date 2015
    @description aggreate Method to get all data related to grids from Scenario_Budget_RoleCountry__c level
    */
   public static List<AggregateResult> getAggFunctionalCntrygriddata (ID PBBScenarioID) {
            
            List<AggregateResult>    FunctionalCntrygridList =   [ select      Scenario_Budget_Task__r.Service_Task__r.Name ServiceTaskName,
                                                                               Scenario_Budget_Task__r.Service_Task__c sertask,
                                                                               Job_Position__r.Id JobPId,
                                                                               Job_Position__r.Name JobpName,                          
                                                                               sum(Budgeted_Effort__c) budgtedeff,
                                                                               sum(Weighted_Avg_Bill_Rate__c) Weightavgbill,
                                                                               sum(BA_Bill_Amount__c) billamt  
                                                                     FROM      Scenario_Budget_RoleCountry__c 
                                                                     WHERE     Scenario_Budget_Task__r.Scenario_Budget__r.PBB_Scenario__c =:PBBScenarioID
                                                                     group by  Scenario_Budget_Task__r.Service_Task__r.Name,Job_Position__r.Id,Job_Position__r.Name,Scenario_Budget_Task__r.Service_Task__c ];
            return  FunctionalCntrygridList;
                
   }
   
           /**
    @author Ramya
    @date 2015
    @description Method to get all data related to grids from Scenario_Budget_RoleCountry__c level
    */
    public static List<Scenario_Budget_RoleCountry__c> getFunctionalCntrygriddata (ID PBBScenarioID) {
            
            List<Scenario_Budget_RoleCountry__c> AggFunctionalCntrygridLists =   [ select      Scenario_Budget_Task__r.Service_Task__r.Name,
                                                                                               Scenario_Budget_Task__r.Service_Task__r.Service_Function__r.Service_Area__r.Name,
                                                                                               Job_Position__r.Id,
                                                                                               Job_Position__r.Name,
                                                                                               Country__r.Id,
                                                                                               Country__r.Name,
                                                                                               Budgeted_Effort__c,
                                                                                               Weighted_Avg_Bill_Rate__c,
                                                                                               BA_Bill_Amount__c,  
                                                                                               Scenario_Budget_Task__r.Effort_per_Unit__c, 
                                                                                               Scenario_Budget_Task__r.Bill_Amount_per_Unit__c
                                                                                     FROM      Scenario_Budget_RoleCountry__c 
                                                                                     WHERE     Scenario_Budget_Task__r.Scenario_Budget__r.PBB_Scenario__c =:PBBScenarioID
                                                                                     order by  Scenario_Budget_Task__r.Service_Task__r.Name];
            return AggFunctionalCntrygridLists;
                
   }
}