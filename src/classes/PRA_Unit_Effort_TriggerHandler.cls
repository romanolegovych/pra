public class PRA_Unit_Effort_TriggerHandler {
    
    ///*ASA 
    private boolean isExecuting = false;
    private integer batchSize = 0;
    
    public PRA_Unit_Effort_TriggerHandler(boolean triggerIsExecuting, integer size){
        isExecuting = triggerIsExecuting;
        batchSize = size;
    }
    
    //Commenting it out as we dont need it for now
    /*
    public void onBeforeInsert(List<Unit_Effort__c> lstUnits){
        //SetDefaultNumber(lstUnits);        
    }
    
    public void onBeforeUpdate(Map<ID,Unit_Effort__c> oldUnits,Map<ID,Unit_Effort__c> newUnits){
        //SetDefaultNumber(newUnits.values());
    }
    
    public void onAfterInsert(Map<ID,Unit_Effort__c> newUnits){
        //createDefaultWorkedUnit(newUnits.values());
    }
    */
    public void onAfterUpdate(Map<ID,Unit_Effort__c> oldUnits,Map<ID,Unit_Effort__c> newUnits){
        // Update the Monthly Approval fields after the worked or forecast unit is changed
        updateMontlyApprovals(oldUnits, newUnits);
       // system.debug('-----------'+oldUnits);
    }
    
    // Update monthly approvals
    private void updateMontlyApprovals(Map<ID, Unit_Effort__c> oldValues, Map<ID, Unit_Effort__c> newValues) {
        // Check if the worked or OF unit confirm or number value has changed
        // If it has, then we need to revert the approval for the PZ released only if the project is not approved
        
        // changed unit value set
        Set<Id> changedUnitSet = new Set<Id>();
        Set<Id> changedForecastUnitSet = new Set<Id>();
           system.debug('-----oldValues------'+oldValues+'---newValues--'+newValues);    
        for(Unit_Effort__c oldUnit : oldValues.values()) {
        
        //if((oldUnit.Is_Confirmed_Worked_Unit__c != newValues.get(oldUnit.Id).Is_Confirmed_Worked_Unit__c)){
               system.debug('-----oldUnit.Worked_Unit__c-----'+oldUnit.Worked_Unit__c+'---newValues.get(oldUnit.Id).Worked_Unit__c--'+newValues.get(oldUnit.Id).Worked_Unit__c); 
            if( (oldUnit.Worked_Unit__c  != newValues.get(oldUnit.Id).Worked_Unit__c)             
                 || (oldUnit.Is_Confirmed_Worked_Unit__c != newValues.get(oldUnit.Id).Is_Confirmed_Worked_Unit__c)  
                  
               ) {                
                // So someone has changed the value of a worked or  forecast unit
                // How dare they? Let us teach them a lesson by updating the Review Approval flags
                system.debug('-----oldUnit.Id-----'+oldUnit.Id); 
                // Add it to the changed unit set
                changedUnitSet.add(oldUnit.Id);                
            } // end if
            
            if((oldUnit.Forecast_Unit__c  != newValues.get(oldUnit.Id).Forecast_Unit__c) || (oldUnit.Forecast_Effort__c  != newValues.get(oldUnit.Id).Forecast_Effort__c)  ){
                changedForecastUnitSet.add(oldUnit.Id);  
            }
        } // end for      
       // }
        system.debug('-----changedUnitSet------'+changedUnitSet);   
        if(changedUnitSet.size() > 0 || changedForecastUnitSet.size()>0) {
            // Get the unit record from the db so that you have a ref to the client project id
            List<Unit_Effort__c> units = [Select Id, Client_Task__r.Project__r.Id
                                   from Unit_Effort__c where Id = :changedUnitSet];                                   
                                   
           List<Unit_Effort__c> Forecastunits = [Select Id,Client_Task__r.Project__r.Id
                                   from Unit_Effort__c where Id = :changedForecastUnitSet];
            // set to hold the project id
            Set<Id> projectIdSet = new Set<Id>();
            // map to hold type vs client id list
            Map<String, Set<Id>> typeProjectMap = new Map<String, Set<Id>>();
            
            // go through the units and add it to the respective maps           
            for(Unit_Effort__c u: units) {
                projectIdSet.add(u.Client_Task__r.Project__r.Id);
                             
                    if(typeProjectMap.get('Worked') == null) {
                        typeProjectMap.put('Worked', new Set<Id>());
                    } 
                    typeProjectMap.get('Worked').add(u.Client_Task__r.Project__r.Id);                    
                
            } 
            for(Unit_Effort__c u: Forecastunits) {
                projectIdSet.add(u.Client_Task__r.Project__r.Id);
               
                    if(typeProjectMap.get('Forecasted') == null) {
                        typeProjectMap.put('Forecasted', new Set<Id>());
                    }
                    typeProjectMap.get('Forecasted').add(u.Client_Task__r.Project__r.Id);                
            } 
                          
            if(projectIdSet.size() > 0 && typeProjectMap.size() > 0)  {                 
                List<Monthly_Approval__c> monthlyApprovals = new List<Monthly_Approval__c>();
                
                // get the monthly approvals for the current month and which haven't been approved by the PM    
                monthlyApprovals = [select Id,Month_Approval_Applies_To__c, Approval_Date__c, Client_Project__c,DPD_Approval_Date__c, DPD_approved_forecast__c,EVP_Approval_Date__c,DOF_Approval_Date__c,EVP_Approved__c,DOF_Approved__c, Is_approval_complete__c, 
                                                        Is_Approved_DPD_forecast__c,Is_Approved_EVP__c,Is_Approved_DOF__c,Is_Approved_PM_Forecast__c,Is_Approved_PM_Worked_Units__c,Is_Approved_PZ_released_forecast__c,
                                                        Need_DOF_EVP_Approval__c,Need_DPD_Approval__c,PM_Forecast_App_Date__c,PM_WU_Approval_Date__c, PZ_Release_Date__c,PM_Approved_Forecast__c, PM_Approved_Worked_Units__c, PZ_released_forecast__c,PZ_Released_Forecast__r.Email
                                                    from Monthly_Approval__c where Month_Approval_Applies_To__c = :Date.today().toStartOfMonth()
                                      and Client_Project__c in :projectIdSet
                                      and (PM_Approved_Forecast__c = null or PM_Approved_Forecast__c = '')];
                                   
                for(Monthly_Approval__c app : monthlyApprovals) {
                    if(typeProjectMap.containsKey('Worked') && typeProjectMap.get('Worked').contains(app.Client_Project__c)) {                                  
                        if(app.PM_Approved_Worked_Units__c != null || app.PM_Approved_Worked_Units__c != '') {
                           // app.PM_Approved_Worked_Units__c = null;
                            //app.PZ_released_forecast__c = null;
                            
                            app.PM_WU_Approval_Date__c=null; 
                            app.PM_Approved_Worked_Units__c=null;
                            app.Is_Approved_PM_Worked_Units__c=false;                            
                            
                            app.PZ_Release_Date__c=null; 
                            app.PZ_released_forecast__c=null; 
                            app.Is_Approved_PZ_released_forecast__c=false;
                            
                            app.PM_Forecast_App_Date__c=null; 
                            app.PM_Approved_Forecast__c=null;   
                            app.Is_Approved_PM_Forecast__c=false;
                           
                            app.DPD_approved_forecast__c=null;
                            app.DPD_Approval_Date__c=null;
                            app.Is_Approved_DPD_forecast__c=false;
                              
                            app.EVP_Approved__c=null;
                            app.Is_Approved_EVP__c=false;
                            app.EVP_Approval_Date__c=null;
                            
                            app.DOF_Approval_Date__c=null; 
                            app.Is_Approved_DOF__c=false;
                            app.DOF_Approved__c=null;
                            
                            app.Need_DOF_EVP_Approval__c=false;
                            app.Need_DPD_Approval__c=false;
                            app.Approval_Date__c=null; 
                            app.Is_approval_complete__c=false;                          
                          
                        } else {
                            app.PZ_released_forecast__c = null;
                        }
                    }
                    else{
                        if(typeProjectMap.containsKey('Forecasted') && typeProjectMap.get('Forecasted').contains(app.Client_Project__c)) {                                   
                            if(app.PZ_released_forecast__c != null || app.PZ_released_forecast__c != '') {                            
                               // app.PZ_released_forecast__c = null;
                               
                                app.PZ_Release_Date__c=null; 
                                app.PZ_released_forecast__c=null; 
                                app.Is_Approved_PZ_released_forecast__c=false;
                                
                                app.PM_Forecast_App_Date__c=null; 
                                app.PM_Approved_Forecast__c=null;   
                                app.Is_Approved_PM_Forecast__c=false;
                               
                                app.DPD_approved_forecast__c=null;
                                app.DPD_Approval_Date__c=null;
                                app.Is_Approved_DPD_forecast__c=false;
                                  
                                app.EVP_Approved__c=null;
                                app.Is_Approved_EVP__c=false;
                                app.EVP_Approval_Date__c=null;
                                
                                app.DOF_Approval_Date__c=null; 
                                app.Is_Approved_DOF__c=false;
                                app.DOF_Approved__c=null;
                                
                                app.Need_DOF_EVP_Approval__c=false;
                                app.Need_DPD_Approval__c=false;
                                app.Approval_Date__c=null; 
                                app.Is_approval_complete__c=false;
                               
                            } 
                        }                    
                    }
                }               
                update monthlyApprovals;               
                
            } // end if projectIdSet
        }// end if update project released         
    }  
}