@isTest
private class BDT_SponsorsControllerTest {
	
	public static List<Account> mySponsorEntityList {get;set;}
	public static List<Sponsor__c> mySponsorList {get;set;}
	
	static void init(){
		mySponsorList = BDT_TestDataUtils.buildSponsor(10);
		insert mySponsorList;
		mySponsorEntityList = BDT_TestDataUtils.buildSponsorEntity(mySponsorList);
		insert mySponsorEntityList;
	}
	
	static testMethod void editSponsorPath() { 
		init();
		
		System.currentPageReference().getParameters().put('sponsorId', mySponsorList[0].id);
		
		BDT_SponsorsController sponsorC = new BDT_SponsorsController();
		
		System.assertNotEquals(null, sponsorC.EditSponsor());
	}
	
	static testMethod void editSponsorEntityPath() { 
		init();
		
		System.currentPageReference().getParameters().put('sponsorId', mySponsorList[0].id);
		System.currentPageReference().getParameters().put('sponsorEntityId', mySponsorEntityList[0].id);
		
		BDT_SponsorsController sponsorEntC = new BDT_SponsorsController();
		
		System.assertNotEquals(null, sponsorEntC.EditSponsorEntity());
	}
	
	
	
	static testMethod void getSponsorEntityPerSponsorPath() { 
		init();
		
		System.currentPageReference().getParameters().put('sponsorId', mySponsorList[0].id);
		System.currentPageReference().getParameters().put('sponsorEntityId', mySponsorEntityList[0].id);
		
		BDT_SponsorsController sponsorEnPS = new BDT_SponsorsController();
		sponsorEnPS.getSponsorEntityPerSponsor();
		
		sponsorEnPS.getSponsorEntityPerSponsorSelected(sponsorEnPS.sponsorId);
		System.assertEquals(5,sponsorEnPS.sponsorEntityPerSponsor.size());
	}

}