/**
 * @author Konstantin Ermolenko
 * @Date 12/24/2014
 * @Description helper class needed for keeping Linked Step properties in sync 
 */
public with sharing class PAWS_StepProperty_LinkedStepHelper
{
	public void BeforeInsert(List<STSWR1__Flow_Step_Property__c> properties)
	{
		List<STSWR1__Flow_Step_Property__c> linkedStepProperties = GetLinkedStepProperties(properties);
		
		for (STSWR1__Flow_Step_Property__c p : linkedStepProperties)
		{
			if (p.STSWR1__Key__c != null)
			{
				Map<String, String> keyData = (Map<String, String>) JSON.deserialize(p.STSWR1__Key__c, Map<String, String>.class);
				keyData.put('invalid', 'true');
				p.STSWR1__Key__c = JSON.serialize(keyData);
			}
		}
	}
	
	public void AfterInsert(List<STSWR1__Flow_Step_Property__c> properties)
	{
		List<STSWR1__Flow_Step_Property__c> linkedStepProperties = GetLinkedStepProperties(properties);
		
		if (linkedStepProperties.size() > 0)
		{
			//new records that have been just created where STSWR1__Key__c is null
			ProcessNewLinkedStepProperties(linkedStepProperties);
		}
	}
	
	public void AfterUpdate(List<STSWR1__Flow_Step_Property__c> newProperties, List<STSWR1__Flow_Step_Property__c> oldProperties)
	{
		List<STSWR1__Flow_Step_Property__c> typeChangedToLinkedStep = new List<STSWR1__Flow_Step_Property__c>();
		List<STSWR1__Flow_Step_Property__c> typeChangedFromLinkedStep = new List<STSWR1__Flow_Step_Property__c>();
		
		List<STSWR1__Flow_Step_Property__c> configChanged = new List<STSWR1__Flow_Step_Property__c>();
		Map<ID, ID> satellitePropIds = new Map<ID, ID>();
		
		STSWR1__Flow_Step_Property__c newRec;
		STSWR1__Flow_Step_Property__c oldRec;
		for (Integer i = 0; i < newProperties.size(); i++)
		{
			newRec = newProperties.get(i);
			oldRec = oldProperties.get(i);
			
			if (newRec.STSWR1__Type__c == 'Linked Step' && oldRec.STSWR1__Type__c != 'Linked Step')
			{
				typeChangedToLinkedStep.add(newRec);
			}
			else if (newRec.STSWR1__Type__c != 'Linked Step' && oldRec.STSWR1__Type__c == 'Linked Step')
			{
				typeChangedFromLinkedStep.add(oldRec);
			}
			else if (newRec.STSWR1__Type__c == 'Linked Step' && oldRec.STSWR1__Type__c == 'Linked Step' && newRec.STSWR1__Value__c != oldRec.STSWR1__Value__c)
			{
				Map<String, String> keyData = (Map<String, String>) JSON.deserialize(newRec.STSWR1__Key__c, Map<String, String>.class);
				
				if (keyData.get('invalid') == 'true')
				{
					typeChangedToLinkedStep.add(newRec);
				}
				else
				{
					satellitePropIds.put(newRec.Id, keyData.get('satelliteID'));
					configChanged.add(newRec);
				}
			}
		}
		
		//property type was changed from Linked Step to Any
		AfterDelete(typeChangedFromLinkedStep);
		
		//Linked Step type or stepID was changed
		if (configChanged.size() > 0)
		{
			PAWS_Utilities.checkLimit('DMLStatements', 1);
			STSWR1.AbstractTrigger.Disabled = true;
			delete [Select Id From STSWR1__Flow_Step_Property__c Where Id in :satellitePropIds.values()];
			STSWR1.AbstractTrigger.Disabled = false;
			
			typeChangedToLinkedStep.addAll(configChanged);
		}
		
		//property type was changed from Any to Linked Step
		List<STSWR1__Flow_Step_Property__c> propsForInsert = new List<STSWR1__Flow_Step_Property__c>();
		for (STSWR1__Flow_Step_Property__c p : typeChangedToLinkedStep)
		{
			propsForInsert.add(new STSWR1__Flow_Step_Property__c(
					Id = p.Id,
					Name = p.Name,
					STSWR1__Flow_Step__c = p.STSWR1__Flow_Step__c,
					STSWR1__Hidden__c = p.STSWR1__Hidden__c,
					STSWR1__Key__c = null,
					STSWR1__Value__c = p.STSWR1__Value__c,
					STSWR1__Type__c = p.STSWR1__Type__c,
					STSWR1__Reqiured__c = p.STSWR1__Reqiured__c,
					STSWR1__Read_Only__c = p.STSWR1__Read_Only__c,
					STSWR1__Options__c = p.STSWR1__Options__c
				)
			);
		}
		AfterInsert(propsForInsert);
	}
	
	public void AfterDelete(List<STSWR1__Flow_Step_Property__c> properties)
	{
		List<STSWR1__Flow_Step_Property__c> linkedStepProperties = GetLinkedStepProperties(properties);
		
		if (linkedStepProperties.size() > 0)
		{
			Set<ID> deletedProperties = new Map<ID, STSWR1__Flow_Step_Property__c>(linkedStepProperties).keySet();
			Set<ID> propertiesToDelete = new Set<ID>();
			for (STSWR1__Flow_Step_Property__c p : linkedStepProperties)
			{
				Map<String, String> keyData = (Map<String, String>) JSON.deserialize(p.STSWR1__Key__c, Map<String, String>.class);
				if (keyData.get('invalid') != 'true')
				{
					ID satelliteID = (ID) keyData.get('satelliteID');
					if (!deletedProperties.contains(satelliteID))
					{
						propertiesToDelete.add(satelliteID);
					}
				}
			}
			
			PAWS_Utilities.checkLimit('DMLStatements', 1);
			STSWR1.AbstractTrigger.Disabled = true;
			delete [Select Id From STSWR1__Flow_Step_Property__c Where Id in :propertiesToDelete];
			STSWR1.AbstractTrigger.Disabled = false;
		}
	}
	
	public void AfterCloneImport(Set<String> flowIds)
	{
		List<STSWR1__Flow_Step_Property__c> properties = [Select STSWR1__Value__c, STSWR1__Key__c, STSWR1__Flow_Step__r.Name, STSWR1__Flow_Step__r.STSWR1__Guid__c, STSWR1__Flow_Step__r.STSWR1__Flow__r.Name From STSWR1__Flow_Step_Property__c Where STSWR1__Type__c = 'Linked Step' And STSWR1__Flow_Step__r.STSWR1__Flow__c in :flowIds];
		
		if (properties.size() > 0)
		{
			Map<String, STSWR1__Flow_Step_Property__c> oldNewProps = new Map<String, STSWR1__Flow_Step_Property__c>();
			Map<String, String> oldSatellitePropIds = new Map<String, String>();
			Map<ID, Map<String, String>> propertyConfig = new Map<ID, Map<String, String>>();
			
			for (STSWR1__Flow_Step_Property__c p : properties)
			{
				Map<String, String> keyData = (Map<String, String>) JSON.deserialize(p.STSWR1__Key__c, Map<String, String>.class);
				PAWS_StepProperty_LinkedStepController.LinkedStepData linkedStepData = (PAWS_StepProperty_LinkedStepController.LinkedStepData) JSON.deserialize(p.STSWR1__Value__c, PAWS_StepProperty_LinkedStepController.LinkedStepData.class);
				
				oldNewProps.put(keyData.get('ID'), p);
				oldSatellitePropIds.put(keyData.get('ID'), keyData.get('satelliteID'));
				propertyConfig.put(p.Id, linkedStepData.Config);
			}
			
			Map<ID, STSWR1__Flow_Step_Property__c> processedProperties = new Map<ID, STSWR1__Flow_Step_Property__c>();
			for (String propId : oldSatellitePropIds.keySet())
			{
				STSWR1__Flow_Step_Property__c property = oldNewProps.get(propId);
				STSWR1__Flow_Step_Property__c satelliteProperty = oldNewProps.get(oldSatellitePropIds.get(propId));
				
				if (!(satelliteProperty == null || processedProperties.containsKey(property.Id)))
				{
					property.STSWR1__Key__c = JSON.serialize(new Map<String, String>{'ID' => property.Id, 'satelliteID' => satelliteProperty.Id});
					satelliteProperty.STSWR1__Key__c = JSON.serialize(new Map<String, String>{'ID' => satelliteProperty.Id, 'satelliteID' => property.Id});
					
					String linkedStepType = propertyConfig.get(property.Id).get('type');
					property.STSWR1__Value__c = new PAWS_StepProperty_LinkedStepController.LinkedStepData(satelliteProperty.STSWR1__Flow_Step__r, linkedStepType, false).toJson();
					
					String linkedStepSatelliteType = propertyConfig.get(satelliteProperty.Id).get('type');
					satelliteProperty.STSWR1__Value__c = new PAWS_StepProperty_LinkedStepController.LinkedStepData(property.STSWR1__Flow_Step__r, linkedStepSatelliteType, false).toJson();
					
					processedProperties.put(property.Id, property);
					processedProperties.put(satelliteProperty.Id, satelliteProperty);
				}
			}
			
			STSWR1.AbstractTrigger.Disabled = true;
			update processedProperties.values();
			STSWR1.AbstractTrigger.Disabled = false;
		}
	}
	
	private List<STSWR1__Flow_Step_Property__c> GetLinkedStepProperties(List<STSWR1__Flow_Step_Property__c> properties)
	{
		List<STSWR1__Flow_Step_Property__c> linkedStepProperties = new List<STSWR1__Flow_Step_Property__c>();
		for (STSWR1__Flow_Step_Property__c p : properties)
		{
			if (p.STSWR1__Type__c == 'Linked Step')
			{
				linkedStepProperties.add(p);
			}
		}
		
		return linkedStepProperties;
	}
	
	private void ProcessNewLinkedStepProperties(List<STSWR1__Flow_Step_Property__c> properties)
	{
		Map<ID, ID> satellitePropertyStepIds = new Map<ID, ID>();
		Map<ID, Map<String, String>> propertiesConfigMap = new Map<ID, Map<String, String>>();
		Map<ID, STSWR1__Flow_Step_Property__c> propertiesMap = new Map<ID, STSWR1__Flow_Step_Property__c>();
		Map<ID, STSWR1__Flow_Step_Property__c> satellitePropertiesMap = new Map<ID, STSWR1__Flow_Step_Property__c>();
		
		for (STSWR1__Flow_Step_Property__c property : properties)
		{
			if (property.STSWR1__Key__c == null)
			{
				propertiesMap.put(property.Id, property);
				satellitePropertyStepIds.put(property.Id, property.STSWR1__Flow_Step__c);
				PAWS_StepProperty_LinkedStepController.LinkedStepData linkedStepData = (PAWS_StepProperty_LinkedStepController.LinkedStepData) JSON.deserialize(property.STSWR1__Value__c, PAWS_StepProperty_LinkedStepController.LinkedStepData.class);
				propertiesConfigMap.put(property.Id, linkedStepData.Config);
			}
		}
		
		if (propertiesMap.keySet().size() > 0)
		{
			PAWS_Utilities.checkLimit('Queries', 1);
			Map<ID, STSWR1__Flow_Step_Junction__c> satelliteStepsMap = new Map<ID, STSWR1__Flow_Step_Junction__c>([Select Name, STSWR1__Guid__c, STSWR1__Flow__r.Name From STSWR1__Flow_Step_Junction__c Where Id in :satellitePropertyStepIds.values()]);
			
			for (ID propertyId : propertiesMap.keySet())
			{
				STSWR1__Flow_Step_Property__c property = propertiesMap.get(propertyId);
				
				String satelliteType = (propertiesConfigMap.get(propertyId).get('type') == 'Master' ? 'Slave' : 'Master');
				STSWR1__Flow_Step_Junction__c satelliteStep = satelliteStepsMap.get(property.STSWR1__Flow_Step__c);
				satellitePropertiesMap.put(property.Id, CreateSatelliteProperty(propertiesConfigMap.get(property.Id).get('stepID'), property.Name, satelliteType, satelliteStep));
			}
			
			PAWS_Utilities.checkLimit('DMLStatements', 1);
			STSWR1.AbstractTrigger.Disabled = true;
			insert satellitePropertiesMap.values();
			STSWR1.AbstractTrigger.Disabled = false;
			
			List<STSWR1__Flow_Step_Property__c> propertiesForUpdate = new List<STSWR1__Flow_Step_Property__c>();
			for (ID propertyId : propertiesMap.keySet())
			{
				STSWR1__Flow_Step_Property__c property = new STSWR1__Flow_Step_Property__c (Id = propertyId);
				STSWR1__Flow_Step_Property__c satelliteProperty = satellitePropertiesMap.get(propertyId);
				
				property.STSWR1__Key__c = JSON.serialize(new Map<String, String>{'ID' => property.Id, 'satelliteID' => satelliteProperty.Id});
				satelliteProperty.STSWR1__Key__c = JSON.serialize(new Map<String, String>{'ID' => satelliteProperty.Id, 'satelliteID' => property.Id});
				
				propertiesForUpdate.add(property);
				propertiesForUpdate.add(satelliteProperty);
			}
			
			PAWS_Utilities.checkLimit('DMLStatements', 1);
			STSWR1.AbstractTrigger.Disabled = true;
			update propertiesForUpdate;
			STSWR1.AbstractTrigger.Disabled = false;
		}
	}
	
	private STSWR1__Flow_Step_Property__c CreateSatelliteProperty(String parentStepId, String name, String satelliteType, STSWR1__Flow_Step_Junction__c satelliteStep)
	{
		STSWR1__Flow_Step_Property__c satelliteProperty = new STSWR1__Flow_Step_Property__c();
		
		PAWS_StepProperty_LinkedStepController.LinkedStepData satelliteLinkedStepData = new PAWS_StepProperty_LinkedStepController.LinkedStepData(satelliteStep, satelliteType, false);
		
		satelliteProperty.Name = name;
		satelliteProperty.STSWR1__Flow_Step__c = parentStepId;
		satelliteProperty.STSWR1__Type__c = 'Linked Step';
		satelliteProperty.STSWR1__Value__c = satelliteLinkedStepData.toJson();
		
		return satelliteProperty;
	}
	
	/******************************** History Dates Changes ********************************/
	public static Boolean SkipSlaveProcessing {get; set;}
	public static Boolean SkipMasterProcessing {get; set;}
	public static Boolean ForceAfterUpdate {get; set;}
	
	private List<STSWR1__Flow_Step_Property__c> propertiesCache {get; set;}
	
	public static Set<ID> ChainStepIds
	{
		get
		{
			if (ChainStepIds == null)
			{
				ChainStepIds = new Set<ID>();
			}
			return ChainStepIds;
		}
		set;
	}
	
	public void HistoryAfterInsert(List<STSWR1__Flow_Instance_History__c> historyRecs)
	{
		if (SkipSlaveProcessing != true)
		{
			List<Map<String, String>> queueTasks = GetHistoryQueueTasks(historyRecs, 'Slave');
			
			if (queueTasks.size() > 0)
			{
				PAWS_Queue.addTasks(queueTasks);
				SkipMasterProcessing = true;
			}
		}
		
		if (SkipMasterProcessing != true)
		{
			List<Map<String, String>> queueTasks = GetHistoryQueueTasks(historyRecs, 'Master');
			
			if (queueTasks.size() > 0)
			{
				PAWS_Queue.addTasks(queueTasks);
			}
		}
	}
	
	public void HistoryAfterUpdate(List<STSWR1__Flow_Instance_History__c> historyNewRecs, List<STSWR1__Flow_Instance_History__c> historyOldRecs)
	{
		List<STSWR1__Flow_Instance_History__c> modifiedHistoryRecs = GetModifiedHistoryRecs(historyNewRecs, historyOldRecs);
		if (modifiedHistoryRecs.size() > 0) HistoryAfterInsert(modifiedHistoryRecs);
	}
	
	public List<Map<String, String>> GetHistoryQueueTasks(List<STSWR1__Flow_Instance_History__c> historyRecs, String linkedStepType)
	{
		List<Map<String, String>> queueTasks = new List<Map<String, String>>();
		
		Set<ID> stepIds = new Set<ID>();
		for (STSWR1__Flow_Instance_History__c h : historyRecs)
		{
			stepIds.add(h.STSWR1__Step__c);
		}
		
		Set<ID> stepsToProcess = new Set<ID>();
		if (propertiesCache == null) propertiesCache = [Select STSWR1__Key__c, STSWR1__Value__c, STSWR1__Flow_Step__c From STSWR1__Flow_Step_Property__c Where STSWR1__Type__c = 'Linked Step' And STSWR1__Flow_Step__c in :stepIds];
		for (STSWR1__Flow_Step_Property__c p : propertiesCache)
		{
			Map<String, String> keyData = (Map<String, String>) JSON.deserialize(p.STSWR1__Key__c, Map<String, String>.class);
			if (keyData.get('invalid') != 'true')
			{
				PAWS_StepProperty_LinkedStepController.LinkedStepData linkedStepData = (PAWS_StepProperty_LinkedStepController.LinkedStepData) JSON.deserialize(p.STSWR1__Value__c, PAWS_StepProperty_LinkedStepController.LinkedStepData.class);
				if (linkedStepData.Config.get('type') == linkedStepType)
				{
					stepsToProcess.add(p.STSWR1__Flow_Step__c);
				}
			}
		}
		
		for (STSWR1__Flow_Instance_History__c h : historyRecs)
		{
			if (stepsToProcess.contains(h.STSWR1__Step__c))
			{
				queueTasks.add(new Map<String, String>{
					'Type__c' => 'Linked Step Sync Dates',
					'Data__c' => JSON.serialize(new Map<String, String>{'type' => linkedStepType, 'historyId' => h.Id, 'stepId' => h.STSWR1__Step__c, 'chainStepIds' => JSON.serialize(ChainStepIds)})
				});
			}
		}
		
		return queueTasks;
	}
	
	
	public List<STSWR1__Flow_Instance_History__c> GetModifiedHistoryRecs(List<STSWR1__Flow_Instance_History__c> historyNewRecs, List<STSWR1__Flow_Instance_History__c> historyOldRecs)
	{
		List<STSWR1__Flow_Instance_History__c> modifiedHistoryRecs = new List<STSWR1__Flow_Instance_History__c>();
		
		for (Integer i = 0; i < historyNewRecs.size(); i++)
		{
			STSWR1__Flow_Instance_History__c newRec = historyNewRecs.get(i);
			STSWR1__Flow_Instance_History__c oldRec = historyOldRecs.get(i);
			
			if (ForceAfterUpdate == true || newRec.STSWR1__Actual_Start_Date__c != oldRec.STSWR1__Actual_Start_Date__c || newRec.STSWR1__Actual_Complete_Date__c != oldRec.STSWR1__Actual_Complete_Date__c)
			{
				modifiedHistoryRecs.add(newRec);
			}
		}
		
		return modifiedHistoryRecs;
	}
	
	/******************************** Proceed Linked Step ********************************/
	public void CursorAfterInsert(List<STSWR1__Flow_Instance_Cursor__c> records)
	{
		CursorAfterUpdate(records, null);
	}
	
	public void CursorAfterUpdate(List<STSWR1__Flow_Instance_Cursor__c> newRecords, List<STSWR1__Flow_Instance_Cursor__c> oldRecords)
	{
		Set<ID> newSteps = new Set<ID>();
		Set<ID> oldSteps = new Set<ID>();
		Set<ID> allSteps = new Set<ID>();
		Map<ID, Map<String, Set<ID>>> stepCursorIds = new Map<ID, Map<String, Set<ID>>>();
		
		for (Integer i = 0; i < newRecords.size(); i++)
		{
			ID newStepId = newRecords.get(i).STSWR1__Step__c;
			ID oldStepId = (oldRecords != null ? oldRecords.get(i).STSWR1__Step__c : null);
			
			String newStatus = newRecords.get(i).STSWR1__Status__c;
			String oldStatus = (oldRecords != null ? oldRecords.get(i).STSWR1__Status__c : newRecords.get(i).STSWR1__Status__c);
			Boolean branchIsComplete = (newStatus == 'Complete' && newStatus != oldStatus);
			
			if (newStepId != oldStepId || branchIsComplete)
			{
				newSteps.add(newStepId);
				allSteps.add(newStepId);
				
				if (oldStepId != null)
				{
					oldSteps.add(oldStepId);
					allSteps.add(oldStepId);
				}
				
				if (!stepCursorIds.containsKey(newStepId)) stepCursorIds.put(newStepId, new Map<String, Set<ID>>{'new' => new Set<ID>(), 'old' => new Set<ID>()});
				if (oldStepId != null && !stepCursorIds.containsKey(oldStepId)) stepCursorIds.put(oldStepId, new Map<String, Set<ID>>{'new' => new Set<ID>(), 'old' => new Set<ID>()});
				
				if (!branchIsComplete) stepCursorIds.get(newStepId).get('new').add(newRecords.get(i).Id);
				if (oldStepId != null) stepCursorIds.get(oldStepId).get('old').add(oldRecords.get(i).Id);
			}
		}
		
		if(allSteps.size() == 0) return;
		
		List<Map<String, String>> queueTasks = new List<Map<String, String>>();
		
		PAWS_Utilities.checkLimit('Queries', 1);
		List<STSWR1__Flow_Step_Property__c> properties = [Select STSWR1__Key__c, STSWR1__Value__c, STSWR1__Flow_Step__c From STSWR1__Flow_Step_Property__c Where STSWR1__Type__c = 'Linked Step' And STSWR1__Flow_Step__c in :allSteps];
		for (STSWR1__Flow_Step_Property__c p : properties)
		{
			Map<String, String> keyData = (Map<String, String>) JSON.deserialize(p.STSWR1__Key__c, Map<String, String>.class);
			if (keyData.get('invalid') != 'true')
			{
				PAWS_StepProperty_LinkedStepController.LinkedStepData linkedStepData = (PAWS_StepProperty_LinkedStepController.LinkedStepData) JSON.deserialize(p.STSWR1__Value__c, PAWS_StepProperty_LinkedStepController.LinkedStepData.class);
				String linkedStepType = linkedStepData.Config.get('type');
				
				if (stepCursorIds.containsKey(p.STSWR1__Flow_Step__c))
				{
					for (ID cursorId : stepCursorIds.get(p.STSWR1__Flow_Step__c).get(linkedStepType == 'Master' ? 'old' : 'new'))
					{
						queueTasks.add(new Map<String, String>{
							'Type__c' => 'Linked Step Proceed',
							'Data__c' => JSON.serialize(new Map<String, String>{'type' => linkedStepType, 'sourceCursorId' => cursorId, 'sourceStepId' => p.STSWR1__Flow_Step__c, 'stepIdToProcess' => (linkedStepType == 'Master' ? linkedStepData.Config.get('stepID') : (String) p.STSWR1__Flow_Step__c)})
						});
					}
				}
			}
		}
		
		if (queueTasks.size() > 0)
		{
			PAWS_Utilities.checkLimit('DMLStatements', 1);
			PAWS_Queue.addTasks(queueTasks);
		}
	}
	
	public void ProceedLinkedStep(Map<String, String> params)
	{
		STSWR1__Flow_Instance_Cursor__c cursorToProceed;
		
		String linkedStepType = params.get('type');
		String sourceCursorId = params.get('sourceCursorId');
		String sourceStepId = params.get('sourceStepId');
		String stepIdToProcess = params.get('stepIdToProcess');
		
		Map<ID, Boolean> stepsIsSkipped = new Map<ID, Boolean>{
			sourceStepId => false,
			stepIdToProcess => false
		};
		
		for (STSWR1__Gantt_Step_Property__c gsp : [Select STSWR1__Is_Skipped__c, STSWR1__Step__c From STSWR1__Gantt_Step_Property__c Where STSWR1__Step__c in :stepsIsSkipped.keySet() And STSWR1__Level__c = 'First'])
		{
			stepsIsSkipped.put(gsp.STSWR1__Step__c, gsp.STSWR1__Is_Skipped__c);
		}
		
		if (stepsIsSkipped.get(sourceStepId) == false)
		{
			if (linkedStepType == 'Master')
			{
				List<STSWR1__Flow_Instance_History__c> historyRecs = [Select STSWR1__Status__c, STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Object_Id__c From STSWR1__Flow_Instance_History__c Where STSWR1__Cursor__c = :sourceCursorId And STSWR1__Step__c = :sourceStepId Order By CreatedDate desc limit 1];
				
				if (historyRecs.size() > 0 && stepsIsSkipped.get(stepIdToProcess) == false)
				{
					String status = historyRecs.get(0).STSWR1__Status__c;
					if (status == 'Completed' || status == 'Approved')
					{
						String objId = historyRecs.get(0).STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Object_Id__c;
						List<STSWR1__Flow_Instance_Cursor__c> targetCursorRecs = [Select STSWR1__Is_Completed__c, STSWR1__Actions_History__c, STSWR1__Step_Type__c From STSWR1__Flow_Instance_Cursor__c Where STSWR1__Flow_Instance__r.STSWR1__Object_Id__c = :objId And STSWR1__Step__c = :stepIdToProcess And STSWR1__Is_Skipped__c = false And STSWR1__Is_Completed__c = 'No' And STSWR1__Status__c = 'In Progress' And STSWR1__Flow_Instance__r.STSWR1__Is_Active__c = true Order By CreatedDate desc limit 1];
						cursorToProceed = (targetCursorRecs.size() > 0 ? targetCursorRecs.get(0) : null);
					}
				}
			}
			else
			{
				Set<ID> masterStepIds = new Set<ID>();
				List<STSWR1__Flow_Step_Property__c> properties = [Select STSWR1__Key__c, STSWR1__Value__c From STSWR1__Flow_Step_Property__c Where STSWR1__Type__c = 'Linked Step' And STSWR1__Flow_Step__c = :stepIdToProcess];
				for (STSWR1__Flow_Step_Property__c p : properties)
				{
					Map<String, String> keyData = (Map<String, String>) JSON.deserialize(p.STSWR1__Key__c, Map<String, String>.class);
					if (keyData.get('invalid') != 'true')
					{
						PAWS_StepProperty_LinkedStepController.LinkedStepData linkedStepData = (PAWS_StepProperty_LinkedStepController.LinkedStepData) JSON.deserialize(p.STSWR1__Value__c, PAWS_StepProperty_LinkedStepController.LinkedStepData.class);
						if (linkedStepData.Config.get('type') == 'Slave')
						{
							masterStepIds.add(linkedStepData.Config.get('stepID'));
						}
					}
				}
				
				for (STSWR1__Gantt_Step_Property__c gsp : [Select STSWR1__Is_Skipped__c, STSWR1__Step__c From STSWR1__Gantt_Step_Property__c Where STSWR1__Step__c in :masterStepIds And STSWR1__Level__c = 'First'])
				{
					if (gsp.STSWR1__Is_Skipped__c) masterStepIds.remove(gsp.STSWR1__Step__c);
				}
				
				List<STSWR1__Flow_Instance_Cursor__c> targetCursorRecs = [Select STSWR1__Is_Completed__c, STSWR1__Actions_History__c, STSWR1__Step_Type__c, STSWR1__Flow_Instance__r.STSWR1__Object_Id__c From STSWR1__Flow_Instance_Cursor__c Where Id = :sourceCursorId And STSWR1__Step__c = :stepIdToProcess And STSWR1__Is_Skipped__c = false And STSWR1__Is_Completed__c = 'No' And STSWR1__Status__c = 'In Progress' And STSWR1__Flow_Instance__r.STSWR1__Is_Active__c = true];
				
				if (targetCursorRecs.size() > 0)
				{
					List<STSWR1__Flow_Instance_History__c> historyRecs = [Select STSWR1__Status__c From STSWR1__Flow_Instance_History__c Where STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Object_Id__c = :targetCursorRecs.get(0).STSWR1__Flow_Instance__r.STSWR1__Object_Id__c And STSWR1__Step__c in :masterStepIds Order By CreatedDate desc limit 1];
					cursorToProceed = (historyRecs.size() > 0 && (historyRecs.get(0).STSWR1__Status__c == 'Completed' || historyRecs.get(0).STSWR1__Status__c == 'Approved') ? targetCursorRecs.get(0) : null);
				}
			}
			
			if (cursorToProceed != null)
			{
				try
				{
					new PAWS_API().completeCursor(cursorToProceed);
				}
				catch(Exception ex)
				{
					if (ex.getMessage() != 'Workflow was locked by sub workflows!') throw ex;
				}
			}
		}
	}
	
	public void SyncDates(PAWS_Queue__c record, Map<String, String> params)
	{
		String linkedStepType = params.get('type');
		String historyId = params.get('historyId');
		String stepId = params.get('stepId');
		
		PAWS_StepProperty_LinkedStepHelper.ChainStepIds = (Set<ID>) JSON.deserialize(params.get('chainStepIds'), Set<ID>.class);
		
		Boolean isSkipped = ([Select Id From STSWR1__Gantt_Step_Property__c Where STSWR1__Step__c = :stepId And STSWR1__Level__c = 'First' And STSWR1__Is_Skipped__c = true].size() > 0);
		
		Boolean dupeTaskIsFound = false;
		for (PAWS_Queue__c pq : [Select Data__c From PAWS_Queue__c Where Status__c = 'Pending' And Type__c = 'Linked Step Sync Dates'])
		{
			if (pq.Id != record.Id)
			{
				Map<String, String> dataMap = (Map<String, String>) JSON.deserialize(pq.Data__c, Map<String, String>.class);
				if (linkedStepType == dataMap.get('type') && historyId == dataMap.get('historyId') && stepId == dataMap.get('stepId'))
				{
					dupeTaskIsFound = true;
					break;
				}
			}
		}
		
		if (!isSkipped && !dupeTaskIsFound && !PAWS_StepProperty_LinkedStepHelper.ChainStepIds.contains((ID) stepId))
		{
			Set<ID> linkedStepIds = new Set<ID>();
			
			List<STSWR1__Flow_Step_Property__c> properties = [Select STSWR1__Key__c, STSWR1__Value__c From STSWR1__Flow_Step_Property__c Where STSWR1__Type__c = 'Linked Step' And STSWR1__Flow_Step__c = :stepId];
			for (STSWR1__Flow_Step_Property__c p : properties)
			{
				Map<String, String> keyData = (Map<String, String>) JSON.deserialize(p.STSWR1__Key__c, Map<String, String>.class);
				if (keyData.get('invalid') != 'true')
				{
					PAWS_StepProperty_LinkedStepController.LinkedStepData linkedStepData = (PAWS_StepProperty_LinkedStepController.LinkedStepData) JSON.deserialize(p.STSWR1__Value__c, PAWS_StepProperty_LinkedStepController.LinkedStepData.class);
					if (linkedStepData.Config.get('type') == linkedStepType)
					{
						linkedStepIds.add(linkedStepData.Config.get('stepID'));
					}
				}
			}
			
			for (STSWR1__Gantt_Step_Property__c gsp : [Select STSWR1__Is_Skipped__c, STSWR1__Step__c From STSWR1__Gantt_Step_Property__c Where STSWR1__Step__c in :linkedStepIds And STSWR1__Level__c = 'First'])
			{
				if (gsp.STSWR1__Is_Skipped__c) linkedStepIds.remove(gsp.STSWR1__Step__c);
			}
			
			STSWR1__Flow_Instance_History__c historyRec = [Select STSWR1__Actual_Start_Date__c, STSWR1__Actual_Complete_Date__c, STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Object_Id__c From STSWR1__Flow_Instance_History__c Where Id = :historyId];
			
			List<STSWR1__Flow_Instance_History__c> historyRecs = [
				Select
					STSWR1__Actual_Start_Date__c,
					STSWR1__Actual_Complete_Date__c,
					STSWR1__Step__c,
					STSWR1__Cursor__r.STSWR1__Flow_Instance__r.Id,
					CreatedDate
				From
					STSWR1__Flow_Instance_History__c
				Where
					STSWR1__Step__c in :linkedStepIds And
					STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Object_Id__c = :historyRec.STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Object_Id__c
				Order By
					STSWR1__Step__c,
					CreatedDate desc
			];
			
			Map<ID, STSWR1__Flow_Instance_History__c> actualHistoryRecs = new Map<ID, STSWR1__Flow_Instance_History__c>();
			for (STSWR1__Flow_Instance_History__c h : historyRecs)
			{
				if (!actualHistoryRecs.containsKey(h.STSWR1__Step__c)) actualHistoryRecs.put(h.STSWR1__Step__c, h);
			}
			
			PAWS_StepProperty_LinkedStepHelper.SkipSlaveProcessing = true;
			PAWS_StepProperty_LinkedStepHelper.ForceAfterUpdate = true;
			
			PAWS_StepProperty_LinkedStepHelper.ChainStepIds.add((ID) stepId);
			
			if (linkedStepType == 'Master')
			{
				List<STSWR1__Flow_Instance_History__c> recsToUpdate = new List<STSWR1__Flow_Instance_History__c>();
				List<STSWR1__Flow_Instance__c> flowInstancesForUpdate = new List<STSWR1__Flow_Instance__c>();
				for (STSWR1__Flow_Instance_History__c h : actualHistoryRecs.values())
				{
					if (h.STSWR1__Actual_Complete_Date__c == null && historyRec.STSWR1__Actual_Start_Date__c != null)
					{
						h.STSWR1__Actual_Start_Date__c = historyRec.STSWR1__Actual_Start_Date__c;
						h.STSWR1__Actual_Complete_Date__c = historyRec.STSWR1__Actual_Complete_Date__c;
						recsToUpdate.add(h);
						flowInstancesForUpdate.add(h.STSWR1__Cursor__r.STSWR1__Flow_Instance__r);
					}
				}
				
				update recsToUpdate;
				//trigger trigger on Flow Instance
				update flowInstancesForUpdate;
			}
			else
			{
				Date actualStartDate;
				Date actualCompleteDate;
				Datetime lastCreatedDate;
				
				for (STSWR1__Flow_Instance_History__c h : actualHistoryRecs.values())
				{
					if (lastCreatedDate == null || h.CreatedDate > lastCreatedDate)
					{
						actualStartDate = h.STSWR1__Actual_Start_Date__c;
						actualCompleteDate = h.STSWR1__Actual_Complete_Date__c;
						lastCreatedDate = h.CreatedDate;
					}
				}
				
				if (historyRec.STSWR1__Actual_Complete_Date__c == null && actualStartDate != null)
				{
					historyRec.STSWR1__Actual_Start_Date__c = actualStartDate;
					historyRec.STSWR1__Actual_Complete_Date__c = actualCompleteDate;
				}
				
				update historyRec;
				//trigger trigger on Flow Instance
				update historyRec.STSWR1__Cursor__r.STSWR1__Flow_Instance__r;
			}
		}
	}
}