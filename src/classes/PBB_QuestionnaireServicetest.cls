/**
@author Bhargav Devaram
@date 2015
@description this is PBB_QuestionnaireService test class
**/
@isTest   
private class PBB_QuestionnaireServicetest{
    static testMethod void testPBB_QuestionnaireService()    {
        test.startTest(); 
            PBB_TestUtils tu = new PBB_TestUtils ();  
            tu.wfmp = tu.createwfmproject();
            tu.bid =tu.createbidproject();
            tu.sm = tu.createSRMModelAttributes();
            //tu.srmScen = tu.createSRMScenario();
            tu.scen =tu.createscenarioAttributes();
            tu.country =tu.createCountryAttributes();
            tu.psc =tu.createPBBScenarioCountry();  
            tu.smm = tu.createServiceModelAttributes();
            tu.sa = tu.createServiceAreaAttributes();
            tu.sf = tu.createServiceFunctionAttributes();
            tu.St = tu.createServiceTaskAttributes();
            
            tu.siqn = tu.createSIQNAttributes();  
            tu.siqs = tu.createSIQSAttributes('Project');  
            tu.sir = tu.createSIRAttributes(); 
            tu.STtoSI = tu.createSTtoSI(tu.st,tu.siqn); 
            tu.siqs.status__c ='Approved';            
            update tu.siqs;
            
            tu.smm = tu.approveServicemodel(tu.smm);
            tu.Wrm = tu.createWRModelAttributes();
            tu.scen.WR_Model__c  = tu.Wrm.id;
            update tu.scen;
            
            PBB_QuestionnaireService qs = new PBB_QuestionnaireService();
            Map<String,String> TaskIdsMap = new Map<String,String>();
            TaskIdsMap.put(tu.st.id,'PRA');        
            PBB_Services.saveServicetaskforSelectedCountry(tu.scen.id,tu.country.name,TaskIdsMap);
            List<Service_Area__c> salist = PBB_QuestionnaireService.getServiceAreasbyPBBScenarioID(tu.scen.id);
            Map<String,PBB_QuestionnaireService.QuestionWrapper> QWMap = new Map<String,PBB_QuestionnaireService.QuestionWrapper>();
            QWMap  = PBB_QuestionnaireService.getQuestionnairebyServiceArea(tu.sa.id,tu.scen.id); 
            PBB_QuestionnaireService.saveQuestionnairebyServiceArea(tu.sa.id,tu.scen.id,QWMap.get(tu.siqs.id),tu.country.name);
            PBB_QuestionnaireService.showErrorMessage('errorMessage'); 
        test.stopTest();
    }   
}