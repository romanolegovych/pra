/* 
Created by: Abhishek Prasad
Description: Standard Controller Extension for Revenue Rate Uploads 
*/
public class AA_RevenueRateUpload {
    
    public string nameFile {
        get;
        set;
    }
    public Blob contentFile {
        get;
        set;
    }
    
   
    String[] filelines = new String[] {};
    List<Revenue_Allocation_Rate__c > rateupload;
    List<Revenue_Allocation_Rate__c > rateuploadupdate;
    List<Revenue_Allocation_Rate__c > allrateupload;
    Map<String , String> uploadentries;
    Boolean error = false;
    Boolean toupdate = false;
    Boolean update_this_record = false;
    Boolean success = false;
    Boolean objectIdset = false;
    String id3Digit;
    
    public boolean displayUpdatePopUp{
        get;
        set;
    }
    
    public boolean backready{
        get;
        set;
    }
    
    public boolean displayConfirmationPopup {
        get;
        set;
    }
    
    public String message{
        get;
        set;
    }
    
    public AA_RevenueRateUpload(ApexPages.StandardSetController controller) {
    }
    
    public AA_RevenueRateUpload() {
    }
    
    public List<String> Functions{
        get{
            return Functions;
        }
        set;
    }
    public List<String> Years{
        get{
            return Years;
        }
        set;
    }
    public List<String> OldRates{
        get{
            return OldRates;
        }
        set;
    }
    public List<String> NewRates{
        get{
            return NewRates;
        }
        set;
    }
    
    public void setFunctions(List<String> vals){
        Functions = vals;
    }
    public void setYears(List<String> vals){
        Years = vals;
    }
    public void setOldRates(List<String> vals){
        OldRates= vals;
    }
    public void setNewRates(List<String> vals){
        NewRates = vals;
    }
    
    /* Function to handle Upload Butotn Click */
    public Pagereference ReadFile() {
    
        /* read the csv file to generate an array of records */
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        
        /* Declarations */
        rateupload = new List<Revenue_Allocation_Rate__c>();
        rateuploadupdate = new List<Revenue_Allocation_Rate__c>();
        allrateupload = new List<Revenue_Allocation_Rate__c>();
        uploadentries = new Map<String,String>();
        allrateupload.clear();
        rateupload.clear();
        rateuploadupdate.clear();
        
        /* Temporary Lists to hold updateable records */
        List<String> tempFunctions =new List<String>();
        List<String> tempYears =new List<String>();
        List<String> tempOldRates =new List<String>();
        List<String> tempNewRates =new List<String>();
        
        /* Get object Id */
        Revenue_Allocation_Rate__c revId = AA_RevenueAllocationService.getRarId();
        id3Digit= String.valueOf(revId.Id).substring(0, 3);
        
        /* Iterate over one record at a time */
        for (Integer i=1;i<filelines.size();i++) {
            update_this_record =  false;
            String[] inputvalues = new String[] {
            }
            ;
            /* get the present row */
            inputvalues = filelines[i].split(',');
            
            BUF_Code__c bufcode = new BUF_Code__c ();
            Currency__c cur = new Currency__c ();
            Revenue_Allocation_Rate__c revrate = new Revenue_Allocation_Rate__c ();
            
            /* Catch invalid Function Code */
            try {
                bufcode = AA_RevenueAllocationService.getBufCodeId(inputvalues[0]);
            }
            catch(Exception e) {
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Function Code at Row '+ String.valueOf(i) + ': ' + inputvalues[0]);
                ApexPages.addMessage(errormsg);
            }
            /* Catch invalid Year */
            try{
            if(Integer.valueOf(inputvalues[1]) < 1999 || Integer.valueOf(inputvalues[1]) > 2040) {
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Year at Row '+ String.valueOf(i) + ': ' + inputvalues[1]);
                ApexPages.addMessage(errormsg);
            }
            }catch(Exception e){
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Year at Row '+ String.valueOf(i) + ': ' + inputvalues[1]);
                ApexPages.addMessage(errormsg);
            }
            
            /* Catch invalid Currency */
            try {
                cur = AA_RevenueAllocationService.getCurrencyId(inputvalues[3]);
            }
            catch(Exception e) {
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Currency Value at Row '+ String.valueOf(i) + ': ' + inputvalues[3]);
                ApexPages.addMessage(errormsg);
            }
            
            /* Catch invalid Rate*/
            try{
                Decimal d = Decimal.valueOf(inputvalues[2]);
            }
            catch(Exception e) {
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Rate Value at Row '+ String.valueOf(i) + ': ' +  inputvalues[2]);
                ApexPages.addMessage(errormsg);
            }
             /* Catch invalid IsActiveCode */
            if((inputvalues[4].trim().toLowerCase().contains('y') || inputvalues[4].trim().toLowerCase().contains('n')) && inputvalues[4].length()==2){
            }
            else{
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid IsActive Flag (Y/N) at Row '+ String.valueOf(i) + ': ' + inputvalues[4]);
                ApexPages.addMessage(errormsg);
            }
            System.Debug('just before');
            
            /* Catch updateable records */
            try {
                revrate = AA_RevenueAllocationService.checkExistingRecord(bufcode , inputvalues[1]);
                System.Debug('revrate');
                if(revrate!=null){
                    System.Debug('true');
                    toupdate = true;
                    update_this_record = true ;
                    tempFunctions.add(inputvalues[0]);
                    tempYears.add(inputvalues[1]);
                    tempOldRates.add(String.valueOf(revrate.Rate__c));
                    tempNewRates.add(inputvalues[2]);
                 }
            }
            catch(Exception e) {
            }
            
            /* separate into update and insert records */
            if(!error) {
                Revenue_Allocation_Rate__c entry = new Revenue_Allocation_Rate__c();
                if(update_this_record)
                                    entry.Id = revrate.Id;
                entry.Function__c = bufcode.Id;
                entry.Year__c = inputvalues[1];
                entry.Rate__c = Decimal.valueOf(inputvalues[2]);
                entry.Currency__c = cur.Id;
                if(inputvalues[4].contains('Y'))
                                    entry.IsActive__c = true; else
                                    entry.IsActive__c = false;
                allrateupload.add(entry);
                
                /* separate update and insert records */
                if(update_this_record) {
                    rateuploadupdate.add(entry);
                    uploadentries.put(inputvalues[0],inputvalues[1]);
                } else
                    rateupload.add(entry);
            }
        }
        
        /* Show popup if some records are to be updated */
        if(!error){
            if(toupdate) {
                setFunctions(tempFunctions);
                setYears(tempYears);
                setOldRates(tempOldRates);
                setNewRates(tempNewRates);
                showUpdatePopup();
            } else {
                showConfirmationPopup();
            }
        }
        return null;
    }
    
    /* generate finalized table */
    public List<Revenue_Allocation_Rate__c> getuploadedRates() {
        if(success) {
            success = false;
            if (rateupload!= NULL)
                        if (allrateupload.size() > 0 && !error)
                            return allrateupload; 
                         else
                            return null;
            else
               return null;
        } else
               return null;
    }
    
    /* Get data for update popup */
    public List<Revenue_Allocation_Rate__c> getUpdateRecords() {
        Revenue_Allocation_Rate__c rev =  new Revenue_Allocation_Rate__c();
        List<Revenue_Allocation_Rate__c> result  = new List<Revenue_Allocation_Rate__c>();
        result.add(rev);
        return result  ;
    }
    
    public void closeUpdatePopup() {
        allrateupload.clear();
        displayUpdatePopup = false;
    }
    public void closeConfirmationPopup() {
        allrateupload.clear();
        displayConfirmationPopup = false;
    }
    
    /* Upload / Insert Records when confirmed submission */
    public void continueButton() {
        if(!error) {
            try {
                insert rateupload;
                if(toupdate)
                    update rateuploadupdate;
                success = true;
                backready = true;
            }
            catch (Exception e) {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage());
                ApexPages.addMessage(errormsg);
            }
        }
        displayUpdatePopup = false;
        displayConfirmationPopup = false;
    }
    
    public PageReference backpressed() {
        PageReference pageRef=new PageReference('/'+id3Digit+'/o');
        pageRef.setRedirect(false);
        return pageRef; 
    }
    
    public void showUpdatePopup() {
        displayUpdatePopup = true;
    }
    public void showConfirmationPopup() {
        displayConfirmationPopup = true;
    }
}