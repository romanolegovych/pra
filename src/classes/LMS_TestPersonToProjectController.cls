/**
 * Test for PersonToProject controller
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestPersonToProjectController {
    
    static list<Job_Class_Desc__c> jobClasses;
    static list<Job_Title__c> jobTitles;
    static list<WFM_Client__c> clients;
    static list<WFM_Project__c> projects;
    static list<WFM_Contract__c> contracts;
    static list<Country__c> countries;
    static list<Role_Type__c> roleTypes;
    static list<LMS_Role__c> roles;
    static list<Employee_Status__c> statuses;
    static list<Employee_Details__c> employees;
    static list<LMS_Role_Employee__c> roleEmps;
    static list<LMSConstantSettings__c> constants;
    static list<CourseDomainSettings__c> domains;
    static list<RoleAdminServiceSettings__c> settings;
    
    
    static void init() {
        
        constants = new LMSConstantSettings__c[] {
        	new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
    		new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
    		new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
    		new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D'),
    		new LMSConstantSettings__c(Name = 'typePRA', Value__c = 'PRA'),
    		new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
    		new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
    		new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
    		new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
    		new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
    		new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
    		new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
    		new LMSConstantSettings__c(Name = 'statusToDelete', Value__c = 'Delete'),
    		new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended'),
    		new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
    		new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
    		new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
    		new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete')
        };
        insert constants;
        
        domains = new CourseDomainSettings__c[] {
            new CourseDomainSettings__c(Name = 'Internal', Domain_Id__c = 'Internal'),
            new CourseDomainSettings__c(Name = 'PST', Domain_Id__c = 'Project Specific'),
            new CourseDomainSettings__c(Name = 'Archive', Domain_Id__c = 'Archive')
        };
        insert domains;
        
        settings = new RoleAdminServiceSettings__c[] {
            new RoleAdminServiceSettings__c(Name = 'DOMAIN', Value__c = 'DOMAIN'),
            new RoleAdminServiceSettings__c(Name = 'ENDPOINT', Value__c = 'ENDPOINT'),
            new RoleAdminServiceSettings__c(Name = 'TIMEOUT', Value__c = '50000')
        };
        insert settings;
    	
        jobClasses = new Job_Class_Desc__c[] {
            new Job_Class_Desc__c(Name = 'class1', Job_Class_Code__c = 'code1', Job_Class_ExtID__c = 'class1'),
            new Job_Class_Desc__c(Name = 'class2', Job_Class_Code__c = 'code2', Job_Class_ExtID__c = 'class2', Is_Assign__c = true)
        };
        insert jobClasses;
        
        jobTitles = new Job_Title__c[] {
            new Job_Title__c(Job_Code__c = 'code1', Job_Title__c = 'title1', Job_Class_Desc__c = jobClasses[0].Id, Status__c = 'A'),
            new Job_Title__c(Job_Code__c = 'code2', Job_Title__c = 'title3', Job_Class_Desc__c = jobClasses[1].Id, Status__c = 'A')
        };
        insert jobTitles;
        
        clients = new WFM_Client__c[] {
            new WFM_Client__c(Name = 'Client1', Client_Unique_Key__c = 'Client1'),
            new WFM_Client__c(Name = 'Client2', Client_Unique_Key__c = 'Client2')
        };
        insert clients;
        
        contracts = new WFM_Contract__c[] {
            new WFM_Contract__c(Name = 'contract1', Contract_Unique_Key__c = 'contract1', Client_Id__c = clients[0].Id),
            new WFM_Contract__c(Name = 'contract2', Contract_Unique_Key__c = 'contract2', Client_Id__c = clients[1].Id)
        };
        insert contracts;
        
        projects = new WFM_Project__c[] {
            new WFM_Project__c(Name = 'Project1', Project_Unique_Key__c = 'Project1', Contract_Id__c = contracts[0].Id),
            new WFM_Project__c(Name = 'Project2', Project_Unique_Key__c = 'Project2', Contract_Id__c = contracts[1].Id)
        };
        insert projects;
        
        countries = new Country__c[] {
            new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='US/Canada', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry2',PRA_Country_ID__c='200', Country_Code__c='JU',Region_Name__c='Latin America', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry3',PRA_Country_ID__c='300', Country_Code__c='TB',Region_Name__c='Asia/Pacific', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry4',PRA_Country_ID__c='400', Country_Code__c='AB',Region_Name__c='Europe/Africa', 
                               Daily_Business_Hrs__c=8.0)
        };
        insert countries;
        
        roleTypes = new Role_Type__c[] {
        	new Role_Type__c(name = 'PRA'),
        	new Role_Type__c(name = 'Project Specific'),
        	new Role_Type__c(name = 'Additional Role')
        };
        insert roleTypes;
        
        statuses = new Employee_Status__c[] {
            new Employee_Status__c(Employee_Status__c = 'AA', Employee_Type__c = 'Employees'),
            new Employee_Status__c(Employee_Status__c = 'CC', Employee_Type__c = 'Contractor'),
            new Employee_Status__c(Employee_Status__c = 'ZZ', Employee_Type__c = 'Terminated')
        };
        insert statuses;
    }
    
    static void initRoles() {
        roles = new LMS_Role__c[] {
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[1].Id, Project_Id__c = projects[0].Id, Client_Id__c = clients[0].Id, Status__c = 'Active', Sync_Status__c = 'Y', Role_Type__c = roleTypes[1].Id),
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[1].Id, Project_Id__c = null, Client_Id__c = clients[0].Id, Status__c = 'Inactive', Sync_Status__c = 'N', Role_Type__c = roleTypes[1].Id),
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[1].Id, Project_Id__c = projects[0].Id, Client_Id__c = null, Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[1].Id)
        };
        insert roles;
        
        employees = new Employee_Details__c[] {
            new Employee_Details__c(Name = 'employee1', Employee_Unique_Key__c = 'employee1', First_Name__c = 'Andrew', Last_Name__c = 'Allen', Email_Address__c = 'a@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU'),
            new Employee_Details__c(Name = 'employee2', Employee_Unique_Key__c = 'employee2', First_Name__c = 'Allen', Last_Name__c = 'Stephens', Email_Address__c = 'a@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU'),
            new Employee_Details__c(Name = 'employee3', Employee_Unique_Key__c = 'employee3', First_Name__c = 'Allen', Last_Name__c = 'Richard', Email_Address__c = 'r@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU')
        };
        insert employees;
    }
    
    static void initMappings(){
        roleEmps = new LMS_Role_Employee__c[] {
            new LMS_Role_Employee__c(Role_Id__c = roles[0].Id, Employee_Id__c = employees[0].Id, Status__c = 'Draft', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today()),
            new LMS_Role_Employee__c(Role_Id__c = roles[0].Id, Employee_Id__c = employees[1].Id, Status__c = 'Committed', Sync__c = 'Y',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today()),
            new LMS_Role_Employee__c(Role_Id__c = roles[0].Id, Employee_Id__c = employees[2].Id, Status__c = 'Draft Delete', Sync__c = 'Y',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today())
        };
        insert roleEmps;
    }
    
    static void initEmployeeError() {
    	delete employees;
    	employees = new list<Employee_Details__c>();
    	for(Integer i = 0; i < 1001; i++) {
    		employees.add(new Employee_Details__c(Name = 'employee' + i, Employee_Unique_Key__c = 'employee' + i, First_Name__c = 'Andrew' + i, Last_Name__c = 'Allen' + i, 
    			Email_Address__c = i + 'a@a.com', Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', 
    			Department__c = 'Analysis and Reporting', Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', 
    			Date_Hired__c = date.parse('12/27/2009'), Business_Unit__c = 'RDU'));
    	}
    	
    	insert employees;
    }

    static testMethod void testConstructor() {
        init();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        System.assert(c.clientText == '' && c.projectText == '' && c.employeeName == '' && c.jobFamily == '' && 
        	c.employees == null && c.selEmps == null);     
    }
    
    static testMethod void testResetPage() {
        init();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        PageReference pr = c.resetPage();
        System.assert(c.clientText == '' && c.projectText == '' && c.projectFilter == ' and Client_Id__c != \'\' and Client_Id__c != null and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')' && 
        	c.searchFilter == ' and Role_Type__r.Name = \'Project Specific\' and Project_Id__r.Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\') and Status__c = \'Active\'' && pr == null);
    }
    
    static testMethod void testClosePersonSearch() {
        init();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        c.closePersonSearch();
        System.assert(c.selEmps == null);
    }
    
    static testMethod void testGetProjectRoles() {
        init();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        list<SelectOption> options = c.getProjectRoles();
        System.assert(options.size() == 2 && options[1].getValue() == jobClasses[1].Name);
    }
    
    static testMethod void testGetJobFamilyList() {
        init();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        list<SelectOption> options = c.getJobFamilyList();
        System.assert(options.size() == 3 && options[1].getValue() == jobClasses[0].Name && 
                      options[2].getValue() == jobClasses[1].Name);
    }
    
    static testMethod void testProjectSearchType() {
        init();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        c.clientText = 'client';
        c.projectText = '';
        c.projectSearchType();
        System.assert(c.projectFilter == ' and Client_Id__c != \'\' and Client_Id__c != null and Client_Id__c = \'' + c.clientText + '\'');
        
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectSearchType();
        System.assert(c.clientText == clients[0].Name);
        
        c.clientText = '';
        c.projectText = projects[0].Name;
        c.projectSearchType();
        System.assert(c.clientText == clients[0].Name);
    }
    
    static testMethod void testSearchSuccess() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = jobClasses[1].Name;
        c.search();
        System.assert(c.employees.size() == 3);
        
        c.clientText = clients[0].Name;
        c.projectText = '';
        c.projectRole = jobClasses[1].Name;  
        c.search();
        System.assert(c.employees.size() == 0 && c.roleErrorText == 'Role is inactive, enter a different role name');
        
        c.clientText = '';
        c.projectText = projects[0].Name;
        c.projectRole = jobClasses[1].Name; 
        c.search();
        System.assert(c.employees.size() == 0);
        
        c.roleName = clients[0].Name + ' - ' + projects[0].Name + ' - ' + jobClasses[1].Name;
        c.search();
        System.assert(c.employees.size() == 3);
                      
        projects[0].Status_Desc__c = 'Lost';
        update projects;
        c.roleName = clients[0].Name + ' - ' + projects[0].Name + ' - ' + jobClasses[1].Name;
        c.search();
        System.assert(c.employees.size() == 3 && 
        	c.roleErrorText == 'The bid project associated to this role was lost, please search for a different role');
        
        projects[0].Status_Desc__c = 'Closed';
        update projects;
        c.roleName = clients[0].Name + ' - ' + projects[0].Name + ' - ' + jobClasses[1].Name;
        c.search();
        System.assert(c.employees.size() == 3 && 
        	c.roleErrorText == 'The project associate to this role has been closed, please search for a different role');
    }
    
    static testMethod void testSearchFailure() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        c.clientText = clients[1].Name;
        c.projectText = projects[1].Name;
        c.projectRole = jobClasses[1].Name;
        c.search();
        System.assert(c.employees == null);
        
        c.roleName = clients[1].Name + ' - ' + projects[1].Name + ' - ' + jobClasses[1].Name;
        c.search();
        System.assert(c.employees == null);
    }
    
    static testMethod void testPersonSearchSuccess() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = jobClasses[1].Name;
        c.search();
        
        c.employeeName = employees[0].Last_Name__c + ', ' + employees[0].First_Name__c;
        c.jobFamily = null;
        c.personSearch();
        System.assert(c.selEmps.size() == 1 && c.selEmps[0].firstName == employees[0].First_Name__c);
        
        c.employeeName = employees[0].Last_Name__c;
        c.jobFamily = null;
        c.personSearch();
        System.assert(c.selEmps.size() == 1 && c.selEmps[0].lastName == employees[0].Last_Name__c);
        
        c.employeeName = employees[0].Last_Name__c + ',';
        c.jobFamily = 'Accountant';
        c.personSearch();
        System.assert(c.selEmps.size() == 1 && c.selEmps[0].lastName == employees[0].Last_Name__c);
    }
    
    static testMethod void testPersonSearchFailure() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = jobClasses[1].Name;
        c.search();
        
        c.employeeName = 'Allen, John';
        c.personSearch();
        System.assert(c.selEmps.size() == 0);
    }
    
    static testMethod void testTooManyPersonSearchFailure() {
        init();
        initRoles();
        initMappings();
        initEmployeeError();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = jobClasses[1].Name;
        c.search();
        
        c.employeeName = '';
        c.jobFamily = '';
        c.personSearch();
        system.assert(c.selEmps.size() == 0 && 
        	c.personErrorText == 'The result contains too many records. Please refine your search criteria and run the search again.');
    }
	
    static testMethod void testAddPerson() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = jobClasses[1].Name;
        c.search();
        
        c.employeeName = employees[0].Last_Name__c + ', ' + employees[0].First_Name__c;
        c.jobFamily = null;
        c.personSearch();
        c.selEmps[0].selected = true;
        
        PageReference pr = c.addPerson();
        System.assert(pr == null);
    }
    
    static testMethod void testCommitPerson() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = jobClasses[1].Name;
        c.search();
        
        PageReference pr = c.commitPerson();
        System.assert(pr == null);
    }
	
	static testMethod void testApplyCommitDate(){
		init();
		initRoles();
		initMappings();
		LMS_PersonToProjectController c = new LMS_PersonToProjectController();
		c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = jobClasses[1].Name;
        c.search();
		
		c.employees[0].selected = true;
		c.commitDate = '12/12/2102';
		PageReference pr = c.setCommitDate();
		System.assert(pr == null);
	}
    
    static testMethod void testCancel() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = jobClasses[1].Name;
        c.search();
        
        PageReference pr = c.cancel();
        System.assert(pr == null);
        
        c.cancelCreateRole();
        system.assert(c.roles == null);
    }
	
    static testMethod void testRemovePerson() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToProjectController c = new LMS_PersonToProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = jobClasses[1].Name;
        c.search();
        for(LMS_PersonAssignment a : c.employees) {
        	a.selected = true;
        }
        PageReference pr = c.removePersons();
        System.assert(pr == null);
    }
    
    static testMethod void testPersonReset() {
    	init();
    	LMS_PersonToProjectController c = new LMS_PersonToProjectController();
    	c.employeeName = 'name';
    	c.jobFamily = 'jf';
    	c.personReset();
    	System.assert(c.employeeName == '' && c.jobFamily == '' && c.selEmps == null);
    }
    
    static testMethod void testIsValidClientId() {
    	init();
    	LMS_PersonToProjectController c = new LMS_PersonToProjectController();
    	c.clientText = clients[0].Name;
    	Boolean isValid = LMS_PersonToProjectController.isValidClientId(c.clientText);
    	System.assert(isValid == true);
    	
    	c.clientText = 'invalidClient';
    	isValid = LMS_PersonToProjectController.isValidClientId(c.clientText);
    	System.assert(isValid == false);
    }
    
    static testMethod void testIsValidProjectId() {
    	init();
    	LMS_PersonToProjectController c = new LMS_PersonToProjectController();
    	c.projectText = projects[0].Name;
    	Boolean isValid = LMS_PersonToProjectController.isValidProjectId(c.projectText);
    	System.assert(isValid == true);
    	
    	c.projectText = 'invalidProject';
    	isValid = LMS_PersonToProjectController.isValidProjectId(c.projectText);
    	System.assert(isValid == false);
    }
    
    static testMethod void testIsValidClientProjectPair() {
    	init();
    	LMS_PersonToProjectController c = new LMS_PersonToProjectController();
    	c.clientText = clients[0].Name;
    	c.projectText = projects[0].Name;
    	Boolean isValid = LMS_PersonToProjectController.isValidClientProjectPair(c.clientText, c.projectText);
    	System.assert(isValid == true);
    	
    	c.clientText = 'invalidClient';
    	c.projectText = projects[0].Name;
    	isValid = LMS_PersonToProjectController.isValidClientProjectPair(c.clientText, c.projectText);
    	System.assert(isValid == false);
    	
    	c.clientText = clients[0].Name;
    	c.projectText = 'invalidProject';
    	isValid = LMS_PersonToProjectController.isValidClientProjectPair(c.clientText, c.projectText);
    	System.assert(isValid == false);
    }
    
    static testMethod void testUnimplementedMethods() {
    	init();
    	LMS_PersonToProjectController c = new LMS_PersonToProjectController();
    	c.viewImpact();
    	c.showErrors();
    }
    
}