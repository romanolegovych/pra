public with sharing class PAWS_ProjectFlowTrigger extends STSWR1.AbstractTrigger
{
	public override void beforeInsert(List<SObject> records)
	{
		updateProjectName(records);
	}
	
	public override void beforeUpdate(List<SObject> newRecords, List<SObject> oldRecords)
	{
		updateProjectName(newRecords);
	}
	
	public override void afterUpdate(List<SObject> newRecords, List<SObject> oldRecords)
	{
		updateChildRecordsLookups(newRecords, oldRecords);
		updateFlowsLocation(newRecords, oldRecords);
	}
	
	public override void beforeDelete(List<SObject> records)
	{		
		PAWS_Utilities.checkLimits(new Map<String, Integer> 
		{
			'Queries' => 1,
			'DMLStatements' => 1
		});
		
		delete [select Id from PAWS_Project_Flow_Junction__c where Project__c in :records];
	}
	
	/********************************** PRIVATE SECTION ************************************/
	
	private void updateProjectName(List<ecrf__c> records)
	{
		for (ecrf__c project : records)
		{
			project.Name = project.PAWS_Project_ID_Generated__c;
		}
	}
	
	private void updateChildRecordsLookups(List<ecrf__c> newRecords, List<ecrf__c> oldRecords)
	{
		Set<ID> modifiedProjects = new Set<ID>();
		
		ecrf__c newRec;
		ecrf__c oldRec;
		
		for (Integer i = 0; i < newRecords.size(); i++)
		{
			newRec = newRecords.get(i);
			oldRec = oldRecords.get(i);
			
			if (newRec.Project_ID__c != oldRec.Project_ID__c || newRec.Protocol_ID__c != oldRec.Protocol_ID__c)
			{
				modifiedProjects.add(newRec.Id);
			}
		}
		
		//trigger triggers on the child objects
		if (!modifiedProjects.isEmpty() || Test.isRunningTest())
		{
			PAWS_Utilities.checkLimits(new Map<String, Integer> 
			{
				'Queries' => 5,
				'DMLStatements' => 5
			});
		
			update [select Id from PAWS_Project_Flow_Country__c    where PAWS_Project__c in :modifiedProjects];
			update [select Id from PAWS_Project_Flow_Site__c       where PAWS_Project__c in :modifiedProjects];
			update [select Id from PAWS_Project_Flow_Agreement__c  where PAWS_Project__c in :modifiedProjects];
			update [select Id from PAWS_Project_Flow_Document__c   where PAWS_Project__c in :modifiedProjects];
			update [select Id from PAWS_Project_Flow_Submission__c where PAWS_Project__c in :modifiedProjects];
		}
	}
	
	private void updateFlowsLocation(List<ecrf__c> newRecords, List<ecrf__c> oldRecords)
	{
		Set<ID> projectsTypeIsSetToRegular = new Set<ID>();
		Set<ID> projectsTypeIsSetToProposal = new Set<ID>();
		
		ecrf__c newRec;
		ecrf__c oldRec;
		
		for (Integer i = 0; i < newRecords.size(); i++)
		{
			newRec = newRecords.get(i);
			oldRec = oldRecords.get(i);
			
			if (newRec.Type__c == 'Regular' && newRec.Type__c != oldRec.Type__c)
			{
				projectsTypeIsSetToRegular.add(newRec.Id);
			}
			
			if (newRec.Type__c == 'Proposal' && newRec.Type__c != oldRec.Type__c)
			{
				projectsTypeIsSetToProposal.add(newRec.Id);
			}
		}
		
		if (projectsTypeIsSetToRegular.size() > 0) moveProjectFlows('PAWS Project Flows', projectsTypeIsSetToRegular);
		if (projectsTypeIsSetToProposal.size() > 0) moveProjectFlows('Proposal Projects', projectsTypeIsSetToProposal);
	}
	
	private void moveProjectFlows(String folderName, Set<ID> projects)
	{
		PAWS_Utilities.checkLimits(new Map<String, Integer> 
		{
			'Queries' => 3,
			'DMLStatements' => 3
		});
		
		List<STSWR1__Item__c> folders = [Select Id From STSWR1__Item__c Where Name = :folderName And STSWR1__Type__c = 'Folder' And STSWR1__Parent__c = null limit 1];
		STSWR1__Item__c targetFolder = (folders.isEmpty() ? null : folders.get(0));
		
		if (targetFolder == null)
		{
			targetFolder = new STSWR1__Item__c(Name = folderName, STSWR1__Parent__c = null);
			insert targetFolder;
		}
		
		List<PAWS_Project_Flow_Junction__c> flowJunctions = [Select Folder__r.Name, Folder__r.STSWR1__Parent__r.Name From PAWS_Project_Flow_Junction__c Where Project__c in :projects];
		
		Map<String, STSWR1__Item__c> flowFoldersInTargetFolder = new Map<String, STSWR1__Item__c>();
		for (PAWS_Project_Flow_Junction__c flowJunction : flowJunctions)
		{
			String flowFolderName = (flowJunction.Folder__r.STSWR1__Parent__r != null ? flowJunction.Folder__r.STSWR1__Parent__r.Name : flowJunction.Folder__r.Name);
			flowFoldersInTargetFolder.put(flowFolderName, new STSWR1__Item__c(Name = flowFolderName, STSWR1__Parent__c = targetFolder.Id));
		}
		
		for (STSWR1__Item__c folder : [Select Name From STSWR1__Item__c Where Name = :flowFoldersInTargetFolder.keySet() And STSWR1__Type__c = 'Folder' And STSWR1__Parent__c = :targetFolder.Id])
		{
			flowFoldersInTargetFolder.put(folder.Name, folder);
		}
		
		upsert flowFoldersInTargetFolder.values();
		
		List<STSWR1__Item__c> itemsForUpdate = new List<STSWR1__Item__c>();
		for (PAWS_Project_Flow_Junction__c flowJunction : flowJunctions)
		{
			String flowFolderName = (flowJunction.Folder__r.STSWR1__Parent__r != null ? flowJunction.Folder__r.STSWR1__Parent__r.Name : flowJunction.Folder__r.Name);
			itemsForUpdate.add(
				new STSWR1__Item__c(
					Id = flowJunction.Folder__r.id,
					STSWR1__Parent__c = flowFoldersInTargetFolder.get(flowFolderName).Id
				)
			);
		}
		update itemsForUpdate;
	}
}