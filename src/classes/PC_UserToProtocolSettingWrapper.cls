public class PC_UserToProtocolSettingWrapper {
	public Id 			protocolId		{ get; private set; }
    public Id 			userId			{ get; private set; }
    public String 		name			{ get; private set; }
    public String 		value			{ get; private set; }
    
    public PC_UserToProtocolSettingWrapper( Id protocolId,
                                            Id userId,
                                            String name,
                                            String value ){
        protocolId = this.protocolId;
        userId = this.userId;
		name = this.name;
        value = this.value;
    }
}