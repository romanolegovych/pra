/** Implements the test for the Service Layer of the object ServiceCategory__c
 * @author	Dimitrios Sgourdos
 * @version	30-Jan-2014
 */
@isTest
private class ServiceCategoryServiceTest {
	
	/** Test the function getFirstLevelServiceCategories.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	10-Dec-2013
	 */
	static testMethod void getFirstLevelServiceCategoriesTest() {
		List<ServiceCategory__c> results = ServiceCategoryService.getFirstLevelServiceCategories();
	}
	
	
	/** Test the function createSelectOptionsFromServiceCategories.
	 * @author	Dimitrios Sgourdos
	 * @version	11-Dec-2013
	 */
	static testMethod void createSelectOptionsFromServiceCategoriesTest() {
		// Create data
		List<ServiceCategory__c> sourceList = new List<ServiceCategory__c>();
		sourceList.add(new ServiceCategory__c(Code__c='001',name='Service Category (1)'));
		sourceList.add( new ServiceCategory__c(Code__c='002',name='Service Category (2)') );
		insert sourceList;
		
		String errorMessage = 'Error in creating select options list from Service Categories';
		
		// Check the function
		List<SelectOption> results = new List<SelectOption>();
		results = ServiceCategoryService.createSelectOptionsFromServiceCategories(sourceList, 'Please Select...');
		
		system.assertEquals(results.size(), 3, errorMessage);
		system.assertEquals(results[0].getValue(), '', errorMessage);
		system.assertEquals(results[0].getLabel(), 'Please Select...', errorMessage);
		system.assertEquals(results[1].getValue(), sourceList[0].Id, errorMessage);
		system.assertEquals(results[1].getLabel(), '1 Service Category (1)', errorMessage);
		system.assertEquals(results[2].getValue(), sourceList[1].Id, errorMessage);
		system.assertEquals(results[2].getLabel(), '2 Service Category (2)', errorMessage);
	}
	
	
	/** Test the function sortServiceCategorySelectList.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Jan-2014
	 */
	static testMethod void sortServiceCategorySelectListTest() {
		// Create data
		List<SelectOption> source = new List<SelectOption>();
		source.add(new SelectOption('First','1.1 Srv11'));
		source.add(new SelectOption('Third','11.1.2 Srv1112'));
		source.add(new SelectOption('Second','2.2 Srv22'));
		
		
		// Check the function
		String errorMessage = 'Error in sorting the Service Category Select Option List';
		List<SelectOption> results = ServiceCategoryService.sortServiceCategorySelectList(source);
		
		system.assertEquals(3, results.size(), errorMessage);
		
		system.assertEquals('First', results[0].getValue(), errorMessage);
		system.assertEquals('1.1 Srv11', results[0].getLabel(), errorMessage);
		
		system.assertEquals('Second', results[1].getValue(), errorMessage);
		system.assertEquals('2.2 Srv22', results[1].getLabel(), errorMessage);
		
		system.assertEquals('Third', results[2].getValue(), errorMessage);
		system.assertEquals('11.1.2 Srv1112', results[2].getLabel(), errorMessage);
	}
	
	
	/** Test the function createServiceCategoryShuttle.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Jan-2014
	 */
	static testMethod void createServiceCategoryShuttleTest() {
		// Create data
		List<ServiceCategory__c> sourceList = new List<ServiceCategory__c>();
		sourceList.add(new ServiceCategory__c(Code__c='001',name='Shown Service Category'));
		sourceList.add( new ServiceCategory__c(Code__c='002',name='Not Shown Service Category') );
		insert sourceList;
		
		List<Service__c> srvList = new List<Service__c>();
		srvList.add(new Service__c(ServiceCategory__c = sourceList[0].id, Name = 'a', IsDesignObject__c = true));
		srvList.add(new Service__c(ServiceCategory__c = sourceList[1].id, Name = 'a', IsDesignObject__c = false));
		insert srvList;
		
		// Check the function
		String errorMessage = 'Error in creating shuttle for Service Categories';
		
		BDT_Utils.SelectOptionShuttle result =  ServiceCategoryService.createDesignServiceCategoryShuttle();
		
		system.assertEquals(1, result.allOptions.size(), errorMessage);
		system.assertEquals(0, result.usedOptions.size(), errorMessage);
		system.assertEquals(sourceList[0].Id, result.allOptions[0].getValue(), errorMessage);
		system.assertEquals('1 Shown Service Category', result.allOptions[0].getLabel(), errorMessage);
	}
	
	
	static testMethod void getAllCatergoriesRelatedToDesignObjectsTest() {
		
		List<ServiceCategory__c> cat_forTrue = BDT_TestDataUtils.buildServiceCategory(1, null);
		List<ServiceCategory__c> cat_forFalse = BDT_TestDataUtils.buildServiceCategory(1, null);
		insert cat_forTrue;
		insert cat_forFalse;
		
		List<Service__c> ser_true = BDT_TestDataUtils.buildService(cat_forTrue, 2, 'Tst', true);
		List<Service__c> ser_false = BDT_TestDataUtils.buildService(cat_forFalse, 2, 'Tst', false);
		insert ser_true;
		insert ser_false;
				
		List<ServiceCategory__c> result =  ServiceCategoryService.getAllCatergoriesRelatedToDesignObjects ();
		
		system.assertEquals(cat_forTrue[0].id,result[0].id);
		
	}
}