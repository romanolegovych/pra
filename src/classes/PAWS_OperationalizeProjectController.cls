public without sharing class PAWS_OperationalizeProjectController
{
	public String ProjectId {get; set;}
	public Boolean IsError {get; set;}
	
	public void Init()
	{
		try
		{
			ProjectId = ApexPages.currentPage().getParameters().get('id');
			IsError = false;
			
			if (String.isEmpty(ProjectId)) throw new PAWS_API.PAWS_APIException('Project ID is empty.');
			ID validateId = ID.valueOf(ProjectId);
		}
		catch(Exception ex)
		{
			HandleException(ex);
		}
	}
	
	public List<Action> GetActionsList()
	{
		List<Action> actionsList = new List<Action>();
		
		actionsList.add(new Action('Processing RM integration...', 'function(){invokeRM_API();}'));
		actionsList.add(new Action('Finalizing changes...', 'function(){finalize();}'));
		
		return actionsList;
	}
	
	public String GetActionsListJSON()
	{
		return JSON.serialize(GetActionsList());
	}
	
	public void Finalize()
	{
		try
		{
			UpdateProjectType('Regular');
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Project has been operationalized successfully!'));
		}
		catch(Exception ex)
		{
			HandleException(ex);
		}
	}
	
	public void InvokeRM_API()
	{
		try
		{
			List<PAWS_Project_Flow_Junction__c> flowJunctions = [Select Project__r.Project_ID__r.Name, Flow__c From PAWS_Project_Flow_Junction__c Where Project__c = :ProjectId Order By CreatedDate limit 1];
			
			if (flowJunctions.isEmpty()) return;
			
			PAWS_Project_Flow_Junction__c flowJunction = flowJunctions.get(0);
			List<ID> flowIds = new List<ID>{flowJunction.Flow__c};
			
			Set<ID> ids = new Set<ID>{flowJunction.Flow__c};
			while(ids.size() > 0)
			{
				List<STSWR1__Flow_Step_Action__c> actions = [select STSWR1__Flow_Step__c, STSWR1__Flow_Step__r.STSWR1__Flow__c, STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active' and STSWR1__Flow_Step__r.STSWR1__Flow__c = :ids];
				
				ids.clear();
				for(STSWR1__Flow_Step_Action__c action : actions)
				{
					Map<String, Object> config = (Map<String, Object>) JSON.deserializeUntyped(action.STSWR1__Config__c);
					if(config != null && !String.isEmpty((String)config.get('flowId')))
					{
						ids.add((String) config.get('flowId'));
						flowIds.add((String) config.get('flowId'));
					}
				}
			}
			
			String projectName = flowJunction.Project__r.Project_ID__r.Name;
			PAWS_API.updateLookupIDsForPAWSProjectFlows(projectName, ProjectId);
			PAWS_API.updateLookupIDsForPAWSProjectFlowsSwimlanes(projectName, flowIds);
		}
		catch(Exception ex)
		{
			HandleException(ex);
		}
	}
	
	/*public void Rollback()
	{
		try
		{
			UpdateProjectType('Proposal');
		}
		catch(Exception ex){/do nothing/}
	}*/
	
	public void HandleException(Exception ex)
	{
		ApexPages.addMessages(ex);
		System.debug(System.LoggingLevel.ERROR, '-----Stacktrace: \n' + ex.getStacktraceString());
		IsError = true;
	}
	
	private void UpdateProjectType(String newType)
	{
		ecrf__c pawsProject = [Select Type__c From ecrf__c Where Id = :ProjectId];
		pawsProject.Type__c = newType;
		update pawsProject;
	}
	
	/******************** HELPER CLASSES ********************/
	public class Action
	{
		public String label {get; set;}
		public String fn {get; set;}
		
		public Action(String label, String jsFn)
		{
			this.label = label;
			this.fn = jsFn;
		}
	}
}