/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AutoCompFieldControllerUTest {

    static Country__c country;
    static List<WFM_COUNTRY_HOURS__c> hrs; 
    static Employee_Details__c  empl;
    static Employee_Details__c  emplMove;
    static WFM_Project__c project;
    static WFM_Project__c projectB;
    static WFM_Client__c client;
  
    
    static void init(){
        country = new Country__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0);
        insert country;
        
           
    
        empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='1000', Employee_Unique_Key__c='1000', First_Name__c='John', Last_Name__c='sssss',email_Address__c='JSmith@gmail.com', 
        Function_code__c='CR',Buf_Code__c='KCICR', Job_Class_Desc__c='Clinical Research Associate', Business_Unit__c='RDU', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'));
        insert empl;
        emplMove = new Employee_Details__c(COUNTRY_name__c = country.id, name='1010', Employee_Unique_Key__c='1010', First_Name__c='Cathrine', Last_Name__c='Smith',email_Address__c='CSmith@gmail.com', 
        Function_code__c='CY',Buf_Code__c='KCICY', Job_Class_Desc__c='Clinical Research Associate', Business_Unit__c='KCI', Status__c='AA', Date_Hired__c=date.parse('12/27/2011'));
        insert emplMove;
        
         //need to insert project
        client = new WFM_Client__c(name='TestClient', Client_Name__c='TestClinet_Name', Client_Unique_Key__c='TestClient');
        insert client;
        WFM_Contract__c contract = new WFM_Contract__c(name='TestContract', Contract_Status__c='AA', Client_ID__c=client.id, Contract_Unique_Key__c='TestContract');
        insert contract;
        project = new WFM_Project__c(name='TestProject', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProject', project_status__c ='RM',  Status_Desc__c='Active');
        insert project;
        projectB = new WFM_Project__c(name='TestProjectB', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProjectB', project_status__c ='BB',  Status_Desc__c='Bid');
        insert projectB; 
    }
    static testMethod void TestAutoComplete() {
        
        init();   
      
        string query = 'and Status_Desc__c <> \'Closed\'';
        list<WFM_Project__c> proj= AutoCompFieldController.findSObjects('WFM_Project__c', 'name', 'Test', '', query, true, false);       
        
        system.assert(proj[0].name == project.name );  
      
        proj= AutoCompFieldController.findSObjects('WFM_Project__c', 'name', 'Tests', '', query, true, false);       
        
        system.assert(proj[0].name == 'No Record' );  

    }
    static testMethod void TestAutoCompleteWithAdditionalField() {
        
        init();   
      
        string query = ' and Term_Date__c = null';
        list<Employee_Details__c> employee= AutoCompFieldController.findSObjects('Employee_Details__c', 'Last_Name__c', 'ssss', 'First_Name__c', query, true, false);       
        
        system.assert(employee[0].Last_Name__c == empl.Last_Name__c );  
      
        employee= AutoCompFieldController.findSObjects('Employee_Details__c', 'Last_Name__c', 'sssssss', 'First_Name__c', query, true, false);       
        system.assert(employee[0].First_Name__c == 'No Record' );  
    }
}