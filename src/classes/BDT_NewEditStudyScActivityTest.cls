@isTest
public with sharing class BDT_NewEditStudyScActivityTest {

	static testmethod void BDT_NewEditStudyScActivityTest(){
		// Create data
		Subcontractor__c sc = new Subcontractor__c(Name='testsc');
		insert sc;
		SubcontractorActivity__c sca = new SubcontractorActivity__c(Subcontractor__c = sc.Id, Name='testtest');
		insert sca;
		Client_Project__c firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		ProjectSubcontractorActivity__c psa = new ProjectSubcontractorActivity__c(ClientProject__c = firstProject.id, SubcontractorActivity__c = sca.Id);
		insert psa;
		
		// Create the controller
		BDT_NewEditStudySubcontractorActivity a = new BDT_NewEditStudySubcontractorActivity();
		
		// Check that the subcontractor list has been created
		system.assertNotEquals(null, a.subcontractorOptionList );
		
		// Check the function buildActivitySelector
		a.SubcontractorId = sc.Id;
		a.projectId = firstProject.Id;
		a.buildActivitySelector();
		system.assert(a.ActivitiesPerSCSelected.size() > 0);
		system.assert(a.ActivitySelector.size() > 0);
		system.assert(a.SelectedActivities.size() > 0);
		
		// Check the save
		for(Integer i=0; i<a.ActivitySelector.size(); i++) {
			a.ActivitySelector[i].isSelected = true;
		}
		a.save();
		for(Integer i=0; i<a.ActivitySelector.size(); i++) {
			a.ActivitySelector[i].isSelected = false;
		}
		a.save();
		
		// Check cancel()
		system.assertNotEquals(null, a.cancel() );
	}
}