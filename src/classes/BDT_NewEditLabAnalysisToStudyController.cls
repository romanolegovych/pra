/** The Controller of the page BDT_NewEditLabAnalysisToStudy
 * @author	Dimitrios Sgourdos
 * @version	30-Oct-2013
 */	
public with sharing class BDT_NewEditLabAnalysisToStudyController {
	
	// Global Variables  
	public Boolean							 editMode				{get;set;}
	// Securities
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA 	{get;set;} // Methods and compounds to study securities
	public BDT_SecuritiesService.pageSecurityWrapper securitiesB 	{get;set;} // Lab method securities 
	// 'Edit Method and compounds' section
	public Boolean							 showSaveWarning		{get;set;}
	public List<Study__c>					 studiesList			{get;set;} 
	public List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer> methodAndCompList {get;set;} 
	public Integer							 removeMethodIndex		{get;set;}
	private List<LaboratoryAnalysis__c> 	 deletedList;	
	// 'More Details of Method' section
	public Boolean							 showMethodMoreDetails 	{get;set;}
	public LaboratoryMethod__c				 labMethod 				{get;set;}
	public String							 labMethodId			{get;set;}
	public List<LaboratoryMethodComedicationService.MethodComedicationWrapper>	comedicationWrapperList   {get;set;}
	public String							 activeTabValue 		{get{return 'BasicInfo';} set;}
	// 'Search and add methods' section
	public LaboratoryMethod__c				 searchCriterialabMethod{get;set;}
	public List<LaboratoryMethodCompound__c> compoundItemsList	 	{get;set;}
	public Integer							 rowIndexDeletedCompound{get;set;}
	public List<SelectOption>				 SpeciesList			{get;set;}
	public List<SelectOption>				 MatricesList			{get;set;}
	public List<SelectOption>				 AntiCoagulantList		{get;set;}
	public List<SelectOption>				 AnalyticalTechList		{get;set;}
	public List<SelectOption>				 DetectionTechList		{get;set;}
	public List<SelectOption>				 LocationList			{get;set;}
	public List<SelectOption>				 DepartmentList			{get;set;}
	public List<SelectOption>				 ProprietaryList		{get;set;}
	public Boolean							 antiCoagulantFlag	 	{get;set;}
	public Boolean							 detectionTechFlag	 	{get;set;}
	public Boolean							 showCreationWarning	{get;set;}
	// 'Search results' section
	public  Boolean							 showResults			{get;set;}
	private Boolean							 previouslyShowResults;
	public List<LaboratoryMethod__c>		 resultsLabMethodList	{get;set;}
	public String							 addingMethodId			{get;set;} 	
	
	
	/** The constructor of the class.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Oct-2013
	 */	
	public BDT_NewEditLabAnalysisToStudyController() {
		init();
		// Find the mode of the page
		editMode = (system.currentPageReference().getParameters().get('editMode') == 'TRUE'); 
		// 'Edit Methods and Compounds' section
		studiesList = StudyService.getStudiesByUserPreference();
		getEditMethodAndCompoundsectionData();
		// 'Search and add methods' section
		if( ! editMode ) {	
			readSearchCriteriaDropDownLists();
			enableAntiCoagulant();
			enableDetectionTechnique();
			addCompoundOption();
		}
	}
	
	
	/** Initialize the global variables of the controller.
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */	
	private void init() {
		// Initialize securities
		securitiesA 			= new BDT_SecuritiesService.pageSecurityWrapper('Methods, Compounds to Studies');
		securitiesB 			= new BDT_SecuritiesService.pageSecurityWrapper('Lab method');
		// Show/Hide page sections flags
		showMethodMoreDetails	= false;
		showCreationWarning		= false;
		showSaveWarning			= false;
		// 'Edit Method and compounds' section
		studiesList				= new List<Study__c>();
		methodAndCompList	 	= new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		deletedList				= new List<LaboratoryAnalysis__c>();
		// 'More Details of Method' section
		labMethod				= new LaboratoryMethod__c();
		comedicationWrapperList = new List<LaboratoryMethodComedicationService.MethodComedicationWrapper>();
		// 'Search dand add methods' section
		searchCriterialabMethod = new LaboratoryMethod__c();
		compoundItemsList		= new List<LaboratoryMethodCompound__c>(); 
		SpeciesList				= new List<SelectOption>();
		MatricesList			= new List<SelectOption>();
		AntiCoagulantList		= new List<SelectOption>();
		AnalyticalTechList		= new List<SelectOption>();
		DetectionTechList		= new List<SelectOption>();
		LocationList			= new List<SelectOption>(); 
		DepartmentList			= new List<SelectOption>(); 
		ProprietaryList			= new List<SelectOption>();
		resultsLabMethodList	= new List<LaboratoryMethod__c>();
	}
	
	
	/**	Get the data for the 'Edit Methods and Compounds' section.
	 * @author	Dimitrios Sgourdos
	 * @version	16-Oct-2013
	 * @return	The list of the wrapper that holds the data.
	 */
	public void getEditMethodAndCompoundsectionData() {
		methodAndCompList = LaboratoryMethodCompoundService.getMethodCompoundStudyAssignmentContainerData(studiesList);
	}
	
	
	/**	Show the details of the selected method.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Oct-2013
	 */
	public void showMoreDetailsOfMethod() {
		showCreationWarning	= false;
		showSaveWarning		= false;
		labMethod = LaboratoryMethodService.getLabMethodById(labMethodId);
		showMethodMoreDetails = true;
		previouslyShowResults = showResults;
		showResults = false;
		if(labMethod != NULL) {
			comedicationWrapperList = LaboratoryMethodComedicationService.createMethodComedicationWrapperList(labMethod.LaboratoryMethodComedication__r, labMethod.LaboratoryMethodCompound__r);
		} else {
			labMethod = new LaboratoryMethod__c();
			comedicationWrapperList = new List<LaboratoryMethodComedicationService.MethodComedicationWrapper>();
		}
	}
	
	
	/**	Close the 'More details of Method' section.
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	public void closeMoreDetailsSection() {
		labMethod = new LaboratoryMethod__c();
		comedicationWrapperList = new List<LaboratoryMethodComedicationService.MethodComedicationWrapper>();
		showCreationWarning	  = false;
		showSaveWarning		  = false;
		showMethodMoreDetails = false; 
		showResults = previouslyShowResults;
	}
	
	
	/**	Close the page.
	 * @author	Dimitrios Sgourdos
	 * @version	16-Oct-2013
	 * @return	The characteristics page
	 */
	public PageReference cancelEdit() {
		PageReference BDT_CharacteristicsPage = new PageReference(System.Page.BDT_Characteristics.getUrl());
		return BDT_CharacteristicsPage;
	}
	
	
	/**	Open the confirmation dialog for saving the method and compounds to study assignment in case there are unassigned compounds.
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	public PageReference requestSaveEdit() {
		showCreationWarning	= false;
		showSaveWarning		= LaboratoryMethodCompoundService.checkForUnassignedCompoundToStudyAssignment(methodAndCompList);
		if( ! showSaveWarning) {
			return saveEdit();
		}
		return NULL;
	}
	
	
	/**	Save the Methods and Compounds to Studies Assignment.
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 * @return	The characteristics page in case of successful save, else NULL with an erro message.
	 */
	public PageReference saveEdit() {
		showCreationWarning	= false;
		showSaveWarning		= false;
		Boolean successFlag = LaboratoryMethodCompoundService.saveMethodCompoundStudyAssignment(methodAndCompList);
		if( ! successFlag ) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The save was unsuccessful! Please, try again!');
			ApexPages.addMessage(msg);
			return NULL;
		} else {
			successFlag = LaboratoryAnalysisService.deleteAnalysisList(deletedList);
			if( ! successFlag ) {
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The deletion of assignment analysis was unsuccessful! Please, try again!');
				ApexPages.addMessage(msg);
				return NULL;
			}
		}
		return cancelEdit();
	}
	
	
	/**	Delete all the laboratory analysis that is associated with the selected method (through its compounds).
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	public void removeMethodFromEditMethodAndCompoundsContainer() {
		showCreationWarning	= false;
		showSaveWarning		= false;
		deletedList.addAll(LaboratoryMethodCompoundService.deleteMethodCompoundStudyAssignment(methodAndCompList, removeMethodIndex));
		Integer endMethodIndex = removeMethodIndex
					+ LaboratoryMethodService.getNumberOfCompounds(methodAndCompList[removeMethodIndex].methodCompound.LaboratoryMethod__c)
					- 1;
		methodAndCompList = LaboratoryMethodCompoundService.removeRecordsFromMethodCompoundStudyAssignmentContainer(methodAndCompList, removeMethodIndex, endMethodIndex);
	}
	
	
	/**	Read/Create the drop-down lists of the search criteria.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Oct-2013
	 */
	public void readSearchCriteriaDropDownLists() {
		// Read all the Laboratory Selection Lists values
		List<LaboratorySelectionLists__c> lslList = new List<LaboratorySelectionLists__c>();
		lslList = LaboratorySelectionListsService.getAllSelectionListValues();
		// Read the map that keeps the Laboratory Selection Lists values splitted depends on their category
		Map<String, List<LaboratorySelectionLists__c>> lslMap = new Map<String, List<LaboratorySelectionLists__c>>(); 
		lslMap = LaboratorySelectionListsService.splitLaboratorySelectionListsValuesPerCategoryInMap(lslList);
		// Read the laboratory selection lists drop-down lists
		SpeciesList 	   = LaboratorySelectionListsService.retrieveValuesForCategoryFromMap(lslMap, 'Species', 			   true,  true);
		MatricesList 	   = LaboratorySelectionListsService.retrieveValuesForCategoryFromMap(lslMap, 'Matrices', 			   true,  true);
		AnalyticalTechList = LaboratorySelectionListsService.retrieveValuesForCategoryFromMap(lslMap, 'Analytical Techniques', true,  true);
		AntiCoagulantList  = LaboratorySelectionListsService.retrieveValuesForCategoryFromMap(lslMap, 'Anti-coagulants', 	   false, true);
		DetectionTechList  = LaboratorySelectionListsService.retrieveValuesForCategoryFromMap(lslMap, 'Detection Techniques',  false, true);
		// Create Location, Department and Proprietary Lists
		LocationList	= LaboratoryMethodService.getSelectOptionsFromLabMethodPickListField('Location__c',    true, true);
		DepartmentList	= LaboratoryMethodService.getSelectOptionsFromLabMethodPickListField('Department__c',  true, true);
		ProprietaryList = LaboratoryMethodService.getSelectOptionsFromLabMethodPickListField('Proprietary__c', true, true);
	}
	
	
	/**	Add an empty compound in the list of Compounds.
	 * @author	Dimitrios Sgourdos
	 * @version	18-Oct-2013
	 */
	public void addCompoundOption() { 
		compoundItemsList.add(new LaboratoryMethodCompound__c(Name='')); 
	}
	
	
	/**	Delete a compound from the search criteria.
	 * @author	Dimitrios Sgourdos
	 * @version	18-Oct-2013
	 */
	public void deleteSearchCompound() { 
		compoundItemsList.remove(rowIndexDeletedCompound);
		if(compoundItemsList.isEmpty()) {
			addCompoundOption();
		}
	} 
	
	
	/**	Enable or not the drop-down list about Anti-coagulant depends on the value of Matrix.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Oct-2013
	 */
	public void enableAntiCoagulant() {
		antiCoagulantFlag = LaboratorySelectionListsService.enableAssociatedSelectOption(
								LaboratorySelectionListsService.readMatrixValuesEnablingAnticoagulant(), 
								searchCriterialabMethod.Matrix__c);
		if( ! antiCoagulantFlag ) {
			searchCriterialabMethod.AntiCoagulant__c = LaboratoryMethodService.getNotApplicableValue();
		}
	}
	
	
	/**	Enable or not the drop-down list about Detection Technique depends on the value of Analytical Technique.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Oct-2013
	 */
	public void enableDetectionTechnique() {
		detectionTechFlag = LaboratorySelectionListsService.enableAssociatedSelectOption(
								LaboratorySelectionListsService.readAnalyticalTechniqueValuesEnablingDetection(), 
								searchCriterialabMethod.AnalyticalTechnique__c);
		if( ! detectionTechFlag ) {
			searchCriterialabMethod.Detection__c = LaboratoryMethodService.getNotApplicableValue();
		}
	}
	
	
	/**	Clear the search criteria.
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	public void clearSearchOptions () {
		showCreationWarning	= false;
		showSaveWarning		= false;
		compoundItemsList = new List<LaboratoryMethodCompound__c>();
		addCompoundOption();
		searchCriterialabMethod = new LaboratoryMethod__c();
		enableAntiCoagulant();
		enableDetectionTechnique();
		resultsLabMethodList = new List<LaboratoryMethod__c>();
		showResults = false;
	}
	
	
	/**	Open the confirmation dialog for creating a new method.
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	public void requestNewMethod() {
		showCreationWarning	= true;
		showSaveWarning		= false;
	}
	
	
	/**	Create a new method from the search criteria and the inserted compounds.
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	public void createNewMethod() {
		showCreationWarning	= false;
		showSaveWarning		= false;
		String creationResult = LaboratoryMethodService.createNewLaboratoryMethodFromSearchCriteria(searchCriterialabMethod, compoundItemsList);
		// If an error had occured
		if(creationResult == LaboratoryMethodService.MANY_COMPOUNDS) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No more than ' + LaboratoryMethodService.MAX_COMPOUNDS + ' compounds are allowed. Please reduce the number of compounds and try to create the method again.');
			ApexPages.addMessage(msg);
			return;
		} else if(creationResult == LaboratoryMethodService.MISSING_FIELDS) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'To create a new Method, all fields must be completed. Please, complete all fields and try again.');
			ApexPages.addMessage(msg);
			return;
		} else if(creationResult == LaboratoryMethodService.SAVING_ERROR) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The creation was unsuccessful! Please, try again!');
			ApexPages.addMessage(msg);
			return;
		} else { // everything went well, so also save the compounds 
			Boolean successFlag = LaboratoryMethodCompoundService.createNewLaboratoryMethodCompoundsFromSearchCriteria(compoundItemsList, searchCriterialabMethod.Id);
			if(! successFlag ) {
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The creation of compounds was unsuccessful! Please, try again!');
				ApexPages.addMessage(msg);
				return;
			}
			searchLabMethods();
		} 
	}
	
	
	/**	Find the laboratory methods that meet the search criteria.
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	public void searchLabMethods() {
		showCreationWarning	= false;
		showSaveWarning		= false;
		resultsLabMethodList = LaboratoryMethodService.getLabMethodsBySearchCriteria(searchCriterialabMethod, compoundItemsList, '11');
		if(resultsLabMethodList.size() > 10) {
			resultsLabMethodList = new List<LaboratoryMethod__c>();
			showResults = false;
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, 'Too many result were obtained. Make the search more specific.');
			ApexPages.addMessage(msg);
			return;
		} else {
			showResults = true;
		}
	}
	
	
	/**	Close the 'Search results' section.
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	public void closeResultsSection() {
		showCreationWarning	= false;
		showSaveWarning		= false;
		resultsLabMethodList = new List<LaboratoryMethod__c>();
		showResults = false;
	}
	
	
	/**	Add a method to the data of 'Edit Methods and Compounds' section.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Oct-2013
	 */
	public void addMethodToEditAssignmentsData() {
		showCreationWarning	= false;
		showSaveWarning		= false;
		LaboratoryMethod__c tmpLabMethod = LaboratoryMethodService.getLabMethodById(addingMethodId);
		methodAndCompList = LaboratoryMethodCompoundService.addMethodToMethodCompoundStudyAssignmentContainerData(
								methodAndCompList, studiesList, tmpLabMethod);
	}
	
}