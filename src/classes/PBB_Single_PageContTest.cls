/* 
@author Kostyantyn Hladkyi
@date 2015  
@description test class for PBB_Single_PageCont
*/
@isTest
private class PBB_Single_PageContTest {
        
    private static final String NOTIFICATION_NAME = 'Test Notification Name';
    private static final String CLASS_NAME = 'SomeClass';
    //private static final String CATALOG_NAME = 'CATALOG_NAME';
    private static final String EMAIL_VALUE = [Select EMail From User Where Id = :UserInfo.getUserId()].Email;
    private static final String PHONE_VALUE = '0679040068';
    
    private static PBB_Single_PageCont cont;
    private static PBB_TestUtils testUtils;
    private static NotificationCatalog__c catalog;
    private static NotificationRegistration__c registration;
    private static NotificationEvent__c event;
    private static Notification__c notification;
    private static ecrf__c project;
    
    @isTest
    static void constructorTest(){
        cont = new PBB_Single_PageCont();
    }
    
    @isTest
    static void getProjectsDetailsTest(){
        PBB_Single_PageCont.getProjectsDetails();
    }
    
    @isTest
    static void getProjectDetailTest(){
        createProjectFlowSite();
        PBB_Single_PageCont.getProjectDetail(project.Id);
        
    }
    
    @isTest
    static void getStatusBySiteTest(){
        
        createTestData();
        Test.startTest();
            List<PRA_WrappersForMobile.WrapperSite> results = PBB_Single_PageCont.getStatusBySite(PBB_TestUtils.PROTOCOL_UNIQUE_KEY_VALUE);
        Test.stopTest();
        
        System.assertEquals( 0, results.size() );
    }
    
    @isTest
    static void getStatusByCountryTest(){
        createTestData();
        Test.startTest();
            List<PRA_WrappersForMobile.WrapperCountry> results = PBB_Single_PageCont.getStatusByCountry(PBB_TestUtils.PROTOCOL_UNIQUE_KEY_VALUE);
        Test.stopTest();
        
        System.assertEquals( 0, results.size() );
    }
    
    @isTest
    static void getWeekEventsByProjectTest(){
        PBB_Single_PageCont.getWeekEventsByProject('Some value');
    }
    
    @isTest
    static void getUserDataTest(){
        PRA_WrappersForMobile.UserData ud = PBB_Single_PageCont.getUserData();
        System.assert( ud != null );
    }
    
    @isTest
    static void getRegistrationsForAllProtocolsPositive(){
        createTestData();
        createNcmData();
        PRA_WrappersForMobile.OperationResult registrations = PBB_Single_PageCont.getRegistrationsForAllProtocols();
        NCM_API_DataTypes.NotificationRegistrationWrapper returnedValues = (NCM_API_DataTypes.NotificationRegistrationWrapper)registrations.returnedValues[0];
        System.assertEquals( catalog.Id, returnedValues.notificationCatalogId );
    }
    
    @isTest
    static void getRegistrationsForProtocolPositive(){
        createTestData();
        createNcmData();
        PRA_WrappersForMobile.OperationResult registrations = PBB_Single_PageCont.getRegistrationsForProtocol(testUtils.protocol.Id);

        NCM_API_DataTypes.NotificationRegistrationWrapper returnedValues = (NCM_API_DataTypes.NotificationRegistrationWrapper)registrations.returnedValues[0];
        System.assertEquals( catalog.Id, returnedValues.notificationCatalogId );
    }
    
    @isTest
    static void getRegistrationsForProtocolNegative(){
        createTestData();
        PRA_WrappersForMobile.OperationResult registrations = PBB_Single_PageCont.getRegistrationsForProtocol(testUtils.protocol.Id);
        System.assertEquals(0, registrations.returnedValues.size());
    }

    @isTest
    static void getNumberOfPendingNotificationsPerRelatedObjectByUser(){
        createTestData();
        PRA_WrappersForMobile.NotificationsPerProtocolResult numberOfPendingNotificationsPerRelatedObjectByUser = PBB_Single_PageCont.getNumberOfPendingNotificationsPerRelatedObjectByUser();
        System.assertEquals(0, numberOfPendingNotificationsPerRelatedObjectByUser.notificationsPerProjects.size());
    }

    @isTest
    static void getPendingNotificationsByUser(){
        createTestData();
        createNcmData();
        PRA_WrappersForMobile.OperationResult pendingNotificationsByUser = PBB_Single_PageCont.getPendingNotificationsByUser();
        NCM_API_DataTypes.NotificationWrapper returnedValues = (NCM_API_DataTypes.NotificationWrapper)pendingNotificationsByUser.returnedValues[0];
        System.assertEquals( event.Id, returnedValues.notificationEvent );
    }

    @isTest
    static void getPendingNotificationsByUserAndRelatedObject(){
        createTestData();
        createNcmData();
        PRA_WrappersForMobile.OperationResult pendingNotificationsByUserAndRelatedObject = PBB_Single_PageCont.getPendingNotificationsByUserAndRelatedObject(testUtils.protocol.Id);
        NCM_API_DataTypes.NotificationWrapper returnedValues = (NCM_API_DataTypes.NotificationWrapper)pendingNotificationsByUserAndRelatedObject.returnedValues[0];
        System.assertEquals( event.Id, returnedValues.notificationEvent );
    }

    @isTest
    static void acknowledgePendingNotifications(){
        createTestData();
        createNcmData();
        List<Id> notificationIds = new List<Id>();
        notificationIds.add(notification.Id);
        PRA_WrappersForMobile.NotificationsPerProtocolResult acknowledgePendingNotifications = PBB_Single_PageCont.acknowledgePendingNotifications(notificationIds);
        System.assertEquals(null, acknowledgePendingNotifications.returnedValues);
    }

    @isTest
    static void acknowledgePendingNotificationsNull(){
        createTestData();
        createNcmData();
        List<Id> notificationIds = new List<Id>();
        PRA_WrappersForMobile.NotificationsPerProtocolResult acknowledgePendingNotifications = PBB_Single_PageCont.acknowledgePendingNotifications(notificationIds);
        System.assert( acknowledgePendingNotifications.notificationsPerProjects.size() > 0 );
    }

    @isTest
    static void getPatientEnrollmentAttributes(){
        createTestData();
        createNcmData();
        List<Id> notificationIds = new List<Id>();
        PRA_WrappersForMobile.PatientEnrollmentWrapperGlobal patientEnrollmentAttributes = PBB_Single_PageCont.getPatientEnrollmentAttributes('');
        System.assertEquals( null, patientEnrollmentAttributes.EnrolledToDate );
    }
    
    @isTest
    static void registerForNotificationsPositive(){
        createTestData();
        createNcmData(false);
        registration.Active__c = false;
        insert registration;
        List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrations = NCM_API.buildAllRegistrationsForNotifications( UserInfo.getUserId(), testUtils.protocol.Id, NCM_API_DataTypes.PROTOCOL_NOTIFICATION);
        for ( NCM_API_DataTypes.NotificationRegistrationWrapper reg : registrations ){
            reg.active = true;
        }
        List<String> protocolIds = new List<String>();
        protocolIds.add(testUtils.protocol.Id);
        Test.startTest();
            System.assertEquals( null, PBB_Single_PageCont.registerForNotifications(registrations, protocolIds).registrationsPerProjects );
        Test.stopTest();
        NotificationRegistration__c registrationSelected = [Select Active__c From NotificationRegistration__c Where Id = :registration.Id];
        System.assert( registrationSelected.Active__c );
        //User userSelected = [Select Phone From User Where Id = :UserInfo.getUserId()];
        //System.assertEquals( PHONE_VALUE, userSelected.Phone);
    }
    
    @isTest
    static void unregisterForNotificationsPositive(){
        createTestData();
        createNcmData(true);
        List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrations = NCM_API.buildAllRegistrationsForNotifications( UserInfo.getUserId(), testUtils.protocol.Id, NCM_API_DataTypes.PROTOCOL_NOTIFICATION);
        for ( NCM_API_DataTypes.NotificationRegistrationWrapper reg : registrations ){
            reg.active = false;
        }
        List<String> protocolIds = new List<String>();
        protocolIds.add(testUtils.protocol.Id);
        Test.startTest();
            System.assertEquals( null, PBB_Single_PageCont.registerForNotifications(registrations, protocolIds).registrationsPerProjects );
        Test.stopTest();
        NotificationRegistration__c registrationSelected = [Select Active__c From NotificationRegistration__c Where Id = :registration.Id];
        System.assert( !registrationSelected.Active__c );
        //User userSelected = [Select Phone From User Where Id = :UserInfo.getUserId()];
        //System.assertEquals( PHONE_VALUE, userSelected.Phone);
    }

    @isTest
    static void sendEmail(){
        createTestData();
        createProjectFlowSite();
        Test.startTest();
            PRA_WrappersForMobile.OperationResult results = PBB_Single_PageCont.sendEmail(UserInfo.getUserId(), 'subject', 'body');
        Test.stopTest();
        System.assertEquals( null, results.returnedValues );
    }

    @isTest
    static void saveUserPhone(){
        createTestData();
        Test.startTest();
            PRA_WrappersForMobile.OperationResult results = PBB_Single_PageCont.saveUserPhone('111');
        Test.stopTest();
        System.assertEquals( null, results.returnedValues );
    }

    @isTest
    static void getNumberOfActiveRegistrationsPerRelatedObjectByUser(){
        createTestData();
        Test.startTest();
            PRA_WrappersForMobile.RegistrationsPerProtocolResult results = PBB_Single_PageCont.getNumberOfActiveRegistrationsPerRelatedObjectByUser();
        Test.stopTest();
        System.assertEquals( null, results.notificationsPerProjects );
    }

    @isTest
    static void getUserPreferrences(){
        createTestData();
        createProjectFlowSite();
        insert new User_Preferences__c(
                Related_Id__c = project.Id,
                Application_Name__c = 'APP_NAME',
                User__c = UserInfo.getUserId(),
                Value__c= 'TEST_VALUE'
        );
        Test.startTest();
            String results = PBB_Single_PageCont.getUserPreferrences(project.Id, 'APP_NAME');
        Test.stopTest();
        System.assertEquals( 'TEST_VALUE', results );
    }

    @isTest
    static void saveUserPreferrences(){
        createTestData();
        createProjectFlowSite();
        insert new User_Preferences__c(
                Related_Id__c = project.Id,
                Application_Name__c = 'APP_NAME',
                User__c = UserInfo.getUserId(),
                Value__c= 'TEST_VALUE'
        );
        Test.startTest();
            PRA_WrappersForMobile.OperationResult results = PBB_Single_PageCont.saveUserPreferrences(project.Id, 'APP_NAME', 'TEST_VALUE');
        Test.stopTest();
        System.assertEquals( null, results.returnedValues );
    }
    
    private static void createTestData(){
        testUtils = new PBB_TestUtils();
        testUtils.createWfmProject();        
        testUtils.createProtocol();
        /*createProjectFlowSite();
        testUtils.createSite();*/
    }

    private static void createProjectFlowSite() {
        WFM_Client__c wfmClient = new WFM_Client__c(Name='Test WFM Client', Client_Name__c='TestClinet_Name',Client_Unique_Key__c='clnty_test_753');
        insert wfmClient;
        WFM_Contract__c wfmContract = new WFM_Contract__c(Client_ID__c=wfmClient.ID, Contract_Status__c='AA', Contract_Unique_Key__c='12cntt_key_test');
        insert wfmContract;
        WFM_Project__c wfmProject = new WFM_Project__c(Name='Test WFM Project', Project_Unique_Key__c='wfm_project_678', Contract_ID__c=wfmContract.ID, Project_Status__c ='RM', Status_Desc__c='Active');
        insert wfmProject;
        project = new ecrf__c(Name='Test', Project_Id__c=wfmProject.Id);
        insert project;

        PAWS_Project_Flow_Country__c projectFlowCountry = new PAWS_Project_Flow_Country__c(Name = 'USA', PAWS_Project__c = project.Id);
        insert projectFlowCountry;

        PAWS_Project_Flow_Site__c projectFlowSite = new PAWS_Project_Flow_Site__c(Name = 'Project Flow Site', PAWS_Country__c = projectFlowCountry.Id);
        insert projectFlowSite;
    }
        
    private static void createNcmData(){
        createNcmData(true);
    }
    
    private static void createNcmData(Boolean toInsertRegistration){
        catalog = new NotificationCatalog__c(
                //Name = CATALOG_NAME, 
                NotificationName__c = NOTIFICATION_NAME,
                ImplementationClassName__c = CLASS_NAME,
                Category__c = NCM_API_DataTypes.PROTOCOL_NOTIFICATION,
                Active__c = true
        );
        insert catalog;
        event = new NotificationEvent__c(
                NotificationCatalog__c = catalog.Id,
                RelatedToId__c = testUtils.protocol.Id
        );
        insert event; 
        registration = new NotificationRegistration__c(
                NotificationCatalog__c = catalog.Id,
                RelatedToId__c = testUtils.protocol.Id,
                UserId__c = UserInfo.getUserId(),
                Active__c = true
        );
        if (toInsertRegistration){
            insert registration;
        }
        notification = new Notification__c(
                NotificationEvent__c = event.Id,
                Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
                NotificationRegistration__c = registration.Id, 
                Reminder__c = DateTime.now().addDays(1)
        );
        if (toInsertRegistration){
            insert notification;
        }
    }
    
}