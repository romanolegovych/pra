/**
 * @description	Implements the test for the NCM_Chatter_API_DataTypes class
 * @author		Dimitrios Sgourdos
 * @version		Created: 14-Sep-2015
 */
@isTest
private class NCM_Chatter_API_DataTypesTest {
	
	/**
	 * @description	Test the constructors of the ChatterPostWrapper.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015
	 */
	static testMethod void ChatterPostWrapperConstructorsTest() {
		// Create data
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		String errorMessage = 'Error in creating a ChatterPostWrapper instance';
		
		// Check the standard constructor
		NCM_Chatter_API_DataTypes.ChatterPostWrapper result = new NCM_Chatter_API_DataTypes.ChatterPostWrapper();
		
		system.assertEquals(NULL, result.objectToPostToId, errorMessage);
		system.assertEquals(NULL, result.postText, errorMessage);
		
		// Check the constructor with the Id and String
		result = new NCM_Chatter_API_DataTypes.ChatterPostWrapper(acc.Id, 'Test post');
		
		system.assertEquals(acc.Id, result.objectToPostToId, errorMessage);
		system.assertEquals('Test post', result.postText, errorMessage);
	}
	
	
	/**
	 * @description	Test the constructors of the SubscriptionToByUserWrapper.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015
	 */
	static testMethod void SubscriptionToByUserWrapperConstructorsTest() {
		// Create data
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		String errorMessage = 'Error in creating a SubscriptionToByUserWrapper instance';
		
		// Check the standard constructor
		NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper result = new NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper();
		
		system.assertEquals(NULL, result.feedId, errorMessage);
		system.assertEquals(NULL, result.subscriberId, errorMessage);
		
		// Check the constructor with the Id and Id
		result = new NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper(acc.Id, testUser.Id);
		
		system.assertEquals(acc.Id, result.feedId, errorMessage);
		system.assertEquals(testUser.Id, result.subscriberId, errorMessage);
	}
}