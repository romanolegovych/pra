public class IPA_BO_LinksManagement
{
    IPA_DAL_LinksManagement ipadal_l;
    IPA_DAL_ContentManagement ipadal_c;
    IPA_DAL_CategoryAndTopic ipadal_ct;
    IPA_DAL_DynamicPage ipadal_dp;
    
    //Constructor
    public IPA_BO_LinksManagement() {  
       ipadal_l = new IPA_DAL_LinksManagement();
       ipadal_c = new IPA_DAL_ContentManagement();
       ipadal_ct = new IPA_DAL_CategoryAndTopic(); 
       ipadal_dp = new IPA_DAL_DynamicPage();
    }
    
    //get special list of Quick Links using Page Widget Links,Topic or Category settings from a specific Page Widget object    
    public List<IPA_Links__c> returnQuickLinks(IPA_Page_Widget__c pageWidgetObj) {  
       
        Set<Id> linkIds = ipadal_ct.returnPageWidgetLinks(pageWidgetObj.Id);
        Set<Id> topicIds = ipadal_ct.returnPageWidgetTopics(pageWidgetObj.Id);
        Id catId = pageWidgetObj.Page__r.Category__c;
        
        if(!linkIds.isEmpty())
            return ipadal_l.returnQuickLinksByLink(linkIds);
        else if(!topicIds.isEmpty())
            return ipadal_l.returnQuickLinksByTopic(topicIds);
        else 
            return ipadal_l.returnQuickLinksByCategory(catId);               
    }
    
    //get default quick links using default category of Master Landing Page
    public List<IPA_Links__c> returnQuickLinks() {
        Id catId = ipadal_ct.returnDefaultCategory();
        return ipadal_l.returnQuickLinksByCategory(catId);
    }
    
    public List<IPA_Articles__c> returnPolicyUpdates() {
        return ipadal_c.returnPolicyUpdates();
    }
    
    //get special list of Events using Page Widget Contents, Topic or Category settings from a specific Page Widget object    
    public List<IPA_Articles__c> returnTop3Events(IPA_Page_Widget__c pageWidgetObj) { 
                
        Set<Id> ContentIds = ipadal_ct.returnPageWidgetContents(pageWidgetObj.Id);
        Set<Id> topicIds = ipadal_ct.returnPageWidgetTopics(pageWidgetObj.Id);
        Id catId = pageWidgetObj.Page__r.Category__c;
        
        if(!ContentIds.isEmpty())
            return ipadal_c.returnTop3EventsByContent(ContentIds);
        else if(!topicIds.isEmpty())
            return ipadal_c.returnTop3EventsByTopic(topicIds);
        else           
            return ipadal_c.returnTop3EventsByCategory(catId);        
    }
    
    //get default Quick Links using default category of Master Landing Page
    public List<IPA_Articles__c> returnTop3Events() {
        Id catId = ipadal_ct.returnDefaultCategory();
        return ipadal_c.returnTop3EventsByCategory(catId);
    }
 
    public List<IPA_Articles__c> returnTop3Holidays() {
        return ipadal_c.returnTop3Holidays();
    }
    
    public IPA_Articles__c returnFeaturedPRANews() {
        return ipadal_c.returnFeaturedPRANews();
    }
    
    public List<IPA_Articles__c> returnPRANews() {
        return ipadal_c.returnPRANews();
    }
    
    public IPA_Articles__c returnIntegrationUpdate() {
        return ipadal_c.returnIntegrationUpdate();
    }
    
    public List<IPA_Articles__c> returnIndustryWatch() {
        return ipadal_c.returnIndustryWatch();
    }
    
    public String returnLeftMenuLinks(String UserId) {
        return ipadal_l.returnLeftMenuLinks(UserId);
    }
    
    /*public IPA_Links__c returnExSiteDetails(String Id) {
        return ipadal_l.returnExSiteDetails(Id);
    }*/
    
    public List<IPA_Articles__c> returnEmpNewsForSlider() {
        return ipadal_c.returnEmpNewsForSlider();
    }
    
    public List<IPA_Links__c> returnLinks(String Type) {
        return ipadal_l.returnLinks(Type);
    }
    
    public IPA_Articles__c returnArticleDetails(String Id) {
        return ipadal_c.returnArticleDetails(Id);
    }
    
    public List<IPA_Custom_Links__c> returnCustomLinks(String Id) {
        return ipadal_l.returnCustomLinks(Id);
    }
    
    public void saveCustomLinks(List<IPA_Custom_Links__c> lstLinks, String UserId) {
        ipadal_l.saveCustomLinks(lstLinks, UserId);
    }
    
    public  List<IPA_Articles__c> returnEventsHolidays(Date sdate, Date ldate) {
        return ipadal_c.returnEventsHolidays(sdate,ldate);
    }
    
    //get special list of Hero Graphic using Page Widget Contents, Topic or Category settings from a specific Page Widget object    
    public IPA_Articles__c returnHeroGraphicContent(IPA_Page_Widget__c pageWidgetObj) {  
        
        Set<Id> ContentIds = ipadal_ct.returnAllPageWidgetContents(pageWidgetObj.Id);
        Set<Id> topicIds = ipadal_ct.returnPageWidgetTopics(pageWidgetObj.Id);  
        Id catId = pageWidgetObj.Page__r.Category__c;
             
        if(!ContentIds.isEmpty())        
            return ipadal_c.returnHeroGraphicContent(ContentIds);        
        else if(!topicIds.isEmpty())
            return ipadal_c.returnHeroGraphicContentbyTopic(topicIds); 
        else  
            return ipadal_c.returnHeroGraphicContentbyCategory(catId);   
    } 
    
    //get special list of Department Contacts using Page Widget Contents, Topic or Category settings from a specific Page Widget object    
    public List<IPA_Articles__c> returnDepartmentContacts(IPA_Page_Widget__c pageWidgetObj) {    
        
        Set<Id> ContentIds = ipadal_ct.returnAllPageWidgetContents(pageWidgetObj.Id);        
        Set<Id> topicIds = ipadal_ct.returnPageWidgetTopics(pageWidgetObj.Id);
        Id catId = pageWidgetObj.Page__r.Category__c;
        
        if(!ContentIds.isEmpty())
            return ipadal_c.returnContactsByContent(ContentIds);        
        else if(!topicIds.isEmpty())
            return ipadal_c.returnDepContactsByTopic(topicIds);        
        else       
            return ipadal_c.returnDepContactsByCategory(catId);
    }
    
    //get special list of Department Contents using Page Widget Contents, Topic or Category settings from a specific Page Widget object    
    public List<IPA_Articles__c> returnDepartmentContents(IPA_Page_Widget__c pageWidgetObj) 
    {           
        Set<Id> Ids = ipadal_ct.returnAllPageWidgetContents(pageWidgetObj.Id);
        Set<Id> topicIds = ipadal_ct.returnPageWidgetTopics(pageWidgetObj.Id);
        Id catId = pageWidgetObj.Page__r.Category__c;
        
        if(!Ids.isEmpty())
            return ipadal_c.returnDepContentsByPageWidget(Ids);
        else if(!topicIds.isEmpty())
            return ipadal_c.returnDepContentsByTopic(topicIds);
        else
            return ipadal_c.returnDepContentsByCategory(catId);
    }
    
    //get special list of Department News using Page Widget Contents, Topic or Category settings from a specific Page Widget object    
    public List<IPA_Articles__c> returnDepartmentNews(IPA_Page_Widget__c pageWidgetObj) 
    {               
        Set<Id> Ids = ipadal_ct.returnAllPageWidgetContents(pageWidgetObj.Id);
        Set<Id> topicIds = ipadal_ct.returnPageWidgetTopics(pageWidgetObj.Id);
        Id catId = pageWidgetObj.Page__r.Category__c;
        
        if(!Ids.isEmpty())
            return ipadal_c.returnDepNewsByPageWidget(Ids);
        else if(!topicIds.isEmpty())
            return ipadal_c.returnDepNewsByTopic(topicIds);
        else
            return ipadal_c.returnDepNewsByCategory(catId);        
    }
    
    //get special list of Resource Cabinet using Page Widget Links, Topic or Category settings from a specific Page Widget object
    public List<IPA_Links__c> returnMediaLibraries(IPA_Page_Widget__c pageWidgetObj) 
    {        
        Set<Id> linkIds = ipadal_ct.returnPageWidgetLinks(pageWidgetObj.Id);
        Set<Id> topicIds = ipadal_ct.returnPageWidgetTopics(pageWidgetObj.Id);
        Id catId = pageWidgetObj.Page__r.Category__c;
        
        if(!linkIds.isEmpty())
            return ipadal_l.returnResourceCabinetsByLink(linkIds);
        else if(!topicIds.isEmpty())
            return ipadal_l.returnResourceCabinetsByTopic(topicIds);
        else
            return ipadal_l.returnResourceCabinetsByCategory(catId);
    }
    
    //This function returns list of child tab widgets for given parent tab group widget
    public List<IPA_Page_Widget__c> returnChildWidgets(IPA_Page_Widget__c parent)
    {
        return ipadal_dp.returnChildWidgets(parent);
    }
}