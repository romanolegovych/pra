/**
 * Class for mocking FilterService responses
 */
@isTest
public class DWF_FilterServiceTestMocks implements WebServiceMock {

	public String operation {get;set;}
	
	public static final String GET_ALL_COMPANY_VO_RESPONSE = 'getAllCompanyVOsResponse';
	public static final String GET_EMPLOYEE_VO_RESPONSE = 'getEmployeeVOResponse';
	public static final String GET_EMPLOYEE_VO_COMPANY_NO_RESPONSE = 'getEmployeeVOCompanyNoResponse';
	public static final String GET_EMPLOYEE_VO_COMPANY_NO_BU_NO_RESPONSE = 'getEmployeeVOCompanyNoBUNoResponse';
	public static final String GET_EMPLOYEE_VO_COMPANY_NO_BU_NAME_RESPONSE = 'getEmployeeVOCompanyNoBUNameResponse';
	
	public DWF_FilterServiceTestMocks(String value) {
		this.operation = value;
	}
	
	public void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
		String requestName, String responseNS, String responseName, String responseType) {
		if (operation.equals(GET_ALL_COMPANY_VO_RESPONSE)) {
			mockGetAllCompanyVOsResponse(response);
		} else if (operation.equals(GET_EMPLOYEE_VO_RESPONSE)) {
			mockGetEmployeeVOFromCompanyEmpResponse(response);
		} else if (operation.equals(GET_EMPLOYEE_VO_COMPANY_NO_RESPONSE)) {
			mockGetEmployeeVOFromCompanyNoResponse(response);
		} else if (operation.equals(GET_EMPLOYEE_VO_COMPANY_NO_BU_NO_RESPONSE)) {
			mockGetEmployeeVOFromCompanyNoBUNoResponse(response);
		} else if (operation.equals(GET_EMPLOYEE_VO_COMPANY_NO_BU_NAME_RESPONSE)) {
			mockGetEmployeeVOFromCompanyNoBUNameResponse(response);
		}
	}
	
	/**
	 * Build DWF_FilterService.getDistinctCompanyDataResponse
	 */
	private static void mockGetAllCompanyVOsResponse(map<String, Object> response) {
		DWF_FilterService.getDistinctCompanyDataResponse resp = new DWF_FilterService.getDistinctCompanyDataResponse();
		DWF_FilterService.companyListResponse companyListResponse = new DWF_FilterService.companyListResponse();
		companyListResponse.errors = null;
		companyListResponse.statusCode = '0';
		companyListResponse.companyVOs = buildMockCompanyVOs();
		resp.GetDistinctCompanyDataResponse = companyListResponse;
		response.put('response_x', resp);
	}
	
	/**
	 * Build DWF_FilterService.getEmployeeVOByCompanyEmpDataResponse
	 */
	private static void mockGetEmployeeVOFromCompanyEmpResponse(map<String, Object> response) {
		DWF_FilterService.getEmployeeVOByCompanyEmpDataResponse resp = new DWF_FilterService.getEmployeeVOByCompanyEmpDataResponse();
		DWF_FilterService.employeeListResponse employeeListResponse = new DWF_FilterService.employeeListResponse();
		employeeListResponse.errors = null;
		employeeListResponse.statusCode = '0';
		employeeListResponse.employeeVOs = buildMockEmployeeVOs();
		resp.GetEmployeeVOByCompanyEmpDataResponse = employeeListResponse;
		response.put('response_x', resp);
	}
	
	/**
	 * Build DWF_FilterService.getEmployeeVOByCompanyNoResponse
	 */
	private static void mockGetEmployeeVOFromCompanyNoResponse(map<String, Object> response) {
		DWF_FilterService.getEmployeeVOByCompanyNoResponse resp = new DWF_FilterService.getEmployeeVOByCompanyNoResponse();
		DWF_FilterService.employeeListResponse employeeListResponse = new DWF_FilterService.employeeListResponse();
		employeeListResponse.errors = null;
		employeeListResponse.statusCode = '0';
		employeeListResponse.employeeVOs = buildMockEmployeeVOs();
		resp.GetEmployeeVOByCompanyNoResponse = employeeListResponse;
		response.put('response_x', resp);
	}
	
	/**
	 * Build DWF_FilterService.getEmployeeVOByCompanyNoBUNoResponse
	 */
	private static void mockGetEmployeeVOFromCompanyNoBUNoResponse(map<String, Object> response) {
		DWF_FilterService.getEmployeeVOByCompanyNoBUNoResponse resp = new DWF_FilterService.getEmployeeVOByCompanyNoBUNoResponse();
		DWF_FilterService.employeeListResponse employeeListResponse = new DWF_FilterService.employeeListResponse();
		employeeListResponse.errors = null;
		employeeListResponse.statusCode = '0';
		employeeListResponse.employeeVOs = buildMockEmployeeVOs();
		resp.GetEmployeeVOByCompanyNoBUNoResponse = employeeListResponse;
		response.put('response_x', resp);
	}
	
	/**
	 * Build DWF_FilterService.getEmployeeVOByCompanyNoBUNameResponse
	 */
	private static void mockGetEmployeeVOFromCompanyNoBUNameResponse(map<String, Object> response) {
		DWF_FilterService.getEmployeeVOByCompanyNoBUNameResponse resp = new DWF_FilterService.getEmployeeVOByCompanyNoBUNameResponse();
		DWF_FilterService.employeeListResponse employeeListResponse = new DWF_FilterService.employeeListResponse();
		employeeListResponse.errors = null;
		employeeListResponse.statusCode = '0';
		employeeListResponse.employeeVOs = buildMockEmployeeVOs();
		resp.GetEmployeeVOByCompanyNoBUNameResponse = employeeListResponse;
		response.put('response_x', resp);
	}
	
	/** Utility Methods to build reponse objects **/
	private static list<DWF_FilterService.companyVO> buildMockCompanyVOs() {
		list<DWF_FilterService.companyVO> companyVOs = new list<DWF_FilterService.companyVO>();
		for (Integer i = 1; i <= 5; i++) {
			DWF_FilterService.companyVO companyVO = new DWF_FilterService.companyVO();
			companyVO.companyNo = i;
			companyVO.companyName = 'MOCK COMPANY ' + i;
			companyVO.companyStatus = 'L';
			companyVO.businessUnits = buildMockBusinessUnitVOs();
			companyVOs.add(companyVO);
		}
		return companyVOs;
	}
	
	private static list<DWF_FilterService.businessUnitVO> buildMockBusinessUnitVOs() {
		list<DWF_FilterService.businessUnitVO> businessUnitVOs = new list<DWF_FilterService.businessUnitVO>();
		for (Integer i = 1; i <= 5; i++) {
			DWF_FilterService.businessUnitVO businessUnitVO = new DWF_FilterService.businessUnitVO();
			businessUnitVO.businessUnitNo = i;
			businessUnitVO.businessUnitName = 'MOCK BU NAME ' + i;
			businessUnitVO.businessUnitStatus = 'AA';
			businessUnitVOs.add(businessUnitVO);
		}
		return businessUnitVOs;
	}
	
	private static list<DWF_FilterService.employeeVO> buildMockEmployeeVOs() {
		list<DWF_FilterService.employeeVO> employeeVOs = new list<DWF_FilterService.employeeVO>();
		for (Integer i = 1; i <= 5; i++) {
			DWF_FilterService.employeeVO employeeVO = new DWF_FilterService.employeeVO();
			employeeVO.employeeId = i;
			employeeVO.employeeStatus = 'AA';
			employeeVO.employeeStatusDesc = 'Active Full-Time';
			employeeVO.firstName = 'first name ' + i;
			employeeVO.lastName = 'last name ' + i;
			employeeVOs.add(employeeVO);
		}
		return employeeVOs;
	}
}