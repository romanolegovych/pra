public with sharing class PAWS_CrossProjectPlanExportController 
{
	@RemoteAction
    public static Map<String, Object> buildQuery(String sponsorFilter, String projectFilter, String flowFilter, String stepFilter, String roleFilter, String assigneeFilter, String tagsFilter, String protocolsFilter, String countriesFilter, Boolean excludeCompletedStepsFilter, Boolean excludeParentStepsFilter, String sdFilterForSD, String edFilterForSD, String sdFilterForED, String edFilterForED, String orderBy, String orderType) 
    {
    	Map<String, Object> result = new Map<String, Object>();

        try
        {
    		PAWS_CrossProjectPlanningController controller = new PAWS_CrossProjectPlanningController();
		    controller.SponsorFilter = (String.isEmpty(sponsorFilter) ? null : sponsorFilter);
		    controller.ProjectFilter = (String.isEmpty(projectFilter) ? null : projectFilter);
		    controller.FlowFilter = (String.isEmpty(flowFilter) ? null : flowFilter);
		    controller.StepFilter = (String.isEmpty(stepFilter) ? null : stepFilter);
		    controller.RoleFilter = (String.isEmpty(roleFilter) ? null : roleFilter);
		    controller.AssigneeFilter = (String.isEmpty(assigneeFilter) ? null : assigneeFilter);
		    controller.ExcludeCompletedStepsFilter = excludeCompletedStepsFilter;
		    controller.ExcludeParentStepsFilter = excludeParentStepsFilter;
		    controller.StartDateFilterForStartDate = (String.isEmpty(sdFilterForSD) ? null : Date.parse(sdFilterForSD));
		    controller.EndDateFilterForStartDate = (String.isEmpty(edFilterForSD) ? null : Date.parse(edFilterForSD));
		    controller.StartDateFilterForEndDate = (String.isEmpty(sdFilterForED) ? null : Date.parse(sdFilterForED));
		    controller.EndDateFilterForEndDate = (String.isEmpty(edFilterForED) ? null : Date.parse(edFilterForED));
    		controller.selectedTags = tagsFilter;
    		controller.selectedProtocols = protocolsFilter;
    		controller.selectedCountries = countriesFilter;
    		controller.OrderBy = (String.isEmpty(orderBy) ? null : orderBy);
    		controller.OrderType = (String.isEmpty(orderType) ? null : orderType);
        	
        	String query = controller.buildQuery();
        	String queryShort = query.substring(0, 7) + ' Id ' + query.substring(query.indexOf(' from '));
        	result.put('query', queryShort);
        	
        	String queryForTotal = query.substring(0, 7) + ' count() ' + query.substring(query.indexOf(' from '), query.indexOf(' where ')) + ' where ' + controller.buildFilters();
			System.debug('queryForTotal: ' + queryForTotal);
			
        	Integer totalRecords = Database.countQuery(queryForTotal);
        	result.put('total', totalRecords);
	        	
        	result.put('filters', new Map<String, String>{
    				'Sponsor' => !String.isEmpty(controller.SponsorFilter) ? controller.SponsorFilter : '',
    				'Project' => !String.isEmpty(controller.ProjectFilter) ? controller.ProjectFilter : '',
    				'Flow' => !String.isEmpty(controller.FlowFilter) ? controller.FlowFilter : '',
    				'Step' => !String.isEmpty(controller.StepFilter) ? controller.StepFilter : '',
    				'Role' => !String.isEmpty(controller.RoleFilter) ? controller.RoleFilter : '',
    				'Assignee' => !String.isEmpty(controller.AssigneeFilter) ? controller.AssigneeFilter : '',
    				'Views' => !String.isEmpty(controller.selectedTags) ? controller.selectedTags : '',
    				'Protocol' => !String.isEmpty(controller.selectedProtocols) ? controller.selectedProtocols : '',
    				'Country' => !String.isEmpty(controller.selectedCountries) ? controller.selectedCountries : '',
    				'Start Date From' => PAWS_Utilities.formatDate(controller.StartDateFilterForStartDate, 'dd-MMM-yyyy'),
    				'Start Date To' => PAWS_Utilities.formatDate(controller.EndDateFilterForStartDate, 'dd-MMM-yyyy'),
    				'End Date From' => PAWS_Utilities.formatDate(controller.StartDateFilterForEndDate, 'dd-MMM-yyyy'),
    				'End Date To' => PAWS_Utilities.formatDate(controller.EndDateFilterForEndDate, 'dd-MMM-yyyy'),
    				'Exclude Completed Steps' => (controller.ExcludeCompletedStepsFilter == true ? 'Yes' : null),
    				'Exclude Parent Steps' => (controller.ExcludeParentStepsFilter == true ? 'Yes' : null)
    			});
    	
    		if(!String.isEmpty(orderBy))
    		{
    			Map<String, String> orderMap = new Map<String, String>
    			{
    				'Project__r.Name' => 'ProjectName',
        			'Project__r.Sponsor__c' =>'Sponsor',
        			'STSWR1__Step__r.STSWR1__Flow__r.Name' =>'FlowName',
        			'STSWR1__Step__r.Name' =>'StepName',
        			'Start_Date__c' =>'PlannedStartDate' ,
        			'STSWR1__Planned_End_Date__c' =>'PlannedEndDate',
        			'STSWR1__Revised_End_Date__c' =>'RevisedEndDate',
        			'STSWR1__Step__r.STSWR1__Flow_Swimlane__r.Name' =>'Role'
    			};
    			
	    		result.put('order', new Map<String, String>{
	    				'field' => orderMap.get(orderBy),
	    				'fieldType' => String.valueOf(PAWS_Utilities.extractFieldDescribe(STSWR1__Gantt_Step_Property__c.getSObjectType().getDescribe(), orderBy).getType()),
	    				'type' => orderType
	    			});
    		}
        }catch(Exception ex)
        {
        	if(Test.isRunningTest()) throw ex;
           	result.put('error', ex.getMessage());
        }
        
        return result;
    }
    
    @RemoteAction
    public static Map<String, Object> loadData(List<String> ids) 
    {
    	Map<String, Object> result = new Map<String, Object>();

        try
        {
        	PAWS_CrossProjectPlanningController controller = new PAWS_CrossProjectPlanningController();
        	
        	String query = controller.buildQuery();
        	query = query.substring(0, query.indexOf(' where ')) + ' where ' + controller.buildInFilter('id', ids);
        	
        	List<Map<String, Object>> objects = new List<Map<String, Object>>();
    		for(PAWS_CrossProjectPlanningController.Row rowItem : controller.buildRows(Database.query(query)))
        	{
        		objects.add(new Map<String, Object>{
        			'Id' => rowItem.Property.Id,
        			'ProjectName' => rowItem.Project.Name,
        			'Sponsor' => rowItem.Project.Sponsor__c,
        			'ObjectId' => rowItem.Project.Id,
        			'ObjectName' => rowItem.Project.Name,
        			'FlowName' => rowItem.Property.STSWR1__Step__r.STSWR1__Flow__r.Name,
        			'StepName' => rowItem.Property.STSWR1__Step__r.Name,
        			'Duration' => rowItem.Property.STSWR1__Step__r.STSWR1__Duration__c,
        			'StartDate' => validateDate(rowItem.Property.Start_Date__c),
        			'PlannedStartDate' => validateDate(rowItem.Property.STSWR1__Planned_Start_Date__c),
        			'PlannedEndDate' => validateDate(rowItem.Property.STSWR1__Planned_End_Date__c),
        			'RevisedStartDate' => validateDate(rowItem.Property.STSWR1__Revised_Start_Date__c),
        			'RevisedEndDate' => validateDate(rowItem.Property.STSWR1__Revised_End_Date__c),
        			'ActualStartDate' => (rowItem.History.STSWR1__Step__r.STSWR1__Is_System_Value__c == true ? rowItem.History.STSWR1__Actual_Start_Date_Value__c : rowItem.History.STSWR1__Actual_Start_Date__c),
        			'ActualEndDate' => (rowItem.History.STSWR1__Step__r.STSWR1__Is_System_Value__c == true ? rowItem.History.STSWR1__Actual_Complete_Date_Value__c : rowItem.History.STSWR1__Actual_Complete_Date__c),
        			'Role' => rowItem.Property.STSWR1__Step__r.STSWR1__Flow_Swimlane__r.Name,
        			'AssignedTo' => rowItem.AssignedTo
        		});
        	}
        	
        	result.put('rows', JSON.serialize(objects));
        }catch(Exception ex)
        {
        	if(Test.isRunningTest()) throw ex;
           	result.put('error', ex.getMessage());
        }
        
        return result;
    }
    
    public static Date validateDate(DateTime dt)
    {
        return (dt != null ? dt.dateGMT() : null);
    }
}