public without sharing class PAWS_WorkflowTaskDetailsController 
{
	public String CursorId {get;set;}
	public String StepId {get;set;}
	public String RequireStartDate {get;set;}
   	public String RequireCompleteDate {get;set;}
   	public String RequireComments {get;set;}
   	
	public void init()
    {
    	CursorId = extractCursorId();
    	StepId = extractStepId();
    	
    	if(!String.isEmpty(CursorId))
    	{
    		List<STSWR1__Flow_Instance_Cursor__c> cursors = [select STSWR1__Flow_Instance__r.STSWR1__Object_Type__c from STSWR1__Flow_Instance_Cursor__c where Id = :CursorId];
    		if(cursors.size() > 0 && cursors[0].STSWR1__Flow_Instance__r.STSWR1__Object_Type__c == 'ecrf__c')
    		{
    			RequireStartDate = 'true';
    			RequireCompleteDate = 'true';
    			RequireComments = '{$environment.ConnectionType} = \'Reject\'';
    		}
    	}
    }
 
 	private String extractCursorId()
 	{
 		if(ApexPages.currentPage().getParameters().containsKey('cursor'))
		{
			return ApexPages.currentPage().getParameters().get('cursor');
		}
		
		String data = ApexPages.currentPage().getParameters().get('signed_request');
		if(data == null) return null;

		String[] items = data.split('\\.');
		if(items.size() == 0) return null;
		
		String context = EncodingUtil.base64Decode(items.size() > 1 ? items[1] : items[0]).toString();
		Map<String, Object> contextMap = (Map<String, Object>)JSON.deserializeUntyped(context);
		if(contextMap == null) return null;
		
		contextMap = (Map<String, Object>)contextMap.get('context');
		if(contextMap == null) return null;
		
		contextMap = (Map<String, Object>)contextMap.get('environment');
		if(contextMap == null) return null;
		
		contextMap = (Map<String, Object>)contextMap.get('parameters');
		if(contextMap == null) return null;
				
		return (String)contextMap.get('cursor');
 	}   
 	
 	private String extractStepId()
 	{
 		if(ApexPages.currentPage().getParameters().containsKey('step'))
		{
			StepId = ApexPages.currentPage().getParameters().get('step');
			return StepId;
		}
    			
		String data = ApexPages.currentPage().getParameters().get('signed_request');
		if(data == null) return null;
	    		
		String[] items = data.split('\\.');
		if(items.size() == 0) return null;
		
		String context = EncodingUtil.base64Decode(items.size() > 1 ? items[1] : items[0]).toString();
		Map<String, Object> contextMap = (Map<String, Object>)JSON.deserializeUntyped(context);
		if(contextMap == null) return null;
		
		contextMap = (Map<String, Object>)contextMap.get('context');
		if(contextMap == null) return null;
		
		contextMap = (Map<String, Object>)contextMap.get('environment');
		if(contextMap == null) return null;
		
		contextMap = (Map<String, Object>)contextMap.get('parameters');
		if(contextMap == null) return null;
		
		return (String)contextMap.get('step');
 	}   
}