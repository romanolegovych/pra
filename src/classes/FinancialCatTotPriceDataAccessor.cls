/** Implements the Selector Layer of the object FinancialCatTotPrice__c
 * @author	Dimitrios Sgourdos
 * @version	17-Dec-2013
 */
public with sharing class FinancialCatTotPriceDataAccessor {
	
	/** Object definition for fields used in application for FinancialCatTotPrice
	 * @author	Dimitrios Sgourdos
	 * @version 17-Dec-2013
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('FinancialCatTotPrice__c');
	}
	
	
	/** Object definition for fields used in application for FinancialCatTotPrice with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 17-Dec-2013
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result =	referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'ConversionRate__c,';
		result += referenceName + 'FinalPrice__c,';
		result += referenceName + 'FinancialCategoryTotal__c,';
		result += referenceName + 'OriginalCurrency__c,';
		result += referenceName + 'PriceDelta__c,';
		result += referenceName + 'TargetCurrency__c,';
		result += referenceName + 'TotalPrice__c';
		return result;
	}
	
	
	/** Retrieve the list of Financial Category Total Prices for the given financial document, and depends on
	 *	the firstLevelSrvCatFlag for first level Service Categories or for all.
	 * @author	Dimitrios Sgourdos
	 * @version 17-Dec-2013
	 * @param	financialDocumentId		The id of the financial document
	 * @param	firstLevelSrvCatFlag	If the query will be only for first level Service Categories or for all levels
	 * @param	orderByField			The field that the Financial Category Total Prices will be ordered by
	 * @return	The list of Financial Category Total Prices.
	 */
	public static List<FinancialCatTotPrice__c> getFinancialCatTotalPricesByDocumentId(String financialDocumentId,
																					Boolean firstLevelSrvCatFlag,
																					String orderByField) {
		// Retrieve first level Service categories or not
		String addCondition = '';
		if(firstLevelSrvCatFlag) {
			addCondition += 'AND FinancialCategoryTotal__r.ServiceCategory__r.ParentCategory__c = NULL ';
		}
		
		// Query
		String query = String.format(
								'SELECT {0}, {1} ' +
								'FROM FinancialCatTotPrice__c ' +
								'WHERE FinancialCategoryTotal__r.FinancialDocument__c = {2} ' +
								'{3} ORDER BY {4}',
								new List<String> {
									getSObjectFieldString(),
									FinancialCategoryTotalDataAccessor.getSObjectFieldString('FinancialCategoryTotal__r'),
									'\'' + financialDocumentId + '\'',
									addCondition,
									orderByField
								}
							);
		
		return (List<FinancialCatTotPrice__c>) Database.query(query);
	}
}