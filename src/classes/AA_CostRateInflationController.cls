public class AA_CostRateInflationController {
    Public String Id {get;set;}
    Public String scheduledYear{get;set;}
    Public List<CostRateInflationWrapper> result {get;set;}
    Public String InflationRate{get;set;}
    Public Boolean inflation_applied =False;
    
    public AA_CostRateInflationController () {
        Id = System.currentPagereference().getParameters().get('functionId');
        scheduledYear = System.currentPagereference().getParameters().get('scheduledYear');
        getresult();
    }
    
    Public List<CostRateInflationWrapper> getresult(){
        result =  new List<CostRateInflationWrapper>();
        if(!inflation_applied){
          List<Cost_Rate__c> costrates = AA_CostRateService.getCostRates(Id,scheduledYear);
          system.debug('------costrates ------'+costrates );
          for(Cost_Rate__c r : costrates){
              system.debug('------r------'+r);
              CostRateInflationWrapper cost = new CostRateInflationWrapper(r,false);
              result.add(cost);
          }
              
        }
        //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'BUF Code : '+ Id +' ' + String.valueOf(result.size())+ ' already exists'));
        return result;
    }
    
    Public Pagereference apply(){
    inflation_applied = true;
        List<CostRateInflationWrapper> tempresult = result; 
        //Integer size = tempresult.size();
        for(Integer i = 1 ; i < tempresult.size(); i ++){        
            Decimal unScaled = (Decimal) tempresult[i-1].costrate.Cost_Rate__c + ( tempresult[i-1].costrate.Cost_Rate__c * Decimal.valueOf(InflationRate));
            tempresult[i].costrate.Cost_Rate__c = unScaled.setScale(5);
        }
        result = tempresult;
        return null;
    }
    
    Public Pagereference save(){
        List<Cost_Rate__c> updatelist = new List<Cost_Rate__c>();
        for(CostRateInflationWrapper c : result){
            updatelist.add(c.costrate);
        }
        update updatelist;
        return null;
    }
        
    Public Pagereference deleterecords(){
        List<Cost_Rate__c> deletelist= new List<Cost_Rate__c>();
        for(CostRateInflationWrapper c : result){
                c.costrate.IsActive__c  = false;
                deletelist.add(c.costrate);
        }
        update deletelist;
        getresult();
        return null;
    }
     public PageReference back() {
         PageReference pageRef = Page.AA_Cost_Rate;
         return pageRef;
    }
    
    public class CostRateInflationWrapper {
        public Cost_Rate__c costrate {get;set;}
        public Boolean selected {get;set;}
        public CostRateInflationWrapper (Cost_Rate__c c, Boolean sel) {
            this.costrate = c;
            this.selected = sel;
        }
    }    
}