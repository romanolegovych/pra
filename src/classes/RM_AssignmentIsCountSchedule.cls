global class RM_AssignmentIsCountSchedule implements Schedulable {
  global void execute(SchedulableContext cxt) {
    String className = 'RM_AssignmentIsCountBatch';
    
    String query = '18';
    String jsonParams = Json.serialize(new list<String>{query});
    
    PRA_Batch_Queue__c mappingBatch = new PRA_Batch_Queue__c(Batch_Class_Name__c = className, Parameters_JSON__c = jsonParams,Status__c = 'Waiting', Priority__c = 5);
        
        insert mappingBatch;
  }
}