@isTest   
private class AA_NewBillRateExtensionTest{
    static testMethod void ConstructorTest() {
        AA_TestUtils utils1 = new AA_TestUtils();
        Bill_Rate__c brr1 = utils1.createBillRate();
        ApexPages.StandardController p = new ApexPages.StandardController(brr1);
        AA_NewBillRateExtension pbbs = new AA_NewBillRateExtension(p);
        brr1.Schedule_Year__c = '1985';
        PageReference pf = pbbs.Create();
        brr1.Bill_Rate__c = 10;
        PageReference pb = pbbs.Create();
        brr1.Bill_Rate__c = 0;
        PageReference pg = pbbs.Create();
        boolean error= false;
        brr1.Schedule_Year__c = '2004';
        PageReference pd = pbbs.Create();
        brr1.Bill_Rate__c = -1;
        PageReference pa = pbbs.Create();
        pageReference pz= pbbs.Create();
        pbbs.createNewRecords();
        pageReference pc = pbbs.cancel();
        AA_NewBillRateExtension w = new AA_NewBillRateExtension();    
   }
   
   static testMethod void testAA_NewBillRateExtension(){
        AA_TestUtils utils1 = new AA_TestUtils();
        Bill_Rate__c brr1 = utils1.createBillRate();
        ApexPages.StandardController p = new ApexPages.StandardController(brr1);
        AA_NewBillRateExtension pbbs = new AA_NewBillRateExtension(p);
        brr1.Schedule_Year__c = '2003';
        brr1.Bill_Rate__c = 10;
        PageReference pb = pbbs.Create();
        pbbs.jobPosition = 'sample';
        pbbs.selectedCountry = 'USA';
        pbbs.selectedCurrency = 'USD';
        pbbs.initialRateValue = 15;
        pbbs.searchTerm = 'search';
        
        
        ApexPages.StandardController p1 = new ApexPages.StandardController(brr1);
        AA_NewBillRateExtension pbbs1 = new AA_NewBillRateExtension(p);
        pbbs1.rate.Schedule_Year__c = '200a';
        pbbs1.rate.Bill_Rate__c = 10;
        PageReference pb1 = pbbs1.Create();
        
   }

}