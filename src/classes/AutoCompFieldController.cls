global class AutoCompFieldController {
    @RemoteAction
    global static SObject[] findSObjects(string obj, string searchfield, string qry, string addFields, string addFilter, boolean norecord, boolean isFieldSearch) {
        // more than one field can be passed in the addFields parameter
        // split it into an array for later use
        system.debug('---------norecord---------' + norecord); 
        List<String> fieldList;
        if (addFields != null && addFields != '') fieldList = addFields.split(',');
       // check to see if the object passed is valid
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sot = gd.get(obj);
        if (sot == null) { // Object name not valid
            return null;
        }
        // create the filter text
        String filter = searchField + ' like \'' + String.escapeSingleQuotes(qry) + '%\'';
        //begin building the dynamic soql query
        String ssitem ='select id,' + searchfield;
        
        // if an additional field was passed in add it to the soql and sort
        string sadditional = '';
        if (addFields != null && addFields != '') {           
            for (String s : fieldList) {
                system.debug('---------s---------' + s); 
                sadditional += ', ' + s;
            }
        }
        // add the object and filter by name to the soql
        string soql = ssitem + sadditional + ' from ' + obj + ' where ' + filter + addFilter + ' order by ' + searchfield + sadditional +' limit 20';
        // add the filter by additional fields to the soql
      
        system.debug('---------soql---------' + soql);
    	Set<String> objL = new Set<String>();
        List<sObject> L = new List<sObject>();
        try {
            L = Database.query(soql);
           
            if (L.size() == 0 ){
                 if (norecord){
                    L = Database.query(ssitem + sadditional + ' from ' + obj + ' limit 1');
                    // replace name to no record
                    if (L.size() > 0){
                        L[0].put(searchfield, 'No Record');
                        if (addFields != null && addFields != '')
                            L[0].put(addFields, 'No Record');
                    }
                 }
            }  
            if (L.size() > 0  && isFieldSearch) {
            	List<sObject> fieldSearchList = new List<sObject>();
            	for (sObject s : L) {
            		system.debug('----------------------' + String.valueOf(s.get(searchField)));
            		if (!objL.contains(String.valueOf(s.get(searchField)))) {
            			objL.add(String.valueOf(s.get(searchField)));
            			fieldSearchList.add(s);
            		}
            	}
            	if (fieldSearchList != null) {
            		L = fieldSearchList;
            	}
            }        
        }
        catch (QueryException e) {
            return null;
        }
        return L;
   }
}