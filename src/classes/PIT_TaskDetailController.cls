public with sharing class PIT_TaskDetailController {
	Public Task 			ta				{get;set;}
	public List<Attachment> attachResults 	{get;set;} 
	public boolean			attachExist 	{get;set;} 
	public String			AttachId		{get;set;} 
	public String			taskId 			{get;set;} 
	public boolean  		showUploadPage	{get;set;}
	public Attachment 		newAttach 		{get;set;}
	
	public PIT_TaskDetailController(ApexPages.standardController stdCtr) {
		taskId = system.currentPageReference().getParameters().get('id');
		attachResults  = new List<Attachment>(); 
		newAttach 		= new Attachment(); 
		attachExist	   = findAttachments(); 
		showUploadPage = false;
		try {
			ta = [SELECT id, 
						 ownerid, 
						 Subject, 
						 ActivityDate, 
						 WhatId, 
						 Description, 
						 Status, 
						 Priority, 
						 CreatedById,
						 CreatedDate,
						 LastModifiedById,
						 LastModifiedDate,
						 ReminderDateTime,
						 IsReminderSet 
				 FROM 	 Task 
				 WHERE 	 id =: taskId];
		} catch (exception e){
			ta = new Task();
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected task. Please, try again.');
			ApexPages.addMessage(msg);  
		}
	} 	
	
	
	// used to find the attachments
	public boolean findAttachments() {
		try{
			attachResults = [SELECT Id, 
									ParentId, 
									Name, 
									BodyLength, 
									LastModifiedDate 
							FROM 	Attachment 
							WHERE 	ParentId =: taskId
							ORDER BY LastModifiedDate];
			return (attachResults.size() > 0);
		} catch(Exception e){
			return false;
		}
	}
	
	
	// used to insert an attachment to the task(edit mode) or temporary to whatId(save mode)
	public void insertAttachment() {
		try {
			if(newAttach.Body != NULL) { // if there is an attachment
				newAttach.parentId = ta.id;
				insert newAttach;
			}
			newAttach = new Attachment();
			attachExist = findAttachments();
		} catch(Exception e) {
			newAttach.Body = null; // to avoid limitations
			newAttach = new Attachment();
		}
		hideAttachSelPanel();
	}
	
	
	// used to show the attachment selection panel
	public void showAttachSelPanel() {
		showUploadPage = true;
	}
	
	
	// used to hide the attachment selection panel
	public void hideAttachSelPanel() {
		showUploadPage = false;
	}
	
	
	// used to remove an attachment from the attachments list
	public void deleteAttach() {
		Attachment tmpAttach = new Attachment();
		try{
			tmpAttach = [SELECT id FROM Attachment WHERE id =: AttachId];
			delete tmpAttach;
			attachExist	  = findAttachments();
		} catch (Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected attachment. Please, try again.');
			ApexPages.addMessage(msg);  
		}	
	}
	
	
	// used to open a new tab to show the attachment
	public PageReference viewAttach(){
		return new PageReference('/servlet/servlet.FileDownload?file='+ AttachId);
	}  	
	
	
	// used to call the edit attachment standard page
	public PageReference editAttach() {
		Attachment tmpAttach = new Attachment();
		try{
			tmpAttach = [SELECT id FROM Attachment WHERE id =: AttachId];
			ApexPages.standardController  tmpStdCtr = new ApexPages.standardController(tmpAttach);
			return tmpStdCtr.edit();
		} catch (Exception e) {
			return ApexPages.currentPage();
		}
	}
}