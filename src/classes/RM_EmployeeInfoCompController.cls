public with sharing class RM_EmployeeInfoCompController {
	public EMPLOYEE_DETAILS__c empl{get; private set;}
	public EMPLOYEE_DETAILS__c manager{get; private set;}
	public string employeeRID{get; set;}
	public boolean bViewNote{get; set;}
   // public Employee_Details__c rmAddNote{get; private set;}
    public string rmAddComment{get; set;} 
     
	public RM_EmployeeInfoCompController(){
    	//empl = RM_EmployeeService.GetEmployeeDetailByEmployee(employeeRID);
    	
    }
    public void getEmployeeInfo(){
    	system.debug('---employeeRID---' + employeeRID);
    	empl = RM_EmployeeService.GetEmployeeDetailByEmployee(employeeRID);
    	manager = getMangerEmail(empl.Supervisor_ID__c);
    	getPermission();
    }
    public PageReference addNote(){
        
        
        empl.RM_Comment__c = rmAddComment;
            
        update empl;
        return null; 
    }
     private EMPLOYEE_DETAILS__c getMangerEmail(string supervisorID){
        
        return RM_EmployeeService.GetEmployeeDetailByEmployeeID(supervisorID);
        
    }
    private void getPermission(){
    	Boolean bRMAdmin = RM_OtherService.IsRMAdmin(UserInfo.getUserId());
       
        set<string> stManagedBU = RM_OtherService.GetUserGroupBU(UserInfo.getUserId()); 
 
        bViewNote = false;
        if (stManagedBU!= null && stManagedBU.size() != 0){
            if (stManagedBU.contains(empl.Business_Unit_Desc__c))
                bViewNote = true;           
        }
        else{
            if ( bRMAdmin )       
                    bViewNote = true;           
        }
        system.debug('---------bViewNote---------'+bViewNote  ); 
        if (bViewNote)
        {           
            rmAddComment = empl.RM_Comment__c;
        }
    }
    
}