public with sharing class EpochDataAccessor {

	/** Object definition for fields used in application for Epoch__c
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('Epoch__c');		
	}
	
	
	/** Object definition for fields used in application for Epoch__c with the parameter referenceName as a prefix
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'ClinicalDesign__c,';
		result += referenceName + 'Epoch_Number__c,';
		result += referenceName + 'Duration_Days__c,';		
		result += referenceName + 'Description__c';
		return result;
	}
	
	/** Retrieve list of Epochs.
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @param	whereClause			The criteria that Laboratory Selection Lists must meet
	 * @return	List of Epochs
	 */
	public static List<Epoch__c> getEpochList(String whereClause) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM Epoch__c ' +
								'WHERE  {1} ' +
								'ORDER BY Epoch_Number__c',
								new List<String> {
									getSObjectFieldString(),
									whereClause
								}
							);
		return (List<Epoch__c>) Database.query(query);
	}

}