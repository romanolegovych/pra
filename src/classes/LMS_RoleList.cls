public class LMS_RoleList {

	public Boolean selected{get;set;}
    public String roleId{get;set;}
    public String name{get;set;}
    public String status{get;set;}
    public Integer count{get;set;}
    
    public LMS_RoleList(LMS_Role__c r, Map<String, LMS_Role_Course__c> m){
        if(m.containsKey(r.Id)) {
            selected = true;
        } else {
    		selected=false;
        }
    	roleId = r.Id;
    	name = r.Role_Name__c;
    	status = r.Status__c;
    	count = r.RoleEmployees__r.size();
    } 
}