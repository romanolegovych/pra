public with sharing class BDT_AssessmentHelper {
		
	//This method returns all ProjectServiceTimePoint__c which have been used to store the ProjectService assigned to the Flowchart. The record will always be TimePoint orphaned.
	//Needs to be retrieved everytime user selected a different flowchart
	public static Map<Id, ProjectServiceTimePoint__c> getProjectServiceAssignedToFlowchart(String flowchartId){
		Map<Id, ProjectServiceTimePoint__c> projectServiceAssignedToFlowchartMap = new Map<Id, ProjectServiceTimePoint__c>();
		List<ProjectServiceTimePoint__c> projectServiceAssignedToFlowchart = new  List<ProjectServiceTimePoint__c>();
		try{ 
    		projectServiceAssignedToFlowchart = [Select TimePoint__c, ProjectService__c, ProjectService__r.service__r.name, ProjectService__r.service__c, Name, Id, Flowchart__c 
    												From ProjectServiceTimePoint__c 
    												where flowchart__c = :flowchartId and TimePoint__c = '' LIMIT 1000];
    	}catch(Exception e){   
            
        } 
        
        for(ProjectServiceTimePoint__c pstp: projectServiceAssignedToFlowchart){
        	projectServiceAssignedToFlowchartMap.put(pstp.ProjectService__c, pstp);
        }
        
        return projectServiceAssignedToFlowchartMap;
	}
	
	//Returns all Service Categories within the system
	public static List<ServiceCategory__c> getExistentServicesCategory(){ 
        List<ServiceCategory__c> existingServicesCatList = new  List<ServiceCategory__c>();
        try{ 
            existingServicesCatList = [Select id, name, Code__c, BDTDeleted__c, 
            							(select id, IsDesignObject__c, InformationalText__c, SequenceNumber__c, BDTDeleted__c, name 
            								from Services__r 
            								where IsDesignObject__c = true 
            								and BDTDeleted__c = false
            								order by SequenceNumber__c ) 
            						   From ServiceCategory__c s 
            						   where id IN (select ServiceCategory__c from Service__c where IsDesignObject__c = true and BDTDeleted__c = false) 
            						   and BDTDeleted__c = false
            						   order by code__c
            						   LIMIT 1000];
        }catch(Exception e){   
           
        }
        
        for (ServiceCategory__c S:existingServicesCatList){
			s.Code__c = BDT_Utils.removeLeadingZerosFromCat(s.Code__c);
		}
        
        return  existingServicesCatList;    
    }
    
    //Returns all Services available for selected Category
    public static List<Service__c> getServicesAvailable(String serviceCategoryId){    	
    	
    	List<Service__c> serviceListAval = new List<Service__c>();
    	
    	try{
    		serviceListAval = [ select id, IsDesignObject__c, InformationalText__c, SequenceNumber__c, BDTDeleted__c, name, ServiceCategory__r.Name, ServiceCategory__r.Code__c
            								from Service__c where IsDesignObject__c = true and BDTDeleted__c = false and ServiceCategory__c = :serviceCategoryId
            								order by SequenceNumber__c LIMIT 1000];
    	}catch(Exception e){
    		
    	} 
    	
    	return serviceListAval;
    }
    
    //Builds a list of all the services available for a selected Service Category and checks if they have already been assigned to ProjectService record
    //which in turn has been assigned to the Flowchart using a orphaned ProjectServiceTimePoint__c record
    //@ List<servicesAvailableWrapper> servicesWrapperList: serviceWrapperList which is being looked at by the user
    //@ List<servicesAvailableWrapper> ALLservicesWrapperList: all serviceWrapperList selected/deselected by the user
    //@ List<Service__c> serviceListAval: List of the Services available for that particular category
    //@ Map<Id, ProjectServiceTimePoint__c> projectServiceAssignedToFlowchart: The Id is the ProjectService__c id NOT THE ProjectServiceTimePoint__c Id
    //@ Map<Id, ProjectService__c> projectServicesPreviouslySelected: The Id is the Service__c record used NOT THE ProjectService__c Id
    public static void getServicesAvailableWrapper(List<servicesAvailableWrapper> servicesWrapperList, List<servicesAvailableWrapper> ALLservicesWrapperList, 
    										Map<Id, ProjectService__c> projectServicesPreviouslySelected, Map<Id, ProjectServiceTimePoint__c> projectServiceAssignedToFlowchart,
    										List<Service__c> serviceListAval){
 
    	//check if this is empty, if the list contains services already, copy them to a general list for later processing and empty 	
    	if(!servicesWrapperList.IsEmpty()){
    		ALLservicesWrapperList.addAll(servicesWrapperList);    					
		}
   	
        servicesWrapperList.clear();
            	    	
    	//check if each Service has been assigned to a ServiceProject used on a ServiceProjectTimePoint and mark as selected if true
    	for(Service__c sv: serviceListAval){
    		Boolean exists = false;
    		boolean disabled = false;
    		if(projectServicesPreviouslySelected.containsKey(sv.id)){
    			String ProjectServiceId = projectServicesPreviouslySelected.get(sv.id).id;
    			if(projectServiceAssignedToFlowchart.containsKey(ProjectServiceId)){
    				ProjectServiceTimePoint__c tmp = projectServiceAssignedToFlowchart.get(ProjectServiceId);
    				exists = true;
    				disabled = true;
    			}
    		} 
    		servicesWrapperList.add(new servicesAvailableWrapper(sv, exists, disabled));
    	}
    }
    
    
    /*Inner class used on the Services (Assessments) Section for time Assessment selection*/
    public class servicesAvailableWrapper{
    	public Service__c serviceRecord {get; set;}
        public boolean selected {get; set;}
        public boolean disabled	{get;set;}
        
        public servicesAvailableWrapper(Service__c p_ServiceRecord, boolean p_Selected, boolean p_disabled){
            serviceRecord = p_ServiceRecord;
            selected = p_Selected;
            disabled = p_disabled;
        }
    }

        
}