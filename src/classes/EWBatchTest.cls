@istest public with sharing class EWBatchTest
{
	@testsetup public static void setup()
	{
		STSWR1__Flow_Instance_Cursor__c cursor = PAWS_ApexTestsEnvironment.FlowInstanceCursor;
		
		Critical_Chain__c chainRecord = new Critical_Chain__c(Name = 'A', Flow__c = PAWS_ApexTestsEnvironment.Flow.Id);
		insert chainRecord;
		
		List<Critical_Chain_Step_Junction__c> chainJunctions = new List<Critical_Chain_Step_Junction__c>();
		for (STSWR1__Flow_Step_Junction__c stepJunction : [select Name from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__c = :PAWS_ApexTestsEnvironment.Flow.Id])
		{
			//new STSWR1.API().call('WorkflowService', 'changeFlowStep', new List<Object>{PAWS_ApexTestsEnvironment.FlowInstanceCursor, stepJunction.Name});
			if (PAWS_ApexTestsEnvironment.FlowInstanceCursor.STSWR1__Step__c == null)
			{
				PAWS_ApexTestsEnvironment.FlowInstanceCursor.STSWR1__Step__c = stepJunction.Id;
			}
			
			chainJunctions.add(new Critical_Chain_Step_Junction__c(Flow_Step__c = stepJunction.Id, Critical_Chain__c = chainRecord.Id, Remaining_Time_Change_Date__c = System.today().addDays(-2)));
		}
		
		//update PAWS_ApexTestsEnvironment.FlowInstanceCursor;
		
		insert chainJunctions;
	}
	
	@istest private static void testBatch()
	{
		Test.startTest();
			String guid = EncodingUtil.convertToHex(Crypto.generateAesKey(128)).toUppercase();
			System.schedule(guid, '0 0 0 * * ?', new EWBatch('select Id from Critical_Chain__c'));
		Test.stopTest();
		
		//System.assertEquals(1, [select count() from Critical_Chain_Step_Junction__c where Remaining_Time_Change_Date__c = TODAY]);
	}
}