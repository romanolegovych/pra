public class DWF_FilterServiceWrapper {

    private static String ENDPOINT = '';
    private static String SERVICENAME = '';
    private static Integer TIMEOUT = 0;
    private static List<String> errors;
    
    static {
        initSettings();
    }
    
    public static DWF_FilterService.companyListResponse getAllCompanyVOs() {
        DWF_FilterService.companyListResponse response = null;
        DWF_FilterService.DwFilterServicePort service = getService();
        try {
            response = service.getDistinctCompanyData();
            system.debug('---------------- Distinct companyVO retreival Successful ----------------');
        } catch (Exception e) {
            errors = new List<String>();
            errors.add(e.getMessage());
            system.debug('---------------- Distinct companyVO retreival failed -----------------');
            system.debug('----- Error: ' + e.getMessage());
            system.debug('----- Stack trace: ' + e.getStackTraceString());
            system.debug('----- Cause: ' + e.getCause());
        }
        return response;
    }
    
    public static DWF_FilterService.employeeListResponse getEmployeeListByCompanyNo(Long companyNo) {
        DWF_FilterService.employeeListResponse response = null;
        DWF_FilterService.DwFilterServicePort service = getService();
        try {
            response = service.getEmployeeVOByCompanyNo(companyNo);
            system.debug('---------------- Employee list retreival Successful ----------------');
        } catch (Exception e) {
            errors = new List<String>();
            errors.add(e.getMessage());
            system.debug('---------------- Employee list retreival failed -----------------');
            system.debug('----- Error: ' + e.getMessage());
            system.debug('----- Stack trace: ' + e.getStackTraceString());
            system.debug('----- Cause: ' + e.getCause());
        }
        return response;
    }
    
    public static DWF_FilterService.employeeListResponse getEmployeeListByCompanyNoBUNo(Long companyNo, String businessUnitNo) {
        DWF_FilterService.employeeListResponse response = null;
        DWF_FilterService.DwFilterServicePort service = getService();
        try {
            response = service.getEmployeeVOByCompanyNoBUNo(companyNo, businessUnitNo);
            system.debug('---------------- Employee list retreival Successful ----------------');
        } catch (Exception e) {
            errors = new List<String>();
            errors.add(e.getMessage());
            system.debug('---------------- Employee list retreival failed -----------------');
            system.debug('----- Error: ' + e.getMessage());
            system.debug('----- Stack trace: ' + e.getStackTraceString());
            system.debug('----- Cause: ' + e.getCause());
        }
        return response;
    }
    
    public static DWF_FilterService.employeeListResponse getEmployeeListByCompanyNoBUName(Long companyNo, String businessUnitName) {
        DWF_FilterService.employeeListResponse response = null;
        DWF_FilterService.DwFilterServicePort service = getService();
        try {
            response = service.getEmployeeVOByCompanyNoBUName(companyNo, businessUnitName);
            system.debug('---------------- Employee list retreival Successful ----------------');
        } catch (Exception e) {
            errors = new List<String>();
            errors.add(e.getMessage());
            system.debug('---------------- Employee list retreival failed -----------------');
            system.debug('----- Error: ' + e.getMessage());
            system.debug('----- Stack trace: ' + e.getStackTraceString());
            system.debug('----- Cause: ' + e.getCause());
        }
        return response;
    }
    
    public static DWF_FilterService.employeeListResponse getEmployeeListByCompanyEmpData(Long companyNo, String businessUnitName,
    	String empFirstName, String empLastName, String empStatusDesc) {
        DWF_FilterService.employeeListResponse response = null;
        DWF_FilterService.DwFilterServicePort service = getService();
        try {
            response = service.getEmployeeVOByCompanyEmpData(companyNo, businessUnitName, empFirstName, empLastName, empStatusDesc);
            system.debug('---------------- Employee list retreival Successful ----------------');
        } catch (Exception e) {
            errors = new List<String>();
            errors.add(e.getMessage());
            system.debug('---------------- Employee list retreival failed -----------------');
            system.debug('----- Error: ' + e.getMessage());
            system.debug('----- Stack trace: ' + e.getStackTraceString());
            system.debug('----- Cause: ' + e.getCause());
        }
        return response;
    }
    
    public static List<String> getErrors() {
        return errors;
    }
    
    private static DWF_FilterService.DwFilterServicePort getService() {
        DWF_FilterService.DwFilterServicePort service = new DWF_FilterService.DwFilterServicePort();
        service.endpoint_x = ENDPOINT + SERVICENAME;
        service.timeout_x = TIMEOUT;
        return service;
    }
    
    private static void initSettings() {
        Map<String, MuleServicesCS__c> settings = MuleServicesCS__c.getAll();
        ENDPOINT = settings.get('ENDPOINT').Value__c;
        SERVICENAME = settings.get('DwFilterService').Value__c;
        Integer to = Integer.valueOf(settings.get('TIMEOUT').Value__c);
        if (to != null && to > 0) {
            TIMEOUT = to;
        } 
        system.debug('--------- Using following Custom Settings for MuleServicesCS ---------');
        system.debug('ENDPOINT:' + ENDPOINT);
        system.debug('SERVICENAME:' + SERVICENAME);
        system.debug('TIMEOUT:' + TIMEOUT);
        system.debug('----------------------- MuleServicesCS END ----------------------------');
    }

}