public class LMS_ViewCurrentImpactController {
	
    public String pageTitle{get;set;}
    public String roleName{get;set;}
    public String courseName{get;set;}
    public String roleId{get;set;}
    public String courseId{get;set;}
    public List<LMS_EmployeeList> empList{get;set;}
    public List<LMS_Role_Employee__c> employees{get;set;}
    private Map<String,String> jobTitles{get;set;}
    
    public LMS_ViewCurrentImpactController() {
        jobTitles = new Map<String,String>();
        roleId = ApexPages.currentPage().getParameters().get('id');
        courseId = ApexPages.currentPage().getParameters().get('cid');
        
        System.debug('-------------------roleId------------------------'+roleId);
        System.debug('-------------------courseID------------------------'+courseId);
        
        List<Job_Title__c> job = [select Job_Title__c, Job_Code__c from Job_Title__c];
        for(Job_Title__c jt : job) {
            jobTitles.put(jt.Job_Code__c,jt.Job_Title__c);
        }
        
        if(roleId != null && roleId != '') {
            LMS_Role__c r = [select Role_Name__c from LMS_Role__c where Id =:roleId];
            roleName = r.Role_Name__c;
            pageTitle='Employee Listing for Role: ' + roleName;
        }
        if(courseId != null && courseId != '') {
            LMS_Course__c c = [select Title__c from LMS_Course__c where Id =:courseId];
            courseName = c.Title__c;
            pageTitle='Employee Listing for Course: ' + courseName;
        }
        
        searchEmployees();
    }
    
    private void searchEmployees() {
        if(roleId != null && roleId != '') {
            String mapQry = 'select Employee_Id__r.First_Name__c,Employee_Id__r.Last_Name__c,Employee_Id__r.Job_Class_Desc__c,' + 
            	'Employee_Id__r.Job_Code__c,Employee_Id__r.Business_Unit_Desc__c,Employee_Id__r.Department__c,' + 
            	'Employee_Id__r.Country_Name__r.Region_Name__c,Employee_Id__r.Country_Name__r.Name,Employee_Id__r.Status_Desc__c,Assigned_On__c,' + 
            	'Commit_Date__c, Status__c ' + 
            	'from LMS_Role_Employee__c where Role_Id__c =:roleId and Status__c != \'Hold\' order by Employee_Id__r.Last_Name__c';
            employees = Database.query(mapQry);
            empList = new List<LMS_EmployeeList>();
            for(LMS_Role_Employee__c re : employees) {
                empList.add(new LMS_EmployeeList(re, jobTitles));
            }
        } else if(courseId != null && courseId != '') {
            String roleQry = 'select Role_Id__r.Id from LMS_Role_Course__c where Course_Id__c =:courseId and Status__c!=\'Draft\'';
            List<LMS_Role_Course__c> roles = Database.query(roleQry);
            Set<id> roleIds = new Set<id>();
            for(LMS_Role_Course__c rc : roles)
                roleIds.add(rc.Role_Id__r.Id);

            String mapQry = 'select Employee_Id__r.First_Name__c,Employee_Id__r.Last_Name__c,Employee_Id__r.Job_Class_Desc__c,' + 
            	'Employee_Id__r.Job_Code__c,Employee_Id__r.Business_Unit_Desc__c,Employee_Id__r.Department__c,' + 
            	'Employee_Id__r.Country_Name__r.Region_Name__c,Employee_Id__r.Country_Name__r.Name,Employee_Id__r.Status_Desc__c,Assigned_On__c,' + 
            	'Commit_Date__c, Status__c ' + 
            	' from LMS_Role_Employee__c where Role_Id__c IN :roleIds and Status__c != \'Hold\' order by Employee_Id__r.Last_Name__c';
            employees = Database.query(mapQry);
            empList = new List<LMS_EmployeeList>();
            for(LMS_Role_Employee__c re : employees) {
                empList.add(new LMS_EmployeeList(re, jobTitles));
            }
        }
    }
}