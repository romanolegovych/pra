@isTest
private class BDT_StudyServicePricingCreationTest {
	public static Client_Project__c firstProject 	{get;set;}
	public static list<Study__c> studiesList 		{get;set;}
	public static list<Business_Unit__c> myBUList	{get;set;}
	public static list<Site__c> mySiteList  		{get;set;}
	public static list<ServiceCategory__c> scList	{get;set;}
	public static list<Service__c> serviceList		{get;set;}
	
	static void init(){
		firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;
		
		myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert myBUList;
		
		
		studiesList = BDT_TestDataUtils.buildStudies(firstProject);
		studiesList[0].Business_Unit__c = myBUList[0].Id; 
		studiesList[1].Business_Unit__c = myBUList[1].Id; 
		studiesList[2].Business_Unit__c = myBUList[2].Id;
		studiesList[3].Business_Unit__c = myBUList[3].Id; 
		studiesList[4].Business_Unit__c = myBUList[4].Id;
		insert studiesList;
		string selectedStudies ='';
		for(Study__c st: studiesList){
			selectedStudies += st.Id+':';
		}
		
		scList = BDT_TestDataUtils.buildServiceCategory(1, null);
		
		serviceList = BDT_TestDataUtils.buildService(scList, 1, 'ServiceTest-', false);
		
						
		BDT_Utils.setPreference('SelectedProject', firstProject.id);
		BDT_Utils.setPreference('SelectedStudies',selectedStudies);
	}

    static testMethod void myUnitTest() {
        init();
        
		ProjectService__c ps = new ProjectService__c(Client_project__c = firstProject.id, service__c = serviceList[0].id);

		//first insert of studyservice, system will create new studyservicepricing object
		insert ps;
		studyService__c sts = new studyService__c(projectservice__c =ps.id, study__c = studiesList[0].Id, NumberOfUnits__c = 10 );
		insert sts;
		
		//update studyservice before locking it with an approved document
		sts.NumberOfUnits__c =20;
		upsert sts;
        
        //create financial document and set the ApprovedFinancialDocument attribute on the studyservicePricing object.
 /*       ProjectStudyGrouping__c psg = new ProjectStudyGrouping__c();
        upsert psg;
        FinancialDocument__c fd = new FinancialDocument__c(ApprovalStatus__c = 'Approved', DocumentType__c='Temp', ProjectStudyGrouping__c = psg.id );
        upsert fd;
                
        StudyServicePricing__c ssp =[select id, ApprovedFinancialDocument__c from studyservicepricing__c where studyService__c = :sts.id];
        ssp.ApprovedFinancialDocument__c = fd.Id;
        upsert ssp; */
        
        sts.NumberOfUnits__c = 15;
        upsert sts;
         
        
    }
}