public with sharing class CM_AgreementRedirectController {

    private ApexPages.StandardController standardController;
    private String objectId = null;
    private String action = null;
    private String method = null;
    private String retURL = null;
    private String RecordType= null;
    
    private void setParams() {
        this.objectId = ApexPages.currentPage().getParameters().get('id');
        this.action = ApexPages.currentPage().getParameters().get('action');
        this.method = ApexPages.currentPage().getParameters().get('method');
        
        this.RecordType = ApexPages.currentPage().getParameters().get('RecordType');
        this.retURL = ApexPages.currentPage().getParameters().get('retURL');
        
        System.debug('id:'+this.objectId+', action:'+this.action+', method:'+this.method);
        System.debug('id:'+this.objectId+', RecordType :'+this.RecordType +', retURL:'+this.retURL);
    }
 
    public CM_AgreementRedirectController(ApexPages.StandardController standardController)
    {
        System.debug('-----------------> CM_AgreementRedirectController');
        this.standardController = standardController;
        setParams();
        System.debug('<----------------- CM_AgreementRedirectController');
    }
    
    public PageReference doCancel()
  {
    return this.standardController.cancel();
  }
  
  public PageReference save()
  {
     System.debug('<-----------------> CM_AgreementRedirectController.save');
    return this.standardController.cancel();
  }
    
    public PageReference redirect()
    {
        setParams();
        String RecordTypeName = [SELECT Id,name from RecordType WHERE id=:this.RecordType].Name;  
        PageReference pageRef = new PageReference('/apex/CM_AgreementforNew?RecordType=' + RecordTypeName+'&retURL='+this.retURL);
        pageRef.setRedirect(true);
        System.debug('-----------------> CM_AgreementRedirectController.redirect');
        /*
        if('sendToCTMS' == this.method) {
            sendToCTMS();
        } else if('sendToEtmf' == this.method) {
            sendToEtmf();
        }
        */
        System.debug('<----------------- CM_AgreementRedirectController.redirect');
        return pageRef;
        
    }
    
    /*private void sendToCTMS()
    {
        System.debug('-----------------> CM_AgreementRedirectController.sendToCTMS');
        Id recordId = standardController.getId();
        System.debug('Id:'+recordId);
        Apttus__APTS_Agreement__c record = (Apttus__APTS_Agreement__c) standardController.getRecord();
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Sent to CTMS'); 
        ApexPages.addMessage(myMsg);
        System.debug('<----------------- CM_AgreementRedirectController.sendToCTMS');
    }
    
    private void sendToEtmf()
    {
        System.debug('-----------------> CM_AgreementRedirectController.sendToEtmf');
        Id recordId = standardController.getId();
        System.debug('Id:'+recordId);
        Apttus__APTS_Agreement__c record = (Apttus__APTS_Agreement__c) standardController.getRecord();
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Sent to eTMF'); 
        ApexPages.addMessage(myMsg);
        System.debug('<----------------- CM_AgreementRedirectController.sendToEtmf');
    }*/

}