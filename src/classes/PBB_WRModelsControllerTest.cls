@isTest
private class PBB_WRModelsControllerTest {
	private static PBB_WRModelsController cont;
	
	@isTest
	static void WRModelsControllerTest() {
		Test.startTest();
    		cont = new PBB_WRModelsController();
    	Test.stopTest();
    	System.assert(cont.WRModelsList.size() == 0);
	}

	@isTest
	static void getNewPageReferenceTest() {
		cont = new PBB_WRModelsController();
		Test.startTest();
    		PageReference pr = cont.getNewPageReference();
    	Test.stopTest();
    	Schema.DescribeSObjectResult  r = WR_Model__c.SObjectType.getDescribe();
    	System.assertEquals(pr.getUrl(), '/'+r.getKeyPrefix()+'/e');
	}
}