/**
* @author Ramya Shree Edara
* @date 1/14/2015
* @description this is test class for scenario related classes
*/
@isTest
private class PBB_UtilsTest{
    
   //test Method to show error Message
    static testMethod void testShowErrorMessage(){
        PBB_Utils.showErrorMessage('Error');
        PBB_Utils.showMessage('Success');
    }
    
    //test Method to show message
    static testMethod void testShowMessage(){
        PBB_Utils.showMessage('Success');
    }
    
    //test Method to generate date from String date in the format of 12/17/2015
    static testMethod void testGetDate(){
    	//System.debug('>>> dateInStringFormat>>> '+'17-Dec-2015');
        Date dt = PBB_Utils.getDate('17-Dec-2015');
        Date newDt = Date.newInstance(2015,12,17);     
        System.assertEquals(dt, newDt);
    }
    
     static testMethod void testGetDate1(){
        Date dt1 = PBB_Utils.getDate('17-Nov-2015');
        Date newDt1 = Date.newInstance(2015,11,17);     
        System.assertEquals(dt1, newDt1);
    }
    
    static testMethod void testGetDate2(){
        Date dt2 = PBB_Utils.getDate('17-Oct-2015');
        Date newDt2 = Date.newInstance(2015,10,17);     
        System.assertEquals(dt2, newDt2);
    }
     
    static testMethod void testGetDate3(){
        Date dt3 = PBB_Utils.getDate('17-Sep-2015');
        Date newDt3 = Date.newInstance(2015,9,17);     
        System.assertEquals(dt3, newDt3);
    }
    
    static testMethod void testGetDate4(){
        Date dt4 = PBB_Utils.getDate('17-Aug-2015');
        Date newDt4 = Date.newInstance(2015,8,17);     
        System.assertEquals(dt4, newDt4);
    }
    
    static testMethod void testGetDate5(){
        Date dt5 = PBB_Utils.getDate('17-Jul-2015');
        Date newDt5 = Date.newInstance(2015,7,17);     
        System.assertEquals(dt5, newDt5);
    }
    
     static testMethod void testGetDate6(){
        Date dt6 = PBB_Utils.getDate('30-May-2015');
        Date newDt6 = Date.newInstance(2015,5,30);    
        System.assertEquals(dt6, newDt6);
    }
    
    static testMethod void testGetDate7(){
        Date dt7 = PBB_Utils.getDate('17-Jun-2015');
        Date newDt7 = Date.newInstance(2015,6,17);     
        System.assertEquals(dt7, newDt7);
    }
    
    static testMethod void testGetDate8(){
        Date dt8 = PBB_Utils.getDate('17-Apr-2015');
        Date newDt8 = Date.newInstance(2015,4,17);     
        System.assertEquals(dt8, newDt8);
    }
       
    static testMethod void testGetDate9(){
        Date dt9 = PBB_Utils.getDate('17-Mar-2015');
        Date newDt9 = Date.newInstance(2015,3,17);     
        System.assertEquals(dt9, newDt9);
    }
    
    static testMethod void testGetDate10(){
        Date dt10 = PBB_Utils.getDate('17-Feb-2015');
        Date newDt10 = Date.newInstance(2015,2,17);     
        System.assertEquals(dt10, newDt10);
    }
    
    static testMethod void testGetDate11(){
        Date dt11 = PBB_Utils.getDate('17-Jan-2015');
        Date newDt11 = Date.newInstance(2015,1,17);     
        System.assertEquals(dt11, newDt11);
    }
    
    
    //test Method to generate date from String date in the format of 07/15/2014
    static testMethod void testGetDateInStringFormat(){
        String strDate = PBB_Utils.getDateInStringFormat(Date.Today());
        System.assertNotEquals(null, strDate);
    }
    
}