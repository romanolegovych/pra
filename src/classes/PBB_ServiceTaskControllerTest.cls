/**
@author Grace Guo
@date 2015
@description this is PBB_ServiceTaskController's test class
**/
@isTest
private class PBB_ServiceTaskControllerTest {
    static testMethod void testPBB_ServiceTaskController(){
        test.startTest();       
        PBB_TestUtils tu = new PBB_TestUtils ();
        tu.smm = tu.createServiceModelAttributes();  
        tu.sa = tu.createServiceAreaAttributes();   
        tu.sf = tu.createServiceFunctionAttributes();   
        tu.st = tu.createServiceTaskAttributes();   
        tu.smm = tu.approveServicemodel(tu.smm);   
        tu.prabu = tu.createPRABusinessUnits();      
        Apexpages.currentPage().getParameters().put('id', tu.st.id);
        PBB_ServiceTaskController stc = new PBB_ServiceTaskController(new Apexpages.Standardcontroller(tu.st));
        System.assertEquals(tu.st.id,stc.stID );
        stc.getBusinessSegmentTypes();
        stc.SelectBSs.add('Strategic Solutions');        
        stc.saveST();system.debug('------tu.st.Business_Segment__c--------------'+tu.st.Business_Segment__c+stc.SelectBSs);
        stc.st = PBB_DataAccessor.getServiceTaskByIDForBS(tu.st.id);
        system.debug('------stc.st--------------'+stc.st);
        System.assertEquals(stc.st.Business_Segment__c,'Product Registration' );        
        stc.SelectBSs = new list<string>();
        stc.saveST();        
        test.stopTest();
    }
}