@isTest
public with sharing class BDT_SecuritiesControllerTest {

	// all logic and data retieval is already tested only code ceverage needed here
	// functional testing of the page is for UAT

	static testMethod void allTests() {
		BDT_SecuritiesController a = new BDT_SecuritiesController();
		
		a.disableAllShowBooleans();
		
		// add role button
		a.buttonAddRole();
		a.AddEditRole.name= 'Hello my pretty new role';
		
		// save the new role
		a.buttonSaveRole();
		
		// edit the just created role
		PageReference pageref = new pagereference(System.Page.BDT_Securities.getURL() );
		pageref.getParameters().put('RoleId',a.displayRoles[0].role.id);
		test.setCurrentPage(pageref);
		a.buttonEditRole();
		
		// remove the role
		a.buttonDeleteRole();
		a.buttonCancelRole();
		
		a.buttonAddUser();
		a.userName = 'Unknown';
		a.buttonSaveNewUser();
		a.buttonCancelAddUser();
		
		// create dummy user
				// add a user to salesforce
		Profile p = [select id from profile where name='System Administrator'];
		User myUser = new User(alias = 'SuperMan', email='clark.kent@dailymail.com',
			emailencodingkey='UTF-8', firstname='Clark', lastname='Kent', languagelocalekey='en_US',
			localesidkey='en_US', profileid = p.Id,
			timezonesidkey='America/Los_Angeles', username='clark.kent@dailymail.com');
		insert myUser;
		
		// create this user
		a.buttonAddUser();
		a.userName = 'Clark Kent';
		a.buttonSaveNewUser();
		
		a.buttonUserRolesMoveToUsed();
		a.buttonUserRolesMoveToUnused();
		a.buttonSaveConfigureUserRoles();
		
		
	}

}