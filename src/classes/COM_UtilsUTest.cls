@isTest
private class COM_UtilsUTest {

    static testMethod void isEmptyTest (){
		system.assertEquals(true, COM_Utils.isEmpty(null));
		system.assertEquals(true, COM_Utils.isEmpty(' '));
		system.assertEquals(false, COM_Utils.isEmpty('asd'));
		//system.assertEquals(true, COM_Utils.isEmpty(new Map<String,String>()));
		//system.assertEquals(true, COM_Utils.isEmpty(new Map<Object,Object>()));
		//system.assertEquals(true, COM_Utils.isEmpty(new Set<String>()));
		//system.assertEquals(true, COM_Utils.isEmpty(new Set<Object>()));
		List<String> strings = new List<String>();
		system.assertEquals(true, COM_Utils.isEmpty(strings));
		strings.add('asd');
		system.assertEquals(false, COM_Utils.isEmpty(strings));
		List<Object> objects = new List<Object>();
		system.assertEquals(true, COM_Utils.isEmpty(objects));
		objects.add('asd');
		system.assertEquals(false, COM_Utils.isEmpty(objects));
	}
	
	static testMethod void isNotEmptyTest (){
		system.assertEquals(true, COM_Utils.isNotEmpty('asd'));
		system.assertEquals(false, COM_Utils.isNotEmpty(' '));
		List<String> strings = new List<String>();
		system.assertEquals(false, COM_Utils.isNotEmpty(strings));
		strings.add('asd');
		system.assertEquals(true, COM_Utils.isNotEmpty(strings));
		List<Object> objects = new List<Object>();
		system.assertEquals(false, COM_Utils.isNotEmpty(objects));
		objects.add('asd');
		system.assertEquals(true, COM_Utils.isNotEmpty(objects));
	}
	
	static testMethod void toStringTest (){
		List<String> l = new List<String> {'a','b','c'};
		String comma = ',';
		String quote = '\'';
		String commaSeparatedStr = 'a,b,c';
		String quoteWrappedCommaSeparatedStr = '\'a\',\'b\',\'c\'';
		system.assertEquals(quoteWrappedCommaSeparatedStr, COM_Utils.toString(l, comma, quote));
		system.assertEquals(commaSeparatedStr, COM_Utils.toString(l, comma, null));
		system.assertEquals(true, COM_Utils.isEmpty(COM_Utils.toString(l, null, null)));
	}

	static testMethod void substringAfterLastTest(){
		String str = '/servlet/servlet.FileDownload/AgreementProtocol %231001_Site0125_2014-06-05_v1_Redlines.doc?mimetype=doc&file=015L00000006frYIAQ';
		String separator = '=';
		system.assertEquals('015L00000006frYIAQ', COM_Utils.substringAfterLast(str, separator));
	}
	
	static testMethod void getObjectNameTest(){
		system.assertEquals('Account',COM_Utils.getObjectName(Account.sObjectType));
		system.assertEquals('INT_SETUP_STATUS',COM_Utils.getObjectName(INT_SETUP_STATUS__c.sObjectType));
	}
	
	static testMethod void getSObjectTypeNameTest(){
		Account acc = new Account(); 
		system.assertEquals('Account',COM_Utils.getSObjectTypeName(acc));
		
		INT_SETUP_STATUS__c setupStatus = new INT_SETUP_STATUS__c();
		system.assertEquals('INT_SETUP_STATUS__c',COM_Utils.getSObjectTypeName(setupStatus));
	}
	
	static testMethod void getEnvironmentTest(){
		COM_CS__c settings = new COM_CS__c(name = 'ENV', Value__c = '');
		upsert settings; 
		system.assertEquals(COM_Utils.Env.DEV,COM_Utils.getEnv());
		
		settings = new COM_CS__c(name = 'ENV', Value__c = 'CI');
		upsert settings; 
		system.assertEquals(COM_Utils.Env.CI,COM_Utils.getEnv());
		
		settings = new COM_CS__c(name = 'ENV', Value__c = 'TEST');
		upsert settings; 
		system.assertEquals(COM_Utils.Env.TEST,COM_Utils.getEnv());
		
		settings = new COM_CS__c(name = 'ENV', Value__c = 'UAT');
		upsert settings; 
		system.assertEquals(COM_Utils.Env.UAT,COM_Utils.getEnv());
		
		settings = new COM_CS__c(name = 'ENV', Value__c = 'PROD');
		upsert settings; 
		system.assertEquals(COM_Utils.Env.PROD,COM_Utils.getEnv());
	}
	
	static testMethod void isSandboxTest(){
		COM_CS__c settings = new COM_CS__c(name = 'ENV', Value__c = '');
		upsert settings;
		system.assertEquals(true,COM_Utils.isSandbox());
		
		settings = new COM_CS__c(name = 'ENV', Value__c = 'PROD');
		upsert settings;
		system.assertEquals(false,COM_Utils.isSandbox());
	}
	
}