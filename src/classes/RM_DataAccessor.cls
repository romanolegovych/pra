public with sharing class RM_DataAccessor {
   	/**
	* @author 	Min Chen	
	* @description  Data accessor other than assignment, demand employee and project 
    **/
   
    public RM_DataAccessor(){ 
         
    }
    public static list<string> GetListActiveStatus(){
        list<string> listActive = new list<string>();
        for (Employee_Status__c e:[SELECT Employee_Status__c FROM Employee_Status__c where employee_group__c in ('Active', 'Leave')]){
            listActive.add(e.Employee_Status__c);
        }
        return listActive;
    }
 

   
    public static list<string> GetStateProvincebySubRegion(set<string> lstSubRegion){
        list<string> lstState = new list<string>();
        for (WFM_Sub_Region__c r : [select State_Provience__c from WFM_Sub_Region__c where Sub_Region__c in :lstSubRegion  order by State_Provience__c asc])
            lstState.add(r.State_Provience__c);
        return lstState;
    }
  
    public static string GetRegionByCountry(string countryName){
        list<Country__c> lstCountry = [select Region_Name__c from Country__c where name=:countryName];
        if (lstCountry.size() > 0)
            return lstCountry[0].Region_Name__c;
        return null; 
    }
    public static list<string> GetCountrybyRMRegion(set<string> sRMRegion){
        list<string> lstCountry = new list<string>();
        for(Country__c c: [select name from Country__c where RM_Region__c in :sRMRegion order by name asc])
            lstCountry.add(c.name);
        return lstCountry;
    }  
 
	public static set<string> getPhase(){
		return RM_Tools.GetSetfromListObject('name', [select name from  WFM_Phase__c order by name asc]);
		
	}
    
    
    /*******************************
    Country Hour
    ********************************/
   
    public static list<WFM_Country_Hours__c> GetEmployeeCountryHour(Employee_Details__c employee, list<string> yearMonthList){
        
        return [select id, NBR_Business_Days_in_Month__c, Year_Month__c,Country_Name__c, country_name__r.PRA_Country_ID__c, country_name__r.daily_Business_Hrs__c from WFM_Country_Hours__c where Year_Month__c in :yearMonthList and Country_Name__c = :employee.Country_Name__c];
    }
    public static list<WFM_Country_Hours__c> GetEmployeeCountryHour(Employee_Details__c employee){
        
        return [select id, NBR_Business_Days_in_Month__c, Year_Month__c,Country_Name__c, country_name__r.PRA_Country_ID__c,  country_name__r.daily_Business_Hrs__c from WFM_Country_Hours__c where  Country_Name__c = :employee.Country_Name__c];
    }
    public static list<WFM_Country_Hours__c> GetCountryHour(string  countryRID, list<string> yearMonthList){
        
        return [select id, NBR_Business_Days_in_Month__c, Year_Month__c,Country_Name__c, country_name__r.PRA_Country_ID__c, country_name__r.daily_Business_Hrs__c from WFM_Country_Hours__c where Year_Month__c in :yearMonthList and Country_Name__c = :countryRID];
    }
    public static list<WFM_Country_Hours__c> GetCountryHour(string  countryRID){
        
        return [select id, NBR_Business_Days_in_Month__c, Year_Month__c,Country_Name__c, country_name__r.PRA_Country_ID__c,  country_name__r.daily_Business_Hrs__c from WFM_Country_Hours__c where  Country_Name__c = :countryRID];
    }
     public static void InsertCountryHourByCountryID( string countryRID, List<string> yearMonths){
        list<WFM_Country_Hours__c> newCountryHrs = new list <WFM_Country_Hours__c>();
        list<WFM_Country_Hours__c> countryHrs = GetCountryHour(countryRID, yearMonths );
        Map<string, string> cMapHr= new map<string, string>();
        if (countryHrs.size() > 0){
            for (WFM_Country_Hours__c hr :countryHrs ){
                cMapHr.put(hr.year_month__c, hr.id);
            }
            if (countryHrs.size() < yearMonths.size()){ // need insert country Hour with default  22 days
                
                for (string m:yearMonths){
                    if (cMapHr.get(m) == null){
                        WFM_Country_Hours__c  hrs = new WFM_Country_Hours__c(name=countryHrs[0].country_name__r.PRA_country_id__c+':'+m, NBR_Business_Days_in_Month__c=22,Country_Hour_Unique_Key__c=countryHrs[0].country_name__r.pra_country_id__c+':'+m, Country_Name__c = countryHrs[0].country_name__c,    Year_Month__c= m);
                        newCountryHrs.add(hrs);   
                    }           
                }
            }
        }
        else{
            countryHrs = GetCountryHour(countryRID);
            for (string m:yearMonths){
                    
                WFM_Country_Hours__c  hrs = new WFM_Country_Hours__c(name=countryHrs[0].country_name__r.PRA_country_id__c+':'+m, NBR_Business_Days_in_Month__c=22,Country_Hour_Unique_Key__c=countryHrs[0].country_name__r.PRA_country_id__c+':'+m, Country_Name__c = countryHrs[0].country_name__c,    Year_Month__c= m);
                newCountryHrs.add(hrs);   
                                
            }
        }
        if (newCountryHrs.size() > 0)
            insert newCountryHrs;
    }
    public static void InsertCountryHour(Employee_Details__c employee, List<string> yearMonths){
        list<WFM_Country_Hours__c> newCountryHrs = new list <WFM_Country_Hours__c>();
        list<WFM_Country_Hours__c> countryHrs = GetEmployeeCountryHour(employee, yearMonths );
        Map<string, string> cMapHr= new map<string, string>();
        if (countryHrs.size() > 0){
            for (WFM_Country_Hours__c hr :countryHrs ){
                cMapHr.put(hr.year_month__c, hr.id);
            }
            if (countryHrs.size() < yearMonths.size()){ // need insert country Hour with default  22 days
                
                for (string m:yearMonths){
                    if (cMapHr.get(m) == null){
                        WFM_Country_Hours__c  hrs = new WFM_Country_Hours__c(name=countryHrs[0].country_name__r.PRA_country_id__c+':'+m, NBR_Business_Days_in_Month__c=22,Country_Hour_Unique_Key__c=countryHrs[0].country_name__r.pra_country_id__c+':'+m, Country_Name__c = countryHrs[0].country_name__c,    Year_Month__c= m);
                        newCountryHrs.add(hrs);   
                    }           
                }
            }
        }
        else{
            countryHrs = GetEmployeeCountryHour(employee );
            for (string m:yearMonths){
                    
                WFM_Country_Hours__c  hrs = new WFM_Country_Hours__c(name=countryHrs[0].country_name__r.PRA_country_id__c+':'+m, NBR_Business_Days_in_Month__c=22,Country_Hour_Unique_Key__c=countryHrs[0].country_name__r.PRA_country_id__c+':'+m, Country_Name__c = countryHrs[0].country_name__c,    Year_Month__c= m);
                newCountryHrs.add(hrs);   
                                
            }
        }
        if (newCountryHrs.size() > 0)
            insert newCountryHrs;
    }
    /**** Location Hours***/
    public static void insertLocationHr(list<string> lstEmplID, string endMonth){
        list<Employee_Details__c> lstEmpl = RM_EmployeesDataAccessor.getEmployeeDetailByEmployeeRIDs(lstEmplID);
        list<string> lstLoc = RM_Tools.GetListStrfromListObject('Location_Code__c', lstEmpl);
         list<AggregateResult> lstEmplLoc =  [select Location_Code__c, max(year_month__c) lastMonth from WFM_Location_Hours__c where Location_Code__c in :lstLoc
            group by Location_Code__c];      
        system.debug('---------lstEmplLoc---------'+lstEmplLoc  );
        //insert country hour if endMonth < 18 month
        map<string, string> mapEmplLoc = new map<string, string>();
        for (AggregateResult arLoc : lstEmplLoc){
            string lastMonth = (string)arLoc.get('lastMonth');
            string LocCode = (string)arLoc.get('Location_Code__c');
            mapEmplLoc.put(LocCode, lastMonth);
        }
            
        list<WFM_Location_Hours__c> newLocHrs = new list <WFM_Location_Hours__c>();
        for (string l: lstLoc){
            if(mapEmplLoc.containsKey(l)){
                string lastMonth = mapEmplLoc.get(l);
                if (lastMonth < endMonth){
                    integer nEndMonth = RM_Tools.GetMonthNumFromNow(endMonth);
                    for (integer i = RM_Tools.GetMonthNumFromNow(lastMonth)+1; i <= nEndMonth; i++){
                        system.debug('---------RM_Tools.GetYearMonth(i)---------'+RM_Tools.GetYearMonth(i)  );
                        
                            
                         WFM_Location_Hours__c  hrs = new WFM_Location_Hours__c(Location_Code__c=l, NBR_Business_Day_In_Month__c=22,Loc_Hour_ExtID__c=l+':'+RM_Tools.GetYearMonth(i), Year_Month__c= RM_Tools.GetYearMonth(i));
                        newLocHrs.add(hrs); 
                        system.debug('---------hrs---------'+hrs  );
                                            
                    }               
                }               
            } 
            else{
                integer nEndMonth = RM_Tools.GetMonthNumFromNow(endMonth);
                for (integer i = 0; i <= nEndMonth; i++){
                    system.debug('---------RM_Tools.GetYearMonth(i)---------'+RM_Tools.GetYearMonth(i)  );
                    
                        
                     WFM_Location_Hours__c  hrs = new WFM_Location_Hours__c(Location_Code__c=l, NBR_Business_Day_In_Month__c=22,Loc_Hour_ExtID__c=l+':'+RM_Tools.GetYearMonth(i), Year_Month__c= RM_Tools.GetYearMonth(i));
                    newLocHrs.add(hrs); 
                    system.debug('---------hrs---------'+hrs  );
                                        
                }       
            }
            
        } 
        
        
            
        insert newLocHrs;
        system.debug('---------newLocHrs---------'+newLocHrs  );
    }
    
     public static list<WFM_Location_Hours__c> GetEmployeeLocationHour(Employee_Details__c employee, list<string> yearMonthList){
        
        return [select id, NBR_Business_Day_In_Month__c, Year_Month__c,location_code__c, Loc_Hour_ExtID__c 
            from WFM_Location_Hours__c 
            where Year_Month__c in :yearMonthList and location_code__c = :employee.location_code__c];
    }
     
  
    
  
    /*Location_Holiday_Schedule__c*/
    public static list<Location_Holiday_Schedule__c> getLstHolidayScheduleByLocationCodeDateRange(string lcode, Date startDate, Date endDate){
        return [SELECT Holiday_Date__c,Holiday_Name__c,Id,Location_Code__c,Loc_Holiday_Ext_ID__c 
            FROM Location_Holiday_Schedule__c where Location_Code__c=:lcode and Holiday_Date__c >=:startDate and Holiday_Date__c <=:endDate];
    }
    
  
    
    
   
    
    /************Modification Request*****************/   
   
  /*  public static list<WFM_Assignment_Mod_Req__c> GetModRequestByStatus(string status){
        return [SELECT Action_By__c, Action_Reason__c,Action_Type__c, employee_id__r.region__c,employee_id__r.last_name__c, employee_id__r.first_name__c, employee_id__c, employee_id__r.Job_Class_Desc__c,
        Change_Request__c,Comment__c,CreatedBy.name, createdby.Email, CreatedDate,Description__c,
        Id,LastModifiedById,LastModifiedDate,Modification_Type__c,Name,Reason__c,Request_Role__c,Status__c,
        Agree_By__r.name, Agree_Comment__c, Agree_DateTime__c, Agree_Role__c, Agree_Type__c, Allocations__r.lastModifiedByID, Allocations__c
        FROM WFM_Assignment_Mod_Req__c
        WHERE Status__c = :status ORDER BY CreatedDate ASC NULLS FIRST];
    }
    public static WFM_Assignment_Mod_Req__c GetModRequestByRID(string RID){
        list<WFM_Assignment_Mod_Req__c> mods = [SELECT Action_By__c, Action_Reason__c,Action_Type__c, 
        employee_id__r.region__c,employee_id__r.last_name__c, employee_id__r.first_name__c, employee_id__r.Job_Class_Desc__c,
        Change_Request__c,Comment__c,CreatedBy.name, createdby.Email, CreatedDate,Description__c,
        Id,LastModifiedById,LastModifiedDate,Modification_Type__c,Name,Reason__c,Request_Role__c,Status__c,
        Agree_By__r.name, Agree_By__r.email,Agree_Comment__c, Agree_DateTime__c, Agree_Role__c, Agree_Type__c, Allocations__c,
        (select Detail_Desc__c,  Detail_Type__c, Detail_Value__c from AMR_Details__r order by name)
        FROM WFM_Assignment_Mod_Req__c WHERE id = :RID];
        if (mods.size()> 0)
            return mods[0];
        else
            return null;
    }
    public static list<WFM_Assignment_Mod_Req__c> GetModRequestByAllRIDStatus(string RID, string status){
        return [SELECT Action_By__c, Action_Reason__c,Action_Type__c, 
        employee_id__r.region__c,employee_id__r.last_name__c, employee_id__r.first_name__c, employee_id__r.Job_Class_Desc__c,
        Change_Request__c,Comment__c,CreatedBy.name, createdby.Email, CreatedDate,Description__c,
        Id,LastModifiedById,LastModifiedDate,Modification_Type__c,Name,Reason__c,Request_Role__c,Status__c,
        Agree_By__r.name, Agree_By__r.email,Agree_Comment__c, Agree_DateTime__c, Agree_Role__c, Agree_Type__c,
        (select Detail_Desc__c,  Detail_Type__c, Detail_Value__c from AMR_Details__r order by name)
        FROM WFM_Assignment_Mod_Req__c WHERE Allocations__c = :RID and status__c = :status];
    }
    public static list<WFM_Assignment_Mod_Req__c> GetModRequestsByAllRIDsStatus(Set<ID> AlloRIDs, string status){
        return [SELECT Change_Request__c,Comment__c,CreatedBy.name, createdby.Email, CreatedDate,Description__c,
        Id,LastModifiedById,LastModifiedDate,Modification_Type__c,Name,Reason__c,Request_Role__c,Status__c,
        Agree_By__r.name, Agree_Comment__c, Agree_DateTime__c, Agree_Role__c, Agree_Type__c, Allocations__c     
        FROM WFM_Assignment_Mod_Req__c WHERE Allocations__c = :AlloRIDs and status__c = :status];
    }
  
    public static list<WFM_employee_Allocations__c> GetAllocationByModRID(string RID){
        return [select id, project_id__r.name,project_id__r.client_id__c, name,Is_lead__c, Is_IEDR__c, Is_Unblinded__c, project_buf_code__c, project_function__c, project_country__c, LastModifiedDate 
        from wfm_employee_allocations__c where id in (
        SELECT Allocations__c FROM WFM_Assignment_Mod_Req__c WHERE id = :RID)];
    }
    public static list<WFM_employee_Allocations__c> GetAllocationByModRIDs(list<string> RIDs){
        return [select id, project_id__r.name,project_id__r.client_id__c, name,Is_lead__c, Is_IEDR__c, Is_Unblinded__c, project_buf_code__c, project_function__c, project_country__c 
        from wfm_employee_allocations__c where id in (
        SELECT Allocations__c FROM WFM_Assignment_Mod_Req__c WHERE id in :RIDs)];
    }*/
  
   
    
    

   
   
    /****************************
    Saved Search
    ****************************/
    public static list<Root_Folder__c> GetRootFolder(){
        return [select Name, Id from Root_Folder__c limit 1000];
    }  
    public static list<Sub_Folder__c> GetCustomSubFolderList(string rId, string userId){        
        return  [select Name, Id, Root__r.name from Sub_Folder__c where Root__c != null and Root__c = :rId and User__c = :userId and Custom__c = true order by name];
    }
    public static list<Sub_Folder__c> GetDefaultSubFolderList(string rId, string userId){        
        return [select Name, Id, Root__r.name from Sub_Folder__c where Root__c != null and Root__c = :rId and User__c = :userId and Custom__c = false order by name];       
    }
    public static Sub_Folder__c GetSubFolderbyID(string subFolderId){
        return [ select Id, name, Custom__c, root__c from Sub_Folder__c where id= :subFolderId];
    }
    public static Sub_Folder__c GetSubFolderID(string subFolder, string path, string userID){
        return [ select Id, name from Sub_Folder__c where Name= :subFolder and root__r.name=:path and User__c = :userId];
    }
    public static list<Saved_Search__c> getSavedSearchList(string subID){
        return [select Name, Id, search_type__c from Saved_Search__c where Sub__c != null and Sub__c = :subID  order by name];
    }
    public static list<Saved_Search__c> getSavedSearchName(string searchName, string userID, string subID){
        return [select Id, Name from Saved_Search__c where Name = :searchName and User__c = :userId and sub__c=:subID];
    }
    public static Saved_Search__c getSavedSearch(string searchName, string userID, string subID){
        return [select Id, Name from Saved_Search__c where Name = :searchName and User__c = :userId and sub__c=:subID];
    }
    public static Saved_Search__c getSavedSearchByID(string saveID){
        return [select Id, Name, sub__c from Saved_Search__c where id=:saveID];
    }
    public static list<Search_Detail__c> getSavedSearchDetail(string saveSearchID){
        return [select Name, Search_Value__c from Search_Detail__c where File__c = :saveSearchID];
    }       
    public static Saved_Search__c getSavedSearchwithRoot(string saveSearchID){
        return [select Id, Name, Sub__r.Root__r.name from Saved_Search__c where id = :saveSearchID];
    }
    
        
    
    /* Job Class desc */
    /**
	* @author 	Min Chen
	* @date 	July 2014
	* @description Return list of job_class_desc  with input list of job_class_desc ID
	*/ 
    public static list<Job_Class_Desc__c> getJobClassDescByListRID(list<string> jcRIDs){
        
        return [select id, name from  Job_Class_Desc__c where id in :jcRIDs order by name limit 1000];           
    }
    public static list<string> getClinicalJobClass(){
        list <string> jobClass = new list<string>();
        for (Job_Class_Desc__c j: [select name from  Job_Class_Desc__c where IsClinical__c = true order by name limit 1000])
            jobClass.add(j.name);
        return jobClass;
    }
    
  
     public static list<string> getJobClassDescByBU( list<string> lstBU){
        system.debug('---------lstBU---------' + lstBU);
        list<string> lstJC = new list<string>();
        transient set<String> setItems=new set<string>();    
        
        
        if (lstBU.size() > 0 && lstBU[0] != ''){
            List<string> lstGroup = new List<string>();
            for (WFM_BU_Group_Mapping__c m : [select group_name__c from WFM_BU_Group_Mapping__c where PRA_Business_Unit__r.name in :lstBU]){
                lstGroup.add(m.group_name__c);
            }
            if (lstGroup.size() > 0 && lstGroup[0] != ''){
                for(WFM_JC_Group_Mapping__c temp:[SELECT Group_Name__c,Job_Class_Desc__r.name FROM WFM_JC_Group_Mapping__c WHERE Group_Name__c  in: lstGroup order by Job_Class_Desc__r.name asc])
                    
                     setItems.add(temp.Job_Class_Desc__r.name);
            }
             lstJC.addAll(setItems);
             return lstJC;
        }
        else
        {   
            return getClinicalJobClass();
        }   
    }  
     public static list<string> getJobClassDescByBU( string strBU){
        system.debug('---------strBU---------' + strBU);
        list<string> lstJC = new list<string>();
        transient set<String> setItems=new set<string>();    
        
        
        if (strBU != ''){
            List<string> lstGroup = new List<string>();
            for (WFM_BU_Group_Mapping__c m : [select group_name__c from WFM_BU_Group_Mapping__c where PRA_Business_Unit__r.name = :strBU]){
                lstGroup.add(m.group_name__c);
            }
            if (lstGroup.size() > 0 && lstGroup[0] != ''){
                for(WFM_JC_Group_Mapping__c temp:[SELECT Group_Name__c,Job_Class_Desc__r.name FROM WFM_JC_Group_Mapping__c WHERE Group_Name__c  in: lstGroup order by Job_Class_Desc__r.name asc])
                    
                     setItems.add(temp.Job_Class_Desc__r.name);
            }
             lstJC.addAll(setItems);
             return lstJC;
        }
        else
        {   
            return getClinicalJobClass();
        }   
    }
   /* public static list<Job_Class_Desc__c> getJobClassDescByRID(list<string> rIDs){
    	 return   [select id,  name from  Job_Class_Desc__c where id in :rIDs];
    	 
    }*/  
   
    /************************************
    Help
    ************************************/
    public static list<Help__c> GetHelpContent(string helpName){
        return [select id, name, Content__c from Help__c where Page_Name__c=:helpName ];
    }
    /***********************
    User, profile
    ************************/
    public static user GetUserInfo(string userID){
       list <user> u = [select id, email from user where id = :userID];
       if (u.size() > 0)
        return u[0];
       else
        return null;
    }
     public static user GetUserInfobyEmail(string email){
       list <user> u = [select id, email from user where email = :email];
       if (u.size() > 0)
        return u[0];
       else
        return null;
    }
    public static list<user> GetUsersInfo(set <string> userIDs){
        return [select id, name, email, EmployeeNumber, lastname, firstname from user where id = :userIDs];
    }
    
    public static Profile GetUserProfileInfo(string userProfileID){
      return [SELECT name FROM Profile WHERE id=:userProfileID];
    }
    public static list<string> GetRMGroupfromUserID(string userID){
        List<GroupMember> lstGrpMember =  [Select GroupID From GroupMember where UserOrGroupId = :userId];
        if (lstGrpMember.size() > 0)
        {   
            List<ID> lstGrpID = new List<ID>();
            for (GroupMember g : lstGrpMember)
                lstGrpID.add(g.GroupID);      
            List<Group> lstGrp = [Select DeveloperName from Group where ID in :lstGrpID];
            
            list<string> lstGrpName = new list<string>();
            for (Group gr : lstGrp){
                if (gr.DeveloperName.startsWith('RM_'))
                    lstGrpName.add(gr.DeveloperName);
            }
            return lstGrpName;
        }
        return null;
    }
    public static list<WFM_BU_Group_Mapping__c> getBUGroupMapping(list<string> lstGrpName){
        return  [SELECT Group_Name__c,PRA_Business_Unit__r.name, Is_Require_ReqID__c, Default_Unit__c FROM WFM_BU_Group_Mapping__c where Group_Name__c in :lstGrpName];
    }
     public static list<WFM_BU_Group_Mapping__c> getBUGroupMappingBysetGroupName(set<string> grpNames){
        return  [SELECT Group_Name__c,PRA_Business_Unit__r.name, Is_Require_ReqID__c, Default_Unit__c FROM WFM_BU_Group_Mapping__c where Group_Name__c in :grpNames];
    }
    public static list<WFM_BU_Group_Mapping__c> getAllBUinGroupMapping(){
        return  [SELECT Group_Name__c,PRA_Business_Unit__r.name, Is_Require_ReqID__c, Default_Unit__c FROM WFM_BU_Group_Mapping__c order by PRA_Business_Unit__r.name ];
    }
    public static list<WFM_JC_Group_Mapping__c> getManagedRoleByGroup(list<string> lstGrpName){
        return [SELECT Group_Name__c,Job_Class_Desc__r.name FROM WFM_JC_Group_Mapping__c WHERE Group_Name__c  in: lstGrpName order by Job_Class_Desc__r.name asc];
    }
   	
    
  
    public static string GetConstant(string constName){
        list<RM_ConstantSetting__c> lstCon = [SELECT Name,Value__c FROM RM_ConstantSetting__c where name=:constName];
            
        if (lstCon.size()> 0)
            return lstCon[0].value__c;
        else
            return '';
        /*RM_ConstantSetting__c r = RM_ConstantSetting__c.getValues(constName);
        if(r != null)
            return r.value__c;
        else
            return '';*/
    }
    /************
    BufCode
    ***************/
    
   
    
    public static boolean validateBufcode(string buf){
        list<buf_code__c> lstBuf = [SELECT Name FROM BUF_Code__c WHERE Function_Code__r.is_rm__c = true and name = :buf];
        if (lstBuf.size()> 0)
            return true;
        else 
            return false;
    }
    public static list<buf_code__c> getBufcodesList(set<string> stBuf){
        //this method only validate if bufcode exist or not
        return [SELECT Name FROM BUF_Code__c WHERE name in :stBuf];    
    }
    /* country BU*/
    public static list<Country_BusinessUnit__c> getCountryBUbyCombo(set<string> lstCountryBU){
        return [select Business_Unit_Name__c, Country_Name__c, Country_BU_ID__c from Country_BusinessUnit__c where Country_BU_ID__c = :lstCountryBU order by Country_Name__c asc];
    }
    
    /*  Notification */
    public static list<WFM_Notification__c> getAllNotification(){
        return [Select Id,ProjectIdPlusSponsor__c, createddate, WFM_Project__c, Change_Type__c, Old_Value__c, New_Value__c, AgeIndays__c from WFM_Notification__c where CreatedDate = LAST_N_DAYS:30 order by LastModifiedDate desc limit 1000];
    }
    public static list<WFM_Notification__c> getUnReadNotification(list<string> lstID){
        return [Select Id,ProjectIdPlusSponsor__c, createddate, WFM_Project__c, Change_Type__c, Old_Value__c, New_Value__c, AgeIndays__c 
        from WFM_Notification__c where id not in :lstID and CreatedDate = LAST_N_DAYS:30 order by LastModifiedDate desc limit 1000];
    }
    public static list<WFM_Notification_view_log__c> getListNotificationViewLog(list<string> ListOfGroupNames, string UserID){
        if (ListOfGroupNames == null)
            return [Select Id, Group_Name__c,User__c , Notification__c          
                    from WFM_Notification_view_log__c where User__c =: UserId]; 
        else{
            if (ListOfGroupNames.size() == 1 )
                return [Select Id, Group_Name__c,User__c , Notification__c
                    from WFM_Notification_view_log__c where Group_Name__c in : ListOfGroupNames or User__c =: UserId];
            else            
                return [Select Id, Group_Name__c,User__c , Notification__c          
                    from WFM_Notification_view_log__c where User__c =: UserId];
        }
    } 
    /* CV Integration*/
    /**
	* @author 	Min Chen
	* @date 	July 2014
	* @description Return list of WFM_JC_Group_Mapping__c for list of Job_Class_Desc record ID 
	*/ 
    /*public static list<WFM_JC_Group_Mapping__c> getJCBUforUpdateCV(){
        return [SELECT id, Group_Name__c,Job_Class_Desc__r.name,Job_Class_Desc__c, BU_Group_Mapping__r.PRA_Business_Unit__r.name
         FROM WFM_JC_Group_Mapping__c 
         WHERE Is_UpdateCV__c = true
          order by Job_Class_Desc__r.name asc];
    }*/
      /**
	* @author 	Min Chen
	* @date 	July 2014
	* @description Return list of WFM_JC_Group_Mapping__c for list of Job_Class_Desc record ID 
	*/ 
     /*public static list<WFM_JC_Group_Mapping__c> getJCBUforUpdateCVByJCGroupID(list<string> lstJCGroupRID){
        return [SELECT id, Group_Name__c,Job_Class_Desc__r.name,Job_Class_Desc__c, BU_Group_Mapping__r.PRA_Business_Unit__r.name
         FROM WFM_JC_Group_Mapping__c 
         WHERE id in: lstJCGroupRID
          order by Job_Class_Desc__r.name asc];
    }*/
    /**
	* @author 	Min Chen
	* @date 	July 2014
	* @description Return list of WFM_Queue__c with input Type and status
	*/ 
    /*public static list<WFM_Queue__c> getProcessingQueueByStatusType(string queueType, string status){
        return [select id, sf_id__c, status__c, type__c,SF_ID_Object__c, Action__c  
            from WFM_Queue__c where status__c= :status and type__c = :queueType];
    }*/
     /**
	* @author 	Min Chen
	* @date 	July 2014
	* @description Return list of WFM_Queue__c with input Type and status
	*/ 
    /*public static list<WFM_Queue__c> getProcessingQueueByStatusTypeAction(string queueType, string status, string action){
        return [select id, sf_id__c, status__c, type__c,SF_ID_Object__c, Action__c  
            from WFM_Queue__c where status__c= :status and type__c = :queueType and action__c = :action];
    }*/
    /**
	* @author 	Min Chen
	* @date 	July 2014
	* @description Return list of WFM_Queue__c with input Type and status and object
	*/ 
    /*public static list<WFM_Queue__c> getProcessingQueueByStatusTypeObject(string objectName, string queueType, string status){
        return [select id, sf_id__c, status__c, type__c, SF_ID_Object__c, Action__c 
            from WFM_Queue__c where SF_ID_Object__c=:objectName and status__c= :status and type__c = :queueType];
    }*/
      /**
	* @author 	Min Chen
	* @date 	July 2014
	* @description Return list of WFM_Queue__c with input object Type and ID
	*/ 
    /*public static list<WFM_Queue__c> getProcessingQueueByobjectTypeID(string objectName, list<string> objectID){
        return [select id, sf_id__c, status__c, type__c, SF_ID_Object__c, Action__c 
            from WFM_Queue__c where SF_ID_Object__c=:objectName and SF_ID__c in :objectID];
    } */                      
}