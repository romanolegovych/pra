/**
* @author Grace Guo
* @date 15-Mar-2015
* @description this is test class for scenario related classes
*/

@isTest
private class PBB_ServiceImpactQuestionsControllerTest {   
    static testMethod void testPBB_ServiceImpactQuestionsController(){      
        test.startTest();
              PBB_TestUtils tu = new PBB_TestUtils ();
              tu.country =tu.createCountryAttributes();
              tu.siqn =tu.createSIQNAttributes();
              tu.siqs = tu.createSIQSAttributes(tu.siqn.id);
              tu.sir = tu.createSIRAttributes();
              //tu.siqs.Countries__c = 'Test India';
              tu.siqs.Question_Applies_to__c = 'Country - Specific';
              tu.siqs.Question_Type__c = 'Single response for All Countries';
              update tu.siqs;
              PageReference pageRef = Page.PBB_Service_Impact_Questions;
              Test.setCurrentPage(pageRef);
              ApexPages.currentPage().getParameters().put('id', tu.siqs.Id); 
              ApexPages.StandardController sc = new ApexPages.StandardController(tu.siqs);
              
              PBB_ServiceImpactQuestionsController siQuestionsController = new PBB_ServiceImpactQuestionsController(sc);
              
              siQuestionsController.siq.Question_Applies_to__c = 'Country - All';  
              siQuestionsController.saveSIQuestions();
              system.assertEquals('Country - All',siQuestionsController.siq.Question_Applies_to__c);
              
              siQuestionsController.siq.Question_Applies_to__c = 'Country - Specific';              
              siQuestionsController.saveSIQuestions();
              system.assertEquals('Country - Specific',siQuestionsController.siq.Question_Applies_to__c);
              
              siQuestionsController.selectedCountries.add(new SelectOption('Test India','Test India')); 
              siQuestionsController.siq.Status__c = 'Approved';  
              siQuestionsController.saveSIQuestions();              
              system.assertEquals(siQuestionsController.siq.Status__c,'Approved');              
              siQuestionsController.cancel();
              
              ApexPages.currentPage().getParameters().put('clone', '1');              
              PBB_ServiceImpactQuestionsController siQuestionsController1 = new PBB_ServiceImpactQuestionsController(sc);
              system.assertEquals('1',ApexPages.currentPage().getParameters().get('clone'));
              siQuestionsController1.saveSIQuestions();
          test.stopTest();      
     } 
}