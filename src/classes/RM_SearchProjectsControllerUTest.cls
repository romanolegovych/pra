/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_SearchProjectsControllerUTest {
    static WFM_Client__c client;
    static WFM_Contract__c contract;
    static WFM_Project__c project;
    static WFM_Project__c projectB;
    static WFM_Protocol__c prot;
    static list<WFM_Site_Detail__c> lstSite;
    static list<WFM_Therapeutic__c> lstThera;
    static list<WFM_Phase__c> lstPhase;
    static Root_Folder__c root1;
    static Sub_Folder__c sub1;
    
    static void init(){
        client = new WFM_Client__c(name='TestClient', Client_Name__c='TestClinet_Name', Client_Unique_Key__c='TestClient');
        insert client;
        contract = new WFM_Contract__c(name='TestContract', Contract_Status__c='AA', Client_ID__c=client.id, Contract_Unique_Key__c='TestContract');
        insert contract;
        project = new WFM_Project__c(name='TestProject', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProject', project_status__c ='RM',  Status_Desc__c='Active',
        Currency_Code__c='USD', Project_Start_Date__c=date.parse('12/27/2009'), Project_End_Date__c=date.parse('1/2/2014'));
        insert project;
        projectB = new WFM_Project__c(name='TestProjectB', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProjectB', project_status__c ='BB',  Status_Desc__c='Bid', Project_Start_Date__c=date.parse('12/27/2013'),
        Bid_Defense_Date__c = date.parse('10/1/2013'));
        insert projectB;
       
        prot = new WFM_Protocol__c(name='TestProtocol', Therapeutic_Area__c='TestThera', Phase__c='II', Protocal_Unique_Key__c='TestProtocol',Project_ID__c=project.id,
            DB_Lock_DT__c=date.parse('12/27/2009'));
        insert prot;
        lstSite = new list<WFM_Site_Detail__c>();
        for (integer i = 0; i < 5; i++){
            WFM_Site_Detail__c site = new WFM_Site_Detail__c(name='site'+string.valueOf(i), Project_Protocol__c=prot.id, Site_ID__c='site'+string.valueOf(i),Country__c='United State',Region_Name__c='North American',
            CRF_Pages_Expected__c=i*10, Total_Forecasted_Visits__c=i*2,  Status__c='Test', Actual_Subj_Enroll__c=i*5);
            lstSite.add(site);
        }
        insert lstSite;
        //look up table
        lstThera = new list <WFM_Therapeutic__c>();
        for (integer i = 0; i < 5; i++){
            WFM_Therapeutic__c t = new WFM_Therapeutic__c(name='test'+string.valueOf(i), Indication_Group__c='Group'+string.valueOf(i),Primary_Indication__c='Ind'+string.Valueof(i) );
            lstThera.add(t);
        }
        insert lstThera;
        lstPhase = new list<WFM_Phase__c>();
        for(integer i = 0; i<5; i++){
            WFM_Phase__c p = new WFM_Phase__c(name='Phase'+string.valueOf(i));
            lstPhase.add(p);
        }
        insert lstPhase;
    }   
    static testMethod void TestDropDowns() {
        init();
        Test.startTest();
        
        RM_SearchProjectsController projController = new RM_SearchProjectsController();   
        List<SelectOption> options = projController.getSearchOnItems();
        //system.debug('---options---' + options[0].getValue());
        system.assert(options[0].getValue() == 'Project ID');
      
        list<SelectOption> option1 = projController.getprojectStatus();
        system.assert(option1[1].getValue() == 'Bid');
        
        list<SelectOption> option2 = projController.getTheropeticAreas();
        system.debug('---option2---' + option2);
        system.assert(option2[0].getValue() == '');
        
        list<SelectOption> option3 = projController.getphase();
        system.debug('---option3---' + option3);
        system.assert(option3[0].getValue() == '');
        
     
        projController.searchOn = 'Project ID';
        PageReference page = projController.SearchType();
        system.assert(projController.strSearchText == 'Enter a project ID');
        projController.searchOn = 'Client/Sponsor Name';
        PageReference page1 = projController.SearchType();
        system.assert(projController.strSearchText == 'Enter a client Name');
        
        PageReference pageExp = projController.export();
        system.assert(pageExp.getUrl() == '/apex/RM_ExcelProjectList');
        
        list<SelectOption> optionFolder = projController.getResourceFolders();
        system.assert(optionFolder.size() > 0);
        Test.stopTest();   
    }
    static testMethod void TestTextSearch(){
        init();
        Test.startTest();
        RM_SearchProjectsController projController = new RM_SearchProjectsController();
        projController.strSearchText='Test';
        projController.searchOn = 'Project ID';
        projController.startFDate ='';
        projController.startEDate='';
        projController.toEDate='';
        projController.toFDate='';
        projController.bidEDate='';
        projController.bidFDate='';
        projController.selectedStatus=null;
        projController.search();
        //list<projectResult> lstResult = projController.searchResult;
        system.assert(projController.searchResult[0].projectID=='TestProject');
        projController.strSearchText='TestProject';
        projController.selSearch = 'TestProject';   
        projController.search();
        system.assert(projController.searchResult[0].projectID=='TestProject');
        projController.searchOn = 'Client';
        projController.strSearchText='Test';
        projController.selSearch = 'TestProject';   
        projController.search();
        system.assert(projController.searchResult[0].projectID=='TestProject');
        projController.strSearchText='TestClient';
        projController.selSearch = 'TestClient';   
        projController.search();
        system.assert(projController.searchResult[0].projectID=='TestProject');
        Test.stopTest(); 
    }
   
    static testMethod void TestSearchProtocol(){
        init();
        Test.startTest();
        RM_SearchProjectsController projController = new RM_SearchProjectsController();
        projController.startFDate ='';
        projController.startEDate='';
        projController.toEDate='';
        projController.toFDate='';
        projController.bidEDate='';
        projController.bidFDate='';
        projController.selectedStatus=null;
        projController.bProtocol = true;
        list<string> lsphase = new list<string>();
        lsphase.add('II');
        projController.selectedPhase = lsphase;
        projController.strTheropeticArea = 'TestThera';
        projController.search();
        system.assert(projController.searchResult[0].projectID=='TestProject');
        Test.stopTest(); 
    }
    static testMethod void TestSearchStatus(){
        init();
        Test.startTest();
        RM_SearchProjectsController projController = new RM_SearchProjectsController();
        projController.startFDate ='';
        projController.startEDate='';
        projController.toEDate='';
        projController.toFDate='';
        projController.bidEDate='';
        projController.bidFDate='';
        projController.bProtocol = true;
        projController.strTheropeticArea = 'TestThera';
        list<string> lsStatus = new list<string>();
        lsStatus.add('Active');
        projController.selectedStatus = lsStatus;
       
        projController.search();
        system.assert(projController.searchResult.size()>0);
        Test.stopTest(); 
    }
    static testMethod void TestSearchStatusError(){
        init();
        Test.startTest();
        RM_SearchProjectsController projController = new RM_SearchProjectsController();
        projController.startFDate ='';
        projController.startEDate='';
        projController.toEDate='';
        projController.toFDate='';
        projController.bidEDate='';
        projController.bidFDate='';
        
        list<string> lsStatus = new list<string>();
        lsStatus.add('Active');
        projController.selectedStatus = lsStatus;
       
        projController.search();
        system.assert(projController.searchResult.size()  > 0);
        Test.stopTest(); 
    }
    static testMethod void TestSearchDate(){
        init();
        Test.startTest();
        RM_SearchProjectsController projController = new RM_SearchProjectsController();
        projController.startFDate ='12/1/2009';
        projController.startEDate='1/1/2012';
        projController.toEDate='1/1/2015';
        projController.toFDate='1/1/2013';
        projController.bidEDate='';
        projController.bidFDate=''; 
        projController.bProtocol = true;
        projController.strTheropeticArea = 'TestThera';
       
        projController.search();
        system.assert(projController.searchResult.size()>0); 
        Test.stopTest(); 
    }
    static testMethod void TestSearchBidDefenseDate(){
        init();
        Test.startTest();
        RM_SearchProjectsController projController = new RM_SearchProjectsController();
        projController.startFDate ='';
        projController.startEDate='';
        projController.toEDate='';
        projController.toFDate='';
        projController.bidEDate='11/1/2013';
        projController.bidFDate='8/1/2013'; 
        projController.bProtocol = false;
       
        projController.search();
        system.assert(projController.searchResult.size()>0);
        Test.stopTest(); 
    }
    static void initSavedSearch(){
        
        //create and query fake roots//
        root1 = new Root_Folder__c(Name = 'root1');      
        insert root1;
        //create and query fake subs//
        sub1 = new Sub_Folder__c(Name = 'sub1', Root__c = root1.id, Custom__c = false, User__c = UserInfo.getUserID(), Sub_Folder_Unique_Key__c='sub1'+root1.id + UserInfo.getUserID());
        
        insert sub1; 
       
    }
    static testMethod void TestSave(){
        initSavedSearch();
        //create default subfolder
        Test.startTest();
        RM_SearchProjectsController projController = new RM_SearchProjectsController(); 
        
        
        projController.saveSubFolder = sub1.id;
        projController.searchName = 'Testtest';
        projController.selectedPhase = new list<string>();
        projController.selectedPhase.add('II');
        projController.selectedStatus = new list<string>();
        projController.selectedStatus.add('Active');
        projController.strTheropeticArea = 'TestThera';
        projController.startFDate ='12/1/2009';
        projController.startEDate='1/1/2012';
        projController.toEDate='1/1/2015';
        projController.toFDate='1/1/2013';
            
        projController.strSearchText='Test';
        projController.searchOn = 'Project ID';
        PageReference page= projController.saveSearch();
        Saved_Search__c searchName = RM_OtherService.getSavedSearch(projController.searchName,UserInfo.getUserId(), projController.saveSubFolder);
        list<Search_Detail__c>  details = RM_OtherService.getSavedSearchDetail(searchName.id);
        system.assert(searchName.name == projController.searchName);
        system.assert(details[0].name == projController.searchOn);
        //test editSearch
        PageReference pageRef = new PageReference('/apex/RM_SearchProjects?cid=' + searchName.id);
     
        Test.setCurrentPage(pageRef);
        RM_SearchProjectsController projController1 = new RM_SearchProjectsController(); 
        system.assert(projController1.searchOn=='Project ID');
        system.assert(RM_SearchProjectsController.CheckSaveSearch(projController.searchName, projController.saveSubFolder) == true);
        
        Test.stopTest(); 
    }
    static testMethod void TestSaveBid(){
        initSavedSearch();
        Test.startTest();
        RM_SearchProjectsController projController = new RM_SearchProjectsController(); 
        
        
        projController.saveSubFolder = sub1.id;
        projController.searchName = 'Testtest';
        projController.bidEDate='11/1/2013';
        projController.bidFDate='8/1/2013'; 
            
        projController.strSearchText='Test';
        projController.searchOn = 'Client';
        PageReference page= projController.saveSearch();
        Saved_Search__c searchName = RM_OtherService.getSavedSearch(projController.searchName,UserInfo.getUserId(), projController.saveSubFolder );
        list<Search_Detail__c>  details = RM_OtherService.getSavedSearchDetail(searchName.id);
        system.assert(searchName.name == projController.searchName);
        system.assert(details[0].name == projController.searchOn);
        //test editSearch
        PageReference pageRef = new PageReference('/apex/RM_SearchProjects?cid=' + searchName.id);
     
        Test.setCurrentPage(pageRef);
        RM_SearchProjectsController projController1 = new RM_SearchProjectsController(); 
        system.assert(projController1.bidEDate=='11/1/2013');
        Test.stopTest(); 
    }
    
}