public with sharing class PIT_IssuesHomeController {
        public List<SelectOption>         viewOptionsList   {get;set;}
        public string                           SelectedView      {get;set;}
        public List<SelectOption>         recentOptionsList {get;set;}
        public string                           SelectedRecent    {get;set;}
        public List<Issue__c>             issueResults      {get;set;}
        public string                           searchTerm                {get;set;}
        public string                           delIssueId                {get;set;} 
        // Variables for sorting the results
        private String sortDirection = 'ASC';
        private String sortExp           = 'name';
        
        public String sortExpression {
                get     { return sortExp; }
                set {
                        //if the column is clicked on then switch between Ascending and Descending modes
                        if (value == sortExp) {
                                sortDirection = (sortDirection == 'ASC')? 'DESC' : 'ASC';
                        } else {
                                sortDirection = 'ASC';
                        }
                        sortExp = value;
                }
        }
        
        public String getSortDirection() {
                //if not column is selected 
                if (sortExpression == null || sortExpression == '') {
                        return 'ASC';
                } else {
                        return sortDirection;
                }
        }
        
        public void setSortDirection(String value) {  
                sortDirection = value;
        }
        

        // Constructor of the controller
        public PIT_IssuesHomeController() {
                issueResults = new List<Issue__c>();
                searchTerm = '';
                buildViewOptionsList();
                buildRecentOptionsList();
                SelectedView   = getCookieValue('SelectedView'); 
                SelectedRecent = getCookieValue('SelectedRecent'); 
                searchTerm         = getCookievalue('searchTerm');
                SelectedView   = (SelectedView=='empty')?   'MyAssigned' : SelectedView;
                SelectedRecent = (SelectedRecent=='empty')? 'Created'    : SelectedRecent;
                searchTerm         = (searchTerm=='empty')?             ''                       : searchTerm;
                filterIssues();
        }
        
        // used to create the view options
        public void buildViewOptionsList() {
                viewOptionsList = new List<SelectOption>();
                viewOptionsList.add(new SelectOption('All',        'All Issues') );
                viewOptionsList.add(new SelectOption('MyCreated',  'My Created Issues') );
                viewOptionsList.add(new SelectOption('MyAssigned', 'My Assigned Issues') );
                viewOptionsList.add(new SelectOption('Late',       'Late Issues') );
                viewOptionsList.add(new SelectOption('Recent',     'Recent Issues') );
        }       
        
        // used to create the recent view options
        public void buildRecentOptionsList() {
                recentOptionsList = new List<SelectOption>();
                recentOptionsList.add(new SelectOption('Created',       'Recently Created') );
                recentOptionsList.add(new SelectOption('Modified',  'Recently Modified') );
                //recentOptionsList.add(new SelectOption('Viewed',    'Recently Viewed') );
        }       
        
        
        // used to filter the Issues depends on the view selection
        public void filterIssues() {
                setCookie ('SelectedView', SelectedView); 
                issueResults = new List<Issue__c>();
                // Create the generic soql
                String soql = 'SELECT  Id, Name, Project__c, Project__r.Name, Issue_Type__c, Status__c, Category__c, Action_Taken__c, Date_Resolved__c, Assign_To__c, Assign_To__r.Name, Escalate_To__c, Escalate_To__r.Name    FROM Issue__c ';
                // Add the WHERE clause depends on the selection
                if(SelectedView == 'MyCreated') { // My Created Issues
                        soql += 'WHERE CreatedById = \'' + UserInfo.getUserId() + '\'';
                } else if(SelectedView == 'MyAssigned') { // My Assigned Issues
                        soql += 'WHERE Assign_To__c = \'' + UserInfo.getUserId() + '\'';
                } else if(SelectedView == 'Late') { // Late Issues
                        soql += 'WHERE Status__c != \'Resolved\' AND Date_Resolved__c < TODAY'; 
                } 
                // Add the search Criteria
                setCookie ('searchTerm', searchTerm); 
                if(searchTerm.length() > 1) {
                        String tmpSearchTerm = '%' + searchTerm + '%';
                        if(SelectedView == 'All' || SelectedView == 'Recent') {
                                soql += ' WHERE (Name LIKE :tmpSearchTerm OR ';
                        } else {
                                soql += ' AND (Name LIKE :tmpSearchTerm OR ';
                        }
                        soql += ' Project__r.Name LIKE :tmpSearchTerm OR ';
                        soql += ' Issue_Type__c LIKE :tmpSearchTerm OR ';
                        soql += ' Status__c LIKE :tmpSearchTerm OR ';
                        soql += ' Category__c LIKE :tmpSearchTerm OR ';
                        soql += ' Action_Taken__c LIKE :tmpSearchTerm OR ';
                        soql += ' Assign_To__r.Name LIKE :tmpSearchTerm OR ';
                        soql += ' Escalate_To__r.Name LIKE :tmpSearchTerm)';
                }
                // Add the sort
                if(SelectedView == 'Recent') { // Recent Issues
                        setCookie ('SelectedRecent', SelectedRecent);
                        if(SelectedRecent == 'Created') { // Recently Created
                                soql += ' order by CreatedDate DESC LIMIT 10';
                        } else if(SelectedRecent == 'Modified') { // Recently Modified
                                soql += ' order by  LastModifiedDate DESC LIMIT 10';
                        } //else if(SelectedRecent == 'Viewed') { // My Assigned Issues
                                //soql += ' order by LastViewedDate DESC LIMIT 10';
                        //}
                } else {
                        string sortFullExp = sortExpression  + ' ' + sortDirection;
                        soql += ' order by ' + sortFullExp;
                }
                // Filter the database
                try {
                        issueResults = database.query(soql);
                        if(SelectedView == 'Recent') {  
                                sortList(issueResults, sortExpression, sortDirection);
                        }
                } catch (exception e) {}
        }
        
        
        
        
        public static void sortList(List<Sobject> items, String sortField, String order){
       List<Sobject> resultList = new List<Sobject>();
        //Create a map that can be used for sorting
       Map<object, List<Sobject>> objectMap = new Map<object, List<Sobject>>();
       for(Sobject ob : items){
                if(objectMap.get(ob.get(sortField)) == null){  // For non Sobject use obj.ProperyName
                    objectMap.put(ob.get(sortField), new List<Sobject>());
                }
                objectMap.get(ob.get(sortField)).add(ob);
        }      
        //Sort the keys
        List<object> keys = new List<object>(objectMap.keySet());
        keys.sort();
        for(object key : keys){
            resultList.addAll(objectMap.get(key));
        }
        //Apply the sorted values to the source list
        items.clear();
        if(order.toLowerCase() == 'asc'){
            for(Sobject ob : resultList){
                items.add(ob);
            }
        }else if(order.toLowerCase() == 'desc'){
            for(integer i = resultList.size()-1; i >= 0; i--){
                items.add(resultList[i]); 
            }
        }
    }
        
        
        
        
        
        
        // used to delete the selected issue
        public void deleteIssue() {
                try{
                        Issue__c tmpIssue = [SELECT id FROM Issue__c WHERE id =: delIssueId];
                        delete tmpIssue;
                        filterIssues();
                }
                catch(Exception e) {}
        }
        
        
        // used to retrieve the cookie
        public Cookie getCookie(string cookieName) {
                Cookie myCookie = ApexPages.currentPage().getCookies().get(cookieName);
                // build cookie if it doesn't exist
                if (myCookie == null) {
                myCookie = new Cookie(cookieName,'empty',null,-1,false);
                setCookie(myCookie);
                }
                return myCookie;
    }
    
    // used to tetrieve the value of the specified cookie
        public String getCookieValue (string cookieName) {
        Cookie myCookie = getCookie(cookieName);
        return myCookie.getValue();
    }
    
    // used to set the cookie
    public void setCookie (Cookie myCookie) {
        ApexPages.currentPage().setCookies(new Cookie[]{myCookie});
    }
    
    // used to set the value of the specified cookie
    public void setCookie (string cookieName, string cookieValue) {
        setCookie (new Cookie(cookieName,cookieValue,null,-1,false));
    }
        
}