@isTest
private class BDT_FinancialDocDownloadControllerTest {

public static FinancialDocument__c financialDocument;
	
	
static void init(){
		//servicecategories
		List<ServiceCategory__c> categoryList = BDT_CreateTestData.buildServiceCategories();
		//services
		List<Service__c> serviceList = BDT_CreateTestData.buildServices(categoryList);
		// Create project
		Client_Project__c firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		// Create studies
		List<Study__c>  studiesList = BDT_TestDataUtils.buildStudies(firstProject,1);
		insert studiesList;
		// create a set of project services
		List<ProjectService__c> prosSrv = new List<ProjectService__c>();
		prosSrv.add(new ProjectService__c(Service__c = serviceList[0].id, Client_Project__c = firstProject.id));
		upsert prosSrv;
		// Select Data for the test
		BDT_Utils.setPreference('SelectedProject', String.valueOf(firstProject.id));
		String StudyIDs = '';
		for (Study__c st : studiesList) {
			StudyIDs = StudyIDs + ':' + st.id;
		}
		BDT_Utils.setPreference('SelectedStudies', StudyIDs.removeStart(':'));
		// add services
		List<StudyService__c> ss = new List<StudyService__c>();
		for (ProjectService__c ps:prosSrv){
			StudyService__c s = new StudyService__c();
			s.ProjectService__c = ps.id;
			s.NumberOfUnits__c = 2;
			s.Study__c = StudiesList[0].id;
			ss.add(s);
		}
		upsert ss;
    	// Create the new financial document, with the predefined attributes values   	
		financialDocument = new FinancialDocument__c();
		financialDocument.Name = 'Test name';
		financialDocument.DocumentStatus__c = ( StudiesList.size()>0 )? 'New' : 'Undefined';
		financialDocument.DocumentType__c   = 'Proposal';
		financialDocument.Client_Project__c = firstproject.id;
		financialDocument.listOfStudyIds__c = StudyIds;
		insert financialDocument; 
		list<StudyServicePricing__c> sspList = [Select s.UnitPricePRAProposal__c, s.UnitPriceNegotiated__c, s.SystemModstamp, s.StudyService__c, s.Service__c, s.RateCardPrice__c, s.RateCardCurrency__c, s.RateCardClass__c, s.NumberOfUnits__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsDeleted, s.Id, s.FinancialDocumentIDs__c, s.DiscountOnTotal__c, s.CreatedDate, s.CreatedById, s.CalculatedTotalPrice__c, s.CalculatedDiffToRateCard__c, s.ApprovedFinancialDocument__c From StudyServicePricing__c s];
		for (StudyServicePricing__c ssp:sspList){
			ssp.FinancialDocumentIDs__c = financialDocument.id;
		}
		upsert sspList;
		// Create a payment term
		FinancialDocumentPaymentTerm__c tmpPaymentObject = new FinancialDocumentPaymentTerm__c();
		tmpPaymentObject.FinancialDocument__c = financialDocument.Id;
		tmpPaymentObject.Description__c 	   = 'Test payment term';
		tmpPaymentObject.StudyIdPercentageJSON__c = '[{ "studyId":"' + financialDocument.listOfStudyIds__c + '" , "percentage":"100" }]';
		insert tmpPaymentObject;
		// Create milestone
		MileStoneDefinition__c tmpMileStoneOblect = new MileStoneDefinition__c();
		tmpMileStoneOblect.Category__c = 'Test Category';
		tmpMileStoneOblect.SequenceNumber__c = 1.0;
		insert tmpMileStoneOblect;
		StudyMileStone__c tmpStMilObject = new StudyMileStone__c();
		tmpStMilObject.MileStoneDefinition__c = tmpMileStoneOblect.Id;
		tmpStMilObject.Study__c = StudiesList[0].Id;
		insert tmpStMilObject;
		// PopulationStudyAssignment__c
		List<Population__c> tmpPopList = BDT_TestDataUtils.buildPopulation(firstproject.Id, 1);
		insert tmpPopList;
		PopulationStudyAssignment__c tmpPopStAssignObj = new PopulationStudyAssignment__c();
		tmpPopStAssignObj.Population__c = tmpPopList[0].id;
		tmpPopStAssignObj.Study__c = StudiesList[0].Id;
		insert tmpPopStAssignObj;
		// Create Clinical Design and association
		List<ClinicalDesign__c> designList = BDT_TestDataUtils.buildClinicDesign(firstproject);
		insert designList;
		ClinicalDesignStudyAssignment__c desToStAssign = new ClinicalDesignStudyAssignment__c();
		desToStAssign.ClinicalDesign__c = designList[0].id;
		desToStAssign.Study__c = StudiesList[0].Id;
		insert desToStAssign;
		StudyDesignPopulationAssignment__c tmpStDesPopObject = new StudyDesignPopulationAssignment__c();
		tmpStDesPopObject.ClinicalDesignStudyAssignment__c = desToStAssign.id;
		tmpStDesPopObject.PopulationStudyAssignment__c = tmpPopStAssignObj.id;
		insert tmpStDesPopObject;
		// Create flowchart, arm and epoch and their association
		Flowcharts__c tmpFlowchart = new Flowcharts__c();
		tmpFlowchart.Description__c = 'Test Flowchart';
		tmpFlowchart.ClinicalDesign__c = designList[0].id;
		tmpFlowchart.Flowchart_Number__c = 1.0;
		insert tmpFlowchart;
		Arm__c tmpArm = new Arm__c();
		tmpArm.Description__c = 'Test arm';
		tmpArm.ClinicalDesign__c = designList[0].id;
		tmpArm.Arm_Number__c = 1.0;
		insert tmpArm;
		Epoch__c tmpEpoch = new Epoch__c();
		tmpEpoch.Description__c = 'Test epoch';
		tmpEpoch.ClinicalDesign__c = designList[0].id;
		tmpEpoch.Duration_days__c = 2.0;
		tmpEpoch.Epoch_Number__c = 1.0;
		insert tmpEpoch;
		Flowchartassignment__c tmpFlowchartAssignObj = new Flowchartassignment__c();
		tmpFlowchartAssignObj.Arm__c = tmpArm.id;
		tmpFlowchartAssignObj.Epoch__c = tmpEpoch.id;
		tmpFlowchartAssignObj.Flowchart__c = tmpFlowchart.id;
		insert tmpFlowchartAssignObj;
	}

	static testMethod void myUnitTest() {
		init();
		// no document
		BDT_FinancialDocDownloadController pageController = new BDT_FinancialDocDownloadController();
		// document set
		PageReference b = new pagereference(System.Page.BDT_FinancialDocDownload.getURL() );
		b.getParameters().put('financialDocumentID',''+financialDocument.id);
		test.setCurrentPage(b);
		pageController = new BDT_FinancialDocDownloadController();
	}
}