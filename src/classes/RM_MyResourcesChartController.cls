public with sharing class RM_MyResourcesChartController {

	//public string timeRange{get; set;}
	//public integer nMonth{get;set;} //number of month to display
	public string nStrMonth{get;set;}
	public string empRID{get; set;}
	
	public string pMonth{get; set;}
	public string nView{get; set;}
	public Integer n{get; set;}
	public Boolean bPrev{get; private set;}
	public Boolean bNext{get; private set;}
	
	private final integer iResStartMonth = -4;
	private final integer iResEndMonth = 11;
	private string month1;	
	private string empList;
	
	//lineChart
	private string lineCat;
	private string lineData;
	
	//pieChart

	private string pieData;	

	private string pieDataHist;
	
	private list<nMonthAllocation> monthAllocationList{get; set;}
	private string superVisorID;
	private Integer nMax;
	private Integer emplSize;
	public RM_MyResourcesChartController(){
		//nMonth = 1;
		nStrMonth = '3';
		n = 1;
		nView = 'All';
		bPrev = false;
		bNext = false;
		superVisorID = ApexPages.currentPage().getParameters().get('sid');
		getDataSeries(Integer.valueOf(nStrMonth));
		getResourceAvailablityData();
		pMonth = RM_Tools.GetYearMonthFromDate(System.Today());
		getPieData(empRID,pMonth);
	}
	public List<SelectOption> getTimeRangeItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('1','Current Month'));
        options.add(new SelectOption('2','2 Months'));
        options.add(new SelectOption('3','3 Months'));
        options.add(new SelectOption('4','4 Months'));
        options.add(new SelectOption('5','5 Months'));
        options.add(new SelectOption('6','6 Months'));
          
       return options;
   	}
   	public List<SelectOption> getProjTimeItems(){
        List<SelectOption> options = new List<SelectOption>();
        for (string m: RM_Tools.GetMonthsList(-4, 6)){
        	   options.add(new SelectOption(m,m));
        } 
       return options;
   	}
   	public List<SelectOption> getEmpNameItems(){
        List<SelectOption> options = new List<SelectOption>();
        for (Employee_Details__c resource: RM_EmployeeService.GetEmployeeDetailByEmployeeSupervisor(superVisorID)){
        	  if (empRID == null || empRID == '')
        	  	empRID = resource.id;
        	  options.add(new SelectOption(resource.id,resource.Last_Name__c + ', ' + resource.First_Name__c));
        } 
      
       return options;
   	}
   	public list<SelectOption> getViewItems(){
   		List<SelectOption> options = new List<SelectOption>();
   		list<Employee_Details__c> emplList = RM_EmployeeService.GetEmployeeDetailByEmployeeSupervisor(superVisorID);
   		
   		emplSize = emplList.size();
   		return RM_LookupDataAccess.getViewNumberItems(emplSize, 10);
   	}
    public  PageReference TimeRangeChange(){
    	if (nStrMonth == null)
    		nStrMonth = '1';
    	getDataSeries(Integer.valueof(nStrMonth));
    	return null;
    }
    public PageReference ViewChange(){
    	if (nView != 'All'){
    		bNext = true;
    		n = 1;
    		nMax = Math.round(emplSize/decimal.valueOf(nView)+0.5);
    		system.debug('-----nMax------'+nMax + '--' +emplSize) ;
    	}
    	else{
    		bPrev = false;
			bNext = false;
    	}
    	getDataSeries(Integer.ValueOf(nStrMonth));
    	
    	return null;
    }
    public PageReference Previous(){
    	if (n > 1)
    		n -= 1;
    	if (n > 1)
    		bPrev = true;
    	else
    		bPrev = false;
    	if (n < nMax)
    		bNext = true;
    	else
    		bNext = false;
    	getDataSeries(Integer.ValueOf(nStrMonth));
    	return null;
    }
    public PageReference Next(){
    	if (n < nMax)
    		n += 1;
    	if (n > 1)
    		bPrev = true;
    	else
    		bPrev = false;
    	if (n < nMax)
    		bNext = true;
    	else
    		bNext = false;
    	getDataSeries(Integer.ValueOf(nStrMonth));
    	return null;
    }
    public  PageReference EmpNameChange(){
    	getResourceAvailablityData();
    	getPieData(empRID,pMonth);
    	return null;
    }
    public  PageReference ProjTimeChange(){
    	getPieData(empRID,pMonth);
    	return null;
    }
    public string getseriesData(){
    	return month1;
    }
    public string getempList(){
    	return empList;
    }
    public string getLineCat(){
    	return lineCat;
    }
    public string getLineData(){
    	return lineData;
    }
    public string getPieData(){
    	return pieData;
    }
    public string getPieDataHist(){
    	return pieDataHist;
    }
    public class nMonthAllocation{
        public string name {get; private set;}
        public list<double> allocation{get;private set;}
      	
      
        
        public nMonthAllocation(list<WFM_Employee_Availability__c> avaList, integer iMonth){          
            system.debug('-----avaList------'+avaList);  
            allocation = new double[iMonth];
            for (integer i = 0; i < iMonth; i++)
            	allocation[i] = 1.0;
            list<string> monthList = RM_Tools.GetMonthsList(0, iMonth-1);
            Map<string, integer> cMapMonth = new Map<string, integer>();
	    	for ( integer i = 0; i < monthList.size(); i++){
	    		cMapMonth.put(monthList[i], i);
	    	}
            integer i = 0;
            for (WFM_Employee_Availability__c ava: avaList){
            	i = cMapMonth.get(ava.year_month__c);
            	if (i == 0){ 
            		string lastName = ava.employee_id__r.last_name__c;
            		system.debug('-----lastName------'+lastName);
            		lastName = lastName.replace('\'', '\\\'');
            		system.debug('-----lastName------'+lastName);
            		//list<string> lstString = lastName.split('\''); 
            		//system.debug('-----lstString------'+lstString);
            		/*if (lastName.indexOf('\'') > -1){
            			string [] lastNameArray = lastName.split('\'');
            			lastName = '';
            			for (string s: lastNameArray)
            				lastName += s + '\'';	
            		}*/
            		
            		name= lastName + ', ' + ava.employee_id__r.first_name__c;
            	}
            	allocation[i] = -ava.Available_FTE__c;
            		
            }  
        }
    }
    public void OnClickAllocation(){
    	string resourceName = ApexPages.CurrentPage().getParameters().get('resourceName');
        pMonth = ApexPages.CurrentPage().getParameters().get('monthYear');
        //get employeeID
        string[] name = resourceName.split(', ');
       	empRID = RM_EmployeeService.GetEmployeeRIDByEmployeeSupervisorAndEmpName(superVisorID, name[0], name[1] );
       	system.debug('-----empRID------'+empRID);
       	getResourceAvailablityData();
       	//need to set month
        getPieData(empRID,pMonth);
    }
    public void OnClickResourceAvail(){
    	 pMonth = ApexPages.CurrentPage().getParameters().get('monthYear');
    	 getPieData(empRID,pMonth);
    	 
    }
    private list<nMonthAllocation> getNumMonthAllocationData(){
    	 
    	 list<nMonthAllocation> monthAll = new list<nMonthAllocation>();
    	 if (superVisorID != null && superVisorID != ''){
	    	
	    	
	    	 //rearrange by employee name
	    	 list<WFM_Employee_Availability__c> avaEmpl = new list<WFM_Employee_Availability__c>();
	    	 string emplID = '';
	    	
	    	 list<WFM_Employee_Availability__c> avaList = RM_AssignmentsService.GetAvailabilityByEmployeeSupervisorID(superVisorID, RM_Tools.GetMonthsList(0, Integer.valueOf(nStrMonth)-1));
			    	 	
    	 	 system.debug('-----avaList------'+avaList);
    	 	 if (avaList.size() > 0){
    	 	 	//paging
    	 	 	
    	 	 	
	 	 		integer nStart = 0;
    	 	 	integer nEnd = avaList.size()-1;
    	 	 	
    	 	 	if (nView!= 'All' && nView != null){
	    	 	 	nStart = Integer.valueof(nView)*(n-1)*Integer.valueOf(nStrMonth);
	    	 	 	
	    	 	 	nEnd = nStart + Integer.valueOf(nView)*Integer.valueOf(nStrMonth);
	    	 	 	if (nEnd > avaList.size()-1)
	    	 	 		nEnd = avaList.size()-1;
    	 	 	}
    	 	 	 emplID = string.valueOf(avaList[nStart].Employee_ID__c);
    	 		 empRID = string.valueOf(avaList[nStart].Employee_ID__c);
	    	 	 for  (integer i = nStart; i <= nEnd; i++) {
	    	 	 	
	    	 		if (string.valueOf(avaList[i].Employee_ID__c) != emplID){
	    	 			if (emplID != ''){
	    	 				system.debug('-----avaEmpl------'+avaEmpl);  
	    	 				monthAll.add(new nMonthAllocation(avaEmpl, Integer.valueOf(nStrMonth))); 
	    	 				avaEmpl.clear();
	    	 				
	    	 			}
	    	 			emplID = string.valueOf(avaList[i].Employee_ID__c);
	    	 			avaEmpl.add(avaList[i]);
	    	 		}
	    	 		else{
	    	 		
	    	 			avaEmpl.add(avaList[i]);
	    	 			//system.debug('-----avaEmpl------'+avaEmpl);  
	    	 		}
	    	 		if (i == avaList.size()-1){
    	 				monthAll.add(new nMonthAllocation(avaEmpl, Integer.valueOf(nStrMonth))); 
    	 				avaEmpl.clear();
	    	 		}
		    	 }
	    	 }
    	 }
    	 return monthAll;
    }
    private void getDataSeries(integer iMonth){
    	list<string> monthList = RM_Tools.GetMonthsList(0, iMonth-1);
		monthAllocationList = getNumMonthAllocationData();
		system.debug('-----monthAllocationList------'+monthAllocationList);
		empList = ''; 
		month1='';
		for (integer i = 0; i < iMonth; i++){
			if (monthAllocationList.size() > 0){
				
				month1 += '{ name: \'' + monthList[i] + '\', data:[';
				for (nMonthAllocation m: monthAllocationList){
					if (i == 0)
						empList+='\''+ m.name + '\',';
					month1 += m.allocation[i] + ',';
				}
				month1 = month1.substring(0, month1.length()-1) + ']},';
			}
			
		}
		system.debug('-----month1------'+month1);
		system.debug('-----empList------'+empList);
		//remove last ,
		if (empList.length() > 0){
			empList = empList.substring(0, empList.length()-1) ;
			month1 = month1.substring(0, month1.length()-1);
		}
    }
    private void getResourceAvailablityData(){
    	list<WFM_Employee_Availability__c> resourceAvaList = RM_AssignmentsService.GetAvailabilityByEmployee(empRID, RM_Tools.GetMonthsList(iResStartMonth, iResEndMonth));
    	//to create string for category and series
    	system.debug('-----empRID------'+empRID);
    	lineCat = '';
    	
    	lineData = '';
    	for (integer i = 0; i <resourceAvaList.size(); i++ ){
    	
	    	lineCat += '\'' + resourceAvaList[i].Year_Month__c + '\',';
	    	lineData += string.valueOf(resourceAvaList[i].Available_FTE__c) + ',';
	    }
	    if (lineData.length() > 1){
	    	lineData = lineData.substring(0, lineData.length() -1);
	    	lineCat = lineCat.substring(0, lineCat.length() -1);
	    }
    	system.debug('-----resourceAvaList------'+resourceAvaList);
    	system.debug('-----lineCat------'+lineCat);
    	system.debug('-----lineData------'+lineData);
    }
    
    private void getPieData(string empRID, string yearMonth){
    	string strData = '';
    	string strAssignDataOther = '';
    	string strPostDataOther = '';
    	string strAva = '';
    	AggregateResult[] postedHrs = RM_EmployeeService.GetPostedFTEbyEmployeeRIDMonth(empRID, yearMonth);
    	AggregateResult[] assignHrs = RM_AssignmentsService.GetTotalAssignedFTEbyEmployeeRIDMonth(empRID, yearMonth);
    	system.debug('-----assignHr------'+assignHrs);
    	List<WFM_Employee_Availability__c> avaHrs = RM_AssignmentsService.GetAvailabHrByEmployeeRIDMonth(empRID, yearMonth);
    	system.debug('-----avaHrs------'+avaHrs);
    	
    	if (avaHrs.size() > 0){
    		double totalHr = avaHrs[0].Total_Working_Hour__c;
    		system.debug('-----totalHr------'+totalHr);
    	
    		if (assignHrs.size() > 0){
	    		for (AggregateResult ar : assignHrs){
	    			double per = 0.0;
	    			if (ar.get('total_assignedFTE') != null)
	    				per =(double)ar.get('total_assignedFTE') ;
	    			
	    			if (FindinHist((string)ar.get('projID'),postedHrs))  
	    				strData+='[\'' + (string)ar.get('projID') + '\',' + string.valueOf(per) + '],';
	    			else
	    				strAssignDataOther+='[\'' + (string)ar.get('projID') + '\',' + string.valueOf(per) + '],';
	    		}
	    		
    		}
    		if (avaHrs[0].Available_Hours__c > 0.0){
    			strData+='[\'Available\',' + string.valueOf(avaHrs[0].Available_fte__c) + '],';
    		}
    		//availability table
    	
    		
			if (avaHrs[0].Leave_in_Hours__c > 0.0){
				strAva+='[\'Leave\',' + string.valueOf(avaHrs[0].Leave_in_Hours__c/totalHr) + '],';
			}
			if (avaHrs[0].Non_Billable_in_Hours__c > 0.0){
				strAva+='[\'Non Billable\',' + string.valueOf(avaHrs[0].Non_Billable_in_Hours__c/totalHr) + '],';
			}
			if (avaHrs[0].PTO_in_Hours__c > 0.0){
				strAva+='[\'PTO\',' + string.valueOf(avaHrs[0].PTO_in_Hours__c/totalHr) + '],';
			}
			if (avaHrs[0].Pre_Start_in_Hours__c > 0.0){
				strAva+='[\'Pre Start\',' + string.valueOf(avaHrs[0].Pre_Start_in_Hours__c/totalHr) + '],';
			}
			if (avaHrs[0].Training_in_Hours__c > 0.0){
				strAva+='[\'Training\',' + string.valueOf(avaHrs[0].Training_in_Hours__c/totalHr) + '],';
			}
			
			if (avaHrs[0].Employee_ID__r.FTE_Equivalent__c <1.0){
				strAva+='[\'Non Available (Part Time)\',' + string.valueOf(1.0-avaHrs[0].Employee_ID__r.FTE_Equivalent__c) + '],';
			}
    		
    		if (strAssignDataOther.length() > 0)
    			strData += strAssignDataOther;
    		if (strAva.length() > 0)
    			strData += strAva;   
    		if (strData.length() > 1) 		
    			pieData = strData.substring(0, strData.length()-1);
    		else
    			pieData = '';
    	}
    	else
    		pieData = '';	
    	//posted time pie
    	if (postedHrs.size() > 0){
    		strData = '';
    		  
    		for (AggregateResult ar : postedHrs){
    			if (FindinAssign((string)ar.get('projID'),assignHrs)) 
    				strData+='[\'' + (string)ar.get('projID') + '\',' + string.valueOf(ar.get('total_FTE')) + '],';
    			else
    				strPostDataOther+='[\'' + (string)ar.get('projID') + '\',' + string.valueOf(ar.get('total_FTE')) + '],';
    		}
    		if (strPostDataOther.length() > 0)
    			strData += strPostDataOther;
    		if (strAva.length() > 0)
    			strData += strAva;   
    		pieDataHist = strData.substring(0, strData.length()-1);
    		
    		system.debug('-----pieDataHist------'+pieDataHist);
    		
    	}
    	else
    		pieDataHist = '';
    	
    }
    
    private boolean FindinHist(string projectID, AggregateResult[] postedHrs){
    	for (AggregateResult ar : postedHrs){
    		if (projectID == (string)ar.get('projID')){
    			return true;
    		}
    	}
    	return false;
    }
     private boolean FindinAssign(string projectID, AggregateResult[] assignHrs){
    	for (AggregateResult ar : assignHrs){
    		if (projectID == (string)ar.get('projID')){
    			return true;
    		}
    	}
    	return false;
    }
}