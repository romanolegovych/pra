public class LMS_PersonToAdhocController {

    // Instance Variables
    public String commitDate {get;set;}
    public String roleText {get;set;}
    public String roleFilter {get;set;}
    public String roleTextStyle {get;private set;}
    public String roleNameDisplay {get;private set;}
    public String roleResultId {get;private set;}
    public String assignment {get;set;}
    public String empName {get;set;}
    public String jobFamily {get;set;}
    public String acPersonFilter {get; private set;}
    public String roleErrorText {get;private set;}
    public String personErrorText {get;private set;}
    public String webServiceError {get;private set;}
    private String statusFilter {get;set;}
    private String employeeIdFilter {get;set;}
    private String wlblFilter {get;set;}
    public Boolean allChecked {get;set;}
    public Boolean allCheckedPerson {get;set;}
    public Boolean allAdhoc {get;set;}
    public Boolean renderRoleCreate {get;set;}
    public Boolean bWebServiceError {get;private set;}

    // Collections
    public List<LMS_PersonAssignment> employees {get;private set;}
    public List<LMS_PersonList> selEmps {get;private set;}
    public List<LMS_Role__c> roles {get;set;}
    public List<String> allRoles {get;private set;}
    private Map<String,String> roleResult {get;private set;}
    private Map<String,String> roleCommitted {get;private set;}
    private Map<String,String> roleDraft {get;private set;}
    private Set<String> excludeStatus {get;private set;}
    private Set<String> excludeEmps {get;private set;}
    private Map<String, String> jobTitleMap {get;private set;}
    private Map<String, LMS_Role_Employee__c> mappings {get;private set;}
    private Map<String, LMSConstantSettings__c> constants {get;private set;}

    // Error Varaibles
    public List<String> sfdcErrors {get;private set;}
    public List<String> calloutErrors {get;private set;}

    /**
     *  Constructor
     *  Initializes all data
     */
    public LMS_PersonToAdhocController() {
        initStatus();
        init();
        initSettings();
        initJobMap();
    }

    /**
     *  Used to exclude terminated status
     *  from person search
     */
    private void initStatus() {
        List<Employee_Status__c> retiredStatus = [select Employee_Status__c from Employee_Status__c where Employee_Type__c = 'Terminated'];
        excludeStatus = new Set<String>();
        for(Employee_Status__c s : retiredStatus) {
            excludeStatus.add(s.Employee_Status__c);
        }
        System.debug('-----------excludeStatuses--------------'+excludeStatus);
    }

    /**
     *  Initialize all lists and other data
     */
    private void init() {
        roleText = 'Enter part of the role name';
        roleTextStyle = 'WaterMarkedTextBox';
        roleFilter = ' and Role_Type__r.Name = \'Additional Role\' and Status__c = \'Active\'';
        statusFilter = ' and Status__c not in (\'T1\',\'T2\',\'T3\',\'T4\',\'XX\')';
        wlblFilter = ' ' + LMS_ToolsFilter.getWhitelistEmployeeFilter() + ' ' + LMS_ToolsFilter.getBlackListEmployeeFilter();
        acPersonFilter = statusFilter + wlblFilter;
        empName = '';
        jobFamily = '';
        allAdhoc = false;
        bWebServiceError = false;
        roles = null;
        employees = null;
        selEmps = null;
    }

    /**
     *  Get constants from custom settings
     */
    private void initSettings() {
        constants = LMSConstantSettings__c.getAll();
    }

    /**
     *  Create job title map to be used with
     *  the person list
     */
    private void initJobMap()   {
        jobTitleMap = new Map<String, String>();
        List<Job_Title__c> titles = [SELECT Job_Code__c, Job_Title__c FROM Job_Title__c ORDER BY Job_Title__c asc];
        if(null != titles) {
            for(Job_Title__c jt : titles) {
                jobTitleMap.put(jt.Job_Code__c, jt.Job_Title__c);
            }
        }
    }

    /**
     *  Initailize person search data
     */
    private void initEmps() {
        empName = '';
        jobFamily = '';
        selEmps = null;
    }

    /**
     *  Reset entire page
     */
    public PageReference roleReset() {
        init();
        return null;
    }

    /**
     *  Reset person search
     */
    public PageReference personReset() {
        initEmps();
        return null;
    }

    /**
     *  Populate Job Family drop-down
     */
    public List<SelectOption> getJobFamilyList() {
        list<selectoption> family = new list<selectoption>();
        family = LMS_LookUpDataAccess.getRoles(false, false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        for(integer i=0; i<family.size(); i++)
            options.add(family[i]);
        return options;
    }

    public PageReference cancelCreateRole() {
    	roles = null;
    	return null;
    }

    /**
     *  Role search logic
     */
    public PageReference search() {
    	bWebServiceError = false;
        String joinQry,roleQry = '';
        roleResult = new Map<String, String>();
        roleCommitted = new Map<String, String>();
        roleDraft = new Map<String, String>();
        excludeEmps = new Set<String>();
        mappings = new Map<String, LMS_Role_Employee__c>();

        String draft = constants.get('statusDraft').Value__c;
        String draftDelete = constants.get('statusDraftDelete').Value__c;
        String committed = constants.get('statusCommitted').Value__c;
        String deleted = constants.get('statusToDelete').Value__c;  

        //Query strings to pull the assignment data for a specific role
        joinQry = 'SELECT Employee_Id__r.Date_Hired__c,Employee_Id__r.Name,Employee_Id__r.Id,Employee_Id__r.First_Name__c,' + 
            'Employee_Id__r.Last_Name__c,Employee_Id__r.Job_Class_Desc__c,Employee_Id__r.Job_Code__c,Employee_Id__r.Business_Unit_Desc__c,' + 
            'Employee_Id__r.Department__c,Employee_Id__r.Country_Name__r.Region_Name__c,Employee_Id__r.Country_Name__r.Name,' + 
            'Employee_Id__r.Status__c,Employee_Id__r.Status_Desc__c,Role_Id__r.SABA_Role_PK__c,Commit_Date__c,Status__c,Previous_Status__c,Sync__c' + 
            ' FROM RoleEmployees__r where Status__c != :deleted ORDER BY Status__c,Sync__c DESC,Employee_Id__r.Last_Name__c,Employee_Id__r.Last_Name__c';
        roleQry = 'select Role_Name__c,SABA_Role_PK__c,Status__c,Sync_Status__c,('+joinQry+') from LMS_Role__c where Adhoc_Role_Name__c != null and Role_Type__r.Name = \'Additional Role\'';

        if((roleText != '' && roleText != 'Enter part of the role name') || allAdhoc == false) {
            roleQry += ' and Adhoc_Role_Name__c=:roleText';
        }
        roleQry += ' ORDER BY Role_Name__c';
        allChecked = false;
        //Query the database
        roles = Database.query(roleQry);
        if(roles.size() == 0 ) {
            renderRoleCreate = true;
            employees = null;
            roleCommitted = null;
            roleErrorText = 'This role doesn\'t exist, you can either search again, or click the link below';
        } else if(roles.size() == 1 && allAdhoc == false) {
            employees = new List<LMS_PersonAssignment>();
            for(LMS_Role__c r : roles) {
                roleNameDisplay = r.Role_Name__c;
                roleResultId = r.Id;
                for(LMS_Role_Employee__c re : r.RoleEmployees__r){
                	mappings.put(re.Id, re);
                    roleResult.put(re.Employee_Id__r.Name, re.Employee_Id__r.Id);
                    if(re.Status__c == committed || re.Status__c == draftDelete) {
                        roleCommitted.put(re.Employee_Id__r.Name, re.Employee_Id__r.Id);
                    } else if(re.Status__c == draft) {
                        roleDraft.put(re.Employee_Id__r.Name, re.Employee_Id__r.Id);
                    }
                    employees.add(new LMS_PersonAssignment(re, jobTitleMap));
                }
            }
            if(roles[0].SABA_Role_PK__c != '') {
           		renderRoleCreate = false;
                roleErrorText = 'Role is out of Sync with SABA, people can be added but not committed until sync is resolved';
            }
            if(roles[0].Status__c == 'Inactive') {
           		renderRoleCreate = false;
            	roleErrorText = 'Role is inactive, please enter different role name';
            }
        } else if(roles.size() > 1) {
            renderRoleCreate = false;
            allRoles = new List<String>();
            for(LMS_Role__c r : roles) {
                allRoles.add(r.Role_Name__c);
            }
        }
        if(roleText != '' && roleText != 'Enter part of the role name') {
            roleTextStyle = 'NormalTextBox';
        }

        String exclude = '';
        String excludeString = '';      
        if(null != roleCommitted && !roleCommitted.isEmpty()) {
            Set<String> excludeIds = roleCommitted.keySet();
            List<Employee_Details__c> excludeEmployees = [select Name from Employee_Details__c where Name IN :excludeIds];
            for(Integer i = 0; i < excludeEmployees.size(); i++) {
                excludeEmps.add(excludeEmployees[i].Name);
                if(i < (excludeEmployees.size() - 1)) {
                    excludeString += '\'' + excludeEmployees[i].Name + '\',';
                } else {
                    excludeString += '\'' + excludeEmployees[i].Name + '\'';
                }
            }
        } else {
            excludeString = '\'\'';
        }
        exclude += '(' + excludeString + ')';
        employeeIdFilter = ' and Name not in ' + exclude;
        acPersonFilter = statusFilter + wlblFilter + employeeIdFilter;
        return null;
    }

    /**
     *  Person search logic
     */
    public PageReference personSearch() {

        Date currentDate = Date.today();
        String empQry = '';
        String firstName = '';
        String lastName = '';
        String filter = '';
        if(empName != '' && empName.contains(',')) {
            String[] names = empName.split(',');
            if(names.size() > 1) {
                lastName = names[0];
                firstName = names[1].trim();
                filter += ' and First_Name__c = :firstName and Last_Name__c = :lastName';
            } else {
                lastName = names[0];
                filter += ' and Last_Name__c = :lastName';
            }
        } else if(empName != '') { 
            filter += ' and Last_Name__c = :empName';
        }
        if(null != jobFamily && '' != jobFamily) {
            filter += ' and Job_Class_Desc__c = :jobFamily';
        }
        filter += wlblFilter;
        empQry += 'select Name,First_Name__c,Last_Name__c,Job_Class_Desc__c,Job_Code__c,Business_Unit_Desc__c,Department__c,' + 
		    'Country_Name__r.Region_Name__c,Country_Name__r.Name,Status_Desc__c,Term_Date__c from Employee_Details__c' + 
		    ' where Term_Date__c != :currentDate AND Status__c not in:excludeStatus and Name not in:excludeEmps' + filter + 
		    ' order by Last_Name__c asc';
        allCheckedPerson = false;
        System.debug('----------person query----------------'+empQry);

        selEmps = new List<LMS_PersonList>();
        List<Employee_Details__c> empList = Database.query(empQry);
        if(empList.size() == 0) {
            personErrorText = 'The result contains no people. Please refine your search criteria and run the search again.';
        } else if(empList.size() > 1000) {
            personErrorText = 'The result contains too many records. Please refine your search criteria and run the search again.';
        } else if(empList.size() > 0) {
            for(Employee_Details__c e : empList){
                selEmps.add(new LMS_PersonList(e, roleResult, jobTitleMap));
            }
        }
        return null;
    }

    /**
     *  Closes the list for person search
     */
    public PageReference closePersonSearch() {
        empName = '';
        jobFamily = '';
        return null;
    }

    /**
     *  Add person to role with
     *  an assignment status of Draft
     */
    public PageReference addPerson() {
    	sfdcErrors = new List<String>();
    	Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.addEmployees(selEmps, employees, mappings, roleResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while adding people to role.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
		
		boolean didError = bWebServiceError;
		
        search();
        bWebServiceError = didError;
        personSearch();
        return null;
    }

    /**
     *  Remove person from the selected role
     */
    public PageReference removePersons() {
    	sfdcErrors = new List<String>();
    	Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.removeEmployees(employees, mappings);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while removing or reverting people from role.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
		
		boolean didError = bWebServiceError;
		
        search();
        bWebServiceError = didError;
        return null;
    }

    /**
     *  Remove all changes made to role assignments
     */
    public PageReference cancel() {
    	sfdcErrors = new List<String>();
    	Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.cancelDraftEmployees(roleResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }

        search();
        return null;
    }

    public PageReference setCommitDate() {

    	sfdcErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        Date dateCommit = Date.parse(commitDate);
        errors = LMS_ToolsModifyAssignments.setCommitDateEmployees(employees, mappings, dateCommit);

        sfdcErrors = errors.get('SFDCEX');
        System.debug('-------------errors------------'+errors);
        System.debug('-------------sfdcErrors-----------'+sfdcErrors);

        if(null != sfdcErrors && sfdcErrors.size() > 0) {
            webServiceError = 'Errors occurred while applying commit date.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
		
		boolean didError = bWebServiceError;
		
        search();
        bWebServiceError = didError;
        return null;
    }

    /**
     *  Commit changes, send data to SABA
     */
    public PageReference commitPerson() {
    	sfdcErrors = new List<String>();
        calloutErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.commitEmployees(employees, mappings);
        sfdcErrors = errors.get('SFDCEX');
        calloutErrors = errors.get('CALLOUT');

        System.debug('-------------sfdcErrors------------'+sfdcErrors);
        System.debug('-------------calloutErrors------------'+calloutErrors);

        if((null != sfdcErrors && sfdcErrors.size() > 0) || (null != calloutErrors && 0 < calloutErrors.size())) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        return null;
    }

    /**
     *  View the impact of adding people to the role
     */
    /*public PageReference viewImpact() {
        return null;
    }*/

    public PageReference showErrors() {
        PageReference pr = new PageReference('/apex/LMS_PersonAdhocError');
        pr.setRedirect(false);
        return pr;
    }

}