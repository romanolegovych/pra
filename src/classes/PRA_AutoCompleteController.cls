global class PRA_AutoCompleteController{
    @RemoteAction
    global static SObject[] findSObjects(string obj, string searchfield, string qry, string addFields, string addFilter) {
        // more than one field can be passed in the addFields parameter
        // split it into an array for later use
        system.debug('---------addFields---------' + addFields);
        List<String> fieldList;
        if (addFields != null && addFields != '') fieldList = addFields.split(',');
       // check to see if the object passed is valid
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sot = gd.get(obj);
        if (sot == null) {
            // Object name not valid
            return null;
        }
        // create the filter text
        String filter = searchField + ' like \'' + String.escapeSingleQuotes(qry) + '%\'';
        //begin building the dynamic soql query
        String soql = 'select id,' + searchfield;
        // if an additional field was passed in add it to the soql
        if (addFields != null && addFields != '') {
          
            for (String s : fieldList) {
              system.debug('---------s---------' + s);
                soql += ', ' + s;
            }
        }
        // add the object and filter by name to the soql
        soql += ' from ' + obj + ' where ' + filter;
        // add the filter by additional fields to the soql
      //  if (fieldList != null) {
      //      for (String s : fieldList) {
      //          soql += ' or ' + s + filter;
     //       }
     //   }
        soql += ' ' + addFilter + ' order by ' + searchfield ;
          if (addFields != null && addFields != '') {
            for (String s : fieldList) {
                soql += ', ' + s;
            }
        }
        soql += ' limit 20';
        system.debug('---------soql---------' + soql);
        List<sObject> L = new List<sObject>();
        try {
            L = Database.query(soql);
          
        }
        catch (QueryException e) {
            return null;
        }
        return L;
   }
}