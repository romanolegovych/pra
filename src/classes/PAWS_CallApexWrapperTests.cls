/**
*   'PAWS_CallApexWrapperTests' is the test class for PAWS_API
*   Test classes are used to write the unit test and cover the code coverage of the code.
*   @author   Work Relay
*/
@isTest 
public with sharing class PAWS_CallApexWrapperTests 
{
	static testmethod void run()
    {
    	PAWS_ApexTestsEnvironment.init();
    	
    	STSWR1__Flow_Instance_Cursor__c cursor = (STSWR1__Flow_Instance_Cursor__c)new STSWR1.API().call('WorkflowService', 'getFlowInstanceCursorById', PAWS_ApexTestsEnvironment.FlowInstanceCursor.Id); 

    	new PAWS_CallApexWrapper().run(PAWS_ApexTestsEnvironment.Project, cursor, 'Proceed', 'CompleteParentStep');
    }
}