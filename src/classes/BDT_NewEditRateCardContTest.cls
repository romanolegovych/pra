@isTest
private class BDT_NewEditRateCardContTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        PageReference pageRef = New PageReference(System.Page.BDT_NewEditRateCard.getUrl());
        Test.setCurrentPage(pageRef);
        
        ServiceCategory__c sc 	= new ServiceCategory__c (name='test');
        insert sc;
        
        Service__c s  			= new Service__c (name='test',ServiceCategory__c = sc.id);
        insert s;
        
        Business_Unit__c bu 	= new Business_Unit__c (name='test');
        insert bu;
        
              
        // Add parameters to page URL
        ApexPages.currentPage().getParameters().put('serviceID', s.id);
        ApexPages.currentPage().getParameters().put('businessunitID', bu.id);
        BDT_NewEditRateCardController myController = new BDT_NewEditRateCardController();
        myController.addRateCardClass();
        
        // check that the ratecardclass element has been created
        System.assert(myController.ratecardclassList.size() == 1);
        myController.ratecardclassList[0].UnitPrice__c = 10;
                
        // there is no ratecard, the system should create it together with blank ratecardclass
        PageReference dummy = myController.save();
        
        dummy = myController.cancel();
        myController.updateList();
        
        // reload the objects to test initiator
        myController = new BDT_NewEditRateCardController();
        
        // remove ratecard
        delete myController.ratecardclassList;
        
         // reload the objects to test initiator
        myController = new BDT_NewEditRateCardController();
        
        // create 3 ratecardclasses then remove the second
        myController.addRateCardClass();
        myController.addRateCardClass();
        myController.addRateCardClass();
        dummy = myController.save();
        ApexPages.currentPage().getParameters().put('RateCardClassId', myController.ratecardclassList[1].id);
        myController.deleteRateCardClass();
        dummy = myController.save();
        
        
    }
}