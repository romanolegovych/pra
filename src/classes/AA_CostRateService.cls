public with sharing class AA_CostRateService{

    public static List<BUF_Code__c> getBufCodes(){
         return AA_CostRateDataAccessor.getBufCodes();
    }
    
    public static List<BUF_Code__c> getBufCodesByName(String searchTerm){
         return AA_CostRateDataAccessor.getBufCodesByName(searchTerm);
    }
    
    public static BUF_Code__c getBufCode(String val){
         return AA_CostRateDataAccessor.getBufCode(val);
    }
    
    public static List<Currency__c> getCurrencies(){
         return AA_CostRateDataAccessor.getCurrencies();
    }
    public static List<BUF_Code__c> getBufCodeIdbyName(String name){
        return AA_CostRateDataAccessor.getBufCodeIdbyName(name);
    }
    public static List<Currency__c> getCurrencyIdbyName(String name){
        return AA_CostRateDataAccessor.getCurrencyIdbyName(name);
    }
    public static void generateCostRates(Cost_Rate__c rate){
        AA_CostRateDataAccessor.insertRecord(rate);
        Integer tempYear = Integer.valueOf(rate.Year_Rate_Applies_To__c);
        for (integer i=0;i<15;i++){
            Cost_Rate__c tempRate = new Cost_Rate__c ();
            tempRate.BUF_Code__c = rate.BUF_Code__c;
            tempRate.Cost_Rate__c = rate.Cost_Rate__c;
            tempRate.Schedule_Year__c = rate.Schedule_Year__c;
            tempYear = tempYear + 1;
            tempRate.Year_Rate_Applies_To__c =  String.valueOf(tempYear) ;
            tempRate.Currency_lookup__c = rate.Currency_lookup__c;
            tempRate.IsActive__c = true;
            AA_CostRateDataAccessor.insertRecord(tempRate);
        }
    }
    public static List<Cost_Rate__c> checkExisting(String code , String year){
         return AA_CostRateDataAccessor.checkExisting(code,year);
    }
    
    public static List<BUF_Code__c> getBufCodeId(Business_Unit__c bu, Function_Code__c f){
        return AA_CostRateDataAccessor.getBufCodeId(bu,f);
    }
    
    public static List<Cost_Rate__c> getCostRatebyFunction(BUF_Code__c bu, String year){
        return AA_CostRateDataAccessor.getCostRatebyFunction(bu,year);
    }
    public static List<Cost_Rate__c> getCostRateId(String bu, String year1, String year2){
        return AA_CostRateDataAccessor.getCostRateId(bu,year1,year2);
    }
    public static List<AggregateResult> getFunctionListByFilter(String year,String bucode,String functioncode,String country,String active){
         return AA_CostRateDataAccessor.getFunctionListByFilter(year,bucode,functioncode,country,active);
    }
      
      
   public static List<Cost_Rate__c> getFunctionDetails(String Id , String Year){
        return AA_CostRateDataAccessor.getFunctionDetails(Id,Year);
   }
  
   public static List<Cost_Rate__c> getCostRates(String Id , String scheduledYear){
       return AA_CostRateDataAccessor.getCostRates(Id ,scheduledYear);
   }
}