public with sharing class BDT_MileStoneDefinitionController {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} //Administration Milestones
	public List<MileStoneDefinition__c> MSD {get;set;}
	public MileStoneDefinition__c msdEdit {get;set;}
	public Boolean ShowEdit {get;set;}
	public Set<String> Categories {get;set;}
	public integer oldsequencenumber;
	
	public BDT_MileStoneDefinitionController() {
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Milestones');
		LoadMileStoneDefinitions();
		ShowEdit = (ShowEdit==null)?false:ShowEdit;
	}
	
	public void LoadMileStoneDefinitions() {
		MSD = new List<MileStoneDefinition__c>();
		MSD = [select id,sequencenumber__c, name,Category__c from MileStoneDefinition__c where BDTDeleted__c = false order by sequencenumber__c];
		//load categories
		Categories = new Set<String>();
		for (MileStoneDefinition__c m:MSD) {
			Categories.add(m.Category__c);
		}
		
	}
	
	public void Edit() {
		string ID = system.currentPageReference().getParameters().get('msdid');
		msdEdit = [select id, sequencenumber__c, name,Category__c, BDTDeleted__c from MileStoneDefinition__c where id = :ID];
		ShowEdit = true;
		oldsequencenumber = Integer.valueOf(msdEdit.sequencenumber__c);
	}
	
	public void Add() {
		msdEdit = new MileStoneDefinition__c();
		ShowEdit = true;
	}
	
	public void Cancel() {
		msdEdit = null;
		ShowEdit = false;
	}

	public void DeleteMSD() {
		
		// test is there are children
		List<StudyMileStone__c> sms = [select id from StudyMileStone__c where MileStoneDefinition__c = :msdEdit.id];
		if (sms.size() > 0) {
			// this item has children, so it should be soft deleted
			msdEdit.BDTDeleted__c = true;
			update msdEdit;
		} else {
			// this item has no children, so it can be permanently deleted
			delete msdEdit;
		}
		LoadMileStoneDefinitions();
		ShowEdit = false;
	}

	public void Save() {
		if (((msdEdit.SequenceNumber__c == null)?0:msdEdit.SequenceNumber__c) == 0) {
			AggregateResult[] ar = [select max(SequenceNumber__c) maxresult from milestonedefinition__c];
			Object max = ar[0].get('maxresult');
			msdEdit.SequenceNumber__c = Integer.ValueOf(max)+1;		
		}
		 
		upsert msdEdit;
		
		fixSequenceNumbers( String.valueOf(msdEdit.Id), Integer.valueOf(msdEdit.SequenceNumber__c) );
		LoadMileStoneDefinitions();
		ShowEdit = false;
	}
	
	public void ApplyCategory() {
		msdEdit.Category__c = system.currentPageReference().getParameters().get('categoryName');
	}
	
	public void fixSequenceNumbers(string msdId, integer requestednumber){
				
		try{
			List<MileStoneDefinition__c> msdlist = [select id, sequencenumber__c, name 
										  			from MileStoneDefinition__c 
										 			where BDTDeleted__c = false 
										 			order by sequencenumber__c, name];
			Integer sequencecount = 1;
			Boolean changed = false;
			for (MileStoneDefinition__c msd: msdlist) {
				msd.SequenceNumber__c = msd.SequenceNumber__c * 10;
				if (msd.id == msdId ) {
					if ((oldsequencenumber*10) > msd.SequenceNumber__c) {
						msd.SequenceNumber__c = msd.SequenceNumber__c - 5;
					} else {
						msd.SequenceNumber__c = msd.SequenceNumber__c + 5;
					}
				} 
			}
			BDT_Utils.sortList(msdlist,'SequenceNumber__c','asc');
			for (MileStoneDefinition__c msd:msdlist) {
				if (msd.SequenceNumber__c != sequencecount) {
					msd.SequenceNumber__c = sequencecount;
					changed = true;
				}
				sequencecount++;
			}
			upsert msdlist;
		}
		catch( exception e ){}	
	}
		
}