global with sharing class RM_ViewAssignmentsCompController {
  
    public List<string> monthLab{get;private set;}
    
 	public Map<string, sObject> avaMap{get; private set;}

	public List<string> NonBillableLst{get; private set;}	
 
    
    public set<string> stManagedBU{get;set;}
    public List<string> lstManagedBU{get;set;}
    public Set<string> stManagedRole{get; private set;}
    public String selectedBusinessUnit{ get; set; }
    public string viewStartMonth{get; set;}
    public string viewEndMonth{get; set;}   
    public string strExportURL{get; set;}

	public Boolean bNewAssignment {get; set;}
    public Boolean bVisHr{get; set;}
    public Boolean bActive{get; set;}
	public Boolean bBatchEdit{get; set;}
    public Boolean bLink{get;private set;}
    public string strTarget{get; set;}  
     public WFM_employee_Allocations__c selAlloc{get;set;} 

    public boolean renderDialog{get;set;}
    public string strTitle{get;private set;} 
    public string strCheck{get;private set;}
  //  public string strFooter{get; private set;}
    //attribute from component
    public string recordID{get; set;}
    public string IDType{get; set;}
    public Integer startMonth{get;set;}
    public Integer endMonth{get;set;}
    public string displayType{get;set;}
    public Boolean bType{get; set;}
    public string pgeName{get; set;}    
    public Boolean bExport{get;private set;}
    public Boolean bEdit{get; set;}
 	public string target{get;set;}
    
    public boolean bRMAdmin{get; set;}
    public boolean bRefresh{get; private set;}
    //BatchEdit
    public decimal newAssignValue{get; set;}
    public string selectedAllUnit{get; set;}
    public string assNewStartDate{get; set;}
    public string assNewEndDate{get; set;}
	public string rejectDate{get; set;}
	
	public string addNote{get; set;}
	private string searchAssignmentNote;
    // data field
    private final string PROJECT = 'Project';
    private final string SPONSOR = 'Sponsor';
    private final string EE_NAME = 'Name';
    private final string ROLE = 'Role';
    private final string LEAD = 'Lead';
    private final string PSSV = 'PSSV';
    private final string UNBL = 'UnBL';
    private final string COUNTRY = 'Country';
    private final string STATUS = 'Status';
    private final string TYPE = 'Type';
    private final string STARTDATE = 'Start';
    private final string ENDDATE = 'End';
    private final string NOTE = 'Note';
    private final string HISTORY = 'Hist';
    private final string EDIT_LINK = 'Edit';
    private final string AID = 'aID';
    private final string RESOURCE_ID = 'eID';
    private final string PROJECT_ID = 'pID';
    private final string BU = 'Business Unit';
    private final string EDIT_NOTE = 'EditNote';
    private final string REQUEST_ID = 'Request ID';
   
    private Set<string> editableStatus = new set<string>{'Bid', 'Active'};
  
    public RM_ViewAssignmentsCompController(){
        
 	  strExportURL = RM_OtherService.getJQExportURL();	 
 	  searchAssignmentNote = RM_OtherService.getViewAssignmentSearchNote();
      bBatchEdit = false;
      string unit = RM_OtherService.getUserDefaultUnit(UserInfo.getUserId());
     
      if (unit == 'FTE')
      	bVisHr = false;
      else
      	bVisHr = true;
      bActive = true;
      renderDialog= false;
      bRefresh = false;

        system.debug('---------recordID---------'+recordID  ); 
        system.debug('-------displayType------'+displayType);  
      if (displayType=='Project'){          
         strTitle='Assignments & Availability';        
      }
      else
      	  strTitle='Assignments';
       strCheck = 'View Assignments in Hours';
       assNewStartDate = RM_Tools.getFormattedDate(Date.today(), 'mm/dd/yyyy');
       rejectDate = RM_Tools.getFormattedDate(Date.today(), 'mm/dd/yyyy');
      	
    }
    private void init(){ 
      
        system.debug('---------recordID---------'+recordID  ); 
        system.debug('-------displayType------'+displayType); 
        
        if (bRMAdmin == null)
        	bRMAdmin = RM_OtherService.IsRMAdmin(UserInfo.getUserId());
	     	system.debug('------bRMAdmin-----'+bRMAdmin ); 
	   	  system.debug('------selectedBusinessUnit first-----'+selectedBusinessUnit );
	    if (!bRefresh){
    	  if (selectedBusinessUnit == null)
    	  {
	     	  stManagedBU = RM_OtherService.GetUserGroupBU(UserInfo.getUserId());  
	   		  system.debug('---------ManagedBU---------'+stManagedBU);
		      if (stManagedBU != null && stManagedBU.size() > 0){
		                lstManagedBU = new list<string>();                
		                lstManagedBU.addAll(stManagedBU);
		                 selectedBusinessUnit = lstManagedBU[0];
		      }
	         else	         
	            selectedBusinessUnit = '';	         
    	  }
	    }  
        system.debug('------selectedBusinessUnit controller-----'+selectedBusinessUnit );
      
        if (stManagedRole == null)
        	stManagedRole = RM_OtherService.getUserManagedRole(UserInfo.getUserId()) ;  
	   
	  	
		monthLab = getMonthList();
		
	    if (IDType== 'Resource'){
	    	if (recordID != null){
	    		avaMap = RM_AssignmentsService.getAvailabilityMapByEmployee(recordID, monthLab);
	    	}
	    }
	    if (RM_OtherService.IsRMAdmin(UserInfo.getUserId()))
			bNewAssignment = true;    
	 
    }   
    public string getDataFieldsDefination(){	
    	
	  	system.debug('---------getDataFieldsDefination---------');	 	            
	  	RM_JsonJQWidgetsUtils jSon = new RM_JsonJQWidgetsUtils('jqxGrid');
		json.startJsonStr();
		json.createDataFiledDefinitionStr(PROJECT, 'string');
		if (displayType=='Project'){
			
			json.createDataFiledDefinitionStr(SPONSOR, 'string');
		}
		else if (displayType == 'Resource'){
			json.createDataFiledDefinitionStr(EE_NAME, 'string');
			json.createDataFiledDefinitionStr(BU, 'string');
			json.createDataFiledDefinitionStr(REQUEST_ID, 'string');
		}
		else if (displayType == 'Both'){
			json.createDataFiledDefinitionStr(SPONSOR, 'string');
			json.createDataFiledDefinitionStr(EE_NAME, 'string');
			json.createDataFiledDefinitionStr(BU, 'string');
		}
		json.createDataFiledDefinitionStr(LEAD, 'string');
		json.createDataFiledDefinitionStr(ROLE, 'string');
		json.createDataFiledDefinitionStr(PSSV, 'string');
		json.createDataFiledDefinitionStr(UNBL, 'string');
		json.createDataFiledDefinitionStr(COUNTRY, 'string');
		json.createDataFiledDefinitionStr(STATUS, 'string');
		json.createDataFiledDefinitionStr(TYPE, 'string');
		json.createDataFiledDefinitionStr(STARTDATE, 'date');
		json.createDataFiledDefinitionStr(ENDDATE, 'date');
		json.createDataFiledDefinitionStr(NOTE, 'string');
		
		json.createDataFiledDefinitionStr(EDIT_LINK, 'string');
		json.createDataFiledDefinitionStr(EDIT_NOTE, 'string');
		json.createDataFiledDefinitionStr(HISTORY, 'string');
		json.createDataFiledDefinitionStr(AID, 'string');	
		json.createDataFiledDefinitionStr(RESOURCE_ID, 'string');
		json.createDataFiledDefinitionStr(PROJECT_ID, 'string');	
			
		// Add assignment year month
		if (monthLab == null)
			monthLab = getMonthList();
		if (monthLab != null && monthLab.size() > 0){
			for (string m: monthLab){
				json.createDataFiledDefinitionStr(m, 'number'); 
			}
		}
		
		json.endJsonStr();	
		return json.getJson();
	}
	private list<string> getMonthList(){
		list<string> monthList = new list<string>();
		if (viewStartMonth == null)
			viewStartMonth = RM_Tools.GetYearMonth(startMonth);
		else
			startMonth = RM_Tools.GetMonthNumFromNow(viewStartMonth);
		if (viewEndMonth == null)
			viewEndMonth = RM_Tools.GetYearMonth(endMonth);
		else
			endMonth = RM_Tools.GetMonthNumFromNow(viewEndMonth);
		if (endMonth != -1)
			monthList = RM_Tools.GetMonthsList(startMonth, endMonth);
		else{
		 	if (recordID != null){
				if (IDType=='Project'){
					monthList = RM_AssignmentsService.getMonthListByProjectID(recordID, startMonth);
				}
				else if (IDType == 'Resource'){
					monthList = RM_AssignmentsService.getMonthListByEmployeeRID(recordID, startMonth);
				}
				else if (IDType == 'Manager'){
					monthList = RM_AssignmentsService.getMonthListBySupervisorID(recordID, startMonth);
				}
				if (monthList.size() < endMonth-startMonth + 1)
					monthList = RM_Tools.GetMonthsList(startMonth, 5);
			}
			
		}
	
    	return monthList;
	}
	public string getActAssignment(){
		system.debug('---------recordID---------'+recordID  ); 
        system.debug('-------displayType------'+displayType);  
		system.debug('---------getActAssignment---------');	 
		init();
		bNewAssignment = false; 
		RM_JsonJQWidgetsUtils jSonU = new RM_JsonJQWidgetsUtils('jqxGrid');
		jSonU.startJsonStr();
		if (displayType=='Project'){
            if (recordID != null){  
            	//system.debug('---------assignment---------' + RM_AssignmentsService.getActiveAssignmentDataByEmployeeRID(recordID, startMonth, endMonth));
            	string bu;
            	for (WFM_employee_Allocations__c a : RM_AssignmentsService.getActiveAssignmentDataByEmployeeRID(recordID, startMonth, endMonth, bActive)){
            			
            		jSonU= createOneRecord(a,jSonU);
            		if (bu == null)
            			bu = a.employee_id__r.Business_unit_desc__c;
            	}
            	if (bu != null && stManagedBU != null){            		
            		if (stManagedBU.contains(bu))
		   				bNewAssignment = true;
            	}
            	
            }
		}
		else if (displayType == 'Resource'){
			if (IDType == 'Project'){
	            if (recordID != null || recordID!=''){
	            	string projectStatus;
	            	for (WFM_employee_Allocations__c a : RM_AssignmentsService.getActiveAssignmentByProject(recordID, selectedBusinessUnit, bEdit, startMonth, endMonth, bActive)){
	            		jSonU= createOneRecord(a,jSonU);
	            		if (projectStatus == null)
	            			projectStatus = a.project_id__r.status_desc__c;
	            	}
	            	if (editableStatus.contains(projectStatus))
	            		bNewAssignment = true;   
	            	else
	            		bNewAssignment = false;         
				}
			}
		}
		else {
			if (IDType == 'Manager'){
				if (recordID != null || recordID!=''){
	            	for (WFM_employee_Allocations__c a : RM_AssignmentsService.getActiveAssignmentBySupervisorID(recordID, startMonth, endMonth, bActive)){
	            		jSonU= createOneRecord(a,jSonU);
	            	}
	            	bNewAssignment = false;	            
				}
			}
			else if (IDType == 'Allocations'){	
				
				if (recordID != null && recordID!=''){				
					if (recordID.length() > 0){
						list<WFM_employee_Allocations__c> allList = (List<WFM_employee_Allocations__c>)JSON.deserialize(recordID, List<WFM_employee_Allocations__c>.class);			
						list<string> aIDList = new list<string> ();
						for (WFM_employee_Allocations__c a: allList){
							aIDList.add(a.id);
						}
						system.debug('---------aIDList---------' +  aIDList);	 
						for (WFM_employee_Allocations__c a : RM_AssignmentsService.getAssignmentByAllocations(aIDList, startMonth, endMonth)){
		            		jSonU= createOneRecord(a,jSonU);
		            	}
					}
					bNewAssignment = false;
				}
			}
		}
		jSonU.endJsonStr();
		return jSonU.getJson();
	}

	private RM_JsonJQWidgetsUtils createOneRecord(WFM_employee_Allocations__c a, RM_JsonJQWidgetsUtils jSon ){
		json.startNextRow();
		//Edit permission:  cannot edit when prject status is closed or lost except RMAdmin
		// if IDType = 'Resource' cannot edit it will based on managed BU
		// IDType == 'Manager' or 'Allocations'  Both applied
		string editlink = 'false';
		if (bRMAdmin)
			editlink = 'true';
		else{
			if (editableStatus.contains(a.project_id__r.Status_Desc__c)){
				//editlink = 'true';
				if (stManagedRole.contains(a.project_function__c))//managed role
					editlink = 'true';
			}
		}
		string AssignHis = 'Created Date : ' + string.ValueOf(a.CreatedDate) + '\\nCreated By : ' + a.CreatedBy.name
             + '\\nLast Modified Date : ' + string.ValueOf(a.LastModifiedDate) + '\\nLast Modified By : ' + a.LastModifiedBy.name; 
       
		json.addOneStrColumn(EDIT_LINK, editlink);
		json.addOneStrColumn(HISTORY, AssignHis);
		if (stManagedRole.contains(a.project_function__c)){
			json.addOneStrColumn(EDIT_NOTE, 'true');
			if (a.Note__c != null){	
				 	
				/* string noteStr = a.Note__c.replaceAll('\n', ' ');
				
				 for (integer i = 0; i< 5; i++){
				 	noteStr = noteStr.replaceAll('\r', ' ');
				  	noteStr = noteStr.replaceAll('\n', ' ');
				 }*/
				//noteStr = noteStr.replaceAll('\\', '');
				system.debug('---------note---------' + a.Note__c);	
				json.addOneStrColumn(NOTE, RM_Tools.escapeStringforJson(a.Note__c));	
			}
			else
				json.addOneStrColumn(NOTE, '');		
			}
		else{
			json.addOneStrColumn(NOTE, '');
			json.addOneStrColumn(EDIT_NOTE, 'false');
		}
		json.addOneStrColumn(AID, a.id);
		json.addOneStrColumn(RESOURCE_ID, a.employee_id__c);
		json.addOneStrColumn(PROJECT_ID, a.project_id__c);
		json.addOneStrColumn(PROJECT, a.project_id__r.name);
		if (displayType=='Project'){            
			
			json.addOneStrColumn(SPONSOR, a.project_id__r.Contract_id__r.Client_ID__r.name);
		}
		else if(displayType == 'Resource'){
			json.addOneStrColumn(EE_NAME, a.employee_id__r.full_name__c);
			json.addOneStrColumn(BU, a.employee_id__r.Business_unit_desc__c);
			if (a.Request__r.Request_ID__c == null)
				json.addOneStrColumn(REQUEST_ID, '');
			else
				json.addOneStrColumn(REQUEST_ID, a.Request__r.Request_ID__c);
		}
		else if (displayType == 'Both'){			
			json.addOneStrColumn(SPONSOR, a.project_id__r.Contract_id__r.Client_ID__r.name);
			json.addOneStrColumn(EE_NAME, a.employee_id__r.full_name__c);
			json.addOneStrColumn(BU, a.employee_id__r.Business_unit_desc__c);		
			
		}
		json.addOneStrColumn(LEAD, (a.Is_lead__c? 'Y': ''));
		json.addOneStrColumn(ROLE, a.project_function__c);
		json.addOneStrColumn(PSSV, (a.Is_IEDR__c? 'Y': ''));
		json.addOneStrColumn(UNBL, (a.Is_Unblinded__c? 'Y':''));
		json.addOneStrColumn(COUNTRY, a.Project_Country__c);
		json.addOneStrColumn(STATUS, a.status__c);
		json.addOneStrColumn(TYPE, a.Type__c);
		json.addOneStrColumn(STARTDATE, RM_Tools.GetStringfromDate(a.Allocation_Start_Date__c, 'mm/dd/yyyy'));
		json.addOneStrColumn(ENDDATE, RM_Tools.GetStringfromDate(a.Allocation_End_Date__c,  'mm/dd/yyyy'));
	 	    
		if (a.Assignments__r!= null){
			List<WFM_Employee_Assignment__c> assignsList = a.Assignments__r;
		
			for (WFM_Employee_Assignment__c e:assignsList){
				if (e.Is_Count__c){
					if (bVisHr){					
						json.addOneStrColumn(e.Year_month__c, string.valueOf(e.hours__c));
					}
					else{					
						json.addOneStrColumn(e.Year_month__c, string.valueOf(e.FTE__c));
					}
				}
			}
		}
		json.endRow();
		return jSon;
	}
	
	public string getColumnDefination(){
		if (displayType == 'Project'){
	  		 return getProjectSecondHeader() + ', columngroups: ' + getProjectFirstHeader();
	  	}
	  	
	  	else {
	  		return 	getProjectColumnDefination();
	  	}
	  	return null;
	}
	public string getProjectColumnDefination(){ // only for display with Resource and both
		searchAssignmentNote = RM_OtherService.getViewAssignmentSearchNote();
		 system.debug('---------searchAssignmentNote---------' + searchAssignmentNote);	 
		RM_JsonJQWidgetsUtils jSon = new RM_JsonJQWidgetsUtils('jqxGrid');
		json.startJsonStr();
		if (monthLab.size() < 6) //not pinned
		{
			if (bNewAssignment)
				json.createColumnFieldDefinitionStr(EDIT_LINK, EDIT_LINK, 'width:25, filterable: false, sortable: false,cellsrenderer: imagerenderer, renderer: imageHeaderrenderer ');
			else
				json.createColumnFieldDefinitionStr(EDIT_LINK, EDIT_LINK, 'width:25, filterable: false, sortable: false,cellsrenderer: imagerenderer ');
			json.createColumnFieldDefinitionStr(HISTORY, HISTORY, 'width:30, filterable: false, sortable: false,cellsrenderer: histRenderer ');
			if (IDType!='Allocations' )
				json.createColumnFieldDefinitionStr(NOTE, NOTE, 'width:32, filterable: false, sortable: false,cellsrenderer: noteRenderer ');
			else if (searchAssignmentNote == '1')
				json.createColumnFieldDefinitionStr(NOTE, NOTE, 'width:32, filterable: false, sortable: false,cellsrenderer: noteRenderer ');
			if (displayType=='Resource'){ 			
				json.createColumnFieldDefinitionStr(EE_NAME, EE_NAME, 'width:120, cellsrenderer: namerenderer');
			}
			else if (displayType == 'Project'){
				json.createColumnFieldDefinitionStr(PROJECT, PROJECT, 'width:125, cellsrenderer: projectrenderer ' ); 
				json.createColumnFieldDefinitionStr(SPONSOR, SPONSOR, 'width:100 ');
			}
			
			else if (displayType == 'Both'){			
				json.createColumnFieldDefinitionStr(PROJECT, PROJECT, 'width:125, cellsrenderer: projectrenderer ' ); 
				json.createColumnFieldDefinitionStr(SPONSOR, SPONSOR, 'width:100 ');
				json.createColumnFieldDefinitionStr(EE_NAME, EE_NAME, 'width:120, cellsrenderer: namerenderer');
			}
			json.createColumnFieldDefinitionStr(LEAD, LEAD, 'width:35, cellsalign: "center"');
			json.createColumnFieldDefinitionStr(ROLE, ROLE, 'width:100');
			json.createColumnFieldDefinitionStr(PSSV, PSSV, 'width:35, cellsalign: "center"');
			json.createColumnFieldDefinitionStr(UNBL, UNBL, 'width:35, cellsalign: "center"' );
			json.createColumnFieldDefinitionStr(COUNTRY, COUNTRY, 'width:100');
		
		}
		else{
			if (bNewAssignment)
				json.createColumnFieldDefinitionStr(EDIT_LINK, EDIT_LINK, 'width:25, pinned: true, filterable: false, sortable: false,cellsrenderer: imagerenderer, renderer: imageHeaderrenderer ');
			else
				json.createColumnFieldDefinitionStr(EDIT_LINK, EDIT_LINK, 'width:25, pinned: true, filterable: false, sortable: false,cellsrenderer: imagerenderer');
			json.createColumnFieldDefinitionStr(HISTORY, HISTORY, 'width:30, pinned: true, filterable: false, sortable: false,cellsrenderer: histRenderer ');
			if (IDType!='Allocations' )
				json.createColumnFieldDefinitionStr(NOTE, NOTE, 'width:32, pinned: true, filterable: false, sortable: false,cellsrenderer: noteRenderer ');
			else if (searchAssignmentNote == '1')
				json.createColumnFieldDefinitionStr(NOTE, NOTE, 'width:32, pinned: true, filterable: false, sortable: false,cellsrenderer: noteRenderer ');
			if (displayType=='Resource'){ 			
				json.createColumnFieldDefinitionStr(EE_NAME, EE_NAME, 'width:120,pinned: true, cellsrenderer: namerenderer');
			}
			else if (displayType == 'Project'){
				json.createColumnFieldDefinitionStr(PROJECT, PROJECT, 'width:125, pinned: true, cellsrenderer: projectrenderer ' ); 
				json.createColumnFieldDefinitionStr(SPONSOR, SPONSOR, 'width:100,pinned: true ');
			}
			else if (displayType == 'Both'){			
				json.createColumnFieldDefinitionStr(PROJECT, PROJECT, 'width:125, pinned: true , cellsrenderer: projectrenderer' ); 
				json.createColumnFieldDefinitionStr(SPONSOR, SPONSOR, 'width:100,pinned: true ');
				json.createColumnFieldDefinitionStr(EE_NAME, EE_NAME, 'width:120,pinned: true, cellsrenderer: namerenderer');
			}
			json.createColumnFieldDefinitionStr(LEAD, LEAD, 'width:35, pinned: true,cellsalign: "center"');
			json.createColumnFieldDefinitionStr(ROLE, ROLE, 'width:100, pinned: true ');
			json.createColumnFieldDefinitionStr(PSSV, PSSV, 'width:35, pinned: true , cellsalign: "center"');
			json.createColumnFieldDefinitionStr(UNBL, UNBL, 'width:35, pinned: true, cellsalign: "center"' );
			json.createColumnFieldDefinitionStr(COUNTRY, COUNTRY, 'width:100, pinned: true ');
			
		}
		json.createColumnFieldDefinitionStr(STATUS, STATUS, 'width:50');
		json.createColumnFieldDefinitionStr(TYPE, TYPE, 'width:50');
		json.createColumnFieldDefinitionStr(STARTDATE, STARTDATE, 'width:70,cellsformat: "d"');
		json.createColumnFieldDefinitionStr(ENDDATE, ENDDATE, 'width:70, cellsformat: "d"');
		if (bEdit)
			json.createColumnFieldDefinitionStr(REQUEST_ID, REQUEST_ID, 'width:70');
		if (IDType=='Project' || IDType=='Allocations' ||IDType == 'Manager'){ 		
			string total = 'aggregatesrenderer: function (aggregates, column, element) {  '+                        
				'var renderstring = "<div class=\'jqCell\' >Total  </>";   ' +                      
				'return renderstring;}';	
			json.createColumnFieldDefinitionStr(BU, BU, 'width:100, ' + total);
		}
	
		// Add assignment year month
		
		if (monthLab != null && monthLab.size() > 0)
		{
			string formatStr = 'd2';
			if (this.bVisHr){
				formatStr = 'd1';
			}
			if (monthLab.size() < 10){
				for (string m: monthLab){
					if (RM_Tools.GetDatefromYearMonth(m) < RM_Tools.GetFirstDayDatefromDate(Date.today()))
						
						json.createColumnFieldDefinitionStr(m, m, 'cellsalign: "right", filterable: false, cellsrenderer: dateRenderer, renderer: headerRenderer, cellsformat: "' + formatStr +'", align: "center", aggregates: ["sum"], aggregatesrenderer: sumRenderer');
					
					else
						json.createColumnFieldDefinitionStr(m, m, 'cellsalign: "right", filterable: false, cellsformat: "' + formatStr +'", align: "center", aggregates: ["sum"], aggregatesrenderer: sumRenderer');
				}
			}
			else{
				for (string m: monthLab){
					if (RM_Tools.GetDatefromYearMonth(m) < RM_Tools.GetFirstDayDatefromDate(Date.today()))
						json.createColumnFieldDefinitionStr(m, m, 'width:50, cellsalign: "right", cellsrenderer: dateRenderer, renderer: headerRenderer, filterable: false, cellsformat: "' + formatStr+'",  align: "center", aggregates: ["sum"], aggregatesrenderer: sumRenderer'); //cellclassname: "alignRightItalicFTE",
					else
						json.createColumnFieldDefinitionStr(m, m, 'width:50, cellsalign: "right", filterable: false, cellsformat: "' + formatStr + '", align: "center", aggregates: ["sum"], aggregatesrenderer: sumRenderer');
				}
			}
			
			
		}
		
		json.endJsonStr();	   
		return json.getJson();
		
		
	}
	
	public string getProjectSecondHeader(){
		system.debug('---------getColumnDefination---------');	 
		RM_JsonJQWidgetsUtils jSon = new RM_JsonJQWidgetsUtils('jqxGrid');
		json.startJsonStr();
		if (monthLab.size() <6){ // no pinned
			if (bNewAssignment)
				json.createColumnFieldDefinitionStr(EDIT_LINK, EDIT_LINK, 'width:25, filterable: false, sortable: false, cellsrenderer: imagerenderer, renderer: imageHeaderrenderer ');
			else
				json.createColumnFieldDefinitionStr(EDIT_LINK, EDIT_LINK, 'width:25, filterable: false, sortable: false, cellsrenderer: imagerenderer ');
				json.createColumnFieldDefinitionStr(HISTORY, HISTORY, 'width:30,  filterable: false, sortable: false, cellsrenderer: histRenderer ');
			json.createColumnFieldDefinitionStr(NOTE, NOTE, 'width:32, filterable: false, sortable: false, cellsrenderer: noteRenderer ');
		
		
			if (displayType=='Project'){ 		
				json.createColumnFieldDefinitionStr(PROJECT, PROJECT, 'width:130, cellsrenderer: projectrenderer' ); 			
				json.createColumnFieldDefinitionStr(SPONSOR, SPONSOR, 'width:100');
			}
			
			
			json.createColumnFieldDefinitionStr(LEAD, LEAD, 'width:35, cellsalign: \'center\'');
			json.createColumnFieldDefinitionStr(ROLE, ROLE, 'width:100' );
			json.createColumnFieldDefinitionStr(PSSV, PSSV, 'width:35, cellsalign: \'center\'');
			json.createColumnFieldDefinitionStr(UNBL, UNBL, 'width:35, cellsalign: \'center\'');
			json.createColumnFieldDefinitionStr(COUNTRY, COUNTRY, 'width:100');
		}
		else{
			if (bNewAssignment)
				json.createColumnFieldDefinitionStr(EDIT_LINK, EDIT_LINK, 'width:25, pinned: true, filterable: false, sortable: false, cellsrenderer: imagerenderer, renderer: imageHeaderrenderer ');
			else
				json.createColumnFieldDefinitionStr(EDIT_LINK, EDIT_LINK, 'width:25, pinned: true, filterable: false, sortable: false, cellsrenderer: imagerenderer');
			json.createColumnFieldDefinitionStr(HISTORY, HISTORY, 'width:30, pinned: true,  filterable: false, sortable: false, cellsrenderer: histRenderer ');
			json.createColumnFieldDefinitionStr(NOTE, NOTE, 'width:32, pinned: true, filterable: false, sortable: false, cellsrenderer: noteRenderer ');
			
		
			if (displayType=='Project'){ 		
				json.createColumnFieldDefinitionStr(PROJECT, PROJECT, 'width:130, pinned: true, cellsrenderer: projectrenderer ' ); 			
				json.createColumnFieldDefinitionStr(SPONSOR, SPONSOR, 'width:100,pinned: true');
			}
			
			
			json.createColumnFieldDefinitionStr(LEAD, LEAD, 'width:35, pinned: true, cellsalign: \'center\'');
			json.createColumnFieldDefinitionStr(ROLE, ROLE, 'width:100, pinned: true');
			json.createColumnFieldDefinitionStr(PSSV, PSSV, 'width:35, pinned: true, cellsalign: \'center\'');
			json.createColumnFieldDefinitionStr(UNBL, UNBL, 'width:35, pinned: true, cellsalign: \'center\'');
			json.createColumnFieldDefinitionStr(COUNTRY, COUNTRY, 'width:100, pinned: true');
		}
		string title = 'aggregatesrenderer: function (aggregates, column, element) {  '+                        
				'var renderstring = "<div style=\'position: relative; margin-top: 5px; text-align: left; overflow: hidden;\' > &nbsp;Non Billable <Hr> &nbsp; Availability   </>";   ' +                      
				'return renderstring;}';
		json.createColumnFieldDefinitionStr(STATUS, STATUS, 'width:50, columngroup:"' + STATUS + '0"');
		json.createColumnFieldDefinitionStr(TYPE, TYPE, 'width:50, columngroup:"' + TYPE + '0"');
		json.createColumnFieldDefinitionStr(STARTDATE, STARTDATE, 'width:70, cellsformat: \'d\', columngroup:"' + STARTDATE + '0"');
		json.createColumnFieldDefinitionStr(ENDDATE, ENDDATE, 'width:70, cellsformat: \'d\', columngroup:"' + ENDDATE + '0",'+ title);
		
		// Add assignment year month
		integer i = 0;
		if (monthLab == null)
			monthLab = getMonthList();			
		system.debug('---------monthLab---------' +monthLab);	 
		if (monthLab != null && monthLab.size() > 0){				
			
			for (string m: monthLab){
				if (IDType=='Resource'){
					string totalAval = '0.0';
					string adjAva = '0.0';
					string totalNonAavl = '0.0';
					string nonBilable = '0.0';
					string grossAval = '';	
					string aval = '';
					string adjAval = '';
					if (avaMap == null)
						avaMap = RM_AssignmentsService.getAvailabilityMapByEmployee(recordID, monthLab);				
					if (avaMap != null && avaMap.containsKey(m)){	
						
						WFM_Employee_Availability__c ava = (WFM_Employee_Availability__c)avaMap.get(m);
						system.debug('---------ava---------' +ava);	
						system.debug('---------bVisHr---------' +bVisHr);	
						if (bVisHr){
							totalAval = string.valueOf(ava.Available_Hours__c);
							adjAva = string.valueOf(ava.Adjusted_Available_Hours__c);
							nonBilable = string.valueOf(ava.Total_Non_Available_Hrs__c);
							decimal gAval = ava.employee_id__r.FTE_Equivalent__c * ava.total_working_hour__c;
							grossAval = string.valueOf(gAval.setscale(1));
						}
						else{
							totalAval = string.valueOf(ava.Available_FTE__c);
							adjAva = string.valueOf(ava.Adjusted_Available_FTE__c);
							nonBilable = string.valueOf(ava.Total_Non_Available_FTE__c);
							decimal gAval =ava.employee_id__r.FTE_Equivalent__c;
							grossAval = string.valueOf(gAval.setscale(2));
						}
						system.debug('---------totalAval---------' +ava.Available_FTE__c);	
						system.debug('---------nonBilable---------' +nonBilable);
						system.debug('---------grossAval---------' +grossAval);	
					}
					if (RM_Tools.GetDatefromYearMonth(m) < RM_Tools.GetFirstDayDatefromDate(Date.today())){
						totalNonAavl = '<label class= alignRightItalicHr>' + nonBilable + '</label>';
						if (totalAval != ''){
							if (decimal.valueOf(adjAva) < 0) // red)
								adjAval = '<label class= alignRightItaNegBold>' + adjAva + '</label>';
							else
								adjAval = '<label class= alignRightItalicBold>' + adjAva + '</label>';
							/*if (decimal.valueOf(totalAval) < 0){ // red
								
								
								aval =   '<Hr><label class= alignRightItaNegBold>'+totalAval+' </label>';
							}
							else
								aval =  '<Hr> <label class= alignRightItalicBold>'+totalAval+' </label>';*/
						}
						
					}
					else{
						totalNonAavl = nonBilable;
						if (totalAval != ''){
							if (decimal.valueOf(adjAva) < 0) // red)
								adjAval = '<label class= alignRightNegBold>' + adjAva + '</label>';
							else
								adjAval = '<label class= alignRightBold>' + adjAva + '</label>';
							/*if (decimal.valueOf(totalAval) < 0){ // red
								aval = '<Hr><label class= alignRightNegBold>'+totalAval+' </label>';
							}
							else
								aval = '<Hr><label class= alignRightBold>'+totalAval+' </label>';*/
						}
						
					}
					
					string agg = 'aggregatesrenderer: function (aggregates, column, element) {  '+                        
					'var renderstring = "<div style=\'position: relative; margin-top: 5px; margin-right: 2px; text-align: right; overflow: hidden;\'> ' 
					+ totalNonAavl + '<Hr>' + adjAval +'</>";   ' +                      
					'return renderstring;}';
					/*'aggregates: [{ '+                            
						'function (aggregatedValue, currentValue, column, record) {' +                                
							'var total = {!availableFTE[' + i + ']};'  +                                
							' return aggregatedValue + total;' +
							'}' +                      
						'}] ';*/
					if (monthLab.size() <10){
						if (RM_Tools.GetDatefromYearMonth(m) < RM_Tools.GetFirstDayDatefromDate(Date.today()))
							json.createColumnFieldDefinitionStr(m + ' (' + grossAval + ')', m, 'cellsalign: \'right\', filterable: false, cellsformat: \'d2\', renderer: headerTooltipRenderer, cellsrenderer: dateRenderer, columngroup:"' + m+'0",'  + agg );
						else
							json.createColumnFieldDefinitionStr(m + ' (' + grossAval + ')', m, 'cellsalign: \'right\', filterable: false, renderer: headerTooltipRenderer, columngroup:"' + m+'0", cellsformat: \'d2\',' + agg  );
						
					}
					else{
						if (RM_Tools.GetDatefromYearMonth(m) < RM_Tools.GetFirstDayDatefromDate(Date.today()))
							json.createColumnFieldDefinitionStr( m + ' (' + grossAval + ')', m,  'width:50, cellsalign: \'right\', filterable: false, cellsformat: \'d2\', renderer: headerTooltipRenderer, cellsrenderer: dateRenderer, columngroup:"' + m+'0",' + agg );
						else
							json.createColumnFieldDefinitionStr(m + ' (' + grossAval + ')', m, 'width:50, cellsalign: \'right\', filterable: false, renderer: headerTooltipRenderer, columngroup:"' + m+'0", cellsformat: \'d2\',' + agg  );
					}
					
				}
				
				i++;
			}
		}
		json.endJsonStr();	   
		return json.getJson();
		
	}
	
	
	public string getProjectFirstHeader(){ //real header for project display
		/* columngroups:                 [                  
		{ text: 'Product Details', align: 'center', name: 'ProductDetails' },                  
		{ text: 'Order Details', parentgroup: 'ProductDetails', align: 'center', name: 'OrderDetails' },                  
		{ text: 'Location', align: 'center', name: 'Location' }                ]*/
		RM_JsonJQWidgetsUtils jSon = new RM_JsonJQWidgetsUtils('jqxGrid');
		json.startJsonStr();		
		
		// Add assignment year month
		if (monthLab != null && monthlab.size() > 0){
		
			for (string m: monthLab){
				if (RM_Tools.GetDatefromYearMonth(m) < RM_Tools.GetFirstDayDatefromDate(Date.today()))
					json.createColumnGroupDefinitionStr(m, m+'0', 'renderer: headerRenderer, align: \'center\'');
				else
					json.createColumnGroupDefinitionStr(m, m+'0', 'align: \'center\'');
			}
		}
		json.endJsonStr();	
		return json.getJson();
	}
	
	
  
    public List<SelectOption>getBusinessUnit(){
        //transient List<SelectOption> options = RM_LookUpDataAccess.getBU(true);
        transient List<SelectOption> options = RM_LookUpDataAccess.getUserEditableBU(true, bEdit,UserInfo.getUserId()); 
        return options;
    } 
    public List<SelectOption>getRejectReason(){
        //transient List<SelectOption> options = RM_LookUpDataAccess.getBU(true);
        transient List<SelectOption> options = RM_LookUpDataAccess.GetRejectReason(false); 
        return options;
    } 
    /*public PageReference SaveNote(){
        if (selNote != null){
            if (selAlloc.Note__c != selNote){ 
                 
                selAlloc.Note__c = selNote;
                update selAlloc;     
            }
        }
        return null;
    
    }*/
    public PageReference refreshData(){
    	system.debug('---------refreshData---------');	
    	bRefresh = true;
    	return null;
    }
   
    
   @RemoteAction
   global static void SaveNote(string aID, string note){
   		List<WFM_employee_Allocations__c>allocList = RM_AssignmentsService.getAllocationNoteByRID(aID);
   		system.debug('---------allocList---------' + allocList);	 
   		if (allocList.size() > 0){
   			allocList[0].note__c = note;
   			RM_AssignmentsService.UpdateListObject(allocList);
   		}
   }
   //batch Edit
   /* public PageReference EnableEdit(){
    	
    	bBatchEdit = true;
    	
    	return null;
    }
    public PageReference EnableViewOnly(){
    	
    	bBatchEdit = false;
    	return null;
    }*/
  	public PageReference UpdateAssignment(){
  		return null;
  	}
   @RemoteAction
   global static void saveBatchNote(string aIDs, string note){
   		system.debug('---------aIDs---------' + aIDs);	
   		system.debug('---------note---------' + note);	
   		list<string> lstID = RM_ViewAssignmentsCompController.getListIdfromString(aIDs);
   		system.debug('---------lstID---------' + lstID);	
   		 
   		if (lstID.size() > 0){   			
   			RM_AssignmentsService.SaveBatchNote(lstID, note);
   		}
   }
   @RemoteAction
   global static void saveRequestID(string aIDs, string RequestID, boolean bOverwrite){
   		list<string> lstID = RM_ViewAssignmentsCompController.getListIdfromString(aIDs);
   		
   		system.debug('---------lstID---------' + lstID);
   		if (lstID.size() > 0){   			
   			RM_AssignmentsService.SaveBatchRequest(lstID, RequestID, bOverwrite);
   		}   		
   }
   
   @RemoteAction
   global static void saveExtAssignment(string aIDs, string endDate){
   		list<string> lstID = RM_ViewAssignmentsCompController.getListIdfromString(aIDs);
   		Date toDate = RM_Tools.GetDateFromString(endDate, 'mm/dd/yyyy');
   		system.debug('---------lstID---------' + lstID);
   		if (lstID.size() > 0){   			
   			RM_AssignmentsService.extBatchAssignment(lstID, toDate);
   		}   		
   }
   
   @RemoteAction
   global static void saveEndAssignment(string aIDs, string endDate){
   		list<string> lstID = RM_ViewAssignmentsCompController.getListIdfromString(aIDs);
   		Date toDate = RM_Tools.GetDateFromString(endDate, 'mm/dd/yyyy');
   		system.debug('---------lstID---------' + lstID);
   		if (lstID.size() > 0){   			
   			RM_AssignmentsService.endAssignment(lstID, toDate); 
   		}   		
   }
   @RemoteAction
   global static void saveDelayStart(string aIDs, integer delayMonth){
   		list<string> lstID = RM_ViewAssignmentsCompController.getListIdfromString(aIDs);
   		
   		system.debug('---------lstID---------' + lstID);
   		if (lstID.size() > 0){   			
   			RM_AssignmentsService.delayBatchAssignment(lstID, delayMonth); 
   		}   		
   }
   @RemoteAction
   global static void saveAdjustAllocation(string aIDs, decimal assignValue, string assignUnit, string startDate, string endDate, Boolean bProjectEnd){
   		list<string> lstID = RM_ViewAssignmentsCompController.getListIdfromString(aIDs);
   		 
   		system.debug('---------lstID---------' + lstID);
   		Date fromDate = RM_Tools.GetDateFromString(startDate, 'mm/dd/yyyy');
   		Date toDate;
   		if (endDate != null)
   			toDate= RM_Tools.GetDateFromString(endDate, 'mm/dd/yyyy');
   		if (lstID.size() > 0){
   						
   			RM_AssignmentsService.editBatchAssignment(lstID, assignValue, assignUnit, fromDate, toDate, bProjectEnd); 
   		}   		
   }
   @RemoteAction
   global static void savePushAssignment(string aIDs, integer pushMonth){
   		list<string> lstID = RM_ViewAssignmentsCompController.getListIdfromString(aIDs);
   		
   		system.debug('---------lstID---------' + lstID);
   		if (lstID.size() > 0){   			
   			RM_AssignmentsService.pushBatchAssignment(lstID, pushMonth); 
   		}   		
   }
   @RemoteAction
   global static void saveRejectAssignment(string aIDs, string rejectReason, string effectDate){
   		list<string> lstID = RM_ViewAssignmentsCompController.getListIdfromString(aIDs);
   		Date toDate = RM_Tools.GetDateFromString(effectDate, 'mm/dd/yyyy');
   		system.debug('---------lstID---------' + lstID);
   		if (lstID.size() > 0){   			
   			RM_AssignmentsService.rejectAssignment(lstID, 'Rejected by ' + rejectReason,toDate); 
   		}   		
   }
    @RemoteAction
   global static void saveStatus(string aIDs){
   		list<string> lstID = RM_ViewAssignmentsCompController.getListIdfromString(aIDs);
   		
   		system.debug('---------lstID---------' + lstID);
   		if (lstID.size() > 0){   			
   			RM_AssignmentsService.updateBatchStatus(lstID, 'Confirmed'); 
   		}   		
   }
   
   @RemoteAction
   global static list<string> getListIdfromString(string sIDs){
 		
 		list<string> lstID = sIDs.split(',');
 		return lstID;
 	}
}