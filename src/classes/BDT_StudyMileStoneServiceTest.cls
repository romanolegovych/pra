/** Implements the test for the Service Layer of the object StudyMileStone__c
 * @author	Dimitrios Sgourdos
 * @version	28-Feb-2014
 */
@isTest
private class BDT_StudyMileStoneServiceTest {
	
	// Global variables
	private static List<Study__c>					studiesList;
	
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	25-Feb-2013
	 */
	static void init() {
		// Create project
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001', 'BDT Project First Experiments');
		insert currentProject;
		
		// Create studies
		studiesList = new List<Study__c>();
		studiesList.add( new Study__c(Project__c=currentProject.Id) );
		studiesList.add( new Study__c(Project__c=currentProject.Id) );
		insert studiesList;
	}
	
	
	/** Test the function getStudyMileStonesByStudy
	 * @author	Dimitrios Sgourdos
	 * @version	25-Feb-2014
	 */
	static testMethod void getStudyMileStonesByStudyTest() {
		// Create data
		init();
		
		List<StudyMileStone__c> source = new List<StudyMileStone__c>();
		source.add( new StudyMileStone__c(Study__c = studiesList[0].Id,	DueDate__c = Date.today()) );
		source.add( new StudyMileStone__c(Study__c = studiesList[0].Id, DueDate__c = Date.today() + 10) );
		source.add( new StudyMileStone__c(Study__c = studiesList[1].Id, DueDate__c = Date.today() + 20) );
		insert source;
		
		String errorMessage = 'Error in retrieving study milestones per study from the system';
		
		// Check the funstion (only the first two StudyMileStone must be retrieved and with the same order.
		// The third belong to another study)
		List<StudyMileStone__c> results = BDT_StudyMileStoneService.getStudyMileStonesByStudy(studiesList[0].Id);
		
		system.assertEquals(2, results.size(), errorMessage);
		system.assertEquals(source[0].Id, results[0].Id, errorMessage);
		system.assertEquals(source[1].Id, results[1].Id, errorMessage);
	}
	
	
	/** Test the function getStudyMileStoneById
	 * @author	Dimitrios Sgourdos
	 * @version	27-Feb-2014
	 */
	static testMethod void getStudyMileStoneByIdTest() {
		// Create data
		init();
		
		StudyMileStone__c source = new StudyMileStone__c(Study__c = studiesList[0].Id,	DueDate__c = Date.today());
		insert source;
		
		String errorMessage = 'Error in retrieving the StudyMileStone from the system';
		
		// Check with existing id
		StudyMileStone__c result = BDT_StudyMileStoneService.getStudyMileStoneById(source.Id);
		system.assertequals(source.Id, result.Id, errorMessage);
		
		// Check with not existing id
		result = BDT_StudyMileStoneService.getStudyMileStoneById(studiesList[0].Id);
		system.assertequals(NULL, result, errorMessage);
	}
	
	
	/** Test the function getStudyMileStonesByStudiesId
	 * @author	Dimitrios Sgourdos
	 * @version	28-Feb-2014
	 */
	static testMethod void getStudyMileStonesByStudiesIdTest() {
		// Create data
		init();
		
		List<StudyMileStone__c> source = new List<StudyMileStone__c>();
		source.add( new StudyMileStone__c(Study__c = studiesList[0].Id,	DueDate__c = Date.today()) );
		source.add( new StudyMileStone__c(Study__c = studiesList[0].Id,	DueDate__c = Date.today()+10) );
		source.add( new StudyMileStone__c(Study__c = studiesList[1].Id,	DueDate__c = Date.today()+20) );
		insert source;
		
		String errorMessage = 'Error in retrieving the StudyMileStones from the system';
		
		// Check without studies
		List<StudyMileStone__c> results = BDT_StudyMileStoneService.getStudyMileStonesByStudiesId(new List<String>());
		system.assertEquals(0, results.size(), errorMessage);
		
		// Check only with the first study selected
		List<String> studiesIds = new List<String> { studiesList[0].Id };
		results = BDT_StudyMileStoneService.getStudyMileStonesByStudiesId(studiesIds);
		
		system.assertEquals(2, results.size(), errorMessage);
		system.assertEquals(source[0].Id, results[0].Id, errorMessage);
		system.assertEquals(source[1].Id, results[1].Id, errorMessage);
		
		// Check with the first and second study selected
		studiesIds.add(studiesList[1].Id);
		results = BDT_StudyMileStoneService.getStudyMileStonesByStudiesId(studiesIds);
		
		system.assertEquals(3, results.size(), errorMessage);
		system.assertEquals(source[0].Id, results[0].Id, errorMessage);
		system.assertEquals(source[1].Id, results[1].Id, errorMessage);
		system.assertEquals(source[2].Id, results[2].Id, errorMessage);
	}
	
	
	/** Test the function getMileStonesOptionsForPaymentTerms
	 * @author	Dimitrios Sgourdos
	 * @version	28-Feb-2014
	 */
	static testMethod void getMileStonesOptionsForPaymentTermsTest() {
		// Create data
		init();
		
		MileStoneDefinition__c mlDef = new MileStoneDefinition__c(Name='Def 1', Category__c='Clinic', SequenceNumber__c=1);
		insert mlDef;
		
		List<StudyMileStone__c> source = new List<StudyMileStone__c>();
		source.add( new StudyMileStone__c(Name='Test1', Study__c = studiesList[0].Id,	DueDate__c = Date.today()) );
		source.add( new StudyMileStone__c(Name='Test2', Study__c = studiesList[0].Id,	DueDate__c = Date.today()+10) );
		source.add( new StudyMileStone__c(Name='Test3', Study__c = studiesList[1].Id,	DueDate__c = Date.today()+20) );
		source.add( new StudyMileStone__c(Name='Test4',
										Study__c = studiesList[1].Id,
										DueDate__c = Date.today()+30,
										MileStonedefinition__c=mlDef.Id) );
		source.add( new StudyMileStone__c(Name='Test5',
										Study__c = studiesList[1].Id,
										DueDate__c = Date.today()+40,
										MileStonedefinition__c=mlDef.Id) );
		insert source;
		
		String errorMessage = 'Error in creating the select options with StudyMileStones';
		
		// Check the function
		List<String> studiesIds = new List<String> { studiesList[0].Id, studiesList[1].Id };
		List<SelectOption> results = BDT_StudyMileStoneService.getMileStonesOptionsForPaymentTerms(studiesIds);
		
		system.assertEquals(5, results.size(), errorMessage);
		
		system.assertEquals('', results[0].getValue(), errorMessage);
		system.assertEquals('Please Select...', results[0].getLabel(), errorMessage);
		
		system.assertEquals('Test1', results[1].getValue(), errorMessage);
		system.assertEquals('Test1', results[1].getLabel(), errorMessage);
		
		system.assertEquals('Test2', results[2].getValue(), errorMessage);
		system.assertEquals('Test2', results[2].getLabel(), errorMessage);
		
		system.assertEquals('Test3', results[3].getValue(), errorMessage);
		system.assertEquals('Test3', results[3].getLabel(), errorMessage);
		
		system.assertEquals('Test4', results[4].getValue(), errorMessage);
		system.assertEquals('Test4', results[4].getLabel(), errorMessage);
	}
	
}