public class CCI_CrsInstanceController {

	public String instName { get; set; }
	public String URLValue { get; set; }
	public String SysName { get; set; }
	public String attributeKey { get;set;}
	public String praSys { get; set; }
	public String selectedInstanceID {get;set;}
	public Long instanceIDselected {get;set;}
	public String SearchInstanceName { get; set; }
	list<MdmMappingService.instanceVO> instaVOs { get; set; }
	public MdmMappingService.instanceVO renderInstanceVO {get;set;}
	public map<Long, MdmMappingService.instanceVO> instanceVOMap{get;set;}
	
	public list<MdmMappingService.instanceVO> getInstaVOs() {
		return instaVOs;
	}
	
	public void searchInstId() {
		ApexPages.getMessages().clear();
		instanceIDselected = Long.valueof(selectedInstanceID);
		renderInstanceVO = instanceVOMap.get(instanceIDselected);
	}
	
	public void Search() {
		ApexPages.getMessages().clear();
		if(SearchInstanceName != null) {
			MdmMappingService.instanceListResponse instanceResponse = new MdmMappingService.instanceListResponse();
			instanceResponse = MdmMappingServiceWrapper.getInstancesLikeName(SearchInstanceName);
			instaVOs = new list<MdmMappingService.instanceVO>();
			
			if(instanceResponse!=null)
				instaVOs = instanceResponse.instanceVOs;
		}
		
		/* Loop through the instanceVO objects and store the ID, Object pair in a map. */
	
		if(instaVOs != null) {
			instanceVOMap = new map<Long, MdmMappingService.instanceVO>();
			for (MdmMappingService.instanceVO instanceItem : instaVOs) {
				instanceVOMap.put(instanceItem.instanceId , instanceItem);
			}
		}
	}

	public void Submit() {
		ApexPages.getMessages().clear();
		if(instName != null && praSys != null && SysName != null) {
			MdmMappingService.instanceVO instVO = new MdmMappingService.instanceVO();
			instVO.instanceId = 0;
			instVO.instanceName = instName;
			instVO.praSystem = praSys;
			instVO.siteType = SysName;
			instVO.source = UserInfo.getName();   
			MdmMappingService.mdmCrudResponse response = MdmMappingServiceWrapper.saveInstanceFromVO(instVO); 
			if (response != null) {
				if (response.operationStatus.equals('0')) {
					Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, 'Instance save successful'));
				} else if ((''+response.errors).toLowerCase().contains(MdmMappingServiceWrapper.UNIQUE_CRS_INSTANCE_NAME_CONSTRAINT.toLowerCase())) {
					Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Instance save failed. CRS Instance name already exists!'));
				}else {
					Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Instance save failed with errors: ' + response.errors));
				}
			}     
		}    
	}
	
	public void Add() {  
		ApexPages.getMessages().clear();
		if(attributeKey != null && URLValue != null) {
			MdmMappingService.instanceVO instVO = instanceVOMap.get(instanceIDselected);
			instVO.source = UserInfo.getName();
			Integer bool = 1;
			MdmMappingService.entry_element el = new MdmMappingService.entry_element();
			el.key = attributeKey;
			el.value = URLValue;
			
			if (instVO.siteAttributes == null) {
				instVO.siteAttributes = new MdmMappingService.siteAttributes_element();
				instVO.siteAttributes.entry = new list<MdmMappingService.entry_element>();
			}
			
			if ((instVO.siteAttributes != null) && (instVO.siteAttributes != null) ) {
				for(MdmMappingService.entry_element p: instVO.siteAttributes.entry) {
					if(p.key == attributeKey)
					bool = 0;
				}
			}
			if(bool == 1) {
				instVO.siteAttributes.entry.add(el);
				MdmMappingService.mdmCrudResponse response = MdmMappingServiceWrapper.saveInstanceFromVO(instVO);
				if (response.operationStatus.equals('0')) {
					Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, 'Instance save successful'));
				} else {
					Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Instance save failed with errors: ' + response.errors));
				}
			} else {
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Attribute type already exists for this instance.'));
			}
		}
	}

	public void saveInstance() {
		if (renderInstanceVO != null) {
			renderInstanceVO.source = UserInfo.getName();
			MdmMappingService.mdmCrudResponse response = MdmMappingServiceWrapper.saveInstanceFromVO(renderInstanceVO);
			if (response.operationStatus.equals('0')) {
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, 'Instance save successful'));
			} else {
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Instance save failed with errors: ' + response.errors));
			}
		}
	}

	public list<SelectOption> getPRASystem() {
		list<SelectOption> BooleanAns = new list<SelectOption>();
		BooleanAns.add(new SelectOption('', '--None--'));
		BooleanAns.add(new SelectOption('YES', 'Yes'));
		BooleanAns.add(new SelectOption('NO', 'No'));
		
		return BooleanAns;
	}
	
	public list<SelectOption> getSystemNames() {
		list<SelectOption> SystemNames = new list<SelectOption>();
		SystemNames.add(new SelectOption('', '--None--'));
		SystemNames.add(new SelectOption('DATALABS', 'DataLabs'));
		SystemNames.add(new SelectOption('NEXTDOC', 'NextDocs'));
		SystemNames.add(new SelectOption('DLSTAGING', 'DLStaging'));
		SystemNames.add(new SelectOption('CTMS', 'CTMS'));
		SystemNames.add(new SelectOption('RAVE', 'Rave'));
		SystemNames.add(new SelectOption('AERS', 'AERS'));
		
		return SystemNames;
	}
	
	public list<SelectOption> getAttributeKeys() {
		list<SelectOption> AttributeKeys = new list<SelectOption>();
		AttributeKeys.add(new SelectOption('EDocs URL', 'EDocs URL'));
		AttributeKeys.add(new SelectOption('HOSTADDRESS', 'HOSTADDRESS'));
		AttributeKeys.add(new SelectOption('DATABASENAME', 'DATABASENAME'));
		AttributeKeys.add(new SelectOption('URL', 'URL'));
		
		return AttributeKeys;
	}
}