/**
* @author Kostya Hladkyi
* @date 03/11/2015
* @description Utilities class for CV (Help Form SideBar) functionality 
*/
public with sharing class CV_Utils {

    public static final Integer MAX_ITEM_NUMBER_IN_MENU = 3;
    private static final Integer SELECT_OPTIONS_LIMIT = 1000;
    private static final Integer SELECT_OPTIONS_LIST_SIZE = 5;
    //private static final String SHOW_MORE = 'More...';
    private static final String DELIMITER = ',';
    private static final String STANDARD_ORIGINAL_URL_PARAM = 'sdfcIFrameOrigin';
    private static final String PASSED_ORIGINAL_URL_PARAM = 'originalPageUrl';
    private static final String ID_PARAM = 'id';
    private static final String RECORD_TYPE_ID_FIELD_NAME = 'RecordTypeId';
    private static final String RECORD_TYPE_REFERENCE_NAME = 'RecordType';
    private static final String DEVELOPER_NAME_FIELD_NAME = 'DeveloperName';
    private static final String UTF_CHARACTER_ENCODING = 'UTF-8';
    private static final String SLASH_CHAR = '/';

    public static String getOriginalPageEncodedUrl(){
        Map<String, String> parameters = ApexPages.currentPage().getParameters();
        return parameters.containsKey(PASSED_ORIGINAL_URL_PARAM)
                ? parameters.get(PASSED_ORIGINAL_URL_PARAM) 
                : parameters.get(STANDARD_ORIGINAL_URL_PARAM);
    }
    
    public static List<SelectOption> getHelpItems(List<Help_Record__c> helpRecords, Boolean toLimitItems){
        List<SelectOption> helpItems = new List<SelectOption>();
        if (UserInfo.getUserId() != '005L0000002TvVDIA0'){
            //----- !!!!!!!!! TEMP
            //return helpItems;
        }
        try{
            for ( Help_Record__c helpRecord : helpRecords ){
                if (toLimitItems && helpItems.size() == MAX_ITEM_NUMBER_IN_MENU){
                    //helpItems.add( new SelectOption('', SHOW_MORE) );
                    break;
                }
                helpItems.add( new SelectOption(helpRecord.Id, helpRecord.Label__c) );
            }
        }catch(Exception e){
            ApexPages.addMessages(e);
        }
        return helpItems;
    }
    
    public static List<Help_Record__c> selectHelpRecords(Boolean toLimitResult){
        List<Help_Record__c> helpRecords = new List<Help_Record__c>();
        try{
            String currentPageName = getCurrentPageName();
            System.debug('currentPageName: ' + currentPageName);
            if (currentPageName == null){
                return helpRecords;
            }
            /*
            System.debug('--- current page: ' + ApexPages.currentPage().getUrl() );
            System.debug('--- parameters: ' + ApexPages.currentPage().getParameters().keySet() );
            Id recordId = getRecordIdFromUrl();
            System.debug('--- recordId: ' + recordId);
            String tab = recordId != null 
                    ? recordId.getSobjectType().getDescribe().getKeyPrefix()
                    : getCurrentTabFromUrl();
            System.debug('--- tab: ' + tab);
            */
            String query = 'Select Label__c From Help_Record__c '
                    + ' Where Related_Pages__c Like \'%' + DELIMITER + currentPageName + DELIMITER + '%\'';
                    
            
            /*
                    + ' Where ( User_Profiles__c Like \'' + getUserProfileMatch() + '\' OR User_Profiles__c = null ) '
                    + ' And ( Related_Objects__c Like \'%' + DELIMITER + tab + DELIMITER + '%\' OR Related_Objects__c = null ) ';
            if (recordId != null){
                query += ' And ( Record_Types__c Like \'%' + DELIMITER + getRecordTypeDeveloperName(recordId) + DELIMITER + '%\' OR Record_Types__c = null )';
            }
            */
            //query += ' And Active__c = true Order By Label__c';
            query += ' And Active__c = true Order By Order__c DESC NULLS LAST';
            if (toLimitResult){
                query += ' Limit ' + (MAX_ITEM_NUMBER_IN_MENU + 1);
            }
            System.debug('--- query: ' + query);
            helpRecords = Database.query( query );     
        }catch(Exception e){
            ApexPages.addMessages(e);
        }
        return helpRecords;
    }   
    
    public static Help_Record__c getHelpRecordById(String menuRecordId){
        if ( String.isEmpty(menuRecordId) ){
            return null;
        }
        try{
            List<Help_Record__c> helpRecords = [Select Label__c, Rich_Html_Content__c From Help_Record__c Where Id = :menuRecordId];
            if (helpRecords.isEmpty()){
                throw new HelpSideBarException('Help Record element not found by Id: ' + menuRecordId);
            }
            return helpRecords[0];
        }catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
    }
    
    private static String getUserProfileMatch(){
        String profileName = [Select Name From Profile Where Id = :UserInfo.getProfileId()].Name;
        return '%' + DELIMITER + profileName + DELIMITER + '%';
    }
    
    private static String getCurrentTabFromUrl(){
        String originalUrlEncoded = getOriginalPageEncodedUrl();
        if ( String.isEmpty(originalUrlEncoded) ){
            throw new HelpSideBarException('Original URL is missing in : ' + ApexPages.currentPage().getUrl() );
        }
        Url pageUrl = new Url( EncodingUtil.urlDecode(originalUrlEncoded, UTF_CHARACTER_ENCODING) );
        return pageUrl.getPath().split(SLASH_CHAR)[1];
    }
    
    public static Id getRecordIdFromUrl(){
        String recordIdValue = ApexPages.currentPage().getParameters().get(ID_PARAM);
        return String.isEmpty( recordIdValue ) ? null : (Id)recordIdValue;
    }
    
    private static String getRecordTypeDeveloperName(Id recordId){
        DescribeSObjectResult recordDescribe = recordId.getSobjectType().getDescribe();
        if ( !recordDescribe.fields.getMap().containsKey(RECORD_TYPE_ID_FIELD_NAME) ){
            //-- This Sobject doesn't have Record type enabled.
            return '';
        }
        String sobjectName = recordDescribe.getName();
        String query = 'Select RecordType.DeveloperName From ' + sobjectName + ' Where Id = :recordId';
        List<SObject> records = Database.query(query);
        if ( records.isEmpty() ){
            throw new HelpSideBarException('Record instance not found by id value: ' + recordId);
        }
        SObject recordType = records[0].getSObject(RECORD_TYPE_REFERENCE_NAME);
        return recordType == null ? '' : (String)recordType.get(DEVELOPER_NAME_FIELD_NAME);
    }
    
    public static List<String> getValueAsList(String profiles){
        if ( String.isEmpty(profiles) ){
            return new List<String>();
        }else{
            return profiles.removeStart(CV_Utils.DELIMITER).removeEnd(CV_Utils.DELIMITER).split(CV_Utils.DELIMITER);
        }
    }
    
    public static String getJoinedValue(List<String> profiles){
        if ( profiles.isEmpty() ){
            return null;
        }else{
            return CV_Utils.DELIMITER + String.join(profiles, CV_Utils.DELIMITER) + CV_Utils.DELIMITER;
        }
    }
    
    public static List<SelectOption> getAllProfiles(){
        List<SelectOption> profiles = new List<SelectOption>();
        for ( Profile p : [Select Name From Profile Order By Name] ){
            profiles.add( new SelectOption(p.Name, p.Name) );
        }
        return profiles;
    }
    
    public static List< List<SelectOption> > getAllPages(){
        List< List<SelectOption> > pages = new List< List<SelectOption> >();
        Integer counter = 0;
        List<SelectOption> partOfPages = new List<SelectOption>();
        for ( ApexPage p : [Select Name, MasterLabel From ApexPage Order By Name] ){
            partOfPages.add( new SelectOption(p.Name, p.Name) );
            if ( ++counter == SELECT_OPTIONS_LIMIT ){
                pages.add( partOfPages );
                partOfPages = new List<SelectOption>();
                counter = 0;
            }
        }
        pages.add(partOfPages);
        while( pages.size() < SELECT_OPTIONS_LIST_SIZE ){
            pages.add( new List<SelectOption>() );
        }
        return pages;
    }
    
    public static List<SelectOption> getAllSObjects(){
        List<SelectOption> sobjects = new List<SelectOption>();
        /*for( Schema.SObjectType t : Schema.getGlobalDescribe().Values() ){
           sobjects.add( new SelectOption( t.getDescribe().getName(), t.getDescribe().getLabel() ) );
        }*/
        sobjects.add( new SelectOption( 'Account', 'Account' ) );       
        sobjects.add( new SelectOption( 'Campaign', 'Campaign' ) );               
        sobjects.add( new SelectOption( 'Contact', 'Contact' ) );       
        sobjects.add( new SelectOption( 'Opportunity', 'Opportunity' ) );               
        return sobjects;
    }
    
    public static List<SelectOption> getRecordTypesForSObject(String sobjectApiName){
        List<SelectOption> recordTypes = new List<SelectOption>();
        try{
            String query = 'SELECT Name, DeveloperName FROM RecordType Where SobjectType  = :sobjectApiName Order By Name';
            for ( RecordType r : Database.query(query) ){
                recordTypes.add( new SelectOption( r.DeveloperName, r.Name ) );
            }
        }catch(Exception e){
            ApexPages.addMessages(e);
        }
        return recordTypes;
    }
    
    private static String getCurrentPageName(){
        try{
            //String originalUrlEncoded = getOriginalPageEncodedUrl();
            //String pageUrl = originalUrlEncoded != null ? EncodingUtil.urlDecode(originalUrlEncoded, UTF_CHARACTER_ENCODING) : ApexPages.currentPage().getUrl(); 
            List<String> parts = ApexPages.currentPage().getUrl().split(SLASH_CHAR);
            if ( parts.size()<3 || parts[0] != '' || parts[1] != 'apex'){
                return null;
            }
            return parts[2].split('\\?')[0];
        }catch(Exception e){
            throw new HelpSideBarException( 'Page name not determined from current page URL: "' + ApexPages.currentPage().getUrl() + '". ' + e.getMessage() );
        }
    }
    
    public static void upsertRecord(SObject record){
        upsert record;
    }
    
    public static void deleteRecord(SObject record){
        if (record == null || record.Id == null){
            throw new HelpSideBarException('Record doesn\'t exist');
        }
        delete record;
    }    
    
    public static SObject cloneRecord(SObject record){
        try{
            return record.clone(false, false, false, false);
        }catch(Exception e){
            throw new HelpSideBarException( 'System error at cloning record: ' + e.getMessage() );
        }
    }
    
    public class HelpSideBarException extends Exception { }
    
}