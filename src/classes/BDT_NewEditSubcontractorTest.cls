@isTest
private class BDT_NewEditSubcontractorTest {
	
	static testmethod void NewEditSubcontractorTest(){
		Subcontractor__c sc = new Subcontractor__c(Name='Test');
		upsert sc;
		SubcontractorActivity__c sca = new SubcontractorActivity__c(Name='tetstest',Subcontractor__c=sc.Id);
		upsert sca;
		
		BDT_NewEditSubcontractor scc = new BDT_NewEditSubcontractor();

		scc.SubcontractorId = sc.Id;
		
		scc.loadSubcontractor();
			
		scc.cancel();
		scc.deleteSoft();
		scc.save();
		
	}	

 
}