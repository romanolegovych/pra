public class MdmSvtServiceWrapper {
	
	private static String ENDPOINT = '';
	private static String SERVICENAME = '';
	private static Integer TIMEOUT = 0;
	private static List<String> errors;
	
	static {
		initSettings();
	}
	
	public static MdmSvtService.svtListResponse getSvtVOsByProtocol(MdmProtocolService.protocolVO protocolVO) {
		MdmSvtService.svtListResponse response = null;
		if (protocolVO != null) {
			MdmSvtService.MdmSvtServicePort service = getService();
			try {
				response = service.getSvtVOsByProtocol(protocolVO);
				system.debug('---------------- SvtVO retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- SvtVO retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmSvtService.visitMappingResponse getSvtEcrfMappingByProtocolPraId(MdmProtocolService.protocolVO protocolVO, String praProjectId) {
		MdmSvtService.visitMappingResponse response = null;
		if (protocolVO != null && praProjectId != null) {
			MdmSvtService.MdmSvtServicePort service = getService();
			try {
				response = service.getSvtEcrfMappingByProtocolPraId(protocolVO, praProjectId);
				system.debug('---------------- SvtEcrf mapping retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- SvtEcrf mapping retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmSvtService.svtEcrfResponse getSvtsEcrfsFromProtocolPraId(MdmProtocolService.protocolVO protocolVO, String praProjectId) {
		MdmSvtService.svtEcrfResponse response = null;
		if (protocolVO != null && praProjectId != null) {
			MdmSvtService.MdmSvtServicePort service = getService();
			try {
				response = service.getSvtsEcrfsFromProtocolPraId(protocolVO, praProjectId);
				system.debug('---------------- Svt/Ecrf retrievals successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Svt/Ecrf retrievals failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmSvtService.svtEcrfMappingResponse getSvtsEcrfsMappingsFromProtocolPraId(MdmProtocolService.protocolVO protocolVO, String praProjectId) {
		MdmSvtService.svtEcrfMappingResponse response = null;
		if (protocolVO != null && praProjectId != null) {
			MdmSvtService.MdmSvtServicePort service = getService();
			try {
				response = service.getSvtsEcrfsAndMappingsFromProtocolPraId(protocolVO, praProjectId);
				system.debug('---------------- Svt/Ecrf and mapping retrievals successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Svt/Ecrf and mapping retrievals failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmSvtService.svtEcrfUsvFormMappingResponse getSvtsEcrfsUsvFormsMappingsFromProtocolPraId(MdmProtocolService.protocolVO protocolVO, String praProjectId) {
		MdmSvtService.svtEcrfUsvFormMappingResponse response = null;
		if (protocolVO != null && praProjectId != null) {
			MdmSvtService.MdmSvtServicePort service = getService();
			try {
				response = service.getSvtsEcrfsUsvFormsAndMappingsFromProtocolPraId(protocolVO, praProjectId);
				system.debug('---------------- Svt/Ecrf/UsvForm and mapping retrievals successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Svt/Ecrf/UsvForm and mapping retrievals failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmSvtService.mdmCrudResponse saveSvtEcrfMappingFromVO(MdmSvtService.ecrfMappingVO ecrfMappingVO) {
		MdmSvtService.mdmCrudResponse response = null;
		if (ecrfMappingVO != null) {
			MdmSvtService.MdmSvtServicePort service = getService();
			try {
				response = service.saveSvtEcrfMappingFromVO(ecrfMappingVO);
				system.debug('---------------- Svt/Ecrf save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Svt/Ecrf save failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmSvtService.mdmCrudResponse saveSvtEcrfMappingFromVOs(List<MdmSvtService.ecrfMappingVO> ecrfMappingVOs) {
		MdmSvtService.mdmCrudResponse response = null;
		if (ecrfMappingVOs != null) {
			MdmSvtService.MdmSvtServicePort service = getService();
			try {
				response = service.saveSvtEcrfMappingFromVOs(ecrfMappingVOs);
				system.debug('---------------- Svt/Ecrf save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Svt/Ecrf save failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmSvtService.mdmCrudResponse deleteSvtEcrfMappingFromVOs(List<MdmSvtService.ecrfMappingVO> ecrfMappingVOs) {
		MdmSvtService.mdmCrudResponse response = null;
		if (ecrfMappingVOs != null) {
			MdmSvtService.MdmSvtServicePort service = getService();
			try {
				response = service.deleteSvtEcrfMappingFromVOs(ecrfMappingVOs);
				system.debug('---------------- Svt/Ecrf delete successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Svt/Ecrf delete failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmSvtService.mdmCrudResponse saveSvtUsvFormMappingFromVOs(List<MdmSvtService.ecrfFormMappingVO> ecrfFormMappingVOs) {
		MdmSvtService.mdmCrudResponse response = null;
		if (ecrfFormMappingVOs != null) {
			MdmSvtService.MdmSvtServicePort service = getService();
			try {
				response = service.saveSvtUsvFormMappingsFromVOs(ecrfFormMappingVOs);
				system.debug('---------------- Svt/UsvForm save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Svt/UsvForm save failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmSvtService.mdmCrudResponse deleteSvtUsvFormMappingFromVOs(List<MdmSvtService.ecrfFormMappingVO> ecrfFormMappingVOs) {
		MdmSvtService.mdmCrudResponse response = null;
		if (ecrfFormMappingVOs != null) {
			MdmSvtService.MdmSvtServicePort service = getService();
			try {
				response = service.deleteSvtUsvFormMappingsFromVOs(ecrfFormMappingVOs);
				system.debug('---------------- Svt/UsvForm delete successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Svt/UsvForm delete failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static List<String> getErrors() {
		return errors;
	}
	
	private static MdmSvtService.MdmSvtServicePort getService() {
		MdmSvtService.MdmSvtServicePort service = new MdmSvtService.MdmSvtServicePort();
		service.endpoint_x = ENDPOINT + SERVICENAME;
		service.timeout_x = TIMEOUT;
		return service;
	}
	
	private static void initSettings() {
		Map<String, MuleServicesCS__c> settings = MuleServicesCS__c.getAll();
		ENDPOINT = settings.get('ENDPOINT').Value__c;
		SERVICENAME = settings.get('MdmSvtService').Value__c;
		Integer to = Integer.valueOf(settings.get('TIMEOUT').Value__c);
		if (to != null && to > 0) {
			TIMEOUT = to;
		} 
		system.debug('--------- Using following Custom Settings for MuleServiceCS ---------');
		system.debug('ENDPOINT:'+ENDPOINT);
		system.debug('TIMEOUT:'+TIMEOUT);
		system.debug('----------------------- MuleServiceCS END ----------------------------');
	}
}