global with sharing class RM_SaveSearchController {

    public string folderId{get;set;}
    public string folderText{get;set;}
    public string userId{get;set;}
    
    public RM_SaveSearchController() {
        // constructor for save search controller
        
        userId = UserInfo.getUserID();
    }
     
 

    public string getRoot() {
        string jsonData = '';
       // rootList = new list<Root_Folder__c>();
        list<Root_Folder__c> rootList = RM_OtherService.GetRootFolder();
        system.debug('-----roots------'+rootList);
        string subData = '';
        for(Integer i = 0; i < rootList.size(); i++){
            string rId = string.valueof(rootList[i].get('Id'));
            if(i == 0 ){
                jsonData += '{attr : {rel : "root", id : "' + rId + '", path: "' + rootList[i].Name + '"}, data : "' + rootList[i].Name + '"';
                subData = getSub(rootList[i]); 
                if(subData.length() > 0 )
                    jsonData += subData + ']}';           
                else
                    jsonData += subData+ '}';
            }
            else {
                jsonData += ', {attr : {rel : "root", id : "' + rId + '", path: "' + rootList[i].Name +'"}, data : "' + rootList[i].Name + '"';
                subData = getSub(rootList[i]);
                if(subData.length() > 0)
                    jsonData += subData + ']}';          
                else
                    jsonData += subData + '}';
            }
        }
        return jsonData;
    }
    
    // Json data for sub folders
    private string getSub(Root_Folder__c rootObj) {
       
        string subData = '';
        list<Sub_Folder__c> userSubList = RM_OtherService.GetCustomSubFolderList(rootObj.id, userId) ;
        system.debug('-------userSubs----------'+userSubList);
        list<Sub_Folder__c> subList = RM_OtherService.GetDefaultSubFolderList(rootObj.id, userId);
      
       
        system.debug('-------defaltSub---------'+subList);
        system.debug('-------defaltSubSize---------'+subList.size());
       
        for(Integer i = 0; i < subList.size(); i++){
            string sId = string.valueof(subList[i].get('Id'));
            string subFolder= string.valueof(subList[i].get('Name'));
            if(i == 0 ){
                subData += ', children: [{attr : {rel : "default", id : "' + sId + '", path: "' + subList[i].root__r.name + '"}, data : "'+ subFolder + '"';
                subData+=getSearches(sId);
                subData += '}';
            }
            else {
                subData += ',{attr : {rel : "default", id : "' + sId + '", path: "' + subList[i].root__r.name + '"}, data : "' + subFolder + '"';
                subData+=getSearches(sId);
                subData += '}';
            }         
        }
        for(Integer i = 0; i < userSubList.size(); i++){
            string sId = string.valueof(userSubList[i].get('Id'));
            string subFolder= string.valueof(userSubList[i].get('Name'));
            if (subList.size() > 0){
                if(i == 0 ){
                    subData += ',{attr : {rel : "folder", id : "' + sId +'", path: "' + userSubList[i].root__r.name +  '"}, data : "'+ subFolder + '"';
                    subData+=getSearches(sId);
                    subData += '}';
                }
                else {
                    subData += ',{attr : {rel : "folder", id : "' + sId + '", path: "' + userSubList[i].root__r.name + '"}, data : "' + subFolder + '"';
                    subData+=getSearches(sId);
                    subData += '}';
                }
            }
            else{
                if(i == 0 ){
                    subData += ', children: [{attr : {rel : "folder", id : "' + sId +'", path: "' + userSubList[i].root__r.name +  '"}, data : "'+ subFolder + '"';
                    subData+=getSearches(sId);
                    subData += '}';
                }
                else {
                    subData += ',{attr : {rel : "folder", id : "' + sId + '", path: "' + userSubList[i].root__r.name + '"}, data : "' + subFolder + '"';
                    subData+=getSearches(sId);
                    subData += '}';
                }
            }
                
              
        }
        /*searchList = [select Name, Id from Saved_Search__c where Root__c != null and Root__c = :rId];
        for(Integer i = 0; i < searchList.size(); i++){
            sId = string.valueof(searchList[i].get('Id'));
            subFolder = string.valueof(searchList[i].get('Name'));
            if(i == 0 && i < searchList.size()){
                jsonData += ',{attr : {rel : "file", id : "' + sId + '"}, data : "'+ subFolder + '"';
                jsonData += '}';
            }else if(i > 0 && i < (searchList.size() - 1)){
                jsonData += ',{attr : {rel : "file", id : "' + sId + '"}, data : "' + subFolder + '"';
                jsonData += '}';
            }else if(i == (searchList.size() - 1)){
                jsonData += ',{attr : {rel : "file", id : "' + sId + '"}, data : "' + subFolder + '"';
                jsonData += '}';
            }
        }*/
        return subData;
    }
    
    // Json data for searches //
    private string getSearches(string sId){
        string jsonChildren = '';
      
        list<Saved_Search__c> searchList = RM_OtherService.getSavedSearchList(sId);  
        for(Integer i = 0; i < searchList.size(); i++){
            if(i==0 && i < searchList.size()){
                string fileId = string.valueof(searchList[i].get('Id'));
                jsonChildren += ', children : [{attr : {rel : "file", id : "' + fileId +'", path: "' + searchList[i].search_type__c + '"}, data : "' + searchList[i].get('Name') + '"}';
            }else if(i > 0 && i < (searchList.size())){
                string fileId = string.valueof(searchList[i].get('Id'));
                jsonChildren += ',{attr : {rel : "file", id : "' + fileId +'", path: "' + searchList[i].search_type__c + '"}, data : "' + searchList[i].get('Name')+ '"}';
            }
        }
        if(searchList.size() > 0){
            jsonChildren += ']';
        }
        
       
        return jsonChildren; 
    }
    
    // gets the value of the selected node's id and text
    public void addText(){
        this.folderId = ApexPages.CurrentPage().getParameters().get('folderId');
        this.folderText = ApexPages.CurrentPage().getParameters().get('folderText');
        
    }
    
    //Send Search Information to Search Page
    public PageReference run(){ 
     
        string searchID = ApexPages.CurrentPage().getParameters().get('searchId');
        system.debug('-------searchID---------'+searchID);
        if (searchID != null){
	        Saved_Search__c ssearch = RM_OtherService.getSavedSearchwithRoot(searchID);
	       
	        PageReference pf = new PageReference('/apex/RM_searchresources');
	        if (ssearch.Sub__r.Root__r.name == 'Projects')  
	            pf = new PageReference('/apex/RM_searchProjects');
	        else if (ssearch.Sub__r.Root__r.name == 'Assignments')   
	        	pf = new PageReference('/apex/RM_SearchAssignments');
	        pf.getParameters().put('cid', searchID);
	        //Redirect to the search page
	        pf.setRedirect(false);
	        return pf;
        }
        return null;
    } 
     @RemoteAction
     global static void RenameLibrary(string nodeType, string folderId, string newFolderName){      
         system.debug('------nodeType-------'+nodeType);
    
        newFolderName = newFolderName.substring(1);
        if (nodeType == 'Folder'){
            Sub_Folder__c checkSubId = RM_OtherService.GetSubFolderbyID(folderId);
            if(checkSubId.id != null){
                checkSubId.Name = newFolderName;
                checkSubId.Sub_Folder_Unique_Key__c = newFolderName+checkSubId.root__c + UserInfo.getUserID();
                upsert checkSubId;
            }      
        } 
        else if (nodeType == 'File'){
            Saved_Search__c checkSearchId = RM_OtherService.getSavedSearchByID(folderId);
            if(checkSearchId.id != null){                  
                checkSearchId.Name = newFolderName;
                checkSearchId.Saved_Search_Unique_Key__c = newFolderName+checkSearchId.sub__c;
                upsert checkSearchId;                   
            }              
        }
     
    }
     @RemoteAction
      global static void removeFolderFile(string nodeType, string folderId) {
     
        if (nodeType == 'Folder'){
            Sub_Folder__c folderToDelete= RM_OtherService.GetSubFolderbyID(folderId);
            system.debug('------folderToDelete-------'+folderToDelete);
            if(folderToDelete.id != null){
                //get file under the folder
                list<Saved_Search__c> savedSearchList = RM_OtherService.getSavedSearchList(folderId);
                system.debug('------savedSearchList-------'+savedSearchList);
                if (savedSearchList.size() > 0)
                    delete savedSearchList; 
                delete folderToDelete;              
            }
        }
        else if (nodeType == 'File'){
            Saved_Search__c fileToDelete= RM_OtherService.getSavedSearchByID(folderId);
            system.debug('------fileToDelete-------'+fileToDelete);
            if(fileToDelete.id != null)                
                delete fileToDelete; 
        }
     
    }
    @RemoteAction
    global static void InsertFolder(string folderName, string parentID) {
      
        system.debug('------folderName-------'+folderName);
        folderName = folderName.substring(1);
       if (parentID != null && parentID != ''){        
            // initialize new folder attributes
            Sub_Folder__c newSub = new Sub_Folder__c();
            newSub.Name = folderName; 
            newSub.Custom__c = true;
            newSub.User__c = UserInfo.getUserID();
            newSub.Root__c = parentID;     
            newSub.Sub_Folder_Unique_Key__c = folderName+parentID+UserInfo.getUserID();
            insert newSub;
       
       }
       
    }
     @RemoteAction
      global static void moveFile(string rID, string moveToParentID, string path, string folderName){    
      	system.debug('------moveFile-------'+moveToParentID + 'path:' + path + 'folderName: ' + folderName);  
		Sub_Folder__c checkSubParent = new Sub_Folder__c();
		if (moveToParentID == 'new'){
			folderName = folderName.substring(1);
			checkSubParent = RM_OtherService.GetSubFolderID(folderName, path, UserInfo.getUserID());
		} 
   		else
        	 checkSubParent = RM_OtherService.GetSubFolderbyID(moveToParentID);        
      
        Saved_Search__c checkSearch = RM_OtherService.getSavedSearchByID(rID);
        
        if(checkSearch.id != null && checkSubParent.id != null){
            string savedSearch = checkSearch.Name;  
             system.debug('------checkSearch-------'+checkSearch);
            checkSearch.Sub__c = checkSubParent.id;
            checkSearch.Saved_Search_Unique_Key__c =savedSearch + checkSubParent.id;
                    
            update checkSearch;  
        }
    
      
    }     
}