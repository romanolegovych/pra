/** 
@Author Abhishek Prasad
@Date 2015
@Description Data Accessor Class for Bill Rates
*/
public with sharing class AA_BillRateDataAccessor{

    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description Get distinct Job Class description by Model Id
    */
    public static List<AggregateResult> getJobPositonsbyModel(String Id){
        return [SELECT Job_Position__r.Id,Job_Position__r.Name
                FROM Bill_Rate__c 
                WHERE Model_Id__c =: Id
                GROUP BY Job_Position__r.Id,Job_Position__r.Name
                ORDER BY Job_Position__r.Name];
    }
    
    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description  Get all distinct Job Class descriptions
    */
    public static List<AggregateResult> getJobPositions(){
        return [SELECT Job_Position__r.Id,Job_Position__r.Name
                FROM Bill_Rate__c 
                GROUP BY Job_Position__r.Id,Job_Position__r.Name
                ORDER BY Job_Position__r.Name];
    }

    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description  Get Distinct Models by Id
    */
    public static List<Bill_Rate_Card_Model__c> getModelById(String Id){
        return [SELECT Id,Name,Year__c,Status__c
                FROM Bill_Rate_Card_Model__c 
                WHERE Id =: Id
                ORDER BY Name];
    }

    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description  Get all Distinct Model Id
    */
    public static List<Bill_Rate_Card_Model__c> getModels(){
        return [SELECT Id,Name
                FROM Bill_Rate_Card_Model__c 
                ORDER BY Name];
    }

    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description  Get all Currencies
    */
    public static List<Currency__c> getCurrencies(){
         return [SELECT  Id,Name FROM Currency__c WHERE NAME='USD' or Name='EUR' order by Name];
    }

    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description  Get Rates by Filter
    */ 
    public static List<AggregateResult> getRateListByFilter(String model,String jobposition,String country,String year,String active){
    
         String query = 'SELECT Model_Id__c,Job_Position__c,Schedule_Year__c,Country__c from Bill_Rate__c WHERE';
        
             query = query + String.format(' Schedule_Year__c =\'\'{0}\'\' ', new String[]{year});   
         if(model !=null)
             query = query + String.format(' AND Model_Id__c=\'\'{0}\'\' ', new String[]{model});  
         if(jobposition !=null)
             query = query + String.format(' AND Job_Position__c=\'\'{0}\'\' ', new String[]{jobposition});  
         if(country !=null)
             query = query + String.format(' AND Country__c =\'\'{0}\'\' ', new String[]{country});
         if(active !='A'){
             if(active == 'Y')
                 query = query + ' AND Bill_Rate__c.IsActive__c= true ';
             else
                 query = query + ' AND Bill_Rate__c.IsActive__c = false '; 
         }
         query = query + ' GROUP BY Model_Id__c,Job_Position__c,Schedule_Year__c,Country__c';

         return Database.query(query);
    }  
    
    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description  Get Rate Details
    */
    public static List<Bill_Rate__c> getRateDetails(String model,String jobposition,String country,String year,String active){
        String query = String.format('SELECT Bill_Rate__c.Model_Id__r.Name,'+
                                             'Bill_Rate__c.Job_Position__r.Name,'+
                                             'Bill_Rate__c.Country__r.Name,'+
                                             'Bill_Rate__c,'+
                                             'Currency__c,'+
                                             'IsActive__c,'+
                                             'Schedule_Year__c,'+
                                             'Year_Rate_Applies_To__c, '+
                                             'Inflation_Rate__c, '+
                                             'Currency__r.Name, '+
                                             'CreatedBy.Name, '+
                                             'CreatedDate, '+
                                             'LastModifiedBy.Name, '+
                                             'LastModifiedDate '+
                                             'FROM Bill_Rate__c '+
                                             'WHERE Model_Id__c= \'\'{0}\'\' AND Job_Position__c= \'\'{1}\'\' AND Country__c= \'\'{2}\'\' AND Schedule_Year__c = \'\'{3}\'\'', new String[]{model,jobposition,country,year,active});         
        query = query + ' ORDER BY Year_Rate_Applies_To__c ';
        return Database.query(query);
    }
    
    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description  Run Aggregate Query
    */
    public static List<AggregateResult> runAggregateQuery(String query){
       return  Database.query(query);
    }
    
    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description  Run Query
    */
    public static List<Cost_Rate__c> runQuery(String query){
       return  Database.query(query);
    }
    
    /** 
    @Author Niharika Reddy
    @Date 2015
    @Description  Get Bill Rate based on Id
    */
    public static Bill_Rate__c getBillRate(String billRateId){
        return [select Id, Name, Model_Id__c, Job_Position__c, Country__c, Schedule_Year__c from Bill_Rate__c where Id=: billRateId limit 1];
    }
    
    /** 
    @Author Niharika Reddy
    @Date 2015
    @Description  Get bill rate based on model Id
    */
    public static List<Bill_Rate__c> getBillRateBasedOnModelId(String modelId){
        return [SELECT Id,
                 Year_Rate_Applies_To__c,
                 Schedule_Year__c,
                 Bill_Rate__c,
                 Currency__c,
                 Currency__r.Name,
                 CreatedBy.Name,
                 CreatedDate,
                 LastModifiedBy.Name,
                 LastModifiedDate,
                 IsActive__c,
                 country__c,
                 country__r.Name,
                 Job_Position__c,
                 Job_Position__r.Name,
                 Model_Id__c,
                 Model_Id__r.Status__c
          FROM   Bill_Rate__c 
          WHERE Model_Id__c =: modelId];
          
    }
    
}