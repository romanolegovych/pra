public with sharing class RM_SiteInfoController {
	private final list<LocationSummary>  locationSummary;
   
    private final list<MileStone> mileStone;
    private final list<siteDetail> siteDetail;
     private list<WFM_Protocol__c> pro;
	
 	public 	RM_SiteInfoController(){
 		string projID = ApexPages.currentPage().getParameters().get('id');
 		pro = RM_ProjectService.GetProtocolByProjectID(projID);
        system.debug('------pro------'+pro);
        if (pro.size() > 0){
            
            list<WFM_Site_Detail__c> sites = RM_ProjectService.GetSitesInfobyProtocolRID(pro[0].id);
            siteDetail = new list<siteDetail>();
            if (sites.size() > 0){
                for (WFM_Site_Detail__c site: sites)
                    siteDetail.add(new siteDetail(site));
            }
            
            locationSummary = getSiteLocData(pro[0]);
            mileStone = GetMileStoneData();
        }
 	}
   	public List<siteDetail> getSiteDetail(){
        return siteDetail;
    }
    public class siteDetail{
        transient public string siteName {get; private set;}
        transient public string country{get;private set;}
        transient public string state{get;private set;}
        transient public string status{get; private set;}
        transient public Integer  ActSubjEnroll{get; private set;}
        transient public Integer  PageExpected{get; private set;}
        transient public Integer  TotalForecasted{get; private set;}
        
        public siteDetail(WFM_Site_Detail__c site){
            siteName = site.name;
            country = site.Country__c;
            state = site.State_Province__c;
            status = site.status__c;
            ActSubjEnroll = Integer.valueOf(site.Actual_Subj_Enroll__c);
            PageExpected = Integer.valueOf(site.CRF_Pages_Expected__c);
            TotalForecasted = Integer.valueOf(site.Total_Forecasted_Visits__c);
        }
        
        
    }
    public class LocationSummary{
 
        transient public string region {get; private set;}        
        transient public string country {get; private set;}
        transient public Integer num_site {get; private set;}
        transient public Integer num_ActiveSite{get; private set;}
        transient public Integer num_Subj{get; private set;}
        
        public LocationSummary(AggregateResult ar){        
            region = (string)ar.get('Region_Name__c');           
            country = (string)ar.get('Country__c');
            num_site= (Integer)ar.get('num_site');
            num_Subj = Integer.valueOf((decimal)ar.get('subjEnroll'));
        }               
    }
    
    public List<LocationSummary> GetSiteLocation(){
        return locationSummary;
    }
    private list<LocationSummary> getSiteLocData(WFM_Protocol__c prot){
        AggregateResult[] actSite = RM_projectService.GetActiveSitesLocationbyProtocolRID(prot.id);
        map <string, integer> mapSite = new map<string, integer>();   
        for(AggregateResult arAct: actSite)
            mapSite.put((string)arAct.get('Region_Name__c') + (string)arAct.get('Country__c'), (integer)arAct.get('num_site'));
        AggregateResult[] siteSummary = RM_projectService.GetSitesLocationbyProtocolRID(prot.id);
        
        List<LocationSummary> locSummary = new List<LocationSummary>();      
        
        for (AggregateResult ar : siteSummary)  {
            LocationSummary ls = new LocationSummary(ar);
            if (mapSite.get(ls.region+ls.country) != null)
                ls.num_ActiveSite = mapSite.get(ls.region+ls.country);      
            else
                ls.num_ActiveSite = 0;
            locSummary.add(ls); 
        }
        return locSummary;
    }
     public class MileStone{
        transient public string description{get;private set;}
        transient public string contractedDate{get; private set;}
        transient public string acturalDate{get; private set;}
        transient public string revisedDate{get; private set;}
        
        public MileStone(string des, date conDate, date actDate, date revDate){
            description = des;
            contractedDate = RM_Tools.GetStringfromDate(conDate, 'mm/dd/yyyy');
            acturalDate = RM_Tools.GetStringfromDate(actDate, 'mm/dd/yyyy');
            revisedDate =  RM_Tools.GetStringfromDate(revDate,'mm/dd/yyyy');
        }       
    }
    
    public List<MileStone> GetMilestones(){
        return mileStone;
    }
    private list<MileStone> GetMileStoneData(){ 
        List<MileStone> miles = new List<MileStone>();
        if (pro.size() > 0){        
            
            miles.add(new MileStone('DB Lock', pro[0].Contracted_DB_Lock_DT__c, pro[0].DB_Lock_DT__c, null));
            miles.add(new MileStone('First Subject Enrolled', pro[0].Contracted_First_Subj_Enroll__c, pro[0].Actual_First_Subj_Enroll__c, pro[0].Revised_First_Subj_Enroll__c));
            miles.add(new MileStone('First Site Initiated', pro[0].Contracted_First_Site_Init__c, pro[0].Actual_First_Site_Init__c, pro[0].Revised_First_Site_Init__c));
            miles.add(new MileStone('Last Subject Enrolled', pro[0].Contracted_Last_Subj_Enroll__c, pro[0].Actual_Last_Subj_Enroll__c, pro[0].Revised_Last_Subj_Enroll__c));
            miles.add(new MileStone('Last Subject Off Study', pro[0].Contracted_Last_Subj_Off_Study__c, pro[0].Actual_Last_Subj_Off_Study__c, pro[0].Revised_Last_Subj_Off_Study__c));
            miles.add(new MileStone('Last Site Closed', pro[0].Contracted_Last_Site_Closed__c, pro[0].Actual_Last_Site_Closed__c, pro[0].Revised_Last_Site_Closed__c));
        }
        return miles;
    }
    
}