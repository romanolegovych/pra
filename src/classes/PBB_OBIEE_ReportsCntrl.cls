/**
@author Bhargav Devaram
@date 2015
@description this controller class is to display reports from the OBIEE app by accessing the URL
**/
public class PBB_OBIEE_ReportsCntrl {
    public  String ProjectID   {get;set;}    //select Project ID
    public  String  GraphType  {get;set;}    //whether Site or Subject graph   
    public  String  endpoint   {get;set;}    //endpoint URL
    
    /** 
    @author Bhargav Devaram
    @date 2015 
    @description get URL for OBIEE report based on the environment
    */ 
    public PageReference generateURL() {
        
        system.debug('---ProjectID--'+ProjectID+GraphType);
        string Environ = COM_Utils.getEnv();
        system.debug('---Environ --'+Environ);
        
        //check if CI,DEV,TEST,UAT,PROD and access thier respective URL's from custom Label
        if (Environ.equalsIgnoreCase('CI') || Environ.equalsIgnoreCase('DEV')){
           endpoint = Label.PBB_OBIEE_Reports_Dev_Env;            
        } 
        if (Environ.equalsIgnoreCase('TEST')){
           endpoint = Label.PBB_OBIEE_Reports_TEST_Env;            
        } 
        if (Environ.equalsIgnoreCase('UAT')){
           endpoint = Label.PBB_OBIEE_Reports_UAT_Env;            
        } 
        if (Environ.equalsIgnoreCase('PROD')){
           endpoint = Label.PBB_OBIEE_Reports_PROD_Env;            
        } 
        if(!Test.isRunningTest()) {
            endpoint = endpoint.replace('SampleProjectID', ProjectID);
            endpoint = endpoint.replace('SampleGraphType', GraphType);        
        }
        system.debug('---endpoint --'+endpoint );
        return null;
    } 
}