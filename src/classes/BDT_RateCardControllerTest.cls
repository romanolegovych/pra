@isTest
private class BDT_RateCardControllerTest {
 
    static testMethod void mytest() {
    	PageReference pageRef = New PageReference(System.Page.BDT_NewEditRateCard.getUrl());
        Test.setCurrentPage(pageRef);
        
        ServiceCategory__c sc 	= new ServiceCategory__c (name='test');
        insert sc;
        
        Service__c s  			= new Service__c (name='test',ServiceCategory__c = sc.id);
        insert s;
        
        Business_Unit__c bu 	= new Business_Unit__c (name='test');
        insert bu;
        
        RateCard__c rc          = new RateCard__c (Service__c = s.id, Business_Unit__c = bu.id);
        insert rc;
        
        BDT_RateCardController myController = new BDT_RateCardController();

        Boolean dummy = myController.showRateCardList;

        myController.BusinessUnit = bu.id;
        myController.ServiceCategoryId = sc.id;

        List<SelectOption> la = myController.getBUList();

        List<ServiceCategory__c> lb = myController.getServiceCategories();

        dummy = myController.showRateCardList;

        myController.buildRCCOut();
        myController = new BDT_RateCardController();

        PageReference pr = myController.dummy();
        ApexPages.currentPage().getParameters().put('serviceID', s.id);
        ApexPages.currentPage().getParameters().put('businessunitID', bu.id);
        pr = myController.EditPage();
		
		myController.getBusinessUnitName();
		myController.getBusinessUnitCurrency();
		
		
		BDT_RateCardController.RateCard_w rcItem = new BDT_RateCardController.RateCard_w('',
																						new Map<string,sObject>(),
																						new Map<string,list<sObject>>());
		
		BDT_RateCardController.Service_w srvItem = new BDT_RateCardController.Service_w(new Service__c(),
																					new Map<string,sObject>(),
																					new Map<string,list<sObject>>());
		
		BDT_RateCardController.ServiceCategory_w srvCatItem = new BDT_RateCardController.ServiceCategory_w(
																				new ServiceCategory__c(),
																				new Map<string,list<sObject>>(),
																				new Map<string,sObject>(),
																				new Map<string,list<sObject>>() );
	}
}