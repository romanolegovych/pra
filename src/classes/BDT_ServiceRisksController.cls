public with sharing class BDT_ServiceRisksController {
	
	public List<ServiceCategory__c> ServiceCategoryList{get;set;}
	public String ServiceCategoryId{get;set;}
	public string ServiceCategoryName{get;set;}
	public List<Service__c> ServiceList{get;set;}
		
	public class ServiceRiskWrapper{
  		public String serviceName{get;set;}
  		public string serviceId{get;set;}
  		public serviceRisk__c serviceRisk{get;set;}
  		
  		
  		public ServiceRiskWrapper(String sServiceName, string sServiceId, ServiceRisk__c sServiceRisk){
  			serviceName = sServiceName;
  			serviceId = sServiceId;
  			serviceRisk = sServiceRisk;
  		}
  	}
	public List<ServiceRiskWrapper> ServiceRiskList{get;set;}
	
	public BDT_ServiceRisksController(){
		//ScId is only set the moment a user returns from the NewEditServiceRisks page
		
		String ScId = system.currentPageReference().getParameters().get('ScId');
		loadServiceCategories();
		if(ScId!=null){
			loadServiceRisksPerCategory( ScId );
		}
	}

	public void loadServiceCategories(){
		ServiceCategoryList=[Select Name, Code__c 
		                     from ServiceCategory__c
		                     where BDTDeleted__c=false
		                     order by code__c];
	}
	
	public List<ServiceCategory__c> getServiceCatListClean(){
  		List<ServiceCategory__c> serviceCategoryRep = new List<ServiceCategory__c>();
  	
  	 	for(ServiceCategory__c sc: ServiceCategoryList){
    		// remove the leading zero's as they are only for sorting purposes
    		sc.Code__c = BDT_Utils.removeLeadingZerosFromCat(sc.Code__c);
    		serviceCategoryRep.add(sc);
	    }
  		return serviceCategoryRep;
  	}
  
  	public void showServiceRisksPerCategory(){
  		ServiceCategoryId = system.currentPageReference().getParameters().get('ServiceCategoryId');
  		ServiceCategoryName = system.currentPageReference().getParameters().get('ServiceCategoryName');
  		system.debug('ServiceCategoryName: '+ ServiceCategoryName);
  		loadServiceRisksPerCategory( ServiceCategoryId );
   
  	}
  
  	public void loadServiceRisksPerCategory(String serviceCategoryId ){
  		ServiceRiskList = new List<ServiceRiskWrapper>();
  		
  		ServiceList = [Select Id, Name, ServiceCategory__r.name, (Select Name, CommonRisk__c, CommonMitigation__c 
  										From ServiceRisks__r
  										where BDTDeleted__c=false) 
  						from service__c
  						where serviceCategory__c = :ServiceCategoryId
  						and BDTDeleted__c = false 
  						order by SequenceNumber__c];
		
		if(ServiceList.size()>0){
			ServiceCategoryName = ServiceList[0].ServiceCategory__r.name;
		}
		
  	  	ID oldServiceId;
  	    String serviceName;
  	    Boolean RiskFound;
  		for(Service__c ser: ServiceList){
   		 	RiskFound = false;	
  		  	for(ServiceRisk__c sr: ser.ServiceRisks__r ){
  		  		RiskFound = true;
   				if (oldServiceId==null||oldServiceId!= ser.id) {
	  				serviceName = ser.name;
	  			} else {
	  				serviceName ='';
	  			}
	  			
	  			ServiceRiskList.add(new ServiceRiskWrapper(serviceName, ser.id,sr  ));
	  			oldServiceId = ser.id;
  			}
  			if(!RiskFound){
  				ServiceRiskList.add(new ServiceRiskWrapper(ser.Name, ser.id,new ServiceRisk__c(service__c = ser.id) ));
  			}
  			
  		}
  	} 
	public PageReference editServiceRisks(){
		String ServiceId = system.currentPageReference().getParameters().get('ServiceId');
		PageReference editServiceRiskPage = new PageReference(System.Page.BDT_NewEditServiceRisk.getUrl());
		editServiceRiskPage.getParameters().put('ServiceId',ServiceId);
	
		return editServiceRiskPage;
	}
	
}