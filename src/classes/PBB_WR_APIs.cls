public class PBB_WR_APIs {
   /** 
    @ author Niharika Reddy
    @ date 2014
    @ Scenario attributes wrapper class
    */
    public class ScenarioAttributeswrapper {
        public ID WFMProjectId               {get; set;}
        public ID WRModelId                  {get; set;}
        public ID TemplateWRFlowId           {get; set;} 
        public Date ProjectDateStart         {get; set;}
        public ID PAWSProjectId              {get; set;} 
        public Boolean IsModified             {get; set;}
        public string ScenarioId             {get; set;}                    
       
        public ScenarioAttributeswrapper(){
           }
     }
       
   /**
    @ author Niharika Reddy
    @ date 2014 
    @ Method to get Scenario attributes from Scenario Object 
    */
    public static ScenarioAttributeswrapper GetScenarioAttributes(ID scenarioId) {
        ScenarioAttributeswrapper sce = new ScenarioAttributeswrapper ( );     
        PBB_Scenario__c sc = PBB_DataAccessor.getScenarioByID(scenarioId);             
        sce.WFMProjectId = sc.Bid_Project__r.PRA_Project_ID__c;
        sce.WRModelId    = sc.WR_Model__c;
        sce.TemplateWRFlowId = sc.WR_Model__r.Flow__c;
        sce.PAWSProjectId = sc.Paws_Project_ID__c;
        sce.IsModified = sc.Is_Modified__c;
        sce.ScenarioId = sc.Name;
        sce.ProjectDateStart = sc.Project_Start_Date__c;
        return Sce;
      }
      
   /** 
    @ author Niharika Reddy
    @ date 2014
    @ Method to get Selected Countries from Model
    */
    public static List<PBB_Scenario_Country__c> GetSelectedCountries(ID scenarioId) {
        List<PBB_Scenario_Country__c> countryList = PBB_DataAccessor.GetSelectedCountries(scenarioId);                                                
        return countryList;
    }
    
       /** 
    @ author Niharika Reddy
    @ date 2014
    @ Method to get Selected Countries from Model
    */
    public static List<PBB_Scenario_Country__c> GetSelectedProjectCountries(ID scenarioId) {
        List<PBB_Scenario_Country__c> prjcountryList = PBB_DataAccessor.GetSelectedProjectCountries(scenarioId);                                                
        return prjcountryList;
    }
       
   /** 
    @ author Niharika Reddy
    @ date 2014
    @ Method to get Service tasks from Model
    */
    public static List<Countries_Service_Tasks__c> GetSelectedServiceTasks(ID scenarioId, ID countryId) {
        List<Countries_Service_Tasks__c> ServiceTaskList = PBB_DataAccessor.GetSelectedServiceTask (scenarioId,countryId);   
        return ServiceTaskList ;
    }
       
    /** 
     @ author Niharika Reddy
     @ date 2014
     @ Method to get Service Task from Model
     */
    public static List<Service_Task_To_Flow_Step__c> GetAssociatedServiceTasksToStep (ID wrModelId, Set<ID>  wrStepId) {
        List<Service_Task_To_Flow_Step__c> ServiceTaskToStepList = PBB_DataAccessor.GetAssociatedServiceTasksToStep (wrModelId,wrStepId);   
        return ServiceTaskToStepList;
       }
       
   /** 
   @ author Niharika Reddy
   @ date 2014
   @ Method to get Job Position
    */
    public static List<Job_Class_Desc__c> GetJobPositionList () {
        List<Job_Class_Desc__c>  JobPositionList   =  PBB_DataAccessor.GetJobPositionList();                                                
        return JobPositionList;
       }
       
   /** 
   @ author Niharika Reddy
   @ date 2014
   @ Method to Link Scenario To PAWS Project (will be called after a PAWS Project is generated for a Scenario)
    */
    public static void LinkScenarioToPAWSProject(ID scenarioId, String pawsProjectId) {
        PBB_Scenario__c PbbScenario = PBB_DataAccessor.getScenarioByID(scenarioId);    
        PbbScenario.Paws_Project_ID__c = pawsProjectId;
        upsert PbbScenario;
    }
        
   /** 
   @ author Niharika Reddy
   @ date 2014
   @ Method to get PAWS Project After Update (needed for setting "IsModified" flag on a Scenario back to "false")
   */
    public static void PAWSProjectAfterUpdate(ID scenarioId) {
        PBB_Scenario__c  PbbScenario = PBB_DataAccessor.getScenarioByID(scenarioId);     
        PbbScenario.Is_Modified__c = false;
        update  PbbScenario;
    } 
        
   /** 
    @author Niharika Reddy
    @date 2014
    @ Method to get unique Service Tasks from Country Service tasks group by Country
    */
    public static List<Service_Task__c> countryServiceTasksGroupbyServicetask(String PBBScenario) {
        Set<String> TaskIDs = new Set<String>() ;
        for (AggregateResult a : PBB_DataAccessor.groupbyServiceTask(PBBScenario) ) { 
            String s=String.valueof(a.get('Service_Task__c'));
            TaskIDs.add(s);
         }
         return PBB_DataAccessor.getServiceTaskList(TaskIDs);
     }
     
     /** 
    @ author Kondal Reddy
    @ date 2015
    @ Patient Enrollment attributes wrapper class
    */
    public class PatientEnrollmentwrapper {
        public Date     FPI_Planned             {get; set;}
        public Date     LPI_Planned             {get; set;}
        public Integer  TargetNoPatients        {get; set;} 
        public Date     LPI_HighRisk            {get; set;}
        public Date     LPI_MedRisk             {get; set;} 
        public Date     LPI_LowRisk             {get; set;}
        public Integer  EnrolledToDate          {get; set;}                    
       
        public PatientEnrollmentwrapper() {
       }
    }
    
    /**
    @ author Niharika Reddy
    @ date 2015 
    @ Method to get Scenario attributes from Scenario Object 
    */
    public static PatientEnrollmentwrapper GetPatientEnrollmentAttributes(String ProtocolUniqueKey ) {
        PatientEnrollmentwrapper sce = new PatientEnrollmentwrapper ( ); 
        try{    
            WFM_Protocol__c sc = PBB_DataAccessor.getProtocolByUniqueKey(ProtocolUniqueKey);
            System.debug('*************protocol********'+sc);                               
            sce.FPI_Planned = sc.Contracted_First_Subj_Enroll__c;
            sce.LPI_Planned    = sc.Contracted_Last_Subj_Enroll__c;
            sce.TargetNoPatients = Integer.valueOf(sc.Expected_Patients__c);
            sce.LPI_HighRisk = sc.Aggressive_Scenario_LPI__c;
            sce.LPI_MedRisk = sc.Medium_Scenario_LPI__c;
            sce.LPI_LowRisk = sc.Conservative_Scenario_LPI__c;            
            sce.EnrolledToDate = Integer.valueOf(sc.Enrolled_To_Date__c);
            //Date todate = Date.Today().toStartOfWeek(); //ToDo change to current week
		/*	Date currentWeekStartDate = SRM_Utils.getStartOfWeek(Date.Today());
			Date previousWeekStartDate =  currentWeekStartDate.addDays(-7);             
            String querystring;
            querystring ='select Name na,Week_Date__c w,sum(Planned_Active_Sites__c) ps,sum(Actual_Active_Sites__c) asi,sum(Aggressive_Active_Sites__c) prs,';
            querystring+=' sum(Planned_Subjects__c) psub,sum(Actual_Subjects__c) asub,sum(Aggressive_Subjects__c) prsub ';
            querystring+=' from Country_Weekly_Event__c';
            querystring+=' where Protocol_Country__r.Client_Protocol__r.Protocal_Unique_Key__c =:ProtocolUniqueKey ';
            querystring+=' and Week_Date__c = :previousWeekStartDate';             
            querystring+=' GROUP BY Name,Week_Date__c ';
            System.debug('----querystring-----'+ querystring);
            
            for(AggregateResult ara : database.query(querystring)){
                System.debug('-----------By Weekly=Sum-Of-actual&planned-subjects&sites=given Scenaro ------------' + ara.get('na') + '\t' +
                                                                                    ara.get('ps') + '\t' + ara.get('asi')+ '\t' +
                                                                                    ara.get('psub') + '\t' + ara.get('asub') + '\t' + ara.get('w'));
            sce.EnrolledToDate = Integer.Valueof(ara.get('asub'));
            }*/
            system.debug('******* SCE *******'+sce);    
        }
        catch (exception ex){
            SRM_Utils.showErrorMessage('Error while returning the dates');          
        }
        return Sce;
    }   
    
    /**
    @ author Kondal Reddy
    @ date 2015 
    @ Method to get Approved PBB Scenario for wfmProject
    */
    public static PBB_Scenario__c getAprrovedScenarioID( String wfmProjectName ){
        PBB_Scenario__c scenario2Return;
        
        List<PBB_Scenario__c> scenarioList = [SELECT Id, Name, Bid_Project__c 
                                              FROM PBB_Scenario__c 
                                              WHERE Bid_Project__r.PRA_Project_ID__r.Name =: wfmProjectName 
                                                    AND Bid_Status__c = 'Approved' 
                                              LIMIT 1];
        if( !scenarioList.isEmpty() ) scenario2Return = scenarioList[0];
        
        return scenario2Return;
        //return [select id, name, Bid_Project__c from PBB_Scenario__c where Bid_Project__r.PRA_Project_ID__r.Name =: wfmProjectName and status__c = 'Approved' LIMIT 1];
    }  
    /**
    @ author Niharika Reddy
    @ date 2015 
    @ description This method returns a map of country Ids and Pbb Weekly Events(Start Date,plannedSites), given PBB Scenario Id and PBB Country Id.
    */
    // Remove below comment to return list of wrapper class
       public static List<CountryWeeklyEventsData> getPbbWeeklyEvntsByCountryId(Id pbbId, Id CntryId){
        
        String querystring;
        querystring = 'Select name, Week_Date__c, srm_scenario_country__r.pbb_Scenario_country__r.Country__c sm, sum(Planned_Sites_Non_Cumulative__c) ps, sum(Planned_Sites__c) psites, sum(Planned_Subjects__c) psub ';
        querystring+= 'From PBB_Weekly_Events__c ';
        querystring+= 'Where srm_scenario_country__r.pbb_Scenario_country__r.pbb_scenario__c = :pbbId ';
        if(CntryId!=null){
            querystring+='AND srm_scenario_country__r.pbb_Scenario_country__r.country__c = :CntryId ';
        }
        querystring+= 'GROUP BY  name,srm_scenario_country__r.pbb_Scenario_country__r.country__c,Week_Date__c ';
        querystring+= 'ORDER BY  srm_scenario_country__r.pbb_Scenario_country__r.country__c,Week_Date__c ';
        
        //Wrapper class list to hold data for weekly events by country
        List<CountryWeeklyEventsData> cwedList = new List<CountryWeeklyEventsData>();
        
        //Map to hold country id and its related values
        Map<Id, List<WeekDatePlannedSites>> wdplMap = new Map<Id, List<WeekDatePlannedSites>>();
        List<AggregateResult> arlist = Database.query(querystring);
        for(AggregateResult ar: arlist){
            List<WeekDatePlannedSites> wdplList = new List<WeekDatePlannedSites>();
            if(wdplMap.containsKey((ID)ar.get('sm'))){
                wdplList = wdplMap.get((ID)ar.get('sm'));
            }
            WeekDatePlannedSites wdps = new WeekDatePlannedSites((Date)ar.get('week_date__c'),(Decimal)ar.get('ps'),(Decimal)ar.get('psub'),(Decimal)ar.get('psites'));
            wdplList.add(wdps);
            wdplMap.put((ID)ar.get('sm'), wdplList);       
        }
        system.debug(wdplMap.keySet());
        // When all processing in for loop completes and map contains any record, then add those records to cwedList
        // By creating wrapper class object
        if(!wdplMap.isEmpty()){
            for(integer i=0; i<wdplMap.keySet().size(); i++){
                CountryWeeklyEventsData cwedObj = new CountryWeeklyEventsData(new List<ID>(wdplMap.keySet())[i], wdplMap.get(new List<ID>(wdplMap.keySet())[i]));
                system.debug(cwedObj);
                cwedList.add(cwedObj);      
            }  
        }
        
        System.debug('==== cwedList final ==== '+cwedList);
        //Return final wrapper class list
        return cwedList;
    }
     
     public class WeekDatePlannedSites{
         public Date weekDate{ get; set; }
         public Decimal plannedSites{ get; set; }
         public Decimal plannedSubjects{get; set; }
         public Decimal plannedSitesCumulative{ get; set; }
         public WeekDatePlannedSites(Date weekDate, Decimal plannedSites, Decimal plannedSubjects, Decimal plannedSitesCumulative){
             this.weekDate = weekDate;
             this.plannedSites = plannedSites;
             this.plannedSubjects = plannedSubjects;
             this.plannedSitesCumulative = plannedSitesCumulative;
         }
     }
     
     
     public class CountryWeeklyEventsData{
         public String countryId{ get; set; }
         public List<WeekDatePlannedSites> wdpsList{ get; set; }
         public CountryWeeklyEventsData(String countryId, List<WeekDatePlannedSites> wdpsList){
             this.countryId = countryId;
             this.wdpsList = wdpsList;
         }
     }
     
    /**
    @ author Niharika Reddy
    @ date 2015 
    @ description This is the wrapper class to hold site and site date(count values)
    */
    public class WrapperSite{
         public String Site{get;set;}
         public map<String,Integer> siteData {get;set;}
         public String Country {get;set;}
         wrapperSite(String s,String c,map<String,Integer> si){
             this.site = s;
             this.Country = c ; 
             this.siteData = si;
             
         }
      }
    
    /**
    @ author Niharika Reddy
    @ date 2015 
    @ description This method returns a wrapper with site and count values for the status, 
                  so basically getting patient Enrollment status by site
    */  
    public static list<wrapperSite> getPatientEnrollmntStatusBySite(String protocolUniqKey){
        list<WrapperSite> siteWrapper = new list<WrapperSite>();
        
        for(WFM_Site_Detail__c w : PBB_DataAccessor.getPatientEnrollmntStatusBySite(protocolUniqKey)){
            map<String, Integer> statusAndCount = new map<String,Integer>(); 
        
            statusAndCount.put('Screened',Integer.valueOf(w.Count_Screened__c));
            statusAndCount.put('Enrolled',Integer.valueOf(w.Count_Enrolled__c));
            statusAndCount.put('Screen Failure',Integer.valueOf(w.Count_Screen_Failure__c));
            statusAndCount.put('Completed',Integer.valueOf(w.Count_Completed__c));
            statusAndCount.put('Discontinued',Integer.valueOf(w.Count_Discontinued__c));
            statusAndCount.put('In-Screening',Integer.valueOf(w.Count_InScreening__c));
            statusAndCount.put('Active',Integer.valueOf(w.Count_Active__c));
            statusAndCount.put('Follow-Up',Integer.valueOf(w.Count_FollowUp__c));
            
            siteWrapper.add(new WrapperSite(w.Name+' '+w.Investigator__r.Last_Name__c,w.country__c,statusAndCount));
            System.debug('///site info by site/////////'+siteWrapper);
        }   
        System.debug('*******siteWrapper'+siteWrapper);  
        return siteWrapper;
     }
     
     /**
    @ author Niharika Reddy
    @ date 2015 
    @ description This is the wrapper class to hold country and status count data 
    */
    public class WrapperCountry{
         public Object country{get;set;}
         public map<String,Integer> countryData {get;set;}
         wrapperCountry(Object c,map<String,Integer> co){
             this.country = c;
             this.countryData = co;
         }
      }
      
      /**
    @ author Niharika Reddy
    @ date 2015 
    @ description This method returns a wrapper with country and count values for the status, so basically getting 
                  Patient Enrollment Status by Country
    */
    public static list<wrapperCountry> getPatientEnrollmntStatusByCountry(String protocolUniqKey){
        list<WrapperCountry> countryWrapper = new list<WrapperCountry>();
        
        for(AggregateResult w : PBB_DataAccessor.getPatientEnrollmentStatusByCountry(protocolUniqKey)){
            map<String,Integer> statusAndCount = new map<String,Integer>(); 
           
            statusAndCount.put('Screened',Integer.valueOf(w.get('screened')));
            statusAndCount.put('Enrolled',Integer.valueOf(w.get('enrolled')));
            statusAndCount.put('Screen Failure',Integer.valueOf(w.get('screenFail')));
            statusAndCount.put('Completed',Integer.valueOf(w.get('completed')));
            statusAndCount.put('Discontinued',Integer.valueOf(w.get('discontinued')));
            statusAndCount.put('In-Screening',Integer.valueOf(w.get('inScreening')));
            statusAndCount.put('Active',Integer.valueOf(w.get('active')));
            statusAndCount.put('Follow-Up',Integer.valueOf(w.get('followup')));
            
            countryWrapper.add(new WrapperCountry(w.get('Country__c'),statusAndCount));
            System.debug('///site info by site/////////'+countryWrapper);
        }   
        System.debug('*******siteWrapper'+countryWrapper);  
        return countryWrapper;
     }
     
     /**
    @ author Niharika Reddy
    @ date 2015 
    @ description This is the wrapper class to hold rollup of pbb weekly events data
    */
    public class CountryWeeklyEvents {
       public Date WeekDate {get;set;}
       public Integer AggressiveSubjects{get;set;}
       public Integer ConservativeSubjects {get;set;}
       public Integer PlannedSubjects {get;set;}
       public Integer ActualSubjects {get;set;}
       public Integer MediumRiskSubjects {get;set;}
        
       public CountryWeeklyEvents (Date wDate,Integer aggSub,Integer consSub,Integer planSub,Integer actSub,Integer medRskSub){
            this.WeekDate= wDate;
            this.AggressiveSubjects= aggSub;
            this.ConservativeSubjects = consSub;
            this.PlannedSubjects = planSub;
            this.ActualSubjects = actSub;
            this.MediumRiskSubjects = medRskSub;
        }
    } 
     
    /**
    @ author Niharika Reddy
    @ date 2015 
    @ description This method returns a wrapper with weekStartDate,ReprojectedSubjects,MediumRiskSubjects,ActualSubjects,LowRiskSubjects and Planned Sites
    */
    public static list<CountryWeeklyEvents> weekEventsByProject(String protocolUniqueKey){
        list<CountryWeeklyEvents> weeklyRollup = new list<CountryWeeklyEvents>();
        for(AggregateResult r : PBB_DataAccessor.getGrpdweeklybyProtocolUniqKey(protocolUniqueKey)){
           weeklyRollup.add(new CountryWeeklyEvents (Date.valueOf(r.get('w')),
                                              Integer.valueOf(r.get('aggSub')),
                                              Integer.valueOf(r.get('consSub')),
                                              Integer.valueOf(r.get('planSub')),
                                              Integer.valueOf(r.get('actSub')),
                                              Integer.valueOf(r.get('medRskSub'))
                                              ));   
        }
        System.debug('=====weeklyRollup=========='+weeklyRollup);
        return weeklyRollup;
    }
    public class WrapperObj{
        public ID countryID{get;set;}
        public Date WeekDate{get;set;}
        public Decimal SitesActivated{get;set;}
        public Decimal SitesPredictedLow{get;set;}
        public Decimal SitesPredictedHigh{get;set;}
        public Decimal SitesPredictedMedium{get;set;}
        public Boolean lastWeek{get;set;}
        
        public wrapperObj(ID c,Date w,Integer sa,Integer spl,Integer sph,Integer sam, Boolean lw){
            this.countryID = c;
            this.WeekDate= w;
            this.SitesActivated= sa;
            this.SitesPredictedLow= spl;
            this.SitesPredictedHigh= sph;
            this.SitesPredictedMedium = sam;
            this.lastWeek = lw;
        }      
    }
    
    public static list<WrapperObj> getcountryWeeklyevents(String protocoluniquekey){
        list<WrapperObj> returnlist = new list<WrapperObj>();
        for(Country_Weekly_Event__c c : [select id, name, week_date__c, protocol_country__r.Region__c from Country_Weekly_Event__c where Protocol_Country__r.Client_Protocol__r.Protocal_unique_key__c =:protocoluniquekey] ){
            WrapperObj wbj = new WrapperObj((ID)c.protocol_country__r.Region__c,(Date)c.week_date__c,100,100,100,100,false);  
            returnlist.add(wbj);
        }
        return returnlist;
    }
    
    public static void setWeeklyEventsByProtocolUniqKey(String protocolUniqKey, list<WrapperObj> wrapList){
        Map<Id,Protocol_Country__c> protocolCountryMap = PBB_DataAccessor.getMapOfProtocolCountries(protocolUniqKey);
        deleteAndClearReprojections(protocolUniqKey);
        setSAWeeklyEvents(protocolUniqKey,wrapList,protocolCountryMap);
        updateWeekEventsWithPrevious(protocolCountryMap.keySet(), protocolUniqKey);
    }
    
    public static void setSAWeeklyEvents(String protocolUniqKey, list<WrapperObj> wrapList, Map<Id,Protocol_Country__c> protocolCountryMap){
        list<country_weekly_event__c> upsertList = new list<country_weekly_event__c>();
        Date contractLPIdate = SRM_Utils.getStartOfWeek([Select Id, name, Contracted_Last_Subj_Enroll__c from WFM_Protocol__c where Protocal_Unique_Key__c =: protocolUniqKey].Contracted_Last_Subj_Enroll__c);
        Date todate = SRM_Utils.getStartOfWeek(Date.Today());
        Boolean isCurrentDateBeyondPlanned = checkIfBeyondPlanned(contractLPIdate,todate);
        for(WrapperObj w : wrapList){
            country_weekly_event__c week = new country_weekly_event__c(); 
            week.External_Id__c = protocolUniqKey+':'+protocolCountryMap.get(w.countryId).Region__r.Name+':'+SRM_Utils.getDateInStringFormat(w.weekDate);
            week.Protocol_Country__c = protocolCountryMap.get(w.countryId).Id;
            week.week_date__c = w.weekDate;
            week.Actual_Active_Sites__c = w.SitesActivated;
            week.Conservative_Active_Sites__c = w.SitesPredictedLow;
            week.Aggressive_Active_Sites__c = w.SitesPredictedHigh;
            week.Medium_Active_Sites__c = w.SitesPredictedMedium ;
            week.Expected_Active_Sites__c = w.SitesPredictedHigh;
            upsertList.add(week);
            if(w.lastWeek){
                if(!isCurrentDateBeyondPlanned){
                    setSAThroughPlannedDuration(week,contractLPIdate,upsertList);   
                }
                else{
                    setSAThroughPlannedDuration(week,toDate,upsertList);
                }
            }
        }
        upsert upsertList External_Id__c;
        
    }
    
    public static void deleteAndClearReprojections(String protocolUniqKey){
        list<country_weekly_event__c> clearReprojections = new list<country_weekly_event__c>();
        list<country_weekly_event__c> deleteList = new list<country_weekly_event__c>();
        for(country_weekly_event__c we : PBB_DataAccessor.getweeklyEventsByProtocolUniqueKey(protocolUniqKey)){
            if(we.Planned_Enrollment_Rate__c == null && we.Actual_Enrollment_Rate__c == null){
                deleteList.add(we);
            }
            else{
                we.Aggressive_Active_Sites__c = null;
                we.Aggressive_Enrollment_Rate__c = null;
                we.Aggressive_Sub_Non_Cum__c = null;
                we.Aggressive_Subjects__c = null;
                we.Applied_Adjustment_Factor__c = null;
                we.Conservative_Active_Sites__c = null;
                we.Conservative_Subjects__c = null;
                we.Expected_Active_Sites__c = null;
                //we.Expected_Subjects__c = null;
                we.Medium_Active_Sites__c = null;
                we.Medium_Risk_Subjects__c = null;
                we.Rolling_Adjustment_Factor__c = null;
                we.Sites_to_Apply_to__c = null;
                we.Sites_Used_Weighing__c = null;
                clearReprojections.add(we);
            }
        }
        delete deleteList;
        update clearReprojections;
    }
    
    public static void updateWeekEventsWithPrevious(Set<Id> countryIdSet, String protocolUniqKey){
        List<Country_Weekly_Event__c> cntryWeekToUpdate = new List<Country_Weekly_Event__c>();
		for(Protocol_Country__c pc : PBB_DataAccessor.getProtocolCountriesAndWeekEvents(countryIdSet,protocolUniqKey)){
			Map<String, Country_Weekly_Event__c> cntryWkEventsInAscOrderMap = new Map<String, Country_Weekly_Event__c>();
			for(Country_Weekly_Event__c w : pc.Country_Weekly_Events__r){
                String prevUniqueKey = (w.External_Id__c).substringBeforeLast(':')+':'+SRM_Utils.getDateInStringFormat((w.Week_Date__c).addDays(-7));
                String curUniqueKey = (w.External_Id__c).substringBeforeLast(':')+':'+SRM_Utils.getDateInStringFormat(w.Week_Date__c);
                System.debug('prevUniqueKey  '+prevUniqueKey+' curUniqueKey  '+curUniqueKey);
                if(cntryWkEventsInAscOrderMap.containsKey(prevUniqueKey)){
                    if(w.Previous_Weekly_Event__c == null){
                     	w.Previous_Weekly_Event__c = cntryWkEventsInAscOrderMap.get(prevUniqueKey).Id;
                        w.week_Number__c = cntryWkEventsInAscOrderMap.get(prevUniqueKey).week_Number__c + 1;
                        w.Name = 'Week '+w.week_Number__c;
                    	cntryWeekToUpdate.add(w);
                    }
                }
				cntryWkEventsInAscOrderMap.put(curUniqueKey,w);
			}
		}
		Database.saveResult[] updateWeeklyEvents = Database.update(cntryWeekToUpdate, false);
        System.debug('*** updateWeeklyEvents *** '+ updateWeeklyEvents);  
    }
    public static void setSAThroughPlannedDuration(country_weekly_event__c lastweek, Date FinalWeekDate, List<Country_Weekly_Event__c> updateWeeklyevents){
        Date weekDate = (lastweek.Week_Date__c).addDays(7);
        String uniquekey = (lastweek.External_Id__c).substringBeforeLast(':');
        for(Date i = Weekdate; i <= FinalWeekDate; i.addDays(7)){
            country_weekly_event__c week = new country_weekly_event__c();
            week.External_Id__c = uniquekey+':'+SRM_Utils.getDateInStringFormat(i);
            week.week_date__c = i;
        	week.Actual_Active_Sites__c = lastWeek.Actual_Active_Sites__c;
            week.Conservative_Active_Sites__c = lastWeek.Conservative_Active_Sites__c;
            week.Aggressive_Active_Sites__c = lastWeek.Aggressive_Active_Sites__c;
            week.Medium_Active_Sites__c = lastWeek.Medium_Active_Sites__c;
            week.Expected_Active_Sites__c = lastweek.Expected_Active_Sites__c;
        	updateWeeklyevents.add(week);
        }
    }
    
    public static Boolean checkIfBeyondPlanned(Date contractLPIdate, Date todate){
        if(todate > contractLPIdate){
            return true;
        }
        else{
            return false;
        }
    }
	
    public static map<ID,String> getPatientEnrollmentStatusPerProtocol(set<ID> ProtocolIdSet){
        map<ID,String> protocolStatus = new map<ID,String>();
        Date currentWeekStartDate = SRM_Utils.getStartOfWeek(Date.Today());
        Date previousWeekStartDate =  currentWeekStartDate.addDays(-7);         
        System.debug('*****last week start Date*******'+ previousWeekStartDate );
          
        for(AggregateResult ar : PBB_DataAccessor.getAggResultByProtocolSet(ProtocolIdSet,currentWeekStartDate,previousWeekStartDate)){
            System.debug('////////record/////////'+ar);                  
            if(!protocolStatus.containskey(String.valueOf(ar.get('proID')))){
                if(Integer.valueOf(ar.get('asub')) == null)
                    protocolStatus.put(String.valueOf(ar.get('proID')),'Not Started');
                else
                    protocolStatus.put(String.valueOf(ar.get('proID')),'Started');
            }
            else if(protocolStatus.get(String.valueOf(ar.get('proID'))) == 'Started'){
            	if(Integer.valueof(ar.get('asub')) >= Integer.valueof(ar.get('e')))
                    protocolStatus.put(String.valueOf(ar.get('proID')),'Completed');
                else
                    protocolStatus.put(String.valueOf(ar.get('proID')),'Not Completed');    
            }
            else if(protocolStatus.get(String.valueOf(ar.get('proID'))) == 'Not Completed'){
                if(Integer.valueOf(ar.get('consub')) >= Integer.valueOf(ar.get('psub')))
                    protocolStatus.put(String.valueOf(ar.get('proID')),'On Track');
                else if(Integer.valueOf(ar.get('agsub')) >= Integer.valueOf(ar.get('psub')))
                    protocolStatus.put(String.valueOf(ar.get('proID')),'At Risk');
                else    
                    protocolStatus.put(String.valueOf(ar.get('proID')),'Delayed');
            }
        } 
        return protocolStatus;    
    } 
}