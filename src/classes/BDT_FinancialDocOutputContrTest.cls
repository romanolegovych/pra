@isTest
private class BDT_FinancialDocOutputContrTest {

    static testMethod void myUnitTest() {
        // Create test data
		BDT_CreateTestData.createall();
		Client_Project__c project = [select id from Client_Project__c limit 1];
		BDT_Utils.setPreference('SelectedProject', String.valueOf(project.id));
    	// Create the new financial document, with the predefined attributes values   	
		FinancialDocument__c financialDocument = new FinancialDocument__c();
		financialDocument.DocumentStatus__c    = 'Undefined';
		financialDocument.DocumentType__c      = 'Proposal';
		financialDocument.Client_Project__c    = project.id;
		financialDocument.TotalValue__c		   = '-';
		insert financialDocument;
		// Create the controller
		BDT_FinancialDocOutputController p = new BDT_FinancialDocOutputController();
		PageReference pageRef = New PageReference(System.Page.BDT_FinancialDocOutput.getUrl());
		pageref.getParameters().put('financialDocumentId' , financialDocument.id);
		Test.setCurrentPage(pageRef); 
    	p = new BDT_FinancialDocOutputController();
    	p.getFilterOptions();
    	p.closePage();
    	p.showDonwloadPage();
    }
}