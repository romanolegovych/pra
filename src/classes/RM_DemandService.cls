public with sharing class RM_DemandService {

    public RM_DemandService(){
    }
    
    // This method retrieves the Assinment Values on given ProjName , Year_Month List
    public static Map<string,Map<string,Double>> getAssignedValuesByRoleAndYrMths(string projName,List<string> yrMthList,String isHours){
        if (projName == null || yrMthList.isEmpty()){
            return null;
        }         
        Map<string,Map<String,Double>> assignedMap = new Map<string,Map<String,Double>>();
      
        List<AggregateResult> assignedList = RM_DemandDataAccessor.getAssignedByMonthList(projName, yrMthList) ;                                                                                                                                               
        System.debug('*************AssignedDataGrid'+assignedList);    
        Map<string,Double> tempMap = new Map<string,Double>();
        for(AggregateResult ar: assignedList){
            if(assignedMap.containsKey((string)ar.get('role'))){
                system.debug('*******entered into If'+assignedMap.get((string)ar.get('role')));
                tempMap =  assignedMap.get((string)ar.get('role'));
            } else {
                tempMap = new Map<string,Double>();
            }
          
           if(isHours.equalsIgnoreCase('true')){
               tempMap.put((string)ar.get('Year_Month__c'),(Double)ar.get('hours'));
               system.debug('*******tempMap'+tempMap);
           } else {
               tempMap.put((string)ar.get('Year_Month__c'),(Double)ar.get('fte'));
           }
           assignedMap.put((string)ar.get('role'),tempMap);
        }  
        system.debug('AssignedMap:::'+assignedMap);
        return AssignedMap;
    }
    
    //This Method retrieves the map of Planned and Forecast values on given Project Name and Year Maonth List.
    public static Map<string, Map<String,List<Double>>> getPlannedForecastValuesByYrMthAndProject(String projName, List<string>yrMthList, string isHours){
        if (projName == null){
            return null;
        }
        Map<string, Map<String,List<Double>>> plannedRoleMap = new Map<string, Map<String,List<Double>>>();                                                                       
        List<AggregateResult> arList = RM_DemandDataAccessor.getPlannedValuesbyYrMthList(projName, yrMthList) ;                                                               
        Map<String, List<Double>> tempMap = new Map<string,List<Double>>();                     
        for(AggregateResult ar: arList){
            List<Double> tempList = new List<Double>();
            
            if (plannedRoleMap.containsKey((string)ar.get('roleName')) ){
                tempMap = plannedRoleMap.get((string)ar.get('roleName'));           
            } else {
                tempMap = new Map<string,List<Double>>();       
            }
            
            if(isHours.equalsIgnoreCase('true')){
              tempList.add((Double)ar.get('plannedHours'));
              tempList.add((Double)ar.get('forecastHours'));
              tempMap.put((string)ar.get('Year_Month__c'),tempList);              
            } else {
              tempList.add((Double)ar.get('plannedFte'));
              tempList.add((Double)ar.get('forecastFte'));
              tempMap.put((string)ar.get('Year_Month__c'),tempList);
            }       
            plannedRoleMap.put((string)ar.get('roleName'),tempMap);
        }
        return plannedRoleMap;
    }
    
    public static Map<string,Map<string,List<Double>>> getPlannedForecastValuesByYrMthAndProjectRole(string projName, string selectedRole, List<string> yrMthList, string isHours){
        if (projName == null || selectedRole == null || yrMthList.isEmpty()){
            return null;
        }    
        List<AggregateResult> childPlannedList = RM_DemandDataAccessor.getPlannedValuesBySelectedRole(ProjName, SelectedRole, yrMthList);
        // For Country Name + Location Name combination map of Year Months with planned and forecast Values 
        Map<string,Map<string,List<Double>>> locationPlannedMap = new Map<string,Map<string,List<Double>>>();
        Map<string, List<Double>> tempMap = new Map<string,List<Double>>();
        for(AggregateResult childAr: childPlannedList){
            if(locationPlannedMap.containsKey((string)childAr.get('country')+':'+(string)childAr.get('location'))){
                tempMap = locationPlannedMap.get((string)childAr.get('country')+':'+(string)childAr.get('location'));
            } else {
                tempMap = new Map<string,List<Double>>();
            }
            List<Double> tempList = new List<Double>();
            if(isHours.equalsIgnoreCase('true')){
                tempList.add((Double)childAr.get('plannedHours'));
                tempList.add((Double)childAr.get('forecastHours'));
                tempMap.put((string)childAr.get('Year_Month__c'),tempList);
            } else {
                tempList.add((Double)childAr.get('plannedFte'));
                tempList.add((Double)childAr.get('forecastFte'));
                tempMap.put((string)childAr.get('Year_Month__c'),tempList);
            }
            locationPlannedMap.put((string)childAr.get('country')+':'+(string)childAr.get('location'),tempMap);
        }
        system.debug('Location Planned Map:::' +locationPlannedMap);
        return locationPlannedMap;
    }
    
    //this method retrieves the map of the assignvalues for a selected role and year-month list
    public static Map<string,Map<string,Double>> getAssignedValuesByRoleAndYrMthAndProject(string projName, string selectedRole, List<string> yrMthList, string isHours){
        if (projName == null || selectedRole == null || yrMthList.isEmpty()){
            return null;
        }                                                                                      
        List<AggregateResult> childAssignedList = RM_DemandDataAccessor.getAssignedvaluesBySelectedRole(ProjName, SelectedRole, yrMthList)  ;                                                                              
        // For Country Name + Location Name cobination map of Year Months with Assigned Values 
        Map<string,Map<string,Double>> locationAssignedMap = new Map<string,Map<string,double>>();
        Map<string, Double> tempMap = new Map<string,Double>();
        for(AggregateResult childAr: childAssignedList){
            if(locationAssignedMap.containsKey((string)childAr.get('country')+':'+(string)childAr.get('location'))){
                tempMap = locationAssignedMap.get((string)childAr.get('country')+':'+(string)childAr.get('location'));
            } else {
                tempMap = new Map<string,Double>();
            }
            if(isHours.equalsIgnoreCase('true')){
                tempMap.put((string)childAr.get('Year_Month__c'),(Double)childAr.get('hours'));
            } else {
                tempMap.put((string)childAr.get('Year_Month__c'),(Double)childAr.get('fte'));
            }
            locationAssignedMap.put((string)childAr.get('country')+':'+(string)childAr.get('location'),tempMap);
        }
        system.debug('Location Assigned Map:::' +locationAssignedMap);
        return locationAssignedMap;
    }
    
    // This method retrieves WFM_Project_Role__c based on Project Id, Role Name, Country Name and Location Name 
    public static WFM_Project_Role__c getProjectbyRoleCntryLocation(List<string> monthList,string previewRole,string previewCntry,string previewLocation){
       String projId = apexpages.currentpage().getparameters().get('Id');        
        if(projId == null) {           
            return null;
        }
        system.debug('PreviewRole::'+previewRole+'::Preview Country::'+previewCntry+'::Preview Location::'+previewLocation);      
        List<WFM_Project_Role__c> prjectRoles = RM_DemandDataAccessor.getProjectRolesValues(monthList,projId,previewRole,previewCntry,previewLocation); 
        
        if (prjectRoles.isEmpty()){
            return null;
        }        
        return prjectRoles[0];        
    } 
    
    // this method retrieves the combination of Locations and countries  
    public static List<Country_BusinessUnit__c> getCountriesOrLocations(string geographyType, string geographyValue){
        if (geographyType == null || geographyType == '' || geographyValue == null || geographyValue == ''){
            return null;
        }
        List<Country_BusinessUnit__c> countryLocations = new List<Country_BusinessUnit__c>();
        if (geographyType.equalsIgnoreCase('Country')){
            countryLocations = [Select country__c, Business_Unit__c,Country_Name__c, Business_Unit_Name__c
                                From Country_BusinessUnit__c
                                Where Country_Name__c = :geographyValue 
                                ORDER BY Business_Unit_Name__c ASC NULLS FIRST];
        } else {
            countryLocations = [Select country__c, Business_Unit__c,Country_Name__c, Business_Unit_Name__c
                                From Country_BusinessUnit__c
                                Where Business_Unit_Name__c = :geographyValue
                                ORDER BY Country_Name__c ASC NULLS FIRST];
        
        }    
        
        return countryLocations;
    }      
    
    // this method gets the country hours for the respective countries and the Year-Months
     public static Map<String,WFM_Country_Hours__c> getCountryHours(List<String> yrMonths,String previewCntry){
        system.debug('***YearMonth Values:::'+yrMonths+':::Preview Country:::'+previewCntry);
        if (yrMonths.isEmpty() || previewCntry == null){
            return null;
            }
         Id CountryId = getCountryId(previewCntry) ;    
        RM_OtherService.InsertCountryHourByCountryID(CountryId,yrMonths);
        List<WFM_Country_Hours__c> countryHours = new List<WFM_Country_Hours__c> ([Select Id, Name, Country_Name__c, Country_Name__r.Name,
                                                                                    Country_Hour_Unique_Key__c, Year_Month__c
                                                                                 From WFM_Country_Hours__c
                                                                                 Where Country_Name__r.Name = :previewCntry
                                                                                 and Year_Month__c =: yrMonths ]);
      
                                                                                                                                                    
        Map<String,WFM_Country_Hours__c> countryHoursMap = new Map<String,WFM_Country_Hours__c>();
        for(WFM_Country_Hours__c wch : countryHours){
           countryHoursMap.put(wch.Year_Month__c,wch); 
        }                                             
        system.debug('*****country hour'+countryHoursMap);                   
        return countryHoursMap;    
    } 
    
    // this method retrieves all the countries
    public static List<Country__c> getAllCountries(){
        return new List<Country__c>([Select c.PRA_Country_Id__c, c.Name, c.Id, c.Country_Code__c,
                                    (Select Name, Country_Name__c, Country_Hour_Unique_Key__c, Year_Month__c, Total_Working_Hours__c From Country_Hours__r)
                                    From Country__c c  where c.Name!= 'Regional' ORDER BY c.Name ASC NULLS FIRST]);        
    
    }
    
    // this method retieves all the locations
    public static List<Business_Unit__c> getAllLocations(){    
        return new List<Business_Unit__c>([Select b.Name, b.Default_Country__r.CreatedById, b.Default_Country__r.CreatedDate, b.Default_Country__r.Name, b.Default_Country__c, b.Currency__c 
                                                            From Business_Unit__c  b  ORDER BY b.Name ASC NULLS FIRST]);
    
    }
    
    //
     public static Id getLocationId(string previewLocation){
        if(previewLocation == null){
            return null;            
        } 
        
        return [select Id From Business_Unit__c where Name = :previewLocation Limit 1].Id;
    }
    
    //
    public static Id getRoleId(string previewRole){
        if(previewRole== null){
            return null;            
        }
        system.debug('***********preview role'+previewRole);
        return [select Id From Job_Class_Desc__c where Name = :previewRole Limit 1].Id;
    }
    
    //
    public static Id getCountryId(string previewCntry){
        if(previewCntry == null){
            return null;            
        }
        return [select Id From Country__c where Name = :previewCntry Limit 1].Id;
    }
    
    //
    //public static WFM_Project__c getProject(){
    //    String projId = apexpages.currentpage().getparameters().get('Id');        
    //    if(projId == null) {           
    //        return null;
    //    }
    //    return [Select Id,Name, Project_Start_Date__c, Project_End_Date__c from WFM_Project__c where Id =: projId ];
    //}
       
}