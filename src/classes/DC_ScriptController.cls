/*********************************************************************************
 Name         :   DC_ScriptController
 Author       :   Ramya Shree Edara
 Created Date :   Feb 21,2014
 Purpose      :   DC_ScriptControllerfor to clean up all sensitive Data
*********************************************************************************/
public class DC_ScriptController {

    Public String baseURL {
        get;
        set;
    }
    public boolean PRAFIT {
        get;
        set;
    }
    public boolean HRExitInterview {
        get;
        set;
    }
    public boolean Chatter {
        get;
        set;
    }
    public boolean Files {
        get;
        set;
    }
    public boolean ReinstateImages {
        get;
        set;
    }
    public DC_ScriptController() {
        baseURL = System.URL.getSalesforceBaseUrl().toExternalForm();
        System.debug(System.URL.getSalesforceBaseUrl().toExternalForm());
    }
    public String getOrgId() 
    {
     String orgId = Userinfo.getOrganizationId();
     System.debug('Org Id: ' + orgId);
     return orgId ;
    }
//Function to submit actions    
     public pageReference submit() {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info,'Request has been submitted'));
        if (PRAFIT == true)
            clearPRAFITActions();
        if (HRExitInterview == true)
            clearHRExitInterviewActions();
        if (Chatter == true)
            clearChatterActions();
        if (Files == true)
            clearFilesctions();
        if (ReinstateImages == true)
            clearReinstateImagessctions();
        PageReference submit = new PageReference('/apex/DC_Statuspage');
        submit.setRedirect(true);
        return submit;
     }
 //Function to clean up PRAFIT data from system    
    public void clearPRAFITActions() {
        List < PRAFIT_Action__c >clearList = [Select Id from PRAFIT_Action__c LIMIT 10000];
        if (!clearList.isEmpty()) {
            try {
                delete clearList;
            }
            catch (DmlException e) {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }

        }
    }
//Function to clean up HRExitInterview data and also mask sensitive data
    public void clearHRExitInterviewActions() {
    
            List<HR_Exit_Interview_Questionnaire__c>hreiqdel=new List<HR_Exit_Interview_Questionnaire__c>([select id,Name from HR_Exit_Interview_Questionnaire__c LIMIT 10000]);
           if(hreiqdel.size()>0){
            delete hreiqdel;
           }

        List < HR_Exit_Interview__c >clearList = new List <HR_Exit_Interview__c > ([select id, Name from HR_Exit_Interview__c LIMIT 10000 ]);
        if (clearList.size() > 0) {
             delete clearList;
        List <HR_Employee_Details__c >emplist = new list <HR_Employee_Details__c >([select id,PTO_Payout__c,Pending_Term_Date__c from HR_Employee_Details__c]);
        
        update emplist;     
        }

        else if (clearList.size() != null) {
            List <HR_Employee_Details__c >emplist = new list <HR_Employee_Details__c >([select id,PTO_Payout__c,Pending_Term_Date__c,CD_F_Ratings__c from HR_Employee_Details__c]);
            List <HR_Employee_Details__c >listtoupdate = new list <HR_Employee_Details__c >();
            if (emplist.size() > 0) {
                for (
                    HR_Employee_Details__c emp:
                    emplist) {
                   emp.Pending_Term_Date__c=null; 
                    emp.PTO_Payout__c ='*******';
                   emp.CD_F_Ratings__c='**********';
                    listtoupdate.add(emp);
                }
                update listtoupdate;
            }
        }
    }
 //Function to clean all chatter data
    public void clearChatterActions(){
       List<CollaborationGroup > clearList = [SELECT Id FROM CollaborationGroup LIMIT 10000];
       if(!clearList.isEmpty()){
          try {
             delete clearList;
         } 
           catch (DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
             
    }
}
//Function to clean all files resides in content document
    public void clearFilesctions() {
        List < ContentDocument >clearList = new List <ContentDocument > ([SELECT LatestPublishedVersionId,Id FROM ContentDocument]);
        if (clearList.size() > 0) {
            try {
                delete clearList;
            }
            catch (DmlException e) {
                System.debug('The following exception has occurred: ' + e.getMessage());
            }

        }
    }
//Batch Apex and Function to update reinstate images   
    public void clearReinstateImagessctions() {
        DC_reInstateImagesUpdate batchUpdate =new DC_reInstateImagesUpdate();
        Database.executeBatch(
         batchUpdate);

    }
}