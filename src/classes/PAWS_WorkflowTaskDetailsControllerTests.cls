/**
*   'PAWS_WorkflowTaskDetailsControllerTests' is the test class for PAWS_WorkflowTaskDetailsController
*   Test classes are used to write the unit test and cover the code coverage of the code.
*   @author   Work Relay
*/
@isTest 
public with sharing class PAWS_WorkflowTaskDetailsControllerTests 
{
	static testmethod void run()
    {
    	PAWS_ApexTestsEnvironment.init();
    	
    	Map<String, Object> contextMap = new Map<String, Object>
    	{
    		'context' => new Map<String, Object>{
    			'environment' => new Map<String, Object>{
    				'parameters' => new Map<String, Object>{
    					'cursor' => PAWS_ApexTestsEnvironment.FlowInstanceCursor.Id,
    					'step' => PAWS_ApexTestsEnvironment.FlowInstanceCursor.STSWR1__Step__c
    				}
    			}
    		}
    	};
    	
    	String context = EncodingUtil.base64Encode(Blob.valueOf(JSON.serialize(contextMap)));

    	
    	PageReference ref = Page.PAWS_WorkflowTaskDetails;
    	ref.getParameters().put('signed_request', context);
    	Test.setCurrentPage(ref);
    	
    	PAWS_WorkflowTaskDetailsController controller = new PAWS_WorkflowTaskDetailsController();
    	controller.init();
    }
}