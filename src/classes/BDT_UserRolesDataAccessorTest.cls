@isTest
public with sharing class BDT_UserRolesDataAccessorTest {

	static void createTestData() {

		// add a user to salesforce
		Profile p = [select id from profile where name='System Administrator'];
		User myUser = new User(alias = 'SuperMan', email='clark.kent@dailymail.com',
			emailencodingkey='UTF-8', firstname='Clark', lastname='Kent', languagelocalekey='en_US',
			localesidkey='en_US', profileid = p.Id,
			timezonesidkey='America/Los_Angeles', username='clark.kent@dailymail.com');
		insert myUser;

		// create user role
		BDT_Role__c role = new BDT_Role__c(name='Super User',Role_Number__c=1);
		insert role;

		// add new user to BDT_role
		BDT_UserRoles__c userRole = new BDT_UserRoles__c(name=myUser.id,RoleNumbers__c = '1');
		insert userRole;
	}

	static testMethod void BatchTest() {
		createTestData();
		list<User> BDTUsers = BDT_UserRolesDataAccessor.getBDTusers();
		system.assertEquals('Clark Kent',BDTUsers[0].Name);
		
		system.assertNotEquals(null, BDT_UserRolesDataAccessor.getSFUser('Clark Kent'));

		String userId = BDT_UserRolesDataAccessor.getSFUser('Clark Kent');
		system.assertEquals('1',BDT_UserRolesDataAccessor.getUserRoles(userId).RoleNumbers__c);

		system.assertEquals('Clark Kent',BDT_UserRolesDataAccessor.getUserName(userId));
		
		List<BDT_UserRoles__c> SuperManRoles = BDT_UserRolesDataAccessor.getUserRolesByRoleNumber(1);
		system.assertEquals(userId,SuperManRoles[0].Name);
	}

}