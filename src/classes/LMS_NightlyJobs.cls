public class LMS_NightlyJobs {
	
	private static String inSync {get;set;}
	private static String outSync {get;set;}
	private static String statusDraft {get;set;}
	private static String statusDelete {get;set;}
	private static String pendingAdd {get;set;}
	private static String pendingDelete {get;set;}
	private static String transitAdd {get;set;}
	private static String transitDelete {get;set;}
    private static String removed {get;set;}
    private static String actionAdd {get;set;}
    private static String actionRemove {get;set;}
    private static String archive {get;set;}
	
    static {
    	// Get custom settings map
    	Map<String, LMSConstantSettings__c> constants = LMSConstantSettings__c.getAll();
    	Map<String, CourseDomainSettings__c> domains = CourseDomainSettings__c.getAll();
    	
    	// Setup values
        inSync = constants.get('inSync').Value__c;
        outSync = constants.get('outSync').Value__c;
        statusDraft = constants.get('statusDraft').Value__c;
        statusDelete = constants.get('statusToDelete').Value__c;
        pendingAdd = constants.get('statusPendingAdd').Value__c;
        pendingDelete = constants.get('statusPendingDelete').Value__c;
        transitAdd = constants.get('statusTransitAdd').Value__c;
        transitDelete = constants.get('statusTransitDelete').Value__c;
        removed = constants.get('statusRemoved').Value__c;
        actionAdd = constants.get('addMappings').Value__c;
        actionRemove = constants.get('removeMappings').Value__c;
        archive = domains.get('Archive').Domain_Id__c;
        
    }
    
    public static void checkRoleCourseMappings() {
    	List<LMS_Role_Course__c> roleCourse = 
    	[SELECT Status__c FROM LMS_Role_Course__c WHERE Course_Id__r.Discontinued_From__c <= TODAY];
    	List<LMS_Role_Course__c> invalidDrafts = new List<LMS_Role_Course__c>();
    	
    	if(roleCourse.size() > 0) {
	    	for(LMS_Role_Course__c rc : roleCourse) {
	    		if(rc.Status__c != statusDraft && rc.Status__c != pendingAdd) {
	    			rc.Status__c = removed; 
	    		} else {
	    			invalidDrafts.add(rc);
	    		}
	    	}
    	}
    	
    	if(roleCourse.size() > 0) {
    		update roleCourse;
    	}
    	if(invalidDrafts.size() > 0) {
    		delete invalidDrafts;
    	}
    }
    public static void checkRoleEmployeeStartDate() {    	
    	List<LMS_Role_Employee__c> roleEmp = 
    	[SELECT Employee_Id__r.Date_Hired__c, Employee_Id__r.Term_Date__c, Status__c FROM LMS_Role_Employee__c 
    		WHERE (Status__c = :pendingAdd AND Commit_Date__c = null AND Employee_Id__r.Date_Hired__c <= :Date.today()) OR Employee_Id__r.Term_Date__c = :Date.today()];
    	List<LMS_Role_Employee__c> updateMaps = new List<LMS_Role_Employee__c>();
    	List<LMS_Role_Employee__c> removeMaps = new List<LMS_Role_Employee__c>();
    	
    	for(LMS_Role_Employee__c re : roleEmp) {
			if(re.Employee_Id__r.Date_Hired__c <= Date.today() && re.Status__c == pendingAdd) {
				re.Status__c = 'Transit - Avail';	
				updateMaps.add(re);
			}
			if(re.Employee_Id__r.Term_Date__c == Date.today()) {
				if(re.Status__c == statusDraft || re.Status__c == pendingAdd){
					removeMaps.add(re);
				} else {
					re.Status__c = removed;
					updateMaps.add(re);
				}
			}
    	}
    	
    	update updateMaps;
    	delete removeMaps;
    }
    
    @future(callout=true)
    public static void sendRoleEmployeeMappings() {
		List<LMS_Role_Employee__c> toUpdate = new List<LMS_Role_Employee__c>();
        List<LMS_Role_Employee__c> toRemove = new List<LMS_Role_Employee__c>();
        List<LMS_Role_Employee__c> updateList = new List<LMS_Role_Employee__c>();
        Map<String, LMS_Role__c> roleMap = new Map<String, LMS_Role__c>();
        Map<String, LMS_Role_Employee__c> personsAdd = new Map<String, LMS_Role_Employee__c>();
        Map<String, LMS_Role_Employee__c> personsRemove = new Map<String, LMS_Role_Employee__c>();
        Map<String, List<LMS_Role_Employee__c>> mapUpdate = new Map<String, List<LMS_Role_Employee__c>>();
        Map<String, Map<String, LMS_Role_Employee__c>> objectsUpdate = new Map<String, Map<String, LMS_Role_Employee__c>>();
        
        // Role Emp add query
        String roleEmpAddQuery = 'SELECT Status__c, Employee_Id__r.Name, Role_Id__r.SABA_Role_PK__c FROM LMS_Role_Employee__c WHERE ' +
        	'((Sync__c = :outSync AND Status__c = :statusDraft AND Role_Id__r.Role_Type__r.Name = \'PRA\') OR ' + 
        	'(Sync__c = :outSync AND Status__c = :pendingAdd AND Commit_Date__c <= TODAY) OR ' + 
        	'(Sync__c = :outSync AND Status__c = \'Transit - Avail\')) ' + LMS_ToolsFilter.getWhitelistRoleEmployeeFilter() + 
        	' ' + LMS_ToolsFilter.getBlackListRoleEmployeeFilter();
        roleEmpAddQuery += ' ORDER BY Role_Id__c';
        
        // Role Emp delete query
        String roleEmpDeleteQuery = 'SELECT Status__c, Employee_Id__r.Name, Role_Id__r.SABA_Role_PK__c FROM LMS_Role_Employee__c WHERE ' +
        	'((Sync__c = :inSync AND Status__c = :removed ) OR ' + 
        	'(Sync__c = :inSync AND Status__c = :pendingDelete AND Commit_Date__c <= TODAY)) ' + LMS_ToolsFilter.getWhitelistRoleEmployeeFilter() + 
        	' ' + LMS_ToolsFilter.getBlackListRoleEmployeeFilter();
        roleEmpDeleteQuery += ' ORDER BY Role_Id__c';
        
    	List<LMS_Role_Employee__c> roleEmpAdd = Database.query(roleEmpAddQuery);
        List<LMS_Role_Employee__c> roleEmpRemove = Database.query(roleEmpDeleteQuery);
    	List<LMS_Role__c> roles = 
    	[SELECT Role_Name__c,Role_Type__r.Name,SABA_Role_PK__c,Sync_Status__c,Status__c FROM LMS_Role__c WHERE 
    		Role_Type__r.Name = 'Project Specific' AND Status__c = 'Active' AND SABA_Role_PK__c != null AND
    		(Project_Id__r.Status_Desc__c = 'Lost' OR Project_Id__r.Status_Desc__c = 'Closed') 
    		ORDER BY SABA_Role_PK__c];
    		
        if(roleEmpAdd.size() > 0) {
        	String roleId;
	        for(LMS_Role_Employee__c re : roleEmpAdd) {
	        	if(roleId != re.Role_Id__c) {
	        		personsAdd = new Map<String, LMS_Role_Employee__c>();
	        	}
	        	personsAdd.put(re.Employee_Id__r.Name, re);
        		objectsUpdate.put(re.Role_Id__r.SABA_Role_PK__c, personsAdd);
        		toUpdate.add(re);
        		roleId = re.Role_Id__c;
	        }
        }
        if(roleEmpRemove.size() > 0) {
        	String roleId;
        	for(LMS_Role_Employee__c re : roleEmpRemove) {
        		if(roleId != re.Role_Id__c) {
        			personsRemove = new Map<String, LMS_Role_Employee__c>();
        		}
        		personsRemove.put(re.Employee_Id__r.Name, re);
        		objectsUpdate.put(re.Role_Id__r.SABA_Role_PK__c, personsRemove);
        		toRemove.add(re);
        		roleId = re.Role_Id__c;
        	}
        }
        if(roles.size() > 0) {
        	for(LMS_Role__c role : roles) {
        		roleMap.put(role.Id, role);
        	}
    		LMS_ToolsService.inactivateRoleViaJob(roles, roleMap);
        }
        
        if(null != toUpdate && toUpdate.size() > 0) {
	        mapUpdate.put(actionAdd, toUpdate);
        }
        if(null != toRemove && toRemove.size() > 0) {
        	mapUpdate.put(actionRemove, toRemove);
        }
        if((null != toUpdate && toUpdate.size() > 0) || (null != toRemove && toRemove.size() > 0)) {
        	updateList = LMS_ToolsService.executeEmployeesToRolesWS(mapUpdate, objectsUpdate);
        }
        if(updateList.size() > 0) {
        	upsert updateList;
        }
    }
    
}