/**
@author Bhargav Devaram
@date 2015
@description this controller class is to display the Planned, Actuals,Re-Projected data of sites and Subjects 
**/
public class PBB_ProjectStrategiesController {
    
    public  String ProtocolUniqueKey {get;set;}    
    public  List<String> HiddenHomeTabsList         {get;set;}    //hometab panel
    public  List<String> HiddenSubTabsList          {get;set;}    //to hide/show the subtabs in the component.
    public string screenState                       {get;set;}    //set the sites activated   
     
    /** 
    @author Bhargav Devaram
    @date 2015 
    @description Constructor  
    */
    Public PBB_ProjectStrategiesController () {
        HiddenHomeTabsList = new List<String>();
        HiddenSubTabsList = new List<String>();
         
        //set the default tab to Sites Activated
        screenState   ='1';
        ProtocolUniqueKey= ApexPages.currentPage().getParameters().get('ProtocolUniqueKey');
        
    } 
}