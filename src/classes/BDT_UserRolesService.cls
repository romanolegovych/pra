public with sharing class BDT_UserRolesService {

	/**
	 * Get all BDT users
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @return String
	 */
	public static list<User> getBDTusers () {
		return BDT_UserRolesDataAccessor.getBDTUsers();
	}

	/**
	 * Get all BDT users
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @return String
	 */
	public static BDT_UserRoles__c createFindBDTUser (String InputName) {

		//find the user in salesforce
		String UserId = BDT_UserRolesDataAccessor.getSFUser(InputName);

		if (UserId == null) {
			// username does not match salesforce username
			return null;
		}

		BDT_UserRoles__c UserRoles = BDT_UserRolesDataAccessor.getUserRoles(UserId);

		if (UserRoles == null) {
			// create new record
			UserRoles = new BDT_UserRoles__c();
			UserRoles.Name = UserId;
			insert UserRoles;
		}
		return UserRoles;
	}

	/**
	 * Get Username for user ID
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @param String ID User ID
	 * @return String
	 */
	public static string getUserName(String ID) {
		return BDT_UserRolesDataAccessor.getUserName(ID);
	}

	/**
	 * Remove a role from all users that have the roles assigned
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @param Decimal roleNumber
	 */
	public static void removeRoleFromAllUsers (Decimal roleNumber) {
		
		List<BDT_UserRoles__c> updateList = new List<BDT_UserRoles__c>();
		
		for (BDT_UserRoles__c bur: BDT_UserRolesDataAccessor.getUserRolesByRoleNumber (roleNumber)) {
			String roles = BDT_Utils.removeElementFromStringList (bur.RoleNumbers__c,String.valueOf(roleNumber));
			// check if there was a change
			if (roles != bur.RoleNumbers__c) {
				bur.RoleNumbers__c = roles;
				updateList.add(bur);
			}
		}

		update updateList;
	}
	
	/**
	 * Users get removed from plenbox by removing the user's UserRoles record.
	 * The user's SalesForce data will remain unchanged.
	 * @author Maurice Kremer
	 * @version 17Oct13
	 * @param String UserRolesID The user UserRole ID to be remover
	 */
	public static void removeAllUsersRolesFromPlenbox (String UserRolesID) {
		delete [select id from BDT_UserRoles__c where id = :UserRolesID];
	}

	/**
	 * Get all user with a specific role
	 * @author Maurice Kremer
	 * @version 17Oct13
	 * @param String RoleNumber The rolenumber to select user from
	 * @return list<user>
	 */
	public static list<user> getUsersByRole (String RoleNumber) {
		return BDT_UserRolesDataAccessor.getUserRolesByRoleNumber(RoleNumber);
	}

	/**
	 * Create a shuttle for a user to assign and remove roles
	 * @author Maurice Kremer
	 * @version 17Oct13
	 * @param String UserId The userid to create the shuttle for
	 */
	public static BDT_Utils.SelectOptionShuttle createShuttle(String UserId) {
		
		// get all roles
		List<BDT_Role__c> rolesList = BDT_RoleDataAccessor.getAllRoles();
		
		// get selected roles for user
		BDT_UserRoles__c userRoles = BDT_UserRolesDataAccessor.getUserRoles(UserId);
		List<String> rolesUser = new List<String>();
		if (!String.isBlank(userRoles.RoleNumbers__c)) {
			rolesUser = userRoles.RoleNumbers__c.split(':');
		}
		
		// build shuttle
		BDT_Utils.SelectOptionShuttle shuttle = new BDT_Utils.SelectOptionShuttle(
			rolesList,'Role_Number__c','Name',rolesUser
		);
		
		return shuttle;
		
	}
	
	/**
	 * Update the user to have the new list of assigned roles set
	 * @author Maurice Kremer
	 * @version 17Oct13
	 * @param BDT_UserRoles__c userRole UserRole record to update.
	 * @List<SelectOption> RolesAssigned SelectOption list of assigned roles.
	 */	public static void saveRoles(BDT_UserRoles__c userRole,List<SelectOption> RolesAssigned) {
		String roles = '';
		for (SelectOption item : RolesAssigned) {
			roles += ':' + item.getValue();
		}
		roles = roles.removeStart(':');
		if (userRole.RoleNumbers__c != roles) {
			userRole.RoleNumbers__c = roles;
			update userRole;
		}
	}
	
	
}