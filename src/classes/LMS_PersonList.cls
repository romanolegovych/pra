public class LMS_PersonList {
    
    public Boolean selected{get;set;}
    public String id{get;private set;}
    public String employeeId{get;private set;}
    public String firstName{get;private set;}
    public String lastName{get;private set;}
    public String jobClass{get;private set;}
    public String jobCode{get;private set;}
    public String jobTitle{get;private set;}
    public String businessUnit{get;private set;}
    public String department{get;private set;}
    public String region{get;private set;}
    public String country{get;private set;}
    public String status{get;private set;}
    private Map<String, String> jobTitleMap{get;set;}
    
    //Returns course information for adding course to roles
    public LMS_PersonList(Employee_Details__c e, Map<String, String> m, Map<String, String> jobTitles) {
        if(m.containsKey(e.Name)) {
            selected = true;
        } else {
            selected = false;
        }
        id = e.Id;
        employeeId = e.Name;
        firstName = e.First_Name__c;
        lastName = e.Last_Name__c;
        jobClass = e.Job_Class_Desc__c;
        jobCode = e.Job_Code__c;
        jobTitle = jobTitles.get(e.Job_Code__c);
        businessUnit = e.Business_Unit_Desc__c;
        department = e.Department__c;
        region = e.Country_Name__r.Region_Name__c;
        country = e.Country_Name__r.Name;
        status = e.Status_Desc__c;
    }   
}