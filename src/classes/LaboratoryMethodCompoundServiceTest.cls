/** Implements the test for the Service Layer of the object LaboratoryMethodCompound__c
 * @author	Dimitrios Sgourdos
 * @version	26-Nov-2013
 */
@isTest
private class LaboratoryMethodCompoundServiceTest {
	
	// Global variables
	private static List<LaboratoryMethod__c> 			lmList;
	private static List<Study__c> 						studiesList;
	private static List<LaboratoryMethodCompound__c> 	compoundList;
	
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static void init(){
		// Create project
		Client_Project__c firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		
		// Create studies
		studiesList = BDT_TestDataUtils.buildStudies(firstProject,2);
		insert studiesList;
		
		// Create Laboratory Methods
		lmList = new List<LaboratoryMethod__c>();
		lmList.add(new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
											Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood', 
											Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle'));
		lmList.add(new LaboratoryMethod__c (Name='PRA-NL-LML-0002', AnalyticalTechnique__c='ELISA', AntiCoagulant__c='N/A', 
											Department__c='SML', Detection__c='N/A', Location__c='PRA-US', Matrix__c='Breast milk', 
											Proprietary__c='N', ShowOnMethodList__c='Y', Species__c='Human'));
		insert lmList; 
		
		// Create Laboratory Method Compounds with alphabetically names
		compoundList = new List<LaboratoryMethodCompound__c>();
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[0], 'Test A') );
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[0], 'Test B') );
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[1], 'Test-C') );
		insert compoundList;
	}
	
	
	/** Test the function createLaboratoryMethodCompoundInstance
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void createLaboratoryMethodCompoundInstanceTest() {
		LaboratoryMethod__c tmpLabMethod = new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
																 Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood', 
																 Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle');
		insert tmpLabMethod;
		
		LaboratoryMethodCompound__c labCompound = LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(
																										tmpLabMethod,
																										'midazolam'
												);
		system.assertNotEquals(labCompound, NULL);
		system.assertEquals(labCompound.LaboratoryMethod__c, tmpLabMethod.Id);
		system.assertEquals(labCompound.Name, 'midazolam');
	}
	
	
	/** Test the function getMethodCompoundStudyAssignmentContainerData
	 * @author	Dimitrios Sgourdos
	 * @version	26-Nov-2013
	 */
	static testMethod void getMethodCompoundStudyAssignmentContainerDataTest() {
		init();
		
		// Create first Analysis associated with a Study that we will not query, and the other two with a study that we will query 
		// to check that the wrapper is nicely created
		List<LaboratoryAnalysis__c> tmpLabAnalysisList = new List<LaboratoryAnalysis__c>();
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],studiesList[0],true, false,false));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[1],studiesList[1],false,true, false));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[2],studiesList[0],false,false,true));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[2],studiesList[1],false,false,false));
		insert tmpLabAnalysisList;
		
		// Set tmpStudies as query studies
		List<Study__c> tmpStudiesList = new List<Study__c>();
		tmpStudiesList.addAll(studiesList);
		tmpStudiesList.remove(0);
		
		// Retrieve Wrapper 
		List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer> containerWrapperList = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		containerWrapperList = LaboratoryMethodCompoundService.getMethodCompoundStudyAssignmentContainerData(tmpStudiesList);
		system.assertEquals(containerWrapperList.size(), 2);
		
		// Test by quering all the studies
		containerWrapperList = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		containerWrapperList = LaboratoryMethodCompoundService.getMethodCompoundStudyAssignmentContainerData(studiesList);
		
		// Check the size of the wrapper. It must be 3 as we expect three compounds
		system.assertEquals(containerWrapperList.size(), 3);
		
		// Check the first compound. Two analysis - The second null ass we haven't associated with the second study
		system.assertEquals(containerWrapperList[0].analysisList.size(), 2);
		system.assertEquals(containerWrapperList[0].analysisList[0].Id, tmpLabAnalysisList[0].Id);
		system.assertEquals(containerWrapperList[0].analysisList[1].Id, NULL);
		List<String> labelList = containerWrapperList[0].getStudyAnalysisInput();
		system.assertEquals(labelList.size(), 2);
		system.assertEquals(labelList[0], 'AN');
		system.assertequals(labelList[1], '');
		
		// Check the second compound. Two analysis - The first null ass we haven't associated with first study
		system.assertEquals(containerWrapperList[1].analysisList.size(), 2);
		system.assertEquals(containerWrapperList[1].analysisList[0].Id, NULL);
		system.assertEquals(containerWrapperList[1].analysisList[1].Id, tmpLabAnalysisList[1].Id);
		labelList = containerWrapperList[1].getStudyAnalysisInput();
		system.assertEquals(labelList.size(), 2);
		system.assertEquals(labelList[0], '');
		system.assertequals(labelList[1], 'MD');
		
		// Check the third compound. Two analysis - associated with the two studies
		system.assertEquals(containerWrapperList[2].analysisList.size(), 2);
		system.assertEquals(containerWrapperList[2].analysisList[0].Id, tmpLabAnalysisList[2].Id);
		system.assertEquals(containerWrapperList[2].analysisList[1].Id, tmpLabAnalysisList[3].Id);
		labelList = containerWrapperList[2].getStudyAnalysisInput();
		system.assertEquals(labelList.size(), 2);
		system.assertEquals(labelList[0], 'VAL');
		system.assertequals(labelList[1], '');
	}
	
	
	/** Test the function saveMethodCompoundStudyAssignment
	 * @author	Dimitrios Sgourdos
	 * @version	26-Nov-2013
	 */
	static testMethod void saveMethodCompoundStudyAssignmentTest() {
		// Initialize data
		init();
		
		List<LaboratoryAnalysis__c> tmpLabAnalysisList = new List<LaboratoryAnalysis__c>();
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],studiesList[0],true, false,false));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[1],studiesList[1],false,true, false));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[2],studiesList[0],false,false,true));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[2],studiesList[1],false,false,false));
		insert tmpLabAnalysisList;
		
		// Set tmpStudies as the query studies
		List<Study__c> tmpStudiesList = new List<Study__c>();
		tmpStudiesList.addAll(studiesList);
		tmpStudiesList.remove(0);
		
		// Retrieve Wrapper 
		List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer> containerWrapperList = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		containerWrapperList = LaboratoryMethodCompoundService.getMethodCompoundStudyAssignmentContainerData(tmpStudiesList);
		
		// Check the function
		containerWrapperList[0].analysisList[0].MethodDevelopment__c = true;
		containerWrapperList[0].analysisList[0].MethodValidation__c	 = false;
		containerWrapperList[0].analysisList[0].Analysis__c			 = true;
		
		Boolean successFlag = LaboratoryMethodCompoundService.saveMethodCompoundStudyAssignment(containerWrapperList);
		containerWrapperList = LaboratoryMethodCompoundService.getMethodCompoundStudyAssignmentContainerData(tmpStudiesList);
		
		system.assertEquals(successFlag, true);
		system.assertEquals(containerWrapperList[0].analysisList[0].MethodDevelopment__c, true);
		system.assertEquals(containerWrapperList[0].analysisList[0].MethodValidation__c,  false);
		system.assertEquals(containerWrapperList[0].analysisList[0].Analysis__c,		  true);
	}
	
	
	/** Test the function checkForUnassignedCompoundToStudyAssignment
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void checkForUnassignedCompoundToStudyAssignmentTest() {
		init();
		
		// Create wrapper
		List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer> containerWrapperList = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer newWrapperItem = new LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer();
		newWrapperItem.analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],studiesList[0],true,false,true));
		newWrapperItem.analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],studiesList[1],true,false,true));
		containerWrapperList.add(newWrapperItem);
		
		newWrapperItem = new LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer();
		newWrapperItem.analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[1],studiesList[0],true, true, true));
		newWrapperItem.analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[1],studiesList[1],false,false,false));
		containerWrapperList.add(newWrapperItem);
		
		// There is no unassigned compound
		Boolean unassignedCompoundFlag = LaboratoryMethodCompoundService.checkForUnassignedCompoundToStudyAssignment(containerWrapperList);
		system.assertEquals(unassignedCompoundFlag, false);
		
		// Create an unassigned compound
		containerWrapperList[0].analysisList[0].MethodDevelopment__c = false;
		containerWrapperList[0].analysisList[0].MethodValidation__c	 = false;
		containerWrapperList[0].analysisList[0].Analysis__c			 = false;
		containerWrapperList[0].analysisList[1].MethodDevelopment__c = false;
		containerWrapperList[0].analysisList[1].MethodValidation__c	 = false;
		containerWrapperList[0].analysisList[1].Analysis__c			 = false;
		
		unassignedCompoundFlag = LaboratoryMethodCompoundService.checkForUnassignedCompoundToStudyAssignment(containerWrapperList);
		
		system.assertEquals(unassignedCompoundFlag, true);
	}
	
	
	/** Test the function deleteMethodCompoundStudyAssignment
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void deleteMethodCompoundStudyAssignmentTest() {
		init();
		
		// Create wrapper
		List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer> containerWrapperList = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer newWrapperItem = new LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer();
		newWrapperItem.analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],studiesList[0],true,false,true));
		newWrapperItem.analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],studiesList[1],true,false,true));
		containerWrapperList.add(newWrapperItem);
		
		newWrapperItem = new LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer();
		newWrapperItem.analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[1],studiesList[0],true, true, true));
		newWrapperItem.analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[1],studiesList[1],false,false,false));
		containerWrapperList.add(newWrapperItem);
		
		// Try to delete with invalid index
		List<LaboratoryAnalysis__c> deletedAnalysisList = LaboratoryMethodCompoundService.deleteMethodCompoundStudyAssignment(containerWrapperList, 4);
		system.assertEquals(deletedAnalysisList.size(), 0);
		
		// Delete the analysis for the first method (we have 2 selected studies and 2 compounds, so 2*2=4 analysis will forward for deletion)
		deletedAnalysisList = LaboratoryMethodCompoundService.deleteMethodCompoundStudyAssignment(containerWrapperList, 0);
		system.assertEquals(deletedAnalysisList.size(), 4);
	}
	
	
	/** Test the function createNewLaboratoryMethodCompoundsFromSearchCriteria
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void createNewLaboratoryMethodCompoundsFromSearchCriteriaTest() {
		LaboratoryMethod__c tmpLabMethod = new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
																 Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood', 
																 Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle');
		insert tmpLabMethod;
		
		List<LaboratoryMethodCompound__c> searchCompoundList = new List<LaboratoryMethodCompound__c>();
		searchCompoundList.add(new LaboratoryMethodCompound__c(Name='Compound 1'));
		searchCompoundList.add(new LaboratoryMethodCompound__c(Name=''));
		searchCompoundList.add(new LaboratoryMethodCompound__c(Name='Compound 2'));
		
		Boolean resultFlag = LaboratoryMethodCompoundService.createNewLaboratoryMethodCompoundsFromSearchCriteria(searchCompoundList, 
																												tmpLabMethod.Id);
		system.assertEquals(resultFlag, true);
		
		// Only the first and the third compound must be inserted
		system.assertNotEquals(searchCompoundList[0].Id, NULL);
		system.assertEquals(searchCompoundList[1].Id, NULL);
		system.assertNotEquals(searchCompoundList[2].Id, NULL);
	}
	
	
	/** Test the function existMethodInMethodCompoundStudyAssignmentContainerData
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */	
	static testMethod void existMethodInMethodCompoundStudyAssignmentContainerDataTest() {
		List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer> addContainerWrapperList = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		
		LaboratoryMethod__c tmpLabMethod = new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
																 Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood', 
																 Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle');
		insert tmpLabMethod;
		
		// Create and retrieve compound to have access in its method name through LaboratoryMethod__r.Name
		LaboratoryMethodCompound__c testCompound = LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(tmpLabMethod, 'Testing');
		insert testCompound;
		
		testCompound = [SELECT Id, LaboratoryMethod__r.Name FROM LaboratoryMethodCompound__c WHERE Id = :testCompound.Id];
		
		LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer tmpWrapperItem = new LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer();
		tmpWrapperItem.methodCompound = testCompound;
		addContainerWrapperList.add(tmpWrapperItem);
		
		// Check with existing method in the wrapper
		Boolean resultFlag = LaboratoryMethodCompoundService.existMethodInMethodCompoundStudyAssignmentContainerData(addContainerWrapperList, 
																													tmpLabMethod);
		system.assertEquals(resultFlag, true);
		
		// Check with not existing method in the wrapper
		addContainerWrapperList = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		resultFlag = LaboratoryMethodCompoundService.existMethodInMethodCompoundStudyAssignmentContainerData(addContainerWrapperList, 
																											tmpLabMethod);
		system.assertEquals(resultFlag, false);
	}
	
	
	/** Test the function addMethodToMethodCompoundStudyAssignmentContainerData
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void addMethodToMethodCompoundStudyAssignmentContainerDataTest() {
		init();
		
		// Retrieve the laboratory method to have access in LaboratoryMethodCompounds__r
		LaboratoryMethod__c tmpMethod = LaboratoryMethodDataAccessor.getById (lmList[0].Id);
		List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer> addContainerWrapperList = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		addContainerWrapperList = LaboratoryMethodCompoundService.addMethodToMethodCompoundStudyAssignmentContainerData(
																							addContainerWrapperList,
																							studiesList,
																							tmpMethod);
		
		// The method tmpMethod has 2 compounds so expected size of wrapper list is 2
		system.assertEquals(addContainerWrapperList.size(), 2);
	}
	
	
	/** Test the function allBlankNamesInLabCompoundsList
	 * @author	Dimitrios Sgourdos
	 * @version	18-Oct-2013
	 */
	static testMethod void allBlankNamesInLabCompoundsListTest() {
		List<LaboratoryMethodCompound__c> searchCompoundList = new List<LaboratoryMethodCompound__c>();
		
		// Check with all blank names
		searchCompoundList.add(new LaboratoryMethodCompound__c(Name=''));
		searchCompoundList.add(new LaboratoryMethodCompound__c(Name=''));
		Boolean allBlankFlag = LaboratoryMethodCompoundService.allBlankNamesInLabCompoundsList(searchCompoundList);
		system.assertEquals(allBlankFlag, true);
		
		// Check with not all blank names 
		searchCompoundList[1].Name = 'Compound B';
		allBlankFlag = LaboratoryMethodCompoundService.allBlankNamesInLabCompoundsList(searchCompoundList);
		system.assertEquals(allBlankFlag, false);
	}
	
	
	/** Test the function removeRecordsFromMethodCompoundStudyAssignmentContainer
	 * @author	Dimitrios Sgourdos
	 * @version	22-Oct-2013
	 */
	static testMethod void removeRecordsFromMethodCompoundStudyAssignmentContainerTest() {
		List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer> wrapperList = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		
		// Add ten records
		for(Integer i=0; i<10; i++) {
			wrapperList.add(new LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer());
		}
		system.assertEquals(wrapperList.size(), 10);
		
		// Add to the 8th record a specific compound
		LaboratoryMethodCompound__c tmpCompound = new LaboratoryMethodCompound__c(Name='test compound');
		wrapperList[7].methodCompound = tmpCompound;
		
		// Delete 3 records from index 4 to 6
		wrapperList = LaboratoryMethodCompoundService.removeRecordsFromMethodCompoundStudyAssignmentContainer(wrapperList, 4, 6);
		system.assertEquals(wrapperList.size(), 7);
		
		// Check if the specific compound is now 5th
		system.assertEquals(wrapperList[4].methodCompound.Name, tmpCompound.Name);
	}
	
	
	/** Test the function mapCompoundStudyAssignmentsToMethods
	 * @author	Dimitrios Sgourdos
	 * @version	26-Nov-2013
	 */
	static testMethod void mapCompoundStudyAssignmentsToMethodsTest() {
		// Create data
		init();
		
		List<LaboratoryAnalysis__c> tmpLabAnalysisList = new List<LaboratoryAnalysis__c>();
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],studiesList[0],true, false,false));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[1],studiesList[1],false,true, false));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[1],studiesList[0],false,false,true));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[2],studiesList[1],false,false,false));
		insert tmpLabAnalysisList;
		
		List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer> initialList = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		initialList = LaboratoryMethodCompoundService.getMethodCompoundStudyAssignmentContainerData(studiesList);
		
		// Check the function
		Map<String, List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>> results = new Map<String, List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>>();
		results = LaboratoryMethodCompoundService.mapCompoundStudyAssignmentsToMethods(initialList);
		
		system.assertEquals(results.keyset().size(), 2);
		system.assertEquals(results.get(lmList[0].Id).size(), 2);
		system.assertEquals(results.get(lmList[1].Id).size(), 1);
	}
	
	
	/** Test the function getMethodDetailsData
	 * @author	Dimitrios Sgourdos
	 * @version	26-Nov-2013
	 */
	static testMethod void getMethodDetailsDataTest() {
		// Create data
		init();
		
		List<LaboratoryAnalysis__c> tmpLabAnalysisList = new List<LaboratoryAnalysis__c>();
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],studiesList[0],true, false,false));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[1],studiesList[1],false,true, false));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[2],studiesList[1],false,false,false));
		insert tmpLabAnalysisList;
		
		List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer> initialList = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		initialList = LaboratoryMethodCompoundService.getMethodCompoundStudyAssignmentContainerData(studiesList);
		
		// Check the function
		List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentMappedToMethod> results = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentMappedToMethod>();
		results = LaboratoryMethodCompoundService.getMethodDetailsData(initialList);
		
		system.assertEquals(results.size(), 2);
		system.assertEquals(results[0].labMethod.Id, lmList[0].Id);
		system.assertEquals(results[0].assignemtsToStudy.size(), 2);
		system.assertEquals(results[0].assignemtsToStudy[0].methodCompound.Id, initialList[0].methodCompound.Id);
		system.assertEquals(results[0].assignemtsToStudy[1].methodCompound.Id, initialList[1].methodCompound.Id);
		system.assertEquals(results[1].labMethod.Id, lmList[1].Id);
		system.assertEquals(results[1].assignemtsToStudy.size(), 1);
		system.assertEquals(results[1].assignemtsToStudy[0].methodCompound.Id, initialList[2].methodCompound.Id);
	}
}