/** Implements the Service Layer of the object ServiceCategory__c
 * @author	Dimitrios Sgourdos
 * @version	10-Dec-2013
 */
public with sharing class ServiceCategoryService {
	
	/** Retrieve the First Level Service Categories.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 10-Dec-2013
	 * @return	The list of First Level Service Categories
	 */
	public static List<ServiceCategory__c> getFirstLevelServiceCategories() {
		return ServiceCategoryDataAccessor.getFirstLevelServiceCategories();
	}
	
	
	/** Create a select option list from the given Service Categories. The value is the Service Category Id and
	 *	the label is the Service Category Code and the Name.
	 * @author	Dimitrios Sgourdos
	 * @version 11-Dec-2013
	 * @return	The created select option list.
	 */
	public static List<SelectOption> createSelectOptionsFromServiceCategories(List<ServiceCategory__c> sourceList,
																		String noSelectionLabel) {
		// Create the select option list
		List<SelectOption> results = new List<SelectOption>();
		
		// Add the none value in case it is needed
		if(String.isNotBlank(noSelectionLabel)) {
			results.add(new SelectOption('', noSelectionLabel));
		}
		
		// Add the list of objects in the results
		for(ServiceCategory__c tmpItem : sourceList) {
			String code = BDT_Utils.removeLeadingZerosFromCat(tmpItem.Code__c);
			results.add(new SelectOption(tmpItem.Id, code + ' ' + tmpItem.Name));
		}
		
		return results;
	}
	
	
	public static List<SelectOption> sortServiceCategorySelectList (List<SelectOption> values) {
		List<String> sortingList = new List<String>();
		List<SelectOption> result = new List<SelectOption>();

		for (SelectOption s : values) {
			String tmpName = s.getLabel().substringAfter(' ');
			String tmpCode = s.getLabel().substringBefore(' ');
			
			tmpCode = BDT_Utils.addLeadingZerosToCat(tmpCode);
			sortingList.add(tmpCode + ' ' + tmpName);
		}

		sortingList.sort();

		Integer iteratornum;
		Integer iteratortag;

		for (String sortItem: sortingList){
			// Restore the normal label
			String tmpName = sortItem.substringAfter(' ');
			String tmpCode = sortItem.substringBefore(' ');
			tmpCode = BDT_Utils.removeLeadingZerosFromCat(tmpCode);
			sortItem = tmpCode + ' ' + tmpName;
			
			// Sort the Select Options List
			iteratornum = 0;
			for (SelectOption s : values) {
				if (s.getLabel() ==  sortItem) {
					result.add(s);
					iteratortag = iteratornum;
					break;
				}
				iteratornum++;
			}
			values.remove(iteratortag);
		}

		return result;
	}
	
	
	/** Create a shuttle for a user to assign and remove designed Service Categories in calculation helper
	 * @author	Dimitrios Sgourdos
	 * @version 30-Jan-2014
	 * @return	The created shuttle.
	 */
	public static BDT_Utils.SelectOptionShuttle createDesignServiceCategoryShuttle() {
		// Get all Catergories Related To Design Objects mapped to their id
		List<ServiceCategory__c> srvCatList = getAllCatergoriesRelatedToDesignObjects();
		
		// build shuttle
		List<String> selectedValues = new List<String>();
		BDT_Utils.SelectOptionShuttle result = new BDT_Utils.SelectOptionShuttle(
													srvCatList, 'Id','Name', selectedValues
												);
		
		// Adjust the labels of the shuttle options
		result.allOptions = createSelectOptionsFromServiceCategories(srvCatList, NULL);
		
		return result;
	}
	
	
	public static List<ServiceCategory__c> getAllCatergoriesRelatedToDesignObjects () {

		Set<id> servicecategoryIdSet = new Set<id>();

		// read all services
		List<Service__c> serviceList = ServiceDataAccessor.getServiceList('isdesignobject__c = true and bdtdeleted__c = false');
		
		// get all categories, parent categories and grantparent categories in a set that have designobjects
		for (Service__c service:serviceList) {
			servicecategoryIdSet.add(service.servicecategory__c);
			servicecategoryIdSet.add(service.servicecategory__r.parentcategory__c);
			servicecategoryIdSet.add(service.servicecategory__r.parentcategory__r.parentcategory__c);
		}

		String listOfIds = '';
		for (id categoryId:servicecategoryIdSet) {
			if (categoryId==null) {continue;}
			listOfIds += ',\''+ categoryId +'\'';
		}
		listOfIds = listOfIds.removeStart(',');
		
		List<ServiceCategory__c> results = new List<ServiceCategory__c>();
		if(String.isNotBlank(listOfIds)) {
			results =  ServiceCategoryDataAccessor.getServiceCategories('id in ('+listOfIds+')', 'Code__c');
		}
		return results;
	}
	
	public static Set<Id> getAllDesignObjectCategories () {
		
		Set<id> CategoryIds = new Set<Id>();
		for (service__c s: [select servicecategory__c
					,servicecategory__r.parentcategory__c
					,servicecategory__r.parentcategory__r.parentcategory__c
				from service__c
				where isdesignobject__c = true]) {
			CategoryIds.add(s.servicecategory__c);
			CategoryIds.add(s.servicecategory__r.parentcategory__c);
			CategoryIds.add(s.servicecategory__r.parentcategory__r.parentcategory__c);
		}
		CategoryIds.remove(null);
		return CategoryIds;

	}
	
	
	
}