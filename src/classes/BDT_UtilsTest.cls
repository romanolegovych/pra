@isTest
private class BDT_UtilsTest {
	static testMethod void test_Admin() {
		boolean a = BDT_Utils.getUserIsAdministrator();
	}


  static testMethod void test_lPad() {
  	System.assertEquals('00a', BDT_Utils.lPad('a',3,'0'));
  	System.assertEquals('aaa', BDT_Utils.lPad('aaa',3,'0'));
  	System.assertEquals('aaaa', BDT_Utils.lPad('aaaa',3,'0'));

  }

  static testMethod void test_rPad() {
  	System.assertEquals('a00', BDT_Utils.rPad('a',3,'0'));
  	System.assertEquals('aaa', BDT_Utils.rPad('aaa',3,'0'));
  	System.assertEquals('aaaa', BDT_Utils.rPad('aaaa',3,'0'));

  }

  static testMethod void test_removeLeadingZerosFromCat() {
  	System.assertEquals('1.2.3', BDT_Utils.removeLeadingZerosFromCat('001.002.003'));
  	System.assertEquals('1', BDT_Utils.removeLeadingZerosFromCat('00001'));
  	System.assertEquals('1', BDT_Utils.removeLeadingZerosFromCat('1'));
  	System.assertEquals(null, BDT_Utils.removeLeadingZerosFromCat(null));
  }

  static testMethod void test_removeTailingZerosFromDecimals() {
  	System.assertEquals('10', 	 BDT_Utils.removeTailingZerosFromDecimals('10.00'));
  	System.assertEquals('10', 	 BDT_Utils.removeTailingZerosFromDecimals('10.'));
  	System.assertEquals('10', 	 BDT_Utils.removeTailingZerosFromDecimals('10'));
  	System.assertEquals('10.1',  BDT_Utils.removeTailingZerosFromDecimals('10.10'));
  	System.assertEquals('10.12', BDT_Utils.removeTailingZerosFromDecimals('10.12'));
  }

  static testMethod void test_addLeadingZerosToCat() {
  	System.assertEquals('001.002.003', BDT_Utils.addLeadingZerosToCat('1.2.3'));
  	System.assertEquals('001', BDT_Utils.addLeadingZerosToCat('1'));
  }


      static testMethod void testSort(){
        List<Account> accList = [select Name,AccountNumber from Account limit 100];
        BDT_Utils.sortList(accList,'Name', 'ASC');
         String lastValue = null;
        String currentValue = null;
        for (Account acc : accList) {
               currentValue = acc.Name;
               System.assertEquals(currentValue.compareTo(lastValue)>=0, true);
        }
        lastValue = currentValue;
     }

     static testMethod void test_preferences(){
     	bdt_Utils.cleanPreferences();
     	string mypref = bdt_Utils.getPreference('just a preference');
     	System.assert(mypref.equals(''));
     	bdt_Utils.setPreference('just a preference', 'test class');
     	mypref = bdt_Utils.getPreference('just a preference');
     	System.assert(mypref.equals('test class'));
     }

     static testMethod void testSorter (){
     	List<Currency__c> test = new List<Currency__c>();
     	test.add(new Currency__c(name='a',officialName__c='a'));
     	test.add(new Currency__c(name='c',officialName__c='c'));
     	test.add(new Currency__c(name='b',officialName__c='b'));

     	List<Currency__c> sorted = BDT_Utils.sortList(test,'name');

     	System.assertEquals(sorted[0].name,'a');
     	System.assertEquals(sorted[1].name,'b');
     	System.assertEquals(sorted[2].name,'c');
     }
     
     static testMethod void sortSelectListTest () {
     	List<SelectOption> so = new List<SelectOption>();
     	so.add(new SelectOption('1','b'));
     	so.add(new SelectOption('2','a'));
     	so = BDT_Utils.sortSelectList(so);
     	system.assertEquals(2,so.size());
     	system.assertEquals('2',so[0].getValue());
     	system.assertEquals('a',so[0].getLabel());
     	system.assertEquals('1',so[1].getValue());
     	system.assertEquals('b',so[1].getLabel());
     }

     static testMethod void testNumericSorter() {
     	List<RateCardClass__c> test = new List<RateCardClass__c>();
     	test.add(new RateCardClass__c(UnitPrice__c = 1.1));
     	test.add(new RateCardClass__c(UnitPrice__c = 0.1));
     	test.add(new RateCardClass__c(UnitPrice__c = 1.0));

		List<RateCardClass__c> sorted = BDT_Utils.sortListNumeric(test,'UnitPrice__c');

     	System.assertEquals(sorted[0].UnitPrice__c,0.1);
     	System.assertEquals(sorted[1].UnitPrice__c,1.0);
     	System.assertEquals(sorted[2].UnitPrice__c,1.1);
     }

     static testMethod void testCurrency () {
     	List<Currency__c> curr = new List<Currency__c>();
     	curr.add(new Currency__c(name='a',officialName__c='a'));
     	curr.add(new Currency__c(name='c',officialName__c='c'));
     	curr.add(new Currency__c(name='b',officialName__c='b'));
     	insert curr;

     	System.assertEquals(BDT_Utils.getCurrency(curr[0].id).name,'a');
     	System.assertEquals(BDT_Utils.getCurrency(curr[1].id).name,'c');
     	System.assertEquals(BDT_Utils.getCurrency(curr[2].id).name,'b');

     	List<Business_Unit__c> bu = new List<Business_Unit__c>();
     	bu.add(new Business_Unit__c(name='a',currency__c=curr[0].id));
     	bu.add(new Business_Unit__c(name='c',currency__c=curr[1].id));
     	bu.add(new Business_Unit__c(name='b',currency__c=curr[2].id));
     	insert bu;

     	System.assertEquals(BDT_Utils.getCurrencyForBusinessUnit(bu[0].id).name,'a');
     	System.assertEquals(BDT_Utils.getCurrencyForBusinessUnit(bu[1].id).name,'c');
     	System.assertEquals(BDT_Utils.getCurrencyForBusinessUnit(bu[2].id).name,'b');

     }

      static testMethod void getClientProject () {
      	Client_Project__c firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;

		Client_Project__c secondProject = new Client_Project__c();
		secondProject = BDT_Utils.getProject(firstProject.id, false);
		System.assertEquals(firstProject.id,secondProject.id);

      }

      static testMethod void getDesign () {
      	Client_Project__c firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;

		List<ClinicalDesign__c> designList = BDT_TestDataUtils.buildClinicDesign(firstProject);
		insert designList;

		ClinicalDesign__c design = BDT_Utils.getDesign(designList[0].id, firstProject.id);

		System.assertEquals(design.id,designList[0].id);
      }


      static testMethod void getPageName () {

      	String myPageName = BDT_Utils.getThisPageName(System.Page.BDT_singleproject.getUrl());
      	System.assertEquals('bdt_singleproject',myPageName);
      }

      static testMethod void setPreferencesTest(){
      	Client_Project__c firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;
      	BDT_Utils.setPreference('SelectedProject', firstProject.id);
      	BDT_Utils.cleanProjectPreferences(firstProject.id, true);
      	System.assertEquals(0,[select count() from BDT_UserPreference__c where text__c =: firstProject.id]);
      	List<Study__c> stydiesList = BDT_TestDataUtils.buildStudies(firstProject);
		insert stydiesList;
		//read selectedStudies before setting any study
      	BDT_Utils.readSelectedStudies(firstProject.id);
      	BDT_Utils.setPreference('SelectedStudies',stydiesList[0].id);
      	//read selectedStudies after setting any study
      	BDT_Utils.readSelectedStudies();
        String[] myPref = new String[]{'SelectedProject','SelectedStudies'};
      	BDT_Utils.cleanPreferences(myPref);

      }

      static testMethod void businessUnitsTest(){
      	Client_Project__c firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;

		List<Business_Unit__c> myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert myBUList;

		List<Study__c> studiesList = BDT_TestDataUtils.buildStudies(firstProject);
		studiesList[0].Business_Unit__c = myBUList[0].Id;
		studiesList[1].Business_Unit__c = myBUList[1].Id;
		studiesList[2].Business_Unit__c = myBUList[2].Id;
		studiesList[3].Business_Unit__c = myBUList[3].Id;
		studiesList[4].Business_Unit__c = myBUList[4].Id;
		insert studiesList;

		List<Site__c> mySiteList = BDT_TestDataUtils.buildSite(myBUList);
		insert mySiteList;

      	Business_Unit__c myBU = BDT_Utils.getBusinessUnitForStudy(studiesList[0].id);
      	System.assertEquals(myBUList[0].Id, myBU.id);
      }

	    static testMethod void rateCardTest(){

	    	Client_Project__c firstProject = BDT_TestDataUtils.buildProject();
			insert firstProject;

			List<Business_Unit__c> myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
			insert myBUList;

			List<Study__c> studiesList = BDT_TestDataUtils.buildStudies(firstProject);
			studiesList[0].Business_Unit__c = myBUList[0].Id;
			studiesList[1].Business_Unit__c = myBUList[1].Id;
			studiesList[2].Business_Unit__c = myBUList[2].Id;
			studiesList[3].Business_Unit__c = myBUList[3].Id;
			studiesList[4].Business_Unit__c = myBUList[4].Id;
			insert studiesList;

			List<Site__c> mySiteList = BDT_TestDataUtils.buildSite(myBUList);
			insert mySiteList;

			List<ClinicalDesign__c> designList  = BDT_TestDataUtils.buildClinicDesign(firstProject);
			insert designList;

			List<Flowcharts__c> flowchartList  = BDT_TestDataUtils.buildFlowchart(designList[0].id);
			insert flowchartList;

			List<TimePoint__c> timepointList  = BDT_TestDataUtils.buildTimePoints(flowchartList[0].id, 10);
			insert timepointList;

			BDT_Utils.setPreference('SelectedProject', firstProject.id);
			BDT_Utils.setPreference('SelectedStudies',studiesList[0].id);

			List<ServiceCategory__c> myServiceCat1 = BDT_TestDataUtils.buildServiceCategory(3,null);
		    insert myServiceCat1 ;

		    List<ServiceCategory__c> myServiceCat2 = BDT_TestDataUtils.buildServiceCategory(3,myServiceCat1);
		    insert myServiceCat2 ;

		    List<Service__c> serviceList = BDT_TestDataUtils.buildService(myServiceCat2, 1, 'myTimePointTest',true);
		    insert serviceList;

		    List<ProjectService__c> projectService  = BDT_TestDataUtils.buildProjectService(serviceList, firstProject.id);
		    insert projectService;

		    RateCardClass__c myRatecard = BDT_Utils.getDefaultRatecard(myBUList[0].Id, projectService[0].id, 5);

	    }

	    static testMethod void updateCalculationTest (){
	        List<Currency__c> CurrencyList;
			List<Business_Unit__c> BusinessUnitList;
			Client_Project__c ClientProject;
			List<Study__c> StudyList;
			List<Study__c> StudyListSecond;
			List<ServiceCategory__c> ServiceCategoryList;
			List<Service__c> ServiceList;
			List<RateCard__c> RateCardList;
			List<RateCardClass__c> RateCardClassList;
			List<ProjectService__c> ProjectServiceList;
			MAP<String,String> RateCardToBusinessUnit;


				Integer index = 0;
				Integer index2 = 0;

				CurrencyList = BDT_TestDataUtils.BuildCurrencies();
				insert CurrencyList;

				BusinessUnitList = BDT_TestDataUtils.buildBusinessUnit(5);
				// assign currencies to business units
				for (Business_Unit__c bu : BusinessUnitList) {
					bu.Currency__c = CurrencyList[index].id;
					index++;
					if (index + 1 > CurrencyList.size()) {
						index = 0;
					}
				}
				insert BusinessUnitList;

				ClientProject = BDT_TestDataUtils.buildProject();
				insert ClientProject;

				StudyList = BDT_TestDataUtils.buildStudies(ClientProject);
				// assign busines units
				index = 0;
				for (Study__c st : StudyList) {
					st.Business_Unit__c = BusinessUnitList[index].id;
					index++;
					if (index + 1 > BusinessUnitList.size()) {
						index = 0;
					}
				}
				insert StudyList;

				ServiceCategoryList = BDT_TestDataUtils.buildServiceCategory(2, null);
				ServiceCategoryList = BDT_TestDataUtils.buildServiceCategory(2, ServiceCategoryList);
				insert ServiceCategoryList;

				ServiceList = BDT_TestDataUtils.buildService(ServiceCategoryList, 2, 'TEST', true);
				ServiceList = BDT_TestDataUtils.buildService(ServiceCategoryList, 2, 'TEST', false);
				insert ServiceList;

				RateCardList = BDT_TestDataUtils.buildRateCard(ServiceList, BusinessUnitList);
				insert RateCardList;

				RateCardToBusinessUnit = new MAP<String,String>();
				for (RateCard__C rc : RateCardList) {
					RateCardToBusinessUnit.put(''+rc.id,''+rc.Business_Unit__c);
				}

				RateCardClassList = BDT_TestDataUtils.buildRateCardClass(RateCardList);
				insert RateCardClassList;

				ProjectServiceList = new List<ProjectService__c>();
				for (Service__c svr: ServiceList){
		  			if(math.random()*100 < 80) {
		  				ProjectServiceList.add(
		  					new ProjectService__c(
		  						Service__c = svr.id,
		  						Client_Project__c = ClientProject.id
		  					)
		  				);
		  			}
		  		}
		  		insert ProjectServiceList;


		  		StudyListSecond = BDT_TestDataUtils.buildStudies(ClientProject);
				// assign busines units
				index = 0;
				for (Study__c st : StudyListSecond) {
					st.Business_Unit__c = BusinessUnitList[index].id;
					index++;
					if (index + 1 > BusinessUnitList.size()) {
						index = 0;
					}
				}
				insert StudyListSecond;


	    }

	static testMethod void mapSObjectListOnKeyTest (){

		List<Account> acc = new List<Account>();
		acc.add(new Account(Name = 'A',BillingCountry='NL'));
		acc.add(new Account(Name = 'B',BillingCountry='NL'));
		acc.add(new Account(Name = 'C',BillingCountry='US'));
		acc.add(new Account(Name = 'D',BillingCountry='US'));
		acc.add(new Account(Name = 'E',BillingCountry='US'));

		Map<String,List<Account>> result = (Map<String,List<Account>>) BDT_Utils.mapSObjectListOnKey(acc, new List<String>{'BillingCountry'});

		system.assertEquals(2, result.values().size());
		system.assertEquals(2, result.get('NL').size());
		system.assertEquals(3, result.get('US').size());

	}

	static testMethod void mapSObjectOnKeyTest (){

		List<Account> acc = new List<Account>();
		acc.add(new Account(Name = 'A',BillingCountry='NL'));
		acc.add(new Account(Name = 'B',BillingCountry='NL'));
		acc.add(new Account(Name = 'C',BillingCountry='US'));
		acc.add(new Account(Name = 'D',BillingCountry='US'));
		acc.add(new Account(Name = 'E',BillingCountry='US'));

		//Map<String,Account> result = (Map<String,Account>) BDT_Utils.mapSObjectOnKey(acc, new List<String>{'Name'});
		Map<String,SObject> result = (Map<String,SObject>) BDT_Utils.mapSObjectOnKey(acc, new List<String>{'Name'});

		system.assertEquals(5, result.values().size());
		system.assertEquals('NL', ((Account)result.get('A')).BillingCountry);
		system.assertEquals('US', result.get('D').get('BillingCountry'));

	}

	static testMethod void getSetFromListTest () {
		List<Account> acc = new List<Account>();
		acc.add(new Account(Name = 'A',BillingCountry='NL'));
		acc.add(new Account(Name = 'B',BillingCountry='NL'));
		acc.add(new Account(Name = 'C',BillingCountry='US'));
		acc.add(new Account(Name = 'D',BillingCountry='US'));
		acc.add(new Account(Name = 'E',BillingCountry='US'));

		Set<Object> result = BDT_Utils.getSetFromList(acc, 'BillingCountry');
		system.assertEquals(2, result.size());
		system.assert(result.contains('NL'));
	}

	static testMethod void getSelectOptionFromListTest () {
		List<Account> acc = new List<Account>();
		acc.add(new Account(Name = 'A',BillingCountry='1'));
		acc.add(new Account(Name = 'B',BillingCountry='2'));
		acc.add(new Account(Name = 'C',BillingCountry='3'));

		List<SelectOption> result = BDT_Utils.getSelectOptionFromList(acc, 'Name', 'BillingCountry');
		system.assertEquals('A', result[0].getValue());
		system.assertEquals('1', result[0].getLabel());
		system.assertEquals('B', result[1].getValue());
		system.assertEquals('2', result[1].getLabel());
		system.assertEquals('C', result[2].getValue());
		system.assertEquals('3', result[2].getLabel());
	}
	
	
	/** Test the function getSelectOptionFromSObjectList
	* @author	Dimitrios Sgourdos
	* @version	10-Dec-2013
	*/
	static testMethod void getSelectOptionFromSObjectListTest () {
		// Create data
		List<Account> acc = new List<Account>();
		acc.add(new Account(Name = 'A',BillingCountry='1'));
		acc.add(new Account(Name = 'C',BillingCountry='3'));
		acc.add(new Account(Name = 'B',BillingCountry='2'));
		
		// Check the function
		List<SelectOption> results = BDT_Utils.getSelectOptionFromSObjectList(acc, 'Name', 'BillingCountry', '---', true);
		system.assertEquals(4, results.size(), 'Error in creating select option list from SObject list');
		system.assertEquals('', results[0].getValue(), 'Error in creating select option list from SObject list');
		system.assertEquals('---', results[0].getLabel(), 'Error in creating select option list from SObject list');
		system.assertEquals('A', results[1].getValue(), 'Error in creating select option list from SObject list');
		system.assertEquals('1', results[1].getLabel(), 'Error in creating select option list from SObject list');
		system.assertEquals('B', results[2].getValue(), 'Error in creating select option list from SObject list');
		system.assertEquals('2', results[2].getLabel(), 'Error in creating select option list from SObject list');
		system.assertEquals('C', results[3].getValue(), 'Error in creating select option list from SObject list');
		system.assertEquals('3', results[3].getLabel(), 'Error in creating select option list from SObject list');
	}
	

	static testMethod void convertListToStringTest () {

		List<String> stringList = new List<String>();
		stringList.add('2');
		stringList.add('3');

		String result = BDT_Utils.convertListToString(stringList);
		system.assertEquals('2:3',result);
	}
	
	static testMethod void getStringSetFromListTest () {
		List<Account> acc = new List<Account>();
		acc.add(new Account(Name = 'A',BillingCountry='1'));
		acc.add(new Account(Name = 'B',BillingCountry='2'));
		acc.add(new Account(Name = 'C',BillingCountry='3'));
		
		Set<String> result = BDT_Utils.getStringSetFromList(acc, 'Name');
		system.assertEquals(3,result.size());
		
	}
	
	
	/** Test the function createConcatinatedStringFromSobject 
	* @author	Dimitrios Sgourdos
	* @version	17-Oct-2013
	*/
	static testMethod void createConcatinatedStringFromSobjectTest () {
		// Create Data
		List<Account> accList = new List<Account>();
		accList.add(new Account(Name = 'A',BillingCountry='NL'));
		accList.add(new Account(Name = 'B',BillingCountry='NL'));
		accList.add(new Account(Name = 'C',BillingCountry='US'));
		// Check the function
		String result = BDT_Utils.createConcatinatedStringFromSobject(accList, 'Name');
		system.assertEquals('A:B:C', result);
	}
	
	
	/** Test the function getSelectOptionsFromPickList 
	* @author	Dimitrios Sgourdos
	* @version	10-Dec-2013
	*/
	static testMethod void getSelectOptionsFromPickListTest () {
		LaboratoryMethod__c labMethod = new LaboratoryMethod__c();
		List<SelectOption> results = BDT_Utils.getSelectOptionsFromPickList(labMethod, 'Location__c', '---', true);
		system.assert(results.size() > 1);
		system.assertEquals('', results[0].getValue(), 'Error in creating select option list from object field picklist');
		system.assertEquals('---', results[0].getLabel(), 'Error in creating select option list from object field picklist');
	}
	
	
	static testMethod void shuttleTest () {

		List<String> itemKeyList;
		List<SelectOption> usedItems;

		List<Account> acc = new List<Account>();
		acc.add(new Account(Name = 'A',BillingCountry='1'));
		acc.add(new Account(Name = 'B',BillingCountry='2'));
		acc.add(new Account(Name = 'C',BillingCountry='3'));

		List<String> usedStringList = new List<String>();
		usedStringList.add('2');
		usedStringList.add('3');

		BDT_Utils.SelectOptionShuttle shuttle = new BDT_Utils.SelectOptionShuttle(
			acc,'BillingCountry','Name',usedStringList
		);

		// verify if unused is correctly returned
		List<SelectOption> unused = shuttle.getUnusedOptions();
		system.assertEquals('1', unused[0].getValue());
		system.assertEquals('A', unused[0].getLabel());

		// move items on key values to used list
		itemKeyList =  new List<String>();
		itemKeyList.add('1');
		shuttle.moveToUsed(itemKeyList);
		unused = shuttle.getUnusedOptions();
		system.assertEquals(0, unused.size());

		// move items on key values to used list
		itemKeyList =  new List<String>();
		itemKeyList.add('2');
		itemKeyList.add('3');
		shuttle.moveToUnused(itemKeyList);
		usedItems = shuttle.usedOptions;
		system.assertEquals(1, usedItems.size());
		system.assertEquals('1', usedItems[0].getValue());
		system.assertEquals('A', usedItems[0].getLabel());

		itemKeyList = shuttle.getUsedKeys();
		system.assertEquals(1, itemKeyList.size());

	}

	static testMethod void haveMatchingElements() {
		
		String a;
		String b;
		
		a = '1';
		b = '2';
		system.AssertEquals(false,bdt_utils.haveMatchingElements(a,b));
		
		a = '1';
		b = '1:2';
		system.AssertEquals(true,bdt_utils.haveMatchingElements(a,b));
		
		a = '1:2';
		b = '1';
		system.AssertEquals(true,bdt_utils.haveMatchingElements(a,b));
		
		a = '1:2:4:5';
		b = '3';
		system.AssertEquals(false,bdt_utils.haveMatchingElements(a,b));
		
		
		a = '1:2:4:5';
		b = '3:1';
		system.AssertEquals(true,bdt_utils.haveMatchingElements(a,b));
		
	}
	
	
	/** Test the function formatNumberWithSeparators
	 * @author	Dimitrios Sgourdos
	 * @version	19-Nov-2013
	 */
	static testMethod void formatNumberWithSeparatorsTest() {
		// Check with a decimal less than a thousand
		String result = BDT_Utils.formatNumberWithSeparators(432.23);
		system.assertEquals(result, '432');
		// Check with a decimal more than a thousand
		result = BDT_Utils.formatNumberWithSeparators(4321.23);
		system.assertEquals(result, '4,321');
		// Check with a decimal more than a milion
		result = BDT_Utils.formatNumberWithSeparators(987654321.23);
		system.assertEquals(result, '987,654,321');
		
		// check null
		system.assertEquals(null,BDT_Utils.formatNumberWithSeparators(null));
	}
	
	
	/** Test the function buildSetForDynamicQueries
	* @author	Dimitrios Sgourdos
	* @version	14-Feb-2014
	*/
	static testMethod void buildSetForDynamicQueriesTest() {
		// Check with empty list
		List<String> source = new List<String>();
		
		String result = BDT_Utils.buildSetForDynamicQueries(source);
		system.assertEquals(NULL, result, 'Error in building set for dynamic queries');
		
		// Check with valid data
		source.add('a');
		source.add('b');
		
		result = BDT_Utils.buildSetForDynamicQueries(source);
		system.assertEquals('(\'a\',\'b\')', result, 'Error in building set for dynamic queries');
	}
	
	
	/** Test the function createEmailInstance
	* @author	Dimitrios Sgourdos
	* @version	23-Jan-2014
	*/
	static testMethod void createEmailInstanceTest() {
		List<String> receiptTo = new List<String> { 'testemail@praintl.com' };
		
		Messaging.SingleEmailMessage result = BDT_Utils.createEmailInstance(receiptTo,
																			'Test Subject',
																			'Test mail body',
																			NULL);
		
		system.assertNotEquals(result, NULL, 'Error in creating e-mail instance');
	}
	
	
	/** Test the function getNumberOfDaysPercentageInMonth
	* @author	Dimitrios Sgourdos
	* @version	26-feb-2014
	*/
	static testMethod void getNumberOfDaysPercentageInMonthTest() {
		Decimal result = BDT_Utils.getNumberOfDaysPercentageInMonth(14, 2014, 2);
		system.assertEquals(0.5, result, 'Error in calculating the number of days percentage in the given month');
	}
	
	
	/** Test the function getNumberOfDaysUntilEndOfMonth
	* @author	Dimitrios Sgourdos
	* @version	26-feb-2014
	*/
	static testMethod void getNumberOfDaysUntilEndOfMonthTest() {
		Integer result = BDT_Utils.getNumberOfDaysUntilEndOfMonth( date.newInstance(2014, 3, 20) );
		system.assertEquals(11, result, 'Error in calculating the number of days until the end of month');
	}
	
	
	/** Test the function getAbsoluteMonthsBetweenDates
	* @author	Dimitrios Sgourdos
	* @version	27-feb-2014
	*/
	static testMethod void getAbsoluteMonthsBetweenDatesTest() {
		Decimal result;
		String errorMessage = 'Error in calculating the absolute month difference between the two given dates';
		
		// Check the function with two dates in the same month-year and the start date earlier than the end date
		result = BDT_Utils.getAbsoluteMonthsBetweenDates( Date.newInstance(2014,6,9), Date.newInstance(2014,6,18) );
		system.assertEquals(0.3, result, errorMessage);
		
		// Check the function with two dates in the same month-year and the start date late than the end date
		result = BDT_Utils.getAbsoluteMonthsBetweenDates( Date.newInstance(2014,6,18), Date.newInstance(2014,6,9) );
		system.assertEquals(-0.3, result, errorMessage);
		
		// Check the function with two dates in the same month-year and the start date equal with the end date
		result = BDT_Utils.getAbsoluteMonthsBetweenDates( Date.newInstance(2014,6,9), Date.newInstance(2014,6,9) );
		system.assertEquals(0, result, errorMessage);
		
		// Check the function with two dates in different month-year, start date earlier than end date and
		// start date day earlier than the end one
		result = BDT_Utils.getAbsoluteMonthsBetweenDates( Date.newInstance(2014,6,9), Date.newInstance(2015,6,18) );
		system.assertEquals(12.3, result, errorMessage);
		
		// Check the function with two dates in different month-year, start date earlier than end date and
		// start date day later than the end one
		result = BDT_Utils.getAbsoluteMonthsBetweenDates( Date.newInstance(2014,6,18), Date.newInstance(2015,6,9) );
		system.assertEquals(11.7, result, errorMessage);
		
		// Check the function with two dates in different month-year, start date earlier than end date and
		// start date day equal with the end one
		result = BDT_Utils.getAbsoluteMonthsBetweenDates( Date.newInstance(2014,6,9), Date.newInstance(2015,6,9) );
		system.assertEquals(12, result, errorMessage);
		
		// Check the function with two dates in different month-year, start date later than end date and
		// start date day earlier than the end one
		result = BDT_Utils.getAbsoluteMonthsBetweenDates( Date.newInstance(2015,6,9), Date.newInstance(2014,6,18) );
		system.assertEquals(-11.7, result, errorMessage);
		
		// Check the function with two dates in different month-year, start date later than end date and
		// start date day later than the end one
		result = BDT_Utils.getAbsoluteMonthsBetweenDates( Date.newInstance(2015,6,18), Date.newInstance(2014,6,9) );
		system.assertEquals(-12.3, result, errorMessage);
		
		// Check the function with two dates in different month-year, start date later than end date and
		// start date day equal with the end one
		result = BDT_Utils.getAbsoluteMonthsBetweenDates( Date.newInstance(2015,6,9), Date.newInstance(2014,6,9) );
		system.assertEquals(-12, result, errorMessage);
	}
	
}