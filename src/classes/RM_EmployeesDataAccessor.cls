public with sharing class RM_EmployeesDataAccessor {
	 public static list<string> getListActiveStatus(){
        list<string> listActive = new list<string>();
        for (Employee_Status__c e:[SELECT Employee_Status__c FROM Employee_Status__c where employee_group__c in ('Active', 'Leave')]){
            listActive.add(e.Employee_Status__c);
        }
        return listActive;
    }
	 public static list<Employee_Details__c> getEmployeeDetailByEmployeeRIDs(set<string> employeeRIDs){
       return [select id, name, First_Name__c, Full_name__c, Last_Name__c, Job_Class_Desc__c, Location__c, Location_Code__c, Country_Name__r.name, 
       		Country_Name__c, Country_Name__r.Region_Name__c, 
       		Buf_Code__c, Business_Unit__c, Date_Hired__c,
            Supervisor_ID__c, Email_Address__c, Zip_PostCode__c,State_Province__c, City__c          
            from Employee_Details__c
            where id in :employeeRIDs 
            order by last_name__c, first_name__c ];
    }
    public static list<Employee_Details__c> getEmployeeDetailByEmployeeRIDs(list<string> employeeRIDs){
       return [select id, name, First_Name__c, Full_name__c, Last_Name__c, Job_Class_Desc__c, Location__c, Location_Code__c, Country_Name__r.name, 
       		Country_Name__c, Country_Name__r.Region_Name__c, 
       		Buf_Code__c, Business_Unit__c, Date_Hired__c,
            Supervisor_ID__c, Email_Address__c, Zip_PostCode__c,State_Province__c, City__c          
            from Employee_Details__c
            where id in :employeeRIDs 
            order by last_name__c, first_name__c ];
    }
    public static list<Employee_Details__c> getEmployeeDetailByEmployeeFullName(list<string> employeeFullName, set<string> stManagedBU){
       if (stManagedBU == null)
       		return [select id, name, full_name__c, First_Name__c, Last_Name__c, Job_Class_Desc__c, Location__c, Country_Name__r.name, 
       		Country_Name__c, Country_Name__r.Region_Name__c, Buf_Code__c, Business_Unit__c, Date_Hired__c,
            Supervisor_ID__c, Email_Address__c, Zip_PostCode__c,State_Province__c, City__c          
            from Employee_Details__c
            where full_name__c in :employeeFullName 
            order by last_name__c, first_name__c];
       else
       
       return [select id, name, full_name__c, First_Name__c, Last_Name__c, Job_Class_Desc__c, Location__c, Country_Name__r.name, 
       		Country_Name__c, Country_Name__r.Region_Name__c, Buf_Code__c, Business_Unit__c, Date_Hired__c,
            Supervisor_ID__c, Email_Address__c, Zip_PostCode__c,State_Province__c, City__c          
            from Employee_Details__c
            where full_name__c in :employeeFullName and Business_Unit_Desc__c in :stManagedBU
            order by last_name__c, first_name__c];
    }
     public static Employee_Details__c getEmployeeDetailByEmployee(string employeeRID){
       return [select id, name, RM_Comment__c, full_name__c, First_Name__c, Last_Name__c, Job_Class_Desc__c, Location__c, 
       		Country_Name__r.name, Country_Name__c, Country_Name__r.Region_Name__c, Buf_Code__c, Business_Unit__c, Date_Hired__c,
            Supervisor_ID__c, Email_Address__c ,Business_Unit_Desc__c, Zip_PostCode__c,State_Province__c, City__c, Manager_Last_Name__c, 
            Manager_First_Name__c, Status_Desc__c, Term_Date__c, Department__c, Manager__c,Manager_Full_Name__c,Home_Company__c, FTE_Equivalent__c, NBR_Study_Role_Yrs__c,
            NBR_PRA_Protocols__c, NBR_PRA_Monitoring_Yrs__c, NBR_Non_PRA_Protocols__c, NBR_Non_PRA_Monitoring_Yrs__c, NBR_Lab_Experience_Yrs__c,
            NBR_EDC_Experience_Yrs__c , NBR_Current_Role_Yrs__c, NBR_At_PRA_Yrs__c 
            from Employee_Details__c
            where id = :employeeRID limit 1];
    }
    public static Employee_Details__c getEmployeeDetailByEmployeeID(string employeeID){
        
        list<Employee_Details__c> e = [select id, name, First_Name__c, Last_Name__c, full_name__c, Job_Class_Desc__c, 
        	Email_Address__c , Location__c, Country_Name__r.name, Country_Name__r.Region_Name__c, Buf_Code__c, Business_Unit__c, 
        	Date_Hired__c, Zip_PostCode__c,State_Province__c, City__c 
            from Employee_Details__c
            where name = :employeeID];
        if (e.size() > 0)
            return e[0];
        else 
            return null;
    }
    
    public static list<Employee_Details__c> getEmployeeDetailByEmployeeEmail(string email){
       return [select id, name, First_Name__c, Last_Name__c, Job_Class_Desc__c, Location__c, Country_Name__r.name, 
       		Country_Name__c, Country_Name__r.Region_Name__c, Buf_Code__c, Business_Unit__c, Date_Hired__c, Zip_PostCode__c,State_Province__c, City__c
            from Employee_Details__c
            where Email_Address__c = :email];
    }
    public static list<Employee_Details__c> getEmployeeDetailByEmployeeSupervisor(string supervisorID){
        list<string> lActive = getListActiveStatus();
        
       return [select id, name, full_name__c, First_Name__c, Last_Name__c, Job_Class_Desc__c, Location__c, 
       		Country_Name__r.name, Country_Name__c, Country_Name__r.Region_Name__c, Buf_Code__c, Business_Unit__c, 
       		Date_Hired__c, Zip_PostCode__c,State_Province__c, City__c 
            from Employee_Details__c
            where Supervisor_ID__c = :supervisorID 
            	and status__c in :lActive 
            order by last_name__c, first_name__c];
    }
    
    public static string GetEmployeeRIDByEmployeeSupervisorAndEmpName(string supervisorID, string emplLastName, string emplFirstName){
       list<Employee_Details__c> empList =  [select id, name, First_Name__c, Last_Name__c, Job_Class_Desc__c, Location__c, Country_Name__r.name, Country_Name__c, Country_Name__r.Region_Name__c, Buf_Code__c, Business_Unit__c, Date_Hired__c 
            , Zip_PostCode__c,State_Province__c, City__c 
            from Employee_Details__c
            where Supervisor_ID__c = :supervisorID and Last_Name__c = :emplLastName and First_Name__c=:emplFirstName order by last_name__c, first_name__c];
        if (empList.size() > 0)
            return empList[0].id;
        return null;
    } 
    
    
    
    public static set<ID> GetEmployeeSetByProjectYearMonth(string projectRID, Integer startMonth, Integer endMonth){
        list <string> jobClass = RM_DataAccessor.getClinicalJobClass();
        system.debug('---------jobClass---------'+jobClass  ); 
        system.debug('---------GetEmployeeSetByProjectYearMonth parameter---------'+projectRID + '--' + startMonth + '--' +   endMonth);
        Set<ID> employeeIDs = new Set<ID>();        
        
        for (WFM_employee_Allocations__c a: [Select EMPLOYEE_ID__c 
            FROM WFM_employee_Allocations__c WHERE Project_ID__c =:projectRID and employee_id__r.job_class_desc__c in :jobClass     
            and allocation_end_date__c >= :RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(startMonth)) 
            and allocation_start_date__c  <= :RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(endMonth))])
            employeeIDs.add(a.EMPLOYEE_ID__c);
        system.debug('---------employeeIDs---------'+employeeIDs  );     
        
        for (WFM_EE_Work_History__c h: [select employee_id__c from WFM_EE_Work_History__c  
        where project_id__c= :projectRID and year_month__c in :RM_Tools.GetMonths(startMonth, endMonth) and employee_id__r.job_class_desc__c in :jobClass])
            employeeIDs.add(h.employee_id__c);
        return employeeIDs;
    }
    public static list<Employee_Details__c> GetEmployeeListBySetEmployeeRID(Set<ID> employeeIDs){
        list<Employee_Details__c> emplAlloList = [SELECT id, Name, last_name__c, first_name__c, job_class_desc__c, country_name__r.name, Zip_PostCode__c,State_Province__c, City__c     
        FROM Employee_Details__c 
        where id in :employeeIDs order  by last_name__c, first_name__c];
        
        return emplAlloList;
    } 
   /* public static list<Employee_Details__c> GetEmployeeListBystrSetEmployeeRID(Set<string> employeeIDs){
        list<Employee_Details__c> emplAlloList = [SELECT id, Name, full_name__c, last_name__c, first_name__c, job_class_desc__c, country_name__r.name, Zip_PostCode__c,State_Province__c, City__c, buf_code__c     
        FROM Employee_Details__c 
        where id in :employeeIDs order  by last_name__c, first_name__c];
        
        return emplAlloList;
    } */
    /*-- Work_history_summary--*/
    public static list<WFM_EE_Work_History_Summary__c> GetWorkHistorySummarybyEmployeeRID(string employeeRID){
        return [select Sum_Worked_Hours__c,Start_Year_Month__c,End_Year_Month__c, project__r.name, project__r.Contract_id__r.Client_ID__r.name, Buf_Code__c 
        from WFM_EE_Work_History_Summary__c 
        where Employee_ID__c=:employeeRID
        order by End_Year_Month__c desc, project__r.name, Buf_Code__c limit 1000];
    }
  /*  public static List<WFM_EE_Work_History__c> GetActiveAssignmentWorkHist(string eID, list<string> projRID, Integer startMonth, Integer endMonth){
        return [SELECT buf_code__c,Employee_ID__c,Hours__c,Name,Project_ID__c, cal_fte__c, year_month__c, project_id__r.name, project_id__r.Contract_id__r.Client_ID__r.name
            FROM WFM_EE_Work_History__c  where
            year_month__c in :RM_Tools.GetMonths(startMonth, endMonth) and employee_id__c = :eID and project_id__c in :projRID
            order by Project_ID__r.name, buf_code__c];
        
    }
    public static List<WFM_EE_Work_History__c> GetWorkHistByEmployee(string eID, Integer startMonth, Integer endMonth){
        return [SELECT buf_code__c,Employee_ID__c,Hours__c,Name,Project_ID__c, cal_fte__c, year_month__c, project_id__r.name, Availability__c, project_id__r.Contract_id__r.Client_ID__r.name
            FROM WFM_EE_Work_History__c  where
            year_month__c in :RM_Tools.GetMonths(startMonth, endMonth) and employee_id__c = :eID and project_id__r.contract_id__r.name <>null
            order by Project_ID__r.name, buf_code__c];
        
    }*/
     public static list<AggregateResult> GetEmployeeWorkHistrySummarybyEmployeesProject(Set<ID> employeeIDs, string projectRID){
        return [SELECT Employee_ID__c, max(End_Year_Month__c) endMonth,Project__c,min(Start_Year_Month__c) startMonth 
            FROM WFM_EE_Work_History_Summary__c WHERE Project__c =: projectRID and employee_id__c in : employeeIDs 
            group by Employee_ID__c, project__c];
    }
    public static list<AggregateResult> GetSumEmployeeWorkHistByEmployeesProjectYearMonth(Set<ID> employeeIDs, string projectRID, Integer startMonth, Integer endMonth){
        return [select employee_id__c, year_month__c, sum(cal_FTE__c) totalFTE, sum(Hours__c) totalHr
        from WFM_EE_Work_History__c 
        where employee_id__c in : employeeIDs and project_id__c= :projectRID and year_month__c in :RM_Tools.GetMonths(startMonth, endMonth) 
        group by employee_id__c, year_month__c
        order by year_month__c];
    }
    public static list<AggregateResult> GetSumEmployeeWorkHistByEmployeeProjectRIDsYearMonth(Set<ID> projectRIDs, string employeeRID, Integer startMonth, Integer endMonth){
        return [select project_id__c, year_month__c, sum(hours__c) totalHr, sum(cal_FTE__c) totalFTE
        from WFM_EE_Work_History__c 
        where employee_id__c = : employeeRID and project_id__c in :projectRIDs and year_month__c in :RM_Tools.GetMonths(startMonth, endMonth) 
        group by project_id__c, year_month__c
        order by year_month__c];
    }
    public static list<AggregateResult> GetEmployeeWorkHistrySummarybyEmployeeProjectIDSet(Set<ID> projectIDs, string EmplRID){
        return [SELECT Employee_ID__c, max(Year_Month__c) endMonth,Project_id__c,min(Year_Month__c) startMonth 
            FROM WFM_EE_Work_History__c WHERE Project_id__c in : projectIDs and employee_id__c =:EmplRID 
            group by Employee_ID__c, Project_id__c];
    }
    public static List<AggregateResult> GetPostedFTEbyEmployeeRIDMonth(string employeeRID, string yMonth){
         AggregateResult[] HrSummary =
        [SELECT sum(cal_FTE__c) total_FTE,  project_id__r.name projID
        FROM WFM_EE_Work_History__c where Employee_ID__c=:employeeRID and year_month__c = :yMonth
        group by project_id__r.name order by project_id__r.name];
         
        return HrSummary;
    }
    public static list<WFM_EE_Work_History_Summary__c> GetWorkHistorybyProjectRID(string projectRID){
        return [select Sum_Worked_Hours__c,Start_Year_Month__c,End_Year_Month__c, Employee_ID__r.First_Name__c, Employee_ID__r.Last_Name__c, Buf_Code__c 
        from WFM_EE_Work_History_Summary__c 
        where project__C=:projectRID
        order by End_Year_Month__c desc, Employee_ID__r.last_name__c,Employee_ID__r.First_Name__c, Buf_Code__c limit 1000];
    }
    public static List<AggregateResult> GetActuralWorkHrByProjectIDCountry(string projectRID, string countryName, string strBU, Integer startMonth, Integer endMonth){
         list <string> jobClass = RM_DataAccessor.getJobClassDescByBU(strBU);
        AggregateResult[] histList;
        if (countryName == 'All')
            histList = [SELECT sum(Cal_FTE__c) fte,Year_Month__c, employee_id__r.job_class_desc__c 
        FROM WFM_EE_Work_History__c where project_id__c = :projectRID and year_month__c in :RM_Tools.GetMonths(startMonth, endMonth) and employee_id__r.job_class_desc__c in :jobClass 
        group by year_month__c,  Employee_ID__r.job_class_desc__c order by employee_id__r.job_class_desc__c, year_month__c];
        
        else
            histList = [SELECT sum(Cal_FTE__c) fte,Year_Month__c, employee_id__r.job_class_desc__c 
            FROM WFM_EE_Work_History__c where project_id__c = :projectRID and employee_id__r.Country_Name__r.name=:countryName and year_month__c in :RM_Tools.GetMonths(startMonth, endMonth) and employee_id__r.job_class_desc__c in :jobClass 
            group by year_month__c,  Employee_ID__r.job_class_desc__c order by Employee_ID__r.job_class_desc__c, year_month__c];
            
        return histList;
    }
    public static List<string> GetEmployeeRolewithActuralWorkHrByProjectIDCountry(string projectRID, string countryName, string strBU, Integer startMonth, Integer endMonth){
        list <string> jobClass = RM_DataAccessor.getJobClassDescByBU(strBU);
        AggregateResult[] histList;
        if (countryName == 'All')
            histList = [SELECT count(id) nRecord,  employee_id__r.job_class_desc__c 
        FROM WFM_EE_Work_History__c where project_id__c = :projectRID and year_month__c in :RM_Tools.GetMonths(startMonth, endMonth) and employee_id__r.job_class_desc__c in :jobClass 
        group by  Employee_ID__r.job_class_desc__c order by employee_id__r.job_class_desc__c];
        
        else
            histList = [SELECT count(id) nRecord,  employee_id__r.job_class_desc__c 
            FROM WFM_EE_Work_History__c where project_id__c = :projectRID and employee_id__r.Country_Name__r.name=:countryName and year_month__c in :RM_Tools.GetMonths(startMonth, endMonth) and employee_id__r.job_class_desc__c in :jobClass 
            group by  Employee_ID__r.job_class_desc__c order by Employee_ID__r.job_class_desc__c];
        list<string> EmployeeRole = new list<string>();
        for(AggregateResult ar:histList)
	   		EmployeeRole.add((string)ar.get('Job_Class_Desc__c'));
				 
        return EmployeeRole;
    }
    public static List<AggregateResult> GetActuralWorkHrByProjectRole(string projectRID, string jobClassDesc, string bu, Integer startMonth, Integer endMonth){
        
        AggregateResult[] histList = [SELECT sum(Cal_FTE__c) fte, Year_Month__c, Employee_ID__r.job_class_desc__c 
        FROM WFM_EE_Work_History__c where project_id__c = :projectRID and year_month__c in :RM_Tools.GetMonths(startMonth, endMonth) and employee_id__r.job_class_desc__c= :jobClassDesc and employee_id__r.business_unit_desc__c =:bu
        group by year_month__c,  Employee_ID__r.job_class_desc__c order by Employee_ID__r.job_class_desc__c, year_month__c];
        return histList;
    }
    
    public static List<AggregateResult> GetEmplActuralWorkHrByProjectRoleYMonth(string projectRID, string jobClassDesc, string bu, string yMonth){
        
        AggregateResult[] histList = [SELECT sum(Cal_FTE__c) fte,Year_Month__c, employee_id__c, employee_id__r.job_class_desc__c, employee_Id__r.last_name__c, employee_Id__r.first_name__c
        FROM WFM_EE_Work_History__c where project_id__c = :projectRID and year_month__c = :yMonth and employee_id__r.job_class_desc__c= :jobClassDesc and employee_id__r.business_unit_desc__c =:bu
        group by year_month__c,  employee_id__r.job_class_desc__c, employee_id__c, employee_Id__r.last_name__c, employee_Id__r.first_name__c 
        order by employee_Id__r.last_name__c, employee_Id__r.first_name__c, year_month__c];
        return histList;
    }
    public static List<AggregateResult> GetActuralWorkHrCountryByProjectRoleYMonth(string projectRID, string jobClassDesc, string yMonth){
        
        AggregateResult[] histList = [SELECT sum(Cal_FTE__c) fte, employee_Id__r.country_name__r.name country
        FROM WFM_EE_Work_History__c where project_id__c = :projectRID and year_month__c = :yMonth and employee_id__r.job_class_desc__c= :jobClassDesc 
        group by employee_Id__r.country_name__r.name
        order by employee_Id__r.country_name__r.name];
        return histList;
    }
    
    /*--- other experience ----*/
    public static List<WFM_EE_Therapeutic_Exp__c> GetTherapeuticExpByEmployeeRID(string employeeRID){
    
        List<WFM_EE_Therapeutic_Exp__c> exps = [SELECT EMPLOYEE_ID__c,Id,primary_indication__c,Indication_Group__c,Therapeutic_Area__c,NBR_PRA_Monitoring_Exp_Yrs__c,NBR_Role_Experience_Yrs__c,NBR_Study_Drug_Yrs__c,NBR_Total_Monitoring_Exp__c,Role__c,Study_Drug_Type__c,Subject_Population__c 
                      FROM WFM_EE_Therapeutic_Exp__c WHERE EMPLOYEE_ID__c = :employeeRID order by name, indication_group__c, primary_indication__c];
        List<WFM_EE_Therapeutic_Exp__c> newExps = new List<WFM_EE_Therapeutic_Exp__c>();   
        if (exps.size() > 0){         
                    
             integer n = 0;
             string thExp = exps[n].Therapeutic_Area__c + exps[n].Indication_group__c + exps[n].primary_indication__c;
             newExps.add(exps[n]);
             
             for (integer i = 1; i < exps.size(); i++){            
             
                boolean bRemove = false;
                string newExp = exps[i].Therapeutic_Area__c  + exps[i].Indication_group__c + exps[i].primary_indication__c;
                if (newExp == thExp){
                
                    if (exps[i].NBR_PRA_Monitoring_Exp_Yrs__c != null && exps[n].NBR_PRA_Monitoring_Exp_Yrs__c == null){                       
                            exps[n].NBR_PRA_Monitoring_Exp_Yrs__c = exps[i].NBR_PRA_Monitoring_Exp_Yrs__c;
                            bRemove = true;
                    }
                    if (exps[i].NBR_Total_Monitoring_Exp__c != null && exps[n].NBR_Total_Monitoring_Exp__c == null){
                            exps[n].NBR_Total_Monitoring_Exp__c = exps[i].NBR_Total_Monitoring_Exp__c;
                            bRemove = true;
                    }
                    if (exps[i].Role__c != null && exps[n].Role__c == null) {
                            exps[n].Role__c = exps[i].Role__c;
                            bRemove = true;
                    }
                    if (exps[i].NBR_Role_Experience_Yrs__c != null && exps[n].NBR_Role_Experience_Yrs__c == null) {
                            exps[n].NBR_Role_Experience_Yrs__c = exps[i].NBR_Role_Experience_Yrs__c;
                            bRemove = true;
                    }
                    if (exps[i].Study_Drug_Type__c != null && exps[n].Study_Drug_Type__c == null) {
                            exps[n].Study_Drug_Type__c = exps[i].Study_Drug_Type__c;
                            bRemove = true;
                    }
                    if (exps[i].NBR_Study_Drug_Yrs__c != null && exps[n].NBR_Study_Drug_Yrs__c == null){
                            exps[n].NBR_Study_Drug_Yrs__c = exps[i].NBR_Study_Drug_Yrs__c;
                            bRemove = true;
                    }
                    if (exps[i].Subject_Population__c != null && exps[n].Subject_Population__c == null){
                            exps[n].Subject_Population__c = exps[i].Subject_Population__c;
                            bRemove = true;
                    }
                    if (!bRemove){
                            newExps.add(exps[i]);
                    }
                }
                else {
                    newExps.add(exps[i]);
                    thExp = newExp;
                    n = i;
                }
             }
             return newExps;
         }
         
         return exps;
    }
    public static List<WFM_EE_LANGUAGE__c> GetLanguagebyEmployeeRID(string employeeRID){
         return [select language__c,Level_of_Speak__c,Level_of_Write__c,Level_of_Med_Term__c 
                      from WFM_EE_LANGUAGE__c where employee_id__c = :employeeRID order by name ];
    }
    public static List<WFM_EE_CERTIFICATION__c> GetCertificatebyEmployeeRID(string employeeRID){
          return [select Certification_Affilication_Name__c,Granting_Institution__c,Effective_Date__c,Expiration_Date__c
                      from WFM_EE_CERTIFICATION__c where employee_id__c = :employeeRID order by name ];
    }
    public static List<WFM_EE_PHASE_EXPERIENCE__c> GetPhaseExpbyEmployeeRID(string employeeRID){
        return [select phase__c,Phase_experience_yrs__c
                      from WFM_EE_PHASE_EXPERIENCE__c where employee_id__c = :employeeRID order by name ];
    }
    public static List<WFM_EE_Education__c> GetEducationbyEmployeeRID(string employeeRID){
        return [select Degree_Type__c,Institution__c,Month_Obtained__c
                      from WFM_EE_Education__c where employee_id__c = :employeeRID order by name ];
    }
    public static List<WFM_EE_SYSTEM_EXPERIENCE__c>GetSysExpbyEmployeeRID(string employeeRID){
        return [select CRS_System__c,Proficiency_Level__c, Proficiency__c
                      from WFM_EE_SYSTEM_EXPERIENCE__c where employee_id__c = :employeeRID order by name ];
    }
}