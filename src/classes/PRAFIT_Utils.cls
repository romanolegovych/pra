/*
 * PRAFIT_Utils supports the various Domain Manager or Controller classes within the PRAFIT App.
 *  
 *  getPickListValues: Dynamically describe the enumerations within a given picklist field and return them as a list of Strings
 *  getUserIdsFromGroup: Get set of User IDs for All Members of a Pulblic Group
 *  getSObjects: Get all fields from a given sobject and generate the needed soql and return as a list of sObjects
 *  
 *  IMPORTANT: Inherits the “with sharing” OR "without sharing" keyword from the calling class to enforce Sharing Rules
*/

public class PRAFIT_Utils{
	
	//check to see if current user has a profile by name
	public static boolean checkProfile(string profileName){
		Id PROFILEID = UserInfo.getProfileId();
        try{
        	Profile prof = [SELECT id
          					FROM Profile
                      		WHERE Id = : PROFILEID AND Name =: profileName LIMIT 1];
  			if(prof != null){
  				return true;
  			}else{
  				return false;
  			}
        }catch(Exception e){
            system.debug('>>>>>>>>>>>>>>>>>>>>  Exception in PRAFIT_Utils.class : Error querying for profile : ' + profileName + 
            	'. The error is as follows : [' + e.getMessage() +']');
        	return false;
        }
		        								   
	}
	//check to see if current user has access to a given permission set by name
	public static boolean checkPermissionSet(string permSetName){
		Id USERID = UserInfo.getUserID();
        try{
        	PermissionSetAssignment permSet = [SELECT PermissionSetId
                      					   	   FROM PermissionSetAssignment
                      					       WHERE AssigneeId = : USERID AND PermissionSet.Label =: permSetName LIMIT 1];
  			if(permSet != null){
  				return true;
  			}else{
  				return false;
  			}
        }catch(Exception e){
            system.debug('>>>>>>>>>>>>>>>>>>>>  Exception in PRAFIT_Utils.class : Error querying for permisison set : ' + permSetName + 
            	'. The error is as follows : [' + e.getMessage() +']');
        	return false;
        }
		        								   
	}
	
	//fetch a given custom setting for the PRAFIT App
    public static String prafitCustomSetting(String customSetting){
        String customSettingValue;
        try{
            PRAFIT_Config__c customSettingObj = PRAFIT_Config__c.getInstance(customSetting);
            customSettingValue = customSettingObj.Value__c;
        }catch(Exception e){
            System.debug('>>>>>>>>>>>>>>>>>>>>  Exception in PRAFIT_Utils.class : CustomSetting missing for : ' + 
                        customSetting + '[' + e.getMessage() +']');
        }
        return customSettingValue;
    }
    
	//get record type id for a given object by record type name
	public static ID getRecordTypeId(String objectName, String recordTypeName) {
		try{
			sObject sObj = Schema.getGlobalDescribe().get(objectName).newSObject();
			Schema.sObjectType sObjType = sObj.getSObjectType();
			Schema.DescribeSObjectResult sObjDescribe = sObjType.getDescribe();
			String recordTypeId = sObjDescribe.getRecordTypeInfosByName().get(recordTypeName).getRecordTypeId(); 
			System.debug('>>>>>>>>>>>>>>>>>>>>  recordTypeId = ' + recordTypeId );
			return recordTypeId;
		}catch(exception e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
				'Unable to find Record Type setup information for the object ' + objectName + '. Please contact your system administrator.');
			ApexPages.addMessage(msg);
			System.Debug('>>>>>>>>>>>>>>>>>>>>  Exception in PRAFIT_Utils.class:' + msg + '(' + e.getMessage() + ')');
			return null;
		}
	}
	
	//check if a record type is available to the user for a given object by record type name
	public static Boolean recordTypeIsAvailable(String objectName, String recordTypeName) {
		try{
			sObject sObj = Schema.getGlobalDescribe().get(objectName).newSObject();
			Schema.sObjectType sObjType = sObj.getSObjectType();
			Schema.DescribeSObjectResult sObjDescribe = sObjType.getDescribe();
			Boolean recordTypeIsAvailable = sObjDescribe.getRecordTypeInfosByName().get(recordTypeName).isAvailable(); 
			System.debug('>>>>>>>>>>>>>>>>>>>>  recordTypeIsAvailable = ' + recordTypeIsAvailable );
			return recordTypeIsAvailable;
		}catch(exception e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
				'Unable to find Record Type setup information for the object ' + objectName + '. Please contact your system administrator.');
			ApexPages.addMessage(msg);
			System.Debug('>>>>>>>>>>>>>>>>>>>>  Exception in PRAFIT_Utils.class:' + msg + '(' + e.getMessage() + ')');
			return null;
		}
	}
	
	//get all picklist option values using the type of sobject and specific picklist field
    //TODO: move this into platform wide sobject utils class as it is not domain specific
    public static List<String> getPickListValues(String objectName, String fieldName) {
        try{
            List<String> pickListValuesList = new List<String>();
            sObject sObj = Schema.getGlobalDescribe().get(objectName).newSObject();
            Schema.sObjectType sObjType = sObj.getSObjectType();
            Schema.DescribeSObjectResult sObjDescribe = sObjType.getDescribe();
            Map<String, Schema.SObjectField> sObjFieldMap = sObjDescribe.fields.getMap();
            List<Schema.PicklistEntry> sObjPickListEntryList = sObjFieldMap.get(fieldName).getDescribe().getPickListValues();
            System.debug('>>>>>>>>>>>>>>>>>>>>  sObjPickListEntryList for ' + objectName + ':' + fieldName + ' = ' + sObjPickListEntryList);
            for (Schema.PicklistEntry sObjPickListEntry : sObjPickListEntryList) {
                pickListValuesList.add(sObjPickListEntry.getLabel());
            }
            return pickListValuesList;
        }catch(exception e){
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR,
            	'Unable to find setup information for the field ' + objectName + '.' + fieldName + 
				'. Please contact your system administrator.');
            ApexPages.addMessage(msg);
            System.Debug('>>>>>>>>>>>>>>>>>>>>  Exception in PRAFIT_Utils.class:' + msg + '(' + e.getMessage() + ')');
        	return null;
        }
        
    }
    
    //Get set of User IDs for All Members of a Pulblic Group
    //TODO: need to enhance this to account for nested Roles and Roles w/Subordinates
    //TODO: move this into platform wide sobject utils class as it is not domain specific
    public static Set<id> getUserIdsFromGroup(Id groupId){
        // store the results in a set so we don't get duplicates
        Set<Id> userIdSet = new Set<Id>();
        String userType = Schema.SObjectType.User.getKeyPrefix();
        String groupType = Schema.SObjectType.Group.getKeyPrefix();
        // Loop through all group members in a group
        for (GroupMember m : [Select Id, UserOrGroupId From GroupMember Where GroupId = :groupId]){
            // If the user or group id is a user
            if (((String)m.UserOrGroupId).startsWith(userType)){
                userIdSet.add(m.UserOrGroupId);
            }
            // If the user or group id is a group
            // Note: there may be a problem with governor limits if this is called too many times
            else if (((String)m.UserOrGroupId).startsWith(groupType)){
                // Call this function again but pass in the group found within this group
                userIdSet.addAll(GetUSerIdsFromGroup(m.UserOrGroupId));
            }
        }
        return userIdSet;  
    }
    
	//get all fields from a given sobject, generate the needed soql, add optional where conditions and return results
    //TODO: move this into platform wide sobject utils class as it is not domain specific
    public static List<sObject> getSObjects(String objName, String criteria, List<String> customFields, Integer limitClause){
		List<sObject> sObjList = new List<sObject>();		
		// build the base soqlQuery
        String soqlQuery = getBaseQuery(objName, customFields);		
		System.debug('>>>>>>>>>>>>>>>>>>>>  base soqlQuery = ' + soqlQuery);
		// add the where clause to the soqlQuery
		if((criteria != null) && (criteria.length() > 0)){
			soqlQuery += ' WHERE  ' + criteria;
			System.debug('>>>>>>>>>>>>>>>>>>>>  base with criteria soqlQuery = ' + soqlQuery);
		}
        if((limitClause != null) && (limitClause > 0)){
			soqlQuery += ' LIMIT  ' + limitClause;
			System.debug('>>>>>>>>>>>>>>>>>>>>  base with limit soqlQuery = ' + soqlQuery);
		}
		// make the dynamic call to get the records
		sObjList = Database.query(soqlQuery);
		return sObjList;
	}
    
	//return a dynamic Query string that includes all the direct sObject fields and the related Name fields from the parent objects
    //dependent method of getSobjects
    //TODO: move this into platform wide sobject utils class as it is not domain specific
    public static String getBaseQuery(String objName, List<String> customFields){		
		// get the describe info for the org, object and its fields
		Map<String,Schema.SObjectType> orgMap = Schema.getGlobalDescribe();
		Schema.SObjectType sObjType = orgMap.get(objName);
		Schema.DescribeSObjectResult sObjDescribe = sObjType.getDescribe();
		Map<String, Schema.SObjectField> sObjFieldMap = sObjDescribe.fields.getMap();
		List<Schema.SObjectField> sObjFieldList = sObjFieldMap.values();
        //build out the base soql query using the describe info
		String soqlQuery = 'SELECT ';
		for(Schema.SObjectField sObjField : sObjFieldList){
			String fieldLabel = sObjField.getDescribe().getLabel(); 
			String fieldName = sObjField.getDescribe().getName();
			String fieldType = String.valueOf(sObjField.getDescribe().getType()); 
			String relationshipName = sObjField.getDescribe().getRelationshipName();
			System.debug('>>>>>>>>>>>>>>>>>>>>  Label = ' + fieldLabel + '    Name = ' + fieldName + '     Type = ' + 
                         fieldType + '    relationshipName = ' + relationshipName);			
			// add each field to the base soql query
			soqlQuery += ' ' + fieldName + ',';
			// if this is a lookup field, also include the name field of the parent
			if(('REFERENCE'.equalsIgnoreCase(fieldType) == true) && ((relationshipName != null) && (relationshipName.length() > 0))){
				// add the name field from the parent object to the base soql query
				soqlQuery += ' ' + relationshipName + '.Name,';
			}
		}
		// add additional custom fields specified in the call to the base soql query
		if((customFields != null) && (customFields.size() > 0)){
			for(String customField : customFields){
				soqlQuery += ' ' + String.escapeSingleQuotes(customField) + ',';
			}
		}		
		// remove the last comma from the base query
		soqlQuery = soqlQuery.subString(0, soqlQuery.length() - 1);
		// add the from clause to the base query
		soqlQuery += ' FROM ' + objName + ' ';
		//return the dynamic base soql query string
		return soqlQuery;
	}
    
}