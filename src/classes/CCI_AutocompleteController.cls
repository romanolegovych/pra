global class CCI_AutocompleteController {
	@RemoteAction
	global static list<Object> retreiveWSObjects(String queryType, String queryValue) {
		list<Object> objects = new list<Object>();
		if (queryType == 'getProtocols') {
			MdmProtocolService.protocolListResponse listResponse = 
				MdmProtocolServiceWrapper.getProtocolsLikeClientProtocolNum(String.escapeSingleQuotes(queryValue));
			if (listResponse != null) {
				objects = listResponse.protocolVOs;
			}
		}
		system.debug(objects);
		return objects;
	}
}