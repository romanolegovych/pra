/** Implements the test for the Selector Layer of the object LaboratorySelectionLists__c
 * @author	Dimitrios Sgourdos
 * @version	23-Oct-2013
 */
@isTest
private class LaboratorySelectionListsDataAccessorTest {
	
	// Global variables
	private static List<LaboratorySelectionLists__c>	lslList;
	
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	16-Oct-2013
	 */
	static void init(){
		lslList = new List<LaboratorySelectionLists__c>();
		
		// Create Laboratory Selection Lists
		lslList.add(new LaboratorySelectionLists__c(Name='Category A'));
		lslList.add(new LaboratorySelectionLists__c(Name='Category B'));
		
		// Add 'Values' Records to the categories
		lslList.add(new LaboratorySelectionLists__c(Name='Category A', listFirstValue__c = 'Value A 1'));
		lslList.add(new LaboratorySelectionLists__c(Name='Category A', listFirstValue__c = 'Value A 2'));
		lslList.add(new LaboratorySelectionLists__c(Name='Category A', listFirstValue__c = 'Value A 3'));
		lslList.add(new LaboratorySelectionLists__c(Name='Category B', listFirstValue__c = 'Value B 1'));
		insert lslList;
	}
	
	
	/** Test the function getByListCategory
	 * @author	Dimitrios Sgourdos
	 * @version	23-Oct-2013
	 */
	static testMethod void getByListCategoryTest() {
		init();
		
		List<LaboratorySelectionLists__c> retrievedList = LaboratorySelectionListsDataAccessor.getByListCategory('Category A');
		system.assertEquals(retrievedList.size(), 3);
		
		retrievedList = LaboratorySelectionListsDataAccessor.getByListCategory('Category B');
		system.assertEquals(retrievedList.size(), 1);
	}
	
	
	/** Test the function getAllByType through functions getAllListValues and getAllListCategories
	 * @author	Dimitrios Sgourdos
	 * @version	23-Oct-2013
	 */
	static testMethod void getAllByTypeTest() {
		init();
		
		List<LaboratorySelectionLists__c> retrievedList = new List<LaboratorySelectionLists__c>();
		
		// Test through the function getAllListValues
		retrievedList = LaboratorySelectionListsDataAccessor.getAllListValues();
		system.assertEquals(retrievedList.size(), 4);
		
		// Test through the function getAllListCategories
		retrievedList = LaboratorySelectionListsDataAccessor.getAllListCategories();
		system.assertEquals(retrievedList.size(), 2);
	}
	
	
	/** Test the function getMatrixValuesEnablingAnticoagulant
	 * @author	Dimitrios Sgourdos
	 * @version	21-Oct-2013
	 */
	static testMethod void getMatrixValuesEnablingAnticoagulantTest() {
		List<String> valuesList = LaboratorySelectionListsDataAccessor.getMatrixValuesEnablingAnticoagulant();
		system.assertEquals(valuesList.size(), 3);
		system.assertEquals(valuesList[0], 'Plasma');
		system.assertEquals(valuesList[1], 'Whole blood');
		system.assertEquals(valuesList[2], 'Blood');
	}
	
	
	/** Test the function getAnalyticalTechniqueValuesEnablingDetection
	 * @author	Dimitrios Sgourdos
	 * @version	21-Oct-2013
	 */
	static testMethod void getAnalyticalTechniqueValuesEnablingDetectionTest() {
		List<String> valuesList = LaboratorySelectionListsDataAccessor.getAnalyticalTechniqueValuesEnablingDetection();
		system.assertEquals(valuesList.size(), 1);
		system.assertEquals(valuesList[0], 'LC');
	}
}