/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */ 
    
@isTest
private class RM_AdjustAvailabilityControllerUTest {
    static void unittest1()
    {
      String a = '2014-01-01';
      List <string> datePart1 = a.split('-');
      date startdate = date.newInstance(Integer.valueOf(datePart1[0]), Integer.valueOf(datePart1[1]), Integer.valueOf(datePart1[2]));

      String b = '2014-01-31';
      List <string> datePart2 = b.split('-');
      date enddate = date.newInstance(Integer.valueOf(datePart2[0]), Integer.valueOf(datePart2[1]), Integer.valueOf(datePart2[2]));
      
      COUNTRY__c country = new COUNTRY__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='North America', 
      daily_Business_Hrs__c=8.0);
      insert country;       
      Employee_Details__c empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='RM100', Employee_Unique_Key__c='100', First_Name__c='John', Last_Name__c='Smith',email_Address__c='JSmith@gmail.com', 
      Function_code__c='PR',Buf_Code__c='KCICR', Job_Class_Desc__c='A', Business_Unit__c='RDU', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), FTE_Equivalent__c=0.5);
      insert empl;     
      
      WFM_EE_Non_Availability_Detail__c enav=new WFM_EE_Non_Availability_Detail__c(Employee_ID__c=empl.id,End_Date__c=enddate,Start_Date__c=startdate,Type__c='PTO',Unit__c='Hours',Value__c=8);
      insert enav;
      
      WFM_Location_Hours__c hrObj = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + '2014-01',YEAR_MONTH__c='2014-01',NBR_Business_Day_In_Month__c=22);
      insert hrObj;
      
      list<WFM_Employee_Availability__c> avasList = new list<WFM_Employee_Availability__c>();
        for (integer i = 0; i <1; i++){
            WFM_Employee_Availability__c awl = new WFM_Employee_Availability__c(PTO_Unit__c='Hours',PTO_Value__c=160,EMPLOYEE_ID__c=empl.id,year_month__c='2014-01', Availability_Unique_Key__c=empl.id+'2014-01'+'Test5'+i, 
            Location_Hours__c=hrObj.id);
            avasList.add(awl);
        }
      insert avasList;
      Test.startTest();
      PageReference pageRef = Page.RM_AdjustAvailability;
      Test.setCurrentPage(pageRef);
       
      ApexPages.currentPage().getParameters().put('id', empl.id);        
      ApexPages.currentPage().getParameters().put('itemidDel',enav.id);
      RM_AdjustAvailabilityController ctrl=new RM_AdjustAvailabilityController();  
      ctrl.detailId =enav.id;       
      ctrl.datefilter='01/01/2014';
      ctrl.datefilter1='01/31/2014';      
      ctrl.hoursval='8';       
      ctrl.saveDetail(); 
      
      ctrl.gettypeOptions();
      ctrl.getHoursOptions();
      ctrl.Cancel();         
      ctrl.switchHr();
      ctrl.editAva();          
      ctrl.CancelAva();
      ctrl.isSwitch=true;
      ctrl.SaveAva();     
      ctrl.delDetail();
      
      Test.stopTest();
    }
    
    @isTest
    static void unittest2()
    {
      String a = '2014-01-01';
      List <string> datePart1 = a.split('-');
      date startdate = date.newInstance(Integer.valueOf(datePart1[0]), Integer.valueOf(datePart1[1]), Integer.valueOf(datePart1[2]));

      String b = '2014-01-25';
      List <string> datePart2 = b.split('-');
      date enddate = date.newInstance(Integer.valueOf(datePart2[0]), Integer.valueOf(datePart2[1]), Integer.valueOf(datePart2[2]));
      
      COUNTRY__c country = new COUNTRY__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='North America', 
      daily_Business_Hrs__c=8.0);
      insert country;       
      Employee_Details__c empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='RM100', Employee_Unique_Key__c='100', First_Name__c='John', Last_Name__c='Smith',email_Address__c='JSmith@gmail.com', 
      Function_code__c='PR',Buf_Code__c='KCICR', Job_Class_Desc__c='A', Business_Unit__c='RDU', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), FTE_Equivalent__c=0.5);
      insert empl;     
      
      WFM_EE_Non_Availability_Detail__c enav=new WFM_EE_Non_Availability_Detail__c(Employee_ID__c=empl.id,End_Date__c=enddate,Start_Date__c=startdate,Type__c='PTO',Unit__c='Hours',Value__c=8);
      insert enav;
      
      WFM_Location_Hours__c hrObj = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + '2014-03',YEAR_MONTH__c='2014-03',NBR_Business_Day_In_Month__c=22);
      insert hrObj;
      
      hrObj = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + '2014-04',YEAR_MONTH__c='2014-04',NBR_Business_Day_In_Month__c=22);
      insert hrObj;
      
      list<WFM_Employee_Availability__c> avasList = new list<WFM_Employee_Availability__c>();
        
      WFM_Employee_Availability__c awl = new WFM_Employee_Availability__c(PTO_Unit__c='Hours',PTO_Value__c=160,EMPLOYEE_ID__c=empl.id,year_month__c='2014-01', Availability_Unique_Key__c=empl.id+'2014-01'+'Test5',Location_Hours__c=hrObj.id);
      avasList.add(awl);
          
      insert avasList;
      
      Test.startTest();
      PageReference pageRef = Page.RM_AdjustAvailability;
      Test.setCurrentPage(pageRef);
       
      ApexPages.currentPage().getParameters().put('id', empl.id);        
      ApexPages.currentPage().getParameters().put('itemidDel',enav.id);
      RM_AdjustAvailabilityController ctrl=new RM_AdjustAvailabilityController();  
      ctrl.detailId =enav.id;     
      ctrl.datefilter='01/01/2014';
      ctrl.datefilter1='01/25/2014';      
      ctrl.hoursval='8'; 
      ctrl.saveDetail(); 
      
      ctrl.gettypeOptions();
      ctrl.getHoursOptions();
      ctrl.Cancel();         
      ctrl.switchHr();
      ctrl.editAva();          
      ctrl.CancelAva();
      ctrl.isSwitch=true;
      ctrl.SaveAva();     
      ctrl.delDetail();
      
      Test.stopTest();
    }
    
    @isTest
    static void unittest3()
    {
      String a = '2014-01-01';
      List <string> datePart1 = a.split('-');
      date startdate = date.newInstance(Integer.valueOf(datePart1[0]), Integer.valueOf(datePart1[1]), Integer.valueOf(datePart1[2]));

      String b = '2014-02-28';
      List <string> datePart2 = b.split('-');
      date enddate = date.newInstance(Integer.valueOf(datePart2[0]), Integer.valueOf(datePart2[1]), Integer.valueOf(datePart2[2]));
      
      COUNTRY__c country = new COUNTRY__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='North America', 
      daily_Business_Hrs__c=8.0);
      insert country;       
      Employee_Details__c empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='RM100', Employee_Unique_Key__c='100', First_Name__c='John', Last_Name__c='Smith',email_Address__c='JSmith@gmail.com', 
      Function_code__c='PR',Buf_Code__c='KCICR', Job_Class_Desc__c='A', Business_Unit__c='RDU', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), FTE_Equivalent__c=0.5);
      insert empl;     
      
      WFM_EE_Non_Availability_Detail__c enav=new WFM_EE_Non_Availability_Detail__c(Employee_ID__c=empl.id,End_Date__c=enddate,Start_Date__c=startdate,Type__c='PTO',Unit__c='Hours',Value__c=8);
      insert enav;
      
      WFM_Location_Hours__c hrObj = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + '2014-01',YEAR_MONTH__c='2014-01',NBR_Business_Day_In_Month__c=22);
      insert hrObj;
      
      WFM_Location_Hours__c hrObj1 = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + '2014-02',YEAR_MONTH__c='2014-02',NBR_Business_Day_In_Month__c=22);
      insert hrObj1;
      
      list<WFM_Employee_Availability__c> avasList = new list<WFM_Employee_Availability__c>();        
      WFM_Employee_Availability__c awl = new WFM_Employee_Availability__c(PTO_Unit__c='Hours',PTO_Value__c=160,EMPLOYEE_ID__c=empl.id,year_month__c='2014-01', Availability_Unique_Key__c=empl.id+'2014-01'+'Test5',Location_Hours__c=hrObj.id);
      avasList.add(awl);
      awl = new WFM_Employee_Availability__c(PTO_Unit__c='Hours',PTO_Value__c=160,EMPLOYEE_ID__c=empl.id,year_month__c='2014-02', Availability_Unique_Key__c=empl.id+'2014-02'+'Test6',Location_Hours__c=hrObj1.id);
      avasList.add(awl);      
          
      insert avasList;
      
      Test.startTest();
      PageReference pageRef = Page.RM_AdjustAvailability;
      Test.setCurrentPage(pageRef);
       
      ApexPages.currentPage().getParameters().put('id', empl.id);        
      ApexPages.currentPage().getParameters().put('itemidDel',enav.id);
      RM_AdjustAvailabilityController ctrl=new RM_AdjustAvailabilityController();  
      ctrl.detailId =enav.id;     
      ctrl.datefilter='01/01/2014';
      ctrl.datefilter1='02/28/2014';      
      ctrl.hoursval='8'; 
      ctrl.saveDetail(); 
      
      ctrl.gettypeOptions();
      ctrl.getHoursOptions();
      ctrl.Cancel();         
      ctrl.switchHr();
      ctrl.editAva();          
      ctrl.CancelAva();
      ctrl.isSwitch=true;
      ctrl.SaveAva();     
      ctrl.delDetail();
      
      Test.stopTest();
    }
    
    @isTest
    static void unittest4()
    {
      String a = '2014-01-01';
      List <string> datePart1 = a.split('-');
      date startdate = date.newInstance(Integer.valueOf(datePart1[0]), Integer.valueOf(datePart1[1]), Integer.valueOf(datePart1[2]));

      String b = '2014-03-31';
      List <string> datePart2 = b.split('-');
      date enddate = date.newInstance(Integer.valueOf(datePart2[0]), Integer.valueOf(datePart2[1]), Integer.valueOf(datePart2[2]));
      
      COUNTRY__c country = new COUNTRY__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='North America', 
      daily_Business_Hrs__c=8.0);
      insert country;       
      Employee_Details__c empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='RM100', Employee_Unique_Key__c='100', First_Name__c='John', Last_Name__c='Smith',email_Address__c='JSmith@gmail.com', 
      Function_code__c='PR',Buf_Code__c='KCICR', Job_Class_Desc__c='A', Business_Unit__c='RDU', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), FTE_Equivalent__c=0.5);
      insert empl;     
      
      WFM_EE_Non_Availability_Detail__c enav=new WFM_EE_Non_Availability_Detail__c(Employee_ID__c=empl.id,End_Date__c=enddate,Start_Date__c=startdate,Type__c='PTO',Unit__c='Hours',Value__c=8);
      insert enav;
      
      WFM_Location_Hours__c hrObj = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + '2014-01',YEAR_MONTH__c='2014-01',NBR_Business_Day_In_Month__c=22);
      insert hrObj;
      
      WFM_Location_Hours__c hrObj1 = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + '2014-02',YEAR_MONTH__c='2014-02',NBR_Business_Day_In_Month__c=22);
      insert hrObj1;
      
      WFM_Location_Hours__c hrObj2 = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + '2014-03',YEAR_MONTH__c='2014-03',NBR_Business_Day_In_Month__c=22);
      insert hrObj2;
      
      list<WFM_Employee_Availability__c> avasList = new list<WFM_Employee_Availability__c>();        
      WFM_Employee_Availability__c awl = new WFM_Employee_Availability__c(PTO_Unit__c='Hours',PTO_Value__c=160,EMPLOYEE_ID__c=empl.id,year_month__c='2014-01', Availability_Unique_Key__c=empl.id+'2014-01'+'Test5',Location_Hours__c=hrObj.id);
      avasList.add(awl);
      awl = new WFM_Employee_Availability__c(PTO_Unit__c='Hours',PTO_Value__c=160,EMPLOYEE_ID__c=empl.id,year_month__c='2014-02', Availability_Unique_Key__c=empl.id+'2014-02'+'Test6',Location_Hours__c=hrObj1.id);
      avasList.add(awl);      
      awl = new WFM_Employee_Availability__c(PTO_Unit__c='Hours',PTO_Value__c=160,EMPLOYEE_ID__c=empl.id,year_month__c='2014-03', Availability_Unique_Key__c=empl.id+'2014-03'+'Test7',Location_Hours__c=hrObj2.id);
      avasList.add(awl);      
      insert avasList;
      
      Test.startTest();
      PageReference pageRef = Page.RM_AdjustAvailability;
      Test.setCurrentPage(pageRef);
       
      ApexPages.currentPage().getParameters().put('id', empl.id);        
      ApexPages.currentPage().getParameters().put('itemidDel',enav.id);
      RM_AdjustAvailabilityController ctrl=new RM_AdjustAvailabilityController();  
      ctrl.detailId =enav.id;     
      ctrl.datefilter='01/01/2014';
      ctrl.datefilter1='03/31/2014';      
      ctrl.hoursval='8'; 
      ctrl.saveDetail(); 
      
      ctrl.gettypeOptions();
      ctrl.getHoursOptions();
      ctrl.Cancel();         
      ctrl.switchHr();
      ctrl.editAva();          
      ctrl.CancelAva();
      ctrl.isSwitch=true;
      ctrl.SaveAva();     
      ctrl.delDetail(); 
      
      Test.stopTest();
    }

    @isTest
    static void unittest()
    { 
      String a = '2014-01-1';
      List <string> datePart1 = a.split('-');
      date startdate = date.newInstance(Integer.valueOf(datePart1[0]), Integer.valueOf(datePart1[1]), Integer.valueOf(datePart1[2]));

      String b = '2015-12-31';
      List <string> datePart2 = b.split('-');
      date enddate = date.newInstance(Integer.valueOf(datePart2[0]), Integer.valueOf(datePart2[1]), Integer.valueOf(datePart2[2]));
      
      COUNTRY__c country = new COUNTRY__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='North America', 
      daily_Business_Hrs__c=8.0);
      insert country;       
      Employee_Details__c empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='RM100', Employee_Unique_Key__c='100', First_Name__c='John', Last_Name__c='Smith',email_Address__c='JSmith@gmail.com', 
      Function_code__c='PR',Buf_Code__c='KCICR', Job_Class_Desc__c='A', Business_Unit__c='RDU', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), FTE_Equivalent__c=0.5);
      insert empl;
      
      list<string> months = RM_Tools.GetMonthsList(-1, 5);
        
       List<WFM_Location_Hours__c> hrs = new List<WFM_Location_Hours__c>();
        for (string month:months){
            WFM_Location_Hours__c hr = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + month,YEAR_MONTH__c=month,NBR_Business_Day_In_Month__c=22);
            hrs.add(hr); 
        }
        insert hrs;  
     
        list<Location_Holiday_Schedule__c> listlhs=new list<Location_Holiday_Schedule__c>();
        Location_Holiday_Schedule__c obj=new Location_Holiday_Schedule__c(Holiday_Name__c='TTT', Loc_Holiday_Ext_ID__c='TTT',Holiday_Date__c=startdate, Location_Code__c='TTT');
        listlhs.add(obj);
        insert listlhs;
        
        list<WFM_Employee_Availability__c> avas = new list<WFM_Employee_Availability__c>();      
        for (integer i = 0; i <months.size(); i++){
            WFM_Employee_Availability__c awl = new WFM_Employee_Availability__c(PTO_Unit__c='Hours',PTO_Value__c=160,EMPLOYEE_ID__c=empl.id,year_month__c=months[i], Availability_Unique_Key__c=empl.id+months[i]+'Test1'+i, 
            Location_Hours__c=hrs[i].id);
            avas.add(awl);
        }
        insert avas;
        
        avas = new list<WFM_Employee_Availability__c>();
         for (integer i = 0; i <months.size(); i++){
            WFM_Employee_Availability__c awl = new WFM_Employee_Availability__c(Training_Unit__c='FTE',EMPLOYEE_ID__c=empl.id,year_month__c=months[i], Availability_Unique_Key__c=empl.id+months[i]+'Test2'+i, 
            Location_Hours__c=hrs[i].id);
            avas.add(awl);
        }
        insert avas;
        
        avas = new list<WFM_Employee_Availability__c>();
        for (integer i = 0; i <months.size(); i++){
            WFM_Employee_Availability__c awl = new WFM_Employee_Availability__c(Pre_Starter_Unit__c='FTE',EMPLOYEE_ID__c=empl.id,year_month__c=months[i], Availability_Unique_Key__c=empl.id+months[i]+'Test3'+i, 
            Location_Hours__c=hrs[i].id);
            avas.add(awl);
        }
        insert avas;
        
        avas = new list<WFM_Employee_Availability__c>();
        for (integer i = 0; i <months.size(); i++){
            WFM_Employee_Availability__c awl = new WFM_Employee_Availability__c(Leave_Unit__c='Hours',Leave_Value__c=5,EMPLOYEE_ID__c=empl.id,year_month__c=months[i], Availability_Unique_Key__c=empl.id+months[i]+'Test4'+i, 
            Location_Hours__c=hrs[i].id);
            avas.add(awl);
        }
        insert avas;
        
        avas = new list<WFM_Employee_Availability__c>();
        for (integer i = 0; i <months.size(); i++){
            WFM_Employee_Availability__c awl = new WFM_Employee_Availability__c(Non_Billable_Unit__c='Hours',EMPLOYEE_ID__c=empl.id,year_month__c=months[i], Availability_Unique_Key__c=empl.id+months[i]+'Test5'+i, 
            Location_Hours__c=hrs[i].id);
            avas.add(awl);
        }
        insert avas;
        
        WFM_EE_Non_Availability_Detail__c enavl=new WFM_EE_Non_Availability_Detail__c(Employee_ID__c=empl.id,End_Date__c=enddate,Start_Date__c=startdate,Type__c='PTO',Unit__c='FTE',Value__c=0.5);
        insert enavl;
        
        WFM_EE_Non_Availability_Detail__c enavl1=new WFM_EE_Non_Availability_Detail__c(Employee_ID__c=empl.id,End_Date__c=enddate,Start_Date__c=startdate,Type__c='PTO',Unit__c='Hours',Value__c=8);
        insert enavl1;
        
        WFM_EE_Non_Availability_Detail__c enavl2=new WFM_EE_Non_Availability_Detail__c(Employee_ID__c=empl.id,End_Date__c=enddate,Start_Date__c=startdate,Type__c='Pre-Starter',Unit__c='FTE',Value__c=0.5);
        insert enavl2;
        
        WFM_EE_Non_Availability_Detail__c enavl3=new WFM_EE_Non_Availability_Detail__c(Employee_ID__c=empl.id,End_Date__c=enddate,Start_Date__c=startdate,Type__c='Training',Unit__c='FTE',Value__c=0.5);
        insert enavl3;
        
        
      PageReference pageRef = Page.RM_AdjustAvailability;
      Test.setCurrentPage(pageRef);
      ApexPages.currentPage().getParameters().put('id', empl.id);     
      ApexPages.currentPage().getParameters().put('itemidDel',enavl1.id);
      RM_AdjustAvailabilityController ctrl=new RM_AdjustAvailabilityController();
      ctrl.delDetail();  
      ctrl.detailId =enavl.id;
      ctrl.datefilter='02/01/2014';
      ctrl.datefilter1='12/31/2015';  
      ctrl.setType='PTO';     
      ctrl.hoursval='8';
      ctrl.saveDetail();
           
      ctrl.gettypeOptions();
      ctrl.getHoursOptions();
      ctrl.Cancel();        
      ctrl.switchHr();      
      ctrl.dateRange(startDate,endDate);   
      ctrl.editAva();          
      ctrl.CancelAva();
      ctrl.isSwitch=true;
      ctrl.SaveAva();     
    }    
    
}