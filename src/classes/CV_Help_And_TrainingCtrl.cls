public with sharing class CV_Help_And_TrainingCtrl {
    
    private final List<Help_Record__c> helpRecords;
    
    public CV_Help_And_TrainingCtrl(){
        System.debug('url: ' + ApexPages.currentPage().getUrl());
        helpRecords = CV_Utils.selectHelpRecords(true);
    }
    
    public List<SelectOption> getHelpItems(){
        return CV_Utils.getHelpItems(helpRecords, true);
    }
    
    public Boolean getShowMore(){
        return helpRecords.size() > CV_Utils.MAX_ITEM_NUMBER_IN_MENU;
    }
    
    public static String getOriginalPageEncodedUrl(){
        System.debug('original encoded url: ' + CV_Utils.getOriginalPageEncodedUrl());
        return CV_Utils.getOriginalPageEncodedUrl();
    }
    
    public static String getRecordIdFromUrl(){
        return CV_Utils.getRecordIdFromUrl();
    }

}