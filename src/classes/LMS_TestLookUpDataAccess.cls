/**
 * Test to cover lookup data accessor
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestLookUpDataAccess {

    static List<Job_Class_Desc__c> jobClasses;
    static List<Job_Title__c> jobTitles;
    static List<PRA_Business_Unit__c> businessUnits;
    static List<Department__c> departments;
    static List<Employee_Type__c> empTypes;
    static List<Region__c> regions;
    static List<Country__c> countries;
    
    static void init(){
    	jobClasses = new Job_Class_Desc__c[]{
			new Job_Class_Desc__c(Name = 'class1', Job_Class_Code__c = 'code1', Job_Class_ExtID__c = 'class1', Is_Assign__c = true),
			new Job_Class_Desc__c(Name = 'class2', Job_Class_Code__c = 'code2', Job_Class_ExtID__c = 'class2', IsClinical__c = true),
			new Job_Class_Desc__c(Name = 'class3', Job_Class_Code__c = 'code3', Job_Class_ExtID__c = 'class3')
		};
		insert jobClasses;
		
		jobTitles = new Job_Title__c[]{
			new Job_Title__c(Job_Title__c = 'title1', Job_Code__c = 'code1', Job_Class_Desc__c = jobClasses[0].Id, Status__c = 'A'),
			new Job_Title__c(Job_Title__c = 'title2', Job_Code__c = 'code2', Job_Class_Desc__c = jobClasses[1].Id, Status__c = 'A'),
			new Job_Title__c(Job_Title__c = 'title3', Job_Code__c = 'code3', Job_Class_Desc__c = jobClasses[1].Id, Status__c = 'A'),
			new Job_Title__c(Job_Title__c = 'title4', Job_Code__c = 'code4', Job_Class_Desc__c = jobClasses[2].Id, Status__c = 'A')
		};
		insert jobTitles;
		
		businessUnits = new PRA_Business_Unit__c[]{
			new PRA_Business_Unit__c(Name = 'unit', Business_Unit_Code__c = 'BUC', Status__c = 'A')
		};
		insert businessUnits;
		
		departments = new Department__c[]{
			new Department__c(Name = 'department', Department_Code__c = 'DEPTA', Status__c = 'A')
		};
		insert departments;	
		
		regions = new Region__c[]{
			new Region__c(Region_Name__c = 'region1', Region_Id__c = 1, Status__c = 'A'),
			new Region__c(Region_Name__c = 'region2', Region_Id__c = 2, Status__c = 'A')
		};
		insert regions;
		
		countries = new Country__c[]{
			new Country__c(Name = 'country1', Region_Name__c = 'region1', Country_Code__c = 'C1', PRA_Country_ID__c = '100'),
			new Country__c(Name = 'country2', Region_Name__c = 'region2', Country_Code__c = 'C2', PRA_Country_ID__c = '200')
		};
		insert countries;
		
		empTypes = new Employee_Type__c[]{
			new Employee_Type__c(Name = 'type1')
		};
		insert empTypes;
		
    }
    
    static testMethod void constructor(){
    	LMS_LookUpDataAccess c = new LMS_LookUpDataAccess();
    }
    
    static testMethod void testGetRoles(){
    	init();
    	List<SelectOption> options = LMS_LookUpDataAccess.getRoles(false, false);
    	System.assert(options.size() == 3 && options[0].getValue() == jobClasses[0].Name && 
    				  options[1].getValue() == jobClasses[1].Name && options[2].getValue() == jobClasses[2].Name);
    	
    	options = LMS_LookUpDataAccess.getRoles(true, false);
    	System.assert(options.size() == 1 && options[0].getValue() == jobClasses[0].Name);
    	
    	options = LMS_LookUpDataAccess.getRoles(false, true);
    	System.assert(options.size() == 4 && options[0].getValue() == '' && options[1].getValue() == jobClasses[0].Name && 
    				  options[2].getValue() == jobClasses[1].Name && options[3].getValue() == jobClasses[2].Name);
    	
    	options = LMS_LookUpDataAccess.getRoles(true, true);
    	System.assert(options.size() == 2 && options[0].getValue() == '' && options[1].getValue() == jobClasses[0].Name);
    }
    
    static testMethod void testGetClinicalRoles(){
    	init();
    	List<SelectOption> options = LMS_LookUpDataAccess.getClinicalRoles(false, false);
    	System.assert(options.size() == 3 && options[0].getValue() == jobClasses[0].Name && 
    				  options[1].getValue() == jobClasses[1].Name && options[2].getValue() == jobClasses[2].Name);
    	
    	options = LMS_LookUpDataAccess.getClinicalRoles(true, false);
    	System.assert(options.size() == 1 && options[0].getValue() == jobClasses[1].Name);
    	
    	options = LMS_LookUpDataAccess.getClinicalRoles(false, true);
    	System.assert(options.size() == 4 && options[0].getValue() == '' && options[1].getValue() == jobClasses[0].Name && 
    				  options[2].getValue() == jobClasses[1].Name && options[3].getValue() == jobClasses[2].Name);
    	
    	options = LMS_LookUpDataAccess.getClinicalRoles(true, true);
    	System.assert(options.size() == 2 && options[0].getValue() == '' && options[1].getValue() == jobClasses[1].Name);
    }
    
    static testMethod void testGetTitles(){
    	init();
    	List<SelectOption> options = LMS_LookUpDataAccess.getTitles(false);
    	System.assert(options.size() == 4 && options[0].getValue() == jobTitles[0].Job_Title__c && options[1].getValue() == jobTitles[1].Job_Title__c &&
    				  options[2].getValue() == jobTitles[2].Job_Title__c && options[3].getValue() == jobTitles[3].Job_Title__c);
    	
    	options = LMS_LookUpDataAccess.getTitles(true);
    	System.assert(options.size() == 5 && options[0].getValue() == '' && options[1].getValue() == jobTitles[0].Job_Title__c && 
    				  options[2].getValue() == jobTitles[1].Job_Title__c && options[3].getValue() == jobTitles[2].Job_Title__c && 
    				  options[4].getValue() == jobTitles[3].Job_Title__c);
    }
    
    static testMethod void testGetBusinessUnits(){
    	init();
    	List<SelectOption> options = LMS_LookUpDataAccess.getBusinessUnits(false);
    	System.assert(options.size() == 1 && options[0].getValue() == businessUnits[0].Name);
    	    	
    	options = LMS_LookUpDataAccess.getBusinessUnits(true);
    	System.assert(options.size() == 2 && options[0].getValue() == '' && options[1].getValue() == businessUnits[0].Name);
    }
    
    static testMethod void testGetDepartments(){
    	init();
    	List<SelectOption> options = LMS_LookUpDataAccess.getDepartments(false);
    	System.assert(options.size() == 1 && options[0].getValue() == departments[0].Name);
    	    	
    	options = LMS_LookUpDataAccess.getDepartments(true);
    	System.assert(options.size() == 2 && options[0].getValue() == '' && options[1].getValue() == departments[0].Name);
    }
    
    static testMethod void testGetStatusGroups(){
    	init();
    	List<SelectOption> options = LMS_LookUpDataAccess.getStatusGroups(false);
    	System.assert(options.size() == 1 && options[0].getValue() == empTypes[0].Name);
    	
    	options = LMS_LookUpDataAccess.getStatusGroups(true);
    	System.assert(options.size() == 2 && options[0].getValue() == '' && options[1].getValue() == empTypes[0].Name);
    }
    
    static testMethod void testGetRegion(){
    	init();
    	List<SelectOption> options = LMS_LookUpDataAccess.getRegion(false);
    	System.assert(options.size() == 2 && options[0].getValue() == regions[0].Region_Name__c && options[1].getValue() == regions[1].Region_Name__c);
    	
    	options = LMS_LookUpDataAccess.getRegion(true);
    	System.assert(options.size() == 3 && options[0].getValue() == '' && options[1].getValue() == regions[0].Region_Name__c && 
    				  options[2].getValue() == regions[1].Region_Name__c);
    }
    
    static testMethod void testGetCountry(){
    	init();
    	List<SelectOption> options = LMS_LookUpDataAccess.getCountry(false);
    	System.assert(options.size() == 2 && options[0].getValue() == countries[0].Name);
    	
    	options = LMS_LookUpDataAccess.getCountry(true);
    	System.assert(options.size() == 3 && options[0].getValue() == '' && options[1].getValue() == countries[0].Name && 
    				  options[2].getValue() == countries[1].Name);
    }
    
}