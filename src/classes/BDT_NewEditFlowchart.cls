public class BDT_NewEditFlowchart{

	public BDT_SecuritiesService.pageSecurityWrapper 	securitiesA 		{get;set;} // Flowchart Setup
	public list<ClinicalDesign__c> 						designList 			{get;set;}
	private BDT_UserPreferenceService 					userPreferences 	{get;set;}
	public ClinicalDesignService.designDisplay			designDisplay		{get;set;}
	public FlowchartService.flowchartDisplay			flowchartDisplay	{get;set;}
	public FlowchartService.servicecategoriesForFlowcharts	serviceCategoryDisplay	{get;set;}
	public integer categoryDisplayed {get;set;}
	public list<FlowchartService.serviceDetail>			servicedetailInCategoryList {get;set;}
	public ID 											FlowchartId			{get;set;} // for apex:actionFunction
	
	public integer AddTimepointStartDay {get;set;}
	public integer AddTimepointStartHour {get;set;}
	public integer AddTimepointStartMinute {get;set;}
	public integer AddTimepointIntervalDay {get;set;}
	public integer AddTimepointIntervalHour {get;set;}
	public integer AddTimepointIntervalMinute {get;set;}
	public integer AddTimepointEndDay {get;set;}
	public integer AddTimepointEndHour {get;set;}
	public integer AddTimepointEndMinute {get;set;}
	public integer AddTimepointTotal {get;set;}
	public string StatusMessage {get;set;}
	public string TimePointsToDelete {get;set;}

	public integer displayState {get;set;}  
		// 0 - some data was migrated for version 4.6, user should reload screen.
		// 1 - show designlist and designdiagram
		// 2 - show flowchart
		// 3 - show timepoints
		// 4 - show services
		// 5 - show flowchart copy search part
		// 6 - show flowchart copy 


	public BDT_NewEditFlowchart() {
		// load securities
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Flowchart Setup');
		if (!securitiesA.getReadPriv()) {
			// user has no privileges
			return;
		}
		
		// get user preferences
		userPreferences = new BDT_UserPreferenceService();

		init();
		
		// if page was loaded with flowchart parameter the load the flowchart data and switch to the timepoint display
		if (displayState <> 0) { // if there is a upgrade needed then do not switch to flowchart
			try {
				FlowchartId = ID.valueOf(system.currentPageReference().getParameters().get('FlowchartId'));
				readFlowchart();
			} catch (Exception e) {
				// there was an issue with the ID, then normal page is displayed.
			}
		}
		
	}
		
	public void init () {
		
		// temporary routine for version 4.6 to recode timepoint__c into flowcharts__c after
		// object timepoint__c is removed the code can be savely removed
		if (FlowchartService.RecodeOldData (ID.valueOf(userPreferences.getPreference(BDT_UserPreferenceDataAccessor.PROJECT)),true)) {
			// some data was migrated, user should reload screen
			displayState = 0;
			return;
		}
		
		// initial display state is to show designlist and designdiagram
		displayState = 1;
		// load design list for users current project
		designList = ClinicalDesignService.getClinicalDesignListForProject(
			userPreferences.getPreference(BDT_UserPreferenceDataAccessor.PROJECT)
		);
	}
	
	public void MigrateData() {
		// temporary routine for version 4.6 to recode timepoint__c into flowcharts__c after
		// object timepoint__c is removed the code can be savely removed
		Boolean changes = FlowchartService.RecodeOldData (ID.valueOf(userPreferences.getPreference(BDT_UserPreferenceDataAccessor.PROJECT)),false);
		init ();
	}

	public void readClinicalDesign() {
		displayState = 1;
		String designId = system.currentPageReference().getParameters().get('DesignId');
		designDisplay = new ClinicalDesignService.designDisplay(designId);
	}
	
	public void readFlowchart() {
		displayState = 2;
		
		flowchartDisplay = new FlowchartService.flowchartDisplay(
			FlowchartId,
			userPreferences.getPreference(BDT_UserPreferenceDataAccessor.PROJECT),
			designDisplay.design.id
		);
		
		// clean out design data to minimize viewstate size
		designDisplay = null;
	}
	
	public void showTimepoints() {
		displayState = 3;
		StatusMessage = null;
		AddTimepointStartDay = 1;
		AddTimepointStartHour = 0;
		AddTimepointStartMinute = 0;
		AddTimepointIntervalDay = null;
		AddTimepointIntervalHour = null;
		AddTimepointIntervalMinute = null;
		AddTimepointEndDay = null;
		AddTimepointEndHour = null;
		AddTimepointEndMinute = null;
		AddTimepointTotal = 1;
		flowchartDisplay.checkTimePointsUsed();
	}


	public integer convertToMinutes(Integer Days, Integer Hours, Integer Minutes) {
		
		Integer result = 0;
		result += Minutes==null?0:Minutes;
		result += (Hours==null?0:Hours)*60;
		result += (Days==null?0:Days)*1440;
		
		return result;
		
	}


	public void createTimepointsByRepeats() {
		
		
		StatusMessage = null;
		
		integer safetyCutOff = 100;
		
		// transform user input to list of minutes
		list<integer> minutes = new list<integer>();
		
		// get start time
		integer startTime = convertToMinutes(
			fixDayNumInput(AddTimepointStartDay), 
			AddTimepointStartHour, 
			AddTimepointStartMinute); 
			
		// get interval
		integer interval = convertToMinutes(
			AddTimepointIntervalDay, 
			AddTimepointIntervalHour, 
			AddTimepointIntervalMinute); 
		
		// get repeats
		integer repeats = AddTimepointTotal; 

		for (Integer i = 0 ; i < safetyCutOff && i < repeats ; i ++ ) {
			minutes.add(startTime + (i * interval));
		}
		
		if (safetyCutOff < repeats ) {
			StatusMessage = 'Only ' + safetyCutOff + ' out of the ' + repeats;
			StatusMessage += ' specified timepoints can be created. Further timepoints need to be created starting';
			StatusMessage += ' at Day: 1 Hour: 0 Minute: ' + (startTime + (interval*safetyCutOff)) + ' .';
		} 
		
		
		flowchartDisplay.addTimePoints(minutes);
		
	}

	public integer fixDayNumInput (Integer day) {
		return (day == null || day == 0 )?0:day-1;
	}

	public void createTimepointsByEndTime() {
		
		
		StatusMessage = null;
		
		integer safetyCutOff = 100;
		
		// transform user input to list of minutes
		list<integer> minutes = new list<integer>();
		
		// get start time
		integer startTime = convertToMinutes(
			fixDayNumInput(AddTimepointStartDay), 
			AddTimepointStartHour, 
			AddTimepointStartMinute); 

		// get interval
		integer interval = convertToMinutes(
			math.abs(AddTimepointIntervalDay), 
			math.abs(AddTimepointIntervalHour), 
			math.abs(AddTimepointIntervalMinute));

		// get end time
		integer endTime = convertToMinutes(
			fixDayNumInput(AddTimepointEndDay), 
			AddTimepointEndHour, 
			AddTimepointEndMinute); 

		if (endTime<startTime) {
			integer temp = endTime;
			endTime = startTime;
			startTime = temp;
		}

		for (Integer i = 0 ; i < safetyCutOff && startTime + (i * interval) <= endTime ; i ++ ) {
			minutes.add(startTime + (i * interval));
		}
		
		if (safetyCutOff < ((endTime - startTime)/interval) ) {
			StatusMessage = 'Only ' + safetyCutOff + ' out of the ' + Integer.valueOf(Math.Floor(((endTime - startTime)/interval)));
			StatusMessage += ' specified timepoints can be created. Further timepoints need to be created starting';
			StatusMessage += ' at Day: 1 Hour: 0 Minute: ' + (startTime + (interval*safetyCutOff)) + ' .';
		} 

		flowchartDisplay.addTimePoints(minutes);
		
	}
	
	public void returnToFlowchart() {
		StatusMessage = null;
		serviceCategoryDisplay = null;
		servicedetailInCategoryList = null;
		categoryDisplayed = null;
		copyFlowchart = null;
		displayState = 2;
	}
	
	
	public void deleteAllUnusedTimePoints () {
		flowchartDisplay.deleteUnusedTimepoints ();
	}
	
	public void deleteTimePoint() {
		list<Integer> minutesToBeDeleted = new list<integer>();
		try {
			minutesToBeDeleted.add(integer.valueOf(TimePointsToDelete));
		} catch (Exception e) {
			return; // invalid integer, skip delete routine
		}
		
		flowchartDisplay.removeTimePoints(minutesToBeDeleted);
	}
	
	public void showCategoryServiceSelection () {
		serviceCategoryDisplay = new FlowchartService.servicecategoriesForFlowcharts(flowchartDisplay);
		servicedetailInCategoryList = null;
		categoryDisplayed = null;
		displayState = 4;
	}

	public void showServiceInCategory () {
		categoryDisplayed = integer.valueOf(system.currentPageReference().getParameters().get('CategoryNumber'));
		servicedetailInCategoryList = serviceCategoryDisplay.categoryDetailList[categoryDisplayed].serviceDetailList;
	}
	
	public void removeService() {
		integer serviceNumber = integer.valueOf(system.currentPageReference().getParameters().get('ServiceNumber'));
		serviceCategoryDisplay.categoryDetailList[categoryDisplayed].deselectService (serviceNumber,flowchartDisplay);
	}
	
	public void addService() {
		integer serviceNumber = integer.valueOf(system.currentPageReference().getParameters().get('ServiceNumber'));
		serviceCategoryDisplay.categoryDetailList[categoryDisplayed].selectService (serviceNumber,flowchartDisplay);
	}

	
	public void save() {
		flowchartDisplay.saveFlowchartAndProjectServiceTimepointChanges();
		cancel();
	}
	
	public void cancel(){
		StatusMessage = null;
		serviceCategoryDisplay = null;
		servicedetailInCategoryList = null;
		categoryDisplayed = null;
		flowchartDisplay = null;
		copyFlowchart = null;
		
		// reload design data
		init();
		displayState = 1;
		readClinicalDesign();
	}

	public FlowchartService.CopyFlowchart copyFlowchart {get;set;}

	public void showCopyFlowchart () {
		displayState = 5;
		copyFlowchart = new FlowchartService.CopyFlowchart(flowchartDisplay);
	}

	public void showCopyFlowchartOverview () {
		displayState = 6;
		copyFlowchart.prepareFlowchartCopyDataList();
	}

	public void copyFlowchart () {
		copyFlowchart.copyFlowChart ();
		returnToFlowchart();
		
	}

}