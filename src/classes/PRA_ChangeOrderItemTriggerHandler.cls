public class PRA_ChangeOrderItemTriggerHandler {
    
    public void onAfterUpdate (Map<Id, Change_Order_Item__c> mapOldCOItem, Map<Id, Change_Order_Item__c> mapNewCOItem) {
        //List <Change_Order_Item__c> liUpdatedCOItems = new List <Change_Order_Item__c> ();
        Set<Id> coItemIds = new Set<Id>();
        Map <Id, Double> changeAmmendmentNoUnits = new Map <Id, Double> ();
        
        for(Id key : mapNewCOItem.keySet()) {
            if(!mapOldCOItem.containsKey(key) || (mapOldCOItem.get(key).Amendment_CO_Number_of_Units__c != mapNewCOItem.get(key).Amendment_CO_Number_of_Units__c)
                    || (mapOldCOItem.get(key).Revised_Number_of_Units__c != mapNewCOItem.get(key).Revised_Number_of_Units__c)){
                    //|| (mapOldCOItem.get(key).Is_Labor_Unit__c != mapNewCOItem.get(key).Is_Labor_Unit__c)){
                coItemIds.add(key);
                if(mapOldCOItem.get(key).Amendment_CO_Number_of_Units__c != mapNewCOItem.get(key).Amendment_CO_Number_of_Units__c){ 
                  //  mapNewCOItem.get(key).Is_Labor_Unit__c == true) {
                    Double oldVal = (mapOldCOItem.get(key).Amendment_CO_Number_of_Units__c != null ? mapOldCOItem.get(key).Amendment_CO_Number_of_Units__c : 0);
                    Double newVal = (mapNewCOItem.get(key).Amendment_CO_Number_of_Units__c != null ? mapNewCOItem.get(key).Amendment_CO_Number_of_Units__c : 0);
                    changeAmmendmentNoUnits.put(key, newVal-oldVal);
                }
            }
        }
        if(coItemIds.size() > 0) {
            calculateRevisedRollUp(coItemIds);
        }
        if(changeAmmendmentNoUnits.size() > 0) {
            updateAmmendmentLineItemHours(changeAmmendmentNoUnits);
        }
        
    }
    
    public void changeOrderLineItemOnAfterInsert(List<Change_Order_Line_Item__c> liNewCOLineItems) {
        Set<Id> coItemIds = new Set<Id>();
        
        for(Change_Order_Line_Item__c coLineItem : liNewCOLineItems) {
        	//if (coLineItem.Is_Labor_Unit__c)
            	coItemIds.add(coLineItem.Change_Order_Item__c);
        }
        
        if(coItemIds.size() > 0) {
            calculateRevisedRollUp(coItemIds);
        }
        
    }
    
    public void changeOrderLineItemOnAfterUpdate(Map<Id, Change_Order_Line_Item__c> mapOldCOLineItems, Map<Id, Change_Order_Line_Item__c> mapNewCOLineItems) {
        
        Set<Id> coItemIds = new Set<Id>();
        for(Id key : mapNewCOLineItems.keySet()) {
            if((!mapOldCOLineItems.containsKey(key)) 
                || (mapOldCOLineItems.get(key).Revised_Unit_Cost__c != mapNewCOLineItems.get(key).Revised_Unit_Cost__c) 
                || (mapOldCOLineItems.get(key).Revised_Total_Cost__c != mapNewCOLineItems.get(key).Revised_Total_Cost__c) 
        		|| (mapOldCOLineItems.get(key).Is_Labor_Unit__c != mapNewCOLineItems.get(key).Is_Labor_Unit__c)) {
                coItemIds.add(mapNewCOLineItems.get(key).Change_Order_Item__c);
            }
        }
        
        if(coItemIds.size() > 0) {
            calculateRevisedRollUp(coItemIds);
        }
        
    }
    
    public void changeOrderLineItemOnAfterDelete(List<Change_Order_Line_Item__c> liCOLineItems) {
        Set<Id> coItemIds = new Set<Id>();
        
        for(Change_Order_Line_Item__c coLineItem : liCOLineItems) {
        	//if (coLineItem.Is_Labor_Unit__c)
            	coItemIds.add(coLineItem.Change_Order_Item__c);
        }
        
        if(coItemIds.size() > 0) {
            calculateRevisedRollUp(coItemIds);
        }
    }
    
    public void calculateRevisedRollUp(Set<Id> setCOItem) {
        system.debug('start calculateRevisedRollUp');
        Map<Id, Change_Order_Item__c> mapCOItemWithLineItems =  new Map<Id, Change_Order_Item__c>([select Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Number_Of_Contracted_Units__c,
                (select Change_Order_Item__c,Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, Amendment_CO_Hours_unit__c, BUF_Code__c,
                        Current_Contract_Hours__c, Current_Contract_Value__c, Id, Revised_Unit_Cost__c, Revised_Total_Cost__c
                        from Change_Order_Line_Item__r)// where Is_Labor_Unit__c = true)
            from Change_Order_Item__c
            where id in :setCOItem]);
        
        for(Change_Order_Item__c updatedCOItem : mapCOItemWithLineItems.values()) {
            Change_Order_Item__c coItem = mapCOItemWithLineItems.get(updatedCOItem.Id);
            Decimal sumUnitCost = 0;
            Decimal sumTotalCost = 0;
            
            
            if(coItem != null) {
                for(Change_Order_Line_Item__c coLineItem : coItem.Change_Order_Line_Item__r) {
                    if(coLineItem.Revised_Unit_Cost__c <> null) {
                        sumUnitCost += coLineItem.Revised_Unit_Cost__c;
                    }
                    if(coLineItem.Revised_Total_Cost__c <> null) {
                        sumTotalCost += coLineItem.Revised_Total_Cost__c;
                    }
                }
                updatedCOItem.Revised_Unit_Cost__c = sumUnitCost;
                updatedCOItem.Revised_Total_Cost__c = sumTotalCost;
            }
            
        }
        update mapCOItemWithLineItems.values();
    }
    
    // Update Ammendment Hours by number of changed order item number of units 
    public void updateAmmendmentLineItemHours(Map<Id, Double> changeAmmendmentNoUnits) {
        List<Change_Order_Line_Item__c> updatedCOLineItems = [select Amendment_CO_Hours__c,Is_Labor_Unit__c, Current_Hours_Unit__c, Id,Change_Order_Item__c
                                                                from Change_Order_Line_Item__c
                                                                where Change_Order_Item__c in :changeAmmendmentNoUnits.keySet() ];
        if (updatedCOLineItems.size() > 0){            
            for(Change_Order_Line_Item__c coLineItem : updatedCOLineItems) {                    
                if(coLineItem.Is_Labor_Unit__c == true){
                    coLineItem.Amendment_CO_Hours__c = changeAmmendmentNoUnits.get(coLineItem.Change_Order_Item__c) * coLineItem.Current_Hours_Unit__c
                                                        + coLineItem.Amendment_CO_Hours__c;
                }
            }
	        update updatedCOLineItems;
        }
    }

}