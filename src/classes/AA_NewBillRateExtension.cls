/**
@author Niharika Reddy
@date 2015
@description this is the controller for new bill rate page
*/
public class AA_NewBillRateExtension {
// Instance fields
    public String searchTerm {get; set;}
    
    private final List<Currency__c> currencies;
    public String  jobPosition            { get; set; }
    public String selectedCountry         { get; set; }
    //public Bill_Rate__c  billRate {get;set;}
    
    public String selectedCurrency{get;set;}
    public String yearValue{get;set;}
    public Decimal initialRateValue{get;set;}
    public Bill_Rate__c rate{get;set;}
    private Boolean error = false;
    Set<String> uniqueBillRateValues{ get; set; }
    public AA_NewBillRateExtension() {}
    public Bill_Rate_Card_Model__c model{get; set;}
    
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this is the constructor for new bill rate 
    */
    public AA_NewBillRateExtension(ApexPages.StandardController controller) {
        yearValue ='2000';
        uniqueBillRateValues = new Set<String>();
        rate = (Bill_Rate__c)controller.getRecord();
        model = AA_BillRateDataAccessor.getModelById(rate.Model_Id__c)[0];
        
        //Bill_Rate_Card_Model__c model = [select Id, Name, Year__c from Bill_Rate_Card_Model__c where Id=: rate.Model_Id__c limit 1];
        rate.Schedule_Year__c = model.Year__c;
        
        currencies = AA_BillRateDataAccessor.getCurrencies(); //[SELECT  Id,Name FROM Currency__c WHERE NAME='USD' or Name='EUR' order by Name];
        List<Bill_Rate__c> billRates = AA_BillRateDataAccessor.getBillRateBasedOnModelId(rate.Model_Id__c);
        /* List<Bill_Rate__c> billrates = [SELECT Id,
                                                 Year_Rate_Applies_To__c,
                                                 Schedule_Year__c,
                                                 Bill_Rate__c,
                                                 Currency__c,
                                                 Currency__r.Name,
                                                 CreatedBy.Name,
                                                 CreatedDate,
                                                 LastModifiedBy.Name,
                                                 LastModifiedDate,
                                                 IsActive__c,
                                                 country__c,
                                                 country__r.Name,
                                                 Job_Position__c,
                                                 Job_Position__r.Name,
                                                 Model_Id__c,
                                                 Model_Id__r.Status__c
                                          FROM   Bill_Rate__c 
                                          WHERE Model_Id__c =: rate.Model_Id__c];
        */
        if(!billRates.isEmpty()){
            for(Bill_Rate__c rt: billrates){
                uniqueBillRateValues.add((rt.Model_Id__c + '' + rt.Job_Position__c +'' + rt.Country__c +'' + rt.Schedule_Year__c + rt.Year_Rate_Applies_To__c + ''));
            }
        }
        
    }
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this method hold the logic for crete button
    */
    public PageReference Create() {
        
            /* Catch invalid Year */
            try{
            if(Integer.valueOf(rate.Schedule_Year__c) < 1999 || Integer.valueOf(rate.Schedule_Year__c) > Integer.valueOf(Date.Today().Year() + 20)) {
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Year : '+ rate.Schedule_Year__c + '. Must be between 1999 and ' + String.valueOf(Date.Today().Year() + 20));
                ApexPages.addMessage(errormsg);
                return null;
            }
            }catch(Exception e){
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Year : '+ rate.Schedule_Year__c);
                ApexPages.addMessage(errormsg);
                return null;
            }
            
            /* Catch invalid Rate*/
            try{
                Integer d = Integer.valueOf(rate.Bill_Rate__c);
                if(d < 0){
                    error = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Bill rate must be greater than 0'));
                    return null;
                }
            }catch(Exception e){
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Rate : '+ rate.Bill_Rate__c);
                ApexPages.addMessage(errormsg);
                return null;
            }
            
            if(!error){
                     String response = createNewRecords();
                     if(response.equals('SUCCESS')){
                         PageReference pageRef = Page.AA_BillRate_InflationOverride; 
                         pageRef.getParameters().put('jobPosition',rate.Job_Position__c);
                         pageRef.getParameters().put('country',rate.Country__c);
                         pageRef.getParameters().put('scheduledYear',rate.Schedule_Year__c);
                         pageRef.getParameters().put('modelId',rate.Model_Id__c);
                         return pageRef ;
                     }
                     else{
                         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,' Please select unique combination for Country, Job Position and Scheduled year'));
                         return null;
                     }
                
            }else
             return null;
    }

    /**
    @author Niharika Reddy
    @date 2015
    @description this method create the bill rate values for next 15 years
    */
    public String createNewRecords(){
       String response = 'SUCCESS';
        try{
            rate.Year_Rate_Applies_To__c = rate.Schedule_Year__c;
            rate.IsActive__c = true;
            System.debug('*** uniqueBillRateValues *** '+uniqueBillRateValues);
            response = generateBillRates(rate, uniqueBillRateValues);
           }
        catch(System.DMLException de){
            if(de.getDMLType(0).equals(StatusCode.DUPLICATE_VALUE)){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,' Please select unique combination for Country, Job Position and Scheduled year'));
                response = 'ERROR';
            }
        }
        
        return response;
    }
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this method create the bill rate records
    */
    public static String generateBillRates(Bill_Rate__c rate, Set<String> uniqueBillRateValues){
        List<Bill_Rate__c> billRateList = new List<Bill_Rate__c>();
        
        System.debug('*** unique *** '+(rate.Model_Id__c + '' + rate.Job_Position__c +'' + rate.Country__c +'' + rate.Schedule_Year__c + rate.Year_Rate_Applies_To__c + ''));
        
        if(!checkUnique(rate, uniqueBillRateValues)){
            billRateList.add(rate);
            
        }
        else{
            return 'ERROR';
        }
        
        Integer tempYear = Integer.valueOf(rate.Year_Rate_Applies_To__c);
        
        String response = 'SUCCESS';
        for (integer i=0;i<15;i++){
            Bill_Rate__c tempRate = new Bill_Rate__c ();
            
            tempRate.Bill_Rate__c = rate.Bill_Rate__c;
            tempRate.Schedule_Year__c = rate.Schedule_Year__c;
            tempYear = tempYear + 1;
            tempRate.Year_Rate_Applies_To__c =  String.valueOf(tempYear) ;
            tempRate.Currency__c = rate.Currency__c;
            tempRate.Country__c = rate.Country__c;
            tempRate.Job_Position__c = rate.Job_Position__c;
            tempRate.Model_Id__c = rate.Model_Id__c;
            tempRate.IsActive__c = true;
            
            if(!checkUnique(tempRate, uniqueBillRateValues)){
                billRateList.add(tempRate);
            }
            else{
                response = 'ERROR';
                return response;
            }
            
        }
        insert billRateList;
        return response;
    }
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this method redirects user to the bill rate's model page
    */
    public PageReference cancel(){
        PageReference ref = new PageReference('/'+rate.Model_Id__c);
        ref.setRedirect(true);
        return ref;
    }
     
    /**
    @author Niharika Reddy
    @date 2015
    @description this method checks for unique bill rate values
    */
     public static Boolean checkUnique(Bill_Rate__c rate, Set<String> uniqueBillRateValues){
        if(uniqueBillRateValues.contains((rate.Model_Id__c + '' + rate.Job_Position__c +'' + rate.Country__c +'' + rate.Schedule_Year__c + rate.Year_Rate_Applies_To__c + ''))){
            return true;
        }
        else{
            return false;
        }
        return false;
    }
}