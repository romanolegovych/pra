/** Implements the test for the Selector Layer of the object FinancialDocumentApproval__c
 * @author	Dimitrios Sgourdos
 * @version	17-Jan-2014
 */
@isTest
private class FinancialDocumentApprovalDataAccessTest {
	
	/** Test the function getFinancialDocumentApprovals
	 * @author	Dimitrios Sgourdos
	 * @version	17-Jan-2014
	 */
	static testMethod void getFinancialDocumentApprovalsTest() {
		// Create data
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001',
																				'BDT Project First Experiments');
		insert currentProject;
	
		FinancialDocument__c financialDocument = new FinancialDocument__c();
		financialDocument.DocumentType__c	= 'Proposal';
		financialDocument.Client_Project__c	= currentProject.Id; 
		insert financialDocument;
		
		List<FinancialDocumentApproval__c> initialList = new List<FinancialDocumentApproval__c>();
		initialList.add(new FinancialDocumentApproval__c(Name = 'Test 2', FinancialDocument__c = financialDocument.Id));
		initialList.add(new FinancialDocumentApproval__c(Name = 'Test 1', FinancialDocument__c = financialDocument.Id));
		insert initialList;
		
		// Check the function
		String whereClause = 'FinancialDocument__c = ' + '\'' + financialDocument.Id + '\'';
		List<FinancialDocumentApproval__c> results =
												FinancialDocumentApprovalDataAccessor.getFinancialDocumentApprovals(
																								whereClause,
																								'Name');
																							
		String errorMessage = 'Error in reading the financial document approvals from the system';
		system.assertEquals(results.size(), 2, errorMessage);
		system.assertEquals(results[0].Id, initialList[1].Id, errorMessage);
		system.assertEquals(results[1].Id, initialList[0].Id, errorMessage);
	}
	
	
	/** Test the function getPlenboxEmployees
	 * @author	Dimitrios Sgourdos
	 * @version	10-Jan-2014
	 */
	static testMethod void getPlenboxEmployeesTest() {
		// Create data
		Country__c country = new Country__c (Name='TestCountry');
		insert country;
		
		List<Employee_Details__c> employeesList = new List<Employee_Details__c>();
		employeesList.add( new Employee_Details__c(Business_Unit__c = 'PRA',
												Country_Name__c = country.id,
												Date_Hired__c = date.today(),
												Employee_Unique_Key__c = '1111',
												First_Name__c='TestFirstName1',
												Last_Name__c='TestLastName1',
												Job_Class_Desc__c='TestJob',
												Status__c='AA',
												Email_Address__c='SelectedEmail@praIntl.com') );
		employeesList.add( new Employee_Details__c(Business_Unit__c = 'PRA',
												Country_Name__c = country.id,
												Date_Hired__c = date.today(),
												Employee_Unique_Key__c = '2222',
												First_Name__c='TestFirstName2',
												Last_Name__c='TestLastName2',
												Job_Class_Desc__c='TestJob',
												Status__c='AA',
												Email_Address__c='NotSelectedEmail@praIntl.com') );
		insert employeesList;
		
		List<User> plenboxUsers = new List<User>();
		plenboxUsers.add( new User(Email = 'SelectedEmail@praIntl.com') );
		plenboxUsers.add( new User(Email = 'NotFoundEmail@praIntl.com') );
		
		// Check the function
		List<Employee_Details__c> results = FinancialDocumentApprovalDataAccessor.getPlenboxEmployees(plenboxUsers,
																									'Email_Address__c');
		
		String errorMessage = 'Error in reading the Employees data from the system';
		system.assertEquals(results.size(), 1, errorMessage);
		system.assertEquals(results[0].Id, employeesList[0].Id, errorMessage);
	}
	
	
	/** Test the function getEmployeeByNameAndEmail
	 * @author	Dimitrios Sgourdos
	 * @version	13-Jan-2014
	 */
	static testMethod void getEmployeeByNameAndEmailTest() {
		// Create data
		Country__c country = new Country__c (Name='TestCountry');
		insert country;
		
		Employee_Details__c employee = new Employee_Details__c(Business_Unit__c = 'PRA',
												Country_Name__c = country.id,
												Date_Hired__c = date.today(),
												Employee_Unique_Key__c = '1111',
												First_Name__c='TestFirstName',
												Last_Name__c='TestLastName',
												Job_Class_Desc__c='TestJob',
												Location_Code__c='TestLoc',
												Status__c='AA',
												Email_Address__c='empemail@praintl.com' );
		insert employee;
		
		String errorMessage = 'Error in reading current user from the Employees';
		
		// Check the function with employee that is not found
		Employee_Details__c result = FinancialDocumentApprovalDataAccessor.getEmployeeByNameAndEmail('FirstName',
																									'LastName',
																									'emp@praintl.com');
		system.assertEquals(result.Id, NULL, errorMessage);
		system.assertEquals(result.First_Name__c, 'FirstName', errorMessage);
		system.assertEquals(result.Last_Name__c, 'LastName', errorMessage);
		system.assertEquals(result.Email_Address__c, 'emp@praintl.com', errorMessage);
		system.assertEquals(result.Job_Class_Desc__c, NULL, errorMessage);
		system.assertEquals(result.Location_Code__c, NULL, errorMessage);
		
		// Check the function with employee that is found
		result = FinancialDocumentApprovalDataAccessor.getEmployeeByNameAndEmail('TestFirstName',
																				'TestLastName',
																				'empemail@praintl.com');
		system.assertEquals(result.Id, employee.Id, errorMessage);
		system.assertEquals(result.First_Name__c, employee.First_Name__c, errorMessage);
		system.assertEquals(result.Last_Name__c, employee.Last_Name__c, errorMessage);
		system.assertEquals(result.Email_Address__c, employee.Email_Address__c, errorMessage);
		system.assertEquals(result.Job_Class_Desc__c, employee.Job_Class_Desc__c, errorMessage);
		system.assertEquals(result.Location_Code__c, employee.Location_Code__c, errorMessage);
	}
}