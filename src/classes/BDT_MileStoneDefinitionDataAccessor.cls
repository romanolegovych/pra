/** Implements the Selector Layer of the object MileStoneDefinition__c
 * @author	Dimitrios Sgourdos
 * @version	25-Feb-2014
 */
public with sharing class BDT_MileStoneDefinitionDataAccessor {
	
	/** Object definition for fields used in application for MileStoneDefinition
	 * @author	Dimitrios Sgourdos
	 * @version 25-Feb-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('MileStoneDefinition__c');
	}
	
	
	/** Object definition for fields used in application for MileStoneDefinition with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 25-Feb-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'BDTDeleted__c,';
		result += referenceName + 'Category__c,';
		result += referenceName + 'SequenceNumber__c';
		
		return result;
	}
	
	
	/** Retrieve the list of MileStoneDefinitions that meets the given criteria.
	 * @author	Dimitrios Sgourdos
	 * @version 25-Feb-2014
	 * @param	whereClause			The criteria that the MileStoneDefinitions must meet
	 * @param	orderByField		The field that the MileStoneDefinitions will be ordered by
	 * @return	The list of MileStoneDefinitions
	 */
	public static List<MileStoneDefinition__c> getMileStoneDefinitions(String whereClause, String orderByField) {
		// Check parmateres
		String tmpWhereClause  = String.IsBlank(whereClause)?  '' : 'WHERE ' + whereClause;
		String tmpOrderByField = String.IsBlank(orderByField)? '' : 'ORDER BY ' + orderByField;
		
		String query = String.format(
								'SELECT {0} ' +
								'FROM MileStoneDefinition__c ' +
								'{1} {2}',
								new List<String> {
									getSObjectFieldString(),
									tmpWhereClause,
									tmpOrderByField
								}
							);
		return (List<MileStoneDefinition__c>) Database.query(query);
	}
}