public with sharing class PAWS_ApexTestsEnvironment 
{
	public static void init()
	{
		STSWR1__Work_Relay_Config__c config = STSWR1__Work_Relay_Config__c.getOrgDefaults();
  		config.STSWR1__Namespace__c = 'STSWR1';
  		upsert config;
	}
	
	public static STSWR1.API API
    {
    	get
        {
            if(API == null)
            {
                API = new STSWR1.API();
            }
            return API;
        }
        set;
    }
    
    public static ecrf__c Project
    {
        get
        {
            if(Project == null)
            {
		        WFM_Client__c client = new WFM_Client__c(name='TestClient', Client_Name__c='TestClinet_Name', Client_Unique_Key__c='TestClient');
		        insert client;
		        
		        WFM_Contract__c contract = new WFM_Contract__c(name='TestContract', Contract_Status__c='AA', Client_ID__c=client.id, Contract_Unique_Key__c='TestContract');
		        insert contract;
		        
		        WFM_Project__c wfmProject = new WFM_Project__c(name='TestProject', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProject', project_status__c ='RM',  Status_Desc__c='Active');
		        insert wfmProject ;
		        
		        Project = new ecrf__c(Name='Test', Project_Id__c=wfmProject.Id, Type__c='Regular');
		        insert Project;
            }
            return Project;
        }
        set;
    }
	
	public static STSWR1__Flow__c Flow
    { 
        get
        {
            if(Flow == null)
            {
		    	Flow = (STSWR1__Flow__c)API.call('FlowApexTestsEnvironment', 'getFlow', null);
		    	Flow.STSWR1__Type__c = 'One Time';
		    	Flow.STSWR1__Object_Type__c = 'ecrf__c';
		    	update Flow;
            }
            return Flow;
        }
        set;
    }
    
    public static STSWR1__Flow_Step_Junction__c FlowStep
    {
        get
        {
            if(FlowStep == null)
            {
                FlowStep = (STSWR1__Flow_Step_Junction__c)API.call('FlowApexTestsEnvironment', 'getFlowStep', null);
            }
            return FlowStep;
        }
        set;
    }
    
    public static STSWR1__Flow_Swimlane__c FlowSwimlane
    {
        get
        {
            if(FlowSwimlane == null)
            {
                FlowSwimlane = (STSWR1__Flow_Swimlane__c)API.call('FlowApexTestsEnvironment', 'getFlowSwimlane', null);
            }
            return FlowSwimlane;
        }
        set;
    }
    
    public static STSWR1__Flow_Milestone__c FlowMilestone
    {
        get
        {
            if(FlowMilestone == null)
            {
                FlowMilestone = (STSWR1__Flow_Milestone__c)API.call('FlowApexTestsEnvironment', 'getFlowMilestone', null);
            }
            return FlowMilestone;
        }
        set;
    }
    
    public static STSWR1__Flow_Branch__c FlowBranch
    {
        get
        {
            if(FlowBranch == null)
            {
                FlowBranch = (STSWR1__Flow_Branch__c)API.call('FlowApexTestsEnvironment', 'getFlowBranch', null);
            }
            return FlowBranch;
        }
        set;
    }
    
    public static STSWR1__Flow_Step_Connection__c FlowStepConnection
    {
        get
        {
            if(FlowStepConnection == null)
            {
                FlowStepConnection = (STSWR1__Flow_Step_Connection__c)API.call('FlowApexTestsEnvironment', 'getFlowStepConnection', null);
            }
            return FlowStepConnection;
        }
        set;
    }
    
    public static STSWR1__Flow_Step_Action__c FlowStepAction
    {
        get
        {
            if(FlowStepAction == null)
            {
                FlowStepAction = (STSWR1__Flow_Step_Action__c)API.call('FlowApexTestsEnvironment', 'getFlowStepAction', null);
            }
            return FlowStepAction;
        }
        set;
    }
    
    public static STSWR1__Flow_Instance__c FlowInstance
    { 
        get
        {
            if(FlowInstance == null)
            {
		    	FlowInstance = (STSWR1__Flow_Instance__c)API.call('FlowApexTestsEnvironment', 'getFlowInstance', null);
		    	FlowInstance.STSWR1__Object_Id__c = Project.Id;
				FlowInstance.STSWR1__Object_Name__c = Project.Name;

				if(Flow != null) update FlowInstance;
            }
            return FlowInstance;
        }
        set;
    }
    
    public static STSWR1__Flow_Instance_Cursor__c FlowInstanceCursor
    { 
        get
        {
            if(FlowInstanceCursor == null)
            {
		    	FlowInstanceCursor = new STSWR1__Flow_Instance_Cursor__c(STSWR1__Flow_Instance__c=FlowInstance.Id, STSWR1__Step__c=FlowStep.Id, STSWR1__Step_Assigned_To__c=UserInfo.getUserId(), STSWR1__Status__c='In Progress');
                insert FlowInstanceCursor;
                
            }
            return FlowInstanceCursor;
        }
        set;
    }
    
    public static STSWR1__Flow_Instance_History__c FlowInstanceHistory
    { 
        get
        {
            if(FlowInstanceHistory == null)
            {
		    	FlowInstanceHistory = new STSWR1__Flow_Instance_History__c(STSWR1__Cursor__c=FlowInstanceCursor.Id, STSWR1__Step__c=FlowStep.Id, STSWR1__Status__c='In Progress');
                insert FlowInstanceHistory;
                
            }
            return FlowInstanceHistory;
        }
        set;
    }
    
    public static STSWR1__Gantt_Step_Property__c GanttStepProperty
    { 
        get
        {
            if(GanttStepProperty == null)
            {
		    	GanttStepProperty = new STSWR1__Gantt_Step_Property__c(STSWR1__Step__c=FlowStep.Id, STSWR1__Revised_End_Date__c = Date.today(), Project__c=Project.Id, STSWR1__Parent_Flow_Id__c=FlowStep.STSWR1__Flow__c);
                insert GanttStepProperty;
                
            }
            return GanttStepProperty;
        }
        set;
    }

    public static PAWS_Project_Flow_Junction__c ProjectFlow
    {
        get
        {
            if(ProjectFlow == null)
            {
            	STSWR1__Item__c item = new STSWR1__Item__c(Name='Test Item');
		        insert item;
		        
		    	ProjectFlow = new PAWS_Project_Flow_Junction__c();
		        ProjectFlow.Folder__c = item.Id;
		        ProjectFlow.Flow__c = Flow.Id;
		        ProjectFlow.Project__c = Project.Id;
		        //ProjectFlow.Flow_Instance__c = FlowInstance.Id;

		        insert ProjectFlow;
            }
            return ProjectFlow;
        }
        set;
    }
    
    public static PAWS_Project_Flow_Country__c ProjectCountry
    {
        get
        {
            if(ProjectCountry == null)
            {
		    	ProjectCountry = new PAWS_Project_Flow_Country__c();
		        ProjectCountry.Name = 'Generic';
		        ProjectCountry.PAWS_Project__c = ProjectFlow.Project__c;

		        insert ProjectCountry;
            }
            return ProjectCountry;
        }
        set;
    }
    
	public static PAWS_Project_Flow_Site__c ProjectSite
	{
		get
			{
				if(ProjectSite == null)
				{
					ProjectSite = new PAWS_Project_Flow_Site__c();
					ProjectSite.Name = 'Generic';
					ProjectSite.PAWS_Country__c = ProjectCountry.Id;
					
					insert ProjectSite;
				}
				return ProjectSite;
			}
		set;
	}
	
	public static PAWS_Project_Flow_Agreement__c ProjectAgreement
	{
		get
			{
				if(ProjectAgreement == null)
				{
					ProjectAgreement = new PAWS_Project_Flow_Agreement__c();
					ProjectAgreement.Name = 'Generic';
					ProjectAgreement.PAWS_Site__c = ProjectSite.Id;
					
					insert ProjectAgreement;
				}
				return ProjectAgreement;
			}
		set;
	}
	
	public static PAWS_Project_Flow_Document__c ProjectDocument
	{
		get
			{
				if(ProjectDocument == null)
				{
					ProjectDocument = new PAWS_Project_Flow_Document__c();
					ProjectDocument.Name = 'Generic';
					ProjectDocument.PAWS_Site__c = ProjectSite.Id;
					
					insert ProjectDocument;
				}
				return ProjectDocument;
			}
		set;
	}
	
	public static PAWS_Project_Flow_Submission__c ProjectSubmission
	{
		get
			{
				if(ProjectSubmission == null)
				{
					ProjectSubmission = new PAWS_Project_Flow_Submission__c();
					ProjectSubmission.Name = 'Generic';
					ProjectSubmission.PAWS_Site__c = ProjectSite.Id;
					
					insert ProjectSubmission;
				}
				return ProjectSubmission;
			}
		set;
	}
	
	public static STSWR1__Email_Message__c EmailMessage
    { 
        get
        {
            if(EmailMessage == null)
            {
		    	EmailMessage = new STSWR1__Email_Message__c(STSWR1__To__c = 'test-wr@workrelay.com', STSWR1__Subject__c='Test');
		    	EmailMessage.STSWR1__Additional_Data__c = GanttStepProperty.Id;
				insert EmailMessage;
            }
            return EmailMessage;
        }
        set;
    }
    
	public static PAWS_Queue__c PAWSQueue
	{
		get
		{
			if (PAWSQueue == null)
			{
				PAWSQueue = new PAWS_Queue__c(
					Type__c = 'Activate Flow',
					Status__c = 'Pending',
					Data__c = '{}'
				);
				insert PAWSQueue;
			}
			return PAWSQueue;
		}
		set;
	}
	
	public static PAWS_Settings__c PAWSSettings
	{
		get
		{
			if (PAWSSettings == null)
			{
				PAWSSettings = new PAWS_Settings__c(
					Name = 'PAWS Settings',
					Project_Folder_Name__c = 'PAWS Project Flows',
					Error_Notification_Email__c = 'minkodmitro@prahs.com'
				);
				insert PAWSSettings;
			}
			return PAWSSettings;
		}
		set;
	}
}