public with sharing class PAWS_FlowStepPropertyTrigger extends STSWR1.AbstractTrigger
{
	private PAWS_StepProperty_LinkedStepHelper LinkedStepHelper
	{
		get
		{
			if (LinkedStepHelper == null)
			{
				LinkedStepHelper = new PAWS_StepProperty_LinkedStepHelper();
			}
			return LinkedStepHelper;
		}
		set;
	}
	
	public override void beforeInsert(List<SObject> records)
	{
		fillFlow((List<STSWR1__Flow_Step_Property__c>)records);
		fillPAWSProject((List<STSWR1__Flow_Step_Property__c>)records);
		LinkedStepHelper.BeforeInsert((List<STSWR1__Flow_Step_Property__c>) records);
	}
	
	public override void beforeUpdate(List<SObject> newRecords, List<SObject> oldRecords)
	{
		fillFlow((List<STSWR1__Flow_Step_Property__c>)newRecords);
		fillPAWSProject((List<STSWR1__Flow_Step_Property__c>)newRecords);
	}
	
	public override void afterInsert(List<SObject> records)
	{
		LinkedStepHelper.AfterInsert((List<STSWR1__Flow_Step_Property__c>) records);
	}
	
	public override void afterUpdate(List<SObject> newRecords, List<SObject> oldRecords)
	{
		LinkedStepHelper.AfterUpdate((List<STSWR1__Flow_Step_Property__c>) newRecords, (List<STSWR1__Flow_Step_Property__c>) oldRecords);
	}
	
	public override void afterDelete(List<SObject> records)
	{
		LinkedStepHelper.AfterDelete((List<STSWR1__Flow_Step_Property__c>) records);
	}
	
	private void fillFlow(List<STSWR1__Flow_Step_Property__c> records)
    {
    	for(STSWR1__Flow_Step_Property__c record : records)
    		record.Flow__c = record.Flow_Id__c;
    }

    private void fillPAWSProject(List<STSWR1__Flow_Step_Property__c> records)
    {
    	Set<ID> ids = new Set<ID>();
    	for(STSWR1__Flow_Step_Property__c record : records)
    	{
    		if(!String.isEmpty(record.PAWS_Project_Id__c)) ids.add((ID)record.PAWS_Project_Id__c);
    	}
    	
    	Map<ID, ecrf__c> projects = new Map<ID, ecrf__c>([select Id from ecrf__c where Id in :ids]);
    	for(STSWR1__Flow_Step_Property__c record : records)
    	{
    		if(!String.isEmpty(record.PAWS_Project_Id__c) && projects.containsKey((ID)record.PAWS_Project_Id__c)) record.PAWS_Project__c = record.PAWS_Project_Id__c;
    		else record.PAWS_Project__c = null;
    	}
    }
}