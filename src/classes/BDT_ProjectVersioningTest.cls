@isTest
private class BDT_ProjectVersioningTest {
	
    static testMethod void testConstructor() {
		BDT_ProjectVersioningController pv = new BDT_ProjectVersioningController();
		
		system.assert(pv.createVersion == false);
		pv.enableCreateVersion();
		system.assert(pv.createVersion == true);
		pv.disableCreateVersion();
		system.assert(pv.createVersion == false);
		
    }
    
    static testMethod void testAJAXgets() {
    	
    	init();
    	
    	String returnJSON;
    	returnJSON = BDT_ProjectVersioningController.getProject(firstProject.Id);
    	system.assertNotEquals(returnJSON, null);
    	returnJSON = BDT_ProjectVersioningController.getDesign(firstProject.Id);
    	system.assertNotEquals(returnJSON, null);
    	returnJSON = BDT_ProjectVersioningController.getPopulation(firstProject.Id);
    	system.assertNotEquals(returnJSON, null);
    	returnJSON = BDT_ProjectVersioningController.getService(firstProject.Id);
    	system.assertNotEquals(returnJSON, null);
    	returnJSON = BDT_ProjectVersioningController.getProjectService(firstProject.Id);
    	system.assertNotEquals(returnJSON, null);
    	returnJSON = BDT_ProjectVersioningController.getRateCards(firstProject.Id);
    	system.assertNotEquals(returnJSON, null);
    	
    	returnJSON = BDT_ProjectVersioningController.getFinancialDocuments(firstProject.Id);
    	returnJSON = BDT_ProjectVersioningController.getFinancialDocuments2(firstProject.Id);
    	returnJSON = BDT_ProjectVersioningController.getProjectDetails(firstProject.Id);
    	returnJSON = BDT_ProjectVersioningController.getPassThrough(firstProject.Id);
    }
    
    public static Client_Project__c 						firstProject;
	public static List<Study__c> 							studiesList;
	public static List<ClinicalDesign__c> 					designList;
	public static List<Business_Unit__c> 					myBUList;
	public static list<ClinicalDesignStudyAssignment__c> 	cdsaList;
	public static List<Arm__c>								armList;
	public static List<Population__c> 						populationList;
	public static List<PopulationStudyAssignment__c> 		spaList;
	public static list<StudyDesignPopulationAssignment__c>  sdpaList;
	public static list<GroupArmAssignment__c>				gaaList;
	public static List<ServiceCategory__c> 					ServiceCategoryList;
	public static List<Service__c> 							ServiceList;
	public static List<RateCard__c> 						RateCardList;
	public static List<RateCardClass__c> 					RateCardClassList;
	public static List<ProjectService__c> 					ProjectServiceList;
	public static List<Epoch__c> 							EpochList;
	public static List<Flowcharts__c> 						FlowchartList;
	public static List<flowchartassignment__c> 				FlowchartAssignmentList;
	public static List<TimePoint__c>						TimepointList;
	public static List<ProjectServiceTimePoint__c>			ProjectServiceTimePointList;
    
    static void init(){
		//create project
		firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		
		//create business units
		myBUList = BDT_TestDataUtils.buildBusinessUnit(2);
		insert myBUList;
		
		//create studies
		studiesList = BDT_TestDataUtils.buildStudies(firstProject,2);
		studiesList[0].Business_Unit__c = myBUList[0].Id; 
		studiesList[1].Business_Unit__c = myBUList[1].Id;  
		insert studiesList;
		
		//create design
		designList = BDT_TestDataUtils.buildClinicDesign(firstProject,1); 
		insert designList;
		
		//create population
		populationList = BDT_TestDataUtils.buildPopulation(firstProject.id, 2);
		insert populationList;

		// add populations to study
		spaList = BDT_TestDataUtils.buildPopulationStudyAssignment(studiesList, populationList);
		insert spaList;		
		
		// add design to study
		cdsaList = BDT_TestDataUtils.buildDesignAssignment(designList,studiesList); 
		insert cdsaList;
		
		// add populations to study design	
		sdpaList = BDT_TestDataUtils.buildStudyDesignPopulationAssignment(cdsaList, spaList );
		insert sdpaList;	
		
		// add arms to clinical design
		armList = BDT_TestDataUtils.buildArms(designList,1);			 				  
		insert armList;
		
		//add arms to groups
		gaaList = new list<GroupArmAssignment__c>();
		for (Arm__c a:armList) {
			for (StudyDesignPopulationAssignment__c sdpa: sdpaList){
				gaaList.add(new GroupArmAssignment__c(arm__c = a.id,
													  studydesignpopulationassignment__c = sdpa.id,
													  NumberOfSubjects__c = 2));
			}
		}
		insert gaaList;
		
		// add epochs
		EpochList = BDT_TestDataUtils.buildEpochs(designList,1);
		insert EpochList;
		
		// add flowcharts
		FlowchartList = BDT_TestDataUtils.buildFlowcharts(designList,1);
		insert FlowchartList;
		
		//add flowchartassignments
		FlowchartAssignmentList = BDT_TestDataUtils.buildFlowchartAssignment (designList,FlowchartList,armList,EpochList);
		insert FlowchartAssignmentList;	
		
		//add timepoints 																		
		TimepointList = BDT_TestDataUtils.buildTimePoints(FlowchartList, 2);
		insert TimepointList;
		
		// create service categories
		ServiceCategoryList = BDT_TestDataUtils.buildServiceCategory(2, null);
		ServiceCategoryList = BDT_TestDataUtils.buildServiceCategory(1, ServiceCategoryList);
		insert ServiceCategoryList;
		
		// create services that are design objects
		ServiceList = BDT_TestDataUtils.buildService(ServiceCategoryList, 2, 'TEST', true);
		insert ServiceList;
		
		// create ratecards
		RateCardList = BDT_TestDataUtils.buildRateCard(ServiceList, myBUList);
		insert RateCardList;
		
		// create ratecard classes
		RateCardClassList = BDT_TestDataUtils.buildRateCardClass(RateCardList);
		insert RateCardClassList;
		
		// create project services
		ProjectServiceList = BDT_TestDataUtils.buildProjectService(ServiceList, firstProject.id);
		insert ProjectServiceList;	
		
		// add projectservice timepoints
		List<ProjectServiceTimePoint__c> ProjectServiceTimePointNullList = new List<ProjectServiceTimePoint__c>();
		ProjectServiceTimePointList = new List<ProjectServiceTimePoint__c>();
		for (TimePoint__c tp:TimepointList){
			for (ProjectService__c ps:ProjectServiceList) {
				ProjectServiceTimePointNullList.add(new ProjectServiceTimePoint__c(ProjectService__c = ps.id,
																				Flowchart__c = tp.flowchart__c,
																				TimePoint__c = null,
																				TimePointCount__c = null,
																				listOfTimeMinutes__c = null));
				ProjectServiceTimePointList.add(new ProjectServiceTimePoint__c(ProjectService__c = ps.id,
																			   Flowchart__c = tp.flowchart__c,
																			   TimePoint__c = tp.id));
			}
		}
		insert ProjectServiceTimePointNullList;
		insert ProjectServiceTimePointList;	

	}
    
    
}