public class IPA_DAL_LinksManagement
{   
	// ********************* Quick Links ***********************//	
	//This function returns the list of Quick Links specifically identified by their ids
    public  List<IPA_Links__c> returnQuickLinksByLink(Set<Id> linkIds) 
    {
        system.debug('in IPA_DAL_LinksManagement');
        List<IPA_Links__c> ipa_quicklinks;
        ipa_quicklinks = [SELECT Name, Link__c, CSS_Class__c, External_Link__c, Restricted_Link__c 
                          FROM IPA_Links__c 
                          WHERE Id = : linkIds 
                          ORDER BY Weight__c, Name 
                          LIMIT 10];
        return ipa_quicklinks;
    }
   
    //This function returns the list of Quick Links filtered by their related Topics
    public  List<IPA_Links__c> returnQuickLinksByTopic(Set<Id> topicIds) 
    {
        system.debug('in IPA_DAL_LinksManagement');
        List<IPA_Links__c> ipa_quicklinks;
        ipa_quicklinks = [SELECT Name, Link__c, CSS_Class__c, External_Link__c, Restricted_Link__c 
                          FROM IPA_Links__c 
                          WHERE RecordType.Name='Quick Link' AND Id in (
                              SELECT Link__c 
                              FROM IPA_Link_Topic__c 
                              WHERE Topic__c = :topicIds) 
                          ORDER BY Weight__c, Name 
                          LIMIT 10];
        return ipa_quicklinks;
    }
     
    //This function returns the list of Quick Links filtered by their Category
    public  List<IPA_Links__c> returnQuickLinksByCategory(Id catId) 
    {
        List<IPA_Links__c> ipa_quicklinks;
        ipa_quicklinks = [SELECT Name, Link__c, CSS_Class__c, External_Link__c, Restricted_Link__c 
                          FROM IPA_Links__c 
                          WHERE RecordType.Name='Quick Link' AND Category__c =: catId 
                          ORDER BY Weight__c, Name 
                          LIMIT 10];
        return ipa_quicklinks;
    }

	// *************************** Left Menus *********************//
    //This function returns list of Main Menu & its Sub Menu links in HTML format 
    public String returnLeftMenuLinks(String UserId) 
    {
        String html = '';
        List<IPA_Links__c> ipa_mainlinks = [select Name, CSS_Class__c, Link__c from IPA_Links__c 
            where RecordType.Name='Main Menu Item' 
            order by Weight__c, Name];
            
        List<IPA_Links__c> ipa_sublinks;
        for(IPA_Links__c m: ipa_mainlinks)
        {
            if(m.Name == 'Dashboard'){
                html += '\n<li class="active"><a href="' + m.Link__c + '"><i class="' + m.CSS_Class__c + '"></i>' + m.Name + '</a>';
            }else if(m.Link__c != null){
                String Tabloc ;
                if(m.Link__c.contains('/apex/IPA_') || m.Link__c.equals('#')){ 
                    Tabloc = ''; 
                }else {
                    Tabloc = '_blank';
                }
                html += '\n<li><a href="' + m.Link__c + '" target="' +Tabloc+ '"><i class="' + m.CSS_Class__c + '"></i>' + m.Name + '</a>';
            }

            //If Sub Menus
            ipa_sublinks = [select Name, Link__c from IPA_Links__c 
                where RecordType.Name='Sub Menu Item' AND Parent_Link__c =: m.Id
                order by Weight__c, Name];
            if(!ipa_sublinks.isEmpty() || m.Name == 'Dashboard')
            {
                if(ipa_sublinks.size() < 5)
                    html += '\n\t<ul class="submenu">';
                else if(ipa_sublinks.size() < 10)
                    html += '\n\t<ul class="submenu" style="top:-150%">';
                else
                    html += '\n\t<ul class="submenu" style="top:-500%">';
                    
                for(IPA_Links__c s: ipa_sublinks)
                {
                    if(s.Link__c != null){
                        String Tabloc ;
                        if(s.Link__c.contains('/apex/IPA_') || s.Link__c.equals('#')){ 
                            Tabloc = ''; 
                        }else {
                            Tabloc = '_blank';
                        }
                        html += '\n\t\t<li><a href="' + s.Link__c + '" target="' +Tabloc+ '">' + s.Name + '</a></li>';
                    }
                }
                
                //Put default Manage Custom Links for Dashboard menu item
                if(m.Name == 'Dashboard')
                {
                    //Put Custom Links as well
                    if(UserId != '')
                    {
                        List<IPA_Custom_Links__c> lstLinks;
                        lstLinks = returnCustomLinks(UserId);
                        for(IPA_Custom_Links__c cus: lstLinks)
                        {    
                            //html += '\n\t\t<li><a href="' + cus.Link__c + '" target="_blank">' + cus.Label__c + '</a></li>';
                            String Tabloc ;
                            if(cus.Link__c.contains('/apex/IPA_')) Tabloc = ''; else Tabloc = '_blank';
                              html += '\n\t\t<li><a href="' + cus.Link__c + '" target="' +Tabloc+ '">' + cus.Label__c + '</a></li>';                             
                        }
                        //Add Manage Custom Links hyperlink
                       html += '\n\t\t<li><a href="" id="managelinks" data-src="manage-custom-links"><i class="pra-icon-gear" style="display: inherit"></i>Manage Custom Links</a></li>';
                    }
                }
                html += '\n\t</ul>';
            }
            html += '\n</li>';
        }
        return html;
    }
    
    // This function is used to return Title & Content of RecordList
    public List<IPA_Links__c> returnLinks(String Type) 
    {
        List<IPA_Links__c> ipa_links;
        ipa_links = [select Id, Name, Link__c from IPA_Links__c where Site_Locations__c  =: Type];
        return ipa_links;
    }
    
    // This function is used to return list of custom links for given User
    public List<IPA_Custom_Links__c> returnCustomLinks(String Id)
    {
        List<IPA_Custom_Links__c> lstLinks;
        lstLinks = [select Link__c, Label__c from IPA_Custom_Links__c where User__c =: Id order by Order__c, Label__c];
        
        if(lstLinks.isEmpty()){
            IPA_Custom_Links__c l = new IPA_Custom_Links__c();
            l.Label__c = 'Corporate Calendar';
            l.Link__c = '/apex/IPA_Calendar?Month=&Year=&Action=';
            l.User__c = Id;
            l.Order__c = 1;
            lstLinks.add(l);
            
            l = new IPA_Custom_Links__c();
            l.Label__c = 'Industry Watch';
            l.Link__c = '/apex/IPA_Industry_Watch?Type=Industry Watch';
            l.User__c = Id;
            l.Order__c = 2;
            lstLinks.add(l);
            
            l = new IPA_Custom_Links__c();
            l.Label__c = 'Careers Page';
            l.Link__c = 'https://sjobs.brassring.com/1033/ASP/TG/cim_home.asp?partnerid=185&siteid=5245';
            l.User__c = Id;
            l.Order__c = 3;
            lstLinks.add(l);
            
            l = new IPA_Custom_Links__c();
            l.Label__c = 'Information Technology';
            l.Link__c = 'http://insidepra/it/';
            l.User__c = Id;
            l.Order__c = 4;
            lstLinks.add(l);   
            
            l = new IPA_Custom_Links__c();
            l.Label__c = 'Legacy InsidePRA';
            l.Link__c = 'http://insidepra/legacy.asp';
            l.User__c = Id;
            l.Order__c = 5;
            lstLinks.add(l);    
        }
        return lstLinks;
    }
    
    // This function is used to save list of custom links for given User
    public void saveCustomLinks(List<IPA_Custom_Links__c> lstLinks, String UserId)
    {
        List<IPA_Custom_Links__c> lstTmp;
        try {
            lstTmp = [select Id from IPA_Custom_Links__c where User__c =: UserId];
            delete lstTmp;
        }catch(Exception e) {}
        
        for(IPA_Custom_Links__c l: lstLinks)
        {
            if(!l.Link__c.trim().startsWithIgnoreCase('http') && !l.Link__c.trim().startsWithIgnoreCase('/')){
                    l.Link__c = 'http://' + l.Link__c.trim();
            }
            l.Link__c = l.Link__c.trim();
            try {
                insert l;
            }catch(Exception e) {}
        }   
        return;
    }
    
    // ******************* Resource Cabinet ***************************//
    //This function returns the list of Quick Links specifically identified by their ids
    public  List<IPA_Links__c> returnResourceCabinetsByLink(Set<Id> linkIds) 
    {
        List<IPA_Links__c> ipa_quicklinks;
        ipa_quicklinks = [SELECT Name, Link__c, CSS_Class__c FROM IPA_Links__c 
                          WHERE Id = : linkIds ORDER BY Weight__c, Name LIMIT 15];
        return ipa_quicklinks;
    }
    
    public  List<IPA_Links__c> returnResourceCabinetsByTopic(Set<Id> topicIds) 
    {
        List<IPA_Links__c> ipa_quicklinks;
        ipa_quicklinks = [SELECT Name, Link__c, CSS_Class__c 
                          FROM IPA_Links__c 
                          WHERE RecordType.Name='Resource Cabinet Item' AND Id in (
                              SELECT Link__c 
                              FROM IPA_Link_Topic__c 
                              WHERE Topic__c = :topicIds) 
                          ORDER BY Weight__c, Name 
                          LIMIT 15];
        return ipa_quicklinks;
    }
    public List<IPA_Links__c> returnResourceCabinetsByCategory(Id catId) 
    {
        List<IPA_Links__c> ipa_quicklinks;
        ipa_quicklinks = [SELECT Name, Link__c, CSS_Class__c FROM IPA_Links__c 
                          WHERE RecordType.Name='Resource Cabinet Item' AND Category__c =: catId 
                          ORDER BY Weight__c, Name LIMIT 15];
        return ipa_quicklinks;
    }
}