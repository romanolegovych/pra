public with sharing class RM_OtherService {
	public static list<buf_code__c> validBufcodesList(set<string> stBuf){
		return RM_DataAccessor.getBufcodesList(stBuf);
	}
	
	public static list<Country_BusinessUnit__c> validBUCountryCombo(set<string> setCountryBU){
		return RM_DataAccessor.getCountryBUbyCombo(setCountryBU);
	}
	public static set<string>getUserGroupBU(string userID){
        list<string>lstGrpName = RM_DataAccessor.GetRMGroupfromUserID(userID);
        
        if (lstGrpName != null){
            list<WFM_BU_Group_Mapping__c> lstBU= RM_DataAccessor.getBUGroupMapping(lstGrpName);
            if (lstBU.size()> 0)
            {
                
                set<String> setItems=new set<string>();
                for (WFM_BU_Group_Mapping__c bu : lstBU){
                    if(!setItems.contains(bu.PRA_Business_Unit__r.name)){              
                        
                         setItems.add(bu.PRA_Business_Unit__r.name);
                    }
                }
                return setItems;
            }
            else
                return null;
        }
        else
            return null;
    }
    public static string getUserDefaultUnit(string userID){
        list<string>lstGrpName = RM_DataAccessor.GetRMGroupfromUserID(userID);
        
        if (lstGrpName != null){
            list<WFM_BU_Group_Mapping__c> lstBU= RM_DataAccessor.getBUGroupMapping(lstGrpName);
            if (lstBU.size()> 0)
            {               
                string unit = '';
                integer iCount = 0;
                for (WFM_BU_Group_Mapping__c bu : lstBU){
                    if (unit != bu.Default_Unit__c ){
                    	unit = bu.Default_Unit__c;
                    	iCount++;
                    }
                }
                
                if (iCount > 1)
                	return 'FTE';
                else
                	return unit;
            }
            else
                return 'FTE';
        }
        else
            return 'FTE';
    }
    public static set<string> getUserManagedRole(string userID){
    	list<string>lstGrpName = RM_DataAccessor.GetRMGroupfromUserID(userID);
    	set<String> setItems=new set<string>();
    	
    	if (lstGrpName != null){
	    	for (WFM_JC_Group_Mapping__c g: RM_DataAccessor.getManagedRoleByGroup(lstGrpName)){ 
	    		if(!setItems.contains(g.Job_Class_Desc__r.name)){ 
	                 setItems.add(g.Job_Class_Desc__r.name);
	            }
	    	}
    	}
    	return setItems;
    }
    public static list<string>getBUinGroup(){
        list<string> lstB = new list<string>();
        list<WFM_BU_Group_Mapping__c> lstBU= RM_DataAccessor.getAllBUinGroupMapping();
        if (lstBU.size()> 0)
        {
            
            set<String> setItems=new set<string>();
            for (WFM_BU_Group_Mapping__c bu : lstBU){
                if(!setItems.contains(bu.PRA_Business_Unit__r.name)){              
                     lstB.add(bu.PRA_Business_Unit__r.name);	
                     setItems.add(bu.PRA_Business_Unit__r.name);
                }
            }
            return lstB;
        }
        else
            return null;
        
    }
    
    public static boolean IsRquireReqID(string UserID){
    	
    	list<string>lstGrpName = RM_DataAccessor.GetRMGroupfromUserID(userID);
        
        if (lstGrpName != null){
             list<WFM_BU_Group_Mapping__c> lstBU= RM_DataAccessor.getBUGroupMapping(lstGrpName);
            if (lstBU.size()> 0)
            {
            	for (WFM_BU_Group_Mapping__c bu : lstBU){	            
	                if (bu.Is_Require_ReqID__c)
	                	return true;
            	}
               
            }
        }
        return false;
    }
    public static boolean IsRMAdmin(string UserID){
    	if (IsSystemAdmin())
    		return true;
    	else{
    	
	    	list<string> lstGroup = RM_DataAccessor.GetRMGroupfromUserID(UserID);
	    	if (lstGroup!= null && lstGroup.Size() > 0){
		    	string groupName = RM_DataAccessor.GetConstant('RMAdminGroupName');
		    	if (groupName != '')
		    	{
			    	for (string g : lstGroup){
			    		if (g == groupName)
			    			return true;
			    	}
		    	}
	    	}
    	}
    	return false;
    }
    public static boolean IsSystemAdmin(){ 
      	Profile p = RM_DataAccessor.GetUserProfileInfo(UserInfo.getProfileId());
    	if (p.name == 'System Administrator')
    		return true;
    	else
    		return false;
    }
    
    public static string getJQExportURL(){
    	return  RM_DataAccessor.GetConstant('JQExportURL');
    }
    public static string getViewAssignmentSearchNote(){
    	return  RM_DataAccessor.GetConstant('ViewAssigmentSearchNote');
    }
     public static string getActiveEmployeeStatusStr(){
        list<string> lActive = RM_DataAccessor.getListActiveStatus();
        string active = '';
        if (lActive.size() > 0){
            for (string s: lActive){
                active += '\'' + s + '\',';
            }
        
            active = active.substring(0, active.length()-1);
            return  active;
        }
        return '';
    }
     public static list<string> GetRMGroupfromUserID(string userID){
        List<GroupMember> lstGrpMember =  [Select GroupID From GroupMember where UserOrGroupId = :userId];
        if (lstGrpMember.size() > 0)
        {   
            List<ID> lstGrpID = new List<ID>();
            for (GroupMember g : lstGrpMember)
                lstGrpID.add(g.GroupID);      
            List<Group> lstGrp = [Select DeveloperName from Group where ID in :lstGrpID];
            
            list<string> lstGrpName = new list<string>();
            for (Group gr : lstGrp){
                if (gr.DeveloperName.startsWith('RM_'))
                    lstGrpName.add(gr.DeveloperName);
            }
            return lstGrpName;
        }
        return null;
    }
    public static user GetUserInfo(string userID){
    	return RM_DataAccessor.GetUserInfo(userID);
    }
    public static user GetUserInfobyEmail(string email){
    	return RM_DataAccessor.GetUserInfobyEmail(email);
    
    }
    public static list<string> getJobClassDescByBU( list<string> lstBU){
    	return RM_DataAccessor.getJobClassDescByBU(lstBU);
    }
     public static list<string> getJobClassDescByBU( string strBU){
    	return RM_DataAccessor.getJobClassDescByBU(strBU);
    }
    
    /* Notification */
    public static list<WFM_Notification__c> getAllNotification(){
     	return RM_DataAccessor.getAllNotification();
    }
    public static list<WFM_Notification__c> getUnReadNotification(list<string> lstID){
     	return RM_DataAccessor.getUnReadNotification(lstID);
    }
    public static list<WFM_Notification_view_log__c> getListNotificationViewLog(list<string> ListOfGroupNames, string UserID){
     	return RM_DataAccessor.getListNotificationViewLog(ListOfGroupNames, UserID);
    }
   	/* country hours */
    public static list<WFM_Country_Hours__c> GetEmployeeCountryHour(Employee_Details__c employee, list<string> yearMonthList){
    	return RM_DataAccessor.GetEmployeeCountryHour(employee,yearMonthList);
    }
    public static void InsertCountryHour(Employee_Details__c employee, List<string> yearMonths){
    	RM_DataAccessor.InsertCountryHour(employee,yearMonths);
    }
    public static void InsertCountryHourByCountryID( string countryRID, List<string> yearMonths){
    	RM_DataAccessor.InsertCountryHourByCountryID( countryRID,  yearMonths);
    }
    /*Location_Holiday_Schedule__c*/
    
    public static list<Location_Holiday_Schedule__c> getLstHolidayScheduleByLocationCodeDateRange(string lcode, Date startDate, Date endDate){
    	return RM_DataAccessor.getLstHolidayScheduleByLocationCodeDateRange(lcode, startDate, endDate);
    }
    
    /* Save Search */
    public static list<Root_Folder__c> GetRootFolder(){
    	return RM_DataAccessor.GetRootFolder();
    }
    public static list<Sub_Folder__c> GetCustomSubFolderList(string rId, string userId){        
        return  RM_DataAccessor.GetCustomSubFolderList(rId, userId);
    }
    public static list<Sub_Folder__c> GetDefaultSubFolderList(string rId, string userId){        
        return RM_DataAccessor.GetDefaultSubFolderList(rId, userId);
    }
    public static Sub_Folder__c GetSubFolderbyID(string subFolderId){
        return RM_DataAccessor.GetSubFolderbyID(subFolderId);
    }
    public static Sub_Folder__c GetSubFolderID(string subFolder, string path, string userID){
        return RM_DataAccessor.GetSubFolderID(subFolder, path, userID);
    }
    public static list<Saved_Search__c> getSavedSearchList(string subID){
        return RM_DataAccessor.getSavedSearchList(subID);
    }
    public static list<Saved_Search__c> getSavedSearchName(string searchName, string userID, string subID){
        return RM_DataAccessor.getSavedSearchName(searchName,userID,subID);
    }
    public static Saved_Search__c getSavedSearch(string searchName, string userID, string subID){
        return RM_DataAccessor.getSavedSearch(searchName,userID,subID);
    }
    public static Saved_Search__c getSavedSearchByID(string saveID){
        return RM_DataAccessor.getSavedSearchByID(saveID);
    }
    public static list<Search_Detail__c> getSavedSearchDetail(string saveSearchID){
        return RM_DataAccessor.getSavedSearchDetail(saveSearchID);
    }       
    public static Saved_Search__c getSavedSearchwithRoot(string saveSearchID){
        return RM_DataAccessor.getSavedSearchwithRoot(saveSearchID);
    }
    public static Boolean insertDefaultSubFolder(string userID){
        try{
            list<Root_Folder__c> rootObjList = GetRootFolder();
            list<Sub_Folder__c> newsubList = new list<Sub_Folder__c>();
            for (Root_Folder__c rootObj : rootObjList){
                list<Sub_Folder__c>subList = GetDefaultSubFolderList(rootObj.id, userID);
                if (subList.size() == 0){
                    string defFolder = '';
                    if (rootObj.name == 'Resources')
                        defFolder = 'My Resource Searches';
                    else if (rootObj.name == 'Projects')
                        defFolder = 'My Project Searches';
                    else if (rootObj.name == 'Assignments')
                        defFolder = 'My Assignment Searches';
                    Sub_Folder__c  newSub = new Sub_Folder__c(name = defFolder, Custom__c = false, root__c = rootObj.id, user__c =UserInfo.getUserID(), Sub_Folder_Unique_Key__c=defFolder+rootObj.id+UserInfo.getUserID());
                    newsubList.add(newSub);
                    
                }
            }
            if (newsubList.size()> 0)
                insert newsubList;
            return true;
        }
        catch (Exception e){
            return false;
        }
    } 
    /* job class desc*/
    public static list<string> getClinicalJobClass(){
    	return RM_DataAccessor.getClinicalJobClass();
    } 
     /************************************
    Help
    ************************************/
    public static list<Help__c> GetHelpContent(string helpName){
    	return RM_DataAccessor.GetHelpContent(helpName);
    }
    
}