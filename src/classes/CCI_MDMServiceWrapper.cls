public class CCI_MDMServiceWrapper {

	private static String ENDPOINT = '';
	private static Integer TIMEOUT = 0;
	private static List<String> errors;
	
	static {
		initSettings();
	}	
	
	/****************************** SVT/ECRF Methods ******************************/
	/*public static CCI_MDMService.svtListResponse getSvtVOsByProtocol(CCI_MDMService.protocolVO protocolVO) {
		CCI_MDMService.svtListResponse response = null;
		if (protocolVO != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getSvtVOsByProtocol(protocolVO);
				system.debug('---------------- SvtVO retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- SvtVO retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.visitMappingResponse getSvtEcrfMappingByProtocolPraId(CCI_MDMService.protocolVO protocolVO, String praProjectId) {
		CCI_MDMService.visitMappingResponse response = null;
		if (protocolVO != null && praProjectId != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getSvtEcrfMappingByProtocolPraId(protocolVO, praProjectId);
				system.debug('---------------- SvtEcrf mapping retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- SvtEcrf mapping retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.visitMappingResponse getSvtEcrfMappingBySvtPraId(CCI_MDMService.svtVO svtVO, String praProjectId) {
		CCI_MDMService.visitMappingResponse response = null;
		if (svtVO != null && praProjectId != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getSvtEcrfMappingBySvtVOPraId(svtVO, praProjectId);
				system.debug('---------------- SvtEcrf mapping retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- SvtEcrf mapping retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.svtEcrfResponse getSvtsEcrfsFromProtocolPraId(CCI_MDMService.protocolVO protocolVO, String praProjectId) {
		CCI_MDMService.svtEcrfResponse response = null;
		if (protocolVO != null && praProjectId != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getSvtsEcrfsFromProtocolPraId(protocolVO, praProjectId);
				system.debug('---------------- Svt/Ecrf retrievals successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Svt/Ecrf retrievals failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}*/
	
	public static CCI_MDMService.svtEcrfMappingResponse getSvtsEcrfsMappingsFromProtocolPraId(CCI_MDMService.protocolVO protocolVO, String praProjectId) {
		CCI_MDMService.svtEcrfMappingResponse response = null;
		if (protocolVO != null && praProjectId != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getSvtsEcrfsAndMappingsFromProtocolPraId(protocolVO, praProjectId);
				system.debug('---------------- Svt/Ecrf and mapping retrievals successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Svt/Ecrf and mapping retrievals failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	/*public static CCI_MDMService.mdmCrudResponse saveSvtEcrfMappingFromVO(CCI_MDMService.ecrfMappingVO ecrfMappingVO) {
		CCI_MDMService.mdmCrudResponse response = null;
		if (ecrfMappingVO != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.saveSvtEcrfMappingFromVO(ecrfMappingVO);
				system.debug('---------------- Svt/Ecrf save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Svt/Ecrf save failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}*/
	
	public static CCI_MDMService.mdmCrudResponse saveSvtEcrfMappingFromVOs(List<CCI_MDMService.ecrfMappingVO> ecrfMappingVOs) {
		CCI_MDMService.mdmCrudResponse response = null;
		if (ecrfMappingVOs != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.saveSvtEcrfMappingFromVOs(ecrfMappingVOs);
				system.debug('---------------- Svt/Ecrf save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Svt/Ecrf save failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.mdmCrudResponse deleteSvtEcrfMappingFromVOs(List<CCI_MDMService.ecrfMappingVO> ecrfMappingVOs) {
		CCI_MDMService.mdmCrudResponse response = null;
		if (ecrfMappingVOs != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.deleteSvtEcrfMappingFromVOs(ecrfMappingVOs);
				system.debug('---------------- Svt/Ecrf delete successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Svt/Ecrf delete failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}

	/****************************** Protocol Methods ******************************/
	/*public static CCI_MDMService.protocolResponse getProtocolVOBySystemId(String systemId) {
		CCI_MDMService.protocolResponse response = null;
		if (systemId != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getProtocolVOBySystemId(systemId);
				system.debug('---------------- ProtocolVO retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- ProtocolVO retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}*/
	
	public static CCI_MDMService.protocolResponse getProtocolVOByClientProtNum(String clientProtNum) {
		CCI_MDMService.protocolResponse response = null;
		if (clientProtNum != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getProtocolVOByClientProtNum(clientProtNum);
				system.debug('---------------- ProtocolVO retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- ProtocolVO retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.protocolListResponse getProtocolsLikeClientProtocolNum(String clientProtNum) {
		CCI_MDMService.protocolListResponse response = null;
		if (clientProtNum != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getProtocolVOsLikeClientProtNum(clientProtNum);
				system.debug('---------------- ProtocolVO retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- ProtocolVO retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	} 
	
	/*public static CCI_MDMService.protocolListResponse getProtocolsNotMappedToSite() {
		CCI_MDMService.protocolListResponse response = null;
		CCI_MDMService.MdmServicePort service = getService();
		try {
			response = service.getProtocolsNotMappedToSite();
			system.debug('---------------- ProtocolVOs retrieval successfull -----------------');
		} catch (Exception e) {
			errors = new List<String>();
			errors.add(e.getMessage());
			system.debug('---------------- ProtocolVOs retrieval failed -----------------');
			system.debug('----- Error: ' + e.getMessage());
			system.debug('----- Stack trace: ' + e.getStackTraceString());
			system.debug('----- Cause: ' + e.getCause());
		}
		system.debug('----- Response is: ' + response);
			
		return response;
	}
	
	public static CCI_MDMService.protocolListResponse getProtocolsNotMappedToSiteBySystemId(String systemId) {
		CCI_MDMService.protocolListResponse response = null;
		if (systemId != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getProtocolsNotMappedToSiteBySystemId(systemId);
				system.debug('---------------- ProtocolVOs retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- ProtocolVOs retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.valueResponse getEDocsUrlForProtocol(CCI_MDMService.ProtocolVO protocolVO) {
		CCI_MDMService.valueResponse response = null;
		if (protocolVO != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getEDocsUrlForProtocol(protocolVO);
				system.debug('---------------- EDocs URL retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- EDocs URL retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.valueResponse getEDocsUrlByClientProtNum(String clientProtNum) {
		CCI_MDMService.valueResponse response = null;
		if (clientProtNum != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getEDocsUrlByClientProtNumResponse(clientProtNum);
				system.debug('---------------- EDocs URL retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- EDocs URL retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.crsInstanceAttribListResponse getDatabaseAttribsByProtProjId(Long protProjId) {
		CCI_MDMService.crsInstanceAttribListResponse response = null;
		if (protProjId != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getDatabaseAttribsByProtProjId(protProjId);
				system.debug('---------------- Instance attributes retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Instance attributes retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.mdmCrudResponse saveProtocolFromVO(CCI_MDMService.protocolVO protocolVO) {
		CCI_MDMService.mdmCrudResponse response = null;
		if (protocolVO != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.saveProtocolFromVO(protocolVO);
				system.debug('---------------- Protocol save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Protocol save failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.mdmCrudResponse deleteProtocolFromVO(CCI_MDMService.protocolVO protocolVO) {
		CCI_MDMService.mdmCrudResponse response = null;
		if (protocolVO != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.deleteProtocolFromVO(protocolVO);
				system.debug('---------------- Protocol delete successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Protocol delete failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.mdmCrudResponse saveRegionMappingForProtocolResponse(CCI_MDMService.regionVO regionVO) {
		CCI_MDMService.mdmCrudResponse response = null;
		if (regionVO != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.saveRegionMappingForProtocolResponse(regionVO);
				system.debug('---------------- Region save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Region save failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}*/
	
	
	/******************************   Site Methods   ******************************/	
	/*public static CCI_MDMService.siteMappingListResponse getSiteMappingVOsForSite(CCI_MDMService.siteVO siteVO) {
		CCI_MDMService.siteMappingListResponse response = null;
		if (siteVO != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getSiteMappingVOsForSite(siteVO);
				system.debug('---------------- Site mapping retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Site mapping retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
		
	public static CCI_MDMService.siteMappingListResponse getSubjectSiteMapping(CCI_MDMService.siteVO siteVO, String studyNo, Integer siteKey) {
		CCI_MDMService.siteMappingListResponse response = null;
		if (siteVO != null && studyNo != null && siteKey != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getSubjectSiteMapping(siteVO, studyNo, siteKey);
				system.debug('---------------- Subject site mapping retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Subject site mapping retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.siteMappingResponse getEDocsMappingForSystemId(CCI_MDMService.protocolVO protocolVO, String systemId) {
		CCI_MDMService.siteMappingResponse response = null;
		if (protocolVO != null && systemId != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getEDocsMappingForSystemId(protocolVO, systemId);
				system.debug('---------------- Site mapping retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Site mapping retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.siteMappingResponse getEDocsMappingForCTMSSystemId(CCI_MDMService.protocolVO protocolVO, String systemId) {
		CCI_MDMService.siteMappingResponse response = null;
		if (protocolVO != null && systemId != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getEDocsMappingForCTMSSystemId(protocolVO, systemId);
				system.debug('---------------- Site mapping retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Site mapping retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.siteMappingResponse getEDocsMappingForTrialSiteId(CCI_MDMService.protocolVO protocolVO, String trialSiteId) {
		CCI_MDMService.siteMappingResponse response = null;
		if (protocolVO != null && trialSiteId != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getEDocsMappingForTrialSiteId(protocolVO, trialSiteId);
				system.debug('---------------- Site mapping retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Site mapping retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.siteMappingListResponse getEDocsMappingFuzzyLookupForTrialSiteId(CCI_MDMService.protocolVO protocolVO, String trialSiteId) {
		CCI_MDMService.siteMappingListResponse response = null;
		if (protocolVO != null && trialSiteId != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.getEDocsMappingFuzzyLookupForTrialSiteId(protocolVO, trialSiteId);
				system.debug('---------------- Site mapping retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Site mapping retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.mdmCrudResponse saveSiteMapping(CCI_MDMService.siteMappingVO siteMappingVO, String toSite, String fromSite) {
		CCI_MDMService.mdmCrudResponse response = null;
		if (siteMappingVO != null && toSite != null && fromSite != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.saveSiteMapping(siteMappingVO, toSite, fromSite);
				system.debug('---------------- Site mapping save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Site mapping delete failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.mdmCrudResponse saveUrlMappingForProtocolAndSite(CCI_MDMService.siteVO siteVO) {
		CCI_MDMService.mdmCrudResponse response = null;
		if (siteVO != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.saveUrlMappingForProtocolAndSite(siteVO);
				system.debug('---------------- URL Site mapping save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- URL Site mapping delete failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static CCI_MDMService.mdmCrudResponse saveEDocsMappingForSite(CCI_MDMService.siteMappingVO siteMappingVO) {
		CCI_MDMService.mdmCrudResponse response = null;
		if (siteMappingVO != null) {
			CCI_MDMService.MdmServicePort service = getService();
			try {
				response = service.saveEDocsMappingForSite(siteMappingVO);
				system.debug('---------------- EDocs Site mapping save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- EDocs Site mapping delete failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}*/
	

    public static List<String> getErrors() {
        return errors;
    }
	
	private static CCI_MDMService.MdmServicePort getService() {
		CCI_MDMService.MdmServicePort service = new CCI_MDMService.MdmServicePort();
		service.endpoint_x = ENDPOINT;
		service.timeout_x = TIMEOUT;
		return service;
	}
	
	private static void initSettings() {
		Map<String, MDMServiceSettings__c> settings = MDMServiceSettings__c.getAll();
		ENDPOINT = settings.get('ENDPOINT').Value__c;
		Integer to = Integer.valueOf(settings.get('TIMEOUT').Value__c);
		if (to != null && to > 0) {
			TIMEOUT = to;
		} 
		system.debug('--------- Using following Custom Settings for MDMServiceSettings ---------');
        system.debug('ENDPOINT:'+ENDPOINT);
        system.debug('TIMEOUT:'+TIMEOUT);
        system.debug('----------------------- MDMServiceSettings END ----------------------------');
	}
}