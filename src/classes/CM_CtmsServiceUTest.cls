/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CM_CtmsServiceUTest {

    private static final String dummyDate = '2014-05-06T21:43:19';
    
    static list<MuleServicesCS__c> serviceSettings;
    
    static void init() {
        serviceSettings = new MuleServicesCS__c[] {
            new MuleServicesCS__c(name = 'ENDPOINT', Value__c = 'ENDPOINT'),
            new MuleServicesCS__c(name = 'TIMEOUT', Value__c = '60000'),
            new MuleServicesCS__c(name = 'CtmsCTAService', Value__c = 'CtmsCTAService')
        };
        insert serviceSettings;
    }

   // These testmethod is just to check that the mocking is working
    // Assertions will be made for classes that use the service wrapper
    static testmethod void testGenerateResponses_POSITIVE() {
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new CM_CtmsServiceTestMocks(false));
        getAccountsTest();
        getContactsTest();
        getContractStatusesTest();
        getContractTypesTest();
        getSiteContactsTest();
        getSiteContractsTest();
        getSitesTest();
        getSiteTeamsTest();
        Test.stopTest();
    }
    
    static testmethod void testGenerateResponses2_POSITIVE() {
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new CM_CtmsServiceTestMocks(false));
        getAccountsTest();
        getContactsTest();
        getContractStatusesTest();
        getContractTypesTest();
        getSiteContactsTest();
        getSiteContractsTest();
        getSitesTest();
        getSiteTeamsTest();
        upsertSiteActivitiesTest();
        upsertSiteContractsTest();
        CM_CtmsServiceWrapper.getErrors();
        Test.stopTest();
    }
    
    static testmethod void testGenerateResponses_NEGATIVE() {
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new CM_CtmsServiceTestMocks(true));
        getAccountsTest();
        getContactsTest();
        getContractStatusesTest();
        getContractTypesTest();
        getSiteContactsTest();
        getSiteContractsTest();
        getSitesTest();
        getSiteTeamsTest();
        Test.stopTest();
    }
    
    static testmethod void testGenerateResponses2_NEGATIVE() {
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new CM_CtmsServiceTestMocks(true));
        getAccountsTest();
        getContactsTest();
        getContractStatusesTest();
        getContractTypesTest();
        getSiteContactsTest();
        getSiteContractsTest();
        getSitesTest();
        getSiteTeamsTest();
        upsertSiteActivitiesTest();
        upsertSiteContractsTest();
        CM_CtmsServiceWrapper.getErrors();
        Test.stopTest();
    }
    
    /*
    *   The below methods when invoked directly (with appropriate input parameters) will make the web service call.
    *   When invoked in test execution context, the callouts will be mocked by the TestMocks.
    *
    */
    
    public static void getSitesTest() {
        CM_CtmsService_Site.site site = new CM_CtmsService_Site.site();
        site.city = 'Somers Point';
        site.country = 'UNITED STATES';
        //CM_CtmsServiceWrapper.getSites(site);
    }
    
    public static void getSiteContractsTest() {
        CM_CtmsService_SiteContract.site site = new CM_CtmsService_SiteContract.site();
        site.id = '1-APKNY8';
        //CM_CtmsServiceWrapper.getSiteContracts(site);
    }
    
    public static void getContractStatusesTest() {
        //CM_CtmsServiceWrapper.getContractStatuses();
    }
    
    public static void getContractTypesTest() {
        //CM_CtmsServiceWrapper.getContractTypes();
    }
    
    public static void getAccountsTest() {
        CM_CtmsService_Account.account account = new CM_CtmsService_Account.account();
        account.id = '1-2B5DJY';
        //CM_CtmsServiceWrapper.getAccounts(account);
    }
    
    public static void getContactsTest() {
        CM_CtmsService_Contact.contact contact = new CM_CtmsService_Contact.contact();
        contact.firstName = 'Kathleen';
        contact.lastName = 'Allen';
        //CM_CtmsServiceWrapper.getContacts(contact);
    }
    
    public static void getSiteContactsTest() {
        CM_CtmsService_SiteContact.site site = new CM_CtmsService_SiteContact.site();
        site.id = '1-3T95ZM';
        //CM_CtmsServiceWrapper.getSiteContacts(site);
    }
    
    public static void getSiteTeamsTest() {
        CM_CtmsService_SiteTeam.site site = new CM_CtmsService_SiteTeam.site();
        site.id = '1-3T95ZM';
        //CM_CtmsServiceWrapper.getSiteTeams(site);
    }
    
    public static void upsertSiteActivitiesTest() {
        String validUser = 'kezerrenata';
        //CM_CtmsServiceWrapper.upsertSiteActivities(getDummySiteActivities('1-3T95ZM',validUser,2), validUser);
    }
    
    public static void upsertSiteContractsTest() {
        String validUser = 'kezerrenata';
        //CM_CtmsServiceWrapper.upsertSiteContracts(getDummySiteContracts('1-3T95ZM',validUser,2), validUser);
    }
    
    private static CM_CtmsService_SiteContract.site getDummySiteContracts(String siteID, String user, Integer size) {
        CM_CtmsService_SiteContract.site site = new CM_CtmsService_SiteContract.site();
        site.id = siteID;
        site.contracts = new CM_CtmsService_SiteContract.contracts();
        site.contracts.contract = new list<CM_CtmsService_SiteContract.contract>();
        
        CM_CtmsService_SiteContract.contract contract = null;
        CM_CtmsService_SiteContract.status status = null;
        
        for (Integer i = 1; i <= size; i++) {
            contract = new CM_CtmsService_SiteContract.contract();
            //contract.id = 'CONTRACT_ID_'+i;
            contract.ctaId = 'CTA_ID_'+i;
            contract.fullyExecutedDate = dummyDate;
            contract.contractType = 'Budget';
            contract.plannedFinalExecutionDate = dummyDate;
            contract.account = 'Országos Onkológiai Intézet';
            contract.accountId = '1-2B5DJY';
            contract.negotiationCompleteDate = dummyDate;
            contract.budgetApprovedDate = dummyDate;
            contract.budgetReceivedDate = dummyDate;
            contract.budgetSentDate = dummyDate;
            contract.comments = 'COMMENTS_'+i;
            contract.contractHolder = 'CONTRACT_HOLDER_'+i;
            contract.firstDraftSentDate = dummyDate;
            contract.firstSiteCommentsDate = dummyDate;
            contract.contractName = 'CONTRACT_NAME_'+i;
            contract.partiallyExecutedDate = dummyDate;
            contract.sponsorName = 'Ortho-McNeil Janssen Scientific Affairs, LLC';
            contract.team = user;
            contract.statuses = new CM_CtmsService_SiteContract.statuses();
            contract.statuses.status = new list<CM_CtmsService_SiteContract.status>();
            for (Integer j = 1; j <= size; j++) {
                status = new CM_CtmsService_SiteContract.status();
                //status.id = 'STATUS_ID_'+i+'_'+j;
                status.comment = 'COMMENT_'+i+'_'+j;
                status.status = 'At PRA-CA';
                status.date_x = dummyDate;
                contract.statuses.status.add(status);
            }
            site.contracts.contract.add(contract);
        }
        return site;
    }
    
    private static CM_CtmsService_SiteActivity.site getDummySiteActivities(String siteID, String user, Integer size) {
        CM_CtmsService_SiteActivity.site site = new CM_CtmsService_SiteActivity.site();
        site.id = siteID;
        site.activities = new CM_CtmsService_SiteActivity.activities();
        site.activities.activity = new list<CM_CtmsService_SiteActivity.activity>();
        
        CM_CtmsService_SiteActivity.activity activity = null;
        CM_CtmsService_SiteActivity.contact contact = null;
        for (Integer i = 1; i <= size; i++) {
            activity = new CM_CtmsService_SiteActivity.activity();
            //activity.id = 'ID_'+i;
            activity.activity = 'General';
            activity.comment = 'COMMENT_'+i;//Length=1499
            activity.createdBy = user;
            activity.description = 'DESC_'+i;
            activity.completedDate = dummyDate;
            activity.completed = 'Y';
            activity.subType = 'CTA';
            activity.callerEmployeeFlag = 'Y';//Set only for Outbound calls
            //activity.phoneCallerFirstName = 'PHONE_CALLER_FNAME_'+i;
            //activity.phoneCallerLastName = 'PHONE_CALLER_LNAME_'+i;
            activity.phoneCallerId = user;
            activity.planned2 = dummyDate;
            activity.plannedCompletion = dummyDate;
            activity.started = dummyDate;
            activity.status = 'Done';
            activity.activityType = 'Call';
            activity.contacts = new CM_CtmsService_SiteActivity.contacts();
            activity.contacts.contact = new list<CM_CtmsService_SiteActivity.contact>();
            for (Integer j = 1; j <= size; j++) {
                contact = new CM_CtmsService_SiteActivity.contact();
                contact.id = '1-3SMI2U';
                //contact.firstName = 'FNAME_'+i+'_'+j;
                //contact.lastName = 'LNAME_'+i+'_'+j;
                //contact.employeeFlag = 'EMP_FLAG_'+i+'_'+j;
                activity.contacts.contact.add(contact);
            }
            site.activities.activity.add(activity);
        }
        return site;
    }
}