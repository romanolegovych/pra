/**
 * @author Konstantin Ermolenko
 * @Date 11/26/2014
 * @Description contains tests for PAWS_ProjectFlowGeneratorController class
 */
 
@isTest(Seealldata=false)
@testvisible
private class PAWS_ProjectFlowGenerator_Tests
{
    private static STSWR1__Flow__c WRFlow {get; set;}
    private static STSWR1__Flow_Step_Junction__c ServiceWRStep {get; set;}
    
    private static PBB_TestUtils PBBTestUtils
    {
        get
        {
            if (PBBTestUtils == null)
            {
                PBBTestUtils = new PBB_TestUtils();
            }
            return PBBTestUtils;
        }
        set;
    }
    
    static testMethod void testProjectFlowGenerator1()
    {
        initWRFlow();
        initScenarioModel();
        initWRModel();
        initScenario();
        
        Test.startTest();
        
	        PageReference pageRef = Page.PAWS_ProjectFlowGenerator;
	        pageRef.getParameters().put('scenarioId', String.valueOf(PBBTestUtils.scen.Id));
	        Test.setCurrentPage(pageRef);
	        
	        PAWS_ProjectFlowGeneratorController controller = new PAWS_ProjectFlowGeneratorController();
	        
	        controller.Init();
	        Boolean isInit = controller.IsInit;
	        isInit = controller.IsInit; //test 2nd "return"
	        
	        controller.GetActionsListJSON();
	        controller.GeneratePAWSProject();
	        controller.UpdatePAWSProject();
	        controller.GenerateProjectWRFlow_BeforeClone();
	        controller.GenerateProjectWRFlow_Clone();
	        controller.GenerateProjectWRFlow_AfterFlowClone();
	        controller.GenerateProjectWRFlow_AfterClone();
	        
	        controller.Finalize();
	        
	        controller.RestartProcess();
	        controller.ResumeProcess();
	        controller.Init(); //test state load
	        
	        controller.RemoveProgressState();
	        controller.Init();
	        
	        controller.RecordIdsToDelete = controller.CurrentPAWSSiteIDInProcess;
	        controller.RemoveWRFlow();
	        
	        controller.Rollback();
	        controller.HandleException(new PAWS_Utilities.PAWSException('test'));
	        
	        Integer CountSubflowsLeft = controller.CountSubflowsLeft;
        
        Test.stopTest();
    }
    
    static testMethod void testProjectFlowGenerator2()
    {
        initWRFlow();
        initScenarioModel();
        initWRModel();
        initScenario();
        
        Test.startTest();
        
	        PageReference pageRef = Page.PAWS_ProjectFlowGenerator;
	        pageRef.getParameters().put('scenarioId', String.valueOf(PBBTestUtils.scen.Id));
	        Test.setCurrentPage(pageRef);
	        
	        PAWS_ProjectFlowGeneratorController controller = new PAWS_ProjectFlowGeneratorController();
	        
	        controller.Init();
	        controller.GeneratePAWSProject();
	        controller.GenerateProjectWRFlow_BeforeClone();
	        controller.GenerateProjectWRFlow_Clone();
	        controller.GenerateProjectWRFlow_AfterFlowClone();
	        controller.GenerateProjectWRFlow_AfterClone();
	        
	        controller.CurrentCountryIDInProcess = (new List<String>(controller.GetSelectedCountries().keySet())).get(0);
	        controller.CurrentCountryInProcess = 'test'; //controller.SelectedCountries.get(controller.CurrentCountryIDInProcess);
	        controller.GenerateCountryWRFlow();
	        controller.GenerateCountryWRFlow_ActivateTemplate();
	        controller.GenerateCountryWRFlow_ProcessSelectedServices();
	        
	        controller.CurrentSiteNameInProcess = '1';
	        controller.CurrentSiteWeekDateInProcess = '2000-1-1';
	        controller.GenerateSiteWRFlow();
	        controller.GenerateSiteWRFlow_ActivateTemplate();
	        controller.GenerateSiteWRFlow_ProcessSelectedServices();
	        
	        controller.GetRemoveProjectActionsListJSON();
        
        Test.stopTest();
    }
    
    @testvisible
    private static void initWRFlow()
    {
        STSWR1__Flow__c mainFlow = new STSWR1__Flow__c(
            Name = 'MainFlow',
            STSWR1__Type__c = 'Template',
            STSWR1__Status__c = 'Inactive',
            STSWR1__Start_Type__c = 'Manual',
            STSWR1__Object_Type__c = 'ecrf__c'
        );
        
        STSWR1__Flow__c countryFlow = new STSWR1__Flow__c(
            Name = 'Country Flow',
            STSWR1__Type__c = 'Template',
            STSWR1__Status__c = 'Inactive',
            STSWR1__Start_Type__c = 'Triggered by another flow',
            STSWR1__Object_Type__c = 'paws_project_flow_country__c' 
        );
        
        STSWR1__Flow__c siteFlow = new STSWR1__Flow__c(
            Name = 'Site Flow',
            STSWR1__Type__c = 'Template',
            STSWR1__Status__c = 'Inactive',
            STSWR1__Start_Type__c = 'Triggered by another flow',
            STSWR1__Object_Type__c = 'paws_project_flow_site__c' 
        );
        
        insert new List<STSWR1__Flow__c>{mainFlow, countryFlow, siteFlow};
        
        WRFlow = mainFlow;
        
        STSWR1__Flow_Swimlane__c mainFlowSwimlane = new STSWR1__Flow_Swimlane__c(
            Name = 'Swimlane1',
            STSWR1__Flow__c = mainFlow.Id
        );
        
        STSWR1__Flow_Swimlane__c countryFlowSwimlane = new STSWR1__Flow_Swimlane__c(
            Name = 'Swimlane2',
            STSWR1__Flow__c = countryFlow.Id
        );
        
        STSWR1__Flow_Swimlane__c siteFlowSwimlane = new STSWR1__Flow_Swimlane__c(
            Name = 'Swimlane3',
            STSWR1__Flow__c = siteFlow.Id
        );
        
        insert new List<STSWR1__Flow_Swimlane__c>{mainFlowSwimlane, countryFlowSwimlane, siteFlowSwimlane};
        
        STSWR1__Flow_Step_Junction__c mainFlowStep = new STSWR1__Flow_Step_Junction__c(
            Name = 'Main Flow Step 1',
            STSWR1__Flow__c = mainFlow.Id,
            STSWR1__Flow_Swimlane__c = mainFlowSwimlane.Id,
            STSWR1__Index__c = 0,
            STSWR1__Is_First_Step__c = true,
            STSWR1__Duration__c = 1
        );
        
        STSWR1__Flow_Step_Junction__c countryFlowStep = new STSWR1__Flow_Step_Junction__c(
            Name = 'Country Flow Step 1',
            STSWR1__Flow__c = countryFlow.Id,
            STSWR1__Flow_Swimlane__c = countryFlowSwimlane.Id,
            STSWR1__Index__c = 0,
            STSWR1__Is_First_Step__c = true,
            STSWR1__Duration__c = 1
        );
        
        STSWR1__Flow_Step_Junction__c siteFlowStep = new STSWR1__Flow_Step_Junction__c(
            Name = 'Site Flow Step 1',
            STSWR1__Flow__c = siteFlow.Id,
            STSWR1__Flow_Swimlane__c = siteFlowSwimlane.Id,
            STSWR1__Index__c = 0,
            STSWR1__Is_First_Step__c = true,
            STSWR1__Duration__c = 1
        );
        
        insert new List<STSWR1__Flow_Step_Junction__c>{mainFlowStep, countryFlowStep, siteFlowStep};
        ServiceWRStep = countryFlowStep;
        
        STSWR1__Flow_Step_Property__c countryConnectionPoint = new STSWR1__Flow_Step_Property__c(
            Name = 'Connection Point',
            STSWR1__Type__c = 'Text',
            STSWR1__Value__c = 'MainFlow:Main Flow Step 1',
            STSWR1__Flow_Step__c = countryFlowStep.Id
        );
        
        STSWR1__Flow_Step_Property__c siteConnectionPoint = new STSWR1__Flow_Step_Property__c(
            Name = 'Connection Point',
            STSWR1__Type__c = 'Text',
            STSWR1__Value__c = 'Country Flow:Country Flow Step 1',
            STSWR1__Flow_Step__c = siteFlowStep.Id
        );
        
        insert new List<STSWR1__Flow_Step_Property__c>{countryConnectionPoint, siteConnectionPoint};
        
        STSWR1__Item__c mainFlowItem = new STSWR1__Item__c(
            Name = mainFlow.Id,
            STSWR1__Source_Flow__c = mainFlow.Id
        );
        
        STSWR1__Item__c countriesFolder = new STSWR1__Item__c(
            Name = 'Countries'
        );
        
        insert new List<STSWR1__Item__c>{mainFlowItem, countriesFolder};
        
        STSWR1__Item__c countryGenericFolder = new STSWR1__Item__c(
            Name = 'Generic',
            STSWR1__Parent__c = countriesFolder.Id
        );
        insert countryGenericFolder;
        
        STSWR1__Item__c countryFlowItem = new STSWR1__Item__c(
            Name = countryFlow.Id,
            STSWR1__Source_Flow__c = countryFlow.Id,
            STSWR1__Parent__c = countryGenericFolder.Id
        );
        
        STSWR1__Item__c siteFlowItem = new STSWR1__Item__c(
            Name = siteFlow.Id,
            STSWR1__Source_Flow__c = siteFlow.Id,
            STSWR1__Parent__c = countryGenericFolder.Id
        );
        insert new List<STSWR1__Item__c>{countryFlowItem, siteFlowItem};
    }
    
    @testvisible
    private static void initScenarioModel()
    {
        PBBTestUtils.createServiceModelAttributes();
        PBBTestUtils.createServiceAreaAttributes();
        PBBTestUtils.createServiceFunctionAttributes();
        PBBTestUtils.createServiceTaskAttributes();
        
        PBBTestUtils.createCountryAttributes();
    }
    
    @testvisible
    private static void initWRModel()
    {   
        PBBTestUtils.approveServicemodel(PBBTestUtils.smm);
        PBBTestUtils.createWRModelattributes();
        
        PBBTestUtils.Wrm.Flow__c = WRFlow.Id;
        update PBBTestUtils.Wrm;
        
        PBBTestUtils.stfs = new Service_Task_To_Flow_Step__c(
            WR_Model__c = PBBTestUtils.Wrm.Id,
            Service_Tasks__c = PBBTestUtils.St.Id,
            Flow_Step__c = ServiceWRStep.Id
        );
        insert PBBTestUtils.stfs;
    }
    
    private static void initScenario()
    {
        PBBTestUtils.Createwfmproject();
        PBBTestUtils.createBidProject();
        
        //PBBTestUtils.createSRMScenario();
        
        PBBTestUtils.createScenarioAttributes();
        PBBTestUtils.scen.WR_Model__c = PBBTestUtils.Wrm.Id;
        update PBBTestUtils.scen;
        
        PBBTestUtils.CreatePBBScenarioCountry();
        
    }
}