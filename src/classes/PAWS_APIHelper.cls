public with sharing class PAWS_APIHelper
{
	/*************************** CONSTANTS *******************************/
	
	public static Set<String> PAWS_OBJECTS = new Set<String>{
    		'ecrf__c', 
    		'paws_project_flow_country__c', 
    		'paws_project_flow_site__c', 
    		'paws_project_flow_document__c', 
    		'paws_project_flow_agreement__c', 
    		'paws_project_flow_submission__c'
    	};
    	
	/*************************** SEARCHING TEMPLATE *******************************/
	
	public static Map<ID, List<ID>> getTemplates(List<sObject> sources)
	{
		Map<ID, sObject> sourcesMap = new Map<ID, sObject>(sources);
		
		Set<ID> projectIds = new Set<ID>();
		for(sObject source : sources)
		{
			projectIds.add((ID)source.get('PAWS_Project__c'));
		}
		
		Map<ID, ID> projectParentMap = new Map<ID, ID>();
		for(PAWS_Project_Flow_Junction__c flowJunction : [select Project__c, Flow__c, Flow__r.STSWR1__Parent__c, Folder__c from PAWS_Project_Flow_Junction__c where Project__c in :projectIds])
		{
			projectParentMap.put(flowJunction.Flow__r.STSWR1__Parent__c, flowJunction.Project__c);
		}
		
		Map<ID, ID> projectFolderMap = new Map<ID, ID>();
		for(STSWR1__Item__c record : [select STSWR1__Source_Flow__c, STSWR1__Parent__c from STSWR1__Item__c where STSWR1__Source_Flow__c in :projectParentMap.keySet()])
		{
			projectFolderMap.put(projectParentMap.get(record.STSWR1__Source_Flow__c), record.STSWR1__Parent__c);
		}

		Set<String> names1 = new Set<String>();
		Set<String> names2 = new Set<String>();
		for(sObject source : sources)
		{
			String path = (String)source.get('PAWS_Template_Path__c');
			List<String> pathItems = path.split(':');
			if(pathItems.size() > 0) names1.addAll(pathItems[0].split('\\|'));
			if(pathItems.size() > 1) names2.addAll(pathItems[1].split('\\|'));
		}
		
		Schema.DescribeSObjectResult describe = sources.getSObjectType().getDescribe();
		Map<String, List<ID>> templateMap = new Map<String, List<ID>>();
		for(STSWR1__Item__c item : [select STSWR1__Parent__r.Name, STSWR1__Parent__r.STSWR1__Parent__r.Name, STSWR1__Parent__r.STSWR1__Parent__r.STSWR1__Parent__c, STSWR1__Source_Flow__c from STSWR1__Item__c where STSWR1__Parent__r.STSWR1__Parent__r.STSWR1__Parent__c = :projectFolderMap.values() and STSWR1__Parent__r.STSWR1__Parent__r.Name in :names1 and STSWR1__Parent__r.Name in :names2 and STSWR1__Parent__r.STSWR1__Type__c = 'Folder' and STSWR1__Source_Flow__r.STSWR1__Object_Type__c = :describe.getLocalName().toLowerCase()])
		{
			String path = item.STSWR1__Parent__r.STSWR1__Parent__r.STSWR1__Parent__c + ':' + item.STSWR1__Parent__r.STSWR1__Parent__r.Name + ':' + item.STSWR1__Parent__r.Name;
			
			if(!templateMap.containsKey(path)) templateMap.put(path, new List<ID>());
			templateMap.get(path).add(item.STSWR1__Source_Flow__c);
		}

		Map<ID, List<ID>> result = new Map<ID, List<ID>>();
		for(sObject source : sources)
		{
			ID projectFolderId = projectFolderMap.get((ID)source.get('PAWS_Project__c'));
			result.put(source.Id, findTemplateByActualPathV2((String)source.get('PAWS_Template_Path__c'), projectFolderId, templateMap));
		}

		return result;
	}
	
	private static ID findTemplateByActualPath(String path, ID projectFolderId, Map<String, ID> templateMap)
	{
		List<String> pathItems = path.split(':');
		if(pathItems.size() < 2) return templateMap.get(path);
		
		for(String pathItem1 : pathItems[0].split('\\|'))
		{
			for(String pathItem2 : pathItems[1].split('\\|'))
			{
				String actualPath =  projectFolderId + ':' + pathItem1 + ':' + pathItem2;
				if(templateMap.containsKey(actualPath)) return templateMap.get(actualPath);
			}
		}
		
		return null;
	}
	
	private static List<ID> findTemplateByActualPathV2(String path, ID projectFolderId, Map<String, List<ID>> templateMap)
	{
		List<String> pathItems = path.split(':');
		if(pathItems.size() < 2) return templateMap.get(path);
		
		for(String pathItem1 : pathItems[0].split('\\|'))
		{
			for(String pathItem2 : pathItems[1].split('\\|'))
			{
				String actualPath =  projectFolderId + ':' + pathItem1 + ':' + pathItem2;
				if(templateMap.containsKey(actualPath)) return templateMap.get(actualPath);
			}
		}
		
		return null;
	}

	/*************************** ACTIVATION *******************************/
	
	public static ID setupSourceFolder(sObject source, ID projectFolderId)
	{
		ID fodlerId = buildFolders((String)source.get('PAWS_Path__c'), projectFolderId);
		source.put('Folder__c', fodlerId);
		return fodlerId;
	}
	
	private static ID buildFolders(String path, ID parentId)
	{
		ID actualParentId = parentId;
		for(String pathItem : path.split(':'))
		{
			List<STSWR1__Item__c> records = [select Name from STSWR1__Item__c where STSWR1__Parent__c = :actualParentId and Name = :pathItem and STSWR1__Type__c = 'Folder'];
			
			if(records.size() == 0)
			{
				STSWR1__Item__c folder = new STSWR1__Item__c(Name = pathItem, STSWR1__Parent__c = actualParentId);
				insert folder;
				
				actualParentId = folder.Id;
			}else{
				actualParentId = records[0].Id;
			}
		}

		return actualParentId;
	}

	public static sObject loadSource(ID sourceId)
	{
		sObjectType objectType = PAWS_Utilities.getSObjectTypeById(sourceId);
		List<sObject> records = PAWS_Utilities.makeQuery(objectType.getDescribe().getLocalName(), 'Id = \'' + sourceId + '\' for update', null);
		
		return records.size() > 0 ? records[0] : null;
	}
	
	public static Map<String, Object> prepareHistory(ID templateId, Map<String, Object> history)
	{
		if(!history.containsKey(templateId)) history.put(templateId, new Map<String, Object>());
		Map<String, Object> templateHistory = (Map<String, Object>)history.get(templateId);
		if(!templateHistory.containsKey('ids')) initTemplateHistory(templateHistory, templateId);
		
		templateHistory.put('status', 'Activating');
		templateHistory.put('errors', null);
		
		return templateHistory;
	}

	public static String findConnectionPoint(ID templateId)
	{
		List<STSWR1__Flow_Step_Property__c> records = [select STSWR1__Value__c from STSWR1__Flow_Step_Property__c where STSWR1__Flow_Step__r.STSWR1__Flow__c = :templateId and STSWR1__Flow_Step__r.STSWR1__Is_First_Step__c = true and Name = 'Connection Point'];
		return records.size() > 0 ? records[0].STSWR1__Value__c : null;
	}
	
	public static PAWS_Project_Flow_Junction__c loadProjectFlow(ID projectId, String parentFlowName)
	{
		List<PAWS_Project_Flow_Junction__c> records = [select Folder__c, Flow__c, Flow__r.Name, Project__c, Flow_Instance__c from PAWS_Project_Flow_Junction__c where Project__c = :projectId];
		for(PAWS_Project_Flow_Junction__c record : records)
		{
			if(record.Flow__r != null && record.Flow__r.Name.startsWith(parentFlowName)) return record;
		}
		
		return records.size() > 0 ? records[0] : null;
	}
	
	public static ID findParentFlow(ID projectFolderId, ID sourceFolderId, String parentFlowName)
	{
		STSWR1__Item__c sourceFolder = [select STSWR1__Parent__c, STSWR1__Parent__r.STSWR1__Parent__c, STSWR1__Parent__r.STSWR1__Parent__r.STSWR1__Parent__c from STSWR1__Item__c where Id = :sourceFolderId];
		
		List<ID> ids = new List<ID>{projectFolderId};
		if(sourceFolder.STSWR1__Parent__c != null) ids.add(sourceFolder.STSWR1__Parent__c);
		if(sourceFolder.STSWR1__Parent__r.STSWR1__Parent__c != null) ids.add(sourceFolder.STSWR1__Parent__r.STSWR1__Parent__c);
		if(sourceFolder.STSWR1__Parent__r.STSWR1__Parent__r.STSWR1__Parent__c != null) ids.add(sourceFolder.STSWR1__Parent__r.STSWR1__Parent__r.STSWR1__Parent__c);
		
		String flowName = parentFlowName + ' (%';
		List<STSWR1__Item__c> record = [select STSWR1__Source_Flow__c from STSWR1__Item__c where STSWR1__Parent__c in :ids and (STSWR1__Source_Flow__r.Name = :parentFlowName or STSWR1__Source_Flow__r.Name like :flowName)];
		return record.size() > 0 ? record[0].STSWR1__Source_Flow__c : null;
	}

	public static STSWR1__Flow__c cloneTemplates(ID folderId, sObject source, Map<String, Object> history)
	{
		if(history.containsKey('cloneComplete')) return null;
		
		ID flowId = null;
		Map<String, String> idsMap = getHistoryIds(history);
		for(ID oldId : idsMap.keySet())
		{
			if(idsMap.get(oldId) == null)
			{
				flowId = oldId;
				break;
			}
		}	
		
		if(flowId == null)
		{
			history.put('cloneComplete', true);
			return null;
		}else{
			if (!Test.isRunningTest()) PAWS_Utilities.checkLimits(new Map<String, Integer> 
            {
                'Queries' => 20,
                'DMLStatements' => 10
            });
            
			if (!Test.isRunningTest()) new STSWR1.API().call('LimitsService', 'checkLimits', new Map<String, Integer> 
			{
				'Queries' => 20,
				'DMLStatements' => 30
			});
		
			STSWR1__Flow__c flow = [select Name from STSWR1__Flow__c where Id = :flowId];
            
	        STSWR1.ItemsService.FolderID = folderId;
	        flow = STSWR1.FlowImportExportService.getInstance().cloneFlow(flow.Id, flow.Name + ' (' + source.get('Name') + ')');
	        flow.STSWR1__Type__c = 'One Time';
	        flow.STSWR1__Status__c = 'Active';
	        flow.STSWR1__Source_Id__c = source.Id;
	        update flow;
	        
	        idsMap.put(flowId, flow.Id);
	        history.put('ids', idsMap);
			return flow;
		}
	}

	public static void fixActions(Map<String, Object> history)
	{
		Map<String, String> idsMap = getHistoryIds(history);

		List<STSWR1__Flow_Step_Action__c> actions = [select STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Flow_Step__r.STSWR1__Flow__c = :idsMap.values()];
		for(STSWR1__Flow_Step_Action__c action : actions)
		{
			for(String oldId : idsMap.keySet())
			{
				action.STSWR1__Config__c = action.STSWR1__Config__c.replace(oldId, idsMap.get(oldId));
			}
		}
		
		update actions;
		
		callPostImportHandler(idsMap);
	}

	public static STSWR1__Flow_Step_Action__c createStartSubFlowAction(STSWR1__Flow__c flow, sObject source, ID projectFlowId, String stepName, Map<String, Object> history)
	{
		List<STSWR1__Flow_Step_Junction__c> steps = [select Id from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__c = :projectFlowId and Name = :stepName];
		if(steps.size() == 0) throw new PAWS_API.PAWS_APIException('Can not find Step with Name \'' + stepName + '\'!');
		
		String actionConfig = '{"restart":"false","lockStepId":"{PROJECT_STEP_ID}","lockParent":"true","flowObjectName":"{OBJECT_NAME}","flowId":"{FLOW_ID}","conditions":{"logic":"","items":[{"value":"{COUNTRY_ID}","operator":"equals","isFieldExists":true,"index":"1","field":"id"}]}}';
		actionConfig = actionConfig.replace('{PROJECT_STEP_ID}', steps[0].Id).replace('{FLOW_ID}', flow.Id).replace('{COUNTRY_ID}', source.Id).replace('{OBJECT_NAME}', String.valueOf(source.getSObjectType()).toLowerCase());
		
		STSWR1__Flow_Step_Action__c action = new STSWR1__Flow_Step_Action__c(Name=flow.Name);
		action.STSWR1__Flow_Step__c = steps[0].Id;
		action.STSWR1__Type__c = 'Start Sub Flow';
		action.STSWR1__Status__c = 'Active';
		action.STSWR1__Run_Before__c = true;
		action.STSWR1__Config__c = actionConfig;
		
		insert action;
		history.put('startSubFlowAction', action.Id);
		
		return action;
	}
	
	public static STSWR1__Flow_Instance__c startWorkflow(STSWR1__Flow__c flow, sObject source, ID parentFlowId, STSWR1__Flow_Step_Action__c startSubFlowAction)
	{
		STSWR1.API wrApi = new STSWR1.API();
		
		List<STSWR1__Flow_Instance__c> flowInstances = [select STSWR1__Flow__c from STSWR1__Flow_Instance__c where STSWR1__Flow__c = :parentFlowId];
		if(flowInstances.size() == 0) return null;
		
		Map<ID, Map<String, List<STSWR1__Flow_Step_Junction__c>>> progress = (Map<ID, Map<String, List<STSWR1__Flow_Step_Junction__c>>>)wrApi.call('WorkflowService', 'getFlowProgress', flowInstances);
		Map<String, List<STSWR1__Flow_Step_Junction__c>> instanceProgress = progress.get(flowInstances[0].Id);
		
		Map<ID, STSWR1__Flow_Step_Junction__c> stepsMap = new Map<ID, STSWR1__Flow_Step_Junction__c>(instanceProgress.get('pending'));
		if(stepsMap.containsKey(startSubFlowAction.STSWR1__Flow_Step__c)) return null;
		
		stepsMap = new Map<ID, STSWR1__Flow_Step_Junction__c>(instanceProgress.get('future'));
		if(stepsMap.containsKey(startSubFlowAction.STSWR1__Flow_Step__c)) return null;
		
		//START WORKFLOW
		
		List<STSWR1__Flow_Instance__c> instances = STSWR1.WorkflowActivator.getInstance().processObjects(new Map<sObject, List<STSWR1__Flow__c>> {source => new List<STSWR1__Flow__c>{flow}}, true);
		if(instances == null) throw new PAWS_API.PAWS_APIException('Can not start workflow for the flow with id \'' + flow.Id + '\'!');

		STSWR1__Flow_Instance__c instance = instances[0];
		instance.STSWR1__Parent__c = flowInstances[0].Id; 
        instance.STSWR1__Parent_Step__c = startSubFlowAction.STSWR1__Flow_Step__c; 
        instance.STSWR1__Lock_Parent__c = true; 
        instance.STSWR1__Lock_Parent_Step__c = startSubFlowAction.STSWR1__Flow_Step__c; 
		update instance;
		
		return instance;
	}
	
	public static STSWR1__Flow__c loadSourceFlow(ID templateId, sObject source, Map<String, Object> history)
	{
		Map<String, String> idsMap = getHistoryIds(history);
		String sourceFlowId = idsMap.get(templateId);
		List<STSWR1__Flow__c> records = [select Name, STSWR1__Object_Type__c, STSWR1__Type__c, STSWR1__Conditions__c, STSWR1__Conditions_Logic__c, STSWR1__Number_Of_Instances__c, STSWR1__Enable_User_Chatter_Notifications__c, STSWR1__Enable_Object_Chatter_Notifications__c,
                                    (select STSWR1__Flow__r.Name, STSWR1__Object_Id__c from STSWR1__Flow_Instances__r where STSWR1__Object_Id__c = :source.Id)
                                from STSWR1__Flow__c where Id = :sourceFlowId and STSWR1__Status__c = 'Active'];
                                
        return (records.size() > 0 ? records[0] : null);
	}

	public static void rollBack(sObject source, Map<String, Object> history, Exception ex)
	{
		if(history == null) return;
		
		Map<String, String> idsMap = getHistoryIds(history);
		if(idsMap != null && idsMap.size() > 0)
		{
			delete [select Id from STSWR1__Flow__c where Id in :idsMap.values()];
		}
		
		if(history.containsKey('startSubFlowAction'))
		{
			delete [select Id from STSWR1__Flow_Step_Action__c where Id = :(ID)history.get('startSubFlowAction')];
		}
		
		history.clear();
		history.put('status', 'Error');
		history.put('errors', ex.getMessage());
	}
	
	public static sObject commitHistory(sObject source, Map<String, Object> history)
	{
		if(source == null) return source;
		
		String status = 'Active';
		Map<String, String> errors = new Map<String, String>();
		for(String templateId : history.keySet())
		{
			Map<String, Object> templateHistory = (Map<String, Object>)history.get(templateId);
			if(templateHistory.get('status') == 'Error') errors.put(templateId, (String)templateHistory.get('errors'));
			if(status != 'Error') status = (String)templateHistory.get('status');
		}
		
		source.put('Status__c', status);
		source.put('Activation_History__c', (history != null ? JSON.serialize(history) : null));
		source.put('Activation_Errors__c', (errors.size() > 0 ? JSON.serialize(errors) : null));

		STSWR1.AbstractTrigger.Disabled = true;
		update source;
		STSWR1.AbstractTrigger.Disabled = false;
		
		return source;
	}
	
	public static Map<String, Object> getHistory(sObject source, ID templateId)
	{
		Map<String, Object> history = new Map<String, Object>();
		if(!String.isEmpty((String)source.get('Activation_History__c'))) history = (Map<String, Object>)JSON.deserializeUntyped((String)source.get('Activation_History__c')); 
		return history;
	}
	
	public static void initTemplateHistory(Map<String, Object> templateHistory, ID templateId)
	{
		Map<String, String> idsMap = new Map<String, String>{templateId => null};
            
    	Set<String> ids = new Set<String>{templateId};
		while(ids.size() > 0)
		{
			List<STSWR1__Flow_Step_Action__c> actions = [select STSWR1__Flow_Step__c, STSWR1__Flow_Step__r.STSWR1__Flow__c, STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active' and STSWR1__Flow_Step__r.STSWR1__Flow__c = :ids];
			
			ids.clear();
			for(STSWR1__Flow_Step_Action__c action : actions)
			{
				Map<String, Object> config = (Map<String, Object>)JSON.deserializeUntyped(action.STSWR1__Config__c);
				if(config != null && !String.isEmpty((String)config.get('flowId')) && !idsMap.containsKey((String)config.get('flowId')))
				{
					ids.add((String)config.get('flowId'));
					idsMap.put((String)config.get('flowId'), null);
				}
			}
		}
		
		Map<String, String> actualIdsMap = new Map<String, String>();
		for(STSWR1__Flow__c flow : [select Id from STSWR1__Flow__c where Id in :idsMap.keySet()])
			actualIdsMap.put(flow.Id, null);
			
		templateHistory.put('ids', actualIdsMap);
	}
	
	public static Map<String, String> getHistoryIds(Map<String, Object> history)
	{
		Map<String, String> idsMap = new Map<String, String>();
		if(!history.containsKey('ids')) return idsMap;
		
		if(history.get('ids') instanceof Map<String, String>) 
		{
			idsMap = (Map<String, String>)history.get('ids');
		}else 
		{
			Map<String, Object> idsUntypedMap = (Map<String, Object>)history.get('ids');
			for(String oldId : idsUntypedMap.keySet())
				idsMap.put(oldId, (String)idsUntypedMap.get(oldId));
		}
		
		return idsMap;
	}
	
	private static void callPostImportHandler(Map<String, String> flowsMap)
    {
    	if(String.isEmpty(STSWR1__Work_Relay_Config__c.getInstance().STSWR1__Flow_Post_Import_Handler__c)) return;
    	
		Type classType = Type.forName(STSWR1__Work_Relay_Config__c.getInstance().STSWR1__Flow_Post_Import_Handler__c);
        STSWR1.FlowPostImportInterface classObject = (STSWR1.FlowPostImportInterface)classType.newInstance();
        classObject.execute(flowsMap);
    }
}