public with sharing class PAWS_QueueTrigger extends STSWR1.AbstractTrigger
{
	public override void beforeInsert(List<SObject> records)
	{
		List<PAWS_Settings__c> settings = PAWS_Settings__c.getall().values();
		String email = (settings.size() > 0 ? settings.get(0).Error_Notification_Email__c : 'minkodmitro@prahs.com');
		
		for (PAWS_Queue__c rec : (List<PAWS_Queue__c>) records)
		{
			rec.Send_Error_Notification_To__c = (rec.Send_Error_Notification_To__c != null ? rec.Send_Error_Notification_To__c : email);
		}
	}
}