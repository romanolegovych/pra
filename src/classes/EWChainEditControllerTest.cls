@istest public with sharing class EWChainEditControllerTest
{
	@testsetup public static void setup()
	{
		EWBatchTest.setup();
	}
	
	@istest private static void coverController()
	{
		Critical_Chain__c chainRecord = [select Name, Flow__c from Critical_Chain__c limit 1];
		Test.setCurrentPage(Page.EW_Chain_Edit);
		ApexPages.currentPage().getParameters().put('id', chainRecord.Id);
		
		EWChainEditController controller = new EWChainEditController(new ApexPages.StandardController(chainRecord));
		System.assert(controller.path != null);
		System.assert(controller.currentItem != null);
		System.assert(controller.currentSelection != null);
		System.assert(controller.folderOptions != null);
		System.assert(controller.availableFlows != null);
	}
}