@isTest
private class AA_CostRateServiceTest{   
    
       /* Test getting all buf codes */
    static testMethod void getBufCodesTest() {
    
         initTestData ();
         
         Test.startTest();
         
         List<BUF_Code__c> result = AA_CostRateService.getBufCodes();
         
         System.assertNotEquals(result.size(),0);
         
         Test.stopTest();
    }
    
    /* Test getting buf codes by Id */
    static testMethod void getBufCodeTest() {
    
         initTestData ();
         
         Test.startTest();

         List<BUF_Code__c> result = AA_CostRateService.getBufCodes();
         
         for(BUF_Code__c bufcode: result){
             BUF_Code__c buf = AA_CostRateService.getBufCode(bufcode.Id);
             System.assertEquals(buf.Id,bufcode.Id);
         }
         
         Test.stopTest();
    }
     
    /* Test getting all currencies */
    static testMethod void getCurrenciesTest() {
    
         initTestData ();
         
         Test.startTest();
         
         List<Currency__c> result = AA_CostRateService.getCurrencies();
         
         System.assertNotEquals(result.size(),0);
         
         Test.stopTest();
    }
    
   
    /* Test check existing function */
    static testMethod void checkExistingTest() {
    
         initTestData ();
         
         Test.startTest();
         
         List<Cost_Rate__c> result = [SELECT Id,BUF_Code__c,Schedule_Year__c FROM Cost_Rate__c];
         
          
         for(Cost_Rate__c costrate : result){
             System.assertEquals(AA_CostRateService.checkExisting(costrate.Id,'2011').size(),0);
         }
         
        
         Test.stopTest();
    }
    
    /* Test getBufCodeIdbyName() */
    static testMethod void getBufCodeIdbyNameTest() {
    
         initTestData ();
         
         Test.startTest();
        
         List<BUF_Code__c> result = AA_CostRateService.getBufCodeIdbyName('BU1F1');

         System.assertNotEquals(result.size(),0);

         Test.stopTest();
    }
    
     /* Test getCurrencyIdbyName() */
    static testMethod void getCurrencyIdbyNameTest() {
    
         initTestData ();
         
         Test.startTest();
        
         List<Currency__c> result = AA_CostRateService.getCurrencyIdbyName('USD');

         System.assertNotEquals(result.size(),0);

         Test.stopTest();
    }
    
    /* Test BUF CODE retrieval */
    static testMethod void getBufCodeIdTest() {
    
         initTestData ();
         
         Test.startTest();
        
         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
         insert BufCode1;

         List<BUF_Code__c> result = AA_CostRateService.getBufCodeId(businessUnit1,functionCode1);

         System.assertNotEquals(result.size(),0);

         Test.stopTest();
    }
    
    /* Test BUF CODE retrieval */
    static testMethod void getCostRatebyFunctionTest() {
    
         initTestData ();
         
         Test.startTest();
        
         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
         Cost_Rate__c costrate1= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Currency__c=currency1.Id,IsActive__c = true);
         insert costrate1;

         List<Cost_Rate__c> result = AA_CostRateService.getCostRatebyFunction(BufCode1,'2011');

         System.assertNotEquals(result.size(),0);

         Test.stopTest();
    }
    
    
     /* Test Cost Rate retrieval */
    static testMethod void getCostRateIdTest() {
    
         initTestData ();
         
         Test.startTest();
        
         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
         Cost_Rate__c costrate1= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Year_Rate_Applies_To__c='2012',Currency__c=currency1.Id,IsActive__c = true);
         insert costrate1;

         List<Cost_Rate__c> result = AA_CostRateService.getCostRateId(BufCode1.Id,'2011','2012');

         System.assertNotEquals(result.size(),0);

         Test.stopTest();
    }

     /* Test query runner */
    static testMethod void getCostRatesTest() {
    
         initTestData ();
         
         Test.startTest();

         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
         Cost_Rate__c costrate1= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Year_Rate_Applies_To__c='2012',Currency__c=currency1.Id,IsActive__c = true);
         insert costrate1;

         List<Cost_Rate__c> result = AA_CostRateService.getCostRates(BufCode1.Id,'2011');

         System.assertNotEquals(result.size(),0);

         Test.stopTest();
    }
    
    
     /* Test Filters runner */
    static testMethod void getFunctionListByFilterTest() {
    
         initTestData ();
         
         Test.startTest();

         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
         
         
         AA_CostRateService service = new AA_CostRateService();
         List<AggregateResult> result = AA_CostRateService.getFunctionListByFilter('2011',businessUnit1.Id,functionCode1.Id,country.Id,'Y');   
         
         System.assertEquals(result.size(),0);

         Test.stopTest();
    }
    
    
    /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }   
}