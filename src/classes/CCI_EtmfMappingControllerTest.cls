/**
 * Unit test for the CCI_EtmfMappingController code
 */
@isTest
public class CCI_EtmfMappingControllerTest {
	
	static list<MuleServicesCS__c> settings;
	
	private static void init() {
		settings = new MuleServicesCS__c[]{
			new MuleServicesCS__c(Name = 'ENDPOINT', Value__c = 'ENDPOINT'),
			new MuleServicesCS__c(Name = 'TIMEOUT', Value__c = '60000'),
			new MuleServicesCS__c(Name = 'MdmMappingService', Value__c = 'MdmMappingService'),
			new MuleServicesCS__c(Name = 'MdmProtocolService', Value__c = 'MdmProtocolService')
		};
		insert settings;
	}
	
	static testMethod void testSearch() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_ETMFMappingController con = new CCI_ETMFMappingController();
		con.protId = '1';
		con.clientProtNum = 'MOCK prot num';
		con.searchProtocol();
		con.getEtmfVO();
		system.assert(con.etmfVO.size() == 5);
		Test.stopTest();
	}
	
	static testMethod void testPopulateETMFMappingDropDown() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_EtmfMappingController con = new CCI_EtmfMappingController();
		list<selectoption> options = con.getEtmf();
		system.assert(options != null && options.size() == 4);
		Test.stopTest();
	}
	
	static testMethod void testAddEtmfMappingToMDM() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_EtmfMappingController con = new CCI_EtmfMappingController();
		con.saveProtSystemId  = 'MOCK prot system Id';
		con.saveCtmsInstance  = 'MOCK Ctms instance';
		con.saveEtmfInstance  = 'MOCK Etmf Instance';
		con.saveProtSystemId = 'MOCK prot system id';
		con.protocolVO = new MdmProtocolService.protocolVO();
		con.Add();
		Test.stopTest();
	}
	
	static testMethod void testPopulateCTMSMappingDropDown() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_EtmfMappingController con = new CCI_EtmfMappingController();
		list<selectoption> options = con.getCtms();
		system.assert(options != null && options.size() == 4);
		Test.stopTest();
	}
	
	static testMethod void testSearchProtocolSysIds() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(false));
		CCI_EtmfMappingController con = new CCI_EtmfMappingController();
		con.saveProtSystemId = 'PROTOCOL SYS ID';
		con.searchProtocolSysIds();
		system.assertNotEquals(con.protocolVO, null);
		Test.stopTest();
	}
	
	static testMethod void testFindEtmfSiteVO() {
		CCI_EtmfMappingController con = new CCI_EtmfMappingController();
		con.selectedProtSiteId = 'protSiteId';
		con.etmfSiteVOMap = new map<String, MdmMappingService.etmfSiteVO>();
		con.etmfSiteVOMap.put(con.selectedProtSiteId, new MdmMappingService.etmfSiteVO());
		con.findEtmfSiteVO();
		system.assertNotEquals(con.selectedEtmfSiteVO, null);
	}
	
	static testMethod void testSave() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_EtmfMappingController con = new CCI_EtmfMappingController();
		con.selectedProtSiteId = 'protSiteId';
		con.etmfSiteVOMap = new map<String, MdmMappingService.etmfSiteVO>();
		con.etmfSiteVOMap.put(con.selectedProtSiteId, new MdmMappingService.etmfSiteVO());
		con.findEtmfSiteVO();
		con.save();
		//system.assertNotEquals(con.protocolVO, null);
		Test.stopTest();
	}
	
	static testMethod void testDeleteMapping() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_EtmfMappingController con = new CCI_EtmfMappingController();
		con.selectedProtSiteId = 'protSiteId';
		con.etmfSiteVOMap = new map<String, MdmMappingService.etmfSiteVO>();
		con.etmfSiteVOMap.put(con.selectedProtSiteId, new MdmMappingService.etmfSiteVO());
		con.findEtmfSiteVO();
		con.deleteMapping();
		//system.assertNotEquals(con.protocolVO, null);
		Test.stopTest();
	}
}