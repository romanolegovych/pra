public with sharing class RM_MyProjectAlloRoleController {	
	
	public string pMonth{get; set;}
	public string selectedProject{get; set;}
	public Integer n{get; set;}
	public Boolean bPrev{get; private set;}
	public Boolean bNext{get; private set;}
	public string nView{get; set;}
	private string projectRID;
	private string selectedRole;
	private string bu;
	private string strProjectRoleData;
	private string strMonth;
	private string strEmplAllocationDate;
	private string strEmplList;
	private string pieData;	
	private string pieHomeData;	
	private string pieDataHist;
	private Integer nMax;
	private Integer emplSize;
	private Integer defaultView = 20;
	
	public RM_MyProjectAlloRoleController(){
		projectRID = ApexPages.currentPage().getParameters().get('id');
		selectedRole = ApexPages.currentPage().getParameters().get('role');
		selectedRole = selectedRole.replaceAll('_', ' ');
		
		bu = ApexPages.currentPage().getParameters().get('bu');
		list<WFM_Project__c> lstProj = RM_ProjectService.GetProjectbyrID(projectRID);
		WFM_Project__c pro;
		if (lstProj != null)
		 	pro= lstProj[0];
		selectedProject = pro.name + ' for ' + pro.Client_ID__c;
		system.debug('-----projectRID------'+projectRID);
		system.debug('-----selectedRole------'+selectedRole); 
		system.debug('-----bu------'+bu); 
		n = 1;
		
		bPrev = false;
		bNext = false;
		getRoleData();
		pMonth = RM_Tools.GetYearMonth(-4);
		getEmplData(pMonth);
		getPieData(pMonth );
		if (emplSize > defaultView){		
   			nView = string.valueOf(defaultView);
   			nMax = Math.round(emplSize/decimal.valueOf(nView)+0.5);
    		system.debug('-----nMax------'+nMax + '--' +emplSize + '----'+ nView + '---' + emplSize/decimal.valueOf(nView)) ;
			bNext = true;
		}
   		else
   			nView = 'All';
		
	}
   	public List<SelectOption> getProjTimeItems(){
        List<SelectOption> options = new List<SelectOption>();
        for (string m: RM_Tools.GetMonthsList(-4, 6)){
        	   options.add(new SelectOption(m,m));
        } 
       return options;
   	}
   	public string GetProjectRoleData(){
   		return strProjectRoleData;
   	}
   	public string  getMonths(){
    	return strMonth;
    }
    public string GetEmplAllocationDate(){
    	return strEmplAllocationDate;
    }
    public string getEmplList(){
    	return strEmplList;
    }
    public void OnClickAllocation(){
    	pMonth = ApexPages.CurrentPage().getParameters().get('yearMonth');
    	system.debug('-----on click------'+pMonth);
    	getPieData(pMonth );
    	getEmplData(pMonth);
    	
    }
     public  PageReference ProjTimeChange(){
    	getPieData(pMonth );
    	
    	return null;
    }
    public string getPieData(){
    	return pieData;
    }
    public string getPieHomeCountryData(){
    	return pieHomeData;
    }
    public string getPieDataHist(){
    	return pieDataHist;
    } 
    public list<SelectOption> getViewItems(){   
    	
   		return RM_LookupDataAccess.getViewNumberItems(emplSize, 10);
   	}
     public PageReference ViewChange(){
    	if (nView != 'All'){
    		bNext = true;
    		n = 1;
    		
    	}
    	else{
    		bPrev = false;
			bNext = false;
    	}
    	getEmplData(pMonth);
    	
    	return null;
    }
    public PageReference Previous(){
    	if (n > 1)
    		n -= 1;
    	if (n > 1)
    		bPrev = true;
    	else
    		bPrev = false;
    	if (n < nMax)
    		bNext = true;
    	else
    		bNext = false;
    	getEmplData(pMonth);
    	return null;
    }
    public PageReference Next(){
    	if (n < nMax)
    		n += 1;
    	if (n > 1)
    		bPrev = true;
    	else
    		bPrev = false;
    	if (n < nMax)
    		bNext = true;
    	else
    		bNext = false;
    	getEmplData(pMonth);
    	return null;
    }
   	private void getRoleData(){
   		strMonth = '';
   		list<string> monthList = RM_Tools.GetMonthsList(-4, 5);
   		for (string s: monthList)
			strMonth +='\''+ s + '\',';
    	List<AggregateResult> arAssignedList = RM_AssignmentsService.GetAssignmentByProjectRole(projectRID, selectedRole, bu, -4, 5);
		emplSize = arAssignedList.size();
		system.debug('-----arAssignedList------'+arAssignedList);
		 
		
		strProjectRoleData = '{ name: \'Assigned\', color: \'#4572A7\', type: \'column\', data:[';
		for (string m: monthList){
			
			
			boolean bFind = false;
			for(AggregateResult ar:arAssignedList){
				if ((string)ar.get('Year_Month__c') == m){
					bFind = true;
					if (ar.get('assignedFTE') != null)					
						strProjectRoleData += string.valueOf((decimal)ar.get('assignedFTE')) + ',';					
				}
			}
			if (!bFind)
				strProjectRoleData +=  '0,';
		}	
			strProjectRoleData = strProjectRoleData.substring(0, strProjectRoleData.length()-1) + ']},';
			
		
		List<AggregateResult> arHistList = RM_EmployeeService.GetActuralWorkHrByProjectRole(projectRID, selectedRole, bu, -4, -1);
		if (arHistList.size()> 0){
			strProjectRoleData += '{ name: \'Actuals\', color: \'#89A54E\', type: \'spline\', data:[';
			for (string m: RM_Tools.GetMonthsList(-4, -1)){
				
				
				boolean bFind = false;
				for(AggregateResult ar:arHistList){
				
					if ((string)ar.get('Year_Month__c') == m){
						bFind = true;
						strProjectRoleData += string.valueOf((decimal)ar.get('fte')) + ',';
					}
				}
				if (!bFind)
					strProjectRoleData +=  '0,';
				
				
				
			}
			strProjectRoleData = strProjectRoleData.substring(0, strProjectRoleData.length()-1) + ']},';
		}
		system.debug('-----strProjectRoleData------'+strProjectRoleData);
		
		//remove last ,
		if (strProjectRoleData.length() > 0){
			strMonth = strMonth.substring(0, strMonth.length()-1);
			strProjectRoleData = strProjectRoleData.substring(0, strProjectRoleData.length()-1);
		}
   
   	}
   	private void getEmplData(string selMonth){
   		strEmplList = '';
   		system.debug('-----bu------'+bu);
   		List<AggregateResult> emplAssignedList = RM_AssignmentsService.GetEmplAssignmentByProjectRoleYMonth(projectRID, selectedRole, bu, selMonth);
   		List<AggregateResult> emplHist = RM_EmployeeService.GetEmplActuralWorkHrByProjectRoleYMonth(projectRID, selectedRole,bu,  selMonth);
   		system.debug('-----emplHist------'+emplHist);
   		set<string> emplSet = new set<string>();
   		for (AggregateResult ar:emplAssignedList)
   			emplSet.add((string)ar.get('employee_id__c'));
   		for (AggregateResult aH: emplHist)
   			emplSet.add((string)aH.get('employee_id__c'));
   		list<Employee_Details__c> empls = RM_EmployeeService.GetEmployeeDetailByEmployeeRIDs(emplSet);
		emplSize =empls.size();
		if (emplSize > 0){
			strEmplAllocationDate = '{ name: \'Assigned\', data:[';
			integer nStart = 0;
	 	 	integer nEnd = empls.size()-1;
		 	 	
	 	 	if (nView!= 'All' && nView != null){
		 	 	nStart = Integer.valueof(nView)*(n-1);
		 	 	
		 	 	nEnd = nStart + Integer.valueOf(nView)-1;
		 	 	if (nEnd > empls.size()-1)
		 	 		nEnd = empls.size()-1;
	 	 	}
	 	 	else if (nView == null){
	 	 		if (emplSize > defaultView ){
		 	 		nView = string.ValueOf(defaultView);
		 	 		nEnd = nStart + Integer.valueOf(nView)-1;
	 	 		}
	 	 	}
	 	 		
			//for (Employee_Details__c e: empls){
			for  (integer j = nStart; j <= nEnd; j++) {	
				string lastName = empls[j].last_name__c;
	    		system.debug('-----lastName------'+lastName);
	    		lastName = lastName.replace('\'', '\\\'');
				strEmplList +='\''+ lastName + ', ' + empls[j].first_name__c + '\',';
				
				boolean bFind = false;
				for(AggregateResult ar:emplAssignedList){
					if ((string)ar.get('employee_id__c') == empls[j].id){
						bFind = true;
						if (ar.get('assignedFTE') != null)					
							strEmplAllocationDate += string.valueOf((decimal)ar.get('assignedFTE')) + ',';					
					} 
				}
				if (!bFind)
					strEmplAllocationDate +=  '0,';
			}	
				strEmplAllocationDate = strEmplAllocationDate.substring(0, strEmplAllocationDate.length()-1) + ']},';
		
		
			if (emplHist.size() > 0){
				strEmplAllocationDate += '{ name: \'Actuals\', color: \'#89A54E\', data:[';
				//for (Employee_Details__c e: empls){
				for  (integer j = nStart; j <= nEnd; j++) {		
					
					boolean bFind = false;
					for(AggregateResult ar:emplHist){
					
						if ((string)ar.get('employee_id__c') == empls[j].id){
							bFind = true;
							strEmplAllocationDate += string.valueOf((decimal)ar.get('fte')) + ',';
						}
					}
					if (!bFind)
						strEmplAllocationDate +=  '0,';
					
					
					
				}
				strEmplAllocationDate = strEmplAllocationDate.substring(0, strEmplAllocationDate.length()-1) + ']},';
				system.debug('-----strEmplAllocationDate------'+strEmplAllocationDate);
			}
			//remove last ,
			if (strEmplList.length() > 0)
				strEmplList = strEmplList.substring(0, strEmplList.length()-1);
			if (strEmplAllocationDate.length() > 0)			
				strEmplAllocationDate = strEmplAllocationDate.substring(0, strEmplAllocationDate.length()-1);
		}
   	}
   	 private string getPieData(string yearMonth){
    	string strData = '';
    	string strAssignOther = '';
    	string strHisOther = '';
    	string strAssignHomeOther = '';
    	AggregateResult[] assignHr = RM_AssignmentsService.GetAssignmentCountryByProjectRoleYMonth(projectRID, selectedRole, yearMonth);
    	AggregateResult[] assignHomeHr = RM_AssignmentsService.GetAssignmentHomeCountryByProjectRoleYMonth(projectRID, selectedRole, yearMonth);
    	
    	AggregateResult[] postedHrs = RM_EmployeeService.GetActuralWorkHrCountryByProjectRoleYMonth(projectRID, selectedRole, yearMonth);
    	system.debug('-----assignHr------'+assignHr+projectRID+selectedRole+yearMonth);
    	
    	if (assignHr.size() > 0){
    		
    		system.debug('-----assignHr------'+assignHr);   		
    		
    		for (AggregateResult ar : assignHr){
    			
    			decimal per = 0.0;
    			if (ar.get('assignedFTE') != null)
					per = (decimal)ar.get('assignedFTE');
					
    			if (FindinHist((string)ar.get('project_country__c'),postedHrs))    			
    				strData+='[\'' + (string)ar.get('project_country__c') + '\',' + string.valueOf(per) + '],';
    			else
    				strAssignOther+='[\'' + (string)ar.get('project_country__c') + '\',' + string.valueOf(per) + '],';
    		}
    		if (strAssignOther.length() > 0)
    			strData += strAssignOther;    		
    		pieData = strData.substring(0, strData.length()-1);
    	}
    	else
    		pieData = '';
    	if (assignHomeHr.size() > 0){
    		strData = '';
    		system.debug('-----assignHr------'+assignHr);   		
    		
    		for (AggregateResult ar : assignHomeHr){
    			
    			decimal per = 0.0;
    			if (ar.get('assignedFTE') != null)						
					per = (decimal)ar.get('assignedFTE');
					
    			if (FindinHist((string)ar.get('country'),postedHrs))    			
    				strData+='[\'' + (string)ar.get('country') + '\',' + string.valueOf(per) + '],';
    			else
    				strAssignHomeOther+='[\'' + (string)ar.get('country') + '\',' + string.valueOf(per) + '],';
    		}
    		if (strAssignHomeOther.length() > 0)
    			strData += strAssignHomeOther;    		
    		pieHomeData = strData.substring(0, strData.length()-1);
    	}
    	else
    		pieHomeData = '';
		decimal dTHr = 0.0;    	
    	strData = '';
    	if (postedHrs.size() > 0){
    		for (AggregateResult ar : postedHrs){
    			if (FindinAssign((string)ar.get('country'),assignHomeHr))   
    				strData+='[\'' + (string)ar.get('country') + '\',' + string.valueOf(ar.get('fte')) + '],';
    			else
    				strHisOther+='[\'' + (string)ar.get('country') + '\',' + string.valueOf(ar.get('fte')) + '],';
    		}
    		if (strHisOther.length() > 0)
    			strData += strHisOther;
    		pieDataHist = strData.substring(0, strData.length()-1);
    		system.debug('-----pieDataHist------'+pieDataHist);
    		
    	}
    	else pieDataHist = '';
    	
    	return strData;
    }
    
    private boolean FindinHist(string countryName, AggregateResult[] postedHrs){
    	for (AggregateResult ar : postedHrs){
    		if (countryName == (string)ar.get('country')){
    			return true;
    		}
    	}
    	return false;
    }
     private boolean FindinAssign(string countryName, AggregateResult[] assignHr){
    	for (AggregateResult ar : assignHr){
    		if (countryName == (string)ar.get('country')){
    			return true;
    		}
    	}
    	return false;
    }
    
}