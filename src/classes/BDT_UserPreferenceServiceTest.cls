@isTest
private class BDT_UserPreferenceServiceTest {


	static testMethod void generalTest() {
		
		// create a fake user
		Profile p = [select id from profile where name='System Administrator'];

		list<user> userList = BDT_TestDataUtils.buildTestUserList(2);

		system.runAs(userList[0]) {
			BDT_UserPreferenceService up = new BDT_UserPreferenceService();
			
			// user has no data, check data is empty new map
			system.assertEquals(new map<string,BDT_UserPreference__c>(), up.userPreferences);
		
			//populate some data
			Client_Project__c cp = new Client_Project__c();
			cp.Code__c = 'ProjectCode1';
			insert cp;
			
			up.setProjectAndStudies(
				cp,
				new Set<String>{'StudyID1','StudyID2'}
			);
			
			// save the user preverences
			up.saveUserPreferences();
		
		
			// reload user preferences
			up = new BDT_UserPreferenceService();
			
			// 3 keys should be generated: PROJECT,STUDIES,QUICKLIST
			system.assertEquals(3, up.userPreferences.keySet().size());
			
			// test contents of 3 keys
			system.assertEquals(cp.Id, up.userPreferences.get(BDT_UserPreferenceDataAccessor.PROJECT).text__c);
			system.assert(up.userPreferences.get(BDT_UserPreferenceDataAccessor.STUDIES).text__c.contains('StudyID1'));
			system.assert(up.userPreferences.get(BDT_UserPreferenceDataAccessor.STUDIES).text__c.contains('StudyID2'));
			
			// check quicklist
			list<BDT_UserPreferenceService.quickListWrapper> quickList = up.getQuickList ();
			system.assertEquals(1,quickList.size());
			system.assertEquals(cp.Id,quickList[0].projectId);
			system.assertEquals(cp.code__c,quickList[0].projectCode);
			system.assert(quickList[0].studySet.contains('StudyID1'));
			system.assert(quickList[0].studySet.contains('StudyID2'));
			
			// check static set project
			BDT_UserPreferenceService.staticSetProject(cp.Id);
		
		}
	}

	static testMethod void moveProjectToTopOfQuickListTest() {
		
		String quickListJson = '[';
		quickListJson += '{"projectId":"projectId1","projectCode":"projectCode1","sponsorCode":"","studySet":["StudyId1","StudyId2"]},';
		quickListJson += '{"projectId":"projectId2","projectCode":"projectCode2","sponsorCode":"","studySet":["StudyId3","StudyId4"]}]';
		List<BDT_UserPreferenceService.quickListWrapper> quickList = (List<BDT_UserPreferenceService.quickListWrapper>)JSON.deserialize(quickListJson,List<BDT_UserPreferenceService.quickListWrapper>.class);
		
		BDT_UserPreferenceService up = new BDT_UserPreferenceService();
		quickList = up.moveProjectToTopOfQuickList(quickList,1);
		
		system.assertEquals('projectId2',quickList[0].projectId);
		system.assertEquals('projectId1',quickList[1].projectId);
		
	}
	
	static testMethod void reloadQuickListProject() {

		//populate some data
		list<Client_Project__c> cp = new list<Client_Project__c>();
		cp.add(new Client_Project__c(code__c='projectCode1'));
		cp.add(new Client_Project__c(code__c='projectCode2'));
		
		insert cp;

		String quickListJson = '[';
		quickListJson += '{"projectId":"'+cp[0].id+'","projectCode":"'+cp[0].Code__c+'","sponsorCode":"","studySet":["StudyId1","StudyId2"]},';
		quickListJson += '{"projectId":"'+cp[1].id+'","projectCode":"'+cp[1].Code__c+'","sponsorCode":"","studySet":["StudyId3","StudyId4"]}]';
		List<BDT_UserPreferenceService.quickListWrapper> quickList = (List<BDT_UserPreferenceService.quickListWrapper>)JSON.deserialize(quickListJson,List<BDT_UserPreferenceService.quickListWrapper>.class);
		BDT_UserPreferenceService up = new BDT_UserPreferenceService();
		up.setQuickList (quickList);

		up.reloadQuickListProject(cp[1]);
		system.assertEquals(cp[1].id, up.userPreferences.get(BDT_UserPreferenceDataAccessor.PROJECT).text__c);
		system.assert(up.userPreferences.get(BDT_UserPreferenceDataAccessor.STUDIES).text__c.contains('StudyId3'));
		system.assert(up.userPreferences.get(BDT_UserPreferenceDataAccessor.STUDIES).text__c.contains('StudyId4'));

		//removeStudyFromQuickListTest
		up.removeStudyFromQuickList(new Set<String>{'StudyId3'});
		quickList = up.getQuickList();
		system.assert(!quickList[0].studySet.contains('StudyId3'));
		system.assert(quickList[0].studySet.contains('StudyId4'));
		
		//removeStudyFromStudieList
		up.removeStudyCodes(new Set<String>{'StudyId3'});
		system.assert(!up.userPreferences.get(BDT_UserPreferenceDataAccessor.STUDIES).text__c.contains('StudyId3'));
		system.assert(up.userPreferences.get(BDT_UserPreferenceDataAccessor.STUDIES).text__c.contains('StudyId4'));
	
		//removeProjectFromQuickList
		system.assertEquals(2,up.getQuickList().size());
		up.removeProjectFromQuickList(String.valueOf(cp[0].id));
		system.assertEquals(1,up.getQuickList().size());
		system.assertNotEquals(cp[0].id,up.getQuickList()[0].projectId);
		
		// switch project
		up.setProject(cp[0].id);
		
	}

}