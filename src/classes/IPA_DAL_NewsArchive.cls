public class IPA_DAL_NewsArchive
{
    public list<IPA_Articles__c> returnNewsOf3TypesArchive(String Type)
    {
        list<IPA_Articles__c> ipa_news;
        
        ipa_news = [select Id, Title__c, RecordType.Name, Display_Date__c, Teaser__c, Display_area__c, Featured_Image__c, URL__c, Link__c from IPA_Articles__c 
                    where RecordType.Name =:Type AND Display_Date__c <= today AND Published__c = true order by Weight__c, Display_date__c DESC, Title__c];
        return ipa_news;
    }
    
    public List<String> returnUniqueMonthsForArchive(String Type)
    {
        List<String> lstDates = new List<String>();
        String tmp;
        Boolean found;
        
        List<AggregateResult> lst = [select Display_Date__c from IPA_Articles__c 
            where RecordType.Name =:Type AND Published__c = true AND Display_Date__c <= today
            group by Display_Date__c order by Display_Date__c desc];
        for(AggregateResult l: lst)
        {
            tmp = Date.valueOf(l.get('Display_Date__c')).month() + '-' + Date.valueOf(l.get('Display_Date__c')).year();
            
            //Add only unique ones
            found = false;
            for(String s: lstDates)
                if(s == tmp) { found = true; break; }
            
            if(found == false)
                lstDates.add(tmp);
        }
        
        return lstDates;
    }
    
    public List<IPA_Articles__c> returnArchiveForMonth(Integer month, Integer year, String Type)
    {
        List<IPA_Articles__c> ipa_news;

        ipa_news = [select Id, Title__c, RecordType.Name, Display_Date__c, Teaser__c, Content__c from IPA_Articles__c 
                    where RecordType.Name=:Type AND Published__c = true
                    AND CALENDAR_MONTH(Display_Date__c) =: month AND CALENDAR_YEAR(Display_Date__c) =: year AND Display_Date__c <= today
                    order by Display_date__c DESC, Title__c];
        return ipa_news;
    }
}