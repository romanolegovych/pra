public with sharing class PAWS_GanttExtensionDataProviderCntrl
{
	private String PAWSProjectID
	{
		get
		{
			if (PAWSProjectID == null)
			{
				PAWSProjectID = ApexPages.currentPage().getParameters().get('pawsProjectId');
			}
			return PAWSProjectID;
		}
		set;
	}
	
	private List<String> Actions
	{
		get
		{
			if (Actions == null)
			{
				Actions = new List<String>();
				try
				{
					Actions = ApexPages.currentPage().getParameters().get('actions').split(',');
				}
				catch(Exception ex){}
			}
			return Actions;
		}
		set;
	}
	
	public String GetOutputJSON()
	{
		Map<String, Object> results = new Map<String, Object>();
		
		for (String action : Actions)
		{
			try
			{
				results.put(action, ProcessAction(action));
			}
			catch (Exception ex)
			{
				results.put(action, new Map<String, String>{'Error' => ex.getMessage()});
			}
		}
		
		return 'window.PAWS_GanttExtensionDataProvider_Results = ' + JSON.serialize(results) + ';';
	}
	
	private Object ProcessAction(String action)
	{
		Object result;
		
		if (action == 'getCountries')
		{
			result = GetCountries();
		}
		else if (action == 'getSites')
		{
			result = GetSites();
		}
		else
		{
			throw new PAWS_Utilities.PAWSException('Unknown action "' + action + '"');
		}
		
		return result;
	}
	
	private List<PAWSCountryWrapper> GetCountries()
	{
		List<PAWSCountryWrapper> result = new List<PAWSCountryWrapper>();
		for (PAWS_Project_Flow_Country__c country : [Select Name From PAWS_Project_Flow_Country__c Where PAWS_Project__c = :PAWSProjectID Order By Name])
		{
			result.add(new PAWSCountryWrapper(country));
		}
		return result;
	}
	
	private List<PAWSSiteWrapper> GetSites()
	{
		List<PAWSSiteWrapper> result = new List<PAWSSiteWrapper>();
		for (PAWS_Project_Flow_Site__c site : [Select Name, PAWS_Country__c, PAWS_Country__r.Name From PAWS_Project_Flow_Site__c Where PAWS_Project__c = :PAWSProjectID Order By PAWS_Country__r.Name, Name])
		{
			result.add(new PAWSSiteWrapper(site));
		}
		return result;
	}
	
	private class PAWSCountryWrapper
	{
		public String id {get; set;}
		public String name {get; set;}
		
		public PAWSCountryWrapper(PAWS_Project_Flow_Country__c country)
		{
			this.id = country.Id;
			this.name = country.Name;
		}
	}
	
	private class PAWSSiteWrapper
	{
		public String id {get; set;}
		public String countryId {get; set;}
		public String name {get; set;}
		
		public PAWSSiteWrapper(PAWS_Project_Flow_Site__c site)
		{
			this.id = site.Id;
			this.countryId = site.PAWS_Country__c;
			this.name = site.PAWS_Country__r.Name + ' - ' + site.Name;
		}
	}
}