public with sharing class RM_JsonJQWidgetsUtils {
	/* This class create Json string for JqxWidgetes*/
	protected string jsonStr{get;set;}
	protected string strType{get; set;} //  UI widgetes such as jqxGrid
	
	public RM_JsonJQWidgetsUtils(string widgets_type){
		strType = widgets_type;
	}
	public void startJsonStr(){
		if (strType == 'jqxGrid')
			jsonStr = '[';
	}
	public void endJsonStr(){
		if (strType == 'jqxGrid')
			jsonStr += ']';
	}
	public void startNextRow(){
		if (strType == 'jqxGrid'){
			if (jsonStr.indexOf('{') == -1)
				jsonStr += '{';
			else
				jsonStr += ', {';
		}
	}
	public void endRow(){
		system.debug('---------jsonStr---------'  + jsonStr);	 
		jsonStr = jsonStr.trim();
		if (jsonStr.endsWith(',')){
			jsonStr = jsonStr.substring(0, jsonStr.length()- 1);
			system.debug('---------jsonStr---------'  + jsonStr);
		}
		jsonStr += '}';
		
	}
	public void addOneStrColumn(string columnName, string value){
		if (strType == 'jqxGrid'){
			jsonStr += String.format('"{0}":"{1}", ', new string[]{columnName, value});
		}
	}
	
	public void addLastStrColumn(string columnName, string value){
		if (strType == 'jqxGrid'){
			jsonStr += string.format('"{0}":"{1}"', new string[]{columnName, value});
		}
	}
	public void addOneNumColumn(string columnName, Decimal value){
		if (strType == 'jqxGrid'){
			jsonStr += string.format('"{0}":{1}, ', new string[]{columnName, string.valueOf(value)});
		}
	}
	
	public void addLastNumStrColumn(string columnName, Decimal value){
		if (strType == 'jqxGrid'){
			jsonStr += string.format('"{0}":{1}', new string[]{columnName, string.valueOf(value)});
		}
	}
	public string getJson(){
		return jsonStr;
	}
	public void createDataFiledDefinitionStr(String columnName, String columnType){
		
		
		if (strType == 'jqxGrid'){
			if (jsonStr.length() > 3)
				jsonStr += ',';
			
			jsonStr += '{';
			jsonStr += string.format('name: "{0}", type: "{1}"', new string[]{columnName, columnType});
			jsonStr += '}';
			
		} 
	}
	public void createColumnFieldDefinitionStr(String columnName, String dataFiled, string others){
		
		
		if (strType == 'jqxGrid'){
			if (jsonStr.length() > 3)
				jsonStr += ',';
			
			jsonStr += '{';
			jsonStr += string.format('text: "{0}", datafield: "{1}", {2}', new string[]{columnName, dataFiled,others });
			jsonStr += '}';
			
		} 
	}
	public void createColumnGroupDefinitionStr(String columnName, String connectName, string others){
		
		
		if (strType == 'jqxGrid'){
			if (jsonStr.length() > 3)
				jsonStr += ',';
			
			jsonStr += '{';
			if (others == ''){
				jsonStr += string.format('text: "{0}", name: "{1}"', new string[]{columnName, connectName });
			}
			else{
				jsonStr += string.format('text: "{0}", name: "{1}", {2}', new string[]{columnName, connectName,others });
			}
			jsonStr += '}';
			
		} 
	}
}