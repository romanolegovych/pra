public with sharing class CCFlowInstanceTrigger extends STSWR1.AbstractTrigger
{
	/************************ PUBLIC SECTION ****************************/
	public override void afterInsert(List<SObject> records)
	{
		try
		{
			updateCriticalChains((List<STSWR1__Flow_Instance__c>) records);
		}
		catch (Exception ex)
		{
			for(Integer i = 0; i < records.size(); i++)
				records[i].addError(ex.getMessage());
		}
	}
	
	/************************ PRIVATE SECTION ****************************/
	private void updateCriticalChains(List<STSWR1__Flow_Instance__c> instances)
	{
		PAWS_Utilities.checkLimits(new Map<String, Integer> 
		{
			'Queries' => 1,//2,
			'DMLStatements' => 1
		});
		
		Map<ID, ID> flowIdFlowInstanceIdMap = new Map<ID, ID>();
		for (STSWR1__Flow_Instance__c instance : instances)
		{
			flowIdFlowInstanceIdMap.put(instance.STSWR1__Flow__c, instance.Id);
		}
		
		List<Critical_Chain__c> associatedChains = new List<Critical_Chain__c>();
		
		for (Critical_Chain__c chain : [select Flow__c, Flow_Instance__c from Critical_Chain__c where Flow__c in :flowIdFlowInstanceIdMap.keyset()])
		{
			chain.Flow_Instance__c = flowIdFlowInstanceIdMap.get(chain.Flow__c);
			associatedChains.add(chain);
		}
		
		update associatedChains;
	}
}