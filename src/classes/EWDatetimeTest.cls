@istest
public class EWDatetimeTest
{
	@istest private static void testAddWorkdays()
	{
		Datetime datetimeValue = Date.newInstance(2015, 5, 22);
		EWDatetime testee = new EWDatetime(datetimeValue);
		
		System.assertEquals(datetimeValue, testee.addWorkdays(0));
		System.assertEquals(datetimeValue, testee.addWorkdays(1));
		System.assertEquals('Monday', testee.addWorkdays(2).formatGmt('EEEE'));
	}
	
	@istest private static void testWorkdaysBetween()
	{
		Datetime datetimeValue = Date.newInstance(2015, 5, 22);
		EWDatetime testee = new EWDatetime(datetimeValue);
		
		Datetime datetimeB = datetimeValue.addDays(7);
		System.assertEquals(5, testee.workdaysBetween(datetimeB));
	}
}