/**
 * @description	The Data Accessor Layer of the 'Notification & Communication Module'
 * @author		Dimitrios Sgourdos
 * @date		Created: 01-Sep-2015, Edited: 08-Oct-2015
 */
public with sharing class NCM_DataAccessor {
	
// *********************************************************************************************************************
//				Notification Registration queries
// *********************************************************************************************************************
	
	/**
	 * @description	Get the notification registrations that are to be disabled
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015
	 * @param		whereClause    The filter for selecting Notification Registrations
	 * @return		The notification registration records that meet the given criteria
	*/
	public static List<NotificationRegistration__c> getNotificationRegistrationWithWhereClause (String whereClause) {
		// Initialize data and build the query
		List<Notificationregistration__c> results;
		
		String soql = 'SELECT Id, '
					+ 		 'Active__c, '
					+		 'NotifyByChatter__c, '
					+		 'RelatedToId__c, '
					+ 		 'UserId__c '
					+ ' FROM NotificationRegistration__c '
					+ ' WHERE ' + whereClause
					+ ' LIMIT 50000';
		
		// Run the query
		try {
			results = (List<NotificationRegistration__c>) Database.query(soql);
		} catch (Exception e) {
			system.debug('Failed on soql: ' + soql);
			// throw the error again so application errors can be located
			throw e;
		}
		
		return results;
	}
	
	
	/**
	 * @description	Get the notification registrations that belong to the given users or to the given notification
	 *				catalog entry or to the given related to objects, to check for possible duplicates
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015
	 * @param		userIdsSet            The users that are assigned with the retrieved registrations
	 * @param		notifCatalogIdsSet    The notification catalog entries that are assigned with the retrieved registrations
	 * @param		relatedToIdsSet       The related to objects that are assigned with the retrieved registrations
	 * @return		The notification registrations that belong to the given users, catalog entry or related to objects
	*/
	public static List<NotificationRegistration__c> getNotificationRegistrationForCheckingDuplicates(
																					Set<ID> userIdsSet,
																					Set<ID> notifCatalogIdsSet,
																					Set<String> relatedToIdsSet) {
		return [SELECT	Id,
						NotificationCatalog__c,
						UserId__c,
						RelatedToId__c,
						Active__c,
						ReminderPeriod__c,
						MaxNumberreminders__c,
						NotifyByEmail__c,
						NotifyBySMS__c,
						NotifyByChatter__c
				FROM	NotificationRegistration__c
				WHERE	userId__c IN :userIdsSet
						OR NotificationCatalog__c IN :notifCatalogIdsSet
						OR RelatedToId__c IN :relatedToIdsSet];
	}
	
	
	/**
	 * @description	Get the list of notification registration that belong to the given notification Catalog Id and the
	 				related to id that they are active and also the user is active.
	 * @author		Jegadheesan Muthusamy
	 * @date		Created: 09-Sep-2015, Edited: 08-Oct-2015
	 * @param		notificationCatalogId	The notification catalog record
	 * @param		relatedToId				The related object id that is connected with the registrations
	 * @return		The list of notification registration that meet the given criteria
	*/
	public static List<NotificationRegistration__c> getListOfNotificationRegistration(  ID notificationCatalogId,
																						String relatedToId ) {
		return [ SELECT	Id,
						NotificationCatalog__c,
						NotificationCatalog__r.AcknowledgementRequired__c,
						NotificationCatalog__r.SMSallowed__c,
						UserId__c,
						UserId__r.IsActive,
						RelatedToId__c,
						Active__c,
						ReminderPeriod__c,
						MaxNumberreminders__c,
						NotifyByEmail__c,
						NotifyBySMS__c,
						NotifyByChatter__c
				FROM	NotificationRegistration__c
				WHERE	Active__c = true
						AND NotificationCatalog__c = :notificationCatalogId
						AND RelatedToId__c = :relatedToId
						AND UserId__r.IsActive = true
						LIMIT 50000];
	}
	
	
	/**
	 * @description	Get the Notification Registration records that they are active, that the assigned subscriber is
	 				active and that the assigned topic is enabled to participate in the batch job.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 25-Sep-2015, Edited: 29-Sep-2015
	 * @return		The notification registration records that meet the given criteria
	*/
	public static Database.queryLocator getNotificationRegistrationsforSchedule() {
		return Database.getQueryLocator([SELECT	Id,
												UserId__c,
												ReminderPeriod__c,
												MaxNumberReminders__c,
												RelatedToId__c,
												NotifyByEmail__c,
												NotifyBySMS__c,
												NotifyByChatter__c,
												NotificationCatalog__c,
												NotificationCatalog__r.ImplementationClassName__c,
												NotificationCatalog__r.AcknowledgementRequired__c,
												NotificationCatalog__r.Type__c,
												NotificationCatalog__r.ChatterEnabled__c
										FROM	NotificationRegistration__c
										WHERE	Active__c = true
												AND UserId__r.IsActive = true
												AND NotificationCatalog__r.TriggeredByBatchJob__c = true
										ORDER BY NotificationCatalog__c,
												 RelatedToId__c]);
	}
	
	
// *********************************************************************************************************************
//				Notification Catalog queries
// *********************************************************************************************************************
	
	/**
	 * @description	Get the notification catalogs that belong to the given notification category. Data are retrieved
	 *				with their children registrations that are enabled for the given user and the given related to id
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	 * @param		userId                  The user that the subscriptions are assigned to
	 * @param		relatedToId             The related to object that the subscriptions are connected with
	 * @param		notificationCategory    The category of the notifications that the subscriptions are connect with
	 * @return		The notification catalog records that meet the given criteria
	*/
	public static List<NotificationCatalog__c> getNotificationTopicsWithActiveSubscriptions (ID userId,
																							String relatedToId,
																							String notificationCategory) {
		return [SELECT	Id,
						NotificationName__c,
						NotificationDescription__c,
						Category__c,
						Type__c,
						Comments__c,
						DefaultReminderPeriod__c,
						Active__c,
						Priority__c,
						AcknowledgementRequired__c,
						ChatterEnabled__c,
						SMSallowed__c,
						TriggeredByBatchJob__c,
						ImplementationClassName__c,
						(select Id,
								NotificationCatalog__c,
								UserId__c,
								RelatedToId__c,
								Active__c,
								ReminderPeriod__c,
								MaxNumberreminders__c,
								NotifyByEmail__c,
								NotifyBySMS__c,
								NotifyByChatter__c
						from	NotificationRegistrations__r
						where	Active__c = true
								and UserId__c = :userId
								and RelatedToId__c = :relatedToId)
				FROM	NotificationCatalog__c
				WHERE	Active__c = true
						AND Category__c = :notificationCategory
				ORDER BY NotificationName__c];
	}
	
	
	/**
	 * @description	Get the notification catalogs that belong to the given notification category. Data are retrieved
	 *				with their children registrations that are enabled for the given user
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 17-Sep-2015
	 * @param		userId                  The user that the subscriptions are assigned to
	 * @param		notificationCategory    The category of the notifications that the subscriptions are connect with
	 * @return		The notification catalog records that meet the given criteria
	*/
	public static List<NotificationCatalog__c> getNotificationTopicsWithActiveSubscriptions (ID userId,
																							String notificationCategory) {
		return [SELECT	Id,
						NotificationName__c,
						NotificationDescription__c,
						Category__c,
						Type__c,
						Comments__c,
						DefaultReminderPeriod__c,
						Active__c,
						Priority__c,
						AcknowledgementRequired__c,
						ChatterEnabled__c,
						SMSallowed__c,
						TriggeredByBatchJob__c,
						ImplementationClassName__c,
						(select Id,
								NotificationCatalog__c,
								UserId__c,
								RelatedToId__c,
								Active__c,
								ReminderPeriod__c,
								MaxNumberreminders__c,
								NotifyByEmail__c,
								NotifyBySMS__c,
								NotifyByChatter__c
						from	NotificationRegistrations__r
						where	Active__c = true
								and UserId__c = :userId)
				FROM	NotificationCatalog__c
				WHERE	Active__c = true
						AND Category__c = :notificationCategory
				ORDER BY NotificationName__c];
	}
	
	
	/**
	 * @description	Get the notification catalogs that belong to the given topic names
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 * @param		topicNameSet    The names of the notification catalog entries that we want to retrieve
	 * @return		The notification catalog records that meet the given criteria
	*/
	public static List<NotificationCatalog__c> getNotificationTopicsForUpdating (Set<String> topicNameSet) {
		return [SELECT 	Id,
						NotificationName__c,
						NotificationDescription__c,
						Category__c,
						Type__c,
						Comments__c,
						DefaultReminderPeriod__c,
						Active__c,
						Priority__c,
						AcknowledgementRequired__c,
						ChatterEnabled__c,
						SMSallowed__c,
						TriggeredByBatchJob__c,
						ImplementationClassName__c
				FROM	NotificationCatalog__c
				WHERE	NotificationName__c IN : topicNameSet
				ORDER BY NotificationName__c
				LIMIT	50000];
	}
	
	
	/**
	 * @description	Get the notification catalog that belong to the given ID
	 * @author		Jegadheesan Muthusamy
	 * @date		Created: 09-Sep-2015
	 * @param		notificationCatalogId    The notification catalog record id
	 * @return		The notification catalog record that meet the given criteria
	*/
	public static NotificationCatalog__c getNotificationTopicById( ID notificationCatalogId ) {
		return [ SELECT 	Id,
							NotificationName__c,
							NotificationDescription__c,
							Category__c,
							Type__c,
							Comments__c,
							DefaultReminderPeriod__c,
							Active__c,
							Priority__c,
							AcknowledgementRequired__c,
							ChatterEnabled__c,
							SMSallowed__c,
							TriggeredByBatchJob__c,
							ImplementationClassName__c
					FROM	NotificationCatalog__c
					WHERE	Id = :notificationCatalogId];
	}
	
	
	/**
	 * @description	Get the notification catalog that belong to the given notification topic name
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015
	 * @param		notificationCatalogName    The notification catalog name
	 * @return		The notification catalog record that meet the given criteria
	*/
	public static NotificationCatalog__c getNotificationTopicByName(String notificationCatalogName) {
		return [ SELECT 	Id,
							NotificationName__c,
							NotificationDescription__c,
							Category__c,
							Type__c,
							Comments__c,
							DefaultReminderPeriod__c,
							Active__c,
							Priority__c,
							AcknowledgementRequired__c,
							ChatterEnabled__c,
							SMSallowed__c,
							TriggeredByBatchJob__c,
							ImplementationClassName__c
					FROM	NotificationCatalog__c
					WHERE	NotificationName__c = :notificationCatalogName]; // Notification name is unique
	}
	
// *********************************************************************************************************************
//				Notification queries
// *********************************************************************************************************************
	
	/**
	 * @description	Get the notification Records based on the condition Status Pending Acknowlegment, Notification 
	 				registration is active, user is active and reminder less than or equal to current date and time 
	 * @author		Kalaiyarasan Karunanithi, Dimitrios Sgourdos
	 * @date		Created: 09-Sep-2015, Edited: 08-Oct-2015
	 * @return		The notification records that meet the given criteria
	*/
	public static Database.queryLocator getNotificationAcknowledgingforSchedule() {
		return Database.getQueryLocator([SELECT	Id,
												Reminder__c,
												Status__c,
												NotificationRegistration__c,
												NotificationRegistration__r.UserId__c,
												NotificationRegistration__r.MaxNumberReminders__c,
												NotificationRegistration__r.ReminderPeriod__c,
												NotificationRegistration__r.NotifyByEmail__c,
												NotificationRegistration__r.NotifyBySMS__c,
												Notificationregistration__r.NotificationCatalog__r.SMSallowed__c,
												NotificationEvent__r.Subject__c,
												NotificationEvent__r.EmailBody__c,
												(select Id, Notification__c from NotificationsHistory__r)
										FROM	Notification__c
										WHERE	Status__c =:NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT
												AND NotificationRegistration__r.Active__c = true
												AND NotificationRegistration__r.UserId__r.IsActive = true
												AND Reminder__c <=:system.now()
												AND (NotificationRegistration__r.NotifyByEmail__c = true
														OR (NotificationRegistration__r.NotifyBySMS__c = true AND Notificationregistration__r.NotificationCatalog__r.SMSallowed__c = true) )
										ORDER BY Reminder__c]);
	}
	
	
	/**
	 * @description	Get the notification Records based on the condition Status Pending Acknowlegment, Notification 
	 				registration is active, user is active and based on given UserId
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 09-Sep-2015, Edited: 09-Oct-2015
	 * @param		userId         The id of the subscribed user to the notifications
	 * @return		The notification records that meet the given criteria
	*/
	public static List<Notification__c> getPendingNotificationsByUser(Id UserId) {
		return [SELECT	Id,
						NotificationEvent__c,
						NotificationRegistration__c,
						Status__c,
						Reminder__c,
						CreatedDate,
						NotificationRegistration__r.RelatedToId__c,
						NotificationRegistration__r.NotificationCatalog__r.NotificationName__c,
						NotificationRegistration__r.NotificationCatalog__r.NotificationDescription__c,
						NotificationRegistration__r.NotificationCatalog__r.Category__c,
						NotificationRegistration__r.NotificationCatalog__r.Type__c,
						NotificationRegistration__r.NotificationCatalog__r.Comments__c,
						NotificationRegistration__r.NotificationCatalog__r.DefaultReminderPeriod__c,
						NotificationRegistration__r.NotificationCatalog__r.Active__c,
						NotificationRegistration__r.NotificationCatalog__r.Priority__c,
						NotificationRegistration__r.NotificationCatalog__r.AcknowledgementRequired__c,
						NotificationRegistration__r.NotificationCatalog__r.ChatterEnabled__c,
						NotificationRegistration__r.NotificationCatalog__r.SMSallowed__c,
						NotificationRegistration__r.NotificationCatalog__r.TriggeredByBatchJob__c,
						NotificationRegistration__r.NotificationCatalog__r.ImplementationClassName__c,
						NotificationEvent__r.NotificationCatalog__c,
						NotificationEvent__r.relatedToId__c,
						NotificationEvent__r.Type__c,
						NotificationEvent__r.Subject__c,
						NotificationEvent__r.EmailBody__c,
						NotificationEvent__r.CreatedDate
				FROM	Notification__c
				WHERE	Status__c =:NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT
						AND NotificationRegistration__r.UserId__c =:UserId
						AND NotificationRegistration__r.Active__c = true
						AND NotificationRegistration__r.UserId__r.IsActive = true
				ORDER BY CreatedDate DESC
				LIMIT	50000];
	}
	
	
	/**
	 * @description	Get the notification Records based on the condition Status Pending Acknowlegment, Notification 
	 				registration is active, user is active and based on given UserId and relatedToId
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 11-Sep-2015, Edited: 09-Oct-2015
	 * @param		userId         The id of the subscribed user to the notifications
	 * @param		relatedToId    The id fo the object that is related to the notification
	 * @return		The notification records that meet the given criteria
	*/
	public static List<Notification__c> getPendingNotificationsByUserAndRelatedObject(Id userId, String relatedToId) {
		return [SELECT	Id,
						NotificationEvent__c,
						NotificationRegistration__c,
						Status__c,
						Reminder__c,
						CreatedDate,
						NotificationRegistration__r.RelatedToId__c,
						NotificationRegistration__r.NotificationCatalog__r.NotificationName__c,
						NotificationRegistration__r.NotificationCatalog__r.NotificationDescription__c,
						NotificationRegistration__r.NotificationCatalog__r.Category__c,
						NotificationRegistration__r.NotificationCatalog__r.Type__c,
						NotificationRegistration__r.NotificationCatalog__r.Comments__c,
						NotificationRegistration__r.NotificationCatalog__r.DefaultReminderPeriod__c,
						NotificationRegistration__r.NotificationCatalog__r.Active__c,
						NotificationRegistration__r.NotificationCatalog__r.Priority__c,
						NotificationRegistration__r.NotificationCatalog__r.AcknowledgementRequired__c,
						NotificationRegistration__r.NotificationCatalog__r.ChatterEnabled__c,
						NotificationRegistration__r.NotificationCatalog__r.SMSallowed__c,
						NotificationRegistration__r.NotificationCatalog__r.TriggeredByBatchJob__c,
						NotificationRegistration__r.NotificationCatalog__r.ImplementationClassName__c,
						NotificationEvent__r.NotificationCatalog__c,
						NotificationEvent__r.relatedToId__c,
						NotificationEvent__r.Type__c,
						NotificationEvent__r.Subject__c,
						NotificationEvent__r.EmailBody__c,
						NotificationEvent__r.CreatedDate
				FROM	Notification__c
				WHERE	Status__c =:NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT
						AND NotificationRegistration__r.UserId__c =:userId
						AND	NotificationRegistration__r.RelatedToId__c = :relatedToId
						AND NotificationRegistration__r.Active__c = true
						AND NotificationRegistration__r.UserId__r.IsActive = true
				ORDER BY CreatedDate DESC
				LIMIT	50000];
	}
	
	
	/**
	 * @description	Get the notification records that corresponds to the given notification ids
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 30-Sep-2015, Edited: 08-Oct-2015
	 * @param		notificationIds    The ids of the record that should be retrieved
	 * @return		The notification records that meet the given criteria
	*/
	public static List<Notification__c> getNotificationsById(Set<Id> notificationIds) {
		return [SELECT	Id,
						Status__c,
						Reminder__c,
						CreatedDate,
						NotificationEvent__c,
						NotificationEvent__r.Subject__c,
						NotificationEvent__r.EmailBody__c,
						NotificationRegistration__c,
						NotificationRegistration__r.UserId__c,
						NotificationRegistration__r.NotifyByEmail__c,
						NotificationRegistration__r.NotifyBySMS__c,
						NotificationRegistration__r.NotifyByChatter__c,
						NOtificationRegistration__r.NotificationCatalog__r.SMSallowed__c
				FROM	Notification__c
				WHERE	Id IN :notificationIds
				ORDER BY Reminder__c
				LIMIT	50000];
	}
	
// *********************************************************************************************************************
//				Notification Event queries
// *********************************************************************************************************************
	
	/**
	 * @description	Get the notification event that are under a batch job procedure
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 25-Sep-2015, Edited: 28-Sep-2015
	 * @return		The notification catalog records that meet the given criteria
	*/
	public static List<NotificationEvent__c> getNotificationEventUnderBatchProcedure() {
		return [SELECT	Id,
						Name,
						NotificationCatalog__c,
						RelatedToId__c,
						Type__c,
						Subject__c,
						EmailBody__c
				FROM	NotificationEvent__c
				WHERE	Name = :NCM_API_DataTypes.EVENT_UNDER_BATCH_PROCEDURE];
	}
}