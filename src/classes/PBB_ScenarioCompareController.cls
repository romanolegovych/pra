/**
 * @author Niharika Reddy
 * @Date 22-June-2015
 * @Description This class contains the variables and methods for  
 *              generating data for compare scenario.
 */
public class PBB_ScenarioCompareController {
    public Integer selScenarioSize{get {return selectedScenarios.size();}} 
    public Bid_Project__c bidproject{get;set;}
    public list<SelectOption> selectedScenarios {get;set;}
    public list<SelectOption> availableScenarios {get;set;}
    public String Sitedata {get;set;}
    public String Subjectdata {get;set;}
    public Map<String,List<String>> graphdatapoints;
        
    /**
    * @author Niharika Reddy
    * @Date 22-June-2015
    * @Description This is the constructor for getting bidId and setting buttonClick to false initially.
    */             
    public PBB_ScenarioCompareController() {
        bidproject = PBB_DataAccessor.getBidProjectByID(ApexPages.currentPage().getParameters().get('id'));    
        availableScenarios = new List<SelectOption>();
        selectedScenarios = new List<SelectOption>(); 
        for(PBB_Scenario__c  c:PBB_DataAccessor.getScenariosListByBidProjID(bidproject.id)){
            availableScenarios.add(new SelectOption(c.id, c.name+ (c.Description__c==null?'':' - '+ ((c.Description__c).length()>20?(c.Description__c).substring(0, 20):c.Description__c))));
        }       
    }
     
    public void scenarioCountCheck(){  
     
    } 
    
    /**
    * @author Niharika Reddy
    * @Date 22-June-2015
    * @Description This method executes on the Submit buttonclick and validates few conditions and holds the seleted scenario list.
    */
    public void compareScenarios(){        
        System.debug('****allScenarios******'+availableScenarios+ selectedScenarios);        
        if((selScenarioSize > 3) || (selScenarioSize < 1)){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Please select atleast 1 or atmost 3 scenarios to view the comparision.');
            ApexPages.addMessage(myMsg);
        }  
        Sitedata = '[' ;
        Subjectdata = '[' ;
        graphdatapoints = new Map<String,List<String>>();
        for(SelectOption so:selectedScenarios){
                getSiteActivationData(so.getValue(),'spline',so.getlabel());                
                Sitedata +=graphdataformat('Scenario:'+so.getlabel()+' Sites',graphdatapoints)+',';
                Subjectdata +=graphdataformat('Scenario:'+so.getlabel()+' Subjects',graphdatapoints)+',';
               
        } 
        Sitedata += ']' ;
        Subjectdata+= ']' ;
        System.debug('++++++++++++++++++Sitedata ++++++++++++++++++++++++++++++++'+Sitedata );
        System.debug('++++++++++++++++++Subjectdata ++++++++++++++++++++++++++++++++'+Subjectdata );
        System.debug('++++++++++++++++++graphdatapoints++++++++++++++++++++++++++++++++'+graphdatapoints); 
    } 
    
    /**
    * @author Niharika Reddy
    * @Date 22-June-2015
    * @Description get all the site and subject graph data points
    */
    public void getSiteActivationData(String PBBScenarioID,String chartOption,String PBBScenarioName){      
       
        for(AggregateResult ara : PBB_DataAccessor.getPBBweeklybyScenario(PBBScenarioID)){
            System.debug('-----------By Weekly=Sum-Of-planned-subjects&sites=given Scenaro ------------' + ara.get('na') + '\t' +
                                                                        ara.get('ps') +  '\t' + ara.get('psub')  + '\t' + ara.get('w'));              
            Date Weekdate = Date.valueof(ara.get('w'));
            System.debug('////weekdate/////'+ Weekdate );
            graphdatapoints = graphdatapointsbyformat(Weekdate,String.valueof(Integer.valueof(ara.get('ps'))),'Scenario:'+PBBScenarioName+' Sites',graphdatapoints);
            graphdatapoints = graphdatapointsbyformat(Weekdate,String.valueof(Integer.valueof(ara.get('psub'))),'Scenario:'+PBBScenarioName+' Subjects',graphdatapoints);           
        }      
    }
    
    /**
    * @author Niharika Reddy
    * @Date 22-June-2015
    * @Description get all Site Activation and Subject Enrollment graph data in String format
    */
    public Map<String,List<String>> graphdatapointsbyformat(Date Weekdate,String value,String type,Map<String,List<String>> datapointmap){
        String DateStr  = '[' + 'Date.UTC(' + Weekdate.year() + ',' + (Weekdate.month() - 1) + ',' + Weekdate.day() + ')' + ',' + value + ']' ;
        List<String> DateLstWithParnths = new List<String>();        
        if(!datapointmap.Containskey(type))
            datapointmap.put(type,DateLstWithParnths);
        DateLstWithParnths = datapointmap.get(type);  
        system.debug('---DateLstWithParnths --'+DateLstWithParnths );
        DateLstWithParnths.add(DateStr);
        datapointmap.put(type,DateLstWithParnths);
        system.debug('---datapointmap)--'+datapointmap);
        return datapointmap;
    } 
    
    /**
    * @author Niharika Reddy
    * @Date 22-June-2015
    * @Description get all Site Activation and Subject Enrollment graph datain String format
    */
    public String graphdataformat(String type,Map<String,List<String>> datapointmap){        
        List<String> DateLstWithParnths = new List<String>(); 
        if(!datapointmap.Containskey(type))
            datapointmap.put(type,DateLstWithParnths);
        DateLstWithParnths = datapointmap.get(type);  
        String DateLsWithoutParnths = String.join(DateLstWithParnths, ',');
        String DateLsWithoutParnthsInSquare = '[' + DateLsWithoutParnths + ']';    
        String type1;
        if(type.contains(' Sites'))
            type1 = type.replace(' Sites','');
        if(type.contains(' Subjects'))
            type1 = type.replace(' Subjects','');
        String dateComaPlan = '{' + 'name:' + '\''+type1 +'\'' + ',' +'data:' + DateLsWithoutParnthsInSquare + '}';    
        System.debug('$$$$$dateComaPlan$$$$$'+dateComaPlan);    
        return dateComaPlan;
    }     
}