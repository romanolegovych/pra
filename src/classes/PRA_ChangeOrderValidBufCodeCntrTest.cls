@isTest (seeAllData=false)
private class PRA_ChangeOrderValidBufCodeCntrTest{
  static testMethod void PRA_ChangeOrderValidBufCodeContrTest() {
       
       test.startTest();
      
       PRA_TestUtils tu = new PRA_TestUtils();
       tu.initAll();
       tu.clientProject.Project_Currency__c='USD';
       tu.clientProject.Currency_exchange_lock_date__c=date.today();
       tu.clientProject.Standard_rate_year__c=date.today();
       tu.clientProject.Estimated_End_Date__c=date.today();      
       update tu.clientProject;
       
       Change_Order__c coObj=tu.createChangeOrder(tu.clientProject.Id);
       Change_Order_Item__c coItemObj=new Change_Order_Item__c();
       coItemObj.Change_Order__c=coObj.Id;       
       coItemObj.Client_Unit_Number__c='123';
       coItemObj.End_Date__c=Date.today().addMonths(3).toStartOfMonth();
       insert coItemObj;
      
       Function_Code__c fc=new Function_Code__c();
       fc.name='DE';
       insert fc;
      
       Function_Code__c fc1=new Function_Code__c();
       fc1.name='FG';
       insert fc1;
      
       Function_Code__c fc2=new Function_Code__c();
       fc2.name='HI';
       insert fc2;
      
       Business_Unit__c bu=new Business_Unit__c();          
       bu.name='ABC';
       insert bu;
      
       BUF_Code__c bc1 = new BUF_Code__c();
       bc1.Function_Code__c=fc.id;
       bc1.Business_Unit__c=bu.id;
       bc1.name='ABCDE';      
       insert bc1;
      
       BUF_Code__c bc2 = new BUF_Code__c();
       bc2.Function_Code__c=fc1.id;
       bc2.Business_Unit__c=bu.id;
       bc2.name='ABCFG';      
       insert bc2;
      
       BUF_Code__c bc3 = new BUF_Code__c();
       bc3.Function_Code__c=fc2.id;
       bc3.Business_Unit__c=bu.id;
       bc3.name='ABCHI';      
       insert bc3;
        
       Change_Order_Line_Item__c coLineItemObj=new Change_Order_Line_Item__c(Change_Order_Item__c=coItemObj.id,BUF_Code__c=bc1.id);
       coLineItemObj.Type__c='New';
       coLineItemObj.Is_Labor_Unit__c=true;
       insert coLineItemObj;
       
      
       Client_task__c ct=[select id,name from client_task__c where project__c=:tu.clientProject.id limit 1];
      
       Change_Order_Line_Item__c coLineItemObj1=new Change_Order_Line_Item__c(Change_Order_Item__c=coItemObj.id,BUF_Code__c=bc2.id);
       coLineItemObj1.Type__c='Existing';
     
       coLineItemObj1.Is_Labor_Unit__c=false;
       insert coLineItemObj1;
      
       Valid_Project_Buf_Code__c vpbf= new Valid_Project_Buf_Code__c();
       vpbf.BUF_Code__c=bc3.id;
       //vpbf.Convert_To_Code__c='USD';
       //datetime CELDT=(date.today());
       //CELDT=CELDT.addHours(CELDT.hour());
       //String  CELDate=CELDT.format('yyyyMMdd');
       //vpbf.Currency_Date_Key__c=CELDate;
       vpbf.Standard_Rate_Year__c=Decimal.valueof(date.today().year());
       vpbf.Cost_Rate_End_Year__c=Decimal.valueof(date.today().year());
       //vpbf.Currency_End_Date_Key__c=CELDate;      
       vpbf.Currency_Code__c='USD';
       insert vpbf;
       system.debug('---------------------------------'+vpbf); 
      
       PageReference oPage = new PageReference('/apex/PRA_change_order_validbufcode?id='+coItemObj.Id+'&unit=labor');
       Test.setCurrentPage(oPage);
       PRA_ChangeOrderValidBufCodeController coControllerObj=new PRA_ChangeOrderValidBufCodeController(); 
      
       system.debug('----'+coControllerObj.COIid);
       coControllerObj.getselectedBUFCode();
       coControllerObj.getselectedAvailableBUFCode();
      
       coControllerObj.getBusinessUnitOptions();      
       coControllerObj.getFunctionCodeOptions();
      
       coControllerObj.getAvailableBUFCodeList();
       coControllerObj.getselectedBUFCodeList();
      
       coControllerObj.SelectedBusinessUnit='ABC';      
       coControllerObj.doFilter();
       coControllerObj.SelectedBusinessUnit=null;
       coControllerObj.SelectedFunctionCode='HI';
       coControllerObj.doFilter();
      
       coControllerObj.SelectedBusinessUnit='ABC';
       coControllerObj.SelectedFunctionCode='HI';
       coControllerObj.doFilter();
      
       string[] ab = new List<string>();
       ab.add('ABCHI');
       coControllerObj.setselectedAvailableBUFCode(ab);
       coControllerObj.doadd();
      
       string[] sb = new List<string>();
       sb.add('ABCDE');
       coControllerObj.setselectedBUFCode(sb);
       coControllerObj.doremove();      
       coControllerObj.apply();  
      
      
      
      PageReference oPage1 = new PageReference('/apex/PRA_change_order_validbufcode?id='+coItemObj.Id+'&move=move');
      Test.setCurrentPage(oPage1);
      PRA_ChangeOrderValidBufCodeController coControllerObj1=new PRA_ChangeOrderValidBufCodeController();
      
      string[] ab1 = new List<string>();
      ab1.add('ABCHI');
      coControllerObj1.setselectedAvailableBUFCode(ab1);
      coControllerObj1.doadd();
      
      
      string[] sb1 = new List<string>();
      sb1.add('ABCFG');
      coControllerObj1.setselectedBUFCode(sb1);
      coControllerObj1.doremove();      
       
      
      bid_contract_detail__c bcd=new bid_contract_detail__c();
      bcd.Client_Task__c=ct.id;
      bcd.buf_code__c=bc2.id;
      insert bcd;
      
      coItemObj.client_task__c=ct.id;
      update coitemobj;
      
      bid_contract_detail__c bb=[select id, buf_code__c from bid_contract_detail__c where client_task__c=:coitemobj.client_task__c limit 1];
      system.debug('--------------------'+coitemobj+ct+bcd+bb);
      coControllerObj1.move();
      
      test.stopTest();         
  } 
}