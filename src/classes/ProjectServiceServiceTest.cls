@isTest
private class ProjectServiceServiceTest {

	static testMethod void getProjectServiceForServiceTest() {
	
		// create a project
		Client_Project__c cp = BDT_TestDataUtils.buildProject();
		insert cp;
		
		// create a couple of services
		List<ServiceCategory__c> scList = BDT_TestDataUtils.buildServiceCategory(1, null);
		insert scList;
		List<Service__c> sList = BDT_TestDataUtils.buildService(scList, 3, 'Test Service', false);
		insert sList;
		
		Set<ID> ServiceIdSet = new Set<ID>();
		For (Service__c s:sList){
			ServiceIdSet.add(s.id);
		}
		
		// call routine
		map<id,ProjectService__c> result =  ProjectServiceService.getProjectServiceForService (ServiceIdSet,cp.id,true);
		
		// verify that all services now have a project service record
		For (Service__c s:sList){
			system.assert(result.containsKey(s.id));
			system.assertNotEquals(null,result.get(s.id).id);
		}
	
	}
}