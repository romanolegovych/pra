@istest public with sharing class CCEmailHandlerTest
{
	@istest private static void coverHandler()
	{
		Map<String, SObject> sobjectMap = CCApiTest.setup();
		//global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope)
		Messaging.InboundEmail email = new Messaging.InboundEmail();
		email.subject = 'How many time is remaining for critical step "' + sobjectMap.get('Critical Chain Step Junction').get('Name') + '"(' + sobjectMap.get('Critical Chain Step Junction').Id + ')';
		email.plainTextBody = '01-JAN-' + System.today().addYears(1).year();
		
		new CCEmailHandler().handleInboundEmail(email, null);
	}
}