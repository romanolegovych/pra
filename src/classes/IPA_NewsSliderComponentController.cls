public class IPA_NewsSliderComponentController
{
    public List<IPA_Articles__c> lstNewsSlider {get; set;}
    
    public IPA_NewsSliderComponentController()
    {
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        lstNewsSlider = ipa_bo.returnEmpNewsForSlider();
    }
}