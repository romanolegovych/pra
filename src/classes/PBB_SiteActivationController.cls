/** 
@Author Niharika Reddy
@Date 2015
@Description this class is for showing the results of Site Activation graph and its Grid.
*/
public class PBB_SiteActivationController {
    
    //PBB Scenario object ID
    public String PBBScenarioID{ get; set; }  
    
    //PBB Scenario object             
    public PBB_Scenario__c PBBScenario{ get; set; }         
    
    //to hide/show the main tabs in the component.
    public  List<String> HiddenHomeTabsList{get;set;}
    
    //to hide/show the subtabs in the component.
    public  List<String> HiddenSubTabsList{get;set;}
    
    public String chartOption { get; set; }
    public String sURL { get; set; }
    
    /** 
    @Author Niharika Reddy
    @Date 2015
    @Description this is the constructor for  Site Activation
    */
    public PBB_SiteActivationController () {    
        HiddenHomeTabsList = new List<String>();
        HiddenSubTabsList = new List<String>();
        HiddenSubTabsList.add('RDpage');        
        HiddenSubTabsList.add('Ppage');
        
        //getting PBB Scenario ID from URL
        PBBScenarioID = ApexPages.currentPage().getParameters().get('PBBScenarioID');
        
        if(PBBScenarioID!=null && PBBScenarioID!=''){
            system.debug('-----------PBBScenarioID---------'+PBBScenarioID);
            PBBScenario = PBB_DataAccessor.getScenarioByID(pbbScenarioID);
            sURL = URL.getSalesforceBaseUrl().toExternalForm();     
            sURL += '/apex/PBB_SiteActivationResults?id='+PBBScenarioID;
        }
        
    }
}