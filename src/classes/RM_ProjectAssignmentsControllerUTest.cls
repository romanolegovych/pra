/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_ProjectAssignmentsControllerUTest {
	static list<Country__c> lstCountry;	
    static List<WFM_Location_Hours__c> lstHrs;
    static list<Employee_Details__c> lstEmpl; 
    static list<Employee_Status__c> lstEmplStatus;
    
    static list<WFM_Client__c> lstClient;
    static list<WFM_Contract__c>  lstContract;
    static list<WFM_Project__c> lstProject;
  	static List<WFM_Employee_Availability__c> lstAvas;
  	static list<Job_Class_Desc__c> lstJob;
    static list<Business_Unit__c> lstBU;
    static RM_UnitTestData init(){
    	RM_UnitTestData testData = new RM_UnitTestData();
    	lstCountry = testData.lstCountry;
    	lstEmpl = testData.lstEmpl;
    	lstEmplStatus = testData.lstEmplStatus;
    	lstClient = testData.lstClient;
    	lstContract = testData.lstContract;
    	lstProject = testData.lstProject;
    	lstJob = testData.lstJobs;
    	lstBU = testData.lstLocation;
    	
    	return testData;
    }
    @future
    private static void GroupMapping()
    {  Group g1 = new Group(name = 'RM_Resource_group',DeveloperName='RM_Resource_group');        
        insert g1;
        GroupMember gm1 = new GroupMember();
        gm1.GroupId=g1.id;
        gm1.UserOrGroupId=UserInfo.getUserId();
        database.insert(gm1); 
        system.debug('----g1-----'+gm1);
        
        PRA_Business_Unit__c PBU = new PRA_Business_Unit__c(name = 'Clinical Informatics',Business_Unit_Code__c='CI');        
        insert PBU; 
        
        WFM_BU_Group_Mapping__c bug=new WFM_BU_Group_Mapping__c();
        bug.PRA_Business_Unit__c=pbu.id;
        bug.Group_Name__c='RM_Resource_group';
        insert bug;
        
    }
   static testMethod void TestController() {
        RM_UnitTestData testData = init();
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        system.assert(testProj.project.id == lstProject[0].id); 
      //  system.assert(testProj.assStartDate == RM_Tools.GetStringfromDate(Date.today(), 'mm/dd/yyyy'));        
        Test.stopTest(); 
    }
    static testMethod void TestDropDown() { 
        RM_UnitTestData testData = init();
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        List<SelectOption> searchItem = testProj.getSearchOnItems();        
        system.assert(searchItem.size() > 0); 
        List<SelectOption> roles = testProj.getRoles();     
        system.assert(roles.size() > 0);
        List<SelectOption> countrys = testProj.getCountry();
        system.assert(countrys.size() > 0);
        Test.stopTest(); 
    }
    static testMethod void TestGetSingleNeg() {
        RM_UnitTestData testData = init();
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        testProj.searchOn = 'Employee';
        testProj.selSearch = '';
        List<SelectOption> names = testProj.getEmployeeNameList();
       
        
        system.assert(names.size() == 0); 
              
        Test.stopTest(); 
    }
    static testMethod void TestGetSinglePOS() {
        RM_UnitTestData testData = init();
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         	Test.startTest();
         	GroupMapping();
       
	        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
	        Test.setCurrentPage(pageGroup);
	        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
	        system.debug('---------lstEmpl[0].name---------' + lstEmpl[0].name);
	        Employee_Details__c e = RM_EmployeeService.GetEmployeeDetailByEmployeeID(lstEmpl[0].name);
	        testProj.searchOn = 'Employee';
	        testProj.selSearch = string.valueOf(e.id);
	        testProj.strSearchText = e.full_name__c;
	        testProj.GetEmployee();
	        List<SelectOption> names = testProj.getEmployeeNameList();
	        SelectOption aName = names[0];
	        system.debug('---------names[0]---------' + names[0]);
	        system.assert(aName.getValue() == e.id); 
	              
	        Test.stopTest(); 
        }
    }
  
    static testMethod void TestGetThroughManagerPOS() {
        RM_UnitTestData testData = init();
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        system.debug('---------lstEmpl[0].name---------' + lstEmpl[1].name);
        Employee_Details__c e = RM_EmployeeService.GetEmployeeDetailByEmployeeID(lstEmpl[1].name);
        testProj.searchOn = 'Manager';
        testProj.selSearch = e.id;
        testProj.SearchType();
        testProj.GetEmployee();
        List<Employee_Details__c> eEmployee = RM_EmployeeService.GetEmployeeDetailByEmployeeSupervisor(lstEmpl[1].name);
        List<SelectOption> names = testProj.getEmployeeNameList();
        SelectOption aName = names[0];
        system.debug('---------aName.getValue()---------' + aName.getValue());
        system.assert(aName.getValue() == eEmployee[0].id); 
              
        Test.stopTest(); 
    }
    static testMethod void TestGetThroughCopyPOS() {
        RM_UnitTestData testData = init();
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         	Test.startTest();
         	GroupMapping();
       
	        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
	        Test.setCurrentPage(pageGroup);
	        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
	        system.debug('---------lstEmpl[0].name---------' + lstEmpl[1].name);
	        Employee_Details__c e = RM_EmployeeService.GetEmployeeDetailByEmployeeID(lstEmpl[0].name);
	        Employee_Details__c e1 = RM_EmployeeService.GetEmployeeDetailByEmployeeID(lstEmpl[1].name);
	        testProj.searchOn = 'Copy/Paste';
	        testProj.copyName = e.full_name__c + '\n' + e1.full_name__c;
	        testProj.SearchType();
	        testProj.validateCopy();
	        testProj.GetEmployee();
	        List<SelectOption> names = testProj.getEmployeeNameList();
	        SelectOption aName = names[0];
	        system.debug('---------aName.getValue()---------' + aName.getValue());
	        system.assert(aName.getValue() == e1.id || aName.getValue() == e.ID); 
	              
	        Test.stopTest(); 
        }
    }
    static testMethod void TestEnable() {
        RM_UnitTestData testData = init();
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        testProj.EnableViewOnly(); 
        system.assert(testProj.bInsert == false); 
        testProj.EnableEdit();
        system.assert(testProj.bEdit == true); 
        testProj.EnableInsert();
        system.assert(testProj.bInsert == true);       
        Test.stopTest(); 
    }
     static testMethod void TestAddAssignment() {
        RM_UnitTestData testData = init();
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        system.debug('---------lstEmpl[0].name---------' + lstEmpl[0].name);
        Employee_Details__c e = RM_EmployeeService.GetEmployeeDetailByEmployeeID(lstEmpl[0].name);
        testProj.searchOn = 'Employee';
        testProj.selSearch = e.id;
        testProj.strSearchText = e.full_name__c;
        testProj.GetEmployee();
        List<SelectOption> names = testProj.getEmployeeNameList();
        SelectOption aName = names[0];
        system.debug('---------aName.getValue()---------' + aName.getValue());
        system.assert(aName.getValue() == e.id); 
        testProj.AddAssignment();
        system.assert(testProj.ProAssigns.size() > 0);
        
        list<RM_projectAssignmentsController.ProjectBatchAssignment> lstBatch = testProj.GetAddAssignments();
        system.assert(testProj.ProAssigns[0] == lstBatch[0]);       
        Test.stopTest(); 
    }
     static testMethod void TestRemoveAssignment() {
        RM_UnitTestData testData = init();
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        system.debug('---------lstEmpl[0].name---------' + lstEmpl[0].name);
        Employee_Details__c e = RM_EmployeeService.GetEmployeeDetailByEmployeeID(lstEmpl[0].name);
        testProj.searchOn = 'Employee';
        testProj.selSearch = e.id;
        testProj.strSearchText = e.full_name__c;
        testProj.GetEmployee();
        List<SelectOption> names = testProj.getEmployeeNameList();
        SelectOption aName = names[0];
        system.debug('---------aName.getValue()---------' + aName.getValue());
        system.assert(aName.getValue() == e.id); 
        testProj.AddAssignment();
        system.assert(testProj.ProAssigns.size() > 0);
        testProj.SelectedAdd ='0';  
        testProj.Remove();
        system.assert(testProj.ProAssigns.size() == 0);       
        Test.stopTest(); 
    }
      static testMethod void TestCancelAssignment() {
        RM_UnitTestData testData = init();
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        system.debug('---------lstEmpl[0].name---------' + lstEmpl[0].name);
        Employee_Details__c e = RM_EmployeeService.GetEmployeeDetailByEmployeeID(lstEmpl[0].name);
        testProj.searchOn = 'Employee';
        testProj.selSearch = e.id;
        testProj.strSearchText = e.full_name__c;
        testProj.GetEmployee();
        List<SelectOption> names = testProj.getEmployeeNameList();
        SelectOption aName = names[0];
        system.debug('---------aName.getValue()---------' + aName.getValue());
        system.assert(aName.getValue() == e.id); 
        testProj.AddAssignment();
        system.assert(testProj.ProAssigns.size() > 0);
       
        testProj.CancelAssignment();
        system.assert(testProj.ProAssigns.size() == 0);       
        Test.stopTest(); 
    }
    static testMethod void TestInsertAssignment() {
        RM_UnitTestData testData = init();
        lstHrs = testData.insertLocationHr(-4, 3, 'TTT'); 
        lstAvas = testData.insertAvailability(lstEmpl[0], lstHrs);
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        system.debug('---------lstEmpl[0].name---------' + lstEmpl[0].name);
        Employee_Details__c e = RM_EmployeeService.GetEmployeeDetailByEmployeeID(lstEmpl[0].name);
        testProj.searchOn = 'Employee';
        testProj.selSearch = e.id;
        testProj.strSearchText = e.full_name__c;
        testProj.GetEmployee();
        List<SelectOption> names = testProj.getEmployeeNameList();
        SelectOption aName = names[0];
       
        system.debug('---------aName.getValue()---------' + aName.getValue());
        system.assert(aName.getValue() == e.id); 
        testProj.AddAssignment();
        system.assert(testProj.ProAssigns.size() > 0);     
        //insert  
        testProj.bAssigninHour = false;
        testProj.ProAssigns[0].reqID = 'TestRequst';
        testProj.ProAssigns[0].bLead = false;
        testProj.ProAssigns[0].bPSSV = false;
        testProj.ProAssigns[0].bUnBL = false;
        testProj.ProAssigns[0].assignValue = '0.2';
        system.debug('---------testProj.ProAssigns[0]---------' + testProj.ProAssigns[0]);
        testProj.InsertAssignment();
        
        system.assert(testProj.ProAssigns.size() == 0);
        
        Test.stopTest(); 
    }
    static testMethod void TestInsertAssignmentwithUpdateAllocation() {
        RM_UnitTestData testData = init();
        lstHrs = testData.insertLocationHr(-4, 3, 'TTT');
        lstAvas = testData.insertAvailability(lstEmpl[0], lstHrs);
        Date startDate = Date.today().addMonths(-4);
        Date endDate = Date.today().addMonths(-3);
       
        Employee_Details__c empl = RM_EmployeeService.GetEmployeeDetailByEmployee(lstEmpl[0].ID);
         system.debug('---------lstEmpl[0]---------' + empl);
        WFM_employee_Allocations__c alloc = testData.insertAllocation(empl, lstProject[0]);
        
        system.debug('---------Test alloc---------' + alloc);
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        system.debug('---------lstEmpl[0].name---------' + lstEmpl[0].name);
        Employee_Details__c e = RM_EmployeeService.GetEmployeeDetailByEmployeeID(lstEmpl[0].name);
        testProj.searchOn = 'Employee';
        testProj.selSearch = e.id;
        testProj.strSearchText = e.full_name__c;
        testProj.GetEmployee();
        List<SelectOption> names = testProj.getEmployeeNameList();
        SelectOption aName = names[0];
       
        system.debug('---------aName.getValue()---------' + aName.getValue());
        system.assert(aName.getValue() == e.id); 
        testProj.AddAssignment();
        system.assert(testProj.ProAssigns.size() > 0);     
        //insert  
        testProj.bAssigninHour = false;
        testProj.ProAssigns[0].reqID = 'TestRequst';
        testProj.ProAssigns[0].bLead = false;
        testProj.ProAssigns[0].bPSSV = false;
        testProj.ProAssigns[0].bUnBL = false;
        testProj.ProAssigns[0].assignValue = '0.2';
        system.debug('---------testProj.ProAssigns[0]---------' + testProj.ProAssigns[0]);
        testProj.InsertAssignment();
        
      //  system.assert(testProj.ProAssigns.size() == 0);
        
        Test.stopTest(); 
    }
    static testMethod void TestInsertAssignmentNEG() {
        RM_UnitTestData testData = init();
        lstHrs = testData.insertLocationHr(-4, 3, 'TTT');
        lstAvas = testData.insertAvailability(lstEmpl[0], lstHrs);
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        system.debug('---------lstEmpl[0].name---------' + lstEmpl[0].name);
        Employee_Details__c e = RM_EmployeeService.GetEmployeeDetailByEmployeeID(lstEmpl[0].name);
        testProj.searchOn = 'Employee';
        testProj.selSearch = e.id;
        testProj.strSearchText = e.full_name__c;
        testProj.GetEmployee();
        List<SelectOption> names = testProj.getEmployeeNameList();
        SelectOption aName = names[0];
        system.debug('---------aName.getValue()---------' + aName.getValue());
        system.assert(aName.getValue() == e.id); 
        testProj.AddAssignment();
        system.assert(testProj.ProAssigns.size() > 0);     
        //insert  
        testProj.bAssigninHour = false;
        testProj.ProAssigns[0].reqID = 'TestRequst';
        testProj.ProAssigns[0].bLead = false;
        testProj.ProAssigns[0].bPSSV = false;
        testProj.ProAssigns[0].bUnBL = false;
        testProj.ProAssigns[0].assignValue = '';
        testProj.InsertAssignment();
        
        system.assert(testProj.ProAssigns.size() > 0);
        
        Test.stopTest(); 
    }
    static testMethod void TestInsertAssignmentNEGValueToBig() {
        RM_UnitTestData testData = init();
        lstHrs = testData.insertLocationHr(-4, 3, 'TTT'); 
        lstAvas = testData.insertAvailability(lstEmpl[0], lstHrs);
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        system.debug('---------lstEmpl[0].name---------' + lstEmpl[0].name);
        Employee_Details__c e = RM_EmployeeService.GetEmployeeDetailByEmployeeID(lstEmpl[0].name);
        testProj.searchOn = 'Employee';
        testProj.selSearch = e.id;
        testProj.strSearchText = e.full_name__c;
        testProj.GetEmployee();
        List<SelectOption> names = testProj.getEmployeeNameList();
        SelectOption aName = names[0];
        system.debug('---------aName.getValue()---------' + aName.getValue());
        system.assert(aName.getValue() == e.id); 
        testProj.AddAssignment();
        system.assert(testProj.ProAssigns.size() > 0);     
        //insert  
        testProj.bAssigninHour = false;
        testProj.ProAssigns[0].reqID = 'TestRequst';
        testProj.ProAssigns[0].bLead = false;
        testProj.ProAssigns[0].bPSSV = false;
        testProj.ProAssigns[0].bUnBL = false;
        testProj.ProAssigns[0].assignValue = '2';
        testProj.InsertAssignment();
        
        system.assert(testProj.ProAssigns.size() > 0);
        
        Test.stopTest(); 
    }
    static testMethod void TestInsertAssignmentNEGnoRole() {
        RM_UnitTestData testData = init();
        lstHrs = testData.insertLocationHr(-4, 3, 'TTT');
        lstAvas = testData.insertAvailability(lstEmpl[0], lstHrs);
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        system.debug('---------lstEmpl[0].name---------' + lstEmpl[0].name);
        Employee_Details__c e = RM_EmployeeService.GetEmployeeDetailByEmployeeID(lstEmpl[0].name);
        testProj.searchOn = 'Employee';
        testProj.selSearch = e.id;
        testProj.strSearchText = e.full_name__c;
        testProj.GetEmployee();
        List<SelectOption> names = testProj.getEmployeeNameList();
        SelectOption aName = names[0];
        system.debug('---------aName.getValue()---------' + aName.getValue());
        system.assert(aName.getValue() == e.id); 
        testProj.AddAssignment();
        system.assert(testProj.ProAssigns.size() > 0);     
        //insert  
        testProj.bAssigninHour = false;
        testProj.ProAssigns[0].reqID = 'TestRequst';
        testProj.ProAssigns[0].bLead = false;
        testProj.ProAssigns[0].bPSSV = false;
        testProj.ProAssigns[0].bUnBL = false;
        testProj.ProAssigns[0].assignValue = '0.2';
        testProj.ProAssigns[0].role='';
        testProj.InsertAssignment();
        
        system.assert(testProj.ProAssigns.size() > 0);
        
        Test.stopTest(); 
    }
     /*   static testMethod void TestInsertAssignmentNEGbadBufCode() {
        RM_UnitTestData testData = init();
        lstHrs = testData.insertLocationHr(-4, 3, lstBU[0].name);  
        lstAvas = testData.insertAvailability(lstEmpl[0], lstHrs);
        Test.startTest();
        PageReference pageGroup = new PageReference('/apex/RM_ProjectAssignments?id='+lstProject[0].id);
        Test.setCurrentPage(pageGroup);
        RM_projectAssignmentsController testProj = new RM_projectAssignmentsController();
        system.debug('---------lstEmpl[0].name---------' + lstEmpl[0].name);
        Employee_Details__c e = RM_EmployeeService.GetEmployeeDetailByEmployeeID(lstEmpl[0].name);
        testProj.searchOn = 'Employee';
        testProj.selSearch = e.id;
        testProj.strSearchText = e.full_name__c;
        testProj.GetEmployee();
        List<SelectOption> names = testProj.getEmployeeNameList();
        SelectOption aName = names[0];
        system.debug('---------aName.getValue()---------' + aName.getValue());
        system.assert(aName.getValue() == e.id); 
        testProj.AddAssignment();
        system.assert(testProj.ProAssigns.size() > 0);   
        system.debug('---------testProj Before---------' + testProj.ProAssigns);  
        //insert  
        testProj.bAssigninHour = false;
        testProj.ProAssigns[0].reqID = 'TestRequst';
        testProj.ProAssigns[0].bLead = false;
        testProj.ProAssigns[0].bPSSV = false;
        testProj.ProAssigns[0].bUnBL = false;
        testProj.ProAssigns[0].assignValue = '0.2';
        testProj.ProAssigns[0].buCode='XXX';
        testProj.InsertAssignment();
        system.debug('---------testProj---------' + testProj.ProAssigns);
        system.assert(testProj.ProAssigns.size() > 0);
        
        Test.stopTest(); 
    }*/
}