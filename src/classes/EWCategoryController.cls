public with sharing class EWCategoryController
{
    public ApexPages.StandardController controller;
    public EW_Category__c thisCategory {get; set;}
    public List<EWWrapper> stepWrappers {get; set;}
    public List<EWCategoryWrapper> categoryWrappers {get; set;}
    
    public static String categoryDataSource 
    {
        get
        {
            if (categoryDataSource == null)
            {
                categoryDataSource = 'PAWS_Project_Flow_Site__c';
            }
            
            return categoryDataSource;
        }
        set;
    }
    
    public List<SelectOption> categoryDataSourceOptions
    {
        get
        {
            return new List<SelectOption>{
                new SelectOption('PAWS_Project_Flow__c', 'PAWS Project Flow'),
                new SelectOption('PAWS_Project_Flow_Country__c', 'PAWS Project Flow Country'),
                new SelectOption('PAWS_Project_Flow_Site__c', 'PAWS Project Flow Site')
            };
        }
    }
          
    public void getCategory()
    {
        thisCategory.Type__c = 'Template';
        thisCategory.Buffer_Index_Strategy__c = 'Min';
        
        for (EW_Category__c category : [select Name, ID, Buffer_Index__c, Buffer_Index_Strategy__c, Cloned_From_Category__c, Max_Buffer_Index__c, Min_Buffer_Index__c, Type__c from EW_Category__c where Id = :ApexPages.currentPage().getParameters().get('Id')])
        {
            thisCategory = category;
        }
    }
    
   
    
    public PageReference save()
    {
        PageReference page;
        try
        {
            upsert thisCategory;
            saveRecords();
            page = new PageReference('/' + thisCategory.Id);
        }
        catch (Exception e)
        {
            ApexPages.addMessages(e);
        }
        return page;
    }
          
    private List<EW_Category_EW_Junction__c> getJunctions()
    {
        return [select Child_Category__c, Early_Warning__c, Early_Warning__r.Flow__c, Early_Warning__r.Name from EW_Category_EW_Junction__c where Parent_Category__c = :thisCategory.Id];
    } 
    
    public EWCategoryController(ApexPages.StandardController controller)
    {
        this.controller = controller;
        thisCategory = new EW_Category__c();
        getCategory();
        loadWrappers();
    }

    public void onDataSourceChange()
    {
        loadWrappers();
    }
    
    public List<Critical_Chain__c> getEWTemplates()
    {
        return [select Name, Flow__r.Name, Flow__r.STSWR1__Type__c, Flow__r.STSWR1__Object_Type__c from Critical_Chain__c where PAWS_Project_Flow__c = null AND Flow__r.STSWR1__Type__c = 'Template' and Flow__r.STSWR1__Object_Type__c = :CategoryDataSource  order by Name];
    }
    
    private List <EW_Category__c> getTemplateCategories ()
    {
        List <Critical_Chain__c> templateWarnings = getEWTemplates();
        return [select Name, ID, Buffer_Index__c, Buffer_Index_Strategy__c, Cloned_From_Category__c, Max_Buffer_Index__c, Min_Buffer_Index__c, Type__c from EW_Category__c WHERE Type__c = 'Template' and ID IN
        (SELECT Parent_Category__c from EW_Category_EW_Junction__c WHERE Early_Warning__c IN :templateWarnings) order by Name];
    }
    
    private void loadWrappers()
    {
        Set <ID> IDs = new Set<ID>();
        for ( EW_Category_EW_Junction__c junction : getJunctions())
        {
            IDs.add(junction.Early_Warning__c); 
            IDs.add(junction.Child_Category__c);
        }        
        
        stepWrappers = new List<EWWrapper>();
        categoryWrappers = new List<EWCategoryWrapper>();
        for (Critical_Chain__c eachEW : getEWTemplates())
        {
            EWWrapper wrapper = new EWWrapper(eachEW);
            wrapper.selected = (IDs.contains(wrapper.EWWrapped.ID) ? true : false); 
            stepWrappers.add(wrapper);
        }
        for (EW_Category__c eachCategory : getTemplateCategories())
        {
            if( eachCategory.ID != thisCategory.ID ) 
            {
                EWCategoryWrapper wrapper = new EWCategoryWrapper(eachCategory);
                wrapper.selected = (IDs.contains(wrapper.EWCategoryWrapped.ID) ? true : false); 
                categoryWrappers.add(wrapper);
            }
        }
    }
    
    private void saveRecords() 
    { 
        List <EW_Category_EW_Junction__c> recordsToSave = new List <EW_Category_EW_Junction__c>();
        for (EWWrapper junction : stepWrappers)
        {   
            if (junction.selected)
            {
                recordsToSave.add(new EW_Category_EW_Junction__c (Early_Warning__c = junction.EWWrapped.ID, Parent_Category__c = thisCategory.ID)); 
            }
        }
        for (EWCategoryWrapper category : categoryWrappers)
        {   
            if (category.selected)
            {
                recordsToSave.add(new EW_Category_EW_Junction__c (Child_Category__c = category.EWCategoryWrapped.ID, Parent_Category__c = thisCategory.ID)); 
            }
        }
        delete [select Name, Child_Category__c from EW_Category_EW_Junction__c where Parent_Category__c = :thisCategory.Id and Id not in :recordsToSave];  
        upsert recordsToSave;
    }
    
    public class EWWrapper
    {
        public Critical_Chain__c EWWrapped {get; set;}
        public Boolean selected {get; set;}
        public EWWrapper(Critical_Chain__c chain)
        {
            EWWrapped = chain;
        }
    }
    
    public class EWCategoryWrapper
    {
        public EW_Category__c EWCategoryWrapped {get; set;}
        public Boolean selected {get; set;}
        public EWCategoryWrapper(EW_Category__c category)
        {
            EWCategoryWrapped = category;
        }
    }
        
}