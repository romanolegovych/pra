@IsTest
/** 
     * @description Test class CM_ReceiveSignedAgreement- 
     * @author      Karthikeyan Sounderrajan
     * @date        Created: 27-Feb-2015
     */
     
private class CM_ReceiveSignedAgreementTest{
	
/** 
     * @description Test method CM_ReceiveSignedAgreement- Test for Global Class  
     * @author      Karthikeyan Sounderrajan
     * @date        Created: 27-Feb-2015
     */
	private static final RecordType RT_Agreement = [SELECT Id from RecordType WHERE Name = 'Contract Agreement'];
	
	static testMethod void CM_ReceiveAgreementMethod() {
 	 WFM_Project__c testProject = new WFM_Project__c();
     WFM_Protocol__c testProtocol = new WFM_Protocol__c();
     WFM_Site_Detail__c testSite = new WFM_Site_Detail__c();
     Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c();
	 
	 //Test Project 
     testProject.Name = 'testProject123';
     testProject.Project_Unique_Key__c = 'Projectabcd';
     insert testProject;
     
     //Test Protocol
	 testProtocol.Name = 'testProtocolabcd';
     testProtocol.Protocal_Unique_Key__c = 'Protocolsfgij';
     testProtocol.Project_ID__c = testProject.Id;
     insert testProtocol;
     
     //Test Site
	 testSite.Project_Protocol__c = testProtocol.Id;
     testSite.Site_ID__c = '012345432';
     testSite.Name = 'tstSiteabcd';
     insert testSite;
     
     //Test Agreement
     agreement.RecordTypeId = RT_Agreement.Id;
     agreement.name='Test unit Agreement abcd';
     agreement.PRA_Status__c = 'At Site Review';   
     agreement.Contract_Type__c='Master CTA';     
     agreement.Site__c = testSite.Id;
     insert agreement;
          
     //Instances of Messaging.InboundEmail and Messaging.InboundEnvelope
     Messaging.InboundEmail email = new Messaging.InboundEmail();
     Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
     Apttus__APTS_Agreement__c Agree=[select id, Apttus__FF_Agreement_Number__c, Apttus__Agreement_Number__c from Apttus__APTS_Agreement__c where id=:agreement.id limit 1];

        
     // Set the Email address, Subject 
     env.fromAddress=COM_Utils.generateRandomEmail();
     email.subject =Agree.Apttus__FF_Agreement_Number__c +'|Partially Executed|New Comment|FW: Sandbox: Agreement ACME4_PUK0';
     email.fromname = 'FirstName LastName';
        
    //	Binary Attachment
	 Messaging.InboundEmail.binaryAttachment battachment = new Messaging.InboundEmail.binaryAttachment();
	 battachment.body = blob.valueof('my attachment text');
	 battachment.fileName = 'textfile.doc';
	 battachment.mimeTypeSubType = 'text/plain';
        
	 Messaging.InboundEmail.textAttachment tattachment = new Messaging.InboundEmail.textAttachment();
	 tattachment.body = 'my attachment text';
	 tattachment.fileName = 'textfile.doc';
	 tattachment.mimeTypeSubType = 'text/plain';	
        
	 // List of Binary And Text Attachments
	 email.binaryAttachments =new Messaging.inboundEmail.BinaryAttachment[] { battachment };
	 email.textAttachments =new Messaging.inboundEmail.textAttachment[] { tattachment };
	 
     CM_ReceiveSignedAgreement ser= new CM_ReceiveSignedAgreement();
    
     //Instances of Messaging.InboundEmail and Messaging.InboundEnvelope
     Messaging.InboundEmail emailb = new Messaging.InboundEmail();
     Messaging.InboundEnvelope envb = new Messaging.InboundEnvelope();
     
     // Set the Email address, Subject 
        
     envb.fromAddress=COM_Utils.generateRandomEmail();
     emailb.subject = '00000366.0Partially ExecutedFW: Sandbox: Agreement ACME4_PUK0';
     emailb.fromname = 'FirstName LastName';
        
    //	Binary Attachment
	 Messaging.InboundEmail.binaryAttachment battachmentb = new Messaging.InboundEmail.binaryAttachment();
	 battachmentb.body = blob.valueof('my attachment text');
	 battachmentb.fileName = 'textfile.doc';
	 battachmentb.mimeTypeSubType = 'application/octet-stream';	
	 
	 //List of Binary And Text Attachments
	 emailb.binaryAttachments =new Messaging.inboundEmail.BinaryAttachment[] { battachmentb };
	 	 
     CM_ReceiveSignedAgreement serb= new CM_ReceiveSignedAgreement();
     
      //Instances of Messaging.InboundEmail and Messaging.InboundEnvelope for testing invalid status
     Messaging.InboundEmail emailc = new Messaging.InboundEmail();
     Messaging.InboundEnvelope envc = new Messaging.InboundEnvelope();
     
     // Set the Email address, Subject 
     envc.fromAddress=COM_Utils.generateRandomEmail();
     emailc.subject = Agree.Apttus__FF_Agreement_Number__c +'|PartiaExecuted|New Comment added|FW: Sandbox: Agreement ACME4_PUK0';
     emailc.fromname = 'FirstName LastName';
     
     CM_ReceiveSignedAgreement serc= new CM_ReceiveSignedAgreement();
        
     Test.startTest();
     Messaging.InboundEmailResult result = ser.handleInboundEmail(email, env);
     Messaging.InboundEmailResult resultc = serc.handleInboundEmail(emailc, envc);
     Messaging.InboundEmailResult resultb = serb.handleInboundEmail(emailb, envb);
     Test.stopTest();
	} 
}