/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_ViewAssignmentsCompControllerUTest {

    static COUNTRY__c country;
    static List<WFM_Location_Hours__c> hrs;
    static List<WFM_Employee_Availability__c> avas;
    static WFM_employee_Allocations__c alloc;
    static WFM_employee_Allocations__c alloc1;
    static WFM_employee_Allocations__c allocR;
    static List<WFM_Employee_Assignment__c> assigns;
    static WFM_Employee_Assignment__c ass1; 
    static List<WFM_EE_Work_History__c> hists;
    static Employee_Details__c  empl;
    static WFM_Project__c project;
    static WFM_Project__c projectB;
    static List<string> months;
 
    
    static void init(boolean bCurrent){
       
        country = new COUNTRY__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0);
        insert country;
        if (bCurrent)
        	months = RM_Tools.GetMonthsList(0, 6);
        else
        	months = RM_Tools.GetMonthsList(-3, 6);        
    
        empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='100', Employee_Unique_Key__c='100', First_Name__c='John', Last_Name__c='Smith',email_Address__c='JSmith@gmail.com',location_code__c='TTT', 
        Function_code__c='PR',Buf_Code__c='KCICR', Job_Class_Desc__c='A', Business_Unit__c='RDU',Business_Unit_Desc__c='Clinical Informatics', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), FTE_Equivalent__c=0.5);
        insert empl;
        
         //need to insert project
        WFM_Client__c client = new WFM_Client__c(name='TestClient', Client_Name__c='TestClinet_Name', Client_Unique_Key__c='TestClient');
        insert client;
        WFM_Contract__c contract = new WFM_Contract__c(name='TestContract', Contract_Status__c='AA', Client_ID__c=client.id, Contract_Unique_Key__c='TestContract');
        insert contract;
        Date projectEnd = Date.today().addMonths(10);
        
        project = new WFM_Project__c(name='TestProject', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProject', project_status__c ='RM',  Status_Desc__c='Active', project_end_date__c = projectEnd);
        insert project;
        projectB = new WFM_Project__c(name='TestProjectB', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProjectB', project_status__c ='BB',  Status_Desc__c='Bid');
        insert projectB;
        
        hrs = new List<WFM_Location_Hours__c>();
        for (string month:months){
            WFM_Location_Hours__c hr = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + month,YEAR_MONTH__c=month,NBR_Business_Day_In_Month__c=22);
            hrs.add(hr); 
        }
        insert hrs; 
         
        avas = new List<WFM_Employee_Availability__c>();
        
        List<WFM_employee_Allocations__c> allocs = new List<WFM_employee_Allocations__c>();
        
        Date allEndDate = Date.Today().addMonths(6);
        string allocUniq = RM_AssignmentsService.getAllocationUniqueKey(empl.name, project.name, 'CR', 'TestCountry', 'KCI', RM_Tools.getFormattedDate(RM_Tools.GetDatefromYearMonth(Rm_Tools.GetYearMonth(0)), 'mm/dd/yyyy'));
      
        alloc= new WFM_employee_Allocations__c(EMPLOYEE_ID__c=empl.id, Project_Buf_Code__c='KCI',Project_Country__c='TestCountry',  Project_ID__c=project.id, Project_Function__c='CR', 
            Status__c='Confirmed', Allocation_Unique_Key__c=allocUniq, Allocation_Start_Date__c=RM_Tools.GetDatefromYearMonth(Rm_Tools.GetYearMonth(0)), Allocation_End_Date__c  =RM_Tools.GetDatefromYearMonth(Rm_Tools.GetYearMonth(5)));
            
        allocs.add(alloc);
        string allocUniq1 = RM_AssignmentsService.getAllocationUniqueKey(empl.name, project.name, 'CY', 'TestCountry', 'KCI', RM_Tools.getFormattedDate(RM_Tools.GetDatefromYearMonth(Rm_Tools.GetYearMonth(0)), 'mm/dd/yyyy'));
        alloc1= new WFM_employee_Allocations__c(EMPLOYEE_ID__c=empl.id, Project_Buf_Code__c='KCI',Project_Country__c='TestCountry',  Project_ID__c=project.id, Project_Function__c='CY', 
            Status__c='Proposed', Allocation_Unique_Key__c=allocUniq1, Allocation_Start_Date__c=RM_Tools.GetDatefromYearMonth(Rm_Tools.GetYearMonth(0)), Allocation_End_Date__c  =RM_Tools.GetDatefromYearMonth(Rm_Tools.GetYearMonth(5)));  // same project different bufcode
        allocs.add(alloc1);
        
        string allocUniqR = RM_AssignmentsService.getAllocationUniqueKey(empl.name, project.name, 'CRA', 'TestCountry', 'KCI', RM_Tools.getFormattedDate(RM_Tools.GetDatefromYearMonth(Rm_Tools.GetYearMonth(0)), 'mm/dd/yyyy'));
        allocR = new WFM_employee_Allocations__c(EMPLOYEE_ID__c=empl.id, Project_Buf_Code__c='KCI',Project_Country__c='TestCountry',  Project_ID__c=projectB.id, Project_Function__c='CRA', 
            Status__c='Proposed', Allocation_Unique_Key__c=allocUniqR,Allocation_Start_Date__c=RM_Tools.GetDatefromYearMonth(Rm_Tools.GetYearMonth(0)), Allocation_End_Date__c  =RM_Tools.GetDatefromYearMonth(Rm_Tools.GetYearMonth(5)));
        allocs.add(allocR);
        
        insert allocs;
            
        assigns = new List<WFM_Employee_Assignment__c>();
        
        for (Integer i = 0; i < months.size(); i++){
            
            WFM_Employee_Availability__c ava = new WFM_Employee_Availability__c(EMPLOYEE_ID__c=empl.id, year_month__c=months[i], Availability_Unique_Key__c=empl.name+':'+months[i], 
            pto_unit__c='Hours', PTO_value__c=8,  location_Hours__c = hrs[i].id);
            avas.add(ava);
        }
        insert avas;
            
        for (Integer i = 0; i < months.size(); i++){    
            WFM_Employee_Assignment__c ass =  new WFM_Employee_Assignment__c(Assignment_Unique_Key__c=avas[i].id+(string)alloc.id, EMPLOYEE_ID__c = empl.id, Availability_FK__c=avas[i].id, Allocation_Key__c=alloc.id, Year_Month__c=months[i], 
            Assigned_unit__c='FTE', assigned_value__c = 0.1);
            
            assigns.add(ass);
        }
        
        insert assigns;
        
        ass1 =  new WFM_Employee_Assignment__c(Assignment_Unique_Key__c=avas[0].id+(string)alloc1.id, EMPLOYEE_ID__c = empl.id, Availability_FK__c=avas[0].id, Allocation_Key__c=alloc1.id, Year_Month__c=months[0], 
            Assigned_unit__c='Hours', assigned_value__c = 10);
        
    }
  
     
    
    @future
    private static void myFunc2()
    {  Group g1 = new Group(name = 'RM_Resource_group',DeveloperName='RM_Resource_group');        
        insert g1;
        GroupMember gm1 = new GroupMember();
        gm1.GroupId=g1.id;
        gm1.UserOrGroupId=UserInfo.getUserId();
        database.insert(gm1); 
        system.debug('----g1-----'+gm1);
        
        PRA_Business_Unit__c PBU = new PRA_Business_Unit__c(name = 'Clinical Informatics',Business_Unit_Code__c='CI');        
        insert PBU; 
        
        WFM_BU_Group_Mapping__c bug=new WFM_BU_Group_Mapping__c();
        bug.PRA_Business_Unit__c=pbu.id;
        bug.Group_Name__c='RM_Resource_group';
        insert bug;
        
    }
   
    static testMethod void TestSaveNote(){
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         Test.startTest();
         myFunc2();
              
	        init(false);
	        
	        PageReference pageRef = new PageReference('/apex/RM_manualassignment?aID='+alloc.id + '&eID='+ empl.id +'&pName=' +project.Name);
	        Test.setCurrentPage(pageRef);
	        
	        RM_ViewAssignmentsCompController.SaveNote(alloc.id, 'Test');
	       	List<WFM_employee_Allocations__c> aList = RM_AssignmentsDataAccessor.getAllocationNoteByRID(alloc.id);
	        system.assert(  aList[0].Note__c =='Test');
	        Test.stopTest();
        }  
    }
      static testMethod void TestGetActAssignments(){
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
         {
         Test.startTest();
         myFunc2();
         Test.stopTest();
        }
        init(false);
       
        PageReference pageRef = new PageReference('/apex/RM_manualassignment?aID='+alloc.id + '&eID='+ empl.id +'&pName=' +project.Name + '&flag=1');
        Test.setCurrentPage(pageRef);
  
        RM_ViewAssignmentsCompController assExt =  new RM_ViewAssignmentsCompController();
        assExt.recordID = empl.id;
        assExt.IDType = 'Resource';
        assExt.startMonth = 0; 
        assExt.endMonth = 5;
        assExt.displayType = 'Project';
        assExt.bEdit = true;
        assExt.bVisHr = true;
       assExt.bNewAssignment = true;
        string jsonStr = assExt.getActAssignment();
        
     
        system.assert(jsonStr.length() > 0);
         string jsonStrData = assExt.getDataFieldsDefination();
        string headerStr = assExt.getColumnDefination();
     
        system.assert(jsonStrData.length() > 0);
        system.assert(headerStr.length() > 0);
       
    }
      static testMethod void TestGetActAssignmentsResource(){
        
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         Test.startTest();
         myFunc2();
         Test.stopTest();
        }
        init(false);
       
        PageReference pageRef = new PageReference('/apex/RM_ViewProject?ID='+ project.id );
        Test.setCurrentPage(pageRef);
  
        RM_ViewAssignmentsCompController assExt =  new RM_ViewAssignmentsCompController();
        assExt.recordID = project.id;
        assExt.IDType = 'Project';
        assExt.startMonth = -3; 
        assExt.endMonth = 5;
        assExt.displayType = 'Resource';
        assExt.bEdit = true;
      assExt.bNewAssignment = true;
        string jsonStr = assExt.getActAssignment();
        
     
        system.assert(jsonStr.length() > 0);
       
       
    }
    
    static testMethod void TestGetActAssignmentsResource2(){
        
        Profile pr = [select id, name from Profile where name = 'System Administrator'];
        User testUser = new User(alias='TestUser',Email='testPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='PZ', 
                            LocaleSidKey='en_US', ProfileId=pr.id, TimeZoneSidKey='America/New_York', Username='Test1t.PZ@praintl.com.unitTestPMConsole');
        insert testUser;
        System.RunAs(testUser)
        {
	        init(false);
	       
	        PageReference pageRef = new PageReference('/apex/RM_manualassignment?ID='+ project.id +'&flag=1' );
	        Test.setCurrentPage(pageRef);
	  
	        RM_ViewAssignmentsCompController assExt =  new RM_ViewAssignmentsCompController();
	        assExt.recordID = project.id;
	        assExt.IDType = 'Project';
	        assExt.startMonth = -3; 
	        assExt.endMonth = 5;
	        assExt.displayType = 'Resource';
	        assExt.bEdit = true;
	       assExt.bNewAssignment = true;
	        string jsonStr = assExt.getActAssignment();
	        
	     
	        system.assert(jsonStr.length() > 0);
        }
    }
     static testMethod void TestgetDataFieldsDefination(){
        
        Profile pr = [select id, name from Profile where name = 'System Administrator'];
        User testUser = new User(alias='TestUser',Email='testPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='PZ', 
                            LocaleSidKey='en_US', ProfileId=pr.id, TimeZoneSidKey='America/New_York', Username='Test1t.PZ@praintl.com.unitTestPMConsole');
        insert testUser;
        init(false);
       
        PageReference pageRef = new PageReference('/apex/RM_manualassignment?ID='+ project.id +'&flag=1' );
        Test.setCurrentPage(pageRef);
  
        RM_ViewAssignmentsCompController assExt =  new RM_ViewAssignmentsCompController();
        assExt.recordID = project.id;
        assExt.IDType = 'Project';
        assExt.startMonth = -3; 
        assExt.endMonth = 5;
        assExt.displayType = 'Resource';
        assExt.bEdit = true;
        assExt.bNewAssignment = true;
        string jsonStrData = assExt.getDataFieldsDefination();
        string headerStr = assExt.getColumnDefination();
     
        system.assert(jsonStrData.length() > 0);
        system.assert(headerStr.length() > 0);
       
    }
   
    static testMethod void TestSaveBatchNote(){
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         	Test.startTest();
         	myFunc2();
              
	        init(false);
	        
	        PageReference pageRef = new PageReference('/apex/RM_Projectassignments?ID=' +project.id);
	        Test.setCurrentPage(pageRef);
	        string idlist = alloc.id +',' + alloc1.id;
	        RM_ViewAssignmentsCompController.saveBatchNote(idlist, 'TestNote');
	       	List<WFM_employee_Allocations__c> aList = RM_AssignmentsDataAccessor.getAllocationNoteByRID(alloc.id);
	        system.debug('---------after save note---------' + aList[0].Note__c);	
	        system.assert(  aList[0].Note__c == RM_Tools.GetStringfromDate(Date.today(), 'mm/dd/yyyy') + ' TestNote');
	        Test.stopTest();
        }  
    }
    static testMethod void TestSaveRequestID(){
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         	Test.startTest();
         	myFunc2();
              
	        init(false);
	        
	        PageReference pageRef = new PageReference('/apex/RM_Projectassignments?ID=' +project.id);
	        Test.setCurrentPage(pageRef);
	        string idlist = alloc.id +',' + alloc1.id;
	        list<string> listAID = new list<string>();
	        listAID.add(alloc.id);
	        listAID.add(alloc1.id);
	        RM_ViewAssignmentsCompController.saveRequestID(idlist, 'Req123', true);
	       	List<WFM_employee_Allocations__c> aList = RM_AssignmentsDataAccessor.getAllocationByRIDs(listAID);
	        system.debug('---------after save Request---------' + aList[0].request__c);	
	        system.assert(  aList[0].request__c != null);
	        Test.stopTest();
        }  
    }
    static testMethod void TestsaveExtAssignment(){
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         	Test.startTest();
         	myFunc2();
              
	        init(false);
	        
	        PageReference pageRef = new PageReference('/apex/RM_Projectassignments?ID=' +project.id);
	        Test.setCurrentPage(pageRef);
	        string idlist = alloc.id +',' + alloc1.id;
	        list<string> listAID = new list<string>();
	        listAID.add(alloc.id);
	        listAID.add(alloc1.id);
	        Date endDate = Date.Today().addMonths(12);
	        RM_ViewAssignmentsCompController.saveExtAssignment(idlist, RM_Tools.getFormattedDate(endDate, 'mm/dd/yyyy'));
	       	List<WFM_employee_Allocations__c> aList = RM_AssignmentsDataAccessor.getAllocationByRIDs(listAID);
	        system.debug('---------after save ext---------' + aList[0].allocation_end_date__c);	
	        system.assertEquals(  aList[0].allocation_end_date__c, endDate);
	        List<WFM_employee_Allocations__c> allList = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	        List<WFM_Employee_Assignment__c> assignsList = allList[0].Assignments__r;
	        system.debug('---------assignsList---------' + assignsList);	
	        system.assertEquals(assignsList.size(), 13);
	        
	        Test.stopTest();
        }  
    }
    static testMethod void TestsaveDelayStart(){
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         	Test.startTest();
         	myFunc2();
              
	        init(false);
	        
	        PageReference pageRef = new PageReference('/apex/RM_Projectassignments?ID=' +project.id);
	        Test.setCurrentPage(pageRef);
	        string idlist = alloc.id +',' + alloc1.id;
	        list<string> listAID = new list<string>();
	        listAID.add(alloc.id);
	        listAID.add(alloc1.id);
	        List<WFM_employee_Allocations__c> allListOld = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	        List<WFM_Employee_Assignment__c> assignsListOld = allListOld[0].Assignments__r;
	       
	        RM_ViewAssignmentsCompController.saveDelayStart(idlist, 1);
	       //	List<WFM_employee_Allocations__c> aList = RM_AssignmentsDataAccessor.getAllocationByRIDs(listAID);
	        
	        List<WFM_employee_Allocations__c> allList = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	        List<WFM_Employee_Assignment__c> assignsList = allList[0].Assignments__r;
	        system.debug('---------allList---------' + allList);	
	        system.assertEquals(assignsListOld.size()-1, assignsList.size());
	        
	        Test.stopTest();
        }  
    }
    static testMethod void TestsaveEndAssignment(){
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         	Test.startTest();
         	myFunc2();
              
	        init(false);
	        
	        PageReference pageRef = new PageReference('/apex/RM_Projectassignments?ID=' +project.id);
	        Test.setCurrentPage(pageRef);
	        string idlist = alloc.id +',' + alloc1.id;
	        list<string> listAID = new list<string>();
	        listAID.add(alloc.id);
	        listAID.add(alloc1.id);
	        List<WFM_employee_Allocations__c> allListOld = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	        List<WFM_Employee_Assignment__c> assignsListOld = allListOld[0].Assignments__r;
	       	system.debug('---------assignsListOld---------' + assignsListOld);	
	        RM_ViewAssignmentsCompController.saveEndAssignment(idlist, RM_Tools.getFormattedDate(Date.today().addMonths(4), 'mm/dd/yyyy'));
	       //	List<WFM_employee_Allocations__c> aList = RM_AssignmentsDataAccessor.getAllocationByRIDs(listAID);
	        
	        List<WFM_employee_Allocations__c> allList = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	        List<WFM_Employee_Assignment__c> assignsList = allList[0].Assignments__r;
	        system.debug('---------allList---------' + allList);	
	     //   system.assertEquals(assignsList.size(), 5);
	        system.assertEquals(allList[0].Allocation_End_Date__c, Date.today().addMonths(4));
	        
	        Test.stopTest();
        }  
    }
   static testMethod void TestsaveAdjustAllocation(){
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         	Test.startTest();
         	myFunc2();
              
	        init(false);
	        
	        PageReference pageRef = new PageReference('/apex/RM_Projectassignments?ID=' +project.id);
	        Test.setCurrentPage(pageRef);
	        string idlist = alloc.id +',' + alloc1.id;
	        list<string> listAID = new list<string>();
	        listAID.add(alloc.id);
	        listAID.add(alloc1.id);
	        List<WFM_employee_Allocations__c> allListOld = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	        List<WFM_Employee_Assignment__c> assignsListOld = allListOld[0].Assignments__r;
	       	system.debug('---------assignsListOld---------' + assignsListOld);	
	        RM_ViewAssignmentsCompController.saveAdjustAllocation(idlist, 0.2, 'FTE', RM_Tools.getFormattedDate(Date.today(), 'mm/dd/yyyy'), RM_Tools.getFormattedDate(Date.today().addMonths(4), 'mm/dd/yyyy'), false);
	       //	List<WFM_employee_Allocations__c> aList = RM_AssignmentsDataAccessor.getAllocationByRIDs(listAID);
	        
	        List<WFM_employee_Allocations__c> allList = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	        List<WFM_Employee_Assignment__c> assignsList = allList[0].Assignments__r;
	        system.debug('---------assignsList---------' + assignsList);	
	        system.assertEquals(assignsList[0].FTE__c, 0.2);
	       
	       //test outside original range
	        RM_ViewAssignmentsCompController.saveAdjustAllocation(idlist, 0.2, 'FTE', RM_Tools.getFormattedDate(Date.today(), 'mm/dd/yyyy'), RM_Tools.getFormattedDate(Date.today().addMonths(12), 'mm/dd/yyyy'), false);
	        allList = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	       	assignsList = allList[0].Assignments__r;
	        system.debug('---------assignsList2---------' + assignsList);
	        system.debug('---------allList---------' + allList);		
	        system.assertEquals(assignsList[11].FTE__c, 0.2);
	      	system.assertEquals(allList[0].Allocation_End_Date__c, Date.today().addMonths(12));
	        Test.stopTest();
        }  
    }
     static testMethod void TestsaveAdjustAllocationtoEndProject(){
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         	Test.startTest();
         	myFunc2();
              
	        init(false);
	        
	        PageReference pageRef = new PageReference('/apex/RM_Projectassignments?ID=' +project.id);
	        Test.setCurrentPage(pageRef);
	        string idlist = alloc.id +',' + alloc1.id;
	        list<string> listAID = new list<string>();
	        listAID.add(alloc.id);
	        listAID.add(alloc1.id);
	        List<WFM_employee_Allocations__c> allListOld = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	        List<WFM_Employee_Assignment__c> assignsListOld = allListOld[0].Assignments__r;
	       	system.debug('---------assignsListOld---------' + assignsListOld);	
	     /*   RM_ViewAssignmentsCompController.saveAdjustAllocation(idlist, 0.2, 'FTE', RM_Tools.getFormattedDate(Date.today(), 'mm/dd/yyyy'), RM_Tools.getFormattedDate(Date.today().addMonths(4), 'mm/dd/yyyy'), false);
	        
	        List<WFM_employee_Allocations__c> allList = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	        List<WFM_Employee_Assignment__c> assignsList = allList[0].Assignments__r;
	        system.debug('---------assignsList---------' + assignsList);	
	        system.assertEquals(assignsList[0].FTE__c, 0.2);*/
	       
	       //test outside original range
	     RM_ViewAssignmentsCompController.saveAdjustAllocation(idlist, 0.2, 'FTE', RM_Tools.getFormattedDate(Date.today(), 'mm/dd/yyyy'), RM_Tools.getFormattedDate(Date.today().addMonths(12), 'mm/dd/yyyy'), true);
	        List<WFM_employee_Allocations__c> allList = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 10);
	        
	       	List<WFM_Employee_Assignment__c> assignsList = allList[0].Assignments__r;
	        system.debug('---------assignsList2---------' + assignsList);
	        system.debug('---------allList---------' + allList);		
	        system.assertEquals(assignsList[assignsList.size() - 1].FTE__c, 0.2);
	      	system.assertEquals(allList[0].Allocation_End_Date__c, Date.today().addMonths(10));
	        Test.stopTest();
        }  
    }
     static testMethod void TestsavePushAssignment(){
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         	Test.startTest();
         	myFunc2();
              
	        init(true);
	        
	        PageReference pageRef = new PageReference('/apex/RM_Projectassignments?ID=' +project.id);
	        Test.setCurrentPage(pageRef);
	        string idlist = alloc.id +',' + alloc1.id;
	        list<string> listAID = new list<string>();
	        listAID.add(alloc.id);
	        listAID.add(alloc1.id);
	        List<WFM_employee_Allocations__c> allListOld = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	        List<WFM_Employee_Assignment__c> assignsListOld = allListOld[0].Assignments__r;
	       
	        RM_ViewAssignmentsCompController.savePushAssignment(idlist, 1);
	       //	List<WFM_employee_Allocations__c> aList = RM_AssignmentsDataAccessor.getAllocationByRIDs(listAID);
	        
	        List<WFM_employee_Allocations__c> allList = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	        List<WFM_Employee_Assignment__c> assignsList = allList[0].Assignments__r;
	        system.debug('---------allList---------' + allList);
	        system.debug('---------assignsList---------' + assignsList);	
	        system.assertEquals(assignsListOld.size(), assignsList.size());
	        
	   //     system.assertEquals(allList[0].Allocation_start_Date__c, RM_Tools.GetFirstDayDatefromDate(Date.today().addMonths(1)));
	   //     system.assertEquals(allList[0].Allocation_end_Date__c, RM_Tools.GetLastDayDatefromYearMonth(RM_Tools.GetYearMonth(7)));
	        Test.stopTest();
        }  
    }
    static testMethod void TestsaveRejectAssignment(){
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {
         	Test.startTest();
         	myFunc2();
              
	        init(false);
	        
	        PageReference pageRef = new PageReference('/apex/RM_Projectassignments?ID=' +project.id);
	        Test.setCurrentPage(pageRef);
	        string idlist = alloc.id +',' + alloc1.id;
	        list<string> listAID = new list<string>();
	        listAID.add(alloc.id);
	        listAID.add(alloc1.id);
	        List<WFM_employee_Allocations__c> allListOld = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	        List<WFM_Employee_Assignment__c> assignsListOld = allListOld[0].Assignments__r;
	       	system.debug('---------assignsListOld---------' + assignsListOld);	
	        RM_ViewAssignmentsCompController.saveRejectAssignment(idlist, 'RejectedTest', RM_Tools.getFormattedDate(Date.today().addMonths(4), 'mm/dd/yyyy'));
	       //	List<WFM_employee_Allocations__c> aList = RM_AssignmentsDataAccessor.getAllocationByRIDs(listAID);
	        
	        List<WFM_employee_Allocations__c> allList = RM_AssignmentsDataAccessor.getAssignmentByAllocations(listAID, 0, 12);
	        
	        List<WFM_Employee_Assignment__c> assignsList = allList[0].Assignments__r;
	        system.debug('---------allList---------' + allList);	
	      //  system.assertEquals(assignsList.size(), 5);
	        system.assertEquals(allList[0].Allocation_End_Date__c, Date.today().addMonths(4));
	        system.assertEquals(allList[0].status__c, 'Rejected');
	        
	        Test.stopTest();
        } 
    } 
}