/* 
Created by: Abhishek Prasad
Description: Standard Controller Extension for Revenue Rate Uploads 
*/
public class AA_CostRateUpload {
    
    public string nameFile {
        get;
        set;
    }
    public Blob contentFile {
        get;
        set;
    }
    
    public List<CostRateWrapper> result{
        get;
        set;
    }
    
    String[] filelines = new String[] {};
    List<Cost_Rate__c > updatelist;
    List<Cost_Rate__c > RatesForUpdate ;
    List<Cost_Rate__c > RatesForUpload ;
    Map<String , String> uploadentries;
    Boolean error = false;
    Boolean toupdate = false;
    Boolean update_this_record = false;
    Boolean success{
        get;
        set;
    }
    Boolean objectIdset = false;
    String id3Digit;

    
    public boolean displayUpdatePopUp{
        get;
        set;
    }
    
    public boolean backready{
        get;
        set;
    }
    
    public boolean displayConfirmationPopup {
        get;
        set;
    }
    
    public String message{
        get;
        set;
    }
    
    public AA_CostRateUpload(ApexPages.StandardSetController controller) {
    }
    
    public AA_CostRateUpload() {
    }
  
    
   
    /* Function to handle Upload Button Click */
    public Pagereference ReadFile() {
    
        /* read the csv file to generate an array of records */
        nameFile=contentFile.toString();
        filelines = nameFile.split('\n');
        RatesForUpdate =  new List<Cost_Rate__c>();
        RatesForUpload =  new List<Cost_rate__c>();
        updatelist = new List<Cost_rate__c>();
        result = new List<CostRateWrapper>();
        /* Declarations */

        
        /* Iterate over one record at a time */
        for (Integer i=1;i<filelines.size();i++) {
            update_this_record =  false;
            String[] inputvalues = new String[] {
            }
            ;
            /* get the present row */
            inputvalues = filelines[i].split(',');
            
            String bufcode;
            String year;
            String currencyvalue;
            Boolean quotes = false;
            
            /* retrieve values */
            if(inputvalues[0].contains('"')){
                   bufcode = inputvalues[0].substringBetween('"');
                   year = inputvalues[1].substringBetween('"');
                   currencyvalue = inputvalues[2].substringBetween('"');
                   quotes = true;
            } else{
                   bufcode = inputvalues[0];
                   year = inputvalues[1];
                   currencyvalue = inputvalues[2];
            }
            
            /* Validate BUF CODE */
            if(AA_CostRateService.getBufCodeIdbyName(bufcode).size() < 1 )
            {
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Error at Row '+ i +'. The BUF CODE : ' + bufcode + ' does not exists.');
                ApexPages.addMessage(errormsg);
            }
            /* Validate Year */
            if(Integer.valueOf(year) < 2000 ||  Integer.valueOf(year) > 2040){
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Error at Row '+ i +'. Invalid Year : '+ year );
                ApexPages.addMessage(errormsg);
            }
            /* Validate Currency */
            if(AA_CostRateService.getCurrencyIdbyName(currencyvalue).size() < 1){
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Error at Row '+ i +'. The Currency : ' + currencyvalue  + ' does not exists.' );
                ApexPages.addMessage(errormsg);
            }       
            List<Decimal> rates = new List<Decimal>();
            
            
            /* Validate Rates */
            for(Integer j = 0 ; j < 16 ; j ++)
            {
                if(quotes){
                    if(inputvalues[j+3].substringBetween('"').trim().length() < 1)
                    {
                        error = true;
                        ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Error at Row ' + i + '. Empty Rate for Year ' +  (Integer.valueOf(year) +  j ));
                        ApexPages.addMessage(errormsg);
                    }else{
                        try{
                            Decimal rate = Decimal.valueOf(inputvalues[j+3].substringBetween('"').trim());
                            rates.add(rate);
                        }catch(Exception e){
                            error = true;
                            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Error at Row ' + i + '. Invalid Rate for Year ' +  (Integer.valueOf(year) +  j ));
                            ApexPages.addMessage(errormsg);
                        }
                    }
                }else{
                    if(inputvalues[j+3].trim().length() < 1)
                        {
                            error = true;
                            ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Error at Row ' + i + '. Empty Rate for Year ' +  (Integer.valueOf(year) +  j ));
                            ApexPages.addMessage(errormsg);
                        }else{
                            try{
                                Decimal rate = Decimal.valueOf(inputvalues[j+3].trim());
                                rates.add(rate);
                            }catch(Exception e){
                                error = true;
                                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Error at Row ' + i + '. Invalid Rate for Year ' +  (Integer.valueOf(year) +  j ));
                                ApexPages.addMessage(errormsg);
                            }
                        }
                }
            }
            Cost_Rate__c rate;
            
            if(!error){
                /* Validate Existing Records */
                if(AA_CostRateService.getBufCodeIdbyName(bufcode).size() > 0 ){
                    if(AA_CostRateService.getCostRatebyFunction(AA_CostRateService.getBufCodeIdbyName(bufcode).get(0), year).size() > 0){
                        update_this_record = true;
                        toupdate = true;
        
                        Cost_Rate__c tempRate = new Cost_Rate__c();
                        tempRate.BUF_Code__c =  AA_CostRateService.getBufCodeIdbyName(bufcode).get(0).Id;
                        tempRate.Schedule_Year__c =  year;
                        updatelist.add(tempRate);
                        
                        
                        for(Integer k = 0 ; k < rates.size() ; k++){
                             rate = new Cost_Rate__c();
                             rate.BUF_Code__c =  tempRate.BUF_Code__c;
                             rate.Schedule_Year__c =  year;
                             rate.Currency_lookup__c=  AA_CostRateService.getCurrencyIdbyName(currencyvalue).get(0).Id;
                             rate.Year_Rate_Applies_To__c = String.valueOf(Integer.valueOf(year) + k);
                             rate.Cost_Rate__c = rates[k]; 
                            
                             rate.Id = AA_CostRateService.getCostRateId(tempRate.BUF_Code__c,year,String.valueOf(Integer.valueOf(year) + k)).get(0).Id;
                             RatesForUpdate.add(rate);               
                         }
                    }else{
                         for(Integer k = 0 ; k < rates.size() ; k++){
                             rate = new Cost_Rate__c();
                             rate.BUF_Code__c =  AA_CostRateService.getBufCodeIdbyName(bufcode).get(0).Id;
                             rate.Schedule_Year__c =  year;
                             rate.Currency_lookup__c =  AA_CostRateService.getCurrencyIdbyName(currencyvalue).get(0).Id;
                             rate.Year_Rate_Applies_To__c = String.valueOf(Integer.valueOf(year) + k);
                             rate.Cost_Rate__c = rates[k]; 
                             RatesForUpload.add(rate);                  
                         }
                    }
                }
            }
             /* Creating Output Table */
             CostRateWrapper res = new CostRateWrapper(rate,rates);
             result.add(res);  
         }
         
         if(toupdate)
             showUpdatePopup();
         else
             showConfirmationPopup();  
               
        return null;
    }
    
    class CostRateWrapper{
        public Cost_Rate__c cost{
            get;
            set;
        }
        public List<Decimal> rates{
            get;
            set;
        }
        
        CostRateWrapper(Cost_Rate__c c ,  List<Decimal> r){
            cost = c;
            rates = r;
        }
    }
    /* Get data for update popup */
    public List<Cost_Rate__c> getUpdateRecords() {
        return updatelist;
    }
    
    public void closeUpdatePopup() {
        displayUpdatePopup = false;
        result=null;
        RatesForUpload.clear();
        RatesForUpdate.clear();
    }
    public void closeConfirmationPopup() {
        RatesForUpdate.clear();
        RatesForUpload.clear();
        displayConfirmationPopup = false;
        result = null ;
    }
    
    /* Upload / Insert Records when confirmed submission */
    public void continueButton() {
  
        if(!error) {
            try {
                insert RatesForUpload ;
                if(toupdate){   
                    update RatesForUpdate ;
                }
                success = true;
                backready = true;
                toupdate = false;
                error =  false;
                RatesForUpload.clear();
                RatesForUpdate.clear();
            }
            catch (Exception e) {
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,e.getMessage());
                ApexPages.addMessage(errormsg);
            }
        }
        displayUpdatePopup = false;
        displayConfirmationPopup = false;
    }
    
    public PageReference backpressed() {
        PageReference pageRef = Page.AA_Cost_Rate;
        return pageRef ;
    }
    
    public void showUpdatePopup() {
        displayUpdatePopup = true;
    }
    public void showConfirmationPopup() {
        displayConfirmationPopup = true;
    }
}