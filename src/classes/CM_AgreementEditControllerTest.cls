/** 
     * @description Test class CM_AgreementEditControllerTest
     * @author      Karthikeyan Sounderrajan
     * @date        Created: 30-Jan-2015
     */

@isTest
public with sharing class CM_AgreementEditControllerTest {
    private static final RecordType RT_Agreement = [SELECT Id from RecordType WHERE Name = 'Contract Agreement'];
    private static final RecordType RT_Template = [SELECT Id from RecordType WHERE Name = 'Contract Template'];
    /** 
     * @description Test class testsaveAgreement on SaveAgreement
     * @author      Karthikeyan Sounderrajan
     * @date        Created: 30-Jan-2015
     */
    static testMethod void testsaveAgreement() {
        WFM_Client__c testClient = new WFM_Client__c();
        WFM_Contract__c testContract = new WFM_Contract__c();       
        WFM_Project__c testProject = new WFM_Project__c();
        WFM_Protocol__c testProtocol = new WFM_Protocol__c();
        Country__c testcountry = new Country__c();
        Protocol_Country__c testProtocolCountry = new Protocol_Country__c();
        WFM_Site_Detail__c testSite = new WFM_Site_Detail__c();
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c();
        Apttus__APTS_Agreement__c template_Protocol = new Apttus__APTS_Agreement__c();
        Apttus__APTS_Agreement__c template_Protocolcountry= new Apttus__APTS_Agreement__c();
                 
        //Test Client, Contract, Project, Protocol, and Protocol Country and agreement records
        testClient.Name = 'Client1';                
        testClient.Client_Unique_Key__c = 'Client001';
        insert testClient;
        
        testContract.Name = 'Contract1';
        testContract.Contract_Unique_Key__c = 'Contract001';
        testContract.Client_ID__c = testClient.Id;
        insert testContract;
        
        testProject.Name = 'Project1';
        testProject.Project_Unique_Key__c = 'Project001';
        insert testProject;
        
        testProtocol.Name = 'Protocol1';
        testProtocol.Protocal_Unique_Key__c = 'Protocol001';
        testProtocol.Project_ID__c = testProject.Id;
        insert testProtocol;
        
        testcountry.Name = 'ALBANIA11';
        insert testcountry;
        
        testProtocolCountry.Name='ProtocolCountry1';
        testProtocolCountry.Client_Protocol__c = testProtocol.id;
        testProtocolCountry.Region__c = testcountry.id;
        insert testProtocolCountry;
        
        testSite.Project_Protocol__c = testProtocol.Id;
        testSite.Site_ID__c = '0123456789';
        testSite.Name = 'tstSite01';
        insert testSite;
                
        agreement.RecordTypeId = RT_Agreement.Id;
        agreement.name='Test unit Agreement';
        agreement.PRA_Status__c = 'Template Sent to Site';    
        agreement.Contract_Type__c='Master CTA';     
        agreement.Site__c = testSite.Id;
        agreement.First_Draft_Sent_Date__c = Date.newInstance(2015, 01, 01);
        agreement.Planned_Final_Exec_Date__c = Date.newInstance(2015, 12, 31);
            
        insert agreement;
        
        template_Protocol.RecordTypeId = RT_Template.Id;
        template_Protocol.name='Test unit Agreement';
        template_Protocol.PRA_Status__c = 'Template Drafted';   
        template_Protocol.Contract_Type__c='Master CTA';     
        template_Protocol.Protocol__c = testProtocol.Id;
                    
        insert template_Protocol;
        
        template_Protocolcountry.RecordTypeId = RT_Template.Id;
        template_Protocolcountry.name='Test unit Agreement pc';
        template_Protocolcountry.PRA_Status__c = 'Template Drafted';   
        template_Protocolcountry.Contract_Type__c='Master CTA';     
        template_Protocolcountry.Protocol_country__c = testProtocolCountry.Id;
        
        insert template_Protocolcountry;
           
           //New CM_AgreementEditController Controller instance
        ApexPages.StandardController Cont = new ApexPages.StandardController(agreement);
        ApexPages.CurrentPage().GetParameters().put('Id',agreement.id);
        CM_AgreementEditController editcont = new CM_AgreementEditController(Cont); 
        
        ApexPages.StandardController TemplateCont = new ApexPages.StandardController(template_Protocol);
        ApexPages.CurrentPage().GetParameters().put('Id',template_Protocol.id);
        CM_AgreementEditController templateeditcont = new CM_AgreementEditController(TemplateCont); 
                
        ApexPages.StandardController pcCont = new ApexPages.StandardController(template_Protocolcountry);
        ApexPages.CurrentPage().GetParameters().put('Id',template_Protocolcountry.id);
        CM_AgreementEditController.statusc();
        CM_AgreementEditController templatepceditcont = new CM_AgreementEditController(pcCont); 
        
        /*
           Call to Method SaveAgreement in the Class: CM_AgreementforEditController
           Method is to Upsert Agreement and Name Agreement
        */
        
        test.startTest();
        editcont.saveAgreement();
        templateeditcont.saveAgreement();
        templatepceditcont.saveAgreement();
        test.stoptest();
                    
    }
}