@isTest
private class AA_BillRateControllerTest{   
    
    /* Test Year Select options */
    static testMethod void getyearSelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_BillRateController controller = new AA_BillRateController();
         List < SelectOption > selection = controller.getyearSelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
     /* Test Bill Rate Model Select options */
    static testMethod void getModelSelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_BillRateController controller = new AA_BillRateController();
         List < SelectOption > selection = controller.getModelSelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
    /* Test Job Position Select options */
    static testMethod void getJobPositionSelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_BillRateController controller = new AA_BillRateController();
         List < SelectOption > selection = controller.getJobPositionSelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
    /* Test Country Select options */
    static testMethod void getcountrySelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_BillRateController controller = new AA_BillRateController();
         List < SelectOption > selection = controller.getcountrySelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
     /* Test Is Active Select options */
    static testMethod void getisactiveSelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_BillRateController controller = new AA_BillRateController();
         List < SelectOption > selection = controller.getisactiveSelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
    /* Test Rate Detail Page redirection */
    static testMethod void goToRateDetailTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_BillRateController controller = new AA_BillRateController();
         PageReference pageRef= controller.goToRateDetail();
         
         System.assertEquals(pageRef.getUrl(),Page.AA_BillRate_InflationOverride.getUrl());
    }
    
    /* Test Submit Button */
    static testMethod void submitTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_BillRateController controller = new AA_BillRateController();
         PageReference pageRef= controller.submitFilter();
         
         System.assertEquals(null,null);
    }
    
    
    /* Test Get Year Function */
    static testMethod void getYearTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_BillRateController controller = new AA_BillRateController();
         List < Integer > years = controller.getyear();
         
         System.assertNotEquals(years.size(),0);
    }
    
    /* Test Get Result Function */
    static testMethod void getresultTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_BillRateController controller = new AA_BillRateController();
         controller.selectedYear = '2015';
         PageReference pageRef= controller.submitFilter();
         List < AA_BillRateController.BillRateWrapper > results = controller.getresult();
         
         System.assertNotEquals(results.size(),0);
    }
    
    /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }   
       
 }