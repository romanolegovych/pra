public class LMS_PersonAssignment {
    
    public Boolean selected{get;set;}
    public Date commitDate{get;set;}
    public String assignmentId{get;private set;}
    public String assignmentStatus{get;private set;}
    public String sync{get;private set;}
    public String employeeId{get;private set;}
    public String employeeCode{get;private set;}
    public String firstName{get;private set;}
    public String lastName{get;private set;}
    public String jobClass{get;private set;}
    public String jobCode{get;private set;}
    public String jobTitle{get;private set;}
    public String businessUnit{get;private set;}
    public String department{get;private set;}
    public String region{get;private set;}
    public String country{get;private set;}
    public String employeeStatus{get;private set;}
    public String processed{get;private set;}
    public String style{get;private set;}
    public String bgStyle{get;private set;}
    public String commitStyle{get;private set;}
    
    //Returns person information on a specific role
    public LMS_PersonAssignment(LMS_Role_Employee__c re, Map<String, String> jobTitleMap) {
    	selected = false;
        assignmentId = re.Id;
        assignmentStatus = re.Status__c;
        sync = re.Sync__c;
        employeeId = re.Employee_Id__r.Id;
        employeeCode = re.Employee_Id__r.Name;
        firstName = re.Employee_Id__r.First_Name__c;
        lastName = re.Employee_Id__r.Last_Name__c;
        jobClass = re.Employee_Id__r.Job_Class_Desc__c;
        jobCode = re.Employee_Id__r.Job_Code__c;
        jobTitle = jobTitleMap.get(re.Employee_Id__r.Job_Code__c);
        businessUnit = re.Employee_Id__r.Business_Unit_Desc__c;
        department = re.Employee_Id__r.Department__c;
        region = re.Employee_Id__r.Country_Name__r.Region_Name__c;
        country = re.Employee_Id__r.Country_Name__r.Name;
        employeeStatus = re.Employee_Id__r.Status_Desc__c;
        if(re.Status__c == 'Committed') {
        	processed = 'Processed';
        } else {
        	if(re.Commit_Date__c != null) {
        		commitDate = re.Commit_Date__c;
        	}
        }
        if(assignmentStatus == 'Draft') {
            style = 'draftCourse';
            bgStyle = 'draftRow';
        } else if(assignmentStatus == 'Draft Delete') {
            style = 'deleteCourse';
            bgStyle = 'draftDeleteRow';
        } else if(assignmentStatus == 'Committed') {
        	commitStyle = 'commitCourse';
        }
    }
}