/**
@author Ramya
@date 2015
@description this controller class is to display the Different Grids
**/

public with sharing class PBB_ClientUnitGridController {
    
    // to hide/show the main tabs in the component.
    public List<String> HiddenHomeTabsList{ get;set; }
    
    //to hide/show the subtabs in the component.
    public List<String> HiddenSubTabsList{ get;set; }
    
    //PBB Scenario object ID
    public String PBBScenarioID{ get; set; }  
    public String ScenarioID{ get; set; }
    
    // PAGE_MODE,PAGE_MODE_VIEW,PAGE_MODE_EDIT variable is to display the type of mode in Visual force page   
    public String PAGE_MODE{get;private set;}
    public String PAGE_MODE_VIEW {get{return 'VIEW';}} 
    
    //type of model used either global or Client/PRA region model 
    public String Budgettype{get;set;}
    
    //display the Client unit grid by ReportBy with different options.  
    public String Reportby{get;set;}
     
    //PBB Scenario object       
    public PBB_Scenario__c PBBScenario{ get; set; } 
    public List<Scenario_Budget_Task__c> ClientgridList { get; set; } 
    public List<AggregateResult> FuncCoutrygridList { get; set; } 
    
    //Map is used for Client/PRA region related grids.
    public Map<String,List<PBB_BudgetEngineService.ClientUnitGridCVO>> MapRegionClientUnitGridGroupList1 {get;set;}   
    public Map<String,PBB_BudgetEngineService.ClientUnitGridCVO> MapRegionClientUnitGridGroupList {get;set;}
    public integer MapRegionSize{ get { return MapRegionClientUnitGridGroupList.size( ); } }   
    
    //display the Client unit grid /budget grid by showpanel and modified tasks.  
    public String Showpanel{get;set;} 
    List<Countries_Service_Tasks__c > modifiedCSTslist;
    public integer modifiedCSTsSize{ get { return modifiedCSTslist.size( ); } }   
     
    //list is used for global related grids.
    public List<PBB_BudgetEngineService.ClientUnitGridCVO> GlobalClientUnitGridGroupList {get;set;}
    
    /**
    * @author Ramya
    * @date 2015
    * @description Constructor.
    */
    public PBB_ClientUnitGridController() {
        HiddenHomeTabsList = new List<String>();
        HiddenSubTabsList = new List<String>();
        
        HiddenSubTabsList.add('RDpage');
        HiddenSubTabsList.add('Ppage'); 
        
        PBBScenarioID = ApexPages.currentPage().getParameters().get('PBBScenarioID');
        ScenarioID = ApexPages.currentPage().getParameters().get('PBBScenarioID');
        if(PBBScenarioID!=null && PBBScenarioID!='') {
            PBBScenario = PBB_DataAccessor.getScenarioByID(PBBScenarioID);
        }
        modifiedCSTslist = [Select id, Name, Is_Modified__c,Service_Task__r.Name, PBB_Scenario_Country__r.country__r.Name
                                From Countries_Service_Tasks__c 
                                where PBB_Scenario_Country__r.PBB_Scenario__c =: PBBScenarioID 
                                and Is_Modified__c=true];
        Showpanel = 'BudgetGrid';
    }
    
    /**
    * @author Ramya
    * @date 2015
    * @description This method is make a picklist of all the region models for a client and PRA regions .
    * @return returns all client and PRA region models . 
    */
    public List<selectOption> BudgettypeList=new List<selectOption>();       
    public List<selectOption> getBudgettypeList(){
        BudgettypeList.clear();
        BudgettypeList.add(new selectOption('PRA','PRA'));
        return BudgettypeList;           
    }
    
    /**
    * @author Ramya
    * @date 2015 
    * @description This method is make a picklist of all the options that a different report types to display for region models for a client and PRA regions .    
    * @return  List<selectOption> different reporting options fo a selected Global or client and PRA region models 
    */
    public List<selectOption> ReportByList=new List<selectOption>();
    public List<selectOption> getReportByList(){
        ReportByList.clear();
        ReportByList.add(new selectOption('Unit Grid','Unit Grid'));     
        ReportByList.add(new selectOption('Functional-Blended','Functional-Blended'));   
        ReportByList.add(new selectOption('Functional-Country Specific','Functional-Country Specific'));         
        return ReportByList;
    }
    
    /**
    * @author Ramya
    * @date 2015 
    * @description This method is used to Create Budget.
    * @return creates budget using scenario Id.
    */
    public void CreateBudget() {        
        if(PBBScenarioID!=null && PBBScenarioID!='') {
            try {
                PBB_BudgetEngineService.createScenarioBudget((ID)PBBScenarioID);
            }
            catch (PRA_BaseService.PRAServiceException ex) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
                for (String errStr : ex.getErrorList()) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errStr));
                }
            }
        }
    }
    
    /**
    * @author Ramya
    * @date 2015 
    * @description This method is used to generate different client unit grids.
    * @return different reporting grids
    */
    public void GenerateReport() {        
        if(PBBScenarioID!=null && PBBScenarioID!='') {
            Showpanel = 'UnitGrid';
            ClientgridList = new List<Scenario_Budget_Task__c>();
            MapRegionClientUnitGridGroupList1 = new Map<String,List<PBB_BudgetEngineService.ClientUnitGridCVO>>();
            MapRegionClientUnitGridGroupList = new Map<String,PBB_BudgetEngineService.ClientUnitGridCVO>();
            GlobalClientUnitGridGroupList = new List<PBB_BudgetEngineService.ClientUnitGridCVO>();  
            
            //Generate report if picklist value is client unit
            if(Reportby == 'Unit Grid'){
                ClientgridList = PBB_BudgetDataAccessor.getclientunitgriddata((ID)PBBScenarioID);
                system.debug('@@@@ClientgridList'+ClientgridList);
            }            
            //Generate report if picklist value is Functional-Blended
            if(Reportby == 'Functional-Blended'){
                MapRegionClientUnitGridGroupList = PBB_BudgetEngineService.getClientgridbyRole((ID)PBBScenarioID);
                system.debug('@@@@MapRegionClientUnitGridGroupList'+MapRegionClientUnitGridGroupList );    
                
            }            
            //Generate report if picklist value is Functional-Country Specific
            if(Reportby == 'Functional-Country Specific'){
                MapRegionClientUnitGridGroupList = PBB_BudgetEngineService.getClientGridByRolectry((ID)PBBScenarioID);
                system.debug('@@@@MapRegionClientUnitGridGroupList'+MapRegionClientUnitGridGroupList );
            }
        }
    }
}