/** Implements the test for the Service Layer of the object FinancialCatTotPrice__c
 * @author	Dimitrios Sgourdos
 * @version	17-Dec-2013
 */
@isTest
private class FinancialCatTotPriceServiceTest {
	
	/** Test the function mapFinancialCatTotPriceListOnServiceCategory
	 * @author	Dimitrios Sgourdos
	 * @version	17-Dec-2013
	 */
	static testMethod void mapFinancialCatTotPriceListOnServiceCategoryTest() {
		// Create data
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001',
																				'BDT Project First Experiments');
		insert currentProject;
	
		Study__c study = new Study__c();
		study.Project__c = currentProject.Id;
		insert study;
		
		FinancialDocument__c financialDocument = new FinancialDocument__c();
		financialDocument.DocumentType__c	= 'Proposal';
		financialDocument.Client_Project__c	= currentProject.Id; 
		insert financialDocument;
		
		List<ServiceCategory__c> srvCatList = new List<ServiceCategory__c>();
		srvCatList.add(new ServiceCategory__c(Code__c='001', Name='Service Category 1'));
		srvCatList.add(new ServiceCategory__c(Code__c='002', Name='Service Category 2'));
		insert srvCatList;
		
		List<FinancialCategoryTotal__c> fctList = new List<FinancialCategoryTotal__c>();
		fctList.add(new FinancialCategoryTotal__c(FinancialDocument__c = financialDocument.Id,
		 										ServiceCategory__c = srvCatList[0].Id,
		 										Study__c = study.Id));
		fctList.add(new FinancialCategoryTotal__c(FinancialDocument__c = financialDocument.Id,
		 										ServiceCategory__c = srvCatList[1].Id,
		 										Study__c = study.Id));
		insert fctList;
		
		List<FinancialCatTotPrice__c> sourceList = new List<FinancialCatTotPrice__c>();
		sourceList.add(new FinancialCatTotPrice__c(FinancialCategoryTotal__c = fctList[0].Id, Name='Test A1'));
		sourceList.add(new FinancialCatTotPrice__c(FinancialCategoryTotal__c = fctList[0].Id, Name='Test A2'));
		sourceList.add(new FinancialCatTotPrice__c(FinancialCategoryTotal__c = fctList[1].Id, Name='Test B'));
		insert sourceList;
		
		sourceList = FinancialCatTotPriceDataAccessor.getFinancialCatTotalPricesByDocumentId (financialDocument.Id,
																							true,
																							'Name');
		// Check the function
		Map<String, List<FinancialCatTotPrice__c>> results =
								FinancialCatTotPriceService.mapFinancialCatTotPriceListOnServiceCategory(sourceList);
		
		String errorMessage = 'Error in mapping Financial Category Total Prices on Service Categories';
		system.assertEquals(results.size(), 2, errorMessage);
		system.assertEquals(results.containsKey(srvCatList[0].Id), true, errorMessage);
		system.assertEquals(results.containsKey(srvCatList[1].Id), true, errorMessage);
		
		List<FinancialCatTotPrice__c> valuesList = new List<FinancialCatTotPrice__c>();
		valuesList = results.get(srvCatList[0].Id);
		system.assertEquals(valuesList.size(), 2, errorMessage);
		system.assertEquals(valuesList[0].Id, sourceList[0].Id, errorMessage);
		system.assertEquals(valuesList[1].Id, sourceList[1].Id, errorMessage);
		
		valuesList = new List<FinancialCatTotPrice__c>();
		valuesList = results.get(srvCatList[1].Id);
		system.assertEquals(valuesList.size(), 1, errorMessage);
		system.assertEquals(valuesList[0].Id, sourceList[2].Id, errorMessage);
	}
}