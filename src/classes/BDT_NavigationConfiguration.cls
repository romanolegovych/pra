public class BDT_NavigationConfiguration {
	
	public class searchWrapper {
		public navigationItemWrapper navigationItem;
		public String Category;
		public string MainCategory;
	}
	
	public List<navigationMainCategoryWrapper> navigation {get;set;}
	public Map<String,searchWrapper> searchMap;
	
	// searchMap.get('PageName').navigationItem --> item.iconActive
	// searchMap.get('PageName').parentMap.getKeys()[0] -- category (name)
	// searchMap.get('PageName').parentMap.getValues()[0] -- main category (name)

	public class navigationMainCategoryWrapper {
		public list<navigationCategoryWrapper> NavigationCategoryList {get;set;}
		public string Name{get;set;}
		public string IconActive{get;set;}
		public string IconInactive{get;set;}
		public string ClassActive{get;set;} 		
		public string ClassInactive{get;set;} 	
		
		public navigationMainCategoryWrapper() {
			NavigationCategoryList 	= new list<navigationCategoryWrapper>();
			ClassActive 			= 'BDT_MainNavigationCategoryActive';
			ClassInactive			= 'BDT_MainNavigationCategoryInactive';
		}
		
	}
	public class navigationCategoryWrapper {
		public list<navigationItemWrapper> NavigationItemList {get;set;}
		public string Name{get;set;}
		public string ClassActive{get;set;} 		
		public string ClassInactive{get;set;} 	
		
		public navigationCategoryWrapper() {
			NavigationItemList	= new list<navigationItemWrapper>();
			ClassActive 		= 'BDT_NavigationCategoryActive';
			ClassInactive 		= 'BDT_NavigationCategoryInactive';
		}
		
	}
	public class navigationItemWrapper {
		public string Name{get;set;}
		public string MainPage{get;set;}
		public string ActiveForPages{get;set;}
		public string ImageActive {get;set;}
		public string ImageInactive {get;set;}
		public string ClassActive{get;set;} 		
		public string ClassInactive{get;set;} 	
		
		public navigationItemWrapper() {
			ClassActive 	= 'BDT_NavigationItemActive';
			ClassInactive 	= 'BDT_NavigationItemInactive';
		}
		
	}
	
	
	
	public void addToSearchMap(navigationItemWrapper NavigationItem , string categoryName, string mainCategoryName ) {
		
		searchWrapper sW;
		
		for (String s: NavigationItem.ActiveForPages.split(' ')) {
			sW = new searchWrapper();
			
			sW.navigationItem = NavigationItem;
			sw.Category = categoryName;
			sw.MainCategory = mainCategoryName;
			searchMap.put(s.toLowerCase(s),sW);
		}
		
	}
	public String retrieveItemName( String pagename ){
		if (searchMap.containsKey(pagename.toLowercase( pagename ))) {
			return searchMap.get(pagename.toLowercase( pagename )).navigationItem.Name;
		} else {return null;}
	}
	
	public navigationItemWrapper retrieveNavItem( String pagename ){
		if (searchMap.containsKey(pagename.toLowercase( pagename ))) {
			return searchMap.get(pagename.toLowercase( pagename )).navigationItem;
		} else {return null;}
	}
	
	public String retrieveCategoryName(string pagename){
		if (searchMap.containsKey(pagename.toLowercase( pagename ))) {
			return searchMap.get(pagename.toLowercase( pagename )).Category;
		} else {return null;}
	}
	
	public string retrieveMainCategoryName(string pagename){
		if (searchMap.containsKey(pagename.toLowercase( pagename ))) {
			return searchMap.get(pagename.toLowercase( pagename )).MainCategory;
		} else {return null;}
	}
	
	
	public BDT_NavigationConfiguration () {
		
		searchMap = new Map<String,searchWrapper>();
				
		navigation = new List<navigationMainCategoryWrapper>();
		
		navigationMainCategoryWrapper navigationMainCatItem;
		navigationCategoryWrapper     navigationCatItem;
		navigationItemWrapper		  navigationItem;
		
		// Singleproject
		navigationMainCatItem = new navigationMainCategoryWrapper();
		navigationMainCatItem.Name = 'Single Project';
		navigationMainCatItem.IconActive = 'Single_project_hover.png';
		navigationMainCatItem.IconInactive = 'Single_project.png';

		
			// Specification
			navigationCatItem = new navigationCategoryWrapper();
			navigationCatItem.Name = 'Specifications';

						
				// General Info
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'General Info';
				navigationItem.MainPage = 'BDT_SingleProject';
				navigationItem.ActiveForPages = 'BDT_SingleProject bdt_neweditstudy';
				navigationItem.ImageActive = '/img/42x42/Specifications_Active.png';
				navigationItem.ImageInactive = '/img/42x42/Specifications_NotActive.png';
				navigationCatItem.NavigationItemList.add(navigationItem);
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
				// Milestones
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Milestones';
				navigationItem.MainPage = 'bdt_studymilestones';
				navigationItem.ActiveForPages = 'bdt_studymilestones';
				navigationItem.ImageActive = '/img/42x42/milestones_hover.png';
				navigationItem.ImageInactive = '/img/42x42/milestones.png';
				navigationCatItem.NavigationItemList.add(navigationItem);	
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );	
		
				// Characteristics
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Characteristics';
				navigationItem.MainPage = 'bdt_characteristics';
				navigationItem.ActiveForPages = 'bdt_characteristics bdt_neweditpopulation bdt_neweditstudysite bdt_neweditstudysubcontractoractivity BDT_NewEditLabAnalysisToStudy';
				navigationItem.ImageActive = '/img/42x42/characteristics_hover.png';
				navigationItem.ImageInactive = '/img/42x42/characteristics.png';
				navigationCatItem.NavigationItemList.add(navigationItem);
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
				// Design
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Designs';
				navigationItem.MainPage = 'bdt_design';
				navigationItem.ActiveForPages = 'bdt_designcopy bdt_design bdt_neweditdesign bdt_neweditpopulationassignment bdt_neweditdesigncopy bdt_neweditgrouparmassignment';
				navigationItem.ImageActive = '/img/42x42/design_hover.png';
				navigationItem.ImageInactive = '/img/42x42/design.png';
				navigationCatItem.NavigationItemList.add(navigationItem);
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );

				// Flowchart
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Flowcharts';
				navigationItem.MainPage = 'bdt_neweditflowchart';
				navigationItem.ActiveForPages = 'bdt_neweditflowchart bdt_neweditflowchartcopy';
				navigationItem.ImageActive = '/img/42x42/charts_hover.png';
				navigationItem.ImageInactive = '/img/42x42/charts.png';
				navigationCatItem.NavigationItemList.add(navigationItem);
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
				// Service
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Services';
				navigationItem.MainPage = 'bdt_projectservices';
				navigationItem.ActiveForPages = 'bdt_projectservices bdt_projectservicecopy';
				navigationItem.ImageActive = '/img/42x42/services_hover.png';
				navigationItem.ImageInactive = '/img/42x42/services.png';
				navigationCatItem.NavigationItemList.add(navigationItem);
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );				
				
				// Pass-through
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Pass-through';
				navigationItem.MainPage = 'bdt_projectpassthrough';
				navigationItem.ActiveForPages = 'bdt_projectpassthrough';
				navigationItem.ImageActive = '/img/42x42/pass-through_hover.png';
				navigationItem.ImageInactive = '/img/42x42/pass-through.png';
				navigationCatItem.NavigationItemList.add(navigationItem);	
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
				
			//After Last Item add NavigationCategory to NavigationMainCategory!!
			navigationMainCatItem.NavigationCategoryList.add(navigationCatItem);
			
			// Financial
			navigationCatItem = new navigationCategoryWrapper();
			navigationCatItem.Name = 'Financial';
			
				// Pricing 
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Unit Prices';
				navigationItem.MainPage = 'BDT_FinancialServicePricing';
				navigationItem.ActiveForPages = 'BDT_FinancialServicePricing';
				navigationItem.ImageActive = '/img/42x42/coin_stack_hover.png';
				navigationItem.ImageInactive = '/img/42x42/coin_stack.png';
				navigationCatItem.NavigationItemList.add(navigationItem);
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
				// Proposal
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Financial Docs';
				navigationItem.MainPage = 'BDT_FinancialDocOverview'; 
				navigationItem.ActiveForPages = 'BDT_FinancialDocOverview BDT_FinancialDocHistory BDT_FinancialDocStudySel BDT_FinancialDocPricing BDT_FinancialDocPayment BDT_FinancialDocContent BDT_FinancialDocApproval BDT_FinancialDocOutput';
				navigationItem.ImageActive = '/img/42x42/proposal-template_hover.png'; 
				navigationItem.ImageInactive = '/img/42x42/proposal-template.png';
				navigationCatItem.NavigationItemList.add(navigationItem);
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
			//After Last Item add NavigationCategory to NavigationMainCategory!!
			navigationMainCatItem.NavigationCategoryList.add(navigationCatItem);

			// Review
			navigationCatItem = new navigationCategoryWrapper();
			navigationCatItem.Name = 'Review';
			
				// Project Versioning
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Project Versioning';
				navigationItem.MainPage = 'BDT_ProjectVersioning';
				navigationItem.ActiveForPages = 'BDT_ProjectVersioning';
				navigationItem.ImageActive = '/img/42x42/Specifications_Active.png';
				navigationItem.ImageInactive = '/img/42x42/Specifications_NotActive.png';
				navigationCatItem.NavigationItemList.add(navigationItem);
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
			//After Last Item add NavigationCategory to NavigationMainCategory!!
			navigationMainCatItem.NavigationCategoryList.add(navigationCatItem);
		
		//After Last Catergor add the list to the main Category
		navigation.add(navigationMainCatItem);
		
		
		// Administration
		navigationMainCatItem = new navigationMainCategoryWrapper();
		navigationMainCatItem.Name = 'Administration';
		navigationMainCatItem.IconActive = '/img/16X16/Rate_Cards_2.png';
		navigationMainCatItem.IconInactive = '/img/16X16/Cog.png';
		
			//// Sources
			navigationCatItem = new navigationCategoryWrapper();
			navigationCatItem.Name = 'Sources';
			
				// Sites
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Sites';
				navigationItem.MainPage = 'BDT_Sites';
				navigationItem.ActiveForPages = 'BDT_Sites BDT_NewEditBusinessUnit bdt_neweditsite';
				navigationItem.ImageActive = '/img/42x42/sites_hover.png';
				navigationItem.ImageInactive = '/img/42x42/sites.png';
				navigationCatItem.NavigationItemList.add(navigationItem);
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
				// Sponsors
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Sponsors';
				navigationItem.MainPage = 'BDT_Sponsors';
				navigationItem.ActiveForPages = 'BDT_Sponsors BDT_NewEditSponsor bdt_neweditsponsorentity';
				navigationItem.ImageActive = '/img/42x42/sponsors_hover.PNG';
				navigationItem.ImageInactive = '/img/42x42/sponsors.PNG';
				navigationCatItem.NavigationItemList.add(navigationItem);		
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
				// Subcontractors
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Subcontractors';
				navigationItem.MainPage = 'BDT_Subcontractors';
				navigationItem.ActiveForPages = 'BDT_Subcontractors BDT_NewEditSubcontractor BDT_NewEditSubcontractorActivity';
				navigationItem.ImageActive = '/img/42x42/subcontractors_hover.png';
				navigationItem.ImageInactive = '/img/42x42/subcontractors.png';
				navigationCatItem.NavigationItemList.add(navigationItem);							
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
				// Services
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Services';
				navigationItem.MainPage = 'BDT_Services';
				navigationItem.ActiveForPages = 'BDT_Services bdt_neweditservicecategory bdt_neweditservice';
				navigationItem.ImageActive = '/img/42x42/services-maintenence_hover.png';
				navigationItem.ImageInactive = '/img/42x42/services-maintenance.png';
				navigationCatItem.NavigationItemList.add(navigationItem);		
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
				// Pass-through
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Pass-through';
				navigationItem.MainPage = 'BDT_PassThroughServices';
				navigationItem.ActiveForPages = 'BDT_PassThroughServices BDT_NewEditPassThroughService';
				navigationItem.ImageActive = '/img/42x42/pass-through_hover.png';
				navigationItem.ImageInactive = '/img/42x42/pass-through.png';
				navigationCatItem.NavigationItemList.add(navigationItem);	
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
				// MileStones
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Milestones';
				navigationItem.MainPage = 'BDT_MileStoneDefinition';
				navigationItem.ActiveForPages = 'BDT_MileStoneDefinition';
				navigationItem.ImageActive = '/img/42x42/milestones_hover.png';
				navigationItem.ImageInactive = '/img/42x42/milestones.png';
				navigationCatItem.NavigationItemList.add(navigationItem);	
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
			//After Last Item add NavigationCategory to NavigationMainCategory!!
			navigationMainCatItem.NavigationCategoryList.add(navigationCatItem);
				
			// Financial
			navigationCatItem = new navigationCategoryWrapper();
			navigationCatItem.Name = 'Financial';
				
				// Rate Cards
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Rate Cards';
				navigationItem.MainPage = 'BDT_RateCards';
				navigationItem.ActiveForPages = 'BDT_RateCards BDT_NewEditRateCard';
				navigationItem.ImageActive = '/img/42x42/rate-cards_hover.png';
				navigationItem.ImageInactive = '/img/42x42/rate-cards.png';
				navigationCatItem.NavigationItemList.add(navigationItem);									
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
				// Approval Rules
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Approval Rules';
				navigationItem.MainPage = 'bdt_approvalrules';
				navigationItem.ActiveForPages = 'bdt_approvalrules';
				navigationItem.ImageActive = '/img/42x42/approval_hover.png';
				navigationItem.ImageInactive = '/img/42x42/approval.png';
				navigationCatItem.NavigationItemList.add(navigationItem);	
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
			//After Last Item add NavigationCategory to NavigationMainCategory!!
			navigationMainCatItem.NavigationCategoryList.add(navigationCatItem);
			
			// Lab
			navigationCatItem = new navigationCategoryWrapper();
			navigationCatItem.Name = 'Lab';
				
				// Methods
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Methods';
				navigationItem.MainPage = 'BDT_LabMethods'; 
				navigationItem.ActiveForPages = 'BDT_LabMethods'; 
				navigationItem.ImageActive = '/img/42x42/methods_hover.png'; 
				navigationItem.ImageInactive = '/img/42x42/methods.png';
				navigationCatItem.NavigationItemList.add(navigationItem);
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
				// Lab Parameters
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Parameters';
				navigationItem.MainPage = 'BDT_LaboratoryLists'; 
				navigationItem.ActiveForPages = 'BDT_LaboratoryLists';
				navigationItem.ImageActive = '/img/42x42/techniques_hover.png'; 
				navigationItem.ImageInactive = '/img/42x42/techniques.png';
				navigationCatItem.NavigationItemList.add(navigationItem);
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
			//After Last Item add NavigationCategory to NavigationMainCategory!!
			navigationMainCatItem.NavigationCategoryList.add(navigationCatItem);

			// Securities
			navigationCatItem = new navigationCategoryWrapper();
			navigationCatItem.Name = 'Securities';
				
				// Methods
				navigationItem = new navigationItemWrapper();
				navigationItem.Name = 'Securities';
				navigationItem.MainPage = 'BDT_Securities'; 
				navigationItem.ActiveForPages = 'BDT_Securities'; 
				navigationItem.ImageActive = '/img/42x42/methods_hover.png'; 
				navigationItem.ImageInactive = '/img/42x42/methods.png';
				navigationCatItem.NavigationItemList.add(navigationItem);
				addToSearchMap(navigationItem ,navigationCatItem.Name, navigationMainCatItem.Name );
				
			//After Last Item add NavigationCategory to NavigationMainCategory!!
			navigationMainCatItem.NavigationCategoryList.add(navigationCatItem);

				
		//After Last Catergor add the list to the main Category
		navigation.add(navigationMainCatItem);		
								
				
	}
}