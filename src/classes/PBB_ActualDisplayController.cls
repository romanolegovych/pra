/**
@author Ramya
@date 2015
@description this controller class is to display the Actuals 
**/

global with sharing class PBB_ActualDisplayController{    
    public List<pbb_weekly_events__c> pscswe{get;set;}
    public ID bidscenario{get;set;}
    public String scenarioCountry{get;set;}
    public Boolean isEdit{get;set;}
        
    //constructor
    public  PBB_ActualDisplayController(ApexPages.StandardController controller){
        isEdit = false;
        if(pscswe == null)
            pscswe = new List<pbb_weekly_events__c>();
    }
    
    //method to get scenariolist
    public List<selectOption> getscenariolist(){
    
        List<selectOption> scenariolist = new List<selectOption>();
        
        //to add blank value as default
        scenariolist.add(new SelectOption('','  ')); 
        
        for(PBB_Scenario__c ps: PBB_DataAccessor.getPbbScenarioOrderByAsc()) {
            scenariolist.add(new selectOption(ps.Id,ps.name));
              system.debug('@@@@scenariolist'+scenariolist);
        } 
        return scenariolist;
    }
    
    //method to get country list
    public List<selectOption> getcountrylist(){
    
        List<SelectOption> cntrylist = new List<SelectOption>();
        cntrylist.add(new SelectOption('',' '));
        system.debug('@@@@Selected Scenario'+bidscenario);
        for(SRM_Scenario_Country__c psc: PBB_DataAccessor.GetPBBWeeklyCountriesbyasc(bidscenario)){
             String IRBtype=psc.IRB_Type__c==null?'':psc.IRB_Type__c;                            
             cntrylist.add(new selectOption(psc.Id,psc.PBB_Scenario_Country__r.Country__r.Name+'//'+IRBtype));  
        }
        return cntrylist;
    }
    
    //method to refresh data
    public pageReference refreshData(){
        pscswe = PBB_DataAccessor.Getweeklyevents(scenarioCountry );
        system.debug('Weekly Data'+pscswe);
        return null;
    }
    
    public void reprojectEnrollment(){
       /* system.debug('Scenario Country'+scenarioCountry);
        Set<String> cntryset = new Set<String>();
        cntryset.add(scenarioCountry);
        List<SRM_Scenario_Country__c> pbbsc = PBB_DataAccessor.getSRMScenarioScountries(cntryset);
        system.debug('PBB Scenario '+pbbsc[0]);
        PBB_Services service = new PBB_Services();
        service.reProjectWeeklyEvents(pbbsc[0]);*/
    }
    
    //method to edit actuals data
    public pageReference editrecord(){
        isEdit = true;
        return null;
    }
    
    //remote action for displaying actuals
    @RemoteAction
    global static string editrecord(String ids,String actualSub,String actualEs) {
        system.debug('@@@@@'+actualSub+actualEs);
    
        if(!String.isEmpty(actualSub)){
            system.debug('@@@@@'+actualSub);
            if(!actualSub.isNumeric())
                return 'value must be numreric';
                pbb_weekly_events__c pbbEvent = new pbb_weekly_events__c(id=ids,Actual_Subjects__c = Decimal.valueOf(actualSub));
            update pbbEvent;     
        }
        
        if(!String.isEmpty(actualEs)){
            system.debug('@@@@@'+actualSub+actualEs);     
            if(!actualEs.isNumeric())
                return 'value must be numreric';
                pbb_weekly_events__c pbbEvent = new pbb_weekly_events__c(id=ids, Expected_Subjects__c= Decimal.valueOf(actualEs));
                update pbbEvent;     
        }       
    return 'Successfully Saved!';
      
    } 
}