public with sharing class AA_CostRateDataAccessor{

    /* Get all buf codes */
    public static List<BUF_Code__c> getBufCodes(){
         return [SELECT  Id,Name FROM BUF_Code__c order by Name ];
    }
    
    public static List<BUF_Code__c> getBufCodesByName(String searchTerm){
         return Database.query('Select Id, Name from BUF_Code__c where name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\'');
    }
    
    /* get buf code by id */
    public static BUF_Code__c getBufCode(String val){
         return [SELECT Name FROM BUF_Code__c where Id=:val];
    }
    
    /* get all currencies */
    public static List<Currency__c> getCurrencies(){
         return [SELECT  Id,Name FROM Currency__c WHERE NAME='USD' or Name='EUR' order by Name];
    }
    
    /* insert cost rate record */
    public static void insertRecord(Cost_Rate__c record){
        insert record;
    }
    
    /* check for existing values */
    public static List<Cost_Rate__c> checkExisting(String code , String year){
        return [SELECT Id,Name,Year_Rate_Applies_To__c FROM Cost_Rate__c where BUF_Code__c=:code AND Schedule_Year__c=: year];
    }
    
    /* get buf code by name */
    public static List<BUF_Code__c> getBufCodeIdbyName(String name){
        return [SELECT Id FROM BUF_Code__c WHERE Name=:name];
    }
    
    /* get currency by id */
    public static List<Currency__c> getCurrencyIdbyName(String name){
        return [SELECT Id FROM Currency__c WHERE Name=:name];
    }
    
    /* get buf code id by bu and f*/
    public static List<BUF_Code__c> getBufCodeId(Business_Unit__c bu, Function_Code__c f){
        return [SELECT Id,Name from BUF_Code__c where Business_Unit__r.Id =: bu.id and Function_Code__r.Id =: f.id];
    }

    /* get cost rate value by function and year */
    public static List<Cost_Rate__c> getCostRatebyFunction(BUF_Code__c bu, String year){
        return [SELECT Id,BUF_Code__c,Cost_Rate__c,Currency__c,Currency_lookup__c,IsActive__c,Schedule_Year__c,Year_Rate_Applies_To__c from Cost_Rate__c where BUF_Code__c =: bu.id AND Schedule_Year__c=: year ORDER BY Year_Rate_Applies_To__c ];
    }
    
    /* get cost rate id by schedule and year rate applies to */
    public static List<Cost_Rate__c> getCostRateId(String bu, String year1, String year2){
        return [SELECT Id from Cost_Rate__c where BUF_Code__c =: bu AND Schedule_Year__c=: year1 AND Year_Rate_Applies_To__c=: year2 ];
    }
    
    public static List<AggregateResult> getFunctionListByFilter(String year,String bucode,String functioncode,String country,String active){
    
        String query = String.format('SELECT BUF_Code__c from Cost_Rate__c WHERE Schedule_Year__c = \'\'{0}\'\' ', new String[]{year});    
         if(bucode !=null)
             query = query + String.format(' AND Cost_Rate__c.Buf_Code__r.Business_Unit__c=\'\'{0}\'\' ', new String[]{bucode});  
         if(functioncode !=null)
             query = query + String.format(' AND Cost_Rate__c.Buf_Code__r.Function_Code__c=\'\'{0}\'\' ', new String[]{functioncode});  
         if(country !=null)
             query = query + String.format(' AND Cost_Rate__c.Buf_Code__r.Business_Unit__r.Default_Country__c=\'\'{0}\'\' ', new String[]{country});
         if(active !='A'){
             if(active == 'Y')
                 query = query + ' AND Cost_Rate__c.IsActive__c= true ';
             else
                 query = query + ' AND Cost_Rate__c.IsActive__c = false '; 
         }
         query = query + ' GROUP BY BUF_Code__c,Buf_Code__r.Name, Schedule_Year__c  ORDER BY Buf_Code__r.Name, Schedule_Year__c';
         return Database.query(query);
    }
      
    public static List<Cost_Rate__c> getFunctionDetails(String Id , String Year){
        String query = String.format('SELECT Cost_Rate__c.BUF_Code__r.Name,BUF_Code__c,Cost_Rate__c,Currency_lookup__c,Currency__c,IsActive__c,Schedule_Year__c,Year_Rate_Applies_To__c from Cost_Rate__c WHERE BUF_Code__c = \'\'{0}\'\' AND Schedule_Year__c = \'\'{1}\'\'', new String[]{Id,Year});         
        query = query + ' ORDER BY Year_Rate_Applies_To__c ';
        return Database.query(query);
    }
    /* run agrregate queries */
    public static List<AggregateResult> runAggregateQuery(String query){
       return  Database.query(query);
    }
    
    /* run string query */
    public static List<Cost_Rate__c> runQuery(String query){
       return  Database.query(query);
    }
    
    /* get all cost rates by buf code and scheduled year */
    public static List<Cost_Rate__c> getCostRates(String Id , String scheduledYear){
       return [SELECT Id,BUF_Code__r.Name,Year_Rate_Applies_To__c,Schedule_Year__c,Cost_Rate__c,Currency_lookup__c,Currency_lookup__r.Name,CreatedBy.Name,CreatedDate,LastModifiedBy.Name,LastModifiedDate,IsActive__c FROM Cost_Rate__c WHERE Buf_Code__c=: Id AND Schedule_Year__c =: scheduledYear AND IsActive__c = true  ORDER BY Year_Rate_Applies_To__c];
    }
}