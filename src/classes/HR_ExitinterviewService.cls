/**
*   'HR_ExitinterviewService' class is to access the bussiness logic involved for HR exit interview.
*   @author   Devaram Bhargav
*   @version  26-Sep-2013
*   @since    17-Sep-2013
*/
public with sharing class HR_ExitinterviewService { 
    /**
    *   This Method converts the sObject list into a sObject Map with the predefined attributes value (field,lstObject).   
    *   @param    field  is the variable to which acts as a key in the Map.
    *   @param    lstObject  is the list variable to which acts as a value in the Map.
    *   @return   The Map of sObject type with the predefined attributes value (field,lstObject).
    */  
    public static Map<string, sObject> getMapfromListObject(String field, List<sObject> lstObject){
        Map<String, sObject> aMap = new Map<String, sObject>();
        for(sObject aObject : lstObject){
            String f = String.valueOf(aObject.get(field));              
            aMap.put(f, aObject);               
        }
        return aMap;
   } 
   
   /**
   *   This Method isEmpty is to find out if the string is empty with predefined attributes value (s).   
   *   @param    s is the variable of string.  
   *   @return   true if the s is empty or false if not.
   */
   public static Boolean isEmpty(String s) {
       if (s == null)
           return true;
       else if (s.length() == 0)
           return true;
       return false;
   }
   
   /**
   *   This Method isStringArrayEmpty is to find out if the Array string is empty with predefined attributes value (keys).   
   *   @param    keys is the variable of Array string.  
   *   @return   true if the array is empty or false if not.
   */
   public static Boolean isStringArrayEmpty(String[] keys) {
       boolean b=false;
       if(keys == null || keys.size() == 0){
           b= true;
           return b;
       }
       //system.debug('--------keys------------'+keys);
       for (String key: keys){
           if(Isempty(key)){
               b= true; 
               return b;           
           }
       }
       return b;
   }
   
   /**
   *   This Method retrieve the  HR_Exit_Interview_Questionnaire__c data with the predefined attributes value (EXIid).   
   *   @param    EXIid   This is the record ID of the form like HR Exit Interview object.
   *   @return   The HR_Exit_Interview_Questionnaire__c data for the EXIid.
   */ 
   public static List<HR_Exit_Interview_Questionnaire__c> getHRQuestionnaireDatabyEXIid(ID EXIid) {
       List<HR_Exit_Interview_Questionnaire__c> HREIQ = new List<HR_Exit_Interview_Questionnaire__c>();
       HREIQ =HR_DataAccessor.getEXIQuestionnaireListbyEXIid(EXIid);
       //system.debug('-----HREIQ -------' + HREIQ.size()); 
       return HREIQ ;
   }
    
    /**
    *   This Method converts the HR_QuestionnaireCVO Map into the List of List with the predefined attributes value (HRQCVOmap).   
    *   @param    HRQCVOmap   This is the Map of HR_QuestionnaireCVO.
    *   @return   The List of HR_QuestionnaireCVOList data for the HRQCVOmap Map.
    */ 
    public static List<List<HR_QuestionnaireCVO>> getListofListfromMapHRQuestionnaireCVO(Map<Decimal,List<HR_QuestionnaireCVO>> HRQCVOmap){
        List<List<HR_QuestionnaireCVO>> HRQListofList = new List<List<HR_QuestionnaireCVO>>();
        //Sort the lsit by Map keys
        List<Decimal> keySetList =  new List<Decimal>() ; 
        keySetList.addAll(HRQCVOmap.keySet());
        keySetList.sort();
        
        //Loop over the sorted list of Map keyset.
        For (Decimal s : keySetList ){        
            List<HR_QuestionnaireCVO> hrqcvolist = new List<HR_QuestionnaireCVO>();
            hrqcvolist=HRQCVOmap.get(s); 
            HRQListofList.add(hrqcvolist);
        }
        return HRQListofList;
    }      
    
    /**
    *   This Method reurns the HR_QuestionnaireCVO the List of List with the predefined attributes value (EXIid).   
    *   @param    EXIid   This is the ID of HR Exit Interview record.
    *   @return   The List of HR_QuestionnaireCVOList List.
    */
    public static List<List<HR_QuestionnaireCVO>> getHRQuestionnaire(ID EXIid) {    
        //create a map with Question ID's and HR_Exit_Interview_Questionnaire__c record        
        Map<String,sObject> HREIQmap=new Map<String,sObject>();        
        //HREIQmap= getMapfromListObject('Question_ID__c',HR_DataAccessor.getEXIQuestionnaireListbyEXIid(EXIid));
        
        //This part of is to foartmatted the response values from different records of same question.
        List<HR_Exit_Interview_Questionnaire__c> HREQlist=new List<HR_Exit_Interview_Questionnaire__c>();
        HREQlist.addall(HR_DataAccessor.getEXIQuestionnaireListbyEXIid(EXIid));
        Map<string,string> tempmap=new Map<string,string>();
        for(integer i=0;i<HREQlist.size();i++){
            if(HREQlist[i].Response__c!=null && HREQlist[i].Response__c!='' && (!tempmap.containsKey(HREQlist[i].Question_ID__c))){
               tempmap.put(HREQlist[i].Question_ID__c,HREQlist[i].Response__c);
            }  
             else if(HREQlist[i].Response__c!=null && HREQlist[i].Response__c!='' && tempmap.containsKey(HREQlist[i].Question_ID__c)){
                  string respstr=tempmap.get(HREQlist[i].Question_ID__c)+','+HREQlist[i].Response__c;
                  tempmap.put(HREQlist[i].Question_ID__c,respstr);
             }          
        
        }
        for(HR_Exit_Interview_Questionnaire__c hrqnr:HREQlist){
            if(tempmap.containsKey(hrqnr.Question_ID__c)){
               hrqnr.Response__c=tempmap.get(hrqnr.Question_ID__c);
            }
        }
        HREIQmap= getMapfromListObject('Question_ID__c',HREQlist);
        
        //Make a map with sections and thiers relative HR_QuestionnaireCVO lists
        Map<Decimal,List<HR_QuestionnaireCVO>> HRQCVOmap=new Map<Decimal,List<HR_QuestionnaireCVO>>();        
         
        /**
        *   Loop it over the list of questionaire objects data for the exit interview.   
        *   Bind the questions with the HR Questionnaire data into the HR_QuestionnaireCVO's HRQCVOmap Map.
        *   Check the logic if the HR Questionnaire record exit retriev the record otherwise create a new record.
        */
        for(Questionnaire__c q:HR_DataAccessor.getQuestionnaireListbyFormtype('Exit Interview')){
            //Instanciate the HR_QuestionnaireCVO record.
            HR_QuestionnaireCVO hrqcvo=new HR_QuestionnaireCVO();           
            hrqcvo.questionnaire=q;//Assign the question record to the instance hrqcvo.
            
            //verifies the HR Questionnaire record exists by verifying in the map                       
            if(HREIQmap.containsKey(q.name)) {
                hrqcvo.EXIquestionnaire=(HR_Exit_Interview_Questionnaire__c)HREIQmap.get(q.name); 
                system.debug('-------hrqcvo*****----------'+hrqcvo);                                                        
                hrqcvo.getResponsesintoPicklists();      //bind the question choices into the picklit.
                system.debug('-------hrqcvo----------'+hrqcvo);                                          
            }else{
                //if the HR Questionnaire record not exits then create a new.            
                HR_Exit_Interview_Questionnaire__c heiq=new HR_Exit_Interview_Questionnaire__c ();
                heiq.Question_ID__c=q.name;
                heiq.Questions__c=q.Question_Text__c; 
                heiq.Question_Order__c=q.Question_Order__c;
                hrqcvo.EXIquestionnaire=heiq;               
            }
            hrqcvo.EXIquestionnaire.HR_Exit_Interview__c= EXIid; //assign the parent key value          
            system.debug('--------hrqcvo------------'+hrqcvo); 
            //Making the Map with section Order as a key value.
            if(!HRQCVOmap.containsKey(q.Section_Order__c)) {
                HRQCVOmap.put(q.Section_Order__c, new List<HR_QuestionnaireCVO>());                       
            }              
            HRQCVOmap.get(q.Section_Order__c).add(hrqcvo);
        }
        //system.debug('--------HRQCVOmap------------'+HRQCVOmap);
        
        //Make a List of List from the section Map.        
        List<List<HR_QuestionnaireCVO>> ListofHRQCVOList=new List<List<HR_QuestionnaireCVO>>();        
        ListofHRQCVOList=getListofListfromMapHRQuestionnaireCVO(HRQCVOmap);//converts the Map to List of List
        system.debug('--------ListofHRQCVOList------------'+ListofHRQCVOList);
                
        return  (ListofHRQCVOList); 
    }
    
    /**
    *   This Method reurns the Error message after saving the HR_Exit_Interview_Questionnaire__c list with the predefined attributes value (ListofHRQCVOList).   
    *   @param    ListofHRQCVOList   This is List of HRQCVOList List.
    *   @return   The ErrorMsg string.
    */
    public static String saveHRQuestionnairefromlistofHRQCVOlist(List<List<HR_QuestionnaireCVO>> ListofHRQCVOList) {        
        /** ErrorMsg variable is to hold if we have any error messages while saving the list*/
        String ErrorMsg;
                
        //make a List of HR_Exit_Interview_Questionnaire__c  
        List<HR_Exit_Interview_Questionnaire__c> hrqlist = new List<HR_Exit_Interview_Questionnaire__c >();
        
        /**
        *   Loop the List of List to get the HR_Exit_Interview_Questionnaire__c list out of the ListofHRQCVOList  .   
        *   Bind the responses from the PicklistResponse and MultiPicklistResponse to Response__c of HR_Exit_Interview_Questionnaire__c.
        *   Verify all the required questions are answered, if not assign a error message to ErrorMsg variable.
        */
           
        List<HR_Exit_Interview_Questionnaire__c> TempList=new List<HR_Exit_Interview_Questionnaire__c>(); 
        Map<string,HR_Exit_Interview_Questionnaire__c> hrmap=new Map<string,HR_Exit_Interview_Questionnaire__c>();        
        Set<string> questionid=new Set<string>();
        Set<string> responses=new Set<string>();                
        integer setsize;
        Set<Id> recordid=new Set<Id>();
       //HR_Exit_Interview_Questionnaire__c Tempqtnr=new HR_Exit_Interview_Questionnaire__c();       
       For (integer i=0; i< ListofHRQCVOList.size();i++) {
            system.debug('--------ListofHRQCVOList------------'+ListofHRQCVOList.get(i));
            //Loop through the HR_QuestionnaireCVO           
            for(HR_QuestionnaireCVO hq:ListofHRQCVOList.get(i)){
                system.debug('--------hq------------'+hq);
                
                if(hq.EXIquestionnaire.Comments_notes__c!=null && hq.EXIquestionnaire.Comments_notes__c!=''){
                       if(hq.EXIquestionnaire.Comments_notes__c.length()>2999){                       
                            ErrorMsg='Comments cannot exceed 3000 characters';
                            break;
                        }
                    }
                //Binding the responses back to respons field.
                if(hq.questionnaire.Question_Type__c=='Picklist'){
                    hq.EXIquestionnaire.Response__c=hq.PicklistResponse;
                    //Validating Comments length
                  
                    //Checking for the required fields.
                    if(hq.questionnaire.required__c==true && hq.PicklistResponse==null){                       
                        ErrorMsg='Missing required values';
                        break;
                    }    
                }else if(hq.questionnaire.Question_Type__c=='Multi-picklist'){                                      
                                //Checking for the required fields and whether the array string is empty or not.
                                if(hq.questionnaire.required__c==true && isStringArrayEmpty(hq.MultiPicklistResponse)==true){
                                    ErrorMsg='Missing required values';
                                    break;
                                }   
                                
                        recordid.add(hq.EXIquestionnaire.HR_Exit_Interview__c);  
                        questionid.add(hq.EXIquestionnaire.Question_ID__c);  
                                //Binding the responses back from Muti-picklist to a string.                    
                                String pickvalue; 
                                integer x=hq.MultiPicklistResponse.size();
                                
                                for(integer j=0;j<hq.MultiPicklistResponse.size();j++){                         
                                    //pickvalue=s+';'+pickvalue; 
                                    //responses.add(hq.MultiPicklistResponse[j]);                     
                                                                                 
                                    //hrmap.put(hq.MultiPicklistResponse[j],hq.EXIquestionnaire);
                                    //hq.EXIquestionnaire.Response__c=pickvalue.replace(';null', '');                       
                                    //hq.EXIquestionnaire.Response__c=hq.MultiPicklistResponse[0];
                                    if(hq.MultiPicklistResponse[j]!=null && hq.MultiPicklistResponse[j]!=''){
                                        HR_Exit_Interview_Questionnaire__c Tempqtnr=new HR_Exit_Interview_Questionnaire__c();
                                        Tempqtnr.Comments_notes__c=hq.EXIquestionnaire.Comments_notes__c;
                                        Tempqtnr.HR_Exit_Interview__c=hq.EXIquestionnaire.HR_Exit_Interview__c;
                                        Tempqtnr.Question_ID__c=hq.EXIquestionnaire.Question_ID__c;
                                        Tempqtnr.Questions__c=hq.EXIquestionnaire.Questions__c;                        
                                        Tempqtnr.Response__c=hq.MultiPicklistResponse[j]; 
                                        Tempqtnr.Question_Order__c=hq.EXIquestionnaire.Question_Order__c; 
                                        TempList.add(Tempqtnr); 
                                    } 
                                                   
                                } 
                                if(x<1){
                                     TempList.clear();
                                }
                       }
                      system.debug('--------hq.EXIquestionnaire------------'+hq.EXIquestionnaire); 
                      
                if(hq.questionnaire.Question_Type__c!='Multi-picklist')
                    hrqlist.add(hq.EXIquestionnaire);               
                
            }
        } 
        Set<ID> existingids=new Set<ID>();
        for(integer s=0;s<hrqlist.size();s++){
           // existingids.add(hrqlist[s].id);
        }       
        List<HR_Exit_Interview_Questionnaire__c> listtodelt=new List<HR_Exit_Interview_Questionnaire__c>();
        listtodelt=[select id from HR_Exit_Interview_Questionnaire__c where Question_ID__c IN:questionid and HR_Exit_Interview__c in:recordid];
        if(listtodelt.size()>0){
           delete listtodelt;
        }
        
        if(TempList.size()>0){
        INSERT TempList;
        TempList.clear();
        } 
        
        // Performing the upsert after collecting the list and has no errors. 
        
        if((ErrorMsg==null) || (ErrorMsg=='')){            
            ErrorMsg=HR_DataAccessor.upsertHRQuestionnairebyList(hrqlist); //getting the error if any at the time of upsert operation.          
        }
        system.debug('------ErrorMsg-----------'+ErrorMsg);        
        return ErrorMsg;
    }
    
    /**
    *   This Method reurns Error message after deleteing the HR_Exit_Interview_Questionnaire__c list with predefined attributes value (EXIid).   
    *   @param    EXIid   This is the ID of HR Exit Interview record.
    *   @return   The ErrorMsg string.
    */
    public static String DeleteHRQuestionnairebyEXIid(ID EXIid) {
        String ErrorMsg;
        List<HR_Exit_Interview_Questionnaire__c> hrqlist = HR_DataAccessor.getEXIQuestionnaireListbyEXIid(EXIid);
        if((ErrorMsg==null) || (ErrorMsg=='')){            
            ErrorMsg=HR_DataAccessor.DeleteHRQuestionnairebyList(hrqlist );   //getting the error if any at the time of delete operation.                  
        }
        return ErrorMsg;
    }
}