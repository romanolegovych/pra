public with sharing class BDT_NewEditSite {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Administration Sites
	public Site__c site {get;set;}
	public String siteId {get;set;}
	public String buId {get;set;}
	
	public BDT_NewEditSite(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Sites');
		buId = System.currentPageReference().getParameters().get('buId');
		if (buId == null || buId == '') {  			
  			buId = System.currentPageReference().getParameters().get('customId');
  		}
  		
   		siteId = System.currentPageReference().getParameters().get('siteId');
  		site = new Site__c();			
		if(siteId!=null && siteId!=''){
			try{
				site = [Select s.Name, s.Id, s.City__c, s.Business_Unit__c,s.Business_Unit__r.name, s.Address_Line_1__c, s.Abbreviation__c, ZipCode__c, Country__c 
						From Site__c s 
						where s.id = :siteId];
			}catch(QueryException e){
				
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Site Id could not be found. ');
        		ApexPages.addMessage(msg); 
			}
		}else{
			if(buId!=null && buId!=''){				
				site = new Site__c(Business_Unit__c = buId);
			}
		}
	
	}
	
	public PageReference save(){
		
		try{
			upsert site;			
		}catch(DMLException e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to save this Site, please contact your Administrator. ');
        	ApexPages.addMessage(msg); 
		}
		
		return returnToBDTSites();
	}
	
	public PageReference cancel(){
		
		return returnToBDTSites();
	}
	
	public PageReference deleteSoft(){
		
		site.BDTDeleted__c = true;
				
		try{
			update site; 
		}catch(DMLException e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to delete this Site, please contact your Administrator. ');
        	ApexPages.addMessage(msg); 
		}
			
		return returnToBDTSites();
	}
	
	public PageReference returnToBDTSites(){		
		PageReference sitePage = new PageReference(System.Page.BDT_Sites.getUrl());
		sitePage.getParameters().put('buId', buId);
		sitePage.setRedirect(true);			
		return sitePage;		
	}

}