@istest private class PAWS_FlowInstanceCursorTriggerTest
{
	private static List<STSWR1__Flow_Instance_Cursor__c> prepare()
	{
		return new List<STSWR1__Flow_Instance_Cursor__c>{
			(STSWR1__Flow_Instance_Cursor__c) System.Json.deserialize('{"STSWR1__Step_Assigned_To__c": "' + UserInfo.getUserId() + '", "STSWR1__Step_Assign_Type__c": "User"}', STSWR1__Flow_Instance_Cursor__c.class)
		};
	}
	
	@istest private static void testAssignedToName()
	{
		PAWS_FlowInstanceCursorTrigger testee = new PAWS_FlowInstanceCursorTrigger();
		
		List<STSWR1__Flow_Instance_Cursor__c> records = prepare();
		
		testee.beforeInsert(records);
		testee.beforeUpdate(records, records);
		testee.afterInsert(records);
		testee.afterUpdate(records, records);
		
		System.assertEquals(UserInfo.getFirstname() + ' ' + UserInfo.getLastname(), records.get(0).Step_Assigned_To_Name__c);
	}
	
	@istest private static void testOnError()
	{
		PAWS_FlowInstanceCursorTrigger testee = new PAWS_FlowInstanceCursorTrigger();
		
		List<STSWR1__Flow_Instance_Cursor__c> records = prepare();
		testee.onError(records, 'Test!');
	}
}