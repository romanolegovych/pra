/**
 * @description	Implements the test for the functions of the NCM_Chatter_API class
 * @author		Dimitrios Sgourdos
 * @version		Created: 09-Sep-2015
 */
@isTest
private class NCM_Chatter_APITest {
	
	/**
	 * @description	The test is only for the code coverage, as the API contains only calls to methods of the
	 *				Service Layers
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sep-2015
	*/
	static testMethod void myUnitTest() {
		// Create dummy data
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		// Call the functions
		NCM_Chatter_API.postFeedItem(acc.Id, 'Test post');
		NCM_Chatter_API.postFeedItem(new List<NCM_Chatter_API_DataTypes.ChatterPostWrapper>());
		FeedItem tmpFeedItem = [SELECT Id FROM FeedItem LIMIT 1];
		NCM_Chatter_API.postComment(tmpFeedItem.Id, 'Test Comment');
		NCM_Chatter_API.postComment(new List<NCM_Chatter_API_DataTypes.ChatterPostWrapper>());
		NCM_Chatter_API.isUserSubscribedToFeed(acc.Id, testUser.Id);
		NCM_Chatter_API.getFeedItems(acc.Id);
		NCM_Chatter_API.getFeedItems(acc.Id, 'TestTopic');
		NCM_Chatter_API.unsubscribeUserFromFeed(acc.Id, testUser.Id);
		NCM_Chatter_API.unsubscribeUserFromFeed(new List<NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper>());
		NCM_Chatter_API.subscribeUserToFeed(acc.Id, testUser.Id);
		NCM_Chatter_API.subscribeUserToFeed(new List<NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper>());
	}
}