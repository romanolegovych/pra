/**
@author Ramya
@date 1/14/2015
@description this controller class got all conversions in general
**/

public with sharing class PBB_Utils{
    
    /**
* @author Ramya
* @Date 1/14/2012
* @Description Method to show error Message
*
*/
    //Method to show error Message
    
    public static void showErrorMessage(String errorMessage){
        if(ApexPages.CurrentPage() != null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
        }
        else{
            System.debug('>>> '+errorMessage+' >>>');
        }
    }
    
    /**
* @author Ramya
* @Date 1/14/2012
* @Description Method to show message
*
*/
    //Method to show message
    
    public static void showMessage(String message){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, message));
    }
    
    /**
* @author Niharika
* @Date 4/2/2015
* @Description Method to generate date from String date in the format of dd-Mmm-yyyy
*
*/
    
    public static Date getDate(String dateInStringFormat){
        Date newDate = null;
        Integer monthInteger;
        if(!String.isEmpty(dateInStringFormat)){
            
            System.debug('>>> dateInStringFormat>>> '+dateInStringFormat);
            
            Integer monthInd = dateInStringFormat.indexOf('-');
            System.debug('monthInd : '+monthInd );
            
            Integer len = dateInStringFormat.length();
            System.debug('len : '+len );
            
            Integer day = Integer.valueOf(dateInStringFormat.subString(0, monthInd));
            System.debug('day : '+ day );
            
            String month = String.valueOf(dateInStringFormat.subString(monthInd+1, dateInStringFormat.lastIndexOf('-')));
            System.debug('month : '+ month );
            
            
           if(month.toLowerCase() == 'jan' ){
                month='Jan';
                monthInteger = 1;
            }else if(month.toLowerCase() == 'feb' ){
                month='Feb';
                monthInteger = 2;
            }else if(month.toLowerCase() == 'mar' ){
                month='Mar';
                monthInteger = 3;
            }else if(month.toLowerCase() == 'apr' ){
                month='Apr';
                monthInteger = 4;
            }else if(month.toLowerCase() == 'may' ){
                month='May';
                monthInteger = 5;
            }else if(month.toLowerCase() == 'jun' ){
                month='Jun';
                monthInteger = 6;
            }else if(month.toLowerCase() == 'jul' ){
                month='Jul';
                monthInteger = 7;
            }else if(month.toLowerCase() == 'aug' ){
                month='Aug';
                monthInteger = 8;
            }else if(month.toLowerCase() == 'sep' ){
                month='Sep';
                monthInteger = 9;
            }else if(month.toLowerCase() == 'oct' ){
                month='Oct';
                monthInteger = 10;
            }else if(month.toLowerCase() == 'nov' ){
                month='Nov';
                monthInteger = 11;
            }else if(month.toLowerCase() == 'dec' ){
                month='Dec';
                monthInteger = 12;
            }
            
            Integer year = Integer.valueOf(dateInStringFormat.subString(dateInStringFormat.lastIndexOf('-')+1, len));
            System.debug('year : '+year );
            
            newDate = Date.newInstance(year,monthInteger,day);
        }
        return newDate;
    }
    
    /**
* @author Niharika
* @Date 4/2/2015
* @Description Method to generate date from String date in the format of dd-Mmm-yyy
*
*/
    public static String getDateInStringFormat(Date dateToConvert){
        String convertedDate = '';
        String datelabel = '';
        if(dateToConvert != null){
            Integer year = dateToConvert.year();
            Integer month = dateToConvert.month();
            Integer day = dateToConvert.day();
            
            datelabel = DateTime.newInstance(year, month, day).format('dd-MMM-yyyy');
        }
        return datelabel;
    }
}