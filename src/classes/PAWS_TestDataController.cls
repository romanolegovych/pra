public without sharing class PAWS_TestDataController
{
    public Integer ExistsPawsProjectCount { get; set; }
    public Integer ExistsPawsProjectFlowCount { get; set; }
    public Integer ExistsPawsCountryFlowCount { get; set; }
    public Integer ExistsPawsSiteFlowCount { get; set; }

    public Integer ProjectCount { get; set; }
    public Integer ProjectProccessedCount { get; set; }
    public Integer ProjectCountryCount { get; set; }
    public Integer ProjectSiteCount { get; set; }
    public Integer pawsProjectLastIdx { get; set; }

    private PAWS_TestData testDataService = new PAWS_TestData();
    
    private Integer wfmClientLastIdx;
    private Integer wfmContractLastIdx;
    private Integer wfmProjectLastIdx;
    private Integer pawsProjectCountryLastIdx;
    private Integer pawsProjectSiteLastIdx;
    private Integer pawsProjectFlowLastIsx;
    private Integer pawsProjectCountryFlowLastIsx;
    private Integer pawsProjectSiteFlowLastIsx;

    private Integer ProjectCountPerBatch { get; private set;}

    public PAWS_TestDataController()
    {
        wfmClientLastIdx = 0;
        wfmContractLastIdx = 0;
        wfmProjectLastIdx = 0;
        pawsProjectLastIdx = 0;
        pawsProjectCountryLastIdx = 0;
        pawsProjectSiteLastIdx = 0;
        pawsProjectFlowLastIsx = 0;
        pawsProjectCountryFlowLastIsx = 0;
        pawsProjectSiteFlowLastIsx = 0;
        ProjectProccessedCount = 0;
        ProjectCountPerBatch = 2;
    }

    public void init()
    {
        ExistsPawsProjectCount = [select count() from ecrf__c where Name like '[#STD#]%' limit 1];
        countFlows();    

        wfmClientLastIdx = ExistsPawsProjectCount;
        wfmContractLastIdx = ExistsPawsProjectCount;
        wfmProjectLastIdx = ExistsPawsProjectCount;
        pawsProjectLastIdx = ExistsPawsProjectCount;

        pawsProjectCountryLastIdx = [select count() from PAWS_Project_Flow_Country__c where Name like '[#STD#]%' limit 1];
        pawsProjectSiteLastIdx = [select count() from PAWS_Project_Flow_Site__c where Name like '[#STD#]%' limit 1];

        pawsProjectFlowLastIsx = [select count() from STSWR1__Flow__c where Name like '[#STD#] Project Flow%' limit 1];
        pawsProjectCountryFlowLastIsx = [select count() from STSWR1__Flow__c where Name like '[#STD#] Country Flow%' limit 1];
        pawsProjectSiteFlowLastIsx = [select count() from STSWR1__Flow__c where Name like '[#STD#] Site Flow%' limit 1];
    }

    private void countFlows()
    {
        ExistsPawsProjectFlowCount = [select count() from STSWR1__Flow__c where Name like '[#STD#] Project Flow%' limit 1];
        ExistsPawsCountryFlowCount = [select count() from STSWR1__Flow__c where Name like '[#STD#] Country Flow%' limit 1];
        ExistsPawsSiteFlowCount = [select count() from STSWR1__Flow__c where Name like '[#STD#] Site Flow%' limit 1];
    }

    public void createTestData()
    {
        Savepoint sp = Database.setSavepoint();

        try
        {
            Integer projectCountToProccess = (ProjectCount - ProjectProccessedCount) >= ProjectCountPerBatch ? ProjectCountPerBatch :  ProjectCount - ProjectProccessedCount;

            List<WFM_Client__c> wfmClientList = new List<WFM_Client__c>();
            wfmClientLastIdx = testDataService.createWFMClient(projectCountToProccess, wfmClientLastIdx, wfmClientList);

            List<WFM_Contract__c> wfmContractList = new List<WFM_Contract__c>();
            wfmContractLastIdx = testDataService.createWFMContractForClients(wfmClientList, 1, wfmContractLastIdx, wfmContractList);

            List<WFM_Project__c> wfmProjectList = new List<WFM_Project__c>();
            wfmProjectLastIdx = testDataService.createWFMProjectsForContract(wfmContractList, 1, wfmProjectLastIdx, wfmProjectList);

            //-------
            List<TestDataWrapper> pawsProjectList = new List<TestDataWrapper>();
            pawsProjectLastIdx = testDataService.createPAWSProjects(wfmProjectList, projectCountToProccess, pawsProjectLastIdx, pawsProjectList);

            List<TestDataWrapper> pawsProjectCountryList = new List<TestDataWrapper>();
            pawsProjectCountryLastIdx = testDataService.createPAWSProjectCountries(pawsProjectList, ProjectCountryCount, pawsProjectCountryLastIdx, pawsProjectCountryList);

            List<TestDataWrapper> pawsProjectSiteList = new List<TestDataWrapper>();
            pawsProjectSiteLastIdx = testDataService.createPAWSProjectSites(pawsProjectCountryList, ProjectSiteCount, pawsProjectSiteLastIdx, pawsProjectSiteList);

            pawsProjectFlowLastIsx = testDataService.createFlows(pawsProjectList, pawsProjectFlowLastIsx, 'One Time', '[#STD#] Project Flow');
            pawsProjectCountryFlowLastIsx = testDataService.createFlows(pawsProjectCountryList, pawsProjectCountryFlowLastIsx, 'Triggered by another flow', '[#STD#] Country Flow');
            pawsProjectSiteFlowLastIsx = testDataService.createFlows(pawsProjectSiteList, pawsProjectSiteFlowLastIsx, 'Triggered by another flow', '[#STD#] Site Flow');

            List<STSWR1__Flow_Step_Junction__c> stepList = testDataService.buildSteps(pawsProjectList);
            insert stepList;

            List<STSWR1__Flow_Step_Action__c> actionList = testDataService.buildActions(pawsProjectList);
            insert actionList;

            List<STSWR1__Flow_Instance__c> flowInstanceList = testDataService.buildFlowInstaces(pawsProjectList);
            insert flowInstanceList;

            List<STSWR1__Flow_Instance_Cursor__c> flowInstanceCursorList = testDataService.buildFlowInstaceCursors(pawsProjectList);
            insert flowInstanceCursorList;

            ExistsPawsProjectCount += pawsProjectList.size();
            ExistsPawsProjectFlowCount += pawsProjectList.size();
            ExistsPawsCountryFlowCount += pawsProjectCountryList.size();
            ExistsPawsSiteFlowCount += pawsProjectSiteList.size();
            ProjectProccessedCount += projectCountToProccess;
        }
        catch(Exception ex)
        {
            Database.rollback(sp);
            ApexPages.addMessages(ex);
        }
    }

    public void deleteTestData()
    {
        Savepoint sp = Database.setSavepoint();
        try
        {
            List<STSWR1__Flow__c> flowList = [select ID from STSWR1__Flow__c where Name like '[#STD#]%' order by ID limit 1000];
            delete flowList;

            List<ecrf__c> pawsProjectList = [select ID from ecrf__c where Name like '[#STD#]%' order by ID limit 1000];
            delete pawsProjectList;

            List<WFM_Project__c> wfmProjectList = [select ID from WFM_Project__c where Name like '[#STD#]%' order by ID limit 1000];
            delete wfmProjectList;

            List<WFM_Contract__c> wfmContractList = [select ID from WFM_Contract__c where Name like '[#STD#]%' order by ID limit 1000];
            delete wfmContractList;

            List<WFM_Client__c> wfmClientList = [select ID from WFM_Client__c where Name like '[#STD#]%' order by ID limit 1000];
            delete wfmClientList;

            ExistsPawsProjectCount -= pawsProjectList.size();
            countFlows();
        }
        catch(Exception ex)
        {
            Database.rollback(sp);
            ApexPages.addMessages(ex);
        }
    }

    public class PAWS_TestData
    {
        public Integer createWFMClient(Integer recordCount, Integer startIndex, List<WFM_Client__c> outClientList)
        {
            Integer lastIdx = startIndex;
            for(Integer idx = 0; idx < recordCount; idx++)
            {
                String name = '[#STD#] Name ' + lastIdx;
                WFM_Client__c wfmClient = new WFM_Client__c(Name=name, Client_Name__c=name.replace(' ', '_'),Client_Unique_Key__c='[#STD#]_key_' + lastIdx);
                outClientList.add(wfmClient);
                lastIdx++;
            }

            insert outClientList;

            return lastIdx;
        }

        public Integer createWFMContractForClients(List<WFM_Client__c> clientList, Integer recordCount, Integer startIndex, List<WFM_Contract__c> outContractList)
        {
            Integer lastIdx = startIndex;
            for(Integer idx = 0; idx < clientList.size(); idx++)
            {
                WFM_Contract__c wfmContract = new WFM_Contract__c(Name='[#STD#] WFM Contrat ' + lastIdx, Client_ID__c=clientList.get(idx).ID, Contract_Status__c='AA', Contract_Unique_Key__c='[#STD#]_key_' + lastIdx);
                outContractList.add(wfmContract);
                lastIdx++;
            }

            insert outContractList;

            return lastIdx;
        }

        public Integer createWFMProjectsForContract(List<WFM_Contract__c> contractListList, Integer recordCount, Integer startIndex, List<WFM_Project__c> outProjectList)
        {
            Integer lastIdx = startIndex;
            for(Integer idx = 0; idx < contractListList.size(); idx++)
            {
                String name = '[#STD#] WFM Project ' + lastIdx;
                WFM_Project__c wfmProject = new WFM_Project__c(Name=name, Project_Unique_Key__c='[#STD#]_key_' + lastIdx, Contract_ID__c=contractListList.get(idx).ID, Project_Status__c ='RM', Status_Desc__c='Active');
                outProjectList.add(wfmProject);
                lastIdx++;
            }

            insert outProjectList;

            return lastIdx;
        }

        public Integer createPAWSProjects(List<WFM_Project__c> wfmProjectList, Integer recordCount, Integer startIndex, List<ecrf__c> outPAWSProject)
        {
            Integer lastIdx = startIndex;
            for(Integer idx = 0; idx < wfmProjectList.size(); idx++)
            {
                ecrf__c pawsProject = new ecrf__c(Name='[#STD#] PAWS Project ' + lastIdx, Project_Id__c=wfmProjectList.get(idx).ID, Type__c='Regular');
                outPAWSProject.add(pawsProject);
                lastIdx++;
            }

            insert outPAWSProject;

            return lastIdx;
        }

        public Integer createPAWSProjects(List<WFM_Project__c> wfmProjectList, Integer recordCount, Integer startIndex, List<TestDataWrapper> outPAWSProject)
        {
            Integer lastIdx = startIndex;
            List<ecrf__c> pawsProjectList = new List<ecrf__c>();
            for(Integer idx = 0; idx < wfmProjectList.size(); idx++)
            {
                ecrf__c pawsProject = new ecrf__c(Name='[#STD#] PAWS Project ' + lastIdx, Project_Id__c=wfmProjectList.get(idx).ID, Type__c='Regular');
                pawsProjectList.add(pawsProject);
                outPAWSProject.add(new TestDataWrapper(pawsProject, 'ecrf__c'));
                lastIdx++;
            }

            insert pawsProjectList;

            return lastIdx;
        }

        public Integer createPAWSProjectCountries(List<ecrf__c> pawsProjectList, Integer recordCount, Integer startIndex, List<PAWS_Project_Flow_Country__c> outPAWSProjectCountry)
        {
            Integer lastIdx = startIndex;
            for(Integer idx = 0; idx < pawsProjectList.size(); idx++)
            {
                for(Integer subIdx = 0; subIdx < recordCount; subIdx++)
                {
                    PAWS_Project_Flow_Country__c projectCountry = new PAWS_Project_Flow_Country__c(Name='[#STD#] Project Country ' + lastIdx, PAWS_Project__c=pawsProjectList.get(idx).ID);
                    outPAWSProjectCountry.add(projectCountry);
                    lastIdx++;
                }
            }

            insert outPAWSProjectCountry;

            return lastIdx;
        }

        public Integer createPAWSProjectCountries(List<TestDataWrapper> pawsProjectList, Integer recordCount, Integer startIndex, List<TestDataWrapper> outPAWSProjectCountry)
        {
            Integer lastIdx = startIndex;
            List<PAWS_Project_Flow_Country__c> countryList = new List<PAWS_Project_Flow_Country__c>();
            for(Integer idx = 0; idx < pawsProjectList.size(); idx++)
            {
                TestDataWrapper wrappedPawsProject = pawsProjectList.get(idx);
                for(Integer subIdx = 0; subIdx < recordCount; subIdx++)
                {
                    PAWS_Project_Flow_Country__c projectCountry = new PAWS_Project_Flow_Country__c(Name='[#STD#] Project Country ' + lastIdx, PAWS_Project__c=wrappedPawsProject.RecordID);
                    countryList.add(projectCountry);
                    outPAWSProjectCountry.add(wrappedPawsProject.createAndAddChild(projectCountry, 'paws_project_flow_country__c'));
                    lastIdx++;
                }
            }

            insert countryList;

            return lastIdx;
        }

        public Integer createPAWSProjectSites(List<PAWS_Project_Flow_Country__c> pawsProjectCountryList, Integer recordCount, Integer startIndex, List<PAWS_Project_Flow_Site__c> outPAWSProjectSiteList)
        {
            Integer lastIdx = startIndex;
            for(Integer idx = 0; idx < pawsProjectCountryList.size(); idx++)
            {
                for(Integer subIdx = 0; subIdx < recordCount; subIdx++)
                {
                    PAWS_Project_Flow_Site__c projectSite = new PAWS_Project_Flow_Site__c(Name='[#STD#] Site ' + lastIdx, PAWS_Country__c=pawsProjectCountryList.get(idx).ID);
                    outPAWSProjectSiteList.add(projectSite);
                    lastIdx++;
                }
            }

            insert outPAWSProjectSiteList;

            return lastIdx;
        }

        public Integer createPAWSProjectSites(List<TestDataWrapper> pawsProjectCountryList, Integer recordCount, Integer startIndex, List<TestDataWrapper> outPAWSProjectSiteList)
        {
            Integer lastIdx = startIndex;
            List<PAWS_Project_Flow_Site__c> projectSiteList = new List<PAWS_Project_Flow_Site__c>();
            for(Integer idx = 0; idx < pawsProjectCountryList.size(); idx++)
            {
                TestDataWrapper wrappedProjectCountry = pawsProjectCountryList.get(idx);
                for(Integer subIdx = 0; subIdx < recordCount; subIdx++)
                {
                    PAWS_Project_Flow_Site__c projectSite = new PAWS_Project_Flow_Site__c(Name='[#STD#] Site ' + lastIdx, PAWS_Country__c=wrappedProjectCountry.RecordID);
                    projectSiteList.add(projectSite);
                    outPAWSProjectSiteList.add(wrappedProjectCountry.createAndAddChild(projectSite, 'paws_project_flow_site__c'));
                    lastIdx++;
                }
            }

            insert projectSiteList;

            return lastIdx;
        }

        public Integer createFlows(List<sObject> pawsSobjectList, Integer startIndex, String flowType, String patternName, String objectType, List<STSWR1__Flow__c> outFlowList)
        {
            Integer lastIdx = startIndex;
            for(Integer idx = 0; idx < pawsSobjectList.size(); idx++)
            {
                STSWR1__Flow__c flow = new STSWR1__Flow__c(Name=patternName + ' ' + lastIdx, STSWR1__Type__c=flowType, STSWR1__Object_Type__c=objectType, STSWR1__Status__c = 'Active', STSWR1__Source_Id__c=pawsSobjectList.get(idx).ID);

                outFlowList.add(flow);
                lastIdx++;
            }

            insert outFlowList;

            return lastIdx;
        }

        public Integer createFlows(List<TestDataWrapper> inTestDataWrappedList, Integer startIndex, String flowType, String patternName)
        {
            Integer lastIdx = startIndex;
            List<STSWR1__Flow__c> flowList = new List<STSWR1__Flow__c>();
            for(Integer idx = 0; idx < inTestDataWrappedList.size(); idx++)
            {
                TestDataWrapper testDataWrapped = inTestDataWrappedList.get(idx);
                STSWR1__Flow__c flow = new STSWR1__Flow__c(Name=patternName + ' ' + lastIdx, STSWR1__Type__c=flowType, STSWR1__Object_Type__c=testDataWrapped.RecordType, STSWR1__Status__c = 'Active', STSWR1__Source_Id__c=testDataWrapped.RecordID);
                testDataWrapped.addFlow(flow);
                flowList.add(flow);
                lastIdx++;
            }

            insert flowList;

            return lastIdx;
        }

        public List<STSWR1__Flow_Step_Junction__c> buildSteps(List<TestDataWrapper> inTestDataWrappedList)
        {
            List<STSWR1__Flow_Step_Junction__c> stepList = new List<STSWR1__Flow_Step_Junction__c>();
            for(Integer idx = 0; idx < inTestDataWrappedList.size(); idx++)
            {
                TestDataWrapper testDataWrapped = inTestDataWrappedList.get(idx);
                for(FlowWrapper eachWrappedFlow : testDataWrapped.Flows)
                {
                    STSWR1__Flow_Step_Junction__c step = new STSWR1__Flow_Step_Junction__c(
                        Name='[#STD#] Step ' + eachWrappedFlow.Steps.size(), 
                        STSWR1__Flow__c=eachWrappedFlow.RecordID, 
                        STSWR1__Is_First_Step__c=eachWrappedFlow.Steps.size() == 0 ? true : false);

                    eachWrappedFlow.Steps.add(step);
                    stepList.add(step);
                }

                if(testDataWrapped.Children.isEmpty() == false)
                {
                    stepList.addAll(buildSteps(testDataWrapped.Children));
                }
            }

            return stepList;
        }

        public List<STSWR1__Flow_Step_Action__c> buildActions(List<TestDataWrapper> inTestDataWrappedList)
        {
            List<STSWR1__Flow_Step_Action__c> actionList = new List<STSWR1__Flow_Step_Action__c>();
            for(Integer idx = 0; idx < inTestDataWrappedList.size(); idx++)
            {
                TestDataWrapper testDataWrapped = inTestDataWrappedList.get(idx);

                if(testDataWrapped.Children.isEmpty() == false)
                {
                    for(TestDataWrapper eachWrappedChild : testDataWrapped.Children)
                    {
                        for(FlowWrapper eachWrappedFlow : eachWrappedChild.Flows)
                        {
                            STSWR1__Flow_Step_Action__c action = new STSWR1__Flow_Step_Action__c(
                            STSWR1__Config__c = JSON.serialize(new Map<String, Object> { 'flowId' => eachWrappedFlow.RecordID}), 
                            STSWR1__Flow_Step__c = eachWrappedFlow.Steps.get(0).ID,
                            STSWR1__Type__c = 'Start Sub Flow');

                            eachWrappedFlow.Actions.add(action);
                            actionList.add(action);  
                        }           
                    }

                    actionList.addAll(buildActions(testDataWrapped.Children));
                }
            }

            return actionList;
        }

        public List<STSWR1__Flow_Instance__c> buildFlowInstaces(List<TestDataWrapper> inTestDataWrappedList)
        {
            List<STSWR1__Flow_Instance__c> flowInstanceList = new List<STSWR1__Flow_Instance__c>();
            for(Integer idx = 0; idx < inTestDataWrappedList.size(); idx++)
            {
                TestDataWrapper testDataWrapped = inTestDataWrappedList.get(idx);
                for(FlowWrapper eachWrappedFlow : testDataWrapped.Flows)
                {
                    STSWR1__Flow_Instance__c flowInstance = new STSWR1__Flow_Instance__c(
                        STSWR1__Object_Id__c = testDataWrapped.RecordID,
                        STSWR1__Object_Name__c = String.valueOf(testDataWrapped.Record.get('Name')),
                        STSWR1__Flow__c = eachWrappedFlow.RecordID,
                        STSWR1__Is_Active__c = true);

                    eachWrappedFlow.FlowInstances.add(flowInstance);
                    flowInstanceList.add(flowInstance);
                }

                if(testDataWrapped.Children.isEmpty() == false)
                {
                    flowInstanceList.addAll(buildFlowInstaces(testDataWrapped.Children));
                }
            }

            return flowInstanceList;
        }

        public List<STSWR1__Flow_Instance_Cursor__c> buildFlowInstaceCursors(List<TestDataWrapper> inTestDataWrappedList)
        {
            List<STSWR1__Flow_Instance_Cursor__c> cursorList = new List<STSWR1__Flow_Instance_Cursor__c>();
            for(Integer idx = 0; idx < inTestDataWrappedList.size(); idx++)
            {
                TestDataWrapper testDataWrapped = inTestDataWrappedList.get(idx);

                for(FlowWrapper eachWrappedFlow : testDataWrapped.Flows)
                {
                    STSWR1__Flow_Instance_Cursor__c cursor = new STSWR1__Flow_Instance_Cursor__c(
                        STSWR1__Flow_Instance__c=eachWrappedFlow.FlowInstances.get(0).ID, 
                        STSWR1__Step__c=eachWrappedFlow.Steps.get(0).ID, 
                        STSWR1__Step_Assigned_To__c=UserInfo.getUserId(), 
                        STSWR1__Status__c='In Progress');

                    eachWrappedFlow.Cursors.add(cursor);
                    cursorList.add(cursor);
                }

                if(testDataWrapped.Children.isEmpty() == false)
                {
                    cursorList.addAll(buildFlowInstaceCursors(testDataWrapped.Children));
                }
            }

            return cursorList;
        }
    }

    public class TestDataWrapper
    {
        public TestDataWrapper()
        {
            Children = new List<TestDataWrapper>();
            Flows = new List<FlowWrapper>();
        }

        public TestDataWrapper(sObject inRecord, String inRecordType)
        {
            this();
            Record = inRecord;
            RecordType = inRecordType;
        }

        public TestDataWrapper(sObject inRecord, String inRecordType, TestDataWrapper inParent)
        {
            this(inRecord, inRecordType);
            Parent = inParent;
        }

        public TestDataWrapper Parent { get; set; }
        public ID RecordID { get { return Record == null ? null : Record.ID; } }
        public sObject Record { get; set; }
        public String RecordType { get; set; }
        public List<FlowWrapper> Flows { get; set; }
        public List<TestDataWrapper> Children { get; set; }
        
        public TestDataWrapper createAndAddChild(sObject inRecord, String inRecordType)
        {
            TestDataWrapper child = new TestDataWrapper(inRecord, inRecordType, this);
            Children.add(child);
            return child;
        }

        public FlowWrapper addFlow(STSWR1__Flow__c inFlow)
        {
            FlowWrapper wrappedFlow = new FlowWrapper(inFlow);
            Flows.add(wrappedFlow);
            return wrappedFlow;
        }
    }

    public class FlowWrapper
    {
        public FlowWrapper()
        {
            Steps = new List<STSWR1__Flow_Step_Junction__c>();
            Actions = new List<STSWR1__Flow_Step_Action__c>();
            FlowInstances = new List<STSWR1__Flow_Instance__c>();
            Cursors = new List<STSWR1__Flow_Instance_Cursor__c>();
        }

        public FlowWrapper(STSWR1__Flow__c inFlow)
        {
            this();
            Record = inFlow;
        }

        public ID RecordID { get{ return Record == null ? null : Record.ID; } }
        public STSWR1__Flow__c Record { get; set; }

        public List<STSWR1__Flow_Step_Junction__c> Steps { get; set; }
        public List<STSWR1__Flow_Step_Action__c> Actions { get; set; }
        public List<STSWR1__Flow_Instance__c> FlowInstances { get; set; }
        public List<STSWR1__Flow_Instance_Cursor__c> Cursors { get; set; }

    }
}