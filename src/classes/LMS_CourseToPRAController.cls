public class LMS_CourseToPRAController{

    /************************************** 
     *  Code for : CourseToPRAController  *
     *  Project : CLEAR                   *    
     *  Author : Andrew Allen             * 
     *  Last Updated Date : 4/9/2013      * 
     *************************************/

    // Instance Variables
    public String commitDate{get;set;}
    public String courseUpdateStart{get;set;}
    public String courseUpdateEnd{get;set;}
    public String roleName{get;set;}
    public String roleNameStyle{get;private set;}
    public String roleFilter{get;private set;}
    public String businessUnit{get;set;}
    public String department{get;set;}
    public String jobTitle{get;set;}
    public String jobFamily{get;set;}
    public String region{get;set;}
    public String country{get;set;}
    public String empType{get;set;}
    public String courseTextStyle{get;set;}
    public String courseType{get;set;}
    public String courseText{get;set;}    
    public String courseTextPackage{get;set;}
    public String courseTextStylePackage{get;private set;}    
    public String searchFilter{get;set;}
    public String searchFilterPackage{get;private set;}    
    public String roleNameDisplay{get;set;}
    public String roleResultId{get;set;}   
    public String assignment{get;set;}
    public String roleErrorText{get;private set;}
    public String courseErrorText{get;private set;}
    public String webServiceError{get;private set;}
    public String impactChange{get;set;}
    public Boolean bCreateRole{get;set;}
    public Boolean doSendRole{get;set;}
    public Boolean blnUpdated{get;set;}
    public Boolean allChecked{get;set;}
    public Boolean allCheckedCourse{get;set;}
    public Boolean bWebServiceError{get;private set;}
    public Integer employeeCount{get;set;}
    public Decimal impactStart{get;set;}
    public Decimal impactEnd{get;set;}
    private Decimal impactNum{get;set;}

    // Collections
    public list<LMS_CourseList> selCourses{get;private set;}
    public list<LMS_CourseAssignments> courses{get;private set;}
    public list<LMS_Role__c> roles{get;private set;}
    private map<String, String> roleResult{get;private set;}
    private map<String, String> roleCommitted{get;private set;}
    private map<String, String> roleDraft{get;private set;}
    private map<String, LMS_Role_Course__c> mappings{get;private set;}
    private map<String, CourseDomainSettings__c> settings{get;private set;}
    private map<String, LMSConstantSettings__c> constants{get;private set;}
    private Integer currentJTSLIndex {get;set;}
    private map<Integer, list<selectoption>> jtSelectoptionMap {get;private set;}

    // Error Varaibles
    public list<String> sfdcErrors{get;private set;}
    public list<String> roleError{get;private set;}
    public list<String> addErrors{get;private set;}
    public list<String> createErrors{get;private set;}
    public list<String> deleteErrors{get;private set;}
    public list<String> calloutErrors{get;private set;}
    
    // Constants
    private static final String NEXT_PAGINATE = '------- Next Page -------';
    private static final String PREV_PAGINATE = '----- Previous Page -----';

    /**
     *  Constructor
     *  Initializes all data
     */
    public LMS_CourseToPRAController() { 
        initSettings();
        initVals();
        checkParams();
    }

    /**
     *  Initialize course search data
     */
    private void initVals() {
        String internalDomain = settings.get('Internal').Domain_Id__c;
        
        currentJTSLIndex = 1;
        jtSelectoptionMap = new map<Integer, list<selectoption>>();
        
        doSendRole = true;
        bWebServiceError = false;
        courseText = 'Enter course title';
        courseTextStyle = 'WaterMarkedTextBox';
        
        courseTextPackage ='Enter package title';
        courseTextStylePackage ='WaterMarkedTextBox';
         
        roleName = 'Enter Role Name';
        roleNameStyle = 'WaterMarkedTextBox';
        roleFilter = ' and Role_Type__r.Name = \'PRA\' and Status__c = \'Active\'';
        searchFilter = 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
		    'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
		    'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';
    
     	searchFilterPackage = 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
		    'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
		    'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';

    }

    /**
     *  Get constants from custom settings
     */
    private void initSettings() {
        settings = CourseDomainSettings__c.getAll();
        constants = LMSConstantSettings__c.getAll();
    }

    /**
     * Look for page parameters, set client, project, project role when found
     */
    private void checkParams() {
        String roleId = ApexPages.currentPage().getParameters().get('roleId');
        System.debug('--------------------------------------'+roleId);
        if(null != roleId) {
            LMS_Role__c role = 
            [SELECT Job_Class_Desc__r.Name, Job_Title__r.Job_Title__c, Business_Unit__r.Name, Department__r.Name,
            Region__r.Region_Name__c, Country__r.Name, Employee_Type__r.Name from LMS_Role__c where Id = :roleId];

            if(null != role.Job_Class_Desc__r.Name)jobFamily = role.Job_Class_Desc__r.Name;
            if(null != role.Job_Title__r.Job_Title__c)jobTitle = role.Job_Title__r.Job_Title__c;
            if(null != role.Business_Unit__r.Name)businessUnit = role.Business_Unit__r.Name;
            if(null != role.Department__r.Name)department = role.Department__r.Name;
            if(null != role.Region__r.Region_Name__c)region = role.Region__r.Region_Name__c;
            if(null != role.Country__r.Name)country = role.Country__r.Name;
            if(null != role.Employee_Type__r.Name)empType = role.Employee_Type__r.Name;
            search();
        }
    }

    /**
     *  Closes the list for course search
     */
    public void closeCourseSearch() {
        selCourses = null;
    }

    /**
     *  Function to change the Autocomplete filters and fields
     */
    public void searchType() {
        String internalDomain = settings.get('Internal').Domain_Id__c;
        if(courseType == 'Select Course Type' || courseType == 'All') {
            searchFilter = 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
    			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
    
    		searchFilterPackage = 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
    			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
    
        } else if(courseType == 'Courses Only') {
            searchFilter = 'and Type__c NOT IN (\'PST\',\'SOP\') and Domain_Id__c = \'' + internalDomain + '\' ' + 
    			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
    
     		searchFilterPackage = 'and Type__c NOT IN (\'PST\',\'SOP\') and Domain_Id__c = \'' + internalDomain + '\' ' + 
    			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
    
        } else if(courseType == 'SOPs Only') {
            searchFilter = 'and Type__c = \'SOP\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
    			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
    
     		searchFilterPackage = 'and Type__c = \'SOP\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
    			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
        }
    }

    /**
     *  Business Unit LOV
     */
    public list<selectoption> getBusinessUnitList() {
        list<selectoption> business = new list<selectoption>();
        business = LMS_LookUpDataAccess.getBusinessUnits(false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        for(integer i=0; i<business.size(); i++)
            options.add(business[i]);

        return options;
    }

    /**
     *  Department LOV w/ filtering based on Business Unit
     */
    public list<selectoption> getDepartmentList() {
        list<selectoption> department = new list<selectoption>();
            department = LMS_LookUpDataAccess.getDepartments(false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        for(integer i=0; i<department.size(); i++)
            options.add(department[i]);

        return options;
    }

    /**
     *  Job Family LOV
     */
    public list<selectoption> getJobFamilyList() {
        list<selectoption> family = new list<selectoption>();
        family = LMS_LookUpDataAccess.getRoles(false, false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        for(integer i = 0; i < family.size(); i++)
            options.add(family[i]);
        return options;
    }

    /**
     *  Job Title LOV w/ filtering based on Job Family
     */
    public list<selectoption> getJobTitleList() {
    	Integer mapIndex = 1;
    	jtSelectoptionMap.clear();
        list<selectoption> title = new list<selectoption>();
        title = LMS_LookUpDataAccess.getTitles(false);
    	list<selectoption> options = new list<selectoption>();
    	options.add(new selectoption('',''));
        if(jobFamily == null) {
            for(Integer i = 0; i < title.size(); i++){
            	if(options.size() == 999) {
            		options.add(new selectoption(NEXT_PAGINATE,NEXT_PAGINATE));
            		jtSelectoptionMap.put(mapIndex, options);
            		mapIndex++;
            		options = new list<selectoption>();
            		options.add(new selectoption('',''));
            		options.add(new selectoption(PREV_PAGINATE,PREV_PAGINATE));
            		options.add(title[i]);
            	} else {
                	options.add(title[i]);
            	}
            }
            jtSelectoptionMap.put(mapIndex, options);
        } else {
            for(Job_Title__c obj : [select Job_Title__c,Job_Class_Desc__r.Name from Job_Title__c where Job_Class_Desc__r.Name =:jobFamily 
                                    and Status__c != 'I' order by Job_Title__c asc]){
                options.add(new selectoption(obj.Job_Title__c,obj.Job_Title__c));
            }
            currentJTSLIndex = mapIndex;
            jtSelectoptionMap.put(mapIndex, options);
        }
        return jtSelectoptionMap.get(currentJTSLIndex);
    }
    
    public Pagereference updateJTList() {
    	if (jobTitle != null && jobTitle != '') {
	    	if (jobTitle.equals(NEXT_PAGINATE)) {
	    		currentJTSLIndex++;
	    	} else if (jobTitle.equals(PREV_PAGINATE)) {
	    		currentJTSLIndex--;
	    	}
    	}
    	getJobTitleList();
    	return null;
    }

    /**
     *  Function to filter down Job Title
     */
    public pageReference getTitleOnFamily() {
        getJobTitleList();
        return null;
    }

    /**
     *  Region LOV
     */
    public list<selectoption> getRegionList() {
        list<selectoption> region = new list<selectoption>();
        region = LMS_LookUpDataAccess.getRegion(false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        for(integer i=0; i<region.size(); i++) {
            options.add(region[i]);
        }
        return options;
    }

    /**
     *  Country LOV w/ filtering based on Region
     */
    public list<selectoption> getCountryList() {
        list<selectoption> country = new list<selectoption>();
        country = LMS_LookUpDataAccess.getCountry(false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        if(region == null) {
            for(integer i=0; i<country.size(); i++) {
                options.add(country[i]);
            }
        } else {
            for(Country__c obj : [select Name from Country__c where Region_Name__c =:region order by Name asc]) {
                options.add(new selectoption(obj.Name, obj.Name));
            }
        }
        return options;
    }

    /**
     *  Function to filter down Country
     */
    public pagereference getCountryOnRegion() {
        getCountryList();
        return null;
    }

    /**
     *  Employee Types LOV
     */
    public list<selectoption> getTypesList() {
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
            options.add(new selectoption('Contractor','Contractor'));
            options.add(new selectoption('Employees','Employee'));
        return options;
    }

    /**
     *  Course Types LOV
     */
    public list<selectoption> getCourseTypes() {
        list<selectoption> cType = new list<selectoption>();
            cType.add(new selectoption('Select Course Type','Select Course Type'));
            cType.add(new selectoption('All','All'));
            cType.add(new selectoption('Courses Only','Courses Only'));
            cType.add(new selectoption('SOPs Only','SOPs Only'));
        return cType;
    }

    /**
     *  Method to reset role information
     */
    public void roleReset() {
        //Reset LOVs
        getTitleOnFamily();
        getCountryOnRegion();

        bWebServiceError = false;
        roles = null;
        courses = null;
        selCourses = null;

        initVals();
        viewImpact();
    }

    /**
     *  Method to reset course information
     */
    public void courseReset() { 
        selCourses = null;
        viewImpact();
    }

    /** 
     *  Search Implementation method :
     *  This method uses the UI role criteria to search for the role
     *  and the courses that have been assigned to that role 
     */
    public void search() {
        Boolean acSearch;
        bWebServiceError = false;
        roleResult = new map<String, String>();
        roleCommitted = new map<String, String>();
        roleDraft = new map<String, String>();
        mappings = new map<String, LMS_Role_Course__c>();

        String draft = constants.get('statusDraft').Value__c;
        String committed = constants.get('statusCommitted').Value__c;
        String draftDelete = constants.get('statusDraftDelete').Value__c;
        String removed = constants.get('statusRemoved').Value__c;

        //Join query to get course information on a certain role
        String courseQry = 'select Role_Id__r.SABA_Role_PK__c,Role_Id__r.Id,Course_Id__r.Name,Course_Id__r.Course_Code__c,' + 
    'Course_Id__r.Title__c,Course_Id__r.Available_From__c,Course_Id__r.Discontinued_From__c,Course_Id__r.Status__c,Course_Id__r.Duration__c,' + 
    'Course_Id__r.Target_Days__c,Course_Id__r.Duration_Display__c,Course_Id__r.Id,Course_Id__r.SABA_ID_PK__c,Assigned_On__c,Assigned_By__c,' + 
    'Commit_Date__c,Status__c,Previous_Status__c,Sync_Status__c,LastModifiedDate ' + 
    'from Rolecourses__r where Status__c != :removed order by Status__c,Course_Id__r.Title__c';
        //Main query to pull the role based on criteria in UI
        String roleQry = 'select Role_Name__c,SABA_Role_PK__c,Status__c,Sync_Status__c,Employee_Count__c,(' + courseQry + ') from LMS_Role__c where Id != null ' + 
    'and Role_Type__r.Name = \'PRA\'';

        if(null != roleName && roleName != '' && roleName != 'Enter Role Name') {
            roleQry += 'and Role_Name__c = :roleName';
            acSearch = true;
        } else {
            acSearch = false;
            /*                                Check for each type of criteria                              */
            /***********************************************************************************************/
            if(jobFamily == null && jobTitle == null) {
                roleQry += 'and Job_Class_Desc__c = null and Job_Title__c = null ';
            } else if(jobFamily != null && jobTitle == null) {
                roleQry += 'and Job_Class_Desc__r.Name =: jobFamily and Job_Title__c = null ';
            } else if((jobFamily == null && jobTitle != null)||(jobFamily != null && jobTitle != null)) {
                roleQry += 'and Job_Title__r.Job_Title__c =: jobTitle ';
            }
            /***********************************************************************************************/     
            if(businessUnit == null && department == null ){
                roleQry += 'and Business_Unit__c = null and Department__c = null ';
            } else if(businessUnit != null && department == null) {
                roleQry += 'and Business_Unit__r.Name =: businessUnit and Department__c = null ';
            } else if(businessUnit == null && department != null) {
                roleQry += 'and Department__r.Name =: department ';
            } else if(businessUnit != null && department != null) {
                roleQry += 'and Business_Unit__r.Name =: businessUnit and Department__r.Name =: department ';
            }
            /***********************************************************************************************/
            if(region == null && country == null)
                roleQry += 'and Region__c = null and Country__c = null ';
            else if(region != null && country == null)
                roleQry += 'and Region__r.Region_Name__c =: region and Country__c = null ';
            else if((region == null && country != null)||(region != null && country != null)){
                roleQry += 'and Country__r.Name = :country ';
            }
            /***********************************************************************************************/
            if(empType == null)
                roleQry += 'and Employee_Type__c = null';
            else
                roleQry += 'and Employee_Type__r.Name = :empType';
            /***********************************************************************************************/
        }
        allChecked = false;
        roles = Database.query(roleQry);
        ///If no role exists do not render table & display message
        bCreateRole = true;
        if(roles.size() == 0) {
            if(acSearch) {
                bCreateRole = false;
                roleErrorText = 'Role not found, please enter a different role name';
            } else {
                bCreateRole = true;
                roleErrorText = 'No role name found, Would you like to create role?';
            }
        } else {
            //else display course information for role
            courses = new list<LMS_CourseAssignments>();
            for(LMS_Role__c r : roles) {
                roleNameDisplay = r.Role_Name__c;
                roleResultId = r.Id;
                for(LMS_Role_Course__c c : r.roleCourses__r) {
                    mappings.put(c.Id, c);
                    roleResult.put(c.Course_Id__c,c.Course_Id__c);
                    if(c.Status__c == committed || c.Status__c == draftDelete) {
                        roleCommitted.put(c.Course_Id__c,c.Course_Id__c);
                    } else if(c.Status__c == draft) {
                        roleDraft.put(c.Course_Id__c,c.Course_Id__c);
                    }
                    courses.add(new LMS_CourseAssignments(c));
                }
            }
            if(roles[0].SABA_Role_PK__c != '') {
                bCreateRole = false;
                roleErrorText = 'Role is out of Sync with SABA, courses can be added but not committed until sync is resolved';
            }
            if(roles[0].Status__c == 'Inactive') {
                bCreateRole = false;
                roleErrorText = 'Role is inactive, enter a different role name';
            }
        }
        viewImpact();         
    }

    /**
     *  Course Search implementation 
     */
    public void courseSearch() {
        // Build the query for the course search
        String courseSearchQry = '';
        Datetime updateStart, updateEnd;
        Set<String> cAssigned = new Set<String>();
        String internalDomain = settings.get('Internal').Domain_Id__c;

        if(roleCommitted != null) {
            cAssigned = roleCommitted.keySet();
        }
        courseSearchQry = 'select Name,Course_Code__c,Title__c,Duration__c,Target_Days__c,Duration_Display__c,Available_From__c,Discontinued_From__c,' + 
            'Status__c,SABA_ID_PK__c from LMS_Course__c where Id not in:cAssigned and Domain_Id__c=\'' + String.escapeSingleQuotes(internalDomain) + 
            '\' and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
            'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';
        allCheckedCourse = false;
        selCourses = new list<LMS_CourseList>();
        // Parse dates from UI if present
        if(courseUpdateStart != null && courseUpdateStart != '') {
            System.debug('--------------------start date-------------------'+courseUpdateStart);
            Date updatedDate = Date.parse(courseUpdateStart);
            updateStart = Datetime.newInstance(updatedDate, Time.newInstance(0,0,0,0));
            System.debug('-------------------start datetime---------------'+updateStart);
            courseSearchQry += ' and LastModifiedDate > :updateStart';
        }
        if(courseUpdateEnd != null && courseUpdateEnd != '') {
            System.debug('--------------------end date-------------------'+courseUpdateEnd);
            Date updatedDate = Date.parse(courseUpdateEnd);
            updateEnd = Datetime.newInstance(updatedDate, Time.newInstance(0,0,0,0));
            System.debug('-------------------start datetime---------------'+updateEnd);
            courseSearchQry += ' and LastModifiedDate < :updateEnd';
        }

        // Determine if search is based on SOPs, Courses or All courses
        if(courseType == 'Select Course Type' || courseType == 'All') {
            courseSearchQry += ' and Type__c != \'PST\'';
        } else if(courseType == 'Courses Only') {
            courseSearchQry += ' and Type__c NOT IN (\'PST\',\'SOP\')';
        } else if(courseType == 'SOPs only') {
            courseSearchQry += ' and Type__c = \'SOP\'';
        }

        // Search on Course Text
        if(courseText != 'Enter course title' && courseText != '')
            courseSearchQry += ' and Title__c like \''+ courseText + '%\'';

        //Search on Package Text
        if(courseTextPackage != 'Enter package title' && courseTextPackage != '') {
        courseSearchQry += 'and Course_Package__c like \'' + String.escapeSingleQuotes(courseTextPackage.trim()) + '%\'';
        }
        
        //Search on Updated within 30 days
        if(blnUpdated != false) {
            Datetime updated = System.now()-30;
            courseSearchQry += ' and Updated_On__c >:updated';
        }
        System.debug('----------------courseSearchQuery---------------'+courseSearchQry);
        list<LMS_Course__c> selectCourses = Database.query(courseSearchQry);
        if(selectCourses.size() > 1000) {
            courseErrorText = 'The result contains too many records. Please refine your search criteria and run the search again.';
            selectCourses = null;
        } else if(selectCourses.size() == 0) {
            courseErrorText = 'The result contains no records. Please refine your search criteria and run the search again.';
            selectCourses = null;
        } else {
            courseErrorText = '';
            for(LMS_Course__c c : selectCourses) {
                selCourses.add(new LMS_CourseList(c, roleResult));
            }
        }
    }    

    /**
     *  Function used to create new roles if query returns no results
     */
    public PageReference createRole() {
        sfdcErrors = new list<String>();
        map<String, list<String>> errors = new map<String, list<String>>();
        list<String> roleValues = new list<String>{jobFamily, jobTitle, businessUnit, department, region, country, empType};

        if(isRoleDataValid() && LMS_ToolsDataAccessor.isRoleValid('PRA', roleValues)){
            errors = LMS_ToolsModifyRoles.createRole(roleValues, 1);
            doSendRole = true;
            sfdcErrors = errors.get('SFDCEX');
        } else {
            webServiceError = 'You cannot change the search criteria during the role creation process.  Please click reset button and search again.';
            doSendRole = false;
            bWebServiceError = true;
        }

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        }

        if(doSendRole) {
            search();
            viewImpact();
        }
        return null;
    }

    private Boolean isRoleDataValid() {
        Boolean isValid = true;
        if(jobFamily == null && jobTitle == null && businessUnit == null && department == null 
            && region == null && country == null && empType == null)
            return !isValid;
        return isValid;
    }

    public PageReference cancelCreateRole() {
        roles = null;
        return null;
    }

    /**
     *  Function to invoke WebService calls after role has been inserted
     */
    public PageReference sendRoleData() {
        createErrors = new list<String>();
        calloutErrors = new list<String>();
        map<String, list<String>> errors = new map<String, list<String>>();
        list<String> roleValues = new list<String>{jobFamily, jobTitle, businessUnit, department, region, country, empType};

        if(doSendRole) {
            LMS_Role__c role = [select Id, Role_Name__c, Status__c, SABA_Role_PK__c, Role_Type__r.Name from LMS_Role__c where Id =:roleResultId];
            errors = LMS_ToolsService.sendRoleData(role);
            createErrors = errors.get('ROLE');
            calloutErrors = errors.get('CALLOUT');
            System.debug('---------------------errors-----------------------'+errors);
            System.debug('---------------------role error-----------------------'+createErrors);
            System.debug('---------------------callout error-----------------------'+calloutErrors);
        }

        if((null != createErrors && 0 < createErrors.size()) || (null != calloutErrors && 0 < calloutErrors.size())) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        }

        if(doSendRole) {
            LMS_ToolsModifyRoles.createEmployeeMapping(roleValues, roleResultId);
            search();
        }
        return null;
    }

    /**
     *  Function sync role with SABA
     */
    public PageReference syncRoleData() {
        createErrors = new list<String>();
        calloutErrors = new list<String>();
        map<String, list<String>> errors = new map<String, list<String>>();
        LMS_Role__c role = [select Id, Role_Name__c, Status__c, SABA_Role_PK__c, Role_Type__r.Name from LMS_Role__c where Id =:roleResultId];
        list<String> roleValues = new list<String>{jobFamily, jobTitle, businessUnit, department, region, country, empType};

        errors = LMS_ToolsService.sendRoleData(role);
        createErrors = errors.get('ROLE');
        calloutErrors = errors.get('CALLOUT');

        System.debug('---------------------errors-----------------------'+errors);
        System.debug('---------------------role error-----------------------'+createErrors);
        System.debug('---------------------callout error-----------------------'+calloutErrors);

        if((null != createErrors && 0 < createErrors.size()) || (null != calloutErrors && 0 < calloutErrors.size())) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }

        search();
        return null;
    }

    /**
     *  This method will add all selected course to the selected role
     *  All of these courses will be given a Draft status
     */
    public PageReference addCourse() {
        sfdcErrors = new list<String>();
        map<String, list<String>> errors = new map<String, list<String>>();

        errors = LMS_ToolsModifyAssignments.addCourses(selCourses, courses, mappings, roleResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while adding course to role.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        courseSearch();
        return null;
    }

    /**
     *  This method performs several functions depending on course assignment status
     *  If current status is Draft : course is deleted/unassigned
     *  If current status is Committed : course status is changed to Draft Delete
     *  If current status is Draft Delete : course status is changed back to Committed
     */
    public PageReference removeCourses() {
        sfdcErrors = new list<String>();
        map<String, list<String>> errors = new map<String, list<String>>();

        errors = LMS_ToolsModifyAssignments.removeCourses(courses, mappings);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while removing or reverting courses from role.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    /**
     *  This method will remove all changes made to the 
     *  current role assignment.
     *  Draft Delete -> Committed
     *  Draft -> Deleted
     */
    public PageReference cancel() {
        sfdcErrors = new list<String>();
        map<String, list<String>> errors = new map<String, list<String>>();

        errors = LMS_ToolsModifyAssignments.cancelDraftCourses(roleResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }

        search();
        courseSearch();
        viewImpact();
        return null;
    }

    public PageReference setCommitDate() {

        sfdcErrors = new list<String>();
        map<String, list<String>> errors = new map<String, list<String>>();

        Date dateCommit = Date.parse(commitDate);
        errors = LMS_ToolsModifyAssignments.setCommitDateCourses(courses, mappings, dateCommit);

        sfdcErrors = errors.get('SFDCEX');
        System.debug('-------------errors------------'+errors);
        System.debug('-------------sfdcErrors-----------'+sfdcErrors);

        if(null != sfdcErrors && sfdcErrors.size() > 0) {
            webServiceError = 'Errors occurred while applying commit date.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    /**
     *  This method finalizes changes to the course assignments
     *  Courses with Draft status will be changed to committed
     *  Courses with Draft Delete status will be unassigned/deleted from the list
     */
    public PageReference commitCourses() {

        sfdcErrors = new list<String>();
        addErrors = new list<String>();
        deleteErrors = new list<String>();
        calloutErrors = new list<String>();
        map<String, list<String>> errors = new map<String, list<String>>();

        errors = LMS_ToolsModifyAssignments.commitCourses(courses, mappings);
        sfdcErrors = errors.get('SFDCEX');
        addErrors = errors.get('A');
        deleteErrors = errors.get('D');
        calloutErrors = errors.get('CALLOUT');

        System.debug('-------------errors------------'+errors);
        System.debug('-------------sfdcErrors-----------'+sfdcErrors);
        System.debug('-------------addErrors------------'+addErrors);
        System.debug('-------------deleteErrors------------'+deleteErrors);
        System.debug('-------------calloutErrors------------'+calloutErrors);

        if((null != sfdcErrors && sfdcErrors.size() > 0) || (null != addErrors && 0 < addErrors.size()) || 
            (null != deleteErrors && 0 < deleteErrors.size()) || (null != calloutErrors && 0 < calloutErrors.size())) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError; 
        viewImpact();
        return null;
    }

    /**
     *  View impact of adding courses to a role
     */
    public void viewImpact() {
        impactNum = 0;
        impactEnd = 0;   
        impactStart = 0;    
        impactChange = '0';

        list<String> courseIds = new list<String>();
        // Get employee count and total hour count
        if(roles != null && roles.size() > 0) {
            employeeCount = Integer.valueOf(roles[0].Employee_Count__c);
        } else {
            employeeCount = 0;
        }
        if(roleResultId != null && roleResultId != '') {
            Decimal duration = LMS_ToolsDataAccessor.getTotalCourseDurationForCommittedMappings(roleResultId);
            if(duration != null) {
                impactStart = (duration*employeeCount/60).setScale(2);
            } else {
                impactStart = 0;
            }
        }
        // Get the course Ids of the current select courses
        if(selCourses != null) {
            for(LMS_CourseList c : selCourses) {
                if(c.selected && !roleResult.containsKey(c.courseId)) {
                    System.debug('-----------------c.selected--------------'+c.courseId);
                    System.debug('-----------------roleDraftKey--------------'+roleDraft.get(c.courseId));
                    courseIds.add(c.courseId);
                }
            }
        }
        if(courses != null) {            
            // Get the durations for the adding and removal of courses
            Double clToAdd = LMS_ToolsDataAccessor.getTotalCourseDurationForAddMappings(roleResultId);
            Double clToRemove = LMS_ToolsDataAccessor.getTotalCourseDurationForDeleteMappings(roleResultId);
            Double cToAdd = LMS_ToolsDataAccessor.getTotalCourseDurationFromCourseList(courseIds);
            impactNum = 0;

            if(clToAdd != null) {
                impactNum += clToAdd;
                System.debug('---------total additions from course list--------------'+clToAdd);
            }
            if(courseIds.size() > 0) {
                if(cToAdd != null) {
                    impactNum += cToAdd;
                    System.debug('---------total from selCourse list--------------'+cToAdd);
                }
            }
            if(clToRemove != null) {
                impactNum -= clToRemove;
                System.debug('---------total deletions from course list--------------'+clToRemove);
            }
            impactNum *= employeeCount;
            impactNum = (impactNum/60).setScale(2);

            if(impactNum > 0) {
                impactChange = '+ '+impactNum;
            } else {
                impactChange = String.valueOf(impactNum);
            }

            impactEnd = (impactStart + impactNum).setScale(2);
        } else {
            employeeCount = 0;
            impactStart = 0;
            impactNum = 0;
            impactChange = String.valueOf(impactNum);
            impactEnd = impactStart + impactNum;
        }
    }

    public PageReference showErrors() {
        PageReference pr = new PageReference('/apex/LMS_CoursePRAError');
        pr.setRedirect(false);
        return pr;
    }

}