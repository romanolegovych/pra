global with sharing class RM_MyProjectsController {
	public string userName{get;private set;}
	public string destLink{get; private set;}
	public list<WFM_Project__c> lstProj {get; private set;}
	public list<string> lstBU{get; private set;}
	private string projectStr;
	
	public RM_MyProjectsController(){
		userName = UserInfo.getName();
        user u = RM_OtherService.GetUserInfo(UserInfo.getUserId());
        if (u != null){ 
        	lstProj = getMyProject(u.email);
        	if (lstProj.size() > 0)
        		lstBU = RM_OtherService.getBUinGroup();
        }
	}
	
    private list<WFM_Project__c>  getMyProject(string userEmail){
		list<Employee_Details__c> pm = RM_EmployeeService.GetEmployeeDetailByEmployeeEmail(userEmail);
	  
		if (pm != null && pm.size() > 0){
    		system.debug('-------myid---------'+ pm[0].name);
    		list<WFM_Project__c> lstProj = RM_ProjectService.GetPMProject(pm[0].id, pm[0].name);
    		return lstProj;
		}
		return new list<WFM_Project__c>();
    		
	}
	@RemoteAction
   	global static list<string> getRoleData(string projectRID, string BU){
    	// { label: "Project Team", icon: "{!URLFOR($Resource.RM_Images,'group.jpg')}" }
		list<string>lstRole = new list<string>();
		
		
		List<AggregateResult> roleList =  RM_AssignmentsService.GetProjectRoleActiveEmplAccountInBU(projectRID, BU);
        system.debug('-------roleList---------'+ roleList);
        for(Integer i = 0; i < roleList.size(); i++){
            string sId = string.valueof(roleList[i].get('Project_Function__c'));
           	string nCount = string.valueOf(roleList[i].get('nEmpl'));
           	string role = sID +' ('+nCount + ')';
           	lstRole.add(role);
        }
        return lstRole;
   	 }
   	@RemoteAction 
   	global static list<string> getEmplData(string projectRID, string projectRole, string bu){
		
		list<string> lstEmp = new list<string>();
		system.debug('---projectRole---' + projectRole);  
  		list<WFM_employee_Allocations__c> emplList = RM_AssignmentsService.GetActiveEmployeeListByRoleProject(projectRID,  projectRole, bu);
        for(WFM_employee_Allocations__c a : emplList){
        	string empl = a.EMPLOYEE_ID__c + '_' + a.employee_id__r.full_name__c;
        	lstEmp.add(empl);
        }
        
       return lstEmp;
        	
	}
	
	
	
}