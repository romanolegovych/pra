public class LMS_RoleAdminServiceWrapper {

    private static String ENDPOINT = 'http://71.153.197.9:55561/services/RoleAdminService';
    private static Integer TIMEOUT = 50000;
    private static String DOMAIN = 'Internal';
    private static List<String> errors;
    
    public static LMS_RoleAdminService.roleResponse processRole(LMS_Role__c role) {
        LMS_RoleAdminService.roleResponse response = null;
        if(null != role) {
            System.debug('------------------sending role data---------------------');
            List<LMS_RoleAdminService.role> roles = new List<LMS_RoleAdminService.role>();
            roles.add(getRole(role));
        
            LMS_RoleAdminService.IRoleAdminServicePort service = getService();
            try {
                response = service.processRoles(roles);
                System.debug('------------------sending role data successful ---------------------');
            } catch (System.CalloutException e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                System.debug('------------------sending role data failed ---------------------');
                System.debug('An error occured:'+e.getMessage());
                System.debug('Stack trace:'+e.getStackTraceString());
                System.debug('Cause:'+e.getCause());
            }
            System.debug('Response is:'+response);
        }
        return response;
    }
    
    public static LMS_RoleAdminService.roleResponse processRoles(List<LMS_Role__c> roles) {
        LMS_RoleAdminService.roleResponse response = null;
        if(null != roles) {
            System.debug('------------------sending roles data---------------------');
            List<LMS_RoleAdminService.role> serviceRoles = new List<LMS_RoleAdminService.role>();
            if(null != roles) {
                for(LMS_Role__c role : roles) {
                    serviceRoles.add(getRole(role));
                }
            }
        
            LMS_RoleAdminService.IRoleAdminServicePort service = getService();
            try {
                response = service.processRoles(serviceRoles);
                System.debug('------------------sending roles data successful ---------------------');
            } catch (System.CalloutException e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                System.debug('------------------sending roles data failed ---------------------');
                System.debug('An error occured:'+e.getMessage());
                System.debug('Stack trace:'+e.getStackTraceString());
                System.debug('Cause:'+e.getCause());
            }
            System.debug('Response is:'+response);
        }
        return response;
    }
    
    public static LMS_RoleAdminService.courseRoleResponse processRolesToCourseAssociation(Map<String,List<LMS_Role_Course__c>> roleCourseListMap) {
        LMS_RoleAdminService.courseRoleResponse response = null;
        if(null != roleCourseListMap) {
            System.debug('------------------sending role course data---------------------');
            LMS_RoleAdminService.IRoleAdminServicePort service = getService();
            try {
                response = service.processRoleCourseAssociation(getRolesToCourseList(roleCourseListMap), null);
                System.debug('------------------sending role course data successful ---------------------');
            } catch (System.CalloutException e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                System.debug('------------------sending role course data failed ---------------------');
                System.debug('An error occured:'+e.getMessage());
                System.debug('Stack trace:'+e.getStackTraceString());
                System.debug('Cause:'+e.getCause());
            }
            System.debug('Response is:'+response);
        }
        return response;
    }
    
    public static LMS_RoleAdminService.courseRoleResponse processCoursesToRoleAssociation(Map<String,List<LMS_Role_Course__c>> roleCourseListMap){
        LMS_RoleAdminService.courseRoleResponse response = null;
        if(null != roleCourseListMap) {
            System.debug('------------------sending role course data---------------------');
            LMS_RoleAdminService.IRoleAdminServicePort service = getService();
            try {
                response = service.processRoleCourseAssociation(null, getCoursesToRoleList(roleCourseListMap));
                System.debug('------------------sending role course data successful ---------------------');
            } catch (System.CalloutException e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                System.debug('------------------sending role course data failed ---------------------');
                System.debug('An error occured:'+e.getMessage());
                System.debug('Stack trace:'+e.getStackTraceString());
                System.debug('Cause:'+e.getCause());
            }
            System.debug('Response is:'+response);
        }
        return response;
    }
    
    public static LMS_RoleAdminService.personRoleResponse processEmployeesToRoleAssociation(Map<String,List<LMS_Role_Employee__c>> roleEmpListMap){
        LMS_RoleAdminService.personRoleResponse response = null;
        if(null != roleEmpListMap) {
            System.debug('------------------sending role employee data---------------------');
            LMS_RoleAdminService.IRoleAdminServicePort service = getService();
            try {
                response = service.processRoleEmployeeAssociation(null, getEmployeesToRoleList(roleEmpListMap));
                System.debug('------------------sending role employee data successful ---------------------');
            } catch (System.CalloutException e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                System.debug('------------------sending role employee data failed ---------------------');
                System.debug('An error occured:'+e.getMessage());
                System.debug('Stack trace:'+e.getStackTraceString());
                System.debug('Cause:'+e.getCause());
            }
            System.debug('Response is:'+response);
        }
        return response;
    }
    
    public static LMS_RoleAdminService.roleEmployeeMessageHolder processEmployeesToRoleAsync(Map<String,List<LMS_Role_Employee__c>> roleEmpListMap){
        LMS_RoleAdminService.roleEmployeeMessageHolder response = null;
        if(null != roleEmpListMap) {
            System.debug('------------------sending role employee data async---------------------');
            LMS_RoleAdminService.IRoleAdminServicePort service = getService();
            try {
                response = service.processRoleEmployeeAsync(null, getEmployeesToRoleList(roleEmpListMap));
                System.debug('------------------sending role employee data async successful ---------------------');
            } catch (System.CalloutException e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                System.debug('------------------sending role employee data async failed ---------------------');
                System.debug('An error occured:'+e.getMessage());
                System.debug('Stack trace:'+e.getStackTraceString());
                System.debug('Cause:'+e.getCause());
            }
            System.debug('Response is:'+response);
        }
        return response;
    }
    
    public static LMS_RoleAdminService.personRoleResponse processRolesToEmployeeAssociation(Map<String,List<LMS_Role_Employee__c>> roleEmpListMap) {
        LMS_RoleAdminService.personRoleResponse response = null;
        if(null != roleEmpListMap) {
            System.debug('------------------sending role employee data---------------------');
            LMS_RoleAdminService.IRoleAdminServicePort service = getService();
            try {
                response = service.processRoleEmployeeAssociation(getRolesToEmployeeList(roleEmpListMap), null);
                System.debug('------------------sending role employee data successful ---------------------');
            } catch (System.CalloutException e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                System.debug('------------------sending role employee data failed ---------------------');
                System.debug('An error occured:'+e.getMessage());
                System.debug('Stack trace:'+e.getStackTraceString());
                System.debug('Cause:'+e.getCause());
            }
            System.debug('Response is:'+response);
        }
        return response;
    }
    
    public static List<String> getErrors() {
        return errors;
    }
    
    private static List<LMS_RoleAdminService.personsRoleMap> getEmployeesToRoleList(Map<String,List<LMS_Role_Employee__c>> roleEmpListMap){
        List<LMS_RoleAdminService.personsRoleMap> rolesPersonMapList = null;
        if(null != roleEmpListMap && !roleEmpListMap.isEmpty()) {
            rolesPersonMapList = new List<LMS_RoleAdminService.personsRoleMap>();
            LMS_RoleAdminService.personsRoleMap rolesPersonMap = null;
            Iterator<String> iter = roleEmpListMap.keySet().iterator();
            String action = null;
            List<LMS_Role_Employee__c> roleEmpList = null;

            Map<String,List<String>> personRoleListMap = null;
            List<String> personNos = null;
            String roleId = null;
            
            while(iter.hasNext()) {
                action = iter.next();
                if(null != action) {
                    roleEmpList = roleEmpListMap.get(action);
                    personRoleListMap = new Map<String,List<String>>();
                    if(null != roleEmpList) {
                        for(LMS_Role_Employee__c re: roleEmpList) {
                            if(null != re && null != re.Employee_Id__r.Name && null != re.Role_Id__r.SABA_Role_PK__c) {
                                roleId = re.Role_Id__r.SABA_Role_PK__c;
                                if(roleId.length() > 0) {
                                    personNos = personRoleListMap.get(roleId);
                                    if(null == personNos) {
                                        personNos = new List<String>();
                                    }
                                    personNos.add(re.Employee_Id__r.Name);
                                    personRoleListMap.put(roleId, personNos);
                                }
                            }
                        }
                    }
                    
                    if(null != personRoleListMap && !personRoleListMap.isEmpty()) {
                        Iterator<String> personRoleListMapIter = personRoleListMap.keySet().iterator();
                        while(personRoleListMapIter.hasNext()) {
                            roleId = personRoleListMapIter.next();
                            rolesPersonMap = new LMS_RoleAdminService.personsRoleMap();
                            rolesPersonMap.action = action;
                            rolesPersonMap.roleId = roleId;
                            rolesPersonMap.personNos = personRoleListMap.get(roleId);
                            rolesPersonMapList.add(rolesPersonMap);
                        }
                    }
                }
            }
        }
        return rolesPersonMapList;
    }
    
    private static List<LMS_RoleAdminService.rolesPersonMap> getRolesToEmployeeList(Map<String,List<LMS_Role_Employee__c>> roleEmpListMap) {
        List<LMS_RoleAdminService.rolesPersonMap> rolesPersonMapList = null;
        if(null != roleEmpListMap && !roleEmpListMap.isEmpty()) {
            rolesPersonMapList = new List<LMS_RoleAdminService.rolesPersonMap>();
            LMS_RoleAdminService.rolesPersonMap rolesPersonMap = null;
            Iterator<String> iter = roleEmpListMap.keySet().iterator();
            String action = null;
            List<LMS_Role_Employee__c> roleEmpList = null;

            Map<String,List<String>> personRoleListMap = null;
            List<String> roleIDs = null;
            String personNo = null;
            
            while(iter.hasNext()) {
                action = iter.next();
                if(null != action) {
                    roleEmpList = roleEmpListMap.get(action);
                    personRoleListMap = new Map<String,List<String>>();
                    if(null != roleEmpList) {
                        for(LMS_Role_Employee__c re: roleEmpList) {
                            if(null != re && null != re.Employee_Id__r.Name && null != re.Role_Id__r.SABA_Role_PK__c) {
                                personNo = re.Employee_Id__r.Name;
                                if(personNo.length() > 0) {
                                    roleIDs = personRoleListMap.get(personNo);
                                    if(null == roleIDs) {
                                        roleIDs = new List<String>();
                                    }
                                    roleIDs.add(re.Role_Id__r.SABA_Role_PK__c);
                                    personRoleListMap.put(personNo, roleIDs);
                                }
                            }
                        }
                    }
                    
                    if(null != personRoleListMap && !personRoleListMap.isEmpty()) {
                        Iterator<String> personRoleListMapIter = personRoleListMap.keySet().iterator();
                        while(personRoleListMapIter.hasNext()) {
                            personNo = personRoleListMapIter.next();
                            rolesPersonMap = new LMS_RoleAdminService.rolesPersonMap();
                            rolesPersonMap.action = action;
                            rolesPersonMap.personNo = personNo;
                            rolesPersonMap.rolesIds = personRoleListMap.get(personNo);
                            rolesPersonMapList.add(rolesPersonMap);
                        }
                    }
                }
            }
        }
        return rolesPersonMapList;
    }
    
    private static List<LMS_RoleAdminService.rolesCourseMap> getRolesToCourseList(Map<String,List<LMS_Role_Course__c>> roleCourseListMap) {
        List<LMS_RoleAdminService.rolesCourseMap> rolesCourseMapList = null;
        if(null != roleCourseListMap && !roleCourseListMap.isEmpty()) {
            rolesCourseMapList = new List<LMS_RoleAdminService.rolesCourseMap>();
            LMS_RoleAdminService.rolesCourseMap rolesCourseMap = null;
            Iterator<String> iter = roleCourseListMap.keySet().iterator();
            String action = null;
            List<LMS_Role_Course__c> roleCourseList = null;

            Map<String,List<String>> courseRoleListMap = null;
            List<String> roleIDs = null;
            String courseId = null;
            
            while(iter.hasNext()) {
                action = iter.next();
                if(null != action) {
                    roleCourseList = roleCourseListMap.get(action);
                    courseRoleListMap = new Map<String,List<String>>();
                    if(null != roleCourseList) {
                        for(LMS_Role_Course__c rc: roleCourseList) {
                            if(null != rc && null != rc.Course_Id__r.SABA_ID_PK__c && null != rc.Role_Id__r.SABA_Role_PK__c) {
                                courseId = rc.Course_Id__r.SABA_ID_PK__c;
                                if(courseId.length() > 0) {
                                    roleIDs = courseRoleListMap.get(courseId);
                                    if(null == roleIDs) {
                                        roleIDs = new List<String>();
                                    }
                                    roleIDs.add(rc.Role_Id__r.SABA_Role_PK__c);
                                    courseRoleListMap.put(courseId, roleIDs);
                                }
                            }
                        }
                    }
                    
                    if(null != courseRoleListMap && !courseRoleListMap.isEmpty()) {
                        Iterator<String> courseRoleListMapIter = courseRoleListMap.keySet().iterator();
                        while(courseRoleListMapIter.hasNext()) {
                            courseId = courseRoleListMapIter.next();
                            rolesCourseMap = new LMS_RoleAdminService.rolesCourseMap();
                            rolesCourseMap.action = action;
                            rolesCourseMap.courseId = courseId;
                            rolesCourseMap.rolesIds = courseRoleListMap.get(courseId);
                            rolesCourseMapList.add(rolesCourseMap);
                        }
                    }
                }
            }
        }
        return rolesCourseMapList;
    }
    private static List<LMS_RoleAdminService.coursesRoleMap> getCoursesToRoleList(Map<String,List<LMS_Role_Course__c>> courseRoleListMap){
        List<LMS_RoleAdminService.coursesRoleMap> coursesRoleMapList = null;
        if(null != courseRoleListMap && !courseRoleListMap.isEmpty()) {
            coursesRoleMapList = new List<LMS_RoleAdminService.coursesRoleMap>();
            LMS_RoleAdminService.coursesRoleMap coursesRoleMap = null;
            Iterator<String> iter = courseRoleListMap.keySet().iterator();
            String action = null;
            List<LMS_Role_Course__c> courseRoleList = null;

            Map<String,List<String>> roleCourseListMap = null;
            List<String> courseIDs = null;
            String roleId = null;
            
            while(iter.hasNext()) {
                action = iter.next();
                if(null != action) {
                    courseRoleList = courseRoleListMap.get(action);
                    roleCourseListMap = new Map<String,List<String>>();
                    if(null != courseRoleList) {
                        for(LMS_Role_Course__c rc: courseRoleList) {
                            if(null != rc && null != rc.Course_Id__r.SABA_ID_PK__c && null != rc.Role_Id__r.SABA_Role_PK__c) {
                                roleId = rc.Role_Id__r.SABA_Role_PK__c;
                                if(roleId.length() > 0) {
                                    courseIDs = roleCourseListMap.get(roleId);
                                    if(null == courseIDs) {
                                        courseIDs = new List<String>();
                                    }
                                    courseIDs.add(rc.Course_Id__r.SABA_ID_PK__c);
                                    roleCourseListMap.put(roleId, courseIDs);
                                }
                            }
                        }
                    }
                    
                    if(null != roleCourseListMap && !roleCourseListMap.isEmpty()) {
                        Iterator<String> courseRoleListMapIter = roleCourseListMap.keySet().iterator();
                        while(courseRoleListMapIter.hasNext()) {
                            roleId = courseRoleListMapIter.next();
                            coursesRoleMap = new LMS_RoleAdminService.coursesRoleMap();
                            coursesRoleMap.action = action;
                            coursesRoleMap.roleId = roleId;
                            coursesRoleMap.courseIDs = roleCourseListMap.get(roleId);
                            coursesRoleMapList.add(coursesRoleMap);
                        }
                    }
                }
            }
        }
        return coursesRoleMapList;
    }
    
    /*public void testGetRoleCourseList() {
        List<LMS_Role_Course__c> roleCourseList = [SELECT Course_Id__r.SABA_ID_PK__c, Id, Name, Role_Id__r.SABA_Role_PK__c, SABA_Role_Course_PK__c, Status__c, Sync_Status__c, SystemModstamp 
                                                  FROM LMS_Role_Course__c];
        if(null != roleCourseList) {
            System.debug('Found '+roleCourseList.size()+' role to course mappings in SFDC DB');
            Map<String,List<LMS_Role_Course__c>> roleCourseListMap = new Map<String,List<LMS_Role_Course__c>>();
            roleCourseListMap.put('D',roleCourseList);                   
            List<LMS_RoleAdminService.rolesCourseMap> rolesCourseMapList = getRolesToCourseList(roleCourseListMap);
            if(null != rolesCourseMapList) {
                System.debug('Created '+rolesCourseMapList.size()+' rolesCourseMap');
                List<String> roleIDs = null;
                for(LMS_RoleAdminService.rolesCourseMap rolesCourseMap: rolesCourseMapList) {
                    if(null != rolesCourseMap) {
                        System.debug('Action:'+rolesCourseMap.action);
                        System.debug('CourseId:'+rolesCourseMap.courseId);
                        roleIDs = rolesCourseMap.rolesIds;
                        if(null != roleIDs) {
                            System.debug('Found :'+roleIDs.size()+' roleIDs');
                            for(String roleID: roleIDs ) {
                                System.debug('roleID:'+roleID);
                            }
                        }
                    }
                }
                LMS_RoleAdminService.courseRoleResponse response = processRolesToCourseAssociation(roleCourseListMap);
                if(null != response) {
                    System.debug('Success? '+response.success);
                    System.debug('Details: '+response.details);
                    if(null != response.errors) {
                        System.debug('Got '+response.errors.size()+' errors');
                    }
                    if(null != response.coursesRoleResponseItems) {
                        System.debug('Got '+response.coursesRoleResponseItems.size()+' coursesRoleResponseItems');
                    }
                    if(null != response.rolesCourseResponseItems) {
                        System.debug('Got '+response.rolesCourseResponseItems.size()+' rolesCourseResponseItems');
                    }
                    /*for(LMS_Role_Course__c rc : roleCourseList) {
                        rc.Sync_Status__c = 'Y';
                    }
                    upsert roleCourseList;*//*
                }
            }
        }
    }
    public void testProcessRoles() {
        List<LMS_Role__c> roleList = [SELECT Adhoc_Role_Name__c, Business_Unit__c, Client_Id__c, Country__c, CreatedById, CreatedDate, IsDeleted, 
                                                    Department__c, Employee_Type__c, Job_Class_Desc__c, Job_Title__c, LastModifiedById, LastModifiedDate, OwnerId, Project_Id__c, 
                                                    Project_Role__c, Id, Region__c, Name, Role_Name__c, Role_Type__r.Name, SABA_Role_PK__c, Status__c, Sync_Status__c, SystemModstamp 
                                                    FROM LMS_Role__c  where SABA_Role_PK__c = null];
        if(null != roleList) {
            System.debug('Found '+roleList.size()+' roles without SABA PK in SFDC DB');
            LMS_RoleAdminService.roleResponse resp = processRoles(roleList);
            if(null != resp) {
                System.debug('Resp Success?'+resp.success);
                System.debug('Resp Details:'+resp.details);
                if(null != resp.errors) {
                    System.debug('Got '+resp.errors.size()+' errors');
                }
            }
            
        }
    }
    
    public void testGetRoleEmployeeList() {
        List<LMS_Role_Employee__c> roleEmpList = [SELECT Employee_Id__r.Name, Id, Name, Role_Id__r.SABA_Role_PK__c, Saba_Role_Employee_PK__c
                                                 FROM LMS_Role_Employee__c where Sync__c IN ('N') and Status__c NOT IN ('Draft')];
        if(null != roleEmpList) {
            System.debug('Found '+roleEmpList.size()+' role to employee mappings in SFDC DB');
            Map<String,List<LMS_Role_Employee__c>> roleEmpListMap = new Map<String,List<LMS_Role_Employee__c>>();
            roleEmpListMap.put('A',roleEmpList);                     
            List<LMS_RoleAdminService.rolesPersonMap> rolesPersonMapList = getRolesToEmployeeList(roleEmpListMap);
            if(null != rolesPersonMapList) {
                System.debug('Created '+rolesPersonMapList.size()+' rolesPersonMap');
                List<String> roleIDs = null;
                for(LMS_RoleAdminService.rolesPersonMap rolesPersonMap: rolesPersonMapList) {
                    if(null != rolesPersonMap) {
                        System.debug('Action:'+rolesPersonMap.action);
                        System.debug('PersonNo:'+rolesPersonMap.personNo);
                        roleIDs = rolesPersonMap.rolesIds;
                        if(null != roleIDs) {
                            System.debug('Found :'+roleIDs.size()+' roleIDs');
                            for(String roleID: roleIDs ) {
                                System.debug('roleID:'+roleID);
                            }
                        }
                    }
                }
            }
        }
    }*/
    
    private static LMS_RoleAdminService.role getRole(LMS_Role__c role) {
        LMS_RoleAdminService.role serviceRole = new LMS_RoleAdminService.role();
        serviceRole.domain = DOMAIN;
        serviceRole.name = role.Role_Name__c;
        serviceRole.status = role.Status__c;
        serviceRole.id = role.Id;
        serviceRole.roleid = role.SABA_Role_PK__c;
        serviceRole.type_x = role.Role_Type__r.Name;
        return serviceRole;
    }
    
    private static LMS_RoleAdminService.IRoleAdminServicePort getService() {
        initSettings();
        LMS_RoleAdminService.IRoleAdminServicePort service = new LMS_RoleAdminService.IRoleAdminServicePort();
        service.endpoint_x = ENDPOINT;
        service.timeout_x = TIMEOUT;
        return service;
    }
    
    private static void initSettings() {
        Map<String, RoleAdminServiceSettings__c> settings = RoleAdminServiceSettings__c.getall();
        ENDPOINT = settings.get('ENDPOINT').Value__c;
        DOMAIN = settings.get('DOMAIN').Value__c;
        Integer to  = Integer.valueof(settings.get('TIMEOUT').Value__c.trim());
        if(to != null && to > 0) {
            TIMEOUT = to;
        }
        System.debug('--------- Using following Custom Settings for RoleAdminServiceSettings ---------');
        System.debug('ENDPOINT:'+ENDPOINT);
        System.debug('DOMAIN:'+DOMAIN);
        System.debug('TIMEOUT:'+TIMEOUT);
        System.debug('--------- RoleAdminServiceSettings END ---------');
    }
    
}