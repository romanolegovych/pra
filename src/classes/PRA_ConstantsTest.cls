@isTest
private class PRA_ConstantsTest {
    
    static testMethod void testConstructor() {
        Test.startTest();
        PRA_Constants testObj = new PRA_Constants();
        Test.stopTest();    
    }
    
    static testMethod void testConstants() {
        String testVal;
        
        Test.startTest();
        testVal = PRA_Constants.CP_STATUS_NOT_STARTED;
        testVal = PRA_Constants.CP_STATUS_IN_PROGRESS;
        testVal = PRA_Constants.CP_STATUS_COMPLETE;   
        testVal = PRA_Constants.CP_STATUS_ON_HOLD;    
        testVal = PRA_Constants.CP_STATUS_IN_REVIEW;  
        testVal = PRA_Constants.CP_STATUS_RM;   
        
        testVal = PRA_Constants.PAGE_MODE_VIEW;  
        testVal = PRA_Constants.PAGE_MODE_EDIT;
        
        testVal = PRA_Constants.ST_NONE;  
        testVal = PRA_Constants.ST_DEFAULT_PROJECT_LABEL;
        testVal = PRA_Constants.MIGRATED_CLIENT_TASK;  
        testVal = PRA_Constants.CP_ALL; 
        
        testVal = PRA_Constants.TG_PF;        
        Test.stopTest();
    } 
}