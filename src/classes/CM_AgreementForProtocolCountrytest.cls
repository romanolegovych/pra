/** 
     * @description Test class CM_AgreementForProtocolCountry
     * @author      Lavakusareddy Poluru
     * @date        Created: 30-Jan-2015
     */



@isTest
public with sharing class CM_AgreementForProtocolCountrytest {
    private static final String Templateid =  [SELECT Id from RecordType WHERE SObjectType = 'Apttus__APTS_Agreement__c' and DeveloperName = 'Template'].Id;
        
        /** 
     * @description Test method testnewAgreement
     * @author      Lavakusareddy Poluru
     * @date        Created: 30-Jan-2015
     */
     
    static testMethod void testnewAgreement() {
    
        WFM_Client__c testClient = new WFM_Client__c();
        WFM_Contract__c testContract = new WFM_Contract__c();       
        WFM_Project__c testProject = new WFM_Project__c();
        Protocol_Country__c testProtocolCountry = new Protocol_Country__c();
        WFM_Protocol__c testProtocol = new WFM_Protocol__c();
        WFM_Site_Detail__c testSite = new WFM_Site_Detail__c();
        Country__c testcountry = new Country__c();
        Apttus__APTS_Agreement__c testtemplate = new Apttus__APTS_Agreement__c();
        list<Apttus__APTS_Agreement__c> testlist = new list<Apttus__APTS_Agreement__c>();
        list<Apttus__APTS_Agreement__c> testlist1 = new list<Apttus__APTS_Agreement__c>();
        
        testClient.Name = 'Ranbaxytest';                
        testClient.Client_Unique_Key__c = 'RBX001';
        insert testClient;
        
        testContract.Name = 'testcontract';
        testContract.Contract_Unique_Key__c = 'crnttest1';
        testContract.Client_ID__c = testClient.Id;
        insert testContract;
        
        testProject.Name = 'LiverCancer';
        testProject.Project_Unique_Key__c = 'LVC1';
        insert testProject;
        
        testProtocol.Name = 'testprotocol';
        testProtocol.Protocal_Unique_Key__c = 'test1';
        testProtocol.Project_ID__c = testProject.Id;
        insert testProtocol;
        
        testSite.Project_Protocol__c = testProtocol.Id;
        testSite.Site_ID__c = '010';
        testSite.Name = 'testsite1';
        insert testSite;
        
        testcountry.Name = 'ALBANIA';
        insert testcountry;
        
        testProtocolCountry.Name = 'Netherlands';
        testProtocolCountry.Client_Protocol__c = testProtocol.id;
        testProtocolCountry.Region__c = testcountry.id;
        insert testProtocolCountry;
                
        testtemplate.RecordTypeId = Templateid;
        testtemplate.name='Test Template';
        testtemplate.PRA_Status__c = 'Budget Template Initiated';   
        testtemplate.Contract_Type__c='Master CTA';   
        testtemplate.protocol_country__c=testProtocolCountry.id;  
        testtemplate.First_Draft_Sent_Date__c = Date.newInstance(2015, 01, 31);
        testtemplate.Planned_Final_Exec_Date__c = Date.newInstance(2015, 12, 31);
        testlist.add(testtemplate);
        insert testlist;
        
        ApexPages.StandardsetController Cont = new ApexPages.StandardsetController(testlist);
        ApexPages.CurrentPage().GetParameters().put('Id',testProtocolCountry.id);
        ApexPages.CurrentPage().GetParameters().put('RecordType',Templateid);
        CM_AgreementForProtocolCountry testController = new CM_AgreementForProtocolCountry(Cont);
        
        ApexPages.StandardsetController Cont1 = new ApexPages.StandardsetController(testlist);
        ApexPages.CurrentPage().GetParameters().put('Id',testProtocol.id);
        ApexPages.CurrentPage().GetParameters().put('RecordType',Templateid);
        CM_AgreementForProtocolCountry testController1 = new CM_AgreementForProtocolCountry(Cont1);
        
        test.startTest();
        testController.saveAgreement();
        testController1.saveAgreement();
        test.stoptest();
                    
    }
}