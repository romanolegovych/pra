@isTest
private class BDT_CharacteristicsTest {
	public static Client_Project__c firstProject {get;set;}
	public static List<Study__c> studyList {get;set;}
	public static List<Population__c> populationList {get;set;}
	public static List<Business_Unit__c> myBUList  {get;set;}
	public static List<Site__c> mySiteList  {get;set;}
	
	static void init(){
		firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;
		
		myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert myBUList;
		
		mySiteList = BDT_TestDataUtils.buildSite(myBUList);
		insert mySiteList;
		
		studyList = BDT_TestDataUtils.buildStudies(firstProject);
		studyList[0].Business_Unit__c = myBUList[0].Id; 
		studyList[1].Business_Unit__c = myBUList[1].Id; 
		studyList[2].Business_Unit__c = myBUList[2].Id;
		studyList[3].Business_Unit__c = myBUList[3].Id; 
		studyList[4].Business_Unit__c = myBUList[4].Id;
		insert studyList;
		
		populationList = BDT_TestDataUtils.buildPopulation(firstProject.id, 10);
		insert populationList;
				
		BDT_Utils.setPreference('SelectedProject', firstProject.id);
		BDT_Utils.setPreference('SelectedStudies',studyList[0].id);
		BDT_Utils.setPreference('populationId',populationList[0].id);
	}
	
	static testMethod void renderPopulationPath() { 
		init();
		
		BDT_Characteristics mycharacteristicsList = new BDT_Characteristics();		
		System.assertEquals(10, mycharacteristicsList.populationList.size());		
	}
	
	static testmethod void testStudySiteAssignments(){
		init();
		List<ProjectSite__c> projectSites = new List<ProjectSite__c>();
		projectSites.add(new ProjectSite__c(Client_Project__c = firstProject.Id, Site__c = mySiteList[0].Id  ));
		upsert projectSites;
		
		BDT_Characteristics a = new BDT_Characteristics();
		
		a.buildStudySiteAssignments();
		a.saveStudySiteAssignments();
		
		system.assertNotEquals(null, a.edit() );
		system.assertNotEquals(null, a.cancel() );
		
		
	}

	static testmethod void testSubcontractorActivities(){
		init();
		BDT_Characteristics c = new BDT_Characteristics();
		
		system.assertNotEquals(null, c.assignSubcontractors());
		
		Subcontractor__c sc = new Subcontractor__c(Name='TestSc');
		insert sc;
		
		SubcontractorActivity__c sca = new SubcontractorActivity__c(Subcontractor__c=sc.Id, Name='TestSCa');
		insert sca;
		
		ProjectSubcontractorActivity__c psca = new ProjectSubcontractorActivity__c( ClientProject__c=firstProject.Id, SubcontractorActivity__c = sca.Id );
		insert psca;
		
		List<SubcontractorActivityStudyAssignment__c> salsa = new List<SubcontractorActivityStudyAssignment__c>();
		
		for( Study__c st: studyList ){
			salsa.add(new SubcontractorActivityStudyAssignment__c(ProjectSubcontractorActivity__c = psca.Id, Study__c = st.Id));			
		}
		
		system.assertNotEquals(null, c.subcontractorAssignmentList );
		
		c.buildSubcontractorAssignments();
		
		system.assertNotEquals(null, c.selectedSASA);
		
		c.saveSubcontractorAssignments();
		
		
	}
	
	static testMethod void BDT_PopulationStudyAssignment_TriggerTest () {
		
		// create a project, study and population
		init();
		
		// assign population to study
		
		List<Population__c> pList = [select id from Population__c limit 5];
		Study__c s = [select id,Project__c from Study__c limit 1];
		
		List<PopulationStudyAssignment__c> psaList = new List<PopulationStudyAssignment__c>();
		For (Population__c p : pList) {
			psaList.add(new PopulationStudyAssignment__c(
				population__c = p.id,
				study__c = s.id
			));
		}
		insert psaList;
		
		// create a design and assign it to the project
		ClinicalDesign__c cd = new ClinicalDesign__c(
			Design_Name__c = 'Test Design',
			Project__c = s.Project__c
		);
		insert cd;
		
		ClinicalDesignStudyAssignment__c cdsa = new ClinicalDesignStudyAssignment__c(
			Study__c = s.id,
			ClinicalDesign__c = cd.id
		);
		insert cdsa;
		
		list<StudyDesignPopulationAssignment__c> sdpaList = new list<StudyDesignPopulationAssignment__c>();
		for (PopulationStudyAssignment__c psa:psaList){
			sdpaList.add(new StudyDesignPopulationAssignment__c(
				PopulationStudyAssignment__c = psa.id,
				ClinicalDesignStudyAssignment__c = cdsa.id
			));
		}
		insert sdpaList;
		
		// remove the populationStudyAssignment and verify that the studyDesignPopulationAssignments are removed
		delete psaList;
		
		list<StudyDesignPopulationAssignment__c> sdpaListAfter = [select id 
				from StudyDesignPopulationAssignment__c
				where ClinicalDesignStudyAssignment__c = :cdsa.id];
		system.assertEquals (0,sdpaListAfter.size());
		
	}
	

}