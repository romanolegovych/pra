/** Implements the test for the Selector Layer of the object FinancialDocumentHistory__c
 * @author	Dimitrios Sgourdos
 * @version	17-Jan-2014
 */
@isTest
private class FinancialDocumentHistoryDataAccessorTest {
	
	// Global variables
	private static FinancialDocument__c financialDocument;
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	17-Jan-2014
	 */
	static void init(){
		// Create project
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001', 'BDT Project First Experiments');
		insert currentProject;
		
		// Create financial document
		financialDocument = new FinancialDocument__c();
		financialDocument.DocumentType__c	= 'Proposal';
		financialDocument.Client_Project__c	= currentProject.Id;
		insert financialDocument;
	}
	
	
	/** Test the function getFinancialDocumentHistoryByProcessStep
	 * @author	Dimitrios Sgourdos
	 * @version	17-Jan-2014
	 */
	static testMethod void getFinancialDocumentHistoryByProcessStepTest() {
		// Create data
		init();
		
		List<FinancialDocumentHistory__c> sourceList = new List<FinancialDocumentHistory__c>();
		sourceList.add( new FinancialDocumentHistory__c(
												FinancialDocument__c = financialDocument.Id,
												ProcessStep__c = FinancialDocumentHistoryDataAccessor.APPROVAL_PROCESS,
												ChangeDescription__c = 'B, First in query')
					);
		sourceList.add( new FinancialDocumentHistory__c(
												FinancialDocument__c = financialDocument.Id,
												ProcessStep__c = FinancialDocumentHistoryDataAccessor.CONTENT_PROCESS,
												ChangeDescription__c = 'A, Second in query')
					);
		insert sourceList;
		
		String errorMessage = 'Error in reading the financial document history from the system';
		
		// Check the function without process step selected and descending order in change order
		List<FinancialDocumentHistory__c> results =	FinancialDocumentHistoryDataAccessor.getFinancialDocumentHistoryByProcessStep(
																								financialDocument.Id,
																								NULL,
																								'ChangeDescription__c',
																								true);
		system.assertEquals(results.size(), 2, errorMessage);
		system.assertEquals(results[0].Id, sourceList[0].Id, errorMessage);
		system.assertEquals(results[1].Id, sourceList[1].Id, errorMessage);
		
		// Check the function with APPROVAL_PROCESS selected
		results =	FinancialDocumentHistoryDataAccessor.getFinancialDocumentHistoryByProcessStep(
																financialDocument.Id,
																FinancialDocumentHistoryDataAccessor.APPROVAL_PROCESS,
																'ChangeDescription__c',
																true);
		system.assertEquals(results.size(), 1, errorMessage);
		system.assertEquals(results[0].Id, sourceList[0].Id, errorMessage);
	}
}