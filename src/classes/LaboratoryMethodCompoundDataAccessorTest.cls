/** Implements the test for the Selector Layer of the object LaboratoryMethodCompound__c
 * @author	Dimitrios Sgourdos
 * @version	24-Oct-2013
 */
@isTest
private class LaboratoryMethodCompoundDataAccessorTest {
	
	// Global variables
	private static List<Study__c> 						studiesList;
	private static List<LaboratoryMethod__c>			lmList;
	private static List<LaboratoryMethodCompound__c> 	compoundList;
	
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static void init(){
		// Create project
		Client_Project__c firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		
		// Create studies
		studiesList = BDT_TestDataUtils.buildStudies(firstProject,2);
		insert studiesList;
		
		// Create Laboratory Methods
		lmList = new List<LaboratoryMethod__c>();
		lmList.add(new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
											Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood',
											Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle'));
		lmList.add(new LaboratoryMethod__c (Name='PRA-US-SML-0001', AnalyticalTechnique__c='ELISA', AntiCoagulant__c='N/A',
											Department__c='SML', Detection__c='N/A', Location__c='PRA-US', Matrix__c='Breast milk',
											Proprietary__c='N', ShowOnMethodList__c='Y', Species__c='Human'));
		insert lmList; 
		
		// Create Laboratory Method Compounds
		compoundList = new List<LaboratoryMethodCompound__c>();
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[0], 'midazolam') );
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[0], '4-OH') );
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[1], '1-OH') );
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[1], 'new midazolam') );
		insert compoundList;
	}
	
	
	/** Test the function getUsedByUserStudies
	 * @author	Dimitrios Sgourdos
	 * @version	23-Oct-2013
	 */
	static testMethod void getUsedByUserStudiesTest() {
		// Initialize data
		List<LaboratoryMethodCompound__c> retrievedCompoundsList = new List<LaboratoryMethodCompound__c>();
		init();
		
		// Check that there is no analysis inserted
		retrievedCompoundsList =  LaboratoryMethodCompoundDataAccessor.getUsedByUserStudies(studiesList);
		system.assertEquals(retrievedCompoundsList.size(), 0);
		
		// Create Analysis associated with a Study thet we will not query, to check that we don't retrieve compounds
		List<LaboratoryAnalysis__c> tmpLabAnalysisList = new List<LaboratoryAnalysis__c>();
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[0], false, false, false));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[1], studiesList[0], true,  false, false));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[2], studiesList[0], false, true,  false));
		tmpLabAnalysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[3], studiesList[0], false, false, true));
		insert tmpLabAnalysisList;
		
		List<Study__c> tmpStudiesList = new List<Study__c>();
		tmpStudiesList.addAll(studiesList);
		tmpStudiesList.remove(0);
		
		retrievedCompoundsList =  LaboratoryMethodCompoundDataAccessor.getUsedByUserStudies(tmpStudiesList);
		system.assertEquals(retrievedCompoundsList.size(), 0);
		
		// Check that we retrieve the compounds if we query with the associated study (4 from before - The one that we will insert below is assigned with an already assigned compound)
		LaboratoryAnalysis__c tmpLabAnalysisItem = LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[3], studiesList[1], false, true, true);
		insert tmpLabAnalysisItem;
		
		retrievedCompoundsList =  LaboratoryMethodCompoundDataAccessor.getUsedByUserStudies(studiesList);
		system.assertEquals(retrievedCompoundsList.size(), 4);
	}
	
	
	/** Test the function getByLabMethod
	 * @author	Dimitrios Sgourdos
	 * @version	23-Oct-2013
	 */
	static testMethod void getByLabMethodTest() {
		// Initialize Data
		List<LaboratoryMethodCompound__c> retrievedCompoundsList = new List<LaboratoryMethodCompound__c>();
		init();
		
		// There are two copounds with lmList[0]
		retrievedCompoundsList = LaboratoryMethodCompoundDataAccessor.getByLabMethod(lmList[0].Id, studiesList);
		system.assertEquals(retrievedCompoundsList.size(), 2);
	}
}