public with sharing class BDT_FinancialCategoryTotalUtils {

	public class Price {
		public decimal amount;
		public string  amountCurrency;
		public decimal delta;
		public decimal originalAmount;
	}

	public class fctWrapper {
		public FinancialCategoryTotal__c fct;
		public list<FinancialCatTotPrice__c> fctp;
	} 

	public Set<ID> addID (Set<ID> IDset, ID IDvalue){
		if (IDvalue!=null) {
			IDset.add(IDvalue);
		}
		return IDset;
	}

	public static void refreshFinancialCategoryTotal(ID fdID){

		if (fdID != null) {

			// build the like clause for the SOQL
			String queryLike = '%' + fdID + '%';

			// get all the studyservicepricing records for this document
			List<AggregateResult> arList = [
					Select StudyService__r.study__c 		studyid
						, Service__r.ServiceCategory__c 	servicecategoryid
						, RateCardCurrency__c 				ratecardcurrencyname
						, sum(s.CalculatedTotalPrice__c) 	totalPrice
					From StudyServicePricing__c s
					where ratecardclass__c != null
					and (ApprovedFinancialDocument__c = :fdID
						or (ApprovedFinancialDocument__c = null 
							and FinancialDocumentIDs__c like :queryLike))
					group by rollup (RateCardCurrency__c
									, StudyService__r.study__c
									, Service__r.ServiceCategory__c)
				];

			// read current conversion and delta map
			Map <String, FinancialCategoryTotal__c> fctMap = readCurrent(fdID);

			// read parents map
			Map <ID,List<ID>> categoryParents = readCategoryParents();

			Map<String, fctWrapper> NEWfctMap = new Map<String, fctWrapper>();

			Map<string,price> FinancialDocPriceMap = new Map<string,price>();
			String FDKey;
			String TargetCurrency;
			Price p;

			For (AggregateResult ar:arList) {

				Decimal finalPrice,totalPrice;

				/* if the aggregate result has a studyid, a servicecategoryid 
					and a ratecardcurrency then the result describes the 
					financial document category total value */
				//if (ar.get('studyid')!=null && ar.get('servicecategoryid')!=null && ar.get('ratecardcurrencyname')!=null) {
				if (ar.get('studyid')!=null && ar.get('servicecategoryid')!=null ) {
					String Key = ''+ar.get('studyid')+':'+ar.get('servicecategoryid');

					// get or create fct
					fctWrapper fctWrpr = new fctWrapper();
					if (NEWfctMap.containsKey(Key)) {
						fctWrpr = NEWfctMap.remove(Key);
					} else {
						fctWrpr.fct = new FinancialCategoryTotal__c ();
						fctWrpr.fct.Study__c = stringArrCast(ar.get('studyid'));
						fctWrpr.fct.ServiceCategory__c = stringArrCast(ar.get('servicecategoryid'));
						fctWrpr.fct.FinancialDocument__c = fdID;
						fctWrpr.fctp = new List<FinancialCatTotPrice__c>();
					}

					// update existing fctp if present
					Boolean stored = false;
					for (FinancialCatTotPrice__c fctp : fctWrpr.fctp){
						if (fctp.OriginalCurrency__c == stringArrCast(ar.get('ratecardcurrencyname'))) {
							fctp = incrementFCTP(fctp,decimalArrCast(ar.get('totalPrice')));
							system.debug('existing fctp' + fctp);
							stored = true;
							totalPrice = fctp.TotalPrice__c;
							finalPrice = fctp.FinalPrice__c;
							TargetCurrency = fctp.TargetCurrency__c; // for total price
						}
					}
					// check if existing was updated
					if (!stored) {
						// create new
						FinancialCatTotPrice__c fctp = new FinancialCatTotPrice__c();

						fctp.OriginalCurrency__c = stringArrCast(ar.get('ratecardcurrencyname'));

						// if possible recover delta, targetcurrency and conversionrate from previous record
						if (fctMap.containsKey(Key)) {
							FinancialCategoryTotal__c fctOld = fctMap.get(Key);
							for (FinancialCatTotPrice__c fctpOld : fctOld.FinancialCatTotPrices__r){
								if (fctpOld.OriginalCurrency__c == stringArrCast(ar.get('ratecardcurrencyname'))) {
									fctp.TargetCurrency__c = fctpOld.TargetCurrency__c;
									fctp.PriceDelta__c     = fctpOld.PriceDelta__c;
									fctp.ConversionRate__c = fctpOld.ConversionRate__c;
								}
							}
						}

						if (fctp.TargetCurrency__c == null) {
							fctp.TargetCurrency__c = fctp.OriginalCurrency__c;
						}

						fctp = incrementFCTP(fctp,decimalArrCast(ar.get('totalPrice')));

						totalPrice = fctp.TotalPrice__c;
						finalPrice = fctp.FinalPrice__c;
						TargetCurrency = fctp.TargetCurrency__c; // for total price
						fctWrpr.fctp.add(fctp);
					}
					// value has been updated, now add it to list again
					NEWfctMap.put(key,fctWrpr);

					// START update document price
					FDKey = '' + TargetCurrency;
					if (FinancialDocPriceMap.containsKey(FDKey)) {
						p = FinancialDocPriceMap.remove(FDKey);
					} else {
						p = new Price();
						p.amount = 0;
						p.amountCurrency = TargetCurrency;
						p.delta = 0;
						p.originalAmount = 0;
					}
					p.amount += finalPrice;
					p.originalAmount += totalPrice;
					p.delta = (p.originalAmount!=null && p.originalAmount>0)?(p.amount/p.originalAmount - 1) * 100:null;
					FinancialDocPriceMap.put(FDKey,p);
					// END update document price

					// add values for any parent categories
					if (ar.get('servicecategoryid') != NULL) {
						for (ID parent: categoryParents.get(stringArrCast(ar.get('servicecategoryid')))){
							Key = ''+ar.get('studyid')+':'+parent;
							fctWrpr = new fctWrapper();
							if (NEWfctMap.containsKey(Key)) {
								fctWrpr = NEWfctMap.remove(Key);
							} else {
								fctWrpr.fct = new FinancialCategoryTotal__c ();
								fctWrpr.fct.Study__c = stringArrCast(ar.get('studyid'));
								fctWrpr.fct.ServiceCategory__c = parent; 
								fctWrpr.fct.FinancialDocument__c = fdID;
								fctWrpr.fctp = new List<FinancialCatTotPrice__c>();
							}
							// update existing fctp if present
							stored = false;
							for (FinancialCatTotPrice__c fctp : fctWrpr.fctp){
								if (fctp.OriginalCurrency__c == stringArrCast(ar.get('ratecardcurrencyname'))) {
									fctp = incrementFCTPandCalculateDeltaAfterConversion(fctp,totalPrice,finalPrice);
									stored = true;
								}
							}
							// check if existing was updated
							if (!stored) {
								// create new
								FinancialCatTotPrice__c fctp = new FinancialCatTotPrice__c();
	
								fctp.OriginalCurrency__c = stringArrCast(ar.get('ratecardcurrencyname'));
	
								// if possible recover delta, targetcurrency and conversionrate from previous record
								if (fctMap.containsKey(Key)) {
									FinancialCategoryTotal__c fctOld = fctMap.get(Key);
									for (FinancialCatTotPrice__c fctpOld : fctOld.FinancialCatTotPrices__r){
										if (fctpOld.OriginalCurrency__c == stringArrCast(ar.get('ratecardcurrencyname'))) {
											fctp.TargetCurrency__c = fctpOld.TargetCurrency__c;
											fctp.PriceDelta__c     = fctpOld.PriceDelta__c;
											fctp.ConversionRate__c = fctpOld.ConversionRate__c;
										}
									}
								}
	
								if (fctp.TargetCurrency__c == null) {
									fctp.TargetCurrency__c = fctp.OriginalCurrency__c;
								}
	
								fctp = incrementFCTPandCalculateDeltaAfterConversion(fctp,totalPrice,finalPrice);
	
								fctWrpr.fctp.add(fctp);
							}
							// value has been updated add it to list again
							NEWfctMap.put(key,fctWrpr);
						}
					}
				}
			} 

			/* clean out everything for this document*/
			delete [select id from FinancialCategoryTotal__c where FinancialDocument__c = :fdID];

			List<FinancialCategoryTotal__c> fct = new list<FinancialCategoryTotal__c>();
			for (fctWrapper a:NEWfctMap.values()){
				fct.add(a.fct);
			}
			upsert fct;
			List<FinancialCatTotPrice__c> fctp = new List<FinancialCatTotPrice__c>();
			for (Integer i = 0; i < fct.size();i++){
				fctWrapper a = NEWfctMap.values()[i];
				for (FinancialCatTotPrice__c b : a.fctp){
					b.FinancialCategoryTotal__c = a.fct.id;
					fctp.add(b);
				}
			}
			upsert fctp;

			FinancialDocument__c fd = [select id, TotalValue__c from FinancialDocument__c where id = :fdID];
			fd.TotalValue__c = JSON.serialize(FinancialDocPriceMap.values());
			upsert fd;

		}

	}
	
	public static Decimal decimalArrCast (Object value){
		try {
			return decimal.valueOf(string.valueOf(value));
		}Catch (Exception e) {
			return decimal.valueOf('0');
		}
	}
	
	public static String stringArrCast (Object value){
		try {
			return string.valueOf(value);
		} catch (Exception e) { return '';}
		
	}
	
	public static Map <String, FinancialCategoryTotal__c> readCurrent(ID fdID) {
		Map <String, FinancialCategoryTotal__c> fctMap = new Map <String, FinancialCategoryTotal__c> ();
		
		For (FinancialCategoryTotal__c fct : [
			Select Study__c
				, ServiceCategory__c
				, Id
				, (Select Id
					, FinancialCategoryTotal__c
					, TotalPrice__c
					, FinalPrice__c
					, OriginalCurrency__c
					, TargetCurrency__c
					, ConversionRate__c
					, PriceDelta__c 
				  From FinancialCatTotPrices__r)
			From FinancialCategoryTotal__c
			where FinancialDocument__c = :fdID
			]) {
			
			fctMap.put(''+fct.Study__c+':'+fct.ServiceCategory__c,fct);
				
		}
		return fctMap;
	}
	
	public static FinancialCatTotPrice__c incrementFCTP(FinancialCatTotPrice__c fctp, Decimal amount) {
		
		system.debug('incrementFCTP');
		system.debug(fctp);
		system.debug(amount);
		
		//convert amount to desired currency
		amount = (fctp.ConversionRate__c != null) ? amount * fctp.ConversionRate__c : amount;

		fctp.PriceDelta__c = (fctp.PriceDelta__c==null) ? 0 : fctp.PriceDelta__c;
		fctp.TotalPrice__c = (fctp.TotalPrice__c==null) ? amount : fctp.TotalPrice__c + amount;
		fctp.FinalPrice__c = (fctp.FinalPrice__c==null) ? amount * (1+fctp.PriceDelta__c/100) : fctp.FinalPrice__c + amount * (1+fctp.PriceDelta__c/100);
		
		system.debug('result' + fctp);
		
		return fctp;
	}
	
	public static FinancialCatTotPrice__c incrementFCTPandCalculateDeltaAfterConversion(FinancialCatTotPrice__c fctp, Decimal totalPrice, Decimal finalPrice) {
		
		/* This method is to update total and final prices with converted input. And to calculate the resulting delta */
	
		fctp.TotalPrice__c = (fctp.TotalPrice__c==null) ? totalPrice : fctp.TotalPrice__c + totalPrice;
		fctp.FinalPrice__c = (fctp.FinalPrice__c==null) ? finalPrice : fctp.FinalPrice__c + finalPrice;
		
		try{
			fctp.PriceDelta__c = (fctp.FinalPrice__c / fctp.TotalPrice__c - 1) * 100;
		} catch (Exception e) {
			// catch division by zero
			fctp.PriceDelta__c = null;
		}
		
		return fctp;
	}
	
	public static Map <ID,List<ID>> readCategoryParents() {

		Map <ID,List<ID>> categoryParentsMap = new Map <ID,List<ID>>();
		Map<ID,ServiceCategory__c> scMap = new Map<ID,ServiceCategory__c>(
										[select id, parentcategory__c 
										from ServiceCategory__c
										where bdtdeleted__c = false]);
										
		for (ServiceCategory__c sc:scMap.values()) {
			
			if (scMap.containsKey(sc.id)) {
				List<ID> parentIDs = new List<ID>();
				String parentCategory = scMap.get(sc.id).parentcategory__c;
				while(parentCategory != null) {
					parentIDs.add(parentCategory);
					parentCategory = scMap.get(parentCategory).parentcategory__c;
				}
				categoryParentsMap.put(sc.id,parentIDs);
			}
		}
		return categoryParentsMap;
	} 

}