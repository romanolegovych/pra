/* Test Class For the Test Coverage and the Assertions
*/
@isTest
private class RM_ClientProjecthandlerTest {
    static testMethod void clientTrigger()
    {    
         Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
         
         User u1 = new User(Alias = 'newU', Email='newuser@testorg.com',firstname='t1', 
         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = p.Id,EmployeeNumber='12345', 
         TimeZoneSidKey='America/Los_Angeles', UserName='newuse999r@testorg.com');
         insert u1;
          
         User u2 = new User(Alias = 'stand', Email='standarduser@testorg.com', firstname='t2',
         EmailEncodingKey='UTF-8', LastName='Testing2', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = p.Id, EmployeeNumber='98765',
         TimeZoneSidKey='America/Los_Angeles', UserName='standarduser999@testorg.com');  
         insert u2;
         
         List<String> Names = new List<String>(); 
         names.add('Abc');
         names.add('XYZ');
         names.add('PQR');
         names.add('MNO');
         names.add('Hardeep');
         
         //for(integer i=0; i<5; i++)
         //{
             WFM_Client__c wfmClientObj = new WFM_Client__c();
             wfmClientObj.Name = Names.get(0);
             wfmClientObj.Client_Unique_Key__c = String.ValueOf(0);
             insert wfmClientObj;
         
             WFM_Contract__c wfmContractObj = new WFM_Contract__c();
             wfmContractObj.Name = Names.get(0);
             wfmContractObj.Client_ID__c = wfmClientObj.Id;
             wfmContractObj.Contract_Unique_Key__c = String.ValueOf(0);
             insert wfmContractObj;  
             
             WFM_Project__c wfmProjectObj = new WFM_Project__c();
             wfmProjectObj.Name = 'ABC';
             wfmProjectObj.Contract_ID__c = wfmContractObj.Id;
             wfmProjectObj.Project_Unique_Key__c = String.ValueOf(0); 
             wfmProjectObj.Global_Project_Manager__c = u1.FirstName+ ' '+u1.LastName;
             system.debug('**********name'+wfmProjectObj.Global_Project_Manager__c);
             wfmProjectObj.Director_Project_Delivery__c = u1.FirstName+' '+u2.LastName;                       
             insert wfmProjectObj;
             
             Client_project__c clientProject = new Client_Project__c();
             clientProject.Name='ABC';
             clientProject.PRA_Project_Id__c ='100100';
             clientProject.Director_of_Project_Delivery__c = u1.Id ;
             clientProject.Global_Project_Manager_Director__c = u2.Id ;
             insert clientProject;
                                 
             clientProject.Director_of_Project_Delivery__c = u2.Id ;
             clientProject.Global_Project_Manager_Director__c = u1.Id;
             
             system.debug('**********************clientProject****'+clientProject);
             update clientProject;
             
             
             Client_Project__c cp = [Select Id,Global_Project_Manager_Director__c,Director_of_Project_Delivery__c,PRA_Project_Id__c,
                                    Director_of_Project_Delivery__r.name,Global_Project_Manager_Director__r.name,Name
                                    from Client_Project__c where Id =: clientProject.Id ];
         	 
             WFM_Project__c wfm =[select Id,Name,Global_Project_Manager__c,Director_Project_Delivery__c,
             			        DPD_Employee_ID__c,GPM_Employee_ID__c from WFM_Project__c where Name =:clientProject.Name ] ;
             
             
            
            system.assertEquals(cp.Global_Project_Manager_Director__r.name,wfm.Global_Project_Manager__c, 'Names are not updated');
            system.assertEquals(cp.Director_of_Project_Delivery__r.name, wfm.Director_Project_Delivery__c,'Names are not updated'); 
            system.assertnotEquals(wfm.DPD_Employee_ID__c,u1.EmployeeNumber); 
            system.assertnotEquals(wfm.GPM_Employee_ID__c, u2.EmployeeNumber); 
                         
    }
}