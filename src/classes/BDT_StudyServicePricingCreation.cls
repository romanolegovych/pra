public with sharing class BDT_StudyServicePricingCreation {
	public map<string, string> financialDocumentStatus = new map<string, string>();
	public map<string, StudyService__c> insertedStSMap = new map<string, StudyService__c>();
    public map<string,list<ratecardclass__c>> rccMap = new map<string,list<ratecardclass__c>>();
    public map<string,Id> buMap = new map<string,Id>();
    public map<string,Id> projectServicestoServiceMap = new map<string,Id>();
        
	public void updateStudyServicePricing( list<StudyService__c> insertedStudyServices, list<StudyService__c> oldStudyServices ){
		list<studyservicePricing__c> sspToCreate = new list<studyservicePricing__c>();
		map<string, StudyServicePricing__c> sspMap = new map<string, StudyServicePricing__c>();
		map<string, studyservice__c> oldStsMap = new map<string, studyService__c>();
		set<Id> studyIds = new set<Id>();
		set<ID> projectServiceIds = new set<Id>();
		
		for(StudyService__c sts: insertedStudyServices){
			studyIds.add(sts.study__c);
			projectServiceIds.add(sts.ProjectService__c);
		}
		map<id,id> projectServiceService = new map<id,id>();
		for (ProjectService__c ps:[select id, service__c from ProjectService__c where id in :projectServiceIds]) {
			projectServiceService.put(ps.id,ps.service__c);
		}
		
		rccMap = buildRCCMap(studyIds, ProjectserviceIds );
		buildBUMap(studyIds);
		buildProjectServicesMap(projectServiceIds);
		
		if(!oldStudyServices.isEmpty()){
			for(StudyService__c sts: oldStudyServices){
				oldStsMap.put(sts.id, sts);
			} 
		}
		
		list<studyservicePricing__c> existingSSP = [SELECT CalculatedDiffToRateCard__c
													, CalculatedTotalPrice__c
													, NumberOfUnits__c
													, RateCardClass__c
													, RateCardCurrency__c
													, RateCardPrice__c
													, Id
													, StudyService__c
													, Service__c
													, Name
													, UnitPriceNegotiated__c
													, UnitPricePRAProposal__c
													, ApprovedFinancialDocument__c
													FROM StudyServicePricing__c 
													where StudyService__c in :insertedStudyServices
													and ApprovedFinancialDocument__c = null];
		
		for(StudyServicePricing__c ssp: existingSSP){
			sspMap.put(ssp.StudyService__c, ssp);
		}
		for(StudyService__c sts: insertedStudyServices){
			if(sspMap.containsKey(sts.id)){
				//update
				StudyServicePricing__c ssp = sspMap.get(sts.id);
				ssp.NumberOfUnits__c = sts.NumberOfUnits__c;
				if( String.IsBlank(ssp.RateCardClass__c) ) {
					try{
						RateCardClass__c rcc = getRateCardClass(sts);
						ssp.RateCardClass__c = rcc.Id;
						ssp.RateCardPrice__c = rcc.UnitPrice__c;
						ssp.RateCardCurrency__c = rcc.Currency__r.Name;
					}catch(exception e){
						//probably no ratecard for the service/business unit combination
					}
				}
				sspToCreate.add(ssp);
			}else{
				//create new
				decimal UnitsDifference =0;
				if(oldStsMap.containsKey(sts.Id)){
					UnitsDifference = sts.NumberOfUnits__c - (oldStsMap.get(sts.id).NumberOfUnits__c);
				}else{
					UnitsDifference = sts.NumberOfUnits__c;
				}
				StudyServicePricing__c ssp = new StudyServicePricing__c( StudyService__c=sts.id, NumberOfUnits__c = UnitsDifference );
				try{
					RateCardClass__c rcc 	= getRateCardClass(sts);
					ssp.RateCardClass__c	= rcc.Id;
					ssp.RateCardPrice__c 	= rcc.UnitPrice__c;
					ssp.RateCardCurrency__c = rcc.Ratecard__r.Business_Unit__r.Currency__r.Name;
				}catch(exception e){
					//probably no ratecard for the service/business unit combination
				}
				ssp.Service__c = projectServiceService.get(sts.ProjectService__c);
				sspToCreate.add(ssp);
			}
		}
		upsert sspToCreate;
	}
	public RateCardClass__c getRateCardClass(StudyService__c sts){
		string searchString = buMap.get(sts.study__c)+'-'+projectServicestoServiceMap.get(sts.ProjectService__c);
		
		list<RateCardClass__c> rccList = RCCMap.get(searchString);
		try{
			RateCardClass__c RCCresult;
			
			for (RateCardClass__c RCC : rccList ) {
				if (RCCresult == null) {
					RCCresult = RCC;
				} else if (RCC.NumericClass__c == true && RCC.MaximumUnits__c >=sts.NumberOfUnits__c) {
					RCCresult = RCC;
				} else if (RCC.NumericClass__c == true && RCC.MaximumUnits__c < sts.NumberOfUnits__c) {
					break;
				}
			}
			
			return RCCresult;
		}catch(exception e){
			return null;
		}
	}

	public map<string,list<RateCardClass__c>> buildRCCMap(set<ID> studyIds, set<ID> ProjectServiceIds){
		map<string,list<RateCardClass__c>> rccMap = new map<string, list<RateCardClass__c>>();

		try {
		
			List<RateCard__c> rcList = [Select UnitDescription__c,
											Service__c, 
											Id, 
											Business_Unit__c,
											(Select Id,
												Name,
												RateCard__c,
												RateCard__r.Business_Unit__r.Currency__r.name,
												MaximumUnits__c,
												NumericClass__c,
												UnitPrice__c
											From 	RateCardClasses__r
											where 	BDTDeleted__c != true
											order by MaximumUnits__c, UnitPrice__c) 
										From RateCard__c
										where BDTDeleted__c != true
										and Service__c in (select service__c
														   from   projectservice__c
														   where  id in :ProjectServiceIds
												   			and   BDTDeleted__c != true)
										and Business_Unit__c in (select business_unit__c
															    	from study__c
																    where id in :studyIds
																    and BDTDeleted__c != true)];
			for(ratecard__c rc: rcList){
				rccMap.put(rc.Business_Unit__c+'-'+rc.Service__c, rc.RateCardClasses__r);
			}
		} catch (Queryexception e){
			rccMap=  new map<string, list<RateCardClass__c>>();
		}
		return rccMap;
	}
	public void buildBUMap(set<ID> studyIDs){
				
		try {
			List<Study__c> studiesList = [ Select Business_Unit__c 
											From Study__c 
											where id in :studyIDs];
											
			for(Study__c st: studiesList){
				BUMap.put(st.Id,st.Business_Unit__c);
			}
			
		} catch (Exception e) {
			BUMap = new map<string,Id>();
		}		
	}
	public void buildProjectServicesMap(set<id> projectServiceIds){
		list<ProjectService__c> psList = [select id,Service__c
											from projectService__c
											where id in: projectServiceIds];
		
		for(ProjectService__c ps: psList){
			projectServicestoServiceMap.put(ps.Id, ps.Service__c);
		}
	}
}