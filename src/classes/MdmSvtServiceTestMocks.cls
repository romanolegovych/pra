/**
 * Test mocks for MDM Svt Service classes
 */
@isTest
public class MdmSvtServiceTestMocks implements WebServiceMock {
	
	public boolean shouldCreateError {get;set;}
	
	private static final String GET_SVT_ECRF_MAPPING_BY_PROT_PRAID_TYPE = 'MdmSvtService.getSvtEcrfMappingByProtocolPraIdResponse';
	private static final String GET_SVTS_ECRFS_BY_PROT_PRAID_TYPE = 'MdmSvtService.getSvtsEcrfsFromProtocolPraIdPraIdResponse';
	private static final String GET_SVTS_ECRFS_MAPPINGS_TYPE = 'MdmSvtService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse';
	private static final String GET_SVTS_ECRFS_USV_FORMS_MAPPINGS_TYPE = 'MdmSvtService.getSvtsEcrfsUsvFormsAndMappingsFromProtocolPraIdResponse';
	private static final String GET_SVTS_BY_PROTOCOL_TYPE = 'MdmSvtService.getSvtVOsByProtocolResponse';
	private static final String SAVE_SVT_ECRF_MAPPING_FROM_VO_TYPE = 'MdmSvtService.saveSvtEcrfMappingFromVOResponse';
	private static final String SAVE_SVT_ECRF_MAPPINGS_FROM_VOS_TYPE = 'MdmSvtService.saveSvtEcrfMappingFromVOsResponse';
	private static final String DELETE_SVT_ECRF_MAPPINGS_FROM_VOS_TYPE = 'MdmSvtService.deleteSvtEcrfMappingFromVOsResponse';
	private static final String SAVE_SVT_USV_FORM_MAPPINGS_FROM_VOS_TYPE = 'MdmSvtService.saveSvtUsvFormMappingsFromVOsResponse';
	private static final String DELETE_SVT_USV_FORM_MAPPINGS_FROM_VOS_TYPE = 'MdmSvtService.deleteSvtUsvFormMappingsFromVOsResponse';
	
	public MdmSvtServiceTestMocks(boolean doError) {
		this.shouldCreateError = doError;
		system.debug('----------- Mocking a MdmSvtService method request -----------------');
	}
	
	public void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
		String requestName, String responseNS, String responseName, String responseType) {
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		if (responseType.equals(GET_SVT_ECRF_MAPPING_BY_PROT_PRAID_TYPE)) {
			response.put('response_x', mockSvtEcrfMappingByProtPraIdResponse());
		} else if (responseType.equals(GET_SVTS_ECRFS_BY_PROT_PRAID_TYPE)) {
			response.put('response_x', mockSvtsEcrfsByProtPraIdResponse());
		} else if (responseType.equals(GET_SVTS_ECRFS_MAPPINGS_TYPE)) {
			response.put('response_x', mockSvtsEcrfsMappingsFromProtocolPraIdResponse());
		} else if (responseType.equals(GET_SVTS_ECRFS_USV_FORMS_MAPPINGS_TYPE)) {
			response.put('response_x', mockSvtsEcrfsUsvFormsMappingsFromProtocolPraIdResponse());
		} else if (responseType.equals(GET_SVTS_BY_PROTOCOL_TYPE)) {
			response.put('response_x', mockSvtVOsByProtocolResponse());
		} else if (responseType.equals(SAVE_SVT_ECRF_MAPPING_FROM_VO_TYPE)) {
			response.put('response_x', mockSaveSvtEcrfMappingFromVOResponse());
		} else if (responseType.equals(SAVE_SVT_ECRF_MAPPINGS_FROM_VOS_TYPE)) {
			response.put('response_x', mockSaveSvtEcrfMappingFromVOsResponse());
		} else if (responseType.equals(DELETE_SVT_ECRF_MAPPINGS_FROM_VOS_TYPE)) {
			response.put('response_x', mockDeleteSvtEcrfMappingFromVOsResponse());
		} else if (responseType.equals(SAVE_SVT_USV_FORM_MAPPINGS_FROM_VOS_TYPE)) {
			response.put('response_x', mockSaveSvtUsvFormMappingFromVOsResponse());
		} else if (responseType.equals(DELETE_SVT_USV_FORM_MAPPINGS_FROM_VOS_TYPE)) {
			response.put('response_x', mockDeleteSvtUsvFormMappingFromVOsResponse());
		}
		system.debug('----------------------------request------------------------' + request);
		system.debug('----------------------------response------------------------' + response);
	}
	
	/********************* Methods to build main response objects ********************************/
	
	private MdmSvtService.getSvtEcrfMappingByProtocolPraIdResponse mockSvtEcrfMappingByProtPraIdResponse() {
		MdmSvtService.getSvtEcrfMappingByProtocolPraIdResponse resp = new MdmSvtService.getSvtEcrfMappingByProtocolPraIdResponse();
		MdmSvtService.visitMappingResponse visitMappingResp = new MdmSvtService.visitMappingResponse();
		visitMappingResp.errors = null;
		visitMappingResp.statusCode = '0';
		visitMappingResp.ecrfMappingVO = createMockEcrfMappingVOList(5);
		resp.GetSvtEcrfMappingByProtPraIdResponse = visitMappingResp;
		return resp;
	}
	
	private MdmSvtService.getSvtsEcrfsFromProtocolPraIdResponse mockSvtsEcrfsByProtPraIdResponse() {
		MdmSvtService.getSvtsEcrfsFromProtocolPraIdResponse resp = new MdmSvtService.getSvtsEcrfsFromProtocolPraIdResponse();
		MdmSvtService.svtEcrfResponse svtEcrfResponse = new MdmSvtService.svtEcrfResponse();
		svtEcrfResponse.errors = null;
		svtEcrfResponse.statusCode = '0';
		svtEcrfResponse.svts = createMockSvtVOList(5);
		svtEcrfResponse.ecrfItems = createMockEcrfItemVOList(20);
		resp.GetSvtsEcrfsResponse = svtEcrfResponse;
		return resp;
	}
	
	private MdmSvtService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse mockSvtsEcrfsMappingsFromProtocolPraIdResponse() {
		MdmSvtService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse resp = new MdmSvtService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse();
		MdmSvtService.svtEcrfMappingResponse svtEcrfMappingResp = new MdmSvtService.svtEcrfMappingResponse();
		svtEcrfMappingResp.errors = null;
		svtEcrfMappingResp.statusCode = '0';
		svtEcrfMappingResp.svts = createMockSvtVOList(5);
		svtEcrfMappingResp.ecrfItems = createMockEcrfItemVOList(20);
		svtEcrfMappingResp.mappings = createMockEcrfMappingVOList(5);
		resp.GetSvtsEcrfsMappingsResponse = svtEcrfMappingResp;
		return resp;
	}
	
	private MdmSvtService.getSvtsEcrfsUsvFormsAndMappingsFromProtocolPraIdResponse mockSvtsEcrfsUsvFormsMappingsFromProtocolPraIdResponse() {
		MdmSvtService.getSvtsEcrfsUsvFormsAndMappingsFromProtocolPraIdResponse resp = new MdmSvtService.getSvtsEcrfsUsvFormsAndMappingsFromProtocolPraIdResponse();
		MdmSvtService.svtEcrfUsvFormMappingResponse svtEcrfFormMappingResp = new MdmSvtService.svtEcrfUsvFormMappingResponse();
		svtEcrfFormMappingResp.errors = null;
		svtEcrfFormMappingResp.statusCode = '0';
		Integer svtListSize = 5;
		svtEcrfFormMappingResp.svts = createMockSvtVOList(svtListSize);
		svtEcrfFormMappingResp.ecrfItems = createMockEcrfItemVOList(20);
		svtEcrfFormMappingResp.usvForms = createMockEcrfFormVOList(10);
		svtEcrfFormMappingResp.visitMappings = createMockEcrfMappingVOList(svtListSize);
		svtEcrfFormMappingResp.formMappings = createMockEcrfFormMappingVOList(svtListSize);
		resp.GetSvtsEcrfsUsvFormsMappingsResponse = svtEcrfFormMappingResp; 
		return resp;
	}
	
	private MdmSvtService.getSvtVOsByProtocolResponse mockSvtVOsByProtocolResponse() {
		MdmSvtService.getSvtVOsByProtocolResponse resp = new MdmSvtService.getSvtVOsByProtocolResponse();
		MdmSvtService.svtListResponse svtListResp = new MdmSvtService.svtListResponse();
		svtListResp.errors = null;
		svtListResp.statusCode = '0';
		svtListResp.svts = createMockSvtVOList(5);
		resp.GetSvtVOsResponse = svtListResp;
		return resp;
	}
	
	private MdmSvtService.saveSvtEcrfMappingFromVOResponse mockSaveSvtEcrfMappingFromVOResponse() {
		MdmSvtService.saveSvtEcrfMappingFromVOResponse resp = new MdmSvtService.saveSvtEcrfMappingFromVOResponse();
		MdmSvtService.mdmCrudResponse mdmCrudResp = new MdmSvtService.mdmCrudResponse();
		mdmCrudResp.errors = null;
		mdmCrudResp.actionType = 'I';
		mdmCrudResp.operationStatus = '0';
		resp.SaveSvtEcrfMappingResponse = mdmCrudResp;
		return resp;
	}
	
	private MdmSvtService.saveSvtEcrfMappingFromVOsResponse mockSaveSvtEcrfMappingFromVOsResponse() {
		MdmSvtService.saveSvtEcrfMappingFromVOsResponse resp = new MdmSvtService.saveSvtEcrfMappingFromVOsResponse();
		MdmSvtService.mdmCrudResponse mdmCrudResp = new MdmSvtService.mdmCrudResponse();
		mdmCrudResp.errors = null;
		mdmCrudResp.actionType = 'I';
		mdmCrudResp.operationStatus = '0';
		resp.SaveSvtEcrfMappingsResponse = mdmCrudResp;
		return resp;
	}
	
	private MdmSvtService.deleteSvtEcrfMappingFromVOsResponse mockDeleteSvtEcrfMappingFromVOsResponse() {
		MdmSvtService.deleteSvtEcrfMappingFromVOsResponse resp = new MdmSvtService.deleteSvtEcrfMappingFromVOsResponse();
		MdmSvtService.mdmCrudResponse mdmCrudResp = new MdmSvtService.mdmCrudResponse();
		mdmCrudResp.errors = null;
		mdmCrudResp.actionType = 'I';
		mdmCrudResp.operationStatus = '0';
		resp.DeleteSvtEcrfMappingsResponse = mdmCrudResp;
		return resp;
	}
	
	private MdmSvtService.saveSvtUsvFormMappingsFromVOsResponse mockSaveSvtUsvFormMappingFromVOsResponse() {
		MdmSvtService.saveSvtUsvFormMappingsFromVOsResponse resp = new MdmSvtService.saveSvtUsvFormMappingsFromVOsResponse();
		MdmSvtService.mdmCrudResponse mdmCrudResp = new MdmSvtService.mdmCrudResponse();
		mdmCrudResp.errors = null;
		mdmCrudResp.actionType = 'I';
		mdmCrudResp.operationStatus = '0';
		resp.SaveSvtUsvFormMappingsResponse = mdmCrudResp;
		return resp;
	}
	
	private MdmSvtService.deleteSvtUsvFormMappingsFromVOsResponse mockDeleteSvtUsvFormMappingFromVOsResponse() {
		MdmSvtService.deleteSvtUsvFormMappingsFromVOsResponse resp = new MdmSvtService.deleteSvtUsvFormMappingsFromVOsResponse();
		MdmSvtService.mdmCrudResponse mdmCrudResp = new MdmSvtService.mdmCrudResponse();
		mdmCrudResp.errors = null;
		mdmCrudResp.actionType = 'I';
		mdmCrudResp.operationStatus = '0';
		resp.DeleteSvtUsvFormMappingsResponse = mdmCrudResp;
		return resp;
	}
	
	/********************** Methods to build objects in responses ******************************/
	
	private MdmSvtService.svtVO createMockSvtVO(Integer index) {
		MdmSvtService.svtVO svtVO = new MdmSvtService.svtVO();
		svtVO.recordId = index;
		svtVO.systemId = 'svt-' + index;
		svtVO.templateName = 'templatename-' + index;
		svtVO.templateVersion = 'version 1';
		svtVO.templateDescription = 'description-' + index;
		svtVO.templateEffectiveDate = Datetime.newInstance(0);
		svtVO.protocol = new MdmProtocolService.protocolVO();
		svtVO.svtItems = createMockSvtItemVOListForSingleSvt(svtVO.systemId);
		return svtVO;
	}
	
	private list<MdmSvtService.svtVO> createMockSvtVOList(Integer listSize) {
		list<MdmSvtService.svtVO> svtVOs = new list<MdmSvtService.svtVO>();
		for (Integer i = 1; i < listSize + 1; i++) {
			svtVOs.add(createMockSvtVO(i));
		}
		return svtVOs;
	}
	
	private MdmSvtService.svtItemVO createMockSvtItemVO(Integer index, String svtId) {
		MdmSvtService.svtItemVO svtItemVO = new MdmSvtService.svtItemVO();
		svtItemVO.protocolNo = 'MOCK PROT NO';
		svtItemVO.svtSystemId = svtId;
		svtItemVO.systemId = 'MOCK SVT ITEM SYS ID';
		svtItemVO.sequenceNumber = index;
		svtItemVO.ecrfPageCount = 10;
		svtItemVO.inBudget = true;
		svtItemVO.visitName = 'MOCK VISIT ' + index;
		if (index < 2)
			svtItemVO.visitType = 'Screening';
		else 
			svtItemVO.visitType = 'Enrollment';
		return svtItemVO;
	}
	
	private list<MdmSvtService.svtItemVO> createMockSvtItemVOList(Integer svtCount) {
		list<MdmSvtService.svtItemVO> svtItemVOs = new list<MdmSvtService.svtItemVO>();
		for (Integer i = 1; i <= svtCount; i++) {
			for (Integer j = 1; j <= 10; j++) {
				svtItemVOs.add(createMockSvtItemVO(j, 'svt-' + i));
			}
		}
		return svtItemVOs;
	}
	
	private list<MdmSvtService.svtItemVO> createMockSvtItemVOListForSingleSvt(String svtId) {
		list<MdmSvtService.svtItemVO> svtItemVOs = new list<MdmSvtService.svtItemVO>();
		for (Integer i = 1; i <= 10; i++) {
			svtItemVOs.add(createMockSvtItemVO(i, svtId));
		}
		return svtItemVOs;
	}
	
	private MdmSvtService.ecrfItemVO createMockEcrfItemVO(Integer index) {
		MdmSvtService.ecrfItemVO ecrfItemVO = new MdmSvtService.ecrfItemVO();
		ecrfItemVO.protocol = new MdmProtocolService.protocolVO();
		ecrfItemVO.eventName = 'MOCK EVENT NAME ' + index;
		ecrfItemVO.eventLabel = 'MOCK EVENT LABEL ' + index;
		ecrfItemVO.ecrfPageCount = 10;
		ecrfItemVO.studyName = 'MOCK STUDY NAME';
		ecrfItemVO.databaseName = 'MOCK DB NAME';
		return ecrfItemVO;
	}
	
	private list<MdmSvtService.ecrfItemVO> createMockEcrfItemVOList(Integer listSize) {
		list<MdmSvtService.ecrfItemVO> ecrfItemVOs = new list<MdmSvtService.ecrfItemVO>();
		for (Integer i = 1; i < listSize + 1; i++) {
			ecrfItemVOs.add(createMockEcrfItemVO(i));
		}
		return ecrfItemVOs;
	}
	
	private MdmSvtService.ecrfFormVO createMockEcrfFormVO(Integer index) {
		MdmSvtService.ecrfFormVO ecrfFormVO = new MdmSvtService.ecrfFormVO();
		ecrfFormVO.formLabel = 'TEST LABEL ' + index;
		ecrfFormVO.formName = 'TEST NAME ' + index;
		ecrfFormVO.protocolVO = new MdmProtocolService.protocolVO();
		return ecrfFormVO;
	}
	
	private list<MdmSvtService.ecrfFormVO> createMockEcrfFormVOList(Integer listSize) {
		list<MdmSvtService.ecrfFormVO> ecrfFormVOs = new list<MdmSvtService.ecrfFormVO>();
		for (Integer i = 1; i < listSize + 1; i++) {
			ecrfFormVOs.add(createMockEcrfFormVO(i));
		}
		return ecrfFormVOs;
	}
	
	private MdmSvtService.ecrfMappingVO createMockEcrfMappingVO(Integer index, String svtId) {
		MdmSvtService.ecrfMappingVO ecrfMappingVO = new MdmSvtService.ecrfMappingVO();
		ecrfMappingVO.svtItemVO = createMockSvtItemVO(index, svtId);
		ecrfMappingVO.ecrfItemVO = createMockEcrfItemVO(index);
		ecrfMappingVO.mapped = true;
		ecrfMappingVO.primary = true;
		ecrfMappingVO.mappedBy = 'SF_UNIT_TEST';
		return ecrfMappingVO;
	}
	
	private list<MdmSvtService.ecrfMappingVO> createMockEcrfMappingVOList(Integer svtCount) {
		list<MdmSvtService.ecrfMappingVO> ecrfMappingVOs = new list<MdmSvtService.ecrfMappingVO>();
		for (Integer i = 1; i <= svtCount; i++) {
			for (Integer j = 1; j <= 20; j++) {
				ecrfMappingVOs.add(createMockEcrfMappingVO(j, 'svt-' + i));
			}
		}
		return ecrfMappingVOs;
	}
	
	private MdmSvtService.ecrfFormMappingVO createMockEcrfFormMappingVO(Integer index, String svtId) {
		MdmSvtService.ecrfFormMappingVO ecrfFormMappingVO = new MdmSvtService.ecrfFormMappingVO();
		ecrfFormMappingVO.svtItemVO = createMockSvtItemVO(index, svtId);
		ecrfFormMappingVO.ecrfFormVO = createMockEcrfFormVO(index);
		ecrfFormMappingVO.mapped = true;
		ecrfFormMappingVO.mappedBy = 'SF_UNIT_TEST';
		return ecrfFormMappingVO;
	}
	
	private list<MdmSvtService.ecrfFormMappingVO> createMockEcrfFormMappingVOList(Integer svtCount) {
		list<MdmSvtService.ecrfFormMappingVO> ecrfFormMappingVOs = new list<MdmSvtService.ecrfFormMappingVO>();
		for (Integer i = 1; i <= svtCount; i++) {
			for (Integer j = 1; j <= 20; j++) {
				ecrfFormMappingVOs.add(createMockEcrfFormMappingVO(i, 'svt-' + i));
			}
		}
		return ecrfFormMappingVOs;
	}
}