/**
 * Mock class for testing CCI_MDMService methods
 */
@isTest
public class CCI_MDMServiceTestMocks implements WebServiceMock {
	
	public String operation{get;set;}
	
	public static final String LIKE_PROTOCOL_SEARCH = 'likeProtocolSearch';
	public static final String CLIENT_PROTOCOL_NUM_SEARCH = 'clientProtocolNumSearch';
	public static final String SVTS_ECRFS_MAPPINGS_SEARCH = 'svtsEcrfsMappingsSearch';
	public static final String SVTS_ECRFS_MAPPINGS_NOT_ALL_MAPPED_SEARCH = 'svtsEcrfsMappingsSearchNotAllMapped';
	public static final String SVTS_ECRFS_MAPPINGS_NOT_ALL_PROCEDURES_MAPPED_SEARCH = 'svtsEcrfsMappingsSearchNotAllProceduresMapped';
	public static final String SVTS_ECRFS_MAPPINGS_NULL_RESPONSE_SEARCH = 'svtsEcrfsMappingsSearchNullResponse';
	public static final String SVTS_ECRFS_MAPPINGS_NULL_MAPPING_RESPONSE_SEARCH = 'svtsEcrfsMappingsSearchNullMappingResponse';
	public static final String SAVE_ECRF_MAPPINGS_SUCCESS = 'saveEcrfMappingSuccessResponse';
	public static final String SAVE_ECRF_MAPPINGS_FAILURE = 'saveEcrfMappingFailureResponse';
	
	/**
	 * Constructor to set the operation for a certain mock
	 */
	public CCI_MDMServiceTestMocks(String operation) {
		this.operation = operation;
	}
	
	/**
	 * Route to appropriate mock depending on the operation type
	 */
	public void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
		String requestName, String responseNS, String responseName, String responseType) {
		if (operation.equals(LIKE_PROTOCOL_SEARCH)) {
			mockLikeProtocolSearchResponse(response);
		} else if (operation.equals(CLIENT_PROTOCOL_NUM_SEARCH)) {
			mockClientProtocolNumSearchResponse(response);
		} else if (operation.equals(SVTS_ECRFS_MAPPINGS_SEARCH)) {
			mockSvtsEcrfsMappingsSearchResponse(response);
		} else if (operation.equals(SVTS_ECRFS_MAPPINGS_NOT_ALL_MAPPED_SEARCH)) {
			mockSvtsEcrfsMappingsSearchNotAllMappedResponse(response);
		} else if (operation.equals(SVTS_ECRFS_MAPPINGS_NULL_RESPONSE_SEARCH)) {
			mockSvtsEcrfsMappingsSearchNullResponse(response);
		} else if (operation.equals(SVTS_ECRFS_MAPPINGS_NOT_ALL_PROCEDURES_MAPPED_SEARCH)) {
			mockSvtsEcrfsMappingsSearchNotAllProceduresMappedResponse(response);
		} else if (operation.equals(SVTS_ECRFS_MAPPINGS_NULL_MAPPING_RESPONSE_SEARCH)) {
			mockSvtsEcrfsMappingsSearchNullMappingsResponse(response);
		} else if (operation.equals(SAVE_ECRF_MAPPINGS_SUCCESS)) {
			mockSaveEcrfMappingstoMdmSuccessResponse(response);
		} else if (operation.equals(SAVE_ECRF_MAPPINGS_FAILURE)) {
			mockSaveEcrfMappingstoMdmFailureResponse(response);
		}
	}
	
	/**
	 * Mock for Protocol Like Search
	 */
	private static void mockLikeProtocolSearchResponse(map<String, Object> response) {
		CCI_MDMService.getProtocolVOsLikeClientProtNumResponse getResponse = new CCI_MDMService.getProtocolVOsLikeClientProtNumResponse();
		CCI_MDMService.protocolListResponse protocolListResponse = new CCI_MDMService.protocolListResponse();
		protocolListResponse.errors = null;
		protocolListResponse.statusCode = '0';
		protocolListResponse.protocolVOs = createMockProtocolVOList();
		getResponse.GetProtocolVOsLikeClientProtNumResponse = protocolListResponse;
		response.put('response_x', getResponse);
	}
	
	/**
	 * Mock for Client Protocol Number Search
	 */
	private static void mockClientProtocolNumSearchResponse(map<String, Object> response) {
		CCI_MDMService.getProtocolVOByClientProtNumResponse getResponse = new CCI_MDMService.getProtocolVOByClientProtNumResponse();
		CCI_MDMService.protocolResponse protocolResponse = new CCI_MDMService.protocolResponse();
		protocolResponse.errors = null;
		protocolResponse.statusCode = '0';
		protocolResponse.protocolVO = createMockProtocolVO();
		getResponse.GetProtocolVOProtNumResponse = protocolResponse;
		response.put('response_x', getResponse);
	}
	
	/**
	 * Mock for Svts Ecrfs and Mappings Search
	 */
	private static void mockSvtsEcrfsMappingsSearchResponse(map<String, Object> response) {
		CCI_MDMService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse getResponse = new CCI_MDMService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse();
		CCI_MDMService.svtEcrfMappingResponse svtEcrfMappingResponse = new CCI_MDMService.svtEcrfMappingResponse();
		svtEcrfMappingResponse.errors = null;
		svtEcrfMappingResponse.statusCode = '0';
		CCI_MDMService.protocolVO protocolVO = createMockProtocolVO();		
		svtEcrfMappingResponse.svts = createMockSvtVOs(protocolVO);
		svtEcrfMappingResponse.ecrfItems = createMockEcrfItemVOs(protocolVO);
		svtEcrfMappingResponse.mappings = createMockEcrfMappingVOs(svtEcrfMappingResponse.svts, svtEcrfMappingResponse.ecrfItems);
		getResponse.GetSvtsEcrfsMappingsResponse = svtEcrfMappingResponse;
		response.put('response_x', getResponse);
	}
	
	/**
	 * Mock for Svts Ecrfs and Mappings Search - not all maped
	 */
	private static void mockSvtsEcrfsMappingsSearchNotAllMappedResponse(map<String, Object> response) {
		CCI_MDMService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse getResponse = new CCI_MDMService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse();
		CCI_MDMService.svtEcrfMappingResponse svtEcrfMappingResponse = new CCI_MDMService.svtEcrfMappingResponse();
		svtEcrfMappingResponse.errors = null;
		svtEcrfMappingResponse.statusCode = '0';
		CCI_MDMService.protocolVO protocolVO = createMockProtocolVO();		
		svtEcrfMappingResponse.svts = createMockSvtVOs(protocolVO);
		svtEcrfMappingResponse.ecrfItems = createMockEcrfItemVOs(protocolVO);
		svtEcrfMappingResponse.mappings = createMockEcrfMappingVOsWithDifferentMappedValues(svtEcrfMappingResponse.svts, svtEcrfMappingResponse.ecrfItems);
		getResponse.GetSvtsEcrfsMappingsResponse = svtEcrfMappingResponse;
		response.put('response_x', getResponse);
	}
	
	/**
	 * Mock for Svts Ecrfs and Mappings Search - not all procedures
	 */
	private static void mockSvtsEcrfsMappingsSearchNotAllProceduresMappedResponse(map<String, Object> response) {
		CCI_MDMService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse getResponse = new CCI_MDMService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse();
		CCI_MDMService.svtEcrfMappingResponse svtEcrfMappingResponse = new CCI_MDMService.svtEcrfMappingResponse();
		svtEcrfMappingResponse.errors = null;
		svtEcrfMappingResponse.statusCode = '0';
		CCI_MDMService.protocolVO protocolVO = createMockProtocolVO();		
		svtEcrfMappingResponse.svts = createMockSvtVOs(protocolVO);
		svtEcrfMappingResponse.ecrfItems = createMockEcrfItemVOs(protocolVO);
		svtEcrfMappingResponse.mappings = createMockEcrfMappingVOsWithMissingProcedures(svtEcrfMappingResponse.svts, svtEcrfMappingResponse.ecrfItems);
		getResponse.GetSvtsEcrfsMappingsResponse = svtEcrfMappingResponse;
		response.put('response_x', getResponse);
	}
	
	/**
	 * Mock for Svts Ecrfs and Mappings Search - null response
	 */
	private static void mockSvtsEcrfsMappingsSearchNullResponse(map<String, Object> response) {
		CCI_MDMService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse getResponse = new CCI_MDMService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse();
		CCI_MDMService.svtEcrfMappingResponse svtEcrfMappingResponse = new CCI_MDMService.svtEcrfMappingResponse();
		svtEcrfMappingResponse.errors = null;
		svtEcrfMappingResponse.statusCode = '0';
		CCI_MDMService.protocolVO protocolVO = createMockProtocolVO();		
		svtEcrfMappingResponse.svts = null;
		svtEcrfMappingResponse.ecrfItems = null;
		svtEcrfMappingResponse.mappings = null;
		getResponse.GetSvtsEcrfsMappingsResponse = svtEcrfMappingResponse;
		response.put('response_x', getResponse);
	}
	
	/**
	 * Mock for Svts Ecrfs and Mappings Search - null mappings
	 */
	private static void mockSvtsEcrfsMappingsSearchNullMappingsResponse(map<String, Object> response) {
		CCI_MDMService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse getResponse = new CCI_MDMService.getSvtsEcrfsAndMappingsFromProtocolPraIdResponse();
		CCI_MDMService.svtEcrfMappingResponse svtEcrfMappingResponse = new CCI_MDMService.svtEcrfMappingResponse();
		svtEcrfMappingResponse.errors = null;
		svtEcrfMappingResponse.statusCode = '0';
		CCI_MDMService.protocolVO protocolVO = createMockProtocolVO();		
		svtEcrfMappingResponse.svts = createMockSvtVOs(protocolVO);
		svtEcrfMappingResponse.ecrfItems = createMockEcrfItemVOs(protocolVO);
		svtEcrfMappingResponse.mappings = null;
		getResponse.GetSvtsEcrfsMappingsResponse = svtEcrfMappingResponse;
		response.put('response_x', getResponse);
	}
	
	/**
	 * Mock for Save EcrfMappings - success
	 */
	private static void mockSaveEcrfMappingstoMdmSuccessResponse(map<String, Object> response) {
		CCI_MDMService.saveSvtEcrfMappingFromVOsResponse saveResponse = new CCI_MDMService.saveSvtEcrfMappingFromVOsResponse();
		CCI_MDMService.mdmCrudResponse mdmCrudResponse = new CCI_MDMService.mdmCrudResponse();
		mdmCrudResponse.errors = null;
		mdmCrudResponse.actionType = 'I';
		mdmCrudResponse.operationStatus = '0';
		saveResponse.SaveSvtEcrfMappingsResponse = mdmCrudResponse;
		response.put('response_x', saveResponse);
	}
	
	/**
	 * Mock for Save EcrfMappings - failure
	 */
	private static void mockSaveEcrfMappingstoMdmFailureResponse(map<String, Object> response) {
		CCI_MDMService.saveSvtEcrfMappingFromVOsResponse saveResponse = new CCI_MDMService.saveSvtEcrfMappingFromVOsResponse();
		CCI_MDMService.mdmCrudResponse mdmCrudResponse = new CCI_MDMService.mdmCrudResponse();
		mdmCrudResponse.errors = null;
		mdmCrudResponse.actionType = 'I';
		mdmCrudResponse.operationStatus = '-1';
		response.put('response_x', saveResponse);
	}
	
	/**
	 * Start utility methods - used to create response elements
	 */
	private static CCI_MDMService.protocolVO createMockProtocolVO() {
		CCI_MDMService.protocolVO protocolVO = new CCI_MDMService.protocolVO();
		protocolVO.clientProtocolNum = 'MOCK PROTOCOL';
		protocolVO.disableEdcAutomation = false;
		list<CCI_MDMService.studyVO> studyVOs = new list<CCI_MDMService.studyVO>();
		CCI_MDMService.studyVO study = new CCI_MDMService.studyVO();
		study.praId = 'MOCK STUDYVO';
		study.systemId = 'A-MOCK-STUDY-ID';
		studyVOs.add(study);
		protocolVO.studyVOs = studyVOs;
		protocolVO.systemId = 'A-MOCK-PROTOCOL-ID';	
		return protocolVO;
	}
	
	private static list<CCI_MDMService.protocolVO> createMockProtocolVOList() {
		list<CCI_MDMService.protocolVO> protocolVOs = new list<CCI_MDMService.protocolVO>();
		for (Integer i = 1; i <= 5; i++) {
			CCI_MDMService.protocolVO protocolVO = new CCI_MDMService.protocolVO();
			protocolVO.clientProtocolNum = 'MOCK PROTOCOL ' + i;
			protocolVO.disableEdcAutomation = false;
			list<CCI_MDMService.studyVO> studyVOs = new list<CCI_MDMService.studyVO>();
			CCI_MDMService.studyVO study = new CCI_MDMService.studyVO();
			study.praId = 'MOCK STUDYVO';
			study.systemId = 'A-MOCK-STUDY-ID';
			studyVOs.add(study);
			protocolVO.studyVOs = studyVOs;
			protocolVO.systemId = 'A-MOCK-PROTOCOL-ID';
			protocolVOs.add(protocolVO);
		}
		return protocolVOs;
	}
	
	private static list<CCI_MDMService.svtVO> createMockSvtVOs(CCI_MDMService.protocolVO protocolVO) {
		list<CCI_MDMService.svtVO> svts = new list<CCI_MDMService.svtVO>();
		for (Integer i = 1; i <= 5; i++) {
			CCI_MDMService.svtVO svt = new CCI_MDMService.svtVO();
			svt.systemId = i + '-MOCKSVTID';
			svt.templateDescription = 'MOCK DESCRIPTION ' + i;
			svt.templateEffectiveDate = Datetime.newInstance(5,5,2013);
			svt.templateName = 'MOCK NAME ' + i;
			svt.templateVersion = 'MOCK VERSION ' + i;
			svt.svtItems = createMockSvtItemVOs(svt.systemId);
			svt.protocol = protocolVO;
			svts.add(svt);
		}
		return svts;
	}
	
	private static list<CCI_MDMService.svtItemVO> createMockSvtItemVOs(String svtSystemId) {
		list<CCI_MDMService.svtItemVO> svtItems = new list<CCI_MDMService.svtItemVO>();
		for (Integer i = 1; i <= 5; i++) {
			CCI_MDMService.svtItemVO svtItem = new CCI_MDMService.svtItemVO();
			svtItem.ecrfPageCount = i;
			svtItem.inBudget = true;
			svtItem.protocolNo = 'MOCK PROTOCOL';
			svtItem.sequenceNumber = i;
			svtItem.systemId = svtSystemId + '-MOCKSVTID-' + i;
			svtItem.svtSystemId = svtSystemId;
			svtItem.visitName = 'MOCK NAME ' + i;
			svtItem.visitType = 'MOCK TYPE ' + i;
			svtItems.add(svtItem);
		}
		return svtItems;
	}
	
	private static list<CCI_MDMService.ecrfItemVO> createMockEcrfItemVOs(CCI_MDMService.protocolVO protocolVO) {
		list<CCI_MDMService.ecrfItemVO> ecrfItems = new list<CCI_MDMService.ecrfItemVO>();
		for (Integer i = 1; i <= 5; i++) {
			CCI_MDMService.ecrfItemVO ecrfItem = new CCI_MDMService.ecrfItemVO();
			ecrfItem.eventLabel = 'MOCK EVENT LABEL ' + i;
			ecrfItem.eventName = 'MOCK EVENT NAME ' + i;
			ecrfItem.studyName = 'MOCK STUDY NAME';
			ecrfItem.databaseName = 'MOCK DATABASENAME';
			ecrfItem.ecrfPageCount = i;
			ecrfItem.protocol = protocolVO;
			ecrfItems.add(ecrfItem);
		}
		return ecrfItems;
	}
	
	private static list<CCI_MDMService.ecrfMappingVO> createMockEcrfMappingVOs(list<CCI_MDMService.svtVO> svts, list<CCI_MDMService.ecrfItemVO> ecrfItems) {
		list<CCI_MDMService.ecrfMappingVO> ecrfMappings = new list<CCI_MDMService.ecrfMappingVO>();
		for (CCI_MDMService.svtVO svt : svts) {
			for (Integer i = 0; i < svt.svtItems.size(); i++) {
				CCI_MDMService.ecrfMappingVO mapping = new CCI_MDMService.ecrfMappingVO();
				mapping.mapped = true;
				mapping.primary = true;
				mapping.mappedBy = 'TEST USER';
				mapping.svtItemVO = svt.svtItems.get(i);
				mapping.ecrfItemVO = ecrfItems.get(i);
				ecrfMappings.add(mapping);
			}
		}
		return ecrfMappings;
	}
	
	private static list<CCI_MDMService.ecrfMappingVO> createMockEcrfMappingVOsWithDifferentMappedValues(list<CCI_MDMService.svtVO> svts, 
		list<CCI_MDMService.ecrfItemVO> ecrfItems) {
		list<CCI_MDMService.ecrfMappingVO> ecrfMappings = new list<CCI_MDMService.ecrfMappingVO>();
		for (CCI_MDMService.svtVO svt : svts) {
			Boolean mapped = false;
			for (Integer i = 0; i < svt.svtItems.size(); i++) {
				CCI_MDMService.ecrfMappingVO mapping = new CCI_MDMService.ecrfMappingVO();
				mapping.mapped = !mapped;
				mapping.primary = true;
				mapping.mappedBy = 'TEST USER';
				mapping.svtItemVO = svt.svtItems.get(i);
				mapping.ecrfItemVO = ecrfItems.get(i);
				ecrfMappings.add(mapping);
				mapped = !mapped;
			}
		}
		return ecrfMappings;
	}
	
	private static list<CCI_MDMService.ecrfMappingVO> createMockEcrfMappingVOsWithMissingProcedures(list<CCI_MDMService.svtVO> svts, 
		list<CCI_MDMService.ecrfItemVO> ecrfItems) {
		list<CCI_MDMService.ecrfMappingVO> ecrfMappings = new list<CCI_MDMService.ecrfMappingVO>();
		for (CCI_MDMService.svtVO svt : svts) {
			Boolean skipMapping = false;
			for (Integer i = 0; i < svt.svtItems.size(); i++) {
				if (!skipMapping) {
					CCI_MDMService.ecrfMappingVO mapping = new CCI_MDMService.ecrfMappingVO();
					mapping.mapped = true;
					mapping.primary = true;
					mapping.mappedBy = 'TEST USER';
					mapping.svtItemVO = svt.svtItems.get(i);
					mapping.ecrfItemVO = ecrfItems.get(i);
					ecrfMappings.add(mapping);
				}
				skipMapping = !skipMapping;
			}
			
		}
		return ecrfMappings;
	}
}