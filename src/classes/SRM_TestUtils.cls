/**
  'SRM_TestUtils' is the Util for create the test data for SRM.
  @author  Kondal Reddy
  @version  1.0.0.0
  @date Jan-2015
**/
@isTest(Seealldata=false)
public with sharing class SRM_TestUtils {

    public Country__c country;
    public SRM_Model_Subject_Enrollment__c subjectEnrollment;
    public SRM_Model__c srmModel;
    public SRM_Model_Site_Activation__c siteActivation;
    
    public void SRM_TestUtils(){
    }
    
    /**
    *   This Method is to create the Country data.          
    */
    Public Country__c createCountryRecord() {
        country = new Country__c(Name = 'Test India',Country_Code__c = 'TI',Daily_Business_Hrs__c = 8,Description__c = 'Test India',PRA_Country_Id__c = 'TI1');
        insert country;
        return country;
    }
    
    public SRM_Model__c createSrmModel(){
        srmModel = new SRM_Model__c(Status__c = 'In Progres', Comments__c = 'Test Comments');
        insert srmModel;
        return srmModel;
    }
    
    public SRM_Model__c createApprovedSrmModel(){
        srmModel = new SRM_Model__c(Status__c = 'Approved', Comments__c = 'Test Comments');
        insert srmModel;
        return srmModel;
    }
    
    public SRM_Model_Site_Activation__c createSiteActivation(){
    	siteActivation = new SRM_Model_Site_Activation__c(SRM_Model__c =srmModel.Id, Country__c = country.Id, IRB_Type__c = 'Local IRB', Site_Activation_Formula__c = 'Gaussian',
    													   Number_Of_Weeks__c = 20, Amplitude__c = 100, Shift_Center_By__c = 1, Standard_Deviation__c = 1, Gaussian_Constant__c = 0,
    													   Start_Week__c = 1, Comments__c = 'Test Comment');
    	insert siteActivation;
    	return siteActivation;
    }
    
    public SRM_Model_Subject_Enrollment__c createSubjectEnrollment(){
        subjectEnrollment = new SRM_Model_Subject_Enrollment__c(SRM_Model__c =srmModel.Id, Shift_Center_By__c = 3, Standard_Deviation__c =3);
        insert subjectEnrollment;
        return subjectEnrollment;
    }
}