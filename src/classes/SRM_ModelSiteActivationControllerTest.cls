/**
* @author Ramya Shree Edara
* @date 21-Oct-2014
* @description this is test class for Model SiteActivation classes
*/
@isTest
public with sharing class  SRM_ModelSiteActivationControllerTest{
    
    public static testMethod void testnewsiteActivation(){
        
        SRM_TestUtils tu = new SRM_TestUtils();
        Country__c country = new Country__c();
        SRM_MOdel__c model = new SRM_MOdel__c();
        SRM_Model_Site_Activation__c siteBZ = new SRM_Model_Site_Activation__c();
		
        country = tu.createCountryRecord();
    	model = tu.createSrmModel();
        siteBZ = tu.createSiteActivation();
        
        PageReference pageRef = Page.SRM_ModelSiteActivation;
        Test.setCurrentPage(pageRef);
             
        ApexPages.StandardController sc = new ApexPages.StandardController(siteBZ);
        SRM_ModelSiteActivationController modelSiteAct = new SRM_ModelSiteActivationController(sc);
        
        modelSiteAct.saveSiteActivation();
        modelSiteAct.generateChartData();
        modelSiteAct.cancel();
    }
    
    public static testMethod void testDuplicateException(){
        
        SRM_TestUtils tu = new SRM_TestUtils();
        Country__c country = new Country__c();
        SRM_MOdel__c model = new SRM_MOdel__c();
        SRM_Model_Site_Activation__c siteBZ = new SRM_Model_Site_Activation__c();
		
        country = tu.createCountryRecord();
    	model = tu.createSrmModel();
        siteBZ = tu.createSiteActivation();
        
        PageReference pageRef = Page.SRM_ModelSiteActivation;
        Test.setCurrentPage(pageRef);
             
        ApexPages.StandardController sc = new ApexPages.StandardController(siteBZ);
        SRM_ModelSiteActivationController modelSiteAct = new SRM_ModelSiteActivationController(sc);
        
        modelSiteAct.saveSiteActivation();
        modelSiteAct.generateChartData();
        modelSiteAct.cancel();      	
        siteBZ.Start_Week__c = -1;
        upsert siteBZ;
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(siteBZ);
        SRM_ModelSiteActivationController modelSiteAct1 = new SRM_ModelSiteActivationController(sc1);
        modelSiteAct1.generateChartData();
        modelSiteAct1.saveSiteActivation();
    }
    
    public static testMethod void testvalidation(){
                
        SRM_TestUtils tu = new SRM_TestUtils();
        Country__c country = new Country__c();
        SRM_MOdel__c model = new SRM_MOdel__c();
        SRM_Model_Site_Activation__c sa1 = new SRM_Model_Site_Activation__c();
		
        country = tu.createCountryRecord();
    	model = tu.createSrmModel();
        sa1 = tu.createSiteActivation();
        
        SRM_Model_Site_Activation__c sa2 = SRM_ScenarioDataAccessor.getSiteActivationById(sa1.id);
        
        sa2.Start_Week__c = -1;
        update sa2;
        system.debug('--sa2--'+sa2);
        PageReference pageRef = Page.SRM_ModelSiteActivation;
        Test.setCurrentPage(pageRef);
        
        ApexPages.currentPage().getParameters().put('Id', sa2.Id);
             
        ApexPages.StandardController sc = new ApexPages.StandardController(sa2);
        SRM_ModelSiteActivationController modelSiteAct = new SRM_ModelSiteActivationController(sc);
        modelSiteAct.generateChartData();
        
        SRM_Model_Site_Activation__c sa3 = SRM_ScenarioDataAccessor.getSiteActivationById(sa2.id);
        sa3.Start_Week__c = 1;
        sa3.Amplitude__c = 0;
        update sa3;
        
        ApexPages.StandardController sc1 = new ApexPages.StandardController(sa3);
        SRM_ModelSiteActivationController modelSiteAct1 = new SRM_ModelSiteActivationController(sc1);
        modelsiteAct1.validate();
        
        SRM_Model_Site_Activation__c sa4 = SRM_ScenarioDataAccessor.getSiteActivationById(sa3.id);
        sa4.Start_Week__c = 3;
        sa4.Amplitude__c =100;
        sa4.Standard_Deviation__c = 0;
        upsert sa4;
        
        ApexPages.StandardController sc2 = new ApexPages.StandardController(sa4);
        SRM_ModelSiteActivationController modelSiteAct2 = new SRM_ModelSiteActivationController(sc2);
        modelsiteAct2.validate();
    }
        
    public static testMethod void testclone(){
        
    	SRM_TestUtils tu = new SRM_TestUtils();
        Country__c country = new Country__c();
        SRM_MOdel__c model = new SRM_MOdel__c();
        SRM_Model_Site_Activation__c siteBZ = new SRM_Model_Site_Activation__c();
		
        country = tu.createCountryRecord();
    	model = tu.createSrmModel();
        siteBZ = tu.createSiteActivation();
        
        PageReference pageRef = Page.SRM_ModelSiteActivation;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', siteBZ.Id);
        ApexPages.currentPage().getParameters().put('returl', siteBZ.Id);
        ApexPages.currentPage().getParameters().put('clone', '1');   
             
        ApexPages.StandardController sc = new ApexPages.StandardController(siteBZ);
        SRM_ModelSiteActivationController modelSiteAct = new SRM_ModelSiteActivationController(sc);
        
        modelSiteAct.saveSiteActivation();
        modelSiteAct.generateChartData();
        modelSiteAct.cancel();
    }
    
    public static testMethod void testOneModelasApproved(){
        SRM_TestUtils tu = new SRM_TestUtils();
        Country__c country = new Country__c();
        SRM_MOdel__c model = new SRM_MOdel__c();
        SRM_Model__c model1 = new SRM_Model__c();
        
        model = tu.createSrmModel();
        model.Status__c = 'Approved';
        update model;
        
        model1 = tu.createSrmModel();
        try{
            model1.Status__c ='Approved';
            model1.Comments__c = 'test2';
            upsert model1;
        }
        catch(Exception e){
            System.Assert(e.getMessage().contains('FIELD_CUSTOM_VALIDATION_EXCEPTION'));
        }
        
    }    

}