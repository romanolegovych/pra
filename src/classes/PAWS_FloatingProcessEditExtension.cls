public with sharing class PAWS_FloatingProcessEditExtension
{
	public final Integer userFieldsLimit = 10;
	
	public PAWS_Floating_Process__c record {get; set;}
	
	public PAWS_FloatingProcessEditExtension(ApexPages.StandardController stdController)
	{
		stdController.addFields(new List<String>{
			'Name',
			'Status__c',
			'Description__c',
			'Flow__c',
			'PAWS_Project_Flow__c',
			'PAWS_Project_Flow_Country__c',
			'PAWS_Project_Flow_Site__c',
			'User_Role_1__c',
			'User_Role_2__c',
			'User_Role_3__c',
			'User_Role_4__c',
			'User_Role_5__c',
			'User_Role_6__c',
			'User_Role_7__c',
			'User_Role_8__c',
			'User_Role_9__c',
			'User_Role_10__c'
		});
		
		record = (PAWS_Floating_Process__c) stdController.getRecord();
		
		if (record.Id == null)
		{
			record.PAWS_Project_Flow__c = PAWSProjectId;
			record.Status__c = 'Not Started';
		}
	}
	
	public String PAWSProjectId
	{
		get
		{
			if (PAWSProjectId == null)
			{
				PAWSProjectId = (record.PAWS_Project_Flow__c != null ? record.PAWS_Project_Flow__c : ApexPages.CurrentPage().getParameters().get('projectId'));
			}
			return PAWSProjectId;
		}
		set;
	}
	
	public String RetURL
	{
		get
		{
			if (RetURL == null)
			{
				RetURL = ApexPages.CurrentPage().getParameters().get('retURL');
			}
			return RetURL;
		}
		set;
	}
	
	public String SelectedFlowId
	{
		get
		{
			return (record.Flow__c == null ? '-1' : record.Flow__c);
		}
		set
		{
			record.Flow__c = (value == '-1' ? null : value);
		}
	}
	
	public List<SelectOption> FloatingProcessesOptions
	{
		get
		{
			if (FloatingProcessesOptions == null)
			{
				FloatingProcessesOptions = new List<SelectOption>();
				FloatingProcessesOptions.add(new SelectOption('-1', '--None--'));
				
				for (STSWR1__Flow__c f : GetFloatingProcesses())
				{
					FloatingProcessesOptions.add(new SelectOption(f.Id, f.Name));
				}
			}
			
			return FloatingProcessesOptions;
		}
		set;
	}
	
	public PageReference Save()
	{
		PageReference pageRef;
		
		try
		{
			ValidateProcess();
			upsert record;
			pageRef = new PageReference(RetURL != null ? RetURL : '/' + record.Id);
		}
		catch(Exception ex)
		{
			ApexPages.addMessages(ex);
		}
		
		return pageRef;
	}
	
	public PageReference Cancel()
	{
		return new PageReference(RetURL != null ? RetURL : '/' + PAWSProjectId);
	}
	
	public void FlowOnChange()
	{
		for (Integer i = 1; i <= userFieldsLimit; i++)
		{
			record.put('User_Role_' + String.valueOf(i) + '__c', null);
		}
		
		FlowSwimlanes = null;
	}
	
	public List<STSWR1__Flow_Swimlane__c> FlowSwimlanes
	{
		get
		{
			if (FlowSwimlanes == null)
			{
				FlowSwimlanes = new List<STSWR1__Flow_Swimlane__c>();
				
				if (record.Flow__c != null)
				{
					FlowSwimlanes = [
						Select
							Name,
							STSWR1__Assign_To__c
						From
							STSWR1__Flow_Swimlane__c
						Where
							STSWR1__Flow__c = :record.Flow__c And
							STSWR1__Assign_Type__c = 'Field' And
							STSWR1__Assign_To__c != null
						Order by
							Name
						limit :userFieldsLimit
					];
				}
			}
			return FlowSwimlanes;
		}
		set;
	}
	
	private void ValidateProcess()
	{
		if (SelectedFlowId == '-1')
		{
			throw new PAWS_Utilities.PAWSException('You need to select a Flow for your process.');
		}
		
		for (STSWR1__Flow_Swimlane__c swimlane : FlowSwimlanes)
		{
			if (record.get(swimlane.STSWR1__Assign_To__c) == null)
			{
				throw new PAWS_Utilities.PAWSException('You need to populate all User Roles before saving your process.');
			}
		}
	}
	
	private List<STSWR1__Flow__c> GetFloatingProcesses()
	{
		/*List<STSWR1__Flow__c> flows = new List<STSWR1__Flow__c>();
		String folderName = 'Floating Processes';
		
		List<STSWR1__Item__c> items = [
			Select
				STSWR1__Source_Flow__r.Name
			From
				STSWR1__Item__c
			Where
				STSWR1__Type__c = 'File' And
				STSWR1__Parent__r.STSWR1__Type__c = 'Folder' And
				STSWR1__Parent__r.Name = :folderName And
				STSWR1__Parent__r.STSWR1__Parent__r.STSWR1__Type__c = 'Folder' And
				STSWR1__Parent__r.STSWR1__Parent__r.Name = 'Templates' And
				STSWR1__Parent__r.STSWR1__Parent__r.STSWR1__Parent__c = null
			Order by
				STSWR1__Source_Flow__r.Name
		];
		
		for (STSWR1__Item__c item : items)
		{
			flows.add(item.STSWR1__Source_Flow__r);
		}
		
		return flows;*/
		
		return [
			Select
				Name
			From
				STSWR1__Flow__c
			Where
				STSWR1__Object_Type__c = 'paws_floating_process__c' And
				STSWR1__Type__c = 'Recurrent' And
				STSWR1__Start_Type__c = 'Manual' And
				STSWR1__Status__c = 'Active'
			Order By
				Name
		];
	}
}