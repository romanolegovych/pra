/* 
@author Kostyantyn Hladkyi
@date 2015  
@description test class for PRA_WrappersForMobile
*/
@isTest
private class PRA_WrappersForMobileTest {
	
	private static final String TEST_STRING_VALUE = 'TEST_STRING_VALUE';
	private static PBB_WR_APIs.CountryWeeklyEvents weeklyEvent;
	private static PRA_WrappersForMobile.CountryWeeklyEventsWrapper countryWeeklyEventsWrapper;

	private static final String COUNTRY = 'Country';
	
	private static final Map<String,Integer> TEST_VALUES = new Map<String,Integer>{
			'KEY_1' => 123
	};

	private static void createData() {
		Date wDate = Date.today();
		Integer aggSub = 1;
		Integer consSub = 1;
		Integer planSub = 1;
		Integer actSub = 1;
		Integer medRskSub =1;
		weeklyEvent = new PBB_WR_APIs.CountryWeeklyEvents(wDate, aggSub, consSub, planSub, actSub, medRskSub);
		countryWeeklyEventsWrapper = new PRA_WrappersForMobile.CountryWeeklyEventsWrapper(weeklyEvent);
	}

	@isTest
	private static void getWeekDate(){
		createData();
		Test.startTest();
			System.assertEquals( weeklyEvent.WeekDate, countryWeeklyEventsWrapper.getWeekDate() );
		Test.stopTest();
	}

	@isTest
	private static void getAggressiveSubjects(){
		createData();
		Test.startTest();
			System.assertEquals( weeklyEvent.AggressiveSubjects, countryWeeklyEventsWrapper.getAggressiveSubjects() );
		Test.stopTest();
	}

	@isTest
	private static void getConservativeSubjects(){
		createData();
		Test.startTest();
			System.assertEquals( weeklyEvent.ConservativeSubjects, countryWeeklyEventsWrapper.getConservativeSubjects() );
		Test.stopTest();
	}

	@isTest
	private static void getPlannedSubjects(){
		createData();
		Test.startTest();
			System.assertEquals( weeklyEvent.PlannedSubjects, countryWeeklyEventsWrapper.getPlannedSubjects() );
		Test.stopTest();
	}

	@isTest
	private static void getActualSubjects(){
		createData();
		Test.startTest();
			System.assertEquals( weeklyEvent.ActualSubjects, countryWeeklyEventsWrapper.getActualSubjects() );
		Test.stopTest();
	}

	@isTest
	private static void getMediumRiskSubjects(){
		createData();
		Test.startTest();
			System.assertEquals( weeklyEvent.MediumRiskSubjects, countryWeeklyEventsWrapper.getMediumRiskSubjects() );
		Test.stopTest();
	}

	@isTest
	private static void getOnTrack(){
		createData();
		Integer result = weeklyEvent.conservativeSubjects - weeklyEvent.actualSubjects;
		Test.startTest();
			System.assertEquals( result, countryWeeklyEventsWrapper.getOnTrack() );
		Test.stopTest();
	}

	@isTest
	private static void getAtRisk(){
		createData();
		Integer result = weeklyEvent.aggressiveSubjects - weeklyEvent.conservativeSubjects;
		Test.startTest();
			System.assertEquals( result, countryWeeklyEventsWrapper.getAtRisk() );
		Test.stopTest();
	}

	@isTest
	private static void getDelayed(){
		createData();
		Integer result = weeklyEvent.plannedSubjects - weeklyEvent.aggressiveSubjects;
		Test.startTest();
			System.assertEquals( result, countryWeeklyEventsWrapper.getDelayed() );
		Test.stopTest();
	}

	@isTest
	private static void getRemaining(){
		createData();
		Integer result = weeklyEvent.plannedSubjects - weeklyEvent.actualSubjects;
		Test.startTest();
			System.assertEquals( result, countryWeeklyEventsWrapper.getRemaining() );
		Test.stopTest();
	}

	@isTest
	private static void getActiveToDate(){
		createData();
		Test.startTest();
			System.assertEquals( weeklyEvent.ActualSubjects, countryWeeklyEventsWrapper.getActiveToDate() );
		Test.stopTest();
	}

	@isTest
	private static void countryWeeklyEventsResultError(){
		//createData();
		Test.startTest();
			PRA_WrappersForMobile.CountryWeeklyEventsResult countryWeeklyEventsResult = new PRA_WrappersForMobile.CountryWeeklyEventsResult('Error');
		Test.stopTest();
	}

	@isTest
	private static void countryWeeklyEventsResult(){
		createData();
		List<PRA_WrappersForMobile.CountryWeeklyEventsWrapper> countryWeeklyEventsWrappers = new List<PRA_WrappersForMobile.CountryWeeklyEventsWrapper>();
		countryWeeklyEventsWrappers.add(countryWeeklyEventsWrapper);
		PRA_WrappersForMobile.CountryWeeklyEventsResult countryWeeklyEventsResult = new PRA_WrappersForMobile.CountryWeeklyEventsResult(countryWeeklyEventsWrappers);
		Test.startTest();
			System.assertEquals( countryWeeklyEventsWrappers, countryWeeklyEventsResult.events );
		Test.stopTest();
	}
	
	@isTest
	private static void wrapperSiteTest(){
		Test.startTest();
			PRA_WrappersForMobile.WrapperSite wrapper = new PRA_WrappersForMobile.WrapperSite(
					TEST_STRING_VALUE,
					TEST_VALUES,
					COUNTRY
			);
		Test.stopTest();
		System.assertEquals( TEST_STRING_VALUE, wrapper.site );
		System.assertEquals( TEST_VALUES, wrapper.siteData );
	}
	
	@isTest
	private static void wrapperCountryTest(){
		Test.startTest();
			PRA_WrappersForMobile.WrapperCountry wrapper = new PRA_WrappersForMobile.WrapperCountry(
					TEST_STRING_VALUE,
					TEST_VALUES
			);
		Test.stopTest();
		System.assertEquals( TEST_STRING_VALUE, wrapper.country );
		System.assertEquals( TEST_VALUES, wrapper.countryData );
	}

	@isTest
	private static void patientEnrollmentWrapperGlobal(){
		Test.startTest();
			PRA_WrappersForMobile.PatientEnrollmentWrapperGlobal patientEnrollmentWrapperGlobal = new PRA_WrappersForMobile.PatientEnrollmentWrapperGlobal( new PBB_WR_APIs.PatientEnrollmentwrapper() );
		Test.stopTest();
	}

	@isTest
	private static void operationResultEmpty(){
		Test.startTest();
			PRA_WrappersForMobile.OperationResult operationResult = new PRA_WrappersForMobile.OperationResult();
		Test.stopTest();
		System.assertEquals(operationResult.errorMessage, null);
	}
	
	@isTest
	private static void operationResult(){
		Test.startTest();
			PRA_WrappersForMobile.OperationResult operationResult = new PRA_WrappersForMobile.OperationResult('error');
		Test.stopTest();
		System.assertEquals(operationResult.errorMessage, 'error');
	}

	@isTest
	private static void operationResults(){
		List<Object> objects = new List<Object>();
		Test.startTest();
			PRA_WrappersForMobile.OperationResult operationResult = new PRA_WrappersForMobile.OperationResult(objects);
		Test.stopTest();
		System.assertEquals(operationResult.returnedValues, objects);
	}

	@isTest
	private static void userDataError(){
		Test.startTest();
			PRA_WrappersForMobile.UserData userDataResult = new PRA_WrappersForMobile.UserData('error');
		Test.stopTest();
		System.assertEquals(userDataResult.errorMessage, 'error');
	}

	@isTest
	private static void userData(){
		User u = new User(Email = 'test@test.com', Phone = '111');
		Test.startTest();
			PRA_WrappersForMobile.UserData userDataResult = new PRA_WrappersForMobile.UserData(u);
		Test.stopTest();
		System.assertEquals(userDataResult.email, u.Email);
		System.assertEquals(userDataResult.phone, u.Phone);
	}

	@isTest
	private static void registrationsPerProtocolResult(){
		Test.startTest();
			PRA_WrappersForMobile.RegistrationsPerProtocolResult registrationsPerProtocolResult = new PRA_WrappersForMobile.RegistrationsPerProtocolResult();
		Test.stopTest();
	}

	@isTest
	private static void registrationsPerProtocolResultMap(){
		Map<String, Integer> registrationsPerProjects = new Map<String, Integer>();
		registrationsPerProjects.put('test', 11);
		Test.startTest();
			PRA_WrappersForMobile.RegistrationsPerProtocolResult registrationsPerProtocolResult = new PRA_WrappersForMobile.RegistrationsPerProtocolResult(registrationsPerProjects);
		Test.stopTest();
		System.assertEquals(registrationsPerProtocolResult.registrationsPerProjects, registrationsPerProjects);
	}

	@isTest
	private static void registrationsPerProtocolResultError(){
		Test.startTest();
			PRA_WrappersForMobile.RegistrationsPerProtocolResult registrationsPerProtocolResult = new PRA_WrappersForMobile.RegistrationsPerProtocolResult('Error');
		Test.stopTest();
		System.assertEquals(registrationsPerProtocolResult.errorMessage, 'Error');
	}

	@isTest
	private static void notificationsPerProtocolResultMap(){
		Map<String, Integer> notificationsPerProjects = new Map<String, Integer>();
		notificationsPerProjects.put('test', 11);
		Test.startTest();
			PRA_WrappersForMobile.NotificationsPerProtocolResult notificationsPerProtocolResult = new PRA_WrappersForMobile.NotificationsPerProtocolResult(notificationsPerProjects);
		Test.stopTest();
		System.assertEquals(notificationsPerProtocolResult.notificationsPerProjects, notificationsPerProjects);
	}

	@isTest
	private static void notificationsPerProtocolResultError(){
		Test.startTest();
			PRA_WrappersForMobile.NotificationsPerProtocolResult notificationsPerProtocolResult = new PRA_WrappersForMobile.NotificationsPerProtocolResult('Error');
		Test.stopTest();
		System.assertEquals(notificationsPerProtocolResult.errorMessage, 'Error');
	}
}