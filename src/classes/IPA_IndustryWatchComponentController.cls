public class IPA_IndustryWatchComponentController
{
    public List<IPA_Articles__c> lsindustryWatch {get; set;}
    
    public IPA_IndustryWatchComponentController()
    {
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        lsindustryWatch = ipa_bo.returnIndustryWatch();
    }
}