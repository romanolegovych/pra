public with sharing class PAWS_FlowTrigger extends STSWR1.AbstractTrigger
{
	private static Map<String, String> KEY_PREFIX_TO_FIELDNAME_MAP = new Map<String, String>{
		SObjectType.PAWS_Project_Flow_Country__c.keyPrefix => SObjectType.PAWS_Project_Flow_Country__c.Name,
		SObjectType.PAWS_Project_Flow_Site__c.keyPrefix => SObjectType.PAWS_Project_Flow_Site__c.Name,
		SObjectType.PAWS_Project_Flow_Agreement__c.keyPrefix => SObjectType.PAWS_Project_Flow_Agreement__c.Name,
		SObjectType.PAWS_Project_Flow_Document__c.keyPrefix => SObjectType.PAWS_Project_Flow_Document__c.Name,
		SObjectType.PAWS_Project_Flow_Submission__c.keyPrefix => SObjectType.PAWS_Project_Flow_Submission__c.Name
	};
	
	/* Uncomment this code if you're planning to go beyond five PAWS lookup fields and object above.
	static
	{
		KEY_PREFIX_TO_FIELDNAME_MAP = new Map<String, String>();
		for (Schema.SObjectType each : Schema.getGlobalDescribe().values())
		{
			Schema.DescribeSObjectResult description = each.getDescribe();
			KEY_PREFIX_TO_FIELDNAME_MAP.put(description.keyPrefix, description.Name);
		}
	}
	/**/
	
	public override void beforeInsert(List<SObject> records)
	{
		for (STSWR1__Flow__c each : (List<STSWR1__Flow__c>) records)
		{
			populatePAWSLookups(each);
		}
	}
	
	public override void beforeDelete(List<SObject> records)
	{
		cleanupEWs((List<STSWR1__Flow__c>) records);
	}
	
	public override void afterInsert(List<SObject> records)
	{
		//createEWs((List<STSWR1__Flow__c>) records);
	}
	
	public override void beforeUpdate(List<SObject> records, List<SObject> oldRecords)
	{
		beforeInsert(records);
	}
	
	public override void afterUpdate(List<SObject> records, List<SObject> oldRecords)
	{
		updateProperties((List<STSWR1__Flow__c>)records, (List<STSWR1__Flow__c>)oldRecords);
	}
	
	private void populatePAWSLookups(STSWR1__Flow__c flow)
	{
		if (flow.STSWR1__Source_Id__c == null || flow.STSWR1__Source_Id__c.length() < 3)
		{
			return;
		}
		
		String keyPrefix = flow.STSWR1__Source_Id__c.substring(0, 3);
		if (keyPrefix != null
				&& KEY_PREFIX_TO_FIELDNAME_MAP.get(keyPrefix) != null
				&& SObjectType.STSWR1__Flow__c.fields.getMap().keyset().contains(KEY_PREFIX_TO_FIELDNAME_MAP.get(keyPrefix).toLowercase()))
		{
			flow.put(KEY_PREFIX_TO_FIELDNAME_MAP.get(keyPrefix).toLowercase(), flow.STSWR1__Source_Id__c);
		}
	}
	
	private void updateProperties(List<STSWR1__Flow__c> records, List<STSWR1__Flow__c> oldRecords)
	{
		Set<ID> ids = new Set<ID>();
		for(Integer i = 0; i < records.size(); i++)
		{
			if(records[i].STSWR1__Source_Id__c != oldRecords[i].STSWR1__Source_Id__c) ids.add(records[i].Id);
		}
		
		if(ids.size() == 0) return;
		
		if (!Test.isRunningTest()) PAWS_Utilities.checkLimits(new Map<String, Integer> 
		{
			'Queries' => 1,
			'DMLStatements' => 1
		});
		
		update [select Id from STSWR1__Flow_Step_Property__c where Flow__c in :ids];
	}
	
	/*
	public void createEWs(List<STSWR1__Flow__c> flows)
	{
		//Check whether CC_Clone class has been deployed
		if (Type.forName('CC_Clone') == null)
		{
			return;
		}
		
		if (!Test.isRunningTest()) PAWS_Utilities.checkLimits(new Map<String, Integer> 
		{
			'Queries' => 3,
			'DMLStatements' => 3
		});
		
		Map<String, String> oldNewFlowIdMap = new Map<String, String>();
		for (STSWR1__Flow__c each : flows)
		{
			if (each.STSWR1__Parent__c != null)
			{
				oldNewFlowIdMap.put(each.STSWR1__Parent__c, each.Id);
			}
		}
		
		//Cloning Early Warnings
		Object ccHandler = Type.forName('CC_Clone').newInstance();
		if (ccHandler instanceof STSWR1.FlowPostImportInterface)
		{
			((STSWR1.FlowPostImportInterface) ccHandler).execute(oldNewFlowIdMap);
		}
	}*/
	
	public static void cleanupEWs(List<STSWR1__Flow__c> flows)
	{
		Set<ID> flowIDs = new Map<ID, STSWR1__Flow__c>(flows).keyset();
		
		delete [select Name from Critical_Chain__c where Flow__c in :flowIDs];
	}
}