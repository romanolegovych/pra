/**
@description this controller class is to display all Models
@author Niharika Reddy
@date 2015
**/
public class PBB_ModelPageController{
   
   /** 
   * to display the list of region models
   */
   public List <Mapping_Region_Model__c> Region_Models    {get;set;}  
   /** 
   * to display the list of SRM models
   */
   public List <SRM_Model__c> SRMModelsList    { get; set; }  
   /** 
   * to display the list of Service models 
   */          
   public List <Service_Model__c> ServiceModelsList    { get; set; } 
   /**
   * to display the list of WR models   
   */
   public List <WR_Model__c> WRModelsList    { get; set; }              
   /**
   * to select the model name
   */
   public String modelName    {get; set;}
   /**
   * to display the list of Bill Rate Card Models 
   */
   public List<Bill_Rate_Card_Model__c> BRModelsList    { get; set; }
   /**
   * to display the list Bill Rate Formulas Names
   */
   public List<Formula_Name__c> billFormulaList    { get; set; }
   /**
   * to display the list of Service Impact Question Names  
   */
   public List<Service_Impact_Question_Name__c> serviceImpactQList    { get; set; }
   /** 
   @description Constructor
   @author Niharika Reddy
   @date 2015   
   */
   public PBB_ModelPageController(){        
       Region_Models= PBB_DataAccessor.GetAllRegionModels();
       SRMModelsList = PBB_DataAccessor.GetAllSrmModels();
       ServiceModelsList = PBB_DataAccessor.GetAllServicesModels();
       WRModelsList =  PBB_DataAccessor.GetAllWrModels();
       BRModelsList = PBB_DataAccessor.getAllBillModels();
       billFormulaList = PBB_DataAccessor.getAllBillFormulaNames();
       serviceImpactQList = PBB_DataAccessor.getAllServiceImpactQuestionNames();
   } 
   
   /** 
   @description Function to call New Page of required Model
   @author Niharika Reddy
   @date 2015  
   */
   public PageReference getNewPageReference() {
       Schema.DescribeSObjectResult R;
       system.debug('------modelName------'+modelName);
       if(modelName=='Mapping_Region_Model__c'){
           R = Mapping_Region_Model__c.SObjectType.getDescribe();           
       }
       if(modelName=='SRM_Model__c'){
           R = SRM_Model__c.SObjectType.getDescribe();           
       }
       if(modelName=='Service_Model__c'){
           R = Service_Model__c.SObjectType.getDescribe();           
       }
       if(modelName=='WR_Model__c'){
           R = WR_Model__c.SObjectType.getDescribe();        
       }
       if(modelName=='Bill_Rate_Card_Model__c'){
           R = Bill_Rate_Card_Model__c.SObjectType.getDescribe();           
       }
       if(modelName=='Formula_Name__c'){
           R = Formula_Name__c.SObjectType.getDescribe();           
       }
       if(modelName=='Service_Impact_Question_Name__c'){
           R = Service_Impact_Question_Name__c.SObjectType.getDescribe();          
       }
       system.debug('------R------'+R);
       return new PageReference('/' + R.getKeyPrefix() + '/e');
   }
}