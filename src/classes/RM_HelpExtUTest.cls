/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_HelpExtUTest {
    
    static testMethod void TestContructor() {
        Help__c hlp = new Help__c(name='Test', Content__c='Test Content', Page_Name__c = 'RM_Resource');
        insert hlp;
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/RM_help?name=RM_Resource');
     
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(hlp);
        RM_HelpExt hlpExt = new RM_HelpExt(sc);
        Help__c newHlp = hlpExt.getHelp();
        system.assert(newHlp.Content__c != null);
      
      
        Test.stopTest(); 
    }
    static testMethod void TestContructorNoName() {
        Help__c hlp = new Help__c(name='Test', Content__c='Test Content', Page_Name__c = 'RM_Resource');
        insert hlp;
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/RM_help?name=Test');
     
        Test.setCurrentPage(pageRef);
       
        ApexPages.StandardController sc = new ApexPages.StandardController(hlp);
        RM_HelpExt hlpExt = new RM_HelpExt(sc);
        Help__c newHlp = hlpExt.getHelp();
        system.assert(newHlp.Content__c != null);       
       
        Test.stopTest(); 
    }
    
}