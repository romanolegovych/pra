public class Rm_NotificationsController {
	
	public List<String> ListOfGroupNames {get; private set;}	
	public String UserId {get; private set;}	
	

	public Boolean belongsto1Group = False;
    
    public List<PgNotificationRecords> ListOfPageNotifications {get; set;}
    
    public Rm_NotificationsController(){    	
        
        UserId = UserInfo.getUserId();    
  
        ListOfGroupNames = RM_OtherService.GetRMGroupfromUserID(UserId);
        if (ListOfGroupNames != null){
       		if(ListOfGroupNames.size() == 1)
             	belongsto1Group = true;
        }
    }  
    public void ShowAll()
    {
        //DateTime TodayDate = System.Today();
       
        ListOfPageNotifications = new List<PgNotificationRecords>();
        for(WFM_Notification__c loopvar :RM_OtherService.getAllNotification())
        {
            PgNotificationRecords pgRecObj = new PgNotificationRecords(loopvar);           
            ListOfPageNotifications.add(pgRecObj);
        }
    }
        
    public void getwfmnotifications()
    {
    	list<string> lstViewID = new list<string>();
    	for(WFM_Notification_view_log__c v: RM_OtherService.getListNotificationViewLog(ListOfGroupNames, UserID)){
    		lstViewID.add(v.Notification__c);
    	}
    	 
        ListOfPageNotifications = new List<PgNotificationRecords>();
        for(WFM_Notification__c loopvar :RM_OtherService.getUnReadNotification(lstViewID)){
       
            PgNotificationRecords pgRecObj = new PgNotificationRecords(loopvar);  
            ListOfPageNotifications.add(pgRecObj);
        }
            
       // return null;
    }
    
    public PageReference MarkedAsRead()
    {
    	
        
        for(PgNotificationRecords loopvar :ListOfPageNotifications)
        {
            if(loopvar.isChecked == true)
            {
                WFM_Notification_view_log__c wfmLofObj = new WFM_Notification_view_log__c();
                if(belongsto1Group == true)
                wfmLofObj.Group_Name__c = ListOfGroupNames.get(0);
                wfmLofObj.Notification__c = loopvar.Id;
                wfmLofObj.User__c = UserId;
                insert wfmLofObj;
            }
        }       
        
     
	    PageReference PageObj = Page.RM_notifications;
	    PageObj.setRedirect(true);
	    return PageObj;
    }
    
   
	public class PgNotificationRecords
    {
        public Boolean isChecked {get; set;}
        public Datetime createdDate {get; set;}
        public String WFM_ProjectId {get; set;}
        public String WFM_Project_Name {get; set;}
        public String Id {get; set;}
        public String Change_Type {get; set;}
        public String Old_Value {get; set;}
        public String New_Value {get; set;}
        public Decimal AgeIndays {get; set;}
        //public string ProjectIdPlusSponsor {get; set;}
        
        public PgNotificationRecords(WFM_Notification__c notification){
        	createdDate = notification.createdDate;
            WFM_ProjectId = notification.WFM_Project__c;
            WFM_Project_Name = notification.ProjectIdPlusSponsor__c;
            Id = notification.Id;
            Change_Type = notification.Change_Type__c;
            Old_Value = notification.Old_Value__c;
            New_Value = notification.New_Value__c;
            AgeIndays = notification.AgeIndays__c;
            isChecked = false; 
        }
    }
}