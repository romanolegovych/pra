public class AA_CostRateController {

    private final List < Business_Unit__c > businessunits;
    private final List < Function_Code__c > functions;
    private final List < Country__c > countries;

    public List < CostRateWrapper > result {
        get;
        set;
    }
    public String selectedBusinessUnit {
        get;
        set;
    }
    public String selectedFunction {
        get;
        set;
    }
    public String selectedCountry {
        get;
        set;
    }
    public String selectedActiveFlag {
        get;
        set;
    }
    public String selectedYear {
        get;
        set;
    }
    public List < Integer > year {
        get;
        set;
    }
    public String functionId{get; set;}
    public String scheduledYear{get; set;}
    
    public List < BUF_Code__c > bufcodeDetails = new List < BUF_Code__c > ();

    public AA_CostRateController (){
        businessunits = AA_RevenueAllocationService.getBusinessUnitCodes();
        functions = AA_RevenueAllocationService.getFunctionCodes();
        countries = AA_RevenueAllocationService.getCountries();
        

    }

    /* Populate Year in Select List */
    public List < SelectOption > getyearSelectoption() {
            List < SelectOption > yearselectoption = new List < SelectOption > ();
            for (Integer i = 1999;i< Date.Today().Year() + 20; i++)
                yearselectoption.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
            return yearselectoption;
        }
    /* Populate Business Unit Codes in Select List */
    public List < SelectOption > getbuSelectoption() {
            List < SelectOption > buselectoption = new List < SelectOption > ();
            buselectoption.add(new SelectOption('', '- All Business Units -'));
            for (Business_Unit__c entry: businessunits) {
                buselectoption.add(new SelectOption(entry.Id, entry.Name));
            }
            return buselectoption;
        }
        /* Populate Function Codes in Select List */
    public List < SelectOption > getfcSelectoption() {
        List < SelectOption > fcselectoption = new List < SelectOption > ();
        fcselectoption.add(new SelectOption('', '- All Functions -'));
        for (Function_Code__c entry: functions) {
            fcselectoption.add(new SelectOption(entry.Id, entry.Name));
        }
        return fcselectoption;
    }

    /* Populate Countries in Select List */
    public List < SelectOption > getcountrySelectoption() {
            List < SelectOption > countryselectoption = new List < SelectOption > ();
            countryselectoption.add(new SelectOption('', '- All Countries -'));
            for (Country__c entry: countries) {
                countryselectoption.add(new SelectOption(entry.Id, entry.Name));
            }
            return countryselectoption;
        }
        /* Populate Active States in Select List */
    public List < SelectOption > getisactiveSelectoption() {
        List < SelectOption > activeselectoption = new List < SelectOption > ();
        activeselectoption.add(new SelectOption('A', '- Both -'));
        activeselectoption.add(new SelectOption('Y', 'Yes'));
        activeselectoption.add(new SelectOption('N', 'No'));
        return activeselectoption;
    }

     public PageReference goToRateDetail() {
         PageReference pageRef = Page.AA_Inflation_Override;
         pageRef.getParameters().put('functionId',functionId);
         pageRef.getParameters().put('scheduledYear',selectedYear);
         return pageRef;
    }
    public PageReference create() {
         PageReference pageRef = Page.AA_New_Cost_Rate;
         return pageRef;
    }
    public PageReference upload() {
         PageReference pageRef = Page.AA_Cost_Rate_Upload;
         return pageRef;
    }
    public Pagereference submit() {
        result = new List < CostRateWrapper > ();
        String year = selectedYear;
        String bucode = selectedBusinessUnit;
        String functioncode = selectedFunction;
        String country = selectedCountry;
        String active = selectedActiveFlag;

        List < AggregateResult > functions = AA_CostRateService.getFunctionListByFilter(year, bucode, functioncode, country, active);
        Integer index;
        for (AggregateResult c: functions) {
            List < Cost_Rate__c > cost = AA_CostRateService.getFunctionDetails((String) c.get('BUF_Code__c'),year);
            //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'BUF Code : '+  cost[0].BUF_Code__r.Name));
            if (cost.size() > 0) {
                List < Decimal > rates = new List < Decimal > ();
                Cost_Rate__c tempRate = cost[0];
                index = 0 ;
                List <Integer> yearlist = getyear();
                for (Integer  y : yearlist){
                    if(index < cost.size()){
                            if(cost[index].Year_Rate_Applies_To__c == String.valueOf(y) ){                        
                                 rates.add(cost[index].Cost_Rate__c);
                                 index++;
                            }else
                                 rates.add(0.00000);
                        }else{
                         rates.add(0.00000);
                    }
                }
                for (Cost_Rate__c r: cost) {
                    rates.add(r.Cost_Rate__c);
                }
                result.add(new CostRateWrapper(tempRate, rates));
            }
        }
        return null;
    }


    /* Get a list of Years in range */
    public List < Integer > getyear() {
        List < Cost_Rate__c > yearList;
        if (selectedYear == null) {
            Cost_Rate__c leastYear = [SELECT Schedule_Year__c FROM Cost_Rate__c ORDER BY Schedule_Year__c LIMIT 1];
            yearList = [SELECT Schedule_Year__c FROM Cost_Rate__c WHERE Schedule_Year__c = : leastYear.Schedule_Year__c ORDER BY Schedule_Year__c];
        } else {
            yearList = [SELECT Schedule_Year__c FROM Cost_Rate__c WHERE Schedule_Year__c = : selectedYear ORDER BY Schedule_Year__c];
        }
        Integer tempYear = Integer.valueOf(yearList[0].Schedule_Year__c);
        year = new List < Integer > ();
        for (Integer i = 0; i < 16; i++) {
            year.add(tempYear);
            tempYear++;
        }
        return year;
    }


    /* get results */
    public List < CostRateWrapper > getresult() {
        return result;
    }

    /* Wrapper function for results */
    public class CostRateWrapper {
        public Cost_Rate__c costrate {
            get;
            set;
        }
        public Boolean selected {
            get;
            set;
        }
        public List < Decimal > rate {
            get;
            set;
        }

        public CostRateWrapper(Cost_Rate__c c, Decimal[] r) {
            this.costrate = c;
            this.rate = r;
        }
    }

}