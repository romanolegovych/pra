public with sharing class BDT_PageBlockHeader {
	public String goToPageName  {get;set;}
	public String customId      {get;set;}
	public String buttonValue   {get;set;} 
/*	public String [] preferencesToDelete {get{
											  if(preferencesToDelete==null){
														preferencesToDelete = new String []{}; 
														
											  }
											  return preferencesToDelete;} 
										  set;}
*/
    public Boolean getShowButton(){
		try   {return (buttonValue!=null) ? true : false;}
		catch(Exception e) {return false;}
	}
	
	public PageReference goToPage(){
		PageReference thePageToGoTo = new PageReference('/apex/'+goToPageName.toLowerCase()); 
		
//if(!preferencesToDelete.isEmpty()){  
//BDT_Utils.cleanPreferences(preferencesToDelete);
//}
		if(customId!=null && customId!=''){
			thePageToGoTo.getParameters().put('customId',customId);
		}
		thePageToGoTo.setRedirect(true);
		return thePageToGoTo;
	}

}