public with sharing class PAWS_DashboardService
{
    public List<STSWR1__Flow__c> findFlowByIDs(List<String> flowIds)
    {
        return findFlow('ID', flowIds);
    }
    
    public List<STSWR1__Flow__c> findFlowByParentIds(List<String> flowIds)
    {
        List<STSWR1__Flow_Instance__c> flowInstList = [select STSWR1__Flow__c, STSWR1__Is_Active__c, STSWR1__Is_Completed__c, STSWR1__Flow__r.STSWR1__Parent__c from STSWR1__Flow_Instance__c where STSWR1__Flow__r.STSWR1__Parent__c = :flowIds and STSWR1__Is_Active__c = true and STSWR1__Is_Completed__c = 'No'];
		Set<String> flowIdsSet = new Set<String>();
        for(STSWR1__Flow_Instance__c eachInstance : flowInstList)
        {
            if(eachInstance.STSWR1__Flow__r.STSWR1__Parent__c != null)
            {
                flowIdsSet.add(eachInstance.STSWR1__Flow__r.ID); 
            }
        }

        return findFlow('ID', new List<String>(flowIdsSet));
        //return findFlow('STSWR1__Parent__c', flowIds);
    }

    public List<STSWR1__Flow__c> findMasterFlows(String folderName)
    {
        List<STSWR1__Item__c> items = [select Id from STSWR1__Item__c where STSWR1__Parent__c = null and STSWR1__Type__c = 'Folder' and Name = :folderName];
        String folderId;
        if(items.size() > 0)
        {
            folderId = '%' + items[0].Id + '%';
        }
        else
        {
            return new List<STSWR1__Flow__c>();
        }

        List<STSWR1__Flow_Instance__c> flowInstList = 
            [select STSWR1__Flow__c, STSWR1__Is_Active__c, STSWR1__Is_Completed__c, STSWR1__Flow__r.STSWR1__Parent__c
            from STSWR1__Flow_Instance__c where STSWR1__Is_Active__c = true and STSWR1__Is_Completed__c = 'No' 
            and STSWR1__Flow__r.STSWR1__Start_Type__c != 'Triggered by another flow' and STSWR1__Object_Type__c = 'ecrf__c'
            and STSWR1__Flow__c in (select STSWR1__Source_Flow__c from STSWR1__Item__c where STSWR1__Source_Flow__c != null and STSWR1__Path__c like :folderId)];

        Set<String> flowIdsSet = new Set<String>();
        for(STSWR1__Flow_Instance__c eachInstance : flowInstList)
        {
            flowIdsSet.add(eachInstance.STSWR1__Flow__c);
        }

        return findFlow('ID', new List<String>(flowIdsSet));
    }

    public List<STSWR1__Flow__c> findFlow(String fieldName, List<String> fieldValue)
    {
		String query = 'select STSWR1__Parent__c, ID, Name, STSWR1__Object_Type__c, STSWR1__Source_Id__c from STSWR1__Flow__c where ' + fieldName + ' = :fieldValue';
		return (List<STSWR1__Flow__c>)Database.query(query);
    }
    
    public List<STSWR1__Flow_Instance__c> loadFlowInstanceByID(List<String> flowIds)
    {
        return [select 
                    STSWR1__Flow__c, STSWR1__Is_Active__c, STSWR1__Is_Completed__c, STSWR1__Number_of_Active_Cursors__c,
                    STSWR1__Number_of_Cursors__c, STSWR1__Object_Id__c, STSWR1__Object_Name__c, STSWR1__Object_Type__c,
                    STSWR1__Flow__r.STSWR1__Source_Id__c, STSWR1__Flow__r.Name, (select Id from STSWR1__Flow_Instance_Cursors__r)
                from
                    STSWR1__Flow_Instance__c
                where
                    STSWR1__Flow__c = :flowIds
                order by
                    STSWR1__Object_Name__c];
    }
    
    public List<STSWR1__Flow_Instance__c> loadFlowInstanceByParentID(List<String> flowIds)
    {
        return [select 
                    STSWR1__Flow__c, STSWR1__Is_Active__c, STSWR1__Is_Completed__c, STSWR1__Number_of_Active_Cursors__c,
                    STSWR1__Number_of_Cursors__c, STSWR1__Object_Id__c, STSWR1__Object_Name__c, STSWR1__Object_Type__c,
                    STSWR1__Flow__r.STSWR1__Source_Id__c, STSWR1__Flow__r.Name, (select Id from STSWR1__Flow_Instance_Cursors__r)
                from
                    STSWR1__Flow_Instance__c
                where
                    STSWR1__Flow__r.STSWR1__Parent__c = :flowIds and STSWR1__Is_Active__c = true and STSWR1__Is_Completed__c = 'No'
                order by
                    STSWR1__Object_Name__c];
    }
    
    public List<STSWR1__Flow_Instance_Cursor__c> loadCursorInstanceWithHistoryByIds(List<String> cursorIds)
    {
        return [select
                    Id, STSWR1__Flow_Instance__c, STSWR1__Step__r.STSWR1__Flow_Swimlane__c, STSWR1__Flow__c,
                    (select 
                        CreatedDate, STSWR1__Cursor__c, STSWR1__Status__c, STSWR1__Step__c, STSWR1__Step_Name__c,
                        STSWR1__Step__r.STSWR1__Flow_Swimlane__c, STSWR1__Actual_Start_Date__c, STSWR1__Step__r.STSWR1__Duration__c,
                        STSWR1__Cursor__r.STSWR1__Step_Changed_Date__c, STSWR1__Step__r.STSWR1__Index__c, STSWR1__Step__r.STSWR1__Flow_Milestone__r.Name,
                        STSWR1__Step__r.STSWR1__Flow_Milestone__r.STSWR1__Index__c, STSWR1__Step__r.Name
                    from
                        STSWR1__Flow_Instance_History__r
                    order by
                        CreatedDate desc)
                from
                    STSWR1__Flow_Instance_Cursor__c
                where
                    Id = :cursorIds];
    }
    
    public List<STSWR1__Flow_Step_Junction__c> loadFlowStepJunction(List<String> flowIds)
    {
        return [select
                Name, STSWR1__Flow_Milestone__r.Name, STSWR1__Flow_Milestone__r.STSWR1__Index__c, STSWR1__Index__c, STSWR1__Flow__c,
                STSWR1__Flow__r.STSWR1__Source_Id__c,
                (
                    select 
                    	name, STSWR1__Type__c, STSWR1__Status__c, STSWR1__Config__c 
                    from 
                    	STSWR1__Flow_Step_Actions__r 
                    where 
                    	STSWR1__Type__c = 'Start Sub Flow'
                )
            from
                STSWR1__Flow_Step_Junction__c
            where
                STSWR1__Flow__c = :flowIds
            order by
                STSWR1__Flow__c, STSWR1__Flow_Milestone__r.STSWR1__Index__c, STSWR1__Index__c];
    }
    
    public Map<ID, STSWR1__Gantt_Step_Property__c> getFlowPropertyByStepAndObject(Set<ID> stepIds)
    {
    	return (Map<ID, STSWR1__Gantt_Step_Property__c>)new STSWR1.API().call('GanttStepPropertyService', 'getStepsProperties', new Map<String, Object> {'stepIds' => stepIds});	
    }
    
    public void updateFlowObject(Map<ID, PAWS_UpsDashboardController.FlowWrapper> inFlowWrapperMap)
    {
        Map<ID, List<PAWS_UpsDashboardController.FlowWrapper>> objectIdFlowWrapperMap = new Map<ID, List<PAWS_UpsDashboardController.FlowWrapper>>();
        Map<String, List<String>> objectTypeObjectIdsMap = new Map<String, List<String>>();

        for(PAWS_UpsDashboardController.FlowWrapper eachFlowWrap : inFlowWrapperMap.values())
        {
            String objectId = eachFlowWrap.FlowInstance == null ? eachFlowWrap.Flow.STSWR1__Source_Id__c : eachFlowWrap.FlowInstance.STSWR1__Object_Id__c;
            String objectType = eachFlowWrap.FlowInstance == null ? eachFlowWrap.Flow.STSWR1__Object_Type__c : eachFlowWrap.FlowInstance.STSWR1__Object_Type__c;
            if(objectId != null && objectType != null)
            {
                List<PAWS_UpsDashboardController.FlowWrapper> flowList = objectIdFlowWrapperMap.get(objectId);
                if(flowList == null)
                {
                    flowList = new List<PAWS_UpsDashboardController.FlowWrapper>();
                    objectIdFlowWrapperMap.put(objectId, flowList);
                }
                flowList.add(eachFlowWrap);
                
                List<String> objectIds = objectTypeObjectIdsMap.get(objectType);
                if(objectIds == null)
                {
                    objectIds = new List<String>();
                }
                objectIds.add(objectId);
                objectTypeObjectIdsMap.put(objectType, objectIds);
            }
        }
        
        for(String eachObjectType : objectTypeObjectIdsMap.keySet())
        {
            List<String> objectIds = objectTypeObjectIdsMap.get(eachObjectType);
            String query = 'select Id, Name from ' + eachObjectType + ' where ID = :objectIds';
            for(sObject eachFlowObject : Database.query(query))
            {
                for(PAWS_UpsDashboardController.FlowWrapper flwWrapperEach : objectIdFlowWrapperMap.get(eachFlowObject.Id))
                {
                    flwWrapperEach.FlowObject = eachFlowObject;
                }
            }
        }
    }
    
    public Map<ID, PAWS_UpsDashboardController.ProjectWrapper> getAllRelatedObjects(Map<ID, PAWS_UpsDashboardController.FlowWrapper> inFlowWrapperMap)
    {
		Set<ID> projectIds = new Set<ID>();
        for(PAWS_UpsDashboardController.FlowWrapper eachFlowWrap : inFlowWrapperMap.values())
        {
            if(eachFlowWrap.FlowInstance != null && eachFlowWrap.FlowInstance.STSWR1__Object_Type__c == 'ecrf__c')
            {
                projectIds.add(eachFlowWrap.FlowInstance.STSWR1__Object_Id__c);
            }
        }

        Map<ID, PAWS_UpsDashboardController.ProjectWrapper> objectIdFlowWrapperMap = new Map<ID, PAWS_UpsDashboardController.ProjectWrapper>();
        Map<ID, PAWS_UpsDashboardController.CountryWrapper> contriesMap = new Map<ID, PAWS_UpsDashboardController.CountryWrapper>();
        for(ecrf__c eachProject : [select Name, (select Name from PAWS_Countries__r) from ecrf__c  where ID = :projectIds])
        {
            PAWS_UpsDashboardController.ProjectWrapper project = new PAWS_UpsDashboardController.ProjectWrapper(eachProject); 
            objectIdFlowWrapperMap.put(eachProject.ID, project);
            for(PAWS_Project_Flow_Country__c eachCountry : eachProject.PAWS_Countries__r)
            {
                PAWS_UpsDashboardController.CountryWrapper country = project.addCountry(eachCountry);
                contriesMap.put(eachCountry.ID, country);
            }
        }
        
        Map<ID, PAWS_UpsDashboardController.SiteWrapper> sitesMap = new Map<ID, PAWS_UpsDashboardController.SiteWrapper>();
        for(PAWS_Project_Flow_Site__c eachSite : [select Name, PAWS_Country__c, (select Name from PAWS_Agreements__r), (select Name from PAWS_Documents__r), (select Name from PAWS_Submissions__r) from PAWS_Project_Flow_Site__c where PAWS_Country__c = :contriesMap.keySet()])
        {
            PAWS_UpsDashboardController.CountryWrapper country = contriesMap.get(eachSite.PAWS_Country__c);
            PAWS_UpsDashboardController.SiteWrapper site = country.addSite(eachSite);
            sitesMap.put(eachSite.ID, site);
        }
        
        return objectIdFlowWrapperMap;
    }
}