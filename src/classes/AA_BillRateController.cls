/**  
@Author Abhishek Prasad
@Date 2015
@Description This is the controller for Bill Rate Search Page
*/
public class AA_BillRateController {

    private final List < Bill_Rate_Card_Model__c> billRateModels;
    private final List < AggregateResult > jobPositions;
    private final List < Country__c > countries;
    
    public String selectedModel{
        get;
        set;
    }
    public String selectedJobPosition{
        get;
        set;
    }
    
    public List < BillRateWrapper > resultfinal {
        get;
        set;
    }

    public String selectedCountry {
        get;
        set;
    }
    public String selectedActiveFlag {
        get;
        set;
    }
    public String selectedYear {
        get;
        set;
    }
    public List < Integer > year {
        get;
        set;
    }
    
    public String jobPositionSelected{get; set;}
    public String countrySelected{get; set;}
    public String scheduledYearSelected{get; set;}
    public String modelIdSelected{get; set;}
    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description Constructer
    */
    public AA_BillRateController (){
        countries = AA_RevenueAllocationService.getCountries();
        billRateModels = AA_BillRateDataAccessor.getModels();
        jobPositions = AA_BillRateDataAccessor.getJobPositions();
    }
    
    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description Populate Models in Select List 
    */
    public List < SelectOption > getModelSelectoption() {
            List < SelectOption > modelselectoption = new List < SelectOption > ();
            modelselectoption.add(new SelectOption('', '- All Models -'));
            for (Bill_Rate_Card_Model__c entry: billRateModels ) {
                modelselectoption.add(new SelectOption(entry.Id, entry.Name));
            }
            return modelselectoption;
        }
        

    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description Populate Job Position in Select List 
    */
    public List < SelectOption > getJobPositionSelectoption() {
            List < SelectOption > jobpositionselectoption = new List < SelectOption > ();
            jobpositionselectoption.add(new SelectOption('', '- All Job Positions -'));
            for (AggregateResult entry: jobPositions ) {
                jobpositionselectoption.add(new SelectOption((String)entry.get('Id'),(String)entry.get('Name')));
            }
            return jobpositionselectoption;
        }
        
    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description Populate Year in Select List 
    */
    public List < SelectOption > getyearSelectoption() {
            List < SelectOption > yearselectoption = new List < SelectOption > ();
            for (Integer i = 1999;i< Date.Today().Year() + 20; i++)
                yearselectoption.add(new SelectOption(String.valueOf(i), String.valueOf(i)));
            return yearselectoption;
        }



    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description Populate Countries in Select List 
    */
    public List < SelectOption > getcountrySelectoption() {
            List < SelectOption > countryselectoption = new List < SelectOption > ();
            countryselectoption.add(new SelectOption('', '- All Countries -'));
            for (Country__c entry: countries) {
                countryselectoption.add(new SelectOption(entry.Id, entry.Name));
            }
            return countryselectoption;
        }
        
    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description Populate Flags in Select List 
    */
    public List < SelectOption > getisactiveSelectoption() {
        List < SelectOption > activeselectoption = new List < SelectOption > ();
        activeselectoption.add(new SelectOption('A', '- Both -'));
        activeselectoption.add(new SelectOption('Y', 'Yes'));
        activeselectoption.add(new SelectOption('N', 'No'));
        return activeselectoption;
    }
    
    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description Edit link Click
    */
     public PageReference goToRateDetail() {
     
        
         PageReference pageRef = Page.AA_BillRate_InflationOverride; 
         pageRef.getParameters().put('jobPosition',jobPositionSelected);
         pageRef.getParameters().put('country',countrySelected);
         pageRef.getParameters().put('scheduledYear',scheduledYearSelected);
         pageRef.getParameters().put('modelId',modelIdSelected);
         
         return pageRef ;
    }
    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description Search Button Click
    */
    public Pagereference submitFilter() {
        resultfinal = new List < BillRateWrapper > ();

        String model = selectedModel;
        String jobposition = selectedJobPosition;
        String country = selectedCountry;
        String active = selectedActiveFlag;
        String year = selectedYear;
        
        List < AggregateResult > rateList= AA_BillRateDataAccessor.getRateListByFilter(model, jobposition, country, year,active);
        Integer index;
        for (AggregateResult c: rateList) {
            List < Bill_Rate__c > cost = AA_BillRateDataAccessor.getRateDetails((String) c.get('Model_Id__c'),(String) c.get('Job_Position__c'),(String) c.get('Country__c'),(String) c.get('Schedule_Year__c'),'Y');
            
            if (cost.size() > 0) {
                List < Decimal > rates = new List < Decimal > ();
                Bill_Rate__c tempRate = cost[0];
                index = 0 ;
                List <Integer> yearlist = getyear();
                for (Integer  y : yearlist){
                    if(index < cost.size()){
                            if(cost[index].Year_Rate_Applies_To__c == String.valueOf(y) ){                        
                                 rates.add(cost[index].Bill_Rate__c);
                                 index++;
                            }else
                                 rates.add(0.00000);
                        }else{
                         rates.add(0.00000);
                    }
                }
                for (Bill_Rate__c r: cost) {
                    rates.add(r.Bill_Rate__c );
                }
                resultfinal.add(new BillRateWrapper(tempRate, rates));
            }
        }
        return null;
    }

    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description Get List of Years in Range
    */
    public List < Integer > getyear() {
        List < Bill_Rate__c > yearList;
        if (selectedYear == null) {
            Bill_Rate__c leastYear = [SELECT Schedule_Year__c FROM Bill_Rate__c ORDER BY Schedule_Year__c LIMIT 1];
            yearList = [SELECT Schedule_Year__c FROM Bill_Rate__c WHERE Schedule_Year__c = : leastYear.Schedule_Year__c ORDER BY Schedule_Year__c];
        } else {
            yearList = [SELECT Schedule_Year__c FROM Bill_Rate__c WHERE Schedule_Year__c = : selectedYear ORDER BY Schedule_Year__c];
        }
        
        year = new List < Integer > ();
        
        if(yearList != null){
            if(yearList.size() > 0 ){
                Integer  tempYear = Integer.valueOf(yearList[0].Schedule_Year__c);
                for (Integer i = 0; i < 16; i++) {
                    year.add(tempYear);
                    tempYear++;
                }
            }
        }
       return year;
    }
        


    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description Get Results
    */
    public List < BillRateWrapper > getresult() {
        return resultFinal;
    }
    
    /** 
    @Author Abhishek Prasad
    @Date 2015
    @Description Wrapper Class for Bill Rates
    */
    public class BillRateWrapper {
        public Bill_Rate__c billrate {
            get;
            set;
        }
        public Boolean selected {
            get;
            set;
        }
        public List < Decimal > rate {
            get;
            set;
        }

        public BillRateWrapper (Bill_Rate__c c, Decimal[] r) {
            this.billrate = c;
            this.rate = r;
        }
    }
   

}