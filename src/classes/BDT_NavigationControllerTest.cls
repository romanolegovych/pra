@isTest
private class BDT_NavigationControllerTest {

    static testMethod void myUnitTest() {
    	
    	Test.setCurrentPage(new PageReference('BDT_SingleProject'));
        BDT_NavigationController NC = new BDT_NavigationController();
        System.assert(NC.ShowNavigation);
        
        // change the main category
        NC.NavigationFunction = 'Administration';
        NC.buildNavigationCategories();
        
        // change the category
        NC.selectedCategory = 'Sources';
        NC.displayNavigationItems();
        
        
        // test none BDT page
        Test.setCurrentPage(new PageReference('PRA_OverallProject'));
        NC = new BDT_NavigationController();
        System.assert(!NC.ShowNavigation);
        
    }
}