public with sharing class PRA_ChangeOrderAddNewClientUnitExt {
    
    public String PEED{get;set;} 
    public Change_Order__c changeOrder{get;set;} 
    public Date EED{get;set;} 
    private Change_Order_Item__c changeOrderItem;
    private list<Client_Task__c> clientUnitNumbers {get;set;}
    private list<Change_Order_Item__c> coItems{get;set;}
    private set<String> clientUnitNos {get;set;}
    private set<String> comboCodes {get;set;} 
    public set<ID> PCRID{get;set;}  
    public String prid  {get;set;}//To Capture the region selected from Picklists. 
   
    public String strSelectedRegion{get;set;}
    public String strSelectedTaskGroup{get;set;}
    
    public String Regionquery;
    public boolean bRAll{get;set;}
    public string hidelist{get;set;}    
   
    //Get all Regions 
    public list<selectoption> getProjectRegionListAll(){
        prid=strSelectedRegion; 
        list<selectoption> options = new list<selectoption>();
        options.add(new SelectOption('',''));
         for(Project_Client_Region__c oa:[ SELECT id,name FROM Project_Client_Region__c  order by Name asc])  {
            if((oa.Name)!='' && oa.name!=null) {                
                options.add(new SelectOption(oa.id,oa.name));
            }
        }
        return options;
    }
    
    //Get All Project based Regions
    public list<selectoption> getProjectRegionList(){
        list<selectoption> options = new list<selectoption>();
        options.add(new SelectOption('',''));
         for(Project_Client_Region__c oa: database.query(Regionquery))  {
            if((oa.Name)!='' && oa.name!=null) {                
                options.add(new SelectOption(oa.id,oa.name));
            }
        }
        return options;
    }
    public list<selectoption> getTaskGroupList(){
        list<selectoption> options = new list<selectoption>();
        options.add(new SelectOption('',''));
        String query =  'SELECT id,name FROM Task_group__c where status__c INCLUDES (\''+PRA_Constants.TG_PF+'\')';
        query += ' order by name asc';
        system.debug('---query---'+query);
        for(Task_group__c oa: Database.query(query)) {
            if((oa.Name)!='' && oa.name!=null) {                
                options.add(new SelectOption(oa.id,oa.name));
            }
        }
        return options;
    }  
    
    public PRA_ChangeOrderAddNewClientUnitExt(Apexpages.Standardcontroller stdController) {
        changeOrderItem = (Change_Order_Item__c)stdController.getRecord();
        
        if(Apexpages.currentPage().getParameters().get('coId') != null) {
            changeOrderItem.Change_Order__c = Apexpages.currentPage().getParameters().get('coId');
            changeOrder = 
                [SELECT Id, Name, Proposed_Estimated_End_Date__c, Client_Project__r.Estimated_End_Date__c, Client_Project__r.Contracted_End_Date__c,
                    Client_Project__r.Name FROM Change_Order__c WHERE Id = :changeOrderItem.Change_Order__c LIMIT 1];
            clientUnitNumbers = 
                [select Client_Unit_Number__c, Combo_Code__c from Client_Task__c where Project__r.Name = :changeOrder.Client_Project__r.Name];
            coItems = 
                [select Client_Unit_Number__c, Combo_Code__c from Change_Order_Item__c where Change_Order__c = :changeOrder.Id 
                    and Client_Task__c = null];
                    
                    
            PCRID=new Set<ID>();
            for(client_task__c ct:[select Project_Region__c from client_task__c where project__r.name= :changeOrder.Client_Project__r.name]){
                PCRID.add(ct.Project_Region__c);
            } 
            //PCRID.add(changeOrderItem.Project_Region__c);
            Regionquery='SELECT id,name FROM Project_Client_Region__c where id in:PCRID  order by Name asc'; //get all project based regions only
            hidelist='hide';      //by default hide all Non project Related Regions   
                            
            clientUnitNos = new set<String>();
            comboCodes = new set<String>(); 
            if(clientUnitNumbers.size() > 0) {
                for(Client_Task__c ct : clientUnitNumbers) {
                    if(!clientUnitNos.contains(ct.Client_Unit_Number__c)) {
                        clientUnitNos.add(ct.Client_Unit_Number__c.toUpperCase());
                    }
                    if(!comboCodes.contains(ct.Combo_Code__c)) {
                        comboCodes.add(ct.Combo_Code__c);
                    }
                }
            }
            if(coItems.size() > 0) {
                for(Change_Order_Item__c coi : coItems) {
                    if(!clientUnitNos.contains(coi.Client_Unit_Number__c)) {
                        clientUnitNos.add(coi.Client_Unit_Number__c.toUpperCase());
                    }
                    if(!comboCodes.contains(coi.Combo_Code__c)) {
                        comboCodes.add(coi.Combo_Code__c);
                    }
                }
            }
            
            PEED = '';
            if(changeOrder.Proposed_Estimated_End_Date__c != null)
                PEED = String.valueof(changeOrder.Proposed_Estimated_End_Date__c); 
            else {
                if(changeOrder.Client_Project__r.Estimated_End_Date__c != null)
                    PEED=String.valueof(changeOrder.Client_Project__r.Estimated_End_Date__c);
                else {
                    if(changeOrder.Client_Project__r.Contracted_End_Date__c != null)
                        PEED=String.valueof(changeOrder.Client_Project__r.Contracted_End_Date__c);
                    else
                        Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Project has no contracted date'));                
                }
            }
            if(PEED != null) 
                EED = Date.valueof(PEED); 
                
                
        }
    }
    
    public pagereference saveCOItem() {
        String errorMsg = '';
        strSelectedRegion=prid;    
        try {
            if(strSelectedRegion!='' && strSelectedRegion!=null && strSelectedTaskGroup!='' && strSelectedTaskGroup!=null ){
            if(!clientUnitNos.contains(changeOrderItem.Client_Unit_Number__c.toUpperCase()) && !comboCodes.contains(changeOrderItem.Combo_Code__c)) {
                
                changeOrderItem.Project_Region__c=strSelectedRegion;
                changeOrderItem.Task_Group__c=strSelectedTaskGroup;
                insert changeOrderItem ;  
                system.debug('-----------' + changeOrderItem);
                changeOrder = [SELECT Id, Name, Proposed_Estimated_End_Date__c, Client_Project__r.Estimated_End_Date__c, Client_Project__r.Contracted_End_Date__c,
                                Client_Project__r.Name FROM Change_Order__c WHERE Id = :changeOrderItem.Change_Order__c LIMIT 1];
                Date d = date.valueof(PEED);
                system.debug('-------d----' + d);
                if(changeOrderItem.New_Client_End_Date__c > d) {
                    changeOrder.Proposed_Estimated_End_Date__c = changeOrderItem.New_Client_End_Date__c;
                    system.debug('----inn-------' + changeOrder.Proposed_Estimated_End_Date__c);
                    update changeOrder;
                }            
                
                PageReference pageRef = new PageReference('/apex/PRA_Change_Order_Modify_client_unit?id=' + changeOrderItem.Id);
                pageRef.setRedirect(true);
                return pageRef;     
            } else {
                Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.WARNING, 'New Client Units cannot contain client unit numbers or combo codes that already exist.'));
            }
         }
         else{
              if(strSelectedTaskGroup==null || strSelectedTaskGroup=='')
                  Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Error, 'Missing Task Group'));
              else
                  Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Error, 'Missing Project Region'));
         }
         
         } catch (Exception ex) {            
            ApexPages.addMessages(ex);      
         } 
         
         
         return null;     
    }
}