public class IPA_UI_ArticleDetails
{
    public IPA_Articles__c ipa_ar {get; set;}
    public String showImgs {get; set;}
    public string titleclass {get; set;}
    public string titleiconclass {get; set;}
    public string displayArea {get; set;}
    
    public IPA_UI_ArticleDetails()
    {
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        ipa_ar = new IPA_Articles__c();
        showImgs = 'display: none';
        titleclass = 'widget-title-gradient';
        titleiconclass = 'widget-title-gradient-newsicon';
        displayArea = '';            
               
        String  ipa_ar_URLId = ApexPages.currentPage().getParameters().get('Id');
        system.debug('******ipa_ar_URLId  :' +ipa_ar_URLId );
        
        if(ipa_ar_URLId != '')
        {
            ipa_ar = ipa_bo.returnArticleDetails(ipa_ar_URLId);
                            
            if(ipa_ar.Image_1__c != null && ipa_ar.Image_1__c != '')
                showImgs = '';
                
            if(ipa_ar.Display_Area__c == 'Corporate Updates' || ipa_ar.Display_Area__c == 'Integration Updates')
            {
                titleclass = 'widget-title-gradient-policy';
                titleiconclass = 'widget-title-gradient-policy-icon';
            }
            
            if(ipa_ar.RecordType.Name == 'Event' || ipa_ar.RecordType.Name == 'Holiday')
            {
                titleclass = 'widget-title-gradient-orange';
                titleiconclass = 'widget-title-gradient-eventdetail-icon';
            }
            
            //Set content type title to be shown
            if(ipa_ar.RecordType.Name == 'Event')
                displayArea = 'Event Detail';
            else if(ipa_ar.RecordType.Name == 'Holiday')
                displayArea = 'Holiday Detail';
            else if(ipa_ar.Display_area__c == 'PRA NEWS – FEATURED ARTICLE')
                displayArea = 'PRA NEWS – FEATURED ARTICLE';
            else if(ipa_ar.Display_area__c == 'PRA NEWS')
                displayArea = 'PRA NEWS';
            else if(ipa_ar.Display_area__c == 'EMPLOYEE NEWS')
                displayArea = 'EMPLOYEE NEWS';
            else if(ipa_ar.Display_area__c == 'CORPORATE UPDATES')
                displayArea = 'CORPORATE UPDATES';
            else if(ipa_ar.Display_area__c == 'INTEGRATION UPDATES')
                displayArea = 'INTEGRATION UPDATES';
        }
        else
        {
            IPA_Articles__c ipa_event = new IPA_Articles__c();
            ipa_event.Display_Date__c = Date.today();
            ipa_ar = ipa_event.clone();
        }   
   }
}