/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PRA_OverallProjectControllerTest {

    static testMethod void myUnitTest() {
    	
    	PRA_TestUtils tu = new PRA_TestUtils();
    	tu.initAll();
    	// TO DO: implement unit test
        Test.startTest();
                   
        PRA_OverallProjectController obj = new PRA_OverallProjectController();
         
	    obj.projectText = tu.clientProject.Name;
	    obj.getSearchOnType();  
	    obj.getSearchOnView();
         
        obj.Selectedview = 'Cumulative View';
        obj.SelectedType = 'Hours';
        obj.getgraphs();        
        obj.searchproject();
        obj.SearchgraphBasedOnHours();
         
        obj.Selectedview = 'Month';
        obj.SelectedType = 'Hours';
        obj.getgraphs();        
        obj.searchproject();
        obj.SearchgraphBasedOnHours();
         
        obj.Selectedview = 'Cumulative View';
        obj.SelectedType = 'Cost';
        obj.getgraphs();        
        obj.searchproject();
        obj.SearchgraphBasedOncost();
        
        obj.Selectedview = 'Month';
        obj.SelectedType = 'Cost';
        obj.getgraphs();        
        obj.searchproject();
        obj.SearchgraphBasedOncost();  
         
        PRA_AutoCompleteController.findSObjects('Client_Project__c', 'Name', 'test', 'Country', '');
        PRA_AutoCompleteController.findSObjects('', '', '', '', '');
                  
        Test.stopTest();
    }
}