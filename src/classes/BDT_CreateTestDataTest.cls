@isTest (seeAllData=false)
private class BDT_CreateTestDataTest {
	
	static testMethod void createFlowchartDesign() { 
		BDT_CreateTestData.createall();
		List<Service__c> services = [select id, name from Service__c LIMIT 10];
		BDT_CreateTestData.buildServiceRisks(services);
		List<Account> account = [select id, name from Account where Name = 'Janssen Pharmaceutica'  LIMIT 1 ];
		List<Client_Project__c> myProjectList = BDT_CreateTestData.buildProjects(account);		
		List<Business_Unit__c> buList = [select id, name from Business_Unit__c];
		List<Study__c> studies = BDT_CreateTestData.buildStudies(myProjectList, buList);
		List<RateCard__c> rateCards = BDT_CreateTestData.buildRateCard(buList, services);
		List<Subcontractor__c> theSubcontractors = BDT_CreateTestData.BuildSubcontractors();
		BDT_CreateTestData.BuildSubcontractorActivities(theSubcontractors);
		BDT_CreateTestData.BuildCountries();
		BDT_CreateTestData.buildTherapeuticAreas();
	}
}