/** Implements the Selector Layer of the object FinancialDocumentContent__c
 * @author	Dimitrios Sgourdos
 * @version	11-Feb-2014
 */
public with sharing class BDT_FinancialDocumentContentDataAccessor {
	
	/** Object definition for fields used in application for FinancialDocumentContent
	 * @author	Dimitrios Sgourdos
	 * @version 11-Feb-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('FinancialDocumentContent__c');
	}
	
	
	/** Object definition for fields used in application for FinancialDocumentContent with the parameter referenceName 
	 *	as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 11-Feb-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'ContactDetails__c,';
		result += referenceName + 'FinancialDocument__c,';
		result += referenceName + 'ProjectConsiderations__c,';
		result += referenceName + 'SelectCheckJSON__c,';
		result += referenceName + 'StrategicInformation__c';
		
		return result;
	}
	
	
	/** Retrieve the FinancialDocumentContent with the given financial document id.
	 * @author	Dimitrios Sgourdos
	 * @version 11-Feb-2014
	 * @param	documentId			The id of the financial document
	 * @return	The retrieved FinancialDocumentContent.
	 */
	public static FinancialDocumentContent__c getContentByDocumentId(String documentId) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM FinancialDocumentContent__c ' +
								'WHERE FinancialDocument__c = {1} ' +
								'LIMIT 1',
								new List<String> {
									getSObjectFieldString(),
									'\'' + documentId + '\''
								}
							);
		
		try {
			return (FinancialDocumentContent__c) Database.query(query);
		} catch (Exception e) {
			system.debug('Query error: ' + query);
			return null;
		}
	}
}