public with sharing class BDT_SponsorsController {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Administration Sponsors
	public List<Account> sponsorEntityPerSponsor{get;set;}
	public List<Sponsor__c> sponsorList{get;set;}
	public String goToPageName {get;set;}
	public String selectedsponsorName {get;set;}
	public String sponsorEntityId {get;set;}
	public String selectedSponsorId {get;set;}
	public Boolean showDeleted{
		get {if (showDeleted == null) {
				showDeleted = false;
			} 
			return showDeleted;
		}
		set;
	}
	public String sponsorId {get;set;}
  		
	public BDT_SponsorsController(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Sponsors');
		SponsorList = new List<Sponsor__c>();
		sponsorId = System.currentPageReference().getParameters().get('sponsorId');  		
		
		try{
			
			SponsorList = [Select s.Name, s.Id, s.Abbreviation__c 
							From Sponsor__c s
							Where ((BDTDeleted__c  = :showDeleted) or (BDTDeleted__c  = false))
							order by Name
							];
			
			if(sponsorId!=null && sponsorId!=''){
				getSponsorEntityPerSponsorSelected(sponsorId);
				for(Sponsor__c sp: SponsorList){
					if(sp.id == sponsorId){
						selectedsponsorName = sp.name;
						selectedSponsorId = sp.id;
					}
				}
			}
								
		}catch(QueryException e){
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, 'No Sponsor could be found. ');
        		ApexPages.addMessage(msg); 
		}
	
	}
	//Method to be used only from link
	public void getSponsorEntityPerSponsor(){
		
		selectedSponsorId = System.currentPageReference().getParameters().get('selectedSponsorId');
		selectedsponsorName = System.currentPageReference().getParameters().get('selectedSponsorName');
						
		getSponsorEntityPerSponsorSelected(selectedSponsorId);		
	}
	
	public void getSponsorEntityPerSponsorSelected(String aSelectedSponsorId){
		sponsorEntityPerSponsor = new List<Account>();
		sponsorEntityPerSponsor.clear();
		sponsorEntityPerSponsor = [Select a.Id, a.Client_Abbreviation__c, a.BillingStreet, a.BillingState, 
									a.BillingPostalCode, a.BillingCountry, a.BillingCity, a.name,
									a.Sponsor__r.Abbreviation__c, a.CurrencyLookup__r.Name, a.CountryLookup__r.Name
									From Account a  
									Where Sponsor__c=: aSelectedSponsorId
									order by name];
	}
	
	public PageReference EditSponsor(){
		selectedSponsorId = System.currentPageReference().getParameters().get('selectedSponsorId');
		
		return new PageReference(System.Page.BDT_NewEditSponsor.getUrl()+'?sponsorId='+selectedSponsorId);
		
	}
	
	public PageReference EditSponsorEntity(){
		sponsorEntityId = System.currentPageReference().getParameters().get('sponsorEntityId');
		selectedSponsorId = System.currentPageReference().getParameters().get('selectedSponsorId');
		
		PageReference newEditSponsorEntityPage = new PageReference(System.Page.BDT_NewEditSponsorEntity.getUrl());
				
		if(sponsorEntityId!=null && sponsorEntityId!=''){
			newEditSponsorEntityPage.getParameters().put('sponsorEntityId', sponsorEntityId);
		}
				
		return newEditSponsorEntityPage;
	}

}