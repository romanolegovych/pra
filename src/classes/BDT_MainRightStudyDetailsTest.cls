@isTest
private class BDT_MainRightStudyDetailsTest {
	
	public static Client_Project__c firstProject {get;set;}
	public static List<Sponsor__c> mySponsorList {get;set;}
	public static List<Account> mySponsorEntityList  {get;set;}
	public static List<Business_Unit__c> myBUList  {get;set;}
	public static List<Study__c> stydiesList {get;set;}	
	public static List<Site__c> mySiteList  {get;set;}

	
	static void init(){		
		
		mySponsorList = BDT_TestDataUtils.buildSponsor(1);		
		insert mySponsorList;
		
		mySponsorEntityList = BDT_TestDataUtils.buildSponsorEntity(mySponsorList);
		insert mySponsorEntityList;
				
		myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert myBUList;
		
		firstProject = BDT_TestDataUtils.buildProject();
		firstProject.Client__c = mySponsorEntityList[0].Id;
		insert firstProject;
		
		mySiteList = BDT_TestDataUtils.buildSite(myBUList);
		insert mySiteList;
		
		stydiesList = BDT_TestDataUtils.buildStudies(firstProject);
		stydiesList[0].Business_Unit__c = myBUList[0].Id; 
		stydiesList[1].Business_Unit__c = myBUList[1].Id; 
		stydiesList[2].Business_Unit__c = myBUList[2].Id;
		stydiesList[3].Business_Unit__c = myBUList[3].Id; 
		stydiesList[4].Business_Unit__c = myBUList[4].Id;
		insert stydiesList;
		
		BDT_Utils.setPreference('SelectedProject', firstProject.id);
		BDT_Utils.setPreference('SelectedStudies',stydiesList[0].id);
		
	}
	
	static testMethod void getSelectedProjectAndSetStudyPreferences() { 
		init();
		
		BDT_MainRightStudyDetails mainRightStudyDet = new BDT_MainRightStudyDetails();
		
		for(BDT_MainRightStudyDetails.studyWrapper stu: mainRightStudyDet.RelatedStudyWrapperList){
			stu.selected = true;
		}
		
		BDT_MainRightStudyDetails.toggleStudy(stydiesList[0].id);
		ApexPages.currentPage().getParameters().put('selectedProjectId',firstProject.id); 	
	}
	
	static testMethod void removeProjectfromQuickList() {
		init();
		BDT_MainRightStudyDetails mainRightStudyDet = new BDT_MainRightStudyDetails();
		ApexPages.currentPage().getParameters().put('deleteProjectId',firstProject.id); 		
		mainRightStudyDet.deleteProjectFromQuickList();
	}
	
}