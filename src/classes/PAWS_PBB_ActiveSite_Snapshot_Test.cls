@isTest
private class PAWS_PBB_ActiveSite_Snapshot_Test
{	
	@isTest
	static void shouldCreateActiveSiteSnapshot()
	{
		setupTestData();

		Test.startTest();
		PAWS_PBB_ActiveSite_Snapshot.startBatch();
		Test.stopTest();

		List<PAWS_Project_Flow_Site_Snapshot__c> snapshotList = [select Id from PAWS_Project_Flow_Site_Snapshot__c];
		System.assert(snapshotList.size() == 1);
	}
	
	private static void setupTestData()
    {
        PAWS_ApexTestsEnvironment.init();

        ecrf__c pawsProject = PAWS_ApexTestsEnvironment.Project;

        PAWS_Project_Flow_Site__c projectSite = PAWS_ApexTestsEnvironment.ProjectSite;
        projectSite.PAWS_Project__c = pawsProject.ID;
        update projectSite;
    }
}