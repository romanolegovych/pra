global class LMS_CourseToProjectController {

    /******************************************* 
     *  Code for : CourseToProjectController   *
     *  Project : LMS                          *    
     *  Author : Andrew Allen                  * 
     *  Last Updated Date : 8/30/2013          * 
     ******************************************/

    //Instance Variables
    public String commitDate{get;set;}
    public String courseUpdateStart{get;set;}
    public String courseUpdateEnd{get;set;}
    public String roleName{get;set;}
    public String roleNameStyle{get;private set;}
    public String roleFilter{get;private set;}
    public String clientText{get;set;}
    private String client{get;set;}
    private String project{get;set;}
    public String projectText{get;set;}
    public String projectRole{get;set;}
    public String projectFilter{get;set;}
    public String searchFilter{get;set;}
    public String searchFilterPackage{get;private set;}
    public String courseTextStyle{get;set;}
    public String courseType{get;set;}
    public String courseCode{get;set;}
    public String courseText{get;set;}
    public String courseTextPackage{get;set;}
    public String courseTextStylePackage{get;private set;}
    public String courseErrorText{get;set;}
    public String roleNameDisplay{get;set;}
    public String assignment{get;set;}
    public String roleResultId{get;private set;} 
    public String createRoleError{get;private set;} 
    public String webServiceError{get;private set;}
    public String impactChange{get;set;}
    public Boolean doSendRole{get;set;}
    public Boolean blnUpdated{get;set;}
    public Boolean allChecked{get;set;}
    public Boolean allCheckedCourse{get;set;}
    public Boolean bCreateRole{get;set;}
    public Boolean bWebServiceError{get;private set;}

    // For impact
    public Integer employeeCount{get;set;}
    public Decimal impactStart{get;set;}
    public Decimal impactEnd{get;set;}
    private Decimal impactNum{get;set;}
    public Boolean renderImpact{get;set;}

    //Collections
    public List<LMS_CourseAssignments> courses{get;set;}
    public List<LMS_CourseList> selCourses{get;set;}
    public List<LMS_Role__c> roles{get;set;}
    private Map<String,String> roleResult{get;set;}
    private Map<String,String> roleCommitted{get;set;}
    private Map<String,String> roleDraft{get;set;}
    private Map<String, LMS_Role_Course__c> mappings{get;private set;}
    private Map<String, CourseDomainSettings__c> settings{get;set;}
    private Map<String, LMSConstantSettings__c> constants{get;set;}

    // Error Varaibles
    public List<String> sfdcErrors{get;private set;}
    public List<String> addErrors{get;private set;}
    public List<String> createErrors{get;private set;}
    public List<String> deleteErrors{get;private set;}
    public List<String> calloutErrors{get;private set;}

    /**
     * Constructor
     * Initializes all data
     */
    public LMS_CourseToProjectController() {
        initSettings();
        initVals();
        checkParams();
    }

    /**
     * Initialize all lists and other data
     */
    private void initVals() {
        String pstDomain = settings.get('PST').Domain_Id__c;
        String internalDomain = settings.get('Internal').Domain_Id__c;
        doSendRole = true;
        assignment = null;
        client = '';
        project = '';
        clientText = '';
        projectText = '';        
        projectFilter = ' and Client_Id__c != \'\' and Client_Id__c != null and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')';
        searchFilter = 'and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
			'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';
        
        searchFilterPackage = 'and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
			'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';

        
        courseText = 'Enter course title';
        courseTextStyle = 'WaterMarkedTextBox';
        
        courseTextPackage ='Enter package title';
        courseTextStylePackage ='WaterMarkedTextBox';
         
        roleName = 'Enter Role Name';
        roleNameStyle = 'WaterMarkedTextBox';
        roleFilter = ' and Role_Type__r.Name = \'Project Specific\' and Project_Id__r.Status_Desc__c NOT IN (\'Lost\',\'Closed\') and Status__c = \'Active\'';
    }

    /**
     * Get constants from custom settings
     */
    private void initSettings() {
        settings = CourseDomainSettings__c.getAll();
        constants = LMSConstantSettings__c.getAll();
    }

    /**
     * Look for page parameters, set client, project, project role when found
     */
    private void checkParams() {
        String client = ApexPages.currentPage().getParameters().get('client');
        String project = ApexPages.currentPage().getParameters().get('project');
        String jobClass = ApexPages.currentPage().getParameters().get('class');
        String roleId = ApexPages.currentPage().getParameters().get('roleId');
        System.debug('-------------------------------------------------'+roleId);

        if(null != client || null != project || null != jobClass) {
            if(client != null)clientText = client;
            if(project != null)projectText = project;
            if(jobClass != null)projectRole = jobClass;
            search();
        }

        if(null != roleId) {
            LMS_Role__c role = 
            [SELECT Client_Id__r.Name, Project_Id__r.Name, Job_Class_Desc__r.Name, PST_Adhoc_Role__r.PST_Adhoc_Role__c from LMS_Role__c where Id = :roleId];

            if(null != role.Client_Id__r.Name)clientText = role.Client_Id__r.Name;
            if(null != role.Project_Id__r.Name)projectText = role.Project_Id__r.Name;
            if(null != role.Job_Class_Desc__r.Name)projectRole = role.Job_Class_Desc__r.Name;
            if(null != role.PST_Adhoc_Role__r.PST_Adhoc_Role__c)projectRole = role.PST_Adhoc_Role__r.PST_Adhoc_Role__c;
            search();
        }
    }

    @RemoteAction
    global static Boolean isValidClientId(String clientId) {
        Boolean isValid;
        try {
            WFM_Client__c client = [SELECT Name FROM WFM_Client__c WHERE Name = :clientId];
            if(client != null)
                    isValid = true;
        } catch(Exception e) {
            System.debug(e.getMessage());
            isValid = false;
        }
        return isValid;
    }

    @RemoteAction
    global static Boolean isValidProjectId(String projectId) {
        Boolean isValid;
        try {
            WFM_Project__c project = [SELECT Name FROM WFM_Project__c WHERE Name = :projectId];
            if(project != null) {
            	isValid = true;
            }
        } catch(Exception e) {
            System.debug(e.getMessage());
            isValid =  false;
        }
        return isValid;
    }

    @RemoteAction
    global static Boolean isValidClientProjectPair(String clientId, String projectId) {
        Boolean isValid;
        try {
            WFM_Project__c project = [SELECT Client_Id__c FROM WFM_Project__c WHERE Name = :projectId];
            if(project.Client_ID__c == clientId) {
                isValid = true;
            } else {
                isValid = false;
            }

        } catch(Exception e) {
            System.debug(e.getMessage());
            isValid = false;
        }
        return isValid;
    }

    /**
     * Closes the list for course search
     */
    public void closeCourseSearch() {
        selCourses = null;
    }

    /**
     * Resets course search data
     */
    public void resetCourseSearch() {
        String pstDomain = settings.get('PST').Domain_Id__c;
        String internalDomain = settings.get('Internal').Domain_Id__c;
        
        courseText = 'Enter course title';
        courseTextStyle = 'WaterMarkedTextBox';
        courseTextPackage ='Enter package title';
        courseTextStylePackage ='WaterMarkedTextBox';
        searchFilter = 'and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
			'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';
        
        searchFilterPackage = 'and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
			'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';
        selCourses = null;
    }

    /**
     * Changes autocomplete filter from ui input
     */
    public void projSearchType() {
        if(clientText != '' && projectText == '') {
            projectFilter = ' and Client_Id__c != \'\' and Client_Id__c != null and Client_Id__c = \'' + clientText + '\''
            	+ ' and Status_Desc__c not in (\'Bid\',\'Lost\',\'Closed\')';
        } else if((clientText != '' && projectText != '') || (clientText == '' && projectText != '')) {
            projectFilter = ' and Client_Id__c != \'\' and Client_Id__c != null and Client_Id__c = \'' + clientText + '\''
            	+ ' and Status_Desc__c not in (\'Bid\',\'Lost\',\'Closed\')';
            List<WFM_Project__c> clientName = [select name,Client_Id__c from WFM_Project__c where Name =:projectText];
            if(clientName.size() > 0)
            clientText = string.valueof(clientName[0].Client_Id__c);
            System.debug('-------------------client text--------------------' + clientText);
        }
    }

    /**
     * Reset AutoComplete component
     */
    public void resetAC() {
        String pstDomain = settings.get('PST').Domain_Id__c;
        String internalDomain = settings.get('Internal').Domain_Id__c;
        client='';
        project='';
        clientText='';
        projectText='';
        projectFilter = 'and Client_Id__c != \'\' and Client_Id__c != null and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')';
        searchFilter = 'and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';

        searchFilterPackage = 'and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';

    }

    /**
     *  Project Role/Job Class Desc LOV
     */
    public List<selectoption> getProjectRoles() {
        List<selectoption> projectRole = new List<selectoption>();
        projectRole = LMS_LookUpDataAccess.getAllPSTRoles(true);
        return projectRole;
    }

    /**
     * Course Types LOV
     */
    public List<SelectOption> getCourseTypes() {
        List<selectoption> cType = new List<selectoption>();
            cType.add(new selectoption('Select Course Type','Select Course Type'));
            cType.add(new selectoption('PRA Courses Only','PRA Courses Only'));
            cType.add(new selectoption('PRA SOPs Only','PRA SOPs Only'));
            cType.add(new selectoption('Project Specific Only','Project Specific Only'));
            // Added new type for Sponsor Specific bandaid - Release v4.3
            cType.add(new selectoption('Sponsor Training', 'Sponsor Training'));
        return cType;
    }

    /**
     * Function to change the Autocomplete filters and fields
     */
    public void searchType() {
        String pstDomain = settings.get('PST').Domain_Id__c;
        String internalDomain = settings.get('Internal').Domain_Id__c;
		system.debug('----------client-------------' + client);
		system.debug('----------project-------------' + project);

        if(courseType == 'Select Course Type') {
            searchFilter =  'and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' and ' + 
				'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';

			searchFilterPackage =  'and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' and ' + 
				'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';

        } else if(courseType == 'PRA Courses Only') {
            searchFilter = 'and Type__c NOT IN (\'PST\',\'SOP\') and Domain_Id__c = \'' + internalDomain + '\' and ' + 
				'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';

			searchFilterPackage = 'and Type__c NOT IN (\'PST\',\'SOP\') and Domain_Id__c = \'' + internalDomain + '\' and ' + 
				'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';

        } else if(courseType == 'PRA SOPs Only') {
            searchFilter = 'and Type__c = \'SOP\' and Domain_Id__c = \'' + internalDomain + '\' and ' + 
				'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';

			searchFilterPackage = 'and Type__c = \'SOP\' and Domain_Id__c = \'' + internalDomain + '\' and ' + 
				'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';

        } else if(courseType == 'Project Specific Only') {
            if(project == null) {
                searchFilter = 'and Client_Id__c = \'' + client + '\' and Project_Id__c = null and ' + 
					'Domain_Id__c IN (\'' + pstDomain + '\',\'' + internalDomain + '\') and ' +
					'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';

				searchFilterPackage = 'and Client_Id__c = \'' + client + '\' and Project_Id__c = null and ' + 
					'Domain_Id__c IN (\'' + pstDomain + '\',\'' + internalDomain + '\') and ' +
					'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';

            } else if(project != null) {
                searchFilter = 'and (Project_Id__c = \'' + project + '\' or (Client_Id__c = \'' + client + '\' and Project_Id__c = null)) and ' + 
					'Domain_Id__c IN (\'' + pstDomain + '\',\'' + internalDomain + '\') and ' +
					'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
					
				searchFilterPackage = 'and (Project_Id__c = \'' + project + '\' or (Client_Id__c = \'' + client + '\' and Project_Id__c = null)) and ' + 
					'Domain_Id__c IN (\'' + pstDomain + '\',\'' + internalDomain + '\') and ' +
					'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
            }
        } else if(courseType == 'Sponsor Training') {
            // Added this code segment for the Sponsor Specific bandaid - Release v4.3
        	searchFilter = 'and Project_Id__c = null AND (Domain_Id__c = \'' + pstDomain + '\' OR (Domain_Id__c = \'' + internalDomain + '\' and Type__c = \'PST\')) and ' + 
        		'(Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
        }
        system.debug('-----------------filter------------------' + searchFilter);
        system.debug('-----------------packageFilter------------------' + searchFilterPackage);
    }

    /**
     * Search Implementation method :
     * This method uses the UI role criteria to search for the role
     * and the courses that have been assigned to that role.
     */
    public PageReference search() {
        Boolean acSearch;
        Boolean isClosed = false;
        bWebServiceError = false;
        roleResult = new Map<String,String>();
        roleCommitted = new Map<String,String>();
        roleDraft = new Map<String,String>();
        mappings = new Map<String, LMS_Role_Course__c>();

        String draft = constants.get('statusDraft').Value__c;
        String draftDelete = constants.get('statusDraftDelete').Value__c;
        String committed = constants.get('statusCommitted').Value__c;
        String removed = constants.get('statusRemoved').Value__c;

        //Join query to get course information on a certain role
        String courseQry = '';
        courseQry += 'select Role_Id__r.SABA_Role_PK__c,Role_Id__r.Id,Course_Id__r.Name,Course_Id__r.Course_Code__c,Course_Id__r.Title__c,' + 
			'Course_Id__r.Available_From__c,Course_Id__r.Discontinued_From__c,Course_Id__r.Status__c,Course_Id__r.Duration__c,' + 
			'Course_Id__r.Target_Days__c,Course_Id__r.Duration_Display__c,Course_Id__r.Id,Course_Id__r.SABA_ID_PK__c,Assigned_On__c,Assigned_By__c,' + 
			'Commit_Date__c,Status__c,Previous_Status__c,Sync_Status__c,LastModifiedDate ' + 
			'from Rolecourses__r where Status__c != :removed order by Status__c,Course_Id__r.Title__c';
        //Main query to pull the role based on criteria in UI
        String roleQry = '';
        roleQry += 'select Role_Name__c,SABA_Role_PK__c,Client_Id__r.Name,Project_Id__r.Name,Project_Id__r.Status_Desc__c,Status__c,Sync_Status__c,Employee_Count__c,('+courseQry+') ' + 
			'from LMS_Role__c where Id != null and Role_Type__r.Name = \'Project Specific\'';

        if(null != roleName && roleName != '' && roleName != 'Enter Role Name'){
            roleQry += 'and Role_Name__c = :roleName';
            acSearch = true;
        } else {
            acSearch = false;
            List<WFM_Project__c> project = new List<WFM_Project__c>();
            string clientName = '';
            if(projectText != '') {
                project = [select Client_Id__c, Status_Desc__c from WFM_Project__c where Name =:projectText];
                clientName = project[0].Client_Id__c;
                if(project[0].Status_Desc__c == 'Bid' || project[0].Status_Desc__c == 'Closed' || project[0].Status_Desc__c == 'Lost') {
                    isClosed = true;
                }
            }
            //Check for each type of criteria
            if((clientText=='' && projectText!='') || (clientText!='' && projectText!=''))
                roleQry+= 'and Project_Id__r.Name = :projectText and Client_Id__r.Name = :clientName';
            else if(clientText!='' && projectText=='')
                roleQry += 'and Project_Id__c = null and Client_Id__r.Name = :clientText';

            if(projectRole!=null)
                roleQry += ' and (Job_Class_Desc__r.Name = :projectRole OR PST_Adhoc_Role__r.PST_Adhoc_Role__c = :projectRole)';
        }
        allChecked = false; 
        roles = Database.query(roleQry);

        if(roles.size() == 0) {
            if(acSearch) {
                bCreateRole = false;
                createRoleError = 'Role not found, please enter a different role name';
            } else {
                bCreateRole = true;
                createRoleError = 'No role name found, Would you like to create role?';   
                if(isClosed) {
                    bCreateRole = false;
                    createRoleError = 'Bid, Closed, or Lost projects cannot be used in a new role, please enter a different project';
                }  
            }
        } else {
            courses = new List<LMS_CourseAssignments>();
            for(LMS_Role__c r : roles) {
                client = r.Client_Id__r.Name;
                project = r.Project_Id__r.Name;
                roleNameDisplay = r.Role_Name__c;
                roleResultId = r.Id;
                for(LMS_Role_Course__c rc : r.roleCourses__r) {
                    mappings.put(rc.Id, rc);
                    roleResult.put(rc.Course_Id__c,rc.Course_Id__c);
                    if(rc.Status__c == committed || rc.Status__c == draftDelete)
                        roleCommitted.put(rc.Course_Id__c,rc.Course_Id__c);
                    else if(rc.Status__c == draft)
                        roleDraft.put(rc.Course_Id__c,rc.Course_Id__c);
                    courses.add(new LMS_CourseAssignments(rc));
                }
            }
            if(roles[0].SABA_Role_PK__c != '') {
                bCreateRole = false;
                createRoleError = 'Role is out of Sync with SABA, courses can be added but not committed until sync is resolved';
            }
            if(roles[0].Status__c == 'Inactive' || roles[0].Project_Id__r.Status_Desc__c == 'Bid' || 
            	roles[0].Project_Id__r.Status_Desc__c == 'Lost' || roles[0].Project_Id__r.Status_Desc__c == 'Closed') {
                bCreateRole = false;
                if(roles[0].Status__c == 'Inactive') {
                    createRoleError = 'Role is inactive, enter a different role name';
                } else if(roles[0].Project_Id__r.Status_Desc__c == 'Bid') {
                    createRoleError = 'The project linked to this role is currently in bid status, please search for a different role';
                }  else if(roles[0].Project_Id__r.Status_Desc__c == 'Lost') {
                    createRoleError = 'The bid project linked to this role was lost, please search for a different role';
                } else if(roles[0].Project_Id__r.Status_Desc__c == 'Closed') {
                    createRoleError = 'The project linked to this role has been closed, please search for a different role';
                }
            }
        }
        viewImpact();  
        return null; 
    }

    /**
     * Course Search implementation
     */
    public PageReference courseSearch() {
        String courseQuery,course,typeQry = '';
        Set<String> cAssigned = new Set<String>();
        Datetime updateStart, updateEnd;
        String pstDomain = settings.get('PST').Domain_Id__c;
        String internalDomain = settings.get('Internal').Domain_Id__c;

        if(courseText == 'Enter course title') {
            course = '';
        } else {
            course = courseText;
        }
        if(roleCommitted != null) {
            cAssigned = roleCommitted.keySet();
        }
        courseQuery = 'select Name,Course_Code__c,Title__c,Duration__c,Target_Days__c,Duration_Display__c,Available_From__c,Discontinued_From__c,' + 
            'Status__c,SABA_ID_PK__c from LMS_Course__c where Id not in :cAssigned and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) AND ' + 
            'Status__c IN (\'Active\',\'Testing\') AND (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND ' + 
            '(NOT Course_Code__c LIKE \'%_RETRAINING\') ';
        allCheckedCourse = false;

        selCourses = new List<LMS_CourseList>();
        // Parse dates from UI if present
        if(courseUpdateStart != null && courseUpdateStart != '') {
            System.debug('--------------------start date-------------------'+courseUpdateStart);
            Date updatedDate = Date.parse(courseUpdateStart);
            updateStart = Datetime.newInstance(updatedDate, Time.newInstance(0,0,0,0));
            System.debug('-------------------start datetime---------------'+updateStart);
            courseQuery += 'and LastModifiedDate > :updateStart ';
        }
        if(courseUpdateEnd != null && courseUpdateEnd != '') {
            System.debug('--------------------end date-------------------'+courseUpdateEnd);
            Date updatedDate = Date.parse(courseUpdateEnd);
            updateEnd = Datetime.newInstance(updatedDate, Time.newInstance(0,0,0,0));
            System.debug('-------------------start datetime---------------'+updateEnd);
            courseQuery += 'and LastModifiedDate < :updateEnd ';
        }

        // Search on Course Text
        if(courseText != 'Enter course title' && courseText != null)
            courseQuery += 'and Title__c like \'' + String.escapeSingleQuotes(courseText) + '%\' ';
         
         //Search on Package Text
        if(courseTextPackage != 'Enter package title' && courseTextPackage != '') {
            courseQuery += 'and Course_Package__c like \'' + String.escapeSingleQuotes(courseTextPackage.trim()) + '%\'';
        }
           
            
        // Search on Updated within 30 days
        if(blnUpdated != false) {
            Datetime updated = system.now()-30;
            courseQuery += 'and Updated_On__c >:updated ';
        }
        // Determine if search is based on SOPs, Courses or All courses
        if(courseType == 'Select Course Type') {
            courseQuery += 'and Type__c != \'PST\' and Domain_Id__c != \'' + internalDomain + '\' ORDER BY Title__c ASC';
        } else if(courseType == 'PRA Courses Only') {
            courseQuery += 'and Type__c NOT IN (\'PST\',\'SOP\') and Domain_Id__c = \'' + internalDomain + '\' ORDER BY Title__c ASC';
        } else if(courseType == 'PRA SOPs only') {
            courseQuery += 'and Type__c = \'SOP\' and Domain_Id__c = \'' + internalDomain + '\' ORDER BY Title__c ASC';
        } else if(courseType == 'Project Specific Only') {
            System.debug('----------------project-----------------'+project);
            System.debug('----------------client-----------------'+client);
            if(project != '' && project != null) {               
                            
                courseQuery += 'and ((Project_Id__c = :project AND Client_Id__c = :client) OR (Client_Id__c = :client AND Project_Id__c = null)) and ' +
                    'Domain_Id__c IN (\'' + pstDomain + '\',\'' + internalDomain + '\') ORDER BY Course_Code__c ASC';                    
                     
            } else if(project == '' || project == null) {
                courseQuery += 'and Client_Id__c = :client and Project_Id__c = null and ' + 
                    'Domain_Id__c IN (\'' + pstDomain + '\',\'' + internalDomain + '\') ORDER BY Course_Code__c ASC';
            }
        } else if(courseType == 'Sponsor Training') {        	
            // Added this code segment for the Sponsor Specific bandaid - Release v4.3
            System.debug('----------------project-----------------'+project);
            System.debug('----------------client-----------------'+client);
            courseQuery += 'and Project_Id__c = null AND (Domain_Id__c = \'' + pstDomain + '\' OR (Domain_Id__c = \'' + internalDomain + '\' and Type__c = \'PST\'))';
        }
        system.debug('-------------------query-------------------' + courseQuery);
        List<LMS_Course__c> courseList = Database.query(courseQuery);
        if(courseList.size() == 0) {
            courseErrorText = 'The result contains no records. Please refine your search criteria and run the search again.';
            courseList = null;
        } else if(courseList.size() > 1000) {
            courseErrorText = 'The result contains too many records. Please refine your search criteria and run the search again.';
            courseList = null;
        } else {
            for(LMS_Course__c c : courseList){
                selCourses.add(new LMS_CourseList(c, roleResult));
            }
        }
        return null;
    }  

    /**
     * Function used to create new roles if query returns no results
     */
    public PageReference createRole() {
        sfdcErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();
        List<String> values = new List<String>{clientText, projectText, projectRole};

        if(isRoleDataValid() && LMS_ToolsDataAccessor.isRoleValid('PST', values)){
            errors = LMS_ToolsModifyRoles.createRole(values, 2);
            doSendRole = true;
            sfdcErrors = errors.get('SFDCEX');
        } else {
            webServiceError = 'You cannot change the search criteria during the role creation process.  Please click reset button and search again.';
            doSendRole = false;
            bWebServiceError = true;
        }

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        }

        if(doSendRole) {
            search();
            viewImpact();
        }
        return null;
    }

    private Boolean isRoleDataValid() {
        Boolean isValid = true;
        if((clientText == null && projectText == null && projectRole == null) ||
            (clientText != null && projectText != null && projectRole == null) ||
            (clientText == null && projectText == null && projectRole != null))
            return !isValid;
        return isValid;
    }

    public PageReference cancelCreateRole() {
        roles = null;
        return null;
    }

    /**
     * Method used to make WebService callout
     */
    public PageReference sendRoleData() {
        createErrors = new List<String>();
        calloutErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();
        try {
            if(doSendRole) {
                LMS_Role__c role = [SELECT Id, Role_Name__c, Status__c, SABA_Role_PK__c, Role_Type__r.Name from LMS_Role__c where Id=:roleResultId];
                errors = LMS_ToolsService.sendRoleData(role);
                createErrors = errors.get('ROLE');
                calloutErrors = errors.get('CALLOUT');
                System.debug('---------------------errors-----------------------'+errors);
                System.debug('---------------------create errors-----------------------'+createErrors);
                System.debug('---------------------callout errors-----------------------'+calloutErrors);  
            }

            if((null != createErrors && 0 < createErrors.size()) || (null != calloutErrors && 0 < calloutErrors.size())) {
                webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
                bWebServiceError = true;
            }     
        } catch(Exception e) {
            createRoleError = 'Role was not able to be saved.  Please try again.';
        }
        if(doSendRole)
            search();
        return null;
    }

    /**
     * Method to remove lists when clicking reset
     */
    public PageReference resetPage() {
        resetAC();
        roles = null;
        courses = null;
        selCourses = null;
        bWebServiceError = false;
        return null;
    }

    /**
     * This method will add all selected course to the selected role
     * All of these courses will be given a Draft status
     */
    public PageReference addCourse() {
        sfdcErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.addCourses(selCourses, courses, mappings, roleResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while adding course to role.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        courseSearch();
        viewImpact();
        return null;
    }

    /**
     * This method performs several functions depending on course assignment status
     * If current status is Draft : course is deleted/unassigned
     * If current status is Committed : course status is changed to Draft Delete
     * If current status is Draft Delete : course status is changed back to Committed
     */
    public PageReference removeCourses() {
        sfdcErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.removeCourses(courses, mappings);
        sfdcErrors = errors.get('SFDCEX');  

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while removing or reverting courses from role.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    /**
     * This method will remove all changes made to the 
     * current role assignment.
     * Draft Delete -> Committed
     * Draft -> Deleted
     */
    public PageReference cancel() {
        sfdcErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.cancelDraftCourses(roleResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }

        search();
        courseSearch();
        viewImpact();
        return null;
    }

    public PageReference setCommitDate() {

        sfdcErrors = new List<String>();
        addErrors = new List<String>();
        deleteErrors = new List<String>();
        calloutErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        Date dateCommit = Date.parse(commitDate);
        errors = LMS_ToolsModifyAssignments.setCommitDateCourses(courses, mappings, dateCommit);

        sfdcErrors = errors.get('SFDCEX');
        System.debug('-------------errors------------'+errors);
        System.debug('-------------sfdcErrors-----------'+sfdcErrors);

        if(null != sfdcErrors && sfdcErrors.size() > 0) {
            webServiceError = 'Errors occurred while applying commit date.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    /**
     * This method finalizes changes to the course assignments
     * Courses with Draft status will be changed to committed
     * Courses with Draft Delete status will be unassigned/deleted from the list
     */
    public PageReference commitCourses() {
        sfdcErrors = new List<String>();
        addErrors = new List<String>();
        deleteErrors = new List<String>();
        calloutErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.commitCourses(courses, mappings);
        sfdcErrors = errors.get('SFDCEX');
        addErrors = errors.get('A');
        deleteErrors = errors.get('D');
        calloutErrors = errors.get('CALLOUT');

        System.debug('-------------errors------------'+errors);
        System.debug('-------------sfdcErrors------------'+sfdcErrors);
        System.debug('-------------addErrors------------'+addErrors);
        System.debug('-------------deleteErrors------------'+deleteErrors);
        System.debug('-------------calloutErrors------------'+calloutErrors);

        if((null != sfdcErrors && sfdcErrors.size() > 0) || (null != addErrors && 0 < addErrors.size()) || 
            (null != deleteErrors && 0 < deleteErrors.size()) || (null != calloutErrors && 0 < calloutErrors.size())) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    public PageReference viewImpact() {
        impactNum = 0;
        impactEnd = 0;   
        impactStart = 0;    
        impactChange = '0';

        List<String> courseIds = new List<String>();
        // Get employee count and total hour count
        if(roles != null && roles.size() > 0) {
            employeeCount = Integer.valueOf(roles[0].Employee_Count__c);
        } else {
            employeeCount = 0;
        }
        if(roleResultId != null && roleResultId != '') {
            Decimal duration = LMS_ToolsDataAccessor.getTotalCourseDurationForCommittedMappings(roleResultId);
            if(duration != null) {
                impactStart = (duration*employeeCount/60).setScale(2);
            } else {
                impactStart = 0;
            }
        }
        // Get the course Ids of the current select courses
        if(selCourses != null) {
            for(LMS_CourseList c : selCourses) {
                if(c.selected && !roleResult.containsKey(c.courseId)) {
                    System.debug('-----------------c.selected--------------'+c.courseId);
                    System.debug('-----------------roleDraftKey--------------'+roleDraft.get(c.courseId));
                    courseIds.add(c.courseId);
                }
            }
        }
        if(courses != null) {            
            // Get the durations for the adding and removal of courses
            Double clToAdd = LMS_ToolsDataAccessor.getTotalCourseDurationForAddMappings(roleResultId);
            Double clToRemove = LMS_ToolsDataAccessor.getTotalCourseDurationForDeleteMappings(roleResultId);
            Double cToAdd = LMS_ToolsDataAccessor.getTotalCourseDurationFromCourseList(courseIds);
            impactNum = 0;

            if(clToAdd != null) {
                impactNum += clToAdd;
                System.debug('---------total additions from course list--------------'+clToAdd);
            }
            if(courseIds.size() > 0) {
                if(cToAdd != null) {
                    impactNum += cToAdd;
                    System.debug('---------total from selCourse list--------------'+cToAdd);
                }
            }
            if(clToRemove != null) {
                impactNum -= clToRemove;
                System.debug('---------total deletions from course list--------------'+clToRemove);
            }
            impactNum *= employeeCount;
            impactNum = (impactNum/60).setScale(2);

            if(impactNum > 0) {
                impactChange = '+ '+impactNum;
            } else {
                impactChange = String.valueOf(impactNum);
            }

            impactEnd = (impactStart + impactNum).setScale(2);
            renderImpact = true;
        } else {
            employeeCount = 0;
            impactStart = 0;
            impactNum = 0;
            impactChange = String.valueOf(impactNum);
            impactEnd = impactStart + impactNum;
        }

        return null;
    }

    public PageReference showErrors() {
        PageReference pr = new PageReference('/apex/LMS_CourseProjectError');
        pr.setRedirect(false);
        return pr;
    }

}