/** Implements the Selector Layer of the object LaboratoryMethodCompound__c
 * @author	Dimitrios Sgourdos
 * @version	07-Nov-2013
 */
public with sharing class LaboratoryMethodCompoundDataAccessor {
	
	/** Object definition for fields used in application for LaboratoryMethodCompound
	 * @author	Dimitrios Sgourdos
	 * @version 08-Oct-2013
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('LaboratoryMethodCompound__c');		
	}
	
	
	/** Object definition for fields used in application for LaboratoryMethodCompound with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 07-Nov-2013
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'AccuracyPrecision__c,';
		result += referenceName + 'AutosamplesStability__c,';
		result += referenceName + 'CarryOver__c,';
		result += referenceName + 'Dilution__c,';
		result += referenceName + 'ExtractionRecovery__c,';
		result += referenceName + 'FreezeThawStability__c,';
		result += referenceName + 'HemolyticPlasmaIntegrity__c,';
		result += referenceName + 'InternalStandard__c,';
		result += referenceName + 'InternalStandardStockStability__c,';
		result += referenceName + 'LaboratoryMethod__c,';
		result += referenceName + 'LipidemicPlasmaIntegrity__c,';
		result += referenceName + 'MatrixEffect__c,';
		result += referenceName + 'MatrixStability__c,';
		result += referenceName + 'MiscellaneousCompound__c,';
		result += referenceName + 'MiscellaneousMethod__c,';
		result += referenceName + 'OtherName__c,';
		result += referenceName + 'pHStability__c,';
		result += referenceName + 'ReinjectionOriginal__c,';
		result += referenceName + 'ReinjectionReinjected__c,';
		result += referenceName + 'Robustness__c,';
		result += referenceName + 'SelectivityMatrixVariability__c,';
		result += referenceName + 'SelectivityTowardsMetabComed__c,';
		result += referenceName + 'Sensitivity__c,';
		result += referenceName + 'StabilityAt4C__c,';
		result += referenceName + 'WholeBloodStability__c,';
		result += referenceName + 'StockStability__c,';
		result += referenceName + 'ValidatedRange__c,';
		result += referenceName + 'ValidatedRangeMaxValue__c,';
		result += referenceName + 'ValidatedRangeMinValue__c,';
		result += referenceName + 'WorkRange__c,';
		result += referenceName + 'WorkRangeMaxValue__c,';
		result += referenceName + 'WorkRangeMinValue__c';
		return result;
	}
	
	
	/** Retrieve the Laboratory Method Compounds that are analyzed in Laboratory Analysis for the given list of studies
	 * @author	Dimitrios Sgourdos
	 * @version 21-Oct-2013
	 * @param	userStudies			The selected studies by the user
	 * @return	The Laboratory Method Compounds list.
	 */
	public static List<LaboratoryMethodCompound__c> getUsedByUserStudies(List<Study__c> userStudies) {
		String query = String.format(
								'SELECT {0}, {1}, ' +
										'(select {2} from LaboratoryAnalysis__r {3}) ' +
								'FROM LaboratoryMethodCompound__c ' +
								'WHERE Id in   	(select LaboratoryMethodCompound__c ' +
											 	'from LaboratoryAnalysis__c {3})' +
								'ORDER BY LaboratoryMethod__r.Name, Name',
								new List<String> {
									getSObjectFieldString(),
									LaboratoryMethodDataAccessor.getSObjectFieldString('LaboratoryMethod__r'),
									LaboratoryAnalysisDataAccessor.getSObjectFieldString(''), 
									'where Study__c IN :userStudies'
								}
							);
		return (List<LaboratoryMethodCompound__c>) Database.query(query);
	}	
	
	
	/** Retrieve the Laboratory Method Compounds that belong to the method with Id equal to labMethodId
	 * @author	Dimitrios Sgourdos
	 * @version 22-Oct-2013
	 * @param	labMethodId			The id of the laboratory method that we want ot retrieve the compounds
	 * @param	userStudies			The selected studies by the user
	 * @return	The Laboratory Method Compounds list.
	 */
	public static List<LaboratoryMethodCompound__c> getByLabMethod(String labMethodId, List<Study__c> userStudies) {
		String query = String.format(
								'SELECT {0}, {1}, ' +
										'(select {2} from LaboratoryAnalysis__r {3}) ' +
								'FROM LaboratoryMethodCompound__c ' +
								'WHERE LaboratoryMethod__c = {4} ' +
								'ORDER BY Name',
								new List<String> {
									getSObjectFieldString(),
									LaboratoryMethodDataAccessor.getSObjectFieldString('LaboratoryMethod__r'),
									LaboratoryAnalysisDataAccessor.getSObjectFieldString(''), 
									'where Study__c IN :userStudies',
									'\'' + labMethodId + '\''
								}
							);
		return (List<LaboratoryMethodCompound__c>) Database.query(query);
	}	
	
}