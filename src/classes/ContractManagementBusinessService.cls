/**
*	Represents the Business Logic Service layer for Contract Management integrations
*/

public with sharing class ContractManagementBusinessService {

	public static void upsertSiteContracts(Apttus__APTS_Agreement__c contract) {
		
		if(null != contract) {
			/*ContractManagementService_SiteContract.contract cont = new ContractManagementService_SiteContract.contract();
			cont.ctaId = contract.Id;
			cont.account = contract.Institution__r.Name;
			cont.accountId = contract.Institution__r.Account__c; //-- Ext Id
			cont.budgetApprovedDate = contract.Budget_Approved_Date__c.format();
			cont.budgetReceivedDate = contract.Budget_Rec_Date__c.format();
			cont.budgetSentDate = contract.Budget_Sent_Date__c.format();
			//cont.comments = contract.Comments__c;
			//cont.contractId = contract. -- External Id Text(60) (External ID) (Unique Case Insensitive)
			cont.contractName = contract.Name;
			cont.contractType = contract.Contract_Type__c;
			cont.firstDraftSentDate = contract.First_Draft_Sent_Date__c.format();
			cont.firstSiteCommentsDate = contract.First_Site_Comments_Rec_Date__c.format();
			cont.fullyExecutedDate = contract.Fully_Exec_Date__c.format();
			//cont.id = contract. -- External Id Text(60) (External ID) (Unique Case Insensitive)
			cont.negotiationCompleteDate = contract.Neg_Complete_Date__c.format();
			cont.partiallyExecutedDate = contract.Partially_Exec_Date__c.format();
			cont.plannedFinalExecutionDate = contract.Planned_Final_Exec_Date__c.format();
			cont.sponsorName = contract.Sponsor__r.Name;
			//cont.team = contract.
			
			ContractManagementService_SiteContract.status status = new ContractManagementService_SiteContract.status();
			status.status = contract.Apttus__Status__c;
			status.date_x = Datetime.now().format(); 
			
			ContractManagementService_SiteContract.statuses statuses = new ContractManagementService_SiteContract.statuses();
			statuses.status = new ContractManagementService_SiteContract.status[]{status};
			cont.statuses = statuses;
			
			ContractManagementService_SiteContract.site site = new ContractManagementService_SiteContract.site();
			site.id = contract.Site__r.Site_ID__c;
			ContractManagementService_SiteContract.contracts contracts = new ContractManagementService_SiteContract.contracts();
			contracts.contract = new ContractManagementService_SiteContract.contract[]{cont};
			site.contracts = contracts;
			ContractManagementService.WriteSiteContract_Output_element resp = ContractManagementServiceWrapper.upsertSiteContracts(site, Userinfo.getUserName());*/
			
		}
	}
}