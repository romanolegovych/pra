/**
@author Bhargav Devaram
@date 2014
@description this controller class is to display the sub tab menus related to a Project Information
**/
public class PBB_ProjectInformationController {
    
    public  List<String> HiddenHomeTabsList  {get;set;}  
    public  List<String> HiddenSubTabsList   {get;set;}
    Public String PBBScenarioID       { get; set; }   //PBB scenarios ID
    Public String urlVal              { get; set; }   //Url for saving new PBB Scenario
    Public PBB_Scenario__c PBBScenario { get; set; }   //PBB scenarios     
    Public Bid_Project__c BidProject  { get; set; }   //Bid Project
    Public string Description         { get; set; } 
    
    //Milestones date fields
    public string Studystartupdate    { get; set; }
    public decimal totalprjct         { get; set; }
    public string Finalpro            { get; set; }
    public string Studydoc            { get; set; }
    public string Finalstudy          { get; set; }
    public string statisrprt          { get; set; }
    public string Allsites            { get; set; }
    public string Dblock              { get; set; }
    public string Lastvisit           { get; set; }
    public string Lastsub             { get; set; }
    public string FstSub              { get; set; }
    public string InvesMeetng         { get; set; }
    public Boolean isEdit             { get; set; }
    
    //new
    public string ExpectedSites       { get; set; }
    public string ExpectedPatients    { get; set; }
    public string FPR                 { get; set; }
    public string LPR                 { get; set; }
    //public string LPO               { get; set; }    
    
    //study specification fields
    public string Protocolno          { get; set; }
    public string Protocolname        { get; set; }
    public string studymed            { get; set; }
    public decimal noofven            { get; set; }
    
    //site specification fields
    public decimal noofcon            { get; set; }
    public decimal nooflang           { get; set; }
    public decimal sitestartup        { get; set; }
    public decimal sitemaintnce       { get; set; }
    public decimal sitecloseout       { get; set; }
    public decimal noofnewsltrs       { get; set; }
    public string frqncyofnews        { get; set; }
    
    //subject specification fields
    public decimal subscreened        { get; set; }
    public String  agerange           { get; set; }
    public decimal failurerate        { get; set; }
    public decimal labspersub         { get; set; }
    public decimal randmzed           { get; set; }
    public decimal discontrate        { get; set; }
    public decimal subcompltd         { get; set; }
    public decimal enrlmntperiod      { get; set; }
    public decimal trmntperd          { get; set; }
    public string selectedNewslttrs   { get; set; }
    
    //Bid Project fields
    public string strtdate            { get; set; }
    public string enddate             { get; set; }
    public string fstpatin            { get; set; }
    public string lastpatin           { get; set; }
    public String selectedAccName     {get; set;}
    public String taNames             { get; set; }
    public String Indgrp              { get; set; }
    public String prmyind             { get; set; }
    public String Indgrpvalues        { get; set; }
    public String prmyindvalues       { get; set; }
    public String selectedPhase       { get; set; }
    public String selectedWRModel     { get; set; }    
    //for PBB record
    public String bidProjName         { get; set; }    
    public String billRateMdl         { get; set; }
    public Decimal saerate            { get; set; }
    public Decimal caseversion        { get; set; }
    public Decimal Adjst              { get; set; }
    
    /**
    @author Ramya 
    @date 2014
    @description This is a constructor for getting value from standard controller variable..  
    **/    
    public PBB_ProjectInformationController(ApexPages.StandardController controller) {
        
        HiddenHomeTabsList = new List<String>();
        HiddenSubTabsList = new List<String>();
        //HiddenSubTabsList.add('SDpage');  
            
        urlVal = Apexpages.currentPage().getUrl();
        system.debug('----urlVal ----'+urlVal );
        if(urlVal !=null)
            urlVal = urlVal.replace('/apex/PBB_ProjectInformation?',''); 
        
        PBBScenario = (PBB_Scenario__c)controller.getRecord();
        PBBScenarioID = ApexPages.currentPage().getParameters().get('PBBScenarioID');        
        
        if(ApexPages.currentPage().getParameters().get('bid')!=null && ApexPages.currentPage().getParameters().get('bid')!='')
            PBBScenario.Bid_Project__c =  ApexPages.currentPage().getParameters().get('bid');
       
        if(ApexPages.currentPage().getParameters().get('id')!=null){
            PBBScenarioID = ApexPages.currentPage().getParameters().get('id');            
        }
        if(pbbScenarioID!=null)
            PBBScenario = PBB_DataAccessor.getScenarioByID(pbbScenarioID);
        system.debug('--------pbbscenario ---'+pbbscenario+'--PBBScenarioID-'+ PBBScenarioID);
           
        BidProject = PBB_DataAccessor.getBidProjectByID(PBBScenario.Bid_Project__c);
        if(String.isEmpty(PBBScenarioID)){           
            bidProjName = BidProject.Name;
            isEdit = false;
            Bid_Project__c bidProj = BidProject;
            system.debug('----bidProj1----'+bidProj);
            
            //Milestones date fields
            Studystartupdate = PBB_Utils.getDateInStringFormat(bidProj.Study_Startup_Activities_Begin__c);
            totalprjct = bidProj.Total_Project_Duration__c;
            Allsites = PBB_Utils.getDateInStringFormat(bidProj.All_Sites_Closed__c);
            Studydoc = PBB_Utils.getDateInStringFormat(bidProj.All_Study_Documents_Returned_to_Sponsor__c);
            Dblock = PBB_Utils.getDateInStringFormat(bidProj.Database_Lock_Top_Line_Results__c);
            Finalstudy = PBB_Utils.getDateInStringFormat(bidProj.Final_Clinical_Study_Report__c);
            Finalpro = PBB_Utils.getDateInStringFormat(bidProj.Final_Protocol_Date__c);
            FstSub = PBB_Utils.getDateInStringFormat(bidProj.First_Subject_First_Visit__c);
            InvesMeetng = PBB_Utils.getDateInStringFormat(bidProj.Investigator_Meeting__c);
            Lastsub = PBB_Utils.getDateInStringFormat(bidProj.Last_Subject_First_Visit__c);
            Lastvisit = PBB_Utils.getDateInStringFormat(bidProj.Last_Subject_Last_Visit__c);
            statisrprt = PBB_Utils.getDateInStringFormat(bidProj.Statistical_Report_Complete__c);
            
            //new
            ExpectedSites    =   String.valueof(bidProj.Expected_Sites__c); 
            ExpectedPatients =   String.valueof(bidProj.Expected_Patients__c);
            FPR              =   PBB_Utils.getDateInStringFormat(bidProj.First_Patient_In__c); 
            LPR              =   PBB_Utils.getDateInStringFormat(bidProj.Last_Patient_In__c);    
            //LPO              =   PBB_Utils.getDateInStringFormat(bidProj.Expected_Patients__c);    
            
            //study specification fields
            Protocolno = bidProj.Protocol_Number__c;
            Protocolname = bidProj.Protocol_Name__c;
            studymed = bidProj.Study_Medication__c;
            noofven = bidProj.Number_Of_Vendors__c;
            
            //site specification fields
            noofcon = bidProj.Number_of_Countries__c;
            nooflang = bidProj.Number_of_Languages__c;
            sitestartup = bidProj.Start_up__c;
            sitemaintnce = bidProj.Site_Maintenance__c;
            sitecloseout = bidProj.Site_Closeout__c;
            noofnewsltrs = bidProj.Number_of_Newletters__c;
            selectedNewslttrs = bidProj.Frequency_of_Newsletters__c;
            
            //subject specification fields
            subscreened = bidProj.Number_of_Subjects_Screened__c;
            agerange = bidProj.Age_Range__c;
            failurerate =bidProj.Subject_Screen_Failure__c;
            labspersub = bidProj.Expected_Labs_per_Subject__c;
            randmzed = bidProj.Number_of_Subjects_Randomized__c;
            discontrate = bidProj.Subjects_Discontinue_Rate__c;
            subcompltd = bidProj.Subjects_Completed__c;
            enrlmntperiod = bidProj.Enrollment_Period_length__c;
            trmntperd = bidProj.Treatment_Period_Length__c;
            
            //bid project detail fields
            strtdate = PBB_Utils.getDateInStringFormat(bidProj.Project_Start_Date__c);
            enddate = PBB_Utils.getDateInStringFormat(bidProj.Project_End_Date__c);
            fstpatin = PBB_Utils.getDateInStringFormat(bidProj.First_Patient_In__c);
            lastpatin = PBB_Utils.getDateInStringFormat(bidProj.Last_Patient_In__c);
            
            //PICKLIST VALUES IN PROJECT DETAIL SECETION 
            taNames = bidProj.Therapeutic_Area__c;            
            selectedPhase = bidProj.Study_Phase__c; 
            prmyind = bidProj.Primary_Indication__c; 
            Indgrpvalues = bidProj.Indication_group__c; 
        }      
        else{            
            bidProjName = PBBScenario.Bid_Project__r.Name;
            if(PBBScenario.Bid_Project__c!=null){
                system.debug(PBBScenario.Bid_Project__c);
                //list<SRM_Scenario__c> scList = new List<SRM_Scenario__c>();
                //sclist = PBB_DataAccessor.getSRMScenarioListByBidProjectId(PBBScenario.Bid_Project__c);
                //system.debug(sclist);
            }            
            isEdit = true;
            //getting from data accessor layer
            PBB_Scenario__c  pbbsce = PBBScenario;
            system.debug('----pbbsce----'+pbbsce);
            
            //details
            Description = pbbsce.Bid_Description__c;
            billRateMdl = pbbsce.Bill_Rate_Card_Model__c;
            
            //Milestones date fields
            Studystartupdate = PBB_Utils.getDateInStringFormat(pbbsce.Study_Startup_Activities_Begin__c);
            system.debug('----Studystartupdate ----'+Studystartupdate );
            totalprjct = pbbsce.Total_Project_Duration__c;
            Allsites = PBB_Utils.getDateInStringFormat(pbbsce.All_Sites_Closed__c);
            Studydoc = PBB_Utils.getDateInStringFormat(pbbsce.All_Study_Documents_Returned_to_Sponsor__c);
            Dblock = PBB_Utils.getDateInStringFormat(pbbsce.Database_Lock_Top_Line_Results__c);
            Finalstudy = PBB_Utils.getDateInStringFormat(pbbsce.Final_Clinical_Study_Report__c);
            Finalpro = PBB_Utils.getDateInStringFormat(pbbsce.Final_Protocol_Date__c);
            FstSub = PBB_Utils.getDateInStringFormat(pbbsce.First_Subject_First_Visit__c);
            InvesMeetng = PBB_Utils.getDateInStringFormat(pbbsce.Investigator_Meeting__c);
            Lastsub = PBB_Utils.getDateInStringFormat(pbbsce.Last_Subject_First_Visit__c);
            Lastvisit = PBB_Utils.getDateInStringFormat(pbbsce.Last_Subject_Last_Visit__c);
            statisrprt = PBB_Utils.getDateInStringFormat(pbbsce.Statistical_Report_Complete__c);            
            
            ExpectedSites    =   String.valueof(pbbsce.Expected_Sites__c);             
            ExpectedPatients =   String.valueof(pbbsce.Expected_Patients__c);
            FPR              =   PBB_Utils.getDateInStringFormat(pbbsce.First_Patient_Randomized__c); 
            LPR              =   PBB_Utils.getDateInStringFormat(pbbsce.Last_Patient_Randomized__c);    
            //LPO              =   PBB_Utils.getDateInStringFormat(pbbsce.Last_Patient_Out__c);
            
            //site specification fields
            Protocolno = pbbsce.Protocol_Number__c;
            Protocolname = pbbsce.Protocol_Name__c;
            studymed = pbbsce.Study_Medication__c;
            noofven = pbbsce.Number_Of_Vendors__c;
            
            //subject specification fields
            noofcon = pbbsce.Number_of_Countries__c;
            nooflang = pbbsce.Number_of_Languages__c;
            sitestartup = pbbsce.Site_management_Burden_Site_Start_up__c;
            sitemaintnce = pbbsce.Site_management_Burden_Site_Maintenance__c;
            sitecloseout = pbbsce.Site_Management_Burden_Site_Closeout__c;
            noofnewsltrs = pbbsce.Number_of_Newletters__c;
            selectedNewslttrs = pbbsce.Frequency_of_Newsletters__c;
            
            //study specification fields
            subscreened = pbbsce.Number_of_Subjects_Screened__c;
            agerange = pbbsce.Age_Range_of_Subjects__c;
            failurerate = pbbsce.Expecte_Subject_Screen_Failure_Rate__c;
            labspersub = pbbsce.Estimated_Number_of_Labs_per_Subject__c;
            randmzed = pbbsce.Number_of_Subjects_Randomized_Enrolled__c;
            discontrate = pbbsce.Expected_Subjects_Discontinue_Rate__c;
            subcompltd = pbbsce.Number_of_Subjects_Completed__c;
            enrlmntperiod = pbbsce.Length_of_Enrollment_Period_Months__c;
            trmntperd = pbbsce.Length_of_Treatment_Period_Months__c;
            
            //bid project detail fields
            strtdate = PBB_Utils.getDateInStringFormat(pbbsce.Project_Start_Date__c);
            enddate = PBB_Utils.getDateInStringFormat(pbbsce.Project_End_Date__c);
            fstpatin = PBB_Utils.getDateInStringFormat(pbbsce.First_Patient_In__c);
            lastpatin = PBB_Utils.getDateInStringFormat(pbbsce.Last_Patient_In__c);
            
            //PICKLIST VALUES IN PROJECT DETAIL SECETION 
            taNames = pbbsce.Therapeutic_Area__c; 
            selectedPhase = pbbsce.Phase__c; 
            prmyind = pbbsce.Primary_Indication__c; 
            Indgrpvalues = pbbsce.Indication_group__c; 
            
            //Picklist values for WR Model
            selectedWRModel = pbbsce.WR_Model__c;
            
            //safety
            saerate = pbbsce.SAE_Rate__c;
            caseversion = pbbsce.No_of_Case_Versions__c;
            Adjst=pbbsce.Number_of_Adjudication_Member_Payments__c;
        }
        system.debug('----pbbscenario----'+pbbscenario+PBBScenarioID);
        if(pbbscenario.id==null && PBBScenarioID==null){ 
           HiddenHomeTabsList.add('PRpage');
           //HiddenHomeTabsList.add('PSpage');
           HiddenHomeTabsList.add('PCpage'); 
           
           HiddenSubTabsList.add('PIpage');
           HiddenSubTabsList.add('CSpage');    
           //HiddenSubTabsList.add('SRMpage');   
           HiddenSubTabsList.add('SDpage'); 
           HiddenSubTabsList.add('IMpage');    
        }    
        if(PBBScenarioID !=null)
            urlVal =  PBBScenarioID ;  
    }    
   
    /**
    * @author Ramya
    * @Date 1/8/2012
    * @Description Method to save Scenario
    *
    */    
    public PageReference save(){    
        PageReference pageRef =null;         
        system.debug('@@@@@'+selectedWRModel);         
               
        //Milestones date fields
        PBBScenario.Study_Startup_Activities_Begin__c = PBB_Utils.getDate(Studystartupdate);
        PBBScenario.All_Sites_Closed__c = PBB_Utils.getDate(Allsites);
        PBBScenario.All_Study_Documents_Returned_to_Sponsor__c = PBB_Utils.getDate(Studydoc);
        PBBScenario.Database_Lock_Top_Line_Results__c = PBB_Utils.getDate(Dblock);
        PBBScenario.Final_Clinical_Study_Report__c = PBB_Utils.getDate(Finalstudy);
        PBBScenario.Final_Protocol_Date__c = PBB_Utils.getDate(Finalpro);
        PBBScenario.First_Subject_First_Visit__c = PBB_Utils.getDate(FstSub);
        PBBScenario.Investigator_Meeting__c = PBB_Utils.getDate(InvesMeetng);
        PBBScenario.Last_Subject_First_Visit__c = PBB_Utils.getDate(Lastsub);
        PBBScenario.Last_Subject_Last_Visit__c = PBB_Utils.getDate(Lastvisit);
        PBBScenario.Statistical_Report_Complete__c = PBB_Utils.getDate(statisrprt);
        
        //new       
        PBBScenario.Expected_Sites__c =(ExpectedSites=='')?null:Integer.valueof(ExpectedSites);         
        PBBScenario.Expected_Patients__c = (ExpectedPatients=='')?null:Integer.valueof(ExpectedPatients);
        PBBScenario.First_Patient_Randomized__c = PBB_Utils.getDate(FPR); 
        PBBScenario.Last_Patient_Randomized__c = PBB_Utils.getDate(LPR);    
        //PBBScenario.Last_Patient_Out__c = PBB_Utils.getDate(LPO);
            
        //details
        PBBScenario.Bid_Description__c = Description;
        if(PBBScenarioID ==null || PBBScenarioID =='')
            PBBScenario.SRM_Model__c = PBB_ScenarioService.getRecentActiveModel();
        
        //study specification fields
        PBBScenario.Protocol_Number__c = Protocolno;
        PBBScenario.Protocol_Name__c = Protocolname;
        PBBScenario.Study_Medication__c = studymed;
        PBBScenario.Number_Of_Vendors__c = noofven;
        
        //site specification fields
        PBBScenario.Number_of_Countries__c = noofcon;
        PBBScenario.Number_of_Languages__c = nooflang;
        PBBScenario.Site_management_Burden_Site_Start_up__c = sitestartup;
        PBBScenario.Site_management_Burden_Site_Maintenance__c = sitemaintnce;
        PBBScenario.Site_Management_Burden_Site_Closeout__c = sitecloseout;
        PBBScenario.Number_of_Newletters__c = noofnewsltrs;
        PBBScenario.Frequency_of_Newsletters__c = selectedNewslttrs;
        
        //subject specification fields      
        PBBScenario.Number_of_Subjects_Screened__c = subscreened;
        PBBScenario.Age_Range_of_Subjects__c = agerange;
        PBBScenario.Expecte_Subject_Screen_Failure_Rate__c = failurerate;
        PBBScenario.Estimated_Number_of_Labs_per_Subject__c = labspersub;
        PBBScenario.Number_of_Subjects_Randomized_Enrolled__c = randmzed;
        PBBScenario.Expected_Subjects_Discontinue_Rate__c = discontrate;
        PBBScenario.Number_of_Subjects_Completed__c = subcompltd;
        PBBScenario.Length_of_Enrollment_Period_Months__c = enrlmntperiod;
        PBBScenario.Length_of_Treatment_Period_Months__c = trmntperd;
        
        //bid project detail fields
        PBBScenario.Project_Start_Date__c = PBB_Utils.getDate(strtdate);
        PBBScenario.Project_End_Date__c = PBB_Utils.getDate(enddate);
        PBBScenario.First_Patient_In__c = PBB_Utils.getDate(fstpatin);
        
        PBBScenario.Last_Patient_In__c = PBB_Utils.getDate(lastpatin);
        
        //PICKLIST VALUES IN PROJECT DETAIL SECETION 
        PBBScenario.Therapeutic_Area__c = taNames;
        PBBScenario.Phase__c = selectedPhase ;
        PBBScenario.Primary_Indication__c = prmyind ;
        PBBScenario.Indication_group__c = Indgrpvalues ;
        PBBScenario.WR_Model__c = selectedWRModel;
        
        //safety
        PBBScenario.SAE_Rate__c = saerate;
        PBBScenario.No_of_Case_Versions__c = caseversion;
        PBBScenario.Number_of_Adjudication_Member_Payments__c = Adjst;
    
        if(validateOnAwarded()){
            upsert PBBScenario;
            if(PBBScenario.Bid_Status__c == 'Awarded'){
                System.debug('****OPERATIONAL WEEKLY EVENTS CREATED*********');
                PBB_Services.createOperationalWeeklyEvents(PBBScenario);   
            }
            pageRef = new PageReference('/apex/PBB_ProjectInformation?id='+PBBScenario.Id);
            pageRef.setRedirect(true);
        }
               
        return pageRef;
   }

    public Boolean validateOnAwarded(){
        if(PBBScenario.Bid_Status__c != 'Awarded'){
            return true;
        }
        else{
            if(PBBScenario.Status__c != 'Approved'){
                SRM_Utils.showErrorMessage('SRM status must be "Approved" in order to update the BID Scenario status to Awarded');
                return false;
            }
            if(String.isEmpty(Protocolname) || String.isBlank(Protocolname)){
                SRM_Utils.showErrorMessage('Protocol Name is required to set status to Awarded');
                return false;
            }
            return true;
        }  
    }    
    
   /** 
   @author Bhargav Devaram
   @date 2014   
   @description method to cancel and navigate to project page
   */ 
   public pageReference Cancel(){
       String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + PBBScenario.Bid_Project__c ;
       PageReference returnPage = new PageReference(baseURL);
       returnPage.setRedirect(true);    
       return returnPage;
   }
   
   
    /** 
   @author Ramya
   @date 2015   
   @description method to get Therapeutic picklist values
   */ 
   public List<selectOption> gettherptic(){    
       List<selectOption> therapticlist = new List<selectOption>();    
       Set<string> uniqueNames = new set<string>(); 
       //to add blank value as default
       therapticlist.add(new SelectOption('','  '));
       for(WFM_Therapeutic__c ta: [select id,name,Therapeutic_Area__c,Therapeutic_Area__r.Name  from WFM_Therapeutic__c 
                                          where Status__c = 'Approved' and Therapeutic_Area__c != null order by Therapeutic_Area__r.Name asc]) {
           if(!uniqueNames.contains(ta.Therapeutic_Area__r.name))                                   
           		therapticlist.add(new selectOption(ta.Therapeutic_Area__c,ta.Therapeutic_Area__r.name));
           uniqueNames.add(ta.Therapeutic_Area__r.name);                                   
           //system.debug('@@@@therapticlist'+therapticlist);
       } 
       return therapticlist;
    }
    
                
    /** 
   @author Ramya
   @date 2015   
   @description method referenced by page to get Indication_Group__c based Therapeutic picklist values
   */             
    public PageReference getIndicationsBasedOnTheropeticArea(){     
        List<selectOption> Indications = new List<selectOption>();
		system.debug('************taNames = '+taNames);        
        for(WFM_Therapeutic__c taObject:[select id,name,IndicationGroup__c,IndicationGroup__r.Name from WFM_Therapeutic__c 
                                          where Therapeutic_Area__r.id  =: taNames and IndicationGroup__c != null order by IndicationGroup__r.Name asc]){
            //system.debug('@@@@taObject'+taObject);
            Indications.add(new SelectOption(taObject.IndicationGroup__c,taObject.IndicationGroup__r.Name));
        }        
        Indgrpvalues =null;        
        List<selectOption> prmyIndications = new List<selectOption>();
        for(Primary_Indication__c prmyObject:PBB_DataAccessor.getIndicationGRPforpicklist(Indgrpvalues)){
            //system.debug('@@@@prmyObject'+prmyObject);
            prmyIndications.add(new SelectOption(prmyObject.Id,prmyObject.Name));
        }
        return null;
   }     
    

   /** 
   @author Ramya
   @date 2015  
   @description method to get Indication_Group__c based Therapeutic picklist values
   */   
    public List<selectOption> getIndicationgrp() {    
        List<selectOption> Indicationgrplist = new List<selectOption>();   
        Set<string> uniqueNames = new set<string>();
        //set default blank value
        Indicationgrplist.add(new SelectOption('','  '));        
        for(WFM_Therapeutic__c Ia: [select id,name,IndicationGroup__c,IndicationGroup__r.Name from WFM_Therapeutic__c 
                                          where Therapeutic_Area__r.id  =: taNames and Status__c = 'Approved' and IndicationGroup__c != null  order by IndicationGroup__r.Name asc] ){
            if(!uniqueNames.contains(Ia.IndicationGroup__r.name))                                  
            	Indicationgrplist.add(new selectOption(Ia.IndicationGroup__c,Ia.IndicationGroup__r.name));
            uniqueNames.add(Ia.IndicationGroup__r.name);                                  
            //system.debug('@@@@Indicationgrplist'+Indicationgrplist);
        }
        return Indicationgrplist;
    }         
         
   /** 
   @author Ramya
   @date 2015  
   @description method refrenced by page to get prmyIndications based Indication_Group__c picklist values
   */ 
   public PageReference getprmyIndicationsBasedOnIndgrp(){     
       List<selectOption> prmyIndications = new List<selectOption>();   
       system.debug('**********Indgrpvalues = '+Indgrpvalues);
       //system.debug('**********WT.IndicationGroup__c = '+WT.IndicationGroup__c);
       for(WFM_Therapeutic__c prmyObject:[select id,name,PrimaryIndication__c,PrimaryIndication__r.Name from WFM_Therapeutic__c 
                                          where Therapeutic_Area__r.id  =: taNames and IndicationGroup__c =:+Indgrpvalues and Status__c = 'Approved' and PrimaryIndication__c != null order by PrimaryIndication__r.Name asc]){
           //system.debug('@@@@prmyObject'+prmyObject);
           prmyIndications.add(new SelectOption(prmyObject.PrimaryIndication__c,prmyObject.PrimaryIndication__r.Name));
       }
       return null;
   }
    
    
    /** 
   @author Ramya
   @date 2015  
   @description method to get prmyIndications based Indication_Group__c picklist values
   */            
   public List<selectOption> getprimaryInd() {    
       List<selectOption> primaryIndlist = new List<selectOption>();
       Set<string> uniqueNames = new set<string>();
       //to set default value
       primaryIndlist.add(new SelectOption('','  '));        
       for(WFM_Therapeutic__c Pi:[select id,name,PrimaryIndication__c,PrimaryIndication__r.Name from WFM_Therapeutic__c 
                                          where Therapeutic_Area__r.id  =: taNames and IndicationGroup__c =:+Indgrpvalues and Status__c = 'Approved' and PrimaryIndication__c != null order by PrimaryIndication__r.Name asc]){
            if(!uniqueNames.contains(Pi.PrimaryIndication__r.name))                                   
           		primaryIndlist.add(new selectOption(Pi.PrimaryIndication__c,Pi.PrimaryIndication__r.name));
            uniqueNames.add(Pi.PrimaryIndication__r.name);                                  
           //system.debug('@@@@primaryIndlist'+primaryIndlist);
       }
       return primaryIndlist;
   }            
    /** 
   @author Ramya
   @date 2015  
   @description method to get phase picklist values
   */ 
   public  List<SelectOption> getstudyPhase(){        
       List<SelectOption> options = new List<SelectOption>();        
       //add default blank value
       options.add(new SelectOption('--None--','  '));        
       for(WFM_Phase__c temp:PBB_DataAccessor.getPhaseValues()){
           options.add(new SelectOption((temp.name).trim(),(temp.name).trim()));
       }
       return options;
   } 
    
    
   /** 
   @author Ramya
   @date 2014
   @description to get all the WR Models 
   @return   list of WR Models
   */
   public List<SelectOption> getWrList() {   
       List<SelectOption> WrOptions = new List<SelectOption>();        
       //add default blank value
       Wroptions.add(new SelectOption('','  '));        
       for(WR_Model__C wr:PBB_DataAccessor.GetAllWrModels()) {  
           Wroptions.add(new SelectOption(wr.id,String.valueof(wr.name)));
       }
       return Wroptions;
   }
   
   /** 
   @author Ramya
   @date 2014
   @description to clone the child records
   */
   public PageReference CloneScenario() {   
       //PBB_Scenario__c cloneScenario = new PBB_Scenario__c();
       PageReference pageRef = null;   
       if(PBBScenarioID != null ){
           PBB_Scenario__c  cloneScenario = PBB_Services.Clone(PBBScenarioID); 
           system.debug('-- PBBScenarioID--- ' + PBBScenarioID);
           PBBScenarioID = cloneScenario.id;            
           pageRef = new PageReference('/apex/PBB_ProjectInformation?id='+PBBScenarioID);
           pageRef.setRedirect(true);
       }
       return pageRef;          
   }
}