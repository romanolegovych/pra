global with sharing virtual class PAWS_CrossProjectProgressController 
{
	public static List<ID> taggedFlows = new List<ID>();
	public PAWS_CrossProjectProgressController() {}
	
	/****************************** PROPERTIES SECTION *********************************/
	
	public DateTime ReinitFlag  
	{
		get
		{
			if(ReinitFlag == null) ReinitFlag = DateTime.now();
			return ReinitFlag;
		}
		set;
	}

	public ApexPages.StandardSetController RowsSetController
    {
        get 
        {
            if(RowsSetController == null) 
            {
                RowsSetController = new ApexPages.StandardSetController(Database.getQueryLocator(buildQuery()));
                RowsSetController.setPageSize(5);
            }
            return RowsSetController;
        }
        set;
    }
    
    public List<Row> Rows
    {
    	get
    	{
    		if(RowsSetController == null) return null;
    		
    		return buildRows((List<PAWS_Project_Flow_Junction__c>)RowsSetController.getRecords());
    	}
    }
    
    public ID SeletedObjectId {get;set;}
    public ID SeletedFlowInstanceId {get;set;}
    public Boolean ObjectSpecificProgressMode {get;set;}
    
    private transient sObject SeletedObjectValue;
    public sObject SeletedObject 
    {
    	get
    	{
    		if(SeletedObjectValue == null && !String.isEmpty(SeletedObjectId))
    		{
    			List<String> fields = new List<String>();
		    	Schema.DescribeSObjectResult describe = ecrf__c.getSObjectType().getDescribe();
		    	fields.addAll(describe.fields.getMap().keySet());

    			SeletedObjectValue = Database.query('select ' + String.join(fields, ',') + ' from ecrf__c where Id = :SeletedObjectId');
    		}
    		return SeletedObjectValue;
    	}
    	private set { SeletedObjectValue = value; }
    }
    
    public String OrderBy 
    {
    	get
    	{
    		if(OrderBy == null) OrderBy = 'Name';
    		return OrderBy;
    	}
    	set;
    }
    
    public String OrderType
    {
    	get
    	{
    		if(OrderType == null) OrderType = 'asc';
    		return OrderType;
    	}
    	set;
    }
    
    //******************** STATUS FILTER SECTION ********************
    
    public String StatusFilter {get;set;}
    
    //******************** SPONSORS FILTER SECTION ********************
    
    public String SponsorFilter {get;set;}

    @RemoteAction
    public @ReadOnly static List<String> loadAvailableSponsors(String filter)
    {
    	PAWS_CrossProjectProgressController controller = new PAWS_CrossProjectProgressController();

		Set<String> names = new Set<String>();
		for (WFM_Client__c record : (List<WFM_Client__c>)Database.query('select Name from WFM_Client__c' + (!String.isEmpty(filter) ? ' where ' + controller.buildLikeFilter2('Name', filter) : '') + ' order by Name limit 100'))
		{
			names.add(record.Name);
		}

		List<String> namesList = new List<String>(names);
		namesList.sort();
		
		List<String> result = new List<String>();
		for(String name : namesList)
			result.add(name);

        return result;
    }
    
    //******************** PROJECTS FILTER SECTION ********************
    
    public String ProjectFilter {get;set;}

    @RemoteAction
    public @ReadOnly static List<String> loadAvailableProjects(String filter)
    {
    	PAWS_CrossProjectProgressController controller = new PAWS_CrossProjectProgressController();
    	
    	String conditions = 'Type__c = \'Regular\'';
		if(!String.isEmpty(filter)) conditions += ' and ' + controller.buildLikeFilter2('Name', filter);
				
		Set<String> names = new Set<String>();
		for (ecrf__c record : (List<ecrf__c>)Database.query('select Name from ecrf__c where ' + conditions + ' order by Name limit 100'))
		{
			names.add(record.Name);
		}

		List<String> namesList = new List<String>(names);
		namesList.sort();
		
		List<String> result = new List<String>();
		for(String name : namesList)
			result.add(name);

        return result;
    }
    
    //******************** COUNTRIES FILTER SECTION ********************
    
    public String SelectedCountries {get; set;}
	
	public String buildCountriesFilter()
	{
		return ' and Project__c in (select PAWS_Project__c from PAWS_Project_Flow_Country__c where Status__c = \'Active\' and ' + 
					buildInFilter('Name', selectedCountries.split(';')) 
				+ ')';
		/*
		return ' and (' + String.join(
			new List<String>{
				buildInFilter('Flow__r.PAWS_Project_Flow_Country__r.Name', selectedCountries.split(';')),
				buildInFilter('Flow__r.PAWS_Project_Flow_Site__r.PAWS_Country__r.Name', selectedCountries.split(';')),
				buildInFilter('Flow__r.PAWS_Project_Flow_Agreement__r.PAWS_Country__r.Name', selectedCountries.split(';')),
				buildInFilter('Flow__r.PAWS_Project_Flow_Document__r.PAWS_Country__r.Name ', selectedCountries.split(';')),
				buildInFilter('Flow__r.PAWS_Project_Flow_Submission__r.PAWS_Country__r.Name', selectedCountries.split(';'))
			}, 
			' or ')
		 + ')';
		 */
	}
	
	@RemoteAction
    public @ReadOnly static List<String> loadAvailableCountries(String filter)
    {
		List<String> result = new List<String>();
		for (AggregateResult each : !String.isEmpty(filter)
				? [select count(id), Name name from PAWS_Project_Flow_Country__c where Name != null and Name like :('%' + String.escapeSingleQuotes(filter) + '%') group by Name limit 100]
				: [select count(id), Name name from PAWS_Project_Flow_Country__c where Name != null group by Name limit 100])
		{
			result.add((String) each.get('name'));
		}
				
        return result;
    }
    
    //******************** PROTOCOLS FILTER SECTION ********************
    
    public String SelectedProtocols {get; set;}

	public String buildProtocolsFilter()
	{
		return ' and ' + buildInFilter('Project__r.Protocol_ID__r.Name', selectedProtocols.split(';'));
	}
	
	@RemoteAction
    public @ReadOnly static List<String> loadAvailableProtocols(String filter)
    {
		List<String> result = new List<String>();
		for (AggregateResult each : !String.isEmpty(filter)
						? [select count(id), Protocol_ID__r.Name name from ecrf__c where Protocol_ID__r.Name != null and Protocol_ID__r.Name like :('%' + String.escapeSingleQuotes(filter) + '%') group by Protocol_ID__r.Name limit 100]
						: [select count(id), Protocol_ID__r.Name name from ecrf__c where Protocol_ID__r.Name != null group by Protocol_ID__r.Name limit 100])
		{
			result.add((String)each.get('name'));
		}
				
        return result;
    }
    
    //******************** TAGS FILTER SECTION ********************
    
    public String selectedTags {get; set;}

	private String buildTagsFilter()
	{
		/*
		taggedFlows = new List<ID>();
		for (sObject each : Database.query('select STSWR1__Flow_Step__r.STSWR1__Flow__c flowid, count(id) from STSWR1__Flow_Step_Property__c where ' + buildInFilter('Name', selectedTags.split(';')) +  ' group by STSWR1__Flow_Step__r.STSWR1__Flow__c'))
		{
			taggedFlows.add((ID)each.get('flowid'));
		}
		
		return ' and Flow__c in :taggedFlows';
		*/
		return ' and Project__c in (select PAWS_Project__c from STSWR1__Flow_Step_Property__c where ' + buildInFilter('Name', selectedTags.split(';')) + ')';
	}
	
	
	@RemoteAction
    public @ReadOnly static List<String> loadAvailableTags(String filter)
    {
    	PAWS_CrossProjectProgressController controller = new PAWS_CrossProjectProgressController();
    	
    	String conditions = 'STSWR1__Type__c = \'Tag\' and Name != null';
		conditions += ' and ' + controller.buildInFilter('STSWR1__Flow_Step__r.STSWR1__Flow__r.STSWR1__Object_Type__c', new List<String>(PAWS_APIHelper.PAWS_OBJECTS));
		if(filter != null) conditions += ' and Name like \'%' + String.escapeSingleQuotes(filter) + '%\'';
		
		List<STSWR1__Flow_Step_Property__c> properties = (List<STSWR1__Flow_Step_Property__c>)Database.query('select Name from STSWR1__Flow_Step_Property__c where ' + conditions + ' order by STSWR1__Flow_Step__r.STSWR1__Flow__c limit 5000');
		Set<String> names = new Set<String>();     
		for (STSWR1__Flow_Step_Property__c property : properties)
		{
			names.add(property.Name);
            if(names.size() >= 50) break;
		}

        List<String> result = new List<String>(names);
        result.sort();
        return result;
    }
    
    /****************************** METHODS SECTION *********************************/
    
    public void refresh()
    {
    	RowsSetController = null;
    	SeletedFlowInstanceId = null;
    	SeletedObjectId = null;
    	SeletedObject = null;
    	ReinitFlag = null;
    }

    public void enableObjectSpecificProgressMode()
    {
    	ObjectSpecificProgressMode = true;
    	ReinitFlag = null;
    }
    
    public void cancelObjectSpecificProgressMode()
    {
    	ObjectSpecificProgressMode = false;
    	SeletedFlowInstanceId = null;
    	SeletedObjectId = null;
    	SeletedObject = null;
    	ReinitFlag = null;
    }
    
    private List<Row> buildRows(List<PAWS_Project_Flow_Junction__c> records)
    {
    	Set<ID> flowIds = new Set<ID>();
		Set<ID> recordIds = new Set<ID>();
		for(PAWS_Project_Flow_Junction__c record : records)
		{
			flowIds.add(record.Flow__c);
			recordIds.add(record.Project__c);
		}
    		
		Map<String, List<STSWR1__Flow_Instance__c>> instancesMap = new Map<String, List<STSWR1__Flow_Instance__c>>();
		for(STSWR1__Flow_Instance__c instance : [select STSWR1__Flow__c, STSWR1__Object_Id__c from STSWR1__Flow_Instance__c where STSWR1__Flow__c in :flowIds and STSWR1__Object_Id__c in :recordIds])
		{
			String key = instance.STSWR1__Flow__c + instance.STSWR1__Object_Id__c;
			if(!instancesMap.containsKey(key)) instancesMap.put(key, new List<STSWR1__Flow_Instance__c>());
			instancesMap.get(key).add(instance);
		}
    		
		List<Row> result = new List<Row>();
		for(PAWS_Project_Flow_Junction__c record : records)
		{
			String key = (String)record.Flow__c + (String)record.Project__c;
			ecrf__c projectObject = record.Project__r;
			
			List<STSWR1__Flow_Instance__c> instances = instancesMap.get(key);
			if(instances == null)
			{
				result.add(new Row(projectObject, null));
			}else{
				for(STSWR1__Flow_Instance__c instance : instances)
    			{
    				result.add(new Row(projectObject, instance));
    			}
			}
		}
		
		return result;
    }

    private String buildQuery()
    {
    	List<String> fields = new List<String>{'Project__c, Flow__c, Project__r.Name, Project__r.Sponsor__c'};
		String orderByLocal = 'Project__r.' + OrderBy;
	    return 'select ' + String.join(fields, ',') + ' from PAWS_Project_Flow_Junction__c where ' + buildFilters() + ' order by ' + orderByLocal + ' ' + OrderType + ', CreatedDate';
    }
    
    //****************************** FILTERS SECTION ******************************
    
    public void applyFilters()
    {
    	refresh();
    }
    
    public void clearFilters()
    {
    	SponsorFilter = null;
    	ProjectFilter = null;
    	StatusFilter = null;
    	selectedTags = null;
    	selectedProtocols = null;
    	selectedCountries = null;
    	refresh();
    }
    
    public virtual String buildFilters()
    {
    	String filters = 'Project__c != null and Project__r.Type__c = \'Regular\' and Flow__r.STSWR1__Status__c = \'Active\'';
    	if(!String.isEmpty(ProjectFilter)) 
		{
			List<String> projects = ProjectFilter.split(';');
			if(projects.size() == 1) filters += ' and ' + buildLikeFilter('Project__r.Name', projects[0]);
			else if(projects.size() > 1) filters += ' and ' + buildInFilter('Project__r.Name', projects);
		}
		
		if(!String.isEmpty(SponsorFilter)) 
		{
			//filters += ' and ' + buildLikeFilter('Project__r.Sponsor__c', SponsorFilter);
			filters += buildMultiselectFilter('Project__r.Sponsor__c', SponsorFilter, 'and');
		}
		
		if(!String.isEmpty(StatusFilter))
		{ 
			if(StatusFilter == 'Started') filters += ' and Flow_Instance__c != null';
			else filters += ' and Flow_Instance__c = null';
		}
		
		if (!String.isEmpty(selectedTags))
		{
			filters += buildTagsFilter();
		}
		
		if (!String.isEmpty(selectedProtocols))
		{
			filters += buildProtocolsFilter();
		}
		
		if (!String.isEmpty(selectedCountries))
		{
			filters += buildCountriesFilter();
		}
		
 		return filters;
    }
	
	/**
	* Builds multi-select filter - if filter value is a single value - returns LIKE clause,
	* otherwise returns IN clause.
	*/
	public String buildMultiselectFilter(String filterField, String filterValue, String operator)
	{
		String result = ' ';
		List<String> values = filterValue.split(';');
		if (values.size() == 1)
		{
			result += operator + ' ' + buildLikeFilter(filterField, values.get(0));
		}
		else if (values.size() > 1)
		{
			result += operator + ' ' + buildInFilter(filterField, values);
		}
		
		return result;
	}
	
    public String buildLikeFilter(String field, String value)
    {
    	return field + ' like \'' + String.escapeSingleQuotes(value) + '%\'';
    }
    
    public String buildLikeFilter2(String field, String value)
    {
    	return field + ' like \'%' + String.escapeSingleQuotes(value) + '%\'';
    }
    
    public String buildInFilter(String field, List<String> values)
    {
    	List<String> escapedValues = new List<String>();
		for (String value : values)
		{
			escapedValues.add(value != null ? String.escapeSingleQuotes(value) : value);
		}
		
		return field + ' in (\'' + String.join(escapedValues, '\',\'') + '\')';
    }
    
    public Map<String, STSWR1__Flow_Instance_History__c> loadHistory(List<STSWR1__Flow_Instance__c> instances)
    {
        List<String> fields = new List<String>();
    	Schema.DescribeSObjectResult describe = STSWR1__Flow_Instance_History__c.getSObjectType().getDescribe();
    	fields.addAll(describe.fields.getMap().keySet());
    	fields.add('STSWR1__Cursor__r.STSWR1__Flow_Instance__c');
    	fields.add('STSWR1__Cursor__r.STSWR1__Step__c');
    	fields.add('STSWR1__Cursor__r.STSWR1__Is_Skipped__c');
    	fields.add('STSWR1__Cursor__r.STSWR1__Flow_Branch__c');
    	fields.add('STSWR1__Step__r.STSWR1__Flow_Branch__c');
        	
       	Map<String, STSWR1__Flow_Instance_History__c> result = new Map<String, STSWR1__Flow_Instance_History__c>();
        List<STSWR1__Flow_Instance_History__c> history = (List<STSWR1__Flow_Instance_History__c>)Database.query('select ' + String.join(fields, ',') + ' from STSWR1__Flow_Instance_History__c where STSWR1__Cursor__r.STSWR1__Flow_Instance__c in :instances order by STSWR1__Cursor__c, CreatedDate, Name');
		for(Integer i = 0; i < history.size(); i++)
		{
			String key = history[i].STSWR1__Cursor__r.STSWR1__Flow_Instance__c + (String)history[i].STSWR1__Step__c;
			
			if(!result.containsKey(key) || history[i].STSWR1__Step__r.STSWR1__Flow_Branch__c == (ID)history[i].STSWR1__Cursor__r.STSWR1__Flow_Branch__c)
				result.put(key, history[i]);
		}

        return result;
    }
    
    public Map<ID, STSWR1__Gantt_Step_Property__c> loadStepProperties(List<STSWR1__Flow_Instance__c> instances)
    {
    	Set<ID> flowIds = new Set<ID>();
    	for(STSWR1__Flow_Instance__c instance : instances)
    		flowIds.add(instance.STSWR1__Flow__c);
    		
    	return (Map<ID, STSWR1__Gantt_Step_Property__c>)new STSWR1.API().call('GanttStepPropertyService', 'getFlowsPropertiesMap', new Map<String, Object> {'flowIds' => flowIds});	
    }
    
    public Map<ID, Set<String>> loadStepTags(List<STSWR1__Flow_Instance__c> instances)
	{	
		Set<ID> ids = new Set<ID>();
		for(STSWR1__Flow_Instance__c instance : instances)
			ids.add(instance.STSWR1__Flow__c);
		
		Map<ID, Set<String>> properties = new Map<ID, Set<String>>();
		for(STSWR1__Flow_Step_Property__c property : [select Name, STSWR1__Flow_Step__c from STSWR1__Flow_Step_Property__c where STSWR1__Flow_Step__r.STSWR1__Flow__c in :ids and STSWR1__Type__c = 'Tag' order by CreatedDate])
		{
			if(!properties.containsKey(property.STSWR1__Flow_Step__c)) properties.put(property.STSWR1__Flow_Step__c, new Set<String>());
			properties.get(property.STSWR1__Flow_Step__c).add(property.Name);
		}
		
		return properties;
	}

    
    /***************************** EXPORT SECTION ******************************/
    
    @RemoteAction
    global static Map<String, Object> exportData(Integer pageNumber, String sponsorFilter, String projectFilter, String statusFilter, String tagsFilter, String protocolsFilter, String countriesFilter, String orderBy, String orderType) 
    {
    	Map<String, Object> result = new Map<String, Object>();
    	result.put('page', pageNumber);
    	
        try
        {
        	Integer pageSize = 50;
				       			
        	PAWS_CrossProjectProgressController controller = new PAWS_CrossProjectProgressController();
		    controller.SponsorFilter = (String.isEmpty(sponsorFilter) ? null : sponsorFilter);
		    controller.ProjectFilter = (String.isEmpty(projectFilter) ? null : projectFilter);
		    controller.StatusFilter = (String.isEmpty(statusFilter) ? null : statusFilter);
		    controller.selectedTags = tagsFilter;
		    controller.selectedProtocols = protocolsFilter;
		    controller.selectedCountries = countriesFilter;
        	controller.OrderBy = (String.isEmpty(orderBy) ? null : orderBy);
    		controller.OrderType = (String.isEmpty(orderType) ? null : orderType);
	
        	String query = controller.buildQuery();
        	String queryForTotal = query.substring(0, 7) + ' count() ' + query.substring(query.indexOf(' from '), query.indexOf(' where '));
        	Integer totalRecords = Database.countQuery(queryForTotal);
        	result.put('total', totalRecords);

    		query += ' LIMIT ' + String.valueOf(pageSize) + ' OFFSET ' + String.valueOf(pageNumber * pageSize);
        	System.debug(query);

        	List<Row> rowList = controller.buildRows((List<PAWS_Project_Flow_Junction__c>)Database.query(query));
        	List<STSWR1__Flow_Instance__c> instances = new List<STSWR1__Flow_Instance__c>();
        	for(Row rowItem : rowList)
        	{
        		if(rowItem.Instance != null) instances.add(rowItem.Instance);
        	}

			Map<ID, Map<String, List<STSWR1__Flow_Step_Junction__c>>> progressMap = (Map<ID, Map<String, List<STSWR1__Flow_Step_Junction__c>>>)new STSWR1.API().call('WorkflowService', 'getFlowProgress', instances);
			Map<String, STSWR1__Flow_Instance_History__c> historyMap = controller.loadHistory(instances);
			Map<ID, STSWR1__Gantt_Step_Property__c> propertiesMap = controller.loadStepProperties(instances);
			Map<ID, Set<String>> stepTagsMap = controller.loadStepTags(instances);
			List<String> tagsList = (!String.isEmpty(tagsFilter) ? tagsFilter.split(';') : null);
			
			List<Map<String, Object>> objects = new List<Map<String, Object>>();
        	for(Row rowItem : rowList)
        	{
        		List<Map<String, Object>> steps = new List<Map<String, Object>>();
        		
        		if(rowItem.Instance != null)
        		{
        			Map<String, List<STSWR1__Flow_Step_Junction__c>> progress = progressMap.get(rowItem.Instance.Id);
	  				for(STSWR1__Flow_Step_Junction__c step : progress.get('past'))
	  				{
	  					if(!isStepAllowed(stepTagsMap.get(step.Id), tagsList)) continue;
	  					
	  					STSWR1__Gantt_Step_Property__c property = propertiesMap.get(step.Id);
	  					STSWR1__Flow_Instance_History__c historyItem = historyMap.get((String)rowItem.Instance.Id + (String)step.Id);
	  					Boolean isSkipped = (property != null && property.STSWR1__Is_Skipped__c == true) || (historyItem != null && historyItem.STSWR1__Comments__c == 'Skipped based on conditions.') || (historyItem != null && historyItem.STSWR1__Step__c == historyItem.STSWR1__Cursor__r.STSWR1__Step__c && historyItem.STSWR1__Cursor__r.STSWR1__Is_Skipped__c == true);
	  
	  					if(isSkipped)
	  					{
	  						continue;
	  					}else{
	  						steps.add(new Map<String, Object>
		  					{
		  						'type' => 'past',
		  						'stepName' => step.Name
		  					});
	  					}
	  				}
	  				
	    			for(STSWR1__Flow_Step_Junction__c step : progress.get('pending'))
	  				{
	  					if(!isStepAllowed(stepTagsMap.get(step.Id), tagsList)) continue;
	  					
	  					STSWR1__Gantt_Step_Property__c property = propertiesMap.get(step.Id);
	  					if(property != null && property.STSWR1__Is_Skipped__c)
	  					{
	  						continue;
	  					}else{
		  					steps.add(new Map<String, Object>
		  					{
		  						'type' => 'pending',
		  						'stepName' => step.Name,
		  						'isLate' => (property != null ? property.STSWR1__Is_Late__c : false)
		  					});
	  					}
	  				}
	  				
	  				for(STSWR1__Flow_Step_Junction__c step : progress.get('current'))
	  				{
	  					if(!isStepAllowed(stepTagsMap.get(step.Id), tagsList)) continue;
	  					
	  					STSWR1__Gantt_Step_Property__c property = propertiesMap.get(step.Id);
	  					STSWR1__Flow_Instance_History__c historyItem = historyMap.get((String)rowItem.Instance.Id + (String)step.Id);
	  					Boolean isSkipped = (property != null && property.STSWR1__Is_Skipped__c == true) || (historyItem != null && historyItem.STSWR1__Comments__c == 'Skipped based on conditions.') || (historyItem != null && historyItem.STSWR1__Step__c == historyItem.STSWR1__Cursor__r.STSWR1__Step__c && historyItem.STSWR1__Cursor__r.STSWR1__Is_Skipped__c == true);

	  					if(isSkipped)
	  					{
	  						continue;
	  					}else{
		  					steps.add(new Map<String, Object>
		  					{
		  						'type' => 'current',
		  						'stepName' => step.Name,
		  						'isLate' => (property != null ? property.STSWR1__Is_Late__c : false)
		  					});
	  					}
	  				}
	  				
	  				for(STSWR1__Flow_Step_Junction__c step : progress.get('future'))
	  				{
	  					if(!isStepAllowed(stepTagsMap.get(step.Id), tagsList)) continue;
	  					
	  					STSWR1__Gantt_Step_Property__c property = propertiesMap.get(step.Id);
	  					if(property != null && property.STSWR1__Is_Skipped__c)
	  					{
	  						continue;
	  					}else{
		  					steps.add(new Map<String, Object>
		  					{
		  						'type' => 'future',
		  						'stepName' => step.Name,
		  						'isLate' => (property != null ? property.STSWR1__Is_Late__c : false)
		  					});
	  					}
	  				}
        		}
        		
        		objects.add(new Map<String, Object>{
        			'ProjectName' => rowItem.Record.Name,
        			'Sponsor' => rowItem.Record.Sponsor__c,
        			'ObjectId' => rowItem.Record.Id,
        			'ObjectName' => rowItem.Record.Name,
        			'Steps' => steps
        		});
        	}
           
           	result.put('filters', new Map<String, String>{
    				'Sponsor' => !String.isEmpty(controller.SponsorFilter) ? controller.SponsorFilter : '',
    				'Project' => !String.isEmpty(controller.ProjectFilter) ? controller.ProjectFilter : '',
    				'Status' => !String.isEmpty(controller.StatusFilter) ? controller.StatusFilter : '',
    				'View' => !String.isEmpty(controller.selectedTags) ? controller.selectedTags : '',
    				'Protocol' => !String.isEmpty(controller.selectedProtocols) ? controller.selectedProtocols : '',
    				'Country' => !String.isEmpty(controller.selectedCountries) ? controller.selectedCountries : ''
           	});
    			
        	result.put('rows', objects);
        	
        	if(objects.size() < pageSize) result.put('done', true);
        }catch(Exception ex)
        {
           result.put('error', ex.getMessage());
        }
        
        return result;
    }
    
    private static Boolean isStepAllowed(Set<String> stepTags, List<String> tagsList)
    {
    	if(tagsList == null) return true;
    	if(stepTags == null) return false;
    	
    	Boolean flag = false;
		for(String tag : tagsList)
			flag |= stepTags.contains(tag);

		return flag;
    }

    /****************************** OTHER SECTION *********************************/
    
    public class Row
    {
    	public Row(ecrf__c record, STSWR1__Flow_Instance__c instance)
    	{
    		this.Record = record;
    		this.Instance = instance;
    	}
    	
    	public ecrf__c Record {get;set;}
    	public STSWR1__Flow_Instance__c Instance {get;set;}
    }
    
    public class Step
    {
    	public Step(STSWR1__Flow_Step_Junction__c record, String status)
    	{
    		this.Record = record;
    		this.Status = status;
    	}
    	
    	public STSWR1__Flow_Step_Junction__c Record {get;set;}
    	public String Status {get;set;}
    }
}