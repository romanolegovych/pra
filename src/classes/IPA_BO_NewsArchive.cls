public class IPA_BO_NewsArchive
{
    IPA_DAL_NewsArchive ipanewsdal_c;
    
    public IPA_BO_NewsArchive()
    {
        ipanewsdal_c = new IPA_DAL_NewsArchive();
    }
    
    /*public List<IPA_Links__c> returnQuickLinks() {    
        return ipanewsdal_c.returnQuickLinks();
    }*/
    
    /*public List<IPA_Articles__c> returnPolicyUpdateArchive(String Type) {    
        return ipanewsdal_c.returnPolicyUpdateArchive(Type);
    }
    
    public List<IPA_Articles__c> returnPRAArchive(String Type) {    
        return ipanewsdal_c.returnPRAArchive(Type);
    }*/
    
    public List<IPA_Articles__c> returnNewsOf3TypesArchive(String Type) {    
        return ipanewsdal_c.returnNewsOf3TypesArchive(Type);
    }
    
    public List<String> returnUniqueMonthsForArchive(String Type) {    
        return ipanewsdal_c.returnUniqueMonthsForArchive(Type);
    }
    
    public List<IPA_Articles__c> returnArchiveForMonth(Integer month, Integer year, String Type) {
        return ipanewsdal_c.returnArchiveForMonth(month, year, Type);
    }
}