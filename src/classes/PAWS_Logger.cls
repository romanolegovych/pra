public with sharing class PAWS_Logger implements STSWR1.ILogger
{
	public void log(String info)
	{
		System.debug(info);
	}
}