@isTest
private class AA_CostRateUploadTest{   
    
    /* Test Read File Button options */
    static testMethod void ReadFileErrorTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Attachment attach=new Attachment();    
         attach.Name='Unit Test Attachment';
         Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');

         AA_CostRateUpload upload = new AA_CostRateUpload();
         String blobCreator = 'Row 1' + '\r\n' + 'BU1F1,2011,CUR1,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16' 
                                      + '\r\n' + 'BU1F1,2011,CUR1,,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16'
                                      + '\r\n' + 'BU1F1,2011,CUR1,1,2,3,4,A,6,7,8,9,10,11,12,13,14,15,16'
                                      + '\r\n' + '"BU1F1","2011","CUR1","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"' 
                                      + '\r\n' + '"BU1F1","2011","CUR1","1","2","3","4","A","6","7","8","9","10","11","12","13","14","15","16"'
                                      + '\r\n' + '"BU1F1","2011","CUR1","","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"'  
                                      + '\r\n' + ',2011,CUR1,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16'
                                      + '\r\n' + 'BU1F1,1990,CUR1,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16'
                                      + '\r\n' + 'BU1F1,2011,,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16'
                                      + '\r\n' + 'BU1F1,2011,,1,2,3,,5,6,7,8,9,10,11,12,13,14,15,16';
         upload.contentFile = blob.valueof(blobCreator);
         upload.ReadFile();
         upload.closeUpdatePopup();
         upload.closeConfirmationPopup();
         upload.continueButton();
         System.assertEquals(upload.ReadFile(),null);
         
         Test.stopTest();
    }
    
        /* Test Read File Button options */
    static testMethod void ReadFilePassTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Attachment attach=new Attachment();    
         attach.Name='Unit Test Attachment';
         Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');

         AA_CostRateUpload upload = new AA_CostRateUpload();
         String blobCreator = 'Row 1' + '\r\n' + 'BU1F1,2011,USD,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16';
         upload.contentFile = blob.valueof(blobCreator);
         upload.ReadFile();
         upload.closeUpdatePopup();
         upload.closeConfirmationPopup();
         upload.continueButton();
         System.assertEquals(upload.ReadFile(),null);
         
         Test.stopTest();
    }
    
    /* Test PopUps options */
    static testMethod void popupTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_CostRateUpload upload = new AA_CostRateUpload();
         upload.showUpdatePopup();
         upload.showConfirmationPopup();
         System.assertEquals(1,1);
         Test.stopTest();
    }
    
    /* Test backpresseed button*/
    static testMethod  void backpressedTest(){
        AA_CostRateUpload upload = new AA_CostRateUpload();
        PageReference pageref = upload.backpressed();
        System.assertEquals(pageref.geturl(),Page.AA_Cost_Rate.geturl());
    }  
    /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }  
    
    
}