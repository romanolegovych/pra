/**
 * Unit test for Mdm Protocol Service and wrapper classes.
 */
@isTest
private class MdmProtocolServiceUTest {
	
	static list<MuleServicesCS__c> serviceSettings;
	
	static void init() {
		serviceSettings = new MuleServicesCS__c[] {
			new MuleServicesCS__c(name = 'ENDPOINT', Value__c = 'ENDPOINT'),
			new MuleServicesCS__c(name = 'TIMEOUT', Value__c = '60000'),
			new MuleServicesCS__c(name = 'MdmProtocolService', Value__c = 'MdmProtocolService')
		};
		insert serviceSettings;
	}
	
	// This testmethod is just to check that the mocking is working
	// Assertions will be made for classes that use the service wrapper
	static testMethod void testGenerateResponses_POSITIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(false));
		MdmProtocolService.protocolListResponse protListResp = MdmProtocolServiceWrapper.getProtocolsLikeClientProtocolNum('value');
		MdmProtocolService.protocolResponse protResp = MdmProtocolServiceWrapper.getProtocolVOByClientProtNum('clientProtNum');
		protResp = MdmProtocolServiceWrapper.getProtocolVOBySystemId('systemId');
		protListResp = MdmProtocolServiceWrapper.getProtocolsNotMappedToSite();
		protListResp = MdmProtocolServiceWrapper.getProtocolsNotMappedToSiteBySystemId('systemId');
		MdmProtocolService.protocolVO protocolVO = new MdmProtocolService.protocolVO();
		MdmProtocolService.valueResponse valueResp = MdmProtocolServiceWrapper.getEDocsUrlForProtocol(protocolVO);
		valueResp = MdmProtocolServiceWrapper.getEDocsUrlByClientProtNum('clientProtNum');
		MdmProtocolService.mdmCrudResponse mdmCrudResp = MdmProtocolServiceWrapper.saveProtocolFromVO(protocolVO);
		mdmCrudResp = MdmProtocolServiceWrapper.deleteProtocolFromVO(protocolVO);
		MdmProtocolService.regionVO regionVO = new MdmProtocolService.regionVO();
		mdmCrudResp = MdmProtocolServiceWrapper.saveRegionMappingForProtocolResponse(regionVO);
		MdmProtocolServiceWrapper.getErrors();
		Test.stopTest();
	}
	
	static testMethod void testGenerateResponses_NEGATIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(true));
		MdmProtocolService.protocolListResponse protListResp = MdmProtocolServiceWrapper.getProtocolsLikeClientProtocolNum('value');
		MdmProtocolService.protocolResponse protResp = MdmProtocolServiceWrapper.getProtocolVOByClientProtNum('clientProtNum');
		protResp = MdmProtocolServiceWrapper.getProtocolVOBySystemId('systemId');
		protListResp = MdmProtocolServiceWrapper.getProtocolsNotMappedToSite();
		protListResp = MdmProtocolServiceWrapper.getProtocolsNotMappedToSiteBySystemId('systemId');
		MdmProtocolService.protocolVO protocolVO = new MdmProtocolService.protocolVO();
		MdmProtocolService.valueResponse valueResp = MdmProtocolServiceWrapper.getEDocsUrlForProtocol(protocolVO);
		valueResp = MdmProtocolServiceWrapper.getEDocsUrlByClientProtNum('clientProtNum');
		MdmProtocolService.mdmCrudResponse mdmCrudResp = MdmProtocolServiceWrapper.saveProtocolFromVO(protocolVO);
		mdmCrudResp = MdmProtocolServiceWrapper.deleteProtocolFromVO(protocolVO);
		MdmProtocolService.regionVO regionVO = new MdmProtocolService.regionVO();
		mdmCrudResp = MdmProtocolServiceWrapper.saveRegionMappingForProtocolResponse(regionVO);
		MdmProtocolServiceWrapper.getErrors();
		Test.stopTest();
	}
}