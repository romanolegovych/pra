@IsTest
public with sharing class BDT_ServiceRiskControllerTest {

	static testMethod void ServiceRiskControllerTest(){
		
		ServiceCategory__c sc = new ServiceCategory__c(name = 'Test',code__c = '001.001');
        upsert sc;
    	List<Service__c> sl =new List<Service__c>();
        sl.add(new Service__c(name = 'Test', ServiceCategory__c = sc.id, IsDesignObject__c=true, SequenceNumber__c=0));
        sl.add(new Service__c(name = 'Test2', ServiceCategory__c = sc.id, IsDesignObject__c=true, SequenceNumber__c=0));
        upsert sl;
        
        List<ServiceRisk__c> srl = new List<ServiceRisk__c>();
        srl.add(new ServiceRisk__c(Service__c=sl[0].Id, Name='Test1', CommonRisk__c='Test1Risk1', CommonMitigation__c='Test1Mitigation1'));
        srl.add(new ServiceRisk__c(Service__c=sl[0].Id, Name='Test2', CommonRisk__c='Test2Risk2', CommonMitigation__c='Test2Mitigation2'));
        upsert srl;
         	
		
		PageReference b = new Pagereference( system.Page.BDT_ServiceRisks.getUrl());
		b.getParameters().put('ServiceCategoryId',sc.id);
		b.getParameters().put('ScId',sc.id);
		b.getParameters().put('ServiceId',sl[0].id);
		
		Test.setCurrentPage(b);
		BDT_ServiceRisksController a = new BDT_ServiceRisksController();
		
		List<ServiceCategory__c> c = a.getServiceCatListClean();
		
		system.assertEquals(1, c.size() );
		
		a.showServiceRisksPerCategory();
		
		system.assertNotEquals(null, a.editServiceRisks());
		
		system.assertNotEquals(null,a);
		
		a.loadServiceCategories();
		system.assertEquals(1, a.ServiceCategoryList.size() );
		
		a.loadServiceRisksPerCategory( sc.id );
		/* ServiceRisklist will contain all the risks + one empty risk for each service without risks - in this case 2+1=3 */
		system.assertEquals(3, a.ServiceRiskList.size() );
	
	}

}