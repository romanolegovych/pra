/**
* @author Sukrut Wagh
* @date 06/24/2014
* @description	Encapsulates the handler attributes for processing from COM_TriggerDispatcherBase
*/
public class COM_TriggerHandlerMeta {

	/** 
	* @description	Reference to Handler instance	
	*/
	public ITriggerHandler handler { get; set; }
	/** 
	* @description	Flag indicating handler is processing. Used to handle rentrant logic 
	*/
	public Boolean isProcessing { get; set; }
	
}