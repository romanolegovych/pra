public with sharing class BDT_NewEditStudySite {

	public Client_Project__c singleProject	{get;set;}
	public Business_Unit__c businessUnit 	{get;set;}
	public String projectId {get;set;}
	public Set<Business_Unit__c> buList{get;set;}
	public List<SelectOption> buOptions{get;set;}
	public String bu {get;set;}	
	public List<ProjectSiteWrapper> projectSites {get;set;}
	public Boolean showDeleted{
		get {if (showDeleted == null) {
				showDeleted = false;
			}
			return showDeleted;
		}
		set;
	}
	
	List<ProjectSite__c> psToSave;
	List<ProjectSite__c> psToDelete;
	List<ProjectSite__c> selectedSites;
	
	public BDT_NewEditStudySite(){
		projectId = BDT_Utils.getPreference('SelectedProject');
				
		if(projectId!=null && projectId!=''){
			singleProject = BDT_Utils.getProject(projectId, showDeleted);
			
			//build Bu select list
			buList = new Set<Business_Unit__c>();
			for(Study__c st: singleProject.Studies__r){
				if(st.Business_Unit__c!=null){
					buList.add( st.Business_Unit__r );
				}
			}
			buildBUOptions();
		}
	}
	
	public void buildBUOptions(){
		buOptions = new List<SelectOption>();
		buOptions.add(new SelectOption('1','Please select a Business Unit'));
		for(Business_Unit__c bu: buList){
			buOptions.add(new SelectOption(bu.Id, bu.Name));
		}
	}
	

	public class ProjectSiteWrapper{
		public Site__c site {get;set;}
		public Boolean selected {get;set;}
		public Boolean siteUsedForStudy {get;set;}
		public String projectId {get;set;}
		
		public ProjectSiteWrapper( Site__c pSite, boolean pSelected, String pProjectId, boolean pSiteUsedForStudy){
			site = pSite;
			selected = pSelected;
			projectId = pProjectId;
			siteUsedForStudy = pSiteUsedForStudy;
		}
	}
	
	public void buildSiteSelector(){
		projectSites = new List<ProjectSiteWrapper>();
		selectedSites = new List<ProjectSite__c>();
		List<Site__c> sitesList = [Select name, id from Site__c where Business_Unit__c =  :Id.valueOf(bu) and BDTDeleted__c = false ];
		try{
			selectedSites = [Select p.site__r.Name
								, p.Site__r.Id
								, p.Site__c
								, p.Name
								, p.Client_Project__c
								, (Select Id From StudySiteAssignments__r) 
							From ProjectSite__c p
							where Client_Project__c = :projectId];
		}
		catch(exception e){
			selectedSites = new List<ProjectSite__c>();
		}
		
		
		for(Site__c s: sitesList){
			Boolean isSelected = false;
			Boolean siteUsedForStudy = false;
			for(ProjectSite__c ps: selectedSites){
				if(ps.Client_Project__c == projectId && ps.Site__c == s.Id  ){
					isSelected = true;
					// check if site is already assigned to study
					if (ps.StudySiteAssignments__r.size()>0) {
						siteUsedForStudy = true;
					}
				}
			}
			projectSites.add(new ProjectSiteWrapper( s, isSelected, projectId, siteUsedForStudy));
		}
	}
	
	public PageReference save(){
		buildProjectSites();
		if(!psToSave.isEmpty() ){
			insert psToSave;
		}
		if(!psToDelete.isEmpty() ){
			delete psToDelete;
		}
		return BDT_Characteristics.returnToCharacteristics();
	}
	
	public void buildProjectSites(){
		psToDelete = new List<ProjectSite__c>();
		psToSave = new List<ProjectSite__c>();
		
		for(ProjectSiteWrapper psw: projectSites ){
			Boolean exists = false;
			if(psw.selected){
				for(ProjectSite__c ps: selectedSites){
					//selectedSites is already filtered for the projectID, so only need to check the site Id
					if(/*ps.Client_Project__c.Id == psw.projectId &&*/ ps.Site__c == psw.site.Id ){
						exists= true;
					}
				}
				if(!exists){
					psToSave.add(new ProjectSite__c(Client_Project__c = psw.projectId, Site__c = psw.site.Id ));
				}
			}
			else{
				for(ProjectSite__c ps: selectedSites){
					//selectedSites is already filtered for the projectID, so only need to check the site Id
					if(ps.Site__c == psw.site.Id ){
						psToDelete.add(ps);
					}
				}
			}
			
		}
		
		
	}
	public PageReference cancel(){
		return BDT_Characteristics.returnToCharacteristics();
	}
}