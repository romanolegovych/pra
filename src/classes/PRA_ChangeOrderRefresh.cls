public with sharing class PRA_ChangeOrderRefresh {
    
    public static void refreshChangeOrder(Id changeOrderId) {
        
        // get change order items and line itmes
        List<Change_Order_Item__c> liChangeOrderItems = 
        [select Id, Change_Order__c, Client_Task__c, Client_Unit_Number__c, Combo_Code__c, Description__c, End_Date__c, Forecast_Curve__c, 
            Number_Of_Contracted_Units__c, Project_Region__c, Start_Date__c, Task_Group__c, Total_Cost__c, Type__c, Unit_Completed_Event_Lookup__c, 
            Unit_Cost__c, Unit_of_Measurement__c, Worked_Hours__c, Worked_Hours_Done__c, Worked_Units__c, Worked_Units_Done__c, 
            Change_Order__r.Client_Project__c,
            (select Id, BUF_Code__c, Current_Contract_Hours__c, Current_Contract_Value__c, Current_Hours_Unit__c, 
                Current_Unit_Cost__c, Current_Total_Cost__c, Type__c from Change_Order_Line_Item__r) 
            from Change_Order_Item__c 
            where Change_Order__c = :changeOrderId];        
        
        Map <Id, Change_Order_Item__c> mapCOItems = new Map <Id, Change_Order_Item__c>(); // key - task.id, value - change order item
        Map <String, Change_Order_Line_Item__c> mapCOLineItems = new Map <String, Change_Order_Line_Item__c>(); // key - task-buf code, change order line item
        
        // check if new client task where created for new change order items 
        // match by client task.client unit number 
        List<String> liClientUnitNumber =  new List<String>();
        List<Id> liProjectIds = new List<Id>();
        for(Change_Order_Item__c coItem : liChangeOrderItems) {
            if(coItem.Type__c == PRA_ChangeOrderController.CHANGE_ORDER_ITEM_STATUS_NEW || coItem.Client_Task__c == null) {
                liClientUnitNumber.add(coItem.Client_Unit_Number__c);
                liProjectIds.add(coItem.Change_Order__r.Client_Project__c);
            }
        }
        
        List<Client_Task__c> liNewTasks = [select Id, Client_Unit_Number__c, Combo_Code__c, Project__c
                                            from Client_Task__c
                                            where Client_Unit_Number__c in :liClientUnitNumber and Project__c in :liProjectIds];
        system.debug('------------liNewTasks------------'+liNewTasks+liClientUnitNumber+liProjectIds);
        Map<String, Client_Task__c> mapClientTasks = new Map<String, Client_Task__c>(); // key - client unit number + project id
        for(Client_Task__c task : liNewTasks) {
            String key = task.Client_Unit_Number__c + '-' + task.Project__c ;
            mapClientTasks.put(key, task);
        }                               
        
        for(Change_Order_Item__c coItem : liChangeOrderItems) {
            // if new task was added set it for change order item
            if(coItem.Type__c == PRA_ChangeOrderController.CHANGE_ORDER_ITEM_STATUS_NEW || coItem.Client_Task__c == null) {
                String key = coItem.Client_Unit_Number__c + '-' + coItem.Change_Order__r.Client_Project__c;
                system.debug('------------------key------------------------'+key);
                if(mapClientTasks.containsKey(key)) {
                    coItem.Type__c = PRA_ChangeOrderController.CHANGE_ORDER_ITEM_STATUS_MODIFIED;
                    coItem.Client_Task__c = mapClientTasks.get(key).Id; // there should be only one task
                    coItem.Client_Unit_Number__c = mapClientTasks.get(key).Client_Unit_Number__c;
                    coItem.Combo_Code__c = mapClientTasks.get(key).Combo_Code__c;
                    system.debug('------------------key-2-----------------------'+coitem);
                }
            }
            mapCOItems.put(coItem.Client_Task__c, coItem);
            for(Change_Order_Line_Item__c coLineItem : coItem.Change_Order_Line_Item__r) {
                String key = coItem.Client_Task__c + '-' + coLineItem.BUF_Code__c;
                system.debug('------------------mapCOLineItems key------------------------');
                mapCOLineItems.put(key, coLineItem);
            }
        }
        
        // update change order items, with new source values
        Map <Id,Change_Order_Item__c> mapChangeOrderItems = new Map <Id, Change_Order_Item__c>();
        List<Client_Task__c> coTasks = 
        [select Id, Description__c, Client_Unit_Number__c, Project_Region__r.Name, Project_Region__c, Total_Worked_Units__c, 
            Total_Worked_Hours__c, Forecast_Curve__c, Combo_Code__c, Start_Date__c, Original_End_Date__c,Revised_End_Date__c, Task_Group__c, Unit_of_Measurement__c, 
            Contract_Value__c, Total_Contract_Units__c, Total_Units__c from Client_Task__c 
            where Id in :mapCOItems.keySet() order by Description__c, Project_Region__r.Name];
        
        for(Client_Task__c task : coTasks) {
            Decimal totalUnits = 0;
            Decimal contractCost = 0;
            Decimal unitCost = 0;
            if(task.Total_Contract_Units__c != null) {
                totalUnits += task.Total_Contract_Units__c;
            }
            if(task.Contract_Value__c != null) {
                contractCost += task.Contract_Value__c;
            }
            if(totalUnits != 0 && contractCost != 0) {
                unitCost = contractCost / totalUnits;
            }
            
            Change_Order_Item__c coItem     = mapCOItems.get(task.Id);
            coItem.Client_Unit_Number__c    = task.Client_Unit_Number__c;
            coItem.Description__c           = task.Description__c;
            coItem.Project_Region__c        = task.Project_Region__c;
            coItem.Unit_of_Measurement__c   = task.Unit_of_Measurement__c;
            coItem.Combo_Code__c            = task.Combo_Code__c;
            coItem.Start_Date__c            = task.Start_Date__c;
            if(task.Revised_End_Date__c!=null)
                coItem.End_Date__c          = task.Revised_End_Date__c;
            else
            coItem.End_Date__c              = task.Original_End_Date__c;
            coItem.Forecast_Curve__c        = task.Forecast_Curve__c;
            coItem.Task_Group__c            = task.Task_Group__c;
            coItem.Number_of_Contracted_Units__c = totalUnits;
            coItem.Worked_Units__c          = task.Total_Worked_Units__c;
            coItem.Worked_Hours__c          = task.Total_Worked_Hours__c;
            coItem.Unit_Cost__c             = unitCost;
            coItem.Total_Cost__c            = contractCost;
        }
        update mapCOItems.values();
        
        // update change order line items
        List<Change_Order_Line_Item__c> liUpsertedCOLineItems = new List<Change_Order_Line_Item__c>();
        List <Bid_Contract_Detail__c> liBidCOntractsDetails = 
        [select Id, Client_Task__c, Client_Task__r.Additional_Units_With_No_Contract_Value__c, 
            Client_Task__r.Contract_Value__c, Client_Task__r.Name, 
            Client_Task__r.Contracted_Number_of_Units__c, Client_Task__r.Status__c, Client_Task__r.Average_Contract_Effort__c,
            Client_Task__r.Total_Contract_Hours__c, Client_Task__r.Total_Contract_Units__c, BUF_Code__c, 
            BUF_Code__r.Name, BUF_Code__r.PRA_BUF_Code_Id__c, BUF_Code__r.PRA_Region__c, Contracted_Hours__c, Contracted_Value__c
        from Bid_Contract_Detail__c
        where Client_Task__c in :mapCOItems.keySet()];
        
        system.debug('-------------------- bidDetail size ----------------------' + liBidCOntractsDetails.size());
                
        for(Bid_Contract_Detail__c bidDetails : liBidCOntractsDetails) {
            
            Change_Order_Line_Item__c coLineItem;
            String key = bidDetails.Client_Task__c + '-' + bidDetails.BUF_Code__c;
            
            system.debug('------------------bcd key------------------------' + key);
            
            if(mapCOLineItems.containsKey(key)) {
                coLineItem = mapCOLineItems.get(key);
                if(coLineItem.Type__c == PRA_ChangeOrderController.CHANGE_ORDER_ITEM_STATUS_NEW) {
                    coLineItem.Type__c = PRA_ChangeOrderController.CHANGE_ORDER_ITEM_STATUS_MODIFIED;
                }
            } else {
                coLineItem = new Change_Order_Line_Item__c();
                coLineItem.Change_Order_Item__c = mapCOItems.get(bidDetails.Client_Task__c).Id;
                coLineItem.BUF_Code__c          = bidDetails.BUF_Code__c;
                coLineItem.Type__c              = PRA_ChangeOrderController.CHANGE_ORDER_ITEM_STATUS_MODIFIED;
            }
            
            coLineItem.Current_Contract_Hours__c    = bidDetails.Contracted_Hours__c;
            coLineItem.Current_Contract_Value__c    = bidDetails.Contracted_Value__c;
                        
            system.debug('--------------- updated contract hours -----------------' + coLineItem.Current_Contract_Hours__c);
            system.debug('--------------- updated contract value -----------------' + coLineItem.Current_Contract_Value__c);
            
            if(bidDetails.Contracted_Hours__c != 0 && bidDetails.Client_Task__r.Total_Contract_Units__c != 0){
                coLineItem.Current_Hours_Unit__c    = bidDetails.Contracted_Hours__c / bidDetails.Client_Task__r.Total_Contract_Units__c;
            } else {
                coLineItem.Current_Hours_Unit__c = 0;
            }
            
            if(bidDetails.Contracted_Value__c != 0 && bidDetails.Client_Task__r.Total_Contract_Units__c != 0) {
                coLineItem.Current_Unit_Cost__c = bidDetails.Contracted_Value__c / bidDetails.Client_Task__r.Total_Contract_Units__c;
            } else {
                coLineItem.Current_Unit_Cost__c = 0;
            }
            
            if(bidDetails.Contracted_Hours__c==0 && bidDetails.Contracted_Value__c!=0)
                    coLineItem.Is_Labor_Unit__c=false;
            system.debug('--------------- contract value -----------------' + bidDetails.Contracted_Value__c);
            system.debug('--------------- # of units -----------------' + bidDetails.Client_Task__r.Total_Contract_Units__c);
            system.debug('--------------- updated current unit cost -----------------' + coLineItem.Current_Unit_Cost__c);
            coLineItem.Current_Total_Cost__c = bidDetails.Contracted_Value__c;
            liUpsertedCOLineItems.add(coLineItem);
        }
        upsert liUpsertedCOLineItems;
        
        // update change order: 
        // refresh flag, refresh date (propsed date is not changed)
        Change_Order__c co = [select Id, Client_Project__c,Client_Project__r.Total_Contracted_Value__c,Total_Contracted_Value__c, Proposed_Estimated_End_Date__c, Status__c, Client_Project__r.Estimated_End_Date__c 
                                from Change_Order__c  where Id =:changeOrderId];
        if(co.Proposed_Estimated_End_Date__c == co.Client_Project__r.Estimated_End_Date__c) {
            co.Proposed_Estimated_End_Date__c = null;
        }
        co.Total_Contracted_Value__c = co.Client_Project__r.Total_Contracted_Value__c;
        co.Is_Project_Updated__c = false;
        co.Last_Refreshed__c   = Datetime.now();
        update co;
    }
}