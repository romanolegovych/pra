public with sharing class BDT_MainRightNavigationBar {
	
	public boolean show_MainRightStudyDetails{get;set;}
	public boolean show_AdminSearch{get;set;}
	public String currentPage {get;set;} 
	public List <String> allowShow_MainRightStudyDetails = new List <String>
		{'bdt_neweditstudy'
		,'bdt_singleproject'
		,'bdt_characteristics'
		,'bdt_neweditpopulation'
		,'bdt_neweditstudysite'
		,'bdt_neweditdesign'
		,'bdt_design'
		,'bdt_neweditpopulationassignment'
		,'bdt_neweditflowchart'
		,'bdt_projectservices'
		,'bdt_projectservicecopy'
		,'bdt_neweditflowchartcopy'
		,'bdt_neweditdesigncopy'
		,'bdt_neweditgrouparmassignment'
		,'bdt_projectpassthrough'
		,'BDT_FinancialDocOverview'
		,'BDT_FinancialDocStudySel'
		,'BDT_FinancialDocPayment'
		,'BDT_FinancialDocContent'
		,'BDT_FinancialDocApproval'
		,'BDT_FinancialDocHistory'
		,'BDT_FinancialDocOutput'
		,'BDT_FinancialDocument'
		,'BDT_FinancialServicePricing'
		,'BDT_NewEditLabAnalysisToStudy'
		,''
		,''
		,''
		,''
		};
	public List <String> allowShow_AdminSearch = new List <String>{'bdt_sites','bdt_neweditsite','bdt_neweditbusinessunit','bdt_neweditstudysite', 'bdt_sponsors','bdt_neweditsponsorentity','bdt_neweditsponsor','bdt_services','bdt_neweditservice','bdt_neweditservicecategory','bdt_ratecards','BDT_LabMethods','BDT_LaboratoryLists'};
	
	public BDT_MainRightNavigationBar(){
   	
    	//from here you can add your code which is already using something similar to this and enbedded on your js
    	currentPage = BDT_Utils.getThisPageName(ApexPages.currentPage().getURL());
     	
    	show_MainRightStudyDetails = getDisplayPermission(allowShow_MainRightStudyDetails);
    	show_AdminSearch = getDisplayPermission(allowShow_AdminSearch);
	}
		
	public boolean getDisplayPermission(List <String> pagesList){
		Boolean show = false;
		for(String s: pagesList){
			if (currentPage.equalsIgnoreCase(s)){
				show = true;
			}
		}
		
		return show;
	}

}