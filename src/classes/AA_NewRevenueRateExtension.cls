public class AA_NewRevenueRateExtension{
   
    // Instance fields
    public String searchTerm {get; set;}
    private final List<BUF_Code__c> bufcodes;
    private final List<Currency__c> currencies;
    public String selectedBufCode{get;set;}
    public String selectedCurrency{get;set;}
    public String yearValue{get;set;}
    public Decimal initialRateValue{get;set;}
    public Boolean isActive{get;set;}
    private Revenue_Allocation_Rate__c rate;
    private Boolean error = false;
     
    public AA_NewRevenueRateExtension(ApexPages.StandardController controller) {
        bufcodes = AA_CostRateService.getBufCodes();
        currencies = AA_CostRateService.getCurrencies();
    }

    // JS Remoting action called when searching for a BUF Code name
    @RemoteAction
    public static List<BUF_Code__c > searchCode(String searchTerm) {
        List<BUF_Code__c > codes = AA_CostRateService.getBufCodesByName(searchTerm);
        return codes;
    }
    
    
    /* Populate BUF Codes in Select List */
    public List<SelectOption> getbufselectoption() {
        List<SelectOption> bufselectoption= new List<SelectOption>();
        for(BUF_Code__c entry : bufcodes){
            bufselectoption.add(new SelectOption(entry.Id, entry.Name));
        }
        return bufselectoption;
    }
    
    /* Populate Currency in Select List     */
    public List<SelectOption> getcurrencyselectoption() {
        List<SelectOption> currencyselectoption= new List<SelectOption>();
        for(Currency__c entry : currencies){
            currencyselectoption.add(new SelectOption(entry.Id, entry.Name));
        }
        return currencyselectoption;
    }

    public PageReference Create() {
            /* Catch invalid Year */
            try{
            if(Integer.valueOf(yearValue) < 1999 || Integer.valueOf(yearValue) > Integer.valueOf(Date.Today().Year() + 20)) {
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Year : '+ yearValue + '. Must be between 1999 and ' + String.valueOf(Date.Today().Year() + 20));
                ApexPages.addMessage(errormsg);
                return null;
            }
            }catch(Exception e){
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Year : '+ yearValue);
                ApexPages.addMessage(errormsg);
                return null;
            }
            
            /* Catch invalid Rate*/
            try{
                Integer d = Integer.valueOf(initialRateValue);
            }catch(Exception e){
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Rate : '+ initialRateValue);
                ApexPages.addMessage(errormsg);
            }
            
            if(!error){
                List<Revenue_Allocation_Rate__c> check = AA_RevenueAllocationService.checkExisting(selectedBufCode,yearValue);   
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,selectedBufCode + ' | ' + yearValue));
                if(check.size()>0)
                {
                    BUF_Code__c tempCode = AA_CostRateService.getBufCode(selectedBufCode);
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Revenue Allocation Rates for ' + tempCode.Name + ' BUF Code for ' +  yearValue + ' exists in the system. Please choose a different BUF Code or a different year.'));
                
                }else {  
                     createNewRecords();
                     return null;
                 }
                 return null;
         }else
             return null;
    }

   public PageReference back() {
         PageReference pageRef = Page.AA_Revenue_Allocation_Rate;
         return pageRef;
    }
    public void createNewRecords(){
         rate =  new Revenue_Allocation_Rate__c ();
         rate.Function__c = selectedBufCode;
         rate.Rate__c = initialRateValue;
         rate.Year__c = yearValue;
         rate.Currency__c = selectedCurrency;
         rate.IsActive__c = isActive;
         insert rate;
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,' Revenue Allocation Rate Generated Succesfully'));
    }
    
}