public class LMS_ViewAdhocController{
        
    /************************************** 
     *  Code for : CourseToPRAController  *
     *  Project : LMS                     *    
     *  Author : Andrew Allen             * 
     *  Last Updated Date : 6/7/2012     * 
     *************************************/
     
    // Instance Variables
    public String roleName{get;set;}
    public String roleNameStyle{get;private set;}
    public String roleFilter{get;private set;}
    public String courseToRoleId{get;private set;}
    public String roleErrorText{get;private set;}
    public String webServiceError{get;private set;}
    public Boolean allChecked{get;set;}
    public Boolean renderBlock{get;set;}
    public Boolean renderError{get;set;}
    public Boolean disableRole{get;set;}
    public Boolean renderExport{get;set;}
    public Boolean bWebServiceError{get;private set;}
    public Boolean bSyncAction{get;private set;}
    
    // Collections
    public List<LMS_RoleWrapper> roleList{get;set;}
    private List<LMS_Role__c> roles{get;set;}
    private Map<String, LMS_Role__c> roleMap{get;set;}
    private Map<String, CourseDomainSettings__c> settings{get;set;}
    private Map<String, LMSConstantSettings__c> constants{get;set;}
    private Map<String, LMSCustomTabSettings__c> tabIds{get;set;}
    
    // Error Varaibles
    public List<String> sfdcErrors{get;private set;}
    public List<String> roleError{get;private set;}
    public List<String> calloutErrors{get;private set;}
    
    /**
     *  Constructor
     *  Initializes all data
     */
    public LMS_ViewAdhocController() { 
        initSettings();
        initVals();
    }
    
    /**
     *  Initialize course search data
     */
    private void initVals() {
        renderBlock = false;
        bWebServiceError = false;
        roleName = 'Enter Role Name';
        roleNameStyle = 'WaterMarkedTextBox';
        roleFilter = ' and Role_Type__r.Name = \'Additional Role\'';
        courseToRoleId = tabIds.get('CourseToRole').tabId__c;
    }
    
    /**
     *  Get constants from custom settings
     */
    private void initSettings() {
        settings = CourseDomainSettings__c.getAll();
        constants = LMSConstantSettings__c.getAll();
        tabIds = LMSCustomTabSettings__c.getAll();
    }
    
    /** 
     *  Search Implementation method :
     *  This method uses the UI role criteria to search for the role
     *  and the courses that have been assigned to that role 
     */
    public void search() {
        roleMap = new Map<String, LMS_Role__c>();
        
        String draft = constants.get('statusDraft').Value__c;
        String committed = constants.get('statusCommitted').Value__c;
        String draftDelete = constants.get('statusDraftDelete').Value__c;
        String removed = constants.get('statusRemoved').Value__c;
        
        //Join query to get course information on a certain role
        String courseQry = 'select Name from Rolecourses__r where Status__c != :removed';
        //Join query to get employee information for course
        String employeeQry = 'select Name from RoleEmployees__r where Status__c != :removed';
        //Main query to pull the role based on criteria in UI
        String roleQry = 'select CreatedBy.Name,CreatedDate, LastModifiedBy.Name, LastModifiedDate, Role_Name__c,Role_Type__r.Name,Employee_Count__c,SABA_Role_PK__c,Sync_Status__c,Status__c,(' + courseQry + '),(' + employeeQry + ') from LMS_Role__c ' + 
            'where Role_Type__r.Name = \'Additional Role\' and Role_Name__c LIKE \'' + String.escapeSingleQuotes(roleName) + '%\' order by Role_Name__c';
        allChecked = false;
        
        roles = Database.query(roleQry);
        roleList = new List<LMS_RoleWrapper>();
        ///If no role exists do not render table & display message
        for(LMS_Role__c r : roles) {
            roleList.add(new LMS_RoleWrapper(r));
            roleMap.put(r.Id, r);
        }
        if(roles.size() == 0) {
            renderError = true;
            renderBlock = false;
            roleErrorText = 'Role not found, please enter a different role name';
        } else {
            renderBlock = true;
            renderError = false;
        }
    }
    
    /**
     *  Method to reset role information
     */
    public void roleReset() {
                
        renderError = false;
        renderBlock = false;
        bWebServiceError = false;
        
        initVals();
    }
    
    public PageReference activateRoles() {
        System.debug('****1*****');
        LMS_ToolsService.activateRole(roleList, roleMap);
        search();
        return null;
    }
    
    public PageReference disableRoles() {
        LMS_ToolsService.inactivateRole(roleList, roleMap);
        search();
        return null;
    }
    
    public PageReference showErrors() {
        PageReference pr = new PageReference('/apex/LMS_ViewAdhocError');
        pr.setRedirect(false);
        return pr;
    }
    
    public PageReference invokeExportRoles(){
    System.debug('****exp1***'+roleList);
   // LMS_ToolsService.exportHelper(roleList);
    
    PageReference pr = new PageReference('/apex/LMS_AdditionalExportRoles');
    pr.setRedirect(false);
    return pr;
    }
    public List<LMS_RoleWrapper> getexportRole() {
    System.debug('****exp2***'+roleList);
        return roleList;
        
    }
}