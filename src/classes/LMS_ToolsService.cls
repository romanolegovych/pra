public class LMS_ToolsService {
	
	private static String rolePRA{get;set;}
	private static String roleActive{get;set;}
	private static String roleInactive{get;set;}
	private static String statusDraft{get;set;}
	private static String statusCommitted{get;set;}
	private static String statusDraftDelete{get;set;}
	private static String statusPendingAdd{get;set;}
	private static String statusPendingDelete{get;set;}
	private static String statusTransitAdd{get;set;}
	private static String statusTransitDelete{get;set;}
	private static String statusRemoved{get;set;}
	private static String inSync{get;set;}
	private static String outSync{get;set;}
	private static String actionAdd{get;set;}
	private static String actionRemove{get;set;}
	private static List<String> sfdcErrors{get;set;}
	private static List<String> errorList{get;set;}
	private static Map<String, List<String>> errorMap{get;set;}
	
	/**
	 * Initialize instance variables
	 */
	static {
		Map<String, LMSConstantSettings__c> constants = LMSConstantSettings__c.getAll();
		inSync = constants.get('inSync').Value__c;
		outSync = constants.get('outSync').Value__c;
		rolePRA = constants.get('typePRA').Value__c;
		roleActive = constants.get('roleActive').Value__c;
		roleInactive = constants.get('roleInactive').Value__c;
		actionAdd = constants.get('addMappings').Value__c;
		actionRemove = constants.get('removeMappings').Value__c;
		statusDraft = constants.get('statusDraft').Value__c;
		statusCommitted = constants.get('statusCommitted').Value__c;
		statusDraftDelete = constants.get('statusDraftDelete').Value__c;
		statusPendingAdd = constants.get('statusPendingAdd').Value__c;
		statusPendingDelete = constants.get('statusPendingDelete').Value__c;
		statusTransitAdd = constants.get('statusTransitAdd').Value__c;
		statusTransitDelete = constants.get('statusTransitDelete').Value__c;
		statusRemoved = constants.get('statusRemoved').Value__c;
	}
	
	public static Map<String, List<String>> getErrorMap() {
		return errorMap;
	}
	
	public static Map<String, List<String>> activateRole(List<LMS_RoleWrapper> roles, Map<String, LMS_Role__c> roleMap) {
		Boolean doCreate = false;
		errorMap = new Map<String, List<String>>();
		errorList = new List<String>();
		sfdcErrors = new List<String>();
		List<LMS_Role__c> roleList = new List<LMS_Role__c>();
		List<LMS_Role__c> toUpdate = new List<LMS_Role__c>();
		for(LMS_RoleWrapper r : roles) {
			if(r.checked && r.status == roleInactive) {
				LMS_Role__c role = roleMap.get(r.roleId);
				role.Status__c = roleActive;
				System.debug('-----------------------role--------------------------' + role);
				roleList.add(role);	
				if(r.roleType == rolePRA) {
					doCreate = true;
				}
			}
		}
		if(null != roleList && roleList.size() > 0) {
			toUpdate = sendRolesData(roleList, roleMap);
			if(toUpdate != null && toUpdate.size() > 0) {
				if(doCreate) {
					LMS_ToolsModifyRoles.createRoleEmployeeAssociation(toUpdate);
				}
			}
		}
		update toUpdate;
		return null;
	}
	
	public static Map<String, List<String>> activateRoleViaJob(List<LMS_Role__c> roles, Map<String, LMS_Role__c> roleMap) {
		Boolean doCreate = false;
		errorMap = new Map<String, List<String>>();
		errorList = new List<String>();
		sfdcErrors = new List<String>();
		List<LMS_Role__c> roleList = new List<LMS_Role__c>();
		List<LMS_Role__c> toUpdate = new List<LMS_Role__c>();
		for(LMS_Role__c r : roles) {
			if(r.Status__c == roleInactive) {
				r.Status__c = roleActive;
				System.debug('-----------------------role--------------------------' + r);
				roleList.add(r);	
				if(r.Role_Type__r.Name == rolePRA) {
					doCreate = true;
				}
			}
		}
		if(null != roleList && roleList.size() > 0) {
			toUpdate = sendRolesData(roleList, roleMap);
			if(toUpdate != null && toUpdate.size() > 0) {
				if(doCreate) {
					LMS_ToolsModifyRoles.createRoleEmployeeAssociation(toUpdate);
				}
			}
		}
		update toUpdate;
		return null;
	}
	
	public static Map<String, List<String>> inactivateRole(List<LMS_RoleWrapper> roles, Map<String, LMS_Role__c> roleMap) {
		errorMap = new Map<String, List<String>>();
		errorList = new List<String>();
		sfdcErrors = new List<String>();
		List<LMS_Role__c> roleList = new List<LMS_Role__c>();
		List<LMS_Role__c> toUpdate = new List<LMS_Role__c>();
		for(LMS_RoleWrapper r : roles) {
			if(r.checked && r.status == roleActive) {
				LMS_Role__c role = roleMap.get(r.roleId);
				role.Status__c = roleInactive;
				System.debug('-----------------------role--------------------------' + role);
				roleList.add(role);
			}
		}	
		if(null != roleList && roleList.size() > 0) {
			toUpdate = sendRolesData(roleList, roleMap);
			if(toUpdate != null && toUpdate.size() > 0) {
				LMS_ToolsModifyRoles.breakRoleEmployeeAssociation(roleList);
			}
		}
		update toUpdate;
		return null;
	}
	
	public static Map<String, List<String>> inactivateRoleViaJob(List<LMS_Role__c> roles, Map<String, LMS_Role__c> roleMap) {
		errorMap = new Map<String, List<String>>();
		errorList = new List<String>();
		sfdcErrors = new List<String>();
		List<LMS_Role__c> toUpdate = new List<LMS_Role__c>();
		
		for(LMS_Role__c r : roles) {
			System.debug('-----------------------role--------------------------' + r);
			r.Status__c = roleInactive;
		}	
		toUpdate = sendRolesData(roles, roleMap);
		if(toUpdate != null && toUpdate.size() > 0) {
			LMS_ToolsModifyRoles.breakRoleEmployeeAssociation(roles);
		}
		
		update toUpdate;
		return null;
	}
	
	/**
	 * Method used to invoke Role WebService methods and send/receive new data
	 */
	public static Map<String, List<String>> sendRoleData(LMS_Role__c role) {
		errorMap = new Map<String, List<String>>();
		errorList = new List<String>();
		sfdcErrors = new List<String>();
		
		LMS_RoleAdminService.roleResponse response = LMS_RoleAdminServiceWrapper.processRole(role);
		errorMap.put('CALLOUT',LMS_RoleAdminServiceWrapper.getErrors());
		
		if(null != response) {
			if(null == role.SABA_Role_PK__c || 0 == role.SABA_Role_PK__c.length()) {
				if(response.success) {
					role.SABA_Role_PK__c = response.details;
					role.Sync_Status__c = inSync;
					System.debug('Role was upserted in SABA & SFDC successfully');
				} else {
					System.debug('Error upserting role to SABA');
				}
				if(null != response.errors) {
					for(LMS_RoleAdminService.error err : response.errors) {
						errorList.add(err.message);
						errorMap.put('ROLE',errorList);
					}
				}
			}
		}
		
		try {
			upsert role;
		} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
		}
		
		return errorMap;
	}
	
	public static List<LMS_Role__c> sendRolesData(List<LMS_Role__c> roles, Map<String, LMS_Role__c> roleMap) {
		List<LMS_Role__c> toUpdate = new List<LMS_Role__c>();
		LMS_RoleAdminService.roleResponse responses = LMS_RoleAdminServiceWrapper.processRoles(roles);
		
		if(responses != null) {
			if(responses.errors != null) {
				for(LMS_RoleAdminService.error err : responses.errors) {
					errorList.add(err.message);
					errorMap.put('CALLOUT', errorList);
				}
			}
			if(null != responses.responseItems) {
				for(LMS_RoleAdminService.roleResponseItem role : responses.responseItems) {
					LMS_Role__c r = roleMap.get(role.roleId);
					if(role.success == true && roleMap.containsKey(role.roleId)) {
    					System.debug('-------------Role Sent to Saba Successfull--------------');
    					toUpdate.add(r);
					} else if(role.success == false && roleMap.containsKey(role.roleId)) {
						System.debug('-------------Could not update role with Id:' + r.SABA_Role_PK__c + ' to SABA');
						for(LMS_RoleAdminService.error err : role.errors) {
							errorList.add(err.message);
							errorMap.put('ROLE', errorList);
						}
					}
				}
			}
		}
		
		return toUpdate;
	}
	
	/**
	 * Send employee<->role associations after
	 * PRA role is created
	 */
	@future(callout=true)
	public static void sendInitialRoleEmployeeMappings(String roleId) {
		List<LMS_Role_Employee__c> toSend = new List<LMS_Role_Employee__c>();
        List<LMS_Role_Employee__c> batchList = new List<LMS_Role_Employee__c>();
        Map<String, List<LMS_Role_Employee__c>> sendAddMap = new Map<String, List<LMS_Role_Employee__c>>();
        Map<String, LMS_Role_Employee__c> objectsAdd = new Map<String, LMS_Role_Employee__c>();
		
		List<LMS_Role_Employee__c> newEmpMappings = 
		[SELECT Status__c, Employee_Id__r.Name, Role_Id__r.SABA_Role_PK__c FROM LMS_Role_Employee__c 
		 WHERE Role_Id__c = :roleId];
        
        for(LMS_Role_Employee__c re : newEmpMappings) { 
    		objectsAdd.put(re.Employee_Id__r.Name, re);
            batchList.add(re);
        }
        
    	sendAddMap.put(actionAdd, batchList);
        sendEmployeesToRoleMappings(sendAddMap, objectsAdd);
	}
	
	/**
	 * Method to delete segments of course<->role associations
	 */
	public static List<LMS_Role_Course__c> courseRoleBatchDelete(List<LMS_Role_Course__c> rc) {
		System.debug('------------------------------------'+rc.size());
		List<LMS_Role_Course__c> batchList = null;
        Map<String, LMS_Role_Course__c> objectsDelete = null;
        Map<String, List<LMS_Role_Course__c>> sendDeleteMap = null;
		List<LMS_Role_Course__c> deleteRoleCourse = new List<LMS_Role_Course__c>();
		// Delete segments of role<->course associations
        Integer start = 0;
        Integer batch = 15;
        Integer stop = batch;
        Integer iterations = 5;
        Integer remainder = 0;
        
        if(batch >= rc.size()) {
        	batch = rc.size();
        	stop = batch;
        	iterations = 1;
        } else if((batch * iterations) > rc.size()){ 
        	remainder = Math.mod(rc.size(), batch);
        	if(Math.mod(rc.size(), batch) == 0) { 
        		iterations = (rc.size() / batch);
        	} else {
        		iterations = (rc.size() / batch) + 1;
        	}
        }
        
        for(Integer j = 0; j < iterations; j++) {
        	batchList = new List<LMS_Role_Course__c>();
        	objectsDelete = new Map<String, LMS_Role_Course__c>();
        	sendDeleteMap = new Map<String, List<LMS_Role_Course__c>>();
        	if((j == (iterations - 1)) && (remainder > 0)) {
        		stop = start + remainder;
        	}
	        for(Integer i = start; i < stop; i++) {
	        	objectsDelete.put(rc[i].Course_Id__r.SABA_ID_PK__c, rc[i]);
	        	batchList.add(rc[i]);
	       		if(i == (stop - 1)) {
	        		sendDeleteMap.put(actionRemove, batchList);
	        		deleteRoleCourse.addAll(sendCoursesToRoleMappings(sendDeleteMap, objectsDelete));
	       		}
	        }
	        start = stop;
	        stop += batch;
        }
        
        return deleteRoleCourse;
	}
	
	/**
	 * Method to delete segments of role<->course associations
	 */
	public static List<LMS_Role_Course__c> roleCourseBatchDelete(List<LMS_Role_Course__c> rc) {
		System.debug('------------------------------------'+rc.size());
		List<LMS_Role_Course__c> batchList = null;
        Map<String, LMS_Role_Course__c> objectsDelete = null;
        Map<String, List<LMS_Role_Course__c>> sendDeleteMap = null;
		List<LMS_Role_Course__c> deleteRoleCourse = new List<LMS_Role_Course__c>();
		// Delete segments of role<->course associations
        Integer start = 0;
        Integer batch = 15;
        Integer stop = batch;
        Integer iterations = 5;
        Integer remainder = 0;
        
        if(batch >= rc.size()) {
        	batch = rc.size();
        	stop = batch;
        	iterations = 1;
        } else if((batch * iterations) > rc.size()){ 
        	remainder = Math.mod(rc.size(), batch);
        	if(Math.mod(rc.size(), batch) == 0) { 
        		iterations = (rc.size() / batch);
        	} else {
        		iterations = (rc.size() / batch) + 1;
        	}
        }
        
        for(Integer j = 0; j < iterations; j++) {
        	batchList = new List<LMS_Role_Course__c>();
        	objectsDelete = new Map<String, LMS_Role_Course__c>();
        	sendDeleteMap = new Map<String, List<LMS_Role_Course__c>>();
        	if((j == (iterations - 1)) && (remainder > 0)) {
        		stop = start + remainder;
        	}
	        for(Integer i = start; i < stop; i++) {
	        	objectsDelete.put(rc[i].Role_Id__r.SABA_Role_PK__c, rc[i]);
	        	batchList.add(rc[i]);
	       		if(i == (stop - 1)) {
	        		sendDeleteMap.put(actionRemove, batchList);
	        		deleteRoleCourse.addAll(sendRolesToCourseMappings(sendDeleteMap, objectsDelete));
	       		}
	        }
	        start = stop;
	        stop += batch;
        }
        
        return deleteRoleCourse;
	}
	
	/**
	 * Method to delete segments of role<->course associations
	 * Many to Many functionality
	 */
	public static List<LMS_Role_Course__c> coursesRolesBatchDeleteMany(List<LMS_Role_Course__c> rc) {
		errorList = new List<String>();
		sfdcErrors = new List<String>();
		errorMap = new Map<String, List<String>>();
		List<LMS_Role_Course__c> batchList = null;
        Map<String, Map<String, LMS_Role_Course__c>> objectsDelete = null;
        Map<String, LMS_Role_Course__c> roleCourseMatch = null;
        Map<String, List<LMS_Role_Course__c>> sendDeleteMap = null;
		List<LMS_Role_Course__c> deleteRoleCourse = new List<LMS_Role_Course__c>();
		
		// Delete segments of role<->course associations
        Integer start = 0;
        Integer batch = 15;
        Integer stop = batch;
        Integer iterations = 5;
        Integer remainder = 0;
        
        if(batch >= rc.size()) {
        	batch = rc.size();
        	stop = batch;
        	iterations = 1;
        } else if((batch * iterations) > rc.size()){ 
        	remainder = Math.mod(rc.size(), batch);
        	if(Math.mod(rc.size(), batch) == 0) { 
        		iterations = (rc.size() / batch);
        	} else {
        		iterations = (rc.size() / batch) + 1;
        	}
        }
        
        for(Integer j = 0; j < iterations; j++) {
	        String roleId;
        	batchList = new List<LMS_Role_Course__c>();
        	objectsDelete = new Map<String, Map<String, LMS_Role_Course__c>>();
	        roleCourseMatch = new Map<String, LMS_Role_Course__c>();
        	sendDeleteMap = new Map<String, List<LMS_Role_Course__c>>();
        	if((j == (iterations - 1)) && (remainder > 0)) {
        		stop = start + remainder;
        	}
	        for(Integer i = start; i < stop; i++) {
	        	if(roleId != rc[i].Role_Id__r.SABA_Role_PK__c) {
	        		roleCourseMatch = new Map<String, LMS_Role_Course__c>();
	        	}
	        	roleCourseMatch.put(rc[i].Course_Id__r.SABA_ID_PK__c, rc[i]);
	        	objectsDelete.put(rc[i].Role_Id__r.SABA_Role_PK__c, roleCourseMatch);
	        	batchList.add(rc[i]);
	        	roleId = rc[i].Role_Id__r.SABA_Role_PK__c;
	       		if(i == (stop - 1)) {
	        		sendDeleteMap.put(actionRemove, batchList);
	        		deleteRoleCourse.addAll(sendCoursesToRolesMappings(sendDeleteMap, objectsDelete));
	       		}
	        }
	        start = stop;
	        stop += batch;
        }
        
        return deleteRoleCourse;
	}
	
	/**
	 * Method to delete segments of roles<->course associations
	 * Many to Many functionality
	 */
	public static List<LMS_Role_Course__c> rolesCoursesBatchDeleteMany(List<LMS_Role_Course__c> rc) {
		errorList = new List<String>();
		sfdcErrors = new List<String>();
		errorMap = new Map<String, List<String>>();
		List<LMS_Role_Course__c> batchList = null;
        Map<String, Map<String, LMS_Role_Course__c>> objectsDelete = null;
        Map<String, LMS_Role_Course__c> roleCourseMatch = null;
        Map<String, List<LMS_Role_Course__c>> sendDeleteMap = null;
		List<LMS_Role_Course__c> deleteRoleCourse = new List<LMS_Role_Course__c>();
		
		// Delete segments of role<->course associations
        Integer start = 0;
        Integer batch = 15;
        Integer stop = batch;
        Integer iterations = 5;
        Integer remainder = 0;
        
        if(batch >= rc.size()) {
        	batch = rc.size();
        	stop = batch;
        	iterations = 1;
        } else if((batch * iterations) > rc.size()){ 
        	remainder = Math.mod(rc.size(), batch);
        	if(Math.mod(rc.size(), batch) == 0) { 
        		iterations = (rc.size() / batch);
        	} else {
        		iterations = (rc.size() / batch) + 1;
        	}
        }
        
        System.debug('---------------start---------------' + start);
        System.debug('---------------stop---------------' + stop);
        System.debug('---------------batch---------------' + batch);
        System.debug('---------------iterations---------------' + iterations);
        
        for(Integer j = 0; j < iterations; j++) {
	        String courseId;
        	batchList = new List<LMS_Role_Course__c>();
        	objectsDelete = new Map<String, Map<String, LMS_Role_Course__c>>();
	        roleCourseMatch = new Map<String, LMS_Role_Course__c>();
        	sendDeleteMap = new Map<String, List<LMS_Role_Course__c>>();
        	if((j == (iterations - 1)) && (remainder > 0)) {
        		stop = start + remainder;
        	}
	        for(Integer i = start; i < stop; i++) {
	        	if(courseId != rc[i].Course_Id__r.SABA_ID_PK__c) {
	        		roleCourseMatch = new Map<String, LMS_Role_Course__c>();
	        	}
	        	roleCourseMatch.put(rc[i].Role_Id__r.SABA_Role_PK__c, rc[i]);
	        	objectsDelete.put(rc[i].Course_Id__r.SABA_ID_PK__c, roleCourseMatch);
	        	batchList.add(rc[i]);
	        	courseId = rc[i].Course_Id__r.SABA_ID_PK__c;
	       		if(i == (stop - 1)) {
	        		sendDeleteMap.put(actionRemove, batchList);
	        		deleteRoleCourse.addAll(sendRolesToCoursesMappings(sendDeleteMap, objectsDelete));
	        		System.debug('-----------------deleteing batch ' + (j + 1) + '-------------------');
	       		}
	        }
	        start = stop;
	        stop += batch;
        }
        
        return deleteRoleCourse;
	}
	
	/**
	 * Method to access course<->role webservice method
	 */
	public static List<LMS_Role_Course__c> executeCoursesToRoleWS(Map<String, List<LMS_Role_Course__c>> mappings, Map<String, LMS_Role_Course__c> objects) {
		List<LMS_Role_Course__c> rc = new List<LMS_Role_Course__c>();
		rc = sendCoursesToRoleMappings(mappings, objects);
		
		return rc;
	}
	
	/**
	 * Method to access courses<->roles webservice call
	 * Many to many operation
	 */
	public static List<LMS_Role_Course__c> executeCoursesToRolesWS(Map<String, List<LMS_Role_Course__c>> mappings, Map<String, Map<String, LMS_Role_Course__c>> objects) {
		List<LMS_Role_Course__c> rc = new List<LMS_Role_Course__c>();
		rc = sendCoursesToRolesMappings(mappings, objects);
		
		return rc;
	}
	
	/**
	 * Method to access roles<->course webservice call
	 * Many(roles) to one(course) operation
	 */
	public static List<LMS_Role_Course__c> executeRolesToCourseWS(Map<String, List<LMS_Role_Course__c>> mappings, Map<String, LMS_Role_Course__c> objects) {
		List<LMS_Role_Course__c> rc = new List<LMS_Role_Course__c>();
		rc = sendRolesToCourseMappings(mappings, objects);
		
		return rc;
	}
	
	/**
	 * Method to access Roles<->Courses webservice call
	 * Many to many operation
	 */
	public static List<LMS_Role_Course__c> executeRolesToCoursesWS(Map<String, List<LMS_Role_Course__c>> mappings, Map<String, Map<String, LMS_Role_Course__c>> objects) {
		List<LMS_Role_Course__c> rc = new List<LMS_Role_Course__c>();
		rc = sendRolesToCoursesMappings(mappings, objects);
		
		return rc;
	}
	
	/**
	 * Method to execute employees<->role webservice call
	 */
	public static List<LMS_Role_Employee__c> executeEmployeesToRoleWS(Map<String, List<LMS_Role_Employee__c>> mappings, Map<String, LMS_Role_Employee__c> objects) {
		List<LMS_Role_Employee__c> maps = new List<LMS_Role_Employee__c>();
		maps = sendEmployeesToRoleMappings(mappings, objects);
		
		return maps;
	}
	
	/**
	 * Method to execute employees<->role webservice call
	 * Many to Many
	 */
	public static List<LMS_Role_Employee__c> executeEmployeesToRolesWS(Map<String, List<LMS_Role_Employee__c>> mappings, Map<String, Map<String, LMS_Role_Employee__c>> objects) {
		List<LMS_Role_Employee__c> maps = new List<LMS_Role_Employee__c>();
		maps = sendEmployeesToRolesMappings(mappings, objects);
		
		return maps;
	}
	
	/**
	 * Method to exectue roles<->employees webservice call
	 */
	public static List<LMS_Role_Employee__c> executeRolesToEmployeesWS(Map<String, List<LMS_Role_Employee__c>> mappings, Map<String, Map<String, LMS_Role_Employee__c>> objects) {
		List<LMS_Role_Employee__c> re = new List<LMS_Role_Employee__c>();
		re = sendRolesToEmployeeMappings(mappings, objects);
		
		return re;
	}
	
	/**
	 * Method to send course<->role associations
	 */
	private static List<LMS_Role_Course__c> sendCoursesToRoleMappings(Map<String, List<LMS_Role_Course__c>> mappings, Map<String, LMS_Role_Course__c> objects) {
		errorList = new List<String>();
		errorMap = new Map<String, List<String>>();
		List<LMS_Role_Course__c> maps = new List<LMS_Role_Course__c>();
		
		LMS_RoleAdminService.courseRoleResponse responses = LMS_RoleAdminServiceWrapper.processCoursesToRoleAssociation(mappings);
		errorMap.put('CALLOUT',LMS_RoleAdminServiceWrapper.getErrors());
		
		if(null != responses) {
			if(responses.errors != null) {
				for(LMS_RoleAdminService.error err : responses.errors) {
					errorList.add(err.message);
					errorMap.put('CALLOUT', errorList);
				}
			}
			if(null != responses.coursesRoleResponseItems) {
				for(LMS_RoleAdminService.coursesRoleResponseItem response : responses.coursesRoleResponseItems) {
					for(LMS_RoleAdminService.courseMapResponse role : response.coursesMapResponses) {
						LMS_Role_Course__c roleCourse = objects.get(role.courseId);
						if(role.success == true && objects.containsKey(role.courseId) && mappings.containsKey(actionAdd)) {
							roleCourse.Sync_Status__c = inSync;
							roleCourse.SABA_Role_Course_PK__c = role.details;
							roleCourse.Status__c = statusCommitted;
							roleCourse.Assigned_By__c = UserInfo.getName();
							maps.add(roleCourse);
	    					System.debug('-------------Course Sent to Saba Successfull--------------');
						} else if(role.success == false && objects.containsKey(role.courseId) && mappings.containsKey(actionAdd)) {
							System.debug('-------------Could not add courseId:' + roleCourse.Course_Id__r.SABA_ID_PK__c + ' to SABA');
							for(LMS_RoleAdminService.error err : role.errors) {
								errorList.add(err.message);
								errorMap.put('A', errorList);
							}
						}
						if (role.success == true && objects.containsKey(role.courseId) && mappings.containsKey(actionRemove)) {
							maps.add(roleCourse);
	    					System.debug('-------------Course Removed from Saba Successfull--------------');
						} else if(role.success == false && objects.containsKey(role.courseId) && mappings.containsKey(actionRemove)) {
							System.debug('-------------Could not remove courseId:' + roleCourse.Course_Id__r.SABA_ID_PK__c + ' from SABA');
							for(LMS_RoleAdminService.error err : role.errors) {
								errorList.add(err.message);
								errorMap.put('D', errorList);
							}
						}
					}
				}
			}
		} else {
			System.debug('----------------Role Sent to Saba Unscuccessfull-----------------------');
		}
		
		return maps;
	}
	
	/**
	 * Method to send Many to Many courses<->roles associations
	 */
	private static List<LMS_Role_Course__c> sendCoursesToRolesMappings(Map<String, List<LMS_Role_Course__c>> mappings, Map<String, Map<String, LMS_Role_Course__c>> objects) {
		errorList = new List<String>();
		errorMap = new Map<String, List<String>>();
		List<LMS_Role_Course__c> maps = new List<LMS_Role_Course__c>();
		
		LMS_RoleAdminService.courseRoleResponse responses = LMS_RoleAdminServiceWrapper.processCoursesToRoleAssociation(mappings);
		errorMap.put('CALLOUT',LMS_RoleAdminServiceWrapper.getErrors());
		
		if(null != responses) {
			if(responses.errors != null) {
				for(LMS_RoleAdminService.error err : responses.errors) {
					errorList.add(err.message);
					errorMap.put('CALLOUT', errorList);
				}
			}
			if(null != responses.coursesRoleResponseItems) {
				for(LMS_RoleAdminService.coursesRoleResponseItem response : responses.coursesRoleResponseItems) {
					for(LMS_RoleAdminService.courseMapResponse role : response.coursesMapResponses) {
						LMS_Role_Course__c roleCourse = objects.get(response.roleId).get(role.courseId);
						if(role.success == true && objects.get(response.roleId).containsKey(role.courseId) && mappings.containsKey(actionAdd)) {
							roleCourse.Sync_Status__c = inSync;
							roleCourse.SABA_Role_Course_PK__c = role.details;
							roleCourse.Status__c = statusCommitted;
							roleCourse.Assigned_By__c = UserInfo.getName();
							maps.add(roleCourse);
	    					System.debug('-------------Course Sent to Saba Successfull--------------');
						} else if(role.success == false && objects.get(response.roleId).containsKey(role.courseId) && mappings.containsKey(actionAdd)) {
							System.debug('-------------Could not add courseId:' + roleCourse.Course_Id__r.SABA_ID_PK__c + ' to SABA');
							for(LMS_RoleAdminService.error err : role.errors) {
								errorList.add(err.message);
								errorMap.put('A', errorList);
							}
						}
						if (role.success == true && objects.get(response.roleId).containsKey(role.courseId) && mappings.containsKey(actionRemove)) {
							maps.add(roleCourse);
	    					System.debug('-------------Course Removed from Saba Successfull--------------');
						} else if(role.success == false && objects.get(response.roleId).containsKey(role.courseId) && mappings.containsKey(actionRemove)) {
							System.debug('-------------Could not remove courseId:' + roleCourse.Course_Id__r.SABA_ID_PK__c + ' from SABA');
							for(LMS_RoleAdminService.error err : role.errors) {
								errorList.add(err.message);
								errorMap.put('D', errorList);
							}
						}
					}
				}
			}
		} else {
			System.debug('----------------Role Sent to Saba Unscuccessfull-----------------------');
		}
		
		return maps;
	}
	
	/**
	 * Sends roles<->course associations to SABA
	 */
	private static List<LMS_Role_Course__c> sendRolesToCourseMappings(Map<String, List<LMS_Role_Course__c>> mappings, Map<String, LMS_Role_Course__c> objects) {
		errorList = new List<String>();
		errorMap = new Map<String, List<String>>();
		List<LMS_Role_Course__c> maps = new List<LMS_Role_Course__c>();
		
		LMS_RoleAdminService.courseRoleResponse responses = LMS_RoleAdminServiceWrapper.processRolesToCourseAssociation(mappings);
		errorMap.put('CALLOUT',LMS_RoleAdminServiceWrapper.getErrors());
		
    	if(null != responses) {
			if(responses.errors != null) {
				for(LMS_RoleAdminService.error err : responses.errors) {
					errorList.add(err.message);
					errorMap.put('CALLOUT', errorList);
				}
			}
			if(responses.rolesCourseResponseItems != null) {
	        	for(LMS_RoleAdminService.rolesCourseResponseItem response : responses.rolesCourseResponseItems) {
	    			for(LMS_RoleAdminService.roleMapResponse course : response.roleMapResponses) {
	    				LMS_Role_Course__c roleCourse = objects.get(course.roleId);
	    				if(course.success == true && objects.containsKey(course.roleId) && mappings.containsKey(actionAdd)) {
	    					roleCourse.Sync_Status__c = inSync;
	    					roleCourse.SABA_Role_Course_PK__c = course.details;
	    					roleCourse.Status__c = statusCommitted;
			                roleCourse.Assigned_By__c = UserInfo.getName();
	    					maps.add(roleCourse);
	    					System.debug('-------------Role Sent to Saba Successfull--------------');
	    				} else if(course.success == false && objects.containsKey(course.roleId) && mappings.containsKey(actionAdd)) {
	    					System.debug('-------------Could not add roleId:' + roleCourse.Role_Id__r.SABA_ROLE_PK__c + ' to SABA');
							for(LMS_RoleAdminService.error err : course.errors) {
								errorList.add(err.message);
								errorMap.put('A', errorList);
								System.debug('-------------error list-------------' + errorList);
							}
	    				}
	    				if(course.success == true && objects.containsKey(course.roleId) && mappings.containsKey(actionRemove)) {
							maps.add(roleCourse);
	    					System.debug('-------------Role Removed from Saba Successfull--------------');
	    				} else if(course.success == false && objects.containsKey(course.roleId) && mappings.containsKey(actionRemove)) {
	    					System.debug('-------------Could not remove roleId:' + roleCourse.Role_Id__r.SABA_ROLE_PK__c + ' from SABA');
							for(LMS_RoleAdminService.error err : course.errors) {
								errorList.add(err.message);
								errorMap.put('D', errorList);
								System.debug('-------------error list-------------' + errorList);
							}
	    				}
	        		}
        		}
			}
        } else {
        	System.debug('-------------Role Sent to Saba Unsuccessfull--------------');
        }

        return maps;
	}
	
	/**
	 * Sends roles<->courses associations to SABA
	 */
	private static List<LMS_Role_Course__c> sendRolesToCoursesMappings(Map<String, List<LMS_Role_Course__c>> mappings, Map<String, Map<String, LMS_Role_Course__c>> objects) {
		errorList = new List<String>();
		errorMap = new Map<String, List<String>>();
		List<LMS_Role_Course__c> maps = new List<LMS_Role_Course__c>();
		
		LMS_RoleAdminService.courseRoleResponse responses = LMS_RoleAdminServiceWrapper.processRolesToCourseAssociation(mappings);
		errorMap.put('CALLOUT',LMS_RoleAdminServiceWrapper.getErrors());
		
    	if(null != responses) {
			if(responses.errors != null) {
				for(LMS_RoleAdminService.error err : responses.errors) {
					errorList.add(err.message);
					errorMap.put('CALLOUT', errorList);
				}
			}
			if(responses.rolesCourseResponseItems != null) {
	        	for(LMS_RoleAdminService.rolesCourseResponseItem response : responses.rolesCourseResponseItems) {
	    			for(LMS_RoleAdminService.roleMapResponse course : response.roleMapResponses) {
	    				LMS_Role_Course__c roleCourse = objects.get(response.courseId).get(course.roleId);
	    				if(course.success == true && objects.get(response.courseId).containsKey(course.roleId) && mappings.containsKey(actionAdd)) {
	    					roleCourse.Sync_Status__c = inSync;
	    					roleCourse.SABA_Role_Course_PK__c = course.details;
	    					roleCourse.Status__c = statusCommitted;
			                roleCourse.Assigned_By__c = UserInfo.getName();
	    					maps.add(roleCourse);
	    					System.debug('-------------Role Sent to Saba Successfull--------------');
	    				} else if(course.success == false && objects.get(response.courseId).containsKey(course.roleId) && mappings.containsKey(actionAdd)) {
	    					System.debug('-------------Could not add roleId:' + roleCourse.Role_Id__r.SABA_ROLE_PK__c + ' to SABA');
							for(LMS_RoleAdminService.error err : course.errors) {
								errorList.add(err.message);
								errorMap.put('A', errorList);
								System.debug('-------------error list-------------' + errorList);
							}
	    				}
	    				if(course.success == true && objects.get(response.courseId).containsKey(course.roleId) && mappings.containsKey(actionRemove)) {
							maps.add(roleCourse);
	    					System.debug('-------------Role Removed from Saba Successfull--------------');
	    				} else if(course.success == false && objects.get(response.courseId).containsKey(course.roleId) && mappings.containsKey(actionRemove)) {
	    					System.debug('-------------Could not remove roleId:' + roleCourse.Role_Id__r.SABA_ROLE_PK__c + ' from SABA');
							for(LMS_RoleAdminService.error err : course.errors) {
								errorList.add(err.message);
								errorMap.put('D', errorList);
								System.debug('-------------error list-------------' + errorList);
							}
	    				}
	        		}
        		}
			}
        } else {
        	System.debug('-------------Role Sent to Saba Unsuccessfull--------------');
        }

        return maps;
	}
	
	/**
	 * Method to send employees<->role associations
	 * Async call
	 */
	private static List<LMS_Role_Employee__c> sendEmployeesToRoleMappings(Map<String, List<LMS_Role_Employee__c>> mappings, Map<String, LMS_Role_Employee__c> objects) {
		errorMap = new Map<String, List<String>>();
		LMS_RoleAdminService.roleEmployeeMessageHolder response = null;
		List<LMS_Role_Employee__c> maps = new List<LMS_Role_Employee__c>();
		response = LMS_RoleAdminServiceWrapper.processEmployeesToRoleAsync(mappings);
		errorMap.put('CALLOUT',LMS_RoleAdminServiceWrapper.getErrors());
		
		if(null != response && null != response.personsRoleMapList) {
			for(LMS_RoleAdminService.personsRoleMap message : response.personsRoleMapList) { 
				if(null != message) {
					for(String emp : message.personNos) {
						LMS_Role_Employee__c re = objects.get(emp);
						System.debug('--------------Accessing Role_Employee Object from Response data---------------------'+re);
						if(message.action == actionAdd && objects.containsKey(emp)) {
							re.Status__c = statusTransitAdd;
							maps.add(re);
						} else if(message.action == actionRemove && objects.containsKey(emp)) {
							re.Status__c = statusTransitDelete;
							maps.add(re);
						}
					}
				}
			}
		}
		
		return maps;
	}
	
	/**
	 * Method to send employees<->role associations
	 * Async call - Many to Many
	 */
	private static List<LMS_Role_Employee__c> sendEmployeesToRolesMappings(Map<String, List<LMS_Role_Employee__c>> mappings, Map<String, Map<String, LMS_Role_Employee__c>> objects) {
		errorMap = new Map<String, List<String>>();
		LMS_RoleAdminService.roleEmployeeMessageHolder response = null;
		List<LMS_Role_Employee__c> maps = new List<LMS_Role_Employee__c>();
		response = LMS_RoleAdminServiceWrapper.processEmployeesToRoleAsync(mappings);
		errorMap.put('CALLOUT',LMS_RoleAdminServiceWrapper.getErrors());
		
		if(null != response && null != response.personsRoleMapList) {
			for(LMS_RoleAdminService.personsRoleMap message : response.personsRoleMapList) { 
				if(null != message) {
					for(String emp : message.personNos) {
						LMS_Role_Employee__c re = objects.get(message.roleId).get(emp);
						System.debug('--------------Accessing Role_Employee Object from Response data---------------------'+re);
						if(message.action == actionAdd && objects.containsKey(message.roleId)) {
							re.Status__c = statusTransitAdd;
							maps.add(re);
						} else if(message.action == actionRemove && objects.containsKey(message.roleId)) {
							re.Status__c = statusTransitDelete;
							maps.add(re);
						}
					}
				}
			}
		}
		
		return maps;
	}
	
	/**
	 * Send roles<->employee mappings to SABA
	 * Many to Many associations 
	 */
	private static List<LMS_Role_Employee__c> sendRolesToEmployeeMappings(Map<String, List<LMS_Role_Employee__c>> mappings, Map<String, Map<String, LMS_Role_Employee__c>> objects) {
		errorList = new List<String>();
		errorMap = new Map<String, List<String>>();
		LMS_RoleAdminService.personRoleResponse responses = LMS_RoleAdminServiceWrapper.processRolesToEmployeeAssociation(mappings);
		errorMap.put('CALLOUT',LMS_RoleAdminServiceWrapper.getErrors());
		List<LMS_Role_Employee__c> maps = new List<LMS_Role_Employee__c>();
		
    	if(null != responses.rolesPersonResponseItems) {
			if(responses.errors != null) {
				for(LMS_RoleAdminService.error err : responses.errors) {
					errorList.add(err.message);
					errorMap.put('CALLOUT', errorList);
				}
			}
        	for(LMS_RoleAdminService.rolesPersonResponseItem response : responses.rolesPersonResponseItems) {
        		if(response.success == true) {
        			for(LMS_RoleAdminService.roleMapResponse role : response.roleMapResponses) {
        				LMS_Role_Employee__c roleEmp = objects.get(response.personNo).get(role.roleId);
        				if(role.success == true && mappings.containsKey(actionAdd)) {
        					roleEmp.Sync__c = inSync;
        					roleEmp.SABA_Role_Employee_PK__c = role.details;
        					roleEmp.Status__c = statusCommitted;
			                roleEmp.Assigned_By__c = UserInfo.getName();
        					maps.add(roleEmp);
        					System.debug('-------------Person Mapping Sent to Saba Successfull--------------');
        				} else if(role.success == false && mappings.containsKey(actionAdd)) {
        					System.debug('-------------Could not add personId:' + roleEmp.Employee_Id__r.Name + ' to SABA');
        				}
        				
        				if(role.success == true && mappings.containsKey(actionRemove)) {
							maps.add(roleEmp);
        					System.debug('-------------Person Mapping Removed from Saba Successfull--------------');
        				} else if(role.success == false && mappings.containsKey(actionRemove)) {
        					System.debug('-------------Could not add personId:' + roleEmp.Employee_Id__r.Name + ' to SABA');
        				}
        			}
        		}
        	}
        } else {
        	System.debug('-------------Role Sent to Saba Unsuccessfull--------------');
        }
        
        return maps;
	}
	
}