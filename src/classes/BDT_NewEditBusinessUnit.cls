public with sharing class BDT_NewEditBusinessUnit {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Administration Sites
	public String buId {get;set;}
	public Business_Unit__c businessUnit {get;set;}
	
	public BDT_NewEditBusinessUnit(){
		
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Sites');
		buId = System.currentPageReference().getParameters().get('buId');
		businessUnit = new Business_Unit__c();
		
		if(buId!=null && buId!=''){
			try{
				businessUnit = [Select b.PRA_Business_Unit_Id__c, b.Name, b.Description__c, b.Currency__c, b.Abbreviation__c 
								From Business_Unit__c b 
								where b.id = :buId];
			}catch(QueryException e){				
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Business Unit Id could not be found. ');
        		ApexPages.addMessage(msg); 
			}
		}
	}
	
	public PageReference save(){
		
		try{			
			upsert businessUnit;
			
		}catch(DMLException e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to save Business Unit. ');
        	ApexPages.addMessage(msg); 
		}
		PageReference sitePage = new PageReference(System.Page.BDT_Sites.getUrl());
		if(businessUnit.id!=null){
			sitePage.getParameters().put('buId', businessUnit.id);
		}
		sitePage.setRedirect(true);
		return sitePage;
	}
	
	public PageReference cancel(){
		PageReference sitePage = new PageReference(System.Page.BDT_Sites.getUrl());
		if(businessUnit.id!=null){
			sitePage.getParameters().put('buId', businessUnit.id);
		}
		sitePage.setRedirect(true);
		return sitePage;
	}
	
	
	public PageReference deleteSoft(){
		
		//set BU to deleted
		businessUnit.BDTDeleted__c = true;
		List<Site__c> siteList = new List<Site__c>();
		try{
			siteList = [Select s.Name, s.Id, s.City__c, s.Business_Unit__c, s.Business_Unit__r.id, s.Address_Line_1__c, s.Abbreviation__c 
						From Site__c s 
						Where s.Business_Unit__c = : businessUnit.id];
			
			//set Sites related to this particular BU are also set to deleted
			for(Site__c siL: siteList){
				siL.BDTDeleted__c = true; 
			}
			
		}catch(QueryException e){
				System.debug('Business Unit doesnt have any sites associated.');
		}
		
		try{
			update siteList;
			update businessUnit;
		}catch(DMLException e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to delete Business Unit. ');
        	ApexPages.addMessage(msg); 
		}
				 
		return new PageReference(System.Page.BDT_Sites.getUrl());
	}
	
	}