/**
 * Test class for course status change trigger
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestHandleCourseStatusChange {
	
    static List<LMSConstantSettings__c> constants;
    static List<CourseDomainSettings__c> domains;
    static List<LMS_Role__c> roles;
    static List<Role_Type__c> roleTypes;
    static List<LMS_Course__c> courses;
    static List<LMS_Role_Course__c> courseMappings;
    
	static void init() {
        constants = new LMSConstantSettings__c[] {
            new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
    		new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
    		new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
    		new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D'),
    		new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
    		new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
    		new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
    		new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
    		new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
    		new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
    		new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
    		new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended'),
    		new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
    		new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
    		new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
    		new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete')
        };
        insert constants;
        
        domains = new CourseDomainSettings__c[] {
            new CourseDomainSettings__c(Name = 'Archive', Domain_Id__c = 'Archive')
        };
        insert domains;
        
        courses = new LMS_Course__c[]{
            new LMS_Course__c(Offering_Template_No__c = 'course123', Course_Code__c = 'C123', Title__c = 'Course 123', Domain_Id__c = 'Internal',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse1',
                              Discontinued_From__c = Date.today()+1),
            new LMS_Course__c(Offering_Template_No__c = 'course321', Course_Code__c = 'C321', Title__c = 'Course 321', Domain_Id__c = 'Internal',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse2',
                              Discontinued_From__c = Date.today()+1)
        };
        insert courses;
        
        roleTypes = new Role_Type__c[]{
            new Role_Type__c(Name = 'Additional Role')
        };
        insert roleTypes;
        
        roles = new LMS_Role__c[]{
            new LMS_Role__c(Adhoc_Role_Name__c = 'role1', Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '1'),
            new LMS_Role__c(Adhoc_Role_Name__c = 'role2', Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '2'),
            new LMS_Role__c(Adhoc_Role_Name__c = 'role3', Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '3'),
            new LMS_Role__c(Adhoc_Role_Name__c = 'role4', Status__c = 'Active', Sync_Status__c = 'N', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '4')
        };
        insert roles;
        
        courseMappings = new LMS_Role_Course__c[]{
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[0].Id, Status__c = 'Draft', Sync_Status__c = 'N',
                                   Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[0].Id, Status__c = 'Pending - Add', Sync_Status__c = 'N',
                                   Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[2].Id, Course_Id__c = courses[0].Id, Status__c = 'Committed', Sync_Status__c = 'Y',
                                   Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[3].Id, Course_Id__c = courses[0].Id, Status__c = 'Draft Delete', Sync_Status__c = 'Y',
                                   Assigned_By__c = 'ASA')
        };
        insert courseMappings;
	}

    static testMethod void testStatusChangeRetired() {
        init();
        courses[0].Status__c = 'Retired';
        update courses;
		List<LMS_Role_Course__c> roleCourse = [SELECT Status__c from LMS_Role_Course__c WHERE Course_Id__c = :courses[0].Id];
		System.assert(roleCourse.size() == 2);
		for(LMS_Role_Course__c rc : roleCourse)	{
			System.assert(rc.Status__c == 'Removed');
		}
    }

    static testMethod void testStatusChangeSuspended() {
        init();
        courses[0].Status__c = 'Suspended';
        update courses;
		List<LMS_Role_Course__c> roleCourse = [SELECT Status__c from LMS_Role_Course__c WHERE Course_Id__c = :courses[0].Id];
		System.assert(roleCourse.size() == 2);
		for(LMS_Role_Course__c rc : roleCourse)	{
			System.assert(rc.Status__c == 'Removed');
		}
    }

    static testMethod void testDomainChangeArchive() {
        init();
        courses[0].Domain_Id__c = 'Archive';
        update courses;
		List<LMS_Role_Course__c> roleCourse = [SELECT Status__c from LMS_Role_Course__c WHERE Course_Id__c = :courses[0].Id];
		System.assert(roleCourse.size() == 2);
		for(LMS_Role_Course__c rc : roleCourse)	{
			System.assert(rc.Status__c == 'Removed');
		}
    }

    static testMethod void testDiscontinuedChangePast() {
        init();
        courses[0].Discontinued_From__c = Date.today()-30;
        update courses;
		List<LMS_Role_Course__c> roleCourse = [SELECT Status__c from LMS_Role_Course__c WHERE Course_Id__c = :courses[0].Id];
		System.assert(roleCourse.size() == 2);
		for(LMS_Role_Course__c rc : roleCourse)	{
			System.assert(rc.Status__c == 'Removed');
		}
    }

    static testMethod void testReplacementCourse() {
        init();
        courses[1].Replacement_Course__c = courses[0].SABA_ID_PK__c;
        update courses;
		List<LMS_Role_Course__c> roleCourse = [SELECT Status__c from LMS_Role_Course__c WHERE Course_Id__c = :courses[1].Id];
		System.debug('----------------RCs----------------'+roleCourse.size());
		System.assert(roleCourse.size() == 4);
		for(LMS_Role_Course__c rc : roleCourse)	{
			System.assert(rc.Status__c == 'Pending - Add');
		}
    }
}