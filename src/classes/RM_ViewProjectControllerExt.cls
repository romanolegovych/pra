public with sharing class RM_ViewProjectControllerExt {
    transient public string therapeuticArea{get; private set;}
    transient public string protocolID{get; private set;}
    transient public string phase{get;private set;}
    transient public string budget{get;private set;}
    transient public boolean bBid{get; private set;}
    transient public string DPD{get; private set;}
    transient public string GPM{get; private set;}
    transient public Boolean bFlag{get; private set;}
    transient public string displayWidth{get; private set;}
   
    private list<WFM_Protocol__c> pro;
    public WFM_Project__c proj{get; private set;}
    
    
    private final list<LocationSummary>  locationSummary;
    private final list<WorkHistory> whist;
    private final list<MileStone> mileStone;
    private final list<siteDetail> siteDetail;
    
    public List<string> monthLab{get;private set;}
    transient public boolean bVisHr{get;set;}
 
    transient public string displayType{get;private set;}
    
    public RM_ViewProjectControllerExt(ApexPages.StandardController stdController){
        this.proj = (WFM_Project__c)stdController.getRecord();
        string flag = ApexPages.currentPage().getParameters().get('flag');
    	system.debug('-------------flag----------------' + flag); 
    	bFlag = false;
    	displayWidth='80%';
    	if (flag != null){
	    	if (flag == '1'){
	    		bFlag = true;
	    		displayWidth='100%';
	    	}	    	
    	}
    	List<WFM_Project__c> lstProj = RM_ProjectService.GetProjectbyrID(proj.id);
		if (lstProj.size() > 0)
			proj = lstProj[0]; 
       
        
        if (proj.Status_Desc__c == 'Bid' && proj.Bid_Defense_Date__c != null)
            bBid = true;
        else
            bBid = false;
        DPD = proj.Director_Project_Delivery__c;
        GPM = proj.Global_Project_Manager__c;
      
        budget = RM_Tools.get2DecimalinStr(proj.Total_Budget__c.format());
        pro = RM_ProjectService.GetProtocolByProjectID(proj.id);
        system.debug('------pro------'+pro);
        if (pro.size() > 0){
            therapeuticArea = pro[0].Therapeutic_Area__c;
            protocolID = pro[0].name;
            phase = pro[0].Phase__c;        
        
            list<WFM_Site_Detail__c> sites = RM_ProjectService.GetSitesInfobyProtocolRID(pro[0].id);
            siteDetail = new list<siteDetail>();
            if (sites.size() > 0){
                for (WFM_Site_Detail__c site: sites)
                    siteDetail.add(new siteDetail(site));
            }
            
            locationSummary = getSiteLocData(pro[0]);
            mileStone = GetMileStoneData();
        }
        whist = getHistoryData();
    }
 
    public List<siteDetail> getSiteDetail(){
        return siteDetail;
    }
    public class siteDetail{
        transient public string siteName {get; private set;}
        transient public string country{get;private set;}
        transient public string state{get;private set;}
        transient public string status{get; private set;}
        transient public Integer  ActSubjEnroll{get; private set;}
        transient public Integer  PageExpected{get; private set;}
        transient public Integer  TotalForecasted{get; private set;}
        
        public siteDetail(WFM_Site_Detail__c site){
            siteName = site.name;
            country = site.Country__c;
            state = site.State_Province__c;
            status = site.status__c;
            ActSubjEnroll = Integer.valueOf(site.Actual_Subj_Enroll__c);
            PageExpected = Integer.valueOf(site.CRF_Pages_Expected__c);
            TotalForecasted = Integer.valueOf(site.Total_Forecasted_Visits__c);
        }
        
        
    }
    public class LocationSummary{
 
        transient public string region {get; private set;}        
        transient public string country {get; private set;}
        transient public Integer num_site {get; private set;}
        transient public Integer num_ActiveSite{get; private set;}
        transient public Integer num_Subj{get; private set;}
        
        public LocationSummary(AggregateResult ar){        
            region = (string)ar.get('Region_Name__c');           
            country = (string)ar.get('Country__c');
            num_site= (Integer)ar.get('num_site');
            num_Subj = Integer.valueOf((decimal)ar.get('subjEnroll'));
        }               
    }
    
    public List<LocationSummary> GetSiteLocation(){
        return locationSummary;
    }
    private list<LocationSummary> getSiteLocData(WFM_Protocol__c prot){
        AggregateResult[] actSite = RM_projectService.GetActiveSitesLocationbyProtocolRID(prot.id);
        map <string, integer> mapSite = new map<string, integer>();   
        for(AggregateResult arAct: actSite)
            mapSite.put((string)arAct.get('Region_Name__c') + (string)arAct.get('Country__c'), (integer)arAct.get('num_site'));
        AggregateResult[] siteSummary = RM_ProjectService.GetSitesLocationbyProtocolRID(prot.id);
        
        List<LocationSummary> locSummary = new List<LocationSummary>();      
        
        for (AggregateResult ar : siteSummary)  {
            LocationSummary ls = new LocationSummary(ar);
            if (mapSite.get(ls.region+ls.country) != null)
                ls.num_ActiveSite = mapSite.get(ls.region+ls.country);      
            else
                ls.num_ActiveSite = 0;
            locSummary.add(ls); 
        }
        return locSummary;
    }
    public class WorkHistory{
    
        transient public string lastName {get; private set;}
        transient public string firstName {get; private set;}
        transient public double Total_work_hr {get; private set;}
        transient public string Start_date {get; private set;}
        transient public string End_date {get; private set;}    
        transient public string Buf_code {get; private set;}
        
        public WorkHistory(WFM_EE_Work_History_Summary__c his)
        {
            lastName = his.Employee_ID__r.Last_Name__c;
            Total_work_hr = his.Sum_Worked_Hours__c;
            Start_date = his.Start_Year_Month__c;
            End_date=his.End_Year_Month__c;
            Buf_code=his.Buf_Code__c;
            firstName=his.Employee_ID__r.first_Name__c;
        }               
    }
    
    Public List<WorkHistory> GetHistory(){
        return whist;
    }
    private list<WorkHistory> getHistoryData(){
        list<WFM_EE_Work_History_Summary__c> HistSummary = RM_EmployeeService.GetWorkHistorybyProjectRID(proj.id);
        List<WorkHistory> whistory = new List<WorkHistory>();
        for (WFM_EE_Work_History_Summary__c his : HistSummary)        
                whistory.add(new WorkHistory(his));
    
        return whistory;
    }
    public class MileStone{
        transient public string description{get;private set;}
        transient public string contractedDate{get; private set;}
        transient public string acturalDate{get; private set;}
        transient public string revisedDate{get; private set;}
        
        public MileStone(string des, date conDate, date actDate, date revDate){
            description = des;
            contractedDate = RM_Tools.GetStringfromDate(conDate, 'mm/dd/yyyy');
            acturalDate = RM_Tools.GetStringfromDate(actDate, 'mm/dd/yyyy');
            revisedDate =  RM_Tools.GetStringfromDate(revDate,'mm/dd/yyyy');
        }       
    }
    
    public List<MileStone> GetMilestones(){
        return mileStone;
    }
    private list<MileStone> GetMileStoneData(){ 
        List<MileStone> miles = new List<MileStone>();
        if (pro.size() > 0){        
            
            miles.add(new MileStone('DB Lock', pro[0].Contracted_DB_Lock_DT__c, pro[0].DB_Lock_DT__c, null));
            miles.add(new MileStone('First Subject Enrolled', pro[0].Contracted_First_Subj_Enroll__c, pro[0].Actual_First_Subj_Enroll__c, pro[0].Revised_First_Subj_Enroll__c));
            miles.add(new MileStone('First Site Initiated', pro[0].Contracted_First_Site_Init__c, pro[0].Actual_First_Site_Init__c, pro[0].Revised_First_Site_Init__c));
            miles.add(new MileStone('Last Subject Enrolled', pro[0].Contracted_Last_Subj_Enroll__c, pro[0].Actual_Last_Subj_Enroll__c, pro[0].Revised_Last_Subj_Enroll__c));
            miles.add(new MileStone('Last Subject Off Study', pro[0].Contracted_Last_Subj_Off_Study__c, pro[0].Actual_Last_Subj_Off_Study__c, pro[0].Revised_Last_Subj_Off_Study__c));
            miles.add(new MileStone('Last Site Closed', pro[0].Contracted_Last_Site_Closed__c, pro[0].Actual_Last_Site_Closed__c, pro[0].Revised_Last_Site_Closed__c));
        }
        return miles;
    }
   /* public list<RM_ViewAssignmentsCompController.SixMonthAssignment> getAssignments(){
  		system.debug('---Hr---'+ ApexPages.currentPage().getParameters().get('Hr'));
  		RM_ViewAssignmentsCompController rmAssignment = new RM_ViewAssignmentsCompController();
  		rmAssignment.recordID = proj.ID;
  		rmAssignment.startMonth = -3;
  		rmAssignment.endMonth = 5;
  		rmAssignment.displayType = 'Resource';
  		rmAssignment.bEdit = true;
  		rmAssignment.pgeName = '/apex/RM_ViewProject';
  		system.debug('------selectedBusinessUnit pass------' + ApexPages.currentPage().getParameters().get('BU'));
  		if (ApexPages.currentPage().getParameters().get('BU') == '')
  			rmAssignment.selectedBusinessUnit  = '';
  		else
  			rmAssignment.selectedBusinessUnit = ApexPages.currentPage().getParameters().get('BU');
  		list<RM_ViewAssignmentsCompController.SixMonthAssignment> assignments = rmAssignment.getActAssignments();
  		monthLab = rmAssignment.monthLab;
  		
  		// system.debug('------rmAssignment.bVisHr------' + rmAssignment.bVisHr);
  		bVisHr = Boolean.valueOf(ApexPages.currentPage().getParameters().get('Hr'));
  		
  		displayType = rmAssignment.displayType;
  		return assignments;
  	}*/
}