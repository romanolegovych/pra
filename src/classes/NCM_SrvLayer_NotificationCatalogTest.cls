/**
 * @description	Implements the test for the functions of the NCM_SrvLayer_NotificationCatalog class
 * @author		Dimitrios Sgourdos
 * @version		Created: 07-Sep-2015, Edited: 08-Oct-2015
 */
@isTest
private class NCM_SrvLayer_NotificationCatalogTest {
	
	/**
	 * @description	Test the function getNotificationCatalogFromWrapperData.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015, Edited: 08-Oct-2015
	 */
	static testMethod void getNotificationCatalogFromWrapperDataTest() {
		// Create data
		NCM_API_DataTypes.NotificationCatalogWrapper source = new NCM_API_DataTypes.NotificationCatalogWrapper();
		source.notificationName			= 'Topic A';
		source.notificationDescription	= 'This is a test topic';
		source.notificationCategory		= NCM_API_DataTypes.PROTOCOL_NOTIFICATION;
		source.notificationType			= NCM_API_DataTypes.ALERT_TYPE;
		source.comments					= 'These are test comments';
		source.defaultReminderPeriod	= 10;
		source.active					= true;
		source.priority					= NCM_API_DataTypes.NORMAL_PRIORITY;
		source.acknowledgementRequired	= true;
		source.chatterEnabled			= true;
		source.SMSallowed				= true;
		source.TriggeredByBatchJob		= false;
		source.implementationClassName	= 'Test_class_name.cls';
		
		String errorMessage = 'Error in creating a notification catalog instance from wrapper item';
		
		// Check the function with invalid parameters
		NotificationCatalog__c result = NCM_SrvLayer_NotificationCatalog.getNotificationCatalogFromWrapperData(NULL, NULL);
		
		system.assertEquals(NULL, result, errorMessage);
		
		// Check the function with valid parameters
		result = NCM_SrvLayer_NotificationCatalog.getNotificationCatalogFromWrapperData(source, result);
		
		system.assertNotEquals(NULL, result, errorMessage);
		system.assertEquals('Topic A', result.NotificationName__c, errorMessage);
		system.assertEquals('This is a test topic', result.NotificationDescription__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PROTOCOL_NOTIFICATION, result.Category__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ALERT_TYPE, result.Type__c, errorMessage);
		system.assertEquals('These are test comments', result.Comments__c, errorMessage);
		system.assertEquals(10, result.DefaultReminderPeriod__c, errorMessage);
		system.assertEquals(true, result.Active__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.NORMAL_PRIORITY, result.Priority__c, errorMessage);
		system.assertEquals(true, result.AcknowledgementRequired__c, errorMessage);
		system.assertEquals(true, result.ChatterEnabled__c, errorMessage);
		system.assertEquals(true, result.SMSallowed__c, errorMessage);
		system.assertEquals(false, result.TriggeredByBatchJob__c, errorMessage);
		system.assertEquals('Test_class_name.cls', result.ImplementationClassName__c, errorMessage);
	}
	
	
	/**
	 * @description	Test the function insertNotificationCatalogEntries.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015, Edited: 08-Oct-2015
	 */
	static testMethod void insertNotificationCatalogEntriesTest() {
		// Create data
		List<NCM_API_DataTypes.NotificationCatalogWrapper> source = new List<NCM_API_DataTypes.NotificationCatalogWrapper>();
		
		NCM_API_DataTypes.NotificationCatalogWrapper tmpItem = new NCM_API_DataTypes.NotificationCatalogWrapper();
		tmpItem.notificationName		= 'Topic A';
		tmpItem.notificationDescription	= 'This is a test topic A';
		tmpItem.notificationCategory	= NCM_API_DataTypes.PROTOCOL_NOTIFICATION;
		tmpItem.notificationType		= NCM_API_DataTypes.ALERT_TYPE;
		tmpItem.comments				= 'These are test comments A';
		tmpItem.defaultReminderPeriod	= 10;
		tmpItem.active					= true;
		tmpItem.priority				= NCM_API_DataTypes.HIGH_PRIORITY;
		tmpItem.acknowledgementRequired	= true;
		tmpItem.chatterEnabled			= true;
		tmpItem.SMSallowed				= true;
		tmpItem.TriggeredByBatchJob		= false;
		tmpItem.implementationClassName	= 'Test_class_name_A.cls';
		source.add(tmpItem);
		
		tmpItem = new NCM_API_DataTypes.NotificationCatalogWrapper();
		tmpItem.notificationName		= 'Topic B';
		tmpItem.notificationDescription	= 'This is a test topic B';
		tmpItem.notificationCategory	= NCM_API_DataTypes.COUNTRY_NOTIFICATION;
		tmpItem.notificationType		= NCM_API_DataTypes.INFO_TYPE;
		tmpItem.comments				= 'These are test comments B';
		tmpItem.defaultReminderPeriod	= 24;
		tmpItem.active					= true;
		tmpItem.priority				= NCM_API_DataTypes.NORMAL_PRIORITY;
		tmpItem.acknowledgementRequired	= false;
		tmpItem.chatterEnabled			= true;
		tmpItem.SMSallowed				= false;
		tmpItem.TriggeredByBatchJob		= true;
		tmpItem.implementationClassName	= 'Test_class_name_B.cls';
		source.add(tmpItem);
		
		String errorMessage = 'Error in inserting notification catalog entries';
		
		// Check the function with invalid parameters
		NCM_SrvLayer_NotificationCatalog.insertNotificationCatalogEntries(NULL);
		
		List<NotificationCatalog__c> results = [SELECT ID FROM NotificationCatalog__c LIMIT 50000];
		
		system.assertEquals(0, results.size(), errorMessage);
		
		// Check the function with invalid parameters
		NCM_SrvLayer_NotificationCatalog.insertNotificationCatalogEntries(source);
		
		results = 	[SELECT NotificationName__c,
							NotificationDescription__c,
							Category__c,
							Type__c,
							Comments__c,
							DefaultReminderPeriod__c,
							Active__c,
							Priority__c,
							AcknowledgementRequired__c,
							ChatterEnabled__c,
							SMSallowed__c,
							TriggeredByBatchJob__c,
							ImplementationClassName__c
					FROM	NotificationCatalog__c
					ORDER BY	NotificationName__c
					LIMIT	50000];
		
		system.assertEquals(2, results.size(), errorMessage);
		
		system.assertEquals('Topic A', results[0].NotificationName__c, errorMessage);
		system.assertEquals('This is a test topic A', results[0].NotificationDescription__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PROTOCOL_NOTIFICATION, results[0].Category__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ALERT_TYPE, results[0].Type__c, errorMessage);
		system.assertEquals('These are test comments A', results[0].Comments__c, errorMessage);
		system.assertEquals(10, results[0].DefaultReminderPeriod__c, errorMessage);
		system.assertEquals(true, results[0].Active__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.HIGH_PRIORITY, results[0].Priority__c, errorMessage);
		system.assertEquals(true, results[0].AcknowledgementRequired__c, errorMessage);
		system.assertEquals(true, results[0].ChatterEnabled__c, errorMessage);
		system.assertEquals(true, results[0].SMSallowed__c, errorMessage);
		system.assertEquals(false, results[0].TriggeredByBatchJob__c, errorMessage);
		system.assertEquals('Test_class_name_A.cls', results[0].ImplementationClassName__c, errorMessage);
		
		system.assertEquals('Topic B', results[1].NotificationName__c, errorMessage);
		system.assertEquals('This is a test topic B', results[1].NotificationDescription__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.COUNTRY_NOTIFICATION, results[1].Category__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.INFO_TYPE, results[1].Type__c, errorMessage);
		system.assertEquals('These are test comments B', results[1].Comments__c, errorMessage);
		system.assertEquals(24, results[1].DefaultReminderPeriod__c, errorMessage);
		system.assertEquals(true, results[1].Active__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.NORMAL_PRIORITY, results[1].Priority__c, errorMessage);
		system.assertEquals(false, results[1].AcknowledgementRequired__c, errorMessage);
		system.assertEquals(true, results[1].ChatterEnabled__c, errorMessage);
		system.assertEquals(false, results[1].SMSallowed__c, errorMessage);
		system.assertEquals(true, results[1].TriggeredByBatchJob__c, errorMessage);
		system.assertEquals('Test_class_name_B.cls', results[1].ImplementationClassName__c, errorMessage);
	}
	
	
	/**
	 * @description	Test the function updateNotificationCatalogEntries.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 */
	static testMethod void updateNotificationCatalogEntriesTest() {
		// Create data
		List<NotificationCatalog__c> initialList = new List<NotificationCatalog__c>();
		initialList.add( new NotificationCatalog__c(NotificationName__c='Topic A', ImplementationClassName__c='ClsA'));
		initialList.add( new NotificationCatalog__c(NotificationName__c='Topic B', ImplementationClassName__c='ClsB'));
		insert initialList;
		
		List<NCM_API_DataTypes.NotificationCatalogWrapper> source = new List<NCM_API_DataTypes.NotificationCatalogWrapper>();
		
		NCM_API_DataTypes.NotificationCatalogWrapper tmpItem = new NCM_API_DataTypes.NotificationCatalogWrapper();
		tmpItem.notificationName		= 'Topic A';
		tmpItem.implementationClassName	= 'Test_class_name_A.cls';
		source.add(tmpItem);
		
		tmpItem = new NCM_API_DataTypes.NotificationCatalogWrapper();
		tmpItem.notificationName		= 'Topic B';
		tmpItem.implementationClassName	= 'Test_class_name_B.cls';
		source.add(tmpItem);
		
		String errorMessage = 'Error in updating notification catalog entries';
		
		// Check the function with invalid parameters
		NCM_SrvLayer_NotificationCatalog.updateNotificationCatalogEntries(NULL);
		
		List<NotificationCatalog__c> results = [SELECT	NotificationName__c, ImplementationClassName__c
												FROM	NotificationCatalog__c
												ORDER BY NotificationName__c
												LIMIT	50000];
		
		system.assertEquals(2, results.size(), errorMessage);
		
		system.assertEquals('Topic A', results[0].NotificationName__c, errorMessage);
		system.assertEquals('ClsA', results[0].ImplementationClassName__c, errorMessage);
		
		system.assertEquals('Topic B', results[1].NotificationName__c, errorMessage);
		system.assertEquals('ClsB', results[1].ImplementationClassName__c, errorMessage);
		
		// Check the function with invalid parameters
		NCM_SrvLayer_NotificationCatalog.updateNotificationCatalogEntries(source);
		
		results =  [SELECT	NotificationName__c, ImplementationClassName__c
					FROM	NotificationCatalog__c
					ORDER BY NotificationName__c
					LIMIT	50000];
		
		system.assertEquals(2, results.size(), errorMessage);
		
		system.assertEquals('Topic A', results[0].NotificationName__c, errorMessage);
		system.assertEquals('Test_class_name_A.cls', results[0].ImplementationClassName__c, errorMessage);
		
		system.assertEquals('Topic B', results[1].NotificationName__c, errorMessage);
		system.assertEquals('Test_class_name_B.cls', results[1].ImplementationClassName__c, errorMessage);
	}
	
	
	/**
	 * @description	Test the function disableNotificationCatalogEntriesByName.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 */
	static testMethod void disableNotificationCatalogEntriesByNameTest() {
		// Create data
		List<NotificationCatalog__c> source = new List<NotificationCatalog__c>();
		source.add( new NotificationCatalog__c(NotificationName__c='Topic A', ImplementationClassName__c='ClsA', Active__c = true));
		source.add( new NotificationCatalog__c(NotificationName__c='Topic B', ImplementationClassName__c='ClsB', Active__c = true));
		source.add( new NotificationCatalog__c(NotificationName__c='Topic C', ImplementationClassName__c='ClsB', Active__c = true));
		insert source;
		
		String errorMessage = 'Error in disabling notification catalog entries by name';
		
		// Check the function with invalid parameters
		NCM_SrvLayer_NotificationCatalog.disableNotificationCatalogEntriesByName(NULL);
		
		List<NotificationCatalog__c> results = [SELECT	NotificationName__c, Active__c
												FROM	NotificationCatalog__c
												ORDER BY NotificationName__c
												LIMIT	50000];
		
		system.assertEquals(3, results.size(), errorMessage);
		
		system.assertEquals('Topic A', results[0].NotificationName__c, errorMessage);
		system.assertEquals(true, results[0].Active__c, errorMessage);
		
		system.assertEquals('Topic B', results[1].NotificationName__c, errorMessage);
		system.assertEquals(true, results[1].Active__c, errorMessage);
		
		system.assertEquals('Topic C', results[2].NotificationName__c, errorMessage);
		system.assertEquals(true, results[2].Active__c, errorMessage);
		
		// Check the function with valid parameters
		Set<String> topicNameSet = new Set<String>{'Topic A', 'Topic B'};
		
		NCM_SrvLayer_NotificationCatalog.disableNotificationCatalogEntriesByName(topicNameSet);
		
		results =  [SELECT	NotificationName__c, Active__c
					FROM	NotificationCatalog__c
					ORDER BY NotificationName__c
					LIMIT	50000];
		
		system.assertEquals(3, results.size(), errorMessage);
		
		system.assertEquals('Topic A', results[0].NotificationName__c, errorMessage);
		system.assertEquals(false, results[0].Active__c, errorMessage);
		
		system.assertEquals('Topic B', results[1].NotificationName__c, errorMessage);
		system.assertEquals(false, results[1].Active__c, errorMessage);
		
		system.assertEquals('Topic C', results[2].NotificationName__c, errorMessage);
		system.assertEquals(true, results[2].Active__c, errorMessage);
	}
	
	
	/**
	 * @description	Test the function disableNotificationCatalogEntries.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 */
	static testMethod void disableNotificationCatalogEntriesTest() {
		// Create data
		List<NotificationCatalog__c> initialList = new List<NotificationCatalog__c>();
		initialList.add( new NotificationCatalog__c(NotificationName__c='Topic A', ImplementationClassName__c='ClsA', Active__c = true));
		initialList.add( new NotificationCatalog__c(NotificationName__c='Topic B', ImplementationClassName__c='ClsB', Active__c = true));
		insert initialList;
		
		List<NCM_API_DataTypes.NotificationCatalogWrapper> source = new List<NCM_API_DataTypes.NotificationCatalogWrapper>();
		
		NCM_API_DataTypes.NotificationCatalogWrapper tmpItem = new NCM_API_DataTypes.NotificationCatalogWrapper();
		tmpItem.notificationName		= 'Topic A';
		tmpItem.implementationClassName	= 'ClsA';
		source.add(tmpItem);
		
		tmpItem = new NCM_API_DataTypes.NotificationCatalogWrapper();
		tmpItem.notificationName		= 'Topic B';
		tmpItem.implementationClassName	= 'ClsB';
		source.add(tmpItem);
		
		String errorMessage = 'Error in disabling notification catalog entries';
		
		// Check the function with invalid parameters
		NCM_SrvLayer_NotificationCatalog.disableNotificationCatalogEntries(NULL);
		
		List<NotificationCatalog__c> results = [SELECT	NotificationName__c, Active__c
												FROM	NotificationCatalog__c
												ORDER BY NotificationName__c
												LIMIT	50000];
		
		system.assertEquals(2, results.size(), errorMessage);
		
		system.assertEquals('Topic A', results[0].NotificationName__c, errorMessage);
		system.assertEquals(true, results[0].Active__c, errorMessage);
		
		system.assertEquals('Topic B', results[1].NotificationName__c, errorMessage);
		system.assertEquals(true, results[1].Active__c, errorMessage);
		
		// Check the function with valid parameters
		NCM_SrvLayer_NotificationCatalog.disableNotificationCatalogEntries(source);
		
		results =  [SELECT	NotificationName__c, Active__c
					FROM	NotificationCatalog__c
					ORDER BY NotificationName__c
					LIMIT	50000];
		
		system.assertEquals(2, results.size(), errorMessage);
		
		system.assertEquals('Topic A', results[0].NotificationName__c, errorMessage);
		system.assertEquals(false, results[0].Active__c, errorMessage);
		
		system.assertEquals('Topic B', results[1].NotificationName__c, errorMessage);
		system.assertEquals(false, results[1].Active__c, errorMessage);
	}
}