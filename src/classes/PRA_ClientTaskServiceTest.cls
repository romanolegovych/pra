/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PRA_ClientTaskServiceTest {
    
    static testMethod void testConstructor() {
        // This is just for code coverage
        test.startTest();
        PRA_ClientTaskService da = new PRA_ClientTaskService();
        test.stopTest();
    }
    
    static testMethod void testgetClientTaskWrapperListbyAction() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll(); 
        
        test.startTest();
        List<PRA_ClientTaskService.ClientTaskWrapper> ctList = PRA_ClientTaskService.getClientTaskWrapperListbyAction(tu.clientProject.id,PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS);        
        System.assertEquals(2, ctList.size());
        //System.assertEquals(null, ctList[0].close_date__c);
        //ctList[0].close_date__c=Date.today().addMonths(1).toStartOfMonth();
        //update ctList;
        ctList = PRA_ClientTaskService.getClientTaskWrapperListbyAction(tu.clientProject.id,PRA_ClientTaskService.TO_DISPLAY_ALL_CLIENT_TASKS);
        System.assertEquals(2, ctList.size());       
        
        test.stopTest();
    }
    
     static testMethod void testValidatefailedCO() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll(); 
        Change_Order__c  co=tu.createChangeOrder(tu.clientProject.id);
        co.Status__c='Failed';
        update co;
         
        test.startTest();
        String  ErrorMsg= PRA_ClientTaskService.ValidatefailedCO(tu.clientProject.id,'Failed');        
        System.assertNotEquals(ErrorMsg, null);
        test.stopTest();
    }
    
     static testMethod void testupdateClientTaskwithCloseDate() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();        
        // Zero out all effort ratios
        List<Client_Task__c> ctList = [select id,project__r.name,Project__c,status__c
                                          from Client_Task__c
                                          where Project__r.id = :tu.clientProject.id
                                          ];
        System.assertEquals(2,ctList.size());
        List<string> ctidsList =new List<string>();
        ctidsList.add(ctList[0].id);
        ctidsList.add(ctList[1].id);
        test.startTest();
        Date ctCloseDate  = Date.today().addMonths(1).toStartOfMonth();
        String ErrorMsg = PRA_ClientTaskService.updateClientTaskwithCloseDate(ctidsList,ctCloseDate);
        List<PRA_ClientTaskService.ClientTaskWrapper> ctwList = PRA_ClientTaskService.getClientTaskWrapperListbyAction(tu.clientProject.id,PRA_ClientTaskService.TO_DISPLAY_ALL_CLIENT_TASKS);        
        System.assertEquals(null,ErrorMsg);  
        System.assertEquals(2,ctwList.size());
        test.stopTest();        

        System.assertEquals(2,ctList.size());
        
    }

}