@isTest
private class BDT_SearchTest {

    static testMethod void myUnitTest() {
    	
    	//create project
		List<Client_Project__c> cpList = new List<Client_Project__c>();
		Client_Project__c Project = BDT_TestDataUtils.buildProject(); 
		Project.code__c = 'PRA1311-003';
		cpList.add(Project.clone());
		Project.code__c = 'PRA1311-004';
		cpList.add(Project.clone());
		Project.code__c = 'PRA1311-005';
		cpList.add(Project.clone());
		insert cpList;
		system.debug(cpList[0].bdtdeleted__c);
		
		//create studies
		List<Study__c> studiesList = new List<Study__c>();
		studiesList.addAll(BDT_TestDataUtils.buildStudies(cpList[0]));
		studiesList.addAll(BDT_TestDataUtils.buildStudies(cpList[1]));
		studiesList.addAll(BDT_TestDataUtils.buildStudies(cpList[2]));
		insert studiesList;
    	
    	// initialize the search
        BDT_Search bdtsearch = new BDT_Search();
        
	   // SOSL are not properly executed in TEST classes. We need to specify the expected results
	    List<ID> SoslResultIDs = new List<ID>();
	    For (Client_Project__c cp : cpList){SoslResultIDs.add(cp.id);}
	    For (Study__c st : studiesList){SoslResultIDs.add(st.id);}
		Test.setFixedSearchResults(SoslResultIDs);
        
        // execute a valid search
        bdtsearch.searchTerm = '-0';
	    bdtsearch.searchObjects();
        system.assertEquals(0,bdtsearch.SearchError.length());
        // test if 3 projects were found
        system.assertEquals(3,bdtsearch.DisplayList.size());
        

        // test close functionality
        bdtsearch.closeSearch();
        
        // test study switching
        // create a fake user

		list<user> userList = BDT_TestDataUtils.buildTestUserList(1);

		system.runAs(userList[0]) {
        	BDT_Search.SetProjectAndStudies(cpList[0].id, String.valueOf(studiesList[0].id));
        	BDT_UserPreferenceService up = new BDT_UserPreferenceService();
			system.assertEquals(cpList[0].id,up.userPreferences.get(BDT_UserPreferenceDataAccessor.PROJECT).text__c);
		}
    }
}