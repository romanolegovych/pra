global class PRA_ProjectHealthIndicatorController {

	// Constants
	private static final String ST_NONE = 'None';
    //value entered by the user in the input text box 
    public String projectText{get;set;}
    public String searchprojectText{get;set;}
    public Date selectedProjectMigratedDate {get;set;}
    //values displayed on the page
    public String  ProjectName{get;set;}
    public String  ClientName{get;set;}
    public Integer TargetSites{get;set;}
    public Integer PlannedActivation{get;set;}
    public Integer SitesActivated{get;set;}
    public Decimal Complete{get;set;}
    public Integer TargetSubjects{get;set;}
    public Integer SubjectsScreenedActive{get;set;}
    public Integer SubjectsEnrolledActive{get;set;}
    public Integer SubjectsScreened{get;set;}
    public Integer SubjectsScreenedFailed{get;set;}
    public Integer SubjectsEnrolled{get;set;}
    public Integer SubjectsCompleted{get;set;}
    public Integer SubjectsEarlyTerminated{get;set;}
    public String  FirstSiteActivated{get;set;}
    public String  LastSiteActivated{get;set;}
    //Contains list of values to display
    public List<PHI_Project__c> projectdetaillist{get;set;}
    //value selected by the user in the picklist
    public String selectedSite{get;set;}
    public String selectedSubject{get;set;}
    //List of values to display in graphs
    public List<String> PlannedSitesList{get;set;}
    public List<String> ActivatedSitesList{get;set;}
    public List<String> ScreenedSubjectsList{get;set;}
    public List<String> EnrolledSubjectsList{get;set;}
    //List of values to display in Pageblocktables
    public List<String>  SiteCountriesList{get;set;}
    public List<Integer> CountriesPlannedSitesList{get;set;}
    public List<Integer> CountriesActivatedSitesList{get;set;}
    public List<String>  SubjectCountriesList{get;set;}
    public List<Integer> CountriesScreenedSubjectsList{get;set;}
    public List<Integer> CountriesEnrolledSubjectsList{get;set;} 
    //Dynamic Header values in pageblocktables
    public String SitesColumnHeader{get;set;}
    public String SubjectsColumnHeader{get;set;}
    //Hide and show of highchart & highstock graphs
    public boolean blnSiteMainGraph{get;set;}
    public boolean blnSiteSubGraph{get;set;}
    public boolean blnsubjectsMainGraph{get;set;}
    public boolean blnsubjectsSubGraph{get;set;}
    // Assining type of graph from the controller dynamically
    public  string SubjectGraphType{get;set;}
    public  string SiteGraphType{get;set;}
    // Assining Subtitles for a graph through the controller dynamically
    public  string SubTitleSiteGraph{get;set;}
    public  string SubTitleSubjectGraph{get;set;}
    //Variables for ShowTabularTable       
    public String showCriteria1{get;set;}
    public String criteriaVal1{get;set;}    
    public String showCriteria{get;set;}
    public String criteriaVal{get;set;}
    //lists of Wrapper Class 
    public List<GridDataTable> SitesTableList{get;set;}
    public List<GridDataTable> SubjectsTableList{get;set;}
    // for user prefs
    private Integer searchCount{get;set;}
    private PRA_Utils.UserPreference userPreference{get;private set;}
    
    //Controller
    public PRA_ProjectHealthIndicatorController() {
    
        SubTitleSubjectGraph='Cumulative View';
        SubTitleSiteGraph='Cumulative View';
        SubjectGraphType='line';
        SiteGraphType='line';
        blnSiteMainGraph=true;
        blnSiteSubGraph=false;
        blnsubjectsMainGraph=true;
        blnsubjectsSubGraph=false;  
        showCriteria1='display: none';
        criteriaVal1='► Show Table';        
        showCriteria='display: none';
        criteriaVal='► Show Table';
        selectedSite = 'Cumulative View';
        selectedSubject = 'Cumulative View';
        searchCount = -1;
        
        userPreference = PRA_Utils.retrieveUserPreference();
        if(userPreference != null) {
            List<Client_Project__c> clientProjectsList = 
            	[SELECT Name, Is_Project_Locked__c, Migrated_Date__c FROM Client_Project__c WHERE Id = :userPreference.projectId and Load_Status__c != 'New'];
            if(clientProjectsList.size() == 1) {
                projectText = clientProjectsList.get(0).Name;
                searchProject();
            } else {
            	searchCount++;
            }
        } else {
        	searchCount++;
        }
	}
	
	public void reset() {
		projectText = null;
		ClientName = null;
		ProjectName = null;
		TargetSites = null;
        PlannedActivation = null;
        SitesActivated = null;
        Complete = null;
        selectedProjectMigratedDate = null;
        FirstSiteActivated = null;
        LastSiteActivated = null;
        TargetSubjects = null;
        SubjectsScreenedActive = null;
        SubjectsEnrolledActive = null;
        SubjectsScreened = null;
	    SubjectsScreenedFailed = null;
	    SubjectsEnrolled = null;
	    SubjectsCompleted = null;
	    SubjectsEarlyTerminated = null;
	    searchProject();
	    SearchSites();
	    SearchSubjects();
	    SearchSiteBasedOnCountry();
	    SearchSubjectsBasedOnCountry();
	    PRA_Utils.deleteUserPreference();
	}
    
    // select list for site Activation section
    public List<SelectOption> getSearchOnSites() {
    
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('Cumulative View','Cumulative View'));
      options.add(new SelectOption('Month','Month'));
      options.add(new SelectOption('Country','Country'));
      return options;
    }
        
    // select list for Screening & Enrollment section
    public List<SelectOption> getSearchOnSubjects() {
    
      List<SelectOption> options = new List<SelectOption>();
      options.add(new SelectOption('Cumulative View','Cumulative View'));
      options.add(new SelectOption('Month','Month'));
      options.add(new SelectOption('Country','Country'));
      return options;
    }
    
    // Method which will return the month in string format
    public String getMonth(Integer month) {
    
        if(month==1)
            return 'Jan';
        if(month==2)
            return 'Feb';
        if(month==3)
            return 'Mar';
        if(month==4)
            return 'Apr';
        if(month==5)
            return 'May';
        if(month==6)
            return 'Jun';
        if(month==7)
            return 'Jul';
        if(month==8)
            return 'Aug';
        if(month==9)
            return 'Sep';
        if(month==10)
            return 'Oct';
        if(month==11)
            return 'Nov';
        if(month==12)
            return 'Dec';
    	return null;
    }

   //go button function
    public void searchProject() {
    
        searchCount++;
        searchprojectText=projectText;
        system.debug('------searchprojectText---------'+searchprojectText);
        FirstSiteActivated=null;
        LastSiteActivated=null;
        
        
        if(searchprojectText!=null && searchprojectText!='') {
        
           
            for(Client_Project__c obj:[select  Name,Client__r.Name
                                       from    Client_Project__c 
                                       where   Name like:searchprojectText and Load_Status__c != 'New' limit 1]) {                 
                if(userPreference != null) {
                    userPreference.projectId = obj.Id;
                } else { 
                    userPreference = new PRA_Utils.UserPreference();
                    userPreference.projectId = obj.Id;
                    userPreference.regionId = new String[]{ST_NONE};
                    userPreference.countryId = new String[]{ST_NONE};
                    userPreference.clientUnitId = new String[]{ST_NONE};
                    userPreference.clientUnitGroupId = new String[]{ST_NONE};
                    userPreference.operationalAreaId = new String[]{ST_NONE};
                    userPreference.unconfirmedOnly = false;
                    userPreference.confirmedOnly = false;
                    userPreference.crctId = '';
                    userPreference.userId = UserInfo.getUserId();
                }   
		        if(searchCount > 0) {
		            // Save user prefs
		            PRA_Utils.saveUserPreference(userPreference);
		        }                                       
                ProjectName=obj.Name;
                ClientName=obj.Client__r.Name; 
            }      
            projectdetaillist=[select id,Project_ID__r.Name,Total_Number_of_Target_Sites__c,Total_Number_of_Planned_Activations__c,Project_ID__r.Migrated_date__c,Total_Number_of_Actual_Site_Activations__c,Sites_Activated_vs_Planned__c,First_Site_Activated__c,
                                      Last_Site_Activated__c,Project_ID__r.Client__r.name,Total_Number_of_Target_Subjects__c,Total_Number_of_Subjects_Screened_Active__c,Total_Number_of_Subjects_Enrolled_Active__c
                               from    PHI_Project__c 
                              where   Project_ID__r.Name like:searchprojectText];
                          
            system.debug('------projectdetaillist------'+projectdetaillist);            
              
            if(projectdetaillist != null && projectdetaillist.size() > 0) {
	                for(PHI_Project__c obj:projectdetaillist) {
                    TargetSites=Integer.valueOf(obj.Total_Number_of_Target_Sites__c);
                    PlannedActivation=Integer.valueOf(obj.Total_Number_of_Planned_Activations__c);
                    SitesActivated=Integer.valueOf(obj.Total_Number_of_Actual_Site_Activations__c);
                    Complete=obj.Sites_Activated_vs_Planned__c;
                    selectedProjectMigratedDate=obj.Project_ID__r.Migrated_date__c;
                    
                    
                    if(obj.First_Site_Activated__c!=null){
                        String strFirstSiteActivatedMonth=getMonth(obj.First_Site_Activated__c.month());
                        FirstSiteActivated=String.valueOf(obj.First_Site_Activated__c.day())+' - '+strFirstSiteActivatedMonth+' - '+String.valueOf(obj.First_Site_Activated__c.year());
                     }
                    
                      if(obj.Last_Site_Activated__c!=null){
                        String strLastSiteActivatedMonth=getMonth(obj.Last_Site_Activated__c.month());                
                        LastSiteActivated=String.valueOf(obj.Last_Site_Activated__c.day())+' - '+strLastSiteActivatedMonth+' - '+String.valueOf(obj.Last_Site_Activated__c.year()); 
                    }                   
                    
                    // FirstSiteActivated=strFirstSiteActivatedMonth+' - '+String.valueOf(obj.First_Site_Activated__c.day())+' - '+String.valueOf(obj.First_Site_Activated__c.year());
                    //LastSiteActivated=strLastSiteActivatedMonth+' - '+String.valueOf(obj.Last_Site_Activated__c.day())+' - '+String.valueOf(obj.Last_Site_Activated__c.year());
                    //FirstSiteActivated=String.valueOf(obj.First_Site_Activated__c);
                   // LastSiteActivated=String.valueof(obj.Last_Site_Activated__c);   
                            
                    TargetSubjects=Integer.valueOf(obj.Total_Number_of_Target_Subjects__c);
                    SubjectsScreenedActive=Integer.valueOf(obj.Total_Number_of_Subjects_Screened_Active__c);
                    SubjectsEnrolledActive=Integer.valueOf(obj.Total_Number_of_Subjects_Enrolled_Active__c);
                    
                }
            }
               
            //Aggregate function 
            AggregateResult[] Projectgroup=[select sum(Total_Number_of_Subjects_Screened__c) TSS,sum(Total_Number_of_Subjects_Screened_Failed__c) TSF,
                                                   sum(Total_Number_of_Subjects_Enrolled__c) TSE,sum(Total_Number_of_Subjects_Complete__c) TSC,
                                                   sum(Total_Number_of_Subjects_Early_Terminate__c) TSET
                                            from   PHI_Monthly__c
                                            where  Project_ID__r.Name like:searchprojectText];
                    
                                            
            for (AggregateResult ar : Projectgroup) {
            
                 SubjectsScreened=Integer.valueOf(ar.get('TSS'));
                 SubjectsScreenedFailed=Integer.valueOf(ar.get('TSF'));
                 SubjectsEnrolled=Integer.valueOf(ar.get('TSE'));
                 SubjectsCompleted=Integer.valueOf(ar.get('TSC'));
                 SubjectsEarlyTerminated=Integer.valueOf(ar.get('TSET'));
                        
            }
              
            if(selectedSite.equalsIgnoreCase('Country')) {
	            SiteGraphType='column';
	            SubTitleSiteGraph='Country View';
	            SitesColumnHeader='Country';
	            SearchSiteBasedOnCountry();
            } else if(selectedSite.equalsIgnoreCase('Month')) {
	            SiteGraphType='column';
	            SubTitleSiteGraph='Monthly View';
	            SitesColumnHeader='Year-Month';
	            SearchSites();
            } else if(selectedSite.equalsIgnoreCase('Cumulative View')) {
	            SiteGraphType='line';
	            SubTitleSiteGraph='Cumulative View';
	            SitesColumnHeader='Year-Month';
	            SearchSites();
            }
               
            if(selectedSubject.equalsIgnoreCase('Country')) {
	            SubjectGraphType='column';
	            SubTitleSubjectGraph='Country View';
	            SubjectsColumnHeader='Country';
	            SearchSubjectsBasedOnCountry();
            } else if(selectedSubject.equalsIgnoreCase('Month')) {
	            SubjectGraphType='column';
	            SubTitleSubjectGraph='Monthly View';
	            SubjectsColumnHeader='Year-Month';
	            SearchSubjects();
            } else if(selectedSubject.equalsIgnoreCase('Cumulative View')) {
	            SubjectGraphType='line';
	            SubTitleSubjectGraph='Cumulative View'; 
	            SubjectsColumnHeader='Year-Month';
	            SearchSubjects();
            }
        }
    }
     
     
    //Site Method for Cummulative & Monthly View
    public void SearchSites() {
         blnSiteMainGraph=true;
         blnSiteSubGraph=false;
         Integer sumtspvalues=0;
         String tspstr;
         Integer sumtsavalues=0;
         String tsastr;
         PlannedSitesList=new List<String>();
         ActivatedSitesList=new List<String>(); 
         SitesTableList=new List<GridDataTable>(); 
         
        if(searchprojectText!=null && searchprojectText!='') {
        
            AggregateResult[] Sitegroup=[select   Month_Applies_To__c,sum(Total_Number_of_Sites_Planned__c) TSP,sum(Total_Number_of_Sites_Activated__c) TSA
                                           from   PHI_Monthly__c
                                           where  Project_ID__r.Name like:searchprojectText
                                           group  by  Month_Applies_To__c
                                           order  by  Month_Applies_To__c];
         
            for(AggregateResult ar : Sitegroup) {
           
                if(ar.get('Month_Applies_To__c')!=null && ar.get('TSP')!=null && ar.get('TSA')!=null) {
                 
                    GridDataTable obj=new GridDataTable();
                    Date dateVal=Date.valueOf(ar.get('Month_Applies_To__c'));
                    String strDate=dateVal.format();
                    obj.column1=strDate;
                    Integer tspvalue=Integer.valueOf(ar.get('TSP'));
                    Integer tsavalue=Integer.valueOf(ar.get('TSA'));
                    Integer month=dateVal.month()-1;
                    Integer year=dateVal.year();
                    if(sumtspvalues==0)
                            sumtspvalues=tspvalue;
                    else
                            sumtspvalues+=tspvalue;
                    if(sumtsavalues==0)
                            sumtsavalues=tsavalue;
                    else
                            sumtsavalues+=tsavalue;
                            
                    system.debug('-------tspvalue--------'+tspvalue);
                    system.debug('-------tsavalue--------'+tsavalue);
                    system.debug('-------dateVal--------'+dateVal);
                    system.debug('-----Month------'+month);
                    system.debug('-----year------'+year);
                    
                    if(selectedSite!=null && selectedSite!='' && selectedSite.equalsIgnoreCase('Cumulative View')) {
                    
                      tspstr='Date.UTC('+year+','+month+'),'+sumtspvalues;
                      tsastr='Date.UTC('+year+','+month+'),'+sumtsavalues;
                      obj.column2=sumtspvalues;
                      obj.column3=sumtsavalues;
                    }
                    else {
                       
                      tspstr='Date.UTC('+year+','+month+'),'+tspvalue;
                      tsastr='Date.UTC('+year+','+month+'),'+tsavalue;
                      obj.column2=tspvalue;
                      obj.column3=tsavalue;
                    }
                    
                    PlannedSitesList.add(tspstr);
                    ActivatedSitesList.add(tsastr);
                    SitesTableList.add(obj);
                }
            }
        } else {
        	PlannedSitesList = new List<String>();
        	ActivatedSitesList = new List<String>();
        	SitesTableList = new List<GridDataTable>();
        }  
    }
     
     
    //Subjects Method for Cummulative & Monthly View
    public void SearchSubjects() {
    
         blnsubjectsMainGraph=true;
         blnsubjectsSubGraph=false;
         Integer sumtssvalues=0;
         String tssstr;  
         Integer sumtsevalues=0;
         String tsestr;      
         ScreenedSubjectsList=new List<String>();   
         EnrolledSubjectsList=new List<String>(); 
         SubjectsTableList=new List<GridDataTable>();
          
        if(searchprojectText!=null && searchprojectText!='') {
           
            AggregateResult[] Subjectsgroup=[select Month_Applies_To__c,sum(Total_Number_of_Subjects_Screened__c) TSS,sum(Total_Number_of_Subjects_Enrolled__c) TSE
                                               from  PHI_Monthly__c
                                               where Project_ID__r.Name like:searchprojectText
                                               group by  Month_Applies_To__c
                                               order by  Month_Applies_To__c];
          
            for (AggregateResult ar : Subjectsgroup) {
            
                 GridDataTable obj1=new GridDataTable();
                 Date dateVal=Date.valueOf(ar.get('Month_Applies_To__c'));
                 String strDate=dateVal.format();
                 obj1.column1=strDate;
                 Integer tssvalue=Integer.valueOf(ar.get('TSS'));
                 Integer tsevalue=Integer.valueOf(ar.get('TSE'));
                 Integer month=dateVal.month()-1;
                 Integer year=dateVal.year();
           
                if(sumtssvalues==0)
                     sumtssvalues=tssvalue;
                else
                     sumtssvalues+=tssvalue;
                if(sumtsevalues==0)
                     sumtsevalues=tsevalue;
                else
                     sumtsevalues+=tsevalue; 
                              
                system.debug('-------tspvalue--------'+tsevalue);
                system.debug('-------tspvalue--------'+tssvalue);
                system.debug('-------dateVal--------'+dateVal);
                system.debug('-----Month------'+month);
                system.debug('-----year------'+year);
                    
                if(selectedSubject!=null && selectedSubject!='' && selectedSubject.equalsIgnoreCase('Cumulative View')) {
                 
                    tssstr='Date.UTC('+year+','+month+'),'+sumtssvalues;
                    tsestr='Date.UTC('+year+','+month+'),'+sumtsevalues;
                    obj1.column2=sumtssvalues;
                    obj1.column3=sumtsevalues;
                }
                else {
                 
                    tssstr='Date.UTC('+year+','+month+'),'+tssvalue;
                    tsestr='Date.UTC('+year+','+month+'),'+tsevalue;
                    obj1.column2=tssvalue;
                    obj1.column3=tsevalue;
                }
                ScreenedSubjectsList.add(tssstr);
                EnrolledSubjectsList.add(tsestr);
                SubjectsTableList.add(obj1);
            }
            system.debug('-----PlannedSitesList------'+ScreenedSubjectsList);                      
        } else {
        	ScreenedSubjectsList = new List<String>();
        	EnrolledSubjectsList = new List<String>();
        	SubjectsTableList = new List<GridDataTable>();
        } 
    }
     
     
    //Site Method for Country View
    public void SearchSiteBasedOnCountry() {
        blnSiteMainGraph=false;
        blnSiteSubGraph=true;
        Set<Id> contryIds1=new Set<Id>();
        SitesTableList=new List<GridDataTable>();
        CountriesPlannedSitesList=new List<Integer>();
        SiteCountriesList=new List<String>();      
        CountriesActivatedSitesList=new List<Integer>();   
            
        if(searchprojectText!=null && searchprojectText!='') {
            
            AggregateResult[] Sitesgroup=[select Country_ID__c,sum(Total_Number_of_Sites_Planned__c) TSP,sum(Total_Number_of_Sites_Activated__c) TSA
                                            from  PHI_Monthly__c
                                            where Project_ID__r.Name like:searchprojectText
                                            group by  Country_ID__c
                                            order by  Country_ID__c];
                     
            for(AggregateResult ar : Sitesgroup) {
            
             if(ar.get('Country_ID__c')!=null && ar.get('TSP')!=null && ar.get('TSA')!=null) {
                
                  GridDataTable obj1=new GridDataTable();
                  obj1.column3=Integer.valueOf(ar.get('TSA'));
                  obj1.column2=Integer.valueOf(ar.get('TSP'));                  
                  String strVal=String.valueOf(ar.get('Country_ID__c'));
                  Id idVal=Id.valueOf(strVal);
                  contryIds1.add(idVal);
                  if(Integer.valueOf(ar.get('TSP'))>0 || Integer.valueOf(ar.get('TSA'))>0) {
                  
                    CountriesPlannedSitesList.add(Integer.valueOf(ar.get('TSP')));
                    CountriesActivatedSitesList.add(Integer.valueOf(ar.get('TSA')));
                    //SiteCountriesList.add(idVal);
                  }                  
                  obj1.countryId=idVal;  
                  SitesTableList.add(obj1);    
                                   
                }
             
            }
           
            if((contryIds1!=null && contryIds1.size()>0) && (SitesTableList!=null && SitesTableList.size()>0)) {
            
                for(Country__c obj:[select Id,Name from Country__c where Id in :contryIds1]) {
                
                    for(GridDataTable obj2:SitesTableList) {
                    
                        if(obj.Id==obj2.countryId) {
                        
                            if(obj2.column2>0 || obj2.column3>0) {
                           
                              SiteCountriesList.add(obj.Name);
                              obj2.column1=obj.Name;
                            }
                            else {
                           
                              obj2.column1=obj.Name;
                            }
                        }
                    }
               
                }               
            }
           
        } else {
        	SitesTableList = new List<GridDataTable>();
        }   
        system.debug('---------------COUNTRIES-----------'+SitesTableList);
    }  
      
      
    //Subjects Method for Country View
    public void SearchSubjectsBasedOnCountry() {
    
        blnsubjectsMainGraph=false;
        blnsubjectsSubGraph=true;
        Set<Id> contryIds1=new Set<Id>();
        SubjectsTableList=new List<GridDataTable>();
        CountriesScreenedSubjectsList=new List<Integer>();
        SubjectCountriesList=new List<String>(); 
        CountriesEnrolledSubjectsList=new List<Integer>(); 
           
        if(searchprojectText!=null && searchprojectText!='') {
             
            AggregateResult[] Subjectsgroup=[select Country_ID__c,sum(Total_Number_of_Subjects_Screened__c) TSS,sum(Total_Number_of_Subjects_Enrolled__c) TSE
                                             from  PHI_Monthly__c
                                             where Project_ID__r.Name like:searchprojectText
                                             group by  Country_ID__c
                                             order by  Country_ID__c];
            
            for(AggregateResult ar : Subjectsgroup) { 
                   
                if(ar.get('Country_ID__c')!=null && ar.get('TSS')!=null && ar.get('TSE')!=null) {
                
                   GridDataTable obj1=new GridDataTable();
                   obj1.column2=Integer.valueOf(ar.get('TSS'));
                   obj1.column3=Integer.valueOf(ar.get('TSE'));
                   String strVal=String.valueOf(ar.get('Country_ID__c'));
                   Id idVal=Id.valueOf(strVal);
                   contryIds1.add(idVal);
                   if(Integer.valueOf(ar.get('TSS'))>0 || Integer.valueOf(ar.get('TSE'))>0) {
                    
                        CountriesScreenedSubjectsList.add(Integer.valueOf(ar.get('TSS')));
                        CountriesEnrolledSubjectsList.add(Integer.valueOf(ar.get('TSE')));
                    }
                   obj1.countryId=idVal;                            
                   SubjectsTableList.add(obj1);
                }
            }
           if((contryIds1!=null && contryIds1.size()>0) && (SubjectsTableList!=null && SubjectsTableList.size()>0)) {
            
               for(Country__c obj:[select Id,Name from Country__c where Id in :contryIds1]) {
               
                   for(GridDataTable obj2:SubjectsTableList) {
                   
                       if(obj.Id==obj2.countryId) {
                       
                           if(obj2.column2>0 || obj2.column3>0) {
                           
                               SubjectCountriesList.add(obj.Name);
                               obj2.column1=obj.Name;
                           }
                           else {
                           
                               obj2.column1=obj.Name;
                           }
                        }
                    
                    }               
                    
                }   
            }  
        } else {
        	SubjectsTableList = new List<GridDataTable>();
        }
    } 
      
       
       
       
    //Method calling from Sites Picklist   
    public void getgraphs1() {
    
        if(selectedSite.equalsIgnoreCase('Country')) {
        
            SiteGraphType='column';
            SubTitleSiteGraph='Country View';
            SitesColumnHeader='Country';
            SearchSiteBasedOnCountry();
        }
        else
        if(selectedSite.equalsIgnoreCase('Month')) {
        
            SiteGraphType='column';
            SitesColumnHeader='Year-Month';
            SubTitleSiteGraph='Monthly View';
            SearchSites();
        }
        else
        if(selectedSite.equalsIgnoreCase('Cumulative View')) {
        
            SiteGraphType='line';
            SitesColumnHeader='Year-Month';
            SubTitleSiteGraph='Cumulative View';
            SearchSites();
        }
    }
      
      
    //Method calling from Subjects Picklist 
    public void getgraphs() {
    
        if(selectedSubject.equalsIgnoreCase('Country')) {
        
            SubjectGraphType='column';
            SubTitleSubjectGraph='Country View'; 
            SubjectsColumnHeader='Country';
            SearchSubjectsBasedOnCountry();
        }
        else
        if(selectedSubject.equalsIgnoreCase('Month')) {
        
            SubjectGraphType='column';
            SubjectsColumnHeader='Year-Month';
            SubTitleSubjectGraph='Monthly View'; 
            SearchSubjects();
        }
        else
        if(selectedSubject.equalsIgnoreCase('Cumulative View')) {
        
            SubjectGraphType='line';
            SubjectsColumnHeader='Year-Month';
            SubTitleSubjectGraph='Cumulative View'; 
            SearchSubjects();
        }
       
    }
    
    // Wrapper class to hold the data to display in pageblock tables
    public class GridDataTable {
    
    	public String  column1{get;set;}
      	public Integer column2{get;set;} 
      	public Integer column3{get;set;}
      	public Id countryId{get;set;}
    }
}