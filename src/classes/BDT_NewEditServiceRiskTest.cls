@isTest
public with sharing class BDT_NewEditServiceRiskTest {

	static testmethod void NewEditServiceRiskTest(){
		
		ServiceCategory__c sc = new ServiceCategory__c(name = 'Test',code__c = '001.001');
        upsert sc;
    	Service__c s =new Service__c(name = 'Test', ServiceCategory__c = sc.id, IsDesignObject__c=true, SequenceNumber__c=0);
        
        upsert s;
        
        List<ServiceRisk__c> srl = new List<ServiceRisk__c>();
        srl.add(new ServiceRisk__c(Service__c=s.Id, Name='Test1', CommonRisk__c='Test1Risk1', CommonMitigation__c='Test1Mitigation1'));
        srl.add(new ServiceRisk__c(Service__c=s.Id, Name='Test2', CommonRisk__c='Test2Risk2', CommonMitigation__c='Test2Mitigation2'));
        upsert srl;
		
		PageReference a = new PageReference(system.Page.BDT_NewEditServiceRisk.getURL() );
		a.getParameters().put('ServiceId',s.Id );
		Test.setCurrentPage( a );
		
		BDT_NewEditServiceRisk b = New BDT_NewEditServiceRisk();
		
		system.assertEquals(2, b.ServiceRiskList.size() );
		
		b.addServiceRisk();
		
		system.assertEquals(3, b.ServiceRiskList.size() );
		
		b.cleanServiceRisks( b.ServiceRiskList );
		
		//system.assertEquals(2, b.ServiceRiskList.size() );		
		
		a.getParameters().put('ServiceRiskId',srl[1].Id );
		Test.setCurrentPage(a);
		
		b.deleteSoft();	
		system.assertEquals(1,b.deletedRisks.size() );
		system.assertNotEquals(null, b.save() );
		
		system.assertNotEquals(null, b.cancel() );
	}

}