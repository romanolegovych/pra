global class EWDIABatch  implements Database.Batchable<SObject>
{
	private static List<String> DIA_STEP_NAMES = new List<String>{
		'Site Activated'
	};
	
	/*
	private static List<String> DIA_EW_NAMES = new List<String>{
		'Site Activation - Essential Docs',
		'Site Activation - Protocol Specific Site Activation Requirements',
		'Site Activation - Local EC/IRB Submissions Approved',
		'Site Activation - CTA'
	};/**/
	
	@testvisible private static List<String> DIA_EW_NAMES = new List<String>{
		'Site Activation - Essential Docs',
		'Site Activation - Protocol Specific Requirements',
		'Site Activation - Submissions',
		'Site Activation - CTA'
	};
	
	public String query;
	
	public static void executeBatch()
	{
		
		List<String> stepNames = new List<String>();
		for (String stepName : DIA_STEP_NAMES)
		{
			stepNames.add('\'' + String.escapeSingleQuotes(stepName) + '\'');
		}
		
		String query = 'select Name, PAWS_Project_Flow_Site__c, PAWS_Project_Flow__c from Critical_Chain__c where '
			+ ' Cloned_From_Early_Warning__r.Name in :DIA_EW_NAMES and PAWS_Project_Flow__c != null';
		
		Database.executeBatch(new EWDIABatch(query), 1);
	}
	
	public EWDIABatch(String query)
	{
		this.query = query;
	}
	
	public EWDIABatch() {/*do nothing*/}
	
	global virtual Database.QueryLocator start(Database.BatchableContext bc)
	{
		return Database.getQueryLocator(query);
	}
	
	global virtual void execute(Database.BatchableContext BC, List<SObject> scope)
	{
		for (Critical_Chain__c ewRecord : (List<Critical_Chain__c>) scope)
		{
			updateDIAData(ewRecord);
		}
	}
	
	public void updateDIAData(Critical_Chain__c ewRecord)
	{
		for (PAWS_Project_Flow_Site__c pawsSite : Database.query('select ' + String.join(new List<String>(SObjectType.PAWS_Project_Flow_Site__c.Fields.getMap().keyset()), ', ') + ' from PAWS_Project_Flow_Site__c where Id = \'' + ewRecord.PAWS_Project_Flow_Site__c + '\''))
		{
			for (EWChain ew : EWChain.buildEWChains(new List<ID>{ewRecord.Id}))
			{
				copyDIAData(ew, pawsSite);
			}
			
			update pawsSite;
		}
	}
	
	public void updateDIAData(EWChain ew)
	{
		ID siteID = ew.chainRecord.PAWS_Project_Flow_Site__c;
		for (PAWS_Project_Flow_Site__c pawsSite : Database.query('select ' + String.join(new List<String>(SObjectType.PAWS_Project_Flow_Site__c.Fields.getMap().keyset()), ', ') + ' from PAWS_Project_Flow_Site__c where Id = :siteID'))
		{
			copyDIAData(ew, pawsSite);
			update pawsSite;
		}
		
		ID projectID = ew.chainRecord.PAWS_Project_Flow__c;
		for (ecrf__c pawsProject : Database.query('select ' + String.join(new List<String>(SObjectType.ecrf__c.Fields.getMap().keyset()), ', ') + ' from ecrf__c where Id = :projectID'))
		{
			copyDIAData(ew, pawsProject);
			update pawsProject;
		}
	}
	
	private void copyDIAData(EWChain ew, ecrf__c pawsProject)
	{
		copyPatientEnrollmentData(ew, pawsProject);
	}
	
	private void copyPatientEnrollmentData(EWChain ew, ecrf__c pawsProject)
	{
		if (ew.chainRecord.Cloned_From_Early_Warning__r.Name != 'Subject Enrollment')
		{
			return;
		}
		
		for (EWStep step : ew.steps)
		{
			if (step.extendedStep.Name == 'Project Enrollment Completed')
			{
				pawsProject.Patient_Enrollment_BI__c = ew.calculateBufferIndex();
				pawsProject.Is_Patient_Enrollment_Completed__c = step.actualEnd != null;
				break;
			}
		}
	}
	
	/*
	* Populates EW related data for PAWS_Project_Flow_Site__c data.
	*/
	@testvisible private void copyDIAData(EWChain ew, PAWS_Project_Flow_Site__c pawsSite)
	{
		copyEssentialDocsData(ew, pawsSite);
		//copyStartUpData(ew, pawsSite);
		copyStartUpData(ew, pawsSite);
		copyApprovalsData(ew, pawsSite);
		copyCTAFullyExecutedData(ew, pawsSite);
	}
	
	/**
	* Populates "Essential Docs" related EW and DIA data.
	*/
	private void copyEssentialDocsData(EWChain ew, PAWS_Project_Flow_Site__c pawsSite)
	{
		if (ew.chainRecord.Cloned_From_Early_Warning__r.Name != 'Site Activation - Essential Docs'
				&& ew.chainRecord.Name != 'Site Activation - Essential Docs')
		{
			return;
		}
		
		pawsSite.Essential_Documents_BI__c = ew.calculateBufferIndex();
		
		copyDatesData(ew, pawsSite, 'Site Activated',
			'Is_Essential_Documents_Completed__c',
			'Essential_Documents_Complete_Date__c',
			'Essential_Documents_Target_Date__c',
			'Essential_Documents_Status_Description__c');
		
		copyForecastData(ew, pawsSite, 
			SObjectType.PAWS_Project_Flow_Site__c.Fields.Essential_Documents_Expected_Date_BI_0__c.getName(),
			SObjectType.PAWS_Project_Flow_Site__c.Fields.Essential_Documents_Expected_Date_BI_05__c.getName(),
			SObjectType.PAWS_Project_Flow_Site__c.Fields.Essential_Documents_Expected_Date_BI_1__c.getName());
			
		copyAggregatedData(ew, pawsSite, 'Site Activation', SObjectType.PAWS_Project_Flow_Site__c.Fields.Essential_Documents_BI_Aggr__c.getName());
	}
	
	/**
	* Populates "Start Up Requirements" related EW and DIA data.
	*/
	private void copyStartUpData(EWChain ew, PAWS_Project_Flow_Site__c pawsSite)
	{
		if (ew.chainRecord.Cloned_From_Early_Warning__r.Name != 'Site Activation - Protocol Specific Requirements'
				&& ew.chainRecord.Name != 'Site Activation - Protocol Specific Requirements')
		{
			return;
		}
		
		pawsSite.Start_Up_Requirements_BI__c = ew.calculateBufferIndex();
		copyDatesData(ew, pawsSite, 'Site Activated',//'Protocol Specific Site Activation Requirements',
			'Is_Start_Up_Requirements_Completed__c',
			'Start_Up_Requirements_Complete_Date__c',
			'Start_Up_Requirements_Target_Date__c',
			'Start_Up_Requirements_Status_Description__c');
		
		copyForecastData(ew, pawsSite, 
			SObjectType.PAWS_Project_Flow_Site__c.Fields.Start_Up_Requirements_Expect_Date_BI_0__c.getName(),
			SObjectType.PAWS_Project_Flow_Site__c.Fields.Start_Up_Requirements_Expect_Date_BI_05__c.getName(),
			SObjectType.PAWS_Project_Flow_Site__c.Fields.Start_Up_Requirements_Expect_Date_BI_1__c.getName());
		
		copyAggregatedData(ew, pawsSite, 'Site Activation', SObjectType.PAWS_Project_Flow_Site__c.Fields.Start_Up_Requirements_BI_Aggr__c.getName());
	}
	
	private void copyForecastData(EWChain ew, PAWS_Project_Flow_Site__c pawsSite, 
									String bi0Field, String bi05Field, String bi1Field)
	{
		pawsSite.put(bi0Field, ew.calculateMilestoneDate(0)); 
		pawsSite.put(bi05Field, ew.calculateMilestoneDate(0.5)); 
		pawsSite.put(bi1Field, ew.calculateMilestoneDate(1)); 
	}
	
	/**
	* Copies Aggregated Milestone data.
	*/
	private void copyAggregatedData(EWChain ew, PAWS_Project_Flow_Site__c pawsSite, String milestoneName, String biField)
	{
		for (EW_Aggregated_Milestone_EW_Junction__c milestoneJunction : ew.chainRecord.Aggregated_Milestone_EW_Junctons__r)
		{
			if (milestoneJunction.Aggregated_Milestone__r.Name == milestoneName
					&& milestoneJunction.Aggregated_Milestone__r.Milestone_Target_Date__c != null)
			{
				pawsSite.put(biField, ew.calculateBufferIndex(milestoneJunction.Aggregated_Milestone__r.Milestone_Target_Date__c));
				break;
			}
		}
	}
	
	/*
	* Populates "Site Approvals" related EW and DIA data in PAWS_Project_Flow_Site__c object.
	*/
	private void copyApprovalsData(EWChain ew, PAWS_Project_Flow_Site__c pawsSite)
	{
		if (ew.chainRecord.Cloned_From_Early_Warning__r.Name != 'Site Activation - Submissions'
				&& ew.chainRecord.Name != 'Site Activation - Submissions')
		{
			return;
		}
		
		pawsSite.Site_Approvals_BI__c = ew.calculateBufferIndex();
		copyDatesData(ew, pawsSite, 'Site Activated',
			'Is_Site_Approvals_Completed__c',
			'Site_Approvals_Complete_Date__c',
			'Site_Approvals_Target_Date__c',
			'Site_Approvals_Status_Description__c');
		
		copyForecastData(ew, pawsSite, 
			SObjectType.PAWS_Project_Flow_Site__c.Fields.Site_Approvals_Expected_Date_BI_0__c.getName(),
			SObjectType.PAWS_Project_Flow_Site__c.Fields.Site_Approvals_Expected_Date_BI_05__c.getName(),
			SObjectType.PAWS_Project_Flow_Site__c.Fields.Site_Approvals_Expected_Date_BI_1__c.getName());
		
		copyAggregatedData(ew, pawsSite, 'Site Activation', SObjectType.PAWS_Project_Flow_Site__c.Fields.Site_Approvals_BI_Aggr__c.getName());
	}
	
	/*
	* Populates "Site Contracts" related EW and DIA data in PAWS_Project_Flow_Site__c object.
	*/
	private void copyCTAFullyExecutedData(EWChain ew, PAWS_Project_Flow_Site__c pawsSite)
	{
		if (ew.chainRecord.Cloned_From_Early_Warning__r.Name != 'Site Activation - CTA'
				&& ew.chainRecord.Name != 'Site Activation - CTA')
		{
			return;
		}
		
		pawsSite.Site_Contracts_BI__c = ew.calculateBufferIndex();
		copyDatesData(ew, pawsSite, 'Site Activated',//'CTA/Budget Fully Executed',
			'Is_Site_Contracts_Complete__c',
			'Site_Contracts_Complete_Date__c',
			'Site_Contracts_Target_Date__c',
			'Site_Contracts_Status_Description__c');
		
		copyForecastData(ew, pawsSite, 
			SObjectType.PAWS_Project_Flow_Site__c.Fields.Site_Contracts_Expected_Date_BI_0__c.getName(),
			SObjectType.PAWS_Project_Flow_Site__c.Fields.Site_Contracts_Expected_Date_BI_05__c.getName(),
			SObjectType.PAWS_Project_Flow_Site__c.Fields.Site_Contracts_Expected_Date_BI_1__c.getName());
		
		copyAggregatedData(ew, pawsSite, 'Site Activation', SObjectType.PAWS_Project_Flow_Site__c.Fields.Site_Contracts_BI_Aggr__c.getName());
	}
	
	private void copyDatesData(EWChain ew, PAWS_Project_Flow_Site__c pawsSite,
			String stepName, String completedCheckboxField, String completedDateField, String targetDateField, String commentsField)
	{
		for (EWStep step : ew.steps)
		{
			if (step.extendedStep.Name == stepName)
			{
				pawsSite.put(completedCheckboxField, (step.actualEnd != null)); 
				pawsSite.put(completedDateField, step.actualEnd); 
				pawsSite.put(targetDateField, (step.revisedEnd != null ? step.revisedEnd : step.plannedEnd));
				if (commentsField != null)
				{
					pawsSite.put(commentsField, step.ccJunction.Comment__c);
				}
				
				break;
			}
		}
	}
	
	global virtual void finish(Database.BatchableContext BC)
	{
		//do nothing
	}
}