public with sharing class CM_AgreementEditController {

    public String RecordType{ get; set; }
    public Apttus__APTS_Agreement__c agreement{ get; set; }  
    public Apttus__APTS_Agreement__c oldagreement{ get;set; }  
    public static String statuschangedate {set;get;}
    public string praStatus {get;set;}
    public Map<string,string> statusMap {get;set;}
    public List<Apttus__APTS_Agreement__c> agreementList = new List<Apttus__APTS_Agreement__c>();
    
    public CM_AgreementEditController(ApexPages.StandardController standardController){
        
        String Aid = ApexPages.CurrentPage().getParameters().get('id');        
        agreement = new Apttus__APTS_Agreement__c();
        agreement =[select id,name,Protocol_ID__c,Contract_Type__c ,PRA_Status__c,Apttus__Status__c,Apttus__Status_Category__c,Protocol_Country__c,Team__c,Protocol__c,site__c,
                           Contract_Holder__c,Comments__c,RecordTypeId, Owner.FirstName, Owner.LastName,Planned_Final_Exec_Date__c ,
                           Site__r.Project_Protocol__r.Project_ID__r.Project_Unique_Key__c,Account_Institution__c,Apttus__Agreement_Number__c,Apttus__Version_Number__c,
                           Protocol__r.Project_ID__r.Project_Unique_Key__c,Protocol__r.Project_ID__r.client_id__C,Site__r.Project_Protocol__r.Project_ID__r.client_id__C,
                           Protocol_Country__r.Client_Protocol__r.Project_ID__r.Project_Unique_Key__c,Protocol_Country__r.Client_Protocol__r.Project_ID__r.client_id__C,
                           Sponsor__c,Study_ID__c,Project_ID__c
                    from Apttus__APTS_Agreement__c 
                    where id=:Aid ];
        praStatus = agreement.PRA_Status__c;
        RecordType  =  [SELECT Id,name from RecordType WHERE SObjectType = 'Apttus__APTS_Agreement__c' and id=: agreement.RecordTypeId].Name;
        statusMapData();
        system.debug('--------agreement ------'+agreement+RecordType  );
    }   
    
    
    
    
    //Method to insert status with their field api names in statusMap
    public void statusMapData(){
        statusMap = new Map<string,string>();
        statusMap.put('Template Sent to Site','Template_Sent_Date__c');
        statusMap.put('Negotiation Complete','Neg_Complete_Date__c');
        statusMap.put('Partially Executed','Partially_Exec_Date__c');
        
    }
    
   //Method to set value for statuschangedate
    public static void statusc(){
            statuschangedate = '04/26/2015';
    }
    
    //Method to convert string to Date format 
    public Date dateFormat(String statusdate){
                String[] dateOnly = statusdate.split(' ');
                String[] changeDate = dateOnly[0].split('/');
                Integer myIntDate = integer.valueOf(changeDate[1]);
                Integer myIntMonth = integer.valueOf(changeDate[0]);
                Integer myIntYear = integer.valueOf(changeDate[2]);
                return Date.newInstance(myIntYear, myIntMonth, myIntDate);
                
                
          }
    
    //Method to save agreement   
    public PageReference saveAgreement(){   
        try{ 
            if(agreement.Protocol__c!=null){
                agreement.Name = agreement.Protocol__r.Project_ID__r.client_id__C+ '_' + agreement.Project_ID__c + '_'+ agreement.Contract_Type__c;            
           
            }else if(agreement.Protocol_Country__c!=null){
                agreement.Name = agreement.Protocol_Country__r.Client_Protocol__r.Project_ID__r.client_id__C+ '_' + agreement.Project_ID__c + '_'+ agreement.Contract_Type__c ;            
           
            }else if(agreement.site__c!=null){
                agreement.Name = agreement.Site__r.Project_Protocol__r.Project_ID__r.client_id__C+ '_' + agreement.Project_ID__c +'_'+ agreement.Contract_Type__c ;            
           
            }
            //agreement.Name = agreement.Protocol_Country__r.Client_Protocol__r.Project_ID__r.client_id__C+ '_' + agreement.Project_ID__c + '_'+ agreement.Contract_Type__c + '_'+RecordType;             
            agreementList.add(agreement);
            if(agreement.Apttus__Status__c == 'In Amendment'){
                oldagreement = [select id,Apttus__Status__c from Apttus__APTS_Agreement__c where Apttus__Agreement_Number__c  = :agreement.Apttus__Agreement_Number__c  and Apttus__Version_Number__c = :agreement.Apttus__Version_Number__c-1];
                oldagreement.Apttus__Status__c = 'Being Amended';
                agreementList.add(oldagreement);
            }
            //checking the status is in status map,if so check the statuschangedate is null then update status date with NOW() or else update with status change date
            if(statuschangedate!='' && statusMap.get(agreement.PRA_Status__c )!=null)
                agreement.put(statusMap.get(agreement.PRA_Status__c ),dateFormat(statuschangedate));
            else if(praStatus!=agreement.PRA_Status__c && statusMap.get(agreement.PRA_Status__c )!=null)
                agreement.put(statusMap.get(agreement.PRA_Status__c ),system.today());
            upsert agreementList;
            system.debug('--------agreement ------'+agreement.Protocol_Country__r.Client_Protocol__r.Project_ID__r.client_id__C);
            String PID=  ((agreement.Protocol__c!= null)?(agreement.Protocol__c):((agreement.Protocol_Country__c!= null)?(agreement.Protocol_Country__c):((agreement.site__c!=null)?(agreement.site__c):null)));
            PageReference pageRef=new PageReference('/'+agreement.id);
            pageRef.setRedirect(true);
            return pageRef; 
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
    }   
    
}