@isTest
private class PBB_ServiceAreaTest {
	
	private static final String TEST_VALUE = 'TEST_VALUE';
		
	private static PBB_ServiceArea sa;
	
	private static List<Service_Area__c> serviceAreas;
	private static PBB_TestUtils testUtils = new PBB_TestUtils();
	
	@isTest
	private static void testConstructor(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		Test.startTest();
			sa = new PBB_ServiceArea(testUtils.smm.Id);
		Test.stopTest();
		System.assertEquals( testUtils.smm.Id, sa.parentServiceId );
		System.assertEquals( testUtils.smm.Id, sa.previousServiceId );
	}
	
	@isTest
	private static void testConstructor2(){
		Test.startTest();
			sa = new PBB_ServiceArea();
		Test.stopTest();
	}
	
	@isTest
	private static void testProperties(){
		sa = new PBB_ServiceArea();
		Test.startTest();
			sa.serviceAreas = new List<Service_Area__c>{ new Service_Area__c() };
			sa.serviceArea = new Service_Area__c();
			sa.serviceAreaForUpdate = new Service_Area__c();
		Test.stopTest();
		System.assertEquals( PBB_ServiceArea.AREA_WORD, sa.getServiceWord() );
		System.assertEquals( 1, sa.serviceAreas.size() );
		System.assertNotEquals( null, sa.serviceArea );
		System.assertNotEquals( null, sa.serviceAreaForUpdate );
	}
	
	@isTest
	private static void refreshServicesTest(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		sa = new PBB_ServiceArea(testUtils.smm.Id);
		testUtils.CreateServiceAreaAttributes();
		Test.startTest();
			sa.refreshServices();
		Test.stopTest();
		System.assertEquals( 1, sa.serviceAreas.size() );
	}
	
	@isTest
	private static void preparationCreateOrEditServiceTestCreateNew(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		Test.setCurrentPage( Page.PBB_LayoutModel );
		sa = new PBB_ServiceArea(testUtils.smm.Id);
		Test.startTest();
			sa.preparationCreateOrEditService();
		Test.stopTest();
		System.assertEquals( null, sa.serviceAreaForUpdate.Id );
		System.assertEquals( testUtils.smm.Id, sa.serviceAreaForUpdate.Service_Model__c );
	}

	@isTest
	private static void preparationCreateOrEditServiceTestUpdate(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		sa = new PBB_ServiceArea(testUtils.smm.Id);
		sa.serviceArea = testUtils.CreateServiceAreaAttributes();
		sa.refreshServices();
		Test.setCurrentPage( Page.PBB_LayoutModel );
		ApexPages.currentPage().getParameters().put( 'editServiceAreaId', sa.serviceArea.Id );
		Test.startTest();
			sa.preparationCreateOrEditService();
		Test.stopTest();
		System.assertEquals( sa.serviceArea.Id, sa.serviceAreaForUpdate.Id );
	}
	
	@isTest
	private static void preparationCreateOrEditServiceTestClone(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		sa = new PBB_ServiceArea(testUtils.smm.Id);
		sa.serviceArea = testUtils.CreateServiceAreaAttributes();
		sa.serviceArea.Name = TEST_VALUE;
		Test.setCurrentPage( Page.PBB_LayoutModel );
		ApexPages.currentPage().getParameters().put( 'cloneServiceAreaId', sa.serviceArea.Id );
		Test.startTest();
			sa.preparationCreateOrEditService();
		Test.stopTest();
		System.assertEquals( null, sa.serviceAreaForUpdate.Id );
		System.assertEquals( TEST_VALUE, sa.serviceAreaForUpdate.Name );
	}
	
	@isTest
	private static void removeServiceTest(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		sa = new PBB_ServiceArea(testUtils.smm.Id);
		sa.serviceArea = testUtils.CreateServiceAreaAttributes();
		Test.startTest();
			sa.removeService();
		Test.stopTest();
		System.assertEquals( 0, sa.serviceAreas.size() );
	}
	
	@isTest
	private static void removeServiceTestNullPointer(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		sa = new PBB_ServiceArea(testUtils.smm.Id);
		Test.setCurrentPage( Page.PBB_LayoutModel );
		Test.startTest();
			sa.removeService();
		Test.stopTest();
		System.assertEquals( 1, ApexPages.getMessages().size() );
	}
		
}