public with sharing class PAWS_FlowStepTrigger extends STSWR1.AbstractTrigger  
{
	public override void beforeDelete(List<SObject> records)
	{
		updateGanttProperties((List<STSWR1__Flow_Step_Junction__c>)records);
	}
	
	private void updateGanttProperties(List<STSWR1__Flow_Step_Junction__c> records)
	{
		PAWS_Utilities.checkLimit('Queries', 1);
		List<STSWR1__Flow_Step_Action__c> actions = [select STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Flow_Step__c in :records and STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active'];
		new PAWS_FlowStepActionTrigger().afterDelete(actions);
	}
}