@istest private class PAWS_FlowInstanceHistoryTriggerTest
{
	@istest private static void coverTrigger()
	{
		PAWS_ApexTestsEnvironment.init();
		STSWR1__Flow_Instance_Cursor__c cursor = (STSWR1__Flow_Instance_Cursor__c)new STSWR1.API().call('WorkflowService', 'getFlowInstanceCursorById', PAWS_ApexTestsEnvironment.FlowInstanceCursor.Id);
		new PAWS_CallApexWrapper().run(PAWS_ApexTestsEnvironment.Project, cursor, 'Proceed', 'CompleteParentStep');
		
		PAWS_FlowInstanceHistoryTrigger testee = new PAWS_FlowInstanceHistoryTrigger();
		testee.afterInsert(new List<SObject>{PAWS_ApexTestsEnvironment.FlowInstanceHistory});
		testee.afterUpdate(new List<SObject>{PAWS_ApexTestsEnvironment.FlowInstanceHistory}, new List<SObject>{PAWS_ApexTestsEnvironment.FlowInstanceHistory});
	}
	
	@istest private static void testOnError()
	{
		PAWS_FlowInstanceHistoryTrigger testee = new PAWS_FlowInstanceHistoryTrigger();
		testee.onError(new List<SObject>{new STSWR1__Flow_Instance_History__c()}, 'Test!');
	}
}