/**
@author Bhargav Devaram
@date 2015
@description this is PBB_ActiveQuestionsforServiceTasksCntrl test class
**/
@isTest
private class PBB_ActiveQuestionsforServiceCntrlTest{
    static testMethod void testPBB_ActiveQuestionsforServiceTasksCntrl(){
        test.startTest();   
            
            PBB_TestUtils tu = new PBB_TestUtils ();
            Apexpages.currentPage().getParameters().put('lksrch', '123');
            Apexpages.currentPage().getParameters().put('Global', 'true');
            
            //initiate the constructor.
            PBB_ActiveQuestionsforServiceTasksCntrl aq= new PBB_ActiveQuestionsforServiceTasksCntrl();            
            System.assertEquals(System.currentPageReference().getParameters().get('Global'),'true');
            System.assertEquals(System.currentPageReference().getParameters().get('lksrch'),'123');
            
            //check the tags are there
            string ftag=aq.getFormTag();
            string TBtag=aq.getTextBox();
            System.assertEquals(ftag,null);
            System.assertEquals(TBtag,null);        
        test.stopTest();
    }
}