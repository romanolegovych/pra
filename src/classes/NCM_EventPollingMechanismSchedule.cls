/**
 * @description	The class for scheduling the event polling mechanism
 * @author		Dimitrios Sgourdos
 * @date		Created: 24-Sep-2015, Edited: 01-Oct-2015
 */
global class NCM_EventPollingMechanismSchedule implements Schedulable {
	
	// Cron string for every 1 hour
	public static String cronSched = '0 0 * * * ?';
	
	/**
	* @description The constructor of the class
	* @author      Dimitrios Sgourdos
	* @date        Created: 24-Sep-2015, Edited: 01-Oct-2015
	**/
	global static String scheduleMe() {
		return System.schedule(	NCM_API_DataTypes.BATCH_JOB_FOR_POLLING_TOPICS,
								cronSched,
								new NCM_EventPollingMechanismSchedule());
	}
	
	/**
	* @description Execute the Event Polling Mechanism Batch
	* @author      Dimitrios Sgourdos
	* @date        Created: 25-Sep-2015
	**/
	global void execute(SchedulableContext sc) {
		Database.executeBatch(new NCM_EventPollingMechanismBatch(), 50);
	}
}