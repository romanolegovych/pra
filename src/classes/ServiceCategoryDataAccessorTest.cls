/** Implements the test for the Selector Layer of the object ServiceCategory__c
 * @author	Dimitrios Sgourdos
 * @version	10-Dec-2013
 */
@isTest
private class ServiceCategoryDataAccessorTest {
	
	/** Test the function getFirstLevelServiceCategories
	 * @author	Dimitrios Sgourdos
	 * @version	10-Dec-2013
	 */
	static testMethod void getFirstLevelServiceCategoriesTest() {
		// Create dummy data
		List<ServiceCategory__c> initialList = new List<ServiceCategory__c>();
		initialList.add(new ServiceCategory__c(Code__c='002',name='Service Category (2)'));
		initialList.add( new ServiceCategory__c(Code__c='001',name='Service Category (1)') );
		insert initialList;
		
		insert ( new ServiceCategory__c(code__c='001.001',name='Service Category (1.1)') );
		
		// Check function
		List<ServiceCategory__c> results = ServiceCategoryDataAccessor.getFirstLevelServiceCategories();
		system.assertEquals(results.size(), 2, 'Error in retrieving first level Service Categories from the system');
		system.assertEquals(results[0].Code__c, initialList[1].Code__c, 'Error in the order of retrieved Service Categories');
		system.assertEquals(results[1].Code__c, initialList[0].Code__c, 'Error in the order of retrieved Service Categories');
	}
}