/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_EmployeeDetailsControllerUTest {

    static list<Country__c> lstCountry;	
    
    static list<Employee_Details__c> lstEmpl; 
    
    
    static RM_UnitTestData init(){
    	RM_UnitTestData testData = new RM_UnitTestData();
    	lstCountry = testData.lstCountry;
    	lstEmpl = testData.lstEmpl;
    
    	return testData;
    }
    static testMethod void TestController() {
    	User usr = [Select id from User where Id = :UserInfo.getUserId()];
    	RM_UnitTestData testData = init();
        System.RunAs(Usr)
        {
	         Test.startTest();
	         myFunc2();
        
       
       
	        PageReference pageGroup = new PageReference('/apex/RM_EmployeeDetail?id='+lstEmpl[0].id);
	        Test.setCurrentPage(pageGroup);
	        RM_EmployeeDetailsController detail = new RM_EmployeeDetailsController();   
	        system.assert(detail.empl.id ==  lstEmpl[0].id);     
	        Test.stopTest(); 
        }
    }
     private static void myFunc2()
    {  Group g1 = new Group(name = 'RM_Resource_group',DeveloperName='RM_Resource_group');        
        insert g1;
        GroupMember gm1 = new GroupMember();
        gm1.GroupId=g1.id;
        gm1.UserOrGroupId=UserInfo.getUserId();
        database.insert(gm1); 
        system.debug('----g1-----'+gm1);
        
        PRA_Business_Unit__c PBU = new PRA_Business_Unit__c(name = 'Clinical Informatics',Business_Unit_Code__c='CI');        
        insert PBU; 
        
        WFM_BU_Group_Mapping__c bug=new WFM_BU_Group_Mapping__c();
        bug.PRA_Business_Unit__c=pbu.id;
        bug.Group_Name__c='RM_Resource_group';
        insert bug;
        
    }
}