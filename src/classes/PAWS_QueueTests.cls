/**
*   'PAWS_QueueTests' is the test class for PAWS_Queue and PAWS_Scheduler
*   Test classes are used to write the unit test and cover the code coverage of the code.
*   @author   Work Relay
*/
@isTest 
public with sharing class PAWS_QueueTests 
{
	static testmethod void forScheduler()
	{
		PAWS_ApexTestsEnvironment.init();
		
		Test.startTest();
		
		PAWS_Queue.addTask('Activate Flow', JSON.serialize(new Map<String, String>{'sourceId' => null, 'templateId' => null}));
		PAWS_Queue.addTasks(new List<Map<String, String>>{
			new Map<String, String>{
				'Type__c' => 'Activate Flow',
				'Data__c' => JSON.serialize(new Map<String, String>{'sourceId' => null, 'templateId' => null})
			}
		});
		
		List<PAWS_Queue__c> linkedStepProceed = PAWS_Queue.addTasks(new List<Map<String, String>>{new Map<String, String>{'Type__c' => 'Linked Step Proceed', 'Data__c' => '{}'}});
		List<PAWS_Queue__c> linkedStepSyncDates = PAWS_Queue.addTasks(new List<Map<String, String>>{new Map<String, String>{'Type__c' => 'Linked Step Sync Dates', 'Data__c' => '{}'}});
		
		PAWS_Queue q = new PAWS_Queue();
		q.execute(null, linkedStepProceed);
		q.execute(null, linkedStepSyncDates);
		
    	
    	new PAWS_Scheduler('PAWS_Queue').execute(null);
    	Test.stopTest();
    }
	
	/**
	* This method tries to recreate "Operation Too Complicated" exception
	*/
	@istest(seealldata=true) private static void testResourcesError()
	{
		System.runAs([select Name from User where LastName = 'Mauer' and FirstName = 'Russell'].get(0))
		{
			List<sObject> records = [select Id, Name from STSWR1__Form__c Order By Name];
			records = [select Id from Attachment limit 1];
			records = [select Id, Name, NamespacePrefix from ApexPage Order By NamespacePrefix, Name limit 500];
			records = [select Name From Group Where Type = 'Regular' Order By Name limit 500];
			records = [select Name From CollaborationGroup Order By Name limit 500];
			records = [select Name From UserRole Order By Name limit 500];
			records = [select Name From Profile Order By Name limit 500];
			records = [select Id, Name, Profile.Name from User where IsActive = true Order By Name];
		}
	}
	
	
	/**
	* This method tries to check whether a limit statement will fix "Operation Too Complicated" exception
	*/
	@istest(seealldata=true) private static void testResourcesLimitFix()
	{
		System.runAs([select Name from User where LastName = 'Mauer' and FirstName = 'Russell'].get(0))
		{
			List<sObject> records = [select Id, Name from STSWR1__Form__c Order By Name limit 500];
			records = [select Id, Name, NamespacePrefix from ApexPage Order By NamespacePrefix, Name limit 500];
			records = [select Name From Group Where Type = 'Regular' Order By Name limit 500];
			records = [select Name From CollaborationGroup Order By Name limit 500];
			records = [select Name From UserRole Order By Name limit 500];
			records = [select Name From Profile Order By Name limit 500];
			records = [select Id, Name, Profile.Name from User where IsActive = true Order By Name limit 500];
		}
	}
}