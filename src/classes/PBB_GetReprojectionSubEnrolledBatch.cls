/**
 * @author Ramya
 * @Date 2015
 * @Description This class batch job that is scheduled weekly for reprojecting subject enrollement.
 */
global class PBB_GetReprojectionSubEnrolledBatch extends PRA_BatchaQueueable {

    private String oName ;
    private Boolean serialExecution;
    private String query;
  
    global PBB_GetReprojectionSubEnrolledBatch (){
    }
  
    global override void setBatchParameters(String parametersJSON){
    
        // set paramters from Json string
        List<String> params = (List<String>)Json.deserialize(parametersJSON, List<String>.class);
        oName = params.get(0);
    }
  
    global override Database.QueryLocator start(Database.BatchableContext BC){
        
        List<AggregateResult> CountprotocolList = new List<AggregateResult>();
        List<Id> ProtocolIdlist = new List<Id>();
    
        CountprotocolList = PBB_DataAccessor.getCountryweeklyeventProtocoldata();
        system.debug('-----CountprotocolList ---'+CountprotocolList);
        for(AggregateResult pro:CountprotocolList){
            ProtocolIdlist.add(String.valueOf(pro.get('protocolId')));
        }
        system.debug('-----ProtocolIdlist---'+ProtocolIdlist);
            
        query = 'select Id, Name, Protocal_Unique_Key__c, Expected_Patients__c '  +
                'from ' + oName + ' where  Status__c = \'In Progress\' and Id in:ProtocolIdlist';
     
        if (Test.isRunningTest()){
            query += ' Limit 200';
        }
        System.debug('query' + ' | ' + oName + ' | ');
        return Database.getQueryLocator(query);
    }
  
    global override void execute(Database.BatchableContext BC, List<Sobject> scope){
  
        List<WFM_Protocol__c> Protocollist = (List<WFM_Protocol__c>) scope;
        system.debug('-----Protocollist ---'+Protocollist+Protocollist[0]);
        PBB_Services pbb_services = new PBB_Services();
        pbb_services.reProjectWeeklyEvents(Protocollist[0]);
                                                 
    }
  
    global override  void finish(Database.BatchableContext BC){
    }  
}