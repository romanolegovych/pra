@isTest
private class BDT_NewEditStudySiteTest {
	public static Client_Project__c firstProject {get;set;}
	public static List<Study__c> stydiesList {get;set;}
	public static List<Business_Unit__c> myBUList  {get;set;}
	public static List<Site__c> mySiteList  {get;set;}
	
	static void init(){
		firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;
		
		myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert myBUList;
		
		mySiteList = BDT_TestDataUtils.buildSite(myBUList);
		insert mySiteList;
		
		stydiesList = BDT_TestDataUtils.buildStudies(firstProject);
		stydiesList[0].Business_Unit__c = myBUList[0].Id; 
		stydiesList[1].Business_Unit__c = myBUList[1].Id; 
		stydiesList[2].Business_Unit__c = myBUList[2].Id;
		stydiesList[3].Business_Unit__c = myBUList[3].Id; 
		stydiesList[4].Business_Unit__c = myBUList[4].Id;
		insert stydiesList;
		
		BDT_Utils.setPreference('SelectedProject', firstProject.id);
		BDT_Utils.setPreference('SelectedStudies',stydiesList[0].id);
		
	}
	
	static testMethod void renderSitePath() { 
		init();
									
		BDT_NewEditStudySite a = new BDT_NewEditStudySite();
		
		a.bu = myBUList[0].Id;
		
		a.buildBUOptions();
		a.buildSiteSelector();
		
		system.assertNotEquals(null, a.save() );
		system.assertNotEquals(null, a.cancel() );
		
	}

}