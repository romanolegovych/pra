public with sharing class IPA_DAL_DynamicPage
{
    public List<IPA_Page_Widget__c> returnWidgets(String pageId, String displayColumn)
    {
        List<IPA_Page_Widget__c> lst = new List<IPA_Page_Widget__c>();
        try
        {
            lst = [select Id, Widget__r.VisualForce_Component_Name__c, Weight__c, Page__r.Category__c, 
                        Page__r.Category__r.Name, Parent_Tab_Group_Widget__c
                        from IPA_Page_Widget__c
                        where Page__r.Id = : pageId and Display_Column__c =:displayColumn and Active__c = true
                            and Widget__r.Active__c = true and Parent_Tab_Group_Widget__c = null
                            order by Weight__c];
        }catch(Exception e) {}
        return lst;
    }
    
    public List<IPA_Page_Widget__c> returnChildWidgets(IPA_Page_Widget__c parent)
    {
        List<IPA_Page_Widget__c> lst = new List<IPA_Page_Widget__c>();
        try
        {
            lst = [select Id, Weight__c, Page__r.Category__c, Page__r.Category__r.Name, Display_Label__c, Widget__c, 
                        Widget__r.VisualForce_Component_Name__c 
                        from IPA_Page_Widget__c
                        where Active__c = true and Parent_Tab_Group_Widget__c =: parent.Id 
                        order by Weight__c Limit 5];
        }catch(Exception e) {}
        return lst;
    }
}