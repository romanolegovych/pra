@isTest
private class EpochDataAccessorTest {

    static testMethod void myUnitTest() {
        // create test data
        ClinicalDesign__c cd = new ClinicalDesign__c(name='1');
        insert cd;
        
        list<Epoch__c> eList = new list<Epoch__c>();
        eList.add(new Epoch__c(ClinicalDesign__c = cd.id, Epoch_Number__c = 1, Description__c = 'Arm 1'));
        eList.add(new Epoch__c(ClinicalDesign__c = cd.id, Epoch_Number__c = 2, Description__c = 'Arm 2'));
        eList.add(new Epoch__c(ClinicalDesign__c = cd.id, Epoch_Number__c = 3, Description__c = 'Arm 3'));
        insert eList;
        
        //test retrieve logic
        eList = EpochDataAccessor.getEpochList('ClinicalDesign__c = \'' + cd.id + '\'');
        system.assertEquals(3,eList.size());
    }
}