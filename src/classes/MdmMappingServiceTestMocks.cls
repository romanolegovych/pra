/**
 * Mocks for MDM Mapping Service classes
 */
@isTest
public class MdmMappingServiceTestMocks implements WebServiceMock {
	
	public boolean shouldCreateError {get;set;}
	
	private static final String GET_ALL_INSTANCES_TYPE = 'MdmMappingService.getAllInstancesResponse';
	private static final String GET_ATTRIB_KEY_VALUES_TYPE = 'MdmMappingService.getAttributeKeyValuesResponse';
	private static final String GET_CTMS_INSTANCES_TYPE = 'MdmMappingService.getCTMSInstancesResponse';
	private static final String GET_ETMF_INSTANCES_TYPE = 'MdmMappingService.getETMFInstancesResponse';
	private static final String GET_ETMF_SITE_VOS_TYPE = 'MdmMappingService.getEtmfSiteVOsForProtocolMappingResponse';
	private static final String GET_INSTANCE_LIKE_NAME_TYPE = 'MdmMappingService.getInstancesLikeNameResponse';
	private static final String GET_PROT_SITE_VOS_TYPE = 'MdmMappingService.getProtocolSiteVOsForProtocolMappingResponse';
	private static final String SAVE_INSTANCE_TYPE = 'MdmMappingService.saveInstanceFromVOResponse';
	private static final String DELETE_INSTANCE_TYPE = 'MdmMappingService.deleteInstanceFromVOResponse';
	private static final String SAVE_CRS_INST_CP_TYPE = 'MdmMappingService.saveCrsInstClientProjFromProtSiteVOResponse';
	private static final String DELETE_CRS_INST_CP_TYPE = 'MdmMappingService.deleteCrsInstClientProjFromProtSiteVOResponse';
	private static final String SAVE_CRS_INST_PROT_TYPE = 'MdmMappingService.saveCrsInstProtFromSiteVOResponse';
	private static final String DELETE_CRS_INST_PROT_TYPE = 'MdmMappingService.deleteCrsInstProtFromSiteVOResponse';
	
	public MdmMappingServiceTestMocks(boolean doError) {
		this.shouldCreateError = doError;
		system.debug('----------- Mocking a MdmMappingService method request -----------------');
	}
	
	public void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
		String requestName, String responseNS, String responseName, String responseType) {
		if (responseType.equals(GET_ALL_INSTANCES_TYPE)) {
			response.put('response_x', createMockGetAllInstancesResponse());
		} else if(responseType.equals(GET_ATTRIB_KEY_VALUES_TYPE)) {
			response.put('response_x', createMockGetAttribKeyValuesResponse());
		} else if(responseType.equals(GET_CTMS_INSTANCES_TYPE)) {
			response.put('response_x', createMockGetCTMSInstancesResponse());
		} else if(responseType.equals(GET_ETMF_INSTANCES_TYPE)) {
			response.put('response_x', createMockGetETMFInstancesResponse());
		} else if(responseType.equals(GET_ETMF_SITE_VOS_TYPE)) {
			response.put('response_x', createMockGetEtmfSiteVOsResponse());
		} else if(responseType.equals(GET_INSTANCE_LIKE_NAME_TYPE)) {
			response.put('response_x', createMockGetInstancesLikeNameResponse());
		} else if(responseType.equals(GET_PROT_SITE_VOS_TYPE)) {
			response.put('response_x', createMockGetProtocolSiteVOsResponse());
		} else if(responseType.equals(SAVE_INSTANCE_TYPE)) {
			response.put('response_x', createMockSaveInstanceFromVOResponse());
		} else if(responseType.equals(DELETE_INSTANCE_TYPE)) {
			response.put('response_x', createMockDeleteInstanceFromVOResponse());
		} else if(responseType.equals(SAVE_CRS_INST_CP_TYPE)) {
			response.put('response_x', createMockSaveCrsInstClientProjFromVOResponse());
		} else if(responseType.equals(DELETE_CRS_INST_CP_TYPE)) {
			response.put('response_x', createMockDeleteCrsInstClientProjFromVOResponse());
		} else if(responseType.equals(SAVE_CRS_INST_PROT_TYPE)) {
			response.put('response_x', createMockSaveCrsInstProtFromVOResponse());
		} else if(responseType.equals(DELETE_CRS_INST_PROT_TYPE)) {
			response.put('response_x', createMockDeleteCrsInstProtFromVOResponse());
		}
		system.debug('----------------------------request------------------------' + request);
		system.debug('----------------------------response------------------------' + response);
	}
	
	/********************* Methods to build main response objects ********************************/
	
	private MdmMappingService.getAllInstancesResponse createMockGetAllInstancesResponse() {
		MdmMappingService.getAllInstancesResponse resp = new MdmMappingService.getAllInstancesResponse();
		MdmMappingService.valueListResposne valueListResp = new MdmMappingService.valueListResposne();
		valueListResp.errors = null;
		valueListResp.statusCode = '0';
		valueListResp.values = new String[]{'value1','value2','value3'};
		resp.GetAllInstancesResponse = valueListResp;
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		return resp;
	}
	
	private MdmMappingService.getAttributeKeyValuesResponse createMockGetAttribKeyValuesResponse() {
		MdmMappingService.getAttributeKeyValuesResponse resp = new MdmMappingService.getAttributeKeyValuesResponse();
		MdmMappingService.valueListResposne valueListResp = new MdmMappingService.valueListResposne();
		valueListResp.errors = null;
		valueListResp.statusCode = '0';
		valueListResp.values = new String[]{'value1','value2','value3'};
		resp.GetAttributeKeysResponse = valueListResp;
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		return resp;
	}
	
	private MdmMappingService.getCTMSInstancesResponse createMockGetCTMSInstancesResponse() {
		MdmMappingService.getCTMSInstancesResponse resp = new MdmMappingService.getCTMSInstancesResponse();
		MdmMappingService.valueListResposne valueListResp = new MdmMappingService.valueListResposne();
		valueListResp.errors = null;
		valueListResp.statusCode = '0';
		valueListResp.values = new String[]{'value1','value2','value3'};
		resp.GetCTMSInstancesResponse = valueListResp;
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		return resp;
	}
	
	private MdmMappingService.getETMFInstancesResponse createMockGetETMFInstancesResponse() {
		MdmMappingService.getETMFInstancesResponse resp = new MdmMappingService.getETMFInstancesResponse();
		MdmMappingService.valueListResposne valueListResp = new MdmMappingService.valueListResposne();
		valueListResp.errors = null;
		valueListResp.statusCode = '0';
		valueListResp.values = new String[]{'value1','value2','value3'};
		resp.GetETMFInstancesResponse = valueListResp;
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		return resp;
	}
	
	private MdmMappingService.getEtmfSiteVOsForProtocolMappingResponse createMockGetEtmfSiteVOsResponse() {
		MdmMappingService.getEtmfSiteVOsForProtocolMappingResponse resp = new MdmMappingService.getEtmfSiteVOsForProtocolMappingResponse();
		MdmMappingService.etmfSiteListResponse etmfSiteListResp = new MdmMappingService.etmfSiteListResponse();
		etmfSiteListResp.errors = null;
		etmfSiteListResp.statusCode = '0';
		etmfSiteListResp.etmfSiteVOs = createEtmfSiteVOList(5);
		resp.GetEtmfSiteVOsForProtocolMappingResponse = etmfSiteListResp;
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		return resp;
	}
	
	private MdmMappingService.getInstancesLikeNameResponse createMockGetInstancesLikeNameResponse() {
		MdmMappingService.getInstancesLikeNameResponse resp = new MdmMappingService.getInstancesLikeNameResponse();
		MdmMappingService.instanceListResponse instanceListResp = new MdmMappingService.instanceListResponse();
		instanceListResp.errors = null;
		instanceListResp.statusCode = '0';
		instanceListResp.instanceVOs = createInstanceVOList(5);
		resp.GetInstancesLikeNameResponse = instanceListResp;
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		return resp;
	}
	
	private MdmMappingService.getProtocolSiteVOsForProtocolMappingResponse createMockGetProtocolSiteVOsResponse() {
		MdmMappingService.getProtocolSiteVOsForProtocolMappingResponse resp = new MdmMappingService.getProtocolSiteVOsForProtocolMappingResponse();
		MdmMappingService.protocolSiteListResponse protocolSiteListResp = new MdmMappingService.protocolSiteListResponse();
		protocolSiteListResp.errors = null;
		protocolSiteListResp.statusCode = '0';
		protocolSiteListResp.protocolSiteVOs = createProtocolSiteVOList(5);
		resp.GetProtocolSiteVOsForProtocolMappingResponse = protocolSiteListResp;
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		return resp;
	}
	
	private MdmMappingService.saveInstanceFromVOResponse createMockSaveInstanceFromVOResponse() {
		MdmMappingService.saveInstanceFromVOResponse resp = new MdmMappingService.saveInstanceFromVOResponse();
		MdmMappingService.mdmCrudResponse mdmCrudResp = new MdmMappingService.mdmCrudResponse();
		mdmCrudResp.errors = null;
		mdmCrudResp.actionType = 'I';
		mdmCrudResp.operationStatus= '0';
		resp.SaveInstanceFromVOResponse = mdmCrudResp;
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		return resp;
	}
	
	private MdmMappingService.deleteInstanceFromVOResponse createMockDeleteInstanceFromVOResponse() {
		MdmMappingService.deleteInstanceFromVOResponse resp = new MdmMappingService.deleteInstanceFromVOResponse();
		MdmMappingService.mdmCrudResponse mdmCrudResp = new MdmMappingService.mdmCrudResponse();
		mdmCrudResp.errors = null;
		mdmCrudResp.actionType = 'D';
		mdmCrudResp.operationStatus= '0';
		resp.DeleteInstanceFromVOResponse = mdmCrudResp;
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		return resp;
	}
	
	private MdmMappingService.saveCrsInstClientProjFromProtSiteVOResponse createMockSaveCrsInstClientProjFromVOResponse() {
		MdmMappingService.saveCrsInstClientProjFromProtSiteVOResponse resp = new MdmMappingService.saveCrsInstClientProjFromProtSiteVOResponse();
		MdmMappingService.mdmCrudResponse mdmCrudResp = new MdmMappingService.mdmCrudResponse();
		mdmCrudResp.errors = null;
		mdmCrudResp.actionType = 'I';
		mdmCrudResp.operationStatus= '0';
		resp.SaveProtocolSiteFromVOResponse = mdmCrudResp;
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		return resp;
	}
	
	private MdmMappingService.deleteCrsInstClientProjFromProtSiteVOResponse createMockDeleteCrsInstClientProjFromVOResponse() {
		MdmMappingService.deleteCrsInstClientProjFromProtSiteVOResponse resp = new MdmMappingService.deleteCrsInstClientProjFromProtSiteVOResponse();
		MdmMappingService.mdmCrudResponse mdmCrudResp = new MdmMappingService.mdmCrudResponse();
		mdmCrudResp.errors = null;
		mdmCrudResp.actionType = 'D';
		mdmCrudResp.operationStatus= '0';
		resp.DeleteProtocolSiteFromVOResponse = mdmCrudResp;
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		return resp;
	}
	
	private MdmMappingService.saveCrsInstProtFromSiteVOResponse createMockSaveCrsInstProtFromVOResponse() {
		MdmMappingService.saveCrsInstProtFromSiteVOResponse resp = new MdmMappingService.saveCrsInstProtFromSiteVOResponse();
		MdmMappingService.mdmCrudResponse mdmCrudResp = new MdmMappingService.mdmCrudResponse();
		mdmCrudResp.errors = null;
		mdmCrudResp.actionType = 'I';
		mdmCrudResp.operationStatus= '0';
		resp.SaveEtmfProtocolMappingFromVOResponse = mdmCrudResp;
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		return resp;
	}
	
	private MdmMappingService.deleteCrsInstProtFromSiteVOResponse createMockDeleteCrsInstProtFromVOResponse() {
		MdmMappingService.deleteCrsInstProtFromSiteVOResponse resp = new MdmMappingService.deleteCrsInstProtFromSiteVOResponse();
		MdmMappingService.mdmCrudResponse mdmCrudResp = new MdmMappingService.mdmCrudResponse();
		mdmCrudResp.errors = null;
		mdmCrudResp.actionType = 'D';
		mdmCrudResp.operationStatus= '0';
		resp.DeleteEtmfProtocolMappingFromVOResponse = mdmCrudResp;
		if (shouldCreateError) {
			Integer i = Integer.valueOf('s');
		}
		return resp;
	}
	
	/********************** Methods to build objects in responses ******************************/
	
	private MdmMappingService.etmfSiteVO createEtmfSiteVO(Integer index) {
		MdmMappingService.etmfSiteVO etmfSiteVO = new MdmMappingService.etmfSiteVO();
		etmfSiteVO.ctmsSiteName = 'ctms site ' + index;
		etmfSiteVO.etmfSiteName = 'etmf site ' + index;
		etmfSiteVO.protocolId = index;
		etmfSiteVO.protocolNo = 'MOCK PROT NO';
		etmfSiteVO.protocolSystemId = 'MOCK PROT SYSID ' + index;
		etmfSiteVO.siteId = index;
		etmfSiteVO.source = 'MOCK TEST CLASS';
		return etmfSiteVO;
	}
	
	private list<MdmMappingService.etmfSiteVO> createEtmfSiteVOList(Integer listSize) {
		list<MdmMappingService.etmfSiteVO> etmfSiteVOs = new list<MdmMappingService.etmfSiteVO>();
		for (Integer i = 1; i < listSize + 1; i++) {
			etmfSiteVOs.add(createEtmfSiteVO(i));
		}
		return etmfSiteVOs;
	}
	
	private MdmMappingService.instanceVO createInstanceVO(Integer index) {
		MdmMappingService.instanceVO instanceVO = new MdmMappingService.instanceVO();
		instanceVO.instanceId = index;
		instanceVO.instanceName = 'MOCK INSTANCE ' + index;
		instanceVO.praSystem = 'Y';
		instanceVO.siteType = 'CTMS';
		instanceVO.source = 'MOCK TEST CLASS';
		instanceVO.createdBy = 'TEST';
		instanceVO.createdDate = Datetime.newInstance(0);
		instanceVO.updatedBy = 'TEST';
		instanceVO.updatedDate = Datetime.newInstance(0);
		return instanceVO;
	}
	
	private list<MdmMappingService.instanceVO> createInstanceVOList(Integer listSize) {
		list<MdmMappingService.instanceVO> instanceVOs = new list<MdmMappingService.instanceVO>();
		for (Integer i = 1; i < listSize + 1; i++) {
			instanceVOs.add(createInstanceVO(i));
		}
		return instanceVOs;
	}
	
	private MdmMappingService.protocolSiteVO createProtocolSiteVO(Integer index) {
		MdmMappingService.protocolSiteVO protocolSiteVO = new MdmMappingService.protocolSiteVO();
		protocolSiteVO.siteId = index;
		protocolSiteVO.protProjId = index;
		protocolSiteVO.protocolNo = 'MOCK PROT NO ' + index;
		protocolSiteVO.praStudyId = 'MOCK PRA ID ' + index;
		protocolSiteVO.instanceVO = createInstanceVO(1);
		protocolSiteVO.instanceName = protocolSiteVO.instanceVO.instanceName;
		protocolSiteVO.source = 'MOCK TEST CLASS';
		return protocolSiteVO;
	}
	
	private list<MdmMappingService.protocolSiteVO> createProtocolSiteVOList(Integer listSize) {
		list<MdmMappingService.protocolSiteVO> protocolSiteVOs = new list<MdmMappingService.protocolSiteVO>();
		for (Integer i = 1; i < listSize + 1; i++) {
			protocolSiteVOs.add(createProtocolSiteVO(i));
		}
		return protocolSiteVOs;
	}
	
}