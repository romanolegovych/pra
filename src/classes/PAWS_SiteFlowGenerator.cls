public with sharing class PAWS_SiteFlowGenerator
{
	@testvisible private static List<ID> TEST_TEMPLATE_IDS;
	//public static Integer NUMBER_OF_SITES_TO_PROCESS = 1;
	public static String SITE_ACTIVATION_STEP_NAME = 'Site Activated';
	
	static
	{
		List<PAWS_Settings__c> settings = PAWS_Settings__c.getall().values();
		if (settings.size() > 0 && String.isNotBlank(settings.get(0).Site_Activation_Step_Name__c))
		{
			SITE_ACTIVATION_STEP_NAME = settings.get(0).Site_Activation_Step_Name__c;
		}
	}
	
	/******************** PUBLIC METHODS/PROPS ********************/
	
	public static Map<String, AggregatedPlannedSites> getAggregatedPlannedSites(ID scenarioId, Integer sitesPerWeekLimit, Integer weeksPerCountryLimit)
	{
		Map<String, AggregatedPlannedSites> result = new Map<String, AggregatedPlannedSites>();
		
		for (PBB_WR_APIs.CountryWeeklyEventsData countryWeeklyData : getPlannedSites(scenarioId))
		{
			result.put(countryWeeklyData.countryId, new AggregatedPlannedSites(countryWeeklyData.wdpsList, sitesPerWeekLimit, weeksPerCountryLimit));
		}
		
		return result;
	}
	
	/**
	* Creates new PAWS_Project_Flow_Site__c record.
	*/
	public static PAWS_Project_Flow_Site__c createSite(String pawsCountryID, String siteName)
	{
		PAWS_Project_Flow_Country__c pawsCountry = selectPAWSCountry(pawsCountryID);
		
		STSWR1.AbstractTrigger.Disabled = true;
		
		PAWS_Project_Flow_Site__c pawsSite = new PAWS_Project_Flow_Site__c(
			Name = siteName,
			PAWS_Country__c = pawsCountry.Id,
			PAWS_Project__c = pawsCountry.PAWS_Project__c
		);
		
		insert pawsSite;
		
		STSWR1.AbstractTrigger.Disabled = false;
		
		return pawsSite;
	}
	
	public static List<ID> getTemplates(ID pawsSiteId)
	{
		PAWS_Project_Flow_Site__c pawsSite = (PAWS_Project_Flow_Site__c) PAWS_APIHelper.loadSource(pawsSiteId);
		//return (!Test.isRunningTest() ? new PAWS_API().getTemplate(pawsSite) : TEST_TEMPLATE_IDS);
		return new PAWS_API().getTemplate(pawsSite);
	}
	
	public static Boolean activateSite(String pawsSiteId, String templateId)
	{
		new PAWS_API().activateFlow(pawsSiteId, templateId);
		
		PAWS_Project_Flow_Site__c siteActivated = [Select Status__c, Activation_Errors__c From PAWS_Project_Flow_Site__c Where Id = :pawsSiteId];
		
		if (siteActivated.Status__c == 'Error') throw new PAWS_API.PAWS_APIException('Site activation failed with error: ' + siteActivated.Activation_Errors__c);
		
		return (siteActivated.Status__c == 'Active');
	}
	
	public static void setEndDate(String pawsSiteID, String endDateStr, String parentFlowID)
	{
		Date endDate = convertToDate(endDateStr);
		
		List<STSWR1__Gantt_Step_Property__c> properties = new List<STSWR1__Gantt_Step_Property__c>();
		for(STSWR1__Flow_Step_Junction__c record : [select STSWR1__Duration__c from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__r.STSWR1__Source_ID__c = :pawsSiteId and Name = :SITE_ACTIVATION_STEP_NAME])
		{
			STSWR1__Gantt_Step_Property__c property = new STSWR1__Gantt_Step_Property__c(STSWR1__Step__c = record.Id, STSWR1__Parent_Flow_Id__c = parentFlowID, STSWR1__Planned_End_Date__c = endDate);
			properties.add(property);
		}
		
		insert properties;
	}
	
	/*public static Boolean activateSite(String pawsSiteId, Date weekDate, String parentFlowID)
	{
		pawsSite = (PAWS_Project_Flow_Site__c) PAWS_APIHelper.loadSource(pawsSite.Id);
		List<ID> templateIds = getTemplates(pawsSite);
		
		PAWS_API pawsApi = new PAWS_API();
		
		if (templateIds == null || templateIds.isEmpty()) throw new PAWS_API.PAWS_APIException(pawsSite.Name + ': cannot find a flow templates for this site.');
		for (ID templateId : templateIds)
		{
			pawsAPI.activateFlow(pawsSite.Id, templateId);
		}
		
		PAWS_Project_Flow_Site__c siteActivated = [Select Status__c, Activation_Errors__c From PAWS_Project_Flow_Site__c where Id = :pawsSite.Id];
		if (siteActivated.Status__c == 'Error') throw new PAWS_API.PAWS_APIException(pawsSite.Name + ': activation failed with error: ' + siteActivated.Activation_Errors__c);
		
		setEndDate(pawsSite.Id, weekDate, parentFlowID);
	}*/
	
	/******************** PRIVATE METHODS/PROPS ********************/
	
	private static PAWS_Project_Flow_Country__c selectPAWSCountry(String pawsCountryID)
	{
		for (PAWS_Project_Flow_Country__c pawsCountry : [select Name, Project__c, PAWS_Project__c from PAWS_Project_Flow_Country__c where Id = :pawsCountryID])
		{
			return pawsCountry;
		}
		
		return new PAWS_Project_Flow_Country__c();
	}
	
	/*private Country__c selectGenericCountry(PAWS_Project_Flow_Country__c pawsCountry)
	{
		for (Country__c genericCountry : [select Name from Country__c where Name = :pawsCountry.Name])
		{
			return genericCountry;
		}
		
		throw new PAWS_API.PAWS_APIException('Country name is missing or incorrect: ' + pawsCountry.Name);
	}*/
	
	/*public Boolean generateSiteWRFlows(String scenarioID, String pawsCountryID, String parentFlowID)
	{
		System.debug(System.LoggingLevel.ERROR, 'Scenario ID: ' + scenarioID + ' Country ID: ' + pawsCountryID);//@TODO: Delete this line!
		PAWS_Project_Flow_Country__c pawsCountry = selectPAWSCountry(pawsCountryID);
		Country__c genericCountry = selectGenericCountry(pawsCountry);
		List<PBB_WR_APIs.CountryWeeklyEventsData> sitesPlan = getPlannedSites(scenarioID, genericCountry.Id);
		
		Decimal sitesPlanned = 0;
		Decimal sitesCreated = pawsCountry.PAWS_Project_Flow_Sites__r.size();
		Decimal newSites = 0;
		for (PBB_WR_APIs.CountryWeeklyEventsData countryEventsData : sitesPlan)
		{
			for (PBB_WR_APIs.WeekDatePlannedSites weeklyEventsData : countryEventsData.wdpsList)
			{
				sitesPlanned += weeklyEventsData.plannedSites;
				if (sitesPlanned <= sitesCreated)
				{
					continue;
				}
				
				for (Decimal i = 0; i < NUMBER_OF_SITES_TO_PROCESS && (newSites + sitesCreated) < sitesPlanned; i++)
				{
					newSites++;
					processSite(pawsCountry, (sitesCreated + newSites), weeklyEventsData.weekDate, parentFlowID);
				}
			}
		}
		
		System.debug(System.LoggingLevel.ERROR, 'New sites: ' + newSites + ' previously created: ' + sitesCreated + ' sites planned: ' + sitesPlanned);//@TODO: Delete this line!
		return (sitesCreated + newSites) >= sitesPlanned;
	}*/
	
	/*private List<PBB_WR_APIs.CountryWeeklyEventsData> getPlannedSites(String scenarioID, String genericCountryID)
	{
		if (Test.isRunningTest())
		{
			return new List<PBB_WR_APIs.CountryWeeklyEventsData>{
				new PBB_WR_APIs.CountryWeeklyEventsData(
					genericCountryID, 
					new List<PBB_WR_APIs.WeekDatePlannedSites>{
						new PBB_WR_APIs.WeekDatePlannedSites(System.today(), 15, 2)
					})
			};
		}
		
		return PBB_WR_APIs.getPbbWeeklyEvntsByCountryId(scenarioId, genericCountryID);
	}*/
	
	private static List<PBB_WR_APIs.CountryWeeklyEventsData> getPlannedSites(ID scenarioId)
	{
		if (Test.isRunningTest())
		{
			PBB_WR_APIs.WeekDatePlannedSites plannedSitesInstance = (PBB_WR_APIs.WeekDatePlannedSites) JSON.deserialize('{}', PBB_WR_APIs.WeekDatePlannedSites.class);
			plannedSitesInstance.weekDate = Date.today();
			plannedSitesInstance.plannedSites = 1;
			
			PBB_WR_APIs.CountryWeeklyEventsData countryWeeklyEvtsData = (PBB_WR_APIs.CountryWeeklyEventsData) JSON.deserialize('{}', PBB_WR_APIs.CountryWeeklyEventsData.class);
			countryWeeklyEvtsData.countryId = 'test';
			countryWeeklyEvtsData.wdpsList = new List<PBB_WR_APIs.WeekDatePlannedSites>{plannedSitesInstance};
			
			return new List<PBB_WR_APIs.CountryWeeklyEventsData>{countryWeeklyEvtsData};
		}
		
		return PBB_WR_APIs.getPbbWeeklyEvntsByCountryId(scenarioId, null);
	}
	
	private static Date convertToDate(String dateVal)
	{
		string[] dateParts = dateVal.split('-');
		Integer dateYear = Integer.valueOf(dateParts[0]);
		Integer dateMonth = Integer.valueOf(dateParts[1]);
		Integer dateDay = Integer.valueOf(dateParts[2]);
		return Date.newInstance(dateYear, dateMonth, dateDay);
	}
	
	/*private PAWS_Project_Flow_Site__c processSite(PAWS_Project_Flow_Country__c pawsCountry, Decimal index, Date weekDate, String parentFlowID)
	{
		PAWS_Project_Flow_Site__c pawsSite = createSite(pawsCountry, index);
		activateSite(pawsSite, weekDate, parentFlowID);
		
		return pawsSite;
	}*/
	
	/******************** HELPER CLASSES ********************/
	public class AggregatedPlannedSites
	{
		public Integer TotalSitesCount {get; set;}
		public List<PBB_WR_APIs.WeekDatePlannedSites> WeekDatePlannedSites {get; set;}
		
		public AggregatedPlannedSites(List<PBB_WR_APIs.WeekDatePlannedSites> weekDatePlannedSites, Integer sitesPerWeekLimit, Integer weeksPerCountryLimit)
		{
			this.TotalSitesCount = 0;
			this.WeekDatePlannedSites = new List<PBB_WR_APIs.WeekDatePlannedSites>();
			
			Integer n = weekDatePlannedSites.size();
			n = (weeksPerCountryLimit > 0 ? (weeksPerCountryLimit > n ? n : weeksPerCountryLimit) : n);
			Integer i;
			for (i = 0; i < n; i++)
			{
				PBB_WR_APIs.WeekDatePlannedSites weekSites = weekDatePlannedSites.get(i);
				weekSites.plannedSites = (sitesPerWeekLimit > 0 && weekSites.plannedSites.intValue() > sitesPerWeekLimit ? Decimal.valueOf(sitesPerWeekLimit) : weekSites.plannedSites);
				this.TotalSitesCount += weekSites.plannedSites.intValue();
				
				this.WeekDatePlannedSites.add(weekSites);
			}
		}
	}
}