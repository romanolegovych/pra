/**
 * Test to cover wrapper controllers for tabbed pages
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestContainerControllers {
	static String val;
	static String id;
	static String client;
	static String project;
	static String role;
	static String adhocName;
	static String roleId;
	static List<User> users;
	static List<Job_Class_Desc__c> jobClasses;
	static List<Job_Title__c> jobTitles;
	static List<PRA_Business_Unit__c> businessUnits;
	static List<Department__c> departments;
	static List<Region__c> regions;
	static List<Country__c> countries;
	static List<Employee_Type__c> empTypes;
	static List<WFM_Client__c> clients;
	static List<WFM_Project__c> projects;
	static List<WFM_Contract__c> contracts;
	static List<Role_Type__c> roleTypes;
	static List<LMS_Role__c> roles;
	static List<LMSConstantSettings__c> constants;
	static List<LMSClearAdminGroup__c> clearAdmins;
	static List<LMSPRARoleAdminGroup__c> praAdmins;
	static List<LMSPSTRoleAdminGroup__c> pstAdmins;
	static List<LMSAdhocRoleAdminGroup__c> adhocAdmins;
	static List<LMSRoleAdminGroup__c> roleAdmins;
	
	static void initProfileTests() {		
		constants = new LMSConstantSettings__c[] {
			new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
			new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
			new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
			new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
			new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete')
		};
		insert constants;
		
		clearAdmins = new LMSClearAdminGroup__c[] {
			new LMSClearAdminGroup__c(Name = 'System Administrator'),
			new LMSClearAdminGroup__c(Name = 'Clear Admin - Platform')
		};
		insert clearAdmins;
		
		praAdmins = new LMSPRARoleAdminGroup__c[] {
			new LMSPRARoleAdminGroup__c(Name = 'PRA Role Admin'),
			new LMSPRARoleAdminGroup__c(Name = 'System Administrator'),
			new LMSPRARoleAdminGroup__c(Name = 'Clear Admin - Platform')
		};
		insert praAdmins;
		
		pstAdmins = new LMSPSTRoleAdminGroup__c[] {
			new LMSPSTRoleAdminGroup__c(Name = 'PST Role Admin'),
			new LMSPSTRoleAdminGroup__c(Name = 'System Administrator'),
			new LMSPSTRoleAdminGroup__c(Name = 'Clear Admin - Platform')
		};
		insert pstAdmins;
		
		adhocAdmins = new LMSAdhocRoleAdminGroup__c[] {
			new LMSAdhocRoleAdminGroup__c(Name = 'Adhoc Role Admin'),
			new LMSAdhocRoleAdminGroup__c(Name = 'Clear Admin - Platform'),
			new LMSAdhocRoleAdminGroup__c(Name = 'System Administrator')
		};
		insert adhocAdmins;
		
		roleAdmins = new LMSRoleAdminGroup__c[] {
			new LMSRoleAdminGroup__c(Name = 'Role Administrator - Platform'),
			new LMSRoleAdminGroup__c(Name = 'System Administrator'),
			new LMSRoleAdminGroup__c(Name = 'Clear Admin - Platform')
		};
		insert roleAdmins;
		
		List<Profile> profiles = 
		[SELECT Id FROM Profile 
		WHERE Name IN ('System Administrator','Clear Admin - Platform','Role Administrator - Platform','PRA Role Admin','PST Role Admin','Adhoc Role Admin')];
		users = new List<User>();
		Integer i = 0;
		
		for(Profile p : profiles) {
			i++;
			users.add(new User(Username = 'lmsuser' + i + '@praintl.com', LastName = 'Allen' + i, Email = 'lmsuser' + i + '@praintl.com', Alias = 'lmu' + i, 
				CommunityNickname = 'a' + i, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'UTF-8', 
				LanguageLocaleKey = 'en_US', ProfileId = p.Id));
		}
		insert users;
	}
	
	static void initData() {
		jobClasses = new Job_Class_Desc__c[] {
			new Job_Class_Desc__c(Name = 'class1', Job_Class_ExtID__c = 'class1', Job_Class_Code__c = 'code1', Is_Assign__c = true)
		};
		insert jobClasses;
		
		jobTitles = new Job_Title__c[] {
			new Job_Title__c(Job_Code__c = 'Code1', Job_Title__c = 'title1', Job_Class_Desc__c = jobClasses[0].Id, Status__c='A')
		};
		insert jobTitles;
		
		businessUnits = new PRA_Business_Unit__c[] {
			new PRA_Business_Unit__c(Name = 'unit', Business_Unit_Code__c = 'BUC', Status__c = 'A')
		};
		insert businessUnits;
		
		departments = new Department__c[] {
			new Department__c(Name = 'department', Department_Code__c = 'DEPTC', Status__c = 'A')
		};
		insert departments;
		
		regions = new Region__c[] {
			new Region__c(Region_Name__c = 'region1', Region_Id__c = 1, Status__c = 'A')
		};
		insert regions;
		
		countries = new Country__c[] {
			new Country__c(Name = 'country1', Region_Name__c = 'region1', Country_Code__c = 'C1', PRA_Country_ID__c = '100')
		};
		insert countries;
		
		empTypes = new Employee_Type__c[] {
			new Employee_Type__c(Name = 'Employees')
		};
		insert empTypes;
		
		clients = new WFM_Client__c[] {
			new WFM_Client__c(Name = 'Client1', Client_Unique_Key__c = 'Client1')
		};
		insert clients;
		
		contracts = new WFM_Contract__c[] {
			new WFM_Contract__c(Name = 'contract1', Contract_Unique_Key__c = 'contract1', Client_Id__c = clients[0].Id)
		};
		insert contracts;
		
		projects = new WFM_Project__c[] {
			new WFM_Project__c(Name = 'Project1', Project_Unique_Key__c = 'Project1', Contract_Id__c = contracts[0].Id)
		};
		insert projects;
		
		roleTypes = new Role_Type__c[] {
			new Role_Type__c(Name = 'PRA'),
			new Role_Type__c(Name = 'Project Specific'),
			new Role_Type__c(Name = 'Additional Role')
		};
		insert roleTypes;
		
		roles = new LMS_Role__c[] {
			new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = jobTitles[0].Id, Business_Unit__c = businessUnits[0].Id,
							Department__c = departments[0].Id, Region__c = regions[0].Id, Country__c = countries[0].Id, Employee_Type__c = empTypes[0].Id, 
							Status__c = 'Active', Sync_Status__c = 'Y', Role_Type__c = roleTypes[0].Id),
			new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Project_Id__c = projects[0].Id, Client_Id__c = clients[0].Id, Status__c = 'Active', Sync_Status__c = 'N'),
			new LMS_Role__c(Adhoc_Role_Name__c = 'TestRole1', Status__c = 'Active', Sync_Status__c = 'Y')
		};
		insert roles;
	}
	
    static testMethod void testCourseToRole() {
    	initProfileTests();
    	for(User u : users) {
	    	System.runAs(users[0]){
	    		LMS_CourseToRoleController ctr = new LMS_CourseToRoleController();
	    	}
    	}
    }
    
    static testMethod void testRoleToCourse() {
    	initProfileTests();
    	for(User u : users) {
    		System.runAs(u) {
    			LMS_RoleToCourseController rtc = new LMS_RoleToCourseController();
    		}
    	}
    }
    
    static testMethod void testCourseToRoleAllParams() {
    	initProfileTests();
    	id = 'p';
    	client = 'client';
    	project = 'project';
    	role = 'role';
    	PageReference pr = new PageReference('/apex/LMS_CourseToRole?id=' + id + '&client=' + client + '&project=' + project + '&class=' + role);
    	Test.setCurrentPageReference(pr);
    	
    	for(User u : users) {
    		System.runAs(u) {
		    	LMS_CourseToRoleController ctr = new LMS_CourseToRoleController();
		    	System.debug('------------------------------path---------------------------------'+ctr.projectPath);
		    	System.assert(ctr.projectPath == '/apex/LMS_CourseToProject?client=' + client + '&project=' + project + '&class=' + role);
    		}
    	}
    }
    
    static testMethod void testCourseToRoleOnlyClient() {
    	initProfileTests();
    	id = 'p';
    	client = 'client';
    	role = 'role';
    	PageReference pr = new PageReference('/apex/LMS_CourseToRole?id=' + id + '&client=' + client + '&class=' + role);
    	Test.setCurrentPageReference(pr);
    	
    	for(User u : users) {
    		System.runAs(u) {
		    	LMS_CourseToRoleController ctr = new LMS_CourseToRoleController();
		    	System.debug('------------------------------path---------------------------------'+ctr.projectPath);
		    	System.assert(ctr.projectPath == '/apex/LMS_CourseToProject?client=' + client + '&class=' + role);
    		}
    	}
    }
    
    static testMethod void testCourseToRoleOnlyProject() {
    	initProfileTests();
    	id = 'p';
    	project = 'project';
    	role = 'role';
    	PageReference pr = new PageReference('/apex/LMS_CourseToRole?id=' + id + '&project=' + project + '&class=' + role);
    	Test.setCurrentPageReference(pr);
    	
    	for(User u : users) {
    		System.runAs(u) {
		    	LMS_CourseToRoleController ctr = new LMS_CourseToRoleController();
		    	System.debug('------------------------------path---------------------------------'+ctr.projectPath);
		    	System.assert(ctr.projectPath == '/apex/LMS_CourseToProject?project=' + project + '&class=' + role);
    		}
    	}
    }
    
    static testMethod void testCourseToRoleByRoleId() {
    	initProfileTests();
    	initData();
    	id = 'pra';
    	roleId = roles[0].Id;
    	PageReference pr = new PageReference('/apex/LMS_CourseToRole?id=' + id + '&roleId=' + roleId);
    	Test.setCurrentPageReference(pr);
    	
    	for(User u : users) {
    		System.runAs(u) {
		    	LMS_CourseToRoleController ctr = new LMS_CourseToRoleController();
		    	System.debug('------------------------------path---------------------------------'+ctr.praPath);
		    	System.assert(ctr.praPath == '/apex/LMS_CourseToPRA?roleId=' + roleId);
    		}
    	}
    	
    	id = 'pst';
    	roleId = roles[1].Id;
    	pr = new PageReference('/apex/LMS_CourseToRole?id=' + id + '&roleId=' + roleId);
    	Test.setCurrentPageReference(pr);
    	
    	for(User u : users) {
    		System.runAs(u) {
		    	LMS_CourseToRoleController ctr = new LMS_CourseToRoleController();
		    	System.debug('------------------------------path---------------------------------'+ctr.projectPath);
		    	System.assert(ctr.projectPath == '/apex/LMS_CourseToProject?roleId=' + roleId);
    		}
    	}
    	
    	id = 'adhoc';
    	roleId = roles[2].Id;
    	pr = new PageReference('/apex/LMS_CourseToRole?id=' + id + '&roleId=' + roleId);
    	Test.setCurrentPageReference(pr);
    	
    	for(User u : users) {
    		System.runAs(u) {
		    	LMS_CourseToRoleController ctr = new LMS_CourseToRoleController();
		    	System.debug('------------------------------path---------------------------------'+ctr.projectPath);
		    	System.assert(ctr.adhocPath == '/apex/LMS_CourseToAdhoc?roleId=' + roleId);
    		}
    	}
    }
    
    static testMethod void testCourseToRoleOnlyAdhoc() {
    	initProfileTests();
    	id = 'a';
    	adhocName = 'name';
    	PageReference pr = new PageReference('/apex/LMS_CourseToRole?id='+ id +'&adhocName='+adhocName);
    	Test.setCurrentPageReference(pr);
    	
    	for(User u : users) {
    		System.runAs(u) {
		    	LMS_CourseToRoleController ctr = new LMS_CourseToRoleController();
		    	System.debug('------------------------------path---------------------------------'+ctr.adhocPath);
		    	System.assert(ctr.adhocPath == '/apex/LMS_CourseToAdhoc?adhocName=' + adhocName);
    		}
    	}
    }
    
    static testMethod void testPersonToRole(){
    	initProfileTests();
    	for(User u : users) {
    		System.runAs(u) {
    			LMS_PersonToRoleController ptr = new LMS_PersonToRoleController();
    		}
    	}
    }
    
    static testMethod void testViewRoles(){
    	initProfileTests();
    	for(User u : users) {
    		System.runAs(u) {
    			LMS_RoleViewerController rvc = new LMS_RoleViewerController();
    		}
    	}
    }
    
}