@isTest
private class PRAFIT_UnitTests{
    
    //  Random String Generator 
    //  TODO: Move this to global Utils or global Test Factory
        public static String createRandomString() {
            String ret = 'TestData' + math.rint(math.random() * 100000);
            return ret; 
        }
    
    //  User Object
    // TODO: Move this to global Test Factory
        // establish a default profile for all User test data
        	public static final String profileNameSysAdmin = 'System Administrator';
            public static final Profile defaultProfile = [Select Id from Profile where Name = : profileNameSysAdmin LIMIT 1]; 
        // method for creating and optionally inserting a single User 
            // --> you need to specify if the record should NOT be inserted immediately (pass value of false)
                // --> you might do this if you want to modify the default test data in your Unit Test before performing the insert
            public static User createTestUser(Boolean insertRecord) {
                String randomWord = createRandomString();
                User testUser = new User();
                testUser.Email = 'email' + randomWord + '@testmyapex.com';
                testUser.Username = 'user' + randomWord + '@testmyapex.com';
                testUser.FirstName = 'First Name ' + randomWord;
                testUser.LastName = 'Last Name' + randomWord;
                testUser.Alias = 'test';
                testUser.ProfileId = defaultProfile.Id;
                testUser.LanguageLocaleKey = 'en_US';
                testUser.LocaleSidKey = 'en_US';
                testUser.TimeZoneSidKey = 'America/Chicago';
                testUser.EmailEncodingKey = 'UTF-8';
                if (insertRecord != false){
                insert testUser;
                }
                return testUser;
            }
            public static User createTestUser(){
                User testUser = createTestUser(true);
                return testUser;
            }
            
        // method for creating and optionally inserting multiple Users 
            // --> you need to specify if the list of records should NOT be inserted immediately (pass value of false)
                // --> you might do this if you want to modify the default test data in your Unit Test before performing the insert
            public static list<User> createTestUsers(Boolean insertRecords, Integer numberOfRecords){
                list<User> testUsers = new list<User>();
                for (Integer i=0; i<numberOfRecords; i++){
                    User testUser = createTestUser(false);
                    testUsers.add (testUser);
                }
                if (insertRecords != false){
                insert testUsers;
                }
                return testUsers;
            }
            public static list<User> createTestUsers(){
                list<User> testUsers = createTestUsers(true, 5);
                return testUsers;
            }
    
    //  PRAFIT_Action Object
        // method for creating and optionally inserting a single PRAFIT_Action
            // --> you need to specify if the record should NOT be inserted immediately (pass value of false)
                // --> you might do this if you want to modify the default test data in your Unit Test before performing the insert
            public static PRAFIT_Action__c createTestAction(boolean insertRecord) {
                Map <String,Schema.RecordTypeInfo> actionRecordTypes = PRAFIT_Action__c.sObjectType.getDescribe().getRecordTypeInfosByName();
    			Id recordTypeId = actionRecordTypes.values()[0].getRecordTypeId();
                String randomWord = createRandomString();
                PRAFIT_Action__c testAction = new PRAFIT_Action__c();
                testAction.RecordTypeId = recordTypeId;
                testAction.Title__c = 'Test Action ' + randomWord;
                testAction.Description__c = 'Description ' + randomWord;
                testAction.Milestone__c = 'Kickoff - 08/26/2013';
                testAction.Due_Date__c = System.today().addDays(90);
                testAction.Status__c = 'Not Started - 0%';
                testAction.Functional_Team__c = 'Benefits';
                User praOwner = createTestUser();
                testAction.Assigned_To__c = praOwner.Id;
                testAction.Dependency__c = 'Legal';
                testAction.Dependency_Reason_Description__c = 'Dependency Reason Description ' + randomWord;
                testAction.Lessons_Learned__c = 'Lessons Learned ' + randomWord;
                if (insertRecord != false){
                    insert testAction;
                }
                return testAction; 
            }
            public static PRAFIT_Action__c createTestAction(){
                PRAFIT_Action__c testAction = createTestAction(true);
                return testAction;
            }
            
        // method for creating and optionally inserting multiple PRAFIT_Actions 
            // --> you need to specify if the list of records should NOT be inserted immediately (pass value of false)
                // --> you might do this if you want to modify the default test data in your Unit Test before performing the insert
            public static list<PRAFIT_Action__c> createTestActions(Boolean insertRecords, Integer numberOfRecords){
                list<PRAFIT_Action__c> testActions = new list<PRAFIT_Action__c>();
                for (Integer i=0; i<numberOfRecords; i++){
                    PRAFIT_Action__c testAction = createTestAction(false);
                    testActions.add (testAction);
                }
                if (insertRecords != false){
                insert testActions;
                }
                return testActions;
            }
            public static list<PRAFIT_Action__c> createTestActions(){
                list<PRAFIT_Action__c> testActions = createTestActions(true, 5);
                return testActions;
            }
    
    //  TESTS for PRAFIT_Action object and supporting classes
        //test basic record creation
        static TestMethod void testCreateTestDataAction() {
            PRAFIT_Action__c actionObject = createTestAction();
            System.assert(actionObject.id != null);
        }
        static TestMethod void testCreateTestDataActions() {
            list<PRAFIT_Action__c> actionObjectList = createTestActions();
            System.assert(!actionObjectList.isEmpty());
            System.assert(actionObjectList[4].id != null);
        }
        
        //test PRAFIT_DashboardController
        static TestMethod void testPRAFIT_DashboardController(){
            //stage test data
            list<PRAFIT_Action__c> actionObjectList = createTestActions(true,10);
            PRAFIT_Action__c actionObject1 = actionObjectList[0];
            actionObject1.Status__c = 'In Progress - 25%';
            actionObject1.Due_Date__c = System.today().addDays(-10);
            PRAFIT_Action__c actionObject2 = actionObjectList[1];
            actionObject2.Status__c = 'Complete - 100%';
            PRAFIT_Action__c actionObject3 = actionObjectList[2];
            actionObject3.Due_Date__c = System.today().addDays(31);
            update actionObjectList;
            //test constructor
            PRAFIT_DashboardController ctrlr = new PRAFIT_DashboardController();
            //test static methods
            List<PRAFIT_DashboardController.ChartDataItem> testPRAFITActionStatusByFunctionalTeamList = ctrlr.getPRAFITActionStatusByFunctionalTeamRPS();
            List<PRAFIT_DashboardController.ChartDataItem> testPRAFITActionsIncompleteByFunctionalTeamList = ctrlr.getPRAFITActionsIncompleteByFunctionalTeamRPS();
            List<PRAFIT_DashboardController.ChartDataItem> testPRAFITActionStatusByFunctionalTeamList2 = ctrlr.getPRAFITActionStatusByFunctionalTeamCRI();
            List<PRAFIT_DashboardController.ChartDataItem> testPRAFITActionsIncompleteByFunctionalTeamList2 = ctrlr.getPRAFITActionsIncompleteByFunctionalTeamCRI();
            ctrlr.getDashboardPageCalendarUrlRPS();
            ctrlr.getDashboardPageCalendarUrlCRI();
        }
        
        //test testPRAFIT_ActionExportController
        static TestMethod void testPRAFIT_ActionExportController(){
            //stage test data
            list<PRAFIT_Action__c> actionObjectList = createTestActions(true,10);
            //test static methods
            list<PRAFIT_Action__c> testActionObjectsList = PRAFIT_ActionExportController.getActivitiesForExport();
            System.Assert(!testActionObjectsList.isEmpty());
       		User praUser = createTestUser();
       		system.runAs(praUser){
       			PRAFIT_ActionExportController.getActivitiesForExport();
       		}	
        }
        
        //test PRAFIT_NewEditActionExtensionExistingRecord
        static TestMethod void testPRAFIT_NewEditActionExtensionExistingRecord(){
            //stage test data
            Group groupObject = new Group(name='PRAFIT:: IT');
            insert groupObject;
            User userObject = createTestUser();
            GroupMember groupMemberObject = new GroupMember(GroupId=groupObject.Id,UserOrGroupId=userObject.Id);     
            insert groupMemberObject;
            //we have to use runAs to avoid a mixedDML error that happens when you work with 
                //setup objects and then other objects in the same test context
            System.RunAs(new User(id = UserInfo.getUserId())){
                //stage test data
                insert new PRAFIT_Config__c(Name='nameSpace',Value__c='PRAFIT::',Description__c='test description');
                PRAFIT_Action__c actionObject = createTestAction();
                //test constructor
                PageReference pageRef = Page.PRAFIT_NewEditActionPage;
                Test.setCurrentPage(pageRef);
                ApexPages.CurrentPage().getParameters().put('id',actionObject.Id);
                ApexPages.StandardController ctrlr = new ApexPages.StandardController(actionObject);
                PRAFIT_NewEditActionExtension ctrlrExt = new PRAFIT_NewEditActionExtension (ctrlr);
                System.Assert(ctrlrExt.actionObj!=null);
                //test methods
                ctrlrExt.getPageTitle();
                System.Assert(ctrlrExt.getPageTitle()!=null);
                System.Assert(ctrlrExt.getPageTitle().contains('Editing'));
                //test static methods
                PRAFIT_NewEditActionExtension.getFunctionalTeamMembers('IT');
             }
        }
        
        //test PRAFIT_NewEditActionExtensionDirtyRecord
        static TestMethod void testPRAFIT_NewEditActionExtensionDirtyRecord(){
            //stage test data
            PRAFIT_Action__c actionObject = new PRAFIT_Action__c();
            //test constructor
            PageReference pageRef = Page.PRAFIT_NewEditActionPage;
            Test.setCurrentPage(pageRef);
            ApexPages.CurrentPage().getParameters().put('id','000000000000000');
            ApexPages.StandardController ctrlr = new ApexPages.StandardController(actionObject);
            PRAFIT_NewEditActionExtension ctrlrExt = new PRAFIT_NewEditActionExtension (ctrlr);
        }
        
        //test PRAFIT_NewEditActionExtensionNewRecord
        static TestMethod void testPRAFIT_NewEditActionExtensionNewRecord(){
            //stage test data
            PRAFIT_Action__c actionObject = new PRAFIT_Action__c();
            //test constructor
            PageReference pageRef = Page.PRAFIT_NewEditActionPage;
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController ctrlr = new ApexPages.StandardController(actionObject);
            PRAFIT_NewEditActionExtension ctrlrExt = new PRAFIT_NewEditActionExtension (ctrlr);
            //test methods
            ctrlrExt.getPageTitle();
            System.Assert(ctrlrExt.getPageTitle()!=null);
            System.Assert(ctrlrExt.getPageTitle().contains('New'));
        }
        
    //test PRAFIT_Utils 
        //TODO: if the methods in the PRAFIT_Utils class are moved to a platform wide class we need to move these unit tests with them      
        static TestMethod void testPRAFIT_UtilsCheckProfile(){
        	//stage test data
        	User praUser = createTestUser();
        	Profile prof = [SELECT Name
          					FROM Profile
          					WHERE Name != : profileNameSysAdmin
          					LIMIT 1];
        	Boolean hasProf;
        	system.runAs(praUser){
        		hasProf = PRAFIT_Utils.checkProfile(prof.Name);
        	}
        	system.assert(hasProf != true);
        }
        
        static TestMethod void testCheckPermissionSet(){
        	//stage test data
        	User praUser = createTestUser();
        	PermissionSet permSet = [SELECT Label
          					   	     FROM PermissionSet
          					   	     ORDER BY Label
          					   	     LIMIT 1];
        	Boolean hasPermSet;
        	system.runAs(praUser){
        		hasPermSet = PRAFIT_Utils.checkPermissionSet(permSet.Label);
        	}
    	}
    	
    	static TestMethod void testRecordTypeIsAvailable() {
    		//stage test data
        	User praUser = createTestUser();
        	Map <String,Schema.RecordTypeInfo> actionRecordTypes = PRAFIT_Action__c.sObjectType.getDescribe().getRecordTypeInfosByName();
			Id recordTypeId = actionRecordTypes.values()[0].getRecordTypeId();
        	Boolean hasRecType;
        	system.runAs(praUser){
        		hasRecType = PRAFIT_Utils.recordTypeIsAvailable('PRAFIT_Action__c',recordTypeId);
        	}
    	}
        
        static TestMethod void testPRAFIT_UtilsGetPickListValues(){
            //test methods
            PRAFIT_Utils.getPickListValues('User','LanguageLocaleKey');
        }
        
        static TestMethod void testPRAFIT_UtilsGetUserIdsFromGroup(){
            //stage test data
            Group groupObject = new Group(name='TEST GROUP');
            insert groupObject;
            User userObject = createTestUser();
            GroupMember groupMemberObject = new GroupMember(GroupId=groupObject.Id,UserOrGroupId=userObject.Id);     
            insert groupMemberObject;
            //test methods
            PRAFIT_Utils.getUserIdsFromGroup(groupObject.Id);
        }
        
        static TestMethod void testPRAFIT_UtilsGetSObjects(){
            //stage test data
            list<User> actionObjects = createTestUsers(true,10);
            //test methods
            PRAFIT_Utils.getSObjects('User', null, new List<String>(),1000);
        }
}