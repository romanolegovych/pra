@isTest
private class BDT_PageBlockHeaderTest {

    static testMethod void goToPageTest() {
        BDT_PageBlockHeader bph = new BDT_PageBlockHeader();
        bph.goToPageName = 'BDT_Sponsors';
        bph.customId = '1111111111111111';
        System.assertNotEquals(null, bph.goToPage());
    }
    
    static testMethod void showButtonTest() {
        BDT_PageBlockHeader bph = new BDT_PageBlockHeader();
        bph.buttonValue = 'true';
        System.assertEquals(true, bph.getShowButton());
    }
}