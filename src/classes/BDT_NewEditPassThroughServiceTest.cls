@IsTest
public with sharing class BDT_NewEditPassThroughServiceTest {

	static TestMethod void NewEditPassThroughServiceTest(){
		
		ServiceCategory__c sc = new ServiceCategory__c(name = 'Test',code__c = '001.001');
        upsert sc;
        
        BDT_NewEditPassThroughServiceController neptsc = new BDT_NewEditPassThroughServiceController();
        
        PassThroughService__c pts = new PassThroughService__c(Name='Test', ServiceCategory__c=sc.Id);
        upsert pts;
        
        neptsc.PassThroughServiceId = pts.Id;
        
        system.assertEquals(pts.Id, neptsc.PassThroughServiceId );
        
        neptsc.loadPassThroughService();
        
        system.assertNotEquals( null, neptsc.save() );
        system.assertNotEquals( null, neptsc.cancel() );
        system.assertNotEquals( null, neptsc.deleteSoft() );
              
        
	}
}