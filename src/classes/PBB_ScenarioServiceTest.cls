/**
@author Bhargav Devaram
@date 2015
@description this is PBB_ScenarioService test class
**/

@isTest   
private class PBB_ScenarioServiceTest{
    static testMethod void testPBB_ScenarioService(){  
        test.startTest();
         //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils (); 
        tu.country = tu.createCountryAttributes();
        tu.therp= tu.createTherapeutic();
        tu.acc= tu.createAccountAttributes();
        tu.opp = tu.createOpportunityAttributes();
        test.stopTest();
        tu.wfmp = [select id,name from wfm_project__c];
        tu.bid =  [select id,name from Bid_project__c];        
        tu.sm= tu.createSRMModelAttributes();
        tu.MSA = tu.createSRMModelSiteActAttributes();        
        tu.MSE= tu.createSRMModelSubjEnrol();        
        tu.SCA= tu.createSRMCalendarAdj();
        tu.SWA= tu.createSRMWeekAdj();
        tu.SMQ= tu.createSRMModelQues();
        tu.SMR= tu.createSRMModelResp();
        tu.sm= tu.approveSRMmodel(tu.sm);               
        tu.bid.Proposal_Director__c = UserInfo.getUserId();
        tu.bid.Regulatory_Expert__c = UserInfo.getUserId();
        tu.bid.Proposal_Manager__c = UserInfo.getUserId(); 
        tu.bid.Therapeutic_Expert__c = UserInfo.getUserId();
        tu.bid.Project_Manager__c = UserInfo.getUserId(); 
        tu.bid.Business_Dev_Expert__c = UserInfo.getUserId();
        tu.bid.Contracts_Expert__c = UserInfo.getUserId();
        tu.bid.Startup_Expert__c = UserInfo.getUserId();
        tu.bid.Project_Director__c = UserInfo.getUserId();        
        update  tu.bid;
        
        tu.scen =tu.createScenarioAttributes();
        tu.scen.Status__c = 'Approved';
        tu.scen.SRM_MODEL__c = tu.sm.id;
        tu.scen.Sponsor__c ='Test Account';
        upsert tu.scen;              
        tu.psc =tu.createPBBScenarioCountry();
        tu.scecont=tu.createSrmCountryAttributes();
        tu.scecont.SRM_Model_Site_Activation__c = tu.MSA.id;
        tu.scecont.SRM_Model_Subject_Enrollment__c = tu.MSE.id;
        update tu.scecont;
        
        PBB_ScenarioService service = new PBB_ScenarioService();        
        List<PBB_Scenario__c> pbblist =PBB_ScenarioService.getSRMScenarios(tu.scen.id);
        System.assertEquals(pbblist[0].id,tu.scen.id);
        
        Bid_Project__c bidp =service.getSRMProject(tu.bid.id);
        System.assertEquals(bidp.id,tu.bid.id);
        
        List<SRM_Scenario_Country__c> sclist  = PBB_ScenarioService.getScenarioCountries(tu.scen.id); 
        System.assertEquals(sclist.size(),1);
        
        Map<String, List<SRM_Model_Site_Activation__c>> countryMap =new Map<String, List<SRM_Model_Site_Activation__c>>();
        List<SRM_Model_Site_Activation__c> srmsalist = new List<SRM_Model_Site_Activation__c>();
        srmsalist.add(tu.MSA);
        countryMap.put(tu.country.name,srmsalist);
        List<SelectOption> SelectOptionList = PBB_ScenarioService.convertToSelectOption(countryMap);   
        System.assertEquals(SelectOptionList.size(),2);
        
        List<PBB_ScenarioDetailsController.InnerClass> inrList = new List<PBB_ScenarioDetailsController.InnerClass>();
        inrList = PBB_ScenarioService.getScenarioSelectedCountries(tu.scen.id,countryMap,true,tu.sm.id);
        System.assertEquals(inrList.size(),1);
        
        inrList = PBB_ScenarioService.getScenarioSelectedCountries(tu.scen.id,countryMap,false,tu.sm.id);
        System.assertEquals(inrList.size(),1);
        
        Boolean b = PBB_ScenarioService.ckeckForDuplicateCountries(inrList);
        System.assertEquals(b,false);
        
        List<SRM_Model_Subject_Enrollment__c> SMSEList = new List<SRM_Model_Subject_Enrollment__c>();
        SMSEList.add(tu.MSE);        
        Map<Integer, Decimal> subEnrollMap = new Map<Integer, Decimal>();
        subEnrollMap.put(1,1.0);
        subEnrollMap.put(2,1.0);
        List<Decimal> yValues = new List<Decimal>();
        yValues = subEnrollMap.values();
        List<SRM_Calender_Adjustments__c> calendarList = new List<SRM_Calender_Adjustments__c>();
        calendarList.add(tu.SCA);
        List<SRM_Week_Adjustments__c> weekList = new List<SRM_Week_Adjustments__c>();
        weekList.add(tu.SWA);        
        String chartDataSiteC = service.generateStringChartData(tu.scen, String.valueof(tu.country.name),10,'cumulative','Site',srmsalist,subEnrollMap,yValues,calendarList,true,weekList,true,'10');
        System.assertNotEquals(chartDataSiteC,null);
        chartDataSiteC = service.generateStringChartData(tu.scen, String.valueof(tu.country.name),10,'noncumulative','Site',srmsalist,subEnrollMap,yValues,calendarList,true,weekList,true,'10');
        System.assertNotEquals(chartDataSiteC,null);
        subEnrollMap = service.getSubEnrollValues(tu.MSE,tu.scen,1);
        String chartDataSubjC = service.generateStringChartData(tu.scen, String.valueof(tu.country.name),10,'cumulative','Subject',srmsalist,subEnrollMap,yValues,calendarList,true,weekList,true,'10');
        System.assertNotEquals(chartDataSubjC,null);
        chartDataSubjC = service.generateStringChartData(tu.scen, String.valueof(tu.country.name),10,'noncumulative','Subject',srmsalist,subEnrollMap,yValues,calendarList,true,weekList,true,'10');
        System.assertNotEquals(chartDataSubjC,null);  
        
        List<String> chartDataSiteCList = new List<String>();
        List<String> chartDataSubjCList = new List<String>();        
        chartDataSiteCList = service.generateGridData(tu.scen, String.valueof(tu.country.name),10,'cumulative','Site',srmsalist,subEnrollMap,yValues,calendarList,true,weekList,true,'10');         
        //System.assertEquals(chartDataSiteCList.size(),26);
        chartDataSiteCList = service.generateGridData(tu.scen, String.valueof(tu.country.name),10,'noncumulative','Site',srmsalist,subEnrollMap,yValues,calendarList,true,weekList,true,'10'); 
        //System.assertEquals(chartDataSiteCList.size(),26);
        chartDataSubjCList = service.generateGridData(tu.scen, String.valueof(tu.country.name),10,'cumulative','Subject',srmsalist,subEnrollMap,yValues,calendarList,true,weekList,true,'10');
        //System.assertEquals(chartDataSubjCList.size(),26);
        chartDataSubjCList = service.generateGridData(tu.scen, String.valueof(tu.country.name),10,'noncumulative','Subject',srmsalist,subEnrollMap,yValues,calendarList,true,weekList,true,'10');        
        //System.assertEquals(chartDataSubjCList.size(),26);
        
        tu.sm.Status__c = 'Approved';      
        upsert tu.sm;
        String Activemodel=PBB_ScenarioService.getRecentActiveModel();   
        System.assertEquals(Activemodel,tu.sm.id);
        
        Map<String, List<sObject>>  SobjectMap = PBB_ScenarioService.getAllCountries('SRM_Model_Site_Activation__c',null);
        System.assertEquals((SobjectMap.values()).size(),0);
        
        SobjectMap = PBB_ScenarioService.getAllCountries('SRM_Model_Site_Activation__c',tu.sm.id);
        System.assertEquals((SobjectMap.values()).size(),1);
        
        SobjectMap = PBB_ScenarioService.getAllCountries('SRM_Model_Subject_Enrollment__c',tu.sm.id);
        System.assertEquals((SobjectMap.values()).size(),1);
        
        SobjectMap = PBB_ScenarioService.getAllCountries('SRM_Calender_Adjustments__c',tu.sm.id);
        System.assertEquals((SobjectMap.values()).size(),1);
        
        SobjectMap = PBB_ScenarioService.getAllCountries('SRM_Week_Adjustments__c',tu.sm.id);  
        System.assertEquals((SobjectMap.values()).size(),1);
        
        Map<String, List<SRM_Model_Site_Activation__c>> SMSAMap = PBB_ScenarioService.getSiteActivationMap(srmsalist);
        System.assertEquals(SMSAMap.size(),1);
        
        List<SRM_Model_Site_Activation__c> SMSAList =PBB_ScenarioService.getListOfSiteActivation(tu.country.name,srmsalist);
        System.assertEquals(SMSAList.size(),0);
        
        Map<String, List<SRM_Model_Subject_Enrollment__c>> SMSEMap = PBB_ScenarioService.getSubjectEnrollments(SMSEList);        
        System.assertEquals(SMSEMap.size(),1);
        
        List<SRM_Model_Subject_Enrollment__c> SSEList =PBB_ScenarioService.getListOfSubjectEnrollment(tu.country.name,SMSEList);        
        System.assertEquals(SSEList.size(),0);
        
        Map<String, List<SRM_Model_Site_Activation__c>> mapToSearch = new Map<String, List<SRM_Model_Site_Activation__c>>();   
        List<sObject> sobjlist = new List<sObject>();
        sobjlist=((List<SRM_Model_Site_Activation__c>)srmsalist);
        mapToSearch.put(tu.country.name,sobjlist);
        mapToSearch = PBB_ScenarioService.getAllCountries('SRM_Model_Site_Activation__c', tu.sm.id);
        System.assertEquals(mapToSearch.get(tu.country.name)[0].id,tu.MSA.id);
        
        List<sObject> SMSA1list =Service.findCountryList('SRM_Model_Site_Activation__c',tu.country.name,tu.scen,mapToSearch); 
        System.assertEquals(SMSA1list.size(),1);
        
        List<PBB_ScenarioDetailsController.RelateSponsorAndTherArea> RelateSponsorAndTherArealist=new List<PBB_ScenarioDetailsController.RelateSponsorAndTherArea>();
        RelateSponsorAndTherArealist = Service.findSponsorAndTherCountryList('SRM_Model_Site_Activation__c',tu.country.name,tu.scen,mapToSearch);
        System.assertEquals(RelateSponsorAndTherArealist.size(),1);
        
        List<SelectOption> SelectOptionListForCountry =PBB_ScenarioService.getSelectOptionListForCountry(RelateSponsorAndTherArealist);
        System.assertEquals(SelectOptionListForCountry.size(),1);
        
        sobjlist = new List<sObject>();
        sobjlist=((List<SRM_Model_Subject_Enrollment__c>)SMSEList);
        Map<String, List<SRM_Model_Subject_Enrollment__c>> mapToSearch1 = new Map<String, List<SRM_Model_Subject_Enrollment__c>>();
        mapToSearch1.put(tu.country.name,sobjlist);
        List<sObject> SMSA2list =Service.findCountryList('SRM_Model_Subject_Enrollment__c',tu.country.name,tu.scen,mapToSearch1);        
        System.assertEquals(SMSA2list.size(),1);
        
        Map<String, List<String>>  WandDlist = Service.getWeeksAndDateLabels(tu.scen);
        System.assertEquals(WandDlist.size(),2);
        
        List<Decimal> declist = Service.convertToPercentage(yValues);
        System.assertEquals(declist.size(),1);
        
        declist = Service.getSiteActivationData(tu.MSA);
        System.assertEquals(declist.size(),11);
        
        List<PBB_ScenarioDetailsController.wrapperQuestionResponsecls> responcesToApply = new List<PBB_ScenarioDetailsController.wrapperQuestionResponsecls>(); 
        PBB_ScenarioDetailsController.wrapperQuestionResponsecls wrcls = new  PBB_ScenarioDetailsController.wrapperQuestionResponsecls();
        wrcls.questionObj = tu.SMQ;
        wrcls.selectedValue = tu.SMR.id;
        wrcls.defaultResponse = null;
        responcesToApply.add(wrcls);
        Map<String, SRM_Model_Response__c> queResponseMap = new Map<String, SRM_Model_Response__c>();
        string key =tu.SMQ.id+''+tu.SMR.id;
        queResponseMap.put(key,tu.SMR);
        
        Decimal adjval = PBB_ScenarioService.adjustValuesOnQueResponceApply(10.0, 'Number_of_Weeks__c', responcesToApply,queResponseMap);
        System.assertEquals(adjval,10.0);
        adjval = PBB_ScenarioService.adjustValuesOnQueResponceApply(10.0, 'Adjust_FPI__c', responcesToApply,queResponseMap);
        System.assertEquals(adjval,10.0);
        
        adjval = PBB_ScenarioService.adjustValuesOnQueResponceApply(10.0, 'Adjust_LPI__c', responcesToApply,queResponseMap);
        System.assertEquals(adjval,15.0);
        adjval = PBB_ScenarioService.adjustValuesOnQueResponceApply(10.0, 'Subject_Enrollment_Rate__c', responcesToApply,queResponseMap);
        System.assertEquals(adjval,10.0);
        adjval = PBB_ScenarioService.adjustValuesOnQueResponceApply(10.0, 'Subject_Enroll_Adjustment__c', responcesToApply,queResponseMap);
        System.assertEquals(adjval,10.0);
        adjval = PBB_ScenarioService.adjustValuesOnQueResponceApply(10.0, 'Start_Week__c', responcesToApply,queResponseMap);  
        System.assertEquals(adjval,10.0);
        
        PBB_ScenarioDetailsController.countryresponsewrapper crw = new PBB_ScenarioDetailsController.countryresponsewrapper();
        crw.questionobj = tu.SMQ;
        crw.isCountrySelected = true;
        crw.selectedValue='none';
        crw.countries= tu.country.name;
        List<PBB_ScenarioDetailsController.countryresponsewrapper>  crwlist=  new List<PBB_ScenarioDetailsController.countryresponsewrapper>();
        crwlist.add(crw);
        List<PBB_ScenarioDetailsController.questionWrapper> questionWrapperresponcesToApply = new List<PBB_ScenarioDetailsController.questionWrapper>();    
        PBB_ScenarioDetailsController.questionWrapper wrclsqwr = new  PBB_ScenarioDetailsController.questionWrapper();
        wrclsqwr.indexQues = 1;
        wrclsqwr.questionObj = tu.SMQ; 
        wrclsqwr.country1queresponseList = new List<PBB_ScenarioDetailsController.countryresponsewrapper>();
        wrclsqwr.country1queresponseList.add(crw) ; 
        questionWrapperresponcesToApply.add(wrclsqwr);
        system.debug('----questionWrapperresponcesToApply---'+questionWrapperresponcesToApply);
        
        adjval = PBB_ScenarioService.adjustValuesOnQueResponceApply(10.0, 'Number_of_Weeks__c', questionWrapperresponcesToApply,queResponseMap,tu.country.name);
        System.assertEquals(adjval,10.0);
        adjval = PBB_ScenarioService.adjustValuesOnQueResponceApply(10.0, 'Adjust_FPI__c', questionWrapperresponcesToApply,queResponseMap,tu.country.name);        
        System.assertEquals(adjval,10.0);
        adjval = PBB_ScenarioService.adjustValuesOnQueResponceApply(10.0, 'Adjust_LPI__c', questionWrapperresponcesToApply,queResponseMap,tu.country.name);        
        System.assertEquals(adjval,10.0);
        adjval = PBB_ScenarioService.adjustValuesOnQueResponceApply(10.0, 'Subject_Enrollment_Rate__c', questionWrapperresponcesToApply,queResponseMap,tu.country.name);        
        System.assertEquals(adjval,10.0);
        adjval = PBB_ScenarioService.adjustValuesOnQueResponceApply(10.0, 'Subject_Enroll_Adjustment__c', questionWrapperresponcesToApply,queResponseMap,tu.country.name);
        System.assertEquals(adjval,10.0);
        adjval = PBB_ScenarioService.adjustValuesOnQueResponceApply(10.0, 'Start_Week__c', questionWrapperresponcesToApply,queResponseMap,tu.country.name);           
        System.assertEquals(adjval,10.0);
        
        Map<String, SRM_Model_Response__c> srmres = PBB_ScenarioService.getQuestionsResponses(null);
        System.assertEquals(srmres,null);
        
        srmres = PBB_ScenarioService.getQuestionsResponses(tu.sm.id);
        System.assertEquals(srmres.size(),1); 
        
        List<Decimal> AdjustedValues = Service.getAdjustedValuesPerDate(tu.scen,10,yValues,calendarList,'site');
        System.assertEquals(AdjustedValues.size(),2); 
        
        AdjustedValues = Service.getAdjustedValuesPerDate(tu.scen,10,yValues,calendarList,'subject');
        System.assertEquals(AdjustedValues.size(),2);  
        
        DateTime stdt = Service.getWeekStartDate(tu.SCA.From_Date__c);
        Decimal adjcal = Service.adjustCalendar(10, stdt, calendarList, 'site');
        System.assertEquals(adjcal,20.0);  
        
        adjcal = Service.adjustCalendar(10, stdt, calendarList, 'subject');
        System.assertEquals(adjcal,0);  
        
        List<Decimal> adjweeklist = Service.getAdjustedValuesPerWeek(tu.scen, 10, yValues,weekList, 'site');
        System.assertEquals(adjweeklist.size(),2);  
        adjweeklist = Service.getAdjustedValuesPerWeek(tu.scen, 10, yValues,weekList, 'subject');
        System.assertEquals(adjweeklist.size(),2);  
        
        SRM_Model_Site_Activation__c smsaa = PBB_ScenarioService.getObjValues(tu.MSA.id);
        System.assertEquals(smsaa.Id,tu.MSA.id);  
        
        Map<String,String> PrefixList = new Map<String,String>();
        PrefixList = Service.PrefixList();
        System.assertNotEquals(PrefixList.size(),0);
        
        String url = Service.ListViewURL(PrefixList,'SRM_Model_Response__c', 'All');
        System.assertEquals(url,'/a9l?fcf=000000000000000');
        
        SRM_Questions_Response__c SQR = new SRM_Questions_Response__c();
        SQR.PBB_Scenario__c=tu.scen.id;
        SQR.QuestionResponseUnique__c=tu.SMQ.id+''+tu.SMR.id;
        SQR.Country__c=tu.Country.id;
        SQR.SRM_Question_ID__c=tu.SMQ.id;
        SQR.SRM_Response_Id__c=tu.SMR.id;
        upsert SQR; 

        tu.sm.status__c = 'In-Progress';
        update tu.sm;
        tu.SMQ.Group__c ='Sponsor';
        upsert tu.SMQ; 
        
        Map<string,PBB_ScenarioService.QuestionresWrapper> QWdatamap = PBB_ScenarioService.getDataByGroup(tu.scen.id,tu.bid.id,'Contract');
        
        //System.assertEquals(QWdatamap.get(tu.smq.id).Role,'Proposal Manager');
        
        tu.SMQ.Group__c ='Country';
        upsert tu.SMQ; 
        Map<string,List<PBB_ScenarioService.CountryWrapper>>  QWcdatamap = PBB_ScenarioService.getDatabycountry(tu.scen.id,tu.bid.id);
        System.assertEquals(QWcdatamap.get(tu.smq.id)[0].Role,'Proposal Manager');
        
        tu.sm.status__c = 'Approved';
        update tu.sm;
        string  email = PBB_ScenarioService.getemailnotification(tu.scen.id,tu.bid.Sponsor__c,tu.bid.name, tu.bid.name);
        System.assertEquals(email,null);
        
        Map<String, SRM_Model_Site_Activation__c> siteMap = PBB_ScenarioService.getAllSiteActivations(tu.sm.Id); 
        System.assertEquals(siteMap.size(),1);
        
        Boolean check = PBB_ScenarioService.checkForCountryApprovedQuestion(questionWrapperresponcesToApply);
        System.assertEquals(check,true);
        
        check = PBB_ScenarioService.checkForApprovedQuestion(responcesToApply);
        System.assertEquals(check,true); 
        
        Map<String, SRM_Model_Questions__c> quesList =PBB_ScenarioService.getAllModelQuestions(tu.sm.Id); 
        System.assertEquals(quesList.size(),1); 
        
         Map<String, SRM_Model_Subject_Enrollment__c> subEnrollList = PBB_ScenarioService.getAllSubEnrollments(tu.sm.Id); 
        System.assertEquals(subEnrollList.size(),1); 
        
        Map<String, SRM_Model_Subject_Enrollment__c> subEnrollMap1 = new Map<String, SRM_Model_Subject_Enrollment__c>();
        tu.MSE.name ='MSE.name';
        subEnrollMap1.put(tu.MSE.id,tu.MSE);
        system.debug('----subEnrollMap1---'+subEnrollMap1);
        List<SelectOption> SubEnrollSeledctOptionList= PBB_ScenarioService.getSubEnrollSeledctOptionList(subEnrollMap1 );
        System.assertEquals(SubEnrollSeledctOptionList.size(),2); 
        
        Map<Integer, Decimal> sev = Service.getSubEnrollValues(tu.MSE, tu.scen, 10.0);        
        System.assertEquals(SubEnrollSeledctOptionList.size(),2); 
        
        PBB_ScenarioDetailsController.InnerClass ic = new PBB_ScenarioDetailsController.InnerClass();
        ic.countryName = tu.country.name;
        ic.noOfSites='10';  
        ic.siteActId = tu.MSA.id;
        ic.expSubEnrollRate  = 10.0;
        ic.subEnrollRate=10.0;
        ic.noOfSubjects='2';
        ic.comments='comments';
        ic.subEnrollment= tu.MSE.id;
        ic.height=1;
        ic.subjectsEnrolled='10';
        String bv = PBB_ScenarioService.checkForBlankValues(ic);
        System.assertEquals(bv,''); 
        
        tu.sm.Status__c = 'In-Progress';      
        upsert tu.sm;        
        String activeModelId = PBB_ScenarioService.getRecentActiveModel();
        System.assertEquals(activeModelId,null); 
    }   
}