/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

/**
*   'PRA_WBSProjectControllerTest' class is to cover the test coverage for 'PRA_WBSProjectController' class.
*   @author   Devaram Bhargav
*   @version  06-Nov-2013
*   @since    06-Nov-2013
*/
@isTest
private class PRA_WBSProjectControllerTest {
    
    static testMethod void testconstructor() {
        
        // given 
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        // Zero out all effort ratios
        List<Client_Task__c> ctList = [select id,project__r.name,Project__c,status__c
                                          from Client_Task__c
                                          where Project__r.id = :tu.clientProject.id
                                          ];
        ctList[0].status__c=PRA_Constants.CU_STATUS_NEW;
        ctList[1].status__c=PRA_Constants.CU_STATUS_READY;
        update ctList;
        // when
        test.startTest();
            PRA_WBSProjectController prawbs = new PRA_WBSProjectController();
            System.assertEquals(1, prawbs.ProjectsWBSList.size());
        	prawbs.Projectworkflow(); 
            System.assertEquals(null, prawbs.ProjectName);
            prawbs.ProjectName=tu.clientProject.Name;
            prawbs.Projectworkflow();        
        test.stopTest();        
    } 
}