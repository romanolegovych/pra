/**
@author 
@date 2014
@description this trigger to validate calendars
**/
public class SRM_ValidateModelCalendarsTriggerHandler{
    
    //Method to validate the calendars records for model
    public static String validateCalendars(List<SRM_Calender_Adjustments__c> calendarList, Map<Id, SRM_Calender_Adjustments__c> CalendarOldValues){
        Boolean isWeekEnd = true;
        String errorReason = '';
        
        List<SRM_Calender_Adjustments__c> existingCalendarList = [select Id, Name, SRM_Model__c, Country__c, From_Date__c, To_Date__c from SRM_Calender_Adjustments__c];
        if(!calendarList.isEmpty()){
            for(SRM_Calender_Adjustments__c cal: calendarList){
                System.debug('>>> cal >>> '+cal.Name);
                if(CalendarOldValues.isEmpty()){
                    System.debug('>>> Empty Old Values >>>');
                    isWeekEnd = isWeekEndDates(cal, existingCalendarList);
                    
                    //Check for date overlap if selected date is not week end date
                    if(isWeekEnd){
                        errorReason = 'WEEKEND';
                    }
                    else if(isDateOverlap(cal, existingCalendarList)){
                        errorReason = 'OVERLAP';
                    }
                    else{
                        errorReason = '';
                    }
                }
                else{
                    if(isValuesChanged(cal, CalendarOldValues)){
                    	
                        //do these if values are changed
                        //Check for sat and sunday
                        isWeekEnd = isWeekEndDates(cal, existingCalendarList);
                        //Check for date overlap if seleccted date is not week end date
                        if(isWeekEnd){
                            errorReason = 'WEEKEND';
                        }
                        else if(isDateOverlap(cal, existingCalendarList)){
                            errorReason = 'OVERLAP';
                        }
                        else{
                            errorReason = '';
                        }
                    }
                    else{
                        errorReason = '';
                    }
                }
            }
        }
        return errorReason;
    }
    
     /**
@author 
@date 2014
@description this Method to check whether values are changed for from date and to date
**/
    //Method to check whether values are changed for from date and to date
    public static boolean isValuesChanged(SRM_Calender_Adjustments__c calNewValues, Map<Id, SRM_Calender_Adjustments__c> CalendarOldValues){
        Boolean isChanged = false;
        if((!CalendarOldValues.isEmpty()) && (CalendarOldValues.keySet().contains(calNewValues.Id))){
            SRM_Calender_Adjustments__c calOldValues = CalendarOldValues.get(calNewValues.Id);            
            if(calNewValues != null){
                Boolean isFromDateChanged = (calNewValues.From_Date__c != calOldValues.From_Date__c);
                Boolean isToDateChanged = (calNewValues.To_Date__c != calOldValues.To_Date__c);
                Boolean isCountryChanged = (calNewValues.Country__c!=calOldValues.Country__c);
                if(isFromDateChanged || isToDateChanged || isCountryChanged){
                    isChanged = true;
                }
            }
        }
        return isChanged;
    }
    
    
     /**
@author 
@date 2014
@description this Check for overlapped dates
**/
    //Check for overlapped dates
    public static Boolean isDateOverlap(SRM_Calender_Adjustments__c cal, List<SRM_Calender_Adjustments__c> existingCalendarList){
        Date toWeekStart, toWeekEnd;
        String errorReason = '';
        Boolean isDateOverlap = false;
        Boolean isNewStartDateBetweenOld = false;
        Boolean isNewEndDateBetweenOld = false;
        Boolean isOldDateBetweenNew = false;
        if(!existingCalendarList.isEmpty()){
            for(SRM_Calender_Adjustments__c extCal: existingCalendarList){
                if((cal.SRM_Model__c.equals(extCal.SRM_Model__c)) && (cal.Country__c.equals(extCal.Country__c))){
                    if(cal.Id == extCal.Id){
                    	
                        //Don't check for current records if it is edited
                    }
                    else{
                        if(extCal.From_Date__c != null && extCal.To_Date__c != null){
                            toWeekStart = extCal.From_Date__c.toStartOfWeek().addDays(1);
                            toWeekEnd = extCal.To_Date__c.addDays(7).toStartOfWeek().addDays(-1);
                            //Check new records start and end date is between existinng records start and end date
                            isNewStartDateBetweenOld = ((cal.From_Date__c >= toWeekStart) && (cal.From_Date__c <= toWeekEnd));
                            isNewEndDateBetweenOld = ((cal.To_Date__c >= toWeekStart) && (cal.To_Date__c <= toWeekEnd));
                            
                            //Check if existinng start and end date is between new start and end date.
                            isOldDateBetweenNew = ((toWeekStart >= cal.From_Date__c) && (toWeekEnd <= cal.To_Date__c ));
                        }
                        if(isNewStartDateBetweenOld || isNewEndDateBetweenOld || isOldDateBetweenNew){
                            isDateOverlap = true;
                            //errorReason = 'OVERLAP';
                        }
                    }
                }
            }
        }
        return isDateOverlap;
    }
    
    
     /**
@author 
@date 2014
@description this method to check is week ends at perticular date
**/
    public static Boolean isWeekEndDates(SRM_Calender_Adjustments__c cal, List<SRM_Calender_Adjustments__c> existingCalendarList){
        Boolean isWeekend = false;
         System.debug('>>> In isWeekendDates >>>');
         if(cal.From_Date__c != null && cal.TO_Date__c != null){
            DateTime dt = cal.From_Date__c.addDays(1);
            DateTime ed = cal.To_Date__c.addDays(1);
            String stdt = dt.format('EEE, d MMM yyyy HH:mm:ss Z');
            String eddt = ed.format('EEE, d MMM yyyy HH:mm:ss Z');
            System.debug('>>> stdt>>> '+stdt+' >>> eddt>>> '+eddt);
            String stdtday = stdt.subString(0, stdt.IndexOf(','));
            String eddtday = eddt.subString(0, eddt.IndexOf(','));
            if((stdtday.startsWith('Sat') || stdtday.startsWith('Sun')) || (eddtday.startsWith('Sat') || eddtday.startsWith('Sun'))){
                isWeekend = true;
            }
        }
        return isWeekend;
    }
}