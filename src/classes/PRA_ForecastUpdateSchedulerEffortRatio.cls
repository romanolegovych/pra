public with sharing class PRA_ForecastUpdateSchedulerEffortRatio {
	
	public static final String THIS_CLASS_IS_DEPRACTED = 'Remove after Migration';
    public String testValue {get;set;}
    
    public PRA_ForecastUpdateSchedulerEffortRatio() {
    	testValue = THIS_CLASS_IS_DEPRACTED;
    }
}