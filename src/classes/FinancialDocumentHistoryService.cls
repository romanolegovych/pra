/** Implements the Service Layer of the object FinancialDocumentHistory__c
 * @author	Dimitrios Sgourdos
 * @version	15-Jan-2014
 */
public with sharing class FinancialDocumentHistoryService {
	
	/** Create and save a Financial Document History instance with the given info.
	 * @author	Dimitrios Sgourdos
	 * @version 15-Jan-2014
	 * @param	financialDocumentId		The id of the assigned financial document
	 * @param	userComments			The comments why the captured action happened
	 * @param	changeDescription		The description of the change that happened in the financial document
	 * @param	totalValue				The total value of the financial document at the moment
	 * @param	processStep				In which process the change happened (e.g. Price, Payment etc.)
	 * @return	If the instance was created and saved successfully.
	 */
	public static Boolean saveHistoryObject (String financialDocumentId,
											String userComments,
											String changeDescription,
											String totalValue,
											String processStep) {
		// Create and save the history instance
		FinancialDocumentHistory__c finDocHistory = new FinancialDocumentHistory__c(
																			ChangeDescription__c = changeDescription,
																			FinancialDocument__c = financialDocumentId,
																			ProcessStep__c = processStep,
																			TotalValue__c = totalValue,
																			UserComments__c = userComments );
		
		try{ // just in case
			insert finDocHistory;
		} catch (Exception e) {
			return false;
		}
		
		return true;
	}
}