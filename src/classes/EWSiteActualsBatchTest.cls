@istest public class EWSiteActualsBatchTest
{
	@istest private static void testBatch()
	{
		System.debug(PAWS_ApexTestsEnvironment.Project);
		System.debug(PAWS_ApexTestsEnvironment.FlowInstance);
		System.debug(PAWS_ApexTestsEnvironment.ProjectSite);
		PAWS_ApexTestsEnvironment.ProjectSite.Essential_Documents_Complete_Date__c = System.today();
		PAWS_ApexTestsEnvironment.ProjectSite.Site_Approvals_Complete_Date__c = System.today();
		PAWS_ApexTestsEnvironment.ProjectSite.Site_Contracts_Complete_Date__c = System.today();
		PAWS_ApexTestsEnvironment.ProjectSite.Start_Up_Requirements_Complete_Date__c = System.today();
		
		insert new Country__c(Name = PAWS_ApexTestsEnvironment.ProjectCountry.Name);
		
		update PAWS_ApexTestsEnvironment.ProjectSite;
		
		Test.startTest();
			//EWSiteActualsBatch.scheduleMe();
			String guid = EncodingUtil.convertToHex(Crypto.generateAesKey(128)).toUppercase();
			System.schedule(guid, '0 0 0 * * ?', new EWSiteActualsBatch(EWSiteActualsBatch.BASE_FLOW_INSTANCE_QUERY));
		Test.stopTest();
		
		System.assertEquals(1, new EWSiteActualsBatch.SiteActivationService().getActualSiteActivationInfo(new WFM_Protocol__c(SRM_Final_Protocol_Date__c = System.today().addDays(5), SRM_LPI_Date__c = System.today().addDays(10))).size());
	}
}