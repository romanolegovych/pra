/**
 * Created by illia on 12/9/15.
 */

public with sharing class EWCategoryEWJunctionTrigger extends STSWR1.AbstractTrigger
{

	Map<Schema.SObjectField, Schema.SObjectField> EW_TO_JUNCTION_FIELDS_MAP = new Map<Schema.SObjectField, Schema.SObjectField>{
			Critical_Chain__c.Buffer_Index__c => EW_Category_EW_Junction__c.Buffer_Index__c,
			Critical_Chain__c.Expected_Date_BI_0__c => EW_Category_EW_Junction__c.Expected_Date_BI_0__c,
			Critical_Chain__c.Expected_Date_BI_05__c => EW_Category_EW_Junction__c.Expected_Date_BI_05__c,
			Critical_Chain__c.Expected_Date_BI_1__c => EW_Category_EW_Junction__c.Expected_Date_BI_1__c
	};

	Map<Schema.SObjectField, Schema.SObjectField> CATEGORY_TO_JUNCTION_FIELDS_MAP = new Map<Schema.SObjectField, Schema.SObjectField>{
			EW_Category__c.Buffer_Index__c => EW_Category_EW_Junction__c.Buffer_Index__c,
			EW_Category__c.Expected_Date_BI_0__c => EW_Category_EW_Junction__c.Expected_Date_BI_0__c,
			EW_Category__c.Expected_Date_BI_05__c => EW_Category_EW_Junction__c.Expected_Date_BI_05__c,
			EW_Category__c.Expected_Date_BI_1__c => EW_Category_EW_Junction__c.Expected_Date_BI_1__c
	};

	public override void beforeInsert(List<SObject> records)
	{
		copyEWData((List<EW_Category_EW_Junction__c>) records);
	}

	public override void afterInsert(List<SObject> records)
	{
		updateCategoryRelatedRecordIDs((List<EW_Category_EW_Junction__c>) records);
	}

	public override void afterUpdate(List<SObject> records, List<SObject> priors)
	{
		updateCategoryRelatedRecordIDs((List<EW_Category_EW_Junction__c>) records);
	}

	public override void beforeUpdate(List<SObject> records, List<SObject> priors)
	{
		copyEWData((List<EW_Category_EW_Junction__c>) records);
	}

	 /**
    * Updates parent category Related_Record_ID__c value with Flow Source ID
     */
	private void updateCategoryRelatedRecordIDs(List<EW_Category_EW_Junction__c> ewJunctions)
	{
		Map<ID, EW_Category__c> categoryMap = new Map<ID, EW_Category__c>();
		for (EW_Category_EW_Junction__c ewJunction : [select Parent_Category__r.Related_Record_ID__c,
					Child_Category__c, Child_Category__r.Related_Record_ID__c,
					Parent_Category__r.Id, Parent_Category__c, Early_Warning__r.Flow__r.STSWR1__Source_ID__c
				from EW_Category_EW_Junction__c where Id in :ewJunctions])
		{
			if (ewJunction.Child_Category__c != null)
			{
				ewJunction.Parent_Category__r.Related_Record_ID__c = ewJunction.Child_Category__r.Related_Record_ID__c;
			}
			else if (ewJunction.Early_Warning__c != null)
			{
				ewJunction.Parent_Category__r.Related_Record_ID__c = ewJunction.Early_Warning__r.Flow__r.STSWR1__Source_ID__c;
			}

			categoryMap.put(ewJunction.Parent_Category__c, ewJunction.Parent_Category__r);
		}

		update categoryMap.values();
	}

	/**
	* Updates EW_Category_EW_Junction__c records with EW data (Buffer Index, Expected Dates, etc.)
	* from related EW or child Category.
	 */
	private void copyEWData(List<EW_Category_EW_Junction__c> ewJunctions)
	{
		List<ID> categoryIDs = new List<ID>();
		List<ID> ewIDs = new List<ID>();

		for (EW_Category_EW_Junction__c ewJunction : ewJunctions)
		{
			if (ewJunction.Early_Warning__c != null)
			{
				ewIDs.add(ewJunction.Early_Warning__c );
			}
			else if (ewJunction.Child_Category__c != null)
			{
				categoryIDs.add(ewJunction.Child_Category__c);
			}
		}

		Map<ID, SObject> sourceMap = new Map<ID, SObject>();
		if (ewIDs != null)
		{
			sourceMap.putAll(new Map<ID, SObject>([select Buffer_Index__c, Expected_Date_BI_0__c,
					Expected_Date_BI_05__c, Expected_Date_BI_1__c from Critical_Chain__c where Id in :ewIDs]));

		}

		if (categoryIDs != null)
		{
			sourceMap.putAll(new Map<ID, SObject>([select Buffer_Index__c, Expected_Date_BI_0__c,
					Expected_Date_BI_05__c, Expected_Date_BI_1__c from EW_Category__c where Id in :categoryIDs]));

		}

		for (EW_Category_EW_Junction__c ewJunction : ewJunctions)
		{
			if (ewJunction.Child_Category__c != null)
			{
				copyData(sourceMap.get(ewJunction.Child_Category__c), ewJunction, CATEGORY_TO_JUNCTION_FIELDS_MAP);
			}
			else if (ewJunction.Early_Warning__c != null)
			{
				copyData(sourceMap.get(ewJunction.Early_Warning__c), ewJunction, EW_TO_JUNCTION_FIELDS_MAP);
			}
		}
	}

	private void copyData(SObject source, EW_Category_EW_Junction__c target,
			Map<Schema.SObjectField, Schema.SObjectField> mapping)
	{
		if (source != null && target != null)
		{
			for (Schema.SObjectField sourceField : mapping.keyset())
			{
				Schema.SObjectField targetField = mapping.get(sourceField);
				target.put(targetField, source.get(sourceField));
			}
		}
	}
}