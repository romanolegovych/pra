public with sharing class COM_WFM_Site_DetailAfterInsertHandler extends TriggerHandlerBase {

	private static final String CLASSNAME = 'COM_WFM_Site_DetailAfterInsertHandler';
    private static final COM_Logger LOGGER = new COM_Logger(CLASSNAME);

	public override void mainEntry(TriggerParameters tp) {
		process((List<WFM_Site_Detail__c>)tp.newList);
	}
	
	private void process(List<WFM_Site_Detail__c> sites) {
		LOGGER.info('Processing new sites:'+sites.size());
		for(WFM_Site_Detail__c site : sites) {
			WFM_Site_Detail__c newSite = new WFM_Site_Detail__c();
			newSite.Id = site.Id;
			newSite.Status__c = 'New';
			//sObjectsToUpdate.put(newSite.Id, newSite);
		}
	}
	
}