global with sharing class PAWS_Scheduler implements Schedulable
{
	public String ClassControllerName;
	
	public PAWS_Scheduler(String classControllerName)
	{
		this.ClassControllerName = classControllerName;
	}
	
	public static void startScheduler(String classControllerName)
	{
		try{ System.schedule('PAWS Scheduler', '0 0 * * * ?', new PAWS_Scheduler(classControllerName)); }catch(Exception ex){}
	}
	
	global void execute(SchedulableContext ctx) 
	{
		Type classType = Type.forName(this.ClassControllerName);
		if(classType != null) ((PAWS_IScheduler)classType.newInstance()).schedulerExecute(ctx);
	}  
}