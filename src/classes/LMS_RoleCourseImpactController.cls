public class LMS_RoleCourseImpactController {

    public String courseTitle{get;private set;}
    public String courseId{get;set;}
    public String operation{get;set;}
    public String roleType{get;set;}
    public List<LMS_EmployeeList> empList{get;private set;}
    
    public LMS_RoleCourseImpactController() {
        String role;
        courseId = ApexPages.currentPage().getParameters().get('cid');
        System.debug('------------------------------------'+courseId);
        operation = ApexPages.currentPage().getParameters().get('type');
        roleType = ApexPages.currentPage().getParameters().get('role');
        
        if(roleType == 'PRA') {
            role = 'PRA';
        } else if(roleType == 'PST') {
            role = 'Project Specific';
        } else if(roleType == 'Adhoc') {
            role = 'Additional Role';
        }
        
        if(courseId != '') {
        	empList = getRoleCourseList(courseId, operation, role);
            courseTitle = getTitleByCourseId(courseId); 
        }
    }
    
    private static List<LMS_EmployeeList> getRoleCourseList(String courseId, String messageType, String roleType) {
        List<String> statusList = new List<String>();
        List<String> excludeList = new List<String>();
        Map<String, String> jobTitles = getJobTitleMap();
        if(messageType == '1') {
            statusList.add('Committed');
            statusList.add('Draft Delete');
            statusList.add('Pending - Delete');
        } else if(messageType == '2') {
            statusList.add('Draft');
            statusList.add('Draft Delete');
            statusList.add('Pending - Delete');
            statusList.add('Pending - Add');
        } else if(messageType == '3') {
            statusList.add('Committed');
            statusList.add('Draft');
            statusList.add('Pending - Add');
        }
        
        List<LMS_Role_Course__c> roleCourse = 
        [SELECT Role_Id__c,Course_Id__r.Title__c FROM LMS_Role_Course__c WHERE Course_Id__c = :courseId 
        	AND Status__c IN :statusList AND Role_Id__r.Role_Type__r.Name = :roleType];
        Set<Id> roleIds = new Set<Id>();
        for(LMS_Role_Course__c rc : roleCourse) {
            roleIds.add(rc.Role_Id__c);
        }
        
        List<LMS_Role_Employee__c> roleEmp = 
        [SELECT Employee_Id__r.First_Name__c,Employee_Id__r.Last_Name__c,Employee_Id__r.Job_Class_Desc__c,Employee_Id__r.Job_Code__c,
            Employee_Id__r.Business_Unit_Desc__c,Employee_Id__r.Department__c,Employee_Id__r.Country_Name__r.Region_Name__c,
            Employee_Id__r.Country_Name__r.Name,Employee_Id__r.Status_Desc__c,Assigned_On__c from LMS_Role_Employee__c where Role_Id__c IN :roleIds
            order by Employee_Id__r.Last_Name__c];
        List<LMS_EmployeeList> empList = new List<LMS_EmployeeList>();
        Set<String> empIds = new Set<String>();
        for(LMS_Role_Employee__c re : roleEmp) {
            if(!empIds.contains(re.Employee_Id__c)) {
                empList.add(new LMS_EmployeeList(re, jobTitles));
                empIds.add(re.Employee_Id__c);
            }
        }
        return empList;
    }
    
    private static Map<String, String> getJobTitleMap() {
        Map<String, String> jobTitles = new Map<String, String>();
        List<Job_Title__c> job = [select Job_Title__c, Job_Code__c from Job_Title__c];
        for(Job_Title__c j : job) {
            jobTitles.put(j.Job_Code__c, j.Job_Title__c);
        }
        return jobTitles;
    }
    
    private static String getTitleByCourseId(String cId) {
        LMS_Course__c course = [SELECT Title__c FROM LMS_Course__c WHERE Id = :cId];
        String title = course.Title__c;
        return title;
    }
}