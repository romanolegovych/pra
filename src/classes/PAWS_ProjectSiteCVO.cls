public with sharing class PAWS_ProjectSiteCVO
{
	public ID RecordID { get; private set; }
	public String Name { get; private set; }
	public String PI { get; private set; }
	
	public String PILastName { get; private set; }
	public String PIFirstName { get; private set; }
	
	public String StudyStartUpStatus { get; private set; }

	public DateTime StudyStartUpTargetDate
	{
		get;
		/*{
			return StudyStartUpTargetDate != null
					? DateTime.newInstanceGMT(StudyStartUpTargetDate.date(), StudyStartUpTargetDate.time())
					: StudyStartUpTargetDate;
		}*/
		private set
		{
			StudyStartUpTargetDate = value != null
				? DateTime.newInstanceGMT(value.date(), value.time())
				: null;
		}
	}

	public DateTime StudyStartUpCompleteDate
	{
		get;
		/*{
			return StudyStartUpCompleteDate != null
					? DateTime.newInstanceGMT(StudyStartUpCompleteDate.date(), StudyStartUpCompleteDate.time())
					: StudyStartUpCompleteDate;
		}*/
		private set
		{
			StudyStartUpCompleteDate = value != null
				? DateTime.newInstanceGMT(value.date(), value.time())
				: null;
		}
	}

	public DateTime StudyStartUpExpectedDateBI0
	{
		get;
		/*{
			return StudyStartUpExpectedDateBI0 !=null
					? DateTime.newInstanceGMT(StudyStartUpExpectedDateBI0.date(), StudyStartUpExpectedDateBI0.time())
					: StudyStartUpExpectedDateBI0;
		}*/
		private set
		{
			StudyStartUpExpectedDateBI0 = value != null
				? DateTime.newInstanceGMT(value.date(), value.time())
				: null;
		}
	}

	public DateTime StudyStartUpExpectedDateBI05
	{
		get;
		/*{
			return StudyStartUpExpectedDateBI05 != null
					? DateTime.newInstanceGMT(StudyStartUpExpectedDateBI05.date(), StudyStartUpExpectedDateBI05.time())
					: StudyStartUpExpectedDateBI05;
		}*/
		private set
		{
			StudyStartUpExpectedDateBI05 = value != null
				? DateTime.newInstanceGMT(value.date(), value.time())
				: null;
		}
	}

	public DateTime StudyStartUpExpectedDateBI1
	{
		get;
		/*{
			return StudyStartUpExpectedDateBI1 != null
					? DateTime.newInstanceGMT(StudyStartUpExpectedDateBI1.date(), StudyStartUpExpectedDateBI1.time())
					: StudyStartUpExpectedDateBI1;
		}*/
		private set
		{
			StudyStartUpExpectedDateBI1 = value != null
				? DateTime.newInstanceGMT(value.date(), value.time())
				: null;
		}
	}

	public String SiteApprovalsStatus { get; private set; }
	public String SiteApprovalsStatusDescription { get; private set; }
	public String EssentialDocumentsStatus { get; private set; }
	public String EssentialDocumentsStatusDescription { get; private set; }
	public String StartUpRequirementsStatus { get; private set; }
	public String StartUpRequirementsStatusDescription { get; private set; }
	public String SiteContractsStatus { get; private set; }
	public String SiteContractsStatusDescription { get; private set; }

	public PAWS_ProjectSiteCVO(PAWS_Project_Flow_Site__c inProjectSite)
	{
		RecordID = inProjectSite.ID;
		Name = inProjectSite.Name;
		PI = inProjectSite.PI_Name__c;
		
		if (inProjectSite.WFM_Site__c != null && inProjectSite.WFM_Site__r.Investigator__c != null)
		{
			PILastName = inProjectSite.WFM_Site__r.Investigator__r.Last_Name__c;
			PIFirstName = inProjectSite.WFM_Site__r.Investigator__r.First_Name__c;					
		}

		StudyStartUpExpectedDateBI0 = inProjectSite.Study_Start_Up_Expected_Date_BI_0__c;
		StudyStartUpExpectedDateBI05 = inProjectSite.Study_Start_Up_Expected_Date_BI_05__c;
		StudyStartUpExpectedDateBI1 = inProjectSite.Study_Start_Up_Expected_Date_BI_1__c;

		SiteApprovalsStatus = inProjectSite.Site_Approvals_Status__c;
		EssentialDocumentsStatus = inProjectSite.Essential_Documents_Status__c;
		StartUpRequirementsStatus = inProjectSite.Start_Up_Requirements_Status__c;
		SiteContractsStatus = inProjectSite.Site_Contracts_Status__c;
		StudyStartUpStatus = inProjectSite.Study_Start_Up_Status__c;

		EssentialDocumentsStatusDescription = inProjectSite.Essential_Documents_Status_Description__c;
		StartUpRequirementsStatusDescription = inProjectSite.Start_Up_Requirements_Status_Description__c;
		SiteContractsStatusDescription = inProjectSite.Site_Contracts_Status_Description__c;
		SiteApprovalsStatusDescription = inProjectSite.Site_Approvals_Status_Description__c;
		
		StudyStartUpCompleteDate = inProjectSite.Study_Start_Up_Complete_Date__c;
		StudyStartUpTargetDate = inProjectSite.Study_Start_Up_Target_Date__c;
	}
}