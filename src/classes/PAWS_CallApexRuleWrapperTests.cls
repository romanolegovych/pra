/**
*   'PAWS_CallApexRuleWrapperTests' is the test class for PAWS_CallApexRuleWrapper class
*   Test classes are used to write the unit test and cover the code coverage of the code.
*   @author   Work Relay
*/
@isTest 
public with sharing class PAWS_CallApexRuleWrapperTests 
{
	static testmethod void run()
    {
    	PAWS_ApexTestsEnvironment.init();
    	
    	STSWR1__Flow_Instance_Cursor__c cursor = (STSWR1__Flow_Instance_Cursor__c)new STSWR1.API().call('WorkflowService', 'getFlowInstanceCursorById', PAWS_ApexTestsEnvironment.FlowInstanceCursor.Id); 

    	PAWS_ApexTestsEnvironment.Flow.STSWR1__Source_Id__c = PAWS_ApexTestsEnvironment.Project.Id;
    	update PAWS_ApexTestsEnvironment.Flow;
    	
    	
		Map<String, String> config = new Map<String, String>
		{
			'object' => 'ecrf__c',
			'flow' => PAWS_ApexTestsEnvironment.Flow.Name,
			'step' => PAWS_ApexTestsEnvironment.FlowStep.Name,
			'statuses' => 'Not Started',
			'condition' => '>:1'
		};

    	new PAWS_CallApexSCFRuleHandler().run(PAWS_ApexTestsEnvironment.Project, cursor, JSON.serialize(config));
    	
    	config = new Map<String, String>
		{
			'object' => 'ecrf__c',
			'flow' => PAWS_ApexTestsEnvironment.Flow.Name,
			'step' => PAWS_ApexTestsEnvironment.FlowStep.Name,
			'statuses' => 'Not Started'
		};

    	new PAWS_CallApexBCFRuleHandler().run(PAWS_ApexTestsEnvironment.Project, cursor, JSON.serialize(config));
    }
}