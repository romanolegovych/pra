public with sharing class BDT_FinancialDocDownloadCOController {
	public FinancialDocument__c		financialDocument				{get;set;}
	public String 					financialDocumentID				{get;set;} 
	// PDF document additionals 
	public String					paperSize 						{get;set;}
	public String					currentDateString 				{get;set;}
	public String					currentDateCoverPageString 		{get;set;}
	public Boolean					showWarningTextFlag				{get;set;}
	// Associated objects and wrappers
	public List<Study__c>				studiesList					{get;set;}
	public List<Study__c>				parentStudiesList			{get;set;}
	public FinancialDocumentContent__c	finDocContent				{get;set;}
	public Map<String,BDT_DC_FinancialDocument.financialDocumentContentEntriesWrapper> contentEntriesMap 				{get;set;}
	public List<BDT_DC_FinancialDocument.passThroughServicesWrapper> passThroughServiceWrapperList		 				{get;set;}
	public List<BDT_DC_FinancialDocument.priceWrapper>				 passThroughServiceTotalsList		 				{get;set;}
	public List<BDT_DC_FinancialDocument.serviceCategoryWrapper> 	 serviceCategoryWrapperList			 				{get;set;}
	public List<BDT_DC_FinancialDocument.serviceCategoryPriceTotalsWrapper> serviceCategoryPriceTotalsWrapperList		{get;set;}
	public List<Boolean>											 collapseDiscountColumnFlagList						{get;set;} 
	public List<BDT_DC_FinancialDocument.serviceCategoryWrapper> 	 parentServiceCategoryWrapperList	 				{get;set;}
	public List<BDT_DC_FinancialDocument.serviceCategoryPriceTotalsWrapper> parentServiceCategoryPriceTotalsWrapperList {get;set;}
	public List<Boolean>											 parentCollapseDiscountColumnFlagList				{get;set;}
	public List<BDT_DC_FinancialDocument.CoAndProposalDeltaWrapper>	 CoAndProposalDeltaWrapperList		 				{get;set;}
	public List<BDT_DC_FinancialDocument.serviceCategoryPriceTotalsWrapper> CoAndProposalDeltaTotalsWrapperList 		{get;set;}
	public List<BDT_DC_FinancialDocument.paymentTermsWrapper> 		 paymentTermsWrapperList 			 				{get;set;}
	public List<String>												 paymentTermTotalsList								{get;set;}
	public List<BDT_DC_FinancialDocument.servicesAndCostsReportStudyWrapper> servicesAndCostsStudiesReportList			{get;set;}
	
	
	/* Class constructor
	 * @author	Dimitrios Sgourdos
	 * @version	17-Sep-2013
	 */	
	public BDT_FinancialDocDownloadCOController() {
		if( initializeGlobalVariables() ) {
			studiesList = BDT_DC_FinancialDocument.readFinancialDocumentStudies(financialDocument);
			readParentStudies(financialDocument);
			contentEntriesMap = BDT_DC_FinancialDocument.readContentSelectionEntriesChangeOrder(financialDocument);
			finDocContent = BDT_FinancialDocumentContentService.getContentByDocumentId(financialDocumentId);
			readServicesAndCostsSectionData();
			readPassThroughServicesData();
			readServiceCategoriesPrices();
			readPaymentTermsData();
		}
	}
	
	
	/**
	*	Initialize the global variables of the class.
	*	@author   Dimitrios Sgourdos
	*	@version  16-Sep-2013
	*	@return   If everything went well
	*/
	private Boolean initializeGlobalVariables() {
		// Initialize lists and maps
		studiesList						 	= new List<Study__c>();
		parentstudiesList				 	= new List<Study__c>();
		finDocContent		   				= new FinancialDocumentContent__c();
		contentEntriesMap 				 	= new Map<String,BDT_DC_FinancialDocument.financialDocumentContentEntriesWrapper>();
		passThroughServiceWrapperList 	 	= new List<BDT_DC_FinancialDocument.passThroughServicesWrapper>();
		passThroughServiceTotalsList  	 	= new  List<BDT_DC_FinancialDocument.priceWrapper>();
		serviceCategoryWrapperList		 	= new List<BDT_DC_FinancialDocument.serviceCategoryWrapper>(); 
		serviceCategoryPriceTotalsWrapperList = new List<BDT_DC_FinancialDocument.serviceCategoryPriceTotalsWrapper>(); 
		parentServiceCategoryWrapperList 	= new List<BDT_DC_FinancialDocument.serviceCategoryWrapper>(); 
		parentServiceCategoryPriceTotalsWrapperList = new List<BDT_DC_FinancialDocument.serviceCategoryPriceTotalsWrapper>(); 
		collapseDiscountColumnFlagList 	 	= new List<Boolean>(); 
		parentCollapseDiscountColumnFlagList= new List<Boolean>(); 
		CoAndProposalDeltaWrapperList	 	= new List<BDT_DC_FinancialDocument.CoAndProposalDeltaWrapper>();
		CoAndProposalDeltaTotalsWrapperList = new List<BDT_DC_FinancialDocument.serviceCategoryPriceTotalsWrapper>();
		paymentTermsWrapperList 		  	= new List<BDT_DC_FinancialDocument.paymentTermsWrapper>();
		paymentTermTotalsList			  	= new List<String>();
		servicesAndCostsStudiesReportList 	= new List<BDT_DC_FinancialDocument.servicesAndCostsReportStudyWrapper>(); 
		// Current date
		currentDateCoverPageString = BDT_DC_FinancialDocument.formatCurrentDateAnalytical();
		currentDateString		   = BDT_DC_FinancialDocument.formatCurrentDate();
		// Pdf paper size
		paperSize=system.currentPageReference().getParameters().get('selectedPaperSize');
		paperSize = ( String.IsBlank(paperSize) )? 'A4' : paperSize;
		// Find the current financial document
		financialDocumentID = system.currentPageReference().getParameters().get('financialDocumentID'); 	
		if(String.isBlank(financialDocumentID)) {
			errorOnReadingFinancialDocument();
			return false;
		} else {
			financialDocument = BDT_DC_FinancialDocument.findFinancialDocument(financialDocumentId);
			if( String.isBlank(financialDocument.Name) ) {
				errorOnReadingFinancialDocument();
				return false;
			}
		} 
		// Warning message
		showWarningTextFlag = BDT_DC_FinancialDocument.showWarningMessagesInPDF(financialDocument);
		// Everything went well
		return true;
	}
	
	
	/**
	*	Give an error message in case the financial document is not found.
	*	@author   Dimitrios Sgourdos
	*	@version  16-Sep-2013
	*/
	private void errorOnReadingFinancialDocument() {
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected financial document.Please, select a document and reload the page.');
		ApexPages.addMessage(msg);
	}
	
	
	/**
	*	Calculate the wrapper list for the services and costs reports.
	*	@author   Dimitrios Sgourdos
	*	@version  25-Sep-2013
	*/
	public void readServicesAndCostsSectionData(){
		// Read the services and costs for the parent proposal
		List<BDT_DC_FinancialDocument.servicesAndCostsReportStudyWrapper> parentServicesAndCostsStudiesReportList = new List<BDT_DC_FinancialDocument.servicesAndCostsReportStudyWrapper>();
		parentServicesAndCostsStudiesReportList = BDT_DC_FinancialDocument.readServicesAndCostsSectionData(financialDocument.FinancialDocumentParent__c, parentStudiesList, false);
		// Create a map for the StudyServicePricing records of the parent proposal
		List<StudyServicePricing__c> tmpParentSspList = new List<StudyServicePricing__c>();
		for(BDT_DC_FinancialDocument.servicesAndCostsReportStudyWrapper tmpReportItem : parentServicesAndCostsStudiesReportList) {
			for(BDT_DC_FinancialDocument.servicesAndCostsReport tmpSrvCatWrapperItem : tmpreportItem.servicesAndCostsReportList) {
				for(BDT_DC_FinancialDocument.studyServicePricingWrapper tmpSspWrItem : tmpSrvCatWrapperItem.sspWrapperList) {
					tmpParentSspList.add(tmpSspWrItem.studyServicePricing);
				}
			}
		}
		Map<String,StudyServicePricing__c> tmpParentSspMap = new Map<String,StudyServicePricing__c>(); 
		tmpParentSspMap = BDT_DC_FinancialDocument.createStudyServicePricingMapWithKeyStudyIdAndService(tmpParentSspList);
		// Read the services and costs for the change order
		servicesAndCostsStudiesReportList = BDT_DC_FinancialDocument.readServicesAndCostsSectionData(financialDocumentId, studiesList, false);
		// Add the difference
		for(BDT_DC_FinancialDocument.servicesAndCostsReportStudyWrapper tmpReportItem : ServicesAndCostsStudiesReportList) {
			for(BDT_DC_FinancialDocument.servicesAndCostsReport tmpSrvCatWrapperItem : tmpreportItem.servicesAndCostsReportList) {
				for(Integer i=0; i<tmpSrvCatWrapperItem.sspWrapperList.size(); i++) {
					String Key = tmpSrvCatWrapperItem.sspWrapperList[i].studyServicePricing.studyService__r.study__c + ':' + tmpSrvCatWrapperItem.sspWrapperList[i].studyServicePricing.Service__c;
					if( tmpParentSspMap.containsKey(Key) ) {
						StudyServicePricing__c tmpSSP = tmpParentSspMap.get(Key);
						if( BDT_DC_financialDocument.findEqualityBetweenStudyServicePricings(tmpSrvCatWrapperItem.sspWrapperList[i].studyServicePricing, tmpSSP) ) {
							tmpSrvCatWrapperItem.sspWrapperList.remove(i);
							i--; // to avoid skipping the next value, as one value is removed
						} else {
							tmpSrvCatWrapperItem.sspWrapperList[i].parentStudyServicePricing = tmpSSP;
						}
					} // end of if( tmpParentSspMap...
				} // end of for(Integer...
			} // end of second for
		} // end of first for
		// Show or hide service categories
		for(BDT_DC_FinancialDocument.servicesAndCostsReportStudyWrapper tmpReportItem : ServicesAndCostsStudiesReportList) {
			for(Integer i=0; i<tmpReportItem.servicesAndCostsReportList.size(); i++) {
				tmpReportItem.servicesAndCostsReportList[i].showInCOReport = BDT_DC_FinancialDocument.showServiceCategoryInReport(tmpReportItem.servicesAndCostsReportList, tmpReportItem.servicesAndCostsReportList[i], i);
			}
		}
	}
	
	
	/**
	*	Read the service categories prices for the parent proposal and the current change order document (Financial Summary - Prices section).
	*	@author   Dimitrios Sgourdos
	*	@version  20-Sep-2013
	*/
	public void readServiceCategoriesPrices() {
		// Parent proposal
		parentServiceCategoryWrapperList = BDT_DC_FinancialDocument.readServiceCategoriesPricesForPDF(financialDocument.FinancialDocumentParent__c, parentStudiesList);
		parentServiceCategoryPriceTotalsWrapperList = BDT_DC_FinancialDocument.calculateServiceCategoryPricesTotals(parentServiceCategoryWrapperList, parentStudiesList.size());
		parentCollapseDiscountColumnFlagList = BDT_DC_FinancialDocument.collapseDeltaColumn(parentServiceCategoryPriceTotalsWrapperList);  
		// Change Order
		serviceCategoryWrapperList = BDT_DC_FinancialDocument.readServiceCategoriesPricesForPDF(financialDocumentId, StudiesList);
		serviceCategoryPriceTotalsWrapperList = BDT_DC_FinancialDocument.calculateServiceCategoryPricesTotals(serviceCategoryWrapperList, studiesList.size());
		collapseDiscountColumnFlagList = BDT_DC_FinancialDocument.collapseDeltaColumn(serviceCategoryPriceTotalsWrapperList);  
		// Calculate Difference between proposal and change order
		CoAndProposalDeltaWrapperList = BDT_DC_FinancialDocument.findPriceDeltaBetweenChangeOrderAndProposal(serviceCategoryWrapperList, studiesList, parentServiceCategoryWrapperList, parentStudiesList);
		CoAndProposalDeltaTotalsWrapperList = BDT_DC_FinancialDocument.findPriceTotalsDeltaBetweenChangeOrderAndProposal(serviceCategoryPriceTotalsWrapperList, studiesList, parentServiceCategoryPriceTotalsWrapperList, parentStudiesList);
	}
	
	
	/**
	*	Read the Payment Terms data from the parent proposal for the studies that are selected in the change order document.
	*	@author   Dimitrios Sgourdos
	*	@version  17-Sep-2013
	*/
	private void readPaymentTermsData() {
		paymentTermsWrapperList = BDT_DC_FinancialDocument.readPaymentTermsForPDF(financialDocument.FinancialDocumentParent__c, StudiesList); 
		paymentTermTotalsList   = BDT_DC_FinancialDocument.calculatePaymentTermSumsForPDF(paymentTermsWrapperList, StudiesList.size());
	}	
	
	
	/**
	*	Read the studies of the parent proposal.
	*	@author   Dimitrios Sgourdos
	*	@version  17-Sep-2013
	*/
	private void readParentStudies(FinancialDocument__c childFinDoc) {
		FinancialDocument__c parentFinDoc = BDT_DC_FinancialDocument.findFinancialDocument(childFinDoc.FinancialDocumentParent__c);
		if( ! String.isBlank(parentFinDoc.Name) ) {
			parentStudiesList = BDT_DC_FinancialDocument.readFinancialDocumentStudies(parentFinDoc);
		}
	}
	
	
	/**
	*	Read the Pass Through Services data for the parent proposal and the current change order document.
	*	@author   Dimitrios Sgourdos
	*	@version  17-Sep-2013
	*/
	private void readPassThroughServicesData() {
		passThroughServiceWrapperList = BDT_DC_FinancialDocument.readPassThroughServicesForPDF(parentStudiesList);
		passThroughServiceWrapperList = BDT_DC_FinancialDocument.addChildDocumentPassThroughServicesForPDF(parentStudiesList, StudiesList, passThroughServiceWrapperList);
		passThroughServiceTotalsList  = BDT_DC_FinancialDocument.calculatePassThroughServicesSumsForPDF(passThroughServiceWrapperList, parentStudiesList.size()+studiesList.size());
	}
	
	
}