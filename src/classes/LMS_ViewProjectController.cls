global class LMS_ViewProjectController{
        
    /************************************** 
     *  Code for : CourseToPRAController  *
     *  Project : LMS                     *    
     *  Author : Andrew Allen             * 
     *  Last Updated Date : 6/7/2012     * 
     *************************************/
     
    // Instance Variables
    public String roleName{get;set;}
    public String roleNameStyle{get;private set;}
    public String roleFilter{get;private set;}
    public String clientText{get;set;}
    public String projectText{get;set;}
    public String projectRole{get;set;}
    public String projectFilter{get;set;}
    public String roleErrorText{get;private set;}
    public String courseToRoleId{get;private set;}
    public String webServiceError{get;private set;}
    public Boolean allChecked{get;set;}
    public Boolean renderBlock{get;set;}
    public Boolean renderError{get;set;}
    public Boolean disableRole{get;set;}
    public Boolean renderExport{get;set;}
    public Boolean bWebServiceError{get;private set;}
    public Boolean bSyncAction{get;private set;}
    
    // Collections
    public List<LMS_RoleWrapper> roleList{get;set;}
    private List<LMS_Role__c> roles{get;set;}
    private Map<String, LMS_Role__c> roleMap{get;set;}
    private Map<String, CourseDomainSettings__c> settings{get;set;}
    private Map<String, LMSConstantSettings__c> constants{get;set;}
    private Map<String, LMSCustomTabSettings__c> tabIds{get;set;}
    
    // Error Varaibles
    public List<String> sfdcErrors{get;private set;}
    public List<String> roleError{get;private set;}
    public List<String> calloutErrors{get;private set;}
    
    /**
     *  Constructor
     *  Initializes all data
     */
    public LMS_ViewProjectController() { 
        initSettings();
        initVals();
    }
    
    /**
     *  Initialize course search data
     */
    private void initVals() {
        renderBlock = false;
        bWebServiceError = false;
        clientText = '';
        projectText = '';
        roleName = 'Enter Role Name';
        roleNameStyle = 'WaterMarkedTextBox';
        //Setting Role filter
        roleFilter = ' and Role_Type__r.Name = \'Project Specific\' and Project_Id__r.Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')';        
        // Setting Project filter
        projectFilter = ' and Client_Id__c != \'\' and Client_Id__c != null and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')';        
        courseToRoleId = tabIds.get('CourseToRole').tabId__c;
    }
    
    /**
     *  Get constants from custom settings
     */
    private void initSettings() {
        settings = CourseDomainSettings__c.getAll();
        constants = LMSConstantSettings__c.getAll();
        tabIds = LMSCustomTabSettings__c.getAll();
    }
    
    @RemoteAction
    global static Boolean isValidClientId(String clientId) {
        Boolean isValid;
        try {
            WFM_Client__c client = [SELECT Name FROM WFM_Client__c WHERE Name = :clientId];
            if(client != null)
            	isValid = true;
        } catch(Exception e) {
            System.debug(e.getMessage());
            isValid = false;
        }
        return isValid;
    }
    
    @RemoteAction
    global static Boolean isValidProjectId(String projectId) {
        Boolean isValid;
        try {
            WFM_Project__c project = [SELECT Name FROM WFM_Project__c WHERE Name = :projectId];
            if(project != null) {
            	isValid = true;
            }
        } catch(Exception e) {
            System.debug(e.getMessage());
            isValid =  false;
        }
        return isValid;
    }
    
    @RemoteAction
    global static Boolean isValidClientProjectPair(String clientId, String projectId) {
        Boolean isValid;
        try {
            WFM_Project__c project = [SELECT Client_Id__c FROM WFM_Project__c WHERE Name = :projectId];
            if(project.Client_ID__c == clientId) {
                isValid = true;
            } else {
                isValid = false;
            }
                
        } catch(Exception e) {
            System.debug(e.getMessage());
            isValid = false;
        }
        return isValid;
    }
    
    /**
     * Changes autocomplete filter from ui input
     */
    public void projSearchType() {
        if(clientText != '' && projectText == '') {
            projectFilter = ' and Client_Id__c != \'\' and Client_Id__c != null and Client_Id__c = \'' + clientText + '\''
            	+ ' and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')';
        } else if((clientText != '' && projectText != '') || (clientText == '' && projectText != '')) {
            projectFilter = ' and Client_Id__c != \'\' and Client_Id__c != null and Client_Id__c = \'' + clientText + '\''
            	+ ' and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')';
            List<WFM_Project__c> clientName = [select name,Client_Id__c from WFM_Project__c where Name =:projectText];
            if(clientName.size() > 0)
            clientText = string.valueof(clientName[0].Client_Id__c);
            System.debug('-------------------client text--------------------' + clientText);
        }
    }
    
    /**
     * Reset AutoComplete component
     */
    public void resetAC() {
        String pstDomain = settings.get('PST').Domain_Id__c;
        clientText='';
        projectText='';
        projectFilter = 'and Client_Id__c != \'\' and Client_Id__c != null and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')';
    }
    
    /**
     *  Project Role/Job Class Desc LOV
     */
    public List<selectoption> getProjectRoles() {
        List<selectoption> projectRole = new List<selectoption>();
        projectRole = LMS_LookUpDataAccess.getAllPSTRoles(true);
        return projectRole;
    }
    
    /** 
     *  Search Implementation method :
     *  This method uses the UI role criteria to search for the role
     *  and the courses that have been assigned to that role 
     */
    public void search() {
        Boolean acSearch;
        roleMap = new Map<String, LMS_Role__c>();
        
        String draft = constants.get('statusDraft').Value__c;
        String committed = constants.get('statusCommitted').Value__c;
        String draftDelete = constants.get('statusDraftDelete').Value__c;
        String removed = constants.get('statusRemoved').Value__c;
        
        //Join query to get course information on a certain role
        String courseQry = 'select Name from Rolecourses__r where Status__c != :removed';
        //Join query to get employee information for course
        String employeeQry = 'select Name from RoleEmployees__r where Status__c != :removed';
        //Main query to pull the role based on criteria in UI
        String roleQry = 'select CreatedBy.Name,LastModifiedBy.Name,CreatedDate,LastModifiedDate, Role_Name__c,Role_Type__r.Name,Employee_Count__c,Sync_Status__c,Status__c,SABA_Role_PK__c,(' + courseQry + '),(' + employeeQry + ') ' + 
            'from LMS_Role__c where Role_Type__r.Name = \'Project Specific\' and Project_Id__r.Status_Desc__c NOT IN (\'Lost\',\'Closed\') ';
        
        if(null != roleName && roleName != '' && roleName != 'Enter Role Name') {
            roleQry += 'and Role_Name__c LIKE \'' + String.escapeSingleQuotes(roleName) + '%\' ';
            acSearch = true;
        } else {
            acSearch = false;
            List<WFM_Project__c> project = new List<WFM_Project__c>();
            string id = '';
            if(projectText != '') {
                project = [select Client_Id__c from WFM_Project__c where Name =:projectText];
                id = project[0].Client_Id__c;
            }
            //Check for each type of criteria
            if((clientText=='' && projectText!='') || (clientText!='' && projectText!=''))
                roleQry+= 'and Project_Id__r.Name = :projectText and Client_Id__r.Name = :id ';
            else if(clientText!='' && projectText=='')
                roleQry += 'and Project_Id__c = null and Client_Id__r.Name = :clientText ';
                    
            if(projectRole != null)
                roleQry += 'and (Job_Class_Desc__r.Name = :projectRole OR PST_Adhoc_Role__r.PST_Adhoc_Role__c = :projectRole) ';
        }
        roleQry += 'order by Role_Name__c';
        allChecked = false;
        
        roles = Database.query(roleQry);
        roleList = new List<LMS_RoleWrapper>();
        ///If no role exists do not render table & display message
        for(LMS_Role__c r : roles) {
            roleList.add(new LMS_RoleWrapper(r));
            roleMap.put(r.Id, r);
        }
        if(roles.size() == 0) {
            if(acSearch) {
                renderError = true;
                renderBlock = false;
                roleErrorText = 'Role either contains a bid, lost, or closed project or could not be found, please enter a different role name';
            } else {
                renderError = true;
                renderBlock = false;
                roleErrorText = 'Role either contains a bid, lost, or closed project or could not be found, please enter different criteria';
            }
        } else {
            renderBlock = true;
            renderError = false;
        }
    }
    
    /**
     *  Method to reset role information
     */
    public void roleReset() {
                
        renderError = false;
        renderBlock = false;
        bWebServiceError = false;
        
        initVals();
    }
    
    public PageReference activateRoles() {
        LMS_ToolsService.activateRole(roleList, roleMap);
        search();
        return null;
    }
    
    public PageReference disableRoles() {
        LMS_ToolsService.inactivateRole(roleList, roleMap);
        search();
        return null;
    }
    
    public PageReference showErrors() {
        PageReference pr = new PageReference('/apex/LMS_ViewProjectError');
        pr.setRedirect(false);
        return pr;
    }
    
    public PageReference invokeExportRoles(){
        System.debug('****exp1***'+roleList);
        PageReference pr = new PageReference('/apex/LMS_ProjectExportRoles');
        pr.setRedirect(false);
        return pr;
    }
    
    public List<LMS_RoleWrapper> getexportRole() {
        System.debug('****exp2***'+roleList);
        return roleList;        
    }
    
}