/**
 * @description	The Service Layer for the Email functionalities
 * @author		Dimitrios Sgourdos
 * @date		Created: 01-Oct-2015, Edited: 02-October-2015
 */
public with sharing class NCM_SrvLayer_Email {
	
	/**
	 * @description	Find the id of the org wide email address that corresponds to the given address.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 02-Oct-2015
	 * @param		emailAddress      The email address that its id will be found
	 * @return		The id of the retrieved org wide email address.
	*/
	public static Id getOrgWideEmailAddressIdFromAddressName(String emailAddress) {
		Id result;
		try {
			OrgWideEmailAddress orgAddress = [SELECT Id FROM OrgWideEmailAddress WHERE Address = :emailAddress LIMIT 1];
			result = orgAddress.Id;
		} catch (Exception e) {
			result = NULL;
		}
		
		return result;
	}
	
	
	/**
	 * @description	Create an email instance with the given info.
	 * @author		Jegadheesan Muthusamy, Dimitrios Sgourdos
	 * @date		Created: 11-Sep-2015, 02-Oct-2015
	 * @param		receiptToId      The id of the user that will be the recipient of the email
	 * @param		orgWideFromAddressId    The from address of the mail (if different than NULL)
	 * @param		emailSubject     The email subject
	 * @param		emailBodyHTML    The email body
	 * @param		attachFile       The attachment of the mail (if different than NULL)
	 * @return		The created e-mail instance.
	*/
	public static Messaging.SingleEmailMessage createEmailInstance(	Id receiptToId,
																	Id orgWideFromAddressId,
																	String emailSubject,
																	String emailBodyHTML,
																	Messaging.EmailFileAttachment attachFile) {
		// Create email instance
		Messaging.SingleEmailMessage mailInstance = new Messaging.SingleEmailMessage();
		
		mailInstance.setTargetObjectId(receiptToId);
		
		if(orgWideFromAddressId != NULL) {
			mailInstance.setOrgWideEmailAddressId(orgWideFromAddressId);
		}
		
		mailInstance.setSaveAsActivity(false);
		mailInstance.setSubject(emailSubject);
		mailInstance.setHtmlBody(emailBodyHTML);
		
		if(attachFile != NULL) {
			mailInstance.setFileAttachments(new Messaging.EmailFileAttachment[] {attachFile});
		}
		
		return mailInstance;
	}
	
	
	/**
	 * @description	Send the given email instances
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015
	 * @param		notificationEmailsList    The given email instances to be sent
	*/
	public static void sendEmailInstances(List<Messaging.SingleEmailMessage> notificationEmailsList) {
		// Check for invalid parameters
		if(notificationEmailsList == NULL || notificationEmailsList.isEmpty()) {
			return;
		}
		
		// Send notification emails 
		try {
			Messaging.sendEmail(notificationEmailsList);
		} catch(Exception e) {
			system.debug('Failed to send email notifications: ' + e.getMessage());
		}
	}
}