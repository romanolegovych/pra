@isTest
private class BDT_UserPreferenceDataAccessorTest {

	static testMethod void getAllUsersPreferencesTest() {

		Profile p = [select id from profile where name='System Administrator'];
		list<user> userList = new list<user>();
		userList.add(new User(alias = 'user1', email='sysadmin@testorg.com',
			emailencodingkey='UTF-8', lastname='User1', languagelocalekey='en_US',
			localesidkey='en_US', profileid = p.Id,
			timezonesidkey='America/Los_Angeles', username='bdtTestUser1@testorg.com'));
		userList.add(new User(alias = 'user2', email='sysadmin@testorg.com',
			emailencodingkey='UTF-8', lastname='User2', languagelocalekey='en_US',
			localesidkey='en_US', profileid = p.Id,
			timezonesidkey='America/Los_Angeles', username='bdtTestUser2@testorg.com'));

		insert userList;

		list<BDT_UserPreference__c> prefList = new list<BDT_UserPreference__c>();
		prefList.add(new BDT_UserPreference__c(name = 'Test', Text__c = 'text for user 1', ownerid = userList[0].id));
		prefList.add(new BDT_UserPreference__c(name = 'Test', Text__c = 'text for user 2', ownerid = userList[1].id));
		insert prefList;

		system.runAs(userList[0]) {
			List<BDT_UserPreference__c> result = BDT_UserPreferenceDataAccessor.getAllUsersPreferences();
			system.assertEquals(1,result.size());
			system.assertEquals('text for user 1',result[0].Text__c);
		}

	}

}