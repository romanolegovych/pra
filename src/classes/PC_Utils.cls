/**
@author Kostya Hladkyi
@date 10/09/2015
@description utility class for Mobile project
**/
public with sharing class PC_Utils {
	
	public static Id getIdFromString(String idValue, String errorMessageSubject){
    	if ( String.isBlank(idValue) ){
    		throw new PC_Exception('Passed ' + errorMessageSubject + ' is empty');
    	}
		try{
			return (Id)idValue;
		}catch(Exception e){
			throw new PC_Exception('Passed ' + errorMessageSubject + ' is incorrect: "' + idValue + '"');
		}
	}
	
	public static String getSObjectNameFromId(String idValue, String errorMessageSubject){
		try{
			return String.valueOf( getIdFromString(idValue, errorMessageSubject).getSObjectType() );
		}catch(PC_Exception e){
			throw e; 
		}catch(Exception e){
			throw new PC_Exception('Passed ' + errorMessageSubject + ' is incorrect: "' + idValue + '"');
		}  
	}
	
    public static List<NCM_API_DataTypes.NotificationWrapper> populateNotificationsWithProtocolNames(List<NCM_API_DataTypes.NotificationWrapper> notifications){
        Set<Id> relatedToIds = getRelatedToIds(notifications);
        if ( !relatedToIds.isEmpty() ){
            Map<Id, WFM_Protocol__c> protocols = new Map<Id, WFM_Protocol__c>( [Select Name FROM WFM_Protocol__c Where Id In:relatedToIds] );
            for ( NCM_API_DataTypes.NotificationWrapper nw : notifications ){
                WFM_Protocol__c protocol = protocols.get(nw.relatedToId);
                if ( protocol != null ){
                    nw.relatedToIdName = protocol.Name;
                }
            }
        }
        return notifications;
    }
    
    private static Set<Id> getRelatedToIds(List<NCM_API_DataTypes.NotificationWrapper> notifications){
        Set<Id> relatedToIds = new Set<Id>();
        for ( NCM_API_DataTypes.NotificationWrapper nw : notifications ){
            if ( !String.isEmpty(nw.relatedToId) ){
                relatedToIds.add( (Id) nw.relatedToId );
            }
        }
        return relatedToIds;
    }
    
    public static PRA_WrappersForMobile.RegistrationsPerProtocolResult registerForNotifications( List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrations, List<String> protocolIds ){
        try{
            System.debug('--- protocolIds: ' + protocolIds);
            Set<String> protocolIdsAsSet = new Set<String>( protocolIds );
            System.debug('protocolIdsAsSet: ' + protocolIdsAsSet);
            System.debug('protocolIdsAsSet: ' + protocolIdsAsSet.size());
            //checkRegistrationsBeforeSave(registrations, email, phone);
            Savepoint sp = Database.setSavepoint();
            try{
                registerActiveRegistrations(registrations, protocolIdsAsSet);
                unregisterDisabledRegistrations(registrations, protocolIdsAsSet);
                //saveUserEmailAndPhone(email, phone);
            }catch(Exception e){
                Database.rollback( sp );
                throw e;
            }
            return protocolIds.size() > 1 
                    ? new PRA_WrappersForMobile.RegistrationsPerProtocolResult( getNumberOfActiveRegistrationsPerRelatedObjectByCurrentUser(), NCM_API.getNumberOfPendingNotificationsPerRelatedObjectByUser( UserInfo.getUserId() ) )
                    : new PRA_WrappersForMobile.RegistrationsPerProtocolResult( null, NCM_API.getNumberOfPendingNotificationsPerRelatedObjectByUser( UserInfo.getUserId() ) );
        }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return new PRA_WrappersForMobile.RegistrationsPerProtocolResult( e.getMessage() );
        }
    }
    
    private static void registerActiveRegistrations(List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrations, Set<String> protocolIds){
        List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrationsForOperation = getRegistrationsForOperation(registrations, protocolIds, true);
        if ( !registrationsForOperation.isEmpty() ){
            NCM_API.registerForNotifications( registrationsForOperation );
        }
    }
    
    private static void unregisterDisabledRegistrations(List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrations, Set<String> protocolIds){
        List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrationsForOperation = getRegistrationsForOperation(registrations, protocolIds, false);
        if ( !registrationsForOperation.isEmpty() ){
            NCM_API.disableRegistrationsByWrapperData( registrationsForOperation );
        }
    }
    
    private static List<NCM_API_DataTypes.NotificationRegistrationWrapper> getRegistrationsForOperation(List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrations, Set<String> protocolIds, Boolean isActive){
        List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrationsForOperation = new List<NCM_API_DataTypes.NotificationRegistrationWrapper>();
        List<NCM_API_DataTypes.NotificationRegistrationWrapper> filteredRegs = filterByActive(registrations, isActive);
        for ( NCM_API_DataTypes.NotificationRegistrationWrapper filteredReg : filteredRegs ){
            for (String protocolId : protocolIds){
                NCM_API_DataTypes.NotificationRegistrationWrapper reg = filteredReg.clone();
                reg.relatedToId = protocolId;
                registrationsForOperation.add(reg);
            }
        }
        System.debug('--- reg. active: ' + isActive + '. registrationsForOperation: ' + registrationsForOperation.size() + '. protocolIds: ' + protocolIds);
        return registrationsForOperation;               
    }
    
    private static List<NCM_API_DataTypes.NotificationRegistrationWrapper> filterByActive(List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrations, Boolean isActive){
        List<NCM_API_DataTypes.NotificationRegistrationWrapper> filtered = new List<NCM_API_DataTypes.NotificationRegistrationWrapper>();
        for ( NCM_API_DataTypes.NotificationRegistrationWrapper reg : registrations ){
            if (reg.active == isActive){
                filtered.add(reg);
            }
        }
        return filtered;
    }
    
    public static Map<String, Integer> getNumberOfActiveRegistrationsPerRelatedObjectByCurrentUser(){
        return NCM_API.getNumberOfActiveRegistrationsPerRelatedObjectByUser( UserInfo.getUserId() );
    }
    
    public static User retrieveCurrentUser(){
    	return [ Select Email, Phone From User Where Id = :UserInfo.getUserId() ];
    }
    
    public static String getTrimmedBody(String body){
    	return body.replaceAll('[\n|\r\n](?!([^<]+)?>)', '<br/>').replaceAll('[\\s](?!([^<]+)?>)', '&nbsp;'); //body.replace('\n', '<br/>');
    }
    
     public static List<PRA_WrappersForMobile.WrapperSite> getStatusBySite(String protocolUniqKey){
        List<PRA_WrappersForMobile.WrapperSite> wrappedSites = new List<PRA_WrappersForMobile.WrapperSite>();
        for (PBB_WR_APIs.WrapperSite site : PBB_WR_APIs.getPatientEnrollmntStatusBySite(protocolUniqKey) ){
            wrappedSites.add( new PRA_WrappersForMobile.WrapperSite(
                    site.Site,
                    site.siteData,
                    site.Country
            ) );
        }
        return wrappedSites;
    }
    
    public static List<PRA_WrappersForMobile.WrapperCountry> getStatusByCountry(String protocolUniqKey){
        List<PRA_WrappersForMobile.WrapperCountry> wrappedCountries = new List<PRA_WrappersForMobile.WrapperCountry>();
        for (PBB_WR_APIs.WrapperCountry country : PBB_WR_APIs.getPatientEnrollmntStatusByCountry(protocolUniqKey) ){
            wrappedCountries.add( new PRA_WrappersForMobile.WrapperCountry(
                    country.country,
                    country.countryData
            ) );
        }
        return wrappedCountries;
    }
    
	/*public static Datetime getLocalDateTimeFromGmt(Datetime gmt){   
		if (gmt == null){
			return null;
		} 
        Datetime local = gmt.Date();
        local = local.addHours(gmt.hour());
        local = local.addMinutes(gmt.minute());
        local = local.addSeconds(gmt.second());
        
        return local;
    }*/
	
	public class PC_Exception extends Exception{}	
}