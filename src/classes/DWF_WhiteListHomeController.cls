public class DWF_WhiteListHomeController {
	
	public Integer pageIndex {get;set;}
	public Integer maxIndex {get;set;}
	public String selectedView {get;set;}
	public String deleteWhitelistID {get;set;}
	public String selectedCompanyNo {get;set;}
	public String selectedBUName {get;set;}
	public String selectedSystemId {get;set;}
	public list<selectoption> companyNames {get;set;}
	public list<selectoption> businessUnits {get;set;}
	public list<DWF_Whitelist__c> whiteLists {get;set;} 
	public DWF_FilterService.companyVO companyVO {get;set;}
	public DWF_FilterService.businessUnitVO businessUnitVO {get;set;}
	public list<DWF_FilterService.companyVO> companyVOs {get;set;}
	public map<Integer, list<DWF_Whitelist__c>> wlPageMap {get;set;}
	
	private static final String VIEW_RECENT = 'RecentWL';
	private static final String VIEW_CREATED = 'MyWL';
	private static final String VIEW_ALL = 'ALL';
	
	public DWF_WhiteListHomeController() {
		pageIndex = 1;
		wlPageMap = new map<Integer, list<DWF_Whitelist__c>>();
		selectedView = VIEW_RECENT;
		searchWhitelists();
		getMapValue();
		companyNames = getCompanyNames();
		businessUnits = getBusinessUnits(null);
	}
	
	public list<selectoption> getFilterTypes() {
		transient list<selectoption> options = new list<selectoption>();
		options.add(new selectoption(VIEW_RECENT, 'Recent Whitelists'));
		options.add(new selectoption(VIEW_CREATED, 'My Created Whitelists'));
		options.add(new selectoption(VIEW_ALL, 'All Whitelists'));
		return options;
	}
	
	public list<selectoption> getCompanyNames() {
		DWF_FilterService.companyListResponse response = DWF_FilterServiceWrapper.getAllCompanyVOs();
		companyVOs = response.companyVOs;
		return DWF_Util.getCompanyNames(companyVOs);
	}
    
    public list<selectoption> getBusinessUnits(list<DWF_FilterService.businessUnitVO> businessUnitVOs) {
        return DWF_Util.getBusinessUnits(businessUnitVOs); 
    }
    
    public list<selectoption> getSystemNames() {
        return DWF_Util.getSystemNames();
    }
	
	public void getCompanyFromSelectedValue() {
		companyVO = new DWF_FilterService.companyVO();
		if (companyVOs != null) {
			if (selectedCompanyNo != null) {
				for (DWF_FilterService.companyVO compVO : companyVOs) {
					if (selectedCompanyNo.equals(String.valueOf(compVO.companyNo))) {
						companyVO = compVO;
						businessUnits = getBusinessUnits(compVO.businessUnits);
					}
				}
			} else {
				businessUnits = getBusinessUnits(null);
				selectedBUName = null;
			}
		}
		searchWhiteLists();
	}
	
	// Get businessUnitVO from the selected BU dropdown value
	public void getBusinessUnitFromSelectedValue() {
		businessUnitVO = new DWF_FilterService.businessUnitVO();
		if (selectedBUName != null) {
			for (DWF_FilterService.businessUnitVO businessUnit : companyVO.businessUnits) {
				if (businessUnit.businessUnitName.equals(selectedBUName)) {
					businessUnitVO = businessUnit;
				}
			}
		}
		searchWhiteLists();
	}
	
	public void searchWhiteLists() {
		Integer companyNo = 0;
		String soql = '';
		if (selectedView.equals(VIEW_RECENT)) {
			soql += 'select id, name, company_name__c, company_no__c, business_unit_name__c, business_unit_no__c, DWF_System__r.Name '
				+ 'from DWF_WhiteList__c where Name != null';
		} else if (selectedView.equals(VIEW_CREATED)) {
			String userId = UserInfo.getUserId();
			soql += 'select id, name, company_name__c, company_no__c, business_unit_name__c, business_unit_no__c, DWF_System__r.Name '
				+ 'from DWF_WhiteList__c where CreatedById = :userId';
		} else if (selectedView.equals(VIEW_ALL)) {
			soql += 'select id, name, company_name__c, company_no__c, business_unit_name__c, business_unit_no__c, DWF_System__r.Name'
				+ ' from DWF_WhiteList__c where name != null';
		}
		if (selectedCompanyNo != null) {
			companyNo = Integer.valueOf(selectedCompanyNo);
			soql += ' and company_no__c = :companyNo ';
		}
		if (selectedBUName != null) {
			soql += ' and business_unit_name__c = :selectedBUName';
		}
		if (selectedSystemId != null) {
			soql += ' and DWF_System__c = :selectedSystemId';
		}
		if (selectedView.equals(VIEW_RECENT)) {
			soql += ' limit 100';
		}
		system.debug('-----------------------------------------------------------' + selectedSystemId);
		system.debug('--------------------------query----------------------------' + soql);
		list<DWF_Whitelist__c> wls = Database.query(soql);
		Integer mapIndex = pageIndex;
		wlPageMap.put(mapIndex, new list<DWF_Whitelist__c>());
		for (Integer i = 0; i < wls.size(); i++) {
			if (i > 0 && Math.mod(i, 1000) == 0) {
				mapIndex++;
				wlPageMap.put(mapIndex, new list<DWF_Whitelist__c>());
			}
			wlPageMap.get(mapIndex).add(wls.get(i));
		}
		maxIndex = mapIndex;
		system.debug('============================================' + wlPageMap);
		system.debug('============================================' + pageIndex);
		system.debug('============================================' + maxIndex);
		getMapValue();
	}
	
	public void getMapValue() {
		whiteLists = wlPageMap.get(pageIndex);
	}
	
	public void incrementPageIndex() {
		pageIndex++;
		getMapValue();
	}
	
	public void decrementPageIndex() {
		pageIndex--;
		getMapValue();
	}
	
	public void deleteWhitelist() {
		DWF_Whitelist__c whiteList = [select Id from DWF_Whitelist__c where Id = :deleteWhitelistID];
		if (whiteList != null) {
			delete whiteList;
			searchWhitelists();
		}
	}
}