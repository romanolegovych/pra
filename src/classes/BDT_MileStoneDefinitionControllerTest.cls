@isTest
private class BDT_MileStoneDefinitionControllerTest {

    static testMethod void myUnitTest() {
        
		BDT_CreateTestData.buildMileStoneDefinition();
		PageReference pageRef = New PageReference(System.Page.BDT_MileStoneDefinition.getUrl());
        Test.setCurrentPage(pageRef);
        
		BDT_MileStoneDefinitionController T = new BDT_MileStoneDefinitionController();
		
		T.Add();
		ApexPages.currentPage().getParameters().put('categoryName'	, 'General');
		T.ApplyCategory();
		T.Cancel();
		
		T.Add();
		ApexPages.currentPage().getParameters().put('categoryName'	, 'General');
		T.ApplyCategory();
		T.msdEdit.Name = 'Test';
		T.Save();
		
        ApexPages.currentPage().getParameters().put('msdid'	, T.MSD[1].id);
        T.Edit();
        T.DeleteMSD();

    }
    
}