/**
 * Test for PersonToAdhoc controller
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestPersonToAdhocController {
    
    static List<Job_Class_Desc__c> jobClasses;
    static List<Job_Title__c> jobTitles;
    static List<Country__c> countries;
    static List<Role_Type__c> roleTypes;
    static List<LMS_Role__c> roles;
    static List<Employee_Status__c> statuses;
    static List<Employee_Details__c> employees;
    static List<LMS_Role_Employee__c> roleEmps;
    static List<LMSConstantSettings__c> constants;
    static List<CourseDomainSettings__c> domains;
    static List<RoleAdminServiceSettings__c> settings;
    
    static void init() {
        
        constants = new LMSConstantSettings__c[] {
        	new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
    		new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
    		new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
    		new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D'),
    		new LMSConstantSettings__c(Name = 'typePRA', Value__c = 'PRA'),
    		new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
    		new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
    		new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
    		new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
    		new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
    		new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
    		new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
    		new LMSConstantSettings__c(Name = 'statusToDelete', Value__c = 'Delete'),
    		new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended'),
    		new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
    		new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
    		new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
    		new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete')
        };
        insert constants;
        
        domains = new CourseDomainSettings__c[] {
            new CourseDomainSettings__c(Name = 'Internal', Domain_Id__c = 'Internal'),
            new CourseDomainSettings__c(Name = 'PST', Domain_Id__c = 'Project Specific'),
            new CourseDomainSettings__c(Name = 'Archive', Domain_Id__c = 'Archive')
        };
        insert domains;
        
        settings = new RoleAdminServiceSettings__c[] {
            new RoleAdminServiceSettings__c(Name = 'DOMAIN', Value__c = 'DOMAIN'),
            new RoleAdminServiceSettings__c(Name = 'ENDPOINT', Value__c = 'ENDPOINT'),
            new RoleAdminServiceSettings__c(Name = 'TIMEOUT', Value__c = '50000')
        };
        insert settings;
        
        jobClasses = new Job_Class_Desc__c[] {
            new Job_Class_Desc__c(Name = 'Accountant', Job_Class_Code__c = 'code1', Job_Class_ExtID__c = 'Accountant'),
            new Job_Class_Desc__c(Name = 'class2', Job_Class_Code__c = 'code2', Job_Class_ExtID__c = 'class2')
        };
        insert jobClasses;
        
        jobTitles = new Job_Title__c[] {
            new Job_Title__c(Job_Code__c = 'Code1', Job_Title__c = 'Title1', Job_Class_Desc__c = jobClasses[0].Id, Status__c = 'A'),
            new Job_Title__c(Job_Code__c = 'Code2', Job_Title__c = 'Title2', Job_Class_Desc__c = jobClasses[1].Id, Status__c = 'A')
        };
        insert jobTitles;
        
        countries = new Country__c[] {
            new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='US/Canada', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry2',PRA_Country_ID__c='200', Country_Code__c='JU',Region_Name__c='Latin America', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry3',PRA_Country_ID__c='300', Country_Code__c='TB',Region_Name__c='Asia/Pacific', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry4',PRA_Country_ID__c='400', Country_Code__c='AB',Region_Name__c='Europe/Africa', 
                               Daily_Business_Hrs__c=8.0)
        };
        insert countries;
        
        roleTypes = new Role_Type__c[] {
        	new Role_Type__c(name = 'PRA'),
        	new Role_Type__c(name = 'Project Specific'),
        	new Role_Type__c(name = 'Additional Role')
        };
        insert roleTypes;
    }
    
    static void initRoles() {
        roles = new LMS_Role__c[] {
            new LMS_Role__c(Adhoc_Role_Name__c = 'TestRole1', Status__c = 'Active', Sync_Status__c = 'Y', Role_Type__c = roleTypes[2].Id),
            new LMS_Role__c(Adhoc_Role_Name__c = 'TestRole2', Status__c = 'Inactive', Sync_Status__c = 'N', Role_Type__c = roleTypes[2].Id)
        };
        insert roles;
        
        statuses = new Employee_Status__c[] {
        	new Employee_Status__c(Employee_Type__c = 'Employee', Employee_Status__c = 'AA'),
        	new Employee_Status__c(Employee_Type__c = 'Terminated', Employee_Status__c = 'XX')
        };
        insert statuses;
        
        employees = new Employee_Details__c[] {
            new Employee_Details__c(Name = 'employee1', Employee_Unique_Key__c = 'employee1', First_Name__c = 'Andrew', Last_Name__c = 'Allen', Email_Address__c = 'a@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU'),
            new Employee_Details__c(Name = 'employee2', Employee_Unique_Key__c = 'employee2', First_Name__c = 'Allen', Last_Name__c = 'Stephens', Email_Address__c = 's@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU'),
        	new Employee_Details__c(Name = 'employee3', Employee_Unique_Key__c = 'employee3', First_Name__c = 'Allen', Last_Name__c = 'Richard', Email_Address__c = 'r@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU')
        };
        insert employees;
    }
    
    static void initMappings() {
        roleEmps = new LMS_Role_Employee__c[] {
            new LMS_Role_Employee__c(Role_Id__c = roles[0].Id, Employee_Id__c = employees[0].Id, Status__c = 'Draft', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today()),
        	new LMS_Role_Employee__c(Role_Id__c = roles[0].Id, Employee_Id__c = employees[1].Id, Status__c = 'Committed', Sync__c = 'Y',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today()),
        	new LMS_Role_Employee__c(Role_Id__c = roles[0].Id, Employee_Id__c = employees[2].Id, Status__c = 'Draft Delete', Sync__c = 'Y',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today())
        };
        insert roleEmps;
    }
    
    static void initEmployeeError() {
    	delete employees;
    	employees = new list<Employee_Details__c>();
    	for(Integer i = 0; i < 1001; i++) {
    		employees.add(new Employee_Details__c(Name = 'employee' + i, Employee_Unique_Key__c = 'employee' + i, First_Name__c = 'Andrew' + i, Last_Name__c = 'Allen' + i, 
    			Email_Address__c = i + 'a@a.com', Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', 
    			Department__c = 'Analysis and Reporting', Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', 
    			Date_Hired__c = date.parse('12/27/2009'), Business_Unit__c = 'RDU'));
    	}
    	
    	insert employees;
    }

    static testMethod void testConstructor() {
		init();
        LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
        System.assert(c.roleText == 'Enter part of the role name' && c.roleTextStyle == 'WaterMarkedTextBox' &&
                      c.empName == '' && c.jobFamily == '' && c.employees == null && c.selEmps == null);
    }
    
    static testMethod void testRoleReset() {
		init();
        LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
        PageReference pr = c.roleReset();
        System.assert(pr == null);
    }
    
    static testMethod void testClosePersonSearch() {
		init();
        LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
        c.empName = 'a, a';
        c.jobFamily = 'a';
        c.closePersonSearch();
        System.assert(c.empName == '' &&  c.jobFamily == '');
    }
    
    static testMethod void testGetJobFamilyList() {
        init();
        LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
        List<SelectOption> options = c.getJobFamilyList();
        System.assert(options.size() == 3 && options[1].getValue() == jobClasses[0].Name && 
                      options[2].getValue() == jobClasses[1].Name);
    }
    
    static testMethod void testSearchSuccess() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
        c.allAdhoc = true;
        c.search();
        System.assert(c.allRoles.size() == 2);
        
        c.allAdhoc = false;
        c.roleText = roles[0].Adhoc_Role_Name__c;
        c.search();
        System.assert(c.employees.size() == 3);
                      
        c.roleText = roles[1].Adhoc_Role_Name__c;
        c.search();
        System.assert(c.employees.size() == 0 && c.roleErrorText == 'Role is inactive, please enter different role name');
    }
    
    static testMethod void testSearchFailure() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();  
        
        c.allAdhoc = false;       
        c.roleText = 'New Role Name';
        c.search();
        System.assert(c.employees == null);
    }
    
    static testMethod void testPersonSearchSuccess() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
        c.roleText = roles[0].Adhoc_Role_Name__c;
        c.search();
        
        c.empName = employees[0].Last_Name__c + ', ' + employees[0].First_Name__c;
        c.jobFamily = null;
        c.personSearch();
        System.assert(c.selEmps.size() == 1 && c.selEmps[0].firstName == employees[0].First_Name__c);
        
        c.empName = employees[0].Last_Name__c;
        c.jobFamily = null;
        c.personSearch();
        System.assert(c.selEmps.size() == 1 && c.selEmps[0].lastName == employees[0].Last_Name__c);
        
        c.empName = employees[0].Last_Name__c + ',';
        c.jobFamily = null;
        c.personSearch();
        System.assert(c.selEmps.size() == 1 && c.selEmps[0].lastName == employees[0].Last_Name__c);
        
        c.empName = '';
        c.jobFamily = employees[0].Job_Class_Desc__c;
        c.personSearch();
        System.assert(c.selEmps.size() == 1);
    }
    
    static testMethod void testPersonSearchFailure() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
        c.roleText = roles[0].Adhoc_Role_Name__c;
        c.search();
        
        c.empName = 'Employee X';
        c.personSearch();
        System.assert(c.selEmps.size() == 0);
    }
    
    static testMethod void testTooManyPersonSearchFailure() {
        init();
        initRoles();
        initMappings();
        initEmployeeError();
        LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
        c.roleText = roles[0].Adhoc_Role_Name__c;
        c.search();
        
        c.empName = '';
        c.personSearch();
        System.assert(c.selEmps.size() == 0 && c.personErrorText == 'The result contains too many records. Please refine your search criteria and run the search again.');
        	
        c.showErrors();
    }
    
    static testMethod void testAddPerson() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
        c.roleText = roles[0].Adhoc_Role_Name__c;
        c.search();
        
        c.empName = employees[0].Last_Name__c + ', ' + employees[0].First_Name__c;
        c.jobFamily = null;
        c.personSearch();
        
        c.selEmps[0].selected = true;
        PageReference pr = c.addPerson();
        System.assert(pr == null);
    }
    
    static testMethod void testRemovePerson() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
        c.roleText = roles[0].Adhoc_Role_Name__c;
        c.search();
        
        c.assignment = roleEmps[0].Id;
        PageReference pr = c.removePersons();
        System.assert(pr == null);
    }
    
    static testMethod void testCommitPerson() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
        c.roleText = roles[0].Adhoc_Role_Name__c;
        c.search();
        
        PageReference pr = c.commitPerson();
        System.assert(pr == null);
    }
	
	static testMethod void testApplyCommitDate(){
		init();
		initRoles();
		initMappings();
		LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
		c.roleText = roles[0].Adhoc_Role_Name__c;
		c.search();
		
		c.employees[0].selected = true;
		c.commitDate = '12/12/2102';
		PageReference pr = c.setCommitDate();
		System.assert(pr == null);
	}
    
    static testMethod void testCancel() {
        init();
        initRoles();
        initMappings();
        LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
        c.roleText = roles[0].Adhoc_Role_Name__c;
        c.search();
        
        PageReference pr = c.cancel();
        System.assert(pr == null);
        
        c.cancelCreateRole();
        //system.assert();
    }
    
    static testMethod void testPersonReset() {
		init();
    	LMS_PersonToAdhocController c = new LMS_PersonToAdhocController();
    	c.empName = 'name';
    	c.jobFamily = 'jobfamily';
    	c.personReset();
    	System.assert(c.empName == '' && c.jobFamily == '' && c.selEmps == null);
    }
    
}