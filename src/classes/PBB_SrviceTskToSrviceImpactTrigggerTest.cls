@isTest 
private class PBB_SrviceTskToSrviceImpactTrigggerTest {
    static testMethod void testPBB_SrviceTskToSrviceImpactTrigggerTest() {        
        test.startTest();
        //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils ();       
        tu.smm = tu.createServiceModelAttributes();
        tu.sa = tu.createServiceAreaAttributes();
        tu.sf = tu.createServiceFunctionAttributes();
        tu.St = tu.createServiceTaskAttributes();
        tu.siqn = tu.createSIQNAttributes();
        Service_Task_To_Service_Impact__c sttsi =tu.createSTtoSI(tu.St,tu.siqn ); 
        try{
            delete sttsi;
            Service_Task_To_Service_Impact__c sttsi1  = tu.createSTtoSI(tu.St,tu.siqn );         
            tu.smm = tu.approveServicemodel(tu.smm);
            delete sttsi1;
        }
        catch(Exception e) {
                system.assert(e.getMessage().contains('UNABLE TO DELETE RECORD BECAUSE SERVICE MODEL IS APPROVED/RETIRED!'));
            }        
        
        test.stopTest();
   }
}