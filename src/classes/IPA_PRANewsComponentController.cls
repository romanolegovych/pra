public class IPA_PRANewsComponentController
{
    public IPA_Articles__c featuredPRANews {get; set;}
    public List<IPA_Articles__c> lstPRANews {get; set;}
    
    public IPA_PRANewsComponentController()
    {
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        featuredPRANews = ipa_bo.returnFeaturedPRANews();
        lstPRANews = ipa_bo.returnPRANews();
    }    
}