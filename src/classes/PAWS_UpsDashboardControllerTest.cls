@isTest
private class PAWS_UpsDashboardControllerTest
{
	@isTest
	static void itShouldCreateInstance()
	{
		PAWS_ApexTestsEnvironment.init();

		PAWS_Settings__c settings = PAWS_ApexTestsEnvironment.PAWSSettings;

		ecrf__c project = PAWS_ApexTestsEnvironment.Project;
		PAWS_Project_Flow_Country__c projectCountry = PAWS_ApexTestsEnvironment.ProjectCountry;
		PAWS_Project_Flow_Site__c projectSite = PAWS_ApexTestsEnvironment.ProjectSite;
		PAWS_Project_Flow_Agreement__c projectAgreement = PAWS_ApexTestsEnvironment.ProjectAgreement;
		PAWS_Project_Flow_Document__c projectDocument = PAWS_ApexTestsEnvironment.ProjectDocument;
		PAWS_Project_Flow_Submission__c projectSubmission = PAWS_ApexTestsEnvironment.ProjectSubmission;

		STSWR1__Flow__c projectFlow = new STSWR1__Flow__c(Name='Project Flow', STSWR1__Type__c='Manual', STSWR1__Object_Type__c='ecrf__c', STSWR1__Status__c = 'Active', STSWR1__Source_Id__c=project.ID);
		STSWR1__Flow__c countryFlow = new STSWR1__Flow__c(Name='Country Flow', STSWR1__Type__c='Triggered by another flow', STSWR1__Object_Type__c='paws_project_flow_country__c', STSWR1__Status__c = 'Active', STSWR1__Source_Id__c=projectCountry.ID);
		STSWR1__Flow__c siteFlow = new STSWR1__Flow__c(Name='Site Flow', STSWR1__Type__c='Triggered by another flow', STSWR1__Object_Type__c='paws_project_flow_site__c', STSWR1__Status__c = 'Active', STSWR1__Source_Id__c=projectSite.ID);
		STSWR1__Flow__c agreementFlow = new STSWR1__Flow__c(Name='Agreement Flow', STSWR1__Type__c='Triggered by another flow', STSWR1__Object_Type__c='paws_project_flow_agreement__c', STSWR1__Status__c = 'Active', STSWR1__Source_Id__c=projectAgreement.ID);
		STSWR1__Flow__c documentFlow = new STSWR1__Flow__c(Name='Document Flow', STSWR1__Type__c='Triggered by another flow', STSWR1__Object_Type__c='paws_project_flow_document__c', STSWR1__Status__c = 'Active', STSWR1__Source_Id__c=projectDocument.ID);
		STSWR1__Flow__c submissionFlow = new STSWR1__Flow__c(Name='Submission Flow', STSWR1__Type__c='Triggered by another flow', STSWR1__Object_Type__c='paws_project_flow_submission__c', STSWR1__Status__c = 'Active', STSWR1__Source_Id__c=projectSubmission.ID);
		List<STSWR1__Flow__c> flows = new List<STSWR1__Flow__c> { projectFlow, countryFlow, siteFlow, agreementFlow, documentFlow, submissionFlow };
		insert flows;

		STSWR1__Item__c folderItem = new STSWR1__Item__c(Name=settings.Project_Folder_Name__c,STSWR1__Path__c=projectFlow.ID);
        insert folderItem;
        STSWR1__Item__c projectItem = new STSWR1__Item__c(Name=projectFlow.Name,STSWR1__Parent__c=folderItem.ID,STSWR1__Source_Flow__c=projectFlow.ID);
        insert projectItem;
		
		STSWR1__Flow_Step_Junction__c projectStep = new STSWR1__Flow_Step_Junction__c(Name='Project Step', STSWR1__Flow__c=projectFlow.Id, STSWR1__Is_First_Step__c=true);
		STSWR1__Flow_Step_Junction__c countryStep = new STSWR1__Flow_Step_Junction__c(Name='Country Step', STSWR1__Flow__c=countryFlow.Id, STSWR1__Is_First_Step__c=true);
		STSWR1__Flow_Step_Junction__c siteStep = new STSWR1__Flow_Step_Junction__c(Name='Site Step', STSWR1__Flow__c=siteFlow.Id, STSWR1__Is_First_Step__c=true);
		STSWR1__Flow_Step_Junction__c agreementStep = new STSWR1__Flow_Step_Junction__c(Name='Agreement Step', STSWR1__Flow__c=agreementFlow.Id, STSWR1__Is_First_Step__c=true);
		STSWR1__Flow_Step_Junction__c documentStep = new STSWR1__Flow_Step_Junction__c(Name='Document Step', STSWR1__Flow__c=documentFlow.Id, STSWR1__Is_First_Step__c=true);
		STSWR1__Flow_Step_Junction__c submissionStep = new STSWR1__Flow_Step_Junction__c(Name='Submission Step', STSWR1__Flow__c=submissionFlow.Id, STSWR1__Is_First_Step__c=true);
		insert new List<STSWR1__Flow_Step_Junction__c> { projectStep, countryStep, siteStep, agreementStep, documentStep, submissionStep };
		
		Map<String, Object> config = new Map<String, Object> { 'flowId' => countryFlow.ID};
		STSWR1__Flow_Step_Action__c projectStepAction = new STSWR1__Flow_Step_Action__c(STSWR1__Config__c = JSON.serialize(config), STSWR1__Flow_Step__c = projectStep.ID, STSWR1__Type__c = 'Start Sub Flow');

		config = new Map<String, Object> { 'flowId' => siteFlow.ID};
		STSWR1__Flow_Step_Action__c countryAction = new STSWR1__Flow_Step_Action__c(STSWR1__Config__c = JSON.serialize(config), STSWR1__Flow_Step__c = countryStep.ID, STSWR1__Type__c = 'Start Sub Flow');

		config = new Map<String, Object> { 'flowId' => agreementFlow.ID};
		STSWR1__Flow_Step_Action__c siteAgreementAction = new STSWR1__Flow_Step_Action__c(STSWR1__Config__c = JSON.serialize(config), STSWR1__Flow_Step__c = siteStep.ID, STSWR1__Type__c = 'Start Sub Flow');

		config = new Map<String, Object> { 'flowId' => documentFlow.ID};
		STSWR1__Flow_Step_Action__c siteDocumentAction = new STSWR1__Flow_Step_Action__c(STSWR1__Config__c = JSON.serialize(config), STSWR1__Flow_Step__c = siteStep.ID, STSWR1__Type__c = 'Start Sub Flow');

		config = new Map<String, Object> { 'flowId' => submissionFlow.ID};
		STSWR1__Flow_Step_Action__c siteSubmissionAction = new STSWR1__Flow_Step_Action__c(STSWR1__Config__c = JSON.serialize(config), STSWR1__Flow_Step__c = siteStep.ID, STSWR1__Type__c = 'Start Sub Flow');
		insert new List<STSWR1__Flow_Step_Action__c> { projectStepAction, countryAction, siteAgreementAction, siteDocumentAction, siteSubmissionAction };

		STSWR1__Flow_Instance__c projectInstance = new STSWR1__Flow_Instance__c(STSWR1__Object_Id__c = project.ID, STSWR1__Object_Name__c = project.Name, STSWR1__Flow__c = projectFlow.ID, STSWR1__Is_Active__c = true);
		insert projectInstance;

		STSWR1__Flow_Instance_Cursor__c projectCursor = new STSWR1__Flow_Instance_Cursor__c(STSWR1__Flow_Instance__c=projectInstance.Id, STSWR1__Step__c=projectStep.Id, STSWR1__Step_Assigned_To__c=UserInfo.getUserId(), STSWR1__Status__c='In Progress');
		insert projectCursor;

		Test.startTest();

		PAWS_UpsDashboardController controller = new PAWS_UpsDashboardController();
		String boardDataString = controller.BoardJson;

		System.assert(boardDataString.contains(project.Name));
		System.assert(boardDataString.contains(projectCountry.Name));
		System.assert(boardDataString.contains(projectSite.Name));
		System.assert(boardDataString.contains(projectAgreement.Name));
		System.assert(boardDataString.contains(projectDocument.Name));
		System.assert(boardDataString.contains(projectSubmission.Name));

		System.assert(boardDataString.contains(projectStep.Name));
		System.assert(boardDataString.contains(countryStep.Name));
		System.assert(boardDataString.contains(siteStep.Name));
		System.assert(boardDataString.contains(agreementStep.Name));
		System.assert(boardDataString.contains(documentStep.Name));
		System.assert(boardDataString.contains(submissionStep.Name));
	}

	@isTest
	static void itShouldRemoveStep()
	{

		PAWS_UpsDashboardController.FlowWrapper flowWrapped = new PAWS_UpsDashboardController.FlowWrapper(PAWS_ApexTestsEnvironment.Flow);
		PAWS_UpsDashboardController.FlowStepWrapper stepWrapped = flowWrapped.addStep(PAWS_ApexTestsEnvironment.FlowStep);

		System.assert(flowWrapped.Steps.size() == 1);
		System.assert(flowWrapped.StepsWraperIdMap.size() == 1);
		

		stepWrapped.removeFromParent();

		System.assert(flowWrapped.Steps.size() == 0);
		System.assert(flowWrapped.StepsWraperIdMap.size() == 0);


		// shouldn't throw an exception
		stepWrapped.removeFromParent();
	}

}