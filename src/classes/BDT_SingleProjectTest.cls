@isTest
private with sharing class BDT_SingleProjectTest {
	
	public static Client_Project__c firstProject {get;set;}
	public static List<Sponsor__c> mySponsorList {get;set;}
	public static List<Account> mySponsorEntityList  {get;set;}
	public static List<Business_Unit__c> myBUList  {get;set;}
	public static List<Study__c> stydiesList {get;set;}	
	public static List<Site__c> mySiteList  {get;set;}

	
	static void init(){		
		
		mySponsorList = BDT_TestDataUtils.buildSponsor(1);		
		insert mySponsorList;
		
		mySponsorEntityList = BDT_TestDataUtils.buildSponsorEntity(mySponsorList);
		insert mySponsorEntityList;
				
		myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert myBUList;
		
		firstProject = BDT_TestDataUtils.buildProject();
		firstProject.Client__c = mySponsorEntityList[0].Id;
		insert firstProject;
		
		mySiteList = BDT_TestDataUtils.buildSite(myBUList);
		insert mySiteList;
		
		stydiesList = BDT_TestDataUtils.buildStudies(firstProject);
		stydiesList[0].Business_Unit__c = myBUList[0].Id; 
		stydiesList[1].Business_Unit__c = myBUList[1].Id; 
		stydiesList[2].Business_Unit__c = myBUList[2].Id;
		stydiesList[3].Business_Unit__c = myBUList[3].Id; 
		stydiesList[4].Business_Unit__c = myBUList[4].Id;
		insert stydiesList;
		
		BDT_Utils.setPreference('SelectedProject', firstProject.id);
		BDT_Utils.setPreference('SelectedStudies',stydiesList[0].id);
		 
	}
	
	
	static testMethod void createnewProject() { 
		BDT_SingleProject newProject = new BDT_SingleProject();		
		//error message as no sponsor was selected
		System.assertEquals(null, newProject.save());
	}
	
	static testMethod void createCancelProject() { 
		BDT_SingleProject newProject = new BDT_SingleProject();		
		System.assertNotEquals(null, newProject.cancel());
	}
	
	static testMethod void createnewProject1() { 
		init();
		
		BDT_SingleProject newProject = new BDT_SingleProject();	
		newProject.singleProject.Client__c = mySponsorEntityList[0].id;
		//newProject.TherapeuticArealkold = '';
		System.assertNotEquals(null, newProject.save());
		
		BDT_SingleProject addStudyProject = new BDT_SingleProject();
		
		System.assertNotEquals(null, addStudyProject.deleteSoft());
		
	}
	
	static testMethod void createnewProject2() { 
		init();
		BDT_SingleProject newProject = new BDT_SingleProject();		
		newProject.singleProject.Client__c = mySponsorEntityList[1].id;
		//newProject.TherapeuticArealkold = '';
		System.assertNotEquals(null, newProject.save());
		
		BDT_SingleProject newProject2 = new BDT_SingleProject();
		newProject2.referToSingleProjectEmpty();
		newProject2.singleProject.Client__c = mySponsorEntityList[1].id;
		//newProject2.TherapeuticArealkold = '';
		System.assertNotEquals(null, newProject2.save());
	}
	
	static testMethod void showProjectWithStudies() { 
		init();
					
		BDT_SingleProject newProject = new BDT_SingleProject();	
		
		// code coverage buster
		list<SelectOption> testoptions = newProject.getTherapeuticAreas();
		string testCOunt = newProject.returnCount();
		
		
		System.assertEquals(1, newProject.studiesList.size());
		System.assertNotEquals(null, newProject.deleteSoft());
		
	}
	
	static testMethod void editStudy() { 
		init();		
		
		BDT_SingleProject newProject = new BDT_SingleProject();	
		
		System.currentPageReference().getParameters().put('studyId', stydiesList[4].id);
		System.assertNotEquals(null, newProject.edit());

		
	}

}