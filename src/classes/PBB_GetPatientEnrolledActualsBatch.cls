/**
 * @author Ramya
 * @Date 2015
 * @Description This class batch job that is scheduled nightly for upserting Actual subjects from site object.
 */
global class PBB_GetPatientEnrolledActualsBatch extends PRA_BatchaQueueable {

    private String oName ;
    private Boolean serialExecution;
    private String query;
    public static Datetime ProcessstartDate = system.now();
    public static Date Weekstart;
  
    global PBB_GetPatientEnrolledActualsBatch (){
    }
  
    global override void setBatchParameters(String parametersJSON){
    
        // set paramters from Json string
        List<String> params = (List<String>)Json.deserialize(parametersJSON, List<String>.class);
        oName = params.get(0);
    }
  
    global override Database.QueryLocator start(Database.BatchableContext BC){
        
        List<AggregateResult> CountprotocolList = new List<AggregateResult>();
        List<Id> ProtocolIdlist = new List<Id>();
    
        CountprotocolList = PBB_DataAccessor.getCountryweeklyeventProtocoldata();
        system.debug('-----CountprotocolList ---'+CountprotocolList);
        for(AggregateResult pro:CountprotocolList){
            ProtocolIdlist.add(String.valueOf(pro.get('protocolId')));
        }
        system.debug('-----ProtocolIdlist---'+ProtocolIdlist);
            
        query = 'select Id, Name, Protocal_Unique_Key__c, Expected_Patients__c '  +
                'from ' + oName + ' where  Status__c = \'In Progress\' and Id in:ProtocolIdlist';
     
        if (Test.isRunningTest()){
            query += ' Limit 200';
        }
        System.debug('query' + ' | ' + oName + ' | ');
        return Database.getQueryLocator(query);
    }
  
    global override void execute(Database.BatchableContext BC, List<Sobject> scope){
  
        // get Week start date
        Date myDate = date.newinstance(ProcessstartDate.year(), ProcessstartDate.month(), ProcessstartDate.day());
        Date WeekstartDate = myDate.toStartofWeek();
        Datetime dt = DateTime.newInstance(WeekstartDate.year(), WeekstartDate.month(), WeekstartDate.day());
        String dayOfWeek = dt.format('E');
        
        //to check condition for all countries time zone
        if(dayOfWeek == 'Sun'){
            Weekstart = WeekstartDate+1;
        }else{
            Weekstart = WeekstartDate;
        }     
        system.debug('@@@@@Weekstart'+Weekstart);
        
        List<Country_Weekly_Event__c> Countryweeklylist = new List<Country_Weekly_Event__c>();
        List<Country_Weekly_Event__c> CWEList= new List<Country_Weekly_Event__c>();
        Map<String,Decimal> protocolcntryMap = new Map<String,Decimal>();
        Set<string> SiteIds = new Set<string>();
                
        for(Sobject sObj : scope){
        	WFM_Protocol__c w1 = (WFM_Protocol__c)sObj;
        	SiteIds.add(w1.Protocal_Unique_Key__c);
        }
        
        for(Protocol_Country__c pc : PBB_DataAccessor.getProtocolCountryListByProtocolUniqueSet(SiteIds)){
        	SiteIds.add(pc.Region__r.Name);
        	protocolcntryMap.put(pc.External_Id__c,0);
        }
        
        for(AggregateResult wsd:PBB_DataAccessor.getCountEnrolledForSiteAgg(SiteIds)){
            protocolcntryMap.put(String.valueof(wsd.get('protocolunq'))+':'+String.valueof(wsd.get('ctry')),integer.valueof(wsd.get('CountEnrolled')));  
        }  
        system.debug('-----protocolcntryMap---'+protocolcntryMap.size()+protocolcntryMap);         
        
        for(Country_Weekly_Event__c cwe :PBB_DataAccessor.getCountryweeklyeventdata (protocolcntryMap.keyset(),Weekstart)){ 
            system.debug('-----Countryweeklylist---'+Countryweeklylist);  
            cwe.Actual_Subjects__c = protocolcntryMap.get(cwe.Protocol_Country__r.External_Id__c);
            CWEList.add(cwe);  
        }
        upsert CWEList;                                              
    }
  
    global override  void finish(Database.BatchableContext BC){
    
        string paramsJSONString = Json.serialize(new List<String>{'WFM_Protocol__c'});
        PRA_Batch_Queue__c PBBPatientEnrollementActualstbatchQueue = new PRA_Batch_Queue__c(Batch_Class_Name__c = 'PBB_GetReprojectionSubEnrolledBatch',Parameters_JSON__c = paramsJSONString,
                                                                                              Status__c ='Waiting',Priority__c = 5,Scope__c =1 );
        insert PBBPatientEnrollementActualstbatchQueue;
    }  
}