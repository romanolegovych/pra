@isTest(SeeAllData=false)
public class PRA_BatchQueueTest {

    static testMethod void shouldAddBatchToQueueAndStartExecution() {
        // given
        
        // when
        test.startTest();
        String paramsJSONString = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', 'Forecast_Unit__c'});
        String jobId = PRA_BatchQueue.addJob('PRA_ForecastUpdateBatch', paramsJSONString, null);
        test.stopTest();
        
        // then
        system.assertNotEquals(null, jobId);
        PRA_Batch_Queue__c bQueue = [select Id, Name, Apex_JobId__c, Batch_Class_Name__c, CreatedDate, Dependent_Job_List__c, Exception__c, LastModifiedDate, 
                                        Parameters_JSON__c, Priority__c, Status__c
                                    from PRA_Batch_Queue__c where id = :jobId];
        system.assertEquals(PRA_BatchQueue.STATUS_STARTED, bQueue.Status__c);
        system.assertNotEquals(null, bQueue.Apex_JobId__c);  
        
    }
    
     static testMethod void shouldAddBatchWithScopeToQueueAndStartExecution() {
        // given
        
        // when
        test.startTest();
        String paramsJSONString = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', 'Forecast_Unit__c'});
        String jobId = PRA_BatchQueue.addJob('PRA_ForecastUpdateBatch', paramsJSONString, null,PRA_BatchQueue.DEFAULT_JOB_PRIORITY,5);
       // String jobId = PRA_BatchQueue.addJob('PRA_WorkedEffortBatch', null, null,PRA_BatchQueue.DEFAULT_JOB_PRIORITY,5);
        test.stopTest();
        
        // then
        system.assertNotEquals(null, jobId);
        PRA_Batch_Queue__c bQueue = [select Id, Name, Apex_JobId__c, Batch_Class_Name__c, CreatedDate, Dependent_Job_List__c, Exception__c, LastModifiedDate, 
                                        Parameters_JSON__c, Priority__c, Status__c, Scope__c
                                    from PRA_Batch_Queue__c where id = :jobId];
        system.assertEquals(PRA_BatchQueue.STATUS_STARTED, bQueue.Status__c);
        system.assertEquals(5, bQueue.Scope__c);
        system.assertNotEquals(null, bQueue.Apex_JobId__c);
        
    }
    
    static testMethod void shouldNotStartNewBatchBeforePreviousIsDone() {
        // given
        
        // when
        test.startTest();
        String paramsJSONString = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', 'Forecast_Unit__c'});
        String jobId = PRA_BatchQueue.addJob('PRA_ForecastUpdateBatch', paramsJSONString, null);
        String paramsJSONString1 = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', 'Forecast_Unit__c'});
        String dependentJobId = PRA_BatchQueue.addJob('PRA_ForecastUpdateBatch', paramsJSONString1, jobId);
       // String dependentJobId   = PRA_BatchQueue.addJob('PRA_WorkedEffortBatch', null, jobId);
        test.stopTest();
        
        // then
        system.assertNotEquals(null, jobId);
        system.assertNotEquals(null, dependentJobId);
        PRA_Batch_Queue__c bQueue = [select Id, Name, Apex_JobId__c, Batch_Class_Name__c, CreatedDate, Dependent_Job_List__c, Exception__c, LastModifiedDate, 
                                        Parameters_JSON__c, Priority__c, Status__c
                                    from PRA_Batch_Queue__c where id = :dependentJobId];
        system.assertEquals(PRA_BatchQueue.STATUS_INITIAL, bQueue.Status__c);
        system.assertEquals(null, bQueue.Apex_JobId__c);
        
        PRA_BatchQueue bq = new PRA_BatchQueue();
        bq.execute(null);
        bQueue = [select Id, Name, Apex_JobId__c, Batch_Class_Name__c, CreatedDate, Dependent_Job_List__c, Exception__c, LastModifiedDate, 
                                        Parameters_JSON__c, Priority__c, Status__c from PRA_Batch_Queue__c where id = :dependentJobId];
        system.assertEquals(PRA_BatchQueue.STATUS_STARTED, bQueue.Status__c);
        system.assertNotEquals(null, bQueue.Apex_JobId__c);
    }
    
    
     // complex example
    static testMethod void shouldWaitForManyBatchesWithParameters(){
        
        //given
        String projectText = 'Not existing project';
        List<String> jobIds = new List<String>();
        
        client_project__c cp=new client_project__c();
        cp.name='Not existing project';
        cp.Estimated_End_Date__c=date.today();
        cp.Contracted_End_Date__c=date.today();
        insert cp;
        
        //when
        test.startTest();
        String paramsJSONString;
        //Units & Effort
         paramsJSONString = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', projectText});
        String jobId1 =PRA_BatchQueue.addJob('PRA_PMApprovalBatch',paramsJSONString, null);
        jobIds.add(jobId1);
       
        // Effort Ratio && Hours
        paramsJSONString = Json.serialize(new List<String>{'Hour_EffortRatio__c', 'Unit_Effort__r.Month_Applies_To__c', projectText});
        String dependentJobId =PRA_BatchQueue.addJob('PRA_PMApprovalBatch',paramsJSONString, jobId1);
        jobIds.add(dependentJobId);
        test.stopTest();
        
        // then
        for (String jobId : jobIds){
            system.assertNotEquals(null, jobId);
        }
        
        
        List<PRA_Batch_Queue__c> bQueues = [select Id, Name, Apex_JobId__c, Batch_Class_Name__c, CreatedDate, Dependent_Job_List__c, Exception__c, LastModifiedDate, 
                                        Parameters_JSON__c, Priority__c, Status__c
                                    from PRA_Batch_Queue__c where id in :jobIds];
        for (PRA_Batch_Queue__c job : bQueues){
            if (job.Id != dependentJobId){
                system.assertEquals(PRA_BatchQueue.STATUS_STARTED, job.Status__c);
            }
            else {
                system.assertEquals(PRA_BatchQueue.STATUS_INITIAL, job.Status__c);
            }
        }
        // add additional class to queue
        //PRA_BatchQueue.addJob('PRA_WorkedEffortBatch', null, null);
        paramsJSONString = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', 'Forecast_Unit__c'});
        String jobId3 = PRA_BatchQueue.addJob('PRA_ForecastUpdateBatch', paramsJSONString, null);
        
        PRA_BatchQueue bq = new PRA_BatchQueue();
        bq.execute(null);
        
        bQueues = [select Id, Name, Apex_JobId__c, Batch_Class_Name__c, CreatedDate, Dependent_Job_List__c, Exception__c, LastModifiedDate, 
                                        Parameters_JSON__c, Priority__c, Status__c
                                    from PRA_Batch_Queue__c where id in :jobIds];
        for (PRA_Batch_Queue__c job : bQueues){
            if (job.Id != dependentJobId){
                system.assertEquals(PRA_BatchQueue.STATUS_COMPLETED, job.Status__c);
            }
            else {
                system.assertEquals(PRA_BatchQueue.STATUS_STARTED, job.Status__c);
            }
        }
    }
    
    static testMethod void shouldExecuteBatchesInOrder() {
        // given
        List<PRA_Batch_Queue__c> jobsStarted = new List<PRA_Batch_Queue__c>();
        List<PRA_Batch_Queue__c> jobsQueued = new List<PRA_Batch_Queue__c>();
        String paramsJSONString = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', 'Forecast_Unit__c'});
        for (Integer i=0; i< PRA_BatchQueue.MAX_NUMBER_OF_CONCURRENT_BATCH_PROCESSES; i++){
            jobsStarted.add(new PRA_Batch_Queue__c(Batch_Class_Name__c = 'PRA_ForecastUpdateBatch',Parameters_JSON__c = paramsJSONString,Priority__c = PRA_BatchQueue.DEFAULT_JOB_PRIORITY,
                                             Status__c = PRA_BatchQueue.STATUS_INITIAL));
            jobsQueued.add(new PRA_Batch_Queue__c(Batch_Class_Name__c = 'PRA_ForecastUpdateBatch',Parameters_JSON__c = paramsJSONString,Priority__c = PRA_BatchQueue.DEFAULT_JOB_PRIORITY,
                                             Status__c = PRA_BatchQueue.STATUS_INITIAL));
        }
        insert jobsStarted;
        // when
        test.startTest();
        insert jobsQueued;
        paramsJSONString = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', 'Forecast_Unit__c'});
        String jobId = PRA_BatchQueue.addJob('PRA_ForecastUpdateBatch', paramsJSONString, null,PRA_BatchQueue.DEFAULT_JOB_PRIORITY,5);
        //String jobId = PRA_BatchQueue.addJob('PRA_WorkedEffortBatch', null, null);
        test.stopTest();
        
        // then
        jobsStarted = [select Id, Name, Apex_JobId__c, Batch_Class_Name__c, CreatedDate, Dependent_Job_List__c, Exception__c, LastModifiedDate, 
                                        Parameters_JSON__c, Priority__c, Status__c
                                    from PRA_Batch_Queue__c where id in :jobsStarted];
        jobsQueued = [select Id, Name, Apex_JobId__c, Batch_Class_Name__c, CreatedDate, Dependent_Job_List__c, Exception__c, LastModifiedDate, 
                                        Parameters_JSON__c, Priority__c, Status__c
                                    from PRA_Batch_Queue__c where id in :jobsQueued];
                                    
        for (Integer i=0; i< PRA_BatchQueue.MAX_NUMBER_OF_CONCURRENT_BATCH_PROCESSES; i++){
            system.assertEquals(PRA_BatchQueue.STATUS_STARTED, jobsStarted.get(i).Status__c);
            system.assertEquals(PRA_BatchQueue.STATUS_INITIAL, jobsQueued.get(i).Status__c);
        }
    }
    // * Test error handling: when wrong class name is provided
       // * Test error handling: when parent job finished with error
    
    static testMethod void shouldGetErrorOnWrongClass() {
        // given
        
        // when
        test.startTest();
        String paramsJSONString = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', 'Forecast_Unit__c'});
        String jobId            = PRA_BatchQueue.addJob('WrongClassName', paramsJSONString, null);        
        String dependentJobId   = PRA_BatchQueue.addJob('PRA_ForecastUpdateBatch', paramsJSONString, jobId);
        test.stopTest();
        
        // then
        system.assertNotEquals(null, jobId);
        system.assertNotEquals(null, dependentJobId);
        List<PRA_Batch_Queue__c> bQueue = [select Id, Name, Apex_JobId__c, Batch_Class_Name__c, CreatedDate, Dependent_Job_List__c, Exception__c, LastModifiedDate, 
                                        Parameters_JSON__c, Priority__c, Status__c
                                    from PRA_Batch_Queue__c where id in (:dependentJobId, :jobId)];
        for(PRA_Batch_Queue__c job : bQueue){
            if (job.id == jobId){
                system.assertEquals(PRA_BatchQueue.STATUS_ERROR, job.Status__c);
                system.debug('--Error msg: ' + job.Exception__c);
            }
            else {
                system.assertEquals(PRA_BatchQueue.STATUS_ERROR, job.Status__c);
                system.debug('--Error msg: ' + job.Exception__c);
                system.assertEquals(PRA_BatchQueue.ERROR_PARENT_JOB_FINISHED_WITH_ERROR, job.Exception__c);
            }
        }
    }
    
    //Test behaviour when apex job (batch) ends with error (or it's aborded)
    
     static testMethod void shouldGetApexJobError() {
        // given
        String jobId;
        try {
            // when
            test.startTest();
            // add job with wrong parametest
            String paramsJSONString = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Hour_Applies_To__c_WrongField', 'Forecast_Unit__c'});
            jobId = PRA_BatchQueue.addJob('PRA_ForecastUpdateBatch', paramsJSONString, null);
            
            // on stopTest we will get exception from execeution of batch process (from other thread)
            test.stopTest();
            
            // then
            
        }
        catch (Exception ex){
            system.debug('--Caught exception: ' + ex);
        }
        
        system.assertNotEquals(null, jobId);
        PRA_Batch_Queue__c bQueue = [select Id, Name, Apex_JobId__c, Batch_Class_Name__c, CreatedDate, Dependent_Job_List__c, Exception__c, LastModifiedDate, 
                                        Parameters_JSON__c, Priority__c, Status__c
                                    from PRA_Batch_Queue__c where id = :jobId];
        system.assertEquals(PRA_BatchQueue.STATUS_STARTED, bQueue.Status__c);
        system.assertNotEquals(null, bQueue.Apex_JobId__c);
        
        // Although there was exception on execution of apex job it still is in preparation status
        // that's why You have to manuly abort him
        system.abortJob(bQueue.Apex_JobId__c);
        
        // update status as job finshed with error
        PRA_BatchQueue bq = new PRA_BatchQueue();
        bq.execute(null);
        bQueue = [select Id, Name, Apex_JobId__c, Batch_Class_Name__c, CreatedDate, Dependent_Job_List__c, Exception__c, LastModifiedDate, 
                                        Parameters_JSON__c, Priority__c, Status__c
                                    from PRA_Batch_Queue__c where id = :jobId];
        AsyncApexJob aaj = [SELECT Id, Status, NumberOfErrors, JobItemsProcessed,ExtendedStatus,TotalJobItems
                                    FROM AsyncApexJob WHERE id = :bQueue.Apex_JobId__c];
        system.debug('-- AAJ: ' + aaj);
        system.assertEquals('Aborted', aaj.Status);
        system.assertEquals(PRA_BatchQueue.STATUS_ERROR, bQueue.Status__c);
        system.debug('--Apex Job error msg: ' + bQueue.Exception__c);
        system.assertNotEquals(null, bQueue.Apex_JobId__c);
    }
    
    //Test error handling: add job with not existing parent 
    
     static testMethod void shouldFailOnParentJobNotFound() {
        // given
        
        // when
        test.startTest();
        String paramsJSONString = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', 'Forecast_Unit__c'});
        String delJobId = PRA_BatchQueue.addJob('PRA_ForecastUpdateBatch', paramsJSONString, null);
        delete [select id from PRA_Batch_Queue__c where id = :delJobId];
        String jobId = PRA_BatchQueue.addJob('PRA_ForecastUpdateBatch', paramsJSONString, delJobId);
        test.stopTest();
        
        // then
        system.assertNotEquals(null, jobId);
        PRA_BatchQueue bq = new PRA_BatchQueue();
        bq.execute(null);
        
        PRA_Batch_Queue__c bQueue = [select Id, Name, Apex_JobId__c, Batch_Class_Name__c, CreatedDate, Dependent_Job_List__c, Exception__c, LastModifiedDate, 
                                        Parameters_JSON__c, Priority__c, Status__c
                                    from PRA_Batch_Queue__c where id = :jobId];
        system.debug('--job: ' + bQueue);
        system.assertEquals(PRA_BatchQueue.STATUS_ERROR, bQueue.Status__c);
        
        // check static function
        String inStr = '1234567890';
        system.assertEquals(9,PRA_BatchQueue.cutStringToMaxSize(inStr, 9).length());
    }
    
    
     static testMethod void shouldExecutionOnPRA_BatchaQueueable() {
        // given
        
        // when
        test.startTest();
        String projectText = 'Not existing project';
      
        String paramsJSONString = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', 'Forecast_Unit__c'});
        PRA_Batch_Queue__c UnitEffortbatchQueue = new PRA_Batch_Queue__c(Batch_Class_Name__c = 'PRA_ForecastUpdateBatch',Parameters_JSON__c = paramsJSONString,
                                      Status__c ='Waiting',Priority__c = 5 );
        insert UnitEffortbatchQueue;
        
        PRA_BatchaQueueable batch=new PRA_BatchaQueueable();
        
        batch.query='select id from user limit 1';
        batch.setBatchParameters(UnitEffortbatchQueue.Parameters_JSON__c);           
        
        String apexJobId;
        apexJobId = database.executebatch(batch);            
        test.stopTest();
       
        
       
        
    }
}