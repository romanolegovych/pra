/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PBB_BudgetDataAccessorTest {

    static testMethod void testCreateBudgetDataStruct() {
        PBB_TestUtilsIntegrated tu = new PBB_TestUtilsIntegrated();
        
        /* All of the code coverage for this test will happen in the
           setup.
           Since other unit tests in this class will need a "created"
           project, the setup code will create that data.
        */ 
        PBB_BudgetDataAccessorTest.createBudget(tu);

        list<Scenario_Budget__c> testSB = [SELECT id FROM Scenario_Budget__c WHERE active__c = true];
        list<Scenario_Budget_Task__c> testSBT = [SELECT id FROM Scenario_Budget_Task__c];
        list<Scenario_Budget_RoleCountry__c> tstSBRC = [SELECT id FROM Scenario_Budget_RoleCountry__c];
        
        System.assertequals(1, testSB.size());
        System.assertequals(3, testSBT.size());
        System.assertequals(9, tstSBRC.size());
        
        /* This data accessor method relies on having active budgets
           created.  Since we have one, let's test here and leverage
           our setup resources.
        */
        list<Scenario_Budget__c> lstActiveBudgets =  PBB_BudgetDataAccessor.getActiveBudgets(tu.objPbbScenario.id);
        System.assert(lstActiveBudgets.size() > 0);
        
        /* This data accessor method relies on having a budget created.
           Since we have one, let's test here and leverage
           our setup resources.
        */
        Set<ID> setCST = new Set<ID>();
        for (Countries_Service_Tasks__c cst : tu.lstCountriesServiceTasks) {
            setCST.add(cst.id);
        }
        list<Scenario_Budget_RoleCountry__c> lstRoleCountries = PBB_BudgetDataAccessor.getRoleCountriesForCountryTasks(setCST);
        System.assert(lstRoleCountries.size() > 0);
    }    
    
    static testMethod void testJobMaps() {
        PBB_TestUtilsIntegrated tu = new PBB_TestUtilsIntegrated();
        tu.createJobs(3);

        set<ID> setJobIds = new set<ID>{tu.lstJobClassDesc[0].id, tu.lstJobClassDesc[1].id, tu.lstJobClassDesc[2].id}; 
        map<ID, Job_Class_Desc__c> mapJobById_1 = PBB_BudgetDataAccessor.getJobCodes(setJobIds);
        System.assert(mapJobById_1.size() > 0);
        
        set<String> setJobCodes = new set<String>{tu.lstJobClassDesc[0].Job_Code__c, tu.lstJobClassDesc[1].Job_Code__c, tu.lstJobClassDesc[2].Job_Code__c};
        map<String, Job_Class_Desc__c> mapJobByCode_1 = PBB_BudgetDataAccessor.getJobIds(setJobCodes);
        System.assert(mapJobByCode_1.size() > 0);
        
        map<ID, Job_Class_Desc__c> mapJobById_2 = new map<ID, Job_Class_Desc__c>(); 
        map<String, Job_Class_Desc__c> mapJobByCode_2 = new map<String, Job_Class_Desc__c>();
        PBB_BudgetDataAccessor.getMappedJobs(setJobCodes, mapJobById_2, mapJobByCode_2);
        System.assert(mapJobById_2.size() > 0);
        System.assert(mapJobByCode_2.size() > 0);
    }
    
    static testMethod void testCountryCodeMap() {
        PBB_TestUtilsIntegrated tu = new PBB_TestUtilsIntegrated();
        tu.createCountries();
        
        set<ID> setCountryIds = new set<ID>();
        for (Country__c ctry : tu.lstCountry) {
            setCountryIds.add(ctry.id);
        }
        map<ID, Country__c> mapCountryCode = PBB_BudgetDataAccessor.getCountryCodes(setCountryIds);
        System.assert(mapCountryCode.size() > 0);
    }
    
    private static void createBudget(PBB_TestUtilsIntegrated tu) {
        tu.createCurrency();
        tu.createCountries();
        tu.createJobs(3);
        tu.createServiceModel();
        tu.createServiceAreas(tu.objServiceModel, 3);
        tu.createServiceFunctions(tu.lstServiceArea, 3);
        tu.createServiceTasks(tu.lstServiceFunction, 2);
        tu.pbbTestUtils.approveServicemodel(tu.objServiceModel);
        tu.createPbbScenario(null);
        tu.createBillRateCardModel();
        System.debug('---- ' + tu.objPbbScenario);
        System.debug('---- ' + tu.lstCountry);
        tu.createScenarioCountries(tu.objPbbScenario, 
                                   new list<Country__c> {tu.lstCountry[tu.COUNTRY_US], tu.lstCountry[tu.COUNTRY_UK] },
                                   true);
        
        list<list<sObject>> countriesAndTasks = new list<list<sObject>>();
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[0]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[1]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[2], tu.lstServiceTask[4]});
        tu.createCountriesServiceTasks(countriesAndTasks);
        
        // Add Bill Rate Card to PBB Scenario
        tu.objPbbScenario.Bill_Rate_Card_Model__c = tu.objBillRateCardModel.id;
        update tu.objPbbScenario;
        
        // Create Mocks
        MockPbbPawsData thePawsContext = new MockPbbPawsData();
        thePawsContext.createMockData(tu.lstJobClassDesc);
        MockBudgetBillRateService theBillRateContext = new MockBudgetBillRateService();

        PBB_BudgetEngineService theService = new PBB_BudgetEngineService();
        theService.setPawsDataContext(thePawsContext);
        theService.setBudgetBillRateContext(theBillRateContext);
        theService.createBudget(tu.objPbbScenario.id);
        
        //Added to call budget task and budget scenario country 
        List<AggregateResult> scenbudgettask = PBB_BudgetDataAccessor.getAggFunctionalCntrygriddata(tu.objPbbScenario.id);
        System.assert(scenbudgettask.size() > 0);
        
        List<Scenario_Budget_Task__c> scenbudgettasks = PBB_BudgetDataAccessor.getclientunitgriddata(tu.objPbbScenario.id);
        System.assert(scenbudgettasks.size() > 0);
         
        List<Scenario_Budget_RoleCountry__c> scenbudgettaskcountry = PBB_BudgetDataAccessor.getFunctionalCntrygriddata(tu.objPbbScenario.id);
        System.assert(scenbudgettaskcountry.size() > 0);
    }
    
    private class MockPbbPawsData implements PBB_BudgetEngineService.IPawsData {
        list<PBB_BudgetEngineService.PawsWorkItem> mockData = new list<PBB_BudgetEngineService.PawsWorkItem>();
        
        public MockPbbPawsData() {}

        public List<PBB_BudgetEngineService.PawsWorkItem> getWorkBreakdown(ID aPawsProjectId) {
            System.debug('---- mockData: ' + mockData);
            return mockData;                        
        }
        
        public void createMockData(list<Job_Class_Desc__c> aLstJCD) {
            list<Countries_Service_Tasks__c> lstCST = [SELECT Service_Task__r.id, PBB_Scenario_Country__r.name, PBB_Scenario_Country__r.Country__r.id
                                                       FROM Countries_Service_Tasks__c];
            for (Countries_Service_Tasks__c cst : lstCST) {
                for (Job_Class_Desc__c jcd : aLstJCD) {
                    PBB_BudgetEngineService.PawsWorkItem theItem =
                    new PBB_BudgetEngineService.PawsWorkItem(jcd.Job_Code__c,
                                                             100,
                                                             Date.today(),
                                                             Date.today().addMonths(3),
                                                             cst.Service_Task__c,
                                                             cst.PBB_Scenario_Country__r.name,
                                                             cst.PBB_Scenario_Country__r.Country__r.id);
                    mockData.add(theItem);
                }
            }
        }
    }
    
    private class MockBudgetBillRateService implements PBB_BudgetBillRateService.IBudgetBillRateService {
        public void setBaseParameters(ID aRateCard, ID aCurrency) {}
        public void setCountryParameters(Set<ID> aCountries) {}
        public void setJobParameters(Set<ID> aJobs) {}
        public void loadBillRates() {}
        public decimal getBillRate(String aCountryId, String aJobClassDescId, String aYear) {
            return 10;
        }
        public decimal getWeightedRateEven(String aCountryId, String aJobClassDescId, Date aStartDate, Date aEndDate) {
            return 10;
        }
    }    
    
}