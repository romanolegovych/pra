public with sharing class BDT_NewEditSponsorEntity {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Administration Sponsors
	public String sponsorId {get;set;}
	public Account sponsorEntity {get;set;}
	public String sponsorEntityId {get;set;}
	public Boolean showDeleted{
		get {
			if (showDeleted == null) {
				showDeleted = false;
			}
			return showDeleted;
		}
		set;
	}
	
	public BDT_NewEditSponsorEntity(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Sponsors');
		sponsorEntityId =  System.CurrentPageReference().getParameters().get('sponsorEntityId');
		
		sponsorId = System.currentPageReference().getParameters().get('sponsorId');
		if (sponsorId == null || sponsorId == '') {  			
			sponsorId = System.currentPageReference().getParameters().get('customId');
		}
		
		if(sponsorEntityId!=null && sponsorEntityId!=''){
			try{
				sponsorEntity = [Select a.Id, a.Client_Abbreviation__c, a.BillingStreet, a.BillingState, 
									a.BillingPostalCode, a.BillingCountry, a.BillingCity, a.Sponsor__c, a.name, a.CurrencyLookup__c, a.CountryLookup__c
									From Account a  
									Where a.id = :sponsorEntityId];
			}catch(QueryException e){
				sponsorEntity = new Account();
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Site Id could not be found. ');
				ApexPages.addMessage(msg); 
			}
		}else{
			if(sponsorId!=null && sponsorId!='' && sponsorId!='empty'){
				sponsorEntity = new Account(Sponsor__c = sponsorId);
			}
		}
	}
	
	public PageReference save(){
		upsert sponsorEntity;
		return returnToBDTSponsors();
	}
	
	public PageReference cancel(){
		return returnToBDTSponsors();
	}
	
	public PageReference deleteSoft(){
		// no softdelete on account
		try{
			delete sponsorEntity;
		} catch (Exception e) {
			// delete failed, likely because of relations to other records
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Deletion failed with: '+e);
			ApexPages.addMessage(msg); 
		}
		return returnToBDTSponsors();
	}
	
	public PageReference returnToBDTSponsors(){
		PageReference sponsorPage = new PageReference(System.Page.BDT_Sponsors.getUrl());
		sponsorPage.getParameters().put('sponsorId', sponsorEntity.Sponsor__c);
		return sponsorPage;
	}
}