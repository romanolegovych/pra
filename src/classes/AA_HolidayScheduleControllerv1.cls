/**
* The HolidayScheduleController program has actions and fucntions
* for storing and displaying Holidays for different Countries and 
* their States.
*
* @author Parul Upadhyaya    
* @version 1.0
* @since July 2014
*/

public with sharing class AA_HolidayScheduleControllerv1 {

    public String SelectedCountry {get; set;}
    public Integer SelectedYear {get; set;}
    
    
    public List<String> SelectedState {get; set;} 
    public String holidayName {get; set;}
    public String holidayDate {get; set;}  //format mm/dd/yyyy  
    public Double holidayDuration {get; set;}
    
    
    public List<Holiday_Schedule__c> holidaysDisplay {get; set;}
    public String paramEdit {get;set;}
    public String paramDel {get; set;}
    public String paramClone {get; set;}
    public String deleteid {get; set;} 
    public String stat {get; set;}
    
    
    public Boolean renderPanel{get; set;}

    public Boolean isEdit {get; set;}
    public Boolean addOrEdit{get; set;} 
 
    public Boolean blnSave {get; set;}
    public Boolean isReadOnly {get; set;}
    public Boolean boolm {get; set;}
   
    public Boolean enableDetails {get; set;}  
 
    public Boolean isSaveEdit{get;set;}
   

/** 
*
* The constructor sets default values on initialisation.  
* The function displayCountryHoliday() is called
* to display the holiday records. 
*
*/  

    public AA_HolidayScheduleControllerv1() {
        
        try {
            // Default values
            holidayDuration = 1.0;
            SelectedYear = Date.today().Year();
            isSaveEdit = false;
            blnSave = true;
            isEdit = false;
            addOrEdit = false;
            renderPanel = false;
            boolm = false;
            
            displayCountryHoliday();
        
        } catch(Exception e) {}
    }  
    
    
/**
*
* This method contains functionality for 'Reset' button.
* It resets the page to original state [null] (First section containing
* Country and Year is displayed).
*
*/    
    
    public PageReference resetCountryHoliday() {
        
        SelectedCountry =null;
        SelectedYear =  Date.today().Year();
        holidaysDisplay.clear();
           
        return null;
        
   }
   
   
/**
*   
* This method contains functionality for 'Search' button.
* Holidays are selected to be displayed, on the basis of 
* country and year selected by the user.
*
*/   
  
    
    public PageReference displayCountryHoliday() {   
        
        system.debug('=================>>>>render'+ renderPanel);
        renderPanel = true;   // Can remove renderPanel
        system.debug('=======>>>>render'+ renderPanel);
        
        try{
            holidaysDisplay = AA_Service.getHolidayByCountryYear(SelectedCountry, SelectedYear);   
       }catch (Exception e) {} 
        
       return null;
   }


  
/**
*  
* This method updates the row entries. The current value of Holiday name 
* gets updated to new value provided by the user in 'Holiday name' field.
* Needs to be updated to reflect new date changes. 
* 
* addOrEdit controls the execution of 'Add New' and 'Edit' button fuctionality.
* isSaveEdit controls the 'show/hide' function of 'Holiday Information'  section.
*
* displayOutput() saves the new entry into the database.
* updateNewEntry() updates a particular entry based on the id number of the row selected. 
*
*/  

    public void saveOrEditFunc() {
    
        if(!addOrEdit)
          displayOutput();     
        else
          updateNewEntry(); 
          isSaveEdit = false;  // Hides the 'Holiday Information' section
            
    }
    
/**
*
* This method controls the execution of 'Save' and 'Save and New' 
* button functionality.
*
* When 'Save and New' is selected, the middle section - 'Holiday inofrmation'  
* is retained.
*
*/    
    
    public void saveAndNewFunc(){
      
        saveOrEditFunc();
        isSaveEdit = true; 
       
        return;
        
    }
   
   
/*
*
* This method controls the fucntionality of 'Cancel' button.
* It collapses the middle section 'Holiday Information' on selection.
*
*/ 
   
   public void cancelFunc(){
   
        isSaveEdit = false;
       
        return;
    
    }
    
    
/**
*
* This method controls the functionality of 'Edit' button.
* It is responsible for updated the existing entries.
*    
*/ 
    
    public PageReference updateNewEntry() {
        try {
            System.Debug('=====================>>>New Holiday Name'+ holidayName);
           
            if(holidayName == null || 0 == holidayName.trim().length() || holidayDate == null) {
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Field cannot be left empty'));
            } 
            else {
                try {
                    System.Debug('==============>>> isEdit'+ isEdit);
                    if(isEdit) {
                        enableDetails = false;
                         
                        Holiday_Schedule__c  hsObjUpdate = AA_service.getHolidayDetailsById(paramEdit);         
                        hsObjUpdate.Holiday_Name__c = holidayName;
                        hsObjUpdate.Holiday_Date__c = RM_Tools.GetDateFromString(holidayDate, 'mm/dd/yyyy'); 
                        hsObjUpdate.Status__c = 'Draft';
                       
                        update hsObjUpdate;
                       
                        holidayName = null;
                        holidayDate = null;
                        paramEdit = null;
                        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Entry Updated.'));
                    } 
                } catch (Exception e) {}
            }
        }  
        catch(Exception e) {}   
        displayCountryHoliday();  
        return null;        
    }   
   
   
/**
*
* This method makes the middle section 'Holiday Information' visible.
* The fields are set to null values.
*
*/   
    
    public void AllowAdd() {
        System.Debug('=====================>>in AllowADD');
        isSaveEdit = true;
        isEdit = false;
        
        holidayName = null;
        holidayDate = null;
        
        return; 
   } 
    
/**
* This function retrieves the row that matches the id of the row selected by the 
* user. It is then output to the fields 'Holiday Name' and 'Date'
* 
*/   
    
    public void Edit() {
        try {
            enableDetails = true;
            addOrEdit = true;
      
            isEdit = true;
            isSaveEdit = true;
            System.debug('===============>>> Edit ID'+ paramEdit);
            Holiday_Schedule__c editObj  = AA_service.getHolidayDetailsById(paramEdit); 
            System.debug('===============>>> editObj'+ editObj);
            holidayName = editObj.Holiday_Name__c;
            holidayDuration = editObj.Duration_of_Day__c;
            holidayDate =  RM_Tools.GetStringfromDate(editObj.Holiday_Date__c, 'mm/dd/yyyy');
            System.debug('===============>>> holidayDate'+ holidayDate);
        }
        catch(Exception e){}
        
       
        return;  
    }
    
/**
* This section is for 'Delete' functionality. 
* Based on the id of the row entry selected, a particular entry is deleted. 
*
*/    
    
    public void Del() {
        try {
            Holiday_Schedule__c delObj = AA_Service.getHolidayToDelete(paramDel);
            delete delObj;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Deleted'));
            }
        catch(Exception e) {}
        
        displayCountryHoliday();
        return;     
    }
    
    
/**
*
* This method enables the user to change the initial 'Draft' status of an entry to 
* 'Approved' status.
*
*/    
 
    public PageReference ApproveAll(){
    
            stat = 'Draft';
            List<Holiday_schedule__c> approveList = AA_service.getHolidayToApprove(SelectedCountry, SelectedYear, stat); 
            List<Holiday_Schedule__c> approveChangeList = new List<Holiday_Schedule__c>();
            try {
              for(Holiday_Schedule__c approveObj : approveList) {
                 approveObj.Status__c = 'Approved';
                 approveChangeList.add(approveObj);  
              } 
              update approveChangeList; 
            } catch(Exception e) {}  
              
              displayCountryHoliday();
              return null; 
     }


/**
* This method creates a duplicate of the holiday entries with Date incremented by one year.
* 
* It clones all the entries corresponding to the selected 'country' and the selected 'year'.
*/

     public PageReference cloneAll() {
     
            system.debug('=============>>>Selected Year' + SelectedYear);
            system.debug('=============>>>Selected Country' + SelectedCountry);
            
        
            List<Holiday_schedule__c> cloneList = AA_service.getHolidayToClone(SelectedCountry, SelectedYear);
            List<Holiday_Schedule__c> cloneObject = new List<Holiday_Schedule__c>();
        try {
            
            for(Holiday_Schedule__c cloneAllObj : cloneList) {
                        
            Holiday_Schedule__c newObj = new Holiday_Schedule__c();
            
            newObj.Holiday_Name__c = cloneAllObj.Holiday_Name__c;
            newObj.Duration_of_Day__c = cloneAllObj.Duration_of_Day__c;
            newObj.Holiday_Country_State__c = cloneAllObj.Holiday_Country_State__c;
            Integer month = Integer.valueOf(cloneAllObj.Holiday_Date__c.month());
            Integer year = Integer.valueOf(cloneAllObj.Holiday_Date__c.year());
            if((math.mod(year,400) == 0 || math.mod(year,4) == 0 && math.mod(year,100) != 0) || (math.mod(year+1,400) == 0 || math.mod(year+1,4) == 0 && math.mod(year+1,100) != 0))
                newObj.Holiday_Date__c = cloneAllObj.Holiday_Date__c + 366;
            else  
                newObj.Holiday_Date__c = cloneAllObj.Holiday_Date__c + 365;
            
            cloneObject.add(newObj);
            system.debug('====================>>>>clone check'+newObj.Duration_of_Day__c);
           } 
           
          }
        catch(Exception e) {}
        
        insert cloneObject;
        
        displayCountryHoliday();
        
        return null;     
    }  

/**
   
/**
* cloneEntry() creates a copy of the entry selected with year incremented by one.
*
* Not in use
*
  
    
    public PageReference cloneEntry() {
        try {
            Holiday_Schedule__c CloneObj = [Select Holiday_Name__c,  Holiday_Country_State__c,  Holiday_Date__c, Duration_of_Day__c from Holiday_Schedule__c where id=:paramClone limit 1];
            
            Holiday_Schedule__c newObj = new Holiday_Schedule__c();
            
            newObj.Holiday_Name__c = cloneObj.Holiday_Name__c;
            newObj.Duration_of_Day__c = cloneObj.Duration_of_Day__c;
            newObj.Holiday_Country_State__c = cloneObj.Holiday_Country_State__c;
            Integer month = Integer.valueOf(cloneObj.Holiday_Date__c.month());
            Integer year = Integer.valueOf(cloneObj.Holiday_Date__c.year());
            if((math.mod(year,400) == 0 || math.mod(year,4) == 0 && math.mod(year,100) != 0) || (math.mod(year+1,400) == 0 || math.mod(year+1,4) == 0 && math.mod(year+1,100) != 0))
                newObj.Holiday_Date__c = cloneObj.Holiday_Date__c + 366;
            else  
                newObj.Holiday_Date__c = cloneObj.Holiday_Date__c + 365;
            
            insert newObj;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Cloned'));
            
           
        }
        catch(Exception e) {}
        
        return null;     
    }
    
    
  */  
 
/**
* This method adds the new holiday information to the database.
*
* The method name can be renamed.
*/  
 
    
    public PageReference displayOutput(){      
    
     system.debug('=============>>>>>holidayName'+holidayName);
     system.debug('=============>>>>>date'+holidayDate);
     system.debug('=============>>>>>duration'+holidayDuration);
     system.debug('=============>>>>>state'+selectedState);
     try 
        {
         System.Debug('=====================>>>New Holiday Name'+ holidayName);
           
         if(holidayName == null || 0 == holidayName.trim().length() || holidayDate == null) {
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Error,'Field cannot be left empty'));
        } 
        else {
        List<Holiday_Schedule__c> holidayObject = new List<Holiday_Schedule__c>();
        
        List<String> ListItems = new List<String>();    
        for( String x : selectedState ) {  
               ListItems.add(x.trim());
        }
        for(String l : ListItems) {  
            if(holidayDuration == 0.5 || holidayDuration == 1)
            {
                Holiday_Schedule__c hsObj = new Holiday_Schedule__c();
                hsObj.Duration_of_Day__c = holidayDuration;
                hsObj.Holiday_Date__c = RM_Tools.GetDateFromString(holidayDate, 'mm/dd/yyyy'); 
                hsObj.Holiday_Name__c = holidayName;
                system.debug('=========================>>>>>>>>>>l for nill'+ l);
                hsObj.Holiday_Country_State__c = l;
                system.debug('==========>>>lvalue'+l);
                
                holidayObject.add(hsObj);
            }
            else
            {
                Integer count = holidayDuration.IntValue();
                Integer i;
                Date hDate;
                for(i = 0; i < count ; i++)
                {    
                    Holiday_Schedule__c hsObj = new Holiday_Schedule__c();  
                    hsObj.Duration_of_Day__c = 1;
                    system.debug('========================>>>> Date BEFORE'  + holidayDate);
                    hDate = RM_Tools.GetDateFromString(holidayDate, 'mm/dd/yyyy');
                    hDate = hDate + i;
                    hsObj.Holiday_Date__c = hDate; 
                    system.debug('========================>>>> Date AFTER' + hsObj.Holiday_Date__c);
                    hsObj.Holiday_Name__c = holidayName;
                    hsObj.Holiday_Country_State__c = l;
                    
                    holidayObject.add(hsObj);
                } 
            } 
        }
        insert holidayObject;
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'New Holiday added'));
       
        holidayName = null;
        holidayDate = null;
        } 
       }catch(Exception e){}
       
        displayCountryHoliday(); 
        return null;
    } 


/*
* This section controls the display of country list on front-end- picklist.
*
*/     
   
    public List<SelectOption> CountryList {
        get
        { 
            boolm = true;
            List<Country__c> CountrySample = AA_service.getCountryList(boolm);
            CountryList = new List<SelectOption>();
            CountryList.add(new SelectOption('','Select'));         
            for(Country__c c : CountrySample)
            {                 
                CountryList.add(new SelectOption(c.Name, c.Name)); 
            }
            System.debug('===============>>> CountryList' + CountryList);
            return CountryList;
        }
        set;       
    }
    


/*
* This section controls the display of Year picklist on front-end.
*
*/
    
    public List<SelectOption> YearList {
        get
        { 
            System.debug('===============>>> YearList');
            integer thisYear = Date.today().Year();
            YearList = new List<SelectOption>();            
            for (integer i = 0; i<=2; i++){
                YearList.add(new SelectOption(string.valueOf(thisYear+i),string.valueOf(thisYear+i)));
            }    
         
            return YearList;
        }
        set;       
    }
    
    
/*
* This section controls the display of duration picklist on front-end.
*
*/    
    
    public List<SelectOption> DurationList {
        get
        { 
            DurationList = new List<SelectOption>();                  
            DurationList.add(new SelectOption('0.5','0.5'));
            DurationList.add(new SelectOption('1.0','1'));
            if (!isEdit){
                DurationList.add(new SelectOption('2.0','2'));
                DurationList.add(new SelectOption('3.0','3'));
                DurationList.add(new SelectOption('4.0','4'));
                DurationList.add(new SelectOption('5.0','5'));
                DurationList.add(new SelectOption('6.0','6'));
                DurationList.add(new SelectOption('7.0','7'));
            }
            return DurationList;
        }
        set;       
    }
    
    
/**
* This section is responsible for querying the list of states corresponding 
* to the country selected (selectedCountry) and displays the results as multiselect picklist 
* in 'Holiday Information' section.
*/    
    
    public List<SelectOption> StateList {
        get
        {    
            StateList = new List<SelectOption>();
            List<Holiday_Country_State__c> StateSample = AA_service.getStateList(SelectedCountry);         
                  
            for(Holiday_Country_State__c s : StateSample)
            {   
                system.debug('==================>>>>>s.State_Province__r.Name'+s.State_Province__r.Name);                    
                stateList.add(new SelectOption(s.Id, s.State_Province__r.Name)); 
            }
            return StateList;
        }
        set;         
    }
    

}