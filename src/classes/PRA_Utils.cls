/*
* This utility class will hold all of the common and useful methods
* used by controller
*/
public with sharing class PRA_Utils {
    // This inner class represents the user preference selected during Search
    public class UserPreference {
        public String userId {get; set;} // 15 or 18 character SF Id
        public String projectId {get; set;} // 15 or 18 character SF Id
        public List<String> regionId {get; set;} // 15 or 18 character SF Id
        public List<String> countryId {get; set;} // 15 or 18 character SF Id
        public List<String> clientUnitId {get; set;} // 15 or 18 character SF Id
        public List<String> clientUnitGroupId {get; set;} // 15 or 18 character SF Id
        public List<String> operationalAreaId {get; set;} // 15 or 18 character SF Id
        public Boolean unconfirmedOnly {get;set;}
        public Boolean confirmedOnly {get;set;} 
        public String crctId {get;set;}
    }
    
    // Call this method to save the user preference to the database 
    public static void saveUserPreference(UserPreference userPref) {
        
        User_Preference__c userPrefObj;
        // check if the user pref already exists in the db
        List<User_Preference__c> userPrefList = [Select id, unconfirmedOnly__c, confirmedOnly__c, user_id__c, region_id__c, Operational_Area_Id__c, 
                                                    country_id__c, project_id__c, client_unit_id__c, client_unit_group_id__c , Crct_Id__c
                                                    from User_Preference__c where user_id__c = :userPref.UserId ];
        if(userPrefList == null || userPrefList.isEmpty()) {
            //user pref doesn't exist in the database, so create a new one
            userPrefObj = new User_Preference__c();
        } else if(userPrefList.size() > 1 ) { // Check if there are more than 1 user pref record in the database
            // return the last element in the list
            System.debug('The database has more than one user pref record. Cannnot determine which one is right one, returning the last element in the list');
            userPrefObj = userPrefList.get(userPrefList.size()-1);
        } else {
            //else assign the object returned from the database
            userPrefObj = userPrefList.get(0);
        }
        //assign the values, and upsert it
        userPrefObj.User_Id__c              = userPref.userId;
        userPrefObj.Project_Id__c           = userPref.projectId;
        System.debug('userPref.regionId: '+userPref.regionId);
        userPrefObj.Region_Id__c = '';
        for(String regId: userPref.regionId){
            userPrefObj.Region_Id__c += regId + ',';
        }
        userPrefObj.Operational_Area_Id__c ='';
        for(String regId: userPref.operationalAreaId){
            userPrefObj.Operational_Area_Id__c += regId + ',';
        }
        userPrefObj.Client_Unit_Id__c = '';
        for(String regId: userPref.clientUnitId){
            userPrefObj.Client_Unit_Id__c += regId + ',';
        }
        userPrefObj.Client_Unit_Group_Id__c = '';
        for(String regId: userPref.clientUnitGroupId){
            userPrefObj.Client_Unit_Group_Id__c += regId + ',';
        }
        
        userPrefObj.unconfirmedOnly__c      = userPref.unconfirmedOnly;
        userPrefObj.confirmedOnly__c        = userPref.confirmedOnly;
        userPrefObj.Crct_Id__c              = userPref.crctId;
        upsert userPrefObj;
    }
    
    // Call this method to retrieve the latest user preference for this user id from the database
    // return: null => user pref doesn't exist in the db
    //         latest user pref inner class obj => if more than one user pref is found in the db
    public static UserPreference retrieveUserPreference() {
        // Getting the User id
        String userId = UserInfo.getUserId();
        // TODO: validate the user id
        // assume for the time being that a valid user id is being passed to the database
        // get the user pref from the db
        List<User_Preference__c> userPrefList = [Select id, unconfirmedOnly__c, confirmedOnly__c, user_id__c, region_id__c, Operational_Area_Id__c, country_id__c, project_id__c, client_unit_id__c, client_unit_group_id__c, Crct_Id__c from User_Preference__c where user_id__c = :userId ];
        if(userPrefList == null || userPrefList.isEmpty()) {
            //user pref doesn't exist in the database, so return null
            return null;
        } else { 
            // return the last element in the list
            // this statement takes care of more than one user prefs in the database
            User_Preference__c userPrefObj =  userPrefList.get(userPrefList.size()-1);
            UserPreference userPref = new UserPreference();
            userPref.userId             = userPrefObj.User_Id__c;
            userPref.projectId          = userPrefObj.Project_Id__c;
            
            userPref.regionId           = userPrefObj.Region_Id__c.split(',');
            userPref.operationalAreaId  = userPrefObj.Operational_Area_Id__c.split(',');
            //userPref.countryId            = userPrefObj.Country_Id__c.split(',');
            userPref.clientUnitId       = userPrefObj.Client_Unit_Id__c.split(',');
            userPref.clientUnitGroupId  = userPrefObj.Client_Unit_Group_Id__c.split(',');
            
            userPref.unconfirmedOnly    = userPrefObj.unconfirmedOnly__c;
            userPref.confirmedOnly      = userPrefObj.confirmedOnly__c;
            userPref.crctId             = userPrefObj.Crct_Id__c;
            return userPref;
        }
        return null; 
    }
    
    // Delete user preferences
    public static void deleteUserPreference() {
        // Getting the User id
        String userId = UserInfo.getUserId();
        List<User_Preference__c> userPrefList = [Select id, unconfirmedOnly__c, confirmedOnly__c, user_id__c, region_id__c, Operational_Area_Id__c, country_id__c, project_id__c, client_unit_id__c, client_unit_group_id__c from User_Preference__c where user_id__c = :userId ];
        if(userPrefList != null && !userPrefList.isEmpty()) {
            // Deleting it
            delete userPrefList;
        }
    }
    
    // Save User Preference
    // Save User prefrs
    public static void savePrefs(String selectedProject, List<String> selectedRegion, List<String> selectedOperationalArea, List<String> selectedTask, 
                                 List<String> selectedGroup, Boolean unconfirmedOnly, Boolean confirmedOnly){
        PRA_Utils.UserPreference userPref = new PRA_Utils.UserPreference();
        userPref.userId = UserInfo.getUserId();
        userPref.projectId          = selectedProject;
        userPref.regionId           = selectedRegion;
        userPref.operationalAreaId  = selectedOperationalArea;
        userPref.clientUnitId       = selectedTask;
        userPref.clientUnitGroupId  = selectedGroup;
        userPref.unconfirmedOnly    = unconfirmedOnly;
        userPref.confirmedOnly      = confirmedOnly;
        saveUserPreference(userPref);
    }
    public static void savePrefs(String selectedProject, List<String> selectedRegion, List<String> selectedOperationalArea, List<String> selectedTask, 
                                 List<String> selectedGroup, Boolean unconfirmedOnly, Boolean confirmedOnly, String crctId){
        PRA_Utils.UserPreference userPref = new PRA_Utils.UserPreference();
        userPref.userId = UserInfo.getUserId();
        userPref.projectId          = selectedProject;
        userPref.regionId           = selectedRegion;
        userPref.operationalAreaId  = selectedOperationalArea;
        userPref.clientUnitId       = selectedTask;
        userPref.clientUnitGroupId  = selectedGroup;
        userPref.unconfirmedOnly    = unconfirmedOnly;
        userPref.confirmedOnly      = confirmedOnly;
        userPref.crctId             = crctId;
        saveUserPreference(userPref);
    }
    
     // set the userpreferences for a partiular project selected.
    public static void setUserPreferenceforProject(String projectId,PRA_Utils.UserPreference userPreference) {
        
        Integer searchCount=0;
        //check whether the userpreference already exists
        if(userPreference == null) {
            userPreference = new PRA_Utils.UserPreference();
            searchCount++;                                    
        } else { 
            if(userPreference.projectId!=projectId)
                searchCount++;
        }
        userPreference.projectId = projectId;  //Update the projectID with new projectID.
        userPreference.regionId = new String[]{PRA_Constants.ST_NONE};
        userPreference.countryId = new String[]{PRA_Constants.ST_NONE};
        userPreference.clientUnitId = new String[]{PRA_Constants.ST_NONE};
        userPreference.clientUnitGroupId = new String[]{PRA_Constants.ST_NONE};
        userPreference.operationalAreaId = new String[]{PRA_Constants.ST_NONE};
        userPreference.unconfirmedOnly = false;
        userPreference.confirmedOnly = false;
        userPreference.crctId = '';
        userPreference.userId = UserInfo.getUserId();
           
        
        system.debug('-----------'+searchCount+projectId+userPreference);     
        if(searchCount > 0 ) {
            // Save user prefs
            PRA_Utils.saveUserPreference(userPreference);
        }
    } 
    
}