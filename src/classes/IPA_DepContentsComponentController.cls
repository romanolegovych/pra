public class IPA_DepContentsComponentController
{
    public IPA_Page_Widget__c pageWidgetObj {get; set;}
    public string showDiv {get; set;}
    public List<IPA_Articles__c> lstContents {get; set;}
    public IPA_Articles__c featuredContent {get; set;}
    
    public void getfetchContents() 
    {
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        if(pageWidgetObj != null)
        {
            lstContents = ipa_bo.returnDepartmentContents(pageWidgetObj);
            if(lstContents.size() == 0)
                hide();
            else
            {
                showDiv = '';
                featuredContent = lstContents[0];
                lstContents.remove(0);
            }
        }
        else
            hide();
    }
    
    private void hide()
    {
        featuredContent = new IPA_Articles__c();
        lstContents = new List<IPA_Articles__c>();
        showDiv = 'hidden';
    }
}