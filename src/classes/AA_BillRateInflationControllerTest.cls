@isTest   
private class AA_BillRateInflationControllerTest {
    
    static testMethod void  testAA_BillRateInflationController(){
        
        AA_TestUtils utils = new AA_TestUtils();
        //AA_BillRateInflationController bill1 = new AA_BillRateInflationController();
        Bill_Rate__c br1 = utils.createBillRate();
        
        PageReference ref = Page.aa_billrate_inflationoverride;
        ref.getParameters().put('country', br1.country__c );
        ref.getParameters().put('jobPosition', br1.Job_Position__c );
        ref.getParameters().put('scheduledYear', br1.Schedule_Year__c);
        ref.getParameters().put('modelId', br1.Model_Id__c);
        
        Test.setCurrentPage(ref);
        
        AA_BillRateInflationController bill = new AA_BillRateInflationController();
        //bill.getresult();
        System.assertNotEquals(null, bill.result );
    }   
    
    static testMethod void testApply(){
        AA_TestUtils utils = new AA_TestUtils();
        
        Bill_Rate__c br1 = new Bill_Rate__c(); 
        br1 = utils.createBillRate();
        
        PageReference ref = Page.aa_billrate_inflationoverride;
        ref.getParameters().put('country', br1.country__c );
        ref.getParameters().put('jobPosition', br1.Job_Position__c );
        ref.getParameters().put('scheduledYear', br1.Schedule_Year__c);
        ref.getParameters().put('modelId', br1.Model_Id__c);
        
        Test.setCurrentPage(ref);
        
        AA_BillRateInflationController bill = new AA_BillRateInflationController();
        bill.getresult();
        
        bill.InflationRate = '2.0';
        bill.apply();
        
        bill.InflationRate = '3.0';
        bill.applyInflationStartYear = 2006;
        
        bill.apply();
        
        Bill_Rate_Card_Model__c bmm = new Bill_Rate_Card_Model__c(); 
        bmm = utils.createBillModel();
        ApexPages.currentPage().getParameters().put(System.URL.getSalesforceBaseUrl().toExternalForm() + '/',bmm.Id);
        pageReference ss = bill.back();
        pageReference pp = bill.save();
        pageReference oo = bill.deleterecords();
    }
    
    
    
    
}