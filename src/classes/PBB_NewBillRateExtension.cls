public class PBB_NewBillRateExtension {
// Instance fields
    public String searchTerm {get; set;}
    
    private final List<Currency__c> currencies;
    public String  jobPosition            { get; set; }
    public String selectedCountry         { get; set; }
    public Bill_Rate__c  billRate {get;set;}
    
    public String selectedCurrency{get;set;}
    public String yearValue{get;set;}
    public Decimal initialRateValue{get;set;}
    public Bill_Rate__c rate{get;set;}
    private Boolean error = false;
    Set<String> uniqueBillRateValues{ get; set; }

    public PBB_NewBillRateExtension(ApexPages.StandardController controller) {
        yearValue ='2000';
        uniqueBillRateValues = new Set<String>();
        rate = (Bill_Rate__c)controller.getRecord();
        
        Bill_Rate_Card_Model__c model = [select Id, Name, Year__c from Bill_Rate_Card_Model__c where Id=: rate.Model_Id__c limit 1];
        rate.Schedule_Year__c = model.Year__c;
        
        currencies = [SELECT  Id,Name FROM Currency__c WHERE NAME='USD' or Name='EUR' order by Name];
        List<Bill_Rate__c> billrates = [SELECT Id,
                                                 Year_Rate_Applies_To__c,
                                                 Schedule_Year__c,
                                                 Bill_Rate__c,
                                                 Currency__c,
                                                 Currency__r.Name,
                                                 CreatedBy.Name,
                                                 CreatedDate,
                                                 LastModifiedBy.Name,
                                                 LastModifiedDate,
                                                 IsActive__c,
                                                 country__c,
                                                 country__r.Name,
                                                 Job_Position__c,
                                                 Job_Position__r.Name,
                                                 Model_Id__c
                                          FROM   Bill_Rate__c 
                                          WHERE Model_Id__c =: rate.Model_Id__c];
        
        if(!billRates.isEmpty()){
            for(Bill_Rate__c rt: billrates){
                uniqueBillRateValues.add((rt.Model_Id__c + '' + rt.Job_Position__c +'' + rt.Country__c +'' + rt.Schedule_Year__c + rt.Year_Rate_Applies_To__c + ''));
            }
        }
        
    }
    
    public List<selectOption> getjobPositionList(){
    
        List<selectOption> jobPositionlst= new List<selectOption>();
        
        //to add blank value as default
        jobPositionlst.add(new SelectOption('-None-','-None-')); 
        
        for(Job_Class_Desc__c ta: [select Name from Job_Class_Desc__c]) {
            jobPositionlst.add(new selectOption(ta.Id,ta.name));
            
        } 
        return jobPositionlst;
    }
    
    public List<selectOption> getcountryList(){
    
        List<selectOption> countryLst= new List<selectOption>();
        
        //to add blank value as default
        countryLst.add(new SelectOption('-None-','-None-')); 
        
        for(Country__c ta: [select Name from Country__c]) {
            countryLst.add(new selectOption(ta.Id,ta.name));
            
        } 
        return countryLst;
    }

    
    /* Populate Currency in Select List     */
    public List<SelectOption> getcurrencyselectoption() {
        List<SelectOption> currencyselectoption= new List<SelectOption>();
        for(Currency__c entry : currencies){
            currencyselectoption.add(new SelectOption(entry.Id, entry.Name));
        }
        return currencyselectoption;
    }

    public PageReference Create() {
        
            /* Catch invalid Year */
            try{
            if(Integer.valueOf(rate.Schedule_Year__c) < 1999 || Integer.valueOf(rate.Schedule_Year__c) > Integer.valueOf(Date.Today().Year() + 20)) {
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Year : '+ rate.Schedule_Year__c + '. Must be between 1999 and ' + String.valueOf(Date.Today().Year() + 20));
                ApexPages.addMessage(errormsg);
                return null;
            }
            }catch(Exception e){
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Year : '+ rate.Schedule_Year__c);
                ApexPages.addMessage(errormsg);
                return null;
            }
            
            /* Catch invalid Rate*/
            try{
                Integer d = Integer.valueOf(rate.Bill_Rate__c);
            }catch(Exception e){
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Rate : '+ rate.Bill_Rate__c);
                ApexPages.addMessage(errormsg);
            }
            
            if(!error){
                     String response = createNewRecords();
                     if(response.equals('SUCCESS')){
                         PageReference pageRef = Page.PBB_InflationOverride; 
                         pageRef.getParameters().put('jobPosition',rate.Job_Position__c);
                         pageRef.getParameters().put('country',rate.Country__c);
                         pageRef.getParameters().put('scheduledYear',rate.Schedule_Year__c);
                         pageRef.getParameters().put('modelId',rate.Model_Id__c);
                         return pageRef ;
                     }
                     else{
                         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,' Please select unique combination for Country, Job Position and Scheduled year'));
                         return null;
                     }
                
            }else
             return null;
    }

  // public PageReference back() {
    //     PageReference pageRef = Page.PBB_BillRate;
    //     return pageRef;
  //  }
    public String createNewRecords(){
       String response = 'SUCCESS';
        try{
            rate.Year_Rate_Applies_To__c = rate.Schedule_Year__c;
            rate.IsActive__c = true;
            System.debug('*** uniqueBillRateValues *** '+uniqueBillRateValues);
            response = generateBillRates(rate, uniqueBillRateValues);
           }
        catch(System.DMLException de){
            if(de.getDMLType(0).equals(StatusCode.DUPLICATE_VALUE)){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,' Please select unique combination for Country, Job Position and Scheduled year'));
                response = 'ERROR';
            }
        }
        
        return response;
    }
    
    public static String generateBillRates(Bill_Rate__c rate, Set<String> uniqueBillRateValues){
        List<Bill_Rate__c> billRateList = new List<Bill_Rate__c>();
        
        System.debug('*** unique *** '+(rate.Model_Id__c + '' + rate.Job_Position__c +'' + rate.Country__c +'' + rate.Schedule_Year__c + rate.Year_Rate_Applies_To__c + ''));
        
        if(!checkUnique(rate, uniqueBillRateValues)){
            billRateList.add(rate);
            
        }
        else{
            return 'ERROR';
        }
        
        Integer tempYear = Integer.valueOf(rate.Year_Rate_Applies_To__c);
        
        String response = 'SUCCESS';
        for (integer i=0;i<15;i++){
            Bill_Rate__c tempRate = new Bill_Rate__c ();
            
            tempRate.Bill_Rate__c = rate.Bill_Rate__c;
            tempRate.Schedule_Year__c = rate.Schedule_Year__c;
            tempYear = tempYear + 1;
            tempRate.Year_Rate_Applies_To__c =  String.valueOf(tempYear) ;
            tempRate.Currency__c = rate.Currency__c;
            tempRate.Country__c = rate.Country__c;
            tempRate.Job_Position__c = rate.Job_Position__c;
            tempRate.Model_Id__c = rate.Model_Id__c;
            tempRate.IsActive__c = true;
            
            if(!checkUnique(tempRate, uniqueBillRateValues)){
                billRateList.add(tempRate);
            }
            else{
                response = 'ERROR';
                return response;
            }
            
        }
        insert billRateList;
        return response;
    }
    
    public PageReference cancel(){
        PageReference ref = new PageReference('/'+rate.Model_Id__c);
        ref.setRedirect(true);
        return ref;
    }
    
     public static Boolean checkUnique(Bill_Rate__c rate, Set<String> uniqueBillRateValues){
        if(uniqueBillRateValues.contains((rate.Model_Id__c + '' + rate.Job_Position__c +'' + rate.Country__c +'' + rate.Schedule_Year__c + rate.Year_Rate_Applies_To__c + ''))){
            return true;
        }
        else{
            return false;
        }
        return false;
    }
}