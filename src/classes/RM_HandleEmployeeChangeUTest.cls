/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_HandleEmployeeChangeUTest {

   
    static Country__c country;
    static List<WFM_Location_Hours__c> hrs;
    static List<WFM_Employee_Availability__c> avas;
    static WFM_employee_Allocations__c alloc; 
    static WFM_employee_Allocations__c alloc1;
    static WFM_employee_Allocations__c allocR;
    static List<WFM_Employee_Assignment__c> assigns;
    static WFM_Employee_Assignment__c ass1; 
    static List<WFM_Employee_Assignment__c> assignsB;
    static Employee_Details__c  empl;
    static Employee_Details__c  emplNoActive;
    static WFM_Project__c project;
    static WFM_Project__c projectB;
    static List<string> months;
    static Job_Class_Desc__c jclass;
    static RM_ConstantSetting__c constSetting;
    static void init(){
        country = new Country__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0);
        insert country;
         months = RM_Tools.GetMonthsList(0, 5);          
    
        empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='1000', Employee_Unique_Key__c='1000', First_Name__c='John', Last_Name__c='Smith',email_Address__c='JSmith@gmail.com', 
        Function_code__c='PR',Buf_Code__c='KCICR', Job_Class_Desc__c='Clinical Research Associate', Business_Unit__c='RDU', Status_Desc__c='Active', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'),
        term_date__c = date.parse('12/27/2012'));
        insert empl;
        emplNoActive = new Employee_Details__c(COUNTRY_name__c = country.id, name='1001', Employee_Unique_Key__c='1001', First_Name__c='John', Last_Name__c='Smith1',email_Address__c='JSmith1@gmail.com', 
        Function_code__c='PR',Buf_Code__c='KCICR', Job_Class_Desc__c='Clinical Research Associate', Business_Unit__c='RDU', Status_Desc__c='Active', Status__c='AA', Date_Hired__c=date.parse('12/27/2010'),
        term_date__c = date.parse('12/27/2012'));
        insert emplNoActive;
 		jclass = new Job_Class_Desc__c(name='Clinical Research Associate', IsClinical__c=true, Job_Class_Code__c = 'CO', Job_Class_ExtID__c='Clinical Research Associate');
       	
       	insert jclass;
       	
       	constSetting = new RM_ConstantSetting__c(name='RMGroupEmail', value__c='Testemail@test.com');
       	insert constSetting;
       	  //need to insert project
        WFM_Client__c client = new WFM_Client__c(name='TestClient', Client_Name__c='TestClinet_Name', Client_Unique_Key__c='TestClient');
        insert client;
        WFM_Contract__c contract = new WFM_Contract__c(name='TestContract', Contract_Status__c='AA', Client_ID__c=client.id, Contract_Unique_Key__c='TestContract');
        insert contract;
        project = new WFM_Project__c(name='TestProject', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProject', project_status__c ='RM',  Status_Desc__c='Active');
        insert project;
        projectB = new WFM_Project__c(name='TestProjectB', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProjectB', project_status__c ='BB',  Status_Desc__c='Bid');
        insert projectB;
        
       	hrs = new List<WFM_Location_Hours__c>();
        for (string month:months){
            WFM_Location_Hours__c hr = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + month,YEAR_MONTH__c=month,NBR_Business_Day_In_Month__c=22);
            hrs.add(hr); 
        }
        insert hrs; 
        avas = new List<WFM_Employee_Availability__c>();
        
        List<WFM_employee_Allocations__c> allocs = new List<WFM_employee_Allocations__c>();
        alloc= new WFM_employee_Allocations__c(EMPLOYEE_ID__c=empl.id, Project_Buf_Code__c='KCICR',Project_Country__c='United State', Project_Region__c='North America', Project_ID__c=project.id, Project_Function__c='CR', 
            Status__c='Confirmed', Allocation_Unique_Key__c=empl.name+project.name+'KCICRUnited State', allocation_start_date__c = Date.today(), allocation_end_date__c = Date.today().addMonths(5));
        allocs.add(alloc);
        alloc1= new WFM_employee_Allocations__c(EMPLOYEE_ID__c=empl.id, Project_Buf_Code__c='KCICY',Project_Country__c='United State', Project_Region__c='North America', Project_ID__c=project.id, Project_Function__c='CY', 
            Status__c='Proposed', Allocation_Unique_Key__c=empl.name+project.name+'KCICYUnited State');  // same project different bufcode
        allocs.add(alloc1);
        
        allocR = new WFM_employee_Allocations__c(EMPLOYEE_ID__c=empl.id, Project_Buf_Code__c='KCICR',Project_Country__c='United State', Project_Region__c='North America', Project_ID__c=projectB.id, Project_Function__c='CRA', 
            Status__c='Proposed', Allocation_Unique_Key__c=empl.name+projectB.name+'KCICRUnited State',  allocation_start_date__c = Date.today(), allocation_end_date__c = Date.today().addMonths(5));
        allocs.add(allocR);
        
        insert allocs;
            
        assigns = new List<WFM_Employee_Assignment__c>();
        
        for (Integer i = 0; i < months.size(); i++){
            
            WFM_Employee_Availability__c ava = new WFM_Employee_Availability__c(EMPLOYEE_ID__c=empl.id, year_month__c=months[i], Availability_Unique_Key__c=empl.name+months[i], 
            location_Hours__c = hrs[i].id);
            avas.add(ava);
        }
        insert avas;
            
        for (Integer i = 0; i < months.size(); i++){    
            WFM_Employee_Assignment__c ass =  new WFM_Employee_Assignment__c(Assignment_Unique_Key__c=avas[i].id+(string)alloc.id, EMPLOYEE_ID__c = empl.id, Availability_FK__c=avas[i].id, Allocation_Key__c=alloc.id, Year_Month__c=months[i], 
            Assigned_value__c=0.1, assigned_unit__c = 'FTE', type__c='Assignment');
            
            assigns.add(ass);
        }
        
        insert assigns;
      
        	
        
        ass1 =  new WFM_Employee_Assignment__c(Assignment_Unique_Key__c=avas[0].id+(string)alloc1.id, EMPLOYEE_ID__c = empl.id, Availability_FK__c=avas[0].id, Allocation_Key__c=alloc1.id, Year_Month__c=months[0], 
            Assigned_value__c=0.1, assigned_unit__c = 'FTE', type__c='Assignment');
        assignsB = new List<WFM_Employee_Assignment__c>();
        assignsB.add(ass1);
        for (Integer i = 0; i < months.size(); i++){    
            WFM_Employee_Assignment__c ass =  new WFM_Employee_Assignment__c(Assignment_Unique_Key__c=avas[i].id+(string)allocR.id, EMPLOYEE_ID__c = empl.id, Availability_FK__c=avas[i].id, Allocation_Key__c=allocR.id, Year_Month__c=months[i], 
            Assigned_value__c=0.3, assigned_unit__c = 'FTE', type__c='Reservation');
            
            assignsB.add(ass);
        }
        
        insert assignsB;
    }
     static testMethod void TestStatusChange() {
     	init();
       
       // system.assert(empl.Status_Desc__c == 'Active');
       	Employee_Details__c emplOut = RM_EmployeeService.GetEmployeeDetailByEmployeeID('1000');
        emplOut.Status_Desc__c = 'Terminated';
        
        update emplOut;
        Test.startTest();
        string subject = 'Employee Change Notification';
        string address = RM_DataAccessor.GetConstant('RMGroupEmail');
        list<WFM_Email_Alert__c> emailAlert = [select id, Email_Body__c,Email_To__c,Subject__c, Footer__c,Header__c from WFM_Email_Alert__c 
        where email_to__c = :address and Subject__c = :subject];
        
        Test.stopTest();
        system.debug('---emailAlert--'+emailAlert);
        system.assert(emailAlert[0].email_to__c  == RM_DataAccessor.GetConstant('RMGroupEmail'));
    
    }
     static testMethod void TestStatusChangeNoActive() {
     	init();
       
       // system.assert(empl.Status_Desc__c == 'Active');
       	Employee_Details__c emplOut = RM_EmployeeService.GetEmployeeDetailByEmployeeID('1001');
        emplOut.Status_Desc__c = 'Terminated';
        
        update emplOut;
        Test.startTest();
        string subject = 'Employee Change Notification';
        string address = RM_DataAccessor.GetConstant('RMGroupEmail');
        list<WFM_Email_Alert__c> emailAlert = [select id, Email_Body__c,Email_To__c,Subject__c, Footer__c,Header__c from WFM_Email_Alert__c 
        where email_to__c = :address and Subject__c = :subject];
        
        Test.stopTest();
        system.debug('---emailAlert--'+emailAlert);
        system.assert(emailAlert.size() == 0);
    
    }
      static testMethod void TestTermDateChange() {
     	init();
       
       // system.assert(empl.Status_Desc__c == 'Active');
       	Employee_Details__c emplOut = RM_EmployeeService.GetEmployeeDetailByEmployeeID('1000');
        emplOut.term_date__c = date.parse('12/25/2012');
        
        update emplOut;
        Test.startTest();
        string subject = 'Employee Change Notification';
        string address = RM_DataAccessor.GetConstant('RMGroupEmail');
        list<WFM_Email_Alert__c> emailAlert = [select id, Email_Body__c,Email_To__c,Subject__c, Footer__c,Header__c from WFM_Email_Alert__c 
        where email_to__c = :address and Subject__c = :subject ];
        
        Test.stopTest();
        system.debug('---emailAlert--'+emailAlert);
        system.debug('---address--'+address);
        system.assert(emailAlert[0].email_to__c  == RM_DataAccessor.GetConstant('RMGroupEmail'));
    
    } 
      static testMethod void TestTermDateChangeNoActiveAllocation() {
     	init();
       
       // system.assert(empl.Status_Desc__c == 'Active');
       	Employee_Details__c emplOut = RM_EmployeeService.GetEmployeeDetailByEmployeeID('1001');
        emplOut.term_date__c = date.parse('12/25/2012');
        
        update emplOut;
        Test.startTest();
        string subject = 'Employee Change Notification';
        string address = RM_DataAccessor.GetConstant('RMGroupEmail');
        list<WFM_Email_Alert__c> emailAlert = [select id, Email_Body__c,Email_To__c,Subject__c, Footer__c,Header__c from WFM_Email_Alert__c 
        where email_to__c = :address and Subject__c = :subject ];
        
        Test.stopTest();
        system.debug('---emailAlert--'+emailAlert);
        system.assert(emailAlert.size() == 0);
    
    } 
    static testMethod void TestRoleChange() {
     	init();
       
       // system.assert(empl.Status_Desc__c == 'Active');
       	Employee_Details__c emplOut = RM_EmployeeService.GetEmployeeDetailByEmployeeID('1000');
        emplOut.Job_Class_Desc__c = 'AA';
        
        update emplOut;
        Test.startTest();
        string subject = 'Employee Change Notification';
        string address = RM_DataAccessor.GetConstant('RMGroupEmail');
        list<WFM_Email_Alert__c> emailAlert = [select id, Email_Body__c,Email_To__c,Subject__c, Footer__c,Header__c from WFM_Email_Alert__c 
        where email_to__c = :address and Subject__c = :subject ];
        
        Test.stopTest();
        system.debug('---emailAlert--'+emailAlert);
        system.assert(emailAlert[0].email_to__c  == RM_DataAccessor.GetConstant('RMGroupEmail'));
    
    }  
}