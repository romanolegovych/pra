public with sharing class FlowchartService {


	public static boolean RecodeOldData (ID ProjectId, Boolean NoDML) {
		// as of release 4.6 the timepoint__c object is to be removed and the timepoints
		// will be stored as a concatinated string of integers in the flowchart object
		// this routine will clean out the timepoint object
		
		List<Flowcharts__c> flowchartList = [
			select id,
				listOfTimeMinutes__c,
				(Select Id, Minutes__c From TimePoint__r order by Minutes__c)
			from Flowcharts__c
			where id in (
				select flowchart__c
				from timepoint__c
				where flowchart__r.clinicaldesign__r.project__c = :ProjectId)
			limit 25
		];
		
		Set<ID> timepointsToBeRemoved = new Set<ID>();
		For (Flowcharts__c flowchart:flowchartList) {
			String minuteList = '';
			For (timepoint__c timepoint:flowchart.TimePoint__r){
				minuteList += (minuteList.length()>0?':':'') + timepoint.Minutes__c;
				timepointsToBeRemoved.add(timepoint.id);
			}
			flowchart.listOfTimeMinutes__c = minuteList;
			if (timepointsToBeRemoved.size()>3000) {
				// respect the govenour limits, 10000 is maximum for dml rows
				// now deleting 3000+ timepoints and updating flowcharts
				// unit calculations in triggers might start to exceed limits
				break;
			}
		}
		
		if (!NoDML) {
			update flowchartList;
			delete [select id from projectservicetimepoint__c where timepoint__c in :timepointsToBeRemoved];
			delete [select id from timepoint__c where id in :timepointsToBeRemoved];
		}
		
		// return if there were any mutations
		return (timepointsToBeRemoved.size() > 0);
		
	}

	public class flowchartDisplay {
		public Flowcharts__c flowchart {get;set;}
		public string DesignId {get;set;}
		public string ProjectId {get;set;}
		public list<flowchartTime> TimePointsInFlowchart {get;set;}
		public list<flowchartservicewrp> FlowchartServices {get;set;}
		
		
		public flowchartDisplay(ID FlowchartID, String pProjectId, String pDesignId) {
			// Query initial data
			flowchart = FlowchartDataAccessor.getFlowchartList('id = \'' + FlowchartID + '\'')[0];
			
			List<ProjectServiceTimePoint__c> pstpList =
				ProjectServiceTimePointDataAccessor.getProjectServiceTimePointList(
					'flowchart__c = \''+ FlowchartID +'\''
			);
			
			// Build secondary data
			flowchartDisplayConstructoHelper(pProjectId, pDesignId, pstpList);
		}
		
		
		public flowchartDisplay(Flowcharts__c pFlowchart, List<ProjectServiceTimePoint__c> pstpList, String pProjectId) {
			// Initial data
			flowchart = pFlowchart;
			
			flowchartDisplayConstructoHelper(pProjectId, flowchart.ClinicalDesign__c, pstpList);
		}
		
		
		public void flowchartDisplayConstructoHelper (String pProjectId,
													String pDesignId,
													List<ProjectServiceTimePoint__c> pstpList) {
			// Construct data
			ProjectId = pProjectId;
			DesignId = pDesignId;
			
			// create a list of timepoints in the flowchart
			TimePointsInFlowchart = new list<flowchartTime>();
			
			if (flowchart.listOfTimeMinutes__c != null) {
				flowchartTime previous = new flowchartTime(null,null);
				for (String timePoint : flowchart.listOfTimeMinutes__c.split(':')) {
					flowchartTime current = new flowchartTime(integer.valueOf(timePoint),previous);
					TimePointsInFlowchart.add(current);
					previous = current;
				}
			}
			
			FlowchartServices = new list<flowchartservicewrp>();
			for (ProjectServiceTimePoint__c pstp:pstpList) {
				FlowchartServices.add(new flowchartservicewrp(pstp,flowchart.listOfTimeMinutes__c));
			}
			
		}

		public void removeService(ID ServiceId) {
			for (integer i = 0; i < FlowchartServices.size(); i++){
				if (FlowchartServices[i].ServiceId == ServiceId) {
					FlowchartServices.remove(i);
				}
			}
		}

		public void deleteUnusedTimepoints () {
			Set<integer> used = getUsedTimePoints ();
			List<integer> toBeDeleted = new list<integer>();
			for (integer i = 0 ; i < TimePointsInFlowchart.size();i++ ){
				if (used.contains(TimePointsInFlowchart[i].totalminutes)) {
					continue; // skip to next itteration
				}
				toBeDeleted.add(TimePointsInFlowchart[i].totalminutes);
			}
			removeTimePoints(toBeDeleted);
		}
		
		public boolean getHasObsoleteTimePoints () {
			return getUsedTimePoints().size() <> TimePointsInFlowchart.size();
		}
		
		public Set<integer> getUsedTimePoints () {
			Set<integer> timepointsUsed = new Set<integer>();
			For (flowchartservicewrp fs:FlowchartServices ) {
				timepointsUsed.addAll(fs.getTimepointsUsed());
			}
			return timepointsUsed;
		}
		
		public void checkTimePointsUsed () {
			
			Set<integer> timepointsUsed = getUsedTimePoints ();
			
			for (flowchartTime ft:TimePointsInFlowchart) {
				if (timepointsUsed.contains(ft.totalminutes)){
					ft.timepointused = true;
				} else {
					ft.timepointused = false;
				}
			}
			
		}

		public void removeTimePoints (List<integer> minutes) {
			
			Set<integer> timepointsToBeRemoved = new Set<integer>();
			timepointsToBeRemoved.addAll(minutes);
			String listOfMinutes = '';
			
			list<flowchartTime> NewTimePointsInFlowchart = new list<flowchartTime>();
			flowchartTime previous;
			
			for (integer i = 0 ; i < TimePointsInFlowchart.size();i++ ){
				if (timepointsToBeRemoved.contains(TimePointsInFlowchart[i].totalminutes)) {
					continue; // skip to next itteration
				}
				
				boolean previouslyused = TimePointsInFlowchart[i].timepointused;
				
				listOfMinutes += ':' + TimePointsInFlowchart[i].totalminutes;
				
				flowchartTime newTp = new flowchartTime(TimePointsInFlowchart[i].totalminutes,previous);
				newTp.timepointused = previouslyused;
				NewTimePointsInFlowchart.add(newTp);
				previous = newTp;
				
			}
			
			TimePointsInFlowchart = NewTimePointsInFlowchart;
			
			For (flowchartservicewrp fs:FlowchartServices ) {
				fs.removeTimePoints(timepointsToBeRemoved);
			}
			
			flowchart.listOfTimeMinutes__c = listOfMinutes.removeStart(':');
		}
		
		public void addTimePoints (List<Integer> minutes) {

			minutes.sort();
			List<Integer> minutesClone = minutes.clone();
			String listOfMinutes = '';
			
			list<flowchartTime> NewTimePointsInFlowchart = new list<flowchartTime>();
			flowchartTime previous = null;

			// process both existing timepoints as new timepoints
			while (minutes.size() > 0 || TimePointsInFlowchart.size() > 0) {

				// as long as there are new timepoints that preceed the first (remaining) existing timepoints then add them to the new list
				while (minutes.size() > 0  && (TimePointsInFlowchart.size() == 0 || minutes[0] < TimePointsInFlowchart[0].totalminutes)) {
					// add new timepoints that preceed existing timepoints
					previous = new flowchartTime(minutes.remove(0),previous);
					previous.timepointused = false;
					listOfMinutes += ':' + previous.totalminutes;
					NewTimePointsInFlowchart.add(previous);
				}

				// as long as there are existing timepoints that preceed the first (remaining) new timepoints then add them to the new list
				while (TimePointsInFlowchart.size() > 0  && (minutes.size() == 0  || TimePointsInFlowchart[0].totalminutes <= minutes[0] )) {

					// remove duplicate
					if (minutes.size() > 0  && TimePointsInFlowchart[0].totalminutes == minutes[0]) {
						minutes.remove(0); 
					}

					// add existing timepoints that preceed new timepoints
					boolean previouslyused = TimePointsInFlowchart[0].timepointused;
					previous = new flowchartTime(TimePointsInFlowchart.remove(0).totalminutes,previous);
					previous.timepointused = previouslyused;
					listOfMinutes += ':' + previous.totalminutes;
					NewTimePointsInFlowchart.add(previous);
					
				}

			}

			TimePointsInFlowchart = NewTimePointsInFlowchart;
			
			For (flowchartservicewrp fs:FlowchartServices ) {
				fs.addTimePoints(minutesClone.clone());
			}
			
			flowchart.listOfTimeMinutes__c = listOfMinutes.removeStart(':');

		}
		
		
		public void addServiceToFlowchart (Service__c newService,ServiceCategory__c servicecategory) {
			
			// get projectservice (create it if needed immediately)
			ProjectService__c ps = ProjectServiceService.getProjectServiceForService(
				new Set<ID>{newService.id}, 
				ProjectId, 
				true).values()[0];
			
			// create project service timepoint
			ProjectServiceTimePoint__c pstp = new ProjectServiceTimePoint__c(
				Flowchart__c = flowchart.id,
				ProjectService__c = ps.id
			);
			
			// add to display
			flowchartservicewrp newItem = new flowchartservicewrp(pstp,flowchart.listOfTimeMinutes__c);
			newItem.ServiceName = newService.name;
			newItem.ServiceShortName = newService.shortName__c;
			newItem.ServiceId = newService.id;
			newItem.CategoryCode = servicecategory.code__c;
			newItem.ServiceSequenceNumber = integer.valueOf((newService.SequenceNumber__c==null)?99:newService.SequenceNumber__c);
			
			list<flowchartservicewrp> newFlowchartServices = new list<flowchartservicewrp>();
			Boolean added = false;
			For (Integer i = 0; i < FlowchartServices.size(); i++) {
				
				system.debug('newItem.CategoryCode '+newItem.CategoryCode);
				system.debug('FlowchartServices[i].CategoryCode '+FlowchartServices[i].CategoryCode);
				
				if (!added) {
					// category before current
					if (newItem.CategoryCode < FlowchartServices[i].CategoryCode) {
						newFlowchartServices.add(newItem);
						added = true;
					}
					// category is current
					else if ((newItem.CategoryCode == FlowchartServices[i].CategoryCode) &&
						(newItem.ServiceSequenceNumber < FlowchartServices[i].ServiceSequenceNumber)) {
						newFlowchartServices.add(newItem);
						added = true;
					}
				}	
				newFlowchartServices.add(FlowchartServices[i]);
				
			}	
			if (!added) {newFlowchartServices.add(newItem);} 	
			FlowchartServices = newFlowchartServices;
			
		}
		
		public void saveFlowchartAndProjectServiceTimepointChanges () {
			// store changes to timepoint information of the flowchart
			update flowchart;
			
			// Read current data in projectservicetimepoint, list will be reduced with the records that still have
			// data in the altered flowchart. What remains are records that were removed from the flowchart
			map<id,ProjectServiceTimePoint__c> existingData = new map<id,ProjectServiceTimePoint__c> (
				ProjectServiceTimepointDataAccessor.getProjectServiceTimePointList('Flowchart__c = \''+flowchart.id+'\'')
			);
			
			list<ProjectServiceTimePoint__c> recordsToUpsert = new list<ProjectServiceTimePoint__c>();
			
			// loop over data in display and collect data to be save
			for (flowchartservicewrp fs : FlowchartServices) {
				
				String ListOfTimeMinutes = '';
				
				// get used timepoints
				for (flowchartservicetime fcst : fs.timepoints) {
					if (fcst.selected) {
						ListOfTimeMinutes += ':' + fcst.totalminutes;
					}
				}
				
				// set the correct list of minutes and add to upsert list
				fs.projectServiceTimePoint.listOfTimeMinutes__c = ListOfTimeMinutes.removeStart(':');
				
				recordsToUpsert.add(fs.projectServiceTimePoint);
				
				// clean item from existingData map
				if (fs.projectServiceTimePoint.id != null && existingData.containsKey(fs.projectServiceTimePoint.id)) {
					existingData.remove(fs.projectServiceTimePoint.id);
				}
				
			}
			
			// Anything remaining in map first needs to be deleted. If the user removed a service and then added
			// it again, the order of first deleting and then creating will make sure that there are no unique 
			// constraint errors.
			delete existingData.values();
			
			// Create and update new and exisiting records
			upsert recordsToUpsert;
			
		}
		

	}

	class flowchartservicewrp {
		public ProjectServiceTimePoint__c projectServiceTimePoint {get;set;}
		public String ServiceName {get;set;}
		public String ServiceShortName {get;set;}
		public String CategoryCode {get;set;}
		public Integer ServiceSequenceNumber {get;set;}
		Public Id ServiceId {get;set;}
		
		public string getPrettyCategoryCode () {
			return bdt_utils.removeLeadingZerosFromCat(CategoryCode);
		}
		
		public list<flowchartservicetime> timepoints {get;set;}
		
		public flowchartservicewrp (ProjectServiceTimePoint__c pst, String ListOfTimeMinutesInFlowchart) {
			
			if (pst.id != null) {
				ServiceName = pst.ProjectService__r.Service__r.Name;
				ServiceShortName = pst.ProjectService__r.Service__r.shortName__c;
				ServiceId = pst.ProjectService__r.Service__r.Id;
				CategoryCode =  pst.ProjectService__r.Service__r.ServiceCategory__r.code__c;
				try{ 
					ServiceSequenceNumber = Integer.valueOf(pst.ProjectService__r.Service__r.SequenceNumber__c);
				} catch (Exception e) {
					ServiceSequenceNumber = 99;
				}
			}
			
			projectServiceTimePoint = pst;
			String psttimepoints = ':' + pst.listOfTimeMinutes__c + ':';
			timepoints = new list<flowchartservicetime>();
			
			if (ListOfTimeMinutesInFlowchart == null) {return;}
			flowchartservicetime previous;
			For(String timepoint:ListOfTimeMinutesInFlowchart.split(':')) {
				
				Boolean timepointSelected = psttimepoints.contains(':' + timepoint + ':');
				flowchartservicetime current = new flowchartservicetime(integer.valueOf(timepoint),timepointSelected,timepointSelected, previous);
				timepoints.add(current);
				previous = current;
			}
			
		}
		
		public list<integer> getTimepointsUsed () {
			list<integer> result = new list<integer>();
			for (flowchartservicetime fst:timepoints) {
				if (fst.selected) {
					result.add(fst.totalminutes);
				}
			}
			return result;
		}
		
		public void addTimePoints (List<Integer> minutes) {

			list<flowchartservicetime> NewTimePointsInService = new list<flowchartservicetime>();
			flowchartservicetime previous;

			// process both existing timepoints as new timepoints
			while (minutes.size() > 0 || timepoints.size() > 0) {

				// as long as there are new timepoints that preceed the first (remaining) existing timepoints then add them to the new list
				while (minutes.size() > 0  && (timepoints.size() == 0 || minutes[0] < timepoints[0].totalminutes)) {
					// add new timepoints that preceed existing timepoints
					previous = new flowchartservicetime(minutes.remove(0),false,false,previous);
					NewTimePointsInService.add(previous);
				}

				// as long as there are existing timepoints that preceed the first (remaining) new timepoints then add them to the new list
				while (timepoints.size() > 0  && (minutes.size() == 0  || timepoints[0].totalminutes <= minutes[0] )) {

					// remove duplicate
					if (minutes.size() > 0  && timepoints[0].totalminutes == minutes[0]) {
						minutes.remove(0); 
					}

					// add existing timepoints that preceed new timepoints
					flowchartservicetime tmp = timepoints.remove(0);
					previous = new flowchartservicetime(tmp.totalminutes,tmp.selected,tmp.initialStateSelected,previous);
					NewTimePointsInService.add(previous);
				}
			}
			timepoints = NewTimePointsInService;
		}
		
		public void removeTimePoints (Set<integer> timepointsToBeRemoved) {
			list<flowchartservicetime> newTimepoints = new list<flowchartservicetime>();
			flowchartservicetime previous;
			
			for (integer i = 0 ; i < timepoints.size();i++ ){
				if (timepointsToBeRemoved.contains(timepoints[i].totalminutes)) {
					continue; // skip to next itteration
				}
				
				flowchartservicetime newTp = new flowchartservicetime(
					timepoints[i].totalminutes,
					timepoints[i].selected,
					timepoints[i].initialStateSelected,
					previous
				);
				
				newTimepoints.add(newTp);
				previous = newTp;
			}
			timepoints = newTimepoints;
		}
	}
	
	class flowchartservicetime {
		public integer totalminutes {get;set;}
		public boolean selected {get;set;}
		public boolean initialStateSelected {get;set;}
		public boolean newDay {get;set;}
		public integer dayNumber {get;set;}
		
		public flowchartservicetime(integer ptotalminutes,boolean pselected,boolean pinitialStateSelected, flowchartservicetime previous) {
			totalminutes = ptotalminutes;
			selected = pselected;
			initialStateSelected = pinitialStateSelected;
			
			integer currentDay;
			if (totalminutes >= 0 ) {
				// first day is daynumber 1 and not 0
				dayNumber = integer.valueOf(math.floor(totalminutes/1440) + 1);
			} else {
				// negative minutes
				dayNumber = integer.valueOf(math.floor((totalminutes+1)/1440) - 1);
			}
			newDay = (previous == null  || dayNumber!=previous.dayNumber);
		}
	}

	class flowchartTime {
		public integer totalminutes {get;set;}
		public integer dayNumber {get;set;}
		public boolean newDayNumber {get;set;} // is this timepoint different from the previous
		public integer hourNumberInDay {get;set;}
		public boolean newHourNumberInDay {get;set;} // is this timepoint different from the previous
		public integer minuteNumberInHour {get;set;}
		public boolean timepointused {get;set;}
		
		public flowchartTime(integer minutes, flowchartTime previous) {
			if (minutes == null) {return;}
			totalminutes = minutes;
			//positive minutes
			if (minutes >= 0 ) {
				// first day is daynumber 1 and not 0
				dayNumber = integer.valueOf(math.floor(totalminutes/1440) + 1);
				hourNumberInDay = integer.valueOf(math.floor(math.mod(totalminutes,1440) / 60));
				minuteNumberInHour = integer.valueOf(math.mod(totalminutes,60));
			} else {
				// negative minutes
				dayNumber = integer.valueOf(math.floor((totalminutes+1)/1440) - 1);
				integer correctedMinutes = 1440 + integer.valueOf(math.mod(totalminutes,1440));
				hourNumberInDay = integer.valueOf(math.floor(math.mod(correctedMinutes,1440) / 60));
				minuteNumberInHour = integer.valueOf(math.mod(correctedMinutes,60));				
			}
			newDayNumber = (previous == null  || dayNumber!=previous.dayNumber);
			newHourNumberInDay = (newDayNumber || hourNumberInDay!=previous.hourNumberInDay);
		}
	}


	public class servicecategoriesForFlowcharts {
		public list<categoryDetail> categoryDetailList {get;set;}
		
		public servicecategoriesForFlowcharts (flowchartDisplay displayData) {
			
			// create map of selected services and boolean for timepoints used
			map<id,boolean> servicesSelected = new map<id,boolean>();
			for (flowchartservicewrp fcs:displayData.FlowchartServices) {
				servicesSelected.put(
					fcs.ServiceId,
					(fcs.getTimepointsUsed().size()>0)
				);
			}
			
			// create a set of categories that have disignobject in them
			Set<id> categoriesToDisplay = ServiceCategoryService.getAllDesignObjectCategories();
			
			// read categories, services for this project
			List<ServiceCategory__c> ServiceCategoryList = ServiceCategoryDataAccessor.getServiceAndCategoriesForProject(ID.valueOf(displayData.ProjectId));
			
			categoryDetailList = new list<categoryDetail>();
			
			for (ServiceCategory__c sc:ServiceCategoryList) {
				
				if (!categoriesToDisplay.contains(sc.id)) {
					continue;
				}
				
				categoryDetail newItem = new categoryDetail();
				newItem.servicecategory = sc;
				newItem.inUse = false; 
				newItem.serviceDetailList = new list<serviceDetail>();
				
				for (Service__c s:sc.Services__r) {
					serviceDetail newSItem = new serviceDetail();
					newSItem.service = s;
					
					if (servicesSelected.containsKey(s.id)) {
						newSItem.selected = true;
						newSItem.inUse = servicesSelected.get(s.id);
						newItem.inUse = true; // parent set to true
					} else {
						newSItem.selected = false;
						newSItem.inUse = false;
					}

					newItem.serviceDetailList.add(newSItem);
				}
				
				categoryDetailList.add(newItem);
			}
		}
	}
	
	public class categoryDetail {
		public ServiceCategory__c servicecategory {get;set;}
		public boolean inUse {get;set;}
		public list<serviceDetail> serviceDetailList {get;set;}
		
		public string getPrettyCategoryCode () {
			return bdt_utils.removeLeadingZerosFromCat(servicecategory.code__c);
		}
		public boolean getHasServices () {
			return ( serviceDetailList != null && serviceDetailList.size() > 0);
		}
		
		public void deselectService (integer serviceNumber,flowchartDisplay displayData) {
			serviceDetailList[serviceNumber].selected = false;
			serviceDetailList[serviceNumber].inUse = false;
			displayData.removeService(serviceDetailList[serviceNumber].service.id);
		}
		
		public void selectService (integer serviceNumber,flowchartDisplay displayData) {
			serviceDetailList[serviceNumber].selected = true;
			inUse = true;
			serviceDetailList[serviceNumber].inUse = false;
			displayData.addServiceToFlowchart (serviceDetailList[serviceNumber].service,servicecategory);
		}
		
		
	}
	
	public class serviceDetail {
		public Service__c service {get;set;}
		public boolean selected {get;set;}
		public boolean inUse {get;set;}
	}
	
	public class CopyFlowchart {
		
		private FlowchartDisplay display;
		
		// search variables
		public string ProjectSearchTerm {get;set;}
		public string ProjectSearchError {get;set;}
		
		// project selection variables
		public string SelectedProjectID {get;set;}
		public List<SelectOption> ProjectSelectList {get;set;}
		
		// design selection variables
		public string SelectedDesignID {get;set;}
		public List<SelectOption> DesignSelectList {get;set;} 
		
		// flowchart selection variables
		public string SelectedFlowchartID {get;set;}
		public List<SelectOption> FlowchartSelectList {get;set;} 
		
		public CopyFlowchart (FlowchartDisplay pDisplay) {
			display = pDisplay;
			init();
			readProjects();
		}
		public void init() {
			
			Client_Project__c cp = [Select Code__c From Client_Project__c where id = :display.ProjectId limit 1];
			ProjectSearchTerm = cp.Code__c;
			
			// initialize
			ProjectSearchError = null;
			SelectedProjectID = null;
			ProjectSelectList = new List<SelectOption>();
			SelectedDesignID = null;
			DesignSelectList = new List<SelectOption>();
			SelectedFlowchartID = null;
			FlowchartSelectList = new List<SelectOption>();
		}
		
		public void readProjects () {
			// query fields that match the users search string
			ProjectSearchError = null;
			SelectedProjectID = null;
			ProjectSelectList = new List<SelectOption>();
			SelectedDesignID = null;
			DesignSelectList = new List<SelectOption>();
			SelectedFlowchartID = null;
			FlowchartSelectList = new List<SelectOption>();
			try {
					String searchquery = 'FIND \''+ProjectSearchTerm+'*\' IN ALL FIELDS RETURNING';
					searchquery += ' Client_Project__c(Code__c, Title__c, Project_Description__c, id WHERE code__c != null and bdtdeleted__c = false ORDER BY Code__c LIMIT 100)';
					List<List<SObject>> searchList = search.query(searchquery);
					List<Client_Project__c> projectSearchResultList = (List<Client_Project__c>)searchList[0];
					
					if (projectSearchResultList.size() > 0) {
						ProjectSelectList = BDT_Utils.getSelectOptionFromList (projectSearchResultList, 'Id', 'Code__c');
						SelectedProjectID = projectSearchResultList[0].id;
						readDesigns ();
					}
					
			}catch (Exception e) {
				ProjectSearchError='Your search could not be completed. Please rephrase you search term.';
			}
		}
		
		public void readDesigns () {
			SelectedDesignID = null;
			DesignSelectList = new List<SelectOption>();
			SelectedFlowchartID = null;
			FlowchartSelectList = new List<SelectOption>();
			try {
				DesignSelectList = BDT_Utils.getSelectOptionFromList (
					[select id, Name from ClinicalDesign__c where Project__c = :SelectedProjectID]
					, 'Id'
					, 'Name'
				);
				SelectedDesignID = DesignSelectList[0].getValue();
				readFlowcharts();
			}catch (Exception e) {}
		}
		
		public void readFlowcharts() {
			SelectedFlowchartID = null;
			FlowchartSelectList = new List<SelectOption>();
			try {
				FlowchartSelectList = BDT_Utils.getSelectOptionFromList (
					[select id, Description__c from Flowcharts__c where ClinicalDesign__c = :SelectedDesignID and id <> :display.flowchart.id]
					, 'Id'
					, 'Description__c'
				);
				SelectedFlowchartID = FlowchartSelectList[0].getValue();
			}catch (Exception e) {}
		}
		
		public list<FlowchartCopyData> flowchartCopyDataList {get;set;}
		
		public void prepareFlowchartCopyDataList () {
			
			// read data from flowchart selected for copy (source)
			List<ProjectServiceTimePoint__c> copySource;
			copySource = ProjectServiceTimePointDataAccessor.getProjectServiceTimePointList('flowchart__c = \''+ SelectedFlowchartID +'\'');
			
			Set<id> ServicesAlreadyInTargetFlowchart = new Set<id>();
			For (flowchartservicewrp fs:display.FlowchartServices){
				ServicesAlreadyInTargetFlowchart.add(fs.ServiceId);
			}
			
			flowchartCopyDataList = new list<FlowchartCopyData>();
			
			// fill data
			for (ProjectServiceTimePoint__c pst:copySource) {
				
				FlowchartCopyData newItem = new FlowchartCopyData();
				newItem.projectServiceTimePoint = pst;
				newItem.isServiceAlreadyInFlowchart = ServicesAlreadyInTargetFlowchart.contains(pst.projectservice__r.service__c);
				newItem.shouldServiceBeCopied = newItem.isServiceAlreadyInFlowchart;
				newItem.shouldTimePointsBeCopied = false;
				
				flowchartCopyDataList.add(newItem);
				
			}
			
			
		}
		
		public void copyFlowChart () {
			
			// read timepoints to be created
			set<integer> timepointsInSelected = new set<integer>();
			For (FlowchartCopyData fcd : flowchartCopyDataList) {
				if (fcd.shouldTimePointsBeCopied && fcd.projectServiceTimePoint.listOfTimeMinutes__c != null) {
					for (string value : fcd.projectServiceTimePoint.listOfTimeMinutes__c.split(':')) {
						try{
							timepointsInSelected.add(integer.valueOf(value));
						} catch (Exception e) {}
					}
				}
			}
			if (timepointsInSelected.size() > 0) {
				List<integer> timepoints = new List<integer>();
				timepoints.addAll(timepointsInSelected);
				display.addTimePoints(timepoints);
			}
			
			// add services and enable their timepoints if needed
			For (FlowchartCopyData fcd : flowchartCopyDataList) {
				
				// add the item?
				if (!fcd.isServiceAlreadyInFlowchart && fcd.shouldServiceBeCopied) {
					display.addServiceToFlowchart (
						fcd.projectServiceTimePoint.projectservice__r.service__r,
						fcd.projectServiceTimePoint.projectservice__r.service__r.servicecategory__r
					);
				}
				
				// enable timepoints for this service?
				if (fcd.shouldTimePointsBeCopied) {
					
					// locate service in list of services
					for (flowchartservicewrp fs : display.FlowchartServices) {
						if (fs.ServiceId == fcd.projectServiceTimePoint.projectservice__r.service__r.id) {
							
							if (fcd.projectServiceTimePoint.listOfTimeMinutes__c == null) {
								// no timepoints to process for this service
								break;
							}
							
							// find matching minutes and enable them
							for (string minute : fcd.projectServiceTimePoint.listOfTimeMinutes__c.split(':')) {
								for (flowchartservicetime fst : fs.timepoints) {
									if (integer.valueOf(minute) == fst.totalminutes) {
										fst.selected = true;
									}
								}
							}
							
							break; // service timepoints processed go to next service
						}
					}
					
				}
				
				
			}
			
		}
		
		
	}
	
	public class FlowchartCopyData {
		
		public ProjectServiceTimePoint__c projectServiceTimePoint {get;set;}
		public boolean isServiceAlreadyInFlowchart {get;set;}
		public boolean shouldServiceBeCopied {get;set;}
		public boolean shouldTimePointsBeCopied {get;set;}
		
		public string getPrettyCode () {
			return bdt_utils.removeLeadingZerosFromCat(projectServiceTimePoint.projectservice__r.service__r.servicecategory__r.code__c) + 
				' - ' + projectServiceTimePoint.projectservice__r.service__r.sequencenumber__c;
		}
		
	}
	
	
}