public class IPA_DAL_User{
    private static IPA_DAL_User dalUserInstance = new IPA_DAL_User();
    public User praUser {get; private set;}
    private IPA_DAL_User(){
        If(UserInfo.getUserId() != null & UserInfo.getUserType() != 'Guest'){
        	praUser = [select FullPhotoUrl, SmallPhotoUrl from User where Id = : UserInfo.getUserId() LIMIT 1];
        }
    }
    public static IPA_DAL_User getdalUserInstance(){
        if(dalUserInstance == null){
        	dalUserInstance = new IPA_DAL_User();
        }
        return dalUserInstance;
    }
}