/** Implements the Selector Layer of the object LaboratorySamples__c
 * @author	Dimitrios Sgourdos
 * @version	07-Nov-2013
 */
public with sharing class LaboratorySamplesDataAccessor {
	
	/** Object definition for fields used in application for LaboratorySamples
	 * @author	Dimitrios Sgourdos
	 * @version 25-Oct-2013
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('LaboratorySamples__c');		
	}
	
	
	/** Object definition for fields used in application for LaboratorySamples with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 07-Nov-2013
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'ActiveNumberOfSamples__c,';
		result += referenceName + 'BackupNumberOfSamples__c,';
		result += referenceName + 'BioHazardIndication__c,';
		result += referenceName + 'Project__c,';
		result += referenceName + 'ListOfStudies__c,';
		result += referenceName + 'OtherNumberOfSamples__c,';
		result += referenceName + 'PlaceboNumberOfSamples__c,';
		result += referenceName + 'SequenceNumber__c,';
		result += referenceName + 'ShipmentNumber__c,';
		result += referenceName + 'TotalNumberOfSamples__c';
		return result;
	}
	
	
	/** Retrieve the Laboratory Samples for the given project.
	 * @author	Dimitrios Sgourdos
	 * @version 29-Oct-2013
	 * @param	projectId			The id of the project
	 * @return	The Laboratory Samples list.
	 */
	public static List<LaboratorySamples__c> getByProjectId (String projectId) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM LaboratorySamples__c ' +
								'WHERE  Project__c = {1} ' +
								'ORDER BY ShipmentNumber__c, SequenceNumber__c',
								new List<String> {
									getSObjectFieldString(),
									'\'' + projectId + '\''
								}
							);
		return (List<LaboratorySamples__c>) Database.query(query);
	} 
}