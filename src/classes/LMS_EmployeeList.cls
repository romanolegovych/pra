public class LMS_EmployeeList {

    public string firstName{get;private set;}
    public string lastName{get;private set;}
    public string jobClass{get;private set;}
    public string jobCode{get;private set;}
    public string jobTitle{get;private set;}
    public string businessUnit{get;private set;}
    public string department{get;private set;}
    public string region{get;private set;}
    public string country{get;private set;}
    public string status{get;private set;}
    public string assignedOn{get;private set;}
    
    public LMS_EmployeeList(LMS_Role_Employee__c emp, Map<String, String> m){
        firstName = emp.Employee_Id__r.First_Name__c;
        lastName = emp.Employee_Id__r.Last_Name__c;
        jobClass = emp.Employee_Id__r.Job_Class_Desc__c;
        jobCode = emp.Employee_Id__r.Job_Code__c;
        jobTitle = m.get(emp.Employee_Id__r.Job_Code__c);
        businessUnit = emp.Employee_Id__r.Business_Unit_Desc__c;
        department = emp.Employee_Id__r.Department__c;
        region = emp.Employee_Id__r.Country_Name__r.Region_Name__c;
        country = emp.Employee_Id__r.Country_Name__r.Name;
        status = emp.Employee_Id__r.Status_Desc__c;
        if(emp.Assigned_On__c != null) {
        	assignedOn = emp.Assigned_On__c.format('MMMM dd, yyyy');
        }
    }   
}