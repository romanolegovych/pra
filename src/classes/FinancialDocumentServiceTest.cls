/** Implements the test for the Service Layer of the object FinancialDocument__c
 * @author	Dimitrios Sgourdos
 * @version	23-Jan-2014
 */
@isTest
private class FinancialDocumentServiceTest {
	
	// Global variables
	private static Client_Project__c	currentProject;
	private static Study__c				study;
	private static FinancialDocument__c financialDocument;
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	19-Nov-2013
	 */
	static void init(){
		// Create project
		currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001', 'BDT Project First Experiments');
		insert currentProject;
		
		// Create study
		study = new Study__c();
		study.Project__c = currentProject.Id;
		insert study;
		
		// Create financial document
		financialDocument = new FinancialDocument__c();
		financialDocument.DocumentType__c	= 'Proposal';
		financialDocument.Client_Project__c	= currentProject.Id;
		insert financialDocument;
	}
	
	
	/** Test the function getFinancialDocumentById.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	06-Jan-2014
	 */
	static testMethod void getFinancialDocumentByIdTest() {
		FinancialDocument__c result = FinancialDocumentService.getFinancialDocumentById('');
	}
	
	
	/** Test the function getFinancialDocumentGrouWrapperData
	 * @author	Dimitrios Sgourdos
	 * @version	17-Dec-2013
	 */
	static testMethod void getFinancialDocumentGrouWrapperDataTest() {
		// Create data
		init();
		
		List<ServiceCategory__c> srvCatList = new List<ServiceCategory__c>();
		srvCatList.add(new ServiceCategory__c(Code__c='001', Name='Service Category 1'));
		srvCatList.add(new ServiceCategory__c(Code__c='002', Name='Service Category 2'));
		insert srvCatList;
		
		List<FinancialCategoryTotal__c> fctList = new List<FinancialCategoryTotal__c>();
		fctList.add(new FinancialCategoryTotal__c(FinancialDocument__c = financialDocument.Id,
		 										ServiceCategory__c = srvCatList[0].Id,
		 										Study__c = study.Id));
		fctList.add(new FinancialCategoryTotal__c(FinancialDocument__c = financialDocument.Id,
		 										ServiceCategory__c = srvCatList[1].Id,
		 										Study__c = study.Id));
		insert fctList;
		
		List<FinancialCatTotPrice__c> sourceList = new List<FinancialCatTotPrice__c>();
		sourceList.add(new FinancialCatTotPrice__c(FinancialCategoryTotal__c = fctList[0].Id, Name='Test A1'));
		sourceList.add(new FinancialCatTotPrice__c(FinancialCategoryTotal__c = fctList[0].Id, Name='Test A2'));
		sourceList.add(new FinancialCatTotPrice__c(FinancialCategoryTotal__c = fctList[1].Id, Name='Test B'));
		insert sourceList;
		
		// Check the function
		FinancialDocumentService.financialDocumentGroupWrapper result = new FinancialDocumentService.financialDocumentGroupWrapper();
		result = FinancialDocumentService.getFinancialDocumentGrouWrapperData(financialDocument.Id);
		
		String errorMessage = 'Error in reading financial group data';
		
		system.assertEquals(result.financialDocument.Id, financialDocument.Id, errorMessage);
		
		system.assertEquals(result.pricesPerServiceCategoryMap.size(), 2, errorMessage);
		system.assertEquals(result.pricesPerServiceCategoryMap.containsKey(srvCatList[0].Id), true, errorMessage);
		system.assertEquals(result.pricesPerServiceCategoryMap.containsKey(srvCatList[1].Id), true, errorMessage);
		
		List<FinancialCatTotPrice__c> valuesList = new List<FinancialCatTotPrice__c>();
		valuesList = result.pricesPerServiceCategoryMap.get(srvCatList[0].Id);
		system.assertEquals(valuesList.size(), 2, errorMessage);
		system.assertEquals(valuesList[0].Id, sourceList[0].Id, errorMessage);
		system.assertEquals(valuesList[1].Id, sourceList[1].Id, errorMessage);
		
		valuesList = new List<FinancialCatTotPrice__c>();
		valuesList = result.pricesPerServiceCategoryMap.get(srvCatList[1].Id);
		system.assertEquals(valuesList.size(), 1, errorMessage);
		system.assertEquals(valuesList[0].Id, sourceList[2].Id, errorMessage);
	}
	
	
	/** Test the function isTheFinancialDocumentOwner
	 * @author	Dimitrios Sgourdos
	 * @version	09-Jan-2014
	 */
	static testMethod void isTheFinancialDocumentOwnerTest() {
		// Create Data
		FinancialDocument__c finDoc = new FinancialDocument__c();
		finDoc.DocumentOwnerName__c  = 'TestFirstName TestLastName';
		finDoc.DocumentOwnerEmail__c = 'TestEmail@praIntl.com';
		
		String errorMessage = 'Error in determining if the user is the financial document owner';
		
		// Check the function with different user full name
		Boolean result = FinancialDocumentService.isTheFinancialDocumentOwner(finDoc, 'Test', 'TestEmail@praIntl.com');
		system.assertEquals(result, false, errorMessage);
		
		// Check the function with differen user e-mail
		result = FinancialDocumentService.isTheFinancialDocumentOwner(finDoc, 
																	'TestFirstName TestLastName', 
																	'Test@praIntl.com');
		system.assertEquals(result, false, errorMessage);
		
		// Check the function with the same user full name and e-mail
		result = FinancialDocumentService.isTheFinancialDocumentOwner(finDoc, 
																	'TestFirstName TestLastName', 
																	'TestEmail@praIntl.com');
		system.assertEquals(result, true, errorMessage);
	}
	
	
	/** Test the function releaseFinancialDocumentForApprovals
	 * @author	Dimitrios Sgourdos
	 * @version	09-Jan-2014
	 */
	static testMethod void releaseFinancialDocumentForApprovalsTest() {
		// Create data
		init();
		
		List<FinancialDocumentApproval__c> approvalsList = new List<FinancialDocumentApproval__c>();
		approvalsList.add(new FinancialDocumentApproval__c(FinancialDocument__c = financialDocument.Id,
														ApproverName__c = 'Test name',
														ApproverEmail__c = 'testEmail@praIntl.com',
														DeadLineDate__c = Date.today()));
		
		String errorMessage = 'Error in releasing the financial document for approvals';
		
		// Check the function with not valid approvals(rule description is missing)
		Boolean result = FinancialDocumentService.releaseFinancialDocumentForApprovals(financialDocument, approvalsList);
		
		system.assertEquals(result, false, errorMessage);
		system.assertEquals(financialDocument.IsApprovalReleased__c, false, errorMessage);
		
		// Check the function with valid approvals 
		approvalsList[0].RuleDescription__c = 'Test approval';
		result = FinancialDocumentService.releaseFinancialDocumentForApprovals(financialDocument, approvalsList);
		
		system.assertEquals(result, true, errorMessage);
		system.assertEquals(financialDocument.IsApprovalReleased__c, true, errorMessage);
		system.assertEquals(approvalsList[0].Status__c, FinancialDocumentApprovalDataAccessor.APPROVAL_APPROVED, errorMessage);
		
		// Check the function by creating an exception (for coverage reasons)
		financialDocument.DocumentType__c = NULL;
		result = FinancialDocumentService.releaseFinancialDocumentForApprovals(financialDocument, approvalsList);
		system.assertEquals(result, false, errorMessage);
	}
	
	
	/** Test the function takeDocumentOwnership
	 * @author	Dimitrios Sgourdos
	 * @version	13-Jan-2014
	 */
	static testMethod void takeDocumentOwnershipTest() {
		// Create data
		init();
		
		String previousFullName = 'PreviousFirstName PreviousLastName';
		String previousEmail	= 'testEmail@praIntl.com';
		
		financialDocument.DocumentOwnerName__c = previousFullName;
		financialDocument.DocumentOwnerEmail__c = previousEmail;
		
		FinancialDocumentApproval__c approval = new FinancialDocumentApproval__c(
														FinancialDocument__c = financialDocument.Id,
														ApproverName__c = previousFullName,
														ApproverEmail__c = previousEmail,
														ApproverJobCode__c = 'TestJob',
														ApproverLocationCode__c = 'TestLocation'
												);
		
		Employee_Details__c employee = new Employee_Details__c(First_Name__c = 'NewFirstName',
															Last_Name__c = 'NewLastName',
															Email_Address__c = 'newEmail@praIntl.com',
															Job_Class_Desc__c = 'NewTestJob',
															Location_Code__c = 'NewTestLocation');
		
		String errorMessage = 'Error in taking the ownership of the financial document';
		
		// Check the function with invalid data (financialDocument = NULL, similar it would be for approval or employee)
		Boolean result = FinancialDocumentService.takeDocumentOwnership(NULL, approval, employee);
		system.assertEquals(result, false, errorMessage);
		
		// Check the function with valid data
		result = FinancialDocumentService.takeDocumentOwnership(financialDocument, approval, employee);
		
		system.assertEquals(result, true, errorMessage);
		
		system.assertequals(financialDocument.DocumentOwnerName__c, 'NewFirstName NewLastName', errorMessage);
		system.assertequals(financialDocument.DocumentOwnerEmail__c, 'newEmail@praIntl.com', errorMessage);
		
		system.assertequals(approval.ApproverName__c, 'NewFirstName NewLastName', errorMessage);
		system.assertequals(approval.ApproverEmail__c, 'newEmail@praIntl.com', errorMessage);
		system.assertequals(approval.ApproverJobCode__c, 'NewTestJob', errorMessage);
		system.assertequals(approval.ApproverLocationCode__c, 'NewTestLocation', errorMessage);
		
		// Check the function by creating an exception (for coverage reasons)
		financialDocument.DocumentType__c = NULL;
		result = FinancialDocumentService.takeDocumentOwnership(financialDocument, approval, employee);
		system.assertEquals(result, false, errorMessage);
	}
	
	
	/** Test the function updateDocumentApprovalStatus
	 * @author	Dimitrios Sgourdos
	 * @version	20-Jan-2014
	 */
	static testMethod void updateDocumentApprovalStatusTest() {
		// Create data
		init();
		financialDocument.ApprovalsUpToDate__c = true;
		
		List<FinancialDocumentApproval__c> approvalsList = new List<FinancialDocumentApproval__c>();
		approvalsList.add( new FinancialDocumentApproval__c(
												SequenceNumber__c = 1,
												FinancialDocument__c = financialDocument.Id,
												Status__c = FinancialDocumentApprovalDataAccessor.APPROVAL_APPROVED)
						);
		approvalsList.add( new FinancialDocumentApproval__c(
												SequenceNumber__c = 2,
												FinancialDocument__c = financialDocument.Id,
												Status__c = FinancialDocumentApprovalDataAccessor.APPROVAL_REJECTED)
						);
		insert approvalsList;
		
		String errorMessage = 'Error in updating the document approval status';
		
		// Check the function with NULL parameter (for the code coverage)
		FinancialDocument__c result = FinancialDocumentService.updateDocumentApprovalStatus(NULL);
		system.assertEquals(result, NULL, errorMessage);
		
		// Check the function with a rejected approval
		result = FinancialDocumentService.updateDocumentApprovalStatus(financialDocument);
		
		system.assertEquals(result.Id, financialDocument.Id, errorMessage);
		system.assertEquals(result.ApprovalStatus__c, FinancialDocumentDataAccessor.DOCUMENT_REJECTED, errorMessage);
		system.assertEquals(result.DocumentStatus__c, FinancialDocumentDataAccessor.DOCUMENT_STATUS_REJECTED, errorMessage);
		
		// Check the function with all the approvals approved
		approvalsList[1].Status__c = FinancialDocumentApprovalDataAccessor.APPROVAL_APPROVED;
		update approvalsList[1];
		
		result = FinancialDocumentService.updateDocumentApprovalStatus(financialDocument);
		
		system.assertEquals(result.Id, financialDocument.Id, errorMessage);
		system.assertEquals(result.ApprovalStatus__c, FinancialDocumentDataAccessor.DOCUMENT_ACCEPTED, errorMessage);
		system.assertEquals(result.DocumentStatus__c, FinancialDocumentDataAccessor.DOCUMENT_STATUS_APPROVED, errorMessage);
		
		// Check with NOT a rejected approvald and NOT all approvals approved
		approvalsList[1].Status__c = NULL;
		update approvalsList[1];
		
		result = FinancialDocumentService.updateDocumentApprovalStatus(financialDocument);
		
		system.assertEquals(result.Id, financialDocument.Id, errorMessage);
		system.assertEquals(result.ApprovalStatus__c, NULL, errorMessage);
		system.assertEquals(result.DocumentStatus__c, FinancialDocumentDataAccessor.DOCUMENT_STATUS_IN_PROGRESS, errorMessage);
	}
	
	
	/** Test the function findOtherProposalsOfTheProject
	 * @author	Dimitrios Sgourdos
	 * @version	20-Jan-2014
	 */
	static testMethod void findOtherProposalsOfTheProjectTest() {
		// Create data
		currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001', 'BDT Project First Experiments');
		insert currentProject;
		
		List<FinancialDocument__c> documentList = new List<FinancialDocument__c>();
		documentList.add( new FinancialDocument__c(Name = 'Doc 1',
												DocumentType__c = FinancialDocumentDataAccessor.PROPOSAL_TYPE,
												Client_Project__c = currentProject.Id)
						);
		documentList.add( new FinancialDocument__c(Name = 'Doc 2',
												DocumentType__c = FinancialDocumentDataAccessor.PROPOSAL_TYPE,
												Client_Project__c = currentProject.Id)
						);
		documentList.add( new FinancialDocument__c(Name = 'Doc 3',
												DocumentType__c = FinancialDocumentDataAccessor.PROPOSAL_TYPE,
												Client_Project__c = currentProject.Id)
						);
		insert documentList;
		
		// Check the function
		String errorMessage = 'Error in retrieving the proposals of the project';
		
		List<FinancialDocument__c> results = FinancialDocumentService.findOtherProposalsOfTheProject(documentList[0],
																									'Name');
		
		system.assertEquals(results.size(), 2, errorMessage);
		system.assertEquals(results[0].Id, documentList[1].Id, errorMessage);
		system.assertEquals(results[1].Id, documentList[2].Id, errorMessage);
	}
	
	
	/** Test the function hasTheDocumentOneOfTHeStudies
	 * @author	Dimitrios Sgourdos
	 * @version	20-Jan-2014
	 */
	static testMethod void hasTheDocumentOneOfTHeStudiesTest() {
		// Create dummy data
		FinancialDocument__c finDoc = new FinancialDocument__c();
		
		Set<String> studiesIds = new Set<String> {'2', '3', '4'};
		
		String errorMessage = 'Error in comparing the document studies with the given ones';
		
		// Check the function with document that has no study selection
		Boolean result = FinancialDocumentService.hasTheDocumentOneOfTHeStudies(finDoc, studiesIds);
		system.assertEquals(result, false, errorMessage);
		
		// Check the function with document that has at least one of the given studies
		finDoc.listOfStudyIds__c = '1:4';
		result = FinancialDocumentService.hasTheDocumentOneOfTHeStudies(finDoc, studiesIds);
		system.assertEquals(result, true, errorMessage);
		
		// Check the function with document that hasn't any of the given studies
		studiesIds.remove('4');
		result = FinancialDocumentService.hasTheDocumentOneOfTHeStudies(finDoc, studiesIds);
		system.assertEquals(result, false, errorMessage);
	}
	
	
	/** Test the function makeOtherDocumentsInvalid
	 * @author	Dimitrios Sgourdos
	 * @version	20-Jan-2014
	 */
	static testMethod void makeOtherDocumentsInvalidTest() {
		// Create data
		currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001', 'BDT Project First Experiments');
		insert currentProject;
		
		List<FinancialDocument__c> documentList = new List<FinancialDocument__c>();
		documentList.add( new FinancialDocument__c(Name = 'Doc 1',
												DocumentType__c = FinancialDocumentDataAccessor.PROPOSAL_TYPE,
												Client_Project__c = currentProject.Id
												)
						);
		documentList.add( new FinancialDocument__c(Name = 'Doc 2',
												DocumentType__c = FinancialDocumentDataAccessor.PROPOSAL_TYPE,
												Client_Project__c = currentProject.Id,
												listOfStudyIds__c='1:2')
						);
		documentList.add( new FinancialDocument__c(Name = 'Doc 3',
												DocumentType__c = FinancialDocumentDataAccessor.PROPOSAL_TYPE,
												Client_Project__c = currentProject.Id,
												ApprovalStatus__c = FinancialDocumentDataAccessor.DOCUMENT_REJECTED,
												listOfStudyIds__c = '3:4')
						);
		documentList.add( new FinancialDocument__c(Name = 'Doc 4',
												DocumentType__c = FinancialDocumentDataAccessor.PROPOSAL_TYPE,
												Client_Project__c = currentProject.Id,
												listOfStudyIds__c = '5:6:7')
						);
		insert documentList;
		
		String errorMessage = 'Error in finding and making proposals invalid';
		
		// Check the function with listOfStudyIds__c NULL
		FinancialDocumentService.makeOtherDocumentsInvalid(documentList[0]);
		
		List<FinancialDocument__c> results = FinancialDocumentService.findOtherProposalsOfTheProject(documentList[0],
																									'Name');
		system.assertEquals(results.size(), 3, errorMessage);
		
		system.assertEquals(results[0].Id, documentList[1].Id, errorMessage);
		system.assertEquals(results[0].ApprovalStatus__c, NULL, errorMessage);
		system.assertEquals(results[0].DocumentStatus__c, NULL, errorMessage);
		
		system.assertEquals(results[1].Id, documentList[2].Id, errorMessage);
		system.assertEquals(results[1].ApprovalStatus__c, FinancialDocumentDataAccessor.DOCUMENT_REJECTED, errorMessage);
		system.assertEquals(results[1].DocumentStatus__c, NULL, errorMessage);
		
		system.assertEquals(results[2].Id, documentList[3].Id, errorMessage);
		system.assertEquals(results[2].ApprovalStatus__c, NULL, errorMessage);
		system.assertEquals(results[2].DocumentStatus__c, NULL, errorMessage);
		
		// Check the function with selected listOfStudyIds__c
		documentList[0].listOfStudyIds__c = '2:3';
		FinancialDocumentService.makeOtherDocumentsInvalid(documentList[0]);
		
		results = FinancialDocumentService.findOtherProposalsOfTheProject(documentList[0], 'Name');
		system.assertEquals(results.size(), 3, errorMessage);
		
		system.assertEquals(results[0].Id, documentList[1].Id, errorMessage);
		system.assertEquals(results[0].ApprovalStatus__c, NULL, errorMessage);
		system.assertEquals(results[0].DocumentStatus__c, FinancialDocumentDataAccessor.DOCUMENT_STATUS_INVALID, errorMessage);
		
		system.assertEquals(results[1].Id, documentList[2].Id, errorMessage);
		system.assertEquals(results[1].ApprovalStatus__c, FinancialDocumentDataAccessor.DOCUMENT_REJECTED, errorMessage);
		system.assertEquals(results[1].DocumentStatus__c, FinancialDocumentDataAccessor.DOCUMENT_STATUS_INVALID, errorMessage);
		
		system.assertEquals(results[2].Id, documentList[3].Id, errorMessage);
		system.assertEquals(results[2].ApprovalStatus__c, NULL, errorMessage);
		system.assertEquals(results[2].DocumentStatus__c, NULL, errorMessage);
	}
	
	
	/** Test function lockStudyServicePricingRecords
	 * @author	Dimitrios Sgourdos
	 * @version	20-Jan-2014
	 */
	static testMethod void lockStudyServicePricingRecordsTest() {
		// Create data
		ServiceCategory__c srvCat = new ServiceCategory__c(code__c='001',name='Service Category (1)');
		insert srvCat;
		
		Service__c srv = new Service__c(Name = 'Srv 1', ServiceCategory__c=srvCat.Id);
		insert srv;
		
		Client_Project__c tmpProject = BDT_BuildTestData.buildClientProject('PRA1312-001', 'BDT Project First Experiments');
		insert tmpProject;
		
		Study__c tmpStudy = new Study__c();
		tmpStudy.Project__c = tmpProject.Id;
		insert tmpStudy;
		
		ProjectService__c prSrv = new ProjectService__c(Service__c = srv.id, Client_Project__c = tmpProject.id);
		insert prSrv;
		
		StudyService__c stSrv = new StudyService__c(ProjectService__c = prSrv.Id,
													NumberOfUnits__c=1,
													Study__c=tmpStudy.Id);
		insert stSrv;
		
		financialDocument = new FinancialDocument__c(
													Name = 'Test doc',
													DocumentStatus__c = FinancialDocumentDataAccessor.DOCUMENT_STATUS_NEW,
													DocumentType__c = FinancialDocumentDataAccessor.PROPOSAL_TYPE,
													Client_Project__c = tmpProject.Id,
													listOfStudyIds__c = tmpStudy.Id);
		insert financialDocument;
		
		// Associate document with study service pricings
		String whereClause = 'StudyService__c = ' + '\'' + stSrv.Id + '\'';
		List<StudyServicePricing__c> sspList = StudyServicePricingDataAccessor.getStudyServicePricings(
																						whereClause,
																						'NumberOfUnits__c');
		
		for (StudyServicePricing__c ssp:sspList){
			ssp.FinancialDocumentIDs__c = financialDocument.id;
		}
		upsert sspList;
		
		// Check the function
		FinancialDocumentService.lockStudyServicePricingRecords(financialDocument.id);
		
		List<StudyServicePricing__c> results = StudyServicePricingDataAccessor.getStudyServicePricings(
																						whereClause,
																						'NumberOfUnits__c');
		
		String errorMessage = 'Error in locking associated study service pricings';
		for(StudyServicePricing__c tmpItem : results) {
			system.assertEquals(tmpItem.ApprovedFinancialDocument__c, financialDocument.id, errorMessage);
		}
	}
	
	
	/** Test function getDocumentSectionPageRef
	 * @author	Dimitrios Sgourdos
	 * @version	22-Jan-2014
	 */
	static testMethod void getDocumentSectionPageRefTest() {
		String errorMessage = 'Error in returning the page reference of the process step';
		
		String result = FinancialDocumentService.getDocumentSectionPageRef(
														FinancialDocumentHistoryDataAccessor.STUDY_SELECTION_PROCESS);
		system.assertEquals(result, '/apex/bdt_financialdocstudysel?financialDocumentId=', errorMessage);
		
		result = FinancialDocumentService.getDocumentSectionPageRef(
														FinancialDocumentHistoryDataAccessor.PRICE_PROCESS);
		system.assertEquals(result, '/apex/bdt_financialdocpricing?financialDocumentId=', errorMessage);
		
		result = FinancialDocumentService.getDocumentSectionPageRef(
														FinancialDocumentHistoryDataAccessor.PAYMENT_PROCESS);
		system.assertEquals(result, '/apex/bdt_financialdocpayment?financialDocumentId=', errorMessage);
		
		result = FinancialDocumentService.getDocumentSectionPageRef(
														FinancialDocumentHistoryDataAccessor.APPROVAL_PROCESS);
		system.assertEquals(result, '/apex/bdt_financialdocapproval?financialDocumentId=', errorMessage);
		
		result = FinancialDocumentService.getDocumentSectionPageRef('Test process');
		system.assertEquals(result, '', errorMessage);
	}
	
	
	/** Test function createHtmlBodyForEmailNotifications
	 * @author	Dimitrios Sgourdos
	 * @version	22-Jan-2014
	 */
	static testMethod void createHtmlBodyForEmailNotificationsTest() {
		// Create data
		init();
		
		financialDocument = FinancialDocumentService.getFinancialDocumentById(financialDocument.Id);
		
		// Check the function
		String result = FinancialDocumentService.createHtmlBodyForEmailNotifications(financialDocument,
																					'Test process',
																					'Test user',
																					dateTime.now(),
																					UserInfo.getTimeZone()
						);
		
		system.assertNotEquals(result, NULL, 'Error in creating the body of email');
	}
	
	
	/** Test function createHtmlBodyForApprovalEmailAppointment
	 * @author	Dimitrios Sgourdos
	 * @version	23-Jan-2014
	 */
	static testMethod void createHtmlBodyForApprovalEmailAppointmentTest() {
		// Create data
		init();
		
		financialDocument = FinancialDocumentService.getFinancialDocumentById(financialDocument.Id);
		
		// Check the function
		String result = FinancialDocumentService.createHtmlBodyForApprovalEmailAppointment(financialDocument,
																						'Test Reason',
																						dateTime.now()	);
		
		system.assertNotEquals(result, NULL, 'Error in creating the body of email');
	}
	
	
	/** Test function createDocumentApprovalAppointmentInstance
	 * @author	Dimitrios Sgourdos
	 * @version	23-Jan-2014
	 */
	static testMethod void createDocumentApprovalAppointmentInstanceTest() {
		blob result = FinancialDocumentService.createDocumentApprovalAppointmentInstance('Test Organizer Name',
																					'test_1_email@praintl.com',
																					'Eastern Standard Time',
																					'Test Attendee Name',
																					'test_2_email@praintl.com',
																					dateTime.now() );
		
		system.assertNotEquals(result, NULL, 'Error in creating document apporval appointment');
	}
}