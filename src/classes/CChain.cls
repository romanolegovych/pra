public with sharing class CChain
{
	private ID rootID;
	public CChain(ID rootID)
	{
		this.rootID = rootID;
		initialize();
	}
	
	private void initialize()
	{
		
	}
	
	private List<ID> selectDescendantFlowIDs()
	{
		List<ID> descendantIDs = new List<ID>();
		List<ID> childIDs = new List<ID>{rootID};
		Boolean keepGoing = true;
		while (keepGoing)
		{
			keepGoing = false;
			for (STSWR1__Flow_Step_Action__c action : [select STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Flow_Step__r.STSWR1__Flow__c in :childIDs and STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active'])
			{
				try
				{
					Map<String, Object> config = (Map<String, Object>) JSON.deserializeUntyped(action.STSWR1__Config__c);
					if (config != null && !String.isEmpty((String) config.get('flowId'))) 
					{
						if (keepGoing == false)
						{
							childIDs.clear();
						}
						
						keepGoing = true;
						childIDs.add((ID) config.get('flowId'));
					}
				}
				catch (Exception ex)
				{
					//do nothing
				}
			}
			
			descendantIDs.addAll(childIDs);
		}
		
		return descendantIDs;
	}
}