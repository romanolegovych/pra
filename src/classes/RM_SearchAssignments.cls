global  class RM_SearchAssignments {
    public string searchOn{get;set;}
    public string additionalfield{get; set;}
    public string[] buSaveSearch = new list<string>();
    
    //public string searchTextStyle{get;set;}
    //public string strSearchText{get;set;}
    //public string selSearch{get;set;}
    public String nomnth { get; set; }   
    public string selectedDate{get; set;}
    public string selectedDir{get; set;}
    
    public List<string> selectedCountry { get; set; }
    transient Set<String> setBUWithoutEmptyValue = new Set<String>(); 
    transient set<string> setRolesWithoutEmptyValue = new set<string>();
    transient set<string> setBufcodesWithoutEmptyValue= new set<string>();
    public  List<String> selectedRoles{get;set;}
    public String selDatType { get; set; }
    public String selassigntype { get; set; }
    public List<String> selectedCodes {get;set;}
    public String selstatus { get; set; }
    //public List<String> selectedFuncCodes { get; set; }
    public set<string> stManagedBU{get; private set;}
    public list<string> lstManagedBU{get; private set;}
    public String myval { get; set; }
    public List<SelectOption> jobClass{get;set;}
    public List<SelectOption> lstCountry{get;set;}
    public List<SelectOption> lstBU{get;set;}
   
    public List<String> selectedBusinessUnit{ get; set; }
    transient List<WFM_employee_Allocations__c> aL{get;set;}
    public String jsonstr{get;set;}
    public List<Id> jsonstrlist{get;set;}
    //public List<WFM_employee_Allocations__c> jsonstrlistid{get;set;}
    public boolean bResult{get; set;}
    public String query{get; private set;}
    public boolean searchCriteria{get;set;}
    public boolean errormsg{get;set;}
    public boolean bDisplayResult{get; set;}
    public boolean norecordFound{get;set;}
    public Date startDate;
    public Date endDate;
    
    //for save function
    static final string PROJECT_ID = 'Project ID';
    static final string SPONSOR = 'Sponsor';
    static final string CREATEDBY = 'Created By';
    static final string LASTMODIFIEDBY = 'Last Modified By';
    static final string REQUESTID = 'Request ID';   
    static final string ASSIGNSTATUS = 'Status';
    static final string ASSIGNTYPE = 'Type';
    static final string DATETYPE = 'Date Type';
    static final string FROMDATE = 'FromDate';
    static final string TODATE = 'ToDate';
    static final string PRABU = 'BU';
    static final string ROLE = 'ROLE';
    static final string LEAD = 'LEAD';
    static final string PSSV = 'PSSV';
    static final string UNBLINDED = 'UNBLINEDE';
    static final string COUNTRY = 'Country';
    static final string BU_CODE = 'BU Code';
    static final string DIRECTION = 'Direction';
    static final string COMPVALUE = 'Compare_value';
    static final string ACTIVEPROJECT = 'Active_Project';
    
    private List<string> lstRoles;
    
    public RM_SearchAssignments ()
    {  
        bDisplayResult=false;
        errormsg=false;
        norecordFound=false;
        //searchCriteria=true;
        bResult = false;
        initVals();
        initSavedSearch();
      //  searchObject = 'WFM_Project__c';
        //empaloc = [SELECT Status__c ,Type__c FROM WFM_employee_Allocations__c];
            // textsearchbox ='Name';
      
       stManagedBU = RM_OtherService.GetUserGroupBU(UserInfo.getUserId());       
        system.debug('---------ManagedBU---------'+stManagedBU); 
        if (stManagedBU != null && stManagedBU.size() > 0){
            lstManagedBU = new list<string>();
            if(buSaveSearch.size()== 0){
              lstManagedBU.addAll(stManagedBU);
              selectedBusinessUnit = lstManagedBU;
            } 
            else
            {
              selectedBusinessUnit=buSaveSearch;
            }   
        }
        else
        {
            lstManagedBU=new list<string>();
            if(buSaveSearch.size()== 0){
               string[] bu = new list<string>();
               bu.Add('');
               selectedBusinessUnit = bu;
            } else{
              selectedBusinessUnit=buSaveSearch;
            }  
        }
        jobClass= RM_LookUpDataAccess.getJobClassDescByBU(true, selectedBusinessUnit);    
        lstCountry = RM_LookUpDataAccess.getCountry(true);
        lstBU= RM_LookUpDataAccess.getLocBUwithListContry(true, selectedCountry);
     }
         
 
      
      //for detail page
    public List<WFM_employee_Allocations__c> getAllocationDetail(){
        //if(strSearchText !='' && myval !=null){
        if (myval.equalsIgnoreCase('Project ID')){
             if (selSearch == strSearchText)
             query=  'select Id from WFM_employee_Allocations__c where Project_ID__r.name=:strSearchText';
              else
              query='select Id from WFM_employee_Allocations__c where Project_ID__r.name LIKE \''+String.escapeSingleQuotes(strSearchText)+'%\'';
        }
        else if (myval == 'Sponsor' ){
             query=  'select Id from WFM_employee_Allocations__c where Project_ID__r.Contract_id__r.Client_ID__r.name =:strSearchText';
        }
        else if(myval =='Created By'){              
              List<String> strList=new List<String>();
              String userName='';
              User u;
              List<User> lu;
              if(strSearchText !=null){
              strList=strSearchText.split(',');
              if(strList.size() >1){
                userName=strList[1]+' '+strList[0];
              } 
              
              }
              if(strList.size() >1){
                u=[select id from User where FirstName =:strList[1] or LastName=:strList[0] limit 1];
                query='select Id from WFM_employee_Allocations__c where createdbyid=\''+u.id+'\'';
              }else {
                lu=[select id from User where LastName LIKE :String.escapeSingleQuotes(strSearchText)+'%'];
                query='select Id from WFM_employee_Allocations__c where createdbyid in :lu';
              }           
        }
        else if(myval=='Last Modified By') {
               List<String> strList=new List<String>();
              String userName='';
              User u;
              List<User> lu;
              if(strSearchText !=null){
              strList=strSearchText.split(',');
              if(strList.size() >1){
                userName=strList[1]+' '+strList[0];
              }              
              }
              if(strList.size() >1){
                u=[select id from User where FirstName =:strList[1] or LastName=:strList[0] limit 1];
                query='select Id from WFM_employee_Allocations__c where LastModifiedById=\''+u.id+'\'';
              }else{
                lu=[select id from User where LastName LIKE :String.escapeSingleQuotes(strSearchText)+'%'];
                query='select Id from WFM_employee_Allocations__c where LastModifiedById in : lu';              
              }
        }
        else  if(myval == 'Request ID' ){ 
                //    query='select Id,Project_ID__r.Contract_id__r.Client_ID__r.name,Project_ID__r.name,Request__r.Request_ID__c,Status__c,Type__c,EMPLOYEE_ID__r.Business_Unit__c,Project_Function__c,Is_Lead__c,Is_IEDR__c,Is_Unblinded__c,BU_Code__c from WFM_employee_Allocations__c where Request__r.Request_ID__c=:strSearchText';
              query=   'select Id from WFM_employee_Allocations__c ';
              if (selSearch == strSearchText)
                    query+=  'where Request__r.Request_ID__c = :strSearchText';
              else
                    query+=  'where Request__r.Request_ID__c LIKE \''+String.escapeSingleQuotes(strSearchText)+'%\'';
        }
   
        
        if( strSearchText.startsWith('Enter')||strSearchText=='' ){
              query='select Id from WFM_employee_Allocations__c where name!=\'\'';
        }          
        if(selstatus!=''&&selstatus!=null)
            query=query+' and Status__c=:selstatus';       
        if(selassigntype !=null && selassigntype !=''){
             query=query+' and Type__c=:selassigntype';   
        }                   
        if((selDattype!='' || selDattype!=null) && (datefilter!=null||datefilter!='')&&(datefilter1!=null||datefilter1!=''))
        {
           startDate= RM_Tools.GetDateFromString(datefilter, 'mm/dd/yyyy');
           endDate = RM_Tools.GetDateFromString(datefilter1, 'mm/dd/yyyy');
           
        if(selDattype=='Created Date'&&(startDate !=null && endDate !=null))                
            query=query+' and CreatedDate>=: startDate and CreatedDate<=: endDate';
        if(selDattype=='Last Modified Date'&&( startDate !=null && endDate !=null))
            query=query+' and LastModifiedDate>=: startDate and LastModifiedDate<= :endDate';
        if(selDattype=='Start Date'&&(startDate !=null && endDate !=null))
            query=query+' and Allocation_Start_Date__c>=:startDate and Allocation_Start_Date__c<=:endDate';
        if(selDattype=='End Date'&&(startDate !=null && endDate !=null))
            query=query+' and Allocation_End_Date__c>=:startDate and Allocation_End_Date__c<=:endDate';            
        }
        try{
            
           if(selectedBusinessUnit.size()!=0  ){ 
                
                if(selectedBusinessUnit[0]!=null&&selectedBusinessUnit[0]!='') {//selected BU
                    if(selectedRoles.size()!=0){//select BU & role
                        if(selectedRoles[0]!=null&&selectedRoles[0]!='')
                            query=query+' and Project_Function__c in:selectedRoles and employee_id__r.business_unit_desc__c in: selectedBusinessUnit ';
                        else{
                            //get all roles
                            lstRoles = RM_OtherService.getJobClassDescByBU(selectedBusinessUnit);                 
                            query=query+' and Project_Function__c in:lstRoles and employee_id__r.business_unit_desc__c in: selectedBusinessUnit ';
                        }
                    }
                    
                }
                else{ //not BU
                        if(selectedRoles.size()!=0){
                       
                           if(selectedRoles[0]!=null&&selectedRoles[0]!='')
                           query=query+' and Project_Function__c in:selectedRoles';
                       }
                }
           
            }
          
        }
        catch(Exception e){}
        
        try{
            if(selectedCodes.size()!=0){
               if(selectedCodes[0]!=null&&selectedCodes[0]!='')
                
                query=query+' and project_buf_code__c IN:selectedCodes';
               }
        }
        catch(Exception e){}               
    
         
        if(selectedCountry.size() >0 ){
            if(selectedCountry[0]!=null&&selectedCountry[0]!=''){
                query=query+' and Project_Country__c in : selectedCountry';  
                system.debug('---------selectedCountry---------'+selectedCountry); 
            }
        }
        /*try{
            if(selectedFuncCodes.size()!=0){
              if(selectedFuncCodes[0]!=null&&selectedFuncCodes[0]!='')
               query=query+' and EMPLOYEE_ID__r.Function_Code__c IN:selectedFuncCodes';
           }
        }
        catch(Exception e){}*/
               
        if(selectedDir != '' && selectedDir != NULL && selectedDate != '' && selectedDate!= null ){
             
             if (selectedDir== 'Before')
                query +=' and DiffEndDate__c < -' + selectedDate; 
             else
                query +=' and DiffEndDate__c > ' + selectedDate; 
             if (isActive)
                query += ' and project_id__r.status_desc__c = \'Active\'';
        }
        if(isLead){                 
             query=query+' and Is_Lead__c=:isLead ';  
        }
        if(ispssv){
             query=query+' and Is_IEDR__c=:ispssv ';
        }
        if(isunblinded){
             query=query+' and Is_Unblinded__c=:isunblinded';
        }
        
        try{
            system.debug('query ##'+query);
            aL = Database.query(query);
            if(aL.size() >0){
                bDisplayResult=true;
                norecordFound=false;
                if(al.size() >1000){
                   errormsg=true;
                   bDisplayResult=false;   
                   
                } 
                else{
                    errormsg=false;
                    bDisplayResult=true;
                }
                 //searchCriteria=false;
            }
            else {
                bDisplayResult=false;
                norecordFound=true;
                errormsg=false;
                bDisplayResult=false;
                //searchCriteria=true;
            }
        }
        catch(Exception e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'List has more than 1000 records');
            ApexPages.addMessage(myMsg); 
       
        
        }
        if(aL !=null){ 
            if (aL.size() > 0 && bDisplayResult){         
                jsonstr = JSON.serialize(al);
                System.debug('----Serialized list of invoices into JSON format: ' + jsonstr);
            }
        }
        return aL;
      }
      
    public String getUserName(String strSearchText){
        List<String> strList=new List<String>();
        strList=strSearchText.split(',');
        String userName=strList[1]+' '+strList[0];
        System.debug('User Name ##'+userName);
        return userName;            
    }
      
    public PageReference go(){     
        bResult = true;
        bDisplayResult=true;
        system.debug('--------go' );
        getAllocationDetail();
        
        return null;
    }
      
    public PageReference changedval() {
    
        system.debug('-------------changevalue---'+myval );
        additionalfield='';   
        if (myval =='Project ID'){  
              
           strSearchText = 'Enter a project ID';
           searchObject = 'WFM_Project__c';
           searchfield='Name';
           
        }  
        else  if(myval == 'Request ID' ) {
             strSearchText = 'Enter a Request ID';               
             SearchObject='WFM_Request__c';
             searchfield='Request_ID__c';            
                         
        } 
        else if (myval == 'Sponsor' ){
        
            strSearchText = 'Enter a Sponsor Name';
             searchObject = 'WFM_Client__c';
             searchfield='Name';
             
        } 
        else if(myval =='Created By') {
             
             strSearchText='Enter User Name';
             SearchObject='User';
             searchfield='LastName';
             additionalfield='FirstName';
        
        }
        else if(myval=='Last Modified By') {
             
             strSearchText='Enter User Name';
             SearchObject='User';
             searchfield='LastName';
             additionalfield='FirstName';
             //searchfield='Name';
        }
         
        return null;
    }

    public string searchTextStyle{get;set;}
    public string strSearchText{get;set;}
    public string selSearch{get;set;}
    public string strTheropeticArea{get;set;}
    public string[] selectedStatus{get;set;}
    public string[] selectedPhase{get;set;}
    public string startFDate{get; set;}
    public string startEDate{get; set;}
    public string toFDate{get; set;}
    public string toEDate{get; set;}
    public string bidFDate{get; set;}
    public string bidEDate{get; set;}
  
    public boolean bShowerror{get; set;}
     
   
    
    public string searchObject{get; set;}
    public string searchfield{get; set;}
   
    public Transient list<WFM_employee_Allocations__c> empaloc {get;set;} 
  
    public string selectedRecord{get; set;}

    public String datefilter1 { get; set; }

    public String datefilter { get; set; }

    public String textsearchbox { get; set; }
    public boolean isLead{get;set;}
    public boolean ispssv{get;set;}
    public boolean isunblinded{get;set;}
    public boolean isActive{get;set;}
    
    public list<selectoption> gettxtsearch(){
        list<selectoption> op = new list<selectoption>();

        op.add(new selectoption ('Created By','Created By'));
        op.add(new SelectOption('Last Modified By','Last Modified By'));
        op.add(new SelectOption('Project ID','Project ID'));
        op.add(new SelectOption('Request ID','Request ID'));
        op.add(new SelectOption('Sponsor','Sponsor'));
    
    
        return op;
    }

    private void initVals(){
        
         myval= 'Project ID';
         searchTextStyle='WaterMarkedTextBox';
       //  strSearchText = 'Enter a project ID';
         strTheropeticArea='';
         List <string> status = new List<string>();
         status.add(''); 
         selectedStatus = status;
         List <string> selPhase = new List<string>();
         selPhase.add(''); 
         selectedPhase = selPhase;
         changedval();
       //  searchObject = 'WFM_Project__c';
        // searchfield='Name';
        
    }
  
         
    public list<selectoption> getstatus(){

        list<selectoption>opp = new list<selectoption>();
        opp.add(new selectoption('',''));
        opp.add(new selectoption('Confirmed','Confirmed'));
        opp.add(new selectoption('Proposed','Proposed'));
        opp.add(new selectoption('Rejected','Rejected'));
    
        return opp;
    }

    public list<selectoption> getassigntype(){

        list<selectoption>opp = new list<selectoption>();

        opp.add(new selectoption('',''));

        opp.add(new selectoption('Assignment','Assignment'));
        opp.add(new selectoption('Reservation','Reservation'));
        return opp;
    }

    public list<selectoption> getdattype(){
    
        list<selectoption>opp = new list<selectoption>();
        opp.add(new selectoption('',''));
        opp.add(new selectoption('Created Date','Created Date'));
        opp.add(new selectoption('Last Modified Date','Last Modified Date'));
        opp.add(new selectoption('Start Date','Start Date'));
        opp.add(new selectoption('End Date','End Date'));
    
        return opp;
    }

    public List<SelectOption>getgroup(){
        transient List<SelectOption> options = RM_LookUpDataAccess.getBU(true);
        return options;
    } 
    
    
    public PageReference getJobClassWithBU(){   
        jobClass= RM_LookUpDataAccess.getJobClassDescByBU(true, selectedBusinessUnit);
        return null; 
    }
    
    public PageReference getBULocWithCountry(){   
        lstBU= RM_LookUpDataAccess.getLocBUwithListContry(true, selectedCountry);
        return null; 
    }

   
    public list<selectoption> getSign(){
      list<selectoption>opp = new list<selectoption>();
      opp.add(new selectoption('',''));
      opp.add(new selectoption('>','>'));
      opp.add(new selectoption('<','<'));   
      return opp;
    }
     public list<selectoption> getComparionDate(){
      list<selectoption>opp = new list<selectoption>();
      opp.add(new selectoption('',''));
      opp.add(new selectoption('15','15 days'));
      opp.add(new selectoption('45','45 days')); 
      opp.add(new selectoption('75','75 days'));     
      return opp;
    }
     public list<selectoption> getDirection(){
      list<selectoption>opp = new list<selectoption>();
      opp.add(new selectoption('',''));
      opp.add(new selectoption('Before','Before'));
      opp.add(new selectoption('After','After')); 
        
      return opp;
    }
    public list<selectoption> getmonths(){
    
        list<selectoption>opp = new list<selectoption>();
        opp.add(new selectoption('',''));
        opp.add(new selectoption('1','1'));
        opp.add(new selectoption('2','2'));
        opp.add(new selectoption('3','3'));
        opp.add(new selectoption('4','4'));
        opp.add(new selectoption('5','5'));
        opp.add(new selectoption('6+','6+'));
        return opp;
    }
   /* private void initSearchText(){
         
        if (myval=='Project ID'){
             strSearchText = 'Enter a project ID';
         
        }
        if (myval=='Sponsor'){
             strSearchText = 'Enter a Sponsor Name';
         
        }
        if (myval=='Created By'){
             strSearchText = 'Enter User Name';
         
        }
        if (myval=='Last Modified By'){
             strSearchText = 'Enter User Name';
         
        }
        if (myval=='Request ID'){
             strSearchText = 'Enter a Request ID';
         
        }
        else{
             strSearchText = 'Enter a client Name';
      
        }
    }*/
    private void initSavedSearch(){
        string searchId = ApexPages.currentPage().getParameters().get('cid');
        system.debug('-------------searchId----------------' + searchId);
        
        if(searchId != ''){
            system.debug('-------------searchId----------------' + searchId);
            list<Search_Detail__c> searchDetails = new list<Search_Detail__c>();
            string strValue;
            string[] status = new list<string>();
            string[] phase = new list<string>();
            string[] savedRole=new list<String>();          
            string[] savedCountry=new list<String>();
            string[] savedBufCode=new list<String>();
                        
           // initSearchText();
            searchDetails = RM_OtherService.getSavedSearchDetail(searchId);
            System.debug('searchDetails ##'+searchDetails);
            for(Integer i = 0; i < searchDetails.size(); i++){
                strValue = string.valueof(searchDetails[i].get('Search_Value__c'));               
                
                if(searchDetails[i].get('Name') == PROJECT_ID ){                        
                        myval = 'Project ID';
                        strSearchText= string.valueof(searchDetails[i].get('Search_Value__c'));
                        system.debug('------------strSearchText--------------'+strSearchText);            
                        searchTextStyle='NormalTextBox';                        
                }
                if(searchDetails[i].get('Name') == SPONSOR ){                        
                        myval='Sponsor';
                        strSearchText = string.valueof(searchDetails[i].get('Search_Value__c'));
                        system.debug('------------strSearchText--------------'+strSearchText);            
                        searchTextStyle='NormalTextBox';                        
                }
                if(searchDetails[i].get('Name') == CREATEDBY){                        
                        myval='Created By';
                        strSearchText = string.valueof(searchDetails[i].get('Search_Value__c'));
                        system.debug('------------strSearchText--------------'+strSearchText);            
                        searchTextStyle='NormalTextBox';                        
                }
                if(searchDetails[i].get('Name') == LASTMODIFIEDBY ){                        
                        myval='Last Modified By';
                        strSearchText = string.valueof(searchDetails[i].get('Search_Value__c'));
                        system.debug('------------strSearchText--------------'+strSearchText);            
                        searchTextStyle='NormalTextBox';                        
                }
                if(searchDetails[i].get('Name') == REQUESTID ){                        
                        myval='Request ID';
                        strSearchText = string.valueof(searchDetails[i].get('Search_Value__c'));
                        system.debug('------------strSearchText--------------'+strSearchText);            
                        searchTextStyle='NormalTextBox';                        
                }                
                if(searchDetails[i].get('Name') == ASSIGNSTATUS){                         
                        selstatus  = string.valueof(searchDetails[i].get('Search_Value__c'));               
                        system.debug('------------selstatus--------------'+selstatus);            
                                                
                }
                         
                if(searchDetails[i].get('Name') == ASSIGNTYPE){                           
                         selassigntype  = string.valueof(searchDetails[i].get('Search_Value__c'));                 
                        system.debug('------------selassigntype--------------'+selassigntype);                                              
                }
                
                if(searchDetails[i].get('Name') == DATETYPE ){                         
                        selDatType  = string.valueof(searchDetails[i].get('Search_Value__c'));                    
                        system.debug('------------selDatType--------------'+selDatType);                                              
                }
                
                                 
                if(searchDetails[i].get('Name') == FROMDATE ){                        
                        //List <string> datePart = string.valueof(searchDetails[i].get('Search_Value__c')).split('-');                      
                        //datefilter=date.newInstance(Integer.valueOf(datePart[1]), Integer.valueOf(datePart[2]), Integer.valueOf(datePart[0]));
                        //system.debug('From Date##'+date.newInstance(Integer.valueOf(datePart[1]), Integer.valueOf(datePart[2]), Integer.valueOf(datePart[0])));
                        datefilter=string.valueof(searchDetails[i].get('Search_Value__c'));                                   
                }               
                if(searchDetails[i].get('Name') == TODATE){                         
                        //List <string> datePart = string.valueof(searchDetails[i].get('Search_Value__c')).split('-');                      
                        //datefilter1=date.newInstance(Integer.valueOf(datePart[1]), Integer.valueOf(datePart[2]), Integer.valueOf(datePart[0]));   
                        //system.debug('To Date##'+date.newInstance(Integer.valueOf(datePart[1]), Integer.valueOf(datePart[2]), Integer.valueOf(datePart[0])));                     
                        datefilter1=string.valueof(searchDetails[i].get('Search_Value__c'));
                }
                if(searchDetails[i].get('Name')== PRABU ){                            
                        buSaveSearch.add(strValue);                                                 
                        system.debug('Business Unite ##'+string.valueof(searchDetails[i].get('Search_Value__c')));                                              
                }
                if(searchDetails[i].get('Name')== ROLE ){                            
                        savedRole.add(strValue);                                                 
                        system.debug('Business Unite ##'+string.valueof(searchDetails[i].get('Search_Value__c')));                                              
                } 
                if(searchDetails[i].get('Name')== LEAD){                            
                       isLead=Boolean.valueof(searchDetails[i].get('Search_Value__c'));                                          
                }
                if(searchDetails[i].get('Name')== PSSV){                            
                       isPssv=Boolean.valueof(searchDetails[i].get('Search_Value__c'));                                          
                }
                if(searchDetails[i].get('Name')== UNBLINDED){                            
                       isunblinded=Boolean.valueof(searchDetails[i].get('Search_Value__c'));                                          
                }
                if(searchDetails[i].get('Name')== COUNTRY){                            
                        savedCountry.add(strValue);                                                 
                        system.debug('Country ##'+string.valueof(searchDetails[i].get('Search_Value__c')));                                              
                } 
                if(searchDetails[i].get('Name')== BU_CODE){                            
                        savedBufCode.add(strValue);                                                 
                        system.debug('Country ##'+string.valueof(searchDetails[i].get('Search_Value__c')));                                              
                } 
                if(searchDetails[i].get('Name')== DIRECTION){                            
                        selectedDir=string.valueof(searchDetails[i].get('Search_Value__c'));                                 
                                                                     
                }
                if(searchDetails[i].get('Name')== COMPVALUE){                            
                        selectedDate=string.valueof(searchDetails[i].get('Search_Value__c'));                                 
                                                                     
                }
                if(searchDetails[i].get('Name')== ACTIVEPROJECT){                            
                        isActive=Boolean.valueof(searchDetails[i].get('Search_Value__c'));                                 
                                                                     
                }
               
            }
            if(buSaveSearch.size() >0 ){
              selectedBusinessUnit = buSaveSearch;
            }
            if(savedRole.size() >0){
               selectedRoles = savedRole;
            }
            if(savedCountry.size() >0){
               selectedCountry = savedCountry;
            }
            if(savedBufCode.size() >0){
               selectedCodes = savedBufCode;
            }
            
        }
    }       
                
    /********
    Save search
    **********/
    public string saveSubFolder {get; set;}
    public string searchName {get; set;}
    public string saveSubId{get;set;}
    public string rootId{get;set;}
 
    private string strIndications{get;set;}

    private Search_Detail__c detail{get;set;}
    
    public list<SelectOption> getResourceFolders(){
        transient List<SelectOption> options = RM_LookUpDataAccess.GetSavedFolder('Assignments');
       
        return options;
    }
    @RemoteAction
    global static boolean CheckSaveSearch(string searchName, string folderID){
         string userId = UserInfo.getUserId();
        
         list<Saved_Search__c> searchNameLookup = RM_OtherService.getSavedSearchName(searchName,userId, folderID );
          system.debug('---------searchNameLookup---------'+searchNameLookup);
         if (searchNameLookup.size() > 0)
            return true;
         else
            return false;
    }
    public PageReference saveSearch() {
        Integer i;
        string userId = UserInfo.getUserId();
        system.debug('---------saveSubFolder---------'+saveSubFolder);
        if (saveSubFolder == '' || saveSubFolder== null){
            RM_OtherService.insertDefaultSubFolder(userId);
            // Get saveSubFolder id
            Sub_Folder__c sub = RM_OtherService.GetSubFolderID('My Assignment Searches', 'Assignments', UserInfo.getUserID());
            if (sub != null && sub.id != null)
                saveSubFolder = sub.id;
        }
        
        if(saveSubFolder != null){  
            
            list<Saved_Search__c> searchNameLookup = RM_OtherService.getSavedSearchName(searchName,userId, saveSubFolder );
            if (searchNameLookup.size() > 0){
                delete searchNameLookup;
            }
            
            Saved_Search__c newSearch = new Saved_Search__c(sub__c = saveSubFolder, Name = searchName, Search_Type__c = 'Project', User__c = UserInfo.getUserId(), Saved_Search_Unique_Key__c=searchName+saveSubFolder);
            insert newSearch;
            system.debug('---------newSearch---------'+newSearch);
            //newSearch = [select Id, Name from Saved_Search__c where Name = :searchName and User_Id__c = :userId];
            //system.debug('---------newSearch---------'+newSearch);
            if(newSearch !=null && newSearch.id != null){
                list<Search_Detail__c> details = new list<Search_Detail__c>();
                system.debug('strSearchText ##'+strSearchText);
                system.debug('myval ##'+myval);
                 if(!strSearchText.contains('Enter') && strSearchText !='' && strSearchText !=null){
                    detail = new Search_Detail__c(Name = myval, Search_Value__c =string.valueof(strSearchText),File__c = newSearch.Id,Search_Detail_Unique_Key__c=myval+strSearchText+newSearch.Id);
                    details.add(detail);
                }
                if(selstatus !=null){                                       
                   detail = new Search_Detail__c(Name =ASSIGNSTATUS , Search_Value__c = string.valueof(selstatus), File__c = newSearch.Id, Search_Detail_Unique_Key__c='status'+selstatus+ newSearch.Id);
                   details.add(detail);                        
                } 
                if(selassigntype !=null){                                       
                   detail = new Search_Detail__c(Name =ASSIGNTYPE , Search_Value__c = string.valueof(selassigntype), File__c = newSearch.Id, Search_Detail_Unique_Key__c='assigntype'+selassigntype+ newSearch.Id);
                   details.add(detail);                        
                }
                //date              
                if(selDatType !=null){                                      
                   detail = new Search_Detail__c(Name =DATETYPE  , Search_Value__c = string.valueof(selDatType), File__c = newSearch.Id, Search_Detail_Unique_Key__c='Date'+selassigntype+ newSearch.Id);
                   details.add(detail); 
                   if (datefilter != null){
                     if(string.valueof(datefilter) !='')
                     detail = new Search_Detail__c(Name = FROMDATE , Search_Value__c = string.valueof(datefilter),File__c = newSearch.Id, Search_Detail_Unique_Key__c='startFromDate' +string.valueof(datefilter)+ newSearch.Id);
                     details.add(detail);  
                   }
                   if (datefilter1 != null){
                    if(string.valueof(datefilter1) !='')
                    detail = new Search_Detail__c(Name = TODATE , Search_Value__c = string.valueof(datefilter1),File__c = newSearch.Id, Search_Detail_Unique_Key__c='startToDate' +string.valueof(datefilter1)+ newSearch.Id);
                    details.add(detail);                   
                   }                   
                }
                
                //BusinessUnit
                if(selectedBusinessUnit.size() == 1 && selectedBusinessUnit[0] == '') {                       
                    system.debug('selectedBusinessUnit @@@@'+selectedBusinessUnit);
                    system.debug('selectedBusinessUnit @@@@'+selectedBusinessUnit.size());
                    selectedBusinessUnit = null;
                }
                if(selectedBusinessUnit != null){
                    for(i = 0; i < selectedBusinessUnit.size(); i++){                        
                        detail = new Search_Detail__c(Name = PRABU , Search_Value__c = string.valueof(selectedBusinessUnit[i]), File__c = newSearch.Id, Search_Detail_Unique_Key__c='BU' +string.valueof(selectedBusinessUnit[i])+ newSearch.Id);
                         details.add(detail);
                    }
                }
                
                if(selectedRoles.size() == 1 && selectedRoles[0] == '')
                        selectedRoles = null;
                if(selectedRoles != null){
                    for(i = 0; i < selectedRoles.size(); i++){                        
                        detail = new Search_Detail__c(Name = ROLE , Search_Value__c = string.valueof(selectedRoles[i]), File__c = newSearch.Id, Search_Detail_Unique_Key__c='ROLE' +string.valueof(selectedRoles[i])+ newSearch.Id);
                         details.add(detail);
                    }
                }
               
                if(isLead){
                  detail = new Search_Detail__c(Name = LEAD, Search_Value__c = string.valueof(isLead), File__c = newSearch.Id, Search_Detail_Unique_Key__c= i+LEAD+string.valueof(isLead)+ newSearch.Id);
                  details.add(detail);
                }
                if(isPssv){
                  detail = new Search_Detail__c(Name = PSSV, Search_Value__c = string.valueof(isPssv), File__c = newSearch.Id, Search_Detail_Unique_Key__c= i+PSSV+string.valueof(isPssv)+ newSearch.Id);
                  details.add(detail);
                }
                if(isUnblinded){                
                  detail = new Search_Detail__c(Name = UNBLINDED, Search_Value__c = string.valueof(isUnblinded), File__c = newSearch.Id, Search_Detail_Unique_Key__c= i+UNBLINDED+string.valueof(isUnblinded)+ newSearch.Id);
                  details.add(detail);
                }               
                if(selectedCountry.size() == 1 && selectedCountry[0]=='')
                    selectedCountry=null;
                if(selectedCountry !=null)                                          
                for(i = 0; i < selectedCountry.size(); i++){                            
                        detail = new Search_Detail__c(Name = COUNTRY, Search_Value__c = string.valueof(selectedCountry[i]), File__c = newSearch.Id, Search_Detail_Unique_Key__c= i+string.valueof(selectedCountry[i])+ newSearch.Id);
                        details.add(detail);
                    
                }
                       
                    
                if(selectedCodes.size()==1 && selectedCodes[0]=='')
                    selectedCodes=null;
                if(selectedCodes != null)
                for(i = 0; i < selectedCodes.size(); i++){                                                   
                        detail = new Search_Detail__c(Name = BU_CODE , Search_Value__c = string.valueof(selectedCodes[i]), File__c = newSearch.Id, Search_Detail_Unique_Key__c= i+string.valueof(selectedCodes[i])+ newSearch.Id);
                        details.add(detail);
                    
                }
                
                if(selectedDir != null && selectedDate != null){                                                    
                    detail = new Search_Detail__c(Name = DIRECTION , Search_Value__c = string.valueof(selectedDir), File__c = newSearch.Id, Search_Detail_Unique_Key__c= string.valueof(selectedDir)+ newSearch.Id);
                    details.add(detail); 
                    detail = new Search_Detail__c(Name = COMPVALUE , Search_Value__c = string.valueof(selectedDate), File__c = newSearch.Id, Search_Detail_Unique_Key__c= string.valueof(selectedDate)+ newSearch.Id);
                    details.add(detail);
                    if (isActive) {     
                        detail = new Search_Detail__c(Name = ACTIVEPROJECT , Search_Value__c = string.valueof(isActive), File__c = newSearch.Id, Search_Detail_Unique_Key__c=  ACTIVEPROJECT + string.valueof(isActive)+ newSearch.Id);
                        details.add(detail);
                    }                  
                }                
               
                system.debug('---------details---------'+details);
                insert details;
            }
        }   
        return null;
    }
}