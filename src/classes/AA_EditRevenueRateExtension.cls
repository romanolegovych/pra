public class AA_EditRevenueRateExtension{

    // Instance fields
    public String searchTerm {get; set;}
    private final List<BUF_Code__c> bufcodes;
    private final List<Currency__c> currencies;
    public String selectedBufCode{get;set;}
    public String selectedCurrency{get;set;}
    public String yearValue{get;set;}
    public Decimal initialRateValue{get;set;}
    public Boolean isActive{get;set;}
    public String createdBy{get;set;}
    public String createdDate{get;set;}
    public String modifiedBy{get;set;}
    public String modifiedDate{get;set;}
    public String functionId{get;set;}
    public Revenue_Allocation_Rate__c rateDetails{get;set;}
    
    private Revenue_Allocation_Rate__c rate;
    private Boolean error = false;

    
    public AA_EditRevenueRateExtension(ApexPages.StandardController controller) {
        bufcodes = AA_CostRateService.getBufCodes();
        currencies = AA_CostRateService.getCurrencies();
        functionId = System.currentPagereference().getParameters().get('functionId');
        rateDetails = AA_RevenueAllocationService.getRateDetails(functionId).get(0);
        selectedBufCode = rateDetails.Function__c;
        yearValue = rateDetails.Year__c;
        initialRateValue = rateDetails.Rate__c;
        selectedCurrency = rateDetails.Currency__c;
        isActive = rateDetails.isActive__c;
        searchTerm = AA_CostRateService.getBufCode(selectedBufCode ).Name ;
    }

    // JS Remoting action called when searching for a BUF Code name
    @RemoteAction
    public static List<BUF_Code__c > searchCode(String searchTerm) {
        List<BUF_Code__c > codes = AA_CostRateService.getBufCodesByName(searchTerm);
        return codes;
    }
    
    /* Populate BUF Codes in Select List */
    public List<SelectOption> getbufselectoption() {
        List<SelectOption> bufselectoption= new List<SelectOption>();
        for(BUF_Code__c entry : bufcodes){
            bufselectoption.add(new SelectOption(entry.Id, entry.Name));
        }
        return bufselectoption;
    }
    
    /* Populate Currency in Select List     */
    public List<SelectOption> getcurrencyselectoption() {
        List<SelectOption> currencyselectoption= new List<SelectOption>();
        for(Currency__c entry : currencies){
            currencyselectoption.add(new SelectOption(entry.Id, entry.Name));
        }
        return currencyselectoption;
    }

    public PageReference Save() {
            /* Catch invalid Year */
            try{
            if(Integer.valueOf(yearValue) < 1999 || Integer.valueOf(yearValue) > Integer.valueOf(Date.Today().Year() + 20)) {
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Year : '+ yearValue + '. Must be between 1999 and ' + String.valueOf(Date.Today().Year() + 20));
                ApexPages.addMessage(errormsg);
                return null;
            }
            }catch(Exception e){
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Year : '+ yearValue);
                ApexPages.addMessage(errormsg);
                return null;
            }
            
            /* Catch invalid Rate*/
            try{
                Integer d = Integer.valueOf(initialRateValue);
            }catch(Exception e){
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Rate : '+ initialRateValue);
                ApexPages.addMessage(errormsg);
            }
            
            if(!error){
                    BUF_Code__c tempCode = AA_CostRateService.getBufCode(selectedBufCode);
                    createNewRecords();
                    PageReference pageRef = Page.AA_Revenue_Allocation_Rate;
                    return pageRef;
         }else
             return null;
    }

   public PageReference back() {
         PageReference pageRef = Page.AA_Revenue_Allocation_Rate;
         return pageRef;
    }
    public PageReference DeActivate() {
         rate =  new Revenue_Allocation_Rate__c ();
         rate.Id = functionId;
         rate.Function__c = selectedBufCode;
         rate.Rate__c = initialRateValue;
         rate.Year__c = yearValue;
         rate.Currency__c = selectedCurrency;
         rate.IsActive__c = false;
         update rate;
         PageReference pageRef = Page.AA_Revenue_Allocation_Rate;
         return pageRef;
    }
    public void createNewRecords(){
         rate =  new Revenue_Allocation_Rate__c ();
         rate.Id = functionId;
         rate.Function__c = selectedBufCode;
         rate.Rate__c = initialRateValue;
         rate.Year__c = yearValue;
         rate.Currency__c = selectedCurrency;
         rate.IsActive__c = isActive;
         update rate;
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,' Revenue Allocation Rate Generated Succesfully'));
    }
    
}