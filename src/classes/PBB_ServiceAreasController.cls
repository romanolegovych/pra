/** 
@Author Bhargav Devaram
@Date 2015
@Description this class is inserting the Service Areas for a service model.
*/
public class PBB_ServiceAreasController {    
    
    public  ID  saID{get;set;}                //to get the Service Area ID
    public  Service_Area__c sa{get;set;}      //to get the Service Area Sobject for edit or new
    public  string[] SelectBSs{get;set;}      //to get the selected Business Segments
    
    //Standard Constructor
    public PBB_ServiceAreasController(ApexPages.StandardController controller) {   
    	     
        SelectBSs = new list<String>(); 
        
        //check if the SA is for edit
        sa= (Service_Area__c )controller.getRecord();
        
        //check if the SA is for edit
        saID = ApexPages.currentPage().getParameters().get('id');
        string clone =ApexPages.currentPage().getParameters().get('clone');
        System.debug('--clone --'+clone );
        if(saID !=null){
            sa  = PBB_DataAccessor.getServiceAreaByID(saID);  
            if(clone !=null)
                sa=sa.clone(false, true);          
        }       System.debug('--sa--'+sa );
    } 
       
    
    /** 
@Author Bhargav Devaram
@Date 2015
@Description get the types of service tasks
*/
    public List<selectOption> getBusinessSegmentTypes(){
        List<SelectOption> options = new List<SelectOption>(); 
        for(PRA_Business_Unit__c bs:PBB_DataAccessor.getPRABusinessUnit()){
            system.debug('-bs.name--'+bs.name);
            options.add(new SelectOption(bs.name,bs.Business_Unit_Code__c+'-'+bs.name));            
        }  
        return options;
    }
    
    
    /** 
@Author Bhargav Devaram
@Date 2015
@Description save the service Area for service model
*/
    public PageReference saveSA () {  
    	
        PageReference pageRef =null;   
        try{    
        	          
            //check if the Business segment is selected 
                //convert the list of selected Business Segments to string
                String BS='';             
                for(String b:SelectBSs){
                    BS+=b+';';
                    System.debug('--bs--'+bs);
                }
                //save as a string to the multipicklist           
                upsert sa;
                pageRef=new PageReference('/'+sa.id);
                pageRef.setRedirect(true);  
                System.debug('--pageRef--'+pageRef);                                         
            
        }
        catch (DMLException e) {
            ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0) );
            ApexPages.addMessage(msg);        
        }
        return pageRef;        
    }    
}