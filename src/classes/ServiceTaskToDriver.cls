public with sharing class ServiceTaskToDriver extends ServiceBase {

    public List<Service_Task_To_Drivers__c> serviceTaskToDriversList {
    	get{
    		return (List<Service_Task_To_Drivers__c>) serviceRecords;
    	}
    	set{
    		serviceRecords = value;
    	}
	}
    public Service_Task_To_Drivers__c serviceTaskToDrivers {
    	get{
    		return (Service_Task_To_Drivers__c)serviceRecord;
    	}
    	set{
    		serviceRecord = value;
    	}
    }
    public String selectedCountries {get;set;}
    public List<String> countries;
    public Boolean isReadOnly {
        get {
            if(isReadOnly == null) {
                isReadOnly = false;
                if(serviceTaskToDrivers != null && (serviceTaskToDrivers.Status__c == 'Approved' || serviceTaskToDrivers.Status__c == 'Retired')) {
                    isReadOnly = true;
                }
            }
            return isReadOnly;
        }
        set;
    }
    
    public List<selectOption> getParametersObjects(){
    	return new PBB_Constants().getParameterList();
    }
    public String selectedParametersObject { get; set; }
    public String selectedObjectFiled { get; set; }
    
    //public String serviceTaskId;
    public Service_Task__c serviceTask;

    Public List<string> leftselectedCountries{get;set;}
    Public List<string> rightselectedCountries{get;set;}

    @TestVisible
    private Set<string> leftCountryValues = new Set<string>();
    @TestVisible
    private Set<string> rightCountryValues = new Set<string>();

    public List<SelectOption> getObjectFields() {
        List<SelectOption> objectFieldOptions = new List<SelectOption>();
        if(!String.isBlank(selectedParametersObject)) {
            List<Schema.DescribeSObjectResult> describeSobjectsResult = Schema.describeSObjects(new List<String>{selectedParametersObject});
            if(describeSobjectsResult != null) {
                Map<String, Schema.SObjectField> allFields = describeSobjectsResult[0].fields.getMap();
                for(String fieldName : allFields.keySet()) {
                    objectFieldOptions.add(new SelectOption(fieldName, allFields.get(fieldName).getDescribe().getLabel()));
                }
            }
        }
        System.debug('objectFieldOptions: ' + objectFieldOptions);
        return objectFieldOptions;
    }

     
    public PageReference selectclick(){
        rightselectedCountries.clear();
        for(String s : leftselectedCountries){
            leftCountryvalues.remove(s);
            rightCountryvalues.add(s);
        }
        return null;
    }
     
    public PageReference unselectclick(){
        leftselectedCountries.clear();
        for(String s : rightselectedCountries){
            rightCountryvalues.remove(s);
            leftCountryvalues.add(s);
        }
        return null;
    }
 
    public List<SelectOption> getunSelectedValues(){
        List<SelectOption> options = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(leftCountryvalues);
        tempList.sort();
        for(string s : tempList)
            options.add(new SelectOption(s,s));
        return options;
    }
 
    public List<SelectOption> getSelectedValues(){
        List<SelectOption> options1 = new List<SelectOption>();
        List<string> tempList = new List<String>();
        tempList.addAll(rightCountryvalues);
        tempList.sort();
        for(String s : tempList)
            options1.add(new SelectOption(s,s));
        return options1;
    }


        
    public ServiceTaskToDriver(String parentServiceId) {
    	super(parentServiceId);
    }
    
    protected override String getServiceWord(){
    	return 'TaskToDrivers';
    }
    
    public override void createService() {
        if(isValidService()) {
            prepData();
            saveService();
        }
        refreshServices();
    }

	public override void refreshServices(){
        serviceTaskToDriversList = ServiceModelServices.getServiceTaskToDriversListByServiceTaskId(parentServiceId);
        serviceTask = new Service_Task__c();
        if(parentServiceId != null) {
            serviceTask = ServiceModelServices.getServiceTaskById((Id)parentServiceId);
        }
	}
    
    public override void getDetailService() {
    }
    
    public override void preparationCreateOrEditService() {
        isReadOnly = null;
        getCountries();
        refreshServices();
        if(String.isBlank(getEditServiceId())) {
            serviceTaskToDrivers = new Service_Task_To_Drivers__c();
            serviceTaskToDrivers.Service_Task__c = parentServiceId;
            serviceTaskToDrivers.Times_Frequency__c = 1;
            serviceTaskToDrivers.Per_Frequency__c = 1;
            serviceTaskToDrivers.Frequency__c = 'Study';
            serviceTaskToDrivers.Drivers__c = '';

            leftselectedCountries = new List<String>();
            rightselectedCountries = new List<String>();
            leftCountryValues.addAll(countries);
        } else {
            Map<Id, Service_Task_To_Drivers__c> serviceTaskToDriversMap = new Map<Id, Service_Task_To_Drivers__c>(serviceTaskToDriversList);
            serviceTaskToDrivers = serviceTaskToDriversMap.get(getEditServiceId());
            leftselectedCountries = new List<String>();
            rightselectedCountries = new List<String>();


            if( serviceTask.Global__c &&
                serviceTaskToDrivers.Drivers__c == 'Countries' && 
                !String.isBlank(serviceTaskToDrivers.Countries_Option__c) && 
                !String.isBlank(serviceTaskToDrivers.Countries__c) &&
                !'All'.equalsIgnoreCase(serviceTaskToDrivers.Countries_Option__c)
            ) {
                rightCountryValues = new Set<String>();
                leftCountryValues = new Set<String>();
                if('Include'.equalsIgnoreCase(serviceTaskToDrivers.Countries_Option__c)) {
                    for(String country : serviceTaskToDrivers.Countries__c.split(';')) {
                        rightCountryValues.add(country);
                    }
                    for(String country : countries) {
                        if(!serviceTaskToDrivers.Countries__c.containsIgnoreCase(country)) {
                            leftCountryValues.add(country);
                        }
                    }
                } else if('Exclude'.equalsIgnoreCase(serviceTaskToDrivers.Countries_Option__c) ) {
                    for(String country : serviceTaskToDrivers.Countries__c.split(';')) {
                        leftCountryValues.add(country);
                    }
                    for(String country : countries) {
                        if(!serviceTaskToDrivers.Countries__c.containsIgnoreCase(country)) {
                            rightCountryValues.add(country);
                        }
                    }
                }
            }
        }
    }
    
    public override void removeService() {
        try {
            Map<Id, Service_Task_To_Drivers__c> serviceTaskToDriversMap = new Map<Id, Service_Task_To_Drivers__c>(serviceTaskToDriversList);
            serviceTaskToDrivers = serviceTaskToDriversMap .get(getEditServiceId());
            delete serviceTaskToDrivers;
            refreshServices();
        } catch(Exception e) {
            showErrorMessage(e);
        }
    }
    
    private void prepData() {
        List<string> tempList = new List<String>();
        if('All'.equalsIgnoreCase(serviceTaskToDrivers.Countries_Option__c) ) {
            serviceTaskToDrivers.Countries__c = String.join(countries, ';');
        } else if('Include'.equalsIgnoreCase(serviceTaskToDrivers.Countries_Option__c) ) {
            tempList.addAll(rightCountryValues);
            serviceTaskToDrivers.Countries__c = String.join(tempList, ';');
        } else if('Exclude'.equalsIgnoreCase(serviceTaskToDrivers.Countries_Option__c) ) {
            tempList.addAll(leftCountryValues);
            serviceTaskToDrivers.Countries__c = String.join(tempList, ';');
       }
    }

    /*private void saveServiceTaskToDrivers() {
        isValid = false;
        try {
            upsert serviceTaskToDrivers;
            isValid = true;
        } catch(Exception e) {
            isValid = false;
            showErrorMessage(e);
        }
    }*/
    
    protected override Boolean isValidService() {
        isValid = false;
        Boolean isValidServiceTaskToDrivers = true;

        if(!String.isBlank(serviceTaskToDrivers.Drivers__c)) {
            if(serviceTaskToDrivers.Drivers__c.equalsIgnoreCase('Formula')) {
                if(String.isBlank(serviceTaskToDrivers.Formula_Name__c)) {
                    isValidServiceTaskToDrivers = false;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Formula Name: You must enter a value'));
                }
            } else if(serviceTaskToDrivers.Drivers__c.equalsIgnoreCase('Parameter')) {
                if(String.isBlank(serviceTaskToDrivers.Status__c)) {
                    isValidServiceTaskToDrivers = false;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'If Drivers equals "Parameter" Status is required'));
                }
            } else if(serviceTaskToDrivers.Drivers__c.equalsIgnoreCase('Countries')) {
                if(!serviceTask.Global__c) {
                    isValidServiceTaskToDrivers = false;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Country picklist value is available only if parent Task SObject global'));
                }
            }
        }
        return isValidServiceTaskToDrivers;
    } 

    public List<SelectOption> getDrivers() {
      List<SelectOption> options = new List<SelectOption>();
            
       Schema.DescribeFieldResult fieldResult =Service_Task_To_Drivers__c.Drivers__c.getDescribe();
       List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
       options.add(new SelectOption('', '--None--'));
       for( Schema.PicklistEntry f : ple) {
          Boolean isDisabled = false;
          if(!serviceTask.Global__c && f.getValue() == 'Countries') {
            isDisabled = true;
          }
          options.add(new SelectOption( f.getValue(), f.getLabel(), isDisabled));
       }
       return options;
    }

    public List<SelectOption> getCountries() {
      List<SelectOption> options = new List<SelectOption>();
      countries = new List<String>();
      List<Country__c> CountryList = ServiceModelServices.getCountry();
       for(Country__c country : CountryList) {
           options.add(new SelectOption(country.Name, country.Name));
           countries.add(country.Name);
       }
       options.sort();
       return options;
    }
    
}