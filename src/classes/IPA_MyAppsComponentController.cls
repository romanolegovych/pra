public with sharing class IPA_MyAppsComponentController {
	//Member Var
	public list<AppMenuItem> myApps {get; private set;}
	private static ID userId;
    private static ID profileId;
    private List<PermissionSetAssignment> myPermSetAsgmnts = new List<PermissionSetAssignment>();
    private List<PermissionSet> profilePermSetAsgmnt = new List<PermissionSet>();
    private Set<ID> permSetIds = new Set<ID>();
    private List<SetupEntityAccess> appList = new List<SetupEntityAccess>();
    private Set<ID> appIds = new Set<ID>();
	//Constructor
    public IPA_MyAppsComponentController() {  
		USERID = UserInfo.getUserID();
        myPermSetAsgmnts = [SELECT PermissionSetId
                      		FROM PermissionSetAssignment
                      		WHERE AssigneeId = : userId];
        PROFILEID = UserInfo.getProfileId();
        if(!myPermSetAsgmnts.isEmpty()){
            for(PermissionSetAssignment permSetAsgmnt : myPermSetAsgmnts){
            	permSetIds.add(permSetAsgmnt.PermissionSetId);
            }
        }
        profilePermSetAsgmnt = [Select Id
                                FROM PermissionSet
                                WHERE ProfileId = : PROFILEID];
        if(!profilePermSetAsgmnt.isEmpty()){
            for(PermissionSet permSet : profilePermSetAsgmnt){
            	permSetIds.add(permSet.Id);
            }
        }
        if(!permSetIds.isEmpty()){
        	appList = [SELECT SetupEntityId
    				  FROM SetupEntityAccess
    				  WHERE SetupEntityType in ('TabSet','ConnectedApplication') AND ParentId = : permSetIds];
            if(!appList.isEmpty()){
                for(SetupEntityAccess app : appList){
                	appIds.add(app.SetupEntityId);
                }
            }
        }
        myApps = [SELECT Id, Label, Description, StartUrl, LogoUrl, IconUrl 
                  FROM AppMenuItem
                  WHERE IsDeleted = false AND Id = : appIds
                  ORDER BY SortOrder];     
    }    
}