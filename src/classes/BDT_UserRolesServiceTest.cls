@isTest
public with sharing class BDT_UserRolesServiceTest {

	static User createTestData() {

		// add a user to salesforce
		Profile p = [select id from profile where name='System Administrator'];
		User myUser = new User(alias = 'SuperMan', email='clark.kent@dailymail.com',
			emailencodingkey='UTF-8', firstname='Clark', lastname='Kent', languagelocalekey='en_US',
			localesidkey='en_US', profileid = p.Id,
			timezonesidkey='America/Los_Angeles', username='clark.kent@dailymail.com');
		insert myUser;
		return myUser;
	}


	static testMethod void getBDTusersTest () {
		// no test needed just calls DataAccessor which is already tested
		list<User> dummy = BDT_UserRolesService.getBDTusers();
	}

	static testMethod void createFindBDTUserTest () {
		User myUser = createTestData();
		BDT_UserRoles__c bdtUser;
		
		// test know SalesForce User
		bdtUser = BDT_UserRolesService.createFindBDTUser('Clark Kent');
		system.assertNotEquals(null,bdtUser.Id);
		
		// test unknown SalesForce User
		bdtUser = BDT_UserRolesService.createFindBDTUser('Bruce Wane');
		system.assertEquals(null,bdtUser);
	}
	
	static testMethod void getUserNameTest() {
		// no test needed just calls DataAccessor which is already tested
		string dummy = BDT_UserRolesService.getUserName(null);
	}

	static testMethod void removeRoleFromAllUsersTest() {
		
		BDT_UserRoles__c userRoles = new BDT_UserRoles__c(Name = 'Dummy', RoleNumbers__c = '1:2:3:33:4:5');
		insert userRoles;
		// now remove role 3 from all users
		BDT_UserRolesService.removeRoleFromAllUsers(3);
		BDT_UserRolesService.removeRoleFromAllUsers(1);
		BDT_UserRolesService.removeRoleFromAllUsers(5);
		
		userRoles = [select RoleNumbers__c from BDT_UserRoles__c limit 1];
		// role 1,3,5 should have been removed but not 33
		system.assertEquals('2:33:4',userRoles.RoleNumbers__c);
		
	}
	
	static testMethod void removeAllUsersRolesFromPlenboxTest() {
		BDT_UserRoles__c userRoles = new BDT_UserRoles__c(Name = 'Dummy', RoleNumbers__c = '1:2:3:33:4:5');
		insert userRoles;
		BDT_UserRolesService.removeAllUsersRolesFromPlenbox(userRoles.ID);
		// verify record was removed
		List<BDT_UserRoles__c> ur = [select id from BDT_UserRoles__c];
		system.assertEquals(0,ur.size());
	}
	
	static testMethod void getUsersByRoleTest() {
		User myUser = createTestData();
		
		List<BDT_UserRoles__c> userRoles = new List<BDT_UserRoles__c>();
		userRoles.add(new BDT_UserRoles__c(Name = 'Dummy1', RoleNumbers__c = '1:2:3:4:5'));
		userRoles.add(new BDT_UserRoles__c(Name = myUser.ID, RoleNumbers__c = '33'));
		userRoles.add(new BDT_UserRoles__c(Name = 'Dummy3', RoleNumbers__c = '1:2:3:4:5'));
		insert userRoles;
		
		list<User> a = BDT_UserRolesService.getUsersByRole('3');
		system.assertEquals(1, a.size());
		
	}
	
	static testMethod void createShuttleTest() {
		
		User myUser = createTestData();
		
		List<BDT_Role__c> roles = new list<BDT_Role__c>();
		roles.add(new BDT_Role__c(name='role1',Role_Number__c=1));
		roles.add(new BDT_Role__c(name='role2',Role_Number__c=2));
		roles.add(new BDT_Role__c(name='role3',Role_Number__c=3));
		insert roles;
		
		BDT_UserRoles__c ur = new BDT_UserRoles__c (name = myUser.id, RoleNumbers__c = '1');
		insert ur;
		
		BDT_Utils.SelectOptionShuttle SO = BDT_UserRolesService.createShuttle(myUser.id);
		
		list<String> usedList = so.getUsedKeys();
		system.assertEquals('1',usedList[0]);
		
		list<String> unusedList = so.getUnusedKeys();
		system.assertEquals('2',unusedList[0]);
		system.assertEquals('3',unusedList[1]);
		
	}
	
	static testMethod void saveRolesTest() {
		
		BDT_UserRoles__c ur = new BDT_UserRoles__c (name = 'Dummy', RoleNumbers__c = '1');
		insert ur;
		
		List<SelectOption> soList = new List<SelectOption>();
		soList.add(new SelectOption('2','2'));
		soList.add(new SelectOption('3','3'));
		
		BDT_UserRolesService.saveRoles(ur,soList);
		
		ur = [select RoleNumbers__c from BDT_UserRoles__c limit 1];
		system.assertEquals('2:3',ur.RoleNumbers__c);
	}
	
}