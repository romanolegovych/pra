public with sharing class RM_EmployeeService {
	 public static list<Employee_Details__c> GetEmployeeDetailByEmployeeRIDs(set<string> employeeRIDs){
	 	return RM_EmployeesDataAccessor.GetEmployeeDetailByEmployeeRIDs(employeeRIDs);
	 }
	 public static Employee_Details__c GetEmployeeDetailByEmployee(string employeeRID){
	 	return RM_EmployeesDataAccessor.GetEmployeeDetailByEmployee(employeeRID);
	 }
	 public static Employee_Details__c getEmployeeDetailByEmployeeID(string employeeID){
	 	return RM_EmployeesDataAccessor.getEmployeeDetailByEmployeeID(employeeID);
	 }
	 public static list<Employee_Details__c> getEmployeeDetailByEmployeeEmail(string email){
	 	return RM_EmployeesDataAccessor.getEmployeeDetailByEmployeeEmail(email);
	 }
	 public static list<Employee_Details__c> GetEmployeeDetailByEmployeeSupervisor(string supervisorID){
	 	return RM_EmployeesDataAccessor.GetEmployeeDetailByEmployeeSupervisor(supervisorID);
	 }
	 
	 public static string GetEmployeeRIDByEmployeeSupervisorAndEmpName(string supervisorID, string emplLastName, string emplFirstName){
	 	return RM_EmployeesDataAccessor.GetEmployeeRIDByEmployeeSupervisorAndEmpName(supervisorID,emplLastName,emplFirstName); 
	 }
	 public static List<SelectOption> getResourceByManager(string managerName){
        
         transient List<SelectOption> options = new List<SelectOption>();
         Employee_Details__c supervisor = RM_EmployeesDataAccessor.GetEmployeeDetailByEmployee(managerName);
		system.debug('---------supervisor---------' + supervisor.name);
		
		List<Employee_Details__c> lstEmpl = RM_EmployeesDataAccessor.GetEmployeeDetailByEmployeeSupervisor(supervisor.name);
		system.debug('---------lstEmpl---------' + lstEmpl);
		for(Employee_Details__c e : lstEmpl){ 
			options.add(new SelectOption(e.id,e.full_name__c));
		}
        return options;
    }   
    
    public static List<Employee_Details__c> GetEmployeeDetailByEmployeeFullName(list<string> employeeFullName, set<string> stManagedBU){
    	
		return RM_EmployeesDataAccessor.GetEmployeeDetailByEmployeeFullName(employeeFullName, stManagedBU);
		
		
    }
     public static list<Employee_Details__c> GetEmployeeListBySetEmployeeRID(Set<ID> employeeIDs){
     	return RM_EmployeesDataAccessor.GetEmployeeListBySetEmployeeRID(employeeIDs);
     }
    public static set<ID> GetEmployeeSetByProjectYearMonth(string projectRID, Integer startMonth, Integer endMonth){
    	return RM_EmployeesDataAccessor.GetEmployeeSetByProjectYearMonth(projectRID, startMonth, endMonth);
    }
    
    /*-- Work_history---*/
    public static list<WFM_EE_Work_History_Summary__c> GetWorkHistorySummarybyEmployeeRID(string employeeRID){
    	return RM_EmployeesDataAccessor.GetWorkHistorySummarybyEmployeeRID(employeeRID);
    }
   // public static List<WFM_EE_Work_History__c> GetActiveAssignmentWorkHist(string eID, list<string> projRID, Integer startMonth, Integer endMonth){
   // 	return RM_EmployeesDataAccessor.GetActiveAssignmentWorkHist(eID, projRID, startMonth, endMonth);
   // }
   // public static List<WFM_EE_Work_History__c> GetWorkHistByEmployee(string eID, Integer startMonth, Integer endMonth){
   //		return RM_EmployeesDataAccessor.GetWorkHistByEmployee(eID, startMonth, endMonth);
   // }
    public static list<AggregateResult> GetEmployeeWorkHistrySummarybyEmployeesProject(Set<ID> employeeIDs, string projectRID){
    	return RM_EmployeesDataAccessor.GetEmployeeWorkHistrySummarybyEmployeesProject(employeeIDs, projectRID);
    }
    public static list<AggregateResult> GetSumEmployeeWorkHistByEmployeeProjectRIDsYearMonth(Set<ID> projectRIDs, string employeeRID, Integer startMonth, Integer endMonth){
    	return RM_EmployeesDataAccessor.GetSumEmployeeWorkHistByEmployeeProjectRIDsYearMonth(projectRIDs, employeeRID, startMonth, endMonth);
    } 
    public static list<AggregateResult> GetSumEmployeeWorkHistByEmployeesProjectYearMonth(Set<ID> employeeIDs, string projectRID, Integer startMonth, Integer endMonth){
    	return RM_EmployeesDataAccessor.GetSumEmployeeWorkHistByEmployeesProjectYearMonth(employeeIDs, projectRID, startMonth, endMonth);
    }
    public static list<AggregateResult> GetEmployeeWorkHistrySummarybyEmployeeProjectIDSet(Set<ID> projectIDs, string EmplRID){
    	return RM_EmployeesDataAccessor.GetEmployeeWorkHistrySummarybyEmployeeProjectIDSet(projectIDs, EmplRID);
    }
    public static List<AggregateResult> GetPostedFTEbyEmployeeRIDMonth(string employeeRID, string yMonth){
   		return RM_EmployeesDataAccessor.GetPostedFTEbyEmployeeRIDMonth(employeeRID, yMonth );
    }
    public static list<WFM_EE_Work_History_Summary__c> GetWorkHistorybyProjectRID(string projectRID){
    	return RM_EmployeesDataAccessor.GetWorkHistorybyProjectRID(projectRID); 
    }
    public static List<AggregateResult> GetActuralWorkHrByProjectIDCountry(string projectRID, string countryName, string strBU, Integer startMonth, Integer endMonth){
    	return RM_EmployeesDataAccessor.GetActuralWorkHrByProjectIDCountry(projectRID, countryName, strBU, startMonth, endMonth);
    }
    public static List<AggregateResult> GetActuralWorkHrByProjectRole(string projectRID, string jobClassDesc, string bu, Integer startMonth, Integer endMonth){
    	return RM_EmployeesDataAccessor.GetActuralWorkHrByProjectRole(projectRID, jobClassDesc, bu, startMonth, endMonth);
    }
    public static List<AggregateResult> GetEmplActuralWorkHrByProjectRoleYMonth(string projectRID, string jobClassDesc, string bu, string yMonth){
     	return RM_EmployeesDataAccessor.GetEmplActuralWorkHrByProjectRoleYMonth(projectRID, jobClassDesc, bu, yMonth);
    }
     public static List<AggregateResult> GetActuralWorkHrCountryByProjectRoleYMonth(string projectRID, string jobClassDesc, string yMonth){
     	return RM_EmployeesDataAccessor.GetActuralWorkHrCountryByProjectRoleYMonth(projectRID, jobClassDesc, yMonth);
     }
    /** experience **/
    public static List<WFM_EE_Therapeutic_Exp__c> GetTherapeuticExpByEmployeeRID(string employeeRID){
    	return RM_EmployeesDataAccessor.GetTherapeuticExpByEmployeeRID(employeeRID);
    }
    public static List<WFM_EE_LANGUAGE__c> GetLanguagebyEmployeeRID(string employeeRID){
    	return RM_EmployeesDataAccessor.GetLanguagebyEmployeeRID(employeeRID);
   
    }
    public static List<WFM_EE_CERTIFICATION__c> GetCertificatebyEmployeeRID(string employeeRID){
    	return RM_EmployeesDataAccessor.GetCertificatebyEmployeeRID(employeeRID);
    }
    public static List<WFM_EE_PHASE_EXPERIENCE__c> GetPhaseExpbyEmployeeRID(string employeeRID){
     	return RM_EmployeesDataAccessor.GetPhaseExpbyEmployeeRID(employeeRID);
    }
    public static List<WFM_EE_Education__c> GetEducationbyEmployeeRID(string employeeRID){
    	return RM_EmployeesDataAccessor.GetEducationbyEmployeeRID(employeeRID);
    }
    public static List<WFM_EE_SYSTEM_EXPERIENCE__c>GetSysExpbyEmployeeRID(string employeeRID){
    	return RM_EmployeesDataAccessor.GetSysExpbyEmployeeRID(employeeRID);
    }
}