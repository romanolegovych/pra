@isTest
public with sharing class BDT_RoleServiceTest {
	
	static testMethod void BatchTest() {
		
		// create initial roles
		list<BDT_Role__c> myRoles = new list<BDT_Role__c>();
		myRoles.add(new BDT_Role__c(name = 'role1'));
		myRoles.add(new BDT_Role__c(name = 'role2'));
		myRoles.add(new BDT_Role__c(name = 'role3'));
		
		BDT_RoleService.saveRoles(myRoles);
		
		
		// create wrapper
		list<BDT_RoleService.displayRoleSelector> result = BDT_RoleService.getDisplayRoles();
		
		// test wrapper contents
		system.assertEquals('role1',result[0].role.name);
		system.assertEquals('role2',result[1].role.name);
		system.assertEquals('role3',result[2].role.name);
		system.assertEquals(1,result[0].role.Role_Number__c);
		system.assertEquals(2,result[1].role.Role_Number__c);
		system.assertEquals(3,result[2].role.Role_Number__c);
		
		Decimal roleNumber = 1;
		BDT_RoleService.deleteRole(roleNumber);
		BDT_Role__c role = BDT_RoleService.getRole(result[0].role.id);
		
		system.assertEquals(null,role);
		
	}

}