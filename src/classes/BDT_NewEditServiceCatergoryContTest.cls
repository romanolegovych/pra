@isTest
private class BDT_NewEditServiceCatergoryContTest {

    static testMethod void myUnitTest() {
        ServiceCategory__c sc = new ServiceCategory__c(name = 'Test',code__c = '001');
        upsert sc;
        
        BDT_NewEditServiceCatergoryController a = new BDT_NewEditServiceCatergoryController();
        a.serviceCategoryID = sc.id;
        a.retrieveSC();
        System.assertEquals(false,a.serviceCategory==null);
        
        PageReference s= a.save();
        PageReference d= a.serviceCategoryDelete();
        PageReference c= a.cancel();
        System.assertEquals(true, a.serviceCategory.BDTDeleted__c);     
        
        Integer messageCount = 0;
        // save with invali characters
        a = null;
        a = new BDT_NewEditServiceCatergoryController();
        a.serviceCategory.Code__c = 'a1.bc2';
        a.save();
        System.assert(ApexPages.getMessages().size() > messageCount);
        messageCount = ApexPages.getMessages().size();
       
        // save with more than 3 digits to identify a category
        a = null;
        a = new BDT_NewEditServiceCatergoryController();
        a.serviceCategory.Code__c = '1.2000.2';
        a.save();
        System.assert(ApexPages.getMessages().size() > messageCount);
        messageCount = ApexPages.getMessages().size();
        
        // save with more than 3 levels
        a = null;
        a = new BDT_NewEditServiceCatergoryController();
        a.serviceCategory.Code__c = '1.1.1.1';
        a.save();
        System.assert(ApexPages.getMessages().size() > messageCount);
        messageCount = ApexPages.getMessages().size();
        
        // save with the parent category having services
        ServiceCategory__c scTmp = new ServiceCategory__c(name = 'Test',code__c = '002');
        upsert scTmp;
        Service__c srv = new Service__c(name = 'Test', ServiceCategory__c = scTmp.id);
        upsert srv;
        a = null;
        a = new BDT_NewEditServiceCatergoryController();
        a.serviceCategory.Code__c = '2.1';
        a.save();
        System.assert(ApexPages.getMessages().size() > messageCount);
        messageCount = ApexPages.getMessages().size();
        
        // save with no existing parent
        a = null;
        a = new BDT_NewEditServiceCatergoryController();
        a.serviceCategory.Code__c = '3.1';
        a.save();
        System.assert(ApexPages.getMessages().size() > messageCount);
        messageCount = ApexPages.getMessages().size();  
                               
        // save new service category - Correct
        a = null;
        a = new BDT_NewEditServiceCatergoryController();
        a.serviceCategory.Code__c = '3';
        a.save();
        System.assert(ApexPages.getMessages().size() == messageCount);
        messageCount = ApexPages.getMessages().size();
              
        
        
    }
}