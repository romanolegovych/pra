public with sharing class RM_ViewResourceControllerExt {
    public list<string> monthLabel{get;private set;}  
    private final EMPLOYEE_DETAILS__c emp;
    private final list<WFM_EE_Therapeutic_Exp__c> theraExp;
    private final list<WFM_EE_LANGUAGE__c> language;
    private final list<WFM_EE_CERTIFICATION__c> certification;
    private final list<WFM_EE_PHASE_EXPERIENCE__c> phaseExp;
    private final list<WFM_EE_Education__c> educations;
    private final list<WFM_EE_SYSTEM_EXPERIENCE__c> sysExp;
   
   
    private final integer startMonth = -4;
    private final integer endMonth = -1;
    public EMPLOYEE_DETAILS__c manager{get;set;}
    public Boolean bFlag{get; private set;}
    public string displayWidth{get; private set;}
   // public string rmAddComment{get; set;}   
  //  public set<string> stManagedBU{get;set;}
   // public boolean bViewNote{get; set;}
   // public Employee_Details__c rmAddNote{get; private set;}
    public List<string> monthLab{get;private set;}
    public boolean bVisHr{get;set;}
    public List<string> availableHr{get;private set;}
    public List<string> availableFTE{get;private set;}
    public string displayType{get;private set;}
    
    public RM_ViewResourceControllerExt(ApexPages.StandardController stdController){
        string flag = ApexPages.currentPage().getParameters().get('flag');
         system.debug('-------------flag----------------' + flag); 
        bFlag = false;
        displayWidth='80%';
        if (flag != null){
            if (flag == '1'){
                bFlag = true;
                displayWidth='100%';
            }           
        }
        monthLabel = RM_Tools.GetMonthsList(startMonth, endMonth);
        this.emp = (EMPLOYEE_DETAILS__c)stdController.getRecord();
     //   rmAddNote= RM_EmployeeService.GetEmployeeDetailByEmployee(emp.Id);
	    theraExp = RM_EmployeeService.GetTherapeuticExpByEmployeeRID(emp.id);
        language = RM_EmployeeService.GetLanguagebyEmployeeRID(emp.id);
        certification = RM_EmployeeService.GetCertificatebyEmployeeRID(emp.id);
        phaseExp = RM_EmployeeService.GetPhaseExpbyEmployeeRID(emp.id);
        educations = RM_EmployeeService.GetEducationbyEmployeeRID(emp.id);
        sysExp = RM_EmployeeService.GetSysExpbyEmployeeRID(emp.id);
     
      //  system.debug('-------------emp----------------' + this.emp.Supervisor_ID__c); 
        
      //  manager = getMangerEmail(emp.Supervisor_ID__c);
        
        
        //need control permission
       // string groupName = RM_DataAccessor.GetConstant('RMAdminGroupName');  
      /*  Boolean bRMAdmin = RM_OtherService.IsRMAdmin(UserInfo.getUserId());
       
        stManagedBU = RM_OtherService.GetUserGroupBU(UserInfo.getUserId()); 
 
        bViewNote = false;
        if (stManagedBU!= null && stManagedBU.size() != 0){
            if (stManagedBU.contains(emp.Business_Unit_Desc__c))
                bViewNote = true;           
        }
        else{
            if ( bRMAdmin )       
                    bViewNote = true;           
        }
        system.debug('---------bViewNote---------'+bViewNote  ); 
        if (bViewNote)
        {           
            rmAddComment = rmAddNote.RM_Comment__c;
        }*/
    }
   
    public List<WFM_EE_Therapeutic_Exp__c> getTherapeuticExp() {
        
         return theraExp;         
    }
    public List<WFM_EE_LANGUAGE__c> getLanguage() {
         return language;
    }   
    
    public List<WFM_EE_CERTIFICATION__c> getCertification() {
         return certification;
    }
     
   
    public List<WFM_EE_PHASE_EXPERIENCE__c> getPhaseExp() {
         return phaseExp;
    }
    
    
    public List<WFM_EE_Education__c> getEducation() {
         return educations;
    }
    
   
    public List<WFM_EE_SYSTEM_EXPERIENCE__c> getSysExp() {
         return sysExp;
    }
   
    
    /*public PageReference addNote(){
        
        
        rmAddNote.RM_Comment__c = rmAddComment;
            
        update rmAddNote;
        return null; 
    }*/
    /*public list<RM_ViewAssignmentsCompController.SixMonthAssignment> getAssignments(){
        system.debug('---Hr---'+ ApexPages.currentPage().getParameters().get('Hr'));
        RM_ViewAssignmentsCompController rmAssignment = new RM_ViewAssignmentsCompController();
        rmAssignment.recordID = emp.ID;
        rmAssignment.startMonth = -3;
        rmAssignment.endMonth = 5;
        rmAssignment.displayType = 'Project';
        rmAssignment.bEdit = true;
        rmAssignment.pgeName = '/apex/rm_viewResource';
        list<RM_ViewAssignmentsCompController.SixMonthAssignment> assignments = rmAssignment.getActAssignments();
        monthLab = rmAssignment.monthLab;
        
        // system.debug('------rmAssignment.bVisHr------' + rmAssignment.bVisHr);
        bVisHr = Boolean.valueOf(ApexPages.currentPage().getParameters().get('Hr'));
        availableHr = rmAssignment.availableHr;
        availableFTE = rmAssignment.availableFTE;
        displayType = rmAssignment.displayType;
        return assignments;
    }*/
    
    
    
   /* private EMPLOYEE_DETAILS__c getMangerEmail(string supervisorID){
        
        return RM_EmployeeService.GetEmployeeDetailByEmployeeID(supervisorID);
        
    }*/
  
    
}