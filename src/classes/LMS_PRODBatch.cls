global class LMS_PRODBatch implements Database.Batchable<sObject>,Database.AllowsCallouts {
	
	String query;
	Boolean buildEmployees;
	
	global LMS_PRODBatch(String q, Boolean b) {
		query = q;
		buildEmployees = b;
	}
	
	global virtual Database.QueryLocator start(Database.BatchableContext BC){
  		return Database.getQueryLocator(query);
  	}
  	
  	global virtual void execute(Database.BatchableContext BC, List<sObject> scope){
    	List<LMS_Role__c> roles = new List<LMS_Role__c>();
    	List<LMS_Role_Course__c> role_courses = new List<LMS_Role_Course__c>();
    	List<LMS_Role_Employee__c> role_employees = new List<LMS_Role_Employee__c>();
    	for(sObject s : scope) {
    		if(s instanceof LMS_Role__c) {
	    		LMS_Role__c r = (LMS_Role__c)s;
	    		roles.add(r);
    		}
    		if(s instanceof LMS_Role_Course__c) {
	    		LMS_Role_Course__c rc = (LMS_Role_Course__c)s;
	    		role_courses.add(rc);
    		}
    		if(s instanceof LMS_Role_Employee__c) {
	    		LMS_Role_Employee__c rc = (LMS_Role_Employee__c)s;
	    		role_employees.add(rc);
    		}
    	}
    	if(roles.size() > 0 && buildEmployees == false) {
    		LMS_PROD_SCRIPTS.sendRoleData(roles);
    	}
    	if(roles.size() > 0 && buildEmployees == true) {
    		LMS_PROD_SCRIPTS.populateRoleEmployees(roles);
    	}
    	if(role_courses.size() > 0) {
    		LMS_PROD_SCRIPTS.sendRoleCourseMappings(role_courses);
    	}
    	if(role_employees.size() > 0) {
    		LMS_PROD_SCRIPTS.sendRoleEmployeeMappings(role_employees);
    	}
  	}
  	
  	global virtual void finish(Database.BatchableContext BC){
  		
  	}
}