/** Implements the test for the Selector Layer of the object LaboratoryMethod__c
 * @author	Dimitrios Sgourdos
 * @version	24-Oct-2013
 */
@isTest
private class LaboratoryMethodDataAccessorTest {
	
	// Global variables
	private static List<LaboratoryMethod__c>	lmList;
	
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static void init(){
		// Create Laboratory Methods
		lmList = new  List<LaboratoryMethod__c>();
		lmList.add(new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
											Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood',
											Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle'));
		lmList.add(new LaboratoryMethod__c (Name='PRA-NL-LML-0002', AnalyticalTechnique__c='ELISA', AntiCoagulant__c='N/A',
											Department__c='SML', Detection__c='N/A',Location__c='PRA-US', Matrix__c='Breast milk',
											Proprietary__c='N', ShowOnMethodList__c='Y', Species__c='Human'));
		insert lmList;
		
		// Create Laboratory Method Compounds
		List<LaboratoryMethodCompound__c> compoundList = new List<LaboratoryMethodCompound__c>();
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[0], 'midazolam') );
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[0], '4-OH') );
		insert compoundList;
	}
	
	
	/** Test the function getById
	 * @author	Dimitrios Sgourdos
	 * @version	23-Oct-2013
	 */
	static testMethod void getByIdTest() {
		init();
		
		// Create Laboratory Method Co-Medication
		List<LaboratoryMethodComedication__c> comedList = new List<LaboratoryMethodComedication__c>();
		comedList.add( LaboratoryMethodComedicationService.createLaboratoryMethodComedicationInstance(lmList[0], 'assessment A') );
		comedList.add( LaboratoryMethodComedicationService.createLaboratoryMethodComedicationInstance(lmList[0], 'assessment B') );
		comedList.add( LaboratoryMethodComedicationService.createLaboratoryMethodComedicationInstance(lmList[0], 'assessment C') );
		insert comedList;
		
		// Retrieve a method with no compounds and comedications
		LaboratoryMethod__c labMethod = LaboratoryMethodDataAccessor.getById(lmList[1].Id);
		system.assertNotEquals(labMethod, NULL);
		system.assertEquals(labMethod.Id, lmList[1].Id);
		system.assertEquals(labMethod.LaboratoryMethodCompound__r.size(), 0);
		system.assertEquals(labMethod.LaboratoryMethodComedication__r.size(), 0);
		
		// Retrieve a method with 2 compounds and 3 comedications
		labMethod = LaboratoryMethodDataAccessor.getById(lmList[0].Id);
		system.assertNotEquals(labMethod, NULL);
		system.assertEquals(labMethod.Id, lmList[0].Id);
		system.assertEquals(labMethod.LaboratoryMethodCompound__r.size(), 2);
		system.assertEquals(labMethod.LaboratoryMethodComedication__r.size(), 3);
	}
	
	
	/** Test the function getByLikeName
	 * @author	Dimitrios Sgourdos
	 * @version	23-Oct-2013
	 */
	static testMethod void getByLikeNameTest() {
		init();
		
		List<LaboratoryMethod__c> retrievedList = new List<LaboratoryMethod__c>();
		retrievedList = LaboratoryMethodDataAccessor.getByLikeName('NL-LML');
		system.assertEquals(retrievedList.size(), 2);
		
		retrievedList = LaboratoryMethodDataAccessor.getByLikeName('US-SML');
		system.assertEquals(retrievedList.size(), 0);
	}
	
	
	/** Test the functions getBySearchCriteria, createSearchCriteriaWhereClause
	 * @author	Dimitrios Sgourdos
	 * @version	23-Oct-2013
	 */
	static testMethod void searchFunctionsTest() {
		init();
		
		List<LaboratoryMethodCompound__c> searchCompoundList = new List<LaboratoryMethodCompound__c>();
		List<LaboratoryMethod__c> retrievedList = new List<LaboratoryMethod__c>();
		
		// Check with all search criteria
		searchCompoundList.add(new LaboratoryMethodCompound__c(Name='midazolam'));
		searchCompoundList.add(new LaboratoryMethodCompound__c(Name='4-OH'));
		retrievedList = LaboratoryMethodDataAccessor.getBySearchCriteria(lmList[0], searchCompoundList, '10');
		system.assertEquals(retrievedList.size(), 1);
		system.assertEquals(retrievedList[0].Name, 'PRA-NL-LML-0001');
		
		// Check without inserted search compounds
		searchCompoundList = new List<LaboratoryMethodCompound__c>();
		retrievedList = LaboratoryMethodDataAccessor.getBySearchCriteria(lmList[0], searchCompoundList, '10');
		system.assertEquals(retrievedList.size(), 1);
		
		// Check with no-inserted search criteria
		LaboratoryMethod__c searchLabMethod = new LaboratoryMethod__c();
		retrievedList = LaboratoryMethodDataAccessor.getBySearchCriteria(searchLabMethod, searchCompoundList, '10');
		system.assertEquals(retrievedList.size(), 2);
		
		// Check with inserted compounds that are not met in the methods
		searchCompoundList.add(new LaboratoryMethodCompound__c(Name='Test compound does not exist'));
		retrievedList = LaboratoryMethodDataAccessor.getBySearchCriteria(searchLabMethod, searchCompoundList, '10');
		system.assertEquals(retrievedList.size(), 0);
	}
	
	
	/** Test the function getBySetIds
	 * @author	Dimitrios Sgourdos
	 * @version	19-Nov-2013
	 */
	static testMethod void getBySetIdsTest() {
		init();
		
		Set<String> methodsIds = new Set<String>();
		
		List<LaboratoryMethod__c> results = LaboratoryMethodDataAccessor.getBySetIds(methodsIds);
		system.assertEquals(results.size(), 0);
		
		methodsIds.add(lmList[0].Id);
		results = LaboratoryMethodDataAccessor.getBySetIds(methodsIds);
		system.assertEquals(results.size(), 1);
		
		methodsIds.add(lmList[1].Id);
		results = LaboratoryMethodDataAccessor.getBySetIds(methodsIds);
		system.assertEquals(results.size(), 2);
	}
}