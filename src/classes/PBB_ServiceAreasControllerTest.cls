/**
@author Bhargav Devaram
@date 2015
@description this is PBB_CountryServicesController's test class
**/
@isTest
private class PBB_ServiceAreasControllerTest {
    static testMethod void testPBB_ServiceAreasController(){
        test.startTest();       
        PBB_TestUtils tu = new PBB_TestUtils ();
        tu.smm = tu.createServiceModelAttributes();  
        tu.sa = tu.createServiceAreaAttributes();      
        tu.prabu = tu.createPRABusinessUnits();      
        Apexpages.currentPage().getParameters().put('id', tu.sa.id);
        PBB_ServiceAreasController sac = new PBB_ServiceAreasController(new Apexpages.Standardcontroller(tu.sa));
        System.assertEquals(tu.sa.id,sac.saID );
        sac.getBusinessSegmentTypes();
        sac.SelectBSs.add('Strategic Solutions');        
        //sac.saveSA();system.debug('------tu.sa.Business_Segment__c--------------'+tu.sa.Business_Segment__c+sac.SelectBSs);
        sac.sa = PBB_DataAccessor.getServiceAreaByID(tu.sa.id);
        system.debug('------sac.sa--------------'+sac.sa);
        //System.assertEquals(sac.sa.Business_Segment__c,'Product Registration;Strategic Solutions' );        
        sac.SelectBSs = new list<string>();
        sac.saveSA();        
        test.stopTest();
    }
}