global with sharing class PRA_ChangeOrderDisplayModifyController {
         
    // Page variables
    public String id;
    public String selectedRecord{get;set;}
    public String selectedProject{get;set;}
    public Id selectedProjectid{get;set;}
    public boolean status {get;set;} 
    public String ChangeorderNo {get;set;}
    public String ClonechangeOrderName {get;set;} 
    public String ChangeOrderExcelName {get;set;}
    public ID COid{get;set;}
    public List<Change_Order_Item__c> LiWrappers1 {get;set;}
    public List<GridDataTable1> LiWrappers2{get;set;}
    public List<GridDataTable1> LiWrappers3{get;set;}
    public List<GridDataTable1> LiWrappers4{get;set;}
    public List<String> LTDList{get;set;}
    public Change_Order__c po {get;set;}
    //Summary table values  
    public Decimal TCOunits{get;set;}
    public Decimal TUnitcost{get;set;}
    public Decimal Tcost{get;set;}
    public Decimal ATCOunits{get;set;}
    public Decimal ATUnitcost{get;set;}
    public Decimal ATcost{get;set;}
    public Decimal RTCOunits{get;set;}
    public Decimal RTUnitcost{get;set;}
    public Decimal RTcost{get;set;} 
    public Decimal TCV{get;set;} 
    public String selectedChangeOrderId {get;set;}  
    private set<String> executableStatuses {get {return new Set<String>{'Active', 'Failed'};}}
    private static String REFRESH_INFO = 'Select the Refresh button to update Change Order values for the Project'; 
    
    //Constructor
    public PRA_ChangeOrderDisplayModifyController() {
    } 
    
    //Standard constructor 
    public PRA_ChangeOrderDisplayModifyController(Apexpages.Standardcontroller stdController) {
        Change_Order__c co = (Change_Order__c)stdController.getRecord();
        if(co.Is_Project_Updated__c) {
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, REFRESH_INFO));
        }  
        this.id = ApexPages.currentPage().getParameters().get('id');
        COid = (id);
        SearchforCO();  
    }
    
    //Search for the change order 
    public void SearchforCO() {   
        TCOunits = 0.0;
        TUnitcost = 0.0;
        Tcost = 0.0;
        ATCOunits = 0.0;
        ATUnitcost = 0.0;
        ATcost = 0.0;
        RTCOunits = 0.0;
        RTUnitcost = 0.0;
        RTcost = 0.0;    
        Change_Order__c COstatus = [select Status__c, Name, Client_Project__r.Name,Total_Contracted_Value__c, Client_Project__r.Total_Contracted_Value__c,Client_Project__c from Change_Order__c where Id = :COid limit 1];
       
       //check for CO is Active or not
        if(COstatus.Name != null)
           ChangeorderNo = COstatus.Name;
        if(executableStatuses.contains(COstatus.Status__c)) {
           status = true;            
        } else
           status = false;
           selectedProject = COstatus.Client_Project__r.Name;
           selectedProjectid = COstatus.Client_Project__r.Id;
           TCV=COstatus.Total_Contracted_Value__c;
       
        List<Change_Order_Item__c> LiWrappers1=([select Id, Client_Unit_Number__c, Combo_Code__c, Description__c, Project_Region__r.Name, Unit_of_Measurement__c,
                                                Number_Of_Contracted_Units__c, Unit_Cost__c, Total_Cost__c, Amendment_CO_Number_of_Units__c, 
                                                Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Revised_Number_of_Units__c, Revised_Total_Cost__c,
                                                Revised_Unit_Cost__c,client_task__c
                                                from Change_Order_Item__c
                                                where Change_Order__r.Id = :COid and Client_Unit_Number__c != 'Migrated Worked Hours' 
                                                order by Client_Unit_Number__c]);
       
        LiWrappers2 = new List<GridDataTable1>();
        for(Change_Order_Item__c a : LiWrappers1) {
            GridDataTable1 obj = new GridDataTable1();
            if(String.valueof(a.Id) != null) {
                obj.id = (a.Id);
                obj.unitNumber = String.valueof(a.Client_Unit_Number__c);
                obj.ClientTaskId = String.valueof(a.client_task__c);
                obj.description = String.valueof(a.Description__c);
                obj.clientRegion = String.valueof(a.Project_Region__r.Name);
                obj.unitDriver = String.valueof(a.Unit_of_Measurement__c);   
                obj.comboCode = String.valueof(a.Combo_Code__c);  //combo code        
                obj.noOfUnits = (a.Number_Of_Contracted_Units__c);        
                obj.unitCost = (a.Unit_Cost__c);
                obj.totalCost = (a.Total_Cost__c); //////////////
                if(a.Amendment_CO_Number_of_Units__c != null)
                    obj.amendIncremental = (a.Amendment_CO_Number_of_Units__c);
                else
                    obj.amendIncremental = 0.0;
                obj.amendUnitCost = (a.Amendment_Unit_Cost__c);
                obj.amendTotalCost = (a.Amendment_Total_Cost_c__c); //////////////
                obj.revisedNoUnits = (a.Revised_Number_of_Units__c);
                obj.revisedUnitCost = (a.Revised_Unit_Cost__c);
                obj.revisedTotalCost = (a.Revised_Total_Cost__c); //////////////         
                if(obj.noOfUnits != null)
                    TCOunits += obj.noOfUnits;          
                if(obj.unitCost != null)
                    TUnitcost += obj.unitCost;         
                if(obj.totalCost != null)
                    Tcost += obj.totalCost;                   
                if(obj.amendIncremental != null)
                    ATCOunits += obj.amendIncremental;         
                if(obj.amendUnitCost != null)
                    ATUnitcost += obj.amendUnitCost;            
                if(obj.amendTotalCost != null)
                    ATcost += obj.amendTotalCost;            
                if(obj.revisedNoUnits != null)
                    RTCOunits += obj.revisedNoUnits;         
                if(obj.revisedUnitCost != null)
                    RTUnitcost += obj.revisedUnitCost;            
                if(obj.revisedTotalCost != null)
                    RTcost += obj.revisedTotalCost; 
                obj.changeOrderItemId = a.Id;         
            }
            LiWrappers2.add(obj);
        } 
        LiWrappers3 = new List<GridDataTable1>();
      
        //Total table values
        LTDList = new String[1];
        LTDList[0] = 'Total';
        for(String s : LTDList) {
            GridDataTable1 obj = new GridDataTable1();            
            obj.noOfUnits = TCOunits;        
            obj.unitCost = TUnitcost;
            obj.totalCost = Tcost;
            obj.amendIncremental =  ATCOunits;
            obj.amendUnitCost = ATUnitcost;
            obj.amendTotalCost = ATcost;
            obj.revisedNoUnits = RTCOunits;
            obj.revisedUnitCost = RTUnitcost;
            obj.revisedTotalCost = RTcost;          
            LiWrappers3.add(obj);  
        }
        
        //Summary table values
        LiWrappers4 = new List<GridDataTable1>();
        for(String s : LTDList) {
            GridDataTable1 obj = new GridDataTable1();            
           
            obj.totalCost = COstatus.Total_Contracted_Value__c;
           
            obj.amendTotalCost = ATcost;
            
            obj.revisedTotalCost =(((COstatus.Total_Contracted_Value__c == null)?0:COstatus.Total_Contracted_Value__c)+ATcost);          
            LiWrappers4.add(obj);  
        }
    }       
    
    //Go to home page on click of project name
    public PageReference viewRecord() {
        if(selectedRecord != null && selectedRecord != '') {
            PageReference pageRef=new PageReference('/apex/PRA_Change_Order?id=' + selectedRecord);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    } 
    
    //Go to Project change order Estimated End Date click of project name
    public PageReference viewEndDate() {
        PageReference pageRef = new PageReference('/apex/PRA_Change_Order_EndDate?id=' + COid);
        pageRef.setRedirect(true);
        return pageRef;
        return null;
    }  
    
    //Add new client unit button action
    public Pagereference addNewClientUnit() {
        Pagereference retVal = Page.PRA_Change_Order_Add_New_Client_Unit;
        retVal.getParameters().put('coId', COid);  
        retVal.getParameters().put('retURL', Apexpages.currentPage().getUrl());
        return retVal;
    }  
    
    //Edit existing client unit for CO
    public Pagereference editChangeOrderItem() {
        Pagereference retVal = Page.PRA_Change_Order_Modify_Client_Unit;
        retVal.getParameters().put('id', selectedChangeOrderId);     
        return retVal;
    } 
    
    //Execute CO button action  
    public void ExecuteCO() {
        Boolean shouldExecute = true;
        Boolean isAlreadyWaiting = false;
        Boolean isMissingCOLI = false;
        
        set<String> tasksNeedToUpdate = new set<String>();
        Change_Order__c changeOrder = new Change_Order__c();
        
        List<Change_Order__c> changeOrders = [select Id, Name,Executedby__c,Executeddate__c, Status__c from Change_Order__c where Client_Project__c = :selectedProjectid];
        List<Change_Order_Item__c> changeOrderItems = 
            [select Client_Unit_Number__c,
            (select BUF_Code__c from Change_Order_Line_Item__r)
            from Change_Order_Item__c 
            where Change_Order__c = :COid and Change_Order__r.Client_Project__c = :selectedProjectid];
        
        // Loop through all change orders for current project
        for(Change_Order__c co : changeOrders) {
            if(co.Status__c == 'Ready for Execution') {
                shouldExecute = false;
                isAlreadyWaiting = true;
            }
            if(co.Id == COid) {
                changeOrder = co;
            }
        }
        // Loop through all change order items to find any empty line items
        for(Change_Order_Item__c coi : changeOrderItems) {
            system.debug('-------------------------coil size-------------------------' + coi.Change_Order_Line_Item__r.size());
            if(coi.Change_Order_Line_Item__r.size() == 0) {
                shouldExecute = false;
                isMissingCOLI = true;
                if(!tasksNeedToUpdate.contains(coi.Client_Unit_Number__c)) {
                    tasksNeedToUpdate.add(coi.Client_Unit_Number__c);
                }
            }
        }
        
        // If no changes orders are ready for execution set current to ready
        if(!shouldExecute) {
            if(isAlreadyWaiting) {
                Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.WARNING, 'Only one change order can be executed per project per day'));
            }
            if(isMissingCOLI) {
                String errorMsg = 'BUF Codes must be associated to the following client units before executing: ';
                for(String s : tasksNeedToUpdate) {
                    errorMsg += s + ', ';
                }
                errorMsg = errorMsg.substring(0, errorMsg.length() - 2);
                Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.WARNING, errorMsg));
            }
        } else {
            if(executableStatuses.contains(changeOrder.Status__c)) {
                changeOrder.Status__c = 'Ready for Execution';
                changeOrder.Executedby__c=Userinfo.getUserId();
                changeOrder.Executeddate__c=Datetime.now();
                update changeOrder;
                SearchforCO(); 
            }
        }
    }
    
    //Clone CO 
    public Pagereference CloneCO() {
           
        //Create a CO order Record
        Change_Order__c changeOrder = new Change_Order__c(Name = ClonechangeOrderName, Status__c = 'Active', Client_Project__c = selectedProjectid); 
        changeOrder.Total_Contracted_Value__c=TCV;
        insert changeOrder;
        id coidf = changeOrder.id;
            
        //Create a CO item Records
        Set<Id> COItemID = new Set<Id>();
        List<Change_Order_Item__c> coItems = new List<Change_Order_Item__c>();
        for(Change_Order_Item__c coit : [select Id, Name, Client_Task__c, Description__c, Amendment_Total_Cost_c__c, Amendment_CO_Number_of_Units__c,
                                        Amendment_Unit_Cost__c, Client_Unit_Number__c, Combo_Code__c, End_Date__c, Forecast_Curve__c,
                                        Number_Of_Contracted_Units__c,Project_Region__c,Revised_Number_of_Units__c, Revised_Total_Cost__c, 
                                        Revised_Unit_Cost__c, Start_Date__c, Task_Group__c, Total_Cost__c, Unit_Completed_Event_Lookup__c, Type__c,
                                        Unit_Cost__c, Unit_of_Measurement__c, Is_Labor_Unit__c, Worked_Hours__c, Worked_Hours_Done__c, Worked_Units__c,
                                        Worked_Units_Done__c
                                        from Change_Order_Item__c
                                        where Change_Order__c = :COid]) {       
          
            // Clone of coItem retains the co line items
            Change_Order_Item__c coItem = coit.clone(false, true);
            coItem.put('Change_Order__c', coidf);
            coItem.put('Client_Task__c', coit.Client_Task__c);
            COItemID.add(coit.Id);
            coItems.add(coItem);
        }                                 
        insert coItems; 
        
        Set<Id> newCOItemIDs = new Set<Id>();
        Map<String,Id> mapnewIDs = new Map<String, Id>();        
        
        for(Change_Order_Item__c coli : coItems){            
            newCOItemIDs.add(coli.Id);
            mapnewIDs.put(coli.Client_Unit_Number__c, coli.Id);
        }        
        
        //create a CO Line item Records
        List<Change_Order_Line_Item__c> COlineItemsnew = new List<Change_Order_Line_Item__c>();
        List<Change_Order_Line_Item__c> liCOLineItems = new List<Change_Order_Line_Item__c>();
        liCOLineItems = [select Id, Name,Is_Labor_Unit__c, Change_Order_Item__r.Client_Unit_Number__c, BUF_Code__c, Change_Order_Item__c, Amendment_CO_Hours_Unit__c,
                        Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, Current_Contract_Hours__c, Current_Contract_Value__c,
                        Current_Hours_Unit__c, Current_Total_Cost__c, Discount_Price__c, Discount_Unit_Price__c, Current_Unit_Cost__c,
                        Revised_Hours_Unit__c, Revised_Total_Cost__c, Revised_Total_Hours__c, Revised_Unit_Cost__c, BUF_Code__r.Name, Type__c
                        from Change_Order_Line_Item__c
                        where Change_Order_Item__c in :COItemID
                        order by Change_Order_Item__r.Client_Unit_Number__c];
        for(Change_Order_Line_Item__c coli : liCOLineItems) {
            if(mapnewIDs.containsKey(coli.Change_Order_Item__r.Client_Unit_Number__c) &&
                mapnewIDs.get(coli.Change_Order_Item__r.Client_Unit_Number__c) != null) {
                
                system.debug('------------------------ coitem ------------------------' + coli.Change_Order_Item__r.Client_Unit_Number__c);
                system.debug('------------------------ coli buf code ------------------------' + coli.BUF_Code__r.Name);
                
                Change_Order_Line_Item__c coLineItem = new Change_Order_Line_Item__c();
                String s = mapnewIDs.get(coli.Change_Order_Item__r.Client_Unit_Number__c);
                coLineItem.Change_Order_Item__c             = s;
                coLineItem.BUF_Code__c                      = coli.BUF_Code__c;
                coLineItem.Current_Contract_Hours__c        = coli.Current_Contract_Hours__c;
                coLineItem.Current_Contract_Value__c        = coli.Current_Contract_Value__c;                    
                coLineItem.Current_Hours_Unit__c            = coli.Current_Hours_Unit__c;                   
                coLineItem.Current_Unit_Cost__c             = coli.Current_Unit_Cost__c;                    
                coLineItem.Current_Total_Cost__c            = coli.Current_Total_Cost__c;
                coLineItem.Type__c                          = coli.Type__c;
                coLineItem.Amendment_BUF_Code_Bill_Rate__c  = coli.Amendment_BUF_Code_Bill_Rate__c;
                coLineItem.Amendment_CO_Hours_unit__c       = coli.Amendment_CO_Hours_unit__c;
                coLineItem.Amendment_CO_Hours__c            = coli.Amendment_CO_Hours__c;
                coLineItem.Discount_Price__c                = coli.Discount_Price__c;  
                coLineItem.Discount_Unit_Price__c           = coli.Discount_Unit_Price__c;
                coLineItem.Is_Labor_Unit__c                 = coli.Is_Labor_Unit__c;
                COlineItemsnew.add(coLineItem);
                
            }                        
        }
        
        insert COlineItemsnew;
        
        if(changeOrder.Id != null) {
            try {
                PRA_ChangeOrderRefresh.refreshChangeOrder(changeOrder.Id);
                PageReference pageRef=new PageReference('/apex/PRA_Change_Order_Display_Modify?id=' + changeOrder.Id);
                pageRef.setRedirect(true);
                return pageRef;
            } catch (Exception ex) {   
                if(ex.getMessage().contains('SUM OF (Number Of Contracted Units + Amendment CO Number of Units)')) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                        'Amendment unit quantities for some units currently result in negative revised number of units'));
                } else {         
                    Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, ex.getMessage()));
                }
                delete changeOrder;
            }
        }
        return null;
    }   
    
    //Export to Excel
    public PageReference ExportExcel() {     
        PageReference pageRef = new PageReference('/apex/PRA_Change_Order_Display_Modify_CSV');
        pageRef.setRedirect(false);
        return pageRef; 
    }   
    
    //Delete a Change Order
    public PageReference DeleteCO() {   
        List<Change_Order__c> CO = [select Id, Name, Status__c from Change_Order__c where Id = :COid limit 1]; 
        delete CO; 
        selectedRecord = selectedProjectid;
        if(selectedRecord != null && selectedRecord != '') {
            PageReference pageRef = new PageReference('/apex/PRA_Change_Order?id=' + selectedRecord);
            pageRef.setRedirect(true);
            return pageRef;
        }         
        return null;   
    }
    
    // PC - Refresh change order
    public Pagereference RefreshChangeOrder() {
        Pagereference retVal;
        try {
            PRA_ChangeOrderRefresh.refreshChangeOrder(coID);
            retVal = Apexpages.currentPage();
            retVal.setRedirect(true);
        } catch (Exception ex) {
            if(ex.getMessage().contains('SUM OF (Number Of Contracted Units + Amendment CO Number of Units)')) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 
                    'Amendment unit quantities for some units currently result in negative revised number of units'));
            } else {
                Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, ex.getMessage()));
            }
        }
        return retVal;
    }
    
   //Remote Action to update Amendment CO #of Units
   @RemoteAction
    global static void updateChangeOrders(List<Change_Order_Item__c> liDataSend) {
        if(liDataSend != null && liDataSend.size() > 0)
            update liDataSend;
    }
    
    //Remote Action to delete a client unit under CO
    @RemoteAction
    global static void deleteChangeOrders(List<Change_Order_Item__c> liDataToDelete) {
        if(liDataToDelete != null && liDataToDelete.size() > 0)
            delete liDataToDelete;
    }

    @RemoteAction
    global static string ComboCodeValid(ID coid) {
        // first get the new change order items for this change order
        List<Change_Order_Item__c> licoi = [select Id, Combo_Code__c, Client_Unit_Number__c,Change_Order__r.Client_Project__c 
                                            from Change_Order_Item__c where Change_Order__r.Id = :coid and  Client_Task__c = null and Type__c = 'New'];
        Change_Order__c co = [select Id, Client_Project__c from Change_Order__c where Id = :coid limit 1];
        
        
        
        Set<string> setComboCodes = new Set<string>();
        Set<string> setCUN = new Set<string>();
        
        // make lists of the combo codes and client unit numbers
        for(Change_Order_Item__c coi : licoi ) {
            setComboCodes.Add(coi.Combo_Code__c);
            setCUN.Add(coi.Client_Unit_Number__c);
        }                             
        
        // grab a list of duplicates from Client task                
        List<Client_Task__c> liCTcun = [select Id, Combo_Code__c, Client_Unit_Number__c from Client_Task__c 
                                        where Client_Task__c.Client_Unit_Number__c in :setCUN and Project__c = :co.Client_Project__c];
                                     
        List<Client_Task__c> liCTcc = [select Id, Combo_Code__c, Client_Unit_Number__c from Client_Task__c 
                                        where Client_Task__c.Combo_Code__c in :setComboCodes and Project__c = :co.Client_Project__c];                             
                                     
        //Set to hold for duplicate combo codes and client units in the change order item object
        Set<string> setComboCodesDup = new Set<string>();
        Set<string> setCUNDup = new Set<string>();
       
        String CC;
        String CU;
        CC = 'Combo Code(s) - ';
        CU = 'Client Unit Number(s) - ';
       
        for(Client_Task__c ct :  liCTcun) {
            setCUNDup.Add(ct.Client_Unit_Number__c);         
        }                            
        for(Client_Task__c ct : liCTcc) {
            setComboCodesDup.Add(ct.Combo_Code__c);         
        }   
                                   
        system.debug('liCTcun: ' + liCTcun);   
        system.debug('liCTcc: ' + liCTcc);       
         
        //Checking the duplicate records in the change order item object
        for(AggregateResult coit : [select Combo_Code__c ccc from Change_Order_Item__c where Change_Order__r.Id = :coid 
                                        group by Combo_Code__c having (COUNT(Combo_Code__c) > 1)]) {         
          setComboCodesDup.Add(String.valueof(coit.get('ccc')));                  
        }
        for(AggregateResult coit : [select Client_Unit_Number__c ccu from Change_Order_Item__c where Change_Order__r.Id = :coid 
                                        group by Client_Unit_Number__c having (COUNT(Client_Unit_Number__c) > 1)]) {
           setCUNDup.Add(String.valueof(coit.get('ccu')));                
        }         
        if(liCTcun.size() > 0 || liCTcc.size() > 0 || setCUNDup.size() > 0 || setComboCodesDup.size() > 0) {
             return 'This Change Order cannot be executed, The Client Unit Number(s) - ' + setCUNDup +', Combo Code(s) - '+setComboCodesDup
                 + ' in this Change Order already exist(s) for this project. Please enter a new Client Unit Number, Combo Code.';
        } else {
           List<Change_Order_Item__c> closedUnitsList = PRA_DataAccessor.getCOItemswithClientTaskCloseDate(coid,PRA_ClientTaskService.TO_RE_OPEN_CLIENT_TASKS);
           if(closedUnitsList.size()>0){
               Set<String> ClientUnitsSet =new Set<string>();
               for(Change_Order_Item__c coi:closedUnitsList){
                   ClientUnitsSet.add(coi.Client_Unit_Number__c);             
               }
               String err='This Change Order contains Closed Client Units. Cannot execute this Change Order.';
               err+=' Please re-open the closed client units or remove the closed units from this Change Order.';
               err+=' Client Units Numbers(s) that are closed are - '+ClientUnitsSet;
               return err;
           }else{
               return 'passed';
           }
            
        }     
    }
    
    //Wrapper class to hold data
    public class GridDataTable1 {
        public Id id {get;set;}
        public String unitNumber {get;set;}
        public String description {get;set;}        
        public String clientRegion {get;set;}
        public String unitDriver {get;set;}        
        public Decimal noOfUnits {get;set;}
        public Decimal unitCost {get;set;}
        public Decimal totalCost {get;set;}
        public Decimal amendIncremental {get;set;}
        public Decimal amendUnitCost {get;set;}
        public Decimal amendTotalCost {get;set;}
        public Decimal revisedNoUnits {get;set;}
        public Decimal revisedUnitCost {get;set;}
        public Decimal revisedTotalCost {get;set;}        
        public String comboCode {get;set;}         
        public String changeOrderItemId  {get;set;} 
        public String ClientTaskId  {get;set;} 
        
        GridDataTable1() {  
             noOfUnits = 0.0;
             unitCost = 0.0;
             totalCost = 0.0;
             amendIncremental = 0.0;
             amendUnitCost = 0.0;
             amendTotalCost = 0.0;
             revisedNoUnits = 0.0;
             revisedUnitCost = 0.0;
             revisedTotalCost = 0.0;
        }
    }
}