@isTest
private class PBB_ServiceTaskToDriverTest {

    private static Service_Model__c serviceModel;
    private static Service_Area__c serviceArea;
    private static Service_Function__c serviceFunction;
    private static Service_Task__c serviceTask;
    private static Service_Task_To_Drivers__c serviceTaskToDrivers;

    private static PBB_ServiceTaskToDriver serviceTaskD;

    static void createTestData() {
        PBB_TestUtils testUtils = new PBB_TestUtils();
        serviceModel = testUtils.CreateServiceModelAttributes();
        serviceArea = testUtils.createServiceAreaAttributes();
        serviceFunction = testUtils.createServiceFunctionAttributes();
        serviceTask = testUtils.createServiceTaskAttributes();
        serviceTask.Global__c = true;
        update serviceTask;

        Formula_Name__c billFormula = testUtils.createBillFormulaNames();
        serviceTaskToDrivers = testUtils.createSTtoBF(serviceTask, billFormula);
        serviceTaskToDrivers.Countries_Option__c = 'Include';
        serviceTaskToDrivers.Countries__c = 'Country2';
        serviceTaskToDrivers.Drivers__c = 'Countries';
        update serviceTaskToDrivers;

        serviceTaskD = new PBB_ServiceTaskToDriver(serviceTask.Id);
        serviceTaskD.serviceTaskToDriversList = new List<Service_Task_To_Drivers__c>{serviceTaskToDrivers};
        serviceTaskD.serviceTaskToDrivers = serviceTaskToDrivers;

        AA_TestUtils ut = new AA_TestUtils();
        ut.createCountry();
        Country__c c = ut.createCountry();
        c.Name = 'Country2';
        update c;
    }

    @isTest
    private static void getCountriesTest() {
        createTestData();
        Test.startTest();
            serviceTaskD.getCountries();
        Test.stopTest();
        System.assert(serviceTaskD.countries.size() > 0);
    }

    @isTest
    private static void getDriversTest() {
        createTestData();
        Test.startTest();
            List<SelectOption> drivers = serviceTaskD.getDrivers();
        Test.stopTest();
        System.assert(drivers.size() > 0);
    }

    @isTest
    private static void preparationCreateOrEditServiceNewTest() {
        createTestData();
        Test.startTest();
            serviceTaskD.preparationCreateOrEditService();
        Test.stopTest();
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Id, null);
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Service_Task__c, serviceTask.Id);
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Times_Frequency__c, 1);
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Per_Frequency__c, 1);
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Frequency__c, 'Study');
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Drivers__c, '');
    }

    @isTest
    private static void preparationCreateOrEditServiceEditIncludeTest() {
        createTestData();
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        Test.startTest();
            serviceTaskD.preparationCreateOrEditService();
        Test.stopTest();
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Id, serviceTaskToDrivers.Id);
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Service_Task__c, serviceTask.Id);
        String first = new List<String> (serviceTaskD.rightCountryValues).get(0);
        System.assertEquals(first, 'Country2');
        System.assert(serviceTaskD.rightCountryValues.size() == 1);
        first = new List<String> (serviceTaskD.leftCountryValues).get(0);
        System.assertEquals(first, 'Country1');

    }

    /*@isTest
    private static void preparationCreateOrEditServiceEditExcludeTest() {
        createTestData();
        serviceTaskToDrivers.Countries_Option__c = 'Exclude';
        update serviceTaskToDrivers;
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        Test.startTest();
            serviceTaskD.preparationCreateOrEditService();
        Test.stopTest();
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Id, serviceTaskToDrivers.Id);
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Service_Task__c, serviceTask.Id);
        String first = new List<String> (serviceTaskD.rightCountryValues).get(0);
        System.assertEquals(first, 'Country1');
        System.assert(serviceTaskD.rightCountryValues.size() == 1);
        first = new List<String> (serviceTaskD.leftCountryValues).get(0);
        System.assertEquals(first, 'Country2');

    }*/

    @isTest
    private static void createServiceIncludeTest() {
        createTestData();
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        serviceTaskD.preparationCreateOrEditService();
        Test.startTest();
            serviceTaskD.createService();
        Test.stopTest();
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Id, serviceTaskToDrivers.Id);
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Countries__c, 'Country2');
    }

    /*@isTest
    private static void createServiceExcludeTest() {
        createTestData();
        serviceTaskToDrivers.Countries_Option__c = 'Exclude';
        update serviceTaskToDrivers;
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        serviceTaskD.preparationCreateOrEditService();
        Test.startTest();
            serviceTaskD.createService();
        Test.stopTest();
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Id, serviceTaskToDrivers.Id);
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Countries__c, 'Country2');
    }*/

    @isTest
    private static void createServiceAllTest() {
        createTestData();
        serviceTaskToDrivers.Countries_Option__c = 'All';
        update serviceTaskToDrivers;
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        serviceTaskD.preparationCreateOrEditService();
        Test.startTest();
            serviceTaskD.createService();
        Test.stopTest();
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Id, serviceTaskToDrivers.Id);
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Countries__c, '');
    }

    @isTest
    private static void createServiceFormulaTest() {
        createTestData();
        serviceTaskToDrivers.Drivers__c = 'Formula';
        serviceTaskToDrivers.Formula_Name__c = null;
        update serviceTaskToDrivers;
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        serviceTaskD.preparationCreateOrEditService();
        serviceTaskD.serviceTaskToDrivers.Name = 'new Name';
        Test.startTest();
            serviceTaskD.createService();
        Test.stopTest();
        serviceTaskD.preparationCreateOrEditService();
        System.assertNotEquals(serviceTaskD.serviceTaskToDrivers.Name, 'new Name');
    }

    @isTest
    private static void createServiceCountriesTest() {
        createTestData();
        serviceTask.Global__c = false;
        update serviceTask;
        serviceTaskToDrivers.Drivers__c = 'Countries';
        update serviceTaskToDrivers;
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        serviceTaskD.preparationCreateOrEditService();
        serviceTaskD.serviceTaskToDrivers.Name = 'new Name';
        Test.startTest();
            serviceTaskD.createService();
        Test.stopTest();
        serviceTaskD.preparationCreateOrEditService();
        System.assertNotEquals(serviceTaskD.serviceTaskToDrivers.Name, 'new Name');
    }

    @isTest
    private static void createServiceParameterTest() {
        createTestData();
        serviceTaskToDrivers.Drivers__c = 'Parameter';
        serviceTaskToDrivers.Status__c =  'Approved';
        update serviceTaskToDrivers;
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        serviceTaskD.preparationCreateOrEditService();
        serviceTaskD.serviceTaskToDrivers.Name = 'new Name';
        Test.startTest();
            serviceTaskD.createService();
        Test.stopTest();
        serviceTaskD.preparationCreateOrEditService();
        system.debug('----serviceTaskD.serviceTaskToDrivers---'+serviceTaskD.serviceTaskToDrivers+'---'+serviceTaskToDrivers);
        System.assertEquals(serviceTaskD.serviceTaskToDrivers.Name, 'new Name');
    }

    @isTest
    private static void selectclickTest() {
        createTestData();
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        serviceTaskD.preparationCreateOrEditService();
        serviceTaskD.leftselectedCountries = new List<String>{'Country1'};
        Test.startTest();
            serviceTaskD.selectclick();
        Test.stopTest();

        String first = new List<String> (serviceTaskD.rightCountryValues).get(0);
        System.assertEquals(first, 'Country2');
    }

    @isTest
    private static void unselectclickTest() {
        createTestData();
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        serviceTaskD.preparationCreateOrEditService();
        serviceTaskD.leftselectedCountries = new List<String>{'Country1'};
        serviceTaskD.selectclick();
        serviceTaskD.rightselectedCountries = new List<String>{'Country1'};
        Test.startTest();
            serviceTaskD.unselectclick();
        Test.stopTest();
        System.assertEquals(serviceTaskD.rightCountryValues.size(), 1);
        System.assertEquals(serviceTaskD.leftCountryValues.size(), 1);
    }

    @isTest
    private static void getSelectedValuesTest() {
        createTestData();
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        serviceTaskD.preparationCreateOrEditService();
        serviceTaskD.rightselectedCountries = new List<String>{'Country1'};
        serviceTaskD.selectclick();
        Test.startTest();
            List<SelectOption> selectedValues = serviceTaskD.getSelectedValues();
        Test.stopTest();
        System.assertEquals(selectedValues.size(), 1);
    }

    @isTest
    private static void getunSelectedValuesTest() {
        createTestData();
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        serviceTaskD.preparationCreateOrEditService();
        serviceTaskD.rightselectedCountries = new List<String>{'Country1'};
        serviceTaskD.selectclick();
        serviceTaskD.getSelectedValues();
        Test.startTest();
            List<SelectOption> selectedValues = serviceTaskD.getunSelectedValues();
        Test.stopTest();
        System.assertEquals(selectedValues.size(), 1);
    }

    @isTest
    private static void removeServiceTest() {
        createTestData();
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        Test.startTest();
            serviceTaskD.removeService();
        Test.stopTest();
        List<Service_Task_To_Drivers__c> serviceTaskToDriversList = [SELECT Id FROM Service_Task_To_Drivers__c WHERE Id = : serviceTaskD.getEditServiceId()];
        System.assertEquals(serviceTaskToDriversList.size(), 0);
    }

    @isTest
    private static void testProperties(){
        createTestData();
        serviceTaskD.getDetailService();
        ApexPages.currentPage().getParameters().put('editServiceTaskToDriversId', serviceTaskToDrivers.Id);
        Test.startTest();
            Boolean isReadOnly = serviceTaskD.isReadOnly;
            Boolean isRetired = serviceTaskD.isRetired;
            String selectedCountries = serviceTaskD.selectedCountries;
        Test.stopTest();
        System.assertEquals(isReadOnly, false);
        System.assertEquals(isRetired, false);
        System.assertEquals(selectedCountries, null);
    }

    @isTest
    private static void refreshServicesTest() {
        createTestData();
        Test.startTest();
            serviceTaskD.refreshServices();
        Test.stopTest();
        System.assert(serviceTaskD.serviceTaskToDriversList.size() > 0);
        System.assertEquals(serviceTaskD.serviceTask.Id, serviceTask.Id);
    }
    
    @isTest
    private static void getCurrenciesTest() {
        createTestData();
        Test.startTest();
            List<SelectOption> currencies = serviceTaskD.getCurrencies();
        Test.stopTest();
        System.assert(currencies.size() == 0);
    }

    @isTest
    private static void getSelectCountriesTest() {
        createTestData();
        Test.startTest();
            List<String> countries = serviceTaskD.getSelectCountries();
        Test.stopTest();
        System.assert(countries.size() != 0);
    }

    @isTest
    private static void getSelectedObjectFiledLabelTest() {
        createTestData();
        List<selectOption> parametersObjects = serviceTaskD.getParametersObjects();
        serviceTaskD.selectedParametersObject = parametersObjects[1].getValue();
        List<selectOption> objectFields = serviceTaskD.getObjectFields();
        serviceTaskD.selectedObjectFiled = objectFields[0].getValue();
        Test.startTest();
            String selectedObjectFiledLabel = serviceTaskD.getSelectedObjectFiledLabel();
        Test.stopTest();
        System.assert(selectedObjectFiledLabel != '');
    }

    @isTest
    private static void getSelectedParametersObjectLabelTest() {
        createTestData();
        List<selectOption> parametersObjects = serviceTaskD.getParametersObjects();
        serviceTaskD.selectedParametersObject = parametersObjects[1].getValue();
        List<selectOption> objectFields = serviceTaskD.getObjectFields();
        serviceTaskD.selectedObjectFiled = objectFields[0].getValue();
        Test.startTest();
            String selectedObjectFiledLabel = serviceTaskD.getSelectedParametersObjectLabel();
        Test.stopTest();
        System.assert(selectedObjectFiledLabel != '');
    }

    @isTest
    private static void getParametersObjectsTest() {
        createTestData();
        Test.startTest();
            List<selectOption> parametersObjects = serviceTaskD.getParametersObjects();
        Test.stopTest();
        System.assert(parametersObjects.size() > 0);
    }

    @isTest
    private static void getObjectFieldsTest() {
        createTestData();
        List<selectOption> parametersObjects = serviceTaskD.getParametersObjects();
        serviceTaskD.selectedParametersObject = parametersObjects[1].getValue();
        Test.startTest();
            List<selectOption> objectFields = serviceTaskD.getObjectFields();
        Test.stopTest();
        System.assert(parametersObjects.size() > 0);
    }
    
}