public with sharing class BDT_SecuritiesService {

	/**
	 * Read securities for specified roles
	 * @author Maurice Kremer
	 * @version 14-Oct-13
	 * @param list<String> selectedRoles a list of the role numbers
	 * @return list<SecuritiesDisplay>
	 */
	public static list<SecuritiesDisplay> getSecurities (list<BDT_Role__c> selectedRoles) {
		
		list<SecuritiesDisplay> result = new list<SecuritiesDisplay>();
		
		// read securities
		list<BDT_Securities__c> allSecurities = BDT_SecuritiesDataAccessor.getAllSecurities();
		
		for (BDT_Securities__c sec : allSecurities) {

			SecuritiesDisplay secDisp = new SecuritiesDisplay();
			secDisp.securityName = sec.Name;
			secDisp.roles = new List<SecuritiesPerRole>();
				
			for (BDT_Role__c role : selectedRoles) {
				
				SecuritiesPerRole s = new SecuritiesPerRole();
				s.roleNumber 	= role.Role_Number__c;
				s.createPriv 	= hasRole(sec.CreateRoleNumbers__c,	role.Role_Number__c);
				s.readPriv		= hasRole(sec.ReadRoleNumbers__c,	role.Role_Number__c);
				s.updatePriv	= hasRole(sec.UpdateRoleNumbers__c,	role.Role_Number__c);
				s.deletePriv	= hasRole(sec.DeleteRoleNumbers__c,	role.Role_Number__c);
				s.privWasAdjusted = false;
				
				secDisp.roles.add(s);
			}
			
			result.add(secDisp);
			
		}
		
		return result;
	}

	/**
	 * Process wrapper SecuritiesDisplay and save changes
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @param list<SecuritiesDisplay> values DisplayWrapper values
	 */
	public static void saveSecurities (list<SecuritiesDisplay> values) {
		
		map<string,BDT_Securities__c> updateList = new map<string,BDT_Securities__c> ();
		
		for (SecuritiesDisplay secDisp : values) {
			for (SecuritiesPerRole secRole : secDisp.roles) {
				if (secRole.privWasAdjusted) {
					
					BDT_Securities__c sec;
					if (updateList.containsKey(secDisp.securityName)) {
						sec = updateList.get(secDisp.securityName);
					} else {
						sec = BDT_Securities__c.getValues(secDisp.securityName);
					}
					
					sec.CreateRoleNumbers__c = updatePrivilege (sec.CreateRoleNumbers__c,
							secRole.roleNumber,
							secRole.createPriv);
					sec.ReadRoleNumbers__c = updatePrivilege (sec.ReadRoleNumbers__c,
							secRole.roleNumber,
							secRole.readPriv);
					sec.UpdateRoleNumbers__c = updatePrivilege (sec.UpdateRoleNumbers__c,
							secRole.roleNumber,
							secRole.updatePriv);
					sec.DeleteRoleNumbers__c = updatePrivilege (sec.DeleteRoleNumbers__c,
							secRole.roleNumber,
							secRole.deletePriv);
							
					updateList.put(secDisp.securityName,sec);
				}
			}
		}
		update updateList.values();
	}

	/**
	 * Ensure a role is not in a set or is in a set based on boolean
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @param String roleNumbers 	Set of roles like "1:2:3"
	 * @param Decimal roleNumber	Role number to check
	 * @param Boolean hasPrivilege	True makes sure role is in list, False makes sure role is not in list
	 * @return boolean
	 */
	public static String updatePrivilege (String roleNumbers, Decimal roleNumber, Boolean hasPrivilege) {
		if (hasRole(roleNumbers,roleNumber) && !hasPrivilege) {
			// remove privilege
			roleNumbers = BDT_Utils.removeElementFromStringList(roleNumbers,String.valueOf(roleNumber));
		} else
		if (!hasRole(roleNumbers,roleNumber) && hasPrivilege) {
			if (roleNumbers==null) {
				roleNumbers='';
			}
			// add privilege
			roleNumbers += ':' + roleNumber;
			rolenumbers = rolenumbers.removeStart(':');
		}
		return roleNumbers;
	}
	
	/**
	 * Test if a role is in a set of roles
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @param String roleSet 		Set of roles like "1:2:3"
	 * @param Decimal roleToCheck	Role number to check
	 * @return boolean
	 */
	public static boolean hasRole(String roleSet, Decimal roleToCheck) {
		return (!String.isBlank(roleSet)) ?
				Pattern.matches('(^|.*:)'+roleToCheck+'($|:.*)', roleSet) :
				false;
	}
	
	/**
	 * Wrapper for displaying roles, security items and their CRUD matrix
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 */
	public class SecuritiesDisplay {
		public String securityName {get;set;}
		public list<SecuritiesPerRole> roles {get;set;}
	}

	/**
	 * Child wrapper for SecuritiesDisplay
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 */
	private class SecuritiesPerRole {
		public Decimal roleNumber 	{get;set;}
		public Boolean createPriv 	{get;set;}
		public Boolean readPriv 	{get;set;}
		public Boolean updatePriv 	{get;set;}
		public Boolean deletePriv 	{get;set;}
		public Boolean privWasAdjusted {get;set;}
	}

	public class pageSecurityWrapper {
		
		public pageSecurityWrapper (String SecurityName) {
			// get the current user id
			String userId = system.Userinfo.getUserId();
			
			if (BDT_UserRoles__c.getValues(userId) == null) {
				// admin type users overide security setting, but admin added to BDT
				// users will allow to test securities by admin type users
				if (BDT_Utils.getUserIsAdministrator()) {
					CreatePriv = true;
					ReadPriv   = true;
					UpdatePriv = true;
					DeletePriv = true;
				} else {
					// not a admin & not a bdt user, no access
					CreatePriv = false;
					ReadPriv   = false;
					UpdatePriv = false;
					DeletePriv = false;
				}
				return;
			}
			
			// get the roles assigned to the user
			String userRoles = BDT_UserRoles__c.getValues(userId).RoleNumbers__c;
			

			// get the security settings
			BDT_Securities__c sec = BDT_Securities__c.getValues(SecurityName);
			
			// if no securities have been defined then grant all
			if (sec == null) {
				CreatePriv = true;
				ReadPriv = true;
				UpdatePriv = true;
				DeletePriv = true;
			} else {
				// set the privilege values
				CreatePriv = 	BDT_Utils.haveMatchingElements(sec.CreateRoleNumbers__c, 	userRoles);
				ReadPriv = 		BDT_Utils.haveMatchingElements(sec.ReadRoleNumbers__c, 		userRoles);
				UpdatePriv = 	BDT_Utils.haveMatchingElements(sec.UpdateRoleNumbers__c, 	userRoles);
				DeletePriv = 	BDT_Utils.haveMatchingElements(sec.DeleteRoleNumbers__c, 	userRoles);
			}
		}
		
		private boolean CreatePriv;
		public boolean getCreatePriv() {return CreatePriv;}
		public void setCreatePriv(Boolean value) {
			// not allowed to update privs
		}
		
		private boolean ReadPriv;
		public boolean getReadPriv() {return ReadPriv;}
		public void setReadtePriv(Boolean value) {
			// not allowed to update privs
		}
		
		private boolean UpdatePriv;
		public boolean getUpdatePriv() {return UpdatePriv;}
		public void setUpdatePriv(Boolean value) {
			// not allowed to update privs
		}
		
		private boolean DeletePriv;
		public boolean getDeletePriv() {return DeletePriv;}
		public void setDeletePriv(Boolean value) {
			// not allowed to update privs
		}
	}

}