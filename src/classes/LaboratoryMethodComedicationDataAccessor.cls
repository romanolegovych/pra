/** Implements the Selector Layer of the object LaboratoryMethodComedication__c
 * @author	Dimitrios Sgourdos
 * @version	07-Nov-2013
 */
public with sharing class LaboratoryMethodComedicationDataAccessor {
	
	/** Object definition for fields used in application for LaboratoryMethodComedication
	 * @author	Dimitrios Sgourdos
	 * @version 14-Oct-2013
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('LaboratoryMethodComedication__c');		
	}
	
	
	/** Object definition for fields used in application for LaboratoryMethodComedication with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 07-Nov-2013
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'Assessment__c,';
		result += referenceName + 'Compound01ID__c,';
		result += referenceName + 'Compound02ID__c,';
		result += referenceName + 'Compound03ID__c,';
		result += referenceName + 'Compound04ID__c,';
		result += referenceName + 'Compound05ID__c,';
		result += referenceName + 'Compound06ID__c,';
		result += referenceName + 'Compound07ID__c,';
		result += referenceName + 'Compound08ID__c,';
		result += referenceName + 'Compound09ID__c,';
		result += referenceName + 'Compound10ID__c,';
		result += referenceName + 'Compound11ID__c,';
		result += referenceName + 'Compound12ID__c,';
		result += referenceName + 'Compound13ID__c,';
		result += referenceName + 'Compound14ID__c,';
		result += referenceName + 'Compound15ID__c,';
		result += referenceName + 'Compound16ID__c,';
		result += referenceName + 'Compound17ID__c,';
		result += referenceName + 'Compound18ID__c,';
		result += referenceName + 'Compound19ID__c,';
		result += referenceName + 'Compound20ID__c,';
		result += referenceName + 'Compound01Text__c,';
		result += referenceName + 'Compound02Text__c,';
		result += referenceName + 'Compound03Text__c,';
		result += referenceName + 'Compound04Text__c,';
		result += referenceName + 'Compound05Text__c,';
		result += referenceName + 'Compound06Text__c,';
		result += referenceName + 'Compound07Text__c,';
		result += referenceName + 'Compound08Text__c,';
		result += referenceName + 'Compound09Text__c,';
		result += referenceName + 'Compound10Text__c,';
		result += referenceName + 'Compound11Text__c,';
		result += referenceName + 'Compound12Text__c,';
		result += referenceName + 'Compound13Text__c,';
		result += referenceName + 'Compound14Text__c,';
		result += referenceName + 'Compound15Text__c,';
		result += referenceName + 'Compound16Text__c,';
		result += referenceName + 'Compound17Text__c,';
		result += referenceName + 'Compound18Text__c,';
		result += referenceName + 'Compound19Text__c,';
		result += referenceName + 'Compound20Text__c,';
		result += referenceName + 'LaboratoryMethod__c';
		return result;
	}
}