public with sharing class BDT_NewEditServiceController {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Administration Services
	public String serviceID 			{get;set;}
	public Service__c service 			{get;set;}
	public integer serviceoldsequence;
	public string oldcategoryId;
	public String serviceCategoryName	{get;set;}
	public String serviceCategoryId 	{get;set;}

	public BDT_NewEditServiceController(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Services');
		serviceID = system.currentPageReference().getParameters().get('serviceID');
		serviceCategoryId = system.currentPageReference().getParameters().get('serviceCategoryId');
		retrieveService();
	}
	
	public void retrieveService() {	
		serviceID = system.currentPageReference().getParameters().get('serviceID');
		serviceCategoryId = system.currentPageReference().getParameters().get('serviceCategoryId');
		
				service = new Service__c();
		
		if(serviceID!=null && serviceID!=''){
			try{
				service = [Select s.ServiceCategory__c,
								  s.ServiceCategory__r.Id,
								  s.ServiceCategory__r.Name, 
								  s.Name,
								  s.ShortName__c,
								  s.IsDesignObject__c,
								  s.IsExpense__c, 
								  s.InformationalText__c, 
								  s.Id,
								  SequenceNumber__c
		                     From Service__c s
							where s.id = :serviceID];
				serviceoldsequence = integer.valueof(service.SequenceNumber__c);
				oldcategoryId      = string.valueof(service.ServiceCategory__r.Id);			
				ServiceCategoryName = service.ServiceCategory__r.Name;
			
				serviceCategoryId = service.ServiceCategory__r.Id;
									
			}catch(QueryException e){
				service = new Service__c();
				
				service.ServiceCategory__c = serviceCategoryId;
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Service could not be found. ');
        		ApexPages.addMessage(msg); 
			}
		}else if (serviceCategoryId!=null && serviceCategoryId!='') {
			try {
				serviceCategory__c sc = new serviceCategory__c();
				sc = Database.query('select Name from ServiceCategory__c where id = \'' + serviceCategoryId +'\'');
				serviceCategoryName = sc.Name;
				
				service = new Service__c();
				service.ServiceCategory__c = serviceCategoryId;
			} catch (Exception e) {}
	    } 
	}

	public PageReference save(){
		// Check for validity
		if(service.IsDesignObject__c && String.isBlank(service.ShortName__c) ) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The "Short Name" field is mandatory when "Allow in Flowchart" is selected. Please, add a Short Name and save again.');
        	ApexPages.addMessage(msg);
        	return null;
		}
		
		// if sequenceNumber is 0 set it to the latest in the list + 1
		if (service.SequenceNumber__c == 0) {
			AggregateResult[] ar = [select max(SequenceNumber__c) maxresult from service__c where ServiceCategory__c = :service.ServiceCategory__c];
			Object max = ar[0].get('maxresult');
			service.SequenceNumber__c = Integer.ValueOf(max)+1;
		}
		
		// Reset ShortName__c if needed
		service.ShortName__c = (service.IsDesignObject__c)? service.ShortName__c : NULL;
		
		try{
			upsert service;
		}catch(DMLException e){
			//ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to save service. ');
        	//ApexPages.addMessage(msg);
        	return null;
		}
		
		fixSequenceNumber (service.ServiceCategory__c,string.valueof(service.ID), serviceoldsequence);
		
		if (oldcategoryId!=service.ServiceCategory__c) {
			// category changed renumber the old category
			fixSequenceNumber (oldcategoryId,string.valueof(service.ID), serviceoldsequence);
		}

		PageReference servicesOverviewPage = new PageReference(System.Page.BDT_Services.getUrl());
		servicesoverviewPage.getParameters().put('ScId', service.ServiceCategory__c );
		
		return servicesOverviewPage;
	}
	
	public void fixSequenceNumber (string ServiceCategoryID
										 ,string ServiceID
										 ,integer OldNumber ) {
		try{
			List<Service__c> serlist = [select id, sequencenumber__c, name 
										  from service__c 
										 where ServiceCategory__c = :ServiceCategoryID
										   and BDTDeleted__c = false 
										 order by sequencenumber__c, name];
			Integer sequencecount = 1;
			Boolean changed = false;
			if (oldnumber == null) {oldnumber = 0;}
			// adjust sequence numbers //
			
			for (Service__c ser:serlist) {
				ser.SequenceNumber__c = ser.SequenceNumber__c * 10;
				if (ser.id == serviceId ) {
					if ((OldNumber*10) > ser.SequenceNumber__c) {
						ser.SequenceNumber__c = ser.SequenceNumber__c - 5;
					} else {
						ser.SequenceNumber__c = ser.SequenceNumber__c + 5;
					}
				} 
			}
	
			BDT_Utils.sortList(serlist,'SequenceNumber__c','asc');
			
			for (Service__c ser:serlist) {
				
				if (ser.SequenceNumber__c != sequencecount) {
					ser.SequenceNumber__c = sequencecount;
					changed = true;
				}
				sequencecount++;
			}
			if (changed) {update serlist;}
		} catch (Exception e) {}
		
	}

	public PageReference serviceDelete(){
		service.BDTDeleted__c =  true;
		
		try{
			update service;
		}catch(DMLException e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to delete service. ');
        	ApexPages.addMessage(msg); 
        	
		}
		
		fixSequenceNumber (service.ServiceCategory__c,string.valueof(service.ID), serviceoldsequence);
		
		return new PageReference(System.Page.BDT_Services.getUrl());
	}
	
	public PageReference cancel(){
		PageReference servicesOverviewPage = new PageReference(System.Page.BDT_Services.getUrl());
		servicesOverviewPage.getParameters().put('ScId',serviceCategoryId);
		
		
		return servicesOverviewPage;
	}
	

   

	

}