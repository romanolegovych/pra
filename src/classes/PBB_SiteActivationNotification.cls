/**
 * @description Site Activation Notification class
 * @author      Niharika Reddy
 * @date        Created: 23-Sep-2015, Edited: 23-Sep-2015
 */
public class PBB_SiteActivationNotification implements NCM_NotificationEventInterface {
    
    public Integer notificationCount {get;set;}
    public WFM_Protocol__c protocol  {get;set;}
    
    /**
     * @description Site Activation notification is implemented in this method
     * @author      Niharika Reddy
     * @date        Created: 24-Sep-2015
     * @param       notificationCatalogName    The name of the notification topic that occurs
     * @param       relatedToId                The related object id that triggers the notification topic
     * @return      Returns a String that has notification subject
    */
    public String getNotificationSubject(Id notificationCatalogId, String relatedToId) {
        protocol = PBB_DataAccessor.getProtocolById(relatedToId);
        System.debug('******subject method 1**********'+protocol);
        String subject = protocol.NBR_Actual_Site__c + ' ' + 'Sites Activated' ;
        return subject; 
        return null;
     }
    
     /**
     * @description Site activation Notification body is implemented in this method
     * @author      Niharika Reddy
     * @date        Created: 23-Sep-2015
     * @param       notificationCatalogName    The name of the notification topic that occurs
     * @param       relatedToId                The related object id that triggers the notification topic
     * @return      Returns a string with notification body
    */
    public String getNotificationEmailBody(Id notificationCatalogId,String relatedToId){
        protocol = PBB_DataAccessor.getProtocolById(relatedToId);
        System.debug('*****bdy method 1**********'+protocol);
        String bdy = 'Notification Date:'+ ' ' +PBB_Utils.getDateInStringFormat(Date.today()) + '<br>' + 
                      '<br/>' +
                      'Protocol:' + ' ' + protocol.name + '<br>' + 
                      '<br/>' +
                       protocol.NBR_Actual_Site__c+ ' ' + 'Sites Activated' + '<br>' + 
                       '<br/>' +
                     '<br/>' +
                     '<br/>' +
                     'Regards'+ ','+ '<br>' +
                     'Predictivv Connect Team' + '<br>' +
                     '_________________________________________________________________________'+ ' <br> ' +
                     '<i>Disclaimer: You are receiving this email due to Notification Subscription settings.</i>' + '<br>' +
                     '<i>To stop these emails please update your Notification Settings.</i>' + '<br>' +
                     '<i>Please do not reply to this email.</i>';               
        return bdy;  
        
    }
    
    /**
     * @description This method checks whether the notification topics has occured or not . 
                    If it occurs it would create and send the email for every ten patients registered.
     * @author      Niharika Reddy
     * @date        Created: 23-Sep-2015
     * @param       notificationCatalogName    The name of the notification topic that occurs
     * @param       relatedToId                The related object id that triggers the notification topic
     * @return      Returns notification body
    */
    public Boolean didNotificationTopicOccur(Id notificationCatalogId, String relatedToId){
         protocol = new WFM_Protocol__c();
        protocol = PBB_DataAccessor.getProtocolById(relatedToId);
        System.debug('******didMethod 1**********'+protocol);
        
        if(protocol.Notification_Site_Count__c== null && protocol.NBR_Actual_Site__c >= 10){
            System.debug('***********'+protocol);
            protocol.Notification_Site_Count__c= protocol.NBR_Actual_Site__c;
            upsert protocol;
            System.debug('******didMethod 2**********'+protocol);
            //NCM_API.activateNotificationEventById(notificationCatalogId,relatedToId);
            return true;
         }
        else if(protocol.Notification_Site_Count__c != null && Integer.valueOf(protocol.Notification_Site_Count__c/10) <  Integer.valueOf((protocol.NBR_Actual_Site__c)/10)){
            protocol.Notification_Site_Count__c= protocol.NBR_Actual_Site__c;
            upsert protocol;
            System.debug('******didMethod 3**********'+protocol);
            //NCM_API.activateNotificationEventById(notificationCatalogId,relatedToId);
            return true;
        } 
        else{
            return false;
        }  
    }  
    
 }