/** Implements the Service Layer of the object LaboratoryAnalysisSlots__c
 * @author	Dimitrios Sgourdos
 * @version	26-Nov-2013
 */
public with sharing class LaboratoryAnalysisSlotsService {
	
	/** Retrieve the Laboratory Slots for laboratory designs.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Nov-2013
	 * @param	studiesList			The project studies that are assigned with AN analysis
	 * @param	MdValFlag			If the slot is for Development-Validation(true) or Analysis(false)
	 * @return	The Laboratory Slots wrapper list for the laboratory design.
	 */
	public static List<LaboratorySlotsDesignWrapper> getLaboratorySlotsForLabDesigns(List<Study__c> studiesList, Boolean MdValFlag) {
		// Declare variables
		List<LaboratorySlotsDesignWrapper> results = new List<LaboratorySlotsDesignWrapper>();
		
		// Read the data and create the wrapper
		List<LaboratoryAnalysisSlots__c> analysisSlotsList = LaboratoryAnalysisSlotsDataAccessor.getSlotsByMdValFlagAndStudies(
																											studiesList,
																											MdValFlag);
		
		for(LaboratoryAnalysisSlots__c tmpSlot : analysisSlotsList) {
			LaboratorySlotsDesignWrapper newItem = new LaboratorySlotsDesignWrapper();
			newItem.labSlot = tmpSlot;
			newItem.studyMethodCombination = tmpSlot.Study__c + ':' + tmpSlot.LaboratoryMethod__c;
			newItem.slotNumber = String.valueOf(tmpSlot.SlotNumber__c);
			results.add(newItem);
		}
		
		return results;
	}
	
	
	/** Count how many slots there are in a LaboratorySlotsDesignWrapper List.
	 * @author	Dimitrios Sgourdos
	 * @version 07-Nov-2013
	 * @param	slotsWrapperList	The LaboratorySlotsDesignWrapper List
	 * @return	How many slots there are in the LaboratorySlotsDesignWrapper List.
	 */
	public static Integer countSlotsInLabSlotsFoMdAndValDesignWrapperList(List<LaboratorySlotsDesignWrapper> slotsWrapperList) {
		Integer result = 0;
		Decimal slotNum = -1;
		
		for(LaboratorySlotsDesignWrapper tmpItem : slotsWrapperList) {
			Decimal tmpDec = tmpItem.labSlot.SlotNumber__c;
			if(slotNum != tmpDec) {
				slotNum = tmpDec;
				result++;
			}
		}
		
		return result;
	}
	
	
	/** Create a LaboratorySlotsDesignWrapper Instance without study/method and slot type selection.
	 * @author	Dimitrios Sgourdos
	 * @version 07-Nov-2013
	 * @param	slotNumber			The slot number that the instance will be associated with
	 * @param	MdValFlag			If the slot is for Development-Validation(true) or Analysis(false)
	 * @return	The LaboratorySlotsDesignWrapper Instance.
	 */
	public static LaboratorySlotsDesignWrapper createLaboratorySlotsDesignWrapperInstance(String slotNumber, Boolean MdValFlag) {
		LaboratorySlotsDesignWrapper result = new LaboratorySlotsDesignWrapper();
		result.labSlot 	  = new LaboratoryAnalysisSlots__c(SlotNumber__c=Decimal.valueOf(slotNumber), MdValSlot__c=MdValFlag);
		result.slotNumber = slotNumber;
		return result;
	}
	
	
	/** Add a slot in lab designs.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Nov-2013
	 * @param	initialWrapperList	The initial wrapper list that holds the slots for lab designs
	 * @param	MdValFlag			If the slot is for Development-Validation(true) or Analysis(false)
	 * @return	The updated wrapper list that holds the slots for lab designs.
	 */
	public static List<LaboratorySlotsDesignWrapper> addSlotToLabDesign(List<LaboratorySlotsDesignWrapper> initialWrapperList, Boolean MdValFlag) {
		// Create the LaboratorySlotsDesignWrapper instance
		String slotNumber = String.valueOf(countSlotsInLabSlotsFoMdAndValDesignWrapperList(initialWrapperList) + 1);
		LaboratorySlotsDesignWrapper newItem = createLaboratorySlotsDesignWrapperInstance(slotNumber, MdValFlag);
		
		initialWrapperList.add(newItem);
		
		return initialWrapperList;
	}
	
	
	/** Add a study/method combination for a slot in a lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Nov-2013
	 * @param	initialWrapperList	The initial wrapper list that holds the slots for lab design
	 * @param	firstSampleIndex	The index of the first study/method combination of the slot in the wrapper list, that we want to add a new combination
	 * @param	MdValFlag			If the slot is for Development-Validation(true) or Analysis(false)
	 * @return	The updated wrapper list that holds the slots for lab design.
	 */
	public static List<LaboratorySlotsDesignWrapper> addSlotCombinationLabDesign(List<LaboratorySlotsDesignWrapper> initialWrapperList, Integer firstCombinationIndex, Boolean MdValFlag) {
		// Check if the firstSampleIndex is well defined
		if(firstCombinationIndex<0 || firstCombinationIndex >= initialWrapperList.size()) {
			return initialWrapperList;
		}
		
		// Create the LaboratorySamplesDesignWrapper instance
		String slotNumber = String.valueOf(initialWrapperList[firstCombinationIndex].labSlot.SlotNumber__c);
		LaboratorySlotsDesignWrapper newItem = createLaboratorySlotsDesignWrapperInstance(slotNumber, MdValFlag);
		
		// Find the position in the wrapper that the new sample must be inserted
		for(Integer i=firstCombinationIndex; i<initialWrapperList.size(); i++) {
			if(initialWrapperList[firstCombinationIndex].labSlot.SlotNumber__c != initialWrapperList[i].labSlot.SlotNumber__c) {
				initialWrapperList.add(i, newItem);
				return initialWrapperList;
			}
		}
		
		initialWrapperList.add(newItem);
		
		return initialWrapperList;
	}
	
	
	/** Remove a laboratory slot from the lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Nov-2013
	 * @param	initialWrapperList	The initial wrapper list that holds the slots for method development and validation lab design
	 * @param	removeSlotIndex		The index in the list of the laboratory slots that will be removed
	 * @return	The updated slots for lab design.
	 */
	public static List<LaboratorySlotsDesignWrapper> removeSlotCombinationFromLabDesign(List<LaboratorySlotsDesignWrapper> initialWrapperList, Integer removeSlotIndex) {
		// Check if the removeSampleIndex is valid
		if(removeSlotIndex<0 || removeSlotIndex>=initialWrapperList.size()) {
			return initialWrapperList;
		}
		
		// Update the next combination (in case it belongs to the same slot) with the inserted slot number to keep the inserted slot number if the deleted combination is first child
		Integer nextIndex = removeSlotIndex + 1;
		if(nextIndex<initialWrapperList.size()
					&& initialWrapperList[nextIndex].labSlot.SlotNumber__c == initialWrapperList[removeSlotIndex].labSlot.SlotNumber__c) {
			initialWrapperList[nextIndex].slotNumber = initialWrapperList[removeSlotIndex].slotNumber;
		}
		
		// remove slot Study/Method combination
		initialWrapperList.remove(removeSlotIndex);
		
		return initialWrapperList;
	}
	
	
	/** Validate if a LaboratorySlotsDesignWrapper for slots for Md and Val lab Design has all the neccessary fields inserted or not.
	 * @author	Dimitrios Sgourdos
	 * @version 08-Nov-2013
	 * @param	wrapperList			The wrapper list that holds the slots for method development and validation lab design
	 * @return	If the LaboratorySlotsDesignWrapper list is valid or not.
	 */
	public static Boolean validateLabSlotsMdAndValDesign(List<LaboratorySlotsDesignWrapper> wrapperList) {
		for(LaboratorySlotsDesignWrapper tmpItem : wrapperList) {
			if(String.isBlank(tmpItem.studyMethodCombination) || String.isBLank(tmpItem.labSlot.SlotType__c) ) {
				return false;
			}
		}
		return true;
	}
	
	
	/** Adjust the slots of a slots wrapper list. The slotNumber of all records must match the slot number
	 *	of the first combination that belongs to the same slot. The sequenceNumber__c of the combinations must
	 *	match the combination sequence in the slot and the Study__c, LaboratoryMethod__c  must match the new selection.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 * @param	initialList			The initial wrapper list that holds the sample shipments lab design
	 * @return	The updated sample shipments lab design.
	 */
	public static List<LaboratorySlotsDesignWrapper> adjustSlotsInSlotsWrapperListForLabDesigns(List<LaboratorySlotsDesignWrapper> initialList) {
		Decimal previousDataSlotNum;
		String insertedSlotNum;
		Integer counter = 1;
		
		// Adjust list
		for(LaboratorySlotsDesignWrapper tmpItem : initialList) {
			
			if(previousDataSlotNum != tmpItem.labSlot.SlotNumber__c ) {
				previousDataSlotNum = tmpItem.labSlot.SlotNumber__c;
				insertedSlotNum 	= tmpItem.slotNumber;
				counter = 1;
			}
			
			tmpItem.slotNumber = insertedSlotNum;
			tmpItem.labSlot.SequenceNumber__c = counter;
			
			if( String.isNotBlank(tmpItem.studyMethodCombination) ) {
				List<String> splittedList = tmpItem.studyMethodCombination.split(':');
				tmpItem.labSlot.Study__c  = splittedList[0];
				tmpItem.labSlot.LaboratoryMethod__c = splittedList[1];
			}
			
			counter++;
		}
		
		return initialList;
	}
	
	
	/** Sort a LaboratorySlotsDesignWrapper list by the slotNumber and renumber the duplicates. 
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 * @param	initialList			The initial wrapper list that holds the slots lab design
	 * @return	The updated LaboratorySlotsDesignWrapper list.
	 */
	public static List<LaboratorySlotsDesignWrapper> sortSlotsDesignWrapperBySlotNumber(List<LaboratorySlotsDesignWrapper> initialList) {
		// Declare variables
		List<LaboratorySlotsDesignWrapper> finalList = new List<LaboratorySlotsDesignWrapper>();
		Set<Decimal> slotNumbersSet = new Set<Decimal>();
		Map<String, List<LaboratorySlotsDesignWrapper>> sortingMap = new Map<String, List<LaboratorySlotsDesignWrapper>>();
		
		// Create the temporary map and the slot numbers set
		for(LaboratorySlotsDesignWrapper wrapperItem : initialList) {
			// Set
			slotNumbersSet.add(Decimal.valueOf(wrapperItem.slotNumber));
			// Map
			List<LaboratorySlotsDesignWrapper> tmpWrapperList = new List<LaboratorySlotsDesignWrapper>();
			if(sortingMap.containsKey(wrapperItem.slotNumber)) {
				tmpWrapperList = sortingMap.remove(wrapperItem.slotNumber);
			}
			tmpWrapperList.add(wrapperItem);
			sortingMap.put(wrapperItem.slotNumber, tmpWrapperList);
		}
		
		// Sort the shipment numbers
		List<Decimal> slotNumsList = new List<Decimal>();
		slotNumsList.addAll(slotNumbersSet);
		slotNumsList.sort();
		
		// Create the final sorted list
		Integer counter = 1;
		for(Decimal tmpDec : slotNumsList) {
			String key = String.valueOf(tmpDec);
			if( sortingMap.containsKey(key) ) {
				List<LaboratorySlotsDesignWrapper> tmpWrapperList = sortingMap.get(key);
				for(LaboratorySlotsDesignWrapper wrapperItem : tmpWrapperList) {
					wrapperItem.slotNumber = String.valueOf(counter);
					finalList.add(wrapperItem);
					counter++;
				}
			}
		}
		
		return finalList;
	}
	
	
	/** Extract the lab analysis slots from the inserted slots for method development and validation lab design. 
	 * @author	Dimitrios Sgourdos
	 * @version 08-Nov-2013
	 * @param	initialList			The initial wrapper list that holds the slots for method development and validation lab design
	 * @return	The lab analysis slots from the inserted lab design.
	 */
	public static List<LaboratoryAnalysisSlots__c> extractLabAnalysisSlotsFromLabDesign(List<LaboratorySlotsDesignWrapper> initialList) {
		// Declare variables
		List<LaboratoryAnalysisSlots__c> results = new List<LaboratoryAnalysisSlots__c>();
		
		// Adjust the wrapper list to be updated with the user's entries
		initialList = adjustSlotsInSlotsWrapperListForLabDesigns(initialList);
		
		// Create a map with key the SlotNumber__c of the slots and value the list of combinations (study/Method slots). 
		// Also retrieve the first combination of each slot and store them to the firstCombinationOfSlotsList
		List<LaboratorySlotsDesignWrapper>				firstCombinationOfSlotsList = new List<LaboratorySlotsDesignWrapper>();
		Map<String, List<LaboratoryAnalysisSlots__c>> 	labSlotsMap 	= new Map<String, List<LaboratoryAnalysisSlots__c>>();
		List<LaboratoryAnalysisSlots__c> 				tmpLabSlotsList = new List<LaboratoryAnalysisSlots__c>();
		Decimal 										previousDataSlotNum;
		
		for(LaboratorySlotsDesignWrapper wrapperItem : initialList) {
			// Add to the temporary list for creating the map
			tmpLabSlotsList.add(wrapperItem.labSlot);
			// Create the list of the first samples of each shipment
			if(previousDataSlotNum != wrapperItem.labSlot.SlotNumber__c ) {
				previousDataSlotNum = wrapperItem.labSlot.SlotNumber__c;
				firstCombinationOfSlotsList.add(wrapperItem);
			}
		}
		
		labSlotsMap = (Map<String,List<LaboratoryAnalysisSlots__c>>) BDT_Utils.mapSObjectListOnKey(tmpLabSlotsList, new List<String>{'SlotNumber__c'});
		
		// Sort the first combination of each slot by the inserted slot numbers and renumber the duplicates
		firstCombinationOfSlotsList = sortSlotsDesignWrapperBySlotNumber(firstCombinationOfSlotsList);
		
		// Create the results from labSlotsMap and firstCombinationOfSlotsList
		for(LaboratorySlotsDesignWrapper wrapperItem : firstCombinationOfSlotsList) {
			String key = String.valueOf(wrapperItem.labSlot.SlotNumber__c);
			if( labSlotsMap.containsKey(Key) ) {
				// Update firstly the renumbered shipmentNumber in the retrieved samples from the map
				List<LaboratoryAnalysisSlots__c> tmpList = new List<LaboratoryAnalysisSlots__c>();
				tmpList = labSlotsMap.get(Key);
				for(LaboratoryAnalysisSlots__c tmpSample : tmpList) {
					tmpSample.SlotNumber__c = Decimal.valueOf(wrapperItem.slotNumber);
				}
				// Add to the results the fully updated lab samples
				results.addAll(tmpList);
			}
		}
		
		return results;
	}
	
	
	/** Save the given LaboratoryAnalysisSlots. 
	 * @author	Dimitrios Sgourdos
	 * @version 08-Nov-2013
	 * @param	initialList			The initial list ofLaboratoryAnalysisSlots
	 * @return	If the save was successful or not.
	 */
	public static Boolean saveLaboratoryAnalysisSlots(List<LaboratoryAnalysisSlots__c> slotsList) {
		try{
			upsert slotsList;
		} catch(Exception e) {
			return false;
		}
		return true;
	}
	
	
	/** Delete the given LaboratoryAnalysisSlots in case they have id. 
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 * @param	initialList			The initial list of LaboratoryAnalysisSlots
	 * @return	If the deletion was successful or not.
	 */
	public static Boolean deleteLaboratoryAnalysisSlots(List<LaboratoryAnalysisSlots__c> initialList) {
		List<LaboratoryAnalysisSlots__c> deletedList = new List<LaboratoryAnalysisSlots__c>();
		
		// delete only the samples that have id
		for(LaboratoryAnalysisSlots__c tmpSlot : initialList) {
			if(tmpSlot.Id != NULL) {
				deletedList.add(tmpSlot);
			}
		}
		
		try{
			delete deletedList;
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}
	
	
	/** Summarize the given Lab Design (Slots for Analysis) by the study-method combinations. 
	 * @author	Dimitrios Sgourdos
	 * @version 11-Nov-2013
	 * @param	slotsDesignWrapperList	The given Lab Design (Slots for Analysis)
	 * @return	The summary of the design by the study-method combinations.
	 */
	public static List<LaboratorySlotsDesignWrapper> summarizeLabSlotsDesignByStudyMethodCombination(
																		List<LaboratorySlotsDesignWrapper> slotsDesignWrapperList,
																		List<SelectOption> studyMethodOptionList) {
		// Declare variables
		List<LaboratorySlotsDesignWrapper> results = new List<LaboratorySlotsDesignWrapper>();
		
		// Create a Map from studyMethodOptionList to retrieve the labels for each value
		Map<String, String> optionsMap = new Map<String, String>();
		for(SelectOption tmpOption : studyMethodOptionList) {
			optionsMap.put(tmpOption.getValue(), tmpOption.getLabel());
		}
		
		// Create a map with key the study-Method Combination(labels) and value the summary of all items with this key
		Map<String,LaboratorySlotsDesignWrapper> summaryMap = new Map<String,LaboratorySlotsDesignWrapper>();
		
		for(LaboratorySlotsDesignWrapper tmpItem : slotsDesignWrapperList) {
			
			// Create the key and initialize the value
			String key = '';
			if(optionsMap.containsKey(tmpItem.studyMethodCombination)) {
				key = optionsMap.get(tmpItem.studyMethodCombination);
			}
			LaboratorySlotsDesignWrapper newValue = new LaboratorySlotsDesignWrapper();
			if(summaryMap.containsKey(key)) {
				newValue = summaryMap.remove(key);
			}
			
			// Fix the previous values
			if(newValue.labSlot == NULL) {
				newValue.labSlot = new LaboratoryAnalysisSlots__c();
			}
			newValue.labSlot.ActiveNumberOfSamples__c  = (newValue.labSlot.ActiveNumberOfSamples__c==NULL)?  0 : newValue.labSlot.ActiveNumberOfSamples__c;
			newValue.labSlot.PlaceboNumberOfSamples__c = (newValue.labSlot.PlaceboNumberOfSamples__c==NULL)? 0 : newValue.labSlot.PlaceboNumberOfSamples__c;
			newValue.labSlot.BackupNumberOfSamples__c  = (newValue.labSlot.BackupNumberOfSamples__c==NULL)?  0 : newValue.labSlot.BackupNumberOfSamples__c;
			newValue.labSlot.OtherNumberOfSamples__c   = (newValue.labSlot.OtherNumberOfSamples__c==NULL)?   0 : newValue.labSlot.OtherNumberOfSamples__c;
			newValue.labSlot.Repeat__c				   = (newValue.labSlot.Repeat__c==NULL)?				 0 : newValue.labSlot.Repeat__c;
			
			// Add the current values
			newValue.labSlot.ActiveNumberOfSamples__c  += ( (tmpItem.labSlot.ActiveNumberOfSamples__c==NULL)?  0 : tmpItem.labSlot.ActiveNumberOfSamples__c );
			newValue.labSlot.PlaceboNumberOfSamples__c += ( (tmpItem.labSlot.PlaceboNumberOfSamples__c==NULL)? 0 : tmpItem.labSlot.PlaceboNumberOfSamples__c );
			newValue.labSlot.BackupNumberOfSamples__c  += ( (tmpItem.labSlot.BackupNumberOfSamples__c==NULL)?  0 : tmpItem.labSlot.BackupNumberOfSamples__c );
			newValue.labSlot.OtherNumberOfSamples__c   += ( (tmpItem.labSlot.OtherNumberOfSamples__c==NULL)?   0 : tmpItem.labSlot.OtherNumberOfSamples__c );
			newValue.labSlot.Repeat__c				   += ( (tmpItem.labSlot.Repeat__c==NULL)?				   0 : tmpItem.labSlot.Repeat__c );
			
			// Store to the map the update value
			summaryMap.put(key, newValue);
		}
		
		// Create the results
		List<String> keyValuesList = new List<String>();
		keyValuesList.addAll(summaryMap.keySet());
		keyValuesList.sort();
		
		for(String key : keyValuesList) {
			LaboratorySlotsDesignWrapper newItem = summaryMap.get(key); // cannot be null
			newItem.studyMethodCombination = String.isBlank(key)? 'No study / method selected' : key;
			results.add(newItem);
		}
		
		return results;
	}
	
	
	/** Validate if a LaboratorySlotsDesignWrapper for slots for analysis lab design has all the neccessary fields inserted or not.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 * @param	wrapperList			The wrapper list that holds the slots for analysis lab design
	 * @return	If the LaboratorySlotsDesignWrapper list is valid or not.
	 */
	public static Boolean validateLabSlotsAnDesign(List<LaboratorySlotsDesignWrapper> wrapperList) {
		for(LaboratorySlotsDesignWrapper tmpItem : wrapperList) {
			if(String.isBlank(tmpItem.studyMethodCombination) || String.isBLank(tmpItem.labSlot.ListOfShipments__c) ) {
				return false;
			}
		}
		return true;
	}
	
	
	/** Get the shipments numbers that are connected with an item in the given list of LaboratoryAnalysisSlots. 
	 * @author	Dimitrios Sgourdos
	 * @version 14-Nov-2013
	 * @param	labSlotsList		The given list of LaboratoryAnalysisSlots
	 * @return	The shipments numbers that are connected with a LaboratoryAnalysisSlots item.
	 */
	public static Set<String> getAssociatedShipmentNumbersToSlots(List<LaboratoryAnalysisSlots__c> labSlotsList) {
		Set<String> results = new Set<String>();
		
		for(LaboratoryAnalysisSlots__c tmpSlot : labSlotsList) {
			if( String.isNotBlank(tmpSlot.ListOfShipments__c) ) {
				List<String> tmpNumsList = new List<String>();
				tmpNumsList = tmpSlot.ListOfShipments__c.split(':');
				results.addAll(tmpNumsList);
			}
		}
		
		return results;
	}
	
	
	/** Update the list of shipment numbers in the given slots depends on the pair key-value of the given map.
	 * @author	Dimitrios Sgourdos
	 * @version 14-Nov-2013
	 * @param	slotsList			The given list of LaboratoryAnalysisSlots
	 * @param	shipmentNumsMap		The map that keeps the old (key) and new (values) shipment numbers
	 * @return	The shipments numbers that are connected with a LaboratoryAnalysisSlots item.
	 */
	public static List<LaboratoryAnalysisSlots__c> updateLabSlotsByShipmentNumbers(List<LaboratoryAnalysisSlots__c> slotsList, Map<String, String> shipmentNumsMap) {
		// Iterate through the slots
		for(LaboratoryAnalysisSlots__c tmpSlot : slotsList) {
			
			if( String.isBlank(tmpSlot.ListOfShipments__c) ) {
				continue;
			}
			
			List<String> numbersList = tmpSlot.ListOfShipments__c.split(':');
			// Update the numbers and recreate ListOfShipments__c
			String listOfShipments = '';
			for(Integer i=0; i<numbersList.size(); i++) {
				numbersList[i] = (shipmentNumsMap.containsKey(numbersList[i])? shipmentNumsMap.get(numbersList[i]) : NULL);
				if(numbersList[i] != NULL) {
					listOfShipments += ':' + numbersList[i];
				}
			}
			
			listOfShipments = listOfShipments.removeStart(':');
			tmpSlot.ListOfShipments__c = listOfShipments;
			
		}
		
		return slotsList;
	}
	
	
	/** Update the list of shipment numbers in the slots depends on the new shipment numbers in the given list of samples wrapper records.
	 * @author	Dimitrios Sgourdos
	 * @version 14-Nov-2013
	 * @param	studiesList			The given list of studies
	 * @param	samplesWrapperList	The samples wrapper list that holds the new shipment numbers
	 */
	public static void updateListOfShipmentsInLabSlots( List<Study__c> studiesList, 
														List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> samplesWrapperList) {
		// Declare variables
		List<LaboratoryAnalysisSlots__c> labSlotsList = new List<LaboratoryAnalysisSlots__c>();
		Set<String> associatedShipmentNumsSet = new Set<String>();
		
		// Get LaboratoryAnalysisSlots and the associated shipment numbers
		labSlotsList = LaboratoryAnalysisSlotsDataAccessor.getSlotsByMdValFlagAndStudies(studiesList, false);
		associatedShipmentNumsSet = getAssociatedShipmentNumbersToSlots(labSlotsList);
		
		// Create a map with the associated shipment numbers with key the initial number and value the final number 
		// (Initial the value will be NULL)
		Map<String, String> shipmentNumsMap = new Map<String, String>();
		for(String tmpStr : associatedShipmentNumsSet) {
			shipmentNumsMap.put(tmpStr, NULL);
		}
		
		// Update the map with the changes
		shipmentNumsMap = LaboratorySamplesService.updateShipmentNumbersMapWithNewNumbers(shipmentNumsMap, samplesWrapperList);
		
		// Update the slots with the new shipment numbers
		labSlotsList = updateLabSlotsByShipmentNumbers(labSlotsList, shipmentNumsMap);
		update labSlotsList;
	}
	
	
	/** Group a LaboratorySlotsDesignWrapper list by the study-method combination
	 * @author	Dimitrios Sgourdos
	 * @version 26-Nov-2013
	 * @param	initialList			The initial wrapper list that holds the slots lab design
	 * @return	The updated LaboratorySlotsDesignWrapper list.
	 */
	public static List<LaboratorySlotsDesignWrapper> groupSlotsDesignWrapperByStudyMethod(
																	List<LaboratorySlotsDesignWrapper> initialList,
																	List<Study__c> studiesList,
																	List<LaboratoryAnalysis__c>	analysisList) {
		// Declare variables
		List<LaboratorySlotsDesignWrapper> finalList = new List<LaboratorySlotsDesignWrapper>();
		Set<String> studyMethodCombSet = new Set<String>();
		Map<String, List<LaboratorySlotsDesignWrapper>> groupingMap = new Map<String, List<LaboratorySlotsDesignWrapper>>();
		
		// Create map for studies and lab methods
		Map<String, Study__c> studiesMap = new Map<String, Study__c>();
		studiesMap.putAll(studiesList);
		
		Map<String, String> methodMap = new Map<String, String>();
		for(LaboratoryAnalysis__c tmpItem : analysisList) {
			methodMap.put(tmpItem.LaboratoryMethodCompound__r.LaboratoryMethod__r.Id,
								tmpItem.LaboratoryMethodCompound__r.LaboratoryMethod__r.Name);
		}
		
		// Create the temporary map and the study-method combination set
		for(LaboratorySlotsDesignWrapper wrapperItem : initialList) {
			// Change the studyMethodCombination
			if( String.IsBlank(wrapperItem.studyMethodCombination) ) {
				continue;
			}
			List<String> splittedComb = wrapperItem.studyMethodCombination.split(':');
			if(splittedComb.size() != 2
					|| splittedComb[0] == NULL
					|| splittedComb[1] == NULL
					|| (!studiesMap.containsKey(splittedComb[0]))
					|| (!methodMap.containsKey(splittedComb[1])) ) {
				continue;
			}
			wrapperItem.studyMethodCombination = studiesMap.get(splittedComb[0]).Code__c.substringAfterLast('-')
													+ ' / '
													+ methodMap.get(splittedComb[1]);
			// Set
			studyMethodCombSet.add(wrapperItem.studyMethodCombination);
			// Map
			List<LaboratorySlotsDesignWrapper> tmpWrapperList = new List<LaboratorySlotsDesignWrapper>();
			if(groupingMap.containsKey(wrapperItem.studyMethodCombination)) {
				tmpWrapperList = groupingMap.remove(wrapperItem.studyMethodCombination);
			}
			tmpWrapperList.add(wrapperItem);
			groupingMap.put(wrapperItem.studyMethodCombination, tmpWrapperList);
		}
		
		// Sort the shipment numbers
		List<String> studyMethodCombList = new List<String>();
		studyMethodCombList.addAll(studyMethodCombSet);
		studyMethodCombList.sort();
		
		// Create the final sorted list
		for(String tmpStr : studyMethodCombList) {
			if(groupingMap.containsKey(tmpStr)) {
				List<LaboratorySlotsDesignWrapper> tmpWrapperList = groupingMap.get(tmpStr);
				for(LaboratorySlotsDesignWrapper wrapperItem : tmpWrapperList) {
					finalList.add(wrapperItem);
				}
			}
		}
		
		return finalList;
	}
	
	
	/** Find all the shipment numbers that are used in a Slots for Analysis lab design
	 * @author	Dimitrios Sgourdos
	 * @version 26-Nov-2013
	 * @param	initialList			The initial wrapper list that holds the slots lab design
	 * @return	The set of the shipment numbers.
	 */
	public static Set<Decimal> getUsedShipmentsNumbersInSlotsForAnalysisDesign(List<LaboratorySlotsDesignWrapper> initialList) {
		Set<Decimal> results = new Set<Decimal>();
		
		for(LaboratorySlotsDesignWrapper tmpItem : initialList) {
			// If there isn't meaning, skip this step
			if(tmpItem.labSlot==NULL || String.isBlank(tmpItem.labSlot.ListOfShipments__c)) {
				continue;
			}
			
			// Find the used shipments in the current slot
			List<String> shipmentList = new List<String>();
			shipmentList = tmpItem.labSlot.ListOfShipments__c.split(':');
			for(String tmpStr : shipmentList) {
				results.add(Decimal.valueOf(tmpStr));
			}
		}
		
		return results;
	}
	
	
	/**	This class keeps a laboratory analysis slot with the study/method and type selection.
	 * @author	Dimitrios Sgourdos
	 * @version	26-Nov-2013
	 */
	public class LaboratorySlotsDesignWrapper {
		public LaboratoryAnalysisSlots__c	labSlot					{get;set;}
		public String						studyMethodCombination	{get;set;}
		public String						slotNumber				{get;set;}
		
		
		// Calculate the total, as the field TotalNumberOfSamples is updated only if the record is saved
		public String getLabSlotTotalNumber() {
			// Correct the values in case they are null
			if(labSlot==NULL) {
				labSlot = new LaboratoryAnalysisSlots__c();
			}
			labSlot.ActiveNumberOfSamples__c  = (labSlot.ActiveNumberOfSamples__c==NULL)?  0 : labSlot.ActiveNumberOfSamples__c;
			labSlot.PlaceboNumberOfSamples__c = (labSlot.PlaceboNumberOfSamples__c==NULL)? 0 : labSlot.PlaceboNumberOfSamples__c;
			labSlot.BackupNumberOfSamples__c  = (labSlot.BackupNumberOfSamples__c==NULL)?  0 : labSlot.BackupNumberOfSamples__c;
			labSlot.OtherNumberOfSamples__c   = (labSlot.OtherNumberOfSamples__c==NULL)?   0 : labSlot.OtherNumberOfSamples__c;
			
			// Calculate Result
			return BDT_Utils.formatNumberWithSeparators( labSlot.ActiveNumberOfSamples__c + labSlot.PlaceboNumberOfSamples__c +
														 labSlot.BackupNumberOfSamples__c + labSlot.OtherNumberOfSamples__c);
		}
		
		
		// Calculate the List Of Shipments assigned with the slot
		public List<String> getLabSlotListOfShipments() {
			List<String> results = new List<String>();
			
			// Check for null values 
			if(labSlot==NULL || String.isBlank(labSlot.ListOfShipments__c)) {
				return results;
			}
			
			// Calculate Result
			results = labSlot.ListOfShipments__c.split(':');
			results.sort();
			
			// Fix the display
			for(Integer i=0; i<results.size(); i++) {
				results[i] = 'Ship-' + Integer.valueOf(results[i]);
			}
			
			return results;
		}
	}
}