/**
* @author Bhargav Devaram
* @date 29-Jun-2015
* @description this is test class for Service Impact Responses Controller
*/

@isTest
private class PBB_ServiceImpactResponsesControllerTest {
    static testMethod void testPBB_ServiceImpactResponsesControllerTest (){      
        test.startTest();
           
            PBB_TestUtils tu = new PBB_TestUtils ();
            tu.country =tu.createCountryAttributes();
            tu.siqn =tu.createSIQNAttributes();
            tu.siqs = tu.createSIQSAttributes(tu.siqn.id);
            tu.sir = tu.createSIRAttributes();
            Service_Impact_Response__c sr = tu.createSIRAttributes();
            Service_Impact_Response__c sr1 = sr.clone(false);
            sr1.Default__c =true;
            insert sr1;
            PageReference pageRef = Page.PBB_ServiceImpactResponses;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id', tu.sir.Id); 
            ApexPages.currentPage().getParameters().put('clone', '1'); 
            ApexPages.StandardController sc = new ApexPages.StandardController(tu.sir);
              
            PBB_ServiceImpactResponsesController SIRController = new PBB_ServiceImpactResponsesController(sc);        	
            SIRController.saveSIRQuestions();	
            SIRController.sir.Default__c = true;
            SIRController.saveSIRQuestions(); 
            System.assertEquals(SIRController.sir.Default__c, true);
            tu.siqs.Status__c = 'Approved';            
            update tu.siqs;            
            SIRController.saveSIRQuestions();
                    
        test.stopTest();      
    } 
}