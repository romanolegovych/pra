/**
@author Niharika Reddy
@date 2015
@description this controller class is to display Vendor and Other Vendor selection.
**/
public class PBB_VendorInformationController {
    public    String    selectedValue    { get; set; }
    public    String    selectedVendor   { get; set; }
    public    String    PBBVendor        { get; set; }
    public    String    otherVendorName  { get; set; }  
    
    public    SelectOption[]    vCategoryOptions       { get; set; }
    public    SelectOption[]    selectedVendorOptions  { get; set; }
    
    public List<SelectOption>    availVendorsList    { get; set; }
    public List<SelectOption>    selectedVendorsList { get; set; }
    public List<OtherVendors>    otherVendorsList    { get; set; }
    
    public    Project_Vendors__c    projectVendor    { get; set; }
    
    public    Map<String, Vendor_Category__c>     vcMap { get; set; }
    
    Set<String> vSet = new Set<String>(); 
    public Set<String> otherPVSet{ get; set; }
    
    public Integer rowNum{get;set;}
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this the wrapper class for other vendor section
    **/
    public class OtherVendors{
        public String vendorName{ get; set; }
        public OtherVendors(String vendorName){
            this.vendorName = vendorName;
        }
    }
    
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this is the constuctor method for getting values from standard controller variable
    **/
    public PBB_VendorInformationController(ApexPages.StandardController controller) {
        vcMap = new Map<String, Vendor_Category__c>();
        vCategoryOptions = new SelectOption[]{};
        vCategoryOptions = getCategoryOptions();
        selectedVendorOptions = new List<SelectOption>();
        availVendorsList = new List<SelectOption>();
        selectedVendorsList = new List<SelectOption>();
        otherPVSet = new Set<String>();
        otherVendorsList = new List<OtherVendors>();
        otherVendorsList.add(new OtherVendors(''));
        if(!Test.isRunningTest())
            controller.addFields(new List<String>{'Vendor_Category__c','Bid_Project__c'});
        projectVendor = (Project_Vendors__c)controller.getRecord();
        system.debug('Project Vendor'+projectVendor);
        if(ApexPages.currentPage().getParameters().get('bid')!=null && ApexPages.currentPage().getParameters().get('bid')!='')
            projectVendor.Bid_Project__c = ApexPages.currentPage().getParameters().get('bid');
        system.debug('Project Vendor'+projectVendor);
        
        if(projectVendor.Id != null){
            selectedValue = projectVendor.Vendor_Category__c;
            findVendors();
            otherVendorsList.clear();
            otherVendorsList = getOtherVendors();
            if(otherVendorsList.isEmpty()){
                otherVendorsList.add(new OtherVendors(''));
            }
        }
    }
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this method to get Vendor Category
    **/
    public List<SelectOption> getCategoryOptions() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('-SELECT-','-SELECT-'));
        for(Vendor_Category__c options1 : [select id,Name from Vendor_Category__c]) {
            options.add(new SelectOption(options1.Name,options1.Name));
            vcMap.put(options1.Name, options1);
            system.debug('-----Available Vendor Categories-------'+options);
            }
        options.sort();    
        return options;
    }
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this method is regarding the Vendor Selection based on Vendor Category
    **/
    public PageReference findVendors(){
        list<SelectOption> availableVendorList = new list<SelectOption>();
        system.debug('selected value'+ selectedValue);
        selectedVendorsList.clear();
        availVendorsList.clear();
        otherPVSet.clear();
        List<Project_Vendors__c> pvList = PBB_DataAccessor.getProjectVendors(selectedValue , projectVendor.Bid_Project__c);
         for(Project_Vendors__c pv: pvList){
             vSet.add(pv.Vendorr__c);
             if(pv.Vendor_Name_Other__c != '')
             otherPVSet.add(pv.Vendor_Name_Other__c);
             
             if(pv.Vendorr__c != null)
             selectedVendorsList.add(new SelectOption(pv.Vendorr__c, pv.Vendorr__c));
         }
         for(Vendor__c v: [select id,name,Vendor_Category__c from Vendor__c where Vendor_Category__r.Name =: selectedValue and Active__c = true 
                           and name NOT IN: vSet]){
             availVendorsList.add(new SelectOption(v.Name,v.Name));
        }
        otherVendorsList.clear();
        otherVendorsList = getOtherVendors();
        if(otherVendorsList.isEmpty()){
            otherVendorsList.add(new OtherVendors(''));
        }
        return null;
    }
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this is the method to Save Project Vendor
    **/
     public PageReference savePV(){
         System.debug('*** Inside save ***');
         List<Project_Vendors__c> upsertPVList = new List<Project_Vendors__c>();
         Integer cntSame = 0;
         if(selectedValue == '-SELECT-'){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Please Select Vendor Category to Save '));
             return null;
         }
         if(selectedVendorsList.isEmpty() && otherVendorsList.isEmpty()){
             //Delete existing records
             List<Project_Vendors__c> pvList = PBB_DataAccessor.getProjectVendors(selectedValue , projectVendor.Bid_Project__c);
             delete pvList;
             System.debug('***  No value selected ***');
             //Redirect to bid project
             return redirect();
         }
         else{
             
             System.debug('*** else *** '+selectedVendorsList);
             
             for(SelectOption so: selectedVendorsList){
                 System.debug(vSet+' *** '+so.getValue());
                     Project_Vendors__c pv = new Project_Vendors__c();
                     pv.Bid_Project__c = projectVendor.Bid_Project__c;
                     pv.Vendorr__c = so.getValue();
                     pv.Vendor_Category__c = selectedValue;
                     upsertPVList.add(pv);
             }
             System.debug('*** other *** '+otherPVSet);
             Set<String> tempSet = new Set<String>();
             if(!otherVendorsList.isEmpty()){
                 System.debug('*** other not empty ***');
                 for(OtherVendors ov: otherVendorsList){
                     if(!String.isEmpty(ov.vendorName) && tempSet.contains(ov.vendorName)){
                         cntSame++;
                         System.debug('*** tempSet contains '+ov.vendorName);
                         tempSet.clear();
                         continue;
                     }
                     else{
                         if(!String.isEmpty(ov.vendorName)){
                             tempSet.add(ov.vendorName);
                             Project_Vendors__c pv = new Project_Vendors__c();
                             pv.Bid_Project__c = projectVendor.Bid_Project__c;
                             pv.Vendor_Category__c = selectedValue;
                             pv.Vendor_Name_Other__c = ov.vendorName;
                             upsertPVList.add(pv);
                         }
                     }                                                                         
                 }
             }
         }
             
         try{
             if(cntSame > 0){
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Duplicate value in Other Vendors, please enter only unique values'));
                 return null;
             }
             else{
                 //Delete existing records
                 List<Project_Vendors__c> pvList = PBB_DataAccessor.getProjectVendors(selectedValue , projectVendor.Bid_Project__c);
                 delete pvList;
                 
                 //Upsert new records
                 upsert upsertPVList;
                 
                 //Redirect to bid project
                 return redirect();
                 
             }
         }
         catch(Exception e){
             if(e.getMessage().contains('DUPLICATE_VALUE')){
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error: Duplicate value in Other Vendors, please enter only unique values'));
             }
             else{
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error while saving: '+e.getMessage()));
             }
             return null;
         }
     return null;
     }
        
        /**
       @author Niharika Reddy
       @date 2015
       @description this is the method to add other Vendor row
       **/
        public void addRow(){
            if(otherVendorsList.isEmpty()){
                otherVendorsList.add(new OtherVendors(''));
            }
            else{
                System.debug('in addRow '+otherVendorsList.size());
                OtherVendors inr = otherVendorsList[(otherVendorsList.size()-1)];
                otherVendorsList.add(new OtherVendors(''));
                System.debug('after addin row: '+otherVendorsList.size());
            }
        }
        
        /**
       @author Niharika Reddy
       @date 2015
       @description this is the method to delete Other Vendor row
       **/
        public void delRow(){
            rowNum = Integer.valueOf(apexpages.currentpage().getparameters().get('index'));
            OtherVendors inrObj = otherVendorsList[rowNum];
            otherVendorsList.remove(rowNum); 
        }
        
         /**
        @author Niharika Reddy
        @date 2015
        @description this method is to get other Vendors
        **/
        public List<OtherVendors> getOtherVendors(){
             List<Project_Vendors__c> pvList = PBB_DataAccessor.getProjectVendors(selectedValue , projectVendor.Bid_Project__c);
             List<OtherVendors> otherVendorsLst = new List<OtherVendors>();
             otherVendorsLst.clear();
             for(Project_Vendors__c pv: pvList){
                 System.debug('*** oooo *** '+pv.Vendor_Name_Other__c);
                 if((pv.Vendor_Name_Other__c != null) && (pv.Vendor_Name_Other__c != 'null') && (!String.isEmpty(pv.Vendor_Name_Other__c)) && (pv.Vendor_Name_Other__c != '')){
                     System.debug('*** dddd *** '+pv.Vendor_Name_Other__c);
                     otherVendorsLst.add(new OtherVendors(pv.Vendor_Name_Other__c));
                 }
                 else{
                     continue;
                 }
             }
             return otherVendorsLst;
        }
        
         /**
        @author Niharika Reddy
        @date 2015
        @description this method to redirect user when he clicks cancel button
        **/
        public pageReference Cancel(){
        String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + projectVendor.Bid_Project__c ;
        PageReference returnPage = new PageReference(baseURL);
        returnPage.setRedirect(true);    
        return returnPage;
    }
        /**
       @author Niharika Reddy
       @date 2015
       @description this method is to Redirect user to bid project page
       **/
       public PageReference redirect(){
         String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + projectVendor.Bid_Project__c ;
         PageReference redirectPage = new PageReference(baseURL);
         redirectPage.setRedirect(true);    
         return redirectPage;
    }

}