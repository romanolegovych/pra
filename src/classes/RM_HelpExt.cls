public with sharing class RM_HelpExt {
    private Help__c hlp;
    
    public RM_HelpExt(ApexPages.StandardController stdController){ 
        // this.hlp = (Help__c)stdController.getRecord();
         if (ApexPages.currentPage().getParameters().get('name') != null && ApexPages.currentPage().getParameters().get('name') != ''){  
            
             list<Help__c> helps = RM_OtherService.GetHelpContent(ApexPages.currentPage().getParameters().get('name'));
             if (helps.size() > 0)
                hlp = helps[0];
             else 
                hlp =  new Help__c(Content__c='Help is under construction!');
        
         }
    }
    public Help__c getHelp(){
        return hlp;
    }
}