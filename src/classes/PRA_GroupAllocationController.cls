public class PRA_GroupAllocationController {
 
    //Project related variables
    public String projectText{get;set;} 
    public String selectedProjectLabel  {get;set;}
    public client_project__c cp{get;set;}
    
    ///Regions
    public List<region__c > PRARegionList{get;set;}
    
    //Group related variables
    public String GroupName{get;set;}//Create/Edit Group.
    public String HiddenGroupName{get;set;} 
    public string SelectedDeletedGroup { get; set; }//For deleting Group.
    public String saveandshowNewGroup{get;set;}//for saving through save and new button.
    
    public List<String> Groupslist{get;set;}//group Lists
    public List<TaskWrapper> NeworEditGrouplist{get;set;}//Group Client tasks lists 
    public Map<string,List<Group_Client_Tasks__c>> GroupClientTasksmap{get;set;} //Group Client tasks Map    
    public List<List<Group_client_tasks__c>> GroupClientTasksList{get;set;}//Group Client tasks list of list
    
    //Client task ID's in Group Task object
    public Set<ID> ClientTaskID{get;set;}
        
    //Totals per regions
    public Map<string,Decimal> AdjustmentsperRegion{get;set;}
    public Decimal TotalAdjustment{get;set;}    
    
    //Save error and for deserialized selected client tasks
    public string SaveError{get;set;}
    public string selectedClienttasks{get;set;}
    
    //User Preferences  
    private Integer searchCount{get;set;}
    private PRA_Utils.UserPreference userPreference{get;private set;}
    public static final String ST_NONE = 'None';
    
    //Picklist for the PRA regions in pageblocktable
    public List<selectOption> options=new List<selectOption>();    
    public List<selectOption> getRegionnames(){
        options.clear();
        for(region__c obj:PRARegionList){        
            options.add(new selectOption(obj.id,obj.Region_Name__c));
        }
        return options;
    }
     
    //Constructor 
    public PRA_GroupAllocationController () { 
    
        userPreference = PRA_Utils.retrieveUserPreference();
        Id projectid;
        searchCount = -1;
        selectedProjectLabel = 'Home';
        TotalAdjustment=0;
        Groupslist=new List<String>();
        
        //Retreving User Preferences
        if(userPreference != null) {
            projectid = userPreference.projectId; 
            List<Client_Project__c> clientProjectsList = [SELECT Name, Is_Project_Locked__c, Migrated_Date__c FROM Client_Project__c WHERE id = :projectid and Load_Status__c != 'New'];           
            if(clientProjectsList.size() == 1) {               
                projectText = clientProjectsList.get(0).Name; 
                selectedProjectLabel = clientProjectsList.get(0).Name;             
                MainSearch();
            }  else {
                searchCount++;                
            }  
        }
    }    
     
    //Main Search    
    public void MainSearch() {     
        searchforGroups();
    }
        
    //reset
    public void Reset() {
        PRA_Utils.deleteUserPreference();
        MainSearch();
        selectedProjectLabel = 'Home';    
    }  
    
    //Search for All Groups and its associated Client tasks for the selected project.
    public void searchforGroups(){
        
        searchCount++; 
        GroupName= null;
        HiddenGroupName=null;
        TotalAdjustment=0;      
        
        cp=new client_project__c();
        Groupslist=new List<String>();
        ClientTaskID=new Set<ID>();     
        NeworEditGrouplist =null;
        
        GroupClientTasksmap=new Map<string,List<Group_Client_Tasks__c>>();        
        GroupClientTasksList=new List<List<Group_Client_Tasks__c>>();       
        AdjustmentsperRegion=new Map<string,Decimal>();
        
        
        if(projectText!=null && projectText!=''){           
            //Client Project
            for(client_project__c cpc:[select id,name from client_project__c where name=:projectText limit 1]){
               cp=cpc;              
            }
            //PRA Regions
            PRARegionList=[select Id,Region_Name__c from region__c where Region_Name__c!='Global' ];
            //User Preferences save
            if(cp.name != null ) {
                if(userPreference != null ) {
                    userPreference.projectId = cp.Id;
                    selectedProjectLabel = cp.name;
                } else {                 
                    userPreference = new PRA_Utils.UserPreference();
                    userPreference.projectId = cp.Id;
                    userPreference.regionId = new String[]{ST_NONE};
                    userPreference.countryId = new String[]{ST_NONE};
                    userPreference.clientUnitId = new String[]{ST_NONE};
                    userPreference.clientUnitGroupId = new String[]{ST_NONE};
                    userPreference.operationalAreaId = new String[]{ST_NONE};
                    userPreference.unconfirmedOnly = false;
                    userPreference.confirmedOnly = false;
                    userPreference.crctId = '';
                    userPreference.userId = UserInfo.getUserId();
                    selectedProjectLabel = cp.name;
                    searchCount++;                
                }   
                if(searchCount > 0) {
                    // Save user prefs
                    PRA_Utils.saveUserPreference(userPreference);
                } 
            } 
            //Search for Groups and Group Client Tasks
            for(Group__c gcc:   [select id,name,(select id,name,group__r.name,Client_Task__c,Description__c,Region__c,PRA_Region__c,Client_Unit_Number__c,Contracted_Units__c,Contracted_Unit_Price__c,Contracted_Value__c,
                                                 ETC_Units__c,CON_BDG_Units__c,CON_BDG_Value__c,Unit_Variance_from_Contract__c,Contract_Value_Variance__c,
                                                 Regional_of_Unit_Forecast__c,Adjustment_to_Regional_CV__c,Revised_Regional_Contract_Value__c,Revised_Unit_Price__c,                                            
                                                 
                                                 //Group Totals
                                                 group__r.Total_Contracted_Unit_Price__c,group__r.Total_Contracted_Value__c,group__r.Total_Contracted_Units__c,                                             
                                                 group__r.Total_ETC_Units__c,group__r.Total_CON_BDG_Units__c,group__r.Total_CON_BDG_Value__c,group__r.Total_Unit_Variance_from_Contract__c,
                                                 group__r.Total_Contracted_Value_Variance__c,group__r.Total_Regional_of_Unit_Forecast__c,group__r.Total_Adjustment_to_Regional_CV__c,
                                                 group__r.Total_Revised_Regional_Contract_Value__c,group__r.Total_Revised_Unit_Price__c,
                                                
                                                //Global Bill rate
                                                 group__r.GBR_Contracted_Unit_Price__c,group__r.GBR_Contract_value_Variance__c,group__r.GBR_ETC_Units__c                                            
                                                 
                                                 from Group_Client_Tasks__r 
                                                 order by Client_Unit_Number__c) c 
                                from group__c 
                                where client_project__c=:cp.id
                                order by name]){
                                                   
            
                //system.debug('=====gcc======='+gcc+gcc.Group_Client_Tasks__r);            
                GroupClientTasksList.add(gcc.Group_Client_Tasks__r);//Adding all GCT to List<List>.
                
                //Looping through the Group Clinet tasks
                for(Group_Client_Tasks__c ct:gcc.Group_Client_Tasks__r){                     
                    if(!GroupClientTasksmap.containsKey(ct.Group__r.name)) {
                        GroupClientTasksmap.put(ct.Group__r.name, new List<Group_Client_Tasks__c>());
                        Groupslist.add(ct.Group__r.name);  
                    }              
                    GroupClientTasksmap.get(ct.Group__r.name).add(ct);                  
                    ClientTaskID.add(ct.Client_Task__c);
                    //system.debug('=====GroupClientTasksmap======='+GroupClientTasksmap);
                    
                    //Getting adjustments for regions
                    if(!AdjustmentsperRegion.containsKey(ct.PRA_Region__c)) {
                        AdjustmentsperRegion.put(ct.PRA_Region__c,ct.Adjustment_to_Regional_CV__c);                       
                    }else{
                        AdjustmentsperRegion.put(ct.PRA_Region__c,Double.valueof(AdjustmentsperRegion.get(ct.PRA_Region__c))+ct.Adjustment_to_Regional_CV__c);   
                    }
                    TotalAdjustment+=ct.Adjustment_to_Regional_CV__c;                 
                }
            }
            //system.debug('=====GroupClientTasksList======='+GroupClientTasksList);
        }    
    }   
    
    
    //Create New Group for the Project.
    public void NewGroup(){
        GroupName=null; 
        HiddenGroupName=null; 
        selectedClienttasks='';  
        NeworEditGrouplist = new List<TaskWrapper>();
        //Get all the remaing client tasks which are not part of Group.
        for(Client_Task__c ct:  [select id,name,client_unit_number__c,combo_code__c, description__c,
                                    Total_Contract_Units__c,Contract_Value__c,Total_Worked_Units__c,Total_Forecast_Units__c,
                                    LTD_Contracted_BDG_Unit__c,Rem_Contracted_BDG_Unit__c
                                from Client_Task__c 
                                where Project__r.name=:projectText 
                                and id NOT IN:ClientTaskID 
                                and client_unit_number__c!='Migrated Worked Hours' 
                                order by client_unit_number__c]){
                                   
            TaskWrapper tw      = new TaskWrapper();
            tw.taskId           = ct.id;
            tw.clientUnitNumber = ct.client_unit_number__c;          
            tw.description      = ct.description__c;             
            tw.selected         = false;
            tw.ContractedUnits  = ct.Total_Contract_Units__c;
            tw.ContractedValue  = ct.Contract_Value__c;
            tw.ETCUnits         = ct.Total_Worked_Units__c+ct.Total_Forecast_Units__c;          
            tw.CBGUnits         = ct.LTD_Contracted_BDG_Unit__c+ct.Rem_Contracted_BDG_Unit__c;
            
            NeworEditGrouplist.add(tw);
        }      
        //system.debug('---------NeworEditGrouplist----------'+NeworEditGrouplist);    
    }
    
    //Edit Existing Group for the Project.
    public void EditGroup(){ 
    
        if(GroupName!=null ){ 
             String Gname=GroupName;
             NewGroup();
             GroupName=Gname;
             HiddenGroupName=GroupName;
             
             //NeworEditGrouplist = GroupClientTasksmap.get(GroupName);
             //system.debug('=====GroupClientTasksList======='+GroupClientTasksmap+GroupName+Gname);
            for(Group_client_tasks__c ct:GroupClientTasksmap.get(GroupName)){
                  TaskWrapper tw      = new TaskWrapper();
                  tw.GtaskId          = ct.id;
                  tw.taskId           = ct.Client_task__c;
                  tw.clientUnitNumber = ct.client_unit_number__c;          
                  tw.description      = ct.description__c;
                  //tw.clientRegion     = ct.project_region__r.name;
                  tw.clientRegion     = ct.Region__c;
                  tw.selected         = true;
                  selectedClienttasks+= ct.client_unit_number__c+'___'+tw.clientRegion+'===';//this is to hold the selected once to display in UI.
                  NeworEditGrouplist.add(tw);
            }
        }
        //system.debug('---------NeworEditGrouplist----------'+NeworEditGrouplist);    
    }
    
    //Deleting the Group
    public void DeleteGroup(){
        //system.debug('======SelectedDeletedGroup ======'+SelectedDeletedGroup );
        if(SelectedDeletedGroup!=null){ 
            Group__c gc=[select id from Group__c where name=:SelectedDeletedGroup and Client_Project__r.name=:cp.name  limit 1];
            delete gc;          
            searchforGroups(); 
        }      
    } 
    
    //Save and New Group button
    public void SaveandNewGroup(){
        saveandshowNewGroup='no';
        saveNewGroup();
        if(saveandshowNewGroup=='Yes'){
           NewGroup();          
        }
    }   
    
    //Save New or Edit Group
    public void saveNewGroup(){
        SaveError=null;
        if(GroupName!=null){
            //saving a New group
            
            //Deserializing the list from the UI(used this bcoz the JQuery datatable is behaving weird             
            Set<String> TaskIdsSet = (Set<String>)JSON.deserialize(selectedClienttasks, Set<String>.class);   
            system.debug('------TaskIdsSet--------------'+TaskIdsSet);
            Map<String,String> ClientUnitswithRegions =new Map<String,String>();
            for(String s:TaskIdsSet){
               if(s!=null && s!=''){
                   string [] ss=s.split('___', 0);                   
                   ClientUnitswithRegions.put(ss[0],ss[1]);
               }
           
            }
            //system.debug('------ClientUnitswithRegions--------------'+ClientUnitswithRegions);
            if(!GroupClientTasksmap.containsKey(GroupName)){           
                try{
                    Group__c gc=new Group__c();
                    gc.name=GroupName;
                    gc.client_project__c=cp.id;
                    insert gc;                 
                    //system.debug('---------GroupName---------'+GroupName+NeworEditGrouplist);                    
                    List<Group_client_tasks__c> SaveGrouptasks=new List<Group_client_tasks__c>();
                    Set<String> Uniqueregion=new set<string>();
                    Integer CountSelectedTasks=0;
                    Integer CountRegions=0;
                    for(TaskWrapper tw:NeworEditGrouplist){
                    
                        //system.debug('-------tw------------'+tw.clientUnitNumber+'-----'+tw.selected);                     
                        if(ClientUnitswithRegions.containsKey(String.Valueof(tw.clientUnitNumber))){
                           //setting the selected values from UI
                           tw.selected=true;   
                           tw.clientRegion= ClientUnitswithRegions.get(String.Valueof(tw.clientUnitNumber));
                           //system.debug('-------tw---1---------'+tw.clientUnitNumber+'-----'+tw.selected+'====='+tw.clientRegion);
                        }else{
                           tw.selected=false;  
                           //system.debug('-------tw----2--------'+tw.clientUnitNumber+'-----'+tw.selected+'====='+tw.clientRegion);                  
                        } 
                        //Picking only selected once.
                        if(tw.selected==true){
                            Group_client_tasks__c ct=new Group_client_tasks__c();
                            //ct.id=tw.taskId;
                            ct.client_task__c=tw.taskId;
                            ct.Group__c=gc.id;
                            ct.Region__c=tw.clientRegion;
                            ct.Contracted_Units__c=tw.ContractedUnits;
                            ct.Contracted_Value__c=tw.ContractedValue ;
                            ct.ETC_Units__c=tw.ETCUnits;
                            ct.CON_BDG_Units__c=tw.CBGUnits;                             
                            CountSelectedTasks+=1;
                            SaveGrouptasks.add(ct);
                            if(!Uniqueregion.contains(tw.clientRegion)) {                           
                                Uniqueregion.add(tw.clientRegion);
                                //CountRegions+=1;
                            } else{
                                 CountRegions+=1;
                            }
                            //system.debug('---------Uniqueregion---------'+CountRegions+Uniqueregion);
                        }
                    }
                    
                    //Verifying if they selected more or less or duplicate regions.
                    if( CountSelectedTasks>=2 && CountSelectedTasks<=4 && CountRegions==0 ){                   
                         insert   SaveGrouptasks;
                         saveandshowNewGroup='Yes';
                         NeworEditGrouplist.clear();
                         NeworEditGrouplist=null;
                         UpdateGroupClientTasks();
                         searchforGroups();
                    }
                    else{ 
                        delete gc;//Doing Rollback
                        SaveError='error';
                        if(CountSelectedTasks<2)                 
                            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Error, 'Selected less than two client tasks in a group.'));
                        else{
                            if(CountSelectedTasks>4)
                                Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Error, 'Selected more than 4 client tasks in a group.'));
                            else
                                Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Error, 'Duplicate regions selected in a group.'));                         
                        
                        }
                    }
                    // system.debug('---------CountSelectedTasks------'+CountSelectedTasks);
                }catch(Exception ex) {
                    Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Error, ex.getMessage()));
                }
            }else{
                //saving the existing groups
                List<Group_client_tasks__c> UpdateGrouptasks=new List<Group_client_tasks__c>();
                List<Group_client_tasks__c> RemoveGrouptasks=new List<Group_client_tasks__c>();
                Group__c gc=[select id,name from Group__c where name=:GroupName and client_project__r.name=:projecttext];
                Integer CountSelectedTasks=0;
                Set<String> Uniqueregion=new set<string>();
                Integer CountRegions=0;
                
                for(TaskWrapper tw:NeworEditGrouplist){                    
                    
                    //system.debug('-------tw------------'+tw.clientUnitNumber+'-----'+tw.selected+'====='+tw.clientRegion);                
                    if(ClientUnitswithRegions.containsKey(String.Valueof(tw.clientUnitNumber))){
                        //setting the selected values from UI
                        tw.selected=true;   
                        tw.clientRegion= ClientUnitswithRegions.get(String.Valueof(tw.clientUnitNumber));
                        //system.debug('-------tw---1---------'+tw.clientUnitNumber+'-----'+tw.selected+'====='+tw.clientRegion);
                    }else{
                        tw.selected=false;  
                         //system.debug('-------tw----2--------'+tw.clientUnitNumber+'-----'+tw.selected+'====='+tw.clientRegion);                  
                    } 
                                   
                    if(tw.selected==true){
                        if(tw.GtaskId==null){
                             Group_client_tasks__c ct=new Group_client_tasks__c();
                             //ct.id=tw.taskId;
                             ct.client_task__c=tw.taskId;
                             ct.Group__c=gc.id;
                             ct.Region__c=tw.clientRegion;
                             
                             ct.Contracted_Units__c=tw.ContractedUnits;
                             ct.Contracted_Value__c=tw.ContractedValue ;
                             ct.ETC_Units__c=tw.ETCUnits;
                             ct.CON_BDG_Units__c=tw.CBGUnits;
                             UpdateGrouptasks.add(ct);
                        }else{
                             Group_client_tasks__c ct=new Group_client_tasks__c();
                             ct.id=tw.GtaskId;
                             ct.client_task__c=tw.taskId;
                             ct.Region__c=tw.clientRegion;
                             //ct.Group__c=gc.id;
                             
                             ct.Contracted_Units__c=tw.ContractedUnits;
                             ct.Contracted_Value__c=tw.ContractedValue ;
                             ct.ETC_Units__c=tw.ETCUnits;
                             ct.CON_BDG_Units__c=tw.CBGUnits;
                             UpdateGrouptasks.add(ct);
                        }
                        CountSelectedTasks+=1;
                        if(!Uniqueregion.contains(tw.clientRegion)) {                           
                            Uniqueregion.add(tw.clientRegion);
                            //CountRegions+=1;
                        } else{
                            CountRegions+=1;
                        }
                    }else{
                        //Deleting the unselected once.
                        if(tw.GtaskId!=null){
                           Group_client_tasks__c ct=new Group_client_tasks__c();
                           ct.id=tw.GtaskId;
                           RemoveGrouptasks.add(ct);
                        }                 
                    }
                }
                //Verifying if they selected more or less or duplicate regions.
                if(CountSelectedTasks>=2 && CountSelectedTasks<=4 && CountRegions==0 ){
                    Upsert   UpdateGrouptasks;
                    Delete  RemoveGrouptasks;
                    saveandshowNewGroup='Yes';
                    NeworEditGrouplist.clear();
                    NeworEditGrouplist=null;
                    //Updating all the Group client tasks and groups with current values
                    UpdateGroupClientTasks();
                    searchforGroups();
                }
                else{
                SaveError='error';                                      
                    if(CountSelectedTasks<2)                 
                        Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Error, 'Selected less than two client tasks in a group.'));
                    else{
                        if(CountSelectedTasks>4)
                            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Error, 'Selected more than 4 client tasks in a group.'));
                        else
                            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Error, 'Duplicate regions selected in a group.'));                         
                        
                    }
                } 
            }
        }    
    }
    
    //Updating the Group tasks and Groups after saving any group.
    public void UpdateGroupClientTasks(){ 
        try{
            list<Group_client_tasks__c> gct=new list<Group_client_tasks__c> ();
            for(Group_client_tasks__c gt:   [select id,name,Client_Task__c,Contracted_Value__c,Contracted_Units__c,ETC_Units__c,CON_BDG_Units__c,
                                                Client_Task__r.Total_Contract_Units__c,Client_Task__r.Contract_Value__c,Client_Task__r.Total_Worked_Units__c,Client_Task__r.Total_Forecast_Units__c,
                                                Client_Task__r.LTD_Contracted_BDG_Unit__c,Client_Task__r.Rem_Contracted_BDG_Unit__c
                                           
                                            from  Group_client_tasks__c 
                                            where Group__r.client_project__c=:cp.id]){
                                           
                        gt.Contracted_Units__c= gt.Client_Task__r.Total_Contract_Units__c;
                        gt.Contracted_Value__c= gt.Client_Task__r.Contract_Value__c;
                        gt.ETC_Units__c= gt.Client_Task__r.Total_Worked_Units__c+gt.Client_Task__r.Total_Forecast_Units__c;          
                        gt.CON_BDG_Units__c= gt.Client_Task__r.LTD_Contracted_BDG_Unit__c+gt.Client_Task__r.Rem_Contracted_BDG_Unit__c;
                        
                        gct.add(gt);            
            }          
            update gct;
          
            List<group__c> groups=new List<group__c>();
            for(AggregateResult gct1 :  [select group__c,sum(Adjustment_to_Regional_CV__c) TAR,sum(Regional_of_Unit_Forecast__c) TRUF ,
                                            sum(Revised_Regional_Contract_Value__c) TRRCV,sum(Revised_Unit_Price__c) TRUP
                                        from Group_client_tasks__c where Group__r.client_project__c=:cp.id
                                        group   by  group__c
                                        order by  group__c]) {
                    group__c gc=new group__c();
                    system.debug('-------gct1)--------'+gct1+gct1.get('group__c'));
                    gc.id=String.valueof(gct1.get('group__c'));
                    gc.Total_Adjustment_to_Regional_CV__c=Double.valueof(gct1.get('TAR'));
                    gc.Total_Regional_of_Unit_Forecast__c=Double.valueof(gct1.get('TRUF'));
                    gc.Total_Revised_Regional_Contract_Value__c= Double.valueof(gct1.get('TRRCV'));
                    gc.Total_Revised_Unit_Price__c= Double.valueof(gct1.get('TRUP'));                      
                    groups.add(gc);                    
            }           
            update groups;
       
        }catch(Exception ex) {
        SaveError='error';  
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.Error, ex.getMessage()));
        }    
    }
    
    //Recalculate the grid values     
    public void RecalculateGroupTasks(){
         UpdateGroupClientTasks();
         searchforGroups();
         
    }
    
    //Export Groups 
    public PageReference exporttoExcel(){
        PageReference pageRef=new PageReference('/apex/PRA_GroupAllocationCSV');
        pageRef.setRedirect(false);
        return pageRef; 
    }
    
    
    // ---------- wrapers --------
    public class TaskWrapper {
        public Id taskId                {get;set;}
        public Id GtaskId                {get;set;}
        public String clientUnitNumber  {get;set;}
        public String description       {get;set;}
        public String clientRegion      {get;set;}       
        public Boolean selected         {get;set;} 
        
        public Decimal ContractedUnits  {get;set;} 
        public Decimal ContractedValue  {get;set;}
        public Decimal ETCUnits         {get;set;} 
        public Decimal CBGUnits         {get;set;}         
        
        public TaskWrapper() {            
        }
        
    }
}