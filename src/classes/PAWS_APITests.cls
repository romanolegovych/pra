/**
*   'PAWS_APITests' is the test class for PAWS_API
*   Test classes are used to write the unit test and cover the code coverage of the code.
*   @author   Work Relay
*/
@isTest 
public with sharing class PAWS_APITests 
{
    static testmethod void runActivation()
    {
        PAWS_ApexTestsEnvironment.init();
        
        STSWR1__Flow__c template = new STSWR1__Flow__c(Name='Test Template', STSWR1__Type__c='Template', STSWR1__Object_Type__c='ecrf__c', STSWR1__Status__c = 'Active');
        insert template;
        
        STSWR1__Item__c templates = new STSWR1__Item__c(Name='Test Templates');
        insert templates;
        
        STSWR1__Item__c templateFolder = [select STSWR1__Parent__c from STSWR1__Item__c where STSWR1__Source_Flow__c = :template.Id];
        templateFolder.STSWR1__Parent__c = templates.Id;
        update templateFolder;

        PAWS_ApexTestsEnvironment.ProjectFlow.Flow_Instance__c = null;
        update PAWS_ApexTestsEnvironment.ProjectFlow;
        
        PAWS_ApexTestsEnvironment.Flow.STSWR1__Parent__c = template.Id;
        PAWS_ApexTestsEnvironment.Flow.STSWR1__Object_Type__c = 'ecrf__c';
        update PAWS_ApexTestsEnvironment.Flow;
        
        STSWR1__Item__c flowFolder = [select STSWR1__Parent__c from STSWR1__Item__c where STSWR1__Source_Flow__c = :PAWS_ApexTestsEnvironment.Flow.Id];
        flowFolder.STSWR1__Parent__c = PAWS_ApexTestsEnvironment.ProjectFlow.Folder__c;
        update flowFolder;

        STSWR1__Item__c countries = new STSWR1__Item__c(Name='Countries', STSWR1__Parent__c=templates.Id);
        insert countries;
        
        STSWR1__Item__c generic = new STSWR1__Item__c(Name='Generic', STSWR1__Parent__c=countries.Id);
        insert generic;
        
        STSWR1__Flow__c countryTemplate = new STSWR1__Flow__c(Name='Country Template', STSWR1__Type__c='Template', STSWR1__Object_Type__c='paws_project_flow_country__c', STSWR1__Status__c = 'Active');
        STSWR1__Flow__c siteTemplate = new STSWR1__Flow__c(Name='Site Template', STSWR1__Type__c='Template', STSWR1__Object_Type__c='paws_project_flow_site__c', STSWR1__Status__c = 'Active');
        STSWR1__Flow__c agreementTemplate = new STSWR1__Flow__c(Name='Agreement Template', STSWR1__Type__c='Template', STSWR1__Object_Type__c='paws_project_flow_agreement__c', STSWR1__Status__c = 'Active');
        STSWR1__Flow__c documentTemplate = new STSWR1__Flow__c(Name='Document Template', STSWR1__Type__c='Template', STSWR1__Object_Type__c='paws_project_flow_document__c', STSWR1__Status__c = 'Active');
        STSWR1__Flow__c submissionTemplate = new STSWR1__Flow__c(Name='Submission Template', STSWR1__Type__c='Template', STSWR1__Object_Type__c='paws_project_flow_submission__c', STSWR1__Status__c = 'Active');
        List<STSWR1__Flow__c> childFlows = new List<STSWR1__Flow__c>{countryTemplate, siteTemplate, agreementTemplate, documentTemplate, submissionTemplate};
        insert childFlows;
        
        STSWR1__Flow_Step_Junction__c countryStep = new STSWR1__Flow_Step_Junction__c(Name='Country Step', STSWR1__Flow__c=countryTemplate.Id, STSWR1__Is_First_Step__c=true);
        STSWR1__Flow_Step_Junction__c siteStep = new STSWR1__Flow_Step_Junction__c(Name='Site Step', STSWR1__Flow__c=siteTemplate.Id, STSWR1__Is_First_Step__c=true);
        STSWR1__Flow_Step_Junction__c agreementStep = new STSWR1__Flow_Step_Junction__c(Name='Agreement Step', STSWR1__Flow__c=agreementTemplate.Id, STSWR1__Is_First_Step__c=true);
        STSWR1__Flow_Step_Junction__c documentStep = new STSWR1__Flow_Step_Junction__c(Name='Document Step', STSWR1__Flow__c=documentTemplate.Id, STSWR1__Is_First_Step__c=true);
        STSWR1__Flow_Step_Junction__c submissionStep = new STSWR1__Flow_Step_Junction__c(Name='Submission Step', STSWR1__Flow__c=submissionTemplate.Id, STSWR1__Is_First_Step__c=true);
        insert new List<STSWR1__Flow_Step_Junction__c>{countryStep, siteStep, agreementStep, documentStep, submissionStep};
        
        STSWR1__Flow_Step_Property__c countryProperty = new STSWR1__Flow_Step_Property__c(Name='Connection Point', STSWR1__Flow_Step__c=countryStep.Id, STSWR1__Value__c=PAWS_ApexTestsEnvironment.Flow.Name + ':' + PAWS_ApexTestsEnvironment.FlowStep.Name);
        STSWR1__Flow_Step_Property__c siteProperty = new STSWR1__Flow_Step_Property__c(Name='Connection Point', STSWR1__Flow_Step__c=siteStep.Id, STSWR1__Value__c=countryTemplate.Name + ':' + countryStep.Name);
        STSWR1__Flow_Step_Property__c agreementProperty = new STSWR1__Flow_Step_Property__c(Name='Connection Point', STSWR1__Flow_Step__c=agreementStep.Id, STSWR1__Value__c=siteTemplate.Name + ':' + siteStep.Name);
        STSWR1__Flow_Step_Property__c documentProperty = new STSWR1__Flow_Step_Property__c(Name='Connection Point', STSWR1__Flow_Step__c=documentStep.Id, STSWR1__Value__c=siteTemplate.Name + ':' + siteStep.Name);
        STSWR1__Flow_Step_Property__c submissionProperty = new STSWR1__Flow_Step_Property__c(Name='Connection Point', STSWR1__Flow_Step__c=submissionStep.Id, STSWR1__Value__c=siteTemplate.Name + ':' + siteStep.Name);
        insert new List<STSWR1__Flow_Step_Property__c>{countryProperty, siteProperty, agreementProperty, documentProperty, submissionProperty};
        
        List<STSWR1__Item__c> childFlowItems = [select STSWR1__Parent__c from STSWR1__Item__c where STSWR1__Source_Flow__c in :childFlows];
        for (STSWR1__Item__c item : childFlowItems)
        {
            item.STSWR1__Parent__c = generic.Id;
        }
        update childFlowItems;
        
        Test.startTest();
        
        PAWS_Project_Flow_Country__c projectCountry = PAWS_ApexTestsEnvironment.ProjectCountry;
        projectCountry = ((List<PAWS_Project_Flow_Country__c>)PAWS_Utilities.makeQuery('PAWS_Project_Flow_Country__c', 'Id = \'' + projectCountry.Id + '\'', null))[0];
        List<ID> templatesIds = new PAWS_API().getTemplate(projectCountry);
        new PAWS_API().activateFlow(projectCountry.Id, templatesIds[0]);
        
        /*PAWS_Project_Flow_Site__c projectSite = PAWS_ApexTestsEnvironment.ProjectSite;
        projectSite = ((List<PAWS_Project_Flow_Site__c>)PAWS_Utilities.makeQuery('PAWS_Project_Flow_Site__c', 'Id = \'' + projectSite.Id + '\'', null))[0];
        new PAWS_API().activateFlow(projectSite.Id, new PAWS_API().getTemplate(projectSite));
        
        PAWS_Project_Flow_Agreement__c projectAgreement = PAWS_ApexTestsEnvironment.ProjectAgreement;
        projectAgreement = ((List<PAWS_Project_Flow_Agreement__c>)PAWS_Utilities.makeQuery('PAWS_Project_Flow_Agreement__c', 'Id = \'' + projectAgreement.Id + '\'', null))[0];
        new PAWS_API().activateFlow(projectAgreement.Id, new PAWS_API().getTemplate(projectAgreement));
        
        PAWS_Project_Flow_Document__c projectDocument = PAWS_ApexTestsEnvironment.ProjectDocument;
        projectDocument = ((List<PAWS_Project_Flow_Document__c>)PAWS_Utilities.makeQuery('PAWS_Project_Flow_Document__c', 'Id = \'' + projectDocument.Id + '\'', null))[0];
        new PAWS_API().activateFlow(projectDocument.Id, new PAWS_API().getTemplate(projectDocument));
        
        PAWS_Project_Flow_Submission__c projectSubmission = PAWS_ApexTestsEnvironment.ProjectSubmission;
        projectSubmission = ((List<PAWS_Project_Flow_Submission__c>)PAWS_Utilities.makeQuery('PAWS_Project_Flow_Submission__c', 'Id = \'' + projectSubmission.Id + '\'', null))[0];
        new PAWS_API().activateFlow(projectSubmission.Id, new PAWS_API().getTemplate(projectSubmission));*/
        
        System.Assert(PAWS_ApexTestsEnvironment.ProjectSite != null);
        System.Assert(PAWS_ApexTestsEnvironment.ProjectAgreement != null);
        System.Assert(PAWS_ApexTestsEnvironment.ProjectDocument != null);
        System.Assert(PAWS_ApexTestsEnvironment.ProjectSubmission != null);
        
        Test.stopTest();        
    }
    
    static testmethod void runCompleteParentStep()
    {
        STSWR1.AbstractTrigger.Disabled = true;
        PAWS_ApexTestsEnvironment.init();
        
        STSWR1__Flow_Instance__c flowInstance = new STSWR1__Flow_Instance__c(STSWR1__Flow__c=PAWS_ApexTestsEnvironment.Flow.Id, STSWR1__Is_Active__c=true, STSWR1__Object_Id__c=PAWS_ApexTestsEnvironment.Project.Id, STSWR1__Object_Name__c=PAWS_ApexTestsEnvironment.Project.Name);
        insert flowInstance;

        STSWR1.API wrapi = new STSWR1.API();
        STSWR1__Flow_Instance__c instance = (STSWR1__Flow_Instance__c)wrapi.call('WorkflowService', 'getFlowInstanceById', PAWS_ApexTestsEnvironment.FlowInstance.Id);
        instance.STSWR1__Parent__c = flowInstance.Id;
        instance.STSWR1__Lock_Parent_Step__c = PAWS_ApexTestsEnvironment.FlowInstanceCursor.STSWR1__Step__c;
        update instance;
        
        STSWR1__Flow_Instance_Cursor__c cursor = (STSWR1__Flow_Instance_Cursor__c)wrapi.call('WorkflowService', 'getFlowInstanceCursorById', PAWS_ApexTestsEnvironment.FlowInstanceCursor.Id);
        new PAWS_API().completeParentStep(PAWS_ApexTestsEnvironment.Project, cursor);   
    }

    static testmethod void runFlowProceedResult()
    {
        PAWS_ApexTestsEnvironment.init();
        
        STSWR1__Flow_Instance_Cursor__c cursor = PAWS_ApexTestsEnvironment.FlowInstanceCursor;
        PAWS_ApexTestsEnvironment.Flow.STSWR1__Object_Type__c = 'ecrf__c';
        update PAWS_ApexTestsEnvironment.Flow;
        
        PAWS_API.FlowProceedResult result = PAWS_API.proceedFlow(PAWS_ApexTestsEnvironment.Project.Id, PAWS_ApexTestsEnvironment.Flow.Id, 'Proceed', 'test');
        while(!result.isDone && result.errors.isEmpty())
        {
            PAWS_API.proceedFlow(result);
        }
        
        result = PAWS_API.proceedFlow(PAWS_ApexTestsEnvironment.Project.Id, PAWS_ApexTestsEnvironment.Flow.Id, 'Proceed', 'test');
        while(!result.isDone && result.errors.isEmpty())
        {
            PAWS_API.proceedFlow(result);
        }
    }
    
    static COUNTRY__c country;
    static WFM_employee_Allocations__c allocation;
    static WFM_employee_Allocations__c allocation2;
    static List<WFM_Location_Hours__c> hrs;
    static List<WFM_Employee_Assignment__c> assigns;  
    static List<WFM_Employee_Availability__c> avas;
    static Employee_Details__c employee;
    static WFM_Project__c project;
    static ecrf__c PAWSProjectFlow;
    static User user;
    static List<string> months;
    
    static void init(){
        
        country = new COUNTRY__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
        insert country;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        user = new User(Alias = 'standt', Email='standarduser@example.com', EmailEncodingKey='UTF-8', FirstName='Jim', LastName='Testing', 
            LanguageLocaleKey='en_US', LocaleSidKey='en_US', ProfileId = p.Id, TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg11212.com');
        insert user;       
                
        employee = new Employee_Details__c(COUNTRY_name__c = country.id, name='100', Employee_Unique_Key__c='100', First_Name__c='Jim', Last_Name__c='Testing',email_Address__c='jim@example.com', 
            Function_code__c='PR',Buf_Code__c='KCICR', Job_Class_Desc__c='A', Business_Unit__c='RDU',Business_Unit_Desc__c='Clinical Informatics', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'),
            FTE_Equivalent__c=0.5, location_code__c = 'TTT', User__c = user.id);
        insert employee;
        
        WFM_Client__c client = new WFM_Client__c(name='TestClient', Client_Name__c='TestClinet_Name', Client_Unique_Key__c='TestClient');
        insert client;
        
        Date projectEnd = Date.today().addMonths(10);
        
        project = new WFM_Project__c(name='TestProjectABC', wfm_client__c=client.id, Project_Unique_Key__c='TestProjectABC', project_status__c ='RM',  Status_Desc__c='Active', project_end_date__c = projectEnd);
        insert project;
        
        Date allEndDate = Date.Today().addMonths(6);
        allocation= new WFM_employee_Allocations__c(EMPLOYEE_ID__c=employee.id, Project_Buf_Code__c='KCICR',Is_Lead__c=true,Project_Country__c='United State',  Project_ID__c=project.id, Project_Function__c='Project Manager', 
            allocation_end_date__c =allEndDate,allocation_start_date__c =Date.Today(), Status__c='Confirmed', Allocation_Unique_Key__c=employee.name+project.name+'KCICRUnited State');
        insert allocation;
        
        allocation2 = new WFM_employee_Allocations__c(EMPLOYEE_ID__c=employee.id, Project_Buf_Code__c='KCICR',Is_Lead__c=true,Project_Country__c='United State',  Project_ID__c=project.id, Project_Function__c='Clinical Team Manager', 
            allocation_end_date__c =allEndDate,allocation_start_date__c =Date.Today(), Status__c='Confirmed', Allocation_Unique_Key__c=employee.name+project.name+'KCIUnited State');
        insert allocation2;
        
        months = RM_Tools.GetMonthsList(0, 6);
        hrs = new List<WFM_Location_Hours__c>();
        for (string month:months){
            WFM_Location_Hours__c hr = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + month,YEAR_MONTH__c=month,NBR_Business_Day_In_Month__c=22);
            hrs.add(hr); 
        }
        insert hrs; 
        
        avas = new List<WFM_Employee_Availability__c>();       
        for (Integer i = 0; i < months.size(); i++){            
            WFM_Employee_Availability__c ava = new WFM_Employee_Availability__c(EMPLOYEE_ID__c=employee.id, year_month__c=months[i], Availability_Unique_Key__c=employee.name+':'+months[i], 
            Location_Hours__c = hrs[i].id);
            avas.add(ava);
        }
        insert avas;
                
        assigns = new List<WFM_Employee_Assignment__c>();    
        for (Integer i = 0; i < months.size(); i++){    
            WFM_Employee_Assignment__c ass =  new WFM_Employee_Assignment__c(Assignment_Unique_Key__c=avas[i].id+(string)allocation.id, EMPLOYEE_ID__c = employee.id, Availability_FK__c=avas[i].id, Allocation_Key__c=allocation.id, Year_Month__c=months[i], 
            Assigned_unit__c='FTE', assigned_value__c = 0.1);            
            assigns.add(ass);
        }        
        insert assigns;
        
        PAWSProjectFlow = new ecrf__c(Project_ID__c = project.id);
        insert PAWSProjectFlow;
    }
    
    static testMethod void updateLookupIDsForPAWSProjectFlowsTest() {
        init();
        Test.startTest();
        PAWS_API.updateLookupIDsForPAWSProjectFlows(project.name, PAWSProjectFlow.id);
        Test.StopTest();
        PAWSProjectFlow = [SELECT Project_ID__c, Project_Manager__c, Clinical_Informatics_Manager__c, CTM__c, Study_Start_Up_Manager__c, Escalation_Executive__c
                            FROM ecrf__c WHERE Project_ID__r.Name = :project.name LIMIT 1];
        system.assertEquals(PAWSProjectFlow.Project_Manager__c, user.id);
    }
    
    static testMethod void updateLookupIDsForPAWSProjectFlowsSwimlanesTest() {
        init();
        STSWR1__Flow__c Flow = PAWS_ApexTestsEnvironment.Flow;
        STSWR1__Flow_Swimlane__c FlowSwimlane1 = new STSWR1__Flow_Swimlane__c();
        STSWR1__Flow_Swimlane__c FlowSwimlane2 = new STSWR1__Flow_Swimlane__c();
        FlowSwimlane1.STSWR1__Flow__c = Flow.id;
        FlowSwimlane1.Name = 'Project Manager';
        FlowSwimlane2.STSWR1__Flow__c = Flow.id;
        FlowSwimlane2.Name = 'Clinical Team Manager';        
        PAWSProjectFlow.Flow__c = Flow.id;
        insert FlowSwimlane1;
        insert FlowSwimlane2;
        update PAWSProjectFlow;
        Test.startTest();
        PAWS_API.updateLookupIDsForPAWSProjectFlowsSwimlanes(project.name, New List <String> {Flow.id});
        Test.StopTest();
        FlowSwimlane1 = [SELECT STSWR1__Assign_To__c, STSWR1__Assign_Type__c FROM STSWR1__Flow_Swimlane__c WHERE id = :FlowSwimlane1.id];
        FlowSwimlane2 = [SELECT STSWR1__Assign_To__c, STSWR1__Assign_Type__c FROM STSWR1__Flow_Swimlane__c WHERE id = :FlowSwimlane2.id];
        system.assertEquals(FlowSwimlane1.STSWR1__Assign_To__c, user.id);
        system.assertEquals(FlowSwimlane1.STSWR1__Assign_Type__c, 'User');
        system.assertEquals(FlowSwimlane2.STSWR1__Assign_To__c, user.id);        
    } 

    static testMethod void shouldGetActiveProjects()
    {
        ecrf__c pawsProject = setupTestData();
        Test.startTest();

        List<PAWS_ProjectCVO> projectCVOList = PAWS_API.PBB_getActiveProjects(pawsProject.Sponsor__c);
        System.assert(projectCVOList.size() == 1);
        System.debug(LoggingLevel.ERROR, 'projectCVOList ==> ' + JSON.serialize(projectCVOList));
    }

    static testMethod void shouldGetProjectDetails()
    {
        ecrf__c pawsProject = setupTestData();
        Test.startTest();

        PAWS_ProjectDetailCVO projectDetailOne = PAWS_API.PBB_getProjectDetails(pawsProject.ID);
        System.debug(LoggingLevel.ERROR, 'projectDetailOne ==> ' + JSON.serialize(projectDetailOne));
        System.assert(projectDetailOne.Countries.size() == 1);
        System.assert(projectDetailOne.Sites.size() == 1);

        List<PAWS_ProjectDetailCVO> projectDetailList = PAWS_API.PBB_getActiveProjectList(pawsProject.Sponsor__c);
        System.assert(projectDetailList.size() == 0);
        
        EW_Aggregated_Milestone__c siteActivationAggregatedMilestone = new EW_Aggregated_Milestone__c(Name='Site Activation', Goal__c=1, PAWS_Project_Flow__c=pawsProject.ID, Milestone_Target_Date__c=Date.today());
        projectDetailOne.updateActivationGoal(siteActivationAggregatedMilestone);
    }
    
    static testMethod void testPBBAPI()
    {
        ecrf__c pawsProject = setupTestData();
        ID flowId = [Select Id From STSWR1__Flow__c limit 1].get(0).Id;
        
        Test.startTest();
        
        List<PAWS_PBB_Helper.WorkItem> items = PAWS_API.PBB_getWorkBreakdown(pawsProject.Id);
        List<PAWS_PBB_Helper.WrModelStepWrapper> steps = PAWS_API.PBB_getWRModelSteps(flowId);
        
        Test.stopTest();
    }


    private static ecrf__c setupTestData()
    {
        PAWS_ApexTestsEnvironment.init();
        WFM_Client__c wfmClient = new WFM_Client__c(Name='Test WFM Client', Client_Name__c='TestClinet_Name',Client_Unique_Key__c='clnty_test_753');
        insert wfmClient;

        WFM_Contract__c wfmContract = new WFM_Contract__c(Client_ID__c=wfmClient.ID, Contract_Status__c='AA', Contract_Unique_Key__c='12cntt_key_test');
        insert wfmContract;

        WFM_Project__c wfmProject = new WFM_Project__c(Name='Test WFM Project', Project_Unique_Key__c='wfm_project_678', Contract_ID__c=wfmContract.ID, Project_Status__c ='RM', Status_Desc__c='Active');
        insert wfmProject;
        

        ecrf__c pawsProject = PAWS_ApexTestsEnvironment.Project;
        pawsProject.Project_ID__c = wfmProject.ID; 
        update pawsProject;

        pawsProject = [select Sponsor__c, Name from ecrf__c where id = :pawsProject.ID];

        PAWS_Project_Flow_Country__c projectCountry = PAWS_ApexTestsEnvironment.ProjectCountry;
        PAWS_Project_Flow_Site__c projectSite = PAWS_ApexTestsEnvironment.ProjectSite;

        STSWR1__Flow__c projectFlow = PAWS_ApexTestsEnvironment.Flow;
        projectFlow.STSWR1__Source_Id__c = pawsProject.ID;
        update projectFlow;

        STSWR1__Flow__c countryFlow = new STSWR1__Flow__c(Name='Country Flow', STSWR1__Type__c='Triggered by another flow', STSWR1__Object_Type__c='paws_project_flow_country__c', STSWR1__Status__c = 'Active', STSWR1__Source_Id__c=projectCountry.ID);
        STSWR1__Flow__c siteFlow = new STSWR1__Flow__c(Name='Site Flow', STSWR1__Type__c='Triggered by another flow', STSWR1__Object_Type__c='paws_project_flow_site__c', STSWR1__Status__c = 'Active', STSWR1__Source_Id__c=projectSite.ID);
        List<STSWR1__Flow__c> flows = new List<STSWR1__Flow__c> { countryFlow, siteFlow };
        insert flows;

        STSWR1__Flow_Step_Junction__c projectStep = new STSWR1__Flow_Step_Junction__c(Name='Project Step', STSWR1__Flow__c=projectFlow.Id, STSWR1__Is_First_Step__c=true);
        STSWR1__Flow_Step_Junction__c countryStep = new STSWR1__Flow_Step_Junction__c(Name='Country Step', STSWR1__Flow__c=countryFlow.Id, STSWR1__Is_First_Step__c=true);
        STSWR1__Flow_Step_Junction__c siteStep = new STSWR1__Flow_Step_Junction__c(Name='Site Activated', STSWR1__Flow__c=siteFlow.Id, STSWR1__Is_First_Step__c=true);
        insert new List<STSWR1__Flow_Step_Junction__c> { projectStep, countryStep, siteStep };

        STSWR1__Gantt_Step_Property__c siteGsnttStepProperty = new STSWR1__Gantt_Step_Property__c(STSWR1__Step__c=siteStep.Id, STSWR1__Revised_End_Date__c=Date.today(), Project__c=pawsProject.Id);
        insert siteGsnttStepProperty;

        EW_Aggregated_Milestone__c siteActivationAggregatedMilestone = new EW_Aggregated_Milestone__c(Name='Site Activation',Goal__c=1,PAWS_Project_Flow__c=pawsProject.ID,Milestone_Target_Date__c=Date.today());
        insert siteActivationAggregatedMilestone;

        Map<String, Object> config = new Map<String, Object> { 'flowId' => countryFlow.ID};
        STSWR1__Flow_Step_Action__c projectStepAction = new STSWR1__Flow_Step_Action__c(STSWR1__Config__c = JSON.serialize(config), STSWR1__Flow_Step__c = projectStep.ID, STSWR1__Type__c = 'Start Sub Flow');

        config = new Map<String, Object> { 'flowId' => siteFlow.ID};
        STSWR1__Flow_Step_Action__c countryAction = new STSWR1__Flow_Step_Action__c(STSWR1__Config__c = JSON.serialize(config), STSWR1__Flow_Step__c = countryStep.ID, STSWR1__Type__c = 'Start Sub Flow');

        insert new List<STSWR1__Flow_Step_Action__c> { projectStepAction, countryAction };

        STSWR1__Flow_Instance__c projectInstance = new STSWR1__Flow_Instance__c(STSWR1__Object_Id__c = pawsProject.ID, STSWR1__Object_Name__c = pawsProject.Name, STSWR1__Flow__c = projectFlow.ID, STSWR1__Is_Active__c = true);
        STSWR1__Flow_Instance__c projectSiteInstance = new STSWR1__Flow_Instance__c(STSWR1__Object_Id__c = projectSite.ID, STSWR1__Object_Name__c = projectSite.Name, STSWR1__Flow__c = siteFlow.ID, STSWR1__Is_Active__c = true);
        insert new List<STSWR1__Flow_Instance__c> { projectInstance, projectSiteInstance };

        STSWR1__Flow_Instance_Cursor__c projectCursor = new STSWR1__Flow_Instance_Cursor__c(STSWR1__Flow_Instance__c=projectInstance.Id, STSWR1__Step__c=projectStep.Id, STSWR1__Step_Assigned_To__c=UserInfo.getUserId(), STSWR1__Status__c='In Progress');
        STSWR1__Flow_Instance_Cursor__c projectSiteCursor = new STSWR1__Flow_Instance_Cursor__c(STSWR1__Flow_Instance__c=projectSiteInstance.Id, STSWR1__Step__c=siteStep.Id, STSWR1__Step_Assigned_To__c=UserInfo.getUserId(), STSWR1__Status__c='In Progress');
        insert new List<STSWR1__Flow_Instance_Cursor__c> { projectCursor, projectSiteCursor };

        STSWR1__Flow_Instance_History__c siteFlowInstanceHistory = new STSWR1__Flow_Instance_History__c(STSWR1__Cursor__c=projectSiteCursor.Id, STSWR1__Step__c=siteStep.Id, STSWR1__Status__c='In Progress');
        insert siteFlowInstanceHistory;

        return pawsProject;
    }
    @testvisible
    @istest private static void testAssociatePAWSProjectSiteWith()
    {
        for (AsyncApexJob pawsJob : [select Id from AsyncApexJob where ApexClass.Name = 'PAWS_Scheduler'])
        {
            System.abortJob(pawsJob.Id);
        }
        
        for (CronTrigger cronJob : [select Id from CronTrigger where CronJobDetailId in (select Id from CronJobDetail where Name = 'PAWS Scheduler')])
        {
            System.abortJob(cronJob.Id);
        }
        
        ecrf__c pawsProject = setupTestData();
        pawsProject = [select Project_ID__c from ecrf__c where Id = :pawsProject.Id limit 1];
        
        PAWS_Project_Flow_Country__c pawsCountry = [select Name from PAWS_Project_Flow_Country__c limit 1];
        
        WFM_Site_Detail__c wfmSite = new WFM_Site_Detail__c();
        for (WFM_Project__c wfmProject : [select Name from WFM_Project__c where Id = :pawsProject.Project_ID__c])
        {
            WFM_Protocol__c wfmProtocol = new WFM_Protocol__c(
                Name = 'Test WFM Project',
                Project_ID__c = wfmProject.Id,
                Protocal_Unique_Key__c = EncodingUtil.ConvertTohex(Crypto.GenerateAESKey(128)));
            
            insert wfmProtocol;
            
            PAWS_Project_Flow_Site__c pawsSite = [select PAWS_Country__c from PAWS_Project_Flow_Site__c limit 1];
            pawsSite.PAWS_Country__c = pawsCountry.Id;
            
            update pawsSite;
            
            
            wfmSite = new WFM_Site_Detail__c(Name = 'Test WFM Site', Project_Protocol__c = wfmProtocol.Id, Site_ID__c = EncodingUtil.ConvertTohex(Crypto.GenerateAESKey(128)), Country__c = pawsCountry.Name);
            insert wfmSite;
            
            PAWS_ApexTestsEnvironment.Project.Protocol_ID__c = wfmProtocol.Id;
            update PAWS_ApexTestsEnvironment.Project;
        }
        
        Test.startTest();
            PAWS_API.associatePAWSProjectSiteWith(wfmSite);
        Test.stopTest();
        
        System.assertEquals(1, [select count() from PAWS_Project_Flow_Site__c where WFM_Site__c != null]);
    }
    
    @istest(seealldata=true) private static void testResourcesError()
    {
        System.runAs([select Name from User where LastName = 'Mauer' and FirstName = 'Russell'].get(0))
        {
            List<sObject> records = [select Id, Name from STSWR1__Form__c Order By Name];
            records = [select Id, Name, NamespacePrefix from ApexPage Order By NamespacePrefix, Name limit 500];
            records = [select Name From Group Where Type = 'Regular' Order By Name limit 500];
            records = [select Name From CollaborationGroup Order By Name limit 500];
            records = [select Name From UserRole Order By Name limit 500];
            records = [select Name From Profile Order By Name limit 500];
            records = [select Id, Name, Profile.Name from User where IsActive = true Order By Name];
        }
    }
    
    @istest(seealldata=true) private static void testResourcesLimitFix()
    {
        System.runAs([select Name from User where LastName = 'Mauer' and FirstName = 'Russell'].get(0))
        {
            List<sObject> records = [select Id, Name from STSWR1__Form__c Order By Name limit 500];
            records = [select Id, Name, NamespacePrefix from ApexPage Order By NamespacePrefix, Name limit 500];
            records = [select Name From Group Where Type = 'Regular' Order By Name limit 500];
            records = [select Name From CollaborationGroup Order By Name limit 500];
            records = [select Name From UserRole Order By Name limit 500];
            records = [select Name From Profile Order By Name limit 500];
            records = [select Id, Name, Profile.Name from User where IsActive = true Order By Name limit 500];
        }
    }
    
    
}