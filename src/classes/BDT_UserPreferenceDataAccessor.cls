public with sharing class BDT_UserPreferenceDataAccessor {

	public static final string PROJECT 		= 'SelectedProject';
	public static final string STUDIES 		= 'SelectedStudies';
	public static final string SHOWPROTOCOL	= 'showProtocolTitle';
	public static final string QUICKLIST 	= 'QuickList';
	public static final integer QUICKLISTSIZE 	= 6;

	/**
	 * Add standard reference name to field list
	 * @author Maurice Kremer
	 * @version 20-Dec-13
	 * @return String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('BDT_UserPreference__c');
	}

	/**
	 * Add custom reference name to field list
	 * @author Maurice Kremer
	 * @version 20-Dec-13
	 * @return String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Name,';
		result += referenceName + 'Text__c,';
		result += referenceName + 'LongText__c';
		return result;
	}

	/**
	 * Get all preferences for current user
	 * @author Maurice Kremer
	 * @version 20-Dec-13
	 * @return List<BDT_UserPreference__c>
	 */
	public static List<BDT_UserPreference__c> getAllUsersPreferences () {
		String query = String.format('select {0} from BDT_UserPreference__c {1}',
								new List<String> {
									getSObjectFieldString(),
									'where ownerid = \'' + UserInfo.getUserId() + '\''
								}
							);
		return Database.query(query);
	}

}