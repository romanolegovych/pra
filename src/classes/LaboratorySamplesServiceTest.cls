/** Implements the test for the Service Layer of the object LaboratorySamples__c
 * @author	Dimitrios Sgourdos
 * @version	27-Nov-2013
 */
@isTest
private class LaboratorySamplesServiceTest {
	
	// Global variables
	private static Client_Project__c 	firstProject;
	private static List<Study__c>		studiesList;
	
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	29-Oct-2013
	 */	
	static void init(){
		// Create project
		firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		
		// Create studies
		studiesList = BDT_TestDataUtils.buildStudies(firstProject,3);
		insert studiesList;
	}
	
	
	/** Test the function getLaboratorySamplesDesignContainer
	 * @author	Dimitrios Sgourdos
	 * @version	27-Nov-2013
	 */
	static testMethod void getLaboratorySamplesDesignContainerTest() {
		init();
		
		// Create Laboratory Samples
		List<LaboratorySamples__c> labSamplesList = new List<LaboratorySamples__c>();
		labSamplesList.add(new LaboratorySamples__c(ShipmentNumber__c=2, SequenceNumber__c=1, Project__c=firstProject.Id, 
													ListOfStudies__c=studiesList[0].Id+':'+studiesList[1].Id,
													ActiveNumberOfSamples__c=1, PlaceboNumberOfSamples__c=2,
													BackupNumberOfSamples__c=3, OtherNumberOfSamples__c=4) );
		labSamplesList.add(new LaboratorySamples__c(ShipmentNumber__c=1, SequenceNumber__c=2, Project__c=firstProject.Id, 
													ListOfStudies__c=studiesList[0].Id,
													ActiveNumberOfSamples__c=10, PlaceboNumberOfSamples__c=20,
													BackupNumberOfSamples__c=30, OtherNumberOfSamples__c=40) );
		labSamplesList.add(new LaboratorySamples__c(ShipmentNumber__c=1, SequenceNumber__c=1, Project__c=firstProject.Id, 
													ListOfStudies__c=studiesList[1].Id,
													ActiveNumberOfSamples__c=100, PlaceboNumberOfSamples__c=200,
													BackupNumberOfSamples__c=300, OtherNumberOfSamples__c=400) );
		labSamplesList.add(new LaboratorySamples__c(ShipmentNumber__c=3, SequenceNumber__c=1, Project__c=firstProject.Id, 
													ListOfStudies__c=studiesList[2].Id,
													ActiveNumberOfSamples__c=1000, PlaceboNumberOfSamples__c=2000,
													BackupNumberOfSamples__c=3000, OtherNumberOfSamples__c=4000) );
		insert labSamplesList;
		
		// Query for the first two studies
		List<Study__c> tmpStudiesList = new List<Study__c>();
		studiesList[0].code__c = 'PRA-01';
		tmpStudiesList.add(studiesList[0]);
		studiesList[1].code__c = 'PRA-02';
		tmpStudiesList.add(studiesList[1]);
		
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> results = LaboratorySamplesService.getLaboratorySamplesDesignContainer(
																													firstProject.Id,
																													tmpStudiesList);
		system.assertEquals(results.size(), 3);
		
		system.assertEquals(results[0].labSample.Id, labSamplesList[2].Id);
		system.assertequals(results[0].shipmentNumber, String.valueOf(labSamplesList[2].ShipmentNumber__c));
		system.assertEquals(results[0].studiesSelectionList[0].selected, false);
		system.assertEquals(results[0].studiesSelectionList[1].selected, true);
		system.assertEquals(results[0].getLabSampleTotalNumber(), BDT_Utils.formatNumberWithSeparators(results[0].labSample.TotalNumberOfSamples__c));
		system.assertEquals(results[0].getStudySelection(), '02');
		
		system.assertEquals(results[1].labSample.Id, labSamplesList[1].Id);
		system.assertequals(results[1].shipmentNumber, String.valueOf(labSamplesList[1].ShipmentNumber__c));
		system.assertEquals(results[1].studiesSelectionList[0].selected, true);
		system.assertEquals(results[1].studiesSelectionList[1].selected, false);
		system.assertEquals(results[1].getLabSampleTotalNumber(), BDT_Utils.formatNumberWithSeparators(results[1].labSample.TotalNumberOfSamples__c));
		system.assertEquals(results[1].getStudySelection(), '01');
		
		system.assertEquals(results[2].labSample.Id, labSamplesList[0].Id);
		system.assertequals(results[2].shipmentNumber, String.valueOf(labSamplesList[0].ShipmentNumber__c));
		system.assertEquals(results[2].studiesSelectionList[0].selected, true);
		system.assertEquals(results[2].studiesSelectionList[1].selected, true);
		system.assertEquals(results[2].getLabSampleTotalNumber(), BDT_Utils.formatNumberWithSeparators(results[2].labSample.TotalNumberOfSamples__c));
		system.assertEquals(results[2].getStudySelection(),  '01' + ' / ' + '02');
	}
	
	
	/** Test the function createLaboratorySamplesDesignWrapperInstance
	 * @author	Dimitrios Sgourdos
	 * @version	31-Oct-2013
	 */
	static testMethod void createLaboratorySamplesDesignWrapperInstanceTest() {
		init();
		
		LaboratorySamplesService.LaboratorySamplesDesignWrapper result = LaboratorySamplesService.createLaboratorySamplesDesignWrapperInstance('1', studiesList, firstProject.Id);
		
		system.assertNotEquals(result, null);
		system.assertEquals(result.shipmentNumber, '1');
		system.assertEquals(result.studiesSelectionList.size(), studiesList.size());
		
		for(Integer i=0; i<studiesList.size(); i++) {
			system.assertEquals(result.studiesSelectionList[i].studyId, studiesList[i].Id);
			system.assertEquals(result.studiesSelectionList[i].selected, false);
		}
	}
	
	
	/** Test the function countShipmentsInLaboratorySamplesDesignWrapperList
	 * @author	Dimitrios Sgourdos
	 * @version	31-Oct-2013
	 */
	static testMethod void countShipmentsInLaboratorySamplesDesignWrapperListTest() {
		init();
		
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> samplesWrapperList = new  List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
		samplesWrapperList.add(LaboratorySamplesService.createLaboratorySamplesDesignWrapperInstance('1', studiesList, firstProject.Id));
		samplesWrapperList.add(LaboratorySamplesService.createLaboratorySamplesDesignWrapperInstance('1', studiesList, firstProject.Id));
		samplesWrapperList.add(LaboratorySamplesService.createLaboratorySamplesDesignWrapperInstance('7', studiesList, firstProject.Id));
		samplesWrapperList.add(LaboratorySamplesService.createLaboratorySamplesDesignWrapperInstance('2', studiesList, firstProject.Id));
		samplesWrapperList.add(LaboratorySamplesService.createLaboratorySamplesDesignWrapperInstance('5', studiesList, firstProject.Id));
		
		// Four different shipment numbers
		Integer result = LaboratorySamplesService.countShipmentsInLaboratorySamplesDesignWrapperList(samplesWrapperList);
		system.assertEquals(result, 4);
	}
	
	
	/** Test the function addSampleToLabDesign
	 * @author	Dimitrios Sgourdos
	 * @version	31-Oct-2013
	 */
	static testMethod void addSampleToLabDesignTest() {
		init();
		
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> results = new  List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
		results.add(LaboratorySamplesService.createLaboratorySamplesDesignWrapperInstance('1', studiesList, firstProject.Id));
		results.add(LaboratorySamplesService.createLaboratorySamplesDesignWrapperInstance('2', studiesList, firstProject.Id));
		results[0].labSample.ActiveNumberOfSamples__c = 10;
		results[1].labSample.ActiveNumberOfSamples__c = 100;
		
		// Use illegal indexes
		results = LaboratorySamplesService.addSampleToLabDesign(results, -1, studiesList, firstProject.Id);
		system.assertEquals(results.size(), 2);
		
		results = LaboratorySamplesService.addSampleToLabDesign(results, 2, studiesList, firstProject.Id);
		system.assertEquals(results.size(), 2);
		
		// Add wrapper record to the first shipment
		results = LaboratorySamplesService.addSampleToLabDesign(results, 0, studiesList, firstProject.Id);
		system.assertEquals(results.size(), 3);
		system.assertequals(results[0].labSample.shipmentNumber__c, 1);
		system.assertequals(results[0].labSample.ActiveNumberOfSamples__c, 10);
		system.assertequals(results[1].labSample.shipmentNumber__c, 1);
		system.assertequals(results[2].labSample.shipmentNumber__c, 2);
		system.assertequals(results[2].labSample.ActiveNumberOfSamples__c, 100);
		
		// Add again wrapper record to the first shipment
		results[1].labSample.ActiveNumberOfSamples__c = 20;
		results = LaboratorySamplesService.addSampleToLabDesign(results, 0, studiesList, firstProject.Id);
		system.assertEquals(results.size(), 4);
		system.assertequals(results[0].labSample.shipmentNumber__c, 1);
		system.assertequals(results[0].labSample.ActiveNumberOfSamples__c, 10);
		system.assertequals(results[1].labSample.shipmentNumber__c, 1);
		system.assertequals(results[1].labSample.ActiveNumberOfSamples__c, 20);
		system.assertequals(results[2].labSample.shipmentNumber__c, 1);
		system.assertequals(results[3].labSample.shipmentNumber__c, 2);
		system.assertequals(results[3].labSample.ActiveNumberOfSamples__c, 100);
		
		// Add to the second shipment number
		results = LaboratorySamplesService.addSampleToLabDesign(results, 3, studiesList, firstProject.Id);
		system.assertEquals(results.size(), 5);
		for(Integer i=0; i<3; i++) {
			system.assertequals(results[i].labSample.shipmentNumber__c, 1);
		}
		system.assertequals(results[3].labSample.ActiveNumberOfSamples__c, 100);
		system.assertequals(results[3].labSample.shipmentNumber__c, 2);
		system.assertequals(results[4].labSample.shipmentNumber__c, 2);
	}
	
	
	/** Test the function addShipmentToLabDesign
	 * @author	Dimitrios Sgourdos
	 * @version	31-Oct-2013
	 */
	static testMethod void addShipmentToLabDesignTest() {
		init();
		
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> results = new  List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
		
		// Add shipment
		results = LaboratorySamplesService.addShipmentToLabDesign(results, studiesList, firstProject.Id);
		system.assertEquals(results.size(), 1);
		system.assertEquals(results[0].labSample.shipmentNumber__c, 1);
		
		// Add second shipment
		results = LaboratorySamplesService.addShipmentToLabDesign(results, studiesList, firstProject.Id);
		system.assertEquals(results.size(), 2);
		system.assertEquals(results[0].labSample.shipmentNumber__c, 1);
		system.assertEquals(results[1].labSample.shipmentNumber__c, 2);
	}
	
	
	/** Test the function removeSampleFromLabdesign
	 * @author	Dimitrios Sgourdos
	 * @version	04-Nov-2013
	 */
	static testMethod void removeSampleFromLabdesignTest() {
		// Create a dummy list
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> results = new List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
		
		LaboratorySamplesService.LaboratorySamplesDesignWrapper newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=1,ListOfStudies__c='Test 1');
		newItem.shipmentNumber = '1';
		results.add(newItem);
		
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=2,ListOfStudies__c='Test 2');
		newItem.shipmentNumber = '2';
		results.add(newItem);
		
		// Check the function with not valid indexes
		results = LaboratorySamplesService.removeSampleFromLabdesign(results,-1);
		system.assertEquals(results.size(), 2);
		
		results = LaboratorySamplesService.removeSampleFromLabdesign(results,2);
		system.assertEquals(results.size(), 2);
		
		// Remove the first wrapper Item from the list
		system.assertEquals(results[0].shipmentNumber, '1');
		results = LaboratorySamplesService.removeSampleFromLabdesign(results,0);
		system.assertEquals(results.size(), 1);
		system.assertEquals(results[0].shipmentNumber, '2');
		
		// Check if the inserted slot number is transfered to the second child in case we delete the first child of a slot
		results[0].shipmentNumber = '8';
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=2,ListOfStudies__c='Test 3');
		newItem.shipmentNumber = '2';
		results.add(newItem);
		
		results =  LaboratorySamplesService.removeSampleFromLabdesign(results,0);
		system.assertEquals(results.size(), 1);
		system.assertEquals(results[0].shipmentNumber, '8');
		system.assertEquals(results[0].labSample.ListOfStudies__c, 'Test 3');
	}
	
	
	/** Test the function adjustListOfStudiesForSample
	 * @author	Dimitrios Sgourdos
	 * @version	04-Nov-2013
	 */
	static testMethod void adjustListOfStudiesForSampleTest() {
		// Create dummy SelectedStudyWrapper List
		List<LaboratorySamplesService.SelectedStudyWrapper> selectList = new List<LaboratorySamplesService.SelectedStudyWrapper>();
		
		LaboratorySamplesService.SelectedStudyWrapper newItem = new LaboratorySamplesService.SelectedStudyWrapper();
		newItem.studyId = 'a1';
		newItem.selected = true;
		selectList.add(newItem);
		
		newItem = new LaboratorySamplesService.SelectedStudyWrapper();
		newItem.studyId = 'a2';
		newItem.selected = false;
		selectList.add(newItem);
		
		newItem = new LaboratorySamplesService.SelectedStudyWrapper();
		newItem.studyId = 'a3';
		newItem.selected = true;
		selectList.add(newItem);
		
		newItem = new LaboratorySamplesService.SelectedStudyWrapper();
		newItem.studyId = 'a4';
		newItem.selected = false;
		selectList.add(newItem);
		
		// Check function with different inputs 
		String result = LaboratorySamplesService.adjustListOfStudiesForSample(selectList);
		system.assertEquals(result, 'a1:a3');
		
		for(LaboratorySamplesService.SelectedStudyWrapper tmpItem : selectList) {
			tmpItem.selected = false;
		}
		
		result = LaboratorySamplesService.adjustListOfStudiesForSample(selectList);
		system.assertEquals(result, '');
	}
	
	
	/** Test the function adjustSamplesInSamplesWrapperList
	 * @author	Dimitrios Sgourdos
	 * @version	04-Nov-2013
	 */
	static testMethod void adjustSamplesInSamplesWrapperListTest() {
		// Create a dummy list
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> results = new List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
		
		// First added sample
		LaboratorySamplesService.LaboratorySamplesDesignWrapper newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.shipmentNumber = '1';
		newItem.labSample = new Laboratorysamples__c(ShipmentNumber__c=2, ListOfStudies__c='');
		results.add(newItem);
		
		// Second added sample
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.shipmentNumber = '2';
		newItem.labSample = new Laboratorysamples__c(ShipmentNumber__c=2, ListOfStudies__c='a1:a2:a3:a4');
		results.add(newItem);
		
		// Third added sample
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.shipmentNumber = '3';
		newItem.labSample = new Laboratorysamples__c(ShipmentNumber__c=4, ListOfStudies__c='b1:a2:a4:b2');
		results.add(newItem);
		
		// Insert the same study selection for all the samples
		for(LaboratorySamplesService.LaboratorySamplesDesignWrapper wrapperItem : results) {
			LaboratorySamplesService.SelectedStudyWrapper tmpSelection = new LaboratorySamplesService.SelectedStudyWrapper();
			tmpSelection.studyId = 'a1';
			tmpSelection.selected = true;
			wrapperItem.studiesSelectionList.add(tmpSelection);
			tmpSelection = new LaboratorySamplesService.SelectedStudyWrapper();
			tmpSelection.studyId = 'a3';
			tmpSelection.selected = false;
			wrapperItem.studiesSelectionList.add(tmpSelection);
		}
		
		// Check the function
		results = LaboratorySamplesService.adjustSamplesInSamplesWrapperList(results);
		
		system.assertEquals(results[0].shipmentNumber, '1');
		system.assertEquals(results[0].labSample.SequenceNumber__c, 1);
		system.assertEquals(results[0].labSample.ListOfStudies__c, 'a1');
		
		system.assertEquals(results[1].shipmentNumber, '1');
		system.assertEquals(results[1].labSample.SequenceNumber__c, 2);
		system.assertEquals(results[1].labSample.ListOfStudies__c, 'a1');
		
		system.assertEquals(results[2].shipmentNumber, '3');
		system.assertEquals(results[2].labSample.SequenceNumber__c, 1);
		system.assertEquals(results[2].labSample.ListOfStudies__c, 'a1');
	} 
	
	
	/** Test the function sortSamplesDesignWrapperByShipmentNumber
	 * @author	Dimitrios Sgourdos
	 * @version	04-Nov-2013
	 */
	static testMethod void sortSamplesDesignWrapperByShipmentNumberTest() {
		// Create a dummy list
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> results = new List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
		
		// First added sample
		LaboratorySamplesService.LaboratorySamplesDesignWrapper newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.shipmentNumber = '3';
		newItem.labSample = new Laboratorysamples__c(ListOfStudies__c='First added');
		results.add(newItem);
		
		// Second added sample
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.shipmentNumber = '3';
		newItem.labSample = new Laboratorysamples__c(ListOfStudies__c='Second added');
		results.add(newItem);
		
		// Third added sample
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.shipmentNumber = '2';
		newItem.labSample = new Laboratorysamples__c(ListOfStudies__c='Third added');
		results.add(newItem);
		
		// check the function
		results = LaboratorySamplesService.sortSamplesDesignWrapperByShipmentNumber(results);
		
		system.assertEquals(results[0].labSample.ListOfStudies__c, 'Third added');
		system.assertEquals(results[0].shipmentNumber, '1');
		
		system.assertEquals(results[1].labSample.ListOfStudies__c, 'First added');
		system.assertEquals(results[1].shipmentNumber, '2');
		
		system.assertEquals(results[2].labSample.ListOfStudies__c, 'Second added');
		system.assertEquals(results[2].shipmentNumber, '3');
	}
	
	
	/** Test the function extractLabSamplesFromLabDesign
	 * @author	Dimitrios Sgourdos
	 * @version	05-Nov-2013
	 */
	static testMethod void extractLabSamplesFromLabDesignTest() {
		// Create a dummy list with mixed inserted shipmentNumbers
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> initialWrapperList = new List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
		
		// First added sample (first sample of shipment 5)
		LaboratorySamplesService.LaboratorySamplesDesignWrapper newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.shipmentNumber = '3';
		newItem.labSample = new Laboratorysamples__c(ShipmentNumber__c=5, ActiveNumberOfSamples__c = 10);
		initialWrapperList.add(newItem);
		
		// Second added sample (second sample of shipment 5)
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.shipmentNumber = '5';
		newItem.labSample = new Laboratorysamples__c(ShipmentNumber__c=5, ActiveNumberOfSamples__c = 20);
		initialWrapperList.add(newItem);
		
		// Third added sample (first sample of shipment shipment 3)
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.shipmentNumber = '3';
		newItem.labSample = new Laboratorysamples__c(ShipmentNumber__c=3, ActiveNumberOfSamples__c = 30);
		initialWrapperList.add(newItem);
		
		// Fourth added sample (first sample of shipment shipment 4)
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.shipmentNumber = '2';
		newItem.labSample = new Laboratorysamples__c(ShipmentNumber__c=4, ActiveNumberOfSamples__c = 40);
		initialWrapperList.add(newItem);
		
		// Fifth added sample (second sample of shipment shipment 4)
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.shipmentNumber = '4';
		newItem.labSample = new Laboratorysamples__c(ShipmentNumber__c=4, ActiveNumberOfSamples__c = 50);
		initialWrapperList.add(newItem);
		
		// check the function
		List<LaboratorySamples__c> results = new List<LaboratorySamples__c>();
		results = LaboratorySamplesService.extractLabSamplesFromLabDesign(initialWrapperList);
		
		system.assertEquals(results.size(),5);
		
		system.assertEquals(results[0].ShipmentNumber__c, 1);
		system.assertEquals(results[0].ActiveNumberOfSamples__c, 40);
		
		system.assertEquals(results[1].ShipmentNumber__c, 1);
		system.assertEquals(results[1].ActiveNumberOfSamples__c, 50);
		
		system.assertEquals(results[2].ShipmentNumber__c, 2);
		system.assertEquals(results[2].ActiveNumberOfSamples__c, 10);
		
		system.assertEquals(results[3].ShipmentNumber__c, 2);
		system.assertEquals(results[3].ActiveNumberOfSamples__c, 20);
		
		system.assertEquals(results[4].ShipmentNumber__c, 3);
		system.assertEquals(results[4].ActiveNumberOfSamples__c, 30);
	}
	
	
	/** Test the function getSamplesWithoutStudySelection
	 * @author	Dimitrios Sgourdos
	 * @version	05-Nov-2013
	 */
	static testMethod void getSamplesWithoutStudySelectionTest() {
		// Create an initial list
		List<LaboratorySamples__c> initialList = new List<LaboratorySamples__c>();
		initialList.add(new LaboratorySamples__c(ListOfStudies__c='', ActiveNumberOfSamples__c=10));
		initialList.add(new LaboratorySamples__c(ListOfStudies__c='Test 1', ActiveNumberOfSamples__c=20));
		initialList.add(new LaboratorySamples__c(ActiveNumberOfSamples__c=30));
		initialList.add(new LaboratorySamples__c(ListOfStudies__c='', ActiveNumberOfSamples__c=40));
		initialList.add(new LaboratorySamples__c(ListOfStudies__c='Test 2', ActiveNumberOfSamples__c=50));
		
		// Check the function
		List<LaboratorySamples__c> results = LaboratorySamplesService.getSamplesWithoutStudySelection(initialList);
		system.assertequals(results.size(), 3);
		system.assertequals(results[0].ActiveNumberOfSamples__c, 10);
		system.assertequals(results[1].ActiveNumberOfSamples__c, 30);
		system.assertequals(results[2].ActiveNumberOfSamples__c, 40);
	}
	
	
	/** Test the function saveLaboratorySamples
	 * @author	Dimitrios Sgourdos
	 * @version	05-Nov-2013
	 */
	static testMethod void saveLaboratorySamplesTest() {
		init();
		
		// Create an initial list
		List<LaboratorySamples__c> initialList = new List<LaboratorySamples__c>();
		initialList.add(new LaboratorySamples__c(ListOfStudies__c='', ActiveNumberOfSamples__c=10, Project__c=firstProject.Id));
		initialList.add(new LaboratorySamples__c(ListOfStudies__c='Test 1', ActiveNumberOfSamples__c=20, Project__c=firstProject.Id));
		initialList.add(new LaboratorySamples__c(ActiveNumberOfSamples__c=30, Project__c=firstProject.Id));
		initialList.add(new LaboratorySamples__c(ListOfStudies__c='', ActiveNumberOfSamples__c=40, Project__c=firstProject.Id));
		initialList.add(new LaboratorySamples__c(ListOfStudies__c='Test 2', ActiveNumberOfSamples__c=50, Project__c=firstProject.Id));
		
		// Check the function without giving a shipment number(that will create an exception)
		Boolean resultFlag = LaboratorySamplesService.saveLaboratorySamples(initialList);
		system.assertEquals(resultFlag, false);
		
		List<LaboratorySamples__c> retrievedList = [SELECT ListOfStudies__c, ActiveNumberOfSamples__c
													FROM   LaboratorySamples__c
													ORDER BY ActiveNumberOfSamples__c];
		system.assertEquals(retrievedList.size(), 0);
		
		// Check the function with giving a shipment number
		for(LaboratorySamples__c tmpSample : initialList) {
			tmpSample.ShipmentNumber__c = 1;
		}
		
		resultFlag = LaboratorySamplesService.saveLaboratorySamples(initialList);
		system.assertEquals(resultFlag, true);
		
		retrievedList = [SELECT ListOfStudies__c, ActiveNumberOfSamples__c
						FROM   LaboratorySamples__c 
						ORDER BY ActiveNumberOfSamples__c];
		
		system.assertEquals(retrievedList.size(), 2);
		
		system.assertEquals(retrievedList[0].ListOfStudies__c, 'Test 1');
		system.assertEquals(retrievedList[0].ActiveNumberOfSamples__c, 20);
		
		system.assertEquals(retrievedList[1].ListOfStudies__c, 'Test 2');
		system.assertEquals(retrievedList[1].ActiveNumberOfSamples__c, 50);
	}
	
	
	/** Test the function deleteLaboratorySamples
	 * @author	Dimitrios Sgourdos
	 * @version	05-Nov-2013
	 */
	static testMethod void deleteLaboratorySamplesTest() {
		init();
		
		List<LaboratorySamples__c> initialList = new List<LaboratorySamples__c>();
		initialList.add(new LaboratorySamples__c(ShipmentNumber__c=1, Project__c=firstProject.Id));
		initialList.add(new LaboratorySamples__c(ShipmentNumber__c=2, Project__c=firstProject.Id));
		insert initialList;
		
		List<LaboratorySamples__c> retrievedList = [Select Id FROM LaboratorySamples__c];
		
		// Before deletion
		system.assert(retrievedList.size() > 0);
		
		// After deletion
		Boolean resultFlag = LaboratorySamplesService.deleteLaboratorySamples(retrievedList);
		system.assertEquals(resultFlag, true);
		
		retrievedList = [Select Id FROM LaboratorySamples__c];
		system.assertEquals(retrievedList.size(), 0);
	}
	
	
	/** Test the function getAvailableShipmentSelection
	 * @author	Dimitrios Sgourdos
	 * @version	13-Nov-2013
	 */
	static testMethod void getAvailableShipmentSelectionTest() {
		init();
		
		// Create Laboratory Samples
		List<LaboratorySamples__c> labSamplesList = new List<LaboratorySamples__c>();
		labSamplesList.add(new LaboratorySamples__c(ShipmentNumber__c=2, SequenceNumber__c=1, Project__c=firstProject.Id, 
													ListOfStudies__c=studiesList[0].Id+':'+studiesList[1].Id) );
		labSamplesList.add(new LaboratorySamples__c(ShipmentNumber__c=1, SequenceNumber__c=2, Project__c=firstProject.Id, 
													ListOfStudies__c=studiesList[0].Id) );
		labSamplesList.add(new LaboratorySamples__c(ShipmentNumber__c=1, SequenceNumber__c=1, Project__c=firstProject.Id, 
													ListOfStudies__c=studiesList[1].Id) );
		labSamplesList.add(new LaboratorySamples__c(ShipmentNumber__c=3, SequenceNumber__c=1, Project__c=firstProject.Id, 
													ListOfStudies__c=studiesList[2].Id) );
		insert labSamplesList;
		
		// Check the function
		List<LaboratorySamplesService.ShipmentSelectionWrapper> results = LaboratorySamplesService.getAvailableShipmentSelection(firstproject.Id, studiesList);
		
		system.assertEquals(results.size(), 3);
		system.assertequals(results[0].shipmentNumber, '01');
		system.assertequals(results[1].shipmentNumber, '02');
		system.assertequals(results[2].shipmentNumber, '03');
		
		for(LaboratorySamplesService.ShipmentSelectionWrapper tmpItem : results) {
			system.assertEquals(tmpItem.selectedFlag, false);
		}
	}
	
	
	/** Test the function updateShipmentSelectionBySelectedInSlot
	 * @author	Dimitrios Sgourdos
	 * @version	13-Nov-2013
	 */
	static testMethod void updateShipmentSelectionBySelectedInSlotTest() {
		// Create initial ShipmentSelectionWrapper list
		List<LaboratorySamplesService.ShipmentSelectionWrapper> initialList = new List<LaboratorySamplesService.ShipmentSelectionWrapper>();
		
		LaboratorySamplesService.ShipmentSelectionWrapper newItem = new LaboratorySamplesService.ShipmentSelectionWrapper();
		newItem.shipmentNumber = '01';
		newItem.selectedFlag = false;
		initialList.add(newItem);
		
		newItem = new LaboratorySamplesService.ShipmentSelectionWrapper();
		newItem.shipmentNumber = '02';
		newItem.selectedFlag = false;
		initialList.add(newItem);
		
		// Create LaboratoryAnalysisSlots__c
		LaboratoryAnalysisSlots__c labSlot = new LaboratoryAnalysisSlots__c(ListOfShipments__c='02:03');
		
		// Check the function
		List<LaboratorySamplesService.ShipmentSelectionWrapper> results = LaboratorySamplesService.updateShipmentSelectionBySelectedInSlot(
																													initialList,
																													labSlot);
		system.assertEquals(results.size(), 2);
		
		system.assertEquals(results[0].shipmentNumber, '01');
		system.assertEquals(results[0].selectedFlag, false);
		
		system.assertEquals(results[1].shipmentNumber, '02');
		system.assertEquals(results[1].selectedFlag, true);
	}
	
	
	/** Test the function saveShipmentsAssociationToSlot
	 * @author	Dimitrios Sgourdos
	 * @version	13-Nov-2013
	 */
	static testMethod void saveShipmentsAssociationToSlotTest() {
		// Create initial ShipmentSelectionWrapper list
		List<LaboratorySamplesService.ShipmentSelectionWrapper> selectionList = new List<LaboratorySamplesService.ShipmentSelectionWrapper>();
		
		LaboratorySamplesService.ShipmentSelectionWrapper newItem = new LaboratorySamplesService.ShipmentSelectionWrapper();
		newItem.shipmentNumber = '01';
		newItem.selectedFlag = false;
		selectionList.add(newItem);
		
		newItem = new LaboratorySamplesService.ShipmentSelectionWrapper();
		newItem.shipmentNumber = '02';
		newItem.selectedFlag = true;
		selectionList.add(newItem);
		
		newItem = new LaboratorySamplesService.ShipmentSelectionWrapper();
		newItem.shipmentNumber = '03';
		newItem.selectedFlag = true;
		selectionList.add(newItem);
		
		// Create LaboratoryAnalysisSlots__c
		LaboratoryAnalysisSlots__c labSlot = new LaboratoryAnalysisSlots__c(ListOfShipments__c='01');
		
		// Check the function
		labSlot = LaboratorySamplesService.saveShipmentsAssociationToSlot(selectionList, labSlot);
		system.assertEquals(labSlot.ListOfShipments__c, '02:03');
	}
	
	
	/** Test the function updateShipmentNumbersMapWithNewNumbers
	 * @author	Dimitrios Sgourdos
	 * @version	14-Nov-2013
	 */
	static testMethod void updateShipmentNumbersMapWithNewNumbersTest() {
		// Create initial map
		Map<String, String> initialMap = new Map<String, String>();
		initialMap.put('01', NULL);
		initialMap.put('02', NULL);
		initialMap.put('03', NULL);
		initialMap.put('04', NULL);
		
		// Create dummy Sample Shipments lab design
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> samplesWrapperList = new List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
		
		LaboratorySamplesService.LaboratorySamplesDesignWrapper newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=1);
		newItem.shipmentNumber = '8';
		samplesWrapperList.add(newItem);
		
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=2);
		newItem.shipmentNumber = '2';
		samplesWrapperList.add(newItem);
		
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=4);
		newItem.shipmentNumber = '9';
		samplesWrapperList.add(newItem);
		
		// Check the function
		initialMap = LaboratorySamplesService.updateShipmentNumbersMapWithNewNumbers(initialMap, samplesWrapperList);
		system.assertEquals(initialMap.keyset().size(), 4);
		system.assertequals(initialMap.get('01'), '08');
		system.assertequals(initialMap.get('02'), '02');
		system.assertequals(initialMap.get('03'), NULL);
		system.assertequals(initialMap.get('04'), '09');
	}
	
	
	/** Test the function cloneSamplesWrapperListShipmentNumbers
	 * @author	Dimitrios Sgourdos
	 * @version	14-Nov-2013
	 */
	static testMethod void cloneSamplesWrapperListShipmentNumbers() {
		// Create dummy Sample Shipments lab design
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> initialList = new List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
		
		LaboratorySamplesService.LaboratorySamplesDesignWrapper newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=1);
		newItem.shipmentNumber = '8';
		initialList.add(newItem);
		
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=2);
		newItem.shipmentNumber = '2';
		initialList.add(newItem);
		
		// Check the function
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> results = LaboratorySamplesService.cloneSamplesWrapperListShipmentNumbers(initialList);
		
		system.assertEquals(results.size(), 2);
		
		system.assertEquals(results[0].labSample.ShipmentNumber__c, 1);
		system.assertEquals(results[0].shipmentNumber, '8');
		
		system.assertEquals(results[1].labSample.ShipmentNumber__c, 2);
		system.assertEquals(results[1].shipmentNumber, '2');
	}
	
	
	/** Test the function getUpdatedFirstSamplesRecordsFromSamplesDesign
	 * @author	Dimitrios Sgourdos
	 * @version	14-Nov-2013
	 */
	static testMethod void getUpdatedFirstSamplesRecordsFromSamplesDesignTest() {
		// Create dummy Sample Shipments lab design
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> initialList = new List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
		
		LaboratorySamplesService.LaboratorySamplesDesignWrapper newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=8);
		newItem.shipmentNumber = '18';
		initialList.add(newItem);
		
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=7);
		newItem.shipmentNumber = '17';
		initialList.add(newItem);
		
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=7);
		newItem.shipmentNumber = '7';
		initialList.add(newItem);
		
		// Check the function
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> results = LaboratorySamplesService.getUpdatedFirstSamplesRecordsFromSamplesDesign(initialList);
		
		system.assertEquals(results.size(), 2);
		
		system.assertEquals(results[0].shipmentNumber, '1');
		system.assertEquals(results[0].labSample.ShipmentNumber__c, 7);
		
		system.assertEquals(results[1].shipmentNumber, '2');
		system.assertEquals(results[1].labSample.ShipmentNumber__c, 8);
	}
	
	
	/** Test the function filterSamplesDesignByShipmentNumberOrStudySelection
	 * @author	Dimitrios Sgourdos
	 * @version	27-Nov-2013
	 */
	static testMethod void filterSamplesDesignByShipmentNumberOrStudySelectionTest() {
		// Create data
		init();
		
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> initialList = new List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
		
		LaboratorySamplesService.LaboratorySamplesDesignWrapper newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=1);
		newItem.studiesSelectionList.add(new LaboratorySamplesService.SelectedStudyWrapper());
		initialList.add(newItem);
		
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=2);
		newItem.studiesSelectionList.add(new LaboratorySamplesService.SelectedStudyWrapper());
		initialList.add(newItem);
		
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=3);
		LaboratorySamplesService.SelectedStudyWrapper tmpStSel = new LaboratorySamplesService.SelectedStudyWrapper();
		tmpStSel.StudyId = studiesList[0].Id;
		tmpStSel.selected = false;
		newItem.studiesSelectionList.add(tmpStSel);
		initialList.add(newItem);
		
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=4);
		tmpStSel = new LaboratorySamplesService.SelectedStudyWrapper();
		tmpStSel.StudyId = studiesList[0].Id;
		tmpStSel.selected = true;
		newItem.studiesSelectionList.add(tmpStSel);
		initialList.add(newItem);
		
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		initialList.add(newItem);
		
		Set<Decimal> usedShipNumbers = new Set<Decimal>{1,2};
		
		// Check the function
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> results = LaboratorySamplesService.filterSamplesDesignByShipmentNumberOrStudySelection(
																													initialList,
																													usedShipNumbers,
																													studiesList
																				);
		system.assertEquals(results.size(), 3);
		system.assertEquals(results[0].labSample.ShipmentNumber__c, 1);
		system.assertEquals(results[1].labSample.ShipmentNumber__c, 2);
		system.assertEquals(results[2].labSample.ShipmentNumber__c, 4);
	}
}