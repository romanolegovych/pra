public with sharing class RM_NoteCompController {
	 public EMPLOYEE_DETAILS__c empl{get; set;}
    public EMPLOYEE_DETAILS__c manager{get; set;}
    public string employeeRID{get; set;}
    public boolean bViewNote{get; set;}   
    public string rmAddComment{ get; set;}    
    public RM_NoteCompController(){
    		
    }
    public void getAssignmentNote(){
        system.debug('---employeeRID---' + employeeRID);
        empl = RM_EmployeeService.GetEmployeeDetailByEmployee(employeeRID);
        manager = getMangerEmail(empl.Supervisor_ID__c);
        getPermission();
    }
    
    public PageReference addNote(){
        system.debug('---employeeRID---' + employeeRID);
        //empl = RM_EmployeeService.GetEmployeeDetailByEmployee(employeeRID);
        empl = new EMPLOYEE_DETAILS__c(id=employeeRID);
        system.debug('---empl---' + empl);
        system.debug('---rmAddComment---' + rmAddComment);
        
        empl.RM_Comment__c = rmAddComment;
                    
        update empl;
        return null; 
    }
    
    private EMPLOYEE_DETAILS__c getMangerEmail(string supervisorID){        
        return RM_EmployeeService.GetEmployeeDetailByEmployeeID(supervisorID);        
    }
    
    private void getPermission(){
        Boolean bRMAdmin = RM_OtherService.IsRMAdmin(UserInfo.getUserId());
		set<string> stManagedBU = RM_OtherService.GetUserGroupBU(UserInfo.getUserId());  
		bViewNote = false;
		if ( bRMAdmin )       
				bViewNote = true;           
		if (stManagedBU!= null && stManagedBU.size()> 0){
			if (stManagedBU.contains(empl.Business_Unit_Desc__c))
				bViewNote = true;           
		}
		
		system.debug('---------bViewNote---------'+bViewNote  ); 
		if (bViewNote)
		{           
			rmAddComment = empl.RM_Comment__c;
		}
    }
}