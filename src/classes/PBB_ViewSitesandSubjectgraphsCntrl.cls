/**
@author Bhargav Devaram
@date 2015
@description this controller class is to display the Planned, Actuals of sites and Subjects graphs of a scenario
**/
global with sharing class PBB_ViewSitesandSubjectgraphsCntrl {

    public  String ProtocolUniqueKey{get;set;}    //select ScenarioID
    public  PBB_Scenario__c PBBScenario             {get;set;}    //PBB Scenario Sobject   
    public  String  chartOption                     {get;set;}    //Project Level or Country 
    public  String  chartType                       {get;set;}    //Spline or Column
    public  String  Cntry                           {get;set;}    //select country to display
    public  String  GraphType                       {get;set;}    //whether Site or Subject graph    
    
    public  String graphdata                        {get;set;}    //graph datapoints for Site
    public  Map<String,List<WrapperClass>> GraphdataTableMap      {get;set;}    //display Site Activation and Subject Enrollment list data for datatables
    public  List<String> dateLabelList              {get;set;}    //display Dates in dd-mmm-yy for datatables
    public  List<WrapperClass> dataTableList        {get;set;}    //display Site Activation and Subject Enrollment list data for datatables
    
    /** 
    @author Bhargav Devaram
    @date 2015 
    @description Constructor  
    */
    Public PBB_ViewSitesandSubjectgraphsCntrl() {
        chartOption ='Project Level';
        chartType='spline';      
        
        system.debug('---ProtocolUniqueKey--'+ProtocolUniqueKey);
            
    }
    
    /** 
    @author Bhargav Devaram
    @date 2015 
    @description get all Site Activation and Subject Enrollment graph and table data for the PBB Scenario and by project level or by country 
    */ 
    public PageReference generateGraphandTableData() {
    
        system.debug('---chartOption--'+chartOption+chartType+cntry);  
        system.debug('---ProtocolUniqueKey--'+ProtocolUniqueKey);      
        system.debug('---GraphType--'+GraphType);
        
        //get the graph data strings and table data from map     
        GraphdataTableMap    = new Map<String,List<WrapperClass>>();
        GraphdataTableMap    = getPlnAndActSuToDatevalues(ProtocolUniqueKey,chartOption,cntry,GraphType);
        
        //get just the graph data from Map
        Set<String> graphdataSet= new Set<String>();
        graphdataSet = GraphdataTableMap.keyset();   
        list<String> graphdataList= new list<String>();
        graphdataList.addall(graphdataSet);
        graphdata = graphdataList[0];  
        
        //get the table data and dates from the map values
        system.debug('---GraphdataTableMap.values()--'+GraphdataTableMap.values());
        dataTableList  = GraphdataTableMap.get(graphdata); 
        dateLabelList =   dataTableList[0].dataValues;  
        dataTableList.remove(0);            
        return null;
    }
         
    /** 
    @author Bhargav Devaram
    @date 2015 
    @description get all Site Activation and Subject Enrollment for the PBB Scenario and by project level or by country 
    */
    public Map<String,List<WrapperClass>> getPlnAndActSuToDatevalues(String ProtocolUniqueKey,String chartOption,String cntry,String GraphType){
        
        Map<String,List<WrapperClass>>  graphdata = new Map<String,List<WrapperClass>>();
        
        //Site Activation lists
        //for data table 
        list<String> PlanSites = new list<String>();
        list<String> ActualSites = new list<String>();
        list<String> ProjSites = new list<String>();  
        list<String> LowRiskSites = new list<String>();  
        list<String> MediumRiskSites = new list<String>();     
        
        //Subject Enrollment lists
        //for data table 
        list<String> PlanSubj = new list<String>();
        list<String> ActualSubj = new list<String>();
        list<String> ExpectedSubj = new list<String>();
        list<String> ProjSubj = new list<String>();
        list<String> LowRiskSubjects = new list<String>();  
        list<String> MediumRiskSubjects = new list<String>();   
        
        //Dates, Site Activation,Subject Enrollment wrapper list
        list<String> dateLabelList= new list<String>();
        list<WrapperClass> siteActivationList = new list<WrapperClass >();
        list<WrapperClass> subEnrollList = new list<WrapperClass >();
        
        //Prepare query to get the data
        String query;
        query ='select Name na,Week_Date__c w,sum(Planned_Active_Sites__c) ps,sum(Actual_Active_Sites__c) asi,sum(Aggressive_Active_Sites__c) prs,';
        query+=' sum(Conservative_Active_Sites__c) lrs,sum(Medium_Active_Sites__c) mrs, ';
        query+=' sum(Planned_Subjects__c) psub,sum(Actual_Subjects__c) asub,sum(Aggressive_Subjects__c) prsub, ';
        query+=' sum(Conservative_Subjects__c) lrsub,sum(Medium_Risk_Subjects__c) mrsub,sum(Expected_Subjects__c) expsub ';
        query+=' from Country_Weekly_Event__c ';
        query+=' where Protocol_Country__r.Client_Protocol__r.Protocal_Unique_Key__c =:ProtocolUniqueKey ';
        
        //if country then add the condition to filter
        if(chartOption!='Project Level'){
            query+=' and Protocol_Country__r.region__c =:cntry ';                         
        }
        query+=' GROUP BY Name,Week_Date__c';
        System.debug('----query-----'+ query);         
        
        Map<String,List<String>> graphdatapoints = new Map<String,List<String>>(); 
        
        //hold the last weeks aggregated record.
        AggregateResult Lastara;
        
        //to get the week where Actual Subjects ends and where Expected Subjects ends
        Integer NullActualSubWeek,NullExpSubWeek,NullActualSiteWeek; 
         
        //to determine whether which graph ends
        String Subjects ='Expected Subjects';  
        
        for(AggregateResult ara : database.query(query)){
            System.debug('-----------By Weekly=Sum-Of-actual&planned-subjects&sites=given Scenaro ------------' + ara.get('na') + '\t' +
                                                                                ara.get('ps') + ara.get('lrs') + '\t' + ara.get('asi')+ '\t' +
            
                                                                              ara.get('psub') + '\t' + ara.get('asub') + '\t' + ara.get('w'));
              
            System.debug('----ara.get----'+ ara.get('lrs') + '\t' + ara.get('mrs'));
            
            Date Weekdate = Date.valueof(ara.get('w'));
            Datetime Weekdate1 = Date.valueof(ara.get('w'));
            
            //change the date format to fit it in datatable
            dateLabelList.add(SRM_Utils.getDateInStringFormat(Weekdate ));
            //dateLabelList.add(String.valueof(Weekdate1.format('dd-MMM-yy')));
            
            //get the Expected Subjects value
            String expsub = null; 
            expsub = String.valueof(Integer.valueof(ara.get('expsub')));
            
            //make the expected Subjects as null only if the Actual Subjects are not null
            if(ara.get('asub')!=null ) {
               expsub = null;
            }
            
            //check if Actual Subjects is null and Expected Subjects are not null 
            if(ara.get('asub')==null && ara.get('expsub')!=null  && NullActualSubWeek==null){                
                //get the current week number and assign when the week is ended
                String week = String.valueof(ara.get('na'));
                week  = week.replace('Week ','');
                System.debug('----week----'+week  );
                NullActualSubWeek = Integer.valueof(week);
            }
            
            //check if Expected Subjects is null and (low,megium and high risk subjects as not null)            
            if(ara.get('expsub')==null &&  (ara.get('prsub')!=null || ara.get('lrsub')!=null || ara.get('mrsub')!=null) && NullExpSubWeek==null ) {                 
                 //get the current week number and assign when the week is ended
                 String week = String.valueof(ara.get('na'));
                 week  = week.replace('Week ','');
                 NullExpSubWeek = Integer.valueof(week);
            } 
            
            //Check if Last week value is not null
            if(Lastara!=null){    
                //check if Expected Subjects is not null and Last weeks Actual Subjects is not null  and (low,megium and high risk subjects as not null)             
                if(  Lastara.get('asub')!=null  && (ara.get('prsub')!=null || ara.get('lrsub')!=null || ara.get('mrsub')!=null) ){
                   //&& Lastara.get('expsub')!=null
                   //assign Actual Subject label, which means Actual are ending prior to Low, Medium and High Risk.
                   Subjects = 'Actual Subjects';
                }
            }
            
            //check if Actual Sites is null  and (low,megium and high risk subjects as not null) 
            if(ara.get('asi')==null  && (ara.get('lrs')!=null || ara.get('mrs')!=null || ara.get('prs')!=null) && NullActualSiteWeek==null){
                String week = String.valueof(ara.get('na'));
                week  = week.replace('Week ','');
                NullActualSiteWeek = Integer.valueof(week);
            }
            graphdatapoints = graphdatapointsbyformat(Weekdate,expsub,'Expected Subjects',graphdatapoints); 
            System.debug('----ExpectedSubj----'+ ExpectedSubj+graphdatapoints );
            
            graphdatapoints = graphdatapointsbyformat(Weekdate,String.valueof(Integer.valueof(ara.get('ps'))),'Planned Sites',graphdatapoints);
            graphdatapoints = graphdatapointsbyformat(Weekdate,String.valueof(Integer.valueof(ara.get('asi'))),'Actual Sites',graphdatapoints);            
            graphdatapoints = graphdatapointsbyformat(Weekdate,String.valueof(Integer.valueof(ara.get('lrs'))),'Conservative Sites',graphdatapoints);            
            graphdatapoints = graphdatapointsbyformat(Weekdate,String.valueof(Integer.valueof(ara.get('mrs'))),'Medium Sites',graphdatapoints);
            graphdatapoints = graphdatapointsbyformat(Weekdate,String.valueof(Integer.valueof(ara.get('prs'))),'Aggressive Sites',graphdatapoints);
            
               
            planSites.add(ara.get('ps')==null?'':String.valueof(ara.get('ps')).substring(0,String.valueof(ara.get('ps')).indexOf('.')));
            ActualSites.add(ara.get('asi')==null?'':String.valueof(ara.get('asi')).substring(0,String.valueof(ara.get('asi')).indexOf('.')));            
            ProjSites.add(ara.get('prs')==null?'':String.valueof(ara.get('prs')).substring(0,String.valueof(ara.get('prs')).indexOf('.')));            
            LowRiskSites.add(ara.get('lrs')==null?'':String.valueof(ara.get('lrs')).substring(0,String.valueof(ara.get('lrs')).indexOf('.')));            
            MediumRiskSites.add(ara.get('mrs')==null?'':String.valueof(ara.get('mrs')).substring(0,String.valueof(ara.get('mrs')).indexOf('.'))); 
            
            //All Subject Enrollment graph and table data
            graphdatapoints = graphdatapointsbyformat(Weekdate,String.valueof(Integer.valueof(ara.get('psub'))),'Planned Subjects',graphdatapoints);
            graphdatapoints = graphdatapointsbyformat(Weekdate,String.valueof(Integer.valueof(ara.get('asub'))),'Actual Subjects',graphdatapoints);
            //graphdatapoints = graphdatapointsbyformat(Weekdate,String.valueof(Integer.valueof(ara.get('expsub'))),'Expected Subjects',graphdatapoints);
            graphdatapoints = graphdatapointsbyformat(Weekdate,String.valueof(Integer.valueof(ara.get('lrsub'))),'Conservative Subjects',graphdatapoints);            
            graphdatapoints = graphdatapointsbyformat(Weekdate,String.valueof(Integer.valueof(ara.get('mrsub'))),'Medium Subjects',graphdatapoints);
            graphdatapoints = graphdatapointsbyformat(Weekdate,String.valueof(Integer.valueof(ara.get('prsub'))),'Aggressive Subjects',graphdatapoints);
            
            PlanSubj.add(ara.get('psub')==null?'':String.valueof(ara.get('psub')).substring(0,String.valueof(ara.get('psub')).indexOf('.')));       
            ActualSubj.add(ara.get('asub')==null?'':String.valueof(ara.get('asub')).substring(0,String.valueof(ara.get('asub')).indexOf('.'))); 
            ExpectedSubj.add(expsub ==null?'':expsub);
            ProjSubj.add(ara.get('prsub')==null?'':String.valueof(ara.get('prsub')).substring(0,String.valueof(ara.get('prsub')).indexOf('.')));
            LowRiskSubjects.add(ara.get('lrsub')==null?'':String.valueof(ara.get('lrsub')).substring(0,String.valueof(ara.get('lrsub')).indexOf('.')));            
            MediumRiskSubjects.add(ara.get('mrsub')==null?'':String.valueof(ara.get('mrsub')).substring(0,String.valueof(ara.get('mrsub')).indexOf('.')));
            
            //Assign the current week record to last week
            Lastara=ara;
            System.debug('---Lastara----'+ ara+Lastara.get('w') );     
        }        
        System.debug('---NullActualSubWeek NullExpSubWeek----'+ NullActualSubWeek+NullExpSubWeek +NullActualSiteWeek );
        System.debug('----Subjects----'+ Subjects); 
        
        //Bind the graph datapoints by substituting the last ended Actual Subject value to Expected Subjects graph
        if(NullActualSubWeek!=null && NullActualSubWeek!=1) 
            graphdatapoints = graphdataupdate('Actual Subjects',graphdatapoints,'Expected Subjects',graphdatapoints,NullActualSubWeek-1);
        
        //Bing the graph datapoints by substituting the last ended Actual/Expected  Subject value to Low/Medium.High Risk Subjects graph points
        if(NullExpSubWeek!=null && NullExpSubWeek!=1) {
            graphdatapoints = graphdataupdate(Subjects,graphdatapoints,'Conservative Subjects',graphdatapoints,NullExpSubWeek-1);
            graphdatapoints = graphdataupdate(Subjects,graphdatapoints,'Medium Subjects',graphdatapoints,NullExpSubWeek-1);
            graphdatapoints = graphdataupdate(Subjects,graphdatapoints,'Aggressive Subjects',graphdatapoints,NullExpSubWeek-1);
        }
        
        //Bind the graph datapoints by substituting the last ended Actual sites value to Low/Medium.High Risk Sites graph points
        if(NullActualSiteWeek !=null && NullActualSiteWeek !=1){ 
            graphdatapoints = graphdataupdate('Actual Sites',graphdatapoints,'Conservative Sites',graphdatapoints,NullActualSiteWeek-1);
            graphdatapoints = graphdataupdate('Actual Sites',graphdatapoints,'Medium Sites',graphdatapoints,NullActualSiteWeek-1);
            graphdatapoints = graphdataupdate('Actual Sites',graphdatapoints,'Aggressive Sites',graphdatapoints,NullActualSiteWeek-1);
        }
        System.debug('---NullActualSubWeek NullExpSubWeek----'+ NullActualSubWeek+NullExpSubWeek+NullActualSiteWeek ); 
        
        
        //Planned        
        String SiteGraph = '[' +graphdataformat('Planned Sites',graphdatapoints)+','+graphdataformat('Actual Sites',graphdatapoints)+','+graphdataformat('Conservative Sites',graphdatapoints)+','+graphdataformat('Medium Sites',graphdatapoints)+','+graphdataformat('Aggressive Sites',graphdatapoints)+']';        
        String SubjGraph = '[' +graphdataformat('Planned Subjects',graphdatapoints) +','+ graphdataformat('Actual Subjects',graphdatapoints)+','+graphdataformat('Conservative Subjects',graphdatapoints)+','+graphdataformat('Medium Subjects',graphdatapoints)+','+graphdataformat('Aggressive Subjects',graphdatapoints)+','+graphdataformat('Expected Subjects',graphdatapoints)+']';
        
        System.debug('----SubjGraph -----'+ SubjGraph );
        System.debug('----SiteGraph -----'+ SiteGraph );
        
        //adding the Site and Subj graphs data
        List<String> graphdataList = new List<String>();
        graphdataList.add(SiteGraph);
        graphdataList.add(SubjGraph );        
                
        WrapperClass dates = new WrapperClass('Dates', dateLabelList);
        
        //adding the list and dates to the wrappper to display on the table
        WrapperClass ps = new WrapperClass('Planned', planSites);
        WrapperClass aas = new WrapperClass('Actual', ActualSites);
        WrapperClass lrs = new WrapperClass('Conservative', LowRiskSites );        
        WrapperClass mrs = new WrapperClass('Medium', MediumRiskSites);
        WrapperClass pas = new WrapperClass('Aggressive', ProjSites);
        
        WrapperClass psub = new WrapperClass('Planned', PlanSubj);
        WrapperClass asub = new WrapperClass('Actual', ActualSubj);
        WrapperClass expsub = new WrapperClass('Expected', ExpectedSubj);
        WrapperClass lrsub = new WrapperClass('Conservative', LowRiskSubjects);        
        WrapperClass mrsub = new WrapperClass('Medium', MediumRiskSubjects);
        WrapperClass prjsub = new WrapperClass('Aggressive', ProjSubj);
        System.debug('----LowRiskSubjects-----'+ LowRiskSubjects);
        System.debug('----lrs -----'+ lrs );
        
        //adding the wrappers to list
        //if the graph return type is SitesActivated        
        if(GraphType=='SitesActivated'){
            siteActivationList.add(dates);
            siteActivationList.add(ps);
            siteActivationList.add(aas);            
            siteActivationList.add(lrs);
            siteActivationList.add(mrs);
            siteActivationList.add(pas);            
            graphdata.put(SiteGraph,siteActivationList);
        }else{    
            subEnrollList.add(dates);    
            subEnrollList.add(psub);
            subEnrollList.add(asub);
            subEnrollList.add(expsub);
            subEnrollList.add(lrsub);
            subEnrollList.add(mrsub);
            subEnrollList.add(prjsub); 
            graphdata.put(SubjGraph,subEnrollList);
        }       
        System.debug('----graphdata-----'+ graphdata);
        return graphdata ;
    }
    
     /** 
    @author Bhargav Devaram
    @date 2015 
    @description get all Site Activation and Subject Enrollment graph data in String format
    */ 
    public Map<String,List<String>> graphdatapointsbyformat(Date Weekdate,String value,String type,Map<String,List<String>> datapointmap){
        String DateStr  = '[' + 'Date.UTC(' + Weekdate.year() + ',' + (Weekdate.month()-1) + ',' + Weekdate.day() + ')' + ',' + value + ']' ;
        List<String> DateLstWithParnths = new List<String>();        
        if(!datapointmap.Containskey(type))
            datapointmap.put(type,DateLstWithParnths);
        DateLstWithParnths = datapointmap.get(type);  
        system.debug('---DateLstWithParnths --'+DateLstWithParnths );
        
        DateLstWithParnths.add(DateStr);
        datapointmap.put(type,DateLstWithParnths);
        system.debug('---datapointmap)--'+datapointmap);
        return datapointmap;
    }
    
     /** 
    @author Bhargav Devaram
    @date 2015 
    @descriptionthis method is used to bind the last ended week value with the other graph datapoints to start from there as origin
    */ 
    public Map<String,List<String>> graphdataupdate(String type1,Map<String,List<String>> datapointmap1,String type2,Map<String,List<String>> datapointmap2,integer xx){
        
        List<String> DateLstWithParnths1 = new List<String>(); 
        if(!datapointmap1.Containskey(type1))
            datapointmap1.put(type1,DateLstWithParnths1);
        DateLstWithParnths1 = datapointmap1.get(type1);  
        system.debug('---type1--'+type1+xx);    
        system.debug('---DateLstWithParnths1 --'+DateLstWithParnths1 );
        
        List<String> DateLstWithParnths2 = new List<String>(); 
        if(!datapointmap2.Containskey(type2))
            datapointmap2.put(type2,DateLstWithParnths2);
            DateLstWithParnths2 = datapointmap2.get(type2);            
        system.debug('---DateLstWithParnths2 --'+DateLstWithParnths2 +xx);
        
        if(DateLstWithParnths1.size()>=1) {           
            DateLstWithParnths2[xx-1]= DateLstWithParnths1[xx-1];
        }
        return datapointmap2;
    }
    
    /** 
    @author Bhargav Devaram
    @date 2015 
    @description get all Site Activation and Subject Enrollment graph datain String format
    */
    public String graphdataformat(String type,Map<String,List<String>> datapointmap){        
        List<String> DateLstWithParnths = new List<String>(); 
        if(!datapointmap.Containskey(type))
            datapointmap.put(type,DateLstWithParnths);
        DateLstWithParnths = datapointmap.get(type);  
        String DateLsWithoutParnths = String.join(DateLstWithParnths, ',');
        String DateLsWithoutParnthsInSquare = '[' + DateLsWithoutParnths + ']';    
        String type1;
        if(type.contains('Sites'))
            type1  = type.remove(' Sites'); 
        else  
            type1 = type.remove(' Subjects');   
        String dateComaPlan = '{' + 'name:' + '\''+type1 +'\'' + ',' +'data:' + DateLsWithoutParnthsInSquare + '}';        
        return dateComaPlan;
    }
    
    /** 
    @author Bhargav Devaram
    @date 2015 
    @description get all countries for the PBB Scenario 
    */
    public List<SelectOption> getCntryList() {
        List<SelectOption> CntryList= new List<SelectOption>();        
        CntryList.add(new SelectOption('',''));
        for(Protocol_Country__c c1 : [select name,Client_Protocol__c,region__c,region__r.name from Protocol_Country__c where Client_Protocol__r.Protocal_Unique_Key__c=: ProtocolUniqueKey]){
                //String IRBtype = c1.region__r.IRB_Type__c==null?'':c1.region__r.IRB_Type__c;                           
                //CntryList.add(new SelectOption(c1.id,c1.region__c+'//'+IRBtype));  
                CntryList.add(new SelectOption(c1.region__c,c1.region__r.name));                  
        }
        return CntryList;
    }

    /** 
    @author Bhargav Devaram
    @date 2015 
    @description wrapper class to hold sites and subjects planned, actual and projected values
    */
    public class WrapperClass{
        public String type{ get; set; }
        public List<String> dataValues{ get; set; }
            
        public WrapperClass(String type, List<String> dataValues){
            this.type = type;
            this.dataValues = dataValues;
        }
    }  
}