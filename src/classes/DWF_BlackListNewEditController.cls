public class DWF_BlackListNewEditController {
	
	private list<DWF_Blacklist__c> blackLists {get;set;}
	public DWF_FilterService.employeeVO employeeVO {get;set;}
	public DWF_FilterService.companyVO companyVO {get;set;}
	public DWF_FilterService.businessUnitVO businessUnitVO {get;set;}
	public String selectedCompanyNo {get;set;}
	public String selectedBusinessUnitName {get;set;}
	public String selectedSystemId {get;set;}
	public String selectedEmpFirstName {get;set;}
	public String selectedEmpLastName {get;set;}
	public String selectedEmpStatus {get;set;}
	public list<selectoption> companyNames {get;set;}
	public list<selectoption> businessUnits {get;set;}
	public list<EmployeeWrapper> employeeWrappers {get;set;}
	private list<DWF_FilterService.companyVO> companyVOs;
	ApexPages.standardController standardController;
	
	// Initialize data
	public DWF_BlackListNewEditController(ApexPages.standardController stdCtr) {
		standardController = stdCtr;
		String recordID = standardController.getId();
		companyNames = getCompanyNames();
		businessUnits = getBusinessUnits(null);
		blackLists = new list<DWF_Blacklist__c>();
	}
	
	// Find employees based on companyNo and businessUnitNo
	public void searchEmployees() {
		try {
			employeeWrappers = new list<EmployeeWrapper>();
			list<DWF_FilterService.employeeVO> employees = new list<DWF_FilterService.employeeVO>();
			DWF_FilterService.employeeListResponse response = new DWF_FilterService.employeeListResponse();
			response = DWF_FilterServiceWrapper.getEmployeeListByCompanyEmpData(Long.valueOf(selectedCompanyNo), selectedBusinessUnitName, selectedEmpFirstName, 
				selectedEmpLastName, selectedEmpStatus);
			employees = response.employeeVOs;
			if (employees != null) {
				if (employees.size() <= 1000) {
					for (DWF_FilterService.employeeVO employeeVO : employees) {
						EmployeeWrapper wrapper = new EmployeeWrapper(false, employeeVO);
						employeeWrappers.add(wrapper);
					}
				} else {
					Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Search returned more than 1000 results, please refine your search.'));
				}
			} else {
				Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'Search returned 0 results, please refine your search.'));
			}
		} catch (Exception e) {
			Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
		}
	}
	
	// Get companyVO from companyNo
	public void getCompanyFromSelectedValue() {
		companyVO = new DWF_FilterService.companyVO();
		if (companyVOs != null) {
			for (DWF_FilterService.companyVO compVO : companyVOs) {
				if (selectedCompanyNo.equals(String.valueOf(compVO.companyNo))) {
					companyVO = compVO;
					businessUnits = getBusinessUnits(compVO.businessUnits);
				}
			}
		}
	}
	
	// Get businessUnitVO from the selected BU dropdown value
	public void getBusinessUnitFromSelectedValue() {
		businessUnitVO = new DWF_FilterService.businessUnitVO();
		if (selectedBusinessUnitName != null) {
			for (DWF_FilterService.businessUnitVO businessUnit : companyVO.businessUnits) {
				if (businessUnit.businessUnitName.equals(selectedBusinessUnitName)) {
					businessUnitVO = businessUnit;
				}
			}
		}
	}
	
	// Populate companyName dropdown list
	public list<selectoption> getCompanyNames() {
		DWF_FilterService.companyListResponse response = DWF_FilterServiceWrapper.getAllCompanyVOs();
		companyVOs = response.companyVOs;
		return DWF_Util.getCompanyNames(companyVOs);
	}
	
	// Populate the businessUnit dropdown list
	public list<selectoption> getBusinessUnits(list<DWF_FilterService.businessUnitVO> businessUnitVOs) {
		return DWF_Util.getBusinessUnits(businessUnitVOs);	
	}
	
	// Populate the systemName dropdown
	public list<selectoption> getSystemNames() {
		return DWF_Util.getSystemNames();
	}
	
	// Persist the blacklist record(s) to the database
	public PageReference saveBlackList() {
		list<DWF_Blacklist__c> objsToUpsert = new list<DWF_Blacklist__c>();
		if (employeeWrappers != null && employeeWrappers.size() > 0) {
			// get blackList objects to upsert
			set<String> employeeIds = DWF_Util.getCurrentEmployeeBLsForSystem(selectedSystemId);
			for (EmployeeWrapper employee : employeeWrappers) {
				if (employee.isSelected && !employeeIds.contains(String.valueOf(employee.employeeVO.employeeId))) {
					objsToUpsert.add(new DWF_Blacklist__c(Employee_Id__c = String.valueOf(employee.employeeVO.employeeId), 
						DWF_System__c = selectedSystemId));
				}
			}
			// Perform data validation
			if (objsToUpsert.size() > 0) {
				try {
					upsert objsToUpsert;
				} catch (Exception e) {
					Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
				}
			}
			return standardController.cancel();
		}
		return null;
	}
	
	// Wrapper for the Employee search
	public class EmployeeWrapper {
		
		public Boolean isSelected {get;set;}
		public DWF_FilterService.employeeVO employeeVO {get;set;}
		
		public EmployeeWrapper(Boolean isSelect, DWF_FilterService.employeeVO empVO) {
			isSelected = isSelect;
			employeeVO = empVO;
		}
	}
	
}