global class PIT_CTMSBatch implements Database.Batchable<sObject>,Database.AllowsCallouts 
{
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Site_ID__c FROM WFM_Site_Detail__c where Site_ID__c =\'1-213KEA\'';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<WFM_Site_Detail__c> scope)
    {
         for(WFM_Site_Detail__c st : scope)
         {
              PIT_CtmsService.ICtmsServicePort testctms = new PIT_CtmsService.ICtmsServicePort();
              System.debug(testctms.getSubjectBySite(st.Site_ID__c));
          
         }
         
    }   
    global void finish(Database.BatchableContext BC)
    {
    }
}