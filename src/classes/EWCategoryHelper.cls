/**
 * Created by illia on 12/1/15.
 * This class contains helper methods for EW Category entity
 */

public with sharing class EWCategoryHelper
{
//	private List<EW_Category__c> relatedCategories = new List<EW_Category__c>();
	private Map<ID, Set<ID>> categoriesBySourceIdMap = new Map<ID, Set<ID>>();
	private Set<String> processedCombinations = new Set<String>();

	/**
	* Associates categories with proper parents
	 */
	//@TODO: Finish this method!
	public void associateCategories(List<EW_Category__c> categories)
	{
		List<ID> categoryIDs = new List<ID>();
		for (EW_Category__c each : categories)
		{
			categoryIDs.addAll(new List<ID>{each.Id, each.Cloned_From_Category__c});
		}

		List<EW_Category__c> newCategories = new List<EW_Category__c>();
 		Map<ID, EW_Category__c> extendedCategoriesMap = new Map<ID, EW_Category__c>([select Name, Related_Record_ID__c, Cloned_From_Category__c,
				(select Parent_Category__c, Child_Category__c, Parent_Category__r.Cloned_From_Category__c from Early_Warnings__r),
				(select Parent_Category__c, Parent_Category__r.Cloned_From_Category__c, Parent_Category__r.Name, Parent_Category__r.Buffer_Index_Strategy__c from Parent_Junctions__r)
				from EW_Category__c where Id in :categoryIDs or Cloned_From_Category__c in :categoryIDs]);

		List<EW_Category__c> allCategories = extendedCategoriesMap.values();

		for (EW_Category__c categoryRecord : categories)
		{
			if (categoryRecord.Cloned_From_Category__c == null || categoryRecord.Related_Record_ID__c == null)
			{
				continue;
			}

//			EW_Category__c extendedCategory = extendedCategoriesMap.get(categoryRecord.Id);
			EW_Category__c templateCategory = extendedCategoriesMap.get(categoryRecord.Cloned_From_Category__c);
//
//			Boolean junctionExists = false;
//			for (EW_Category_EW_Junction__c ewJunction : extendedCategory.Parent_Junctions__r)
//			{
//				if (ewJunction.Parent_Category__r.Cloned_From_Category__c == templateCategory.Id)
//				{
//					junctionExists = true;
//				}
//			}
//
//			if (junctionExists)
//			{
//				continue;
//			}

			for (EW_Category_EW_Junction__c parentJunction : templateCategory.Parent_Junctions__r)
			{
				Boolean categoryExists = false;
				for (EW_Category__c eachCategory : allCategories)
				{
					if (eachCategory.Cloned_From_Category__c == parentJunction.Parent_Category__c
							&& eachCategory.Related_Record_ID__c == categoryRecord.Related_Record_ID__c)
					{
						categoryExists = true;
						break;
					}
				}

				if (!categoryExists)
				{
					EW_Category__c newCategory = parentJunction.Parent_Category__r.clone(false, true);
					newCategory.Cloned_From_Category__c = parentJunction.Parent_Category__c;
					newCategory.Related_Record_ID__c = categoryRecord.Related_Record_ID__c;
					newCategory.Type__c = 'Operational';

					newCategories.add(newCategory);
					allCategories.add(newCategory);
				}
			}

		}

		insert newCategories;
		List<EW_Category_EW_Junction__c> newJunctions = new List<EW_Category_EW_Junction__c>();

		for (EW_Category__c categoryRecord : categories)
		{
			if (categoryRecord.Cloned_From_Category__c == null || categoryRecord.Related_Record_ID__c == null)
			{
				continue;
			}

			EW_Category__c extendedCategory = extendedCategoriesMap.get(categoryRecord.Id);
			EW_Category__c templateCategory = extendedCategoriesMap.get(categoryRecord.Cloned_From_Category__c);

			for (EW_Category_EW_Junction__c parentJunction : templateCategory.Parent_Junctions__r)
			{
				Boolean junctionExists = false;
				for (EW_Category_EW_Junction__c ewJunction : extendedCategory.Parent_Junctions__r)
				{
					if (ewJunction.Parent_Category__r.Cloned_From_Category__c == parentJunction.Parent_Category__c)
					{
						junctionExists = true;
					}
				}

				if (junctionExists)
				{
					continue;
				}

				Boolean categoryExists = false;
				for (EW_Category__c eachCategory : allCategories)
				{
					if (eachCategory.Cloned_From_Category__c == parentJunction.Parent_Category__c
							&& eachCategory.Related_Record_ID__c == categoryRecord.Related_Record_ID__c)
					{
						newJunctions.add(new EW_Category_EW_Junction__c(
								Parent_Category__c = eachCategory.Id,
								Child_Category__c = categoryRecord.Id
						));

						break;
					}
				}
			}

		}

		insert newJunctions;

//		List<EW_Category__c> extendedCategories = [select Name, Cloned_From_Category__c,
//				(select Parent_Category__c, Parent_Category__r.Cloned_From_Category__c from Parent_Junctions__r)
//				from EW_Category__c where Id in :categories];
//
//		List<ID> templateIDs = new List<ID>();
//		for (EW_Category__c categoryRecord : categories)
//		{
//			templateIDs.add(categoryRecord.Cloned_From_Category__c);
//		}
//
//		Map<ID, EW_Category__c> templateCategories = new Map<ID, EW_Category__c>([select Name,
//				(select Child_Category__c from Early_Warnings__r)
//				from EW_Category__c where Id in :templateIDs]);
//
//		List<EW_Category_EW_Junction__c> newJunctions = new List<EW_Category_EW_Junction__c>();
//		for (EW_Category__c extendedCategory : extendedCategories)
//		{
//			EW_Category__c templateCategory = templateCategories.get(extendedCategory.Cloned_From_Category__c);
//			for (Integer i = 0; templateCategory != null && i < templateCategory.Early_Warnings__r.size(); i++)
//			{
//				EW_Category_EW_Junction__c templateJunction = templateCategory.Early_Warnings__r.get(i);
//				//Skip templateJunction if it's not related to a current category
//				if (templateJUnction.Child_Category__c != extendedCategory.Cloned_From_Category__c)
//				{
//					continue;
//				}
//
//				Boolean operationalJunctionExists = false;
//				for (EW_Category_EW_Junction__c operationalJunction : extendedCategory.Parent_Junctions__r)
//				{
//					if (operationalJunction.Parent_Category__r.Cloned_From_Category__c == templateJunction.Parent_Category__c)
//					{
//						operationalJunctionExists = true;
//						break;
//					}
//				}
//
//				if (!operationalJunctionExists)
//				{
//					newJunctions.add(new EW_Category_EW_Junction__c());
//				}
//			}
//		}
	}

	public void cloneCategories(List<Critical_Chain__c> chains)
	{
		List<Critical_Chain__c> extendedChains = selectExtendedChains(chains);
		List<EW_Category__c> categories = selectRelatedCategories(extendedChains);
		List<EW_Category__c> newCategories = new List<EW_Category__c>();
		for (Critical_Chain__c chain : extendedChains)
		{
			newCategories.addAll(buildEWCategories(chain, categories));
		}

		insert newCategories;

		categories = selectRelatedCategories(extendedChains);
		List<EW_Category_EW_Junction__c> newJunctions = new List<EW_Category_EW_Junction__c>();
		for (Critical_Chain__c chain : extendedChains)
		{
			List<EW_Category__c> templateCategories = chooseTemplateCategories(chain.Cloned_From_Early_Warning__r, categories);
			for (EW_Category__c templateCategory : templateCategories)
			{
				newJunctions.addAll(buildEWCategoryJunctions(chain, templateCategory, categories));
			}
		}

		insert newJunctions;
	}

	private List<Critical_Chain__c> selectExtendedChains(List<Critical_Chain__c> chains)
	{
		return [select Id, Name, Cloned_From_Early_Warning__r.Name, Cloned_From_Early_Warning__r.Id,
				Cloned_From_Early_Warning__c, Flow__r.STSWR1__Source_ID__c
				from Critical_Chain__c where Id in :chains];
	}

	private List<EW_Category__c> selectRelatedCategories(List<Critical_Chain__c> chains)
	{
		List<ID> chainIDs = new List<ID>();
		for (Critical_Chain__c each : chains)
		{
			chainIDs.addAll(new List<ID>{each.Id, each.Cloned_From_Early_Warning__c});
		}

		List<EW_Category__c> relatedCategories = [select Name, Cloned_From_Category__c, Type__c, Buffer_Index_Strategy__c,
		(select Early_Warning__c, Child_Category__c, Early_Warning__r.Cloned_From_Early_Warning__c,
				Early_Warning__r.Flow__r.STSWR1__Source_ID__c from Early_Warnings__r)
		from EW_Category__c
		where Id in (select Parent_Category__c from EW_Category_EW_Junction__c where Early_Warning__c in :chainIDs)];

		relatedCategories.addAll([select Name, Cloned_From_Category__c, Type__c, Buffer_Index_Strategy__c,
		(select Early_Warning__c, Child_Category__c, Early_Warning__r.Cloned_From_Early_Warning__c,
				Early_Warning__r.Flow__r.STSWR1__Source_ID__c from Early_Warnings__r)
		from EW_Category__c
		where Cloned_From_Category__c in (select Parent_Category__c from EW_Category_EW_Junction__c where Early_Warning__c in :chainIDs)]);

		return relatedCategories;
	}


	/**
	* Chooses template categories for given template chain
	 */
	private List<EW_Category__c> chooseTemplateCategories(Critical_Chain__c templateChain, List<EW_Category__c> categories)
	{
		List<EW_Category__c> templateCategories = new List<EW_Category__c>();
		for (Integer i = 0, n = categories.size(); templateChain != null && i < n; i++)
		{
			EW_Category__c categoryRecord = categories.get(i);
			for (EW_Category_EW_Junction__c ewJunction : categoryRecord.Early_Warnings__r)
			{
				if (ewJunction.Early_Warning__c == templateChain.Id)
				{
					templateCategories.add(categoryRecord);
				}
			}
		}

		return templateCategories;
	}

//	/*
//	* Method considers that "operational" category exist when the following conditions are met:
//	* - operational category was cloned from template category which has junction to a template chain,
//	* from which operational chain has been cloned
//	 */
//	private Boolean isOperationalCategoryExists(Critical_Chain__c operationalChain, EW_Category__c templateCategory,
//			List<EW_Category__c> operationalCategories)
//	{
//		for (EW_Category__c operationalCategory : operationalCategories)
//		{
//			for (EW_Category_EW_Junction__c operationalJunction : operationalCategory.Early_Warnings__r)
//			{
//				if (operationalJunction.Early_Warning__c != operationalChain.Id)
//				{
//					continue;
//				}
//
//				for (EW_Category_EW_Junction__c templateJunction : templateCategory.Early_Warnings__r)
//				{
//					if (templateJunction.Early_Warning__c == operationalChain.Cloned_From_Early_Warning__c)
//					{
//						return true;
//					}
//				}
//			}
//		}
//
//		return false;
//	}

	/**
	* Builds new EW_Category_EW_Junction__c records for operational EW and template Category
	 */
	private List<EW_Category_EW_Junction__c> buildEWCategoryJunctions(Critical_Chain__c operationalChain,
			EW_Category__c templateCategory, List<EW_Category__c> operationalCategories)
	{
		List<EW_Category_EW_Junction__c> newJunctions = new List<EW_Category_EW_Junction__c>();
		for (EW_Category__c operationalCategory : operationalCategories)
		{
			if (operationalCategory.Cloned_From_Category__c != templateCategory.Id)
			{
				continue;
			}

			Boolean operationalExists = false;
			for (EW_Category_EW_Junction__c operationalJunction : operationalCategory.Early_Warnings__r)
			{
				if (operationalJunction.Early_Warning__c == operationalChain.Id)
				{
					operationalExists = true;
				}
			}

			if (!operationalExists && processedCombinations.add('' + operationalCategory.Id + operationalChain.Id))
			{
				newJunctions.add(new EW_Category_EW_Junction__c(
						Parent_Category__c = operationalCategory.Id,
						Early_Warning__c = operationalChain.Id
				));
			}
		}

		return newJunctions;
	}

	/**
	* Builds new EW_Category__c records for operational EW
	 */
	private List<EW_Category__c> buildEWCategories(Critical_Chain__c operationalChain, List<EW_Category__c> categories)
	{
		List<EW_Category__c> newCategories = new List<EW_Category__c>();
		List<EW_Category__c> templateCategories = chooseTemplateCategories(operationalChain.Cloned_From_Early_Warning__r, categories);

		for (EW_Category__c templateCategory : templateCategories)
		{
			Boolean operationalCategoryExists = false;
			for (EW_Category__c category : categories)
			{
				for (Integer i = 0, n = templateCategory.Early_Warnings__r.size();
						category.Cloned_From_Category__c == templateCategory.Id && i < n; i++)
				{
					EW_Category_EW_Junction__c templateJunction = templateCategory.Early_Warnings__r.get(i);
					for (Integer j = 0, m = category.Early_Warnings__r.size();
							templateJunction.Early_Warning__c == operationalChain.Cloned_From_Early_Warning__c && j < m; j++)
					{
						EW_Category_EW_Junction__c operationalJunction = category.Early_Warnings__r.get(j);
						if (operationalJunction.Early_Warning__r.Flow__r.STSWR1__Source_ID__c == operationalChain.Flow__r.STSWR1__Source_ID__c)
						{
							operationalCategoryExists = true;
						}
					}
				}
			}

			if (!operationalCategoryExists
					&& processedCombinations.add('' + templateCategory.Id + operationalChain.Flow__r.STSWR1__Source_ID__c))
			{
				EW_Category__c newCategory = templateCategory.clone(false, true);
				newCategory.Cloned_From_Category__c = templateCategory.Id;
				newCategory.Type__c = 'Operational';

				newCategories.add(newCategory);
			}
		}

		return newCategories;
	}

//	/**
//	* Chooses all Categories
//	 */
//	private List<EW_Category__c> chooseExistingCategories(Critical_Chain__c operationalChain, List<EW_Category__c> categories)
//	{
//		List<EW_Category__c> existingCategories = new List<EW_Category__c>();
//		for (EW_Category__c categoryRecord : categories)
//		{
//			for (Integer i = 0, n = categoryRecord.Early_Warnings__r.size();
//					categoryRecord.Cloned_From_Category__c != null && i < n; i++)
//			{
//				if (categoryRecord.Early_Warnings__r.get(i).Early_Warning__r.Flow__r.STSWR1__Source_ID__c
//						== operationalChain.Flow__r.STSWR1__Source_ID__c)
//				{
//					existingCategories.add(categoryRecord);
//				}
//			}
//		}
//
//		return existingCategories;
//	}

//	/**
//	* Matches EW_Category__c record against given EW (Critical_Chain__c) record
//	 */
//	private EW_Category__c matchExistingCategory(Critical_Chain__c chain, List<EW_Category__c> categories, EW_Category__c templateCategory)
//	{
//		for (EW_Category__c categoryRecord : categories)
//		{
//			if (templateCategory == null || categoryRecord.Cloned_From_Category__c != templateCategory.Id)
//			{
//				continue;
//			}
//
//			for (EW_Category_EW_Junction__c ewJunction : categoryRecord.Early_Warnings__r)
//			{
//				if (ewJunction.Early_Warning__c == chain.Id
//						|| ewJunction.Early_Warning__r.Flow__r.STSWR1__Source_ID__c == chain.Flow__r.STSWR1__Source_ID__c)
//				{
//					return categoryRecord;
//				}
//			}
//		}
//
//		return null;
//	}

//	private List<EW_Category__c> buildCategories(Critical_Chain__c chain, List<EW_Category__c> categories)
//	{
//		List<EW_Category__c> newCategories = new List<EW_Category__c>();
//
//		List<EW_Category__c> templateCategories = chooseTemplateCategories(chain.Cloned_From_Early_Warning__r, categories);
//		EW_Category__c existingCategory = matchExistingCategory(chain, categories, templateCategory);
//
//		if (templateCategory != null)
//		{
//
//		}
//
//		return newCategories;
//	}

//	private Boolean junctionExists(EW_Category__c categoryRecord, Critical_Chain__c chainRecord)
//	{
//		for (EW_Category_EW_Junction__c ewJunction : categoryRecord.Early_Warnings__r)
//		{
//			if (ewJunction.Early_Warning__c == chainRecord.Id)
//			{
//				return true;
//			}
//		}
//
//		return false;
//	}
}