/**
 * @description	The API that applications will communicate with the 'Notification & Communication Module'
 * @author		Dimitrios Sgourdos
 * @date		Created: 01-Sep-2015, Edited: 25-Sep-2015
 */
public class NCM_API {
	
	
// *********************************************************************************************************************
//				Build / Search for Notification Registrations
// *********************************************************************************************************************
	
	/**
	 * @description	Build registrations in the 'NotificationRegistrationWrapper' format, to let users register for a
	 *				notification topic. The data includes all the current active registrations for the given user and
	 *				for the given related object id (thus also for the given category), but also registrations 
	 *				instances for the notification topics that the user isn't subscribed (for the given related object
	 *				id). The data are ordered by the notification topic name.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	 * @param		userId                  The id of the user that is assigned with the registrations
	 * @param		relatedToId             The related object id that is connected with the registrations
	 * @param		notificationCategory    The category of the notification topics that are connected with the
	 *										registrations
	 * @return		The registration data in the 'NotificationRegistrationWrapper' format including existing
	 *				registrations and possible new registrations.
	*/
	public static List<NCM_API_DataTypes.NotificationRegistrationWrapper> buildAllRegistrationsForNotifications(
																						ID userId,
																						String relatedToId,
																						String notificationCategory) {
		return NCM_SrvLayer_NotificationRegistration.buildAllRegistrationsForNotifications(	userId,
																							relatedToId,
																							notificationCategory);
	}
	
	/**
	 * @description	Build registrations in the 'NotificationRegistrationWrapper' format, to let users register for a
	 *				notification topic. The data includes all the current active registrations for the given user and
	 *				for the given category, but also registration instances for the notification topics that the user
	 *				isn't subscribed. The data are ordered by the notification topic name.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	 * @param		userId                  The id of the user that is assigned with the registrations
	 * @param		notificationCategory    The category of the notification topics that are connected with the
	 *										registrations
	 * @return		The registration data in the 'NotificationRegistrationWrapper' format including existing
	 *				registrations and possible new registrations.
	*/
	public static List<NCM_API_DataTypes.NotificationRegistrationWrapper> buildAllRegistrationsForNotifications(
																					ID userId,
																					String notificationCategory) {
		return NCM_SrvLayer_NotificationRegistration.buildAllRegistrationsForNotifications(	userId,
																							notificationCategory);
	}
	
	/**
	 * @description	Build active registrations in the 'NotificationRegistrationWrapper' format. The data includes all
	 *				the current active registrations for the given user and for the given related object id (thus also
	 *				for the given category). The data are ordered by the notification topic name.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	 * @param		userId                  The id of the user that is assigned with the registrations
	 * @param		relatedToId             The related object id that is connected with the registrations
	 * @param		notificationCategory    The category of the notification topic that are connected with the
	 *										registrations
	 * @return		The active registrations in the 'NotificationRegistrationWrapper' format
	*/
	public static List<NCM_API_DataTypes.NotificationRegistrationWrapper> buildActiveRegistrationsForNotifications(
																						ID userId,
																						String relatedToId,
																						String notificationCategory) {
		return NCM_SrvLayer_NotificationRegistration.buildActiveRegistrationsForNotifications(userId,
																							relatedToId,
																							notificationCategory);
	}
	
	/**
	 * @description	Build active registrations in the 'NotificationRegistrationWrapper' format. The data includes all
	 *				the current active registrations for the given user and for the given category.
	 *				The data are ordered by the notification topic name.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 17-Sep-2015
	 * @param		userId                  The id of the user that is assigned with the registrations
	 * @param		notificationCategory    The category of the notification topic
	 * @return		The active registrations in the 'NotificationRegistrationWrapper' format
	*/
	public static List<NCM_API_DataTypes.NotificationRegistrationWrapper> buildActiveRegistrationsForNotifications(
																						ID userId,
																						String notificationCategory) {
		return NCM_SrvLayer_NotificationRegistration.buildActiveRegistrationsForNotifications(userId,
																							notificationCategory);
	}
	
	/**
	 * @description	Build registrations in the 'NotificationRegistrationWrapper' format.The built data are registrations
	 *				instances for the notification topics that the user isn't already subscribed (for the given related
	 *				object id). The data are ordered by the notification topic name.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	 * @param		userId                  The id of the user that will be assigned with the registrations
	 * @param		relatedToId             The related object id that will be connected with the registrations
	 * @param		notificationCategory    The category of the notification topics
	 * @return		The registration instances in the 'NotificationRegistrationWrapper' format
	*/
	public static List<NCM_API_DataTypes.NotificationRegistrationWrapper> buildNonSubscribedRegistrationsForNotifications(
																						ID userId,
																						String relatedToId,
																						String notificationCategory) {
		return NCM_SrvLayer_NotificationRegistration.buildNonSubscribedRegistrationsForNotifications(userId,
																								relatedToId,
																								notificationCategory);
	}
	
	/**
	 * @description	Build registrations in the 'NotificationRegistrationWrapper' format.The built data are registrations
	 *				instances for the notification topics that the user isn't already subscribed. The data are ordered
	 *				by the notification topic name.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 17-Sep-2015
	 * @param		userId                  The user that is assigned with the registrations
	 * @param		notificationCategory    The category of the notification topic
	 * @return		The registration instances in the 'NotificationRegistrationWrapper' format
	*/
	public static List<NCM_API_DataTypes.NotificationRegistrationWrapper> buildNonSubscribedRegistrationsForNotifications(
																						ID userId,
																						String notificationCategory) {
		return NCM_SrvLayer_NotificationRegistration.buildNonSubscribedRegistrationsForNotifications(
																								userId,
																								notificationCategory);
	}
	
	
	/**
	 * @description	Get the number Of Active Registrations per Related Object for the given user
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 24-Sep-2015
	 * @param		userId    The id of the user that the results are connected with
	 * @return		The Map with key the related object id and value the Number of active registrations assigned to
	 *				this related object id for the given user
	*/
	public static Map<String, Integer> getNumberOfActiveRegistrationsPerRelatedObjectByUser(Id userId) {
		return NCM_SrvLayer_NotificationRegistration.getNumberOfActiveRegistrationsPerRelatedObjectByUser(userId);
	}
	
	
// *********************************************************************************************************************
//				Create Notification Registrations
// *********************************************************************************************************************
	/**
	 * @description	Register the given subscriptions to the Notification Registration object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 03-Sep-2015
	 * @param		source    The subscriptions to be registered
	*/
	public static void registerForNotifications(List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrations) {
		NCM_SrvLayer_NotificationRegistration.registerForNotifications(registrations);
	}
	
	
// *********************************************************************************************************************
//				Disable Notification Registrations
// *********************************************************************************************************************
	
	/**
	 * @description	Unregister the given subscriptions by making the Active field equal to false in the corresponding
	 *				records in the Notification Registration object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 25-Sep-2015
	 * @param		source    The subscriptions to be unregistered
	*/
	public static void disableRegistrationsByWrapperData(List<NCM_API_DataTypes.NotificationRegistrationWrapper> source) {
		NCM_SrvLayer_NotificationRegistration.disableRegistrationsByWrapperData(source);
	}
	
	
	/**
	 * @description	Disable the subscriptions to notifications that corresponds to the given notification registration
	 *				ids.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015
	 * @param		registrationsIds    The ids of the subscriptions that must be disabled
	*/
	public static void disableRegistrationsByIds (Set<ID> registrationsIds) {
		NCM_SrvLayer_NotificationRegistration.disableRegistrationsByIds(registrationsIds);
	}
	
	/**
	 * @description	Disable all the subscriptions that are related to the given object id and to the given user
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015
	 * @param		userId         The id of the user that his subscriptions will be disabled
	 * @param		relatedToId    The id of the object that is related to the subscriptions that will be disabled
	*/
	public static void disableRegistrationsByUserAndRelatedToId(ID userId, String relatedToId) {
		NCM_SrvLayer_NotificationRegistration.disableRegistrationsByUserAndRelatedToId(userId, relatedToId);
	}
	
	/**
	 * @description	Disable all the subscriptions that are connected to the given catalog and to the given user
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 02-Sep-2015
	 * @param		userId                   The id of the user that his subscriptions will be disabled
	 * @param		notificationCatalogId    The id of the notification topic that is related to the subscription that
	 *										 will be disabled.
	*/
	public static void disableRegistrationsByUserAndNotificationTopic(ID userId, ID notificationCatalogId) {
		NCM_SrvLayer_NotificationRegistration.disableRegistrationsByUserAndNotificationTopic(	userId,
																								notificationCatalogId);
	}
	
	/**
	 * @description	Disable all the subscriptions of the given user
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015
	 * @param		userId    The id of the user that all his subscriptions will be disabled
	*/
	public static void disableRegistrationsByUser (ID userId) {
		NCM_SrvLayer_NotificationRegistration.disableRegistrationsByUser(userId);
	}
	
	/**
	 * @description	Disable all the subscriptions that are related to the given object id
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015
	 * @param		relatedToId    The id of the object that is related to the subscription
	*/
	public static void disableRegistrationsByRelatedToId (String relatedToId) {
		NCM_SrvLayer_NotificationRegistration.disableRegistrationsByRelatedToId(relatedToId);
	}
	
	
// *********************************************************************************************************************
//				Insert, update, disable Notification Catalog entries
// *********************************************************************************************************************
	
	/**
	 * @description	Insert the given Notification Catalog wrapper instances in the Notification Catalog object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 * @param		source    The wrapper data that holds the new Notification Catalog records
	*/
	public static void insertNotificationCatalogEntries(List<NCM_API_DataTypes.NotificationCatalogWrapper> source) {
		NCM_SrvLayer_NotificationCatalog.insertNotificationCatalogEntries(source);
	}
	
	
	/**
	 * @description	Update the given Notification Catalog wrapper instances in the Notification Catalog object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 * @param		source    The wrapper that corresponds to the Notification Catalog records to be updated
	*/
	public static void updateNotificationCatalogEntries(List<NCM_API_DataTypes.NotificationCatalogWrapper> source) {
		NCM_SrvLayer_NotificationCatalog.updateNotificationCatalogEntries(source);
	}
	
	
	/**
	 * @description	Disable the notification catalog entries that corresponds to the given topic names
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 * @param		topicNameSet    The names of the notification catalog entries that we want to disable
	*/
	public static void disableNotificationCatalogEntriesByName(Set<String> topicNameSet) {
		NCM_SrvLayer_NotificationCatalog.disableNotificationCatalogEntriesByName(topicNameSet);
	}
	
	
	/**
	 * @description	Disable the notification catalog entries that corresponds to the given notification catalog wrapper
	 *				data
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 * @param		source    The wrapper data that corrresponds to the Notification Catalog records to be disabled
	*/
	public static void disableNotificationCatalogEntries(List<NCM_API_DataTypes.NotificationCatalogWrapper> source) {
		NCM_SrvLayer_NotificationCatalog.disableNotificationCatalogEntries(source);
	}
	
	
// *********************************************************************************************************************
//				Retrieve and acknowledge notifications
// *********************************************************************************************************************
	
	/**
	 * @description	Build notifications in the 'NotificationWrapper' format, to let the given user acknowledge pending
	 				notifications. The data includes all the 'Pending Acknowledgment' notifications for the given user.
	 *				The data are ordered by the notification Reminder.
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 10-Sep-2015
	 * @param		userId    The id of the user that is assigned with the 'Pending Acknowledgement' notifications
	 * @return		The 'Pending Acknowledgement' notifications of the given user
	*/
	public static List<NCM_API_DataTypes.NotificationWrapper> getPendingNotificationsByUser(Id userId) {
		return NCM_SrvLayer_Notification.getPendingNotificationsByUser(userId);
	}
	
	/**
	 * @description	Build notifications in the 'NotificationWrapper' format, to let the given user acknowledge pending
	 *				notifications under a specific related to object. The data are ordered by the notification Reminder.
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 10-Sep-2015
	 * @param		userId         The id of the user that is assigned with the 'Pending Acknowledgement' notifications
	 * @param		relatedToId    The related Id that corresponds to the the notifications
	 * @return		The 'Pending Acknowledgement' notifications of the given user and related to id
	*/
	public static List<NCM_API_DataTypes.NotificationWrapper> getPendingNotificationsByUserAndRelatedObject(
																								Id userId,
																								String relatedToId) {
		return NCM_SrvLayer_Notification.getPendingNotificationsByUserAndRelatedObject(userId, relatedToId);
	}
	
	/**
	 * @description	Get the number Of Pending Notifications per Related Object for the given user
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 11-Sep-2015
	 * @param		userId    The id of the user to find the number of pending notification per related object
	 * @return		The Map with key the related object id and value the Number of pending acknowledgement notifications
	*/
	public static Map<String, Integer> getNumberOfPendingNotificationsPerRelatedObjectByUser(Id userId) {
		return NCM_SrvLayer_Notification.getNumberOfPendingNotificationsPerRelatedObjectByUser(userId);
	}
	
	/**
	 * @description	Acknowledge the given notifications
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 10-Sep-2015
	 * @param		source    The wrapper data that holds the pending acknowledgement notifications
	*/
	public static void acknowledgePendingNotifications(List<NCM_API_DataTypes.NotificationWrapper> source) {
		NCM_SrvLayer_Notification.acknowledgePendingNotifications(source);
	}
	
	
// *********************************************************************************************************************
//				Trigger notification events
// *********************************************************************************************************************

	/**
	 * @description	Create the notification events, notifications per subscribed user and the history of communication
	 *				with users. Also, it sends the selected per user communication (email, sms or chatter post).
	 * @author		Jegadheesan Muthusamy, Dimitrios Sgourdos
	 * @date		Created: 10-Sep-2015
	 * @param		notificationCatalogId    The id of the notification topic that occurs
	 * @param		relatedToId              The related object id that is connected with the registrations
	*/
	@future
	public static void activateNotificationEventById(Id notificationCatalogId, String relatedToId) {
		NCM_SrvLayer_NotificationEvent.activateNotificationEventById( notificationCatalogId, relatedToId );
	}
	
	
	/**
	 * @description	Create the notification events, notifications per subscribed user and the history of communication
	 *				with users. Also, it sends the selected per user communication (email, sms or chatter post).
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015
	 * @param		notificationCatalogName    The name of the notification topic that occurs
	 * @param		relatedToId                The related object id that is connected with the registrations
	*/
	@future
	public static void activateNotificationEventByName(String notificationCatalogName, String relatedToId) {
		NCM_SrvLayer_NotificationEvent.activateNotificationEventByName( notificationCatalogName, relatedToId );
	}
	
// *********************************************************************************************************************
//				Transformations between DataTypes and objects
// *********************************************************************************************************************
	
	/**
	 * @description	Retrieve a notification registration instance with attributes filled from the corresponding wrapper
	 *				data type. The record that will be refreshed with new values is passed as parameter in order to
	 *				capture situations that we want to refresh an existing (already stored in the DB) record.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015
	 * @param		source    The wrapper that holds the values for the attributes of the Notification Registration Record
	 * @param		result    The Notification Registration record to be updated with the attributes from the wrapper item
	 * @return		The refreshed Notification Registration record.
	*/
	public static NotificationRegistration__c getNotificationRegistrationFromWrapperData(
															NCM_API_DataTypes.NotificationRegistrationWrapper source,
															NotificationRegistration__c result) {
		return NCM_SrvLayer_NotificationRegistration.getNotificationRegistrationFromWrapperData(source, result);
	}
	
	
	/**
	 * @description	Retrieve a notification catalog instance with attributes filled from the corresponding wrapper
	 *				data type. The record that will be refreshed with new values is passed as parameter in order to
	 *				capture situations that we want to refresh an existing (already stored in the DB) record.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 * @param		source    The wrapper that holds the values for the attributes of the Notification Catalog Record
	 * @param		result    The Notification Catalog record to be updated with the attributes from the wrapper item
	 * @return		The refreshed Notification Catalog record.
	*/
	public static NotificationCatalog__c getNotificationCatalogFromWrapperData(
																	NCM_API_DataTypes.NotificationCatalogWrapper source,
																	NotificationCatalog__c result) {
		return NCM_SrvLayer_NotificationCatalog.getNotificationCatalogFromWrapperData(source, result);
	}
	
	
	/**
	 * @description	Retrieve a notification instance with attributes filled from the corresponding wrapper data type.
	 *				The record that will be refreshed with new values is passed as parameter in order to capture
	 *				situations that we want to refresh an existing (already stored in the DB) record.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015
	 * @param		source    The wrapper that holds the values for the attributes of the Notification Record
	 * @param		result    The Notification record to be updated with the attributes from the wrapper item
	 * @return		The updated Notification record.
	*/
	public static Notification__c getNotificationFromWrapperData(	NCM_API_DataTypes.NotificationWrapper source,
																	Notification__c result) {
		return NCM_SrvLayer_Notification.getNotificationFromWrapperData(source, result);
	}
}