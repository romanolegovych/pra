/**
* @author Ramya Shree Edara
* @date 21-Oct-2014
* @description this is test class for scenario related classes
*/
@isTest
public with sharing class SRM_ScenarioDataAccessorTest{
    
    public static testMethod void testDataAccessor(){
    	
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Rating = 'A';
        acc.Type = 'Competitor';
        acc.Industry = 'Small/Speciality Pharma';
        insert acc;
        
        Test.startTest();
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Oppty';
        opp.AccountId = acc.Id;
        opp.StageName = 'Opportunity Identified';
        opp.Type = 'New Business';
        opp.Business_Unit__c = 'PR-Safety & Risk Management';
        opp.PRA_Project_ID__c='44441230-333123';
        opp.Phase__c = 'Phase I';
        opp.Therapeutic_Area__c = 'Cardio-Metabolic Diseases';
        opp.Indication_Group__c = 'Arterial disease';
        opp.Study_Start_Date__c = Date.Today().addDays(1);
        opp.Study_End_Date__c = Date.Today().addDays(185);
        opp.CloseDate = Date.Today().addDays(190);
        insert opp;
        
        Test.stopTest();
        
        System.debug('>>> Oppty Created >>> '+opp.Name);
        
        list<WFM_Project__c> projList = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        System.debug('>>> WFM Project Created >>> '+projList[0].Name);
        system.assertEquals(projList[0].Name, opp.PRA_Project_ID__c);
        
        WFM_Project__c prj = projList[0];
        prj.Project_Start_Date__c = Date.Today().addDays(1);
        prj.Project_End_Date__c = Date.Today().addDays(450);
        update prj;
        Bid_Project__c bs = new Bid_Project__c(Name='BS1',PRA_Project_ID__c=prj.Id);        
        insert bs;
        List<Bid_Project__c> srmProjList = SRM_ScenarioDataAccessor.getSRMProjectByWFMProjectName(prj.Name);
  
        //Create SRM Model
        SRM_Model__c model = new SRM_MOdel__c();
        model.Status__c = 'In Progress';
        model.Comments__c = 'Test Comments';
        insert model;
        
        //Create Country
        Country__c country = new Country__c();
        country.Name = 'Test India';
        country.Country_Code__c = 'TI';
        country.Daily_Business_Hrs__c = 8;
        country.Description__c = 'Test India';
        country.PRA_Country_Id__c = 'TI1';
        insert country;
        
        //Create SRM Site Activation
        SRM_Model_Site_Activation__c site = new SRM_Model_Site_Activation__c();
        site.SRM_Model__c = model.Id;
        site.Country__c = country.Id;
        site.Number_of_Weeks__c = 10;
        site.Gaussian_Constant__c = 10;
        site.Amplitude__c = 100;
        site.Shift_Center_By__c = 2;
        site.Standard_Deviation__c = 4;
        site.Start_Week__c = 2;
        site.Site_Activation_Formula__c = 'Gaussian';
        insert site;
        
        //Create Subject Enrollment
        SRM_Model_Subject_Enrollment__c subEnroll = new SRM_Model_Subject_Enrollment__c();
        subEnroll.SRM_Model__c = model.Id;
        subEnroll.Country__c = country.Id;
        subEnroll.Subject_Enrollment_Rate__c = 3;
        insert subEnroll;
        
        //Create Calendar Adjustment records
        SRM_Calender_Adjustments__c cal = new SRM_Calender_Adjustments__c();
        cal.Country__c = country.Id;
        cal.SRM_Model__c = model.Id;
        cal.From_Date__c = Date.newInstance(2015,1,21);
        cal.To_Date__c = Date.newInstance(2015, 1, 22);
        System.debug('>>> sdtd >>> '+cal.From_Date__c  + ' >>> todt >>> '+cal.To_Date__c);
        cal.Site_Act_Adjustment__c = 100;
        cal.Subject_Enroll_Adjustment__c = -100;
        insert cal;
        
        //Create Week Adjustment
        SRM_Week_Adjustments__c week = new SRM_Week_Adjustments__c();
        week.Country__c = country.Id;
        week.SRM_Model__c = model.Id;
        week.Week_Of_Project__c = 15;
        week.Site_Act_Adjustment__c = 100;
        week.Subject_Enroll_Adjustment__c = 100;
        insert week;
        
        //Create question
        SRM_Model_Questions__c que = new SRM_Model_Questions__c();
        que.SRM_Model__c = model.Id;
        que.Question__c = 'This is test question';
        que.Primary_Role__c = 'Proposal Director';
        que.Group__c = 'Project';
        que.Sub_Group__c = 'Site Activation';
        insert que;
        
        SRM_Model_Response__c res = new SRM_Model_Response__c();
        res.SRM_Question_ID__c = que.Id;
        res.Response__c = 'Yes';
        res.Adjust_LPI__c = 5;
        insert res;
        
        model.Status__c = 'Approved';
        update model;
        
        SRM_Scenario_Country__c scencont = new SRM_Scenario_Country__c();
        scencont.Country__c = country.Id;
        scencont.Expected_Sub_Enrollment_Rate__c = 1;
        scencont.SRM_Model_Site_Activation__c = site.Id;
        scencont.Subject_Enrollment_Rate__c = 2;
        scencont.Target_Number_Of_Sites__c = 300;
        scencont.Target_Number_Subjects__c = 2000;
        insert scencont;
        
        SRM_Questions_Response__c scenQue = new SRM_Questions_Response__c();
        scenQue.Country__c = country.Id;
        scenQue.SRM_Question_ID__c = que.Id;
        //scenQue.QuestionResponseUnique__c = scen.Id+':'+que.Id;
        scenQue.SRM_Response_Id__c = res.Id;
        insert scenQue;
        
        List<Bid_Project__c>  prjList = SRM_ScenarioDataAccessor.getSRMProjectByWFMProjectName(opp.PRA_Project_ID__c);
        System.assertNotEquals(null, prjList);
        
        String projectId = prjList[0].Id;
                   
        Bid_Project__c prj1 = SRM_ScenarioDataAccessor.getSRMProject('Select Id from Bid_Project__c', projectId);
        Bid_Project__c prj2 = SRM_ScenarioDataAccessor.getSRMProjectbyscenario(prj1.id);
        //String scenarioId = scen.Id;
        /*String query = 'select '+SRM_Utils.getAllFieldsFromObject('SRM_Scenario_Country__c') + ' , Country__r.Name, SRM_Model_Site_Activation__r.IRB_Type__c'+
                     + ' , SRM_Model_Site_Activation__r.Sponsor__c, SRM_Model_Site_Activation__r.Therapeutic__c from SRM_Scenario_Country__c where SRM_Scenario__c =: scenarioId';
        List<SRM_Scenario_Country__c> scenarioCountriesList = SRM_ScenarioDataAccessor.getScenarioCountries(query, scenarioId);
        System.assertNotEquals(null, scenarioCountriesList);*/
        
        
        String fields = SRM_ScenarioDataAccessor.getAllFieldsFromObject('SRM_Model_Site_Activation__c');
        System.assertNotEquals(null, fields);
        
        String activeModelId = model.Id;
        String query1 = ' select '+SRM_Utils.getAllFieldsFromObject('SRM_Model_Site_Activation__c') + ', Country__r.Name from SRM_Model_Site_Activation__c where SRM_Model__c =: activeModelId';
        List<sObject> countryList = SRM_ScenarioDataAccessor.getAllCountries(query1, activeModelId);
        System.assertNotEquals(null, countryList);
        
        List<SRM_Model__c> modelList = SRM_ScenarioDataAccessor.findRecentActiveModel();
        System.assertEquals(1, modelList.size());
        
        String modelId = SRM_ScenarioDataAccessor.findScenarioSelectedModelId(model.Name);
        System.assertEquals(null, modelId);
        
        String modelName = SRM_ScenarioDataAccessor.findScenarioSelectedModelName(model.Id);
        System.assertNotEquals(null, modelName);
        
        List<SRM_Model__c> scenModelsList = SRM_ScenarioDataAccessor.findScenarioModels();
        
        List<SRM_Model_Questions__c> modelQuesList = SRM_ScenarioDataAccessor.getQuestionAndAnswersForModel(model.Id);
        
        List<SRM_Week_Adjustments__c> weekAdjList = SRM_ScenarioDataAccessor.getAllWeekAdjustments(model.Id, country.Id);
        System.assertNotEquals(null, weekAdjList);
        
        /*List<SRM_Questions_Response__c> quesResList = SRM_ScenarioDataAccessor.getexporttbyproject(scenarioId);
        System.assertNotEquals(null, quesResList);
        
        List<SRM_Questions_Response__c> projSponsorList = SRM_ScenarioDataAccessor.getexporttbysponser(scenarioId);
        
        List<SRM_Questions_Response__c> projCountryList = SRM_ScenarioDataAccessor.getexporttbycountry(scenarioId);
        
        List<SRM_Questions_Response__c> projContractList = SRM_ScenarioDataAccessor.getexporttbycontract(scenarioId);
        
        List<SRM_Model_Site_Activation__c> siteActList = SRM_ScenarioDataAccessor.getAllSiteActivations(model.Id);
        
        scen.Status__c = 'Approved';
        upsert scen;*/
        
    }
}