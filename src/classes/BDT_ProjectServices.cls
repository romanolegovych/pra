public with sharing class BDT_ProjectServices {

	public BDT_SecuritiesService.pageSecurityWrapper	securitiesA			 {get;set;}
	public List<Study__c>								Studies				 {get;set;}
	public List<ServiceCategoryWrapper>					StudyCategories		 {get;set;}
	public Integer										getNumberOfStudies() {return Studies.size();}
	public String										projectId;
	// Variables for calculation help
	public String										serviceCatListIndex		 {get;set;}
	public String										serviceListIndex		 {get;set;}
	public String										studyListIndex			 {get;set;}
	public String										selectedStudyId			 {get;set;}
	public Boolean										showCalculatorFlag		 {get;set;}
	public Boolean										showOutcomeFlag			 {get;set;}
	public List<SelectOption>							calcRuleSelectList		 {get;set;}
	public StudyServiceService.CalculationHelper		calculationHelperItem	 {get;set;}
	public List<SelectOption>							startMileStoneSelectList {get;set;}
	public List<SelectOption>							endMileStoneSelectList	 {get;set;}
	public List<SelectOption>							expresInTimeSelectList	 {get;set;}
	// Variables for shuttle
	public BDT_Utils.SelectOptionShuttle 				shuttle 			 	 {get;set;}
	public List<String>									shuttleUsed				 {get;set;}
	public List<String>									shuttleUnused			 {get;set;}
	public List<SelectOption>							shuttleUsedSelectOption  {get;set;}
	public List<SelectOption>							shuttleUnusedSelectOption{get;set;}
	// Variables to read Constants
	public String	AssessmentsForAllFcInStudyRule 				{get;set;}
	public String	PredefinedCategoriesAssessmentsInStudyRule	{get;set;}
	public String	TimePeriodBetweenMilestones					{get;set;}


	public BDT_ProjectServices(){
		init();
		shuttle = ServiceCategoryService.createDesignServiceCategoryShuttle();
		projectId = BDT_Utils.getPreference('SelectedProject');
		Studies = BDT_Utils.readSelectedStudies();
		calcRuleSelectList = StudyServiceService.createSelectOptionsForCalculations('Please select...');
		loadData();
	}
	
	
	/** Initialize the global variables of the controller.
	 * @author	Dimitrios Sgourdos
	 * @version	27-Feb-2014
	 */
	private void init() {
		// General variables
		securitiesA				  = new BDT_SecuritiesService.pageSecurityWrapper('Service Units');
		Studies					  = new List<Study__c>();
		StudyCategories			  = new List<ServiceCategoryWrapper>();
		
		// Variables for calculation help
		calculationHelperItem = new StudyServiceService.CalculationHelper();
		calcRuleSelectList		  = new List<SelectOption>();
		startMileStoneSelectList  = new List<SelectOption>();
		endMileStoneSelectList	  = new List<SelectOption>();
		expresInTimeSelectList	  = new List<SelectOption>();
		
		// Variables for shuttle
		shuttleUsed				  = new List<String>();
		shuttleUnused			  = new List<String>();
		shuttleUsedSelectOption	  = new List<SelectOption>();
		shuttleUnusedSelectOption = new List<SelectOption>();
		
		// Read the constants from data accessor
		TimePeriodBetweenMilestones	   = StudyServiceDataAccessor.TIME_PERIOD_BETWEEN_MILESTONES;
		AssessmentsForAllFcInStudyRule = StudyServiceDataAccessor.ASSESSMENTS_FOR_ALL_FC_IN_STUDY_RULE;
		PredefinedCategoriesAssessmentsInStudyRule = StudyServiceDataAccessor.PREDEFINED_CATEGORIES_ASSESSMENTS_IN_STUDY_RULE;
	}
	

	public void loadData(){

		Set <ID> StudyIDs = new Set<ID>();
		for (Study__c s:Studies){StudyIDs.add(s.id);}

		List<ServiceCategory__c> serviceCategoryList = ServiceCategoryDataAccessor.getServiceAndCategoriesForProject(projectId);

		Map<String,StudyService__c> studyServices = new Map<String,StudyService__c>();

		for (StudyService__c s : [Select Study__c,
										 ProjectService__r.Service__c,
										 NumberOfUnits__c,
										 Name,
										 Id,
										 explanationJSON__c,
										 CalculationHelperJSON__c
									From StudyService__c s
									where Study__c in :StudyIDs] ) {

			studyServices.put(s.Study__c +''+ s.ProjectService__r.Service__c,s);

		}

		StudyCategories = new List<ServiceCategoryWrapper>();
		for (ServiceCategory__c serviceCategory:serviceCategoryList) {

			ServiceCategoryWrapper newStudyCategory = new ServiceCategoryWrapper();
			newStudyCategory.ServiceCategory = serviceCategory;
			newStudyCategory.Services = new List<ServiceWrapper>();

			for (Service__c service:serviceCategory.Services__r) {

				ServiceWrapper newServiceWrapper = new ServiceWrapper();
				newServiceWrapper.Service = service;
				newServiceWrapper.StudyServices = new List<StudyServiceWrapper>();

				for (Study__c study:Studies){
					StudyServiceWrapper ssw = new StudyServiceWrapper();
					if (studyServices.containsKey(study.id+''+service.id)){
						ssw.StudyService = studyServices.get(study.id+''+service.id);
						ssw.DisplayText  = BDT_Utils.formatNumberWithSeparators(ssw.StudyService.NumberOfUnits__c,2);
						//String.valueOf(ssw.StudyService.NumberOfUnits__c);
						// trim off .00
						ssw.DisplayText = ssw.DisplayText.replaceAll('[.]0+$','');
						//ssw.DisplayText = ssw.DisplayText.replaceAll('$','');

					} else {
						ssw.StudyService = new StudyService__c(Study__c = study.id);
						ssw.DisplayText  = '';
					}
					newServiceWrapper.StudyServices.add(ssw);
				}
				newStudyCategory.Services.add(newServiceWrapper);
			}
			StudyCategories.add(newStudyCategory);
		} 

	}
	public void saveStudyServices(){

		list<StudyService__c> studyservicesToInsert	= new list<StudyService__c>();
		list<StudyService__c> studyservicesToUpdate	= new list<StudyService__c>();
		list<StudyService__c> studyservicesToDelete	= new list<StudyService__c>();
		map<id,ProjectService__c> projectServiceMap = new map<id,ProjectService__c>();
		
		// Create set for checking for refresh the deleted study services in the wrapper
		List<StudyServicePricing__c> tmpStSrvPricingList = [select StudyService__c
															from StudyServicePricing__c
															where ApprovedFinancialDocument__c != null];
		Set<String> notRefreshStSrvSet = BDT_Utils.getStringSetFromList(tmpStSrvPricingList, 'StudyService__c');
		
		// process data to save
		for(ServiceCategoryWrapper scw: StudyCategories){

			// check if there was a change
			if (!scw.changed) {
				// continue to next iteration
				continue;
			} else {
				// reset
				scw.changed = false;
			}

			for(ServiceWrapper sw: scw.services){
				// design objects are only displayed but never saved on this screen
				if (!sw.Service.IsDesignObject__c){
					for(StudyServiceWrapper ssw:sw.StudyServices){
						// check if value was changed
						if (ssw.DisplayText!=BDT_Utils.formatNumberWithSeparators(ssw.StudyService.NumberOfUnits__c)
							&& !(ssw.DisplayText=='' && ssw.StudyService.NumberOfUnits__c==null)
							){
							if (ssw.StudyService.ProjectService__c == null){
								/* PROJECT SERVICE UNKNOWN */
								if (!projectServiceMap.containsKey(sw.service.id)) {
									projectServiceMap.put(sw.service.id,null);
								}
							}

							try {
								// set units to NULL if entry was empty or convert user input string to decimal
								ssw.StudyService.NumberOfUnits__c = (ssw.DisplayText=='')?null:Decimal.valueOf(ssw.DisplayText.replace(',',''));

								if (ssw.StudyService.id == null) {
									// set name to service id
									ssw.StudyService.name = sw.service.id;
									// records to insert
									studyservicesToInsert.add(ssw.StudyService);
								} else {
									// records to update
									studyservicesToUpdate.add(ssw.StudyService);
								}

							} catch (Exception e) {
								// conversion failed, user might have entered text, value will not be changed
							}

						}
						if (ssw.DisplayText=='' && ssw.StudyService.id != null){
							studyServicesToDelete.add(ssw.StudyService);
							// Clean record for future use
							if( ! notRefreshStSrvSet.contains(ssw.StudyService.Id) ) {
								ssw.StudyService = new StudyService__c(Study__c = ssw.StudyService.Study__c);
							}
						}
					}
				}
			}
		}

		// are there project services to be resolved?
		if (projectServiceMap.size() > 0) {

			// find the project services
			List<ProjectService__c> psList = [select id, client_project__c, service__c
					from projectservice__c
					where service__c in :projectServiceMap.keySet()
					and client_project__c = :projectId];

			Set<ID> ServicesNotFound = new set<Id>();
			ServicesNotFound.addAll(projectServiceMap.keySet());
			for (ProjectService__c ps:psList) {
				ServicesNotFound.remove(ps.service__c);
				projectServiceMap.put(ps.service__c,ps);
			}

			// if project services where not found then populate these
			if (ServicesNotFound.size()>0){
				Map<ID,ProjectService__c> newProjectServicesList = new Map<ID,ProjectService__c>();
				for (ID serviceID : ServicesNotFound) {
					newProjectServicesList.put(
						serviceID,
						new ProjectService__c(
							client_project__c = projectId,
							service__c = serviceID
						));
				}

				insert newProjectServicesList.values();

				// add new records to map too
				projectServiceMap.putAll(newProjectServicesList);
			}

			// fix insert record with project service
			for (StudyService__c ss : studyservicesToInsert){
				if (ss.projectservice__c != null) continue;
				ss.ProjectService__c = projectServiceMap.get(ID.valueOf(ss.name)).id;
			}

		}

		insert studyservicesToInsert;
		update studyservicesToUpdate;

		// deletion
		if (studyServicesToDelete.size() > 0) {

			// only remove if there is not accepted document referencing this study service.
			delete [select id
					from studyservice__c
					where id in :studyServicesToDelete
					and id not in (select studyservice__c
							from studyservicepricing__c
							where not approvedfinancialdocument__c = null)];

			// clean unnessasary project services
			delete [select id
					from projectservice__c
					where client_project__c = :projectId
					and service__r.isdesignobject__c = false
					and id not in (select projectservice__c
							from studyservice__c)];

		}

	}
	
	
	/** Show the calculation helper section.
	 * @author	Dimitrios Sgourdos
	 * @version 29-Jan-2014
	 */
	public void showCalculatorHelper() {
		// Assign the selected StudyService
		Integer tmpSrvCatIndex = Integer.valueOf(serviceCatListIndex);
		Integer tmpSrvIndex	   = Integer.valueOf(serviceListIndex);
		Integer tmpStudyIndex  = Integer.valueof(studyListIndex);
		
		selectedStudyId = StudyCategories[tmpSrvCatIndex].Services[tmpSrvIndex].StudyServices[tmpStudyIndex].StudyService.Study__c;
		
		// Find the calculation info
		calculationHelperItem = StudyServiceService.deserializeFromStringToCalculationHelper(
				StudyCategories[tmpSrvCatIndex].Services[tmpSrvIndex].StudyServices[tmpStudyIndex].StudyService.CalculationHelperJSON__c
		);
		
		showOutcomeFlag = false;
		if(calculationHelperItem.rule == TimePeriodBetweenMilestones) {
			readMileStonesSelectLists();
		} else if(calculationHelperItem.rule == AssessmentsForAllFcInStudyRule) {
			calculationHelperItem.outcome = StudyServiceService.calculateAssessmentForAllFlowchartsInStudy(selectedStudyId);
			showOutcomeFlag = true;
		} else if(calculationHelperItem.rule == PredefinedCategoriesAssessmentsInStudyRule) {
			showServiceCategoriesShuttle();
		}
		
		showCalculatorFlag = true;
	}
	
	
	/** Hide the calculation helper section.
	 * @author	Dimitrios Sgourdos
	 * @version 31-Jan-2014
	 */
	public void hideCalculatorHelper() {
		calculationHelperItem = new StudyServiceService.CalculationHelper();
		resetShuttleData();
		showOutcomeFlag	   = false;
		showCalculatorFlag = false;
	}
	
	
	/** Reset the data that are assigned with the suttle.
	 * @author	Dimitrios Sgourdos
	 * @version 31-Jan-2014
	 */
	private void resetShuttleData() {
		shuttleUsed   = new List<String>();
		shuttleUnused = new List<String>();
		shuttleUsedSelectOption	  = new List<SelectOption>();
		shuttleUnusedSelectOption = new List<SelectOption>();
		shuttle.usedOptions = new List<SelectOption>();
	}
	
	
	/** Update Study Service with the selected calculations and close the calculation helper section.
	 * @author	Dimitrios Sgourdos
	 * @version 28-Jan-2014
	 */
	public void includeOutcome() {
		// Find selected StudyServiceWrapper
		Integer tmpSrvCatIndex = Integer.valueOf(serviceCatListIndex);
		Integer tmpSrvIndex	   = Integer.valueOf(serviceListIndex);
		Integer tmpStudyIndex  = Integer.valueof(studyListIndex);
		
		StudyCategories[tmpSrvCatIndex].Services[tmpSrvIndex].StudyServices[tmpStudyIndex].DisplayText =
																			calculationHelperItem.getOutcomeValue();
		
		calculationHelperItem.cleanUnusedData();
		StudyCategories[tmpSrvCatIndex].Services[tmpSrvIndex].StudyServices[tmpStudyIndex].StudyService.CalculationHelperJSON__c =
																			calculationHelperItem.serializeToString();
		StudyCategories[tmpSrvCatIndex].changed = true;
		
		hideCalculatorHelper();
	}
	
	
	/** Update the calculation helper section when the calculation type changes.
	 * @author	Dimitrios Sgourdos
	 * @version 30-Jan-2014
	 */
	public void changeCalculationSelection() {
		showOutcomeFlag = false;
		if(calculationHelperItem.rule == TimePeriodBetweenMilestones) {
			readMileStonesSelectLists();
		} else if(calculationHelperItem.rule == AssessmentsForAllFcInStudyRule) {
			calculationHelperItem.outcome = StudyServiceService.calculateAssessmentForAllFlowchartsInStudy(selectedStudyId);
			showOutcomeFlag = true;
		} else if(calculationHelperItem.rule == PredefinedCategoriesAssessmentsInStudyRule) {
			showServiceCategoriesShuttle();
		} else {
			calculationHelperItem.outcome = NULL;
		}
	}
	
	
	/** Read the milestones selection lists depends on the selected study.
	 * @author	Dimitrios Sgourdos
	 * @version 25-Feb-2014
	 */
	public void readMileStonesSelectLists() {
		List<StudyMileStone__c> source = BDT_StudyMileStoneService.getStudyMileStonesByStudy(selectedStudyId);
		startMileStoneSelectList = BDT_Utils.getSelectOptionFromSObjectList(source,'Id','Name','Please select...',false);
		endMileStoneSelectList	 = BDT_Utils.getSelectOptionFromSObjectList(source,'Id','Name','Please select...',false);
		expresInTimeSelectList	 = StudyServiceService.createOptionsForExpressTypeOfMilestoneCalculation('Please select...');
	}
	
	
	/** Show the Service Categories shuttle.
	 * @author	Dimitrios Sgourdos
	 * @version 31-Jan-2014
	 */
	public void showServiceCategoriesShuttle() {
		resetShuttleData();
		shuttleUsed = new List<String>();
		shuttleUnused = calculationHelperItem.getSelectedCategoriesIds();
		buttonMoveToUsed();
		shuttleUsedSelectOption	  = shuttle.usedOptions;
		shuttleUnusedSelectOption = shuttle.getUnusedOptions();
		shuttleUnusedSelectOption = ServiceCategoryService.sortServiceCategorySelectList(shuttleUnusedSelectOption);
	}
	
	
	/** Move the selected unused Service Categories to the used ones.
	 * @author	Dimitrios Sgourdos
	 * @version 30-Jan-2014
	 */
	public void buttonMoveToUsed () {
		shuttle.moveToUsed(shuttleUnused);
		shuttleUnused = new List<String>();
		shuttleUsedSelectOption	  = shuttle.usedOptions;
		shuttleUnusedSelectOption = shuttle.getUnusedOptions();
		shuttleUnusedSelectOption = ServiceCategoryService.sortServiceCategorySelectList(shuttleUnusedSelectOption);
	}
	
	
	/** Move the selected used Service Categories to the unused ones.
	 * @author	Dimitrios Sgourdos
	 * @version 30-Jan-2014
	 */
	public void buttonMoveToUnused () {
		shuttle.moveToUnused(shuttleUsed);
		shuttleUsed = new List<String>();
		shuttleUsedSelectOption	  = shuttle.usedOptions;
		shuttleUnusedSelectOption = shuttle.getUnusedOptions();
		shuttleUnusedSelectOption = ServiceCategoryService.sortServiceCategorySelectList(shuttleUnusedSelectOption);
	}
	
	
	/** Implement the calculation for the selected calculation rule.
	 * @author	Dimitrios Sgourdos
	 * @version 27-Feb-2014
	 */
	public void calculateUnits() {
		if(calculationHelperItem.rule == TimePeriodBetweenMilestones) {
			calculationHelperItem.outcome = StudyServiceService.calculateTimePeriodBetweenMileStones(
																			calculationHelperItem.startMileStone,
																			calculationHelperItem.endMileStone,
																			calculationHelperItem.timeExpressIn);
		} else if(calculationHelperItem.rule == PredefinedCategoriesAssessmentsInStudyRule) {
			calculationHelperItem.categoriesIds = BDT_Utils.convertListToString(shuttle.getUsedKeys());
			calculationHelperItem.outcome = StudyServiceService.calculateAssessmentForPredefCategoriesInStudy(
																							selectedStudyId,
																							shuttle.getUsedKeys() );
		}
		showOutcomeFlag = true;
	}
	
	
	
	public class ServiceCategoryWrapper {
		public ServiceCategory__c ServiceCategory {get;set;}
		public String getServiceCategoryCode() {return BDT_Utils.removeLeadingZerosFromCat(ServiceCategory.Code__c);}
		public Integer getPadding() {return (ServiceCategory.Code__c.length()-3)*4;}
		public List<ServiceWrapper> Services {get;set;}

		public ServiceCategoryWrapper(){
			changed = false;
		}
		public Boolean changed {get;set;}

	}
	public class ServiceWrapper {
		public Service__c Service {get;set;}

		/* this list must be sorted to the order of "List<Study__c> Studies" */
		public List<StudyServiceWrapper> StudyServices {get;set;}
	}
	public class StudyServiceWrapper {
		public String 			DisplayText {get;set;}
		public StudyService__c 	StudyService {get;set;}
	}

	public PageReference copyServices(){
		PageReference copyServicesPage = new PageReference(system.page.BDT_ProjectServiceCopy.getUrl() );
		copyServicesPage.getParameters().put('ProjectId',projectId);
		return copyServicesPage;

	}

}