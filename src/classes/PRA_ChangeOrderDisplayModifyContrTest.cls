@isTest (seeAllData=false)
private class  PRA_ChangeOrderDisplayModifyContrTest {
	
    static testMethod void PRA_ChangeOrderDisplayModifyContrTest() {
       test.startTest(); 
       
       PRA_TestUtils tu = new PRA_TestUtils();
       tu.initAll();
       Change_Order__c coObj=tu.createChangeOrder(tu.clientProject.Id);
        
       PageReference oPage = new PageReference('/apex/PRA_Change_Order_Display_Modify?id='+coObj.Id);
       Test.setCurrentPage(oPage);
     
       ApexPages.StandardController stController = new ApexPages.StandardController(coObj);
       PRA_ChangeOrderDisplayModifyController coController=new PRA_ChangeOrderDisplayModifyController(stController);
       coController.selectedProjectid=tu.clientProject.Id;
       Change_Order_Item__c coItemObj=new Change_Order_Item__c();
       coItemObj.Change_Order__c=coObj.Id;
       //coItemObj.Amendment_Unit_Cost__c=123;
       coItemObj.Client_Unit_Number__c='123';
       coItemObj.End_Date__c=Date.today().addMonths(3).toStartOfMonth();
       insert coItemObj;
       
       BUF_Code__c bc1 = new BUF_Code__c();
       insert bc1;
        
       Change_Order_Line_Item__c coLineItemObj=new Change_Order_Line_Item__c(Change_Order_Item__c=coItemObj.id,BUF_Code__c=bc1.id);
       insert coLineItemObj;
       
       
       List<Change_Order_Item__c> li=new List<Change_Order_Item__c>();
       li.add(coItemObj);

       PRA_ChangeOrderDisplayModifyController coControllerObj=new PRA_ChangeOrderDisplayModifyController(); 
       coController.selectedRecord=coObj.Id;
       coController.viewRecord();
       coController.ExecuteCO();
       coController.ExportExcel();
       coController.CloneCO();
       coController.addNewClientUnit();
       coController.editChangeOrderItem();
       coController.RefreshChangeOrder();
       PRA_ChangeOrderDisplayModifyController.updateChangeOrders(li);
       PRA_ChangeOrderDisplayModifyController.deleteChangeOrders(li);
       PRA_ChangeOrderDisplayModifyController.ComboCodeValid(coObj.Id);
       coController.selectedRecord=null;
       coController.viewRecord();
       coController.viewEndDate();
       coController.CloneCO();
       coController.DeleteCO();
       coController.selectedProjectid=null;
       coController.DeleteCO();
       
       test.stopTest();         
    }
}