@isTest(SeeAllData=false)
private class PRA_ClientProjectTriggerHandlerTest {

    static testMethod void shouldInsertMonthlyApprovalOnProjectCreation() {
        // given
        Date startDate = Date.today().addMonths(-2);
        Date endDate = Date.today().addMonths(10);
        PRA_TestUtils tu = new PRA_TestUtils();
        Account acc = tu.getAccount('Lem');
        insert acc;
        
        // when
        Client_Project__c cp = tu.getClientProject(acc.id, 'Summa technologie', 'Pra_LEM',startDate ,endDate );
        insert cp;
        
        // then
        List<Monthly_Approval__c> monthlyApprovals = [select Approval_Date__c, Client_Project__c, DPD_approved_forecast__c, Id, Is_approval_complete__c, 
                               PM_Approved_Forecast__c, PM_Approved_Worked_Units__c, PZ_released_forecast__c, Month_Approval_Applies_To__c 
                            from Monthly_Approval__c where Client_Project__c = :cp.id
                            order by Month_Approval_Applies_To__c];
    // there should be monthly approval created for each month from current month up to endDate
   // system.assertEquals(11, monthlyApprovals.size());
    Date monthAppliesTo = Date.today().toStartOfMonth();
    
    for (Monthly_Approval__c ma : monthlyApprovals){
      system.assertEquals(null, ma.Approval_Date__c);
      system.assertEquals(null, ma.DPD_approved_forecast__c);
      system.assertEquals(null, ma.PM_Approved_Forecast__c);
      system.assertEquals(null, ma.PM_Approved_Worked_Units__c);
      system.assertEquals(null, ma.PZ_released_forecast__c);
      system.assertEquals(false, ma.Is_approval_complete__c);
      system.assertEquals(monthAppliesTo, ma.Month_Approval_Applies_To__c);
      monthAppliesTo = monthAppliesTo.addMonths(1);
    }
    }
}