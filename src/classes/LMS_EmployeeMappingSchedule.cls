global class LMS_EmployeeMappingSchedule implements Schedulable {
	
	global void execute(SchedulableContext cxt) {
		String className = 'LMS_EmployeeMappingBatch';
		String query = 'select Job_Class_Desc__r.Name, Job_Title__r.Job_Code__c, Business_Unit__r.Name, Department__r.Name,'
			+ 'Region__r.Region_Name__c, Country__r.Name, Employee_Type__r.Name from LMS_Role__c where Role_Type__r.Name = \'PRA\' '
			+ 'and Status__c != \'Inactive\'';
		String jsonParams = Json.serialize(new list<String>{query});
		Integer scope = 10;
		PRA_Batch_Queue__c mappingBatch =
        new PRA_Batch_Queue__c(Batch_Class_Name__c = className, Parameters_JSON__c = jsonParams, 
            Status__c = 'Waiting', Priority__c = 5, Scope__c = scope);
        
        insert mappingBatch;
	}
}