@isTest
private class AA_RevenueAllocationDataAccessorTest{   
    
    /* Test getting all buf codes */
    static testMethod void getBuCodesTest() {
    
         initTestData ();
         
         Test.startTest();
         
         List<Business_Unit__c> result = AA_RevenueAllocationDataAccessor.getBuCodes();
         
         System.assertNotEquals(result.size(),0);
         
         Test.stopTest();
    }
    
    /* Test getting all FC codes */
    static testMethod void getFCodesTest() {
    
         initTestData ();
         
         Test.startTest();
         
         List<Function_Code__c> result = AA_RevenueAllocationDataAccessor.getFCodes();
         
         System.assertNotEquals(result.size(),0);
         
         Test.stopTest();
    }
    
    
    /* Test getting all Aggregate Year Result */
    static testMethod void getValidYearsTest() {
    
         initTestData ();
         
         Test.startTest();
         
         List<AggregateResult> result = AA_RevenueAllocationDataAccessor.getValidYears();
         
         System.assertNotEquals(result.size(),0);
         
         Test.stopTest();
    }
    
    /* Test getting all Countries */
    static testMethod void getCountriesTest() {
    
         initTestData ();
         
         Test.startTest();
         
         List<Country__c> result = AA_RevenueAllocationDataAccessor.getCountries();
         
         System.assertNotEquals(result.size(),0);
         
         Test.stopTest();
    }
    
    /* Test getting bufcode details by name */
    static testMethod void getBufCodeDetailsTest() {
    
         initTestData ();
         
         Test.startTest();
         
         BUF_Code__c result = AA_RevenueAllocationDataAccessor.getBufCodeDetails('BU1F1');
         
         System.assertEquals(result.Name,'BU1F1');
         
         Test.stopTest();
    }
 
     /* Test getting bufcode details by name */
    static testMethod void getBufCodeIdTest() {
    
         initTestData ();
         
         Test.startTest();
         
         BUF_Code__c result = AA_RevenueAllocationDataAccessor.getBufCodeId('BU1F1');
         
         System.assertEquals(result.Id,AA_RevenueAllocationDataAccessor.getBufCodeId('BU1F1').Id);
         
         Test.stopTest();
    }
    
     /* Test getting Currency Id by name */
    static testMethod void getCurrencyIdTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Currency__c result = AA_RevenueAllocationDataAccessor.getCurrencyId('USD');
         
         System.assertEquals(result.Id,AA_RevenueAllocationDataAccessor.getCurrencyId('USD').Id);
         
         Test.stopTest();
    }
    
     /* Test check existing details */
    static testMethod void checkExistingRecord() {
    
         initTestData ();
         
         Test.startTest();
         
         
         Revenue_Allocation_Rate__c result = AA_RevenueAllocationDataAccessor.checkExistingRecord(AA_RevenueAllocationDataAccessor.getBufCodeId('BU1F1'),'2011');
         
         System.assertNotEquals(result,null);
         
         Test.stopTest();
    }
       
   /* Test getting bufcode by name */
    static testMethod void getBufCodesTest() {
    
         initTestData ();
         
         Test.startTest();
         
         List<BUF_Code__c> result = AA_RevenueAllocationDataAccessor.getBufCodeName('BUF1');
         
         System.assertEquals(result.size(),0);
         
         Test.stopTest();
    }
    
    /* Test getting rar id*/
    static testMethod void getRARIdTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Revenue_Allocation_Rate__c result = AA_RevenueAllocationDataAccessor.getRARId();
         
         System.assertNotEquals(result,null);
         
         Test.stopTest();
    }
    
    /* Test inserting bufcode */
    static testMethod void insertBufCodeTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
         
         AA_RevenueAllocationDataAccessor.insertBufCode(BufCode1);
         
         System.assertNotEquals([SELECT Id FROM BUF_Code__c WHERE NAME='BUF1'],null);
         
         Test.stopTest();
    }
    
    /* Test inserting bufcode */
    static testMethod void getRateDetailsTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);

         
         System.assertNotEquals(AA_RevenueAllocationDataAccessor.getRateDetails(BufCode1.Id),null);
         
         Test.stopTest();
    }
    
    
    /* Test existing rates */
    static testMethod void checkExistingTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);

         
         System.assertNotEquals(AA_RevenueAllocationDataAccessor.checkExisting(BufCode1.Id,'2011'),null);
         
         Test.stopTest();
    }
    
    /* Test query runner */
    static testMethod void runQueryTest() {
    
         initTestData ();
         
         Test.startTest();

         List<Revenue_Allocation_Rate__c> result = AA_RevenueAllocationDataAccessor.runQuery('SELECT Id FROM Revenue_Allocation_Rate__c');

         System.assertNotEquals(result.size(),0);

         Test.stopTest();
    }
    
    /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }   
       
 }