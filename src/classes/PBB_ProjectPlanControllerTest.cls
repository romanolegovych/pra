/**
@author Bhargav Devaram
@date 2014
@description this test class is to display the Project Plan to a scenario results
**/
@isTest
private class PBB_ProjectPlanControllerTest {
    
    static testMethod void testConstructor() {
        
   /** 
    * This is just for code coverage
    */
    test.startTest();
    PBB_DataAccessor da = new PBB_DataAccessor();
    test.stopTest();
   }
   
    static testMethod void testPBB_ProjectPlanController() {
        
   /** 
    * Init Test Utils
    */
    PBB_TestUtils tu = new PBB_TestUtils();
    test.startTest();
   
    PBB_ProjectPlanController ppp = new PBB_ProjectPlanController();
   /** 
    * Testing if condition
    */
    WFM_Project__c pcw = new WFM_Project__c();
    pcw = tu.createwfmproject();
    Bid_Project__c bpc = new Bid_Project__c();
    bpc = tu.createbidproject();
    system.assertEquals(bpc.Name,'BS1');
    Service_Model__c ms = new Service_Model__c ();
    ms = tu.createServiceModelattributes();
    ms= tu.approveServicemodel(ms);
    WR_Model__c wm = new WR_Model__c ();
    wm = tu.createWRModelattributes();
    PBB_Scenario__c ssc = new PBB_Scenario__c();
    SRM_Model__c sm = tu.createSRMModelattributes();    
    //SRM_Scenario__c  srmScen= tu.createSRMScenario();
    ssc = tu.createscenarioattributes();
    ssc = [select id,Name from PBB_Scenario__c];
    Apexpages.currentPage().getParameters().put('PBBScenarioID', ssc.Id);
    PBB_ProjectPlanController ppc = new PBB_ProjectPlanController();
    system.assert(ssc.id != null, 'Record inserted');
    test.stopTest();
    }
   
}