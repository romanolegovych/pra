public class PRA_ChangeOrderValidBufCodeController{

    public set<String> BusinessUnit;    
    public set<String> FunctionCode;    
    public String SelectedBusinessUnit{get;set;}
    public String SelectedFunctionCode{get;set;}
    
    
    public boolean isFiltered = false;
    public set<String> ValidBufCodeSet;
    public set<String> selectedBufCodeSet;
    public set<String> selectedbufcodes {get;set;}
    public set<String> Existingbufcodes {get;set;}    
    
    private Change_Order_Item__c changeOrderItem;
    public ID COIid {get;set;}
    public  String query{ get; set; }
    public  String unit{ get; set; }
    public String moveBUFCode {get;set;}
    
    //Available BUFCode picklist selected values
    String[] selectedAvailableBUFCode= new String[]{};
    public String[] getselectedAvailableBUFCode(){
        return selectedAvailableBUFCode;
    }    
    public void setselectedAvailableBUFCode(String[] selectedAvailableBUFCode){
        this.selectedAvailableBUFCode= selectedAvailableBUFCode;
    } 
    
    
    //Buf code piclist selected values
    String[] selectedBUFCode= new String[]{};
    public String[] getselectedBUFCode(){
        return selectedBUFCode;
    }    
    public void setselectedBUFCode(String[] selectedBUFCode){
        this.selectedBUFCode= selectedBUFCode;
    } 
    
    //Constructor
    public PRA_ChangeOrderValidBufCodeController() {
      
        ValidBufCodeSet=new set<String>();
        selectedBufCodeSet=new set<String>();        
        
        BusinessUnit=new set<String>();        
        FunctionCode=new set<String>();
        
        selectedbufcodes=new set<String>();
        Existingbufcodes=new set<String>();
        COIid = ApexPages.currentPage().getParameters().get('id'); 
        moveBUFCode = ApexPages.currentPage().getParameters().get('Move');        
        
        if(COIid!=null){
        
            changeOrderItem = [select Id, Name, Change_Order__r.Client_Project__r.Name, Client_Unit_Number__c, Combo_Code__c,
                                Is_Labor_Unit__c, Change_Order__c, Client_Task__c, Type__c,Project_Region__c,Task_group__c,
                                Change_Order__r.Client_Project__r.Standard_rate_year__c,Change_Order__r.Client_Project__r.Estimated_End_Date__c,
                                Change_Order__r.Client_Project__r.Project_Currency__c,Change_Order__r.Client_Project__r.id,
                                Change_Order__r.Client_Project__r.Currency_exchange_lock_date__c
                                from Change_Order_Item__c where id = :COIid];
           
            
            //All COLI NEW bufCodes             
            for(Change_Order_Line_Item__c  a:[select Id, Name,Is_Labor_Unit__c, Discount_Price__c, Discount_Unit_Price__c, Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, 
                                                Amendment_CO_Hours_unit__c, BUF_Code__c, Change_Order_Item__c, Current_Contract_Value__c, Current_Contract_Hours__c, 
                                                Current_Hours_Unit__c, Current_Total_Cost__c, Current_Unit_Cost__c, Revised_Hours_Unit__c, Revised_Total_Cost__c, 
                                                Revised_Total_Hours__c, Revised_Unit_Cost__c, Type__c, BUF_Code__r.Name, Change_Order_Item__r.Revised_Number_of_Units__c,
                                                Change_Order_Item__r.Is_Labor_Unit__c,Change_Order_Item__r.change_order__r.client_project__r.name
                                                from Change_Order_Line_Item__c where Change_Order_Item__c = :COIid  order by buf_code__r.name ASC]){
                 
                 if(moveBUFCode==null){
                     if(a.Type__c=='New'){
                         selectedBufCodeSet.add(String.valueof(a.BUF_Code__r.name)) ;                             
                         selectedbufcodes.add(String.valueof(a.BUF_Code__r.name)) ;                 
                     }else{
                         Existingbufcodes.add(String.valueof(a.BUF_Code__r.name)) ; 
                     }
                 }else{
                      if(a.Is_Labor_Unit__c==false){
                         selectedBufCodeSet.add(String.valueof(a.BUF_Code__r.name)) ;                             
                         selectedbufcodes.add(String.valueof(a.BUF_Code__r.name)) ;                 
                     }else{
                         Existingbufcodes.add(String.valueof(a.BUF_Code__r.name)) ; 
                     }
                 } 
            }
                      
            if(moveBUFCode==null){
                String Query='select id,name,BUF_Code_Name__c,BUF_Code__r.name,BUF_Code__c from Valid_Project_Buf_Code__c where';
                String AdditionalFilter;
                //String CELDate;
                //datetime CELDT=(changeOrderItem.Change_Order__r.Client_Project__r.Currency_exchange_lock_date__c);
                //CELDT=CELDT.addHours(CELDT.hour());
                //CELDate=CELDT.format('yyyyMMdd');
               
                //system.debug('--------------'+CELDate);
                //AdditionalFilter=' Convert_To_Code__c =\''+ changeOrderItem.Change_Order__r.Client_Project__r.Project_Currency__c+'\'';
                //AdditionalFilter+='and Currency_End_Date_Key__c>=\''+changeOrderItem.Change_Order__r.Client_Project__r.Estimated_End_Date__c+'\''; 
                //AdditionalFilter+='and Currency_Date_Key__c=\''+CELDate+'\''; 
                AdditionalFilter=' Standard_Rate_Year__c='+Decimal.valueof((changeOrderItem.Change_Order__r.Client_Project__r.Standard_rate_year__c).year())+''; 
                AdditionalFilter+='and Cost_Rate_End_Year__c>='+Decimal.valueof((changeOrderItem.Change_Order__r.Client_Project__r.Estimated_End_Date__c).year())+''; 
                AdditionalFilter+='and BUF_Code_Name__c NOT IN:selectedbufcodes and BUF_Code_Name__c NOT IN:Existingbufcodes order by BUF_Code__r.name asc '; 
                query+=AdditionalFilter; 
                
                system.debug('------- query---------------------'+query);
                //All Valid BufCodes excluding the existing New once                
                for(Valid_Project_Buf_Code__c vbc:Database.query(query)){
                    ValidBufCodeSet.add(String.valueof(vbc.BUF_Code_Name__c)) ;
                }
            
            }else{
               ValidBufCodeSet.addall(Existingbufcodes);
            } 
        }     
         
       //business Unit selectlist
        for (BUF_Code__c a: [SELECT Name, Business_Unit__r.Name FROM BUF_Code__c WHERE Name  IN :ValidBufCodeSet order by Business_Unit__r.Name asc]){                              
             BusinessUnit.add(String.valueof(a.Business_Unit__r.Name));
        }
        //Function Code selectlist
        for (BUF_Code__c a: [SELECT Name, Function_Code__r.Name FROM BUF_Code__c WHERE Name  IN :ValidBufCodeSet order by Function_Code__r.Name asc]){                             
             FunctionCode.add(String.valueof(a.Function_Code__r.Name)) ;
        }                     
    }
    
     // select BusinessUnitOptions
    public List<SelectOption> getBusinessUnitOptions() { 
        List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('',''));
            for(String ro: BusinessUnit) {
            if((ro)!='' && ro!=null) { 
                options.add(new SelectOption(ro,ro));
            }
        }
           options.sort();
            return options;
    }
    // select FunctionCodeOptions
    public List<SelectOption> getFunctionCodeOptions() { 
        List<SelectOption> options = new List<SelectOption>();
            options.add(new SelectOption('',''));
            for(String fo: FunctionCode) {
            if((fo)!='' && fo!=null) {                
                options.add(new SelectOption(fo,fo));
            }
        }
           options.sort();
          return options;
    }
   
    //Available BUFCODE List
    public List<selectoption> getAvailableBUFCodeList(){
        List<selectoption> options = new list<selectoption>();
        options.add(new SelectOption('',''));
        for(String oa: ValidBufCodeSet) {
            if((oa)!='' && oa!=null) {     
                if(isFiltered == true) {  
                    if(SelectedFunctionCode == null && oa.contains(SelectedBusinessUnit)){
                        String BU = oa.Substring(0,3);
                        system.debug('------- bu---------------------'+BU);
                        if(BU== SelectedBusinessUnit)   
                        options.add(new SelectOption(oa,oa));
                    }
                    
                    else if(SelectedBusinessUnit == null && oa.contains(SelectedFunctionCode)){
                        String FC = oa.Substring(oa.length()-2,oa.length());
                        system.debug('------- FC---------------------'+FC);
                        if(FC== SelectedFunctionCode)      
                            options.add(new SelectOption(oa,oa));
                    }
                    else if(SelectedFunctionCode != null && SelectedBusinessUnit != null){
                        
                        String bufcode=SelectedBusinessUnit;
                        bufcode+=SelectedFunctionCode;
                        system.debug('------- BUFCode---------------------'+SelectedBusinessUnit+SelectedFunctionCode+bufcode);
                        if(oa==bufcode)
                        options.add(new SelectOption(oa,oa));
                    }
                }
                else{        
                    options.add(new SelectOption(oa,oa));
                }
            }
        }
        options.sort();
        return options;
    }  
   
    // Selected BUFCode List
    public List<SelectOption> getselectedBUFCodeList() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',''));
        for(String oa: selectedBufCodeSet) {
            if((oa)!='' && oa!=null) {                
                options.add(new SelectOption(oa,oa));
            }
        }
        options.sort();
        return options;
    }
    
    //Filter method
    public void doFilter() {
        if(SelectedBusinessUnit != null || SelectedFunctionCode != null){        
            isFiltered = true;
        }        
        else{
            isFiltered = false;
        }
        getAvailableBUFCodeList();
    }
    
     
    //Add button method
    public void doAdd() {
        system.debug('----------'+selectedAvailableBUFCode+selectedBUFCode);
        moveCodes(selectedAvailableBUFCode, selectedBufCodeSet, ValidBufCodeSet);                
    }
    
    //Remove button method 
    public void doRemove() {
    
        moveCodes(selectedBUFCode, ValidBufCodeSet, selectedBufCodeSet);
    }
    
    //Move bufcodes 
    private void moveCodes(String[] items,set<String> moveTo, set<String> removeFrom) {
        for (String s: items) {            
                moveTo.add(s);
                removeFrom.remove(s);
                system.debug('selectedCodes');           
        }
        getselectedBUFCodeList();
        getAvailableBUFCodeList();                             
    }
    
    //Apply method
    public void  Apply() { 
                   
         try {               
            Set<String> DeletedBufcodes=new set<string>();
            Set<String> InsertBufcodes=new set<string>();
            //if(selectedbufcodes.size()>0 && selectedBufCodeSet.size()>0){
            if(ApexPages.currentPage().getParameters().get('unit')!=null){
               boolean bIsLabor;               
               if(ApexPages.currentPage().getParameters().get('unit')!=null){
                 if(ApexPages.currentPage().getParameters().get('unit')=='Labor'){
                     bIsLabor=true;
                     unit='Labor';
                 }    
                 else    
                     if(ApexPages.currentPage().getParameters().get('unit')=='NonLabor'){
                         bIsLabor=false;
                         unit='NonLabor';
                     }    
               }               
               system.debug('------------ApexPages.currentPage().getParameters().-------------->'+ApexPages.currentPage().getParameters().get('unit')+bIsLabor+unit);
               //Delete
               DeletedBufcodes=selectedbufcodes.clone();
               DeletedBufcodes.removeall(selectedBufCodeSet);
               List<Change_Order_Line_Item__c> BUFCodeListDel = [select id,BUF_Code__r.name from  Change_Order_Line_Item__c where 
                                                                   BUF_Code__r.name in: DeletedBufcodes and Is_Labor_Unit__c=:bIsLabor and Change_Order_Item__c = :COIid and Type__c='New'];
               system.debug('------------BUFCodeListDel--------------->'+BUFCodeListDel);
               
               //Insert
               InsertBufcodes=selectedBufCodeSet.clone();
               InsertBufcodes.removeall(selectedbufcodes);
               system.debug('------------DeletedBufcodes-+--InsertBufcodes------------>'+DeletedBufcodes+InsertBufcodes);
               List<Change_Order_Line_Item__c> BUFCodeListInsert = new List<Change_Order_Line_Item__c>();
               for(BUF_code__c bf:[select id,name from BUF_code__c where name in:InsertBufcodes]){
                 if(bf.id!=null ){
                   Change_Order_Line_Item__c coli = new Change_Order_Line_Item__c(Change_Order_Item__c = changeOrderItem.Id, Amendment_BUF_Code_Bill_Rate__c = 0, Amendment_CO_Hours__c = 0,
                                                    Discount_Unit_Price__c = 0, Discount_Price__c = 0, Amendment_CO_Hours_unit__c = 0,  Current_Contract_Value__c = 0,
                                                    Current_Contract_Hours__c = 0,Current_Hours_Unit__c = 0, Current_Total_Cost__c = 0, Current_Unit_Cost__c = 0,
                                                     Type__c = 'New',Is_Labor_Unit__c=bIsLabor,Buf_code__c=bf.id);
                 
                 system.debug('------------coli-+bf-------------->'+coli+bf);
                 BUFCodeListInsert.add(coli);
                 }    
               }                                              
               system.debug('------------BUFCodeListInsert--------------->'+BUFCodeListInsert);               
               if(BUFCodeListDel.size()>0){
                   delete BUFCodeListDel;                   
               }               
               if(BUFCodeListInsert.size()>0){
                   Insert BUFCodeListInsert;                   
               }     
               system.debug('------------unit--------------->'+unit);
               
            }
       } catch (Exception ex) {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
       }
    }
    
     //Apply method
    public void  Move() {  
        try {
            Set<String> LaborAddedBufcodes=new set<string>();
            Set<String> NonLaborAddedBufcodes=new set<string>();
            if(ApexPages.currentPage().getParameters().get('move')!=null && COIid!=null ){
              system.debug('-----------ValidBufCodeSet--------------->'+ValidBufCodeSet);
              system.debug('-----------selectedbufcodeset--------------->'+selectedbufcodeset);
              system.debug('-----------Existingbufcodes--------------->'+Existingbufcodes);
              system.debug('-----------selectedBufCodes--------------->'+selectedBufCodes);
              
              //Delete
               NonLaborAddedBufcodes=Existingbufcodes.clone();
               NonLaborAddedBufcodes.removeall(ValidBufCodeSet);
               system.debug('-----------NonLaborAddedBufcodes-------------->'+NonLaborAddedBufcodes);
               
               LaborAddedBufcodes=selectedBufCodes.clone();
               LaborAddedBufcodes.removeall(selectedbufcodeset);
               system.debug('-----------LaborAddedBufcodes-------------->'+LaborAddedBufcodes);
               map<string,Bid_Contract_Detail__c> mapbc=new map<string,Bid_Contract_Detail__c> ();
               for(Bid_Contract_Detail__c bidDetails : [select Id, Client_Task__c, Client_Task__r.Additional_Units_With_No_Contract_Value__c, 
                                                         Client_Task__r.Contract_Value__c, Client_Task__r.Name, 
                                                        Client_Task__r.Contracted_Number_of_Units__c, Client_Task__r.Status__c, Client_Task__r.Average_Contract_Effort__c,
                                                        Client_Task__r.Total_Contract_Hours__c, Client_Task__r.Total_Contract_Units__c, BUF_Code__c, 
                                                        BUF_Code__r.Name, BUF_Code__r.PRA_BUF_Code_Id__c, BUF_Code__r.PRA_Region__c, Contracted_Hours__c, Contracted_Value__c
                                                        from Bid_Contract_Detail__c
                                                        where Client_Task__c =:changeOrderItem.Client_Task__c and (BUF_Code__r.Name in: LaborAddedBufcodes or BUF_Code__r.Name in: NonLaborAddedBufcodes )]){
                            
                     system.debug('-----------bidDetails--------->'+bidDetails);
                     if(bidDetails.buf_code__c!=null){
                         mapbc.put(bidDetails.buf_code__r.name,bidDetails);
                     
                     }
                                                        
        
               }
               system.debug('----------mapbc--------->'+mapbc);
               
                List<Change_Order_Line_Item__c> LaborBUFCodeList =new List<Change_Order_Line_Item__c>();
                for(Change_Order_Line_Item__c coli: [select Id, Name,Is_Labor_Unit__c, Discount_Price__c, Discount_Unit_Price__c, Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, 
                                                        Amendment_CO_Hours_unit__c, BUF_Code__c, Change_Order_Item__c, Current_Contract_Value__c, Current_Contract_Hours__c, 
                                                        Current_Hours_Unit__c, Current_Total_Cost__c, Current_Unit_Cost__c, Revised_Hours_Unit__c, Revised_Total_Cost__c, 
                                                        Revised_Total_Hours__c, Revised_Unit_Cost__c, Type__c, BUF_Code__r.Name, Change_Order_Item__r.Revised_Number_of_Units__c,
                                                        Change_Order_Item__r.Is_Labor_Unit__c,Change_Order_Item__r.change_order__r.client_project__r.name
                                                        from Change_Order_Line_Item__c where 
                                                       BUF_Code__r.name in: LaborAddedBufcodes and Change_Order_Item__c = :COIid ]){
                    
                    system.debug('-----------coli----1----------->'+coli);
                    
                    if(mapbc.containsKey(coli.BUF_Code__r.name)) {                    
                       
                       Bid_Contract_Detail__c bidDetails=mapbc.get(coli.BUF_Code__r.name);
                       coli.Current_Contract_Hours__c=bidDetails.Contracted_Hours__c;
                       if(bidDetails.Contracted_Hours__c != 0 && bidDetails.Client_Task__r.Total_Contract_Units__c != 0){
                            coli.Current_Hours_Unit__c    = bidDetails.Contracted_Hours__c / bidDetails.Client_Task__r.Total_Contract_Units__c;
                        } else {
                            coli.Current_Hours_Unit__c = 0;
                        }                     
                    }
                    
                    coli.Discount_Price__c=0;
                    coli.Discount_Unit_Price__c=0;                    
                    coli.Amendment_BUF_Code_Bill_Rate__c=0;
                    coli.Amendment_CO_Hours__c=0;
                    coli.Amendment_CO_Hours_unit__c=0;
                    coli.Is_Labor_Unit__c=true;
                    LaborBUFCodeList.add(coli);                                               
                                                                   
                }
                
                List<Change_Order_Line_Item__c> NonLaborBUFCodeList =new List<Change_Order_Line_Item__c>();
                for(Change_Order_Line_Item__c coli: [select Id, Name,Is_Labor_Unit__c, Discount_Price__c, Discount_Unit_Price__c, Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, 
                                                        Amendment_CO_Hours_unit__c, BUF_Code__c, Change_Order_Item__c, Current_Contract_Value__c, Current_Contract_Hours__c, 
                                                        Current_Hours_Unit__c, Current_Total_Cost__c, Current_Unit_Cost__c, Revised_Hours_Unit__c, Revised_Total_Cost__c, 
                                                        Revised_Total_Hours__c, Revised_Unit_Cost__c, Type__c, BUF_Code__r.Name, Change_Order_Item__r.Revised_Number_of_Units__c,
                                                        Change_Order_Item__r.Is_Labor_Unit__c,Change_Order_Item__r.change_order__r.client_project__r.name
                                                        from Change_Order_Line_Item__c where 
                                                                   BUF_Code__r.name in: NonLaborAddedBufcodes and Change_Order_Item__c = :COIid ]){
                    system.debug('-----------coli----2----------->'+coli);
                     //current
                    coli.Current_Contract_Hours__c=0;
                    coli.Current_Hours_Unit__c=0; 
                    
                   
                    coli.Discount_Price__c=0;
                    coli.Discount_Unit_Price__c=0;                    
                    coli.Amendment_BUF_Code_Bill_Rate__c=0;
                    coli.Amendment_CO_Hours__c=0;
                    coli.Amendment_CO_Hours_unit__c=0;
                    coli.Is_Labor_Unit__c=false;
                    NonLaborBUFCodeList.add(coli);                                               
                                                                   
                }
                
                if(LaborBUFCodeList.size()>0)
                    update LaborBUFCodeList;
                if(NonLaborBUFCodeList.size()>0)
                    update NonLaborBUFCodeList;    
                    

            
            }
        }catch (Exception ex) {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
       }
    
    }
}