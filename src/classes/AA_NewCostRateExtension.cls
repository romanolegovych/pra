public class AA_NewCostRateExtension{

    // Instance fields
    public String searchTerm {get; set;}
    private final List<BUF_Code__c> bufcodes;
    private final List<Currency__c> currencies;
    public String selectedBufCode{get;set;}
    public String selectedCurrency{get;set;}
    public String yearValue{get;set;}
    public Decimal initialRateValue{get;set;}
    private Cost_Rate__c rate;
    private Boolean error = false;
     

    public AA_NewCostRateExtension(ApexPages.StandardController controller) {
        bufcodes = AA_CostRateService.getBufCodes();
        currencies = AA_CostRateService.getCurrencies();
    }
    
    // JS Remoting action called when searching for a BUF Code name
    @RemoteAction
    public static List<BUF_Code__c > searchCode(String searchTerm) {
        List<BUF_Code__c > codes = AA_CostRateService.getBufCodesByName(searchTerm);
        return codes;
    }
    
    /* Populate BUF Codes in Select List */
    public List<SelectOption> getbufselectoption() {
        List<SelectOption> bufselectoption= new List<SelectOption>();
        for(BUF_Code__c entry : bufcodes){
            bufselectoption.add(new SelectOption(entry.Id, entry.Name));
        }
        return bufselectoption;
    }
    
    /* Populate Currency in Select List     */
    public List<SelectOption> getcurrencyselectoption() {
        List<SelectOption> currencyselectoption= new List<SelectOption>();
        for(Currency__c entry : currencies){
            currencyselectoption.add(new SelectOption(entry.Id, entry.Name));
        }
        return currencyselectoption;
    }

    public PageReference Create() {
            /* Catch invalid Year */
            try{
            if(Integer.valueOf(yearValue) < 1999 || Integer.valueOf(yearValue) > Integer.valueOf(Date.Today().Year() + 20)) {
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Year : '+ yearValue + '. Must be between 1999 and ' + String.valueOf(Date.Today().Year() + 20));
                ApexPages.addMessage(errormsg);
                return null;
            }
            }catch(Exception e){
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Year : '+ yearValue);
                ApexPages.addMessage(errormsg);
                return null;
            }
            
            /* Catch invalid Rate*/
            try{
                Integer d = Integer.valueOf(initialRateValue);
            }catch(Exception e){
                error = true;
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Rate : '+ initialRateValue);
                ApexPages.addMessage(errormsg);
            }
            
            if(!error){
                List<Cost_Rate__c> check = AA_CostRateService.checkExisting(selectedBufCode,yearValue);   
                //ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,selectedBufCode + ' | ' + yearValue));
                if(check.size()>0)
                {
                    BUF_Code__c tempCode = AA_CostRateService.getBufCode(selectedBufCode);
                    ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Cost Rates for ' + tempCode.Name + ' BUF Code for ' +  yearValue + ' exists in the system. Please choose a different BUF Code or a different year.'));
                
                }else {  
                     createNewRecords();
                    
                     PageReference pageRef = Page.AA_Inflation_Override;
                     pageRef.getParameters().put('functionId',selectedBufCode);
                     pageRef.getParameters().put('scheduledYear',yearValue);
                     
                     return pageRef ;
                 }
                 return null;
         }else
             return null;
    }

   public PageReference back() {
         PageReference pageRef = Page.AA_Cost_Rate;
         return pageRef;
    }
    public void createNewRecords(){
         rate =  new Cost_Rate__c ();
         rate.BUF_Code__c = selectedBufCode;
         rate.Cost_Rate__c = initialRateValue;
         rate.Schedule_Year__c = yearValue;
         rate.Year_Rate_Applies_To__c = yearValue;
         rate.Currency_lookup__c = selectedCurrency;
         rate.IsActive__c = true;
         AA_CostRateService.generateCostRates(rate);
         ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,' Cost Rates Generated Succesfully'));
    }
    
}