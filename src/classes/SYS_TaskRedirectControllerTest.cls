/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class SYS_TaskRedirectControllerTest {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        try
        {
            Account objAccount = new Account();
            objAccount.Name= 'Test Account';
            objAccount.Type= 'Other';
            insert objAccount;
            Task objTask = new Task();
            objTask.Type = 'Voice Mail';        
            objTask.WhatId=objAccount.id;
            objTask.subject='Call';
            objTask.ActivityDate=system.today();
            insert objTask;
            
            ApexPages.StandardController con = new ApexPages.StandardController(objTask);    
            ApexPages.CurrentPage().GetParameters().put('Id',objTask.id);
            ApexPages.CurrentPage().GetParameters().put('what_id',objAccount.id);
            SYS_TaskRedirectController objTaskRedirect=new SYS_TaskRedirectController(con);
            objTaskRedirect.currentPage ='SYS_TaskRedirectViewPage';
            objTaskRedirect.taskRedirect();
            objTaskRedirect.currentPage ='SYS_TaskRedirectEditPage';
            objTaskRedirect.taskRedirect();
            objTaskRedirect.currentPage ='SYS_TaskRedirectClosePage';
            objTaskRedirect.taskRedirect();
        }catch(Exception ex){
            
        }
    }
    static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        try
        {
            Account objAccount = new Account();
            objAccount.Name= 'Test Account';
            objAccount.Type= 'Other';
            insert objAccount;
            Task objTask = new Task();
            objTask.Type = 'Voice Mail';        
            objTask.WhatId=objAccount.id;
            objTask.subject='Call';
            objTask.ActivityDate=system.today();
            insert objTask;
            
            ApexPages.StandardController con = new ApexPages.StandardController(objTask);    
            ApexPages.CurrentPage().GetParameters().put('what_id',objAccount.id);
            SYS_TaskRedirectController objTaskRedirect=new SYS_TaskRedirectController(con);
            objTaskRedirect.taskRedirect();
        }catch(Exception ex){
            
        }
    }
}