@isTest
private class BDT_NewEditSponsorEntityTest {
	
	public static List<Sponsor__c> mySponsorList {get;set;}
	
	static void init(){
		mySponsorList = BDT_TestDataUtils.buildSponsor(1);
		insert mySponsorList;
	}
	

    static testMethod void createSponsorEntityPath() { 
    	init();
		System.currentPageReference().getParameters().put('sponsorId', mySponsorList[0].id);		
		BDT_NewEditSponsorEntity newSponsorEntity = new BDT_NewEditSponsorEntity();
		
		newSponsorEntity.sponsorEntity.Name = 'my new sponsor entity';
		newSponsorEntity.sponsorEntity.BillingCity = 'Kansas';
		newSponsorEntity.sponsorEntity.Sponsor__c = mySponsorList[0].id;
		
		System.assertNotEquals(null, newSponsorEntity.save());
		
		System.currentPageReference().getParameters().put('sponsorEntityId', newSponsorEntity.sponsorEntity.id);
		BDT_NewEditSponsorEntity editSponsorEntity = new BDT_NewEditSponsorEntity();
		
		System.assertNotEquals(null, editSponsorEntity.cancel());
		
	}
	
	static testMethod void failEditSponsorEntityPath() { 
		BDT_NewEditSponsorEntity failEditSponsorEntity = new BDT_NewEditSponsorEntity();
		
		System.assertEquals(null, failEditSponsorEntity.sponsorEntity);
		
	}
	
	static testMethod void deleteSponsorEntityPath() { 
    	init();
		
		System.currentPageReference().getParameters().put('sponsorId', mySponsorList[0].id);	
		
		BDT_NewEditSponsorEntity newSponsorEntity = new BDT_NewEditSponsorEntity();
		
		newSponsorEntity.sponsorEntity.Name = 'my new sponsor entity';
		newSponsorEntity.sponsorEntity.BillingCity = 'Kansas';
		newSponsorEntity.sponsorEntity.Sponsor__c = mySponsorList[0].id;
		
		System.assertNotEquals(null, newSponsorEntity.save());
		
		System.currentPageReference().getParameters().put('sponsorEntityId', newSponsorEntity.sponsorEntity.id);
		BDT_NewEditSponsorEntity editSponsorEntity = new BDT_NewEditSponsorEntity();
		
		System.assertNotEquals(null, editSponsorEntity.deleteSoft());
		
	}
}