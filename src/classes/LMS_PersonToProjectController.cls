global class LMS_PersonToProjectController {

    // Instance Variables
    public String commitDate {get;set;}
    public String clientText {get;set;}
    public String projectText {get;set;}
    public String projectFilter {get;set;}
    public String roleResultId {get;set;}
    public String searchFilter {get;set;}
    public String projectRole {get;set;}
    public String employeeName {get;set;}
    public String jobFamily {get;set;}
    public String acPersonFilter {get;private set;}
    public String roleNameDisplay {get;set;}
    public String roleName {get;set;}
    public String roleNameStyle {get;private set;}
    public String roleFilter {get;private set;}
    public String roleErrorText {get;private set;}
    public String personErrorText {get;private set;}
    public String webServiceError {get;private set;}
    private String statusFilter {get;set;}
    private String wlblFilter {get;set;}
    public Boolean renderRoleCreate {get;set;}
    public Boolean allChecked {get;set;}
    public Boolean allCheckedPerson {get;set;}
    public Boolean bWebServiceError {get;private set;}

    // Collections
    public List<LMS_PersonAssignment> employees {get;set;}
    public List<LMS_PersonList> selEmps {get;set;}
    public List<LMS_Role__c> roles {get;set;}
    private Map<String,String> roleResult {get;set;}
    private Map<String,String> roleCommitted {get;set;}
    private Map<String,String> roleDraft {get;set;}
    private Set<String> excludeStatus {get;set;}
    private Set<String> excludeEmps {get;set;}
    private Map<String, String> jobTitleMap {get;set;}
    private Map<String, LMS_Role_Employee__c> mappings {get;private set;}
    private Map<String, LMSConstantSettings__c> constants {get;private set;}

    // Error Varaibles
    public List<String> sfdcErrors {get;private set;}
    public List<String> calloutErrors {get;private set;}

    /**
     *  Constructor
     *  Initialize Data
     */
    public LMS_PersonToProjectController() {
        initStatus();
        initSettings();
        init();
        initJobMap();
    }

    /**
     *  Excludes all terminated employees
     */
    private void initStatus() {
        List<Employee_Status__c> retiredStatus = [select Employee_Status__c from Employee_Status__c where Employee_Type__c = 'Terminated'];
        excludeStatus = new Set<String>();
        for(Employee_Status__c s : retiredStatus) {
            excludeStatus.add(s.Employee_Status__c);
        }
    }

    /**
     *  Initialize all role and person data
     */
    private void init() {
        bWebServiceError = false;
        roles = null;
        employees = null;
        selEmps = null;
        clientText = '';
        projectText = '';
        employeeName = '';
        jobFamily = '';
        projectFilter = ' and Client_Id__c != \'\' and Client_Id__c != null and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')';
        statusFilter = ' and Status__c not in (\'T1\',\'T2\',\'T3\',\'T4\',\'XX\')';
        wlblFilter = ' ' + LMS_ToolsFilter.getWhitelistEmployeeFilter() + ' ' + LMS_ToolsFilter.getBlackListEmployeeFilter();
        acPersonFilter = statusFilter + wlblFilter;
        roleName = 'Enter Role Name';
        roleNameStyle = 'WaterMarkedTextBox';
        roleFilter = ' and Role_Type__r.Name = \'Project Specific\' and Project_Id__r.Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\') and Status__c = \'Active\'';
    }

    /**
     *  Get constants from custom settings
     */
    private void initSettings() {
        constants = LMSConstantSettings__c.getAll();
    }

    /**
     *  Initialize job map to be used in person list
     */
    private void initJobMap()   {
        jobTitleMap = new Map<String, String>();
        List<Job_Title__c> titles = [SELECT Job_Code__c, Job_Title__c FROM Job_Title__c ORDER BY Job_Title__c asc];
        if(null != titles) {
            for(Job_Title__c jt : titles) {
                jobTitleMap.put(jt.Job_Code__c, jt.Job_Title__c);
            }
        }
    }

    /**
     *  Initialize person data
     */
    private void personInit() {
        employeeName = '';
        jobFamily = '';
        selEmps = null;
    }

    /**
     *  Reset entire page
     */
    public PageReference resetPage() {
        init();
        personInit();
        resetAC();
        return null;
    }

    /**
     *  Reset person search section
     */
    public PageReference personReset() {
        personInit();
        return null;
    }

    /**
     *  Close person search list
     */
    public PageReference closePersonSearch() {
        selEmps = null;
        return null;
    }

    /**
     *  Resets AutoComplete component
     */
    private void resetAC() {
        clientText = '';
        projectText = '';
        projectFilter = '';
        searchFilter = ' and Role_Type__r.Name = \'Project Specific\' and Project_Id__r.Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\') and Status__c = \'Active\'';
        projectRole = '';
        projectFilter = ' and Client_Id__c != \'\' and Client_Id__c != null and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')';
    }

    /**
     *  Returns values for project role drop-down
     */
    public List<SelectOption> getProjectRoles() {
        List<SelectOption> projRole = new List<SelectOption>();
        projRole = LMS_LookUpDataAccess.getAllPSTRoles(true);
        return projRole;
    }

    /**
     *  Returns values for job family drop-down
     */
    public List<SelectOption> getJobFamilyList() {
        List<selectoption> family = new List<selectoption>();
        family = LMS_LookUpDataAccess.getRoles(false, false);
        List<selectoption> options = new List<selectoption>();
            options.add(new selectoption('',''));
        for(integer i=0; i<family.size(); i++)
            options.add(family[i]);
        return options;
    }

    @RemoteAction
    global static Boolean isValidClientId(String clientId) {
        Boolean isValid;
        try {
            WFM_Client__c client = [SELECT Name FROM WFM_Client__c WHERE Name = :clientId];
            if(client != null)
                    isValid = true;
        } catch(Exception e) {
            System.debug(e.getMessage());
            isValid = false;
        }
        return isValid;
    }

    @RemoteAction
    global static Boolean isValidProjectId(String projectId) {
        Boolean isValid;
        try {
            WFM_Project__c project = [SELECT Name FROM WFM_Project__c WHERE Name = :projectId];
            if(project != null) {
                    isValid = true;
            }
        } catch(Exception e) {
            System.debug(e.getMessage());
            isValid =  false;
        }
        return isValid;
    }

    @RemoteAction
    global static Boolean isValidClientProjectPair(String clientId, String projectId) {
        Boolean isValid;
        try {
            WFM_Project__c project = [SELECT Client_Id__c FROM WFM_Project__c WHERE Name = :projectId];
            if(project.Client_ID__c == clientId) {
                isValid = true;
            } else {
                isValid = false;
            }

        } catch(Exception e) {
            System.debug(e.getMessage());
            isValid = false;
        }
        return isValid;
    }

    /**
     *  Changes project filter / sets value of client text
     */
    public void projectSearchType() {
        if(clientText != '' && projectText == '') {
            projectFilter = ' and Client_Id__c != \'\' and Client_Id__c != null and Client_Id__c = \'' + 
            	clientText + '\'';
        } else if((clientText != '' && projectText != '') || (clientText == '' && projectText != '')) {
            projectFilter = ' and Client_Id__c != \'\' and Client_Id__c != null and Client_Id__c = \'' + 
            	clientText + '\'';
            List<WFM_Project__c> clientName = [select name,Client_Id__c from WFM_Project__c where Name =:projectText];
            if(clientName.size() > 0)
            clientText = string.valueof(clientName[0].Client_Id__c);
            System.debug('-------------------client text--------------------' + clientText);
        }
    }

    public PageReference cancelCreateRole() {
    	roles = null;
    	return null;
    }

    /**
     *  Project role search logic
     */
    public PageReference search() {
    	bWebServiceError = false;
        //Set local and instace variables
    	Boolean acSearch;
        roleDraft = new Map<String,String>();
        roleResult = new Map<String,String>();
        roleCommitted = new Map<String,String>();
        excludeEmps = new Set<String>();
        mappings = new Map<String, LMS_Role_Employee__c>();

        String joinQry = '';
        String roleQry = '';
        String employeeIdFilter = '';
        String draft = constants.get('statusDraft').Value__c;
        String draftDelete = constants.get('statusDraftDelete').Value__c;
        String committed = constants.get('statusCommitted').Value__c;
        String deleted = constants.get('statusToDelete').Value__c;

        //Join query to get course information on a certain role
        joinQry += 'SELECT Employee_Id__r.Date_Hired__c,Employee_Id__r.Name,Employee_Id__r.Id,Employee_Id__r.First_Name__c,' + 
            'Employee_Id__r.Last_Name__c,Employee_Id__r.Job_Class_Desc__c,Employee_Id__r.Job_Code__c,Employee_Id__r.Business_Unit_Desc__c,' + 
            'Employee_Id__r.Department__c,Employee_Id__r.Country_Name__r.Region_Name__c,Employee_Id__r.Country_Name__r.Name,' + 
            'Employee_Id__r.Status__c,Employee_Id__r.Status_Desc__c,Role_Id__r.SABA_Role_PK__c,Commit_Date__c,Status__c,Previous_Status__c,Sync__c' + 
            ' FROM RoleEmployees__r WHERE Status__c != :deleted order by Status__c,Sync__c DESC,Employee_Id__r.Last_Name__c,Employee_Id__r.First_Name__c';
        //Main query to pull the role based on criteria in UI
        roleQry += 'SELECT Role_Name__c,SABA_Role_PK__c,Client_Id__r.Name,Project_Id__r.Name,Project_Role__r.Name,Project_Id__r.Status_Desc__c,Status__c,Sync_Status__c,' + 
        	'('+joinQry+') FROM LMS_Role__c WHERE Role_Name__c != null and Role_Type__r.Name = \'Project Specific\'';

        if(null != roleName && roleName != '' && roleName != 'Enter Role Name'){
        	roleQry += ' and Role_Name__c = :roleName';
        	acSearch = true;
        } else {
        	acSearch = false;
	        List<WFM_Project__c> project = new List<WFM_Project__c>();
	        string id = '';
	        if(projectText != '') {
	            project = [select Client_Id__c from WFM_Project__c where Name =:projectText];
	            WFM_Client__c clientObj = [select Id from WFM_Client__c where Name = :project[0].Client_Id__c];
	            id = clientObj.Id;
	        }
	        //Check for each type of criteria
	        if((clientText == '' && projectText != '') || (clientText != '' && projectText != '')) {
	            roleQry+= ' and Project_Id__r.Name=:projectText and Client_Id__r.Name=:clientText';
	        } else if(clientText != '' && projectText == '') {
	            roleQry += ' and Project_Id__c = null and Client_Id__r.Name=:clientText';
	        }
	        roleQry += ' and (Job_Class_Desc__r.Name=:projectRole OR PST_Adhoc_Role__r.PST_Adhoc_Role__c = :projectRole)';
        }
        allChecked = false;
        //Execute DB query to get search results
        roles = Database.query(roleQry);

        employees = null;
        //perform different operation depening on search results
        if(roles.size() == 0) {
        	if(acSearch) {
	            renderRoleCreate = false;
	            roleCommitted = null;
	            roleErrorText = 'Role not found, please enter a different role name';
        	} else {
	            renderRoleCreate = true;
	            roleCommitted = null;
	            roleErrorText = 'This role doesn\'t exist, you can either search again, or click the link below';
        	}
        } else if(roles.size() == 1) {
        	employees = new list<LMS_PersonAssignment>();
            for(LMS_Role__c r : roles) {
                roleNameDisplay = r.Role_Name__c;
                roleResultId = r.Id;
                for(LMS_Role_Employee__c re : r.RoleEmployees__r) {
                	mappings.put(re.Id, re);
                    roleResult.put(re.Employee_Id__r.Name,re.Employee_Id__c);
                    if(re.Status__c == committed || re.Status__c == draftDelete)
                        roleCommitted.put(re.Employee_Id__r.Name,re.Employee_Id__c);
                    else if(re.Status__c == draft)
                        roleDraft.put(re.Employee_Id__r.Name,re.Employee_Id__c);
                    employees.add(new LMS_PersonAssignment(re, jobTitleMap));
                }
            }   
            if(roles[0].SABA_Role_PK__c != '') {
                renderRoleCreate = false;
                roleErrorText = 'Role is out of Sync with SABA, people can be added but not committed until sync is resolved';
            }  
            if(roles[0].Status__c == 'Inactive' || roles[0].Project_Id__r.Status_Desc__c == 'Bid' || 
            	roles[0].Project_Id__r.Status_Desc__c == 'Lost' || roles[0].Project_Id__r.Status_Desc__c == 'Closed') {
                renderRoleCreate = false;
                if(roles[0].Status__c == 'Inactive') {
            		roleErrorText = 'Role is inactive, enter a different role name';
            	} else if(roles[0].Project_Id__r.Status_Desc__c == 'Bid') {
                	roleErrorText = 'The project linked to this role is currently in bid status, please search for a different role';
                }  else if(roles[0].Project_Id__r.Status_Desc__c == 'Lost') {
                	roleErrorText = 'The bid project associated to this role was lost, please search for a different role';
                } else if(roles[0].Project_Id__r.Status_Desc__c == 'Closed') {
                	roleErrorText = 'The project associate to this role has been closed, please search for a different role';
                }
            }   
        }

        String exclude = '';
        String excludeString = '';
        if(null != roleCommitted && !roleCommitted.isEmpty()) {
            Set<String> excludeIds = roleCommitted.keySet();
            List<Employee_Details__c> excludeEmployees = [select Name from Employee_Details__c where Name IN :excludeIds];
            for(Integer i = 0; i < excludeEmployees.size(); i++) {
                excludeEmps.add(excludeEmployees[i].Name);
                if(i < (excludeEmployees.size() - 1)) {
                    excludeString += '\'' + excludeEmployees[i].Name + '\',';
                } else {
                    excludeString += '\'' + excludeEmployees[i].Name + '\'';
                }
            }
        } else {
            excludeString = '\'\'';
        }
        exclude += '(' + excludeString + ')';
        employeeIdFilter = ' and Name not in ' + exclude;
        acPersonFilter = statusFilter + wlblFilter + employeeIdFilter;

        return null;
    }

    /**
     *  Person search logic
     */
    public PageReference personSearch() {

        //Build query for person search
        Date currentDate = Date.today();
        String empQry = '';
        String firstName = '';
        String lastName = '';
        String filter = '';
        //Add extra criteria based on user input
        if(employeeName != '' && employeeName.contains(',')) {
            String[] names = employeeName.split(',');
            if(names.size() > 1) {
                lastName = names[0];
                firstName = names[1].trim();
                filter += ' and First_Name__c = :firstName and Last_Name__c = :lastName';
            } else {
                lastName = names[0];
                filter += ' and Last_Name__c = :lastName';
            }
        } else if(employeeName != '') { 
            filter += ' and Last_Name__c = :employeeName';
        }
        if(null != jobFamily && '' != jobFamily) {
            filter += ' and Job_Class_Desc__c = :jobFamily';
        }
        filter += wlblFilter;
        empQry += 'select Name,First_Name__c,Last_Name__c,Job_Class_Desc__c,Job_Code__c,Business_Unit_Desc__c,Department__c,' + 
			'Country_Name__r.Region_Name__c,Country_Name__r.Name,Status_Desc__c,Term_Date__c from Employee_Details__c' + 
			' where Term_Date__c != :currentDate AND Status__c not in:excludeStatus and Name not in :excludeEmps' + filter + 
			' order by Last_Name__c asc';
        allCheckedPerson = false;
        //Query DB to get search results    
        List<Employee_Details__c> empList = Database.query(empQry);

        selEmps = new List<LMS_PersonList>();
        //Based on results determine errors messages or lists to be displayed
        if(empList.size() == 0) {
            personErrorText = 'The result contains no people. Please refine your search criteria and run the search again.';
        } else if(empList.size() > 1000) {
            personErrorText = 'The result contains too many records. Please refine your search criteria and run the search again.';
        } else if(empList.size() > 0) {
            for(Employee_Details__c e : empList) {
                selEmps.add(new LMS_PersonList(e, roleResult, jobTitleMap));
            }
        }
        return null;
    }

    /**
     *  Add person to role with an assignment
     *  status as draft
     */
    public PageReference addPerson() {
    	sfdcErrors = new List<String>();
    	Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.addEmployees(selEmps, employees, mappings, roleResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while adding people to role.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
		
		boolean didError = bWebServiceError;
		
        search();
        bWebServiceError = didError;
        personSearch();
        return null;
    }

    /**
     *  Remove person from role
     *  Sets assignment status to Draft Delete
     */
    public PageReference removePersons() {
    	sfdcErrors = new List<String>();
    	Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.removeEmployees(employees, mappings);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while removing or reverting people from role.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
		
		boolean didError = bWebServiceError;
		
        search();
        bWebServiceError = didError;
        return null;
    }

    /**
     *  Cancel all assignment changes
     */
    public PageReference cancel() {
    	sfdcErrors = new List<String>();
    	Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.cancelDraftEmployees(roleResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }

        search();
        personSearch();
        return null;
    }

    public PageReference setCommitDate() {

    	sfdcErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        Date dateCommit = Date.parse(commitDate);
        errors = LMS_ToolsModifyAssignments.setCommitDateEmployees(employees, mappings, dateCommit);

        sfdcErrors = errors.get('SFDCEX');
        System.debug('-------------errors------------'+errors);
        System.debug('-------------sfdcErrors-----------'+sfdcErrors);

        if(null != sfdcErrors && sfdcErrors.size() > 0) {
            webServiceError = 'Errors occurred while applying commit date.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
		
		boolean didError = bWebServiceError;
		
        search();
        bWebServiceError = didError;   
        viewImpact();
        return null;
    }

    /**
     *  Commit assignment changes, send data to SABA
     */
    public PageReference commitPerson() {
    	sfdcErrors = new List<String>();
        calloutErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();
        errors = LMS_ToolsModifyAssignments.commitEmployees(employees, mappings);

        sfdcErrors = errors.get('SFDCEX');
        calloutErrors = errors.get('CALLOUT');

        System.debug('-------------sfdcErrors------------'+sfdcErrors);
        System.debug('-------------calloutErrors------------'+calloutErrors);

        if((null != sfdcErrors && 0 < sfdcErrors.size()) || (null != calloutErrors && 0 < calloutErrors.size())) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        return null;
    }

    /**
     *  Shows the impact of adding persons to a role
     */
    public PageReference viewImpact() {
        return null;
    }

    public PageReference showErrors() {
        PageReference pr = new PageReference('/apex/LMS_PersonProjectError');
        pr.setRedirect(false);
        return pr;
    }
}