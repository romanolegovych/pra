public with sharing class PAWS_EW_RemainingTimeEditController
{
	public class PAWS_EW_RemainingTimeEditControllerException extends Exception { }

	public Integer EW_Days { get { return (Integer)Stub.STSWR1__Number__c; } set { Stub.STSWR1__Number__c = value; } }
	public Date EW_Date { get { return Stub.STSWR1__Date__c; } set { Stub.STSWR1__Date__c = value; } }

	public String stepId { get; set; }
	public STSWR1__Stub__c Stub { get; set; }
	public String Comment { get; set; }
	public String HerokuRequest { get; set; }
	public Boolean IsCriticalChainAssigned { get; set; }

	public void init()
    {
    	stepId = extractStepId();
    	Stub = new STSWR1__Stub__c();
		IsCriticalChainAssigned = true;

    	List<Critical_Chain_Step_Junction__c> chainStepList = [select Remaining_Time__c, Comment__c from Critical_Chain_Step_Junction__c where Flow_Step__c = :stepId limit 1];
    	if(chainStepList.size() == 0)
    	{
    		ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'This step is not a part of Early Warning. No remaining time data entry required.'));
    		IsCriticalChainAssigned = false;
    	}
    	else
    	{
    		EW_Days = Integer.valueOf(chainStepList.get(0).Remaining_Time__c);
    		Comment = chainStepList.get(0).Comment__c;
    	}
    }

    public void save()
    {
    	try
    	{
    		if(EW_Days == 0 && EW_Date == null)
    		{
    			throw new PAWS_EW_RemainingTimeEditControllerException('Please fill the form.');
    		}
    		else if(EW_Days != 0 && EW_Date != null) {
    			throw new PAWS_EW_RemainingTimeEditControllerException('You should enter either Days or Date value, but not both. Please leave the only one value.');
    		}

	    	List<Critical_Chain_Step_Junction__c> steps = [select Remaining_Time__c from Critical_Chain_Step_Junction__c where Flow_Step__c = :stepId];
	    	for(Critical_Chain_Step_Junction__c eachStep : steps)
	    	{
	    		eachStep.Remaining_Time__c = EW_Days = EW_Date == null ? EW_Days : new EWDatetime(Datetime.now()).workdaysBetween(EW_Date);
	    		eachStep.Last_Known_Remaining_Time__c = eachStep.Remaining_Time__c;
	    		eachStep.Remaining_Time_Change_Date__c = System.today();
	    		eachStep.Comment__c = Comment;
	    	}
	    	update steps;
	    }
	    catch(Exception ex)
	    {
	    	ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
	    }
    }

    private String extractStepId()
 	{
 		if(ApexPages.currentPage().getParameters().containsKey('step'))
		{
			StepId = ApexPages.currentPage().getParameters().get('step');
			return StepId;
		}
    			
		String data = HerokuRequest = ApexPages.currentPage().getParameters().get('signed_request');
		if(data == null) return null;
	    		
		String[] items = data.split('\\.');
		if(items.size() == 0) return null;
		
		String context = HerokuRequest = EncodingUtil.base64Decode(items.size() > 1 ? items[1] : items[0]).toString();
		Map<String, Object> contextMap = (Map<String, Object>)JSON.deserializeUntyped(context);
		if(contextMap == null) return null;
		
		contextMap = (Map<String, Object>)contextMap.get('context');
		if(contextMap == null) return null;
		
		contextMap = (Map<String, Object>)contextMap.get('environment');
		if(contextMap == null) return null;
		
		contextMap = (Map<String, Object>)contextMap.get('parameters');
		if(contextMap == null) return null;
		
		return (String)contextMap.get('step');
 	}   

}