/** Implements the Selector Layer of the object LaboratoryAnalysisSlots__c
 * @author	Dimitrios Sgourdos
 * @version	11-Nov-2013
 */
public with sharing class LaboratoryAnalysisSlotsDataAccessor {
	
	/** Object definition for fields used in application for LaboratoryAnalysisSlots
	 * @author	Dimitrios Sgourdos
	 * @version 06-Nov-2013
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('LaboratoryAnalysisSlots__c');
	}
	
	
	/** Object definition for fields used in application for LaboratoryAnalysisSlots with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 07-Nov-2013
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'ActiveNumberOfSamples__c,';
		result += referenceName + 'BackupNumberOfSamples__c,';
		result += referenceName + 'DaysMdOrVal__c,';
		result += referenceName + 'LaboratoryMethod__c,';
		result += referenceName + 'ListOfShipments__c,';
		result += referenceName + 'MdValSlot__c,';
		result += referenceName + 'OtherNumberOfSamples__c,';
		result += referenceName + 'PlaceboNumberOfSamples__c,';
		result += referenceName + 'Repeat__c,';
		result += referenceName + 'SequenceNumber__c,';
		result += referenceName + 'SlotNumber__c,';
		result += referenceName + 'SlotType__c,';
		result += referenceName + 'Study__c,';
		result += referenceName + 'TotalNumberOfSamples__c';
		return result;
	}
	
	
	/** Retrieve the Laboratory analysis slots that the MdValSlot__c flag is equal to MdValFlag for the given studies.
	 * @author	Dimitrios Sgourdos
	 * @version 11-Nov-2013
	 * @param	studiesList			The given studies
	 * @return	The Laboratory analysis list.
	 */
	public static List<LaboratoryAnalysisSlots__c> getSlotsByMdValFlagAndStudies(List<Study__c> studiesList, Boolean MdValFlag) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM LaboratoryAnalysisSlots__c ' +
								'WHERE 	MdValSlot__c = {1} AND {2} ' +
								'ORDER BY SlotNumber__c, SequenceNumber__c',
								new List<String> {
									getSObjectFieldString(),
									(MdValFlag)?'TRUE':'FALSE',
									'Study__c IN :studiesList'
								}
							);
		return (List<LaboratoryAnalysisSlots__c>) Database.query(query);
	}
}