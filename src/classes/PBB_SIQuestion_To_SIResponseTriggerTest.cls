/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

@isTest 
private class PBB_SIQuestion_To_SIResponseTriggerTest {
    static testMethod void validate() {
        PBB_TestUtils tu = new PBB_TestUtils();
        Service_Impact_Question_Name__c sb = tu.createSIQNAttributes();
        
        Service_Impact_Questions__c sa = tu.createSIQSAttributes('sumstring');
        sa.status__c = 'In-Process';
        delete sa;  
        
        Service_Impact_Questions__c saa = tu.createSIQSAttributes(''); 
        saa.status__c = 'Approved';
        upsert saa;
        
        try{
            delete saa;
        }
         catch(Exception e) {
                system.assert(e.getMessage().contains('UNABLE TO DELETE QUESTION BECAUSE SERVICE IMPACT QUESTION IS APPROVED/RETIRED!'));
         }  
         
        Service_Impact_Questions__c saab = tu.createSIQSAttributes('sumstring'); 
        saab.status__c = 'Retired';
        upsert saab;
        
        try{
            delete saab;
        }
         catch(Exception e) {
                Boolean expectedExceptionThrown =  e.getMessage().contains('UNABLE TO DELETE QUESTION BECAUSE SERVICE IMPACT QUESTION IS APPROVED/RETIRED!') ? true : false;
                System.AssertEquals(expectedExceptionThrown, true);
                //system.assert(e.getMessage().contains('UNABLE TO DELETE QUESTION BECAUSE SERVICE IMPACT QUESTION IS APPROVED/RETIRED!'));
         }
         
         
    }   
}