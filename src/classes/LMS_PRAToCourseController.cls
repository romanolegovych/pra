public class LMS_PRAToCourseController {

    /************************************** 
     *  Code for : PRAToCourseController  *
     *  Project : LMS                     *    
     *  Author : Andrew Allen             * 
     *  Last Updated Date : 11/14/2012    * 
     *************************************/

    // Instance Variables
    public String commitDate{get;set;}
    public String roleName{get;set;}
    public String roleFilter{get;set;}
    public String roleNameStyle{get;set;}
    public String roleErrorText{get;private set;}
    public String courseText{get;set;}
    public String courseTextStyle{get;set;}
    public String courseNameDisplay{get;set;}
    public String courseResultId{get;set;}
    public String businessUnit{get;set;}
    public String department{get;set;}
    public String jobTitle{get;set;}
    public String jobFamily{get;set;}
    public String region{get;set;}
    public String country{get;set;}
    public String empType{get;set;}
    public String courseFilter{get;set;}
    public String webServiceError{get;private set;}
	public Boolean allChecked{get;set;}
	public Boolean allCheckedRole{get;set;}
    public Boolean bWebServiceError{get;private set;}

    // For Impact calculations
    public Integer employeeStart{get;set;}
    public Integer employeeChange{get;set;}
    public Integer employeeEnd{get;set;}
    private Integer employeeNum{get;set;}
    public Decimal impactStart{get;set;}
    public String impactChange{get;set;}
    public Decimal impactEnd{get;set;}
    private Decimal impactNum{get;set;}   

    // Collections
    public list<LMS_RoleAssignment> roles{get;set;}
    public list<LMS_RoleList> selRoles{get;set;}
    public list<LMS_EmployeeList> empChange{get;set;}
    public list<LMS_EmployeeList> empTotal{get;set;}
    private list<AggregateResult> durationList{get;set;}
    public list<LMS_Course__c> courses{get;set;}
    private map<String, LMS_Role_Course__c> courseNoDraft{get;set;}
    private map<String, LMS_Role_Course__c> courseCommitted{get;set;}
    private map<String, LMS_Role_Course__c> courseDraft{get;set;}
    private map<String, LMS_Role_Course__c> courseDraftDelete{get;set;}
    private map<String, LMS_Role_Course__c> mappings{get;set;}
    private map<String, CourseDomainSettings__c> settings{get;set;}
    private map<String, LMSConstantSettings__c> constants{get;set;}
    private Integer currentJTSLIndex {get;set;}
    private map<Integer, list<selectoption>> jtSelectoptionMap {get;private set;}

    // Error Varaibles
    public list<String> sfdcErrors{get;private set;}
    public list<String> addErrors{get;private set;}
    public list<String> deleteErrors{get;private set;}
    public list<String> calloutErrors{get;private set;}
    
    // Constants
    private static final String NEXT_PAGINATE = '------- Next Page -------';
    private static final String PREV_PAGINATE = '----- Previous Page -----';

    /**
     *  Constructor
     *  Initialize values
     */
    public LMS_PRAToCourseController() {
        initSettings();
        init();
    }

    /**
     *  Initialize settings and course domains from custom settings
     */ 
    private void initSettings() {
        settings = CourseDomainSettings__c.getAll();
        constants = LMSConstantSettings__c.getAll();
    }

    /**
     *  Initializes all other data
     */
    private void init() {        
        currentJTSLIndex = 1;
        jtSelectoptionMap = new map<Integer, list<selectoption>>();
        bWebServiceError = false;
        String internalDomain = settings.get('Internal').Domain_Id__c;
    	roleName = 'Enter Role Name';
    	roleNameStyle = 'WaterMarkedTextBox';
    	roleFilter = ' and Role_Type__r.Name = \'PRA\' and Status__c = \'Active\'';
        courseText = 'Enter course title';
        courseTextStyle = 'WaterMarkedTextBox';
        courseFilter = 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
		    'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
		    'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';
    }

    /**
     *  Business Unit LOV 
     */
    public list<selectoption> getBusinessUnitList() {
        list<selectoption> business = new list<selectoption>();
        business = LMS_LookUpDataAccess.getBusinessUnits(false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        for(integer i = 0; i < business.size(); i++)
            options.add(business[i]);
        return options;
    }

    /** 
     *  Department LOV w/ filtering based on Business Unit 
     */
    public list<selectoption> getDepartmentList() {
        list<selectoption> department = new list<selectoption>();
            department = LMS_LookUpDataAccess.getDepartments(false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        for(integer i = 0; i < department.size(); i++)
            options.add(department[i]); 
        return options;
    }

    /** 
     *  Job Family LOV 
     */
    public list<selectoption> getJobFamilyList() {
        list<selectoption> family = new list<selectoption>();
        family = LMS_LookUpDataAccess.getRoles(false, false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        for(integer i = 0; i < family.size(); i++)
            options.add(family[i]);
        return options;
    }

    /**
     *  Job Title LOV w/ filtering based on Job Family 
     */
    public list<selectoption> getJobTitleList() {
    	Integer mapIndex = 1;
    	jtSelectoptionMap.clear();
        list<selectoption> title = new list<selectoption>();
        title = LMS_LookUpDataAccess.getTitles(false);
    	list<selectoption> options = new list<selectoption>();
    	options.add(new selectoption('',''));
        if(jobFamily == null) {
            for(Integer i = 0; i < title.size(); i++){
            	if(options.size() == 999) {
            		options.add(new selectoption(NEXT_PAGINATE,NEXT_PAGINATE));
            		jtSelectoptionMap.put(mapIndex, options);
            		mapIndex++;
            		options = new list<selectoption>();
            		options.add(new selectoption('',''));
            		options.add(new selectoption(PREV_PAGINATE,PREV_PAGINATE));
            		options.add(title[i]);
            	} else {
                	options.add(title[i]);
            	}
            }
            jtSelectoptionMap.put(mapIndex, options);
        } else {
            for(Job_Title__c obj : [select Job_Title__c,Job_Class_Desc__r.Name from Job_Title__c where Job_Class_Desc__r.Name =:jobFamily 
                                    and Status__c != 'I' order by Job_Title__c asc]){
                options.add(new selectoption(obj.Job_Title__c,obj.Job_Title__c));
            }
            currentJTSLIndex = mapIndex;
            jtSelectoptionMap.put(mapIndex, options);
        }
        return jtSelectoptionMap.get(currentJTSLIndex);
    }
    
    public Pagereference updateJTList() {
    	if (jobTitle != null && jobTitle != '') {
	    	if (jobTitle.equals(NEXT_PAGINATE)) {
	    		currentJTSLIndex++;
	    	} else if (jobTitle.equals(PREV_PAGINATE)) {
	    		currentJTSLIndex--;
	    	}
    	}
    	getJobTitleList();
    	return null;
    }

    /**
     *  Function to filter down Job Title 
     */
    public pageReference getTitleOnFamily() {
        getJobTitleList();
        return null;
    }

    /**
     *  Region LOV 
     */
    public list<selectoption> getRegionList() {
        list<selectoption> region = new list<selectoption>();
        region = LMS_LookUpDataAccess.getRegion(false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        for(integer i = 0; i < region.size(); i++) {
            options.add(region[i]);
        }
        return options;
    }

    /**
     *  Country LOV w/ filtering based on Region 
     */
    public list<selectoption> getCountryList() {
        list<selectoption> country = new list<selectoption>();
        country = LMS_LookUpDataAccess.getCountry(false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        if(region == null) {
            for(integer i = 0; i < country.size(); i++){
                options.add(country[i]);
            }
        } else {
            for(Country__c obj : [select Name,Region_Name__c from Country__c where Region_Name__c =:region order by Region_Name__c asc]) {
                options.add(new selectoption(obj.Name, obj.Name));
            }
        }
        return options;
    }

    /**
     *  Function to filter down Country 
     */
    public pagereference getCountryOnRegion() {
        getCountryList();
        return null;
    }

    /**
     *  Employee Types LOV 
     */
    public list<selectoption> getTypesList() {
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
            options.add(new selectoption('Contractor','Contractor'));
            options.add(new selectoption('Employees','Employee'));
        return options;
    }

    public void courseReset() {
        //Reset LOVs
    	currentJTSLIndex = 1;
        getTitleOnFamily();
        getCountryOnRegion();
        bWebServiceError = false;
        roles = null;
        courses = null;
        selRoles = null;
        viewImpact();
    } 

    public void roleReset() {
    	currentJTSLIndex = 1;
        getTitleOnFamily();
        getCountryOnRegion();
        selRoles = null;
        viewImpact();
    }

    public void closeCourseSearch() {
        selRoles = null;
    }

    /**
     *  Implementation for course search 
     */
    public void search() {
		bWebServiceError = false;
        courseNoDraft = new map<String,LMS_Role_Course__c>();
        courseCommitted = new map<String,LMS_Role_Course__c>();
        courseDraft = new map<String,LMS_Role_Course__c>();
        courseDraftDelete = new map<String,LMS_Role_Course__c>();
        mappings = new map<String, LMS_Role_Course__c>();

        Date sysDate = Date.today();
        String draft = constants.get('statusDraft').Value__c;
        String committed = constants.get('statusCommitted').Value__c;
        String draftDelete = constants.get('statusDraftDelete').Value__c;
        String pendingAdd = constants.get('statusPendingAdd').Value__c;
        String pendingDelete = constants.get('statusPendingDelete').Value__c;
        String internalDomain = settings.get('Internal').Domain_Id__c;

        String roleQry = '';
        roleQry = 'select Role_Id__r.Role_Name__c,Role_Id__r.SABA_Role_PK__c,Role_Id__r.Status__c,Course_Id__r.Available_From__c,' + 
		    'Course_Id__r.SABA_ID_PK__c,Assigned_On__c,Assigned_By__c,Commit_Date__c,Status__c,Previous_Status__c,Sync_Status__c ' + 
		    'from Rolecourses__r where Role_Id__r.Role_Type__r.Name =\'PRA\' and Role_Id__r.Status__c = \'Active\' and Status__c != \'Removed\' ' + 
		    'order by Status__c,Role_Id__r.Role_Name__c';
        String courseQry = '';
        courseQry = 'select Title__c,Duration__c,Duration_Display__c,('+roleQry+') from LMS_Course__c ' + 
		    'where Title__c=:courseText and Type__c != \'PST\' and Domain_Id__c=\'' + String.escapeSingleQuotes(internalDomain) + 
		    '\' and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
		    'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';
        allChecked = false;
        courses = Database.query(courseQry);

        if(courses.size() > 0) {
            roles = new list<LMS_RoleAssignment>();
            for(LMS_Course__c c : courses) {
                courseNameDisplay = c.Title__c;
                courseResultId = c.Id;
                for(LMS_Role_Course__c rc : c.roleCourses__r) {
                	mappings.put(rc.Id, rc);
                    if(rc.Status__c == committed || rc.Status__c == draftDelete || rc.Status__c == pendingDelete) {
                        courseNoDraft.put(rc.Role_Id__r.Id,rc);
                    }
                    if(rc.Status__c == draft || rc.Status__c == pendingAdd) {
                        courseDraft.put(rc.Role_Id__r.Id,rc);
                    } 
                    if(rc.Status__c == draftDelete || rc.Status__c == pendingDelete) {
                        courseDraftDelete.put(rc.Role_Id__r.Id,rc);
                    } 
                    if(rc.Status__c == committed) {
                        courseCommitted.put(rc.Role_Id__r.Id,rc);
                    }
                    roles.add(new LMS_RoleAssignment(rc));
                }
            }
            viewImpact();
        }
        if(courseNoDraft != null && !courseNoDraft.isEmpty()) {
        	String excludeString = '\'\'';
        	Set<String> excludeIds = courseNoDraft.keySet();
        	for(String s : excludeIds) {
    			excludeString += ',\'' + s + '\'';
        	}
	        roleFilter = ' and Role_Type__r.Name = \'PRA\' and Status__c = \'Active\' and Id NOT IN (' + excludeString + ')';
        } else {
        	roleFilter = ' and Role_Type__r.Name = \'PRA\' and Status__c = \'Active\'';
        }
    }

    /**
     *  Implementation for role search 
     */
    public void roleSearch() {
    	Boolean acSearch;
        Set<String> cAssigned = new Set<String>();
        if(courseNoDraft != null) {
            cAssigned = courseNoDraft.keySet();
        }

        String roleQuery = '';
        roleQuery = 'select id,Role_Name__c,Status__c,(select name from RoleEmployees__r) from LMS_Role__c ' + 
        	'where Id not in : cAssigned and Role_Type__r.Name=\'PRA\' and Status__c = \'Active\' and Sync_Status__c=\'Y\'';

        // Check for each type of criteria
        if(null != roleName && roleName != '' && roleName != 'Enter Role Name') {
        	roleQuery += ' and Role_Name__c LIKE \'' + String.escapeSingleQuotes(roleName) + '%\'';
        	acSearch = true;
        } else {
        	acSearch = false;
        	if(null != jobFamily) {roleQuery += ' and Job_Class_Desc__r.Name=:jobFamily';}
	        if(null != jobTitle) {roleQuery += ' and Job_Title__r.Job_Title__c=:jobTitle';}    
	        if(null != businessUnit) {roleQuery += ' and Business_Unit__r.Name=:businessUnit';}
	        if(null != department) {roleQuery += ' and Department__r.Name=:department';}
	        if(null != region) {roleQuery += ' and Region__r.Name=:region';}
	        if(null != country) {roleQuery += ' and Country__r.Name=:country';}
	        if(null != empType) {roleQuery += ' and Employee_Type__r.Name=:empType';}
        }
        roleQuery += ' order by Role_Name__c';
        allCheckedRole = false;

        selRoles = new list<LMS_RoleList>();
        list<LMS_Role__c> roleList = Database.query(roleQuery);
        if(roleList.size() > 0) {
            for(LMS_Role__c r : roleList) {
                selRoles.add(new LMS_RoleList(r, courseDraft));
            }
            viewImpact();
        } else {
        	if(acSearch) {
	            roleErrorText = 'No results found, role does not exist, is inactive, or is currently mapped to course. Please enter a different role name.';
        	} else {
	            roleErrorText = 'No results found, role does not exist, is inactive, or is currently mapped to course. Please enter different criteria.';
        	}
        }
    }

    /**
     *  This method will add all selected roles to the selected course
     *  All of these roles will be given a Draft status 
     */
    public PageReference addRole() {
    	sfdcErrors = new list<String>();
    	map<String, list<String>> errors = new map<String, list<String>>();

        errors = LMS_ToolsModifyAssignments.addRoles(selRoles, roles, mappings, courseResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while adding role to course.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    /**
     *  This method performs several functions depending on course assignment status
     *  If current status is Draft : course is deleted/unassigned
     *  If current status is Committed : course status is changed to Draft Delete
     *  If current status is Draft Delete : course status is changed back to Committed
     */
    public PageReference removeRoles() {
    	sfdcErrors = new list<String>();
    	map<String, list<String>> errors = new map<String, list<String>>();

        errors = LMS_ToolsModifyAssignments.removeRoles(roles, mappings);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while removing or reverting role from course.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    /**
     *  This method will remove all changes made to the
     *  current role assignment.
     *  Draft Delete -> Committed
     *  Draft -> Deleted 
     */
    public PageReference cancel() {
    	sfdcErrors = new list<String>();
    	map<String, list<String>> errors = new map<String, list<String>>();

        errors = LMS_ToolsModifyAssignments.cancelDraftRoles(courseResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }

        search();
        roleSearch();
        viewImpact();
        return null;
    }

    public PageReference setCommitDate() {

    	sfdcErrors = new list<String>();
        addErrors = new list<String>();
        deleteErrors = new list<String>();
        calloutErrors = new list<String>();
        map<String, list<String>> errors = new map<String, list<String>>();

        Date dateCommit = Date.parse(commitDate);
        errors = LMS_ToolsModifyAssignments.setCommitDateRoles(roles, mappings, dateCommit);

        sfdcErrors = errors.get('SFDCEX');
        System.debug('-------------errors------------'+errors);
        System.debug('-------------sfdcErrors-----------'+sfdcErrors);

        if(null != sfdcErrors && sfdcErrors.size() > 0) {
            webServiceError = 'Errors occurred while applying commit date.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    /**
     *  This method finalizes changes to the course assignments 
     *  Courses with Draft status will be changed to committed
     *  Courses with Draft Delete status will be unassigned/deleted from the list 
     */
    public PageReference commitRoles() {
        map<String, list<String>> errors = new map<String, list<String>>();
        sfdcErrors = new list<String>();
        addErrors = new list<String>();
        deleteErrors = new list<String>();

        errors = LMS_ToolsModifyAssignments.commitRoles(roles, mappings);
        sfdcErrors = errors.get('SFDCEX');
        addErrors = errors.get('A');
        deleteErrors = errors.get('D');
        calloutErrors = errors.get('CALLOUT');

        System.debug('-------------errors------------'+errors);
        System.debug('-------------sfdcErrors------------'+sfdcErrors);
        System.debug('-------------addErrors------------'+addErrors);
        System.debug('-------------deleteErrors------------'+deleteErrors);
        System.debug('-------------calloutErrors------------'+calloutErrors);

        if((null != sfdcErrors && sfdcErrors.size() > 0) || (null != addErrors && 0 < addErrors.size()) || 
        	(null != deleteErrors && 0 < deleteErrors.size()) || (null != calloutErrors && 0 < calloutErrors.size())) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    /** 
     *  Method used to calculate the impact of adding roles to a course 
     */
    public void viewImpact() {

        transient map<String, String> empMap = new map<String, String>();
        transient map<String, String> empMapCommitted = new map<String, String>();
        transient Integer cToAdd,cToDelete = 0;
        transient Decimal courseDuration = 0;

        if(courseCommitted != null && !courseCommitted.isEmpty()) {
        	empMapCommitted = LMS_ToolsDataAccessor.getCommittedMapFromRoleIds(courseCommitted.keySet()); // Map for course committed
        }
        if(courseNoDraft != null && !courseNoDraft.isEmpty()) {
        	empMap = LMS_ToolsDataAccessor.getNoDraftMapFromRoleIds(courseNoDraft.keySet()); // Map for no draft
        }
        if(courseDraft != null && !courseDraft.isEmpty()) {        
            cToAdd = LMS_ToolsDataAccessor.getTotalEmployeeCountForAddMappings(courseDraft.keySet(), empMap);
        } else {
            cToAdd = 0;
        }
        if(courseDraftDelete != null && !courseDraftDelete.isEmpty()) {
        	cToDelete = LMS_ToolsDataAccessor.getTotalEmployeeCountForDeleteMappings(courseResultId, courseDraftDelete.keySet());
        } else {
            cToDelete = 0;
        }

        System.debug('-------------empMapSize----------------' + empMap.size());
        employeeStart = empMap.size();
        if(null != courseResultId && courseResultId != '') {
        	courseDuration = LMS_ToolsDataAccessor.getTotalCourseDurationFromCourseId(courseResultId);
            impactStart = (LMS_ToolsDataAccessor.getTotalCourseDurationFromCourseId(courseResultId)*employeeStart/60).setScale(2);
        } else {
            impactStart = 0;
        }

        employeeChange = cToAdd - cToDelete;
        impactNum = 0;
        if(courseDuration != null) {
            impactNum = (employeeChange*courseDuration/60).setScale(2);
        }
        if(impactNum > 0) {
            impactChange = '+ '+impactNum;
        } else if(impactNum <= 0 || impactNum == null) {
            impactChange = String.valueOf(impactNum);
        }
        impactEnd = (impactStart + impactNum).setScale(2);
        employeeEnd = employeeStart + employeeChange;
    }

    public PageReference showErrors() {
        PageReference pr = new PageReference('/apex/LMS_PRACourseError');
        pr.setRedirect(false);
        return pr;
    }

}