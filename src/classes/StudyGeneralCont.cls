public with sharing class StudyGeneralCont {
    public PAWS_ProjectDetailCVO    projectDetailItem   {get;set;}
    public Map<String,Decimal>      countrySummary      {get;set;}
    public Id                       scenarioId          {get;set;}
    public Integer                  goalInteger         {get;set;}
    public Date                     siteActivatedByDate {get;set;}
    
    public List<PBBMobileSettingWrapper> settingList {get;set;}

    private PBB_Scenario__c scenarioItem {get;set;}
    private Id projectId = ApexPages.currentPage().getParameters().get( PBBUtils.PROJECTID_PARAM_STRING );

    public StudyGeneralCont(  ) {
        settingList = PBBMobileSettingServices.getSettingList( Id.valueOf(UserInfo.getUserId() ) );

        if( projectId != null ) projectDetailItem = PAWS_API.PBB_getProjectDetails( projectId );
                
        scenarioItem = PBB_WR_APIs.getAprrovedScenarioID( projectDetailItem.ProjectID );
                
        if( scenarioItem == null ){
            scenarioId = Id.valueOf('a0YL0000008ar8ZMAQ');  // TODO Dima HARDCODE
        } else {
            scenarioId = scenarioItem.Id;
        }

        countrySummary = new Map<String,Decimal>();
                
        for( PAWS_ProjectCountryCVO countryItem : projectDetailItem.Countries ){
            countrySummary.put( 'NumberOfSites',            ( countrySummary.get( 'NumberOfSites' )             != null ? countrySummary.get( 'NumberOfSites' ) : 0 )           +  ( countryItem.NumberOfSites          != null ? countryItem.NumberOfSites : 0 ) );
            countrySummary.put( 'NumberOfSitesAtRisk',      ( countrySummary.get( 'NumberOfSitesAtRisk' )       != null ? countrySummary.get( 'NumberOfSitesAtRisk' ) : 0 )     +  ( countryItem.NumberOfSitesAtRisk    != null ? countryItem.NumberOfSitesAtRisk : 0 ) );
            countrySummary.put( 'NumberOfSitesOnTrack',     ( countrySummary.get( 'NumberOfSitesOnTrack' )      != null ? countrySummary.get( 'NumberOfSitesOnTrack' ) : 0 )    +  ( countryItem.NumberOfSitesOnTrack   != null ? countryItem.NumberOfSitesOnTrack : 0 ) );
            countrySummary.put( 'NumberOfSitesDelayed',     ( countrySummary.get( 'NumberOfSitesDelayed' )      != null ? countrySummary.get( 'NumberOfSitesDelayed' ) : 0 )    +  ( countryItem.NumberOfSitesDelayed   != null ? countryItem.NumberOfSitesDelayed : 0 ) );
            countrySummary.put( 'NumberOfSitesComplete',    ( countrySummary.get( 'NumberOfSitesCompleted' )    != null ? countrySummary.get( 'NumberOfSitesCompleted' ) : 0 )  +  ( countryItem.NumberOfSitesCompleted != null ? countryItem.NumberOfSitesCompleted : 0 ) );
        }

    }

    public void saveData(){
        PAWS_API.PBB_setAggregatedMilestone ( projectDetailItem.RecordID, 
                                                goalInteger,
                                                siteActivatedByDate,
                                                'Site Activation'
                                                );

    }
}