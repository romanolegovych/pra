/**
@author 
@date 2014
@description this class for srm subject enrollement 
**/
public with sharing class SRM_ModelSubjectEnrollmentcontroller{
    
    //Member variables
    public SRM_Model_Subject_Enrollment__c subjectEnrollment{ get; set; }
    public String chartData{ get; set; }
    public List<String> xValuesList{ get; set; }
    public List<Decimal> yValuesList{ get; set; }
    public String showChart{ get; set; }
    public String returnUrl{ get; set; }
    public String recordId{ get; set; }
    public Boolean applyWeekAdj{ get; set; }
    
    public Boolean booVal {get;set;}
    
    public SRM_ModelSubjectEnrollmentController(ApexPages.StandardController controller) {
        String id = ApexPages.CurrentPage().getParameters().get('id');
        returnUrl = ApexPages.CurrentPage().getParameters().get('retUrl');
        String clone = ApexPages.CurrentPage().getParameters().get('clone');
        recordId = id;
        subjectEnrollment = (SRM_Model_Subject_Enrollment__c)controller.getRecord();
        String parentId = subjectEnrollment.SRM_Model__c;
        xValuesList = new List<String>();
        yValuesList = new List<Decimal>();
        showChart = 'none';
        applyWeekAdj = false;
        if(String.isEmpty(id)){
            subjectEnrollment = new SRM_Model_Subject_Enrollment__c();
            if(!String.isEmpty(parentId)){
                subjectEnrollment.SRM_Model__c = parentId;
            }
            //list<SRM_Model_Subject_Enrollment__c>  ss1 = [select name,default__c,srm_model__c,standard_deviation__c from SRM_Model_Subject_Enrollment__c where srm_model__c =: parentid and default__c=true];
            list<SRM_Model_Subject_Enrollment__c>  ss1 = PBB_DataAccessor.getSrmModelSubjectEnrollmetbymodelId(parentid);
            
            if(!ss1.isempty()){
                booVal = true;
            }
            else{
                booVal = false;
            }
        }
        else if(!String.isEmpty(clone)){
            
            SRM_Model_Subject_Enrollment__c temp = SRM_ScenarioDataAccessor.getSubEnrollById(id);
            SRM_Model_Subject_Enrollment__c clonerecord = temp.clone(false,true);
            subjectEnrollment = clonerecord;
            //list<SRM_Model_Subject_Enrollment__c>  ss2 = [select name,default__c,srm_model__c,standard_deviation__c from SRM_Model_Subject_Enrollment__c where srm_model__c =: parentid and default__c=true];
            list<SRM_Model_Subject_Enrollment__c> ss2 = PBB_DataAccessor.getSrmModelSubjectEnrollmetbymodelId(parentid);
            if(!ss2.isempty()){
                booVal = true;
                subjectEnrollment.Default__c = false;
            }
            generateChartData();
            
        }
        else{
            SRM_Model_Subject_Enrollment__c tmpObj = SRM_ScenarioDataAccessor.getSubEnrollById(id);
            
            //list<SRM_Model_Subject_Enrollment__c>  ss = [select name,default__c,srm_model__c,standard_deviation__c from SRM_Model_Subject_Enrollment__c where srm_model__c =: parentid and default__c=true];
            list<SRM_Model_Subject_Enrollment__c>  ss = PBB_DataAccessor.getSrmModelSubjectEnrollmetbymodelId(parentid);
            if(!ss.isempty() && tmpObj.id != ss[0].id){
                booVal = true;
            }
            else{
                booVal = false;
            }
            if(tmpObj != null){
                subjectEnrollment = tmpObj;
                system.debug('---subjectEnrollment---'+subjectEnrollment);
                generateChartData();
            }
            
        }
        
    }
    
    public SRM_ModelsubjectEnrollmentController(){}
    
    
    /**
@author 
@date 2014
@description this method to generate data
**/
    public PageReference generateChartData(){
        SRM_ModelSiteActivationSevice siteService = new SRM_ModelSiteActivationSevice();
        
        if(subjectEnrollment.Standard_Deviation__c <= 0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a positive value for Curve Width');
            ApexPages.addMessage(myMsg);
            return null; 
        }
        showChart = '';
        xValuesList.clear();
        yValuesList.clear();
        yValuesList = siteService.generateChartData(subjectenrollment, 'SRM_Model_Subject_Enrollment__c', false);
        for(Integer i=1; i<=10; i++){
            xValuesList.add(i+'');
            System.debug('xxValuesList: '+i);
        }
        System.debug('txValuesList: '+xValuesList);
        return null;
    }
    
    
    /**
@author 
@date 2014
@description this method to save
**/
    //Method to save
    public PageReference savesubjectEnrollment(){
        
        if(subjectEnrollment.Standard_Deviation__c <= 0){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a positive value for Curve Width');
            ApexPages.addMessage(myMsg);
            return null; 
        }
        
       if(subjectEnrollment != null){
            try{
                upsert subjectEnrollment;
                PageReference p = new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+subjectEnrollment.Id);
                p.setRedirect(true);
                System.debug('Created or Updated Id: '+subjectEnrollment.Id);
                return p;
            }
            catch(DMLException ex){
                if(ex.getDmlType(0) == StatusCode.DUPLICATE_VALUE){
                    SRM_Utils.showErrorMessage('A record already exists with this Name. Please modify this record before saving so it is not an exact duplicate.');
                }
                
                ApexPages.addMessages(ex);   
               
            }
        }
        
         return null; 
       
    }
    
    /**
@author 
@date 2014
@description this method to cancel
**/
    //Method to return
    public PageReference cancel(){
        String returnRecordId = '';
        String redUrl = '';
        if(subjectEnrollment.Id == null){
            redUrl = returnUrl;
        }
        else{
            returnRecordId = subjectEnrollment.Id;
            redUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+returnRecordId;
        }
        PageReference p = new PageReference(redUrl);
        p.setRedirect(true);
        return p;
   }
   }