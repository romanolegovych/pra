public class PRA_OverallProjectController {
    
    //value entered by the user in the input text box 
    public static final String ST_NONE = 'None';
    public String projectText{get;set;}    
    public String searchprojectText{get;set;}
    public Date selectedProjectMigratedDate {get;set;}
    public Date LastApprovedDate {get;set;}
    //values displayed on the page
    public String ProjectName{get;set;}
    public String ClientName{get;set;}
    public String Protocol{get;set;}
    public String Phase{get;set;}
    public String TherapeuticArea{get;set;}
    public String Indication{get;set;}
    //Contains list of values to display
    public List<Client_Project__c> projectdetaillist{get;set;}
    //value selected by the user in the picklists
    public String SelectedType{get;set;}
    public String Selectedview{get;set;}
    //List of values to display in graphs
    public List<String> ContractedList{get;set;}
    public List<String> ActualList{get;set;}
    public List<String> ForecastList{get;set;}
    //Assining Subtitles,types for a graph through the controller dynamically
    public  string SubTitleGraph{get;set;}
    public  string type{get;set;}
    //Variables for Show & Hide Table    
    public String showCriteria1{get;set;}
    public String criteriaVal1{get;set;}
    //lists of Wrapper Class 
    public List<GridDataTable> HoursTableList{get;set;}
    // for user prefs
    private Integer searchCount{get;set;}
    private PRA_Utils.UserPreference userPreference{get;private set;}
    
    //Controller
    public PRA_OverallProjectController() {
        type = 'Hours';
        SubTitleGraph = 'Hours in Cumulative View';
        showCriteria1 = 'display: none';
        criteriaVal1 = '► Show Table';
        searchCount = -1;
        
        userPreference = PRA_Utils.retrieveUserPreference();
        if(userPreference != null) {
            List<Client_Project__c> clientProjectsList = 
            	[SELECT Name, Is_Project_Locked__c, Migrated_Date__c FROM Client_Project__c WHERE Id = :userPreference.projectId 
            		and Load_Status__c != 'New'];
            if(clientProjectsList.size() == 1) {
                SelectedType = 'Hours';
                Selectedview = 'Cumulative View';
                projectText = clientProjectsList.get(0).Name;
                searchProject();
            } else {
            	searchCount++;
            }
        } else {
        	searchCount++;
        }
    }
    
    public void reset() {
    	projectText = null;
    	searchProject();
    	SearchgraphBasedOncost();
    	SearchgraphBasedOnHours();
    	PRA_Utils.deleteUserPreference();
    }
    
    // select list for Overall Project Type section
    public List<SelectOption> getSearchOnType() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Hours', 'Hours'));
        options.add(new SelectOption('Cost', 'Cost'));
        return options;
    }
    
    // select list for Overall Project View section
    public List<SelectOption> getSearchOnView() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Cumulative View', 'Cumulative View'));
        options.add(new SelectOption('Month', 'Month'));
        return options;
    }
    
    //Search Method for Search Button   
    public void searchProject() {   
        searchCount++;
        searchprojectText = projectText;
        if(searchprojectText != null && searchprojectText != '') {
            projectdetaillist = [SELECT  Id, Name,Client__r.Name, Migrated_date__c, PRA_Protocol__c, Phase__c, Therapeutic_Area__c, Therapeutic_Indication__c
                                FROM Client_Project__c 
                                WHERE Name LIKE :searchprojectText and Load_Status__c != 'New'];
            if(projectdetaillist != null && projectdetaillist.size() > 0) {              
                for(Client_Project__c obj : projectdetaillist) { 
                    if(userPreference != null) {
                        userPreference.projectId = obj.Id;
                    } else { 
                        userPreference = new PRA_Utils.UserPreference();
                        userPreference.projectId = obj.Id;
                        userPreference.regionId = new String[]{ST_NONE};
                        userPreference.countryId = new String[]{ST_NONE};
                        userPreference.clientUnitId = new String[]{ST_NONE};
                        userPreference.clientUnitGroupId = new String[]{ST_NONE};
                        userPreference.operationalAreaId = new String[]{ST_NONE};
                        userPreference.unconfirmedOnly = false;
                        userPreference.confirmedOnly = false;
                        userPreference.crctId = '';
                        userPreference.userId = UserInfo.getUserId();
                    }   
			        if(searchCount > 0) {
			            // Save user prefs
			            PRA_Utils.saveUserPreference(userPreference);
			        }       
                    ProjectName = obj.Name;
                    ClientName = obj.Client__r.Name;
                    Protocol = obj.PRA_Protocol__c;
                    Phase = obj.Phase__c;
                    TherapeuticArea = obj.Therapeutic_Area__c;
                    Indication = obj.Therapeutic_Indication__c;
                    selectedProjectMigratedDate = obj.Migrated_date__c;
                }
            }
            //To get Last Approved Date 
            for(Unit_Effort__c ue : [SELECT Month_Forecast_Created__c FROM Unit_Effort__c 
                            WHERE Client_Task__r.Project__r.Name LIKE :searchprojectText 
                            ORDER BY Month_Forecast_Created__c DESC LIMIT 1]) {
                LastApprovedDate = ue.Month_Forecast_Created__c; 
            }
                                                  
            if(SelectedType.equalsIgnoreCase('Hours')) {            
                if(Selectedview.equalsIgnoreCase('Cumulative View')) {                    
                    SubTitleGraph = 'Hours in Cumulative View';
                } else {                    
                    SubTitleGraph = 'Hours in Monthly View';
                }
                type = 'Hours';
                SearchgraphBasedOnHours();
            } else {
                if(SelectedType.equalsIgnoreCase('Cost')) {            
                    if(Selectedview.equalsIgnoreCase('Cumulative View')) {                
                        SubTitleGraph = 'Cost in Cumulative View';
                    } else {                
                        SubTitleGraph = 'Cost in Monthly View';
                    }
                    type = 'Dollars($)';
                    SearchgraphBasedOncost();
                } 
            }            
        } else {
        	ProjectName = null;
            ClientName = null;
            Protocol = null;
            Phase = null;
            TherapeuticArea = null;
            Indication = null;
            selectedProjectMigratedDate = null;
        }                                    
    }
    
    //Search on Hours  
    public void SearchgraphBasedOnHours() {      
        String tchstr;
        String tahstr;
        String tfhstr;
        Double sumtchvalues = 0;
        Double sumtahvalues = 0;
        Double sumtfhvalues = 0;        
        ContractedList = new List<String>();
        ActualList = new List<String>();
        ForecastList = new List<String>();
        HoursTableList = new List<GridDataTable>();        
        if(searchprojectText != null && searchprojectText != '') {
            // Contracted
            Map<Date,Decimal> mapcontracted = new Map<Date,Decimal>();
            for(AggregateResult ar : [SELECT Month_Applies_To__c MC, sum(LA_Total_Contracted_BDG_Hours__c) TCH FROM Unit_Effort__c 
                                        WHERE Client_Task__r.Project__r.Name LIKE :searchprojectText
                                        GROUP BY Month_Applies_To__c ORDER BY Month_Applies_To__c]) {
                if(ar.get('MC') != null && ar.get('TCH') != null) {
                    Decimal myDecimal = Decimal.valueOf(Double.valueOf(ar.get('TCH')));
                    mapcontracted.put(Date.valueof(ar.get('MC')), myDecimal);
                }
            }              
            
            // Actual
            Map<Date,Decimal> mapActual = new Map<Date, Decimal>();
            for(AggregateResult ar : [SELECT Month_Applies_To__c MA, sum(Total_Worked_Hours__c) TAH FROM Unit_Effort__c
                                        WHERE Client_Task__r.Project__r.Name LIKE :searchprojectText AND 
                                        Month_Applies_To__c <= LAST_MONTH
                                        GROUP BY Month_Applies_To__c ORDER BY Month_Applies_To__c]) {
                if(ar.get('MA') != null && ar.get('TAH') != null) {
                    Decimal myDecimal = Decimal.valueOf(Double.valueOf(ar.get('TAH')));
                    mapActual.put(Date.valueof(ar.get('MA')), myDecimal);
                }
            }
            
            // Forecast
            Map<Date,Decimal> mapForecast = new Map<Date,Decimal>();                
            for(AggregateResult ar : [SELECT Month_Applies_To__c MF, sum(Total_Forecast_Hours__c) TFH FROM Unit_Effort__c
                                         WHERE Client_Task__r.Project__r.Name LIKE :searchprojectText AND
                                         Month_Applies_To__c >= THIS_MONTH  
                                         GROUP BY Month_Applies_To__c ORDER BY Month_Applies_To__c]) {
                if(ar.get('MF') != null && ar.get('TFH') != null) {
                    Decimal myDecimal = Decimal.valueOf(Double.valueOf(ar.get('TFH')));
                    mapForecast.put(Date.valueof(ar.get('MF')),myDecimal);
                }
            }
                                 
            // Adding values to Lists and table for Hour Graphs
            for(AggregateResult ar : [SELECT Month_Applies_To__c PMD FROM Unit_Effort__c 
                                        WHERE Client_Task__r.Project__r.Name LIKE :searchprojectText
                                        GROUP BY Month_Applies_To__c ORDER BY Month_Applies_To__c]) {              
                if(ar.get('PMD') != null) {               
                    GridDataTable obj = new GridDataTable(); 
                    Date dateVal = Date.valueOf(ar.get('PMD'));
                    String strDate = dateVal.format();
                    obj.column1 = strDate;
                    // Adding Contracted Budgeted values
                    if(mapcontracted.containsKey(Date.Valueof(ar.get('PMD'))) != null) {   
                        if(mapcontracted.get(Date.Valueof(ar.get('PMD'))) != null) {
                            Date dateValc = Date.valueOf(ar.get('PMD'));
                            Integer monthc = dateValc.month() - 1;
                            Integer yearc = dateValc.year();                                 
                            Decimal tchd = mapcontracted.get(Date.Valueof(ar.get('PMD')));
                            Decimal tchd1 = tchd.divide(1, 2);
                            Decimal tchvalue = tchd1;                                        
                            if(sumtchvalues == 0)
                                sumtchvalues = tchvalue;
                            else
                                sumtchvalues += tchvalue;                                    
                            if(Selectedview != null && Selectedview != '' && Selectedview.equalsIgnoreCase('Cumulative View')) {                    
                                tchstr = 'Date.UTC(' + yearc + ',' + monthc + '),' + sumtchvalues;
                                obj.column2 = sumtchvalues;
                            }
                            else {                  
                                tchstr = 'Date.UTC(' + yearc + ',' + monthc + '),' + tchvalue;
                                obj.column2 = tchvalue;
                            }            
                            ContractedList.add(tchstr);
                        }                     
                    }                          
                    //Adding Actual values
                    if(mapActual.containsKey(Date.Valueof(ar.get('PMD'))) != null) {  
                        if(mapActual.get(Date.Valueof(ar.get('PMD'))) != null) {
                            Date dateVala = Date.valueOf(ar.get('PMD'));
                            Integer montha = dateVala.month() - 1;
                            Integer yeara = dateVala.year();                                 
                            Decimal tahd = mapActual.get(Date.Valueof(ar.get('PMD')));
                            Decimal tahd1 = tahd.divide(1, 2);
                            Decimal tahvalue = tahd1;                                
                            if(sumtahvalues == 0)
                                sumtahvalues = tahvalue;
                            else
                                sumtahvalues += tahvalue;
                            if(Selectedview != null && Selectedview != '' && Selectedview.equalsIgnoreCase('Cumulative View')) {                        
                                tahstr = 'Date.UTC(' + yeara + ',' + montha + '),' + sumtahvalues;
                                obj.column3 = sumtahvalues;
                            }
                            else {                        
                                tahstr = 'Date.UTC(' + yeara + ',' + montha + '),' + tahvalue;
                                obj.column3 = tahvalue;
                            }           
                            ActualList.add(tahstr);  
                        }                     
                    }  
                    //Adding Forecasted values
                    if(mapForecast.containsKey(Date.Valueof(ar.get('PMD'))) != null) { 
                        if(mapForecast.get(Date.Valueof(ar.get('PMD'))) != null) {
                            Date dateValf = Date.valueOf(ar.get('PMD'));
                            Integer monthf = dateValf.month() - 1;
                            Integer yearf = dateValf.year();
                            Decimal tfhd = mapForecast.get(Date.Valueof(ar.get('PMD')));
                            Decimal tfhd1 = tfhd.divide(1, 2);
                            Decimal tfhvalue = tfhd1;                                
                            if(sumtfhvalues == 0)
                                sumtfhvalues = tfhvalue;
                            else
                                sumtfhvalues += tfhvalue;                         
                            if(Selectedview != null && Selectedview != '' && Selectedview.equalsIgnoreCase('Cumulative View')) { 
                                tfhstr = 'Date.UTC(' + yearf + ',' + monthf + '),' + sumtfhvalues;
                                obj.column4 = sumtfhvalues;
                            }
                            else {                  
                                tfhstr = 'Date.UTC(' + yearf + ',' + monthf + '),' + tfhvalue;
                                obj.column4 = tfhvalue;                    
                            }                                     
                            ForecastList.add(tfhstr);   
                        }                     
                    }             
                    HoursTableList.add(obj); 
                }
            }
        } else {
        	HoursTableList = new List<GridDataTable>();
        }      
    }
        
    // Search on Cost   
    public void SearchgraphBasedOncost() {
        String tccstr;
        String tacstr;
        String tfcstr;
        Double sumtccvalues = 0;
        Double sumtacvalues = 0;
        Double sumtfcvalues = 0;        
        ContractedList = new List<String>();
        ActualList = new List<String>();
        ForecastList = new List<String>();
        HoursTableList = new List<GridDataTable>();
        if(searchprojectText != null && searchprojectText != '') {
            // Official Forecast Budgeted 
            Map<Date,Decimal> mapcontracted = new Map<Date,Decimal>();                  
            for(AggregateResult ar : [SELECT Month_Applies_To__c MC, sum(Sum_Contracted_BDG_Cost__c) tcc FROM Unit_Effort__c 
                                        WHERE Client_Task__r.Project__r.Name LIKE :searchprojectText
                                        GROUP BY Month_Applies_To__c ORDER BY Month_Applies_To__c]) {
                if(ar.get('MC') != null && ar.get('tcc') != null) {
                    Decimal myDecimal = Decimal.valueOf(Double.valueOf(ar.get('tcc')));
                    mapcontracted.put(Date.valueof(ar.get('MC')),myDecimal);
                }
            }
            
            // Worked
            Map<Date,Decimal> mapActual = new Map<Date,Decimal>();
            for(AggregateResult ar : [SELECT Month_Applies_To__c MA, sum(Total_Worked_Cost__c) tac FROM Unit_Effort__c
                                        WHERE Client_Task__r.Project__r.Name LIKE :searchprojectText AND 
                                        Month_Applies_To__c <= LAST_MONTH
                                        GROUP BY Month_Applies_To__c ORDER BY Month_Applies_To__c]) {
                if(ar.get('MA') != null && ar.get('tac') != null) {
                    Decimal myDecimal = Decimal.valueOf(Double.valueOf(ar.get('tac')));
                    mapActual.put(Date.valueof(ar.get('MA')),myDecimal);
                }
            }
              
            // Historical/LA
            Map<Date,Decimal> mapForecast = new Map<Date,Decimal>();
            for(AggregateResult ar : [SELECT Month_Applies_To__c MF, sum(Sum_Forecast_Cost__c) tfc FROM Unit_Effort__c
                                         WHERE Month_Applies_To__c >= THIS_MONTH AND 
                                         Client_Task__r.Project__r.Name LIKE :searchprojectText  
                                         GROUP BY Month_Applies_To__c ORDER BY Month_Applies_To__c]) {
                if(ar.get('MF') != null && ar.get('tfc') != null) {
                    Decimal myDecimal = Decimal.valueOf(Double.valueOf(ar.get('tfc')));
                    mapForecast.put(Date.valueof(ar.get('MF')),myDecimal);
                }
            }

            // Adding values to Lists and table for Cost Graphs 
            for(AggregateResult ar : [SELECT Month_Applies_To__c PMD FROM Unit_Effort__c 
                                        WHERE Client_Task__r.Project__r.Name LIKE :searchprojectText
                                        GROUP BY Month_Applies_To__c ORDER BY Month_Applies_To__c]) {              
                if(ar.get('PMD') != null) {               
                    GridDataTable obj = new GridDataTable(); 
                    Date dateVal = Date.valueOf(ar.get('PMD'));
                    String strDate = dateVal.format();
                    obj.column1 = strDate; 
                     
                    // Adding Contracted Budgeted values
                    if(mapcontracted.containsKey(Date.Valueof(ar.get('PMD'))) != null) {  
                        if(mapcontracted.get(Date.Valueof(ar.get('PMD'))) != null) {
                            Date dateValc = Date.valueOf(ar.get('PMD'));
                            Integer monthc = dateValc.month() - 1;
                            Integer yearc = dateValc.year(); 
                            
                            Decimal tccd = mapcontracted.get(Date.Valueof(ar.get('PMD')));
                            Decimal tccd1 = tccd.divide(1, 2);
                            Decimal tccvalue = tccd1;
                            if(sumtccvalues == 0)
                                sumtccvalues = tccvalue;
                            else
                                sumtccvalues += tccvalue;                                    
                            if(Selectedview != null && Selectedview != '' && Selectedview.equalsIgnoreCase('Cumulative View')) {                    
                                tccstr = 'Date.UTC(' + yearc + ',' + monthc + '),' + sumtccvalues;
                                obj.column2 = sumtccvalues;
                            }
                            else {                  
                                tccstr = 'Date.UTC(' + yearc + ',' + monthc + '),' + tccvalue;
                                obj.column2 = tccvalue;
                            }                  
                            ContractedList.add(tccstr);
                        }                     
                    } 
                     
                    // Adding Actual values
                    if(mapActual.containsKey(Date.Valueof(ar.get('PMD'))) != null) {  
                        if(mapActual.get(Date.Valueof(ar.get('PMD'))) != null) {
                            Date dateVala = Date.valueOf(ar.get('PMD'));
                            Integer montha = dateVala.month() - 1;
                            Integer yeara = dateVala.year(); 
                            
                            Decimal tacd = mapActual.get(Date.Valueof(ar.get('PMD')));
                            Decimal tacd1 = tacd.divide(1, 2);
                            Decimal tacvalue = tacd1;                                
                            if(sumtacvalues == 0)
                                sumtacvalues = tacvalue;
                            else
                                sumtacvalues += tacvalue;
                            if(Selectedview != null && Selectedview != '' && Selectedview.equalsIgnoreCase('Cumulative View')) {                        
                                tacstr = 'Date.UTC(' + yeara + ',' + montha + '),' + sumtacvalues;
                                obj.column3 = sumtacvalues;
                            }
                            else {                        
                                tacstr = 'Date.UTC(' + yeara + ',' + montha + '),' + tacvalue;
                                obj.column3 = tacvalue;
                            }                         
                            ActualList.add(tacstr);    
                        }                     
                    }             
                     
                    // Adding Forecasted values
                    if(mapForecast.containsKey(Date.Valueof(ar.get('PMD'))) != null) {
                        if(mapForecast.get(Date.Valueof(ar.get('PMD'))) != null) {
                            Date dateValf = Date.valueOf(ar.get('PMD'));
                            Integer monthf = dateValf.month() - 1;
                            Integer yearf = dateValf.year(); 
                                                      
                            Decimal tfcd = mapForecast.get(Date.Valueof(ar.get('PMD')));
                            Decimal tfcd1 = tfcd.divide(1, 2);
                            Decimal tfcvalue = tfcd1;                                
                            if(sumtfcvalues == 0)
                                sumtfcvalues = tfcvalue;
                            else
                                sumtfcvalues += tfcvalue;                         
                            if(Selectedview != null && Selectedview != '' && Selectedview.equalsIgnoreCase('Cumulative View')){ 
                                 tfcstr = 'Date.UTC(' + yearf + ',' + monthf + '),' + sumtfcvalues;
                                 obj.column4 = sumtfcvalues;
                            }
                            else {                  
                                tfcstr = 'Date.UTC(' + yearf + ',' + monthf + '),' + tfcvalue;
                                obj.column4 = tfcvalue;                    
                            }           
                            ForecastList.add(tfcstr);      
                        }                     
                    }             
                    HoursTableList.add(obj); 
                }
            }  
        } else {
        	HoursTableList = new List<GridDataTable>();
        }   
    }
      
    public void getgraphs() {     
        if(SelectedType.equalsIgnoreCase('Hours')) {        
            if(Selectedview.equalsIgnoreCase('Cumulative View')) {            
                SubTitleGraph = 'Hours in Cumulative View';
            } else {            
                SubTitleGraph = 'Hours in Monthly View';
            }
            type = 'Hours';
            SearchgraphBasedOnHours();
        } else {
            if(SelectedType.equalsIgnoreCase('Cost')) {        
                if(Selectedview.equalsIgnoreCase('Cumulative View')) {            
                    SubTitleGraph = 'Cost in Cumulative View';
                } else {            
                    SubTitleGraph = 'Cost in Monthly View';
                }
                type = 'Dollars($)';
                SearchgraphBasedOncost();
            }
        }
    }
    
    // Wrapper class to hold the data to display in pageblock tables
    public class GridDataTable {    
        public String  column1{get;set;}
        public Decimal column2{get;set;} 
        public Decimal column3{get;set;}
        public Decimal column4{get;set;}        
        
        GridDataTable() {         
            column2 = 0;
            column3 = 0;
            column4 = 0;   
        }        
    }       
}