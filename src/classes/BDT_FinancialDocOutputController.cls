public with sharing class BDT_FinancialDocOutputController {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Financial Documents Output
	public FinancialDocument__c 	financialDocument 	 {get;set;}
	public String 					financialDocumentID	 {get;set;} 
	public Boolean					showPage 			 {get;set;}
	public string					selectedFilterMethod {get;set;}
	public List<Attachment>			pdfAttachments 		 {get;set;}
	
	/* Class constructor
	 * @author	Dimitris Sgourdos, Maurice Kremer
	 * @version	16-Sep-2013
	 */	
	public BDT_FinancialDocOutputController() {
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Financial Documents Output');
		showPage = true;
		financialDocumentID = system.currentPageReference().getParameters().get('financialDocumentID'); 	
		if( String.isBlank(financialDocumentID) ) {
			errorOnReadingFinancialDocument();
         	return;
		} else {
			financialDocument = FinancialDocumentService.getFinancialDocumentById(financialDocumentID);
			if( String.isBlank(financialDocument.Name) ) {
				errorOnReadingFinancialDocument();
				return;
			}
			selectedFilterMethod = 'A4';
			pdfAttachments = BDT_DC_FinancialDocument.readPdfAttachments(financialDocumentID);
		}
	} 
	
	
	/**
	*	Give an error message in case the financial document is not found.
	*	@author   Dimitrios Sgourdos
	*	@version  16-Sep-2013
	*/
	private void errorOnReadingFinancialDocument() {
		showPage = false;
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected financial document.Please, select a document and reload the page.');
		ApexPages.addMessage(msg);
	}
	
	 
	/**
	*	Create the radio buttons options for selecting the paper size of the selected financial document
	*	@author   Dimitrios Sgourdos
	*	@version  09-Sep-2013
	*	@return   The radio buttons options
	*/ 
	public list<SelectOption> getFilterOptions() {
		list<SelectOption> FilterOptions = new list<SelectOption>();
		FilterOptions.add(new SelectOption('A4','A4'));
		FilterOptions.add(new SelectOption('Letter','Letter'));
		return FilterOptions;
	}
	
	
	/**
	*	Hide the content of the page and let only the progress bullets to be visible.
	*	@author   Dimitrios Sgourdos
	*	@version  09-Sep-2013
	*/
	public void closePage() {
		showPage = false;
	}
	
	
	/* Start the procedure for generating a pdf for the current financial document and store it as an attachment
	 * @author	Sgourdos Dimitrios, Maurice Kremer
	 * @version	16-Sep-2013
	 */
	//public PageReference showDonwloadPage() { 
	public void showDonwloadPage() {
		String commentValue = 'Create the document pdf';
		String changeDescription = '';
		String totalValueString = '';
		if(financialDocument.TotalValue__c != null) {
			totalValueString = financialDocument.TotalValue__c;
		}
		FinancialDocumentHistoryService.saveHistoryObject(financialDocument.id, commentValue, changeDescription, totalValueString, 'Output');
		generateAndStorePdf();
		//PageReference pr = new PageReference(System.Page.bdt_financialdocdownload.getUrl());
		//pr.getParameters().put('selectedPaperSize',selectedFilterMethod); 
		//pr.getParameters().put('financialDocumentId',financialDocumentId); 		
		//pr.setRedirect(true);
		//return pr;
	}
	
	
	/* Generate a pdf for the current financial document and store it as an attachment
	 * @author	Maurice Kremer, Sgourdos Dimitrios
	 * @version	16-Sep-2013
	 */
	public void generateAndStorePdf () {
		// Take the proper page depends on the type of the document
		PageReference pdfDownloadPage;
		if(financialDocument.DocumentType__c == 'Proposal') {
			pdfDownloadPage = new PageReference(System.Page.bdt_financialdocdownload.getUrl());
		} else {
			pdfDownloadPage = new PageReference(System.Page.bdt_financialdocdownloadco.getUrl());
		}
		pdfDownloadPage.getParameters().put('selectedPaperSize',selectedFilterMethod); 
		pdfDownloadPage.getParameters().put('financialDocumentId',financialDocumentId);
		// Create the content of the attachment
		Blob body;
		body = pdfDownloadPage.getContent();
		// Delete previous attachments and save the new one
		BDT_DC_FinancialDocument.deletePdfAttachments(financialDocument.Id);
		Attachment pdfAttachment = new Attachment();
		pdfAttachment.Body = body;
		pdfAttachment.Name = financialDocument.DocumentType__c + '.pdf';
		pdfAttachment.IsPrivate = false;
		pdfAttachment.ParentId = financialDocument.Id;
		insert pdfAttachment;
		// Read the document attachments (In that case it wil be one as each attatchment ovewrites the previous one)
		pdfAttachments = BDT_DC_FinancialDocument.readPdfAttachments(financialDocument.Id);
	}
}