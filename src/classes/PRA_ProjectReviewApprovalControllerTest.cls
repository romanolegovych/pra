@isTest
private class PRA_ProjectReviewApprovalControllerTest {
    private static Integer PROJECT_LENGHT = 3;
    static void initTestData (String projectName){
   
        Operational_Area__c oa = new Operational_Area__c(Name = 'Project Management', Description__c = 'Project Management');
        insert oa;
        
        Project_Client_Region__c projectClientRegionObj=new Project_Client_Region__c(Name='TestRegion');
        insert projectClientRegionObj;
        
        Date start_date = Date.today();
        Date end_date = Date.today();
        end_date = end_date.addMonths( PROJECT_LENGHT );
        
        Client_Project__c cp = new Client_Project__c( contracted_start_date__c = Date.today().addMonths(-2).toStartOfMonth(), contracted_end_date__c = end_date );
        if(projectName!='myT3stPr0j3ct')
        cp.Estimated_End_Date__c = end_date ;
        
        cp.PRA_Project_ID__C = projectName;
        cp.name = projectName;
        cp.Discount_Type__c='%';
        cp.Discount__c=5;
        insert cp;
        
        
        
        Task_Group__c tg = new Task_Group__c( Operational_Area__c = oa.id );
        insert tg;
        
        Client_Task__c ct = new Client_Task__c( project__c = cp.id, task_group__c = tg.id );
        ct.PRA_Client_Task_ID__C = 'asdfasdfxxx';
        ct.Project_Region__c=projectClientRegionObj.id;
        ct.Client_Unit_Number__c='123';
        ct.Start_Date__c = start_date;
        ct.Original_End_date__c = end_date;
        ct.LA_Total_Forecast_Units__c=3;
        ct.LA_Total_Worked_Unit__c=4;
        ct.Contract_Value__c=4;
        
        ct.Forecast_Curve__c = 'Project Management';
        insert ct;
        
        Client_Task__c ct1 = new Client_Task__c( project__c = cp.id, task_group__c = tg.id );
        ct1.PRA_Client_Task_ID__C = 'asdfasdafxxx';
        ct1.Project_Region__c=projectClientRegionObj.id;
        ct.Client_Unit_Number__c='345';
        ct1.Start_Date__c = start_date;
        ct1.Original_End_date__c = end_date;
        ct1.LA_Total_Forecast_Units__c=3;
        ct1.LA_Total_Worked_Unit__c=4;
        ct1.Contract_Value__c=4;
        
        ct1.Forecast_Curve__c = 'Project change Management';
        insert ct1;
        
        
        Unit_Effort__c ef1 = new Unit_Effort__c(month_applies_to__c = Date.today().addMonths(0).toStartOfMonth());
        ef1.Client_Task__c = ct.id;
        ef1.Forecast_Unit__c = 1;
        ef1.Forecast_Effort__c = 2;
        ef1.Worked_Unit__c=3;
        ef1.Total_Forecast_Cost__c=3;
        ef1.Unit_Effort_ID__c = 'asdfasdfassdf1'; 
        ef1.Is_Confirmed_Worked_Unit__c=true;      
        insert ef1;
        
        Unit_Effort__c ef2 = new Unit_Effort__c(month_applies_to__c = Date.today().addMonths(1).toStartOfMonth());
        ef2.Client_Task__c = ct.id;
        ef2.Forecast_Unit__c = 2;
        ef2.Forecast_Effort__c = 4;
        ef2.Worked_Unit__c=3; 
        ef2.Total_Forecast_Cost__c=3;      
        ef2.Unit_Effort_ID__c = 'asdfasdfassdf2';
        ef2.Is_Confirmed_Worked_Unit__c=true;           
        insert ef2;
        
         Unit_Effort__c ef3 = new Unit_Effort__c(month_applies_to__c = Date.today().addMonths(2).toStartOfMonth());
        ef3.Client_Task__c = ct.id;
        ef3.Forecast_Unit__c = 3;
        ef3.Forecast_Effort__c = 6; 
        ef3.Worked_Unit__c=3;  
        ef3.Total_Forecast_Cost__c=3;      
        ef3.Unit_Effort_ID__c = 'asdfasdfassdf3';
        ef3.Is_Confirmed_Worked_Unit__c=true;          
        insert ef3;
       
        
        Unit_Effort__c ef4 = new Unit_Effort__c(month_applies_to__c = Date.today().addMonths(0).toStartOfMonth());
        ef4.Client_Task__c = ct.id;
        ef4.Forecast_Unit__c = 1;
        ef4.Forecast_Effort__c = 2;
        ef4.Contracted_BDG_Unit__c=4;
        ef4.Total_Forecast_Cost__c=3;
         ef4.Worked_Unit__c=3; 
        ef4.Unit_Effort_ID__c = 'atttsdfasdfassdf1'; 
        ef4.Is_Confirmed_Worked_Unit__c=true;       
        insert ef4;
        
        Unit_Effort__c ef5 = new Unit_Effort__c(month_applies_to__c = Date.today().addMonths(-2).toStartOfMonth());
        ef5.Client_Task__c = ct.id;
        ef5.Forecast_Unit__c = 1;
        ef5.Forecast_Effort__c = 2;
        ef5.Worked_Unit__c=3;
        ef5.Total_Forecast_Cost__c=3;
        ef5.Contracted_BDG_Unit__c=4;
        ef5.Is_Historical__c=true;
        ef5.Unit_Effort_ID__c = 'asdfasdfassdf5';  
        ef5.Is_Confirmed_Worked_Unit__c=true;      
        insert ef5;
        
         // make 2 imaginary buff codes
        BUF_Code__c bc1 = new BUF_Code__c();
        insert bc1;
        
        BUF_Code__c bc2 = new BUF_Code__c();
        insert bc2;
        
        
        Hour_EffortRatio__c her1 = new Hour_EffortRatio__c();
        her1.Unit_Effort__c = ef1.id;
        her1.BUF_Code__c = bc1.id;
        her1.Hour_ER_ID__c = 'asdfasdfassdf4';
        her1.Forecast_ER__c= 0.2; 
        her1.Worked_Hours__c=5;       
        insert her1;
        
        Hour_EffortRatio__c her2 = new Hour_EffortRatio__c();
        her2.Unit_Effort__c = ef1.id;
        her2.BUF_Code__c = bc2.id;
        her2.Hour_ER_ID__c= 'asdfasdfassdf5';
        her2.Forecast_ER__c= 0.4;
        //her2.Worked_Hours__c=5;        
        insert her2;
        
        Hour_EffortRatio__c her3 = new Hour_EffortRatio__c();
        her3.Unit_Effort__c = ef1.id;
        her3.BUF_Code__c = bc2.id;
        her3.Hour_ER_ID__c= 'asdfasdfassdf55';
        her3.Forecast_ER__c= 0.4;
        her3.Worked_Hours__c=5;
        her3.Cost_Rate__c=4;
        her3.Exchange_Rate__c=4;        
        insert her3;
        
        
        Hour_EffortRatio__c her4 = new Hour_EffortRatio__c();
        her4.Unit_Effort__c = ef5.id;
        her4.BUF_Code__c = bc2.id;
        her4.Hour_ER_ID__c= 'asdfasdfassdf44';
        her4.Forecast_ER__c= 1;
        her4.Worked_Hours__c=5;
        her4.Cost_Rate__c=4;
        her4.Exchange_Rate__c=4;        
        insert her4;
        
        Hour_EffortRatio__c her5 = new Hour_EffortRatio__c();
        her5.Unit_Effort__c = ef4.id;
        her5.BUF_Code__c = bc2.id;
        her5.Hour_ER_ID__c= 'asdfasdfassdf88';
        her5.Forecast_ER__c= 1;
        her5.Worked_Hours__c=5;
        her5.Cost_Rate__c=4;
        her5.Exchange_Rate__c=4;        
        insert her5;        
        
        Hour_EffortRatio__c her6 = new Hour_EffortRatio__c();
        her6.Unit_Effort__c = ef2.id;
        her6.BUF_Code__c = bc2.id;
        her6.Hour_ER_ID__c= 'erforef2';
        her6.Forecast_ER__c= 1;
        her6.Worked_Hours__c=5;
        her6.Cost_Rate__c=4;
        her6.Exchange_Rate__c=4;        
        insert her6;        

        Hour_EffortRatio__c her7 = new Hour_EffortRatio__c();
        her7.Unit_Effort__c = ef3.id;
        her7.BUF_Code__c = bc2.id;
        her7.Hour_ER_ID__c= 'erforef3';
        her7.Forecast_ER__c= 1;
        her7.Worked_Hours__c=5;
        her7.Cost_Rate__c=4;
        her7.Exchange_Rate__c=4;        
        insert her7;        

        Group__c gc=new Group__c();
        gc.name='Group 1';
        gc.client_project__c=cp.id;
        insert gc;
        
        Group_client_tasks__c gct=new Group_client_tasks__c();
        gct.client_task__c=ct.id;
        gct.Group__c=gc.id;
        insert gct;
          
   }
   //prepare a test data for project with contract end date
   static testMethod void shouldretrieveDemographics() {
      
      
        String testProjectName = 'myT3stPr0j3ct';           // should be something that's probably unique
        initTestData(testProjectName);        
        PRA_ProjectReviewApprovalController projectRevAppCtrl = new PRA_ProjectReviewApprovalController();
        projectRevAppCtrl.projectText = testProjectName;
        Test.startTest();
        Client_Project__c cp;
        cp=[select id,Name from Client_Project__c where name=:testProjectName];
        Date thismonth = System.today().toStartOfMonth();
        Monthly_Approval__c mp =new Monthly_Approval__c();
        mp.Month_Approval_Applies_To__c=thismonth;
        String  month=(string.valueof(thismonth.month()).length()==1)?string.valueof('0'+thismonth.month()):string.valueof(thismonth.month());                
        String madate=string.valueof(thismonth.year())+string.valueof(month)+'01';
        system.debug('------month--madate----'+month+madate);
        mp.MA_Unique_ID__c = testProjectName+':'+madate;
        mp.Client_Project__c=cp.id;
        insert mp;
        
        PRA_ProjectReviewApprovalController.ProjectWBS(cp.Name);
        projectRevAppCtrl.MainSearch();
        projectRevAppCtrl.exportExcel();
        projectRevAppCtrl.exportGraphExcel();
        projectRevAppCtrl.reset();
        Test.stopTest();   
             
                
    }
     static testMethod void shouldApproveProject() {
        
        //initTestData( testProjectName );
        Profile pr = [select id, name from Profile where name = 'Project Analyst'];
        User testUser = new User(alias='TestUser',Email='testPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='PZ', 
                            LocaleSidKey='en_US', ProfileId=pr.id, TimeZoneSidKey='America/New_York', Username='Test.PZ@praintl.com.unitTestPMConsole');
        insert testUser;
         
         PMC_ProfileGroup__c pg= new PMC_ProfileGroup__c ();
         pg.name='Project Analyst';
         pg.profile__c='Project Analyst';
         insert pg;
        // make fake data
        // client project
       /*
       
        Date start_date = Date.today();
        Date end_date = Date.today();
        end_date = end_date.addMonths( 3 );
        Client_Project__c cp = new Client_Project__c( contracted_start_date__c = start_date, contracted_end_date__c = end_date,
                                                        Global_Project_Manager_Director__c = testUser.id );
        cp.PRA_Project_ID__C = 'asdfasdfassdf';
        cp.name = testProjectName;
        
        */
        
        Client_Project__c cp;
        //String testProjectName = 'Th1z1zmYT3stPr0j3ct';
        String testProjectName = 'asdfasdfassdf2';
        initTestData(testProjectName); 
        cp=[select id,Global_Project_Manager_Director__c,LA_Over_Under_Contracted_Cost__c from Client_Project__c where name=:testProjectName];
        
        
        cp.Global_Project_Manager_Director__c = testUser.id;
        cp.Director_of_Project_Delivery__c=testUser.id;
        cp.Manager_Director_of_Operations_Finance__c=testUser.id;
        cp.General_Partner__c=testUser.id;
        cp.Executive_Vice_President__c=testUser.id;
        cp.LA_Over_Under_Contracted_Cost__c = 1;
        cp.Manager_of_Operations_Finance__c=testUser.id;
        cp.Global_Project_Analyst__c=testUser.id;
        
        update cp;
              
        
        Date monthAppliesTo = Date.today().toStartOfMonth();
        
        PRA_ProjectReviewApprovalController projectRevAppCtrl = new PRA_ProjectReviewApprovalController();
        projectRevAppCtrl.projectText = testProjectName;
        
        System.runAs(testUser){
            // PM Approved worked units
            projectRevAppCtrl.MainSearch();
            
            // check status of approval indicators (icons)
            system.assertEquals('green',    projectRevAppCtrl.workedUnitsConfirmedStatus);
            system.assertEquals('red',      projectRevAppCtrl.workedUnitsApprovedStatus);
            system.assertEquals('red',      projectRevAppCtrl.forecastReleasedStatus);
            system.assertEquals('red',      projectRevAppCtrl.forecastApprovedStatus);
            
            // check availblity of approval buttons
            system.assertEquals(true,   projectRevAppCtrl.getShowPMblock()); // PM section visable
            system.assertEquals(false,  projectRevAppCtrl.getDisablePMWorkedUnitApproval()); // Worked units approval
            system.assertEquals(true,   projectRevAppCtrl.getDisablePZForecastPMApproval()); // PZ Forecast Release
            system.assertEquals(true,   projectRevAppCtrl.getDisablePMForecastApproval()); // Forecast approval
            
         
            // get number of batch processes
            Integer ExpectedNoOfQueuedBatches = [select count() from PRA_Batch_Queue__c];
            
            projectRevAppCtrl.PMWorkedApprovalClicked();
            projectRevAppCtrl.GetProjectStatus();
            projectRevAppCtrl.getShowPZblock();
            Monthly_Approval__c approval = [select Id,Month_Approval_Applies_To__c, Approval_Date__c, Client_Project__c,DPD_Approval_Date__c, DPD_approved_forecast__c,EVP_Approval_Date__c,DOF_Approval_Date__c,EVP_Approved__c,DOF_Approved__c, Is_approval_complete__c, 
                                                        Is_Approved_DPD_forecast__c,Is_Approved_EVP__c,Is_Approved_DOF__c,Is_Approved_PM_Forecast__c,Is_Approved_PM_Worked_Units__c,Is_Approved_PZ_released_forecast__c,
                                                        Need_DOF_EVP_Approval__c,Need_DPD_Approval__c,PM_Forecast_App_Date__c,PM_WU_Approval_Date__c, PZ_Release_Date__c,PM_Approved_Forecast__c, PM_Approved_Worked_Units__c, PZ_released_forecast__c,PZ_Released_Forecast__r.Email
                                                    from Monthly_Approval__c where client_project__c  = :cp.id and Month_Approval_Applies_To__c = :monthAppliesTo];
           
           system.assertEquals(null,                    approval.PM_Approved_Forecast__c);
           system.assertEquals(Userinfo.getUserId(),    approval.PM_Approved_Worked_Units__c);
           system.assertEquals(null,                    approval.PZ_released_forecast__c);
           
           // check status of approval indicators (icons)
           system.assertEquals('green', projectRevAppCtrl.workedUnitsConfirmedStatus);
           system.assertEquals('green', projectRevAppCtrl.workedUnitsApprovedStatus);
           system.assertEquals('red',       projectRevAppCtrl.forecastReleasedStatus);
           system.assertEquals('red',      projectRevAppCtrl.forecastApprovedStatus);
            
           // check availblity of approval buttons
           system.assertEquals(true,    projectRevAppCtrl.getShowPMblock()); // PM section visable
           system.assertEquals(true,    projectRevAppCtrl.getDisablePMWorkedUnitApproval()); // Worked units approval
           system.assertEquals(false,   projectRevAppCtrl.getDisablePZForecastPMApproval()); // PZ Forecast Release
           system.assertEquals(true,    projectRevAppCtrl.getDisablePMForecastApproval()); // Forecast approval
           
          
           // PZ release forecast
           projectRevAppCtrl.PZForecastReleaseClicked();
           projectRevAppCtrl.GetProjectStatus();
           approval = [select Id,Month_Approval_Applies_To__c, Approval_Date__c, Client_Project__c,DPD_Approval_Date__c, DPD_approved_forecast__c,EVP_Approval_Date__c,DOF_Approval_Date__c,EVP_Approved__c,DOF_Approved__c, Is_approval_complete__c, 
                                                        Is_Approved_DPD_forecast__c,Is_Approved_EVP__c,Is_Approved_DOF__c,Is_Approved_PM_Forecast__c,Is_Approved_PM_Worked_Units__c,Is_Approved_PZ_released_forecast__c,
                                                        Need_DOF_EVP_Approval__c,Need_DPD_Approval__c,PM_Forecast_App_Date__c,PM_WU_Approval_Date__c, PZ_Release_Date__c,PM_Approved_Forecast__c, PM_Approved_Worked_Units__c, PZ_released_forecast__c,PZ_Released_Forecast__r.Email
                                                    from Monthly_Approval__c where client_project__c  = :cp.id and Month_Approval_Applies_To__c = :monthAppliesTo];
           
           system.assertEquals(null,                    approval.PM_Approved_Forecast__c);
           system.assertEquals(Userinfo.getUserId(),    approval.PM_Approved_Worked_Units__c);
           system.assertEquals(Userinfo.getUserId(),    approval.PZ_released_forecast__c);
           
            // check status of approval indicators (icons)
           system.assertEquals('green', projectRevAppCtrl.workedUnitsConfirmedStatus);
           system.assertEquals('green',     projectRevAppCtrl.workedUnitsApprovedStatus);
           system.assertEquals('green',     projectRevAppCtrl.forecastReleasedStatus);
           system.assertEquals('red',       projectRevAppCtrl.forecastApprovedStatus);
            
            // check availblity of approval buttons
           system.assertEquals(true,    projectRevAppCtrl.getShowPMblock()); // PM section visable
           system.assertEquals(true,    projectRevAppCtrl.getDisablePMWorkedUnitApproval()); // Worked units approval
           system.assertEquals(true,    projectRevAppCtrl.getDisablePZForecastPMApproval()); // PZ Forecast Release
           system.assertEquals(false,   projectRevAppCtrl.getDisablePMForecastApproval()); // Forecast approval
          
          
          
           // PM approval
           projectRevAppCtrl.PMForecastApprovalClicked();
           projectRevAppCtrl.GetProjectStatus();
           approval = [select Id,Month_Approval_Applies_To__c, Approval_Date__c, Client_Project__c,DPD_Approval_Date__c, DPD_approved_forecast__c,EVP_Approval_Date__c,DOF_Approval_Date__c,EVP_Approved__c,DOF_Approved__c, Is_approval_complete__c, 
                                                        Is_Approved_DPD_forecast__c,Is_Approved_EVP__c,Is_Approved_DOF__c,Is_Approved_PM_Forecast__c,Is_Approved_PM_Worked_Units__c,Is_Approved_PZ_released_forecast__c,
                                                        Need_DOF_EVP_Approval__c,Need_DPD_Approval__c,PM_Forecast_App_Date__c,PM_WU_Approval_Date__c, PZ_Release_Date__c,PM_Approved_Forecast__c, PM_Approved_Worked_Units__c, PZ_released_forecast__c,PZ_Released_Forecast__r.Email
                                                    from Monthly_Approval__c where client_project__c  = :cp.id and Month_Approval_Applies_To__c = :monthAppliesTo];
           
           system.assertEquals(Userinfo.getUserId(),    approval.PM_Approved_Forecast__c);
           system.assertEquals(Userinfo.getUserId(),    approval.PM_Approved_Worked_Units__c);
           system.assertEquals(Userinfo.getUserId(),    approval.PZ_released_forecast__c);
           
            // check status of approval indicators (icons)
           system.assertEquals('green', projectRevAppCtrl.workedUnitsConfirmedStatus);
           system.assertEquals('green', projectRevAppCtrl.workedUnitsApprovedStatus);
           system.assertEquals('green', projectRevAppCtrl.forecastReleasedStatus);
           system.assertEquals('green', projectRevAppCtrl.forecastApprovedStatus);
            
            // check availblity of approval buttons
           system.assertEquals(true,    projectRevAppCtrl.getShowPMblock()); // PM section visable
           system.assertEquals(true,    projectRevAppCtrl.getDisablePMWorkedUnitApproval()); // Worked units approval
           system.assertEquals(true,    projectRevAppCtrl.getDisablePZForecastPMApproval()); // PZ Forecast Release
           system.assertEquals(true,    projectRevAppCtrl.getDisablePMForecastApproval()); // Forecast approval
         
          /*  
          // DPD Rejection
          projectRevAppCtrl.DPDForecastRejectClicked();          
          projectRevAppCtrl.PZForecastReleaseClicked();           
          projectRevAppCtrl.PMForecastApprovalClicked();
          projectRevAppCtrl.DPDForecastEscalateClicked();          
          projectRevAppCtrl.EVPDOFForecastRejectClicked();
          projectRevAppCtrl.PZForecastReleaseClicked();   
          //cp.Cost_Variance_From_LA__c = 2500;
          
          Client_Project__c cp1=[select id,Global_Project_Manager_Director__c,LA_Over_Under_Contracted_Cost__c from Client_Project__c where name=:testProjectName];
          cp1.LA_Over_Under_Contracted_Cost__c = 3000;
        
          update cp1; 
          //pc.Sub_Type__c='lastApprovedCost';
          //pc.Value__c=-4;           
          //update pc;      
          projectRevAppCtrl.PMForecastApprovalClicked();
          */
        
          // check that project was locked
          // Client_Project__c project = [select Id, Is_Project_Locked__c from Client_Project__c where id = :cp.id];
          //system.assertEquals(true, project.Is_Project_Locked__c);
           
          // check that 5 new batch processes had been added to queue
          //Integer noOfQueuedBatches = [select count() from PRA_Batch_Queue__c];
          //system.assertEquals(expectedNoOfQueuedBatches + 5, noOfQueuedBatches);
          // projectRevAppCtrl.EVPDOFForecastApprovalClicked();
        }
       
    }
    
     static testMethod void shouldApproveProjectdpd() {
        
        //initTestData( testProjectName );
        Profile pr = [select id, name from Profile where name = 'Project Analyst'];
        User testUser = new User(alias='TestUser',Email='testPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='PZ', 
                            LocaleSidKey='en_US', ProfileId=pr.id, TimeZoneSidKey='America/New_York', Username='Test.PZ@praintl.com.unitTestPMConsole');
        insert testUser;
         
        PMC_ProfileGroup__c pg= new PMC_ProfileGroup__c ();
        pg.name='Project Analyst';
        pg.profile__c='Project Analyst';
        insert pg;
        
        Client_Project__c cp;
        //String testProjectName = 'Th1z1zmYT3stPr0j3ct';
        String testProjectName = 'asdfasdfassdf2';
        initTestData(testProjectName); 
        cp=[select id,Global_Project_Manager_Director__c,LA_Over_Under_Contracted_Cost__c from Client_Project__c where name=:testProjectName];
        
        
        cp.Global_Project_Manager_Director__c = testUser.id;
        cp.Director_of_Project_Delivery__c=testUser.id;
        cp.Manager_Director_of_Operations_Finance__c=testUser.id;
        cp.General_Partner__c=testUser.id;
        cp.Executive_Vice_President__c=testUser.id;
        cp.LA_Over_Under_Contracted_Cost__c = 1;
        cp.Manager_of_Operations_Finance__c=testUser.id;
        cp.Global_Project_Analyst__c=testUser.id;
        
        update cp;
              
        
        Date monthAppliesTo = Date.today().toStartOfMonth();
        
        PRA_ProjectReviewApprovalController projectRevAppCtrl = new PRA_ProjectReviewApprovalController();
        projectRevAppCtrl.projectText = testProjectName;
        
        System.runAs(testUser){
            projectRevAppCtrl.MainSearch();
            Monthly_Approval__c approval = [select Id,Month_Approval_Applies_To__c, Approval_Date__c, Client_Project__c,DPD_Approval_Date__c, DPD_approved_forecast__c,EVP_Approval_Date__c,DOF_Approval_Date__c,EVP_Approved__c,DOF_Approved__c, Is_approval_complete__c, 
                                                        Is_Approved_DPD_forecast__c,Is_Approved_EVP__c,Is_Approved_DOF__c,Is_Approved_PM_Forecast__c,Is_Approved_PM_Worked_Units__c,Is_Approved_PZ_released_forecast__c,
                                                        Need_DOF_EVP_Approval__c,Need_DPD_Approval__c,PM_Forecast_App_Date__c,PM_WU_Approval_Date__c, PZ_Release_Date__c,PM_Approved_Forecast__c, PM_Approved_Worked_Units__c, PZ_released_forecast__c,PZ_Released_Forecast__r.Email
                                                    from Monthly_Approval__c where client_project__c  = :cp.id and Month_Approval_Applies_To__c = :monthAppliesTo];
        
            approval.PZ_Released_Forecast__c=  testUser.id; 
            update approval;
            projectRevAppCtrl.DPDForecastRejectClicked();
            approval.PM_Approved_Worked_Units__c=testUser.id; 
            update approval;
            projectRevAppCtrl.PZForecastReleaseClicked();                  
            projectRevAppCtrl.PMForecastApprovalClicked();          
            projectRevAppCtrl.DPDForecastEscalateClicked();          
            projectRevAppCtrl.EVPDOFForecastRejectClicked();
            projectRevAppCtrl.PZForecastReleaseClicked(); 
         
            Client_Project__c cp1=[select id,Global_Project_Manager_Director__c,LA_Over_Under_Contracted_Cost__c from Client_Project__c where name=:testProjectName];
            cp1.LA_Over_Under_Contracted_Cost__c = 3000;        
            update cp1; 
          
            projectRevAppCtrl.PMForecastApprovalClicked();
        }
        
    }
    
    static testMethod void shouldApproveSecondProject() {
    
       
        Profile pr = [select id, name from Profile where name = 'Project Analyst'];
        User testUser = new User(alias='TestUser',Email='testPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='PZ', 
                            LocaleSidKey='en_US', ProfileId=pr.id, TimeZoneSidKey='America/New_York', Username='Test.PZ@praintl.com.unitTestPMConsole');
        insert testUser;
        
        Client_Project__c cp;
        //String testProjectName = 'Th1z1zmYT3stPr0j3ct';
        String testProjectName = '2asdfasdfassdf2';
        initTestData(testProjectName); 
        cp=[select id,Global_Project_Manager_Director__c,LA_Over_Under_Contracted_Cost__c from Client_Project__c where name=:testProjectName];
        
        
        cp.Global_Project_Manager_Director__c = testUser.id;
        cp.Director_of_Project_Delivery__c=testUser.id;
        cp.Manager_Director_of_Operations_Finance__c=testUser.id;
        cp.General_Partner__c=testUser.id;
        cp.Executive_Vice_President__c=testUser.id;
        cp.LA_Over_Under_Contracted_Cost__c = 1;
        
        update cp;
        
        Date monthAppliesTo = Date.today().toStartOfMonth();
        
        PRA_ProjectReviewApprovalController projectRevAppCtrl = new PRA_ProjectReviewApprovalController();
        projectRevAppCtrl.projectText = testProjectName;
                
        System.runAs(testUser){
            // PM Approved worked units
            projectRevAppCtrl.MainSearch();
            
             // get number of batch processes
            Integer ExpectedNoOfQueuedBatches = [select count() from PRA_Batch_Queue__c];
            
            projectRevAppCtrl.PMWorkedApprovalClicked();
            projectRevAppCtrl.GetProjectStatus();
            projectRevAppCtrl.getShowPZblock();            
            
             // PZ release forecast
            projectRevAppCtrl.PZForecastReleaseClicked();
            projectRevAppCtrl.GetProjectStatus();            
            
            // PM approval
            projectRevAppCtrl.PMForecastApprovalClicked();
            projectRevAppCtrl.GetProjectStatus();
            
            // DPD Approval
            projectRevAppCtrl.DPDForecastApprovalClicked();            
        }  
        
    
    }
    
    static testMethod void shouldApproveThirdProject() {
    
       
        Profile pr = [select id, name from Profile where name = 'Project Analyst'];
        User testUser = new User(alias='TestUser',Email='testPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='PZ', 
                            LocaleSidKey='en_US', ProfileId=pr.id, TimeZoneSidKey='America/New_York', Username='Test.PZ@praintl.com.unitTestPMConsole');
        insert testUser;
        
        Client_Project__c cp;
        //String testProjectName = 'Th1z1zmYT3stPr0j3ct';
        String testProjectName = '3asdfasdfassdf3';
        initTestData(testProjectName); 
        cp=[select id,Global_Project_Manager_Director__c,LA_Over_Under_Contracted_Cost__c from Client_Project__c where name=:testProjectName];
        
        
        cp.Global_Project_Manager_Director__c = testUser.id;
        cp.Director_of_Project_Delivery__c=testUser.id;
        cp.Manager_Director_of_Operations_Finance__c=testUser.id;
        cp.General_Partner__c=testUser.id;
        cp.Executive_Vice_President__c=testUser.id;
        cp.LA_Over_Under_Contracted_Cost__c = 1;
        
        update cp;
        
        Date monthAppliesTo = Date.today().toStartOfMonth();
        
        PRA_ProjectReviewApprovalController projectRevAppCtrl = new PRA_ProjectReviewApprovalController();
        projectRevAppCtrl.projectText = testProjectName;
                
        System.runAs(testUser){
            // PM Approved worked units
            projectRevAppCtrl.MainSearch();
            
             // get number of batch processes
            Integer ExpectedNoOfQueuedBatches = [select count() from PRA_Batch_Queue__c];
            
            projectRevAppCtrl.PMWorkedApprovalClicked();
            projectRevAppCtrl.GetProjectStatus();
            projectRevAppCtrl.getShowPZblock();            
            
             // PZ release forecast
             
           
            projectRevAppCtrl.PZForecastReleaseClicked();
            projectRevAppCtrl.GetProjectStatus();            
            
            // PM approval
            projectRevAppCtrl.PMForecastApprovalClicked();
            projectRevAppCtrl.GetProjectStatus();
            
            // DPD Approval
            projectRevAppCtrl.EVPDOFForecastApprovalClicked();            
        }    
      
    }
}