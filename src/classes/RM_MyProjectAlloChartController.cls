public with sharing class RM_MyProjectAlloChartController {
public string selectedCountry{get; set;}
	public string nView{get; set;}

	public Integer n{get; set;}
	public Boolean bPrev{get; private set;}
	public Boolean bNext{get; private set;}
	
	private string projectRID;
	private string strProjectRoleData;
	private string strEmpList;
	private Integer nMax;
	private Integer emplSize;
	private Integer defaultView = 20;
	public RM_MyProjectAlloChartController(){
		projectRID = ApexPages.currentPage().getParameters().get('id');
		system.debug('-----projectRID------'+projectRID); 
		n = 1;
		
		bPrev = false;
		bNext = false;
		getRoleData();
		if (emplSize > defaultView){		
   			nView = string.valueOf(defaultView);
   			bNext = true;
   			nMax = Math.round(emplSize/decimal.valueOf(nView)+0.5);
   			system.debug('-----nMax------'+nMax + '--' +emplSize) ;
		}
   		else
   			nView = 'All';
	}
	
   	
   	
   	public string GetProjectRoleData(){
   		return strProjectRoleData;
   	}
   	public string  getEmplList(){
    	return strEmpList;
    }
    public list<SelectOption> getViewItems(){
   	
   	
   		return  RM_LookupDataAccess.getViewNumberItems(emplSize, 10);
   	}
     public PageReference ViewChange(){
    	if (nView != 'All'){
    		bNext = true;
    		n = 1;
    		
    		
    	}
    	else{
    		bPrev = false;
			bNext = false;
    	}
    	getRoleData();
    	
    	return null;
    }
    public PageReference Previous(){
    	if (n > 1)
    		n -= 1;
    	if (n > 1)
    		bPrev = true;
    	else
    		bPrev = false;
    	if (n < nMax)
    		bNext = true;
    	else
    		bNext = false;
    	getRoleData();
    	return null;
    }
    public PageReference Next(){
    	if (n < nMax)
    		n += 1;
    	if (n > 1)
    		bPrev = true;
    	else
    		bPrev = false;
    	if (n < nMax)
    		bNext = true;
    	else
    		bNext = false;
    	getRoleData();
    	return null;
    }
   	private void getRoleData(){
   		
   		strEmpList = '';
   		list<RM_AssignmentOp.ProjectAllocation> proAllList = RM_AssignmentOp.GetProjectAllocationList(projectRID, -3, -1, false);
   		system.debug('-----proAllList------'+proAllList);	
   		list <string> monthList = RM_Tools.GetMonthsList(-3, -1); 
   		emplSize = proAllList.size();
		 
		strProjectRoleData='';
		if (proAllList.size() > 0){
			integer nStart = 0;
	 	 	integer nEnd = proAllList.size()-1;
    	 	 	
	 	 	if (nView!= 'All' && nView != null){
    	 	 	nStart = Integer.valueof(nView)*(n-1);
    	 	 	
    	 	 	nEnd = nStart + Integer.valueOf(nView)-1;
    	 	 	if (nEnd > proAllList.size()-1)
    	 	 		nEnd = proAllList.size()-1;
	 	 	}	
	 	 	else if (nView == null){
	 	 		if (emplSize > defaultView ){
		 	 		nView = string.ValueOf(defaultView);
		 	 		nEnd = nStart + Integer.valueOf(nView)-1;
	 	 		}
	 	 	}
	 	 	
			for (Integer i = 0; i < monthList.size(); i++){
				strProjectRoleData += '{ name: \'Assigned ' + monthList[i] + '\', data:[';
				//for(RM_AssignmentOp.ProjectAllocation p:proAllList){
				for  (integer j = nStart; j <= nEnd; j++) {	
						
						if (i == 0){
							string empName = proAllList[j].emplName;
							if (proAllList[j].emplName.contains('\''))
								empName = proAllList[j].emplName.replace('\'', '\\');
							strEmpList += '\''+ empName + '\',';
						}
						if (proAllList[j].assignList[i] != '')					
							strProjectRoleData += proAllList[j].assignList[i] + ',';
						else 
							strProjectRoleData +=  '0,';					
				}		
				strProjectRoleData = strProjectRoleData.substring(0, strProjectRoleData.length()-1) + '], stack: \'Assigned\'},';
			}		
			
				
			
			for ( Integer i = 0; i < monthList.size(); i++){
				strProjectRoleData += '{ name: \'Actuals ' + monthList[i] + '\', data:[';
				//for(RM_AssignmentOp.ProjectAllocation p:proAllList){	
				for  (integer j = nStart; j <= nEnd; j++) {		
					
				
					if (proAllList[j].histList[i] != '')					
						strProjectRoleData += proAllList[j].histList[i] + ',';
					else 
						strProjectRoleData +=  '0,';					
				}
				strProjectRoleData = strProjectRoleData.substring(0, strProjectRoleData.length()-1) + '], stack: \'Actuals\'},';
			}	
			
				
			
			
		
			system.debug('-----strProjectRoleData------'+strProjectRoleData);
			system.debug('-----strEmpList------'+strEmpList);
			//remove last ,
			if (strProjectRoleData.length() > 0){
				strEmpList = strEmpList.substring(0, strEmpList.length()-1);
				strProjectRoleData = strProjectRoleData.substring(0, strProjectRoleData.length()-1);
			}
		}
   	}
}