/** Implements the test for the controller BDT_NewEditLabAnalysisToStudyController
 * @author	Dimitrios Sgourdos
 * @version	24-Oct-2013
 */
@isTest
private class BDT_NewEditLabAnalysisToStudyContrTest {
	
	/** The test is only for code coverage as in controller there are only calls to functions 
	 *	from other classes that are already tested.
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void myUnitTest() {
		// Create the controller
		PageReference pageRef = New PageReference(System.Page.BDT_NewEditLabAnalysisToStudy.getUrl());
		pageref.getParameters().put('editMode' , 'FALSE');
		Test.setCurrentPage(pageRef); 
		BDT_NewEditLabAnalysisToStudyController p = new BDT_NewEditLabAnalysisToStudyController();
		
		// Call functions for obtaining the code coverage
		p.showMoreDetailsOfMethod();
		p.closeMoreDetailsSection();
		p.requestSaveEdit();
		p.saveEdit();
		p.rowIndexDeletedCompound = 0;
		p.deleteSearchCompound();
		p.clearSearchOptions();
		p.searchLabMethods();
		p.closeResultsSection();
		p.cancelEdit();
		
		// Create new method, add it to the method-compounds to study assignment data and then delete it from the assignment data
		for(Integer i = 0; i<21; i++) {
			p.compoundItemsList.add(new LaboratoryMethodCompound__c());
		}
		p.requestNewMethod();
		p.createNewMethod();
		p.clearSearchOptions();
		p.createNewMethod();
		p.searchCriterialabMethod = new LaboratoryMethod__c (Species__c='Human', Matrix__c='Breast milk', AntiCoagulant__c='N/A', AnalyticalTechnique__c='ELISA', 
									  Detection__c='N/A', Department__c='SML', Location__c='PRA-US', Proprietary__c='N'	);
		p.compoundItemsList[0].Name = 'Test compound';
		p.createNewMethod();
		
		// Add the created method to the method-compounds to study assignment data and then delete it from the assignment data
		p.addingMethodId = p.searchCriterialabMethod.Id;
		p.addMethodToEditAssignmentsData();
		p.removeMethodIndex = 0;
		p.removeMethodFromEditMethodAndCompoundsContainer();	
	}
	
}