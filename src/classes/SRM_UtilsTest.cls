/**
* @author Ramya Shree Edara
* @date 21-Oct-2014
* @description this is test class for scenario related classes
*/
@isTest
public with sharing class SRM_UtilsTest{
    
    public static testMethod void testShowErrorMessage(){
        SRM_Utils.showErrorMessage('Error');
        SRM_Utils.showMessage('Success');
    }
    
    public static testMethod void testShowMessage(){
        SRM_Utils.showMessage('Success');
    }
    
    public static testMethod void testGetWeekStartDate(){
        Date weekStartDate = SRM_Utils.getWeekStartDate(Date.newInstance(2014, 11, 03));
        Date compareDate = Date.newInstance(2014, 11, 04).toStartOfWeek();
        Date nextDate = SRM_Utils.getWeekStartDate(Date.newInstance(2014, 11, 07));
        System.debug('>>> weekStartDate >>> '+weekStartDate+' >>> compareDate>>> '+compareDate+' >>> nextDate >>>'+nextDate);
        System.assertEquals(weekStartDate, compareDate.addDays(2));
        System.assertEquals(7, weekStartDate.daysBetween(nextDate));
        
    }
    
    public static testMethod void testGetNumberOfWeeks(){
        Date stdt = Date.newInstance(2014, 11, 03);
        Date endt = Date.newInstance(2014, 11, 04);
        Date cmdt = Date.newInstance(2014, 11, 10);
        Integer cnt = SRM_Utils.getNumberOfWeeks(stdt, endt);
        System.assertEquals(1, cnt);
        Integer cnt1 = SRM_Utils.getNumberOfWeeks(stdt, cmdt);
        System.assertEquals(2, cnt1);
    }
    
    public static testMethod void testGetAllFieldsFromObject(){
        String fields = SRM_Utils.getAllFieldsFromObject('SRM_Model__c');
        System.assertNotEquals(null, fields);
    }
    
    public static testMethod void testGetDate(){
        
        Date dt1 = SRM_Utils.getDate('7-Jan-2015');
        Date newDt1 = Date.newInstance(2015, 1, 7);
        System.assertEquals(dt1, newDt1);
        
        Date dt2 = SRM_Utils.getDate('7-Feb-2015');
        Date newDt2 = Date.newInstance(2015, 2, 7);
        System.assertEquals(dt2, newDt2);
        
        Date dt3 = SRM_Utils.getDate('7-Mar-2015');
        Date newDt3 = Date.newInstance(2015, 3, 7);
        System.assertEquals(dt3, newDt3);
        
        Date dt4 = SRM_Utils.getDate('7-Apr-2015');
        Date newDt4 = Date.newInstance(2015, 4, 7);
        System.assertEquals(dt4, newDt4);
        
        
        Date dt5 = SRM_Utils.getDate('7-May-2015');
        Date newDt5 = Date.newInstance(2015, 5, 7);
        System.assertEquals(dt5, newDt5);
        
        Date dt6 = SRM_Utils.getDate('7-Jun-2015');
        Date newDt6 = Date.newInstance(2015, 6, 7);
        System.assertEquals(dt6, newDt6);
        
        Date dt7 = SRM_Utils.getDate('7-Jul-2015');
        Date newDt7 = Date.newInstance(2015, 7, 7);
        System.assertEquals(dt7, newDt7);
        
        Date dt8 = SRM_Utils.getDate('7-Aug-2015');
        Date newDt8 = Date.newInstance(2015, 8, 7);
        System.assertEquals(dt8, newDt8);
        
        
        Date dt9 = SRM_Utils.getDate('7-Sep-2015');
        Date newDt9 = Date.newInstance(2015, 9, 7);
        System.assertEquals(dt9, newDt9);
        
        Date dt10 = SRM_Utils.getDate('7-Oct-2015');
        Date newDt10 = Date.newInstance(2015, 10, 7);
        System.assertEquals(dt10, newDt10);
        
        Date dt11 = SRM_Utils.getDate('7-Nov-2015');
        Date newDt11 = Date.newInstance(2015, 11, 7);
        System.assertEquals(dt11, newDt11);
        
        Date dt12 = SRM_Utils.getDate('7-Dec-2015');
        Date newDt12 = Date.newInstance(2015, 12, 7);
        System.assertEquals(dt12, newDt12);
        
    }
    
    public static testMethod void testGetDateInStringFormat(){
        String strDate = SRM_Utils.getDateInStringFormat(Date.Today());
        System.assertNotEquals(null, strDate);
    }
    
}