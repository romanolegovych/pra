global with sharing class PRA_RegionalContractValueController {
      
	// Expose PRA_ClientTaskSearchService properties as
	// belonging to the controller class.
	public String selectedProject {
		get { return ctSearch.selectedProject; }
		set { ctSearch.selectedProject = value;}
	}
	public List<String> selectedOperationalArea {
		get { return ctSearch.selectedOperationalAreas; }
		set { ctSearch.selectedOperationalAreas = value;}
	}
	public List<String> selectedGroup {
		get { return ctSearch.selectedGroups; }
		set { ctSearch.selectedGroups = value;}
	}
	public List<String> selectedTask {
		get { return ctSearch.selectedTasks; }
		set { ctSearch.selectedTasks = value;}
	}
	public List<String> selectedRegion {
		get { return ctSearch.selectedRegions; }
		set { ctSearch.selectedRegions = value;}
	}
	public List<SelectOption> regionOptions {
		get { return ctSearch.regionOptions; }
		set { ctSearch.regionOptions = value;}
	}
	public List<SelectOption> operationalAreaOptions {
		get { return ctSearch.operationalAreaOptions; }
		set { ctSearch.operationalAreaOptions = value;}
	}
	public List<SelectOption> groupOptions {
		get { return ctSearch.groupOptions; }
		set { ctSearch.groupOptions = value;}
	}
	public List<SelectOption> taskOptions {
		get { return ctSearch.taskOptions; }
		set { ctSearch.taskOptions = value;}
	}
    // Constant representing the select value option in the picklist
    public final String ST_PLACEHOLDER = 'Enter name or project ID....';    
    // Value selected by the user in the picklist
    public String selectedProjectLabel  {get;set;}
    public Date selectedProjectMigratedDate {get;set;}
    //Value to store ProjectID for VF
    public String selectedProjectName   {get;set;}        
    public Boolean blocked   {get;set;}    //block the edit button    
    // Picklist
    public List<SelectOption> projectOptions {get;set;}
    // Checkbox for unconfirmed units
    public Boolean unconfirmedOnly {get;set;}
    public Boolean confirmedOnly {get;set;} 
    //for RCV///////////////////////////////
    public Map<Id, RCVWrapper> mapRCVWrapper = new Map<Id, RCVWrapper>();
    public List<RCVWrapper> ListRCVWrapper {get;set;}   
    // Collapsed
    public Boolean isCollapsed {get;set;}
    public Boolean isFirstTime {get;set;}    
    public boolean showPageBlockTable{get;set;}
    public boolean showNoRecordsMessage{get;set;}    
      
    public PRA_ClientTaskSearchService ctSearch = new PRA_ClientTaskSearchService();
    public PRA_RegionalContractValueController() {             
      
        PRA_Utils.UserPreference userPref = PRA_Utils.retrieveUserPreference();
        if(userPref != null){
            selectedProject         = userPref.projectId;
            List<Client_Project__c> clientProjectsList = 
                [select name,Is_Project_Locked__c,migrated_date__c from Client_Project__c where id=:selectedProject and Load_Status__c != 'New'];
            if(clientProjectsList.size() == 1) {
                    selectedProjectLabel    = clientProjectsList.get(0).name;
                    selectedProjectName     = clientProjectsList.get(0).name;//Assigning for display in VF page
                    blocked                 = clientProjectsList.get(0).Is_Project_Locked__c;
                    selectedProjectMigratedDate = clientProjectsList.get(0).migrated_date__c;
                    selectedOperationalArea = userPref.operationalAreaId;
                    System.debug('selectedOperationalArea: '+selectedOperationalArea);
                    selectedRegion          = userPref.regionId;
                    selectedTask            = userPref.clientUnitId;
                    selectedGroup           = userPref.clientUnitGroupId;
                    unconfirmedOnly         = userPref.unconfirmedOnly;
                    confirmedOnly           = userPref.confirmedOnly;
                    selectProject();
                    isCollapsed = true;
                    // EXECUTE THE SEARCH
                    isFirstTime = true;
                    searchTasks();
            } else {
                // Init of Unconfirmed
                unconfirmedOnly = false;
                confirmedOnly = false;
                isFirstTime = true;
                // Reset the filters
                reset();
                
                // Set the default project to the first project in the list
                selectedProject = ctSearch.ST_NONE;
                isCollapsed = false;
            }
        } else {
            // Init of Unconfirmed
            unconfirmedOnly = false;
            confirmedOnly = false;
            isFirstTime = true;    
            reset();
            
            //set the default project to the first project in the list
            selectedProject = ctSearch.ST_NONE;
            isCollapsed = false;
        }
        
    }       
  
    // Reset every filters
    public void reset() {
        showPageBlockTable=false;
        selectedProject = ctSearch.ST_NONE;
        selectedProjectName = 'Home';
        selectedProjectLabel = null;
        selectedProjectMigratedDate = null;
        selectProject();
        ListRCVWrapper = null;
        confirmedOnly = false;
        unConfirmedOnly = false;
        if(!isFirstTime)
        PRA_Utils.deleteUserPreference();
    }
    
    // Selecting a Project updates OA and Regions
    public void selectProject() {
    	ctSearch.initOnSelectProject();
    }
    
    
    public void searchTasksVF() {
    
        for(Client_Project__c c : [select name,Is_Project_Locked__c from Client_Project__c where id = :selectedProject limit 1]) {
                selectedProjectName     = c.Name;
                selectedProjectLabel    = c.Name;
                blocked                 = c.Is_Project_Locked__c;
        }
        isFirstTime = false;
        searchTasks();
    }
    // Event Handlers
    public void initGroupOptions() {
		ctSearch.initGroupOptions();    	
    }
    
    public void initTaskOptions() {
    	ctSearch.initTaskOptions();
    }
    // Search method
    public void searchTasks() {    
		mapRCVWrapper.clear();
		
		List<Regional_Contract_Value__c> rcValList = ctSearch.getRegionalContractValues();
		for(Regional_Contract_Value__c RCValue: rcValList) {
		   // Set project migrated date           
		   // Init
		   if(mapRCVWrapper.get(RCValue.Client_Task__c) == null) {
		       // Link the Wrapper to the Task Id
		       mapRCVWrapper.put(RCValue.Client_Task__c, new RCVWrapper());                
		       mapRCVWrapper.get(RCValue.Client_Task__c).sfId = RCValue.id;
		       
		       // Task Name
		       if(RCValue.Client_Task__r.Description__c == null) {
		           mapRCVWrapper.get(RCValue.Client_Task__c).taskName  ='';
		       } else {
		           mapRCVWrapper.get(RCValue.Client_Task__c).taskName = RCValue.Client_Task__r.Description__c;
		       }
		        
		       // Region
		       if(RCValue.Client_Task__r.Project_Region__r.name == null) {
		           mapRCVWrapper.get(RCValue.Client_Task__c).region  ='';
		       } else {
		           mapRCVWrapper.get(RCValue.Client_Task__c).region = RCValue.Client_Task__r.Project_Region__r.name;
		       }
		     
		       // RCValue of measurement
		       if(RCValue.Client_Task__r.Unit_of_Measurement__c == null) {
		           mapRCVWrapper.get(RCValue.Client_Task__c).unitMeasurement  ='';
		       } else {
		           mapRCVWrapper.get(RCValue.Client_Task__c).unitMeasurement = RCValue.Client_Task__r.Unit_of_Measurement__c;
		       }
		       
		       //Unit Number
		       if(RCValue.Client_Task__r.Client_Unit_Number__c == null) {
		           mapRCVWrapper.get(RCValue.Client_Task__c).unitnumber ='';
		       } else {
		           mapRCVWrapper.get(RCValue.Client_Task__c).unitnumber = RCValue.Client_Task__r.Client_Unit_Number__c;
		       }
		       
		       //Regional values               
		       if(RCValue.US_Canada__c == null) {
		           mapRCVWrapper.get(RCValue.Client_Task__c).uc = 0;
		       } else {
		           mapRCVWrapper.get(RCValue.Client_Task__c).uc = RCValue.US_Canada__c * RCValue.Contract_Value_Per_Unit__c;
		           mapRCVWrapper.get(RCValue.Client_Task__c).percentUC = Math.round((RCValue.US_Canada__c * 100).setScale(1));
		       }
		       
		       if(RCValue.Europe_Africa__c == null) {
		           mapRCVWrapper.get(RCValue.Client_Task__c).ea = 0;
		       } else {                
		           mapRCVWrapper.get(RCValue.Client_Task__c).ea = RCValue.Europe_Africa__c * RCValue.Contract_Value_Per_Unit__c;
		           mapRCVWrapper.get(RCValue.Client_Task__c).percentEA = Math.round((RCValue.Europe_Africa__c * 100).setScale(1));
		       }
		       
		       if(RCValue.Latin_America__c == null) {
		           mapRCVWrapper.get(RCValue.Client_Task__c).la = 0;
		       } else {
		           mapRCVWrapper.get(RCValue.Client_Task__c).la = RCValue.Latin_America__c * RCValue.Contract_Value_Per_Unit__c;
		           mapRCVWrapper.get(RCValue.Client_Task__c).percentLA = Math.round((RCValue.Latin_America__c * 100).setScale(1));
		       }
		       
		       if(RCValue.Asia_Pacific__c == null) {
		           mapRCVWrapper.get(RCValue.Client_Task__c).ap = 0;
		       } else { 
		           mapRCVWrapper.get(RCValue.Client_Task__c).ap = RCValue.Asia_Pacific__c * RCValue.Contract_Value_Per_Unit__c;  
		           mapRCVWrapper.get(RCValue.Client_Task__c).percentAP = Math.round((RCValue.Asia_Pacific__c * 100).setScale(1));
		       }
		       
		       if(RCValue.Contract_Value_Per_Unit__c == null) {
		          mapRCVWrapper.get(RCValue.Client_Task__c).unitPrice = 0;
		       } else { 
		          mapRCVWrapper.get(RCValue.Client_Task__c).unitPrice = RCValue.Contract_Value_Per_Unit__c;
		       }
		       
		       Date dateEffective = RCValue.Effective_Date__c;
		       Integer month = dateEffective.month();
		       String strMonth = '';
		       if(month == 1) strMonth = 'Jan';
		       if(month == 2) strMonth = 'Feb';
		       if(month == 3) strMonth = 'Mar';
		       if(month == 4) strMonth = 'Apr';
		       if(month == 5) strMonth = 'May';
		       if(month == 6) strMonth = 'Jun';
		       if(month == 7) strMonth = 'Jul';
		       if(month == 8) strMonth = 'Aug';
		       if(month == 9) strMonth = 'Sep';
		       if(month == 10) strMonth = 'Oct';
		       if(month == 11) strMonth = 'Nov';
		       if(month == 12) strMonth = 'Dec';
		           
		       mapRCVWrapper.get(RCValue.Client_Task__c).efd = String.valueOf(dateEffective.day()) + '-' + strMonth + '-' + String.valueOf(dateEffective.year());
		       mapRCVWrapper.get(RCValue.Client_Task__c).currency1 = RCValue.Client_Task__r.Project__r.Project_Currency__c;            
		 
		   }    
		}   
		
		ListRCVWrapper = new List<RCVWrapper>();
		for(RCVWrapper compWrap: mapRCVWrapper.values()){
		    // Handle the coloration when Worked <> Source Worked
		    // Added for class
		    compWrap.differentFromSourceClass = 'differentFromSource';
		    ListRCVWrapper.add(compWrap);
		}
		if(ListRCVWrapper!=null && ListRCVWrapper.size() > 0) {
		    showPageBlockTable = true;
		    showNoRecordsMessage = false;
		} else {
		    showPageBlockTable = false;
		    showNoRecordsMessage = true;
		}
		
		if(!isFirstTime) {
		    // Save user prefs
		    PRA_Utils.savePrefs(selectedProject, selectedRegion, selectedOperationalArea, selectedTask, 
		                         selectedGroup, unconfirmedOnly, confirmedOnly);
		} else {
		    isFirstTime = false;
		}
    }   
   
    
    // JS Remoting action called when looking for Project
    @RemoteAction
    global static List<Client_Project__c> searchProject(String projectName) {
        System.debug('projectName: '+projectName );
        List<Client_Project__c> liProj = Database.query('select id, name from Client_Project__c where name like \'%' 
            + String.escapeSingleQuotes(projectName) + '%\' and Load_Status__c != \'New\'');
        return liProj;
    }
    
    //JS Remoting action Updates the RCV values
    @RemoteAction
    global static void updateRCVList(List<Regional_Contract_Value__c> liDataSend, List<String> dates) {
        Integer index = 0;
        List<Regional_Contract_Value__c> updatedList = new List<Regional_Contract_Value__c>();
        for(Regional_Contract_Value__c obj : liDataSend) {
            String strDate = dates[index];
            obj.US_Canada__c /= 100;
            obj.Europe_Africa__c /= 100;
            obj.Latin_America__c /= 100;
            obj.Asia_Pacific__c /= 100;
            
            system.debug('RCV Values - UC:' + obj.US_Canada__c + ', EA:' + obj.Europe_Africa__c + ', LA:' 
                + obj.Latin_America__c + ', AP:' + obj.Asia_Pacific__c);
            
            if(strDate != null && strDate != '') {
                List<String> lstDate = strDate.split('-');                
                Integer year;
                Integer day;
                Integer month;
                if(lstDate[0].length() > 2) {
                    year = Integer.valueOf(lstDate[0]);
                    day = Integer.valueOf(lstDate[2].substring(0, 2));
                    month = Integer.valueOf(lstDate[1]);
                } else {
                    day = Integer.valueOf(lstDate[0]);
                    year = Integer.valueOf(lstDate[2]);
                    system.debug('----------date--------' + strDate + lstDate + lstDate[0] + lstDate[1] + lstDate[2]);
                    if(lstDate[1] == 'Jan') month = 1;
                    if(lstDate[1] == 'Feb')  month = 2; 
                    if(lstDate[1] == 'Mar') month = 3;
                    if(lstDate[1] == 'Apr') month = 4;
                    if(lstDate[1] == 'May') month = 5;
                    if(lstDate[1] == 'Jun')month = 6;
                    if(lstDate[1] == 'Jul') month = 7;
                    if(lstDate[1] == 'Aug') month = 8;
                    if(lstDate[1] == 'Sep') month = 9;
                    if(lstDate[1] == 'Oct') month = 10;
                    if(lstDate[1] == 'Nov') month = 11;   
                    if(lstDate[1] == 'Dec') month = 12; 
                }
                obj.Effective_Date__c = Date.newInstance(year, month, day);
            }
            index++;
            updatedList.add(obj);
        }  
          
        update updatedList;
    }
    
    public class NoCumulativeException extends Exception {}
    
    // Wrapper to hold the data to display
    global class RCVWrapper implements Comparable {
        public String unitnumber {get;set;}
        public String sfId {get;set;}
        public Decimal nbUnits {get;set;}
        public String operationalArea {get;set;}
        public String region {get;set;}
        public String country {get;set;}
        public String taskName {get;set;}            
        public String unitMeasurement{get;set;}
        public String differentFromSourceClass {get;set;}        
        public Decimal uc {get;set;}
        public Decimal ea {get;set;}
        public Decimal la {get;set;}
        public Decimal ap {get;set;}
        public Integer percentUC {get;set;}
        public Integer percentEA {get;set;}
        public Integer percentLA {get;set;}
        public Integer percentAP {get;set;}
        public Decimal unitPrice {get;set;}
        public String efd {get;set;}
        public String currency1 {get;set;}        
        
        public RCVWrapper() {
            
        }
        
        // implement the compareTo method
        global Integer compareTo(Object compareTo) {
            RCVWrapper compareToRCVWrapper = (RCVWrapper)compareTo;
            if (taskName == compareToRCVWrapper.taskName) return 0;
            if (taskName > compareToRCVWrapper.taskName) return 1;
            return -1;        
        }
    }
}