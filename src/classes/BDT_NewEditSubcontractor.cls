public with sharing class BDT_NewEditSubcontractor {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} //Administration Subcontractors
	public Subcontractor__c Subcontractor{get;set;}
	public String SubcontractorId;
	
	public BDT_NewEditSubcontractor(){
		
		loadSubcontractor();
	}
	
	public void loadSubcontractor(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Subcontractors');
		SubcontractorId = System.currentPageReference().getParameters().get('SubcontractorId');

		try{ Subcontractor = [Select Name, Address__c, ZipCode__c, City__c, Country__c
									from Subcontractor__c
									where Subcontractor__c.Id = :SubcontractorId];
		}
		catch(QueryException e){Subcontractor = new Subcontractor__c();}
		
	}
	
	public PageReference save(){
		try{
			upsert Subcontractor;
		}
		catch( DMLException e ){ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Subcontractor could not be saved !');
        		ApexPages.addMessage(msg);
		}
		PageReference SubcontractorPage = new PageReference( System.Page.BDT_Subcontractors.getUrl() );
		SubcontractorPage.getParameters().put( 'ScId',Subcontractor.Id );
		Return SubcontractorPage;
	}
	
	public PageReference cancel(){
		PageReference SubcontractorPage = new PageReference( System.Page.BDT_Subcontractors.getUrl() );
		SubcontractorPage.getParameters().put( 'ScId',SubcontractorId );
		Return SubcontractorPage;
	}
	
	public PageReference deleteSoft(){
		Subcontractor.BDTDeleted__c = true;
		
		List<SubcontractorActivity__c> ActivitiesList = [Select id 
		                                                 from SubcontractorActivity__c 
		                                                 where Subcontractor__c = :SubcontractorId ];
		                                                 
		for(SubcontractorActivity__c act: ActivitiesList){
			act.BDTDeleted__c = true;
		}
		try{
			update activitiesList;
			update Subcontractor;
		}
		catch(DMLException e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to delete Subcontractor !');
        	ApexPages.addMessage(msg);
		}
				
		PageReference SubcontractorPage = new PageReference( System.Page.BDT_Subcontractors.getUrl() );
		// not passing the SubcontractorId here because the subcontractor just got deleted.
		Return SubcontractorPage;
	}
}