@isTest(SeeAllData=false)
private class PRA_RegionalContractValueControllerTest{
     static testMethod void PRA_RegionalContractValueControllerTest() {
       
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        UserPreference userPref = new UserPreference();
        PRA_Utils.savePrefs(tu.clientProject.Id, new List<String>{'Asia Pacific', 'North America'}, new List<String>{'Testing', 'Druggist'}, new List<String>{'Task1', 'Project Management'}, 
                                     new List<String>{'Group1', 'Group2'}, true, false);
        PRA_RegionalContractValueController rcValueControllerObj=new PRA_RegionalContractValueController();
        test.startTest();              
           rcValueControllerObj.selectedProject=tu.clientProject.Id;
           PRA_RegionalContractValueController.searchProject(tu.clientProject.Name);
           //rcValueControllerObj.selectedOperationalArea.add(tu.operationalArea);
           rcValueControllerObj.selectProject();
           rcValueControllerObj.searchTasks();
           //rcValueControllerObj.exportToExcel();
           rcValueControllerObj.searchTasksVF();
           rcValueControllerObj.reset();
           List<Client_Task__c> clientRegionCountryTasks = tu.clientTasks;
           List<Regional_Contract_Value__c> regionalContractValues = new List<Regional_Contract_Value__c>();
           system.debug('------regionalContractValues---'+regionalContractValues);
           // String strDate=String.valueOf( System.today().day())+'-'+String.valueOf( System.today().month())+'-'+String.valueOf( System.today().year());
            for(Integer i=0;i<tu.regionalContractValues.size();i++){
                Regional_Contract_Value__c retVal = new Regional_Contract_Value__c(Id=tu.regionalContractValues.get(i).Id);              
                
                retVal.Effective_Date__c = Date.today();
                retVal.US_Canada__c = 2;
                retVal.Europe_Africa__c = 2;
                retVal.Latin_America__c = 2;
                retVal.Asia_Pacific__c = 2;
                
                regionalContractValues.add(retVal);
            }
            //PRA_RegionalContractValueController.updateRCVList(regionalContractValues);
        test.stopTest();
    }
    static testmethod void PRA_RegionalContractValueControllerTest2() {
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        PRA_RegionalContractValueController rcValueControllerObj=new PRA_RegionalContractValueController();
        test.startTest(); 
           rcValueControllerObj.selectedProject=tu.clientProject.Id;
           PRA_RegionalContractValueController.searchProject(tu.clientProject.Name);
           //rcValueControllerObj.selectedOperationalArea.add(tu.operationalArea);
           rcValueControllerObj.selectProject();
           rcValueControllerObj.searchTasks();
           //rcValueControllerObj.exportToExcel();
           rcValueControllerObj.searchTasksVF();
           rcValueControllerObj.reset();
        test.stopTest();           
    }
    static testmethod void PRA_RegionalContractValueControllerTest3() {
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        UserPreference userPref = new UserPreference();
        PRA_Utils.savePrefs(tu.clientProject.Id, new List<String>{'Asia Pacific', 'North America'}, new List<String>{'Testing', 'Druggist'}, new List<String>{'Task1', 'Project Management'}, 
                                     new List<String>{'Group1', 'Group2'}, true, false);
        PRA_RegionalContractValueController rcValueControllerObj=new PRA_RegionalContractValueController();
        test.startTest(); 
            rcValueControllerObj.selectedProject=null;
            PRA_RegionalContractValueController.searchProject(tu.clientProject.Name);
            rcValueControllerObj.reset();
        test.stopTest();           
    }
}