/** Implements the Selector Layer of the object LaboratoryAnalysis__c
 * @author	Dimitrios Sgourdos
 * @version	19-Nov-2013
 */
public with sharing class LaboratoryAnalysisDataAccessor {
	
	/** Object definition for fields used in application for LaboratoryAnalysis
	 * @author	Dimitrios Sgourdos
	 * @version 08-Oct-2013
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('LaboratoryAnalysis__c');
	}
	
	
	/** Object definition for fields used in application for LaboratoryAnalysis with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 07-Nov-2013
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'Analysis__c,';
		result += referenceName + 'LaboratoryMethodCompound__c,';
		result += referenceName + 'MethodDevelopment__c,';
		result += referenceName + 'MethodValidation__c,';
		result += referenceName + 'Study__c';
		return result;
	}
	
	
	/** Retrieve the Laboratory analysis for the given studies that meet the parameter description.
	 * @author	Dimitrios Sgourdos
	 * @version 19-Nov-2013
	 * @param	studiesList			The given studies
	 * @return	The Laboratory analysis list.
	 */
	public static List<LaboratoryAnalysis__c> getByUsedType(List<Study__c> studiesList, String description) {
		String query = String.format(
							'SELECT {0}, {1}, {2} ' +
							'FROM LaboratoryAnalysis__c ' +
							'WHERE {3} ' +
							'AND {4} ' +
							'ORDER BY Study__r.Code__c, LaboratoryMethodCompound__r.LaboratoryMethod__r.Name',
							new List<String> {
								getSObjectFieldString(),
								LaboratoryMethodDataAccessor.getSObjectFieldString('LaboratoryMethodCompound__r.LaboratoryMethod__r'),
								StudyDataAccessor.getSObjectFieldString('Study__r'),
								description,
								'Study__c IN :studiesList'
							}
						);
		return (List<LaboratoryAnalysis__c>) Database.query(query);
	}
	
	
	/** Retrieve the Laboratory analysis that the MethodDevelopment or MethodValidation flag is true 
	 *	for the given studies.
	 * @author	Dimitrios Sgourdos
	 * @version 19-Nov-2013
	 * @param	studiesList			The given studies
	 * @return	The Laboratory analysis list.
	 */
	public static List<LaboratoryAnalysis__c> getByMdOrValAndUserStudies(List<Study__c> studiesList) {
		return getByUsedType(studiesList, '(MethodDevelopment__c = TRUE OR MethodValidation__c = TRUE)');
	}
	
	
	/** Retrieve the Laboratory analysis that the Analysis__c flag is true for the given studies.
	 * @author	Dimitrios Sgourdos
	 * @version 19-Nov-2013
	 * @param	studiesList			The given studies
	 * @return	The Laboratory analysis list.
	 */
	public static List<LaboratoryAnalysis__c> getByAnalysisAndUserStudies(List<Study__c> studiesList) {
		return getByUsedType(studiesList, 'Analysis__c = TRUE');
	}
}