/**
 * Test class to cover wrapper classes
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestListClasses {
    
    static List<LMS_Course__c> courses;
    static List<LMS_Role__c> roles;
    static List<LMS_Role_Course__c> roleCourses;
    static List<LMS_Role_Employee__c> roleEmployees;
    static List<Employee_Details__c> employees;
    static List<Employee_Details__c> persons;
    static List<Country__c> countries;
    static List<Job_Class_Desc__c> jobClasses;
    static List<Job_Title__c> jobTitles;
    static List<LMSConstantSettings__c> constants;
    static List<CourseDomainSettings__c> domains;
    
    static Map<String, String> courseMap;
    static Map<String, LMS_Role_Course__c> roleMap;
    static Map<String, String> jobTitleMap;
    static Map<String, String> personMap;
    
    static void initCourseData() {
        courses = new LMS_Course__c[] {
            new LMS_Course__c(Offering_Template_No__c = 'course123', Course_Code__c = 'C123', Title__c = 'Course 123', Domain_Id__c = 'Internal',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse1',
                              Discontinued_From__c = Date.today()-1),
            new LMS_Course__c(Offering_Template_No__c = 'course321', Course_Code__c = 'C124', Title__c = 'Course 321', Domain_Id__c = 'Internal',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse2',
                              Discontinued_From__c = Date.today()+1)
        };
        insert courses;
        
        courseMap = new Map<String, String>();
        courseMap.put(courses[0].Course_Code__c, courses[0].Course_Code__c);
    }
    
    static void initRoleData() {
        roles = new LMS_Role__c[] {
            new LMS_Role__c(Adhoc_Role_Name__c = 'TestRole1', Status__c = 'Active', Sync_Status__c = 'N'),
            new LMS_Role__c(Adhoc_Role_Name__c = 'TestRole2', Status__c = 'Active', Sync_Status__c = 'N')
        };
        insert roles;
    }
    
    static void initRoleCourseData() {
        initCourseData();
        initRoleData();
        
        roleCourses = new LMS_Role_Course__c[] {
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Committed', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA', Commit_Date__c = Date.parse('12/6/2012')),
            new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Draft Delete', Assigned_By__c = 'ASA', Assigned_On__c = Datetime.now())
        };
        insert roleCourses;
        
        roleMap = new Map<String, LMS_Role_Course__c>();
        roleMap.put(roles[0].Id, roleCourses[0]);
    }
    
    static void initRoleEmployeeData() {
        initRoleData();
        initEmployeeData();
        
        roleEmployees = new LMS_Role_Employee__c[] {
            new LMS_Role_Employee__c(Role_Id__c = roles[0].Id, Employee_Id__c = employees[0].Id, Status__c = 'Draft', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today()),
            new LMS_Role_Employee__c(Role_Id__c = roles[0].Id, Employee_Id__c = employees[1].Id, Status__c = 'Draft Delete', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today(), Assigned_On__c = Date.parse('12/6/2012'))
        };
        insert roleEmployees;
    }
    
    static void initEmployeeData() {
        
        constants = new LMSConstantSettings__c[] {
        	new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
    		new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
    		new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
    		new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D'),
    		new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
    		new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
    		new LMSConstantSettings__c(Name = 'typePRA', Value__c = 'PRA'),
    		new LMSConstantSettings__c(Name = 'typeProject', Value__c = 'Project Specific'),
    		new LMSConstantSettings__c(Name = 'typeAdhoc', Value__c = 'Additional Role'),
    		new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
    		new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
    		new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
    		new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
    		new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
    		new LMSConstantSettings__c(Name = 'statusToDelete', Value__c = 'Delete'),
    		new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended'),
    		new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
    		new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
    		new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
    		new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete')
        };
        insert constants;
        
        domains = new CourseDomainSettings__c[] {
        	new CourseDomainSettings__c(Name='Archive', Domain_Id__c = 'Archive')
        };
        insert domains;
        
        countries = new Country__c[] {
            new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='US/Canada',
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry2',PRA_Country_ID__c='200', Country_Code__c='JU',Region_Name__c='Latin America', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry3',PRA_Country_ID__c='300', Country_Code__c='TB',Region_Name__c='Asia/Pacific', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry4',PRA_Country_ID__c='400', Country_Code__c='AB',Region_Name__c='Europe/Africa', 
                               Daily_Business_Hrs__c=8.0)
        };
        insert countries;
        
        employees = new Employee_Details__c[] {
            new Employee_Details__c(Name = 'employee1', Employee_Unique_Key__c = 'employee1', First_Name__c = 'Andrew', Last_Name__c = 'Allen', Email_Address__c = 'a@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU'),
            new Employee_Details__c(Name = 'employee2', Employee_Unique_Key__c = 'employee2', First_Name__c = 'Allen', Last_Name__c = 'Stephens', Email_Address__c = 's@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU')
        };
        insert employees;
        
        jobClasses = new Job_Class_Desc__c[]{
            new Job_Class_Desc__c(Name = 'class1', Job_Class_Code__c = 'code1', Job_Class_ExtID__c = 'class1'),
            new Job_Class_Desc__c(Name = 'class2', Job_Class_Code__c = 'code2', Job_Class_ExtID__c = 'class2')
        };
        insert jobClasses;
        
        jobTitles = new Job_Title__c[] {
            new Job_Title__c(Job_Code__c = 'Code1', Job_Title__c = 'title1', Job_Class_Desc__c = jobClasses[0].Id, Status__c = 'A'),
            new Job_Title__c(Job_Code__c = 'Code2', Job_Title__c = 'title2', Job_Class_Desc__c = jobClasses[1].Id, Status__c = 'A')
        };
        insert jobTitles;
        
        personMap = new Map<String, String>();
        personMap.put(employees[0].Name, employees[0].Name);
        
        jobTitleMap = new Map<String, String>();
        for(Integer i = 0; i < jobTitles.size(); i++) {
            jobTitleMap.put(jobTitles[i].Job_Code__c, jobTitles[0].Job_Title__c);
        }
    }

    static testMethod void testCourseList() {
        
    	initEmployeeData();
        initCourseData();
        LMS_CourseList cList = null;
        List<LMS_CourseList> courseList = new List<LMS_CourseList>();
        
        for(Integer i = 0; i < courses.size(); i++) {
            cList = new LMS_CourseList(courses[i], courseMap);
            courseList.add(cList);
        }
        
        System.assert(courseList.size() == 2 && courseList[0].courseId == courses[0].Id);
        
    }
    
    static testMethod void testRoleList() {
        
    	initEmployeeData();
        initRoleCourseData();
        LMS_RoleList rList = null;
        List<LMS_RoleList> roleList = new List<LMS_RoleList>();
        
        for(Integer i = 0; i < roles.size(); i ++) {
            rList = new LMS_RoleList(roles[i], roleMap);
            roleList.add(rList);
        }
        
        System.assert(roleList.size() == 2 && roleList[0].roleId == roles[0].Id);
        
    }
    
    static testMethod void testEmployeeList() {
        
        initRoleEmployeeData();
        LMS_EmployeeList eList = null;
        List<LMS_EmployeeList> employeeList = new List<LMS_EmployeeList>();
        
        for(Integer i = 0; i < roleEmployees.size(); i++) {
            eList = new LMS_EmployeeList(roleEmployees[i], jobTitleMap);
            employeeList.add(eList);
        }
        
        System.assert(employeeList.size() == 2);
        
    }
    
    static testMethod void testPersonList() {
        
        initEmployeeData();
        LMS_PersonList pList = null;
        List<LMS_PersonList> personList = new List<LMS_PersonList>();
        
        for(Integer i = 0; i < employees.size(); i++) {
            pList = new LMS_PersonList(employees[i], personMap, jobTitleMap);
            personList.add(pList);
        }
        
        System.assert(personList.size() == 2 && personList[0].jobTitle == jobTitleMap.get(employees[0].Job_Code__c));
        
    }
    
    static testMethod void testCourseAssignments() {
        
    	initEmployeeData();
        initRoleCourseData();
        LMS_CourseAssignments cList = null;
        List<LMS_CourseAssignments> assignments = new List<LMS_CourseAssignments>();
        
        for(Integer i = 0; i < roleCourses.size(); i++) {
            cList = new LMS_CourseAssignments(roleCourses[i]);
            assignments.add(cList);
        }
        
        System.assert(assignments.size() == 4 && assignments[0].assignmentStatus == 'Committed');
        
    }
    
    static testMethod void testRoleAssignments() {
        
    	initEmployeeData();
        initRoleCourseData();
        LMS_RoleAssignment rList = null;
        List<LMS_RoleAssignment> assignments = new List<LMS_RoleAssignment>();
        
        for(Integer i = 0; i < roleCourses.size(); i++) {
            rList = new LMS_RoleAssignment(roleCourses[i]);
            assignments.add(rList);
        }
        
        System.assert(assignments.size() == 4 && assignments[0].assignmentStatus == 'Committed');
        
    }
    
    static testMethod void testPersonAssignments() {
        
        initRoleEmployeeData();
        LMS_PersonAssignment pList = null;
        List<LMS_PersonAssignment> personList = new List<LMS_PersonAssignment>();
        
        for(Integer i = 0; i < roleEmployees.size(); i++) {
            pList = new LMS_PersonAssignment(roleEmployees[i], jobTitleMap);
            personList.add(pList);
        }
        
        System.assert(personList.size() == 2 && personList[0].assignmentStatus == 'Draft');
        
    }
    
}