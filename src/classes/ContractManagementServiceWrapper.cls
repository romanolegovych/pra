/**
*	A wrapper class for the Contract Management Web Service (WS).
*/
public with sharing class ContractManagementServiceWrapper {
	
	private static String ENDPOINT = '';
	private static String SERVICENAME = '';
	private static Integer TIMEOUT = 0;
	private static List<String> errors;
	
	static {
		initSettings();
	}  
	
	public static List<String> getErrors() {
		return errors;
	}

	/**
	*	Configure the WS endpoint
	*/
	private static void initSettings() {
		Map<String, MuleServicesCS__c> settings = MuleServicesCS__c.getAll();
		ENDPOINT = settings.get('ENDPOINT').Value__c;
		SERVICENAME = settings.get('ContractManagementService').Value__c;
		Integer to = Integer.valueOf(settings.get('TIMEOUT').Value__c);
		if (to != null && to > 0) {
			TIMEOUT = to;
		} 
		system.debug('--------- Using following Custom Settings for MuleServiceCS ---------');
		system.debug('ENDPOINT:'+ENDPOINT);
		system.debug('TIMEOUT:'+TIMEOUT);
		system.debug('----------------------- MuleServiceCS END ----------------------------');
	}
	
	/**
	*	Returns the service
	*/
	private static ContractManagementService.PRACTAInboundWebService getService() {
		ContractManagementService.PRACTAInboundWebService service = new ContractManagementService.PRACTAInboundWebService();
		service.endpoint_x = ENDPOINT + SERVICENAME;
		service.timeout_x = TIMEOUT;
		return service;
	}
	
	/**
	*	Returns the list of Sites for the input criteria
	*/
	public static ContractManagementService.ReadSite_Output_element getSites(ContractManagementService_Site.site site) {
		ContractManagementService.ReadSite_Output_element response = null;
		if (site != null) {
			ContractManagementService.PRACTAInboundWebService service = getService();
			try {
				ContractManagementService_Site.sites sites = new ContractManagementService_Site.sites();
				sites.site = new ContractManagementService_Site.site[] {site};
				response = service.ReadSite(sites);
				system.debug('---------------- getSites successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- getSites failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	/**
	*	Returns the list of Site Contracts for the input criteria
	*/
	public static ContractManagementService.ReadSiteContract_Output_element getSiteContracts(ContractManagementService_SiteContract.site site) {
		ContractManagementService.ReadSiteContract_Output_element response = null;
		if (site != null && null != site.id) {
			ContractManagementService.PRACTAInboundWebService service = getService();
			try {
				ContractManagementService_SiteContract.sites sites = new ContractManagementService_SiteContract.sites();
				sites.site = new ContractManagementService_SiteContract.site[] {site};
				response = service.ReadSiteContract(sites);
				system.debug('---------------- getSiteContracts successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- getSiteContracts failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	/**
	 * Returns the LOV - List of values
	 */
	public static ContractManagementService.ReadLOV_Output_element getLOVs(ContractManagementService_LOV.lov lov) {
		ContractManagementService.ReadLOV_Output_element response = null;
		if (lov != null && null != lov.type_x) {
			ContractManagementService.PRACTAInboundWebService service = getService();
			try {
				ContractManagementService_LOV.lovs lovs = new ContractManagementService_LOV.lovs();
				lovs.lov = new ContractManagementService_LOV.lov[] {lov};
				response = service.ReadLOV(lovs);
				system.debug('---------------- getLOVs successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- getLOVs failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	/**
	 * Returns the LOV's for Contract Statuses
	 */
	public static ContractManagementService.ReadLOV_Output_element getContractStatuses() {
		ContractManagementService.ReadLOV_Output_element response = null;
		ContractManagementService_LOV.lov lov = new ContractManagementService_LOV.lov();
		lov.active = 'Y';
		lov.type_x = 'PRA_CONTRACT_INV_STATUS';
		if (lov != null) {
			try {
				response = getLOVs(lov);
				system.debug('---------------- getContractStatuses successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- getContractStatuses failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	/**
	 * Returns the LOV's for Contract Types
	 */
	public static ContractManagementService.ReadLOV_Output_element getContractTypes() {
		ContractManagementService.ReadLOV_Output_element response = null;
		ContractManagementService_LOV.lov lov = new ContractManagementService_LOV.lov();
		lov.active = 'Y';
		lov.parent = 'Inv Contracts';
		lov.type_x = 'TODO_TYPE';
		if (lov != null) {
			try {
				response = getLOVs(lov);
				system.debug('---------------- getContractTypes successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- getContractTypes failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	/**
	*	Returns the list of Accounts for the input criteria
	*/
	public static ContractManagementService.ReadAccount_Output_element getAccounts(ContractManagementService_Account.account account) {
		ContractManagementService.ReadAccount_Output_element response = null;
		if (account != null) {
			ContractManagementService.PRACTAInboundWebService service = getService();
			try {
				ContractManagementService_Account.accounts accounts = new ContractManagementService_Account.accounts();
				accounts.account = new ContractManagementService_Account.account[] {account};
				response = service.ReadAccount(accounts);
				system.debug('---------------- getAccounts successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- getAccounts failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	/**
	*	Returns the list of Contacts for the input criteria
	*/
	public static ContractManagementService.ReadContact_Output_element getContacts(ContractManagementService_Contact.contact contact) {
		ContractManagementService.ReadContact_Output_element response = null;
		if (contact != null) {
			ContractManagementService.PRACTAInboundWebService service = getService();
			try {
				ContractManagementService_Contact.contacts contacts = new ContractManagementService_Contact.contacts();
				contacts.contact = new ContractManagementService_Contact.contact[] {contact};
				response = service.ReadContact(contacts);
				system.debug('---------------- getContacts successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- getContacts failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	/**
	*	Returns the list of Site Contacts for the input criteria
	*/
	public static ContractManagementService.ReadSiteContact_Output_element getSiteContacts(ContractManagementService_SiteContact.site site) {
		ContractManagementService.ReadSiteContact_Output_element response = null;
		if (site != null && null != site.id) {
			ContractManagementService.PRACTAInboundWebService service = getService();
			try {
				ContractManagementService_SiteContact.sites sites = new ContractManagementService_SiteContact.sites();
				sites.site = new ContractManagementService_SiteContact.site[] {site};
				response = service.ReadSiteContact(sites);
				system.debug('---------------- getSiteContacts successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- getSiteContacts failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	/**
	*	Returns the list of Site Teams for the input criteria
	*/
	public static ContractManagementService.ReadSiteTeam_Output_element getSiteTeams(ContractManagementService_SiteTeam.site site) {
		ContractManagementService.ReadSiteTeam_Output_element response = null;
		if (site != null && null != site.id) {
			ContractManagementService.PRACTAInboundWebService service = getService();
			try {
				ContractManagementService_SiteTeam.sites sites = new ContractManagementService_SiteTeam.sites();
				sites.site = new ContractManagementService_SiteTeam.site[] {site};
				response = service.ReadSiteTeam(sites);
				system.debug('---------------- getSiteTeams successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- getSiteTeams failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	/**
	*	Upserts the list of Site Activities
	*/
	public static ContractManagementService.WriteSiteActivity_Output_element upsertSiteActivities(ContractManagementService_SiteActivity.site site, String user) {
		ContractManagementService.WriteSiteActivity_Output_element response = null;
		if (null != site && null != site.id && null != site.activities && null != site.activities.activity && null != user) {
			ContractManagementService.PRACTAInboundWebService service = getService();
			try {
				ContractManagementService_SiteActivity.sites sites = new ContractManagementService_SiteActivity.sites();
				sites.site = new ContractManagementService_SiteActivity.site[] {site};
				response = service.WriteSiteActivity(sites, user);
				system.debug('---------------- upsertSiteActivities successfull -----------------');
				for(ContractManagementService_SiteActivity.site siteResponse : response.sites.site) {
					if (null != siteResponse  && null != siteResponse.activities) {
						for(ContractManagementService_SiteActivity.activity activity : siteResponse.activities.activity) {
							system.debug('upsertSiteActivities:siteID:'+siteResponse.id+', contractID:'+activity.id);
						}
					}
				}
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- upsertSiteActivities failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	/**
	*	Upserts the list of Site Contracts
	*/
	public static ContractManagementService.WriteSiteContract_Output_element upsertSiteContracts(ContractManagementService_SiteContract.site site, String user) {
		ContractManagementService.WriteSiteContract_Output_element response = null;
		if (null != site && null != site.id && null != site.contracts && null != site.contracts.contract) {
			ContractManagementService.PRACTAInboundWebService service = getService();
			try {
				ContractManagementService_SiteContract.sites sites = new ContractManagementService_SiteContract.sites();
				sites.site = new ContractManagementService_SiteContract.site[] {site};
				response = service.WriteSiteContract(sites, user);
				system.debug('---------------- upsertSiteContracts successfull -----------------');
				if(null != response && null != response.sites && null != response.sites.site) {
					for(ContractManagementService_SiteContract.site siteResponse : response.sites.site) {
					if (null != siteResponse  && null != siteResponse.contracts && null != siteResponse.contracts.contract) {
						for(ContractManagementService_SiteContract.contract contract : siteResponse.contracts.contract) {
							system.debug('upsertSiteContracts:siteID:'+siteResponse.id+', contractID:'+contract.id);
						}
					}
					}
				}
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- upsertSiteContracts failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
}