/** 
 * Test to get partial coverage from imported RoleAdminService code
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestRoleAdminService {
    static testMethod void testProcessRoleEmployeeAssociationResponse(){
    	LMS_RoleAdminService.processRoleEmployeeAssociationResponse r = new LMS_RoleAdminService.processRoleEmployeeAssociationResponse();
    }
    static testMethod void testPersonMapResponse(){
    	LMS_RoleAdminService.personMapResponse r = new LMS_RoleAdminService.personMapResponse();
    }
    static testMethod void testRolesPersonMap(){
    	LMS_RoleAdminService.rolesPersonMap r = new LMS_RoleAdminService.rolesPersonMap();
    }
    static testMethod void testError(){
    	LMS_RoleAdminService.error r = new LMS_RoleAdminService.error();
    }
    static testMethod void testCourseRoleResponse(){
    	LMS_RoleAdminService.courseRoleResponse r = new LMS_RoleAdminService.courseRoleResponse();
    }
    static testMethod void testProcessRoleEmployeeAssociation(){
    	LMS_RoleAdminService.processRoleEmployeeAssociation r = new LMS_RoleAdminService.processRoleEmployeeAssociation();
    }
    static testMethod void testRoleResponseItem(){
    	LMS_RoleAdminService.roleResponseItem r = new LMS_RoleAdminService.roleResponseItem();
    }
    static testMethod void testProcessRoleCourseAssociation(){
    	LMS_RoleAdminService.processRoleCourseAssociation r = new LMS_RoleAdminService.processRoleCourseAssociation();
    }
    static testMethod void testCourseMapResponse(){
    	LMS_RoleAdminService.courseMapResponse r = new LMS_RoleAdminService.courseMapResponse();
    }
    static testMethod void testRoleMapResponse(){
    	LMS_RoleAdminService.roleMapResponse r = new LMS_RoleAdminService.roleMapResponse();
    }
    static testMethod void testRoleCourseResponseItem(){
    	LMS_RoleAdminService.rolesCourseResponseItem r = new LMS_RoleAdminService.rolesCourseResponseItem();
    }
    static testMethod void tesRoleCourseMap(){
    	LMS_RoleAdminService.rolesCourseMap r = new LMS_RoleAdminService.rolesCourseMap();
    }
    static testMethod void testCoursesRoleResponseItem(){
    	LMS_RoleAdminService.coursesRoleResponseItem r = new LMS_RoleAdminService.coursesRoleResponseItem();
    } 
    static testMethod void testPersonRoleResponse(){
    	LMS_RoleAdminService.personRoleResponse r = new LMS_RoleAdminService.personRoleResponse();
    }
    static testMethod void testCoursesRoleMap(){
    	LMS_RoleAdminService.coursesRoleMap r = new LMS_RoleAdminService.coursesRoleMap();
    }
    static testMethod void testPersonsRoleResponseItem(){
    	LMS_RoleAdminService.personsRoleResponseItem r = new LMS_RoleAdminService.personsRoleResponseItem();
    }
    static testMethod void testProcessRoleCourseAssociationResponse(){
    	LMS_RoleAdminService.processRoleCourseAssociationResponse r14 = new LMS_RoleAdminService.processRoleCourseAssociationResponse();
    }
    static testMethod void testProcessRolesReponse(){
    	LMS_RoleAdminService.processRolesResponse r = new LMS_RoleAdminService.processRolesResponse();
    }
    static testMethod void testPersonsRoleMap(){
    	LMS_RoleAdminService.personsRoleMap r = new LMS_RoleAdminService.personsRoleMap();
    }
    static testMethod void testRoleResponse(){
    	LMS_RoleAdminService.roleResponse r = new LMS_RoleAdminService.roleResponse();
    }
    static testMethod void testRolesPersonResponseItem(){
    	LMS_RoleAdminService.rolesPersonResponseItem r = new LMS_RoleAdminService.rolesPersonResponseItem();
    }
}