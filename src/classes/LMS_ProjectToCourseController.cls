global class LMS_ProjectToCourseController {

    // Instance Vars
    public String helpTextCourse{get;set;}
    public String helpTextRole{get;set;}
    public String commitDate{get;set;}
    public String courseText{get;set;}
    public String courseClient{get;set;}
    public String courseProject{get;set;}
    public String courseFilter{get;private set;}
    public String courseResultId{get;private set;}
    public String courseTextStyle{get;private set;}
    public String courseNameDisplay{get;private set;}
    public String projectRole{get;set;}
    public String projectFilter{get;private set;}
    public String roleName{get;set;}
    public String roleFilter{get;private set;}
    public String roleNameStyle{get;set;}
    public String roleErrorText{get;private set;}
    public String webServiceError{get;private set;}
    public Boolean allChecked{get;set;}
    public Boolean allCheckedRole{get;set;}
    public Boolean bWebServiceError{get;private set;}

    // For Impact calculations
    public Integer employeeStart{get;set;}
    public Integer employeeChange{get;set;}
    public Integer employeeEnd{get;set;}
    private Integer employeeNum{get;set;}
    public Decimal impactStart{get;set;}
    public String impactChange{get;set;}
    public Decimal impactEnd{get;set;}
    private Decimal impactNum{get;set;}

    // Collections
    public list<LMS_RoleAssignment> roles{get;set;}
    public list<LMS_RoleList> selRoles{get;set;}
    public list<LMS_EmployeeList> empChange{get;set;}
    public list<LMS_EmployeeList> empTotal{get;set;}
    private list<AggregateResult> durationList{get;set;}
    public list<LMS_Course__c> courses{get;set;}
    private Map<String, LMS_Role_Course__c> courseNoDraft{get;set;}
    private Map<String, LMS_Role_Course__c> courseCommitted{get;set;}
    private Map<String, LMS_Role_Course__c> courseDraft{get;set;}
    private Map<String, LMS_Role_Course__c> courseDraftDelete{get;set;}
    private Map<String, LMS_Role_Course__c> mappings{get;set;}
    private Map<String, CourseDomainSettings__c> settings{get;set;}
    private Map<String, LMSConstantSettings__c> constants{get;set;}

    // Error Varaibles
    public List<String> sfdcErrors{get;private set;}
    public List<String> addErrors{get;private set;}
    public List<String> deleteErrors{get;private set;}
    public List<String> calloutErrors{get;private set;}

    public LMS_ProjectToCourseController() {
        initSettings();
        init();
    }

    /**
     *  Initialize settings and course domains from custom settings
     */ 
    private void initSettings() {
        settings = CourseDomainSettings__c.getAll();
        constants = LMSConstantSettings__c.getAll();
    }

    /**
     *  Initializes all other data
     */
    private void init() {
        bWebServiceError = false;
        String internalDomain = settings.get('Internal').Domain_Id__c;
        String pstDomain = settings.get('PST').Domain_Id__c;
        courseText = 'Enter course title';
        courseTextStyle = 'WaterMarkedTextBox';
        courseFilter = 'and Domain_Id__c IN (\'' + internalDomain + '\',\'' + pstDomain + '\') ' + 
			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
			'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';
        roleName = 'Enter Role Name';
        roleNameStyle = 'WaterMarkedTextBox';
        roleFilter = ' and Role_Type__r.Name = \'Project Specific\' and Project_Id__r.Status_Desc__c NOT IN (\'Bid\',\'Lost\',\'Closed\') and Status__c = \'Active\'';
        projectFilter = ' and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\') and Client_Id__c != \'\' and Client_Id__c != null';
    	helpTextCourse = 'Enter the course title of the course you want to retrieve.  As you begin entering the course title, the search list will be narrowed. \n\n' +
			'You can also use a wildcard “%” to filter the search list.  For example, entering ABC%Protocol will return courses starting with ABC and with protocol in the course title. \n\n' + 
			'After selecting your course from the search list, click the “Submit Search” button';
		helpTextRole = 'If the sponsor and/or project associated with the selected course (displayed below the course title), match the sponsor and/or project of the roles you want to assign, just click on the “Submit Search” button to retrieve roles.\n\n' + 
			'If you want to retrieve roles associated with a different sponsor and/or project, enter the sponsor, project and/or project role in the Role Name to retrieve the appropriate roles.  This is useful for assigning roles to PRA SOPS and sponsor SOPs\n\n' + 
			'Examples searches:\n' + 
			'-  ABC returns all roles where sponsor starts with ABC (ABC Pharma, ABC Inc, ABC US)\n' + 
			'-  %P01ABCD2-34EFG5 returns all roles for project P01ABCD2-34EFG5\n' + 
			'-  ABC%Medical Director returns all roles where sponsor starts with ABC and project role is Medical Director\n\n' +
			'The role name format is Sponsor ,Project, Project Role.  ' + 
			'If the selected course has a project associated, you can not retrieve roles with a different project ID.';
    }

    public PageReference search() {
		bWebServiceError = false;
        courseNoDraft = new Map<String,LMS_Role_Course__c>();
        courseCommitted = new Map<String,LMS_Role_Course__c>();
        courseDraft = new Map<String,LMS_Role_Course__c>();
        courseDraftDelete = new Map<String,LMS_Role_Course__c>();
        mappings = new Map<String, LMS_Role_Course__c>();

        String extraFilter = '';
        String draft = constants.get('statusDraft').Value__c;
        String committed = constants.get('statusCommitted').Value__c;
        String draftDelete = constants.get('statusDraftDelete').Value__c;
        String pendingAdd = constants.get('statusPendingAdd').Value__c;
        String pendingDelete = constants.get('statusPendingDelete').Value__c;
        String internalDomain = settings.get('Internal').Domain_Id__c;
        String pstDomain = settings.get('PST').Domain_Id__c;

        String roleQry = '';
        roleQry = 'select Role_Id__r.Role_Name__c,Role_Id__r.SABA_Role_PK__c,Role_Id__r.Status__c,Course_Id__r.Available_From__c,' + 
			'Course_Id__r.SABA_ID_PK__c,Assigned_On__c,Assigned_By__c,Commit_Date__c,Status__c,Previous_Status__c,Sync_Status__c ' + 
			'from Rolecourses__r where Role_Id__r.Role_Type__r.Name =\'Project Specific\' and Role_Id__r.Status__c = \'Active\' ' + 
			'and Role_Id__r.Project_Id__r.Status_Desc__c NOT IN (\'Lost\',\'Closed\') and Role_Id__r.Status__c = \'Active\' and Status__c != \'Removed\' ' + 
			'order by Status__c,Role_Id__r.Role_Name__c';
        String courseQry = '';
        courseQry = 'select Title__c,Client_Id__c,Project_Id__c,Duration__c,Duration_Display__c,('+roleQry+') from LMS_Course__c ' + 
			'where Title__c=\'' + String.escapeSingleQuotes(courseText) + '\' and Domain_Id__c IN (\'' + internalDomain + '\',\'' + pstDomain + '\') ' + 
			'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
			'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';
        allChecked = false;

        courses = Database.query(courseQry);

        if(courses.size() > 0) {
            roles = new list<LMS_RoleAssignment>();
            for(LMS_Course__c c : courses) {
                courseClient = c.Client_Id__c;
                courseProject = c.Project_Id__c;
                courseNameDisplay = c.Title__c;
                courseResultId = c.Id;
                for(LMS_Role_Course__c rc : c.roleCourses__r) {
                    mappings.put(rc.Id, rc);
                    if(rc.Status__c == committed || rc.Status__c == draftDelete || rc.Status__c == pendingDelete) {
                        courseNoDraft.put(rc.Role_Id__r.Id,rc);
                    }
                    if(rc.Status__c == draft || rc.Status__c == pendingAdd) {
                        courseDraft.put(rc.Role_Id__r.Id,rc);
                    } 
                    if(rc.Status__c == draftDelete || rc.Status__c == pendingDelete) {
                        courseDraftDelete.put(rc.Role_Id__r.Id,rc);
                    } 
                    if(rc.Status__c == committed) {
                        courseCommitted.put(rc.Role_Id__r.Id,rc);
                    }
                    roles.add(new LMS_RoleAssignment(rc));
                }
            }
            viewImpact();
        }
        if(courseProject != '' && courseProject != null) {
            extraFilter += 'and Project_Id__r.Name = \'' + courseProject + '\'';
        }
        
        if(courseNoDraft != null && !courseNoDraft.isEmpty()) {
            String excludeString = '\'\'';
            Set<String> excludeIds = courseNoDraft.keySet();
            for(String s : excludeIds) {
                excludeString += ',\'' + s + '\'';
            }
            projectFilter = ' and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\') and Client_Id__c != \'\' and Client_Id__c != null';
            roleFilter = ' and Role_Type__r.Name = \'Project Specific\' and Project_Id__r.Status_Desc__c NOT IN (\'Bid\',\'Lost\',\'Closed\') ' 
                + extraFilter + ' and Status__c = \'Active\' and Id NOT IN (' + excludeString + ')';
        } else {
            projectFilter = ' and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\') and Client_Id__c != \'\' and Client_Id__c != null';
            roleFilter = ' and Role_Type__r.Name = \'Project Specific\' and Project_Id__r.Status_Desc__c NOT IN (\'Bid\',\'Lost\',\'Closed\') ' 
                + extraFilter + ' and Status__c = \'Active\'';
        }
        return null;
    }

    public PageReference courseReset() {
        bWebServiceError = false;
        roles = null;
        courses = null;
        selRoles = null;
        viewImpact();
        return null;
    }

    public PageReference closeCourseSearch() {
        selRoles = null;
        return null;
    }

    public PageReference roleSearch() {
        Boolean acSearch = false;
        Boolean hasErrored = false;
        set<String> cAssigned = new set<String>();
        list<String> roleAttributes = new list<String>();
        if(courseNoDraft != null) {
            cAssigned = courseNoDraft.keySet();
        }

        roleErrorText = '';
        String roleQuery = '';
        roleQuery = 'select id,Role_Name__c,Status__c,(select name from RoleEmployees__r) ' + 
            'from LMS_Role__c where Id not in : cAssigned and Role_Type__r.Name=\'Project Specific\' and Status__c = \'Active\' ' + 
            'and Project_Id__r.Status_Desc__c not in (\'Lost\',\'Closed\') and Sync_Status__c=\'Y\'';

        // Check for each type of criteria
        if(roleName != null && roleName != '' && roleName != 'Enter Role Name') {
            acSearch = true;
        }

        // Check for client id / project id for course
        System.debug('--------------------courseClient------------------'+courseClient);
        System.debug('--------------------courseProject------------------'+courseProject);
        if(courseProject == null && courseClient != null) {
            if (acSearch) {
            	roleQuery += ' and Role_Name__c LIKE \'' + String.escapeSingleQuotes(roleName) + '%\'';
            } else {
            	roleQuery += ' and Client_Id__r.Name = :courseClient';
            }
        } else if(courseProject != null) {
            roleQuery += ' and Project_Id__r.Name = :courseProject';
        } else {
            if(acSearch) {
                roleQuery += ' and Role_Name__c LIKE \'' + String.escapeSingleQuotes(roleName) + '%\'';
            }
        }
		
        roleQuery += ' order by Role_Name__c';
        System.debug('-----------------query-----------------'+roleQuery);
        allCheckedRole = false;

        selRoles = new List<LMS_RoleList>();
        List<LMS_Role__c> roleList = Database.query(roleQuery);
        if(roleList.size() == 0) {
        	roleErrorText = 'The result contain no records.  Please refine your search criteria and run the search again.';
        } else if(roleList.size() <= 1000) {
            for(LMS_Role__c r : roleList) {             
                selRoles.add(new LMS_RoleList(r, courseDraft));
            }
            viewImpact();
        } else if(roleList.size() > 1000) {
        	roleErrorText = 'The result contains too many records. Please refine your search criteria and run the search again.';
        }
        return null;
    }

    public PageReference roleReset() {
        roleErrorText = '';
        selRoles = null;
        viewImpact();
        return null;
    }

    public PageReference setCommitDate() {

        sfdcErrors = new List<String>();
        addErrors = new List<String>();
        deleteErrors = new List<String>();
        calloutErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        Date dateCommit = Date.parse(commitDate);
        errors = LMS_ToolsModifyAssignments.setCommitDateRoles(roles, mappings, dateCommit);

        sfdcErrors = errors.get('SFDCEX');
        System.debug('-------------errors------------'+errors);
        System.debug('-------------sfdcErrors-----------'+sfdcErrors);

        if(null != sfdcErrors && sfdcErrors.size() > 0) {
            webServiceError = 'Errors occurred while applying commit date.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    public PageReference commitRoles() {
        Map<String, List<String>> errors = new Map<String, List<String>>();
        sfdcErrors = new List<String>();
        addErrors = new List<String>();
        deleteErrors = new List<String>();

        errors = LMS_ToolsModifyAssignments.commitRoles(roles, mappings);
        sfdcErrors = errors.get('SFDCEX');
        addErrors = errors.get('A');
        deleteErrors = errors.get('D');
        calloutErrors = errors.get('CALLOUT');

        System.debug('-------------errors------------'+errors);
        System.debug('-------------sfdcErrors------------'+sfdcErrors);
        System.debug('-------------addErrors------------'+addErrors);
        System.debug('-------------deleteErrors------------'+deleteErrors);
        System.debug('-------------calloutErrors------------'+calloutErrors);

        if((null != sfdcErrors && sfdcErrors.size() > 0) || (null != addErrors && 0 < addErrors.size()) || 
            (null != deleteErrors && 0 < deleteErrors.size()) || (null != calloutErrors && 0 < calloutErrors.size())) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    public PageReference cancel() {
        sfdcErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.cancelDraftRoles(courseResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }

        search();
        roleSearch();
        viewImpact();
        return null;
    }

    public PageReference addRole() {
        sfdcErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.addRoles(selRoles, roles, mappings, courseResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while adding role to course.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    public PageReference removeRoles() {
        sfdcErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.removeRoles(roles, mappings);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while removing or reverting role from course.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    /** 
     *  Method used to calculate the impact of adding roles to a course 
     */
    public void viewImpact() {

        transient Map<String, String> empMap = new Map<String, String>();
        transient Map<String, String> empMapCommitted = new Map<String, String>();
        transient Integer cToAdd,cToDelete = 0;
        transient Decimal courseDuration = 0;

        if(courseCommitted != null && !courseCommitted.isEmpty()) {
            empMapCommitted = LMS_ToolsDataAccessor.getCommittedMapFromRoleIds(courseCommitted.keySet()); // Map for course committed
        }
        if(courseNoDraft != null && !courseNoDraft.isEmpty()) {
            empMap = LMS_ToolsDataAccessor.getNoDraftMapFromRoleIds(courseNoDraft.keySet()); // Map for no draft
        }
        if(courseDraft != null && !courseDraft.isEmpty()) {        
            cToAdd = LMS_ToolsDataAccessor.getTotalEmployeeCountForAddMappings(courseDraft.keySet(), empMap);
        } else {
            cToAdd = 0;
        }
        if(courseDraftDelete != null && !courseDraftDelete.isEmpty()) {
            cToDelete = LMS_ToolsDataAccessor.getTotalEmployeeCountForDeleteMappings(courseResultId, courseDraftDelete.keySet());
        } else {
            cToDelete = 0;
        }

        System.debug('-------------empMapSize----------------' + empMap.size());
        employeeStart = empMap.size();
        if(null != courseResultId && courseResultId != '') {
            courseDuration = LMS_ToolsDataAccessor.getTotalCourseDurationFromCourseId(courseResultId);
            impactStart = (LMS_ToolsDataAccessor.getTotalCourseDurationFromCourseId(courseResultId)*employeeStart/60).setScale(2);
        } else {
            impactStart = 0;
        }

        employeeChange = cToAdd - cToDelete;
        impactNum = 0;
        if(courseDuration != null) {
            impactNum = (employeeChange*courseDuration/60).setScale(2);
        }
        if(impactNum > 0) {
            impactChange = '+ '+impactNum;
        } else if(impactNum <= 0 || impactNum == null) {
            impactChange = String.valueOf(impactNum);
        }
        impactEnd = (impactStart + impactNum).setScale(2);
        employeeEnd = employeeStart + employeeChange;
    }

    public PageReference showErrors() {
        PageReference pr = new PageReference('/apex/LMS_ProjectCourseError');
        pr.setRedirect(false);
        return pr;
    }
}