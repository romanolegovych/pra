public without sharing class PAWS_Utilities 
{
    public class PAWSException extends Exception {}
    
    public static String formatDate(Date dt, String format)
    {
        if(dt == null) return null;
        return DateTime.newInstance(dt, Time.newInstance(0, 0, 0, 0)).format(format);
    }
    
    public static String getDefaultDateFormat()
    {
        return Date.newInstance(2000, 1, 3).format().replace('2000', 'yyyy').replace('01', 'MM').replace('1', 'M').replace('03', 'dd').replace('3', 'd');
    }
    
    public static String getDefaultDateTimeFormat()
    {
        String result = DateTime.newInstance(2000, 1, 3, 4, 5, 6).format().replace('2000', 'yyyy').replace('01', 'MM').replace('1', 'M').replace('03', 'dd').replace('3', 'd').replace('04', 'HH').replace('4', 'H').replace('05', 'mm').replace('5', 'm').replace('06', 'ss').replace('6', 's').replace('AM', 'a').replace('PM', 'a');
        return result.contains('a') ? result.replace('HH', 'hh').replace('H', 'h') : result;
    }
    
    public static Boolean isLimitException(Exception ex)
    {
        return ex.getTypeName().contains('LimitsException');
    }

    public static void checkLimits(Map<String, Integer> options)
    {
        if(!System.isBatch() && !Test.isRunningTest()) return;
        
        for(String key : options.keySet())
            checkLimit(key, options.get(key));
    }
    
    public static void checkLimit(String limitName, Integer total)
    {
        if(!System.isBatch() && !Test.isRunningTest()) return;
		
        if(limitName == 'Queries' && Limits.getQueries() + total >= Limits.getLimitQueries())
            throw new STSWR1.LimitsException('Queries limit exception!');
        else if(limitName == 'DMLStatements' && Limits.getDMLStatements() + total >= Limits.getLimitDMLStatements())
            throw new STSWR1.LimitsException('DMLStatements limit exception!');
        else if(limitName == 'QueryRows' && Limits.getQueryRows() + total >= Limits.getLimitQueryRows())
            throw new STSWR1.LimitsException('QueryRows limit exception!');
        else if(limitName == 'Callouts' && Limits.getCallouts() + total >= Limits.getLimitCallouts())
            throw new STSWR1.LimitsException('Callouts limit exception!');
        else if(limitName == 'CpuTime' && Limits.getCpuTime() + total >= Limits.getLimitCpuTime())
            throw new STSWR1.LimitsException('CpuTime limit exception!');
        else if(limitName == 'DMLRows' && Limits.getDMLRows() + total >= Limits.getLimitDMLRows())
            throw new STSWR1.LimitsException('DMLRows limit exception!');
        else if(limitName == 'FieldsDescribes' && Limits.getFieldsDescribes() + total >= Limits.getLimitFieldsDescribes())
            throw new STSWR1.LimitsException('FieldsDescribes limit exception!');
    }
    
    public static Map<ID, String> loadAssigners(Map<String, Set<ID>> typeMap)
    {
        Map<ID, String> result = new Map<ID, String>();
        
        checkLimit('Queries', typeMap.size());
        
        if(typeMap.containsKey('Field'))
        {
            for(User record : [select Name from User where Id in :typeMap.get('Field')])
                result.put(record.Id, record.Name);
        }
        
        if(typeMap.containsKey('User'))
        {
            for(User record : [select Name from User where Id in :typeMap.get('User')])
                result.put(record.Id, record.Name);
        }
        
        if(typeMap.containsKey('Role'))
        {
            for(UserRole record : [select Name from UserRole where Id in :typeMap.get('Role')])
                result.put(record.Id, record.Name);
        }
        
        if(typeMap.containsKey('Group'))
        {
            for(Group record : [select Name from Group where Id in :typeMap.get('Group')])
                result.put(record.Id, record.Name);
        }
        
        if(typeMap.containsKey('Chatter Group'))
        {
            for(CollaborationGroup  record : [select Name from CollaborationGroup  where Id in :typeMap.get('Chatter Group')])
                result.put(record.Id, record.Name);
        }

        if(typeMap.containsKey('Profile'))
        {
            for(Profile record : [select Name from Profile where Id in :typeMap.get('Profile')])
                result.put(record.Id, record.Name);
        }
        
        return result;
    }

    private static Map<ID, String> assigneeCache = new Map<ID, String>();
    
    public static Map<ID, String> loadAssigners(List<STSWR1__Flow_Instance_Cursor__c> records)
    {
        Map<ID, String> result = new Map<ID, String>();
        
        Map<String, Set<ID>> typeMap = new Map<String, Set<ID>>();
        for(STSWR1__Flow_Instance_Cursor__c record : records)
        {
            if(String.isEmpty(record.STSWR1__Step_Assigned_To__c)) continue;

            if(!typeMap.containsKey(record.STSWR1__Step_Assign_Type__c)) typeMap.put(record.STSWR1__Step_Assign_Type__c, new Set<ID>());
            
            List<String> assigneeList = (List<String>)record.STSWR1__Step_Assigned_To__c.split(',');
            for(String assignee : assigneeList)
            {
            	if(!(assignee instanceof ID)) continue;
            	
                if(assigneeCache.containsKey((ID)assignee)) result.put((ID)assignee, assigneeCache.get((ID)assignee));
                else typeMap.get(record.STSWR1__Step_Assign_Type__c).add((ID)assignee);
            }
        }
        
        result.putAll(loadAssigners(typeMap));
        return result;
    }

    public static sObjectType getSObjectTypeById(String id)
    {
        String prefix = id.substring(0,3);      
        for(sObjectType objectType : Schema.getGlobalDescribe().values())
        {
            Schema.DescribeSObjectResult describe = objectType.getDescribe();
            if(prefix.equals(describe.getKeyPrefix())) return objectType;
        }
        return null;
    }
    
    public static Object extractValue(sObject obj, String fieldPath)
    {
        fieldPath = fieldPath.replace('.',':');
        String[] splited = fieldPath.split(':',-2);
        for(Integer i = 0; i < splited.size() - 1; i++)
        {
            String fieldName = splited[i];
            obj = obj.getSObject(fieldName);
            if(obj == null) return null;
        }
        return obj.get(splited[splited.size() - 1]);
    }
    
    public static Schema.DescribeFieldResult extractFieldDescribe(Schema.DescribeSObjectResult describe, String fieldPath)
    {
        Map<String, Schema.SObjectField> fieldsMap = describe.fields.getMap();
            
        Schema.DescribeFieldResult tempDescribe = null;
        String[] splited = fieldPath.toLowerCase().split('\\.');
        for(Integer i = 0; i < splited.size(); i++)
        {
            String fieldName = splited[i];
            if(i < splited.size() - 1)
            {
                fieldName = fieldName.replace('__r','__c');
                if(!fieldName.endsWith('__c')) fieldName += 'id';
            }

            Schema.sObjectField field = null;
            for(String key : fieldsMap.keySet())
            {
                if(key.toLowerCase() == fieldName) 
                {
                    field = fieldsMap.get(key);break;
                }
            }
            if(field == null) return null;
   
            tempDescribe = field.getDescribe();
            if(i < splited.size() - 1)
            {
                if(tempDescribe.getReferenceTo().size() == 0) return null;
                
                Schema.DescribeSObjectResult objectDescribe = tempDescribe.getReferenceTo()[0].getDescribe();
                if(objectDescribe == null || !objectDescribe.isAccessible()) return null;
                
                fieldsMap = objectDescribe.fields.getMap();
            }else break;
        }
        
        return tempDescribe;
    }
    
    public static List<sObject> makeQuery(String objectType, String conditions, Set<String> additionalFields)
    {
        String fields = '';
        Schema.DescribeSObjectResult describe = Schema.getGlobalDescribe().get(objectType).getDescribe();
        Set<String> lowerFields = new Set<String>();
        for(String fieldName : describe.fields.getMap().keySet())
        {
            lowerFields.add(fieldName.toLowerCase());
            fields += fieldName.toLowerCase() + ',';
        }
        
        if(additionalFields != null)
        {
            for(String fieldName : additionalFields)
            {
                if(lowerFields.contains(fieldName.toLowerCase())) continue;
                fields += fieldName.toLowerCase() + ',';
            }
        }

        fields = fields.substring(0, fields.length() - 1);
       
        return Database.query('select ' + fields + ' from ' + objectType + (!String.isEmpty(conditions) ? ' where ' + conditions : ''));
    }
    
    public static Boolean IGNORE_PAWS_TRIGGERS;
    public static void updatePAWSFlag(List<STSWR1__Flow_Instance_Cursor__c> records)
    {
        PAWS_Utilities.IGNORE_PAWS_TRIGGERS = false;
        
        Set<String> pawsObjectTypes = new Set<String>{'ecrf__c','paws_project_flow_country__c',
            'paws_project_flow_document__c', 'paws_project_flow_agreement__c','paws_project_flow_site__c',
            'paws_project_flow_submission__c','paws_project_flow_junction__c','stub__c'};
            
        for(STSWR1__Flow_Instance_Cursor__c record : records)
        {
            //system.debug('----record.Flow_Object_Type__c.toLowerCase() = ' + record.Flow_Object_Type__c.toLowerCase());
            if(!pawsObjectTypes.contains(record.Flow_Object_Type__c.toLowerCase()))
            {
                PAWS_Utilities.IGNORE_PAWS_TRIGGERS = true;
                break;
            }
        }
    }
    
    /*public static Boolean hasPermissionSet(String permissionSetName)
    {
        checkLimit('Queries', 1);
        List<PermissionSetAssignment> currentUserPerSets = [select Id from PermissionSetAssignment where PermissionSet.Name = :permissionSetName and AssigneeId = :Userinfo.getUserId()];
        return (currentUserPerSets.size() > 0);
    }*/

    /*
    * Used to substitute system-defined System.today() method and provide custom value
     * to support "time machine" feature.
     */
    public static Date today()
    {
        return System.today();//@TODO: Update to use some custom settings!!!
    }

    /*
    * Used to substitute system-defined System.now() method and provide custom value
     * to support "time machine" feature.
     * References today() method to get Date portion of a Datetime value.
     */
    public static Datetime now()
    {
        return Datetime.newInstance(today(), System.now().time());
    }
}