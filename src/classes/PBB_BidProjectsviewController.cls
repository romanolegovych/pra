/**
@author Bhargav Devaram
@date 2015
@description this controller class is to display the Bid Project and its related object information.  
**/
public class PBB_BidProjectsviewController{
    
    //Defining the object accesses
    public PermissionSets psforPV{get;set;}
    public PermissionSets psforPBBS{get;set;}
    public PermissionSets psforSRMS{get;set;}
    
    //to display the error message on vidsualforce page via javascript
    public String ErrorMsg{get;set;}  
    
    //to select the record to delete   
    public String deleteID {get; set;}
    public String bidId;
    
    /**
@author Bhargav Devaram
@date 2015
@description This is a constructor get the Bid Project and its related object information.  
**/
    public PBB_BidProjectsviewController(ApexPages.StandardController controller) {
        bidId = ApexPages.currentPage().getParameters().get('id');
        //get all the grant objects of read,write,delete,create of objects
        List<PermissionSetAssignment> userPermissionsets = new List<PermissionSetAssignment>();
        List<String> PermLabels = new List<String>();
        for(PermissionSetAssignment  psa:[SELECT Id, PermissionSet.Name,PermissionSet.label,AssigneeId 
                                          FROM PermissionSetAssignment 
                                          WHERE AssigneeId = :UserInfo.getUserId()]){
                                              PermLabels.add(psa.PermissionSet.label);                                                        
                                          }
        System.debug('PermLabels:' + PermLabels);
        psforPV = new PermissionSets ();
        psforPBBS = new PermissionSets ();
        psforSRMS = new PermissionSets ();
        for(ObjectPermissions obp:[SELECT Id, Parent.label, SobjectType, PermissionsRead,Permissionscreate,Permissionsedit,PermissionsDelete,
                                   Parent.PermissionsModifyAllData, ParentId
                                   FROM ObjectPermissions
                                   WHERE (PermissionsRead = true or Permissionscreate= true  or Permissionsedit= true ) 
                                   and SobjectType in ('Project_Vendors__c','PBB_Scenario__c') 
                                   and Parent.label in:PermLabels]){
                                       
                                       if(obp.SobjectType=='Project_Vendors__c'){
                                           if(obp.PermissionsRead==true){
                                               psforPV.canRead = true;
                                           }
                                           if(obp.Permissionscreate==true){
                                               psforPV.canCreate = true;
                                           }
                                           if(obp.Permissionsedit==true){
                                               psforPV.canEdit = true;
                                           }
                                           if(obp.PermissionsDelete==true){
                                               psforPV.canDelete = true;
                                           }
                                       }
                                       if(obp.SobjectType=='PBB_Scenario__c'){
                                           if(obp.PermissionsRead==true){
                                               psforPBBS.canRead = true;
                                           }
                                           if(obp.Permissionscreate==true){
                                               psforPBBS.canCreate = true;
                                           }
                                           if(obp.Permissionsedit==true){
                                               psforPBBS.canEdit = true;
                                           }
                                           if(obp.PermissionsDelete==true){
                                               psforPBBS.canDelete = true;
                                           }
                                       }
                                              
                                   }
        System.debug('obp:' +psforPV+psforPBBS+psforSRMS);
    }
    
    
    /**
@author Bhargav Devaram
@date 2015
@description This method is to delete the records of the bidprojects like project vendors,SRM Scenario and PBB Scenarios.
**/
    public PageReference  deleteRelatedObjectRecord() {
        ErrorMsg = null;   
        PageReference pageRef =null; 
        try{             
            system.debug('-------deleteID ----'+deleteID );
            Schema.SObjectType sobjectType = id.valueof(deleteID).getSObjectType();
            String sobjectName = sobjectType.getDescribe().getName();
            system.debug('-------sobjectType ----'+sobjectType +sobjectName );
            //List<PBB_Scenario__c> pbbsclist = PBB_DataAccessor.getPbbScenariobySRMScenarioID(deleteID);                  
            SObject record = Database.query('Select Id, Name,Bid_Project__c From '+ sobjectName + ' Where Id = :deleteID');
            system.debug('-------record ----'+record );
            
            if(sobjectName == 'PBB_Scenario__c'){
                    Id idp = Id.valueOf(deleteID);
                    pbb_scenario__c pbbScenario = [select id,name,bid_project__c from pbb_scenario__c where id = :idp ];
                    System.debug('-------pbb Scenario----------'+pbbScenario);
                    list<pbb_scenario_country__c> pbbCount = new list<pbb_scenario_country__c>();
                    list<SRM_Scenario_Country__c> srmCountry = new list<SRM_Scenario_Country__c>();
                    list<SRM_Questions_Response__c> queRes = new list<SRM_Questions_Response__c>();
                    list<Countries_Service_Tasks__c> countrServtask =  new list<Countries_Service_Tasks__c>();
                    list<Service_Task_Drivers__c> servTskFor = new list<Service_Task_Drivers__c>();
                    list<Service_Task_Responses__c> servTskRes = new list<Service_Task_Responses__c>();
                    
                    //list of releted records of pbb scenario
                    pbbCount = PBB_DataAccessor.getpbbScenarioCountryByScenarioID(pbbScenario.id);
                    srmCountry = PBB_DataAccessor.getSrmCountriesgvnPBBScenarioid(pbbScenario.id);
                    queRes = PBB_DataAccessor.getQuestions(pbbScenario.id);
                    countrServtask = PBB_DataAccessor.GetCountryServiceTasksByScenario(pbbScenario.id);
                    servTskFor = PBB_DataAccessor.getServicetskFormulaById(pbbScenario.id);
                    servTskRes = PBB_DataAccessor.getservTskResponseByPbbId(pbbScenario.Id);
                    
                    //deletion of related records of pbb scenario
                    delete queRes ;
                    delete servTskFor ;
                    delete servTskRes ;
                    delete countrServtask ;
                    delete srmCountry ;
                    delete pbbCount ;
                    
            }
            delete record;
            System.debug('--------------record-------------------'+record);
            pageRef=new PageReference('/'+ApexPages.currentPage().getParameters().get('id'));                               
            pageRef.setRedirect(false);  
            }catch (Exception e) {
                ErrorMsg = e.getMessage() ;
             }  
            return  pageRef ;
      }
    
     /**
    @author Niharika Reddy
    @date 2015
    @description This redirects to the Scenario Compare page once the Compare button is clicked.
    **/
    public PageReference compareRedirect(){
        PageReference reRend = new PageReference('/apex/PBB_scenarioCompare?id='+bidId);
        reRend.setRedirect(true);
        return reRend;
    }
    
    
    /**
@author Bhargav Devaram
@date 2015
@description This is the  Wrapper class to hold the data to display in pageblock tables
**/
    public class PermissionSets{
        public Boolean canCreate{get;set;}
        public Boolean canRead{get;set;}
        public Boolean canEdit{get;set;}
        public Boolean canDelete{get;set;}
        
        PermissionSets(){
            canCreate = false;
            canRead = false;
            canEdit = false;
            canDelete = false;
        }
    }    
}