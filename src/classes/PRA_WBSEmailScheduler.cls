/**
*   'PRA_WBSEmailScheduler' class is to initiate the PRA_WBSEmailBatch tosend the Emails to Financial Analysts (FA's)
*   for Work Breakdown Structure(WBS) of a project.
*   @author   Devaram Bhargav
*   @version  06-Dec-2013
*   @since    06-Dec-2013
*/
global class PRA_WBSEmailScheduler implements Schedulable{
   
    //Execute method is to get all the permissionSet labels and initate the PRA_WBSEmailBatch by inserting a record into Batch Queue object
    global void execute(SchedulableContext sc) {
        
        //make a map with all the values in the custom setting object , which also has the permission set names.
        Map<String,PMC_ProfileGroup__c> ProfileSelectList = PMC_ProfileGroup__c.getall() ;
        system.debug('-------ProfileSelectList----'+ProfileSelectList);        
        Set<String> ProfileSelectSet =  ProfileSelectList.keyset();
        
        //Pass all the permissionset labels to batch queue object as s JSON string.
        String paramsJSONString ='["'+ProfileSelectSet+'"]';       
        
        //Insert the batch queue record with required parameters.
        PRA_Batch_Queue__c PRA_WBSEmailBatchQueue = new PRA_Batch_Queue__c(Batch_Class_Name__c = 'PRA_WBSEmailBatch',Parameters_JSON__c = paramsJSONString,
                                                                            Status__c ='Waiting',Priority__c = 5);
        insert PRA_WBSEmailBatchQueue ;
    }
}