@istest
public with sharing class BDT_FinancialServicePricingContrTest {
	
	public static Client_Project__c firstProject 	{get;set;}
	public static list<Study__c> studiesList 		{get;set;}
	public static list<Business_Unit__c> myBUList	{get;set;}
	public static list<Site__c> mySiteList  		{get;set;}
	public static list<ServiceCategory__c> scList	{get;set;}
	public static list<Service__c> serviceList		{get;set;}
	public static ProjectService__c ps				{get;set;}
	public static StudyService__c sts				{get;set;}
	
	
	
static void init(){
		firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;
		
		myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert myBUList;
		
		
		studiesList = BDT_TestDataUtils.buildStudies(firstProject);
		studiesList[0].Business_Unit__c = myBUList[0].Id; 
		studiesList[1].Business_Unit__c = myBUList[1].Id; 
		studiesList[2].Business_Unit__c = myBUList[2].Id;
		studiesList[3].Business_Unit__c = myBUList[3].Id; 
		studiesList[4].Business_Unit__c = myBUList[4].Id;
		insert studiesList;
		string selectedStudies ='';
		for(Study__c st: studiesList){
			selectedStudies += st.Id+':';
		}
		
		scList = BDT_TestDataUtils.buildServiceCategory(1, null);
		upsert sclist;
		
		serviceList = BDT_TestDataUtils.buildService(scList, 1, 'ServiceTest-', false);
		system.debug('serviceList: '+serviceList);
		insert servicelist;
								
		BDT_Utils.setPreference('SelectedProject', firstProject.id);
		BDT_Utils.setPreference('SelectedStudies',selectedStudies);
		
		BDT_TestDataUtils.createRateCardClass();
		
		ps = new ProjectService__c(Client_project__c = firstProject.id, service__c = serviceList[0].id);
		insert ps;
		
		sts = new studyService__c(projectservice__c =ps.id, study__c = studiesList[0].Id, NumberOfUnits__c = 10 );
		insert sts;
	}

 	static testMethod void myUnitTest() {
 		init();
 		BDT_FinancialServicePricingController a = new BDT_FinancialServicePricingController();
 		
 		PageReference pageRef = New PageReference(System.Page.BDT_FinancialServicePricing.getUrl());
        pageref.getParameters().put('CatNum','0');
        pageref.getParameters().put('RowNum','0');
        pageref.getParameters().put('ColNum','0');
        
        Test.setCurrentPage(pageRef);
        
        a.editStudyServicePricing();
        a.cancelEdit();
        a.editStudyServicePricing();
      //  a.saveStudyServicePricing();
        
        a.exportToExcel();
        
 	}
}