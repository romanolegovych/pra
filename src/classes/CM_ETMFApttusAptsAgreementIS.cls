/**
 *	@author		Sukrut Wagh
 *	@date		07/07/2014
 *	@description	Service responsible for integrating sObject=Apttus__APTS_Agreement__c with ETMF system
 *					INT_SETUP_STATUS__c.Unique_Key__c=Apttus__Agreement_Document__c.Id
 *					Ex:	a7wL00000008OIvIAM
 *	
 */
public class CM_ETMFApttusAptsAgreementIS extends COM_BaseIntegrationService {

	private static final String CLASSNAME = 'CM_ETMFApttusAptsAgreementIS';
	private static final COM_Logger LOGGER = new COM_Logger(CLASSNAME); 

	public CM_ETMFApttusAptsAgreementIS() {
		super();
	}
	
	public CM_ETMFApttusAptsAgreementIS(final String intSystem, final String sObjType) {
		super(intSystem,sObjType);
	}
	
	/**
	 *	@description Returns the query that will fetch the records to be processed for this integration.
	*/
	public String getProcessingQuery(){
		String methodName = 'getProcessingQuery';
		LOGGER.entry(methodName);
		String query = getAgreementDocsToProcessQuery();
		LOGGER.info(query);
		LOGGER.exit(methodName);
		return query;
	}
	
	/**
	* @description Fetch Apttus__Agreement_Document__c records with record type=Agreement, status=Fully Executed, Disable_CTA_to_eTMF__c=false,
	* 			   exclude records synched successfully & in-transit for x amount of time.
	*/
	private String getAgreementDocsToProcessQuery() {
		String query = 'SELECT Id,Apttus__Agreement__r.Id,Apttus__Agreement__r.PRA_Status__c,Apttus__Agreement__r.Contract_Type__c, ';
		query += ' Apttus__Agreement__r.Site__r.Investigator__r.LastName,Apttus__Agreement__r.Site__r.Investigator__r.FirstName, ';
		query += ' Apttus__Agreement__r.Site__r.Country__c, ';
		query += ' Apttus__Agreement__r.Site__r.Project_Protocol__r.Name, ';
		query += ' LastModifiedDate, Apttus__URL__c ';
		query += ' FROM Apttus__Agreement_Document__c ';
		query += ' WHERE Apttus__Agreement__r.RecordType.Name= \''+CM_Constants.AGREEMENT_RECORD_TYPE_AGREEMENT+'\' ';
		query += ' AND Apttus__Agreement__r.Site__r.Project_Protocol__r.Disable_CTA_to_eTMF__c=false ';
		query += ' AND Apttus__Agreement__r.PRA_Status__c=\''+CM_Constants.AGREEMENT_STATUS_FULLY_EXECUTED+'\' ';
		
		List<INT_SETUP_STATUS__c> setupStatuses = COM_SObjectUtils.getSelectedFieldsByDynamicSOQL(COM_IntegrationUtils.getIntSetupStatusSelectFields(),
			COM_IntegrationConstants.API_INT_SETUP_STATUS, getSetupStatusesExcludeWhereClause(), Limits.getLimitQueryRows());
		
		if(COM_Utils.isNotEmpty(setupStatuses)) {
			List<String> agrDocIds = new List<String>();
			for(INT_SETUP_STATUS__c setupStatus : setupStatuses) {
				agrDocIds.add(setupStatus.Unique_Key__c);
			}
			String agrDocIdsStr = COM_Utils.toString(agrDocIds, ',', '\'');
			query += ' AND Id NOT IN ('+agrDocIdsStr+')';
		}
		return query;
	}
	
	/**
	 *  @description	Returns a filter criteria to fetch records synched up successfully & which are in transit for x amount of time.
	 *					This makes sure that we are not processing:
	 *					1. successfully synched up records
	 *					2. records which are in transit for x amount of time
	*/
	private String getSetupStatusesExcludeWhereClause() {
		List<String> transitStatuses = new List<String>();
		transitStatuses.add(COM_IntegrationConstants.STATUS_TRANSIT_ADD);
		transitStatuses.add(COM_IntegrationConstants.STATUS_TRANSIT_UPDATE);
		transitStatuses.add(COM_IntegrationConstants.STATUS_TRANSIT_DELETE);
		String transitStatusesStr = COM_Utils.toString(transitStatuses, ',', '\'');
		
		Datetime modifiedBefore = Datetime.now().addHours(CM_Constants.ETMF_AGREEMENT_IN_TRANSIT_HOURS);
		String formattedDt = modifiedBefore.format(COM_Constants.DTF_ISO_8601);
		String query = '';
		query += ' IntegrationSetup__r.System__r.System__c = \''+getIntSystem()+'\'';
		query += ' AND IntegrationSetup__r.Type_API_Name__c = \''+getSObjectType()+'\'';
		query += ' AND ( ';
		query += ' IntegrationStatus__r.Status__c IN (\''+COM_IntegrationConstants.STATUS_INSYNC+'\') OR ';
		query += ' (IntegrationStatus__r.Status__c IN ('+transitStatusesStr+') AND LastModifiedDate < '+formattedDt+' )';
		query += ' ) ';
		
		return query;
	}
	
	public void prepare(){
		
	}
	
	/**
	 * @description Posts the documents asynhronously via callout and persists the setup status for each record.
	 * @param document records to be posted  
	 */
	public COM_ServiceResult invoke(List<sObject> scope){
		COM_ServiceResult result = new COM_ServiceResult();
		if(COM_Utils.isNotEmpty(scope) && (Apttus__Agreement_Document__c.sObjectType == scope.getSObjectType())) {
			result.setRequest(scope);
			List<Exception> errors = new List<Exception>();
			INT_STATUS__c status = null;
			try {
				status = COM_IntegrationUtils.getIntStatus(COM_IntegrationConstants.STATUS_TRANSIT_ADD);
				CM_DocumentServiceWrapper.postDocumentsAsync(CLASSNAME, scope);
				LOGGER.info('Documents posted successfully');
			} catch(COM_Exception ex) {
				LOGGER.error(ex);
				errors.add(ex);
				status = COM_IntegrationUtils.getIntStatus(COM_IntegrationConstants.STATUS_ERROR);
			} finally {
				result.setErrors(errors);
				upsertIntStatus(result,status);
			}
		}
		return result;
	}
	
	/**
	 *	@description Upserts the INT_SETUP_STATUS__c for Apttus__Agreement_Document__c
	 *	@param	records for which INT_SETUP_STATUS__c will be upserted
	 *	@param	records updated with status 
	*/
	private void upsertIntStatus(COM_ServiceResult result, INT_STATUS__c status) {
		if(COM_Utils.isNotEmpty(result)) {
			List<Apttus__Agreement_Document__c> scope = (List<Apttus__Agreement_Document__c>)result.getRequest();
			if(COM_Utils.isNotEmpty(scope)) {
				List<INT_SETUP_STATUS__c> setupStatuses = new List<INT_SETUP_STATUS__c>();
				INT_SETUP_STATUS__c setupStatus = new INT_SETUP_STATUS__c();
				INT_SETUP__c setup = COM_IntegrationUtils.getSetupsForSObjectTypeAndSystem(null,getSObjectType(),getIntSystem());
				for(Apttus__Agreement_Document__c agrDoc: scope) {
					setupStatus.IntegrationStatus__c = status.Id;
					setupStatus.IntegrationSetup__c = setup.Id;
					setupStatus.Unique_Key__c = agrDoc.Id;
					if(COM_Utils.isNotEmpty(result.getErrors())) {
						setupStatus.Exception__c = result.getErrors()+'';
					}
				}
				upsert setupStatuses;
				LOGGER.info('Upserted INT_SETUP_STATUS__c for Apttus__Agreement_Document__c with status:'+status.Status__c+', size:'+scope.size());
			}
		}
	}
	
	public Boolean processResult(List<COM_ServiceResult> results){
		if(COM_Utils.isNotEmpty(results)) {
			for(COM_ServiceResult result:	results) {
				
			}
		}
		return true;
	}
	
	public String getEmailRecipients() {
		return CM_Constants.ETMF_AGREEMENT_EMAIL_RECIPIENT;
	}

}