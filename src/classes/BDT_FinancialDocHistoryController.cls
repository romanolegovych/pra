public with sharing class BDT_FinancialDocHistoryController {
	public List<FinancialDocumentHistory__c> 	finHistoryList		 {get;set;}
	public FinancialDocument__c 	  			financialDocument 	 {get;set;}
	public string 								financialDocumentID	 {get;set;} 
	public string								selectedFilterMethod {get;set;}
	public Boolean								showPage 			 {get;set;}
	
	public BDT_FinancialDocHistoryController() {
		showPage = true;
		financialDocumentID = system.currentPageReference().getParameters().get('financialDocumentID');
		if(String.isBlank(financialDocumentID)) {
			errorOnReadingFinancialDocument();
		} else {
			financialDocument = FinancialDocumentService.getFinancialDocumentById(financialDocumentID);
			if( String.isBlank(financialDocument.Name) ) {
				errorOnReadingFinancialDocument();
				return;
			}
			selectedFilterMethod = 'All';
			changeFilterSelection();
		}
	}
	
	
	/**
	*	Give an error message in case the financial document is not found.
	*	@author   Dimitrios Sgourdos
	*	@version  09-Sep-2013
	*/
	private void errorOnReadingFinancialDocument() {
		showPage = false;
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected financial document.Please, select a document and reload the page.');
		ApexPages.addMessage(msg);
	}
	
	
	/**
	*	Filter the history of the selected financial document.
	*	@author   Dimitrios Sgourdos
	*	@version  09-Sep-2013
	*/
	public void changeFilterSelection() {
		String tmpProcessStep = (selectedFilterMethod=='ALL')? NULL : selectedFilterMethod;
		finHistoryList = FinancialDocumentHistoryDataAccessor.getFinancialDocumentHistoryByProcessStep(
																								financialDocumentID,
																								tmpProcessStep,
																								'LastModifiedDate',
																								true);
	}
	
	
	/**
	*	Create the radio buttons options for searching the history of the selected financial document
	*	@author   Dimitrios Sgourdos
	*	@version  09-Sep-2013
	*	@return   The radio buttons options
	*/
	public list<SelectOption> getFilterOptions() {
		list<SelectOption> FilterOptions = new list<SelectOption>();
		FilterOptions.add(new SelectOption('All','All'));
		FilterOptions.add(new SelectOption('Study Selection','Study Selection'));
		FilterOptions.add(new SelectOption('Price','Price'));
		if(financialDocument.DocumentType__c == 'Proposal') { // Hide these two options in case of the document is 'Change Order'
			FilterOptions.add(new SelectOption('Payment','Payment'));
			FilterOptions.add(new SelectOption('Content','Content'));
		}
		FilterOptions.add(new SelectOption('Approval','Approval'));
		FilterOptions.add(new SelectOption('Output','Output'));
		return FilterOptions;
	}
	
	
	/**
	*	Hide the content of the page and let only the progress bullets to be visible.
	*	@author   Dimitrios Sgourdos
	*	@version  09-Sep-2013
	*/
	public void closePage() {
		showPage = false;
	} 	
}