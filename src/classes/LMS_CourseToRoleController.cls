public class LMS_CourseToRoleController {
	
    public String val{get;private set;}
    public String praPath{get;private set;}
    public String projectPath{get;private set;}
    public String adhocPath{get;private set;}
    public Boolean renderPRATab{get;private set;}
    public Boolean renderPSTTab{get;private set;}
    public Boolean renderAdhocTab{get;private set;}
    
    public LMS_CourseToRoleController(){
    	checkPermissions();
        checkParams();
    }
    
    private void checkPermissions() {
    	renderPRATab = true;
		renderPSTTab = true;
		renderAdhocTab = true;
		
		String profileName = LMS_ToolsDataAccessor.getProfileNameFromId(UserInfo.getProfileId());
		Boolean isPRAAdhocAdmin = LMS_ToolsDataAccessor.isPRAAdhocAdmin(profileName);
		Boolean isPRAAdmin = LMS_ToolsDataAccessor.isPRAAdmin(profileName);
		Boolean isPSTAdmin = LMS_ToolsDataAccessor.isPSTAdmin(profileName);
		Boolean isAdhocAdmin = LMS_ToolsDataAccessor.isAdhocAdmin(profileName);
		
		if(isPRAAdhocAdmin) {
			renderPSTTab = false;
    	} else if(isPRAAdmin) {
			renderPSTTab = false;
			renderAdhocTab = false;
		} else if(isPSTAdmin) {
			renderPRATab = false;
			renderAdhocTab = false;
		} else if(isAdhocAdmin) {
			renderPRATab = false;
			renderPSTTab = false;
		}
    }
    
    private void checkParams(){
    	praPath = '/apex/LMS_CourseToPRA';
        projectPath = '/apex/LMS_CourseToProject';
        adhocPath = '/apex/LMS_CourseToAdhoc';
        String id,client,project,role,adhocName,roleId = '';
        
        id = ApexPages.currentPage().getParameters().get('id');
        client = ApexPages.currentPage().getParameters().get('client');
        project = ApexPages.currentPage().getParameters().get('project');
        role = ApexPages.currentPage().getParameters().get('class');
        adhocName = ApexPages.currentPage().getParameters().get('adhocName');
        roleId = ApexPages.currentPage().getParameters().get('roleId');
        
        System.debug('-----------------------------'+id);
        System.debug('-----------------------------'+client);
        System.debug('-----------------------------'+project);
        System.debug('-----------------------------'+role);
        System.debug('-----------------------------'+adhocName);
        System.debug('-----------------------------'+roleId);
        
        if(client != null || project != null || role != null){
            projectPath += '?';
        }
        if(id == 'p'){
            val = 'Project';
            if(client != null && project != null){
                projectPath += 'client=' + client + '&project=' + project;
            }else if(client != null && project == null){
                projectPath += 'client=' + client;
            }else if(client == null && project != null){
                projectPath += 'project=' + project;
            }
            if(role != null)
                projectPath += '&class=' + role;
        } else if (id == 'a'){
            val = 'Adhoc';
            if(adhocName != null){
                adhocPath += '?adhocName=' + adhocName;
            }
        } else if (id == 'pra') {
        	val = 'PRA';
        	if(null != roleId) {
        		praPath += '?roleId=' + roleId;
        	}
        } else if (id == 'pst') {
        	val = 'Project';
        	if(null != roleId) {
        		projectPath += '?roleId=' + roleId;
        	}
        } else if (id == 'adhoc') {
        	val = 'Adhoc';
        	if(null != roleId) {
        		adhocPath += '?roleId=' + roleId;
        	}
        } else { val = 'PRA'; }
        
        System.debug('-----------------------------'+praPath);
        System.debug('-----------------------------'+projectPath);
        System.debug('-----------------------------'+adhocPath);
        System.debug('-----------------------------'+val);
    }
}