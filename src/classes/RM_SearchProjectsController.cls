global with sharing class RM_SearchProjectsController {
    public string searchOn{get;set;}
    public string searchTextStyle{get;set;}
    public string strSearchText{get;set;}
    public string selSearch{get;set;}
    public string strTheropeticArea{get;set;}
    public string[] selectedStatus{get;set;}
    public string[] selectedPhase{get;set;}
    public string startFDate{get; set;}
    public string startEDate{get; set;}
    public string toFDate{get; set;}
    public string toEDate{get; set;}
    public string bidFDate{get; set;}
    public string bidEDate{get; set;}
    public boolean bDisplayResult{get; set;}
    public boolean bShowerror{get; set;}
    public boolean bProtocol{get; set;}
    
    public string searchObject{get; set;}
    public string searchfield{get; set;}
    public list<projectResult> searchResult{get;set;}
   // public list<WFM_Project__c> results{get;private set;}
  
    public string selectedRecord{get; set;}
    
    public RM_SearchProjectsController(){
        initVals();
        bDisplayResult = false;
        bShowerror = false;
        bProtocol = false;
        initSavedSearch();
    }

    public List<SelectOption> getSearchOnItems(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Project ID','Project ID'));
        options.add(new SelectOption('Client/Sponsor Name','Client/Sponsor Name'));
          
       return options;
    }
    public List<SelectOption> getprojectStatus(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',''));
        options.add(new SelectOption('Bid','Bid'));
        options.add(new SelectOption('Lost','Lost'));
        options.add(new SelectOption('Active','Active'));
        options.add(new SelectOption('Closed','Closed'));
        
       return options;
    }
    public List<SelectOption> getTheropeticAreas(){       
         
         List<SelectOption> options = RM_LookUpDataAccess.getTheropeticAreas(true);
        
        return options;
    } 
    public List<SelectOption> getphase(){       
         
         List<SelectOption> options = RM_LookUpDataAccess.getPhase(true);
        
        return options;
    } 
    private void initVals(){
        
         searchOn = 'Project ID';
         searchTextStyle='WaterMarkedTextBox';
         strSearchText = 'Enter a project ID';
         strTheropeticArea='';
         List <string> status = new List<string>();
         status.add(''); 
         selectedStatus = status;
         List <string> selPhase = new List<string>();
         selPhase.add(''); 
         selectedPhase = selPhase;
        
         searchObject = 'WFM_Project__c';
         searchfield='Name';
        
    }
    private void initSearchText(){
         
        if (searchOn=='Project ID'){
             strSearchText = 'Enter a project ID';
         
        }
        else{
             strSearchText = 'Enter a client Name';
      
        }
    }
    private void initSavedSearch(){
        string searchId = ApexPages.currentPage().getParameters().get('cid');
        system.debug('-------------searchId----------------' + searchId);
        if(searchId != ''){
            list<Search_Detail__c> searchDetails = new list<Search_Detail__c>();
            string strValue;
            string[] status = new list<string>();
            string[] phase = new list<string>();
            
            initSearchText();
            searchDetails = RM_OtherService.getSavedSearchDetail(searchId);
            for(Integer i = 0; i < searchDetails.size(); i++){
                strValue = string.valueof(searchDetails[i].get('Search_Value__c'));
                
                
                if(searchDetails[i].get('Name') == 'Project ID'){
                        searchOn = 'Project ID';
                        strSearchText = string.valueof(searchDetails[i].get('Search_Value__c'));
                        system.debug('------------strSearchText--------------'+strSearchText);            
                        searchTextStyle='NormalTextBox';                        
                }
                if(searchDetails[i].get('Name') == 'Client/Sponsor Name'){
                        searchOn = 'Client/Sponsor Name';
                        strSearchText = string.valueof(searchDetails[i].get('Search_Value__c'));
                        system.debug('------------strSearchText--------------'+strSearchText);            
                        searchTextStyle='NormalTextBox';                        
                }
                if(searchDetails[i].get('Name') == 'status'){
                        status.add(strValue);
                        selectedStatus = status;
                }
                if(searchDetails[i].get('Name') == 'startFromDate'){
                        strValue = string.valueof(searchDetails[i].get('Search_Value__c'));
                        startFDate = strValue;
                }
                if(searchDetails[i].get('Name') == 'startToDate'){
                        strValue = string.valueof(searchDetails[i].get('Search_Value__c'));
                        startEDate = strValue;
                }
                if(searchDetails[i].get('Name') == 'endFromDate'){
                        strValue = string.valueof(searchDetails[i].get('Search_Value__c'));
                        toFDate = strValue;
                }
                if(searchDetails[i].get('Name') == 'endToDate'){
                        strValue = string.valueof(searchDetails[i].get('Search_Value__c'));
                        toEDate = strValue;
                }
                if(searchDetails[i].get('Name') == 'bidFromDate'){
                        strValue = string.valueof(searchDetails[i].get('Search_Value__c'));
                        bidFDate = strValue;
                }
                if(searchDetails[i].get('Name') == 'bidToDate'){
                        strValue = string.valueof(searchDetails[i].get('Search_Value__c'));
                        bidEDate = strValue;
                }
                if(searchDetails[i].get('Name') == 'theropetic'){
                        strTheropeticArea = string.valueof(searchDetails[i].get('Search_Value__c'));
                }
                if(searchDetails[i].get('Name') == 'phase'){
                        phase.add(strValue);
                        selectedPhase = phase;
                }
            }
        }
    }
    public  PageReference SearchType(){
        system.debug('-----searchOn---------' + searchOn); 
        if (searchOn=='Project ID'){
             strSearchText = 'Enter a project ID';
             searchObject = 'WFM_Project__c';
             searchfield='Name';
        }
        else{
             strSearchText = 'Enter a client Name';
             searchObject = 'WFM_Client__c';
             searchfield='Name';
        }
        return null;
        
        
    }
   
    public PageReference export(){
        PageReference pageRef=new PageReference('/apex/RM_ExcelProjectList');
        pageRef.setRedirect(false);
        return pageRef; 
    }
    public class projectResult{
        
     //   public WFM_Project__c proj {get; private set;} 
        public string projectID{get; private set;}
        public string sponsor{get; private set;}
        public string status{get; private set;}
        public string startDate{get; private set;}
       // public date strStartDate{get;private set;}
        public string endDate{get; private set;}
       // public string strEndDate{get; private set;}
      //  public string budget{get; private set;}
        public string director{get; private set;}
        public string manager{get; private set;}
        public string therapeutic{get;private set;}
        public string phase{get; private set;}
        public string rID{get; private set;}
        
        public projectResult(WFM_Project__c proSearch, string strTheropeticArea){
        
            
           // proj = proSearch.clone(true);
            rID = proSearch.id;
            projectID = proSearch.name;
            sponsor= proSearch.contract_id__r.client_id__r.name;
            status = proSearch.Status_Desc__c;
            StartDate = RM_Tools.getFormattedDate(proSearch.Project_Start_Date__c, 'mm/dd/yyyy');
          //  strStartDate = proSearch.Project_Start_Date__c;
            endDate = RM_Tools.getFormattedDate(proSearch.Project_End_Date__c, 'mm/dd/yyyy');
          //  strEndDate=String.valueOf(proSearch.Project_End_Date__c);
          //  budget = RM_Tools.get2DecimalinStr(proSearch.Total_Budget__c.format());
            
            director = proSearch.Director_Project_Delivery__c;
            manager = proSearch.Global_Project_Manager__c;
            
            transient List<WFM_Protocol__c> protocols = proSearch.Protocols__r;
            system.debug('------size------'+protocols.size());
            if (protocols.size() > 0){
                if (strTheropeticArea != null && strTheropeticArea != ''){
                    for (WFM_Protocol__c protocol : protocols){
                        if (strTheropeticArea == protocol.Therapeutic_Area__c){
                            therapeutic = protocol.Therapeutic_Area__c;
                            phase = protocol.Phase__c;
                        }
                    }
                    
                }
                else{
                
                    therapeutic = protocols[0].Therapeutic_Area__c;
                    phase = protocols[0].Phase__c;
                }
            }
        }
    }
    public void search(){
        
        set<string> setStatusWithoutEmptyValue = new set<string>();
        set<string> setPhaseWithoutEmptyValue = new set<string>();
        string sqlFilter = '';
        
        string sqlCre = '';
        string sql = 'select name, id, contract_id__r.Client_ID__r.name, Project_Start_Date__c, Project_End_Date__c, Director_Project_Delivery__c, Global_Project_Manager__c, status_desc__c, ' 
                +' (select Therapeutic_Area__c, phase__c from protocols__r)' 
                +' from WFM_Project__c where ';
                
        system.debug('------strSearchText------'+strSearchText);
        if (strSearchText.indexOf('Enter') == -1){ //new search
            if (searchOn == 'Project ID'){
                if (selSearch == strSearchText)
                    sqlFilter = ' name = \'' + string.escapeSingleQuotes(strSearchText) +'\' and ';
                else
                    sqlFilter = ' name like \'' + string.escapeSingleQuotes(strSearchText) +'%\' and ';
            }
            else{ // search client
                if (selSearch == strSearchText)
                    sqlFilter = ' contract_id__r.client_id__r.name = \'' + string.escapeSingleQuotes(strSearchText) +'\' and ';
                else
                    sqlFilter = ' contract_id__r.client_id__r.name like \'' + string.escapeSingleQuotes(strSearchText) +'%\' and ';
            }   
        }
        system.debug('------strTheropeticArea------' + strTheropeticArea);
        if (startFDate.length() > 0){
            Date sFDate = RM_Tools.GetDateFromString(startFDate, 'mm/dd/yyyy');
            sqlFilter += 'Project_Start_Date__c >= :sFDate and '; 
        }
        if (startEDate.length() > 0){
            Date sEDate = RM_Tools.GetDateFromString(startEDate, 'mm/dd/yyyy');
            sqlFilter += 'Project_Start_Date__c <= :sEDate and ' ;
        }
        if (toFDate.length() > 0){
            Date tFDate = RM_Tools.GetDateFromString(toFDate, 'mm/dd/yyyy');
            sqlFilter += 'Project_End_Date__c >= :tFDate and '; 
        }
        if (toEDate.length() > 0){
            Date tEDate = RM_Tools.GetDateFromString(toEDate, 'mm/dd/yyyy');
            sqlFilter += 'Project_End_Date__c <= :tEDate and ' ;
        }
        if (bidFDate.length() > 0){
            Date bFDate = RM_Tools.GetDateFromString(bidFDate, 'mm/dd/yyyy');
            sqlFilter += 'Bid_Defense_Date__c >= :bFDate and '; 
        }
        if (bidEDate.length() > 0){
            Date bEDate = RM_Tools.GetDateFromString(bidEDate, 'mm/dd/yyyy');
            sqlFilter += 'Bid_Defense_Date__c <= :bEDate and ' ;
        }
        if(selectedStatus!=null && selectedStatus.size()>0){
            for(integer i=0;i<selectedStatus.size();i++){
              if(selectedStatus[i]!='' && selectedStatus[i]!=null)
                    setStatusWithoutEmptyValue.add(selectedStatus[i]);
           }
        
            if (setStatusWithoutEmptyValue.size() > 0){
                sqlFilter += ' Status_Desc__c in :setStatusWithoutEmptyValue and ';
            }
        }
        system.debug('------bProtocol------'+bProtocol);
        
        
        if (bProtocol){
            sqlFilter += ' id in (select project_id__c from WFM_Protocol__c where ';
            if (selectedPhase != null && selectedPhase.size() > 0){
                for(integer i=0;i<selectedPhase.size();i++){
                  if(selectedPhase[i]!='' && selectedPhase[i]!=null)
                        setPhaseWithoutEmptyValue.add(selectedPhase[i]);
               }
            
                if (setPhaseWithoutEmptyValue.size() > 0){
                    sqlFilter += ' Phase__c in :setPhaseWithoutEmptyValue and ';
                }
                
            }
            if (strTheropeticArea != null && strTheropeticArea!= ''){
                sqlFilter += ' Therapeutic_Area__c = :strTheropeticArea and ';
            }
            
            sqlCre += sqlFilter.substring(0, sqlFilter.length()-4) + ')';
            system.debug('------strTemp------'+sqlCre);
            
        }
        // make final query
        else{
            Integer n = sqlFilter.length();
            sqlCre = sqlFilter.substring(0, n-4);
        }   
        system.debug('------sqlCre------'+sql+sqlCre);
        searchResult= new list<projectResult>();
        list<WFM_Project__c>results = database.query(sql+sqlCre+' Order by name limit 1000');        
         if (results.size() < 1000){
               if (results.size() > 0){
                system.debug('------resultssize------'+results.size());
        
                    for (WFM_Project__c r:results){
                        searchResult.add(new projectResult(r, strTheropeticArea));
                
                    }
                }
            
                bDisplayResult = true;
                bShowerror = false;
            }
            else{
                bShowerror = true;
                bDisplayResult = false;
            }
            system.debug('------searchResult size------'+searchResult.size());
        
    }
   
    /********
    Save search
    **********/
    public string saveSubFolder {get; set;}
    public string searchName {get; set;}
    public string saveSubId{get;set;}
    public string rootId{get;set;}
 
    private string strIndications{get;set;}

    private Search_Detail__c detail{get;set;}
    
    public list<SelectOption> getResourceFolders(){
        transient List<SelectOption> options = RM_LookUpDataAccess.GetSavedFolder('Projects');
       
        return options;
    }
    @RemoteAction
    global static boolean CheckSaveSearch(string searchName, string folderID){
         string userId = UserInfo.getUserId();
        
         list<Saved_Search__c> searchNameLookup = RM_OtherService.getSavedSearchName(searchName,userId, folderID );
          system.debug('---------searchNameLookup---------'+searchNameLookup);
         if (searchNameLookup.size() > 0)
            return true;
         else
            return false;
    }
    
    public PageReference saveSearch() {
        Integer i;
        string userId = UserInfo.getUserId();
        system.debug('---------saveSubFolder---------'+saveSubFolder);
        if (saveSubFolder == '' || saveSubFolder== null){
            RM_OtherService.insertDefaultSubFolder(userId);
            // Get saveSubFolder id
            Sub_Folder__c sub = RM_OtherService.GetSubFolderID('My Project Searches', 'Projects', UserInfo.getUserID());
            if (sub != null && sub.id != null)
                saveSubFolder = sub.id;
        }
        
        if(saveSubFolder != null){
           
            
            list<Saved_Search__c> searchNameLookup = RM_OtherService.getSavedSearchName(searchName,userId, saveSubFolder );
            if (searchNameLookup.size() > 0){
                delete searchNameLookup;
            }
            
            Saved_Search__c newSearch = new Saved_Search__c(sub__c = saveSubFolder, Name = searchName, Search_Type__c = 'Projects', User__c = UserInfo.getUserId(), Saved_Search_Unique_Key__c=searchName+saveSubFolder);
            insert newSearch;
            system.debug('---------newSearch---------'+newSearch);
            //newSearch = [select Id, Name from Saved_Search__c where Name = :searchName and User_Id__c = :userId];
            //system.debug('---------newSearch---------'+newSearch);
            if(newSearch !=null && newSearch.id != null){
                list<Search_Detail__c> details = new list<Search_Detail__c>();
                 if(!strSearchText.contains('Enter') ){
                    detail = new Search_Detail__c(Name = searchOn, Search_Value__c = strSearchText,File__c = newSearch.Id, Search_Detail_Unique_Key__c=searchOn +strSearchText+ newSearch.Id);
                    details.add(detail);
                }
                if(selectedStatus.size() == 1 && selectedStatus[0] == '')
                    selectedStatus = null;
                if(selectedStatus != null){
                    for(i = 0; i < selectedStatus.size(); i++){                        
                        detail = new Search_Detail__c(Name = 'status', Search_Value__c = string.valueof(selectedStatus[i]), File__c = newSearch.Id, Search_Detail_Unique_Key__c='status' +string.valueof(selectedStatus[i])+ newSearch.Id);
                        details.add(detail);
                    }
                }
                //date
                if (startFDate != null && startFDate != ''){
                    detail = new Search_Detail__c(Name = 'startFromDate', Search_Value__c = string.valueof(startFDate),File__c = newSearch.Id, Search_Detail_Unique_Key__c='startFromDate' +string.valueof(startFDate)+ newSearch.Id);
                    details.add(detail);  
                }
                if (startEDate != null && startEDate != ''){
                    detail = new Search_Detail__c(Name = 'startToDate', Search_Value__c = string.valueof(startEDate),File__c = newSearch.Id, Search_Detail_Unique_Key__c='startToDate' +string.valueof(startEDate)+ newSearch.Id);
                    details.add(detail);                   
                }
                if (toFDate != null && toFDate != ''){
                    detail = new Search_Detail__c(Name = 'endFromDate', Search_Value__c = string.valueof(toFDate),File__c = newSearch.Id, Search_Detail_Unique_Key__c='endFromDate' + string.valueof(toFDate)+ newSearch.Id);
                    details.add(detail);  
                }
                if (toEDate != null && toEDate != ''){
                    detail = new Search_Detail__c(Name = 'endToDate', Search_Value__c = string.valueof(toEDate),File__c = newSearch.Id, Search_Detail_Unique_Key__c='endToDate' +string.valueof(toEDate)+ newSearch.Id);
                    details.add(detail);                   
                }
                if (bidFDate != null && bidFDate != ''){
                    detail = new Search_Detail__c(Name = 'bidFromDate', Search_Value__c = string.valueof(bidFDate),File__c = newSearch.Id, Search_Detail_Unique_Key__c='bidFromDate' +string.valueof(bidFDate)+ newSearch.Id);
                    details.add(detail);  
                }
                if (bidEDate != null && bidEDate != ''){
                    detail = new Search_Detail__c(Name = 'bidToDate', Search_Value__c = string.valueof(bidEDate),File__c = newSearch.Id, Search_Detail_Unique_Key__c='bidToDate' +string.valueof(bidEDate)+ newSearch.Id);
                    details.add(detail);                   
                }
                if(strTheropeticArea != null &&strTheropeticArea != '' ){
                    detail = new Search_Detail__c(Name = 'theropetic', Search_Value__c = strTheropeticArea, File__c = newSearch.Id, Search_Detail_Unique_Key__c='theropetic' +strTheropeticArea+ newSearch.Id);
                    details.add(detail);
                }
                //phase
                
                if(selectedPhase.size() == 1 && selectedPhase[0] == '')
                    selectedPhase = null;
                if(selectedPhase != null){
                    for(i = 0; i < selectedPhase.size(); i++){                        
                        detail = new Search_Detail__c(Name = 'phase', Search_Value__c = string.valueof(selectedPhase[i]), File__c = newSearch.Id, Search_Detail_Unique_Key__c='phase' +string.valueof(selectedPhase[i])+ newSearch.Id);
                        details.add(detail);
                    }
                }
                system.debug('---------details---------'+details);
                insert details;
            }
        }   
        return null;
    }
    
}