/**
*   'PRA_DataAccessor' class is to access all the SOQL queries and also to perform DML operations on P&F application objects.
*   @author   Devaram Bhargav
*   @version  18-Oct-2013
*   @since    18-Oct-2013
*/
public with sharing class PRA_DataAccessor {
    public static final decimal SUM_FORECAST_ER_THRESHOLD_LOW = 0.995;
    public static final decimal SUM_FORECAST_ER_THRESHOLD_HIGH = 1.005; 
    
    public PRA_DataAccessor() {
    }
    
    /**
    *   Method retrieve the  Aggregateresult data with the predefined attributes value ().
    *   @return   The Aggregateresult data for the client task whose status is not null.
    */
    public static List<Aggregateresult> getClientTaskCountbyNullStatus(String Project){       
        String query='Select count(id) c,status__c s,project__r.Name p  FROM client_task__c';                             
        query +=  ' WHERE  status__c!=null';
        if(Project!=PRA_Constants.CP_ALL)
            query +=' and project__r.name=:Project';     //if project name is passed only.
        query +=  ' GROUP by project__r.Name, status__c';
        system.debug('-----query --------'+query);      
        return Database.query(query);
    }
    
    public static List<Client_task__c> getOpenedClientTasks(String projectId, Datetime targetDate) {
        String targetDateSOQL = targetDate.format('yyyy-MM-dd');
        String mwh = PRA_Constants.MIGRATED_CLIENT_TASK;
        String query  = 'Select id ';
               query += 'From Client_Task__c ';
               query += 'Where Project__c = :projectId ';
               query += 'and   Client_Unit_Number__c != :mwh ';
               query += 'and  (Close_Date__c = NULL ';
               query += 'or    Close_Date__c > ' + targetDateSOQL + ') ';

        return Database.query(query);
    }
    
    /**
    *   Method retrieve the  client tasks data with the predefined attributes value (Project,ActionStatus).    
    *   @return   The List of data for the client task with predefined attributes.
    */
    public static List<Client_task__c> getClientTaskbyProjectOpenClosedStatus(String Projectid,String Actionstatus){       
        
        string mwh = PRA_Constants.MIGRATED_CLIENT_TASK;
        String query  ='Select id ,Close_Date__c,Client_Unit_Number__c,Description__c,Combo_Code__c,project__r.Name,Total_Contract_Units__c, Total_Baseline_Units__c'; 
               query +=',Unit_of_Measurement__c, Contract_Value__c, Total_Units__c,Project_Region__r.Name  FROM client_task__c'; 
               query +=' WHERE Project__c=:Projectid AND  Client_Unit_Number__c !=:mwh';
                
            
        if(Actionstatus==PRA_ClientTaskService.TO_RE_OPEN_CLIENT_TASKS)
            query +=' AND  Close_Date__c!=null';
        if(Actionstatus==PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS)
            query +=' AND  Close_Date__c=null';
                   
        system.debug('-----query --------'+Projectid+Actionstatus+query);      
        return Database.query(query);
    }
    
    /**
    *   Method retrieve the  client tasks data with the predefined attributes value (Project,ActionStatus,COItemsID).    
    *   @return   The List of data for the client task with predefined attributes.
    */
    public static List<Client_task__c> getClientTaskbyProjectandNotInCOItems(String Projectid,String Actionstatus,Set<ID> COItemsID){       
        
        string mwh = PRA_Constants.MIGRATED_CLIENT_TASK;
        String query  ='Select id ,Close_Date__c,Client_Unit_Number__c,Description__c,Combo_Code__c,project__r.Name,Total_Contract_Units__c, Total_Baseline_Units__c'; 
               query +=',Unit_of_Measurement__c, Contract_Value__c, Total_Units__c,Project_Region__r.Name  FROM client_task__c'; 
               query +=' WHERE Project__c=:Projectid AND  Client_Unit_Number__c !=:mwh';
               query +=' AND  Id NOT in :COItemsID'; 
            
        if(Actionstatus==PRA_ClientTaskService.TO_RE_OPEN_CLIENT_TASKS)
            query +=' AND  Close_Date__c!=null';
        if(Actionstatus==PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS)
            query +=' AND  Close_Date__c=null';
                   
        system.debug('-----query --------'+Projectid+Actionstatus+query);      
        return Database.query(query);
    }
    
    /**
    *   Method retrieve the  client tasks data with the predefined attributes value (COid,ActionStatus).    
    *   @return   The List of data for the client task with predefined attributes.
    */
    public static List<Change_Order_Item__c> getCOItemswithClientTaskCloseDate(String COid,String Actionstatus){       
        
        string mwh = PRA_Constants.MIGRATED_CLIENT_TASK;
        String query  ='SELECT Id, Combo_Code__c, Client_Unit_Number__c,Change_Order__r.Client_Project__c FROM Change_Order_Item__c ';
               query +=' WHERE Change_Order__r.Id = :coid and  Client_Task__c!= null' ;              
            
        if(Actionstatus==PRA_ClientTaskService.TO_RE_OPEN_CLIENT_TASKS)
            query +=' AND  Client_Task__r.Close_Date__c!=null';
        if(Actionstatus==PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS)
            query +=' AND  Client_Task__r.Close_Date__c=null';
                   
        system.debug('-----query --------'+coid+Actionstatus+query);      
        return Database.query(query);
    }
    
    /**
    *   Method retrieve the  client tasks data with the predefined attributes value (ClientTask IDs list).    
    *   @return   The List of data for the client task with predefined attributes.
    */
    public static List<Client_task__c> getClientTaskbyctIDs(List<String> ctIdsList){     
        
        String query ='Select id ,Close_Date__c,Client_Unit_Number__c,Description__c,Combo_Code__c,project__r.Name  FROM client_task__c'; 
               query +=' WHERE id=:ctIdsList ';
        system.debug('-----query --------'+ctIdsList+query);      
        return Database.query(query);
    }
    
    /**
    *   Method retrieve the  Change Orders List with the predefined attributes value (Proejct ID, status).    
    *   @return   The List of data for the Change Orders List with predefined attributes.
    */
    public static List<Change_Order__c> getCObyProjectIDandStatus(string ProjectID, string Status){    
        
        String query ='Select id ,Status__c,name,client_project__c,client_project__r.Name  FROM Change_Order__c'; 
               query +=' WHERE client_project__c=:ProjectID ';
               query +=' AND Status__c=:Status ';
        system.debug('-----query --------'+Status+query);      
        return Database.query(query);
    }
    
    /**
    *   Method retrieve the  client tasks data with the predefined attributes value (Project,Status,include).    
    *   @return   The List of data for the client task with predefined attributes.
    */
    public static List<Client_task__c> getClientTaskbyProjectStatus(String Project,String status){       
        String query='Select id ,status__c ,project__r.Name,Description__c,Client_Unit_Number__c  FROM client_task__c'; 
        query +=' WHERE  status__c=:status';        
        query +=' and project__r.name=:Project';             
        system.debug('-----query --------'+query);      
        return Database.query(query);
    }
    
    /**
    *   Method upserts the Client_task__c data with the predefined attributes value (ClientTaskList).   
    *   @param    ClientTaskList   This is the record of the Client_task__c object.
    *   @return   The errors message.
    */ 
    public static String upsertClientTaskList(List<Client_task__c> ClientTaskList){
        String ErrorMsg;
        try{
            UPSERT  ClientTaskList;                
        }catch(Exception ex) {
            ErrorMsg=ex.getMessage();                
        }
        return ErrorMsg;
    }
    
    /**
    *   Method retrieve the  wbs_project__c data with the predefined attributes value (ProjectName).
    *   @return   The data for the wbs_project__c for a particular Project.
    */
    public static List<wbs_project__c> getWBSprojectbyProjectName(String ProjectName){        
        system.debug('-----WBSProject -------'+ProjectName); 
        return[Select id,
                      name,
                      Client_Project__c,
                      Client_Project__r.name                                              
                FROM  wbs_project__c
                WHERE client_project__r.name=:ProjectName
                LIMIT 1];
    } 
    
    /**
    *   Method retrieve the  client_project__c data with the predefined attributes value (ProjectName).
    *   @return   The data for the client_project__c for a particular Project.
    */
    public static client_project__c getClientProjectDetailsbyProjectName(String ProjectName){       
        client_project__c cp=null;
        for(client_project__c c :[Select id,
                                           name                                                                               
                                    FROM   client_project__c
                                    WHERE  name=:ProjectName and Load_Status__c != 'New'                
                                   LIMIT  1]){                                        
            cp =  c;                             
        }
        return cp;
    }
    
    
    /**
    *   Method retrieve the  client_project__c data with the predefined attributes value (Projectid).
    *   @return   The data for the client_project__c for a particular Project.
    */
    public static client_project__c getClientProjectDetailsbyProjectID(String Projectid){       
        client_project__c cp=null;
        for(client_project__c c :[Select id,
                                           name                                                                               
                                    FROM   client_project__c
                                    WHERE  ID=:Projectid and Load_Status__c != 'New'                
                                   LIMIT  1]){                                        
            cp =  c;                             
        }
        return cp;
    }
    
    /**
    *   Method upserts the wbs_project__c data with the predefined attributes value (WBSProject).   
    *   @param    WBSProject   This is the record of the wbs_project__c object.
    *   @return   The errors message.
    */ 
    public static String upsertWBSProject(List<wbs_project__c> WBSProjectList){
        String ErrorMsg;
        try{
            UPSERT  WBSProjectList;                
        }catch(Exception ex) {
            ErrorMsg=ex.getMessage();                
        }
        return ErrorMsg;
    }
        
    /**
    *   Method retrieve the  wbs_project__c data with the predefined attributes value (id).
    *   @return   The data for the wbs_project__c for a record wbsProjId.
    */
    public static List<wbs_project__c> getWBSprojectbyID(id wbsProjId){        
        system.debug('----- wbsProjId: '+wbsProjId);        
        return[Select id,
                      name,
                      Reference_Number__c,
                      Activity_Group__c,
                      Six_Character_Contract__c,
                      Eight_Character_Contract__c,
                      Accounting_Unit__c,
                      Company_Number__c,
                      Company_Number__r.Name,
                      Company_Number__r.Division__c,
                      Company_Number__r.Global_Region__r.Region_Name__c,                      
                      Contract_Level_Address__c,
                      Currency__c,
                      End_Date__c,
                      Invoice_Method__c,
                      Lawson_Status__c,
                      Project_Level_Address__c,
                      Setup_Year__c,
                      Start_Date__c,
                      Client_Project__c,                      
                      Client_Project__r.name                                                                    
                FROM  wbs_project__c
                WHERE id=:wbsProjId
                LIMIT 1];
    } 
    
    /**
    *   Method retrieve the  wbs_project__c data with the predefined attributes value (id).
    *   @param    ID of a Client_Project__c record
    *   @return   The data for the wbs_project__c for a record wbsProjId.
    */
    public static List<WBS_Project__c> getWbsProjectByProjID(ID projID) {
        WBS_Project__c wbsProj = [Select id
                                  From WBS_Project__c
                                  Where Client_Project__r.id = :projID
                                  LIMIT 1];
        return getWBSprojectbyID(wbsProj.id);
    }

    /*
    *   Method retrieves client task and task group data need for wbs export
    *   @param  ID of a Client_Project__c record
    *           One of CU_STATUS_NEW, CU_STATUS_READY,
    *           CU_STATUS_SENT, CU_STATUS_ACK
    *   @return List of Client_Task__c objects with 'joined' data from
    *           the Task_Group__c referenced object  
    */
    public static List<Client_Task__c> getWbsTaskData(Id projId, String status) {
        List<Client_Task__c> ctLst = [Select Description__c,
                                             Client_Unit_Number__c,
                                             Combo_Code__c,
                                             Task_Group__c,
                                             Task_Group__r.Lawson_Short_Desc__c,
                                             Task_Group__r.Lawson_30_Char_Desc__c,
                                             Task_Group__r.Activity_Code__c
                                      From Client_Task__c
                                      Where Project__r.id = :projId
                                      and   Status__c = :status
                                      and   Description__c != :PRA_Constants.MIGRATED_CLIENT_TASK];
        
        return ctLst;
    }
    
    /*
    *   Method retrieves task group data need for wbs export
    *   @param  Set of Task_Group__c Ids  
    *   @return List of Task_Group__c objects 
    */
    public static List<Task_Group__c> getWbsTaskGroupData(Set<Id> taskGroupIds) {
        List<Task_Group__c> tgLst = [Select Lawson_Short_Desc__c,   
                                            Lawson_30_Char_Desc__c,
                                            Activity_Code__c
                                     From   Task_Group__c
                                     Where  Id in :taskGroupIds];
        return tgLst;
    }
    
    // Get a list of Unit_Effort objects that are missing Effort Ratios
    // for the given Client Project and Date Range
    // Params: projectID -- ID field from Client_Project__c
    //         startDate -- Date for the first month of records to include in the search
    //         endDate   -- Date for the last month of records to include in the search
    public static List<Unit_Effort__c> getMissingEffortRatios(ID projectID, Date startDate, Date endDate) {
        
        Date startMonth = startDate.toStartOfMonth();
        Date endMonth   = endDate.toStartOfMonth();

        List<Unit_Effort__c> ue = [select Client_Task__c, Unit_Effort_ID__c, Month_Applies_To__c,
                                          Forecast_Unit__c, Forecast_Effort__c, Sum_Forecast_ER__c,
                                          Client_Task__r.id
                                   from Unit_Effort__c
                                   where Client_Task__r.Project__r.id = :projectID
                                   and Client_Task__r.Description__c != :PRA_Constants.MIGRATED_CLIENT_TASK
                                   and (Month_Applies_To__c >= :startMonth
                                   and  Month_Applies_To__c <= :endMonth)
                                   and (Forecast_Unit__c != 0
                                   and  Forecast_Effort__c != 0)
                                   and (Sum_Forecast_ER__c < :SUM_FORECAST_ER_THRESHOLD_LOW
                                   or   Sum_Forecast_ER__c > :SUM_FORECAST_ER_THRESHOLD_HIGH)];
        return ue;                          
    }
    
    // Get a list of Unit_Effort objects that are missing Effort Ratios
    // for the given Client Project and Month
    // Params: projectID   -- ID field from Client_Project__c
    //         searchMonth -- Date for the month of records to include in the search
    public static List<Unit_Effort__c> getMissingEffortRatios(ID projectID, Date searchMonth) {
        return getMissingEffortRatios(projectID, searchMonth, searchMonth);
    }
    
    // Get a list of Unit_Effort objects that are missing Effort Ratios
    // for the given Client Task and Date Range
    // Params: taskID    -- ID field from Client_Task__c
    //         startDate -- Date for the first month of records to include in the search
    //         endDate   -- Date for the last month of records to include in the search
    public static List<Unit_Effort__c> getMissingEffortRatiosByTask(List<ID> taskID, Date startDate, Date endDate) {
        
        Date startMonth = startDate.toStartOfMonth();
        Date endMonth   = endDate.toStartOfMonth();

        List<Unit_Effort__c> ue = [select Client_Task__c, Unit_Effort_ID__c, Month_Applies_To__c,
                                          Forecast_Unit__c, Forecast_Effort__c, Sum_Forecast_ER__c,
                                          Client_Task__r.id
                                   from Unit_Effort__c
                                   where Client_Task__r.id in :taskID
                                   and Client_Task__r.Description__c != :PRA_Constants.MIGRATED_CLIENT_TASK
                                   and (Month_Applies_To__c >= :startMonth
                                   and  Month_Applies_To__c <= :endMonth)
                                   and (Forecast_Unit__c != 0
                                   and  Forecast_Effort__c != 0)
                                   and (Sum_Forecast_ER__c < :SUM_FORECAST_ER_THRESHOLD_LOW
                                   or   Sum_Forecast_ER__c > :SUM_FORECAST_ER_THRESHOLD_HIGH)];
        
        return ue;
    }
    
    // Get a list of Unit Effort objects that have forecast units, but
    // no forecast effort.
    // Params: taskID    -- ID field from Client_Task__c
    //         startDate -- Date for the first month of records to include in the search
    //         endDate   -- Date for the last month of records to include in the search
    public static List<Unit_Effort__c> getMissingEffortByTask(List<ID> taskID, Date startDate, Date endDate) {

        Date startMonth = startDate.toStartOfMonth();
        Date endMonth   = endDate.toStartOfMonth();

        List<Unit_Effort__c> ue = [select Client_Task__c, Unit_Effort_ID__c, Month_Applies_To__c,
                                          Forecast_Unit__c, Forecast_Effort__c, Sum_Forecast_ER__c,
                                          Client_Task__r.id
                                   from Unit_Effort__c
                                   where Client_Task__r.id in :taskID
                                   and Client_Task__r.Description__c != :PRA_Constants.MIGRATED_CLIENT_TASK
                                   and (Month_Applies_To__c >= :startMonth
                                   and  Month_Applies_To__c <= :endMonth)
                                   and (Forecast_Unit__c != 0
                                   and  Forecast_Effort__c = 0)];
        
        return ue;
    }
    
    // Get a list of Unit Effort objects that have forecast units, but
    // no forecast effort.
    // Params: taskIDs    -- ID field from Client_Task__c
    //         startDate -- Date for the first month of records to include in the search    
    public static List<Unit_Effort__c> getUnitEffortByTaskIDsandMonth(List<string> taskIDs, Date startDate) {

        Date startMonth = startDate.toStartOfMonth();
        List<Unit_Effort__c> ue = [select Client_Task__c, Unit_Effort_ID__c, Month_Applies_To__c,
                                          Forecast_Unit__c, Forecast_Effort__c,Contracted_BDG_Unit__c,
                                          Client_Task__r.id
                                   from Unit_Effort__c
                                   where Client_Task__r.id = :taskIDs
                                   AND Client_Task__r.Description__c != :PRA_Constants.MIGRATED_CLIENT_TASK
                                   AND (Month_Applies_To__c >= :startMonth)
                                  ];
        
        return ue;
    }
    
     /*
    *   Method retrieves the List of worked units for Project,confirmed,date,open/close units
    *   @param  ProjectID, DatesList, confirmed   
    *   @return List of Unit_Effort__c objects data.
    */
    public static List<Unit_Effort__c> getWorkedUnitsforOpenUnits(String ProjectID, List<Date> DatesList,Boolean confirmed) {
            
        String MWH = PRA_Constants.MIGRATED_CLIENT_TASK;        
        String query = 'select Client_Task__c, Unit_Effort_ID__c,Worked_unit__c, Month_Applies_To__c,Forecast_Unit__c, Forecast_Effort__c,Contracted_BDG_Unit__c,Client_Task__r.id';
               query+= ' from Unit_Effort__c where Client_Task__r.Project__r.id = :ProjectID AND Client_Task__r.Description__c != :MWH ';
               query+= 'AND (Month_Applies_To__c in :DatesList) AND Is_Confirmed_Worked_Unit__c =:confirmed';        
               query +=' AND ( (Client_Task__r.Close_Date__c!=null AND Client_Task__r.Close_Date__c>=THIS_MONTH)';       //check if any close units are there, if so it should be >= current month
               query +=' OR  Client_Task__r.Close_Date__c=null)';                                                        //check for non closed units also.
        
        system.debug('--------query----getWorkedUnitsforOpenUnits----------'+query);
        return Database.query(query);
    }
    
    /**
    *   Method upserts the wbs_project__c data with the predefined attributes value (WBSProject).   
    *   @param    WBSProject   This is the record of the wbs_project__c object.
    *   @return   The errors message.
    */ 
    public static String upsertUnitEffortList(List<Unit_Effort__c> UnitEffortList){
        String ErrorMsg;
        try{
            UPSERT  UnitEffortList;                
        }catch(Exception ex) {
            ErrorMsg=ex.getMessage();                
        }
        return ErrorMsg;
    }
    
    /**
    *   Method retrieve the  Profile data with the predefined attributes value (ProfielID).
    *   @return   The data for the Profile for a particular user.
    */
    public static Profile getProfilebyId(String ProfielID){        
        system.debug('-----ProfielID-------'+ProfielID); 
        return[SELECT id,
                      name                                                                    
                FROM  Profile
                WHERE id=:ProfielID
                LIMIT 1];
    } 
    
    /**
    *   Method retrieve the  PermissionSetAssignment list data with the predefined attributes value (UserId,Permissionset).
    *   @return   The data for the PermissionSetAssignment for a particular user with permission set of values.
    */
    public static List<PermissionSetAssignment> getPermissionSetAssignmentbyUserIdandProfiles(String userID,Set<string> PermissionSetList){        
        system.debug('-----userID---PermissionSetList-------'+userID+PermissionSetList); 
        return[SELECT PermissionSet.Id,
                      PermissionSet.Name,
                      PermissionSetID
               FROM   PermissionSetAssignment
               WHERE  AssigneeId =: userID 
                      and PermissionSet.label in: PermissionSetList];
    } 
    
    /**
    *   Method retrieve the  user listwith the predefined attributes value (UserIDList).
    *   @return   The data for the user list for a particular user list.
    */
    public static List<User> getUserListbyIds(Set<String> UserIDList){        
        system.debug('-----UserIDList-------'+UserIDList); 
       
        return[SELECT id,
                      name,
                      email                                                                    
                FROM  User
                WHERE id in :UserIDList
                AND   IsActive=true
                ];
    } 
    
    /**
    *   Method retrieve the  Map data with the predefined attributes value (ProjectName).
    *   @return   The Map<String,Decimal> for the ProjectName as Map<RegionName,sum(Adjustment_to_Regional_CV__c)>.
    *   This method is a rollup group by which sends the total of the regions with an extra row a s null
    */
    public static Map<String,Decimal> getAdjtoRCVbyGroupRollupbyProject(String ProjectName){        
        system.debug('-----ProjectName-------'+ProjectName);
        //Initiating the Map
        Map<String,Decimal> AdjRCVMap=new Map<String,Decimal>(); 
        
        //As we are doing the group by roolup, so we are putting the default total as 0.00,
        //because if we dont have any records we dont get the total even
        AdjRCVMap.put('Total',0.00);        
        
        for(AggregateResult ar:[SELECT Region__r.Region_Name__c r, SUM(Adjustment_to_Regional_CV__c) sum
                                FROM Group_Client_Tasks__c
                                WHERE group__r.client_project__r.name=:ProjectName
                                GROUP BY Rollup(Region__r.Region_Name__c)]){
            
            if(ar.get('r')==null || ar.get('r')=='')
                AdjRCVMap.put('Total',(decimal)ar.get('sum'));
            else
                AdjRCVMap.put((String)ar.get('r'),(decimal)ar.get('sum'));
                                    
            system.debug('-----AdjRCVMap-------'+AdjRCVMap+ar);    
        }        
        return AdjRCVMap;
    }     
}