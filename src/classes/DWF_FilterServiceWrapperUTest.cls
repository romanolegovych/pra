/**
 * This class will cover any DWF_FilterServiceWrapper methods not covered by
 * controller test cases.
 */
@isTest
private class DWF_FilterServiceWrapperUTest {

	static list<MuleServicesCS__c> serviceSettings;
	
	static void init() {
		serviceSettings = new MuleServicesCS__c[] {
			new MuleServicesCS__c(name = 'ENDPOINT', Value__c = 'ENDPOINT'),
			new MuleServicesCS__c(name = 'TIMEOUT', Value__c = '60000'),
			new MuleServicesCS__c(name = 'DwFilterService', Value__c = 'DwFilterService')
		};
		insert serviceSettings;
	}

	static testMethod void testGetEmployeeListByCompanyNo() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_EMPLOYEE_VO_COMPANY_NO_RESPONSE));
		DWF_FilterService.employeeListResponse resp = DWF_FilterServiceWrapper.getEmployeeListByCompanyNo(100L);
		system.assertNotEquals(resp, null);
		Test.stopTest();
	}

	static testMethod void testGetEmployeeListByCompanyNoBUNo() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_EMPLOYEE_VO_COMPANY_NO_BU_NO_RESPONSE));
		DWF_FilterService.employeeListResponse resp = DWF_FilterServiceWrapper.getEmployeeListByCompanyNoBUNo(100L, '95356');
		system.assertNotEquals(resp, null);
		Test.stopTest();
	}

	static testMethod void testGetEmployeeListByCompanyNoBUName() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_EMPLOYEE_VO_COMPANY_NO_BU_NAME_RESPONSE));
		DWF_FilterService.employeeListResponse resp = DWF_FilterServiceWrapper.getEmployeeListByCompanyNoBUName(100L, 'BU NAME');
		system.assertNotEquals(resp, null);
		Test.stopTest();
	}
}