/**
@author Bhargav Devaram
@date 2014
@description this test class is to display the sub tab menus related to a scenario results
**/
@isTest
private class PBB_ProjectResultsControllerTest {
    
   /** 
    * This is just for code coverage
    */
    static testMethod void testConstructor() {
        test.startTest();
        PBB_DataAccessor da = new PBB_DataAccessor();
        test.stopTest();
    }    
   
    static testMethod void testPBB_ProjectResultsController() {
        
       /**
        * Init Test Utils
        */
        PBB_TestUtils tu = new PBB_TestUtils();
        test.startTest();
       /** 
        * Covering if in constructor
        */
        Service_Model__c sm = new Service_Model__c();
        sm = tu.createServiceModelAttributes();
        sm= tu.approveServicemodel(sm);
        WR_Model__c wm = new WR_Model__c();
        wm = tu.createWRModelAttributes();
        WFM_Project__c cpw = new WFM_Project__c();
        cpw = tu.createwfmproject();
        Bid_Project__c bp = new Bid_Project__c();
        bp = tu.createbidproject();
        PBB_Scenario__c scs = new PBB_Scenario__c();
        SRM_Model__c smm = tu.createSRMModelAttributes();
        //SRM_Scenario__c  srmScen= tu.createSRMScenario();
        scs = tu.createscenarioAttributes();
        scs = [select id,name from PBB_Scenario__c];
        ApexPages.currentPage().getParameters().put('Id', scs.id);
        PBB_ProjectResultsController prc = new PBB_ProjectResultsController();
        system.assertEquals(sm.Name,'name16');
        system.assert(scs.id != null, 'Record existing');
        test.stopTest();    
   }
   
}