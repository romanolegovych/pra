global class RM_AssignmentIsCountBatch extends PRA_BatchaQueueable{
  global String parameter;
  global RM_AssignmentIsCountBatch() {
        
    }
    //Set the parameters over the variables
    global override void  setBatchParameters(String parametersJSON){
        List<String> params = (List<String>) Json.deserialize(parametersJSON, List<String>.class);
        parameter = params.get(0);    
     }
    
    global override Database.QueryLocator start(Database.BatchableContext BC) {
      String query = 'SELECT Cal_Is_Count__c,diffCount__c,Is_Count__c FROM WFM_Employee_Assignment__c WHERE diffCount__c = true';
      system.debug('---------query---------'+query  ); 
        return Database.getQueryLocator(query);
    }
    
    global override void execute(Database.BatchableContext BC, List<sObject> scope){
    list<string> lstEmpl = new list<string>();	
    for(Sobject s : scope) {
        system.debug('---------s---------'+s  );
          if(s instanceof WFM_Employee_Assignment__c) {
            WFM_Employee_Assignment__c a = (WFM_Employee_Assignment__c)s;
              lstEmpl.add(a.ID);
          }
      }	
    	
	List<WFM_Employee_Assignment__c > diffCountList = [SELECT Id,Cal_Is_Count__c,diffCount__c,Employee_ID__c,Is_Count__c FROM WFM_Employee_Assignment__c WHERE diffCount__c = true];
	List<WFM_Employee_Assignment__c> diffCountUpdateList = new List<WFM_Employee_Assignment__c>();
	if(!diffCountList.isEmpty()){		
		for(WFM_Employee_Assignment__c wea : diffCountList){
			if(wea.diffCount__c == true){
				wea.Is_Count__c  = wea.Cal_Is_Count__c;					
			}
			diffCountUpdateList.add(wea);
		}		
	}
	update diffCountUpdateList;
    
    }
    
    global override void finish(Database.BatchableContext BC) {
    
    }
}