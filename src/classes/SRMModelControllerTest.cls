@isTest
private class SRMModelControllerTest {
	private static SRMModelController cont;

	@isTest
	static void SRMModelControllerTest() {
		Test.startTest();
    		cont = new SRMModelController();
    	Test.stopTest();
    	System.assert(cont.SRMModelsList.size() == 0);
	}

	@isTest
	static void getNewPageReferenceTest() {
		cont = new SRMModelController();
		Test.startTest();
    		PageReference pr = cont.getNewPageReference();
    	Test.stopTest();
    	Schema.DescribeSObjectResult  r = SRM_Model__c.SObjectType.getDescribe();
    	System.assertEquals(pr.getUrl(), '/'+r.getKeyPrefix()+'/e');
	}
}