@isTest
private class BDT_WrapperTest {

	static testMethod void createObjectTest() {
		//sObject createObject(String typeName, String parentId, String childId)
		sObject result;
		
		// fail target type
		try {
			result = BDT_Wrapper.createObject('Non Existent', null, null);
		} catch (Exception e) {
			// was not properly handled in class
		}
		
		// test PopulationStudyAssignment__c
		result = BDT_Wrapper.createObject('PopulationStudyAssignment__c', null, null);
		system.AssertEquals('PopulationStudyAssignment__c', result.getSObjectType().getDescribe().getName());
		
		result = BDT_Wrapper.createObject('ClinicalDesignStudyAssignment__c', null, null);
		system.AssertEquals('ClinicalDesignStudyAssignment__c', result.getSObjectType().getDescribe().getName());
	}
	
	static testMethod void saveChildWrapperTest() {
		
		// create some test data
		Client_Project__c Project = BDT_TestDataUtils.buildProject();
		insert Project;
		list<Study__c> studyList = BDT_TestDataUtils.buildStudies(Project,4);
		insert studyList;
		list<Population__c> populationList = BDT_TestDataUtils.buildPopulation(Project.id, 5);
		insert populationList;
		
		// all new records so no existing relationships
		Map<String, PopulationStudyAssignment__c> existingRelationships = new Map<String, PopulationStudyAssignment__c>();
		
		list<bdt_wrapper.parentWrapper> parentWrapperList = new list<bdt_wrapper.parentWrapper>();
			
		bdt_wrapper.buildParentWrapperList(parentWrapperList
				, populationList
				, studyList
				, existingRelationships);

		// each parent is a population
		// each child is a study
		for (Integer i = 0; i < 5 ; i++) {
			system.assertEquals(4,parentWrapperList[i].childWrapperList.size()); // 4 studies per population
		}
		
		// select population 1 for all studies
		for (Integer i = 0; i < 5 ; i++) {
			parentWrapperList[i].childWrapperList[0].selected = true;
		}
		
		// save the result
		bdt_wrapper.saveChildWrapper(parentWrapperList, existingRelationships);
		
		// check if the records were saved
		list<PopulationStudyAssignment__c> psaList = [select id,study__c,population__c from PopulationStudyAssignment__c];
		system.assertEquals(5,psaList.size());
		
		// now remove a child relationship, (disconnect population from study)
		
		// populate existing map
		for (PopulationStudyAssignment__c psa:psaList) {
			existingRelationships.put(psa.population__c+':'+psa.study__c,psa);
		}
		
		bdt_wrapper.buildParentWrapperList(parentWrapperList
				, populationList
				, studyList
				, existingRelationships);

		// previously saveitems should be selected now
		for (Integer i = 0; i < 5 ; i++) {
			system.assertEquals(true,parentWrapperList[i].childWrapperList[0].selected);
			// others should be false
			for (Integer j = 1; j < 4 ; j++) {
				system.assertEquals(false,parentWrapperList[i].childWrapperList[j].selected);
			}
		}
		
		parentWrapperList[0].childWrapperList[0].selected = false;
		parentWrapperList[1].childWrapperList[0].selected = false;
		
		bdt_wrapper.saveChildWrapper(parentWrapperList, existingRelationships);
		psaList = [select id,study__c,population__c from PopulationStudyAssignment__c];
		system.assertEquals(3,psaList.size());
		
	}
	
	
	
}