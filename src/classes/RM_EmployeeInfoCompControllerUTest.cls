/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_EmployeeInfoCompControllerUTest {
	static COUNTRY__c country;
    static list<WFM_Location_Hours__c> hrs;
    static Employee_Details__c empl;
    
    static void init(){
        list<string> months = RM_Tools.GetMonthsList(-4, 5);
        country = new COUNTRY__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0);
        insert country;
        
        hrs = new List<WFM_Location_Hours__c>();
        for (string month:months){
            WFM_Location_Hours__c hr = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + month,YEAR_MONTH__c=month,NBR_Business_Day_In_Month__c=22);
            hrs.add(hr); 
        }
        insert hrs;  
        empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='1000', Employee_Unique_Key__c='1000', First_Name__c='John', Last_Name__c='Smith',email_Address__c='JSmith@gmail.com', RM_Comment__c='Test',
        Function_code__c='PR',Buf_Code__c='KCIPR', Business_Unit__c='BUC', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), FTE_Equivalent__c=1 );
        insert empl;
 
      
      
    }
    static testMethod void TestEmployeeInfo() {

        
     
        //need to insert project
        init();
        Test.startTest();
        RM_EmployeeInfoCompController detailExt = new RM_EmployeeInfoCompController();
        detailExt.employeeRID = empl.id;
        detailExt.getEmployeeInfo();
        
        system.assert(detailExt.empl.last_name__c  == empl.last_name__c);
      
        
        Test.stopTest();
        
      }
    static testMethod void TestNote() {

        
     
        //need to insert project
        init();
        Test.startTest();
        RM_EmployeeInfoCompController detailExt = new RM_EmployeeInfoCompController();
        detailExt.employeeRID = empl.id;
        detailExt.getEmployeeInfo();
        detailExt.rmAddComment = 'New';
        detailExt.addNote();
        Employee_Details__c newEmp = RM_EmployeeService.GetEmployeeDetailByEmployee(empl.ID);
        system.assert(newEmp.RM_Comment__c  == 'New');
      
        
        Test.stopTest();
        
      }
}