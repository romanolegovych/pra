@isTest
public class  CCI_CrsInstancesTest {
	
	static list<MuleServicesCS__c> serviceSettings;
	
	static void init() {
		serviceSettings = new MuleServicesCS__c[] {
			new MuleServicesCS__c(name = 'ENDPOINT', Value__c = 'ENDPOINT'),
			new MuleServicesCS__c(name = 'TIMEOUT', Value__c = '60000'),
			new MuleServicesCS__c(name = 'MdmMappingService', Value__c = 'MdmMappingService')
		};
		insert serviceSettings;
	}
	
	static testMethod void testSearchInstId() {
		CCI_CrsInstanceController con = new CCI_CrsInstanceController();
		con.selectedInstanceID = '1';
		con.instanceVOMap = new map<Long, MdmMappingService.instanceVO>();
		con.instanceVOMap.put(1L, new MdmMappingService.instanceVO());
		con.searchInstId();
		system.assertNotEquals(con.renderInstanceVO, null);
	}
	
	static testMethod void testPopulatePRASystemDropdown() {
		CCI_CrsInstanceController con = new CCI_CrsInstanceController();
		list<selectoption> options = con.getPRASystem();
		system.assert(options != null && options.size() == 3);
	}
	
	static testMethod void testPopulateSystemNamesDropdown() {
		CCI_CrsInstanceController con = new CCI_CrsInstanceController();
		list<selectoption> options = con.getSystemNames();
		system.assert(options != null && options.size() == 7);
	}
	
	static testMethod void testPopulateAttribKeyDropdown() {
		CCI_CrsInstanceController con = new CCI_CrsInstanceController();
		list<selectoption> options = con.getAttributeKeys();
		system.assert(options != null && options.size() == 4);
	}
	
	static testMethod void testSearch_POSITIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_CrsInstanceController con = new CCI_CrsInstanceController();
		con.SearchInstanceName = 'SEARCH VALUE';
		con.Search();
		system.assertNotEquals(con.getInstaVOs(), null);
		system.assert(con.getInstaVOs().size() > 1);
		Test.stopTest();
	}
	
	static testMethod void testSearch_NEGATIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(true));
		CCI_CrsInstanceController con = new CCI_CrsInstanceController();
		con.SearchInstanceName = 'SEARCH VALUE';
		con.Search();
		system.assertNotEquals(con.getInstaVOs(), null);
		system.assert(con.getInstaVOs().size() == 0);
		Test.stopTest();
	}
	
	static testMethod void testSubmit_POSITIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_CrsInstanceController con = new CCI_CrsInstanceController();
		con.instName = 'INSTANCE NAME';
		con.praSys = 'CTMS SYS';
		con.SysName = 'SIEBEL CRS';
		con.Submit();
		Test.stopTest();
	}
	
	static testMethod void testAdd_POSITIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_CrsInstanceController con = new CCI_CrsInstanceController();
		con.instanceVOMap = new map<Long, MdmMappingService.instanceVO>();
		con.instanceVOMap.put(1L, new MdmMappingService.instanceVO());
		con.instanceIDselected = 1L;
		con.attributeKey = 'ATTRIB KEY';
		con.URLValue = 'TEST URL';
		con.Add();
		Test.stopTest();
	}
	
	static testMethod void testSaveInstance_POSITIVE() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_CrsInstanceController con = new CCI_CrsInstanceController();
		con.renderInstanceVO = new MdmMappingService.instanceVO();
		con.saveInstance();
		Test.stopTest();
	}
}