/**
 * This test class is designed to lull my conscience so that I may get a few hours of sleep and stop
 * worrying about the nasty bugs that might be lurking in the deep dark alleys of my PRA_EffortRatioModuleController code.
 * Albeit, it is not capable of annihilating _all_ bugs, but it does promise to increase the quality of my deliverable.
 * 
 * Yours truly,
 * A cautious developer
 * 
 * So, what do I really want to test here?
 * 
 */
@isTest(SeeAllData=false)
private class PRA_EffortRatioModuleControllerTest {
    
    // So lets start with the constructor where we set the userpreferences when one is present
    static testmethod void  testUserPreference() {
        
        // Test when no user prefs are present
        PRA_EffortRatioModuleController effortRatioController = new PRA_EffortRatioModuleController();
        System.assertEquals(effortRatioController.selectedProject , 'None');
        //System.assertEquals(effortRatioController.selectedTask[0], 'None');
        System.assertEquals(effortRatioController.isCollapsed, false);
        
        // Insert client project
        Client_Project__c cp = new Client_Project__c(name = 'Project 1');
        insert cp;
        // Now setup some user prefs
        effortRatioController.selectProject();
        UserPreference userPref = new UserPreference();
        // Save user prefs
        PRA_Utils.savePrefs(cp.Id, new List<String>{'Asia Pacific', 'North America'}, new List<String>{'Testing', 'Druggist'}, new List<String>{'Task1', 'Project Management'}, 
                                 new List<String>{'Group1', 'Group2'}, true, false);
                                 
        PRA_EffortRatioModuleController effortRatioController2 = new PRA_EffortRatioModuleController(); 
                
        // Check for the values that are supposed to be in there
        System.assertEquals(effortRatioController2.selectedProject , cp.Id);
        System.assertEquals(effortRatioController2.selectedTask[0], 'None');
        System.assertEquals(effortRatioController2.unconfirmedOnly, true);
        System.assertEquals(effortRatioController2.isCollapsed, true);
        
        // Test reset now
        effortRatioController2.reset();
        System.assertEquals(effortRatioController2.selectedProject , 'None');
        System.assertEquals(effortRatioController2.selectedTask[0], 'None');
        System.assertEquals(effortRatioController2.unconfirmedOnly, false);
        System.assertEquals(effortRatioController2.isCollapsed, true);
        
    }
    
    static testmethod void testSearchCRCTTasks() {
        
        PRA_TestUtils utils = new PRA_TestUtils();
        utils.initAll();
        PRA_EffortRatioModuleController effortRatioController = new PRA_EffortRatioModuleController();
        System.assertEquals(effortRatioController.isFirstTime, true);
        effortRatioController.isFirstTime = false;
        effortRatioController.selectedProject = utils.clientProject.Id;
        
        effortRatioController.selectProject();
        effortRatioController.searchTasks();
        
        // Test if the userpref is null
        PRA_Utils.UserPreference uPref = PRA_Utils.retrieveUserPreference();
        System.assertNotEquals(upref, null);    
        
        // Test number of crcts retrieved   
        System.assertEquals(2, effortRatioController.liWrappers.size());
        
    }
    
    static testmethod void testSearchProjects() {
        PRA_TestUtils utils = new PRA_TestUtils();
        utils.initAll();
        PRA_EffortRatioModuleController effortRatioController = new PRA_EffortRatioModuleController();
        List<Client_Project__c> clientProjects = PRA_EffortRatioModuleController.searchProject('Human');
        System.assertNotEquals(null, clientProjects);
        System.assertEquals(1, clientProjects.size());
    }
    
    static testmethod void shouldDisplayDataForSelectedTask() {
        // Should test:
        // - buf codes
        // - date from & date to & month labels
        // - effort ratio values
        // - hour values per buf code
        // - effort
        
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        // prepare expected values
        Client_Task__c task = tu.clientTasks.get(0);
        
        Date startMonth = date.today().toStartOfMonth();
        List<Hour_EffortRatio__c> erCurrentMonthList = [Select Id, Name, Unit_Effort__c, Unit_Effort__r.Client_Task__c, Buf_Code__c, Buf_Code__r.Name, 
                                                        Forecast_ER__c from Hour_EffortRatio__c
	                                                    where Unit_Effort__r.Client_Task__c = :task.Id and Unit_Effort__r.Month_Applies_To__c = :startMonth
	                                                    order by Unit_Effort__r.Month_Applies_To__c];
        Set<String> bufCodes = new Set<String>();
        for(Hour_EffortRatio__c er : erCurrentMonthList) {
            //system.debug('er=' + er);
            bufCodes.add(er.Buf_Code__r.Name);
        }
        system.assertEquals(2, erCurrentMonthList.size()); // just check that records have been created - check that unit test data generator class was not changed
                
        List<Unit_Effort__c> forecastEfforts = [Select Id, Forecast_Effort__c, Month_Applies_To__c from Unit_Effort__c 
                                                where Client_Task__c = :task.Id
                                                and Month_Applies_To__c >= :Date.today().toStartOfMonth()
                                                and Month_Applies_To__c <= :tu.clientProject.Estimated_End_Date__c
                                                order by Month_Applies_To__c ];
        // set values of forecast (to make sure that correct values are display in each month)      
        for(Integer i = 0; i < forecastEfforts.size(); i++) {
            forecastEfforts.get(i).Forecast_Effort__c = i;
        }
        update forecastEfforts;
        
        // calculate expected hour per buf code
        Map<String, List<Double>> expectedHours = new Map<String, List<Double>>();
            
        // when
        Test.startTest();
        PRA_EffortRatioModuleController effRatioCntr = new PRA_EffortRatioModuleController();
        effRatioCntr.selectedProject = tu.clientProject.Id;
        effRatioCntr.selectProject();
        effRatioCntr.searchTasksVF();
        effRatioCntr.selectedTaskId = task.Id;
        effRatioCntr.savePrefsUpdateClientUnits();
        
        // then
        // check month labels
        Integer monthsBetween = startMonth.monthsBetween(tu.clientProject.Estimated_End_Date__c);
        Integer monthsAdded = 9 - math.mod(3 + monthsBetween, 9);
        
        List<String> liMonths = (List<String>) Json.deserialize(effRatioCntr.jsonMonthlyLabels, List<String>.class);
        system.debug ('Project end date=' + tu.clientProject.Estimated_End_Date__c);
        for(String str : liMonths){system.debug(str);}
        
        system.assertEquals(3 + monthsBetween + monthsAdded, liMonths.size()); // 3 months in past + month to end of project + n months to be divadable by 9
        system.assertEquals(1 + monthsBetween, effRatioCntr.dateFromOptions.size()); // current month + month to end of project
        system.assertEquals(1 + monthsBetween, effRatioCntr.dateToOptions.size()); // current month + month to end of project
        
        for(Integer i = -3; i < (monthsBetween+monthsAdded); i++) {
            Date d = startMonth.addMonths(i);
            Datetime dtime = Datetime.newInstance(d.year(), d.month(), d.day());
            String expectedDateLabel = dtime.format('MMM yyyy');
            system.assertEquals(expectedDateLabel, liMonths.get(i+3));
            
            if(i >= 0 && i <= monthsBetween) {
                String expectedDate = dtime.format('dd MMM yyyy');
                system.assertEquals(expectedDate, effRatioCntr.dateFromOptions.get(i).getLabel());
                system.assertEquals(expectedDate, effRatioCntr.dateToOptions.get(i).getLabel());
            }
            
        }
        
        // check buf codes
        for(PRA_EffortRatioModuleController.BufCodeWrapper bcWrap : effRatioCntr.bufCodeWrappers){
            system.assert(bufCodes.contains(bcWrap.bufCode));
        }
        
        // check effort - jsonEffortValueWrappers
        
        List<PRA_EffortRatioModuleController.EffortValueWrapper> effWrapers = (List<PRA_EffortRatioModuleController.EffortValueWrapper>)
                                                Json.deserialize(effRatioCntr.jsonEffortValueWrappers, List<PRA_EffortRatioModuleController.EffortValueWrapper>.class);
        
        system.assertEquals(forecastEfforts.size() + 3, effWrapers.size()); // check list size = worked(3) + forecast
        for(Integer i = -3; i < forecastEfforts.size(); i++) {
            if(i < 0) {
            	System.debug('----------effWrapper index ' + i + 3 + '---------------' + effWrapers.get(i + 3).value);
                //system.assertEquals(2.00000000, effWrapers.get(i + 3).value);               
            } else {
                //system.assertEquals(forecastEfforts.get(i).Number__c, effWrapers.get(i+3).value);
                //system.assertEquals(forecastEfforts.get(i).Month_Effort_Applies_To__c, effWrapers.get(i+3).monthAppliesTo);
            }
        }
        
        //check effRatioCntr.jsonCalculationWrappers
        // number of rows
        Integer noEffRat = [select count() from Hour_EffortRatio__c
                           where Unit_Effort__r.Client_Task__c = :task.Id 
                           and Unit_Effort__r.Month_Applies_To__c = :Date.today().toStartOfMonth() 
                           ];   
        List<PRA_EffortRatioModuleController.CalculationWrapper> calWrap = (List<PRA_EffortRatioModuleController.CalculationWrapper>)
                                        Json.deserialize(effRatioCntr.jsonCalculationWrappers, List<PRA_EffortRatioModuleController.CalculationWrapper>.class);
        system.assertEquals(noEffRat, calWrap.size());  
        test.stopTest();
    }
    
    static testMethod void testGetVariationFromContract() {
        PRA_TestUtils utils = new PRA_TestUtils();
        utils.initAll();
        
        String taskId = utils.clientTasks[0].Id;
        Decimal totalForecastUnits = 4.00000000;
        
        Client_Task__c task = [SELECT Total_Contract_Units__c FROM Client_Task__c where Id = :utils.clientTasks.get(0).Id];
       
        System.assertEquals(totalForecastUnits - task.Total_Contract_Units__c, PRA_EffortRatioModuleController.getVariantionFromContract(taskId, totalForecastUnits));
    }
    
    static testMethod void testSaveEfforts() {
    	PRA_TestUtils utils = new PRA_TestUtils();
    	utils.initAll();
    	
        List<Client_Task__c> tasks = [select Start_Date__c from Client_Task__c order by Project__c];
        
        Map<Integer, String> monthMap = new Map<Integer, String>();
        monthMap.put(1, 'Jan');
        monthMap.put(2, 'Feb');
        monthMap.put(3, 'Mar');
        monthMap.put(4, 'Apr');
        monthMap.put(5, 'May');
        monthMap.put(6, 'Jun');
        monthMap.put(7, 'Jul');
        monthMap.put(8, 'Aug');
        monthMap.put(9, 'Sep');
        monthMap.put(10, 'Oct');
        monthMap.put(11, 'Nov');
        monthMap.put(12, 'Dec');
        Date applyDate = tasks[0].Start_Date__c.addMonths(3).toStartOfMonth();
        String dateApplied = applyDate.day() + ' ' + monthMap.get(applyDate.month()) + ' ' + applyDate.year();
        
        List<Unit_Effort__c> unitEfforts = [select Id from Unit_Effort__c where Client_Task__c = :tasks[0].Id];
    	List<Hour_EffortRatio__c> hourERs = [select Forecast_ER__c, Unit_Effort__r.Client_Task__c, BUF_Code__r.Name 
    		from Hour_EffortRatio__c where Unit_Effort__r.Month_Applies_To__c = :tasks[0].Start_Date__c.addMonths(3).toStartOfMonth()
        	order by Unit_Effort__r.Client_Task__c];
        	
        System.debug('----------------task--------------------' + tasks.size());
        System.debug('----------------unitEfforts--------------------' + unitEfforts.size());
        System.debug('----------------hourERs--------------------' + hourERs.size());
        
        String jsonInner = '';    
        // Update the effort ratio value and prepare json string
        for(Hour_EffortRatio__c hrER : hourERs) {
	        String recordId = hrER.Unit_Effort__r.Client_Task__c;
	        Decimal value = 50.0;
	        jsonInner += '{"taskId":"' + recordId + '","percentageSplit":"' + value + '","bufCode":"' + hrEr.BUF_Code__r.Name + '"},';
        }
        jsonInner = jsonInner.substring(0, jsonInner.lastIndexOf(',')); 
        String json = '[' + jsonInner + ']';      
        
        // Send json string to save method
        PRA_EffortRatioModuleController.saveEfforts(json, dateApplied, dateApplied);
        Hour_EffortRatio__c hrER = [select Forecast_ER__c from Hour_EffortRatio__c where Id = :hourERs[0].Id];
        System.debug('----------------hourERs--------------------' + hrER);
        System.assertEquals(.50000000,hrER.Forecast_ER__c);
    }

    static testMethod void testFilterER() {
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        // prepare expected values
        Client_Task__c task = tu.clientTasks.get(0);
        
        Date startMonth = date.today().toStartOfMonth();
        List<Hour_EffortRatio__c> erCurrentMonthList = [Select Id, Name, Unit_Effort__c, Unit_Effort__r.Client_Task__c, Buf_Code__c, Buf_Code__r.Name, 
                                                        Forecast_ER__c from Hour_EffortRatio__c
                                                        where Unit_Effort__r.Client_Task__c = :task.Id and Unit_Effort__r.Month_Applies_To__c = :startMonth
                                                        order by Unit_Effort__r.Month_Applies_To__c];
        Set<String> bufCodes = new Set<String>();
        for(Hour_EffortRatio__c er : erCurrentMonthList) {
            //system.debug('er=' + er);
            bufCodes.add(er.Buf_Code__r.Name);
        }
        system.assertEquals(2, erCurrentMonthList.size()); // just check that records have been created - check that unit test data generator class was not changed
                
        List<Unit_Effort__c> forecastEfforts = [Select Id, Forecast_Effort__c, Month_Applies_To__c from Unit_Effort__c 
                                                where Client_Task__c = :task.Id
                                                and Month_Applies_To__c >= :Date.today().toStartOfMonth()
                                                and Month_Applies_To__c <= :tu.clientProject.Estimated_End_Date__c
                                                order by Month_Applies_To__c ];
        // set values of forecast (to make sure that correct values are display in each month)      
        for(Integer i = 0; i < forecastEfforts.size(); i++) {
            forecastEfforts.get(i).Forecast_Effort__c = i;
        }
        update forecastEfforts;

        Test.startTest();
        PRA_EffortRatioModuleController effRatioCntr = new PRA_EffortRatioModuleController();
        effRatioCntr.selectedProject = tu.clientProject.Id;
        effRatioCntr.selectProject();
        effRatioCntr.searchTasksVF();
        effRatioCntr.selectedTaskId = task.Id;
        effRatioCntr.savePrefsUpdateClientUnits();
        
        effRatioCntr.filterMissingER = true;
        effRatioCntr.onChangeFilterMissingER();
        effRatioCntr.refreshData();
        effRatioCntr.filterMissingER = false;
        effRatioCntr.onChangeFilterMissingER();
        
        Test.stopTest();    	
    }
    
	static testMethod void testDisplayProperties() {
        PRA_EffortRatioModuleController contERM = new PRA_EffortRatioModuleController();
        
        Boolean testVal;
        
        Test.startTest();
        testVal = contERM.showDataBlock;
        system.assert(!testVal);
        
        testVal = contERM.showClientUnitBlock;
        system.assert(!testVal);
        
        testVal = contERM.showNoRowsBlock;
        system.assert(!testVal);
        
        testVal = contERM.showNoMissingERBlock;
        system.assert(!testVal);
        
        contERM.filterMissingER = true;
        testVal = contERM.showDataBlock;
        system.assert(!testVal);

        Test.stopTest();	        
	}    
}