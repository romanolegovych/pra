/* 
@author Dima Smirnov
@date 2015  
@description global wrapper class
*/
global class PRA_WrappersForMobile {
    
    global class WrapperSite{
         
         global String Site{get;private set;}
         global map<String,Integer> siteData{get;private set;}
         global String Country {get;set;}
        
         public wrapperSite(String s,map<String,Integer> si,String c){
             Site = s;
             siteData = si;
             Country = c;
             
         }
      }
      
      global class WrapperCountry{
         
         global String country{get;set;}
         global map<String,Integer> countryData{get;private set;}
         
         public wrapperCountry(Object c,map<String,Integer> co){
             country = (String)c;
             countryData = co;
         }
      }
      
      global class CountryWeeklyEventsResult extends OperationResult{
            
            global List<CountryWeeklyEventsWrapper> events{ get; private set; }
            
            public CountryWeeklyEventsResult(List<CountryWeeklyEventsWrapper> events){
                this.events = events;
            }
            
            public CountryWeeklyEventsResult(String errorMessage){
                super(errorMessage);
            }
      }
      
      global class CountryWeeklyEventsWrapper{
            
            private final PBB_WR_APIs.CountryWeeklyEvents weeklyEvent;
            
            public CountryWeeklyEventsWrapper(PBB_WR_APIs.CountryWeeklyEvents weeklyEvent){
                this.weeklyEvent = weeklyEvent;
            }
            
            global Date getWeekDate(){
                return weeklyEvent.weekDate;
            }
            
            global Integer getAggressiveSubjects(){
                return weeklyEvent.AggressiveSubjects;
            }
            
            global Integer getConservativeSubjects(){
                return weeklyEvent.ConservativeSubjects;
            }
            
            global Integer getPlannedSubjects(){
                return weeklyEvent.PlannedSubjects;
            }
            
            global Integer getActualSubjects(){
                return weeklyEvent.ActualSubjects;
            }
            
            global Integer getMediumRiskSubjects(){
                return weeklyEvent.MediumRiskSubjects;
            }
            
            global Integer getActiveToDate(){
                return weeklyEvent.actualSubjects;
            }
            
            global Integer getOnTrack(){
                return weeklyEvent.conservativeSubjects - weeklyEvent.actualSubjects;
            }
        
            global Integer getAtRisk(){
                return weeklyEvent.aggressiveSubjects - weeklyEvent.conservativeSubjects;
            }

            global Integer getDelayed(){
                return weeklyEvent.plannedSubjects - weeklyEvent.aggressiveSubjects;
            }

            global Integer getRemaining(){
                return weeklyEvent.plannedSubjects - weeklyEvent.actualSubjects;
            }

      }
      
      global class PatientEnrollmentWrapperGlobal{
        
            global Date     FPI_Planned             {get; private set;}
            global Date     LPI_Planned             {get; private set;}
            global Integer  TargetNoPatients        {get; private set;} 
            global Date     LPI_HighRisk            {get; private set;}
            global Date     LPI_MedRisk             {get; private set;} 
            global Date     LPI_LowRisk             {get; private set;}
            global Integer  EnrolledToDate          {get; private set;}                    
        
            public PatientEnrollmentWrapperGlobal( PBB_WR_APIs.PatientEnrollmentwrapper localWrapper ){
                FPI_Planned = localWrapper.FPI_Planned;
                LPI_Planned = localWrapper.LPI_Planned;
                TargetNoPatients = localWrapper.TargetNoPatients;
                LPI_HighRisk = localWrapper.LPI_HighRisk;
                LPI_MedRisk = localWrapper.LPI_MedRisk;
                LPI_LowRisk = localWrapper.LPI_LowRisk;
                EnrolledToDate = localWrapper.EnrolledToDate;
            }
      }
      
      global virtual class OperationResult{
        
            global String errorMessage { get; private set; }
            
            global List<Object> returnedValues { get; private set; }
            
            public OperationResult(){
                
            }
            
            public OperationResult(List<Object> returnedValues ){
                this.returnedValues = returnedValues ;
            }
            
            public OperationResult(String errorMessage){
                this.errorMessage = errorMessage;
            }
      }

    global class UserData extends OperationResult{
        
        global String email { get; private set; }
        global String phone { get; private set; }
        
        public UserData(User u){
            email = u.Email;
            phone = u.Phone;
        }

        public UserData(String errorMessage){
            super(errorMessage);
        }

    }
    
    global virtual class NotificationsPerProtocolResult extends OperationResult{
        
        global Map<String, Integer> notificationsPerProjects{ get; protected set; }
        
        public NotificationsPerProtocolResult(Map<String, Integer> notificationsPerProjects){
            this.notificationsPerProjects = notificationsPerProjects;
        }
        
        public NotificationsPerProtocolResult(String errorMessage){
            super(errorMessage);
        }
        
        protected NotificationsPerProtocolResult(){
        	
        }
        
    }
    
    global class RegistrationsPerProtocolResult extends NotificationsPerProtocolResult{
        
        global Map<String, Integer> registrationsPerProjects{ get; private set; }
        
        public RegistrationsPerProtocolResult(){
        }
        
        public RegistrationsPerProtocolResult(Map<String, Integer> registrationsPerProjects, Map<String, Integer> notificationsPerProjects){
            this.registrationsPerProjects = registrationsPerProjects;
            this.notificationsPerProjects = notificationsPerProjects;
        }
        
        public RegistrationsPerProtocolResult(String errorMessage){
            super(errorMessage);
        }
        
    }
    
	  /*global class ProjectDetailsResult{
	    
	        global String errorMessage { get; private set; }
	        
	        global List<PAWS_ProjectDetailCVO> returnedValues { get; private set; }
	        
	        public ProjectDetailsResult(List<PAWS_ProjectDetailCVO> returnedValues ){
	            this.returnedValues = returnedValues ;
	        }
	        
	        public ProjectDetailsResult(String errorMessage){
	            this.errorMessage = errorMessage;
	        }
	  }*/

}