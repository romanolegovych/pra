public with sharing class RM_UnitTestData {
    // 2 countries, 2 employees
    // 2 projects with 2 clients
    public list<Country__c> lstCountry; 
    
    public list<Employee_Details__c> lstEmpl; 
    public list<Employee_Status__c> lstEmplStatus;
    
    public list<WFM_Client__c> lstClient;
    public list<WFM_Contract__c>  lstContract;
    public list<WFM_Project__c> lstProject; 
    public List<WFM_Project_Role__c> lstProjectRole;
    public List<WFM_Project_Demand__c> lstProjectDemand;
    public list<WFM_EE_Therapeutic_Exp__c> lstTheras;
    public list<WFM_EE_LANGUAGE__c> lstLang;
    public list<WFM_EE_Education__c> lstEdu;
    public list<WFM_EE_CERTIFICATION__c> lstCer;
    public list<WFM_EE_SYSTEM_EXPERIENCE__c> lstSys;
    public list<WFM_EE_PHASE_EXPERIENCE__c> lstPhase;
    
  
    public Root_Folder__c root1;
    public Sub_Folder__c sub1;
    
    public list <WFM_Therapeutic__c> lstT;
    public list<WFM_Phase__c> lstP;
    public list<Job_Class_Desc__c> lstJobs;
    public list<Buf_Code__c> lstBuf;
    
    public list<WFM_JC_Group_Mapping__c> lstJCGmap;
    public list<WFM_BU_Group_Mapping__c> lstBUGmap;
    public list<PRA_Business_Unit__c> lstpraBU;
    public list<Group> lstGroup;
    public List<Business_Unit__c> lstLocation;
    RM_AMRReasonLookupSetting__c amrReason;
    
    
    public RM_ConstantSetting__c emailGroup;
    
    public RM_UnitTestData(){
        InsertEmpls();
        InsertEmplStatus();
        InsertClientProject();
        InsertLookup();
        InsertEmailConst();
        InsertGroupandMapping(5);
        lstLocation=InsertLocation(5);
    }
    public list<Country__c> InsertCountry(Integer nCountry){
        list<Country__c> lstCountry = new list<Country__c>();
        for (integer i = 0; i < nCountry; i++){
            Country__c country = new Country__c(name='TestCountry' + string.valueOf(i),PRA_Country_ID__c='100'+ string.valueOf(i), Country_Code__c='T'+string.valueOf(i),
                Region_Name__c='TestRegion' + string.valueOf(i), daily_Business_Hrs__c=8.0);
            lstCountry.add(country);
        }
        insert lstCountry;
        return lstCountry;
    }
     public list<Business_Unit__c> InsertLocation(Integer nLocation){
        list<Business_Unit__c> lstLocation = new list<Business_Unit__c>();
        for (integer i = 0; i < nLocation; i++){
            Business_Unit__c lc = new Business_Unit__c(name='TestLocation' + string.valueOf(i),PRA_Business_Unit_Id__c='100'+ string.valueOf(i), Abbreviation__c='TS'+string.valueOf(i),
                Default_Country__c=lstCountry[0].Id);
            lstLocation.add(lc);
        }
        insert lstLocation;
        return lstLocation;
    }
   /* public list<Country_BusinessUnit__c> InsertCountryBusinessUnit(Integer nCountryBU){
        lstCountry = InsertCountry(5);
        lstLocation = InsertLocation(5);
        
        list<Country_BusinessUnit__c> lstCountryBusinessUnit = new list<Country_BusinessUnit__c>();
        for (integer i = 0; i < nCountryBU; i++){
            Country_BusinessUnit__c country = new Country_BusinessUnit__c(name='TestCountryBU' + string.valueOf(i),Business_Unit__c=lstLocation[i].id, Country__c=lstCountry[i].Id,
                Country_BU_ID__c=lstLocation[i].Name+lstCountry[i].Name);
            lstCountryBusinessUnit.add(country);
        }
        insert lstCountryBusinessUnit;
        return lstCountryBusinessUnit;
    }*/
    public list<WFM_Country_Hours__c> insertCountryHr(integer startMonth, integer endMonth, ID countryRID) {
        list<string> months = RM_Tools.GetMonthsList(startMonth, endMonth);  
        //country hr
        list <WFM_Country_Hours__c> lstHrs = new list <WFM_Country_Hours__c>();            
        for (string m: months){
            WFM_Country_Hours__c ch = new WFM_Country_Hours__c(COUNTRY_Name__c = countryRID, country_hour_unique_key__c=countryRID+m, name='100',YEAR_MONTH__c=m,NBR_BUSINESS_DAYS_IN_MONTH__c=22);
            lstHrs.add(ch);
        }
        insert lstHrs;  
        return lstHrs;
    }
    public list<WFM_Location_Hours__c> insertLocationHr(integer startMonth, integer endMonth, string LocCode){
        list<string> months = RM_Tools.GetMonthsList(startMonth, endMonth);  
        list <WFM_Location_Hours__c> lstHrs = new list <WFM_Location_Hours__c>();    
        for (string m: months){
            WFM_Location_Hours__c  hrs = new WFM_Location_Hours__c(Location_Code__c=LocCode, NBR_Business_Day_In_Month__c=22,Loc_Hour_ExtID__c=LocCode+':'+m, Year_Month__c= m);
            lstHrs.add(hrs);
        }
        insert lstHrs;  
        return lstHrs;
    }  
    public list<WFM_Country_Hours__c> insertCountryHr(integer startMonth, integer endMonth, list<Country__c> lstC) {
        list<string> months = RM_Tools.GetMonthsList(startMonth, endMonth);  
        //country hr
        list <WFM_Country_Hours__c> lstHrs = new list <WFM_Country_Hours__c>(); 
        Integer i = 0;
        for (Country__c c : lstC){     
            for (string m: months){
                WFM_Country_Hours__c ch = new WFM_Country_Hours__c(COUNTRY_Name__c = c.id, country_hour_unique_key__c=c.name+':'+ m, name=string.valueOf(100+i),YEAR_MONTH__c=m,NBR_BUSINESS_DAYS_IN_MONTH__c=22);
                lstHrs.add(ch);
            }
            i++;
        }
        insert lstHrs;  
        return lstHrs;
    } 
      public void insertLookup(){
         //look up table
        lstT = new list <WFM_Therapeutic__c>();
        for (integer i = 0; i < 5; i++){
            WFM_Therapeutic__c t = new WFM_Therapeutic__c(name='test'+string.valueOf(i), Indication_Group__c='Group'+string.valueOf(i),Primary_Indication__c='Ind'+string.Valueof(i) );
            lstT.add(t);
        }
        insert lstT;
        lstP = new list<WFM_Phase__c>();
        for(integer i = 0; i<5; i++){
            WFM_Phase__c p = new WFM_Phase__c(name='Phase'+string.valueOf(i));
            lstP.add(p);
        }
        insert lstP;
          //insert lookup table
      
        lstBuf = new Buf_Code__c[]{
            new Buf_Code__c(name='KCICR', PRA_BUF_Code_Id__c='KCICR' ),
            new Buf_Code__c(name='KCICZ', PRA_BUF_Code_Id__c='KCICZ' ),
            new Buf_Code__c(name='TSTTT', PRA_BUF_Code_Id__c='TSTTT' )
        };
        
        insert lstBuf;
        
       // amrReason = new RM_AMRReasonLookupSetting__c (name='Test', lookup_Type__c ='AMR_Reason', value__c = 'Test');
       // insert amrReason;
    }
   
    public void InsertEmailConst(){
        emailGroup = new RM_ConstantSetting__c(name='RMGroupEmail', value__c = 'test@test.com');
    }
    public void InsertGroupandMapping(integer nGroup)
    {  
        lstGroup = new list<group>();
        lstpraBU = new list<PRA_Business_Unit__c>();
        lstJobs = new list<Job_Class_Desc__c>();
        lstBUGmap = new list<WFM_BU_Group_Mapping__c>();
        lstJCGmap = new list<WFM_JC_Group_Mapping__c>();
        for (integer i = 0; i < nGroup; i++){
            Group g1 = new Group(name = 'RM_Group' + string.valueOf(i),DeveloperName='RM_Group' + string.valueOf(i));
            lstGroup.add(g1);
            PRA_Business_Unit__c PBU = new PRA_Business_Unit__c(name = 'BusinessUnit'+ string.valueOf(i),Business_Unit_Code__c='BU'+ string.valueOf(i));        
            lstpraBU.add(PBU);
            
            Job_Class_Desc__c jc = new Job_Class_Desc__c(name='JobClass'+string.valueOf(i), Job_Class_ExtID__c='JobClass'+string.valueOf(i),Is_Assign__c = true, IsClinical__c = true, Job_Class_Code__c='C'+string.valueOf(i));
          
            lstJobs.add(jc);
            
        }        
        insert lstGroup;  
        insert lstpraBU; 
        insert lstJobs;
        system.debug('******'+lstpraBU);
        for (integer i = 0; i < nGroup; i++){
            WFM_BU_Group_Mapping__c buGroup = new WFM_BU_Group_Mapping__c(Group_Name__c=lstGroup[i].Name, PRA_Business_Unit__c=lstpraBU[i].ID);
            lstBUGmap.add(buGroup);
            WFM_JC_Group_Mapping__c jcGroup = new WFM_JC_Group_Mapping__c(Group_Name__c=lstGroup[i].Name, Job_Class_Desc__c=lstJobs[i].ID);
            lstJCGmap.add(jcGroup);
        }
     
        insert lstBUGmap;
        insert lstJCGmap;
        
        
    }
    public list<Employee_Details__c> InsertEmplsSupervisor(list<Country__c> lstC){
        //each country has one supervior
        list<Employee_Details__c>lstEmplFM = new list<Employee_Details__c>();        
        Integer i = 0;
        for (Country__c c: lstC){
            Employee_Details__c emplManager = new Employee_Details__c(COUNTRY_name__c = c.id, name='FM' + string.valueOf(i), Employee_Unique_Key__c='FM' + string.valueOf(i), First_Name__c='Fun'+ string.valueOf(i), 
            Last_Name__c='Manager'+ string.valueOf(i), email_Address__c='JManager@gmail.com', Job_Class_Desc__c='JobClass1',
            Function_code__c='TT',Buf_Code__c='TSTTT', State_Province__c = 'TestState', Business_Unit__c='TST', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), Manager_Last_Name__c='TestManager',  Manager_First_Name__c='TestFirst', 
            FTE_Equivalent__c = 0.8, NBR_PRA_Monitoring_Yrs__c = 5.0, NBR_Non_PRA_Monitoring_Yrs__c = 6.0, NBR_AT_PRA_Yrs__c=5.0);
            lstEmplFM.add(emplManager);
            i++;
        }
        insert lstEmplFM;
        return lstEmplFM;
               
    } 
     public list<Employee_Details__c> InsertActiveEmpls(Integer n, Integer startNum, Country__c country, Employee_Details__c supervisor){
        //only insert basic, it need to update for jc, bufcode
        
        lstEmpl = new list<Employee_Details__c>();
        for (integer i = 0; i < n; i++){
            Employee_Details__c empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='Empl'+ string.valueOf(startNum + i), Employee_Unique_Key__c='Empl'+ string.valueOf(startNum + i), 
            First_Name__c='First'+ string.valueOf(startNum + i), Last_Name__c='Last' + string.valueOf(startNum + i), email_Address__c='fl' + string.valueOf(startNum + i) + '@testpraintl.com', 
            //Job_Class_Desc__c='Clinical Research Associate', Function_code__c='TT',Buf_Code__c='TSTTT', State_Province__c = 'TestState', Business_Unit__c='TST', 
            Status__c='AA', Date_Hired__c=date.today().addMonths(-6), Manager_Last_Name__c=supervisor.last_name__c,  Manager_First_Name__c=supervisor.First_Name__c, 
            FTE_Equivalent__c = 0.8, NBR_PRA_Monitoring_Yrs__c = 5.0, NBR_Non_PRA_Monitoring_Yrs__c = 6.0, NBR_AT_PRA_Yrs__c=5.0, supervisor_id__c = supervisor.name);
            
            lstEmpl.add(empl);
        }
        insert lstEmpl;
        return lstEmpl;       
    } 
    //need be delete late
    public void InsertEmpls(){
        lstCountry = new list<Country__c>();
        Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0);
        lstCountry.add(country);
        Country__c country1 = new Country__c(name='TestCountry2',PRA_Country_ID__c='101', Country_Code__c='T2',Region_Name__c='North America2', 
        daily_Business_Hrs__c=7.5);
        lstCountry.add(country1);
        insert lstCountry;
     
              
        lstEmpl = new list<Employee_Details__c>();
        Employee_Details__c empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='1000', Employee_Unique_Key__c='1000', First_Name__c='John', Last_Name__c='TestSmith', email_Address__c='JSmith@gmail.com', Job_Class_Desc__c='Clinical Research Associate',
        Function_code__c='TT',Buf_Code__c='TSTTT', State_Province__c = 'TestState', Business_Unit__c='TST', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), Manager_Last_Name__c='TestManager',  Manager_First_Name__c='TestFirst', 
        FTE_Equivalent__c = 0.8, NBR_PRA_Monitoring_Yrs__c = 5.0, NBR_Non_PRA_Monitoring_Yrs__c = 6.0, NBR_AT_PRA_Yrs__c=5.0, supervisor_id__c = '1020', location_code__c = 'TTT');
        
        lstEmpl.add(empl);
        Employee_Details__c emplManager = new Employee_Details__c(COUNTRY_name__c = country.id, name='1020', Employee_Unique_Key__c='1020', First_Name__c='John', Last_Name__c='Manager', email_Address__c='JManager@gmail.com', Job_Class_Desc__c='Clinical Research Associate',
        Function_code__c='TT',Buf_Code__c='TSTTT', State_Province__c = 'TestState', Business_Unit__c='TST', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), Manager_Last_Name__c='TestManager',  Manager_First_Name__c='TestFirst', 
        FTE_Equivalent__c = 0.8, NBR_PRA_Monitoring_Yrs__c = 5.0, NBR_Non_PRA_Monitoring_Yrs__c = 6.0, NBR_AT_PRA_Yrs__c=5.0, location_code__c = 'TTT');
        lstEmpl.add(emplManager);
        Employee_Details__c emplMove = new Employee_Details__c(COUNTRY_name__c = country1.id, name='1010', Employee_Unique_Key__c='1010', First_Name__c='Cathrine', Last_Name__c='Smith',email_Address__c='CSmith@gmail.com', 
        FTE_Equivalent__c = 1.0, Function_code__c='CY',Buf_Code__c='KCICY', State_Province__c = 'TestState', Job_Class_Desc__c='Clinical Research Associate', Business_Unit__c='KCI', Status__c='AA', Date_Hired__c=date.parse('12/27/2011'), location_code__c = 'TTS');
        lstEmpl.add(emplMove);
        insert lstEmpl;
       
    }
    public void InsertEmplStatus(){
        lstEmplStatus = new list<Employee_Status__c>();
        Employee_Status__c emplStatus = new Employee_Status__c(employee_status__c = 'AA', employee_Type__c = 'Employees', Employee_Group__c = 'Active');
        lstEmplStatus.add(emplStatus);
        insert lstEmplStatus;
        
    }
    
     
    public void InsertOtherAttributeEmpl(ID emplID){
         lstTheras = new WFM_EE_Therapeutic_Exp__c[]{
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Colorectal', employee_id__c=emplID, Therapeutic_area__c='TestOncology',Indication_Group__c='Solid Tumors',NBR_Total_Monitoring_Exp__c=4.29, NBR_PRA_Monitoring_Exp_Yrs__c=4.29),
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Colorectal', employee_id__c=emplID, Therapeutic_area__c='TestOncology',Indication_Group__c='Solid Tumors',Role__c='UNDEFINED', NBR_Role_Experience_Yrs__c=0.43), 
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Colorectal', employee_id__c=emplID, Therapeutic_area__c='TestOncology',Indication_Group__c='Solid Tumors',Study_Drug_Type__c='UNDEFINED', NBR_Study_Drug_Yrs__c=0.43),   
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Colorectal', employee_id__c=emplID, Therapeutic_area__c='TestOncology',Indication_Group__c='Solid Tumors',Subject_Population__c='UNDEFINED'),                                  
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Sedation', employee_id__c=emplID, Therapeutic_area__c='TestAnesthesiology',Indication_Group__c='Anesthesia',Role__c='Clinical Team Manager', NBR_Role_Experience_Yrs__c=1.4),
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Sedation', employee_id__c=emplID, Therapeutic_area__c='TestAnesthesiology',Indication_Group__c='Anesthesia',NBR_Total_Monitoring_Exp__c=4.29, NBR_PRA_Monitoring_Exp_Yrs__c=4.29),
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Surgical Anesthesia', employee_id__c=emplID, Therapeutic_area__c='TestAnesthesiology',Indication_Group__c='Anesthesia',Role__c='UNDEFINED', NBR_Role_Experience_Yrs__c=1.92),
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Gout', employee_id__c=emplID, Therapeutic_area__c='TestEndocrinology',Indication_Group__c='Metabolic',Role__c='Clinical Team Manager', NBR_Role_Experience_Yrs__c=0.15)
            
        
        };
        insert lstTheras;
        lstEdu = new WFM_EE_Education__c[]{
            new WFM_EE_Education__c(Degree_Type__c='BA', employee_id__c=emplID,  Institution__c='Instituation', Month_Obtained__c= Date.parse('4/1/2010'))       
        };
        insert lstEdu;
        lstLang = new WFM_EE_LANGUAGE__c[]{
            new WFM_EE_LANGUAGE__c(Language__c='TestEnglish',employee_id__c=emplID, Level_of_Med_Term__c='Low',   Level_of_Speak__c='High',  Level_of_Write__c='High')
        };
        insert lstLang;
        
        lstCer = new WFM_EE_CERTIFICATION__c[]{
            new WFM_EE_CERTIFICATION__c(Certification_Affilication_Name__c='Certification', employee_id__c=emplID, Effective_Date__c = Date.parse('4/1/2010'),Granting_Institution__c='Granting')
        };
        insert lstCer;
        
       lstSys = new WFM_EE_SYSTEM_EXPERIENCE__c[]{
            new WFM_EE_SYSTEM_EXPERIENCE__c(CRS_System__c='Sys', employee_id__c=emplID,Proficiency_Level__c = 1)
        };
        insert lstSys;
        
        lstPhase = new WFM_EE_PHASE_EXPERIENCE__c[]{
            new WFM_EE_PHASE_EXPERIENCE__c(phase__c='IV', employee_id__c=emplID,Phase_experience_yrs__c=3)
        };
        insert lstPhase;
    }
       
    public void InsertClientProject(){
        lstClient = new list<WFM_Client__c>();
        WFM_Client__c client = new WFM_Client__c(name='TestClient', Client_Name__c='TestClinet_Name', Client_Unique_Key__c='TestClient');
        lstClient.add(client);
        WFM_Client__c client1 = new WFM_Client__c(name='TestClient1', Client_Name__c='TestClinet1_Name', Client_Unique_Key__c='TestClient1');
        lstClient.add(client1);
        insert lstClient;
        
        lstContract = new list<WFM_Contract__c>();
        WFM_Contract__c contract = new WFM_Contract__c(name='TestContract', Contract_Status__c='AA', Client_ID__c=client.id, Contract_Unique_Key__c='TestContract');
        lstContract.add(contract);
        WFM_Contract__c contract1 = new WFM_Contract__c(name='TestContract1', Contract_Status__c='AA', Client_ID__c=client1.id, Contract_Unique_Key__c='TestContract1');
        lstContract.add(contract1);
        insert lstContract;
        
        lstProject = new list<WFM_Project__c>();
        Date endDate = Date.today().addMonths(12);
        WFM_Project__c project = new WFM_Project__c(name='TestProject', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProject', project_status__c ='RM',  Status_Desc__c='Active',
        Currency_Code__c='USD', Project_Start_Date__c=date.parse('12/27/2009'), Project_End_Date__c=endDate);
        lstProject.add(project);
        
        WFM_Project__c projectB = new WFM_Project__c(name='TestProjectB', Contract_ID__c=contract1.id, Project_Unique_Key__c='TestProjectB', project_status__c ='BB',  Status_Desc__c='Bid', Project_Start_Date__c=date.parse('12/27/2013'),
        Bid_Defense_Date__c = date.parse('10/1/2013'), Project_End_Date__c=endDate);
        lstProject.add(projectB); 
        insert lstProject;
        
          
    } 
   
    public void insertProjDemand(){
        lstProjectRole = new List<WFM_Project_role__c>();        
        system.debug('************lstLocation'+lstLocation);
        WFM_Project_role__c projectRole = new WFM_Project_role__c(Business_Unit__c=lstLocation[0].id,Country__c=lstCountry[0].Id,Job_Class_Desc__c=lstJobs[0].Id,Project_Role_ID__c='TestProjectRole',WFM_Project__c=lstProject[0].Id);
        WFM_Project_role__c projectRole1 = new WFM_Project_role__c(Business_Unit__c=lstLocation[1].id,Country__c=lstCountry[0].Id,Job_Class_Desc__c=lstJobs[0].Id,Project_Role_ID__c='TestProjectRole2',WFM_Project__c=lstProject[0].Id);
        lstProjectRole.add(projectRole);
        lstProjectRole.add(projectRole1);
        insert lstProjectRole;
        
        List<WFM_Country_Hours__c> CountryHour = insertCountryHr(1, 12,lstCountry[0].Id );
        lstProjectDemand = new List<WFM_Project_demand__c>();
        WFM_Project_demand__c projectDemand = new WFM_Project_demand__c(Country_Hour__c=CountryHour[0].id,Demand_unit__c='FTE',Demand_Value__c=null,Planned_Unit__c='FTE',Planned_Value__c= 1,Project_Role__c= lstProjectRole[0].Id,Year_Month__c='2014-02',Project_Demand_ID__c='testdemand');
        WFM_Project_demand__c projectDemand1 = new WFM_Project_demand__c(Country_Hour__c=CountryHour[1].id,Demand_unit__c='FTE',Demand_Value__c=1,Planned_Unit__c='FTE',Planned_Value__c=Null,Project_Role__c= lstProjectRole[0].Id,Year_Month__c='2014-02',Project_Demand_ID__c='testdemand1');
        lstProjectDemand.add(projectDemand) ;
        lstProjectDemand.add(projectDemand1); 
        insert lstProjectDemand;
    }
    public WFM_Protocol__c insertProt(ID projRID){
        
        WFM_Protocol__c prot = new WFM_Protocol__c(name='TestProtocol', Therapeutic_Area__c='TestThera', Phase__c='II', Protocal_Unique_Key__c='TestProtocol',Project_ID__c=projRID,
            DB_Lock_DT__c=date.parse('12/27/2009'));
            
        insert prot;
        return prot;
    }
    public list<WFM_Site_Detail__c> insertSite(ID protRID){
        list<WFM_Site_Detail__c> lstSite = new list<WFM_Site_Detail__c>();
        for (integer i = 0; i < 5; i++){
            WFM_Site_Detail__c site = new WFM_Site_Detail__c(name='site'+string.valueOf(i), Project_Protocol__c=protRID, Site_ID__c='site'+string.valueOf(i),Country__c='United State',Region_Name__c='North American',
            CRF_Pages_Expected__c=i*10, Total_Forecasted_Visits__c=i*2,  Status__c='Test', Actual_Subj_Enroll__c=i*5);
            lstSite.add(site);
        }
        insert lstSite;
        return lstSite;
    }
   
    public list<WFM_Employee_Availability__c> insertAvailability(Employee_Details__c empl, list <WFM_Location_Hours__c> lstLocHr){   
        
        list<WFM_Employee_Availability__c> lstAva = new list<WFM_Employee_Availability__c>();
      
        for (integer i = 0; i < lstLocHr.size(); i++){
            WFM_Employee_Availability__c a = new WFM_Employee_Availability__c(EMPLOYEE_ID__c=empl.ID, year_month__c=lstLocHr[i].year_month__c, Availability_Unique_Key__c=empl.name+':'+lstLocHr[i].year_month__c, 
            pto_unit__c='Hours', PTO_value__c=8,  Location_Hours__c=lstLocHr[i].id);
            lstAva.add(a);
        }
        insert lstAva;
        return lstAva;
    }  
   
    public WFM_employee_Allocations__c insertAllocation(ID emplRID, ID projRID, string alloStartDate, string alloEndDate, string role, string countryName){ 
          //allocation  
         
        WFM_employee_Allocations__c alloc= new WFM_employee_Allocations__c(EMPLOYEE_ID__c=emplRID, Project_Buf_Code__c='TSTTT',Project_Country__c=countryName, Project_ID__c=projRID, Project_Function__c=role, 
            Status__c='Confirmed', allocation_start_date__c = date.parse(alloStartDate), allocation_end_date__c = date.parse(alloEndDate), Allocation_Unique_Key__c=(string)emplRID+projRID+role+countryName);
      
        insert alloc;
        return alloc;
    } 
    public WFM_employee_Allocations__c insertAllocation(Employee_Details__c empl, WFM_Project__c proj){ 
          //allocation  
         
        WFM_employee_Allocations__c alloc= new WFM_employee_Allocations__c(EMPLOYEE_ID__c=empl.ID, Project_Buf_Code__c=empl.Buf_Code__c,Project_Country__c=empl.Country_Name__r.name, Project_ID__c=proj.ID, Project_Function__c=empl.Job_Class_Desc__c, 
            Status__c='Confirmed', allocation_start_date__c = Date.today(), allocation_end_date__c = proj.Project_End_Date__c, Allocation_Unique_Key__c=empl.name+'-' + proj.name + '-'+empl.Buf_Code__c+'-' + empl.Job_Class_Desc__c + '-' + empl.Country_Name__r.name);
      
        insert alloc;
        return alloc;
    } 
    public List<WFM_Employee_Assignment__c> insertAssignment(ID emplRID, WFM_employee_Allocations__c alloc, list<WFM_Employee_Availability__c> lstAva, decimal FTE){   
        //assignment
        List<WFM_Employee_Assignment__c> lstAssign = new List<WFM_Employee_Assignment__c>();
        for (Integer i = 0; i < lstAva.size(); i++){    
            WFM_Employee_Assignment__c ass =  new WFM_Employee_Assignment__c(Assignment_Unique_Key__c=lstAva[i].id+(string)alloc.id, EMPLOYEE_ID__c = emplRID, Availability_FK__c=lstAva[i].id, Allocation_Key__c=alloc.id, Year_Month__c=lstAva[i].year_month__c, 
            Assigned_value__c=FTE, assigned_unit__c = 'FTE');
            
            lstAssign.add(ass);
        }
        insert lstAssign;
        return lstAssign;
    }
    public list<WFM_EE_Work_History__c> insertWorkHist(ID projRID, ID emplRID, list<WFM_Employee_Availability__c> lstAva){   
        // four month history
        list <string> monthHistry = RM_Tools.GetMonthsList(-4, -1);
        list<WFM_EE_Work_History__c> lstHistory = new list<WFM_EE_Work_History__c>();
    
        for (integer i = 0; i < monthHistry.size(); i++){
            WFM_EE_Work_History__c h = new WFM_EE_Work_History__c(Project_ID__c=projRID, employee_id__c=emplRID, Availability__c = lstAva[i].id, Work_History_Unique_Key__c=emplRID+(string)projRID+monthHistry[i]+'KCIPR',Buf_Code__c='KCIPR',year_month__c=monthHistry[i], Hours__c=20);
            lstHistory.add(h);
        }
        insert lstHistory;
        return lstHistory;
    }
    public list<WFM_EE_Work_History_Summary__c> insertWorkHistSummary(ID projRID, ID emplRID){
        list<WFM_EE_Work_History_Summary__c> lstHistSummary = new list<WFM_EE_Work_History_Summary__c>();
        WFM_EE_Work_History_Summary__c hisS = new WFM_EE_Work_History_Summary__c(Employee_ID__c = emplRID, Project__c=projRID, Start_Year_Month__c=RM_Tools.GetYearMonth(-5), End_Year_Month__c=RM_Tools.GetYearMonth(-1), Buf_Code__c='BBBBB',Sum_Worked_Hours__c=100.0,Work_History_Summary_Unique_Key__c =emplRID+(string)projRID+'KCIPR');
        lstHistSummary.add(hisS);
        insert lstHistSummary;
        return lstHistSummary;
    }
    
  /*  public WFM_Assignment_Mod_Req__c InsertFlag(ID emplRID, ID AllocationRID, string modifyType, string changeRequest, string Reason){
        WFM_Assignment_Mod_Req__c mod = new WFM_Assignment_Mod_Req__c( Allocations__c = AllocationRID, Modification_Type__c=modifyType, Description__c = 'Test', 
                                            Change_Request__c=changeRequest,  Reason__c = Reason,  Comment__c='Test', employee_id__c = emplRID, Request_Role__c = 'FM');
        insert mod;
        return mod;
    }*/
    
    public list<WFM_AMR_Detail__c > InsertModDetail(ID modRID, string selectedChange, string FTE, string effDate, string endDate, string delayMonth){
        list<WFM_AMR_Detail__c> lstDetail = new list<WFM_AMR_Detail__c>(); 
        if (selectedChange == 'FTE Allocation'){
            WFM_AMR_Detail__c amr = new WFM_AMR_Detail__c(Assignment_Modification__c = modRID, detail_desc__c= 'Test', Detail_Value__c= FTE, Detail_Type__c='FTE'); 
            lstDetail.add(amr);
            amr = new WFM_AMR_Detail__c(Assignment_Modification__c = modRID, detail_desc__c= 'Test', Detail_Value__c= effDate, Detail_Type__c='effDate'); 
            lstDetail.add(amr);
            if (endDate != '' && endDate != null){
                amr = new WFM_AMR_Detail__c(Assignment_Modification__c = modRID, detail_desc__c= 'Test', Detail_Value__c= endDate, Detail_Type__c='endDate'); 
                lstDetail.add(amr);
            }                               
        }
        else if (selectedChange == 'Delay Start' ||selectedChange == 'Push Assignment') {
            WFM_AMR_Detail__c amr = new WFM_AMR_Detail__c(Assignment_Modification__c = modRID, detail_desc__c= 'Test', Detail_Value__c= delayMonth, Detail_Type__c='delayMonth'); 
            lstDetail.add(amr);
        }
        else if (selectedChange == 'End Assignment' ||selectedChange == 'Move Assignment'){
            WFM_AMR_Detail__c amr = new WFM_AMR_Detail__c(Assignment_Modification__c = modRID, detail_desc__c= 'Test', Detail_Value__c= effDate, Detail_Type__c='effDate'); 
            lstDetail.add(amr);
        }
        insert lstDetail;
        return lstDetail;
    }
  
    
}