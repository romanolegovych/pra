@isTest
private class BDT_NewEditFlowchartTest {
	
	static testmethod  void BDT_NewEditFlowchartTest(){
	
	
		// create test data
		Client_Project__c cp = BDT_TestDataUtils.buildProject();
		insert cp;
		List<ClinicalDesign__c> designList = BDT_TestDataUtils.buildClinicDesign(cp,1);
		insert designList;
		List<Flowcharts__c> flowchartList =  BDT_TestDataUtils.buildFlowcharts(designList,1);
		insert flowchartList;
		List<Arm__c> armList = BDT_TestDataUtils.buildArms(designList,1);
		insert armList;
		List<Epoch__c> epochList = BDT_TestDataUtils.buildEpochs(designList,1);
		insert epochList;
		
		List<flowchartassignment__c> FlowchartAssignmentList = BDT_TestDataUtils.buildFlowchartAssignment (designList,
																flowchartList,
																armList,
																epochList);
		insert FlowchartAssignmentList;
		
		List<ServiceCategory__c> categoryList = BDT_TestDataUtils.buildServiceCategory(3, null);
		insert categoryList;
		List<Service__c> serviceList = BDT_TestDataUtils.buildService(categoryList, 3, 'tst', true);
		insert serviceList;
		List<ProjectService__c> projectServiceList = BDT_TestDataUtils.buildProjectService(serviceList, cp.Id);
		insert projectServiceList;
		List<ProjectServiceTimePoint__c> projectServiceTimepointList = 
			BDT_TestDataUtils.buildProjectServiceTimePointNew(projectServiceList, flowchartList);
		insert projectServiceTimepointList;

		user testUser = BDT_TestDataUtils.buildTestUserList(1)[0];

		system.runAs(testUser) {
			
			BDT_UserPreferenceService.staticSetProject(cp.id);
			BDT_NewEditFlowchart controller = new BDT_NewEditFlowchart();
			
			// select a design
			PageReference page = new Pagereference( system.Page.BDT_NewEditFlowchart.getUrl());
			page.getParameters().put('DesignId',designList[0].id);
			Test.setCurrentPage(page);
			controller.readClinicalDesign();
			
			// select a flowchart
			controller.FlowchartId = flowchartList[0].id;
			controller.readFlowchart();
			
			// add and remove a timepoint
			controller.showTimepoints();
			//user input on screen
			controller.AddTimepointStartDay 		= 10;
			controller.AddTimepointStartHour 		= 0;
			controller.AddTimepointStartMinute 		= 0;
			controller.AddTimepointIntervalDay 		= 0;
			controller.AddTimepointIntervalHour 	= 0;
			controller.AddTimepointIntervalMinute 	= 30;
			controller.AddTimepointEndDay			= 10;
			controller.AddTimepointEndHour			= 6;
			controller.AddTimepointEndMinute 		= 0;
			controller.createTimepointsByEndTime();
			
			controller.AddTimepointStartDay 		= 11;
			controller.AddTimepointStartHour 		= 0;
			controller.AddTimepointStartMinute 		= 0;
			controller.AddTimepointIntervalDay 		= 0;
			controller.AddTimepointIntervalHour 	= 0;
			controller.AddTimepointIntervalMinute 	= 30;
			controller.AddTimepointTotal = 2;
			controller.createTimepointsByRepeats();
			
			controller.TimePointsToDelete = '14400';
			controller.deleteTimePoint();
			
			controller.returnToFlowchart();
			
			controller.showCategoryServiceSelection ();
			
			// select a category
			page = new Pagereference( system.Page.BDT_NewEditFlowchart.getUrl());
			page.getParameters().put('CategoryNumber','1');
			Test.setCurrentPage(page);
			controller.showServiceInCategory ();
			
			controller.returnToFlowchart();
			
			page = new Pagereference( system.Page.BDT_NewEditFlowchart.getUrl());
			page.getParameters().put('DesignId',designList[0].id);
			Test.setCurrentPage(page);
			controller.save();
		}
		
	
	}
	

}