@isTest
private class ServiceModelTest {
	
	private static final String TEST_VALUE = 'TEST_VALUE';
		
	private static ServiceModel sm;
	
	private static List<Service_Model__c> serviceModels;
	private static PBB_TestUtils testUtils = new PBB_TestUtils();
	
	@isTest
	private static void testProperties(){
		sm = new ServiceModel();
		Test.startTest();
			sm.serviceModels = new List<Service_Model__c>{ new Service_Model__c() };
			sm.serviceModel = new Service_Model__c();
			sm.serviceModelForUpdate = new Service_Model__c();
		Test.stopTest();
		System.assertEquals( ServiceModel.MODEL_WORD, sm.getServiceWord() );
		System.assertEquals( 1, sm.serviceModels.size() );
		System.assertNotEquals( null, sm.serviceModel );
		System.assertNotEquals( null, sm.serviceModelForUpdate );
	}
	
	@isTest
	private static void refreshServicesTest(){
		sm = new ServiceModel();
		testUtils.CreateServiceModelAttributes();
		Test.startTest();
			sm.refreshServices();
		Test.stopTest();
		System.assertEquals( 1, sm.serviceModels.size() );
	}
	
	@isTest
	private static void preparationCreateOrEditServiceTestCreateNew(){
		Test.setCurrentPage( Page.LayoutModel );
		sm = new ServiceModel();
		Test.startTest();
			sm.preparationCreateOrEditService();
		Test.stopTest();
		System.assertEquals( null, sm.serviceModelForUpdate.Id );
	}

	@isTest
	private static void preparationCreateOrEditServiceTestUpdate(){
		sm = new ServiceModel();
		sm.serviceModel = testUtils.CreateServiceModelAttributes();
		Test.setCurrentPage( Page.LayoutModel );
		ApexPages.currentPage().getParameters().put( 'editServiceModelId', sm.serviceModel.Id );
		Test.startTest();
			sm.preparationCreateOrEditService();
		Test.stopTest();
		System.assertEquals( sm.serviceModel.Id, sm.serviceModelForUpdate.Id );
	}
	
	@isTest
	private static void preparationCreateOrEditServiceTestClone(){
		sm = new ServiceModel();
		sm.serviceModel = testUtils.CreateServiceModelAttributes();
		sm.serviceModel.Name = TEST_VALUE;
		Test.setCurrentPage( Page.LayoutModel );
		ApexPages.currentPage().getParameters().put( 'cloneServiceModelId', sm.serviceModel.Id );
		Test.startTest();
			sm.preparationCreateOrEditService();
		Test.stopTest();
		System.assertEquals( null, sm.serviceModelForUpdate.Id );
		System.assertEquals( TEST_VALUE, sm.serviceModelForUpdate.Name );
	}
	
	@isTest
	private static void removeServiceTest(){
		sm = new ServiceModel();
		sm.serviceModel = testUtils.CreateServiceModelAttributes();
		Test.startTest();
			sm.removeService();
		Test.stopTest();
		System.assertEquals( 0, sm.serviceModels.size() );
		
	}
	
	@isTest
	private static void removeServiceTestNullPointer(){
		sm = new ServiceModel();
		Test.setCurrentPage( Page.LayoutModel );
		Test.startTest();
			sm.removeService();
		Test.stopTest();
		System.assertEquals( 1, ApexPages.getMessages().size() );
	}
	
}