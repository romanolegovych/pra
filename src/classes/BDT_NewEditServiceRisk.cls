public with sharing class BDT_NewEditServiceRisk {
	public List<ServiceRisk__c> ServiceRiskList {get;set;}
	//public String ServiceCategoryId{get;set;}
	public Service__c service  {get;set;}
	public String ServiceId{get;set;}
	public List<ServiceRisk__c> deletedRisks{get;set;}
	 
	public BDT_NewEditServiceRisk(){
		loadServiceRisks();
		retrieveServiceCategory();
		deletedRisks = new List<ServiceRisk__c>();
	}
	
	public void loadServiceRisks(){
		ServiceId = system.currentPageReference().getParameters().get('ServiceId');
		try{
			ServiceRiskList = [Select name, id, CommonRIsk__c, CommonMitigation__c, service__c 
								from ServiceRisk__c 
								where service__c = :ServiceId
								and BDTDeleted__c = false];
		}
		catch( Queryexception e){
			ServiceRiskList = new List<ServiceRisk__c>();
		}
	}
	
	public void retrieveServiceCategory(){
		
		try{
			service = [Select name, servicecategory__c, servicecategory__r.name 
									from service__c
									where id = :ServiceId];
		}catch( Queryexception e){		
			service = new Service__c();
		}					
		//ServiceCategoryId = tmpList[0].ServiceCategory__c;
	}
	
	public void addServiceRisk(){
		ServiceRiskList.Add(new ServiceRisk__c(service__c = ServiceId));
	}
	
	public List<ServiceRisk__c> cleanServiceRisks( List<ServiceRisk__c> listToClean ){
	
		List<ServiceRisk__c> cleanList = new List<ServiceRisk__c>();
		
		for(ServiceRisk__c sr: listToClean){
			//remove elements that have no risk nor a mitigation description
			if(sr.CommonRisk__c!=null||sr.CommonMitigation__c!=null){
				cleanList.add(sr);
			}
		}
		return cleanList;
	}
	
	
	public PageReference save(){
		/* clean the list before saving (remove empty entries) */
		ServiceRiskList = cleanServiceRisks( ServiceRiskList );
		try{
			upsert ServiceRiskList;	
		}
		catch( DMLException e ){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Service Risk could not be saved !');
        	ApexPages.addMessage(msg);
		}
		
		/* Update deleted service risks */
		if(!deletedRisks.isEmpty() ){
			try{
				upsert deletedRisks;
			}
			catch(DMLException e){
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Service Risk could not be deleted !');
        		ApexPages.addMessage(msg);	
			}
		}
		/* return to overview of service risks page*/
		PageReference serviceRisksPage = new PageReference(System.Page.BDT_ServiceRisks.getUrl());
		serviceRisksPage.getParameters().put('ScId',service.ServiceCategory__c );
		
		return serviceRisksPage;
	}
	
	public PageReference cancel(){
		PageReference serviceRisksPage = new PageReference(System.Page.BDT_ServiceRisks.getUrl());
		serviceRisksPage.getParameters().put('ScId',service.ServiceCategory__c );
		
		return serviceRisksPage;
	}
	
	public void deleteSoft(){
       String ServiceRiskId=system.currentPageReference().getParameters().get('ServiceRiskId');
       //system.debug('got serviceRiskId: '+ServiceRiskId);
       integer i = 0;
       integer index;
       for(ServiceRisk__c sr: ServiceRiskList){
       		if(sr.Id==ServiceRiskId){
       			index = i;
       		}
       		i++;	
       }
	   ServiceRiskList[index].BDTDeleted__c = true;
	   deletedRisks.add(ServiceRiskList[index]);
	   ServiceRiskList.remove(index);
	}

}