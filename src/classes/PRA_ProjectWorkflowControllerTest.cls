/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

/**
*   'PRA_ProjectWorkflowControllerTest' class is to cover the test coverage for 'PRA_ProjectWorkflowController' class.
*   @author   Devaram Bhargav
*   @version  07-Nov-2013
*   @since    07-Nov-2013
*/
@isTest
private class PRA_ProjectWorkflowControllerTest {
    
    static testMethod void testconstructor() {
        
        // given 
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        // Zero out all effort ratios
        List<Client_Task__c> ctList = [select id,project__r.name,Project__c,status__c
                                          from Client_Task__c
                                          where Project__r.id = :tu.clientProject.id
                                          ];
        ctList[0].status__c=PRA_Constants.CU_STATUS_NEW;
        ctList[1].status__c=PRA_Constants.CU_STATUS_READY;
        update ctList;
        // when
        test.startTest();
            PageReference oPage = new PageReference('/apex/PRA_ProjectWorkflow?id='+tu.wbsProject.id);
            Test.setCurrentPage(oPage);
            PRA_ProjectWorkflowController prawbs = new PRA_ProjectWorkflowController();
            System.assertEquals(true, prawbs.InitiateWBS);
            System.assertEquals(true, prawbs.ExportWBS);
            System.assertEquals(false, prawbs.ConfirmWBS);
            prawbs.viewWBSProject();
        	System.assertEquals(prawbs.PAGE_MODE,prawbs.PAGE_MODE_VIEW);        
            prawbs.editWBSProject();
        	System.assertEquals(prawbs.PAGE_MODE,prawbs.PAGE_MODE_EDIT);
            prawbs.wbsProject.Activity_Group__c='ABC';
            prawbs.saveWBSProject();
            System.assertEquals('ABC',prawbs.wbsProject.Activity_Group__c);
            prawbs.InitiateWBSProject();
            System.assertEquals(false, prawbs.InitiateWBS);
        	prawbs.ExcelButton='WBS';
            prawbs.SaveandExportWBSProject();
            System.assertEquals(true, prawbs.ExportWBS);
            System.assertEquals(false, prawbs.ConfirmWBS);
        	System.assertEquals(prawbs.ExcelButton,'WBS');
            prawbs.exportfile();
            System.assertEquals(prawbs.ExcelButton,'WBS');
            prawbs.ExcelButton='BD1';
            prawbs.exportfile();
            System.assertEquals(prawbs.ExcelButton,'BD1');
            prawbs.confirmWBSProject();
            System.assertEquals(false, prawbs.ConfirmWBS);
            prawbs.getShowPZblock();
            prawbs.getShowFAblock();        
        test.stopTest();        
    } 
}