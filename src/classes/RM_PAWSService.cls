/**
* @author       Min Chen
* @description  RM PAWSService provide API for PAWS (which include dataaccesor here)
                
*/
public with sharing class RM_PAWSService {
	   /**
        * @author   Min Chen
        * @date     Feb 2015   
        * @description  Implement getActiveAssignmentByProject for PAWS     
        **/
	  public static list<RM_PAWSCVO> getActiveAssignmentByProject(string projectID){
	  		list<RM_PAWSCVO> lstAss = new list<RM_PAWSCVO>();
	  		for (WFM_employee_Allocations__c a: getAssignmentByProject(projectID)){
	  			RM_PAWSCVO actCVO = new RM_PAWSCVO(a);
	  			lstAss.add(actCVO);
	  		}
	  		return lstAss;
	  }
	   /**
        * @author   Min Chen
        * @date     Feb 2015   
        * @description  Implement getActiveAssignmentByProjectinBU for PAWS    
        **/
     
     public static list<RM_PAWSCVO> getActiveAssignmentByProjectinBU(string projectID, string BU){
	  		list<RM_PAWSCVO> lstAss = new list<RM_PAWSCVO>();
	  		for (WFM_employee_Allocations__c a: getAssignmentByProjectinBU(projectID, BU)){
	  			RM_PAWSCVO actCVO = new RM_PAWSCVO(a);
	  			lstAss.add(actCVO);
	  		}
	  		return lstAss;
	  }
	     /**
        * @author   Min Chen
        * @date     Feb 2015   
        * @description  Implement getActiveAssignmentByProjectinBUwithRole for PAWS    
        **/
	  public static list<RM_PAWSCVO> getActiveAssignmentByProjectinBUwithRole(string projectID, string BU, string Role){
	  		list<RM_PAWSCVO> lstAss = new list<RM_PAWSCVO>();
	  		for (WFM_employee_Allocations__c a: getAssignmentByProjectinBUwithRole(projectID, BU, Role)){
	  			RM_PAWSCVO actCVO = new RM_PAWSCVO(a);
	  			lstAss.add(actCVO);
	  		}
	  		return lstAss;
	  }
	  
	    /**
        * @author   Min Chen
        * @date     Feb 2015   
        * @description  Implement DataAccessor
        **/
	  
	  private static List<WFM_employee_Allocations__c> getAssignmentByProject(string projectID){
        
            
        return [SELECT id, project_id__c, EMPLOYEE_ID__c, employee_id__r.full_name__c, employee_id__r.first_name__c, employee_id__r.last_name__c, 
        	employee_id__r.business_unit_desc__c, project_id__r.name, project_id__r.Status_Desc__c, Project_Buf_Code__c, Project_Country__c,
			project_function__c, Type__c, Allocation_End_Date__c, Allocation_Start_Date__c, status__c, Is_lead__c,employee_id__r.user__c              
        FROM WFM_employee_Allocations__c
        where project_id__r.name = :projectID 
            and Allocation_End_Date__c!= null           
            and Allocation_End_Date__c > :Date.Today()
            and status__c = 'Confirmed'
            and Type__c = 'Assignment'
            
        order by employee_id__r.full_name__c limit 1000];
           
          
       
    }
      private static List<WFM_employee_Allocations__c> getAssignmentByProjectinBU(string projectID, string BU){
          return [SELECT id, project_id__c, EMPLOYEE_ID__c, employee_id__r.full_name__c, employee_id__r.first_name__c, employee_id__r.last_name__c, 
          employee_id__r.business_unit_desc__c, project_id__r.name, project_id__r.Status_Desc__c, Project_Buf_Code__c, Project_Country__c, 
          project_function__c, Type__c, Allocation_End_Date__c, Allocation_Start_Date__c, status__c, Is_lead__c, employee_id__r.user__c               
        FROM WFM_employee_Allocations__c
        where project_id__r.name = :projectID 
            and Allocation_End_Date__c!= null 
            and employee_id__r.business_unit_desc__c=:BU 
            and Allocation_End_Date__c > :Date.Today()
            and status__c = 'Confirmed'
            and Type__c = 'Assignment'
            
        order by employee_id__r.full_name__c limit 1000];
           
          
         
    }
      private static List<WFM_employee_Allocations__c> getAssignmentByProjectinBUwithRole(string projectID, string BU, string projectRole){
         return [SELECT id, project_id__c, EMPLOYEE_ID__c, employee_id__r.full_name__c, employee_id__r.first_name__c, employee_id__r.last_name__c, 
         	employee_id__r.business_unit_desc__c, project_id__r.name, project_id__r.Status_Desc__c, Project_Buf_Code__c, Project_Country__c, 
         	project_function__c, Type__c, Allocation_End_Date__c, Allocation_Start_Date__c, status__c, Is_lead__c, employee_id__r.user__c               
        FROM WFM_employee_Allocations__c
        where project_id__r.name = :projectID 
            and Allocation_End_Date__c!= null 
            and employee_id__r.business_unit_desc__c=:BU 
            and project_function__c = :projectRole
            and Allocation_End_Date__c > :Date.Today()
            and status__c = 'Confirmed'
            and Type__c = 'Assignment'
            
        order by employee_id__r.full_name__c limit 1000];
    }
}