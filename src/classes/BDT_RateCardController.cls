public with sharing class BDT_RateCardController {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} //Administration Rate Cards
	public String  panelToDisplay  {get;set;}
	public String  RateCardId      {get;set;}
	public String  RateCardClassId {get;set;}
	
	public BDT_RateCardController() {
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Rate Cards');
		SWOut = new List<ServiceWrapper>();
		showDeletedServices 		= False;
		showDeletedRateCard 		= False;
		showDeletedRAteCardClass 	= False;
		ServiceCategory__c s = new ServiceCategory__c();
		 
		if (ServiceCategoryId!='empty' || ServiceCategoryId!=''){
			try{
				s = [select name, code__c from servicecategory__c where id = :ServiceCategoryId limit 1];
			}catch(QueryException e){}
			
			ServiceCategoryName = s.name;
		}
		
		buildRCCOut();
	}

/* Service Category */
	public  String 	ServiceCategoryId{
		get{if (ServiceCategoryId == null) {
				ServiceCategoryId = System.currentPageReference().getParameters().get('ServiceCategoryId');
				} 
			 
			 return ServiceCategoryId;
		}
		set{
			servicecategory__c s = [select name, code__c from servicecategory__c where id = :value limit 1];
			ServiceCategoryName = s.name;
			ServiceCategoryId=value;
			buildRCCOut();
		}
	}
	public  String 	ServiceCategoryName			{get;set;}
	
/* Business unit select list */
	public String BusinessUnit {
		get {if (BusinessUnit == null) {
				BusinessUnit = System.currentPageReference().getParameters().get('BusinessUnit');
				} 
			 return BusinessUnit;}
		set {if (value != BusinessUnit) {
				BusinessUnit = value;
				SWOut	= new List<ServiceWrapper>();
				if (value!='empty'&&value!=null&&value!='') {
					// rebuild list of ratecards
					buildRCCOut();
				}
			}
		}
	}
	
	public String getBusinessUnitName() {
		String result = '';
		try {
			Business_Unit__c BU = [Select Name, Id From Business_Unit__c where id = :BusinessUnit];
			result = bu.name;
		} catch (Exception e) {}
		return result;
	}
	
	public String getBusinessUnitCurrency() {
		String result = '';
		try {
			result = bdt_utils.getCurrencyForBusinessUnit(BusinessUnit).Name;
		} catch (Exception e) {}
		return result;
	}

	public List<SelectOption> getBUList() {

		List<SelectOption> SO = new List<SelectOption>();
		SO.add(new SelectOption('empty','--Select--'));

		List<Business_Unit__c> BU = new List<Business_Unit__c>();
		BU = [Select b.Name, b.Id From Business_Unit__c b];
		
		for(Business_Unit__c B: BU){
			SO.add(new SelectOption(B.Id,B.name));			
		}
		
		return SO;
	} 
	
/* Display the service categories */

	public List<ServiceCategory__c> getServiceCategories() {
		List<ServiceCategory__c> SC = new List<ServiceCategory__c>();
		SC = [Select s.Name,
					s.Id,
					s.Code__c,
					(select code__c From ServiceCategories__r where BDTDeleted__c  = false)
				From ServiceCategory__c s 
				where BDTDeleted__c = false
				order by s.Code__c];
		
		// codes are like 001.002.001 and we like to display as 1.2.1
		for (ServiceCategory__c s : SC){
			String tmpStr = BDT_Utils.removeLeadingZerosFromCat(s.Code__c);
			if( tmpStr!=NULL && tmpStr.contains('.') ) {
				tmpStr = tmpStr.substringBeforeLast('.') + '.:' + tmpStr.substringAfterLast('.');
			} else {
				tmpStr = ':' + tmpStr;
			}
			s.Code__c = tmpStr;
		}
		
		return SC; 
	}

/* Display the ratecards */

	public boolean showRateCardList{
		get{			
			Boolean result = true;
			
			if (BusinessUnit=='empty' || BusinessUnit=='') {result = false;}
			 
			return result; 
		   }
		Set;
	}
	
	public	List<ServiceWrapper> SWOut 			{get;set;} 
	public List<DisplayWrapper> Dout{get;set;}
	
	public  boolean showDeletedServices 		{get{return false;}set;}
	public  boolean showDeletedRateCard 		{get{return false;}set;} 
	public  boolean showDeletedRateCardClass	{get{return false;}set;}
	
	public void buildRCCOut() {
		
		SWOut	= new List<ServiceWrapper>();
		
		/* it only makes sense to run this when a business unit has been selected */
		
		if (BusinessUnit != '' && ServiceCategoryId != '' ) {
			system.debug('BusinessUnit: '+ BusinessUnit);
			system.debug('ServiceCategoryId: '+ ServiceCategoryId);
			
			Set<ID> ServiceIds 	= new Set<ID>();
			Set<ID> RateCardIds = new Set<ID>();
			SWOut	= new List<ServiceWrapper>();
			
			Map<ID,List<RateCard__c>> SrvRtcMAP 	 = new Map<ID,List<RateCard__c>>();
			Map<ID,List<RateCardClass__c>> RtcRccMAP = new Map<ID,List<RateCardClass__c>>();
			
			
			// get all the services 
			
			List<Service__c> S = new List<Service__c>();
			S = [Select s.Name, s.Id
				 From   Service__c s
				 Where  ServiceCategory__c = :ServiceCategoryId
				 and    ((BDTDeleted__c  = :showDeletedServices) or (BDTDeleted__c  = false))
				  	    Order by s.SequenceNumber__c];
			// store the found ID's for selection of ratecards
			for (Service__c mS:S) {ServiceIds.add(mS.id);}
		
			// get all the ratecards for the specific business unit and for specific services
			for (RateCard__c R : [Select  r.Id
								    	, r.name
										, r.Business_Unit__c
										, r.Service__c, Service__r.SequenceNumber__c
										, r.UnitDescription__c
								  From  RateCard__c r
								  where r.Service__c 			= :ServiceIds
								  and   r.Business_Unit__c 		= :BusinessUnit
								  and   ((BDTDeleted__c  = :showDeletedRateCard) or (BDTDeleted__c  = false)) 
								  order by Service__r.SequenceNumber__c]) {
				// store found ID's for selection of ratecardclasses
				RateCardIds.add(R.id);
				// update the mapping of services to ratecards
				if (SrvRtcMAP.containsKey(R.Service__c)) {
					SrvRtcMAP.get(R.Service__c).add(R); // extend list
				}else{
					SrvRtcMAP.put(R.Service__c,new List<RateCard__c>{R}); // create list
				}
			}
		
			
			for (Ratecardclass__c RC : [Select  r.id 
										    	, r.UnitPrice__c
												, r.RateCard__c, RateCard__r.Service__r.SequenceNumber__c
												, r.Name
												, r.MaximumUnits__c 
										   From RateCardClass__c r 
										  where ratecard__c = :RateCardIds
										  and   ((BDTDeleted__c  = :showDeletedRateCardClass) or (BDTDeleted__c  = false))
										  order by MaximumUnits__c desc,UnitPrice__c]) {
				// update the mapping of ratecard to ratecardclasses
				if (RtcRccMAP.containsKey(RC.RateCard__c)) {
					RtcRccMAP.get(RC.RateCard__c).add(RC); // extend list
				}else{
					RtcRccMAP.put(RC.RateCard__c,new List<RateCardClass__c>{RC}); // create list
				}
			}
		
		
			// create the content of the wrapper
			for (Service__c Service: S){
				ServiceWrapper sw = new ServiceWrapper();
				sw.service  = Service;
				
				// see if there is a ratecard for the service
				if (SrvRtcMAP.get(Service.id) != null) {
					for(RateCard__c RateCard: SrvRtcMAP.get(Service.id)){
						
						RateCardWrapper rw 	 = new RateCardWrapper();
						rw.RateCard 		 = RateCard;
						rw.RateCardClass 	 = RtcRccMAP.get(RateCard.id);
						sw.RCW.add(rw);
					
					}
				}
				
				SWOut.add(sw);
				
			}
			
			
			Dout = new List<DisplayWrapper>();
			DisplayWrapper dw = new DisplayWrapper();
			for (Service__c Service: S){				
				dw = new DisplayWrapper();
				dw.name = Service.name;
				dw.serviceID   = service.id;
						
				
					if (SrvRtcMAP.get(Service.id) != null) {
						for(RateCard__c RateCard: SrvRtcMAP.get(Service.id)){
						dw.ratecardID  		= RateCard.id;	
						if (RtcRccMAP.get(RateCard.id) == null){
							Dout.add(dw);
							dw = new DisplayWrapper();
						} else if (RtcRccMAP.get(RateCard.id).size() == 1) {
						
							for (RateCardClass__c RateCardClass: RtcRccMAP.get(RateCard.id)) {
								dw.price = string.valueOf(RateCardClass.UnitPrice__c);
								dw.ratecardclassID 	= RateCardClass.id;
							}
							
							//dw.serviceID   		= service.id;
							Dout.add(dw);
							dw = new DisplayWrapper();
							
					   } else {
					   	 Dout.add(dw);	
					   	 dw = new DisplayWrapper();
					   	 	for (RateCardClass__c RateCardClass: RtcRccMAP.get(RateCard.id)) {
								
								//dw.serviceID   		= service.id;
								dw.ratecardID  		= RateCard.id;						
								dw.ratecardclassID 	= RateCardClass.id;
								
								dw.name  = RateCardClass.Name;
								dw.price = string.valueOf(RateCardClass.UnitPrice__c);
								Dout.add(dw);	
								dw = new DisplayWrapper();
							}
							
						}
				}
					}
				if (dw.name == Service.name) {Dout.add(dw);	
								dw = new DisplayWrapper();}
			}
		}
	}

/* */

 	public PageReference dummy(){return null;}
 	
 	
 	public string serviceID{get;set;}
 	public PageReference EditPage(){
 		serviceID = System.currentPageReference().getParameters().get('serviceID');
 		string params = '?serviceID='+serviceID+'&businessunitID='+BusinessUnit;
		return new PageReference(System.Page.BDT_NewEditRateCard.getUrl()+params);
	}

/* Wrapper classes */

	class ServiceWrapper {
		public Service__c 				Service		{get;set;}
		public List<RateCardWrapper>    RCW			{get;set;}
		public ServiceWrapper() {
			if (Service==null) 		{Service 	= new Service__c();}
			if (RCW==null)			{RCW 		= new List<RateCardWrapper>();}
		}
	}
	class RateCardWrapper {
		public RateCard__c 	 			RateCard	  {get;set;}
		public List<RateCardClass__c>	RateCardClass {get;set;}
		public RateCardWrapper() {
			if (RateCard==null){RateCard=new Ratecard__c();}
			if (RateCardClass==null){RateCardClass= new List<RateCardClass__c>();}
		}
				
		public boolean SingleRateCardClass () {
			return (RateCardClass.size() == 1);
		}
	}
    class DisplayWrapper {
    	public string Name {get;set;}
    	public string Price {get;set;}
    	
    	public string serviceID {get;set;}
    	public string ratecardID {get;set;}
    	public string ratecardclassID {get;set;}
    	
    	public boolean isFirstLine {
    		get {return (serviceID != null);}
    	}
    }





	
	public list<ServiceCategory_w> FullBusinessUnitReport {get;set;}
	
	public void buildFullBusinessUnitReport() {
		
		
		
		// create a map on service for ratecard
		Map<string,sObject> RcOnS = COM_SObjectUtils.mapSObjectOnKey(
			COM_SObjectUtils.getAllFieldsByDynamicSOQL (
				'RateCard__c', 
				'Business_Unit__c = \''+ BusinessUnit +'\'', 
				10000),
			new List<String>{'Service__c'}
		);
		
		Map<string,list<sObject>> RccListOnRc = COM_SObjectUtils.mapSObjectListOnKey(
			COM_SObjectUtils.getAllFieldsByDynamicSOQL (
				'RateCardClass__c', 
				'RateCard__r.Business_Unit__c = \''+ BusinessUnit +'\' order by MaximumUnits__c asc, unitprice__c asc', 
				10000),
			new List<String>{'RateCard__c'}
		);
		
		// create a map on service ratecard for service
		Map<string,list<sObject>> SListOnSc = COM_SObjectUtils.mapSObjectListOnKey(
			COM_SObjectUtils.getAllFieldsByDynamicSOQL (
				'Service__c', 
				'BDTDeleted__c = false and id in (select service__c from ratecard__c where bdtdeleted__c = false and Business_Unit__c = \''+BusinessUnit+'\') order by SequenceNumber__c',
				10000),
			new List<String>{'ServiceCategory__c'}
		);
		
		
		FullBusinessUnitReport = new List<ServiceCategory_w>();
		
		For (ServiceCategory__c sc: [select id, name, code__c 
				from ServiceCategory__c 
				where bdtdeleted__c = false
				order by code__c]) {
			
			FullBusinessUnitReport.add(new ServiceCategory_w(sc,SListOnSc,RcOnS,RccListOnRc));
		}
	}

	public class ServiceCategory_w {
		public ServiceCategory__c ServiceCategory {get;set;}
		public list<Service_w> ServiceList {get;set;}
		
		public ServiceCategory_w(
				ServiceCategory__c ServiceCategoryParam
				,Map<string,list<sObject>> SListOnSc
				,Map<string,sObject> RcOnS
				,Map<string,list<sObject>> RccListOnRc
		) {
			ServiceCategory = ServiceCategoryParam;
			ServiceList = new list<Service_w>();

			if (SListOnSc.containsKey(ServiceCategory.id)) {
				List<SObject> serList = SListOnSc.get(ServiceCategory.id);
				For (SObject ser:serList){
					ServiceList.add(
						new Service_w(
							(Service__c)ser,
							RcOnS,
							RccListOnRc
						)
					);
				}
			}
		}
		
		public string getCode () {
			return bdt_utils.removeLeadingZerosFromCat(ServiceCategory.code__c);
		}
		
	}

	public class Service_w {
		public Service__c Service {get;set;}
		public RateCard_w RateCard {get;set;}
		
		public Service_w(
				Service__c ServiceParam
				,Map<string,sObject> RcOnS
				,Map<string,list<sObject>> RccListOnRc
		) {
			Service = ServiceParam;
			RateCard = new RateCard_w(Service.Id,RcOnS,RccListOnRc);
		}
	}

	public class RateCard_w {
		public RateCard__c RateCard {get;set;}
		public list<RateCardClass__c> RateCardClassList {get;set;}
		
		public RateCard_w(
				String ServiceId
				,Map<string,sObject> RcOnS
				,Map<string,list<sObject>> RccListOnRc
		) {
			// read ratecard
			RateCard = new RateCard__c();
			if (RcOnS.containsKey(ServiceId)) {
				RateCard = (RateCard__c)RcOnS.get(ServiceId);
			}
			// read ratecardclass if ratecard was known
			RateCardClassList = new list<RateCardClass__c>();
			if (RcOnS.containsKey(ServiceId) && RccListOnRc.containsKey(RateCard.id)) {
				RateCardClassList = (list<RateCardClass__c>)RccListOnRc.get(RateCard.id);
			}
		}

	}


}