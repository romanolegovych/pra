@istest
private class CCControllerTest
{
	private static Map<String, SObject> setup()
	{
		Map<String, SObject> result = CCApiTest.setup();
		result.put('Flow Step Action', PAWS_ApexTestsEnvironment.FlowStepAction);
		
		PAWS_ApexTestsEnvironment.FlowStepAction.STSWR1__Type__c = 'Start Sub Flow';
		
		update PAWS_ApexTestsEnvironment.FlowStepAction;
		
		return result;
	}
	
	@istest private static void testController()
	{
		Map<String, SObject> sobjectmap = setup();
		CCController testee = new CCController(new ApexPages.StandardController(sobjectMap.get('Critical Chain')));
		
		
		System.debug(testee);
		
		System.assert(testee.chainSteps != null);
		System.assert(testee.flowOptions != null);
		System.assert(testee.stepOptions != null);
		
		testee.newJunction.Flow_Step__c = sobjectMap.get('Flow Step').Id;
		testee.addStep();
		CCController.selectDescendants(sobjectMap.get('Flow Step').Id);
		testee.deleteStep();
	}
}