global with sharing class BDT_ProjectVersioningController {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} //Project Version
	public string projectId {get;set;}
	public list<Attachment> attachmentList {get;set;}
	public boolean createVersion {get;set;}
	
	public BDT_ProjectVersioningController(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Project Version');
		createVersion = false;
		projectId = BDT_Utils.getPreference('SelectedProject');
		attachmentList = [select id, CreatedDate, Description from Attachment where ParentId = :projectId order by CreatedDate];
	}
	
	public void enableCreateVersion(){
		createVersion = true;
	}
	public void disableCreateVersion(){
		createVersion = false;
	}
	
	public boolean getUserIsAdministrator() {
		return BDT_Utils.getUserIsAdministrator();
	}
	
	public void deleteAttachment(){
		String attachmentId =  System.CurrentPageReference().getParameters().get('attachmentId');
		delete [select id from Attachment where id = :attachmentId];
		attachmentList = [select id, CreatedDate, Description from Attachment where ParentId = :projectId order by CreatedDate];
	}
	
	private static String runDynamicSOQL (String objectName, String whereClause){
		
		String soql = 'select ';
		
		// get all the attributes of the object
		Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap();
		for(String s : objectFields.keySet()) {
			try {
				soql += s + ',';
			} catch (Exception e ) {}
		}
		
		// remove tailing comma
		soql = soql.removeEnd(',');
		soql += ' from ' + objectName;
		
		// include where clause
		if (String.isNotBlank(whereClause)) {
			soql += ' where ' + whereClause.removeStart('where');
		}
		
		try {
			List<sObject> result = Database.query(soql);
			return JSON.serialize(result);
		} catch (Exception e) {
			return 'Failed on objectName: ' + objectName + ' \n whereClause: ' + whereClause + ' \n soql: ' + soql;
		}
		
		return null;
	}
	
	private static String mergeJSON (String a, String b) {
		if ((String.isEmpty(a))&&(String.isEmpty(b))) {return null;}
		if (String.isEmpty(a)) {return b;}
		if (String.isEmpty(b)) {return a;}
		try{
			String result = a.removeEnd(']') + ',' + b.removeStart('[');
			result = result.replace(',]',']');
			result = result.replace('[,','[');
			return result;
		} catch (Exception e) {
			return a+','+b;
		}
	}
	
	@RemoteAction
	global static String getProject (String projectId){
		String result = runDynamicSOQL('Client_Project__c','id = \''+projectId+'\'');
		result = mergeJSON(result,runDynamicSOQL('Study__c','Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('Account','id in (select client__c from client_project__c where id = \''+projectId+'\')'));
		return result;		
	}
	
	@RemoteAction
	global static string getBusinessUnits(String projectId){
		
		String complexWhere = 'id in (select business_unit__c from study__c where project__c = \''+projectId+'\')';
		String result = runDynamicSOQL('Business_Unit__c',complexWhere);
		
		complexWhere = 'id in (select business_unit__c from site__c where id in (\'\',';
		for (ProjectSite__c a : [select Site__c 
				from ProjectSite__c
				where Client_Project__c = :projectId]) {
				complexWhere += '\'' + a.Site__c + '\',';
		}
		complexWhere = complexWhere.removeEnd(',') + '))';
		complexWhere = ' and id not in (select business_unit__c from study__c where project__c = \''+projectId+'\')';
		
		result = mergeJSON(result,runDynamicSOQL('Business_Unit__c',complexWhere));
		return result;
	}
	
	@RemoteAction
	global static String getDesign (String projectId) {
		String result = runDynamicSOQL('ClinicalDesign__c','Project__c = \''+projectId+'\'');
		
		String complexWhere = 'flowchartassignment__c in ( ';
		complexWhere += 'select id ';
		complexWhere += 'from flowchartassignment__c ';
		complexWhere += 'where Arm__r.ClinicalDesign__r.Project__c = \''+projectId+'\')';
		result = mergeJSON(result,runDynamicSOQL('FlowchartServiceTotal__c',complexWhere));
		
		result = mergeJSON(result,runDynamicSOQL('ProjectServiceTimePoint__c','ProjectService__r.Client_Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('Flowchartassignment__c','Arm__r.ClinicalDesign__r.Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('TimePoint__c','Flowchart__r.ClinicalDesign__r.Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('Arm__c','ClinicalDesign__r.Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('EPOCH__c','ClinicalDesign__r.Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('Flowcharts__c','ClinicalDesign__r.Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('ClinicalDesignStudyAssignment__c','ClinicalDesign__r.Project__c = \''+projectId+'\''));
		
		return result;
	}
	
	@RemoteAction
	global static String getPopulation (String projectId) {
		String result = runDynamicSOQL('Population__c','Client_Project__c = \''+projectId+'\'');
		result = mergeJSON(result,runDynamicSOQL('Criteria__c','Populations__r.Client_Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('PopulationStudyAssignment__c','Population__r.Client_Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('StudyDesignPopulationAssignment__c','PopulationStudyAssignment__r.Population__r.Client_Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('GroupArmAssignment__c','Arm__r.ClinicalDesign__r.Project__c = \''+projectId+'\''));
		return result;
	}
	
	@RemoteAction
	global static String getService (String projectId) {
		
		String complexWhere = '';
		complexWhere += 'id in (select service__c ';
		complexWhere +=          'from ProjectService__c ';
		complexWhere +=          'where Client_Project__c = \''+projectId+'\')';
		String result = runDynamicSOQL('Service__c',complexWhere);

		// read all categories
		result = mergeJSON(result,runDynamicSOQL('ServiceCategory__c',''));
		return result;
	}
	
	@RemoteAction
	global static String getProjectService (String projectId) {
		String result = runDynamicSOQL('ProjectService__c','Client_Project__c = \''+projectId+'\'');
		result = mergeJSON(result,runDynamicSOQL('StudyService__c','ProjectService__r.Client_Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('StudyServicePricing__c','StudyService__r.ProjectService__r.Client_Project__c = \''+projectId+'\''));
		return result;
	}
	
	@RemoteAction
	global static String getRateCards (String projectId) {
		
		String ratecardclassWhere = 'id in (\'\',';
		Boolean ratecardclassAvailable = false;
		
		Set<ID> ratecardSet = new Set<ID>();
		String ratecardWhere = 'id in (\'\',';
		
		Set<ID> businessunitSet = new Set<ID>();
		String businessunitWhere = 'id in (\'\',';
		
		for (ratecardclass__c rcc:[select id
									,      ratecard__r.id
									,      ratecard__r.business_unit__c
									from   ratecardclass__c
									where  id in (select ratecardclass__c 
										 from studyservicepricing__c 
										 where StudyService__r.ProjectService__r.Client_Project__c = :projectId)]) {
			ratecardclassAvailable = true;
			ratecardclassWhere += '\'' + rcc.id + '\',';
			// add ratecard id's to where clause only once 
			if (!ratecardSet.contains(rcc.ratecard__r.id)) {
				ratecardSet.add(rcc.ratecard__r.id);
				ratecardWhere += '\'' + rcc.ratecard__r.id + '\',';
			}
			// add businessunit id's to where clause only once 
			if (!businessunitSet.contains(rcc.ratecard__r.business_unit__c)) {
				businessunitSet.add(rcc.ratecard__r.business_unit__c);
				businessunitWhere += '\'' + rcc.ratecard__r.business_unit__c + '\',';
			}
		}
	
		for (Study__c s:[select business_unit__c from study__c where Project__c = :projectId ]){
			// add businessunit id's to where clause only once 
			if (!businessunitSet.contains(s.business_unit__c) && s.business_unit__c != null) {
				businessunitSet.add(s.business_unit__c);
				businessunitWhere += '\'' + s.business_unit__c + '\',';
			}
		}
		
		ratecardclassWhere = ratecardclassWhere.removeEnd(',') + ')';
		ratecardWhere = ratecardWhere.removeEnd(',') + ')';
		businessunitWhere = businessunitWhere.removeEnd(',') + ')';
		
		String result = '';
		if (ratecardclassAvailable) {
			result = runDynamicSOQL('Ratecardclass__c',ratecardclassWhere);
			if (ratecardSet.size()>0) {
				result = mergeJSON(result,runDynamicSOQL('Ratecard__c',ratecardWhere));
			}
			if (businessunitSet.size()>0) {
				result = mergeJSON(result,runDynamicSOQL('Business_Unit__c',businessunitWhere));
			}
		}
		
		return result;
	}
	
	@RemoteAction
	global static String getFinancialDocuments(String projectId) {
		String result = runDynamicSOQL('FinancialDocument__c','Client_Project__c = \''+projectId+'\'');
		result = mergeJSON(result,runDynamicSOQL('FinancialDocumentPaymentTerm__c','FinancialDocument__r.Client_Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('FinancialDocumentHistory__c','FinancialDocument__r.Client_Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('FinancialDocumentExchangeRates__c','FinancialDocument__r.Client_Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('FinancialDocumentContent__c','FinancialDocument__r.Client_Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('FinancialDocumentApproval__c','FinancialDocument__r.Client_Project__c = \''+projectId+'\''));
		return result;
	}
	
	@RemoteAction
	global static String getFinancialDocuments2(String projectId) {
		String result = runDynamicSOQL('FinancialCategoryTotal__c','Study__r.Project__c = \''+projectId+'\'');
		result = mergeJSON(result,runDynamicSOQL('FinancialCatTotPrice__c','FinancialCategoryTotal__r.Study__r.Project__c = \''+projectId+'\''));
		
		String complexWhere = '';
		complexWhere += 'id in (select FinancialDocumentApprRule__c ';
		complexWhere +=          'from FinancialDocumentApproval__c ';
		complexWhere +=          'where FinancialDocument__r.Client_Project__c = \''+projectId+'\')';
		result = mergeJSON(result,runDynamicSOQL('FinancialDocumentApprRule__c',complexWhere));
		
		return result;
	}
	
	@RemoteAction
	global static String getProjectDetails(String projectId) {
		String result = runDynamicSOQL('ProjectSite__c','Client_Project__c = \''+projectId+'\'');
		result = mergeJSON(result,runDynamicSOQL('StudySiteAssignment__c','ProjectSite__r.Client_Project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('StudyMileStone__c','Study__r.Project__c = \''+projectId+'\''));
		
		String complexWhere = '';
		complexWhere += 'id in (select MileStoneDefinition__c ';
		complexWhere +=          'from StudyMileStone__c ';
		complexWhere +=          'where Study__r.Project__c = \''+projectId+'\')';
		result = mergeJSON(result,runDynamicSOQL('MileStoneDefinition__c',complexWhere));
		
		result = mergeJSON(result,runDynamicSOQL('Site__c','id in (select Site__c from ProjectSite__c where Client_Project__c = \''+projectId+'\')'));
		
		return result;
	}
	
	@RemoteAction
	global static String getUsers() {
		List<User> users = [SELECT Id, Name FROM User];
		return JSON.serialize(users);
	}
	
	@RemoteAction
	global static String getPassThrough(String projectId) {
		String complexWhere = ' id in (select PassThroughService__c ';
		complexWhere += 'from ProjectPassThroughStudyAssignment__c ';
		complexWhere += 'where study__r.project__c = \''+projectId+'\')';
		String result = runDynamicSOQL('PassThroughService__c',complexWhere);
		
		result = mergeJSON(result,runDynamicSOQL('ProjectPassThroughStudyAssignment__c','study__r.project__c = \''+projectId+'\''));
		return result;
	}
	
	@RemoteAction
	global static String getSubcontractor(String projectId) {
		
		String result = runDynamicSOQL('ProjectSubcontractorActivity__c','clientproject__c = \''+projectId+'\'');
		result = mergeJSON(result,runDynamicSOQL('SubcontractorActivityStudyAssignment__c'
				,'ProjectSubcontractorActivity__r.clientproject__c = \''+projectId+'\''));
		
		String complexWhere = ' id in (select subcontractoractivity__c ';
		complexWhere += 'from ProjectSubcontractorActivity__c ';
		complexWhere += 'where clientproject__c = \''+projectId+'\')';
		result = mergeJSON(result,runDynamicSOQL('SubcontractorActivity__c',complexWhere));
		
		complexWhere = '';
		complexWhere += 'id in (select subcontractor__c from SubcontractorActivity__c where id in (\'\',';
		for (ProjectSubcontractorActivity__c a : [select subcontractoractivity__c 
				from ProjectSubcontractorActivity__c
				where ClientProject__c = :projectId]) {
				complexWhere += '\'' + a.subcontractoractivity__c + '\',';
		}
		complexWhere = complexWhere.removeEnd(',') + '))';
		result = mergeJSON(result,runDynamicSOQL('Subcontractor__c',complexWhere));
		
		return result;
	}
	
	@RemoteAction
	global static String getLaboratoryObjects(String projectId) {
		String result = runDynamicSOQL('LaboratorySamples__c','project__c = \''+projectId+'\'');
		result = mergeJSON(result,runDynamicSOQL('LaboratoryAnalysisSlots__c','study__r.project__c = \''+projectId+'\''));
		result = mergeJSON(result,runDynamicSOQL('LaboratoryAnalysis__c','study__r.project__c = \''+projectId+'\''));
		
		String complexWhere = '(\'\',';
		for (LaboratoryAnalysis__c a : [select laboratorymethodcompound__c 
				from LaboratoryAnalysis__c
				where study__r.project__c = :projectId]) {
				complexWhere += '\'' + a.laboratorymethodcompound__c + '\',';
		}
		complexWhere = complexWhere.removeEnd(',') + ')';
		result = mergeJSON(result,runDynamicSOQL('LaboratoryMethodCompound__c','laboratorymethod__c in '+complexWhere));
		result = mergeJSON(result,runDynamicSOQL('LaboratoryMethodComedication__c','laboratorymethod__c in '+complexWhere));
		result = mergeJSON(result,runDynamicSOQL('LaboratoryMethod__c','id in '+complexWhere));
		
		return result;
	}
	
	@RemoteAction
	public static String doUploadAttachment( String attachmentBody, String attachmentName, String projectId, String description) {
		Attachment att = new Attachment();
		att.Body = EncodingUtil.base64Decode(attachmentBody);
		att.Name = attachmentName;
		att.parentId = projectId;
		att.Description = description;
		upsert att;
		return att.id;
	}

	@RemoteAction
	public static String doDownloadAttachment(String attachmentId) {
		try{
			Attachment att = [select Body from Attachment where id = :attachmentId];
			return EncodingUtil.base64Encode(att.Body);
		} catch (Exception e) {return string.valueOf(e);}
		return null;
	}
	
}