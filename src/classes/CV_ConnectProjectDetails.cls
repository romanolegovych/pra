@RestResource(urlMapping='/CV_ConnectProjectDetails/*')
global with sharing class CV_ConnectProjectDetails{
	
    @HttpGet
    global static ProjectsDetailsResult getProjectsDetails() {
        try{
            return new ProjectsDetailsResult( PAWS_API.PBB_getActiveProjectList( null ) );
        }catch(Exception e){
            return new ProjectsDetailsResult( e.getMessage() );
        }
    }
        
    @HttpPost
    global static ProjectDetailResult getProjectDetail(Id projectId) {
        try{
            return new ProjectDetailResult( PAWS_API.PBB_getProjectDetails( projectId ) );
        }catch(Exception e){
            return new ProjectDetailResult( e.getMessage() );
        }
    }
    
	global class ProjectsDetailsResult{
	    
	        global String errorMessage { get; private set; }
	        
	        global List<PAWS_ProjectDetailCVO> returnedValues { get; private set; }
	        
	        public ProjectsDetailsResult(List<PAWS_ProjectDetailCVO> returnedValues ){
	            this.returnedValues = returnedValues ;
	        }
	        
	        public ProjectsDetailsResult(String errorMessage){
	            this.errorMessage = errorMessage;
	        }
	}

	global class ProjectDetailResult{
	    
	        global String errorMessage { get; private set; }
	        
	        global PAWS_ProjectDetailCVO returnedValues { get; private set; }
	        
	        public ProjectDetailResult(PAWS_ProjectDetailCVO returnedValues ){
	            this.returnedValues = returnedValues ;
	        }
	        
	        public ProjectDetailResult(String errorMessage){
	            this.errorMessage = errorMessage;
	        }
	}
	
}