@isTest
private class PAWS_PBB_HelperTest
{
	@isTest
	static void itShouldCreateInstance()
	{
		PAWS_ApexTestsEnvironment.init();

		ecrf__c project = PAWS_ApexTestsEnvironment.Project;
		PAWS_Project_Flow_Country__c projectCountry = PAWS_ApexTestsEnvironment.ProjectCountry;
		PAWS_Project_Flow_Site__c projectSite = PAWS_ApexTestsEnvironment.ProjectSite;
		PAWS_Project_Flow_Agreement__c projectAgreement = PAWS_ApexTestsEnvironment.ProjectAgreement;

		STSWR1__Flow__c projectFlow = PAWS_ApexTestsEnvironment.Flow;
		projectFlow.STSWR1__Source_Id__c = project.ID;
		update projectFlow;

		STSWR1__Flow__c countryFlow = new STSWR1__Flow__c(Name='Country Flow', STSWR1__Type__c='Triggered by another flow', STSWR1__Object_Type__c='paws_project_flow_country__c', STSWR1__Status__c = 'Active', STSWR1__Source_Id__c=projectCountry.ID);
		STSWR1__Flow__c siteFlow = new STSWR1__Flow__c(Name='Site Flow', STSWR1__Type__c='Triggered by another flow', STSWR1__Object_Type__c='paws_project_flow_site__c', STSWR1__Status__c = 'Active', STSWR1__Source_Id__c=projectSite.ID);
		STSWR1__Flow__c agreementFlow = new STSWR1__Flow__c(Name='Agreement Flow', STSWR1__Type__c='Triggered by another flow', STSWR1__Object_Type__c='paws_project_flow_agreement__c', STSWR1__Status__c = 'Active', STSWR1__Source_Id__c=projectAgreement.ID);
		List<STSWR1__Flow__c> flows = new List<STSWR1__Flow__c> { countryFlow, siteFlow, agreementFlow };
		insert flows;

		STSWR1__Flow_Step_Junction__c projectStep = new STSWR1__Flow_Step_Junction__c(Name='Project Step', STSWR1__Flow__c=projectFlow.Id, STSWR1__Is_First_Step__c=true);
		STSWR1__Flow_Step_Junction__c countryStep = new STSWR1__Flow_Step_Junction__c(Name='Country Step', STSWR1__Flow__c=countryFlow.Id, STSWR1__Is_First_Step__c=true);
		STSWR1__Flow_Step_Junction__c siteStep = new STSWR1__Flow_Step_Junction__c(Name='Site Step', STSWR1__Flow__c=siteFlow.Id, STSWR1__Is_First_Step__c=true);
		STSWR1__Flow_Step_Junction__c agreementStep = new STSWR1__Flow_Step_Junction__c(Name='Agreement Step', STSWR1__Flow__c=agreementFlow.Id, STSWR1__Is_First_Step__c=true);
		insert new List<STSWR1__Flow_Step_Junction__c> { projectStep, countryStep, siteStep, agreementStep };

		Datetime startDate = Datetime.now();
		Datetime endDate = Datetime.now().addDays(2);
		STSWR1__Gantt_Step_Property__c projectGanttProperty = new STSWR1__Gantt_Step_Property__c(STSWR1__Step__c=projectStep.ID,STSWR1__Level__c='First',STSWR1__Planned_End_Date__c=endDate, STSWR1__Planned_Start_Date__c=startDate, STSWR1__Is_Skipped__c=false);
		STSWR1__Gantt_Step_Property__c countryGanttProperty = new STSWR1__Gantt_Step_Property__c(STSWR1__Step__c=countryStep.ID,STSWR1__Level__c='First',STSWR1__Planned_End_Date__c=endDate, STSWR1__Planned_Start_Date__c=startDate, STSWR1__Is_Skipped__c=false);
		STSWR1__Gantt_Step_Property__c siteGanttProperty = new STSWR1__Gantt_Step_Property__c(STSWR1__Step__c=siteStep.ID,STSWR1__Level__c='First',STSWR1__Planned_End_Date__c=endDate, STSWR1__Planned_Start_Date__c=startDate, STSWR1__Is_Skipped__c=true);
		STSWR1__Gantt_Step_Property__c agreementGanttProperty = new STSWR1__Gantt_Step_Property__c(STSWR1__Step__c=agreementStep.ID,STSWR1__Level__c='First',STSWR1__Planned_End_Date__c=endDate, STSWR1__Planned_Start_Date__c=startDate, STSWR1__Is_Skipped__c=false);
		insert new List<STSWR1__Gantt_Step_Property__c> { projectGanttProperty, countryGanttProperty, siteGanttProperty, agreementGanttProperty };

		String rolesAndEffortsJson = '{"Efforts":[{"RoleCode":"P52","Effort":1}],"Description":"1 Role"}';
		STSWR1__Flow_Step_Property__c projectStepProperty = new STSWR1__Flow_Step_Property__c(Name = 'Roles & Efforts', STSWR1__Type__c = 'Roles & Efforts', STSWR1__Value__c = rolesAndEffortsJson, STSWR1__Flow_Step__c = projectStep.ID);
		STSWR1__Flow_Step_Property__c countryStepProperty = new STSWR1__Flow_Step_Property__c(Name = 'Roles & Efforts', STSWR1__Type__c = 'Roles & Efforts', STSWR1__Value__c = rolesAndEffortsJson, STSWR1__Flow_Step__c = countryStep.ID);
		STSWR1__Flow_Step_Property__c siteStepProperty = new STSWR1__Flow_Step_Property__c(Name = 'Roles & Efforts', STSWR1__Type__c = 'Roles & Efforts', STSWR1__Value__c = rolesAndEffortsJson, STSWR1__Flow_Step__c = siteStep.ID);
		STSWR1__Flow_Step_Property__c agreementStepProperty = new STSWR1__Flow_Step_Property__c(Name = 'Roles & Efforts', STSWR1__Type__c = 'Roles & Efforts', STSWR1__Value__c = rolesAndEffortsJson, STSWR1__Flow_Step__c = agreementStep.ID);
		insert new List<STSWR1__Flow_Step_Property__c> { projectStepProperty, countryStepProperty, siteStepProperty, agreementStepProperty };

		Map<String, Object> config = new Map<String, Object> { 'flowId' => countryFlow.ID};
		STSWR1__Flow_Step_Action__c projectStepAction = new STSWR1__Flow_Step_Action__c(STSWR1__Config__c = JSON.serialize(config), STSWR1__Flow_Step__c = projectStep.ID, STSWR1__Type__c = 'Start Sub Flow');

		config = new Map<String, Object> { 'flowId' => siteFlow.ID};
		STSWR1__Flow_Step_Action__c countryAction = new STSWR1__Flow_Step_Action__c(STSWR1__Config__c = JSON.serialize(config), STSWR1__Flow_Step__c = countryStep.ID, STSWR1__Type__c = 'Start Sub Flow');

		config = new Map<String, Object> { 'flowId' => agreementFlow.ID};
		STSWR1__Flow_Step_Action__c siteAgreementAction = new STSWR1__Flow_Step_Action__c(STSWR1__Config__c = JSON.serialize(config), STSWR1__Flow_Step__c = siteStep.ID, STSWR1__Type__c = 'Start Sub Flow');

		insert new List<STSWR1__Flow_Step_Action__c> { projectStepAction, countryAction, siteAgreementAction };

		STSWR1__Flow_Instance__c projectInstance = new STSWR1__Flow_Instance__c(STSWR1__Object_Id__c = project.ID, STSWR1__Object_Name__c = project.Name, STSWR1__Flow__c = projectFlow.ID, STSWR1__Is_Active__c = true);
		insert projectInstance;

		STSWR1__Flow_Instance_Cursor__c projectCursor = new STSWR1__Flow_Instance_Cursor__c(STSWR1__Flow_Instance__c=projectInstance.Id, STSWR1__Step__c=projectStep.Id, STSWR1__Step_Assigned_To__c=UserInfo.getUserId(), STSWR1__Status__c='In Progress');
		insert projectCursor;

		Test.startTest();

		List<PAWS_PBB_Helper.WorkItem> items = PAWS_PBB_Helper.getWorkBreakdown(project.Id);
		List<PAWS_PBB_Helper.WrModelStepWrapper> steps = PAWS_PBB_Helper.getWRModelSteps(projectFlow.Id);
		System.assert(items.isEmpty() == false);
	}
}