public with sharing class PRA_ChangeOrderEndDateController {

    // Page variables
    public String ChangeorderNo	{get;set;}
    public String selectedProject {get;set;}
    public String selectedProjectid {get;set;}
    public Date EstimatedProjectEndDate {get;set;}
    public String ProposedProjectEndDate {get;set;}
    public Date NEWProposedProjectEndDate {get;set;}   
    public String id;
    public Id COid {get;set;}
    public List<GridDataTable1> LiWrappers2 {get;set;}
    public List<Change_Order_Item__c> LiWrappers1 {get;set;}   
    public String dd {get;set;}
    public String selectedRecord {get;set;} 
   
    //Constructor    
    public PRA_ChangeOrderEndDateController() {
        this.id = ApexPages.currentPage().getParameters().get('id');
        COid = (id);
        Change_Order__c CO = [select Id, Name, Client_Project__r.Estimated_End_Date__c, Proposed_Estimated_End_Date__c,
        						Client_Project__c, Client_Project__r.Name from Change_Order__c where Id = :COid limit 1];
        if(CO.id != null) {
            EstimatedProjectEndDate = CO.Client_Project__r.Estimated_End_Date__c;
            Datetime d = CO.Proposed_Estimated_End_Date__c;
            //converting date to string          
            if(CO.Proposed_Estimated_End_Date__c != null) {
                ProposedProjectEndDate = String.valueof(CO.Proposed_Estimated_End_Date__c);
                List<String> p = ProposedProjectEndDate.split('-');
                String s = String.valueof(Integer.valueOf(p[1]) + '/' + Integer.valueOf(p[2]) + '/' + Integer.valueOf(p[0]));
                ProposedProjectEndDate = s;
            }
            selectedProject = CO.Client_Project__r.Name;
            selectedProjectid = CO.Client_Project__c;
            NEWProposedProjectEndDate = CO.Proposed_Estimated_End_Date__c;
            ChangeorderNo = CO.Name;
            searchbypropose();
        }
    }  
    
    //Search by New proposed date
    public void searchbypropose() {
        List<Client_Task__c> LiWrappers1 = ([select Id, Description__c, Original_End_Date__c, Client_Unit_Number__c, 
        									Project_Region__r.Name, Revised_End_Date__c from Client_Task__c
	                                        where Project__r.Name = :selectedProject  
	                                        order by Client_Unit_Number__c DESC]);
           
        LiWrappers2 = new List<GridDataTable1>();
        for(Client_Task__c a : LiWrappers1) {
            if(a.Revised_End_Date__c != null && a.Revised_End_Date__c > NEWProposedProjectEndDate) {
                GridDataTable1 obj = new GridDataTable1();
                if(String.valueof(a.id) != null) {
                	obj.unitNumber = String.valueof(a.Client_Unit_Number__c);
                    obj.description = String.valueof(a.Description__c);
                    obj.clientRegion = String.valueof(a.Project_Region__r.Name);
                    obj.endDate = (a.Revised_End_Date__c);
                    LiWrappers2.add(obj);
                }
            } else {              
                GridDataTable1 obj = new GridDataTable1();
                if(String.valueof(a.id) != null  && a.Original_End_Date__c > NEWProposedProjectEndDate ) {
                     obj.unitNumber = String.valueof(a.Client_Unit_Number__c);
                     obj.description = String.valueof(a.Description__c);
                     obj.clientRegion = String.valueof(a.Project_Region__r.Name);
                     obj.endDate = (a.Original_End_Date__c);
                     LiWrappers2.add(obj);
                }
            }            
        } 
        
        for(Change_Order_Item__c coit : [select Id, Name, Client_Task__c, Description__c, Client_Unit_Number__c, End_Date__c, Project_Region__r.Name, Type__c                                            
                                       	from Change_Order_Item__c
                                       	where Change_Order__c = :COid and Type__c = 'New']) {
        	if(coit.End_Date__c>NEWProposedProjectEndDate) {
                GridDataTable1 obj = new GridDataTable1();
                if(String.valueof(coit.Id) != null) {
                	obj.unitNumber = String.valueof(coit.Client_Unit_Number__c);
                    obj.description = String.valueof(coit.Description__c);
                    obj.clientRegion = String.valueof(coit.Project_Region__r.Name);
                    obj.endDate = (coit.End_Date__c);
                    system.debug('-------' + obj.endDate);
                    LiWrappers2.add(obj);
        		}
        	}
        	system.debug('-------' + LiWrappers2);
       	}
    }
    
    //get the date format that fits Jquery datepicker
    public static Date GetDateFromString(String strDate, String format) {
        if(format == 'mm/dd/yyyy') {
            List<String> datePart = strDate.split('/');
            return date.newinstance(Integer.valueOf(datePart[2]), Integer.valueOf(datePart[0]), Integer.valueOf(datePart[1])); 
        }
        return null;
    }
    
    //Save New Proposed date 
    public void save() {
        List<Change_Order__c> CO = [select Id, Name, Client_Project__r.Estimated_End_Date__c, Proposed_Estimated_End_Date__c 
									from Change_Order__c where Id = :COid limit 1];
        for(Change_Order__c c : CO) {
            if(ProposedProjectEndDate != null && ProposedProjectEndDate != '')           
                c.Proposed_Estimated_End_Date__c = GetDateFromString(ProposedProjectEndDate, 'mm/dd/yyyy');
            else
                c.Proposed_Estimated_End_Date__c = null;
                NEWProposedProjectEndDate = c.Proposed_Estimated_End_Date__c;         
        }
        update CO;
        searchbypropose();
    }
    
    //Go to home page on click of project name           
    public PageReference viewRecord() {
        if(selectedRecord != null && selectedRecord != '') {
            PageReference pageRef = new PageReference('/apex/PRA_Change_Order?id=' + selectedRecord);
            pageRef.setRedirect(true);
            return pageRef;
        }
        return null;
    }  
    
    //Wrapper class to hold data   
    public class GridDataTable1 {      
        public String unitNumber {get;set;}
        public String description {get;set;}       
        public String clientRegion {get;set;}
        public Date endDate  {get;set;}      
    }
      
}