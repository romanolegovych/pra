public with sharing class RM_ProjectWorkHistoryController {
	 private final list<WorkHistory> whist;
	 public RM_ProjectWorkHistoryController(){
	 	string projID = ApexPages.currentPage().getParameters().get('id');
	 	whist = getHistoryData(projID);
	 }
	 public class WorkHistory{
    
        transient public string lastName {get; private set;}
        transient public string firstName {get; private set;}
        transient public double Total_work_hr {get; private set;}
        transient public string Start_date {get; private set;}
        transient public string End_date {get; private set;}    
        transient public string Buf_code {get; private set;}
        
        public WorkHistory(WFM_EE_Work_History_Summary__c his)
        {
            lastName = his.Employee_ID__r.Last_Name__c;
            Total_work_hr = his.Sum_Worked_Hours__c;
            Start_date = his.Start_Year_Month__c;
            End_date=his.End_Year_Month__c;
            Buf_code=his.Buf_Code__c;
            firstName=his.Employee_ID__r.first_Name__c;
        }               
    }
    
    Public List<WorkHistory> GetHistory(){
        return whist;
    }
    private list<WorkHistory> getHistoryData(string projID){
        list<WFM_EE_Work_History_Summary__c> HistSummary = RM_EmployeeService.GetWorkHistorybyProjectRID(projID);
        List<WorkHistory> whistory = new List<WorkHistory>();
        for (WFM_EE_Work_History_Summary__c his : HistSummary)        
                whistory.add(new WorkHistory(his));
    
        return whistory;
    }
}