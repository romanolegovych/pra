global class PRA_MonthlyApprovalBatch implements Schedulable {
   
  global void execute(SchedulableContext SC) {
    
    Date startOfMonth = Date.today().toStartOfMonth();    
    system.debug ('startOfMonth  = ' +  startOfMonth ); 
    
    Date monthappliesto;    
    if(date.today()==startOfMonth){
        monthappliesto=Date.today().toStartOfMonth().addMonths(-1);    
    }else{
        monthappliesto=Date.today().toStartOfMonth().addmonths(0);     
    }   
    
    List <Approval_Calendar__c> approvalsCalendar = [select Forecast_Approved_by_DPD__c, Forecast_Approved_by_Project_Manager__c
                                                     from Approval_Calendar__c 
                                                     where Month_Approval_Applies_To__c = :monthappliesto ];
 
    List <id> cpneeddpdapp = new List<id>();
    List <id> cpneedEVPapp = new List<id>();
    List <id> PZnotReleasebyPM = new List<id>();
    List <id> PZnotReleasebyDPD = new List<id>();
   
    List <Monthly_Approval__c> monthApproval = [select Id,Month_Approval_Applies_To__c, Approval_Date__c, Client_Project__c,DPD_Approval_Date__c, DPD_approved_forecast__c,EVP_Approval_Date__c,DOF_Approval_Date__c,EVP_Approved__c,DOF_Approved__c, Is_approval_complete__c, 
                                                       Is_Approved_DPD_forecast__c,Is_Approved_EVP__c,Is_Approved_DOF__c,Is_Approved_PM_Forecast__c,Is_Approved_PM_Worked_Units__c,Is_Approved_PZ_released_forecast__c,
                                                       Need_DOF_EVP_Approval__c,Need_DPD_Approval__c,PM_Forecast_App_Date__c,PM_WU_Approval_Date__c, PZ_Release_Date__c,PM_Approved_Forecast__c, PM_Approved_Worked_Units__c, PZ_released_forecast__c,PZ_Released_Forecast__r.Email
                                                from Monthly_Approval__c 
                                                where Month_Approval_Applies_To__c = :monthappliesto 
                                                and PM_Approved_Worked_Units__c!=null 
                                                and Client_Project__r.Global_Project_Manager_Director__c!=null
                                                and Client_Project__r.Status__c != :PRA_Constants.CP_STATUS_COMPLETE
                                                and Client_Project__r.Status__c != :PRA_Constants.CP_STATUS_ON_HOLD 
                                                and (Client_Project__r.CreatedDate = NULL
                                                or   Client_Project__r.CreatedDate < :monthappliesto)];
      
    system.debug ('monthApproval.size()  = ' +  monthApproval.size());  
    system.debug ('monthApproval ' +  monthApproval); 
    List <Monthly_Approval__c> updatemonthApproval =new List <Monthly_Approval__c>(); 
    List <Monthly_Approval__c> updatemonthApproval1 =new List <Monthly_Approval__c>();   
    
    if (monthApproval.size() > 0 && approvalsCalendar.size()>0 ) {
      for(Monthly_Approval__c montapprvals: monthApproval ){
         system.debug ('----montapprvals----' +montapprvals);            
         if ((Date.today()) == (approvalsCalendar[0].Forecast_Approved_by_Project_Manager__c+1)){ 
            if (montapprvals.Is_approval_complete__c == false && montapprvals.PM_Approved_Forecast__c == null && montapprvals.DPD_approved_forecast__c == null && montapprvals.PM_Approved_Worked_Units__c != null){
               if(montapprvals.PZ_released_forecast__c != null){
                   montapprvals.Need_DPD_Approval__c = True;             
                   system.debug ('montapprvals.Client_Project__c  = ' +  montapprvals.Client_Project__c);  
                   cpneeddpdapp.add(montapprvals.Client_Project__c); 
                   updatemonthApproval.add(montapprvals);
               }else{
                   PZnotReleasebyPM.add(montapprvals.Client_Project__c); 
               }
            }    
        }   
      }
      //Update monthApproval;
    }
    
    if (monthApproval.size() > 0 && approvalsCalendar.size()>0 ) {    
      for(Monthly_Approval__c montapprvals: monthApproval ) {         
        system.debug ('----montapprvals----' +montapprvals); 
        if (Date.today()== (approvalsCalendar[0].Forecast_Approved_by_DPD__c)+1){ 
            if (montapprvals.Is_approval_complete__c == false && montapprvals.DPD_approved_forecast__c == null && montapprvals.PM_Approved_Worked_Units__c != null && montapprvals.EVP_Approved__c == null){
                if(montapprvals.PZ_released_forecast__c != null){
                    montapprvals.Need_DOF_EVP_Approval__c = True;  
                    system.debug ('montapprvals.Client_Project__c  = ' +  montapprvals.Client_Project__c);            
                   cpneedEVPapp.add(montapprvals.Client_Project__c);
                   updatemonthApproval1.add(montapprvals); 
                }else{
                    PZnotReleasebyDPD.add(montapprvals.Client_Project__c); 
                }
            }    
        }   
      }
      //Update monthApproval;
    }
    
    if(updatemonthApproval.size()>0)
    Update updatemonthApproval;
     if(updatemonthApproval1.size()>0)
    Update updatemonthApproval1;
   
    List <Client_Project__c> clprjt;      
    if (cpneeddpdapp != null) { 
        clprjt = [Select Id, Name, Director_of_Project_Delivery__c, Executive_Vice_President__c, General_Partner__c,
                    Global_Project_Analyst__c, Global_Project_Manager_Director__c, Manager_Director_of_Operations_Finance__c,
                    Vice_President_of_Operations_Finance__c from Client_Project__c where id in :cpneeddpdapp];
     }
     
     List <User> userL = [Select Id, email from User];
     Map<Id,String> userMapForDpd = new Map<Id,String>();
     for (User usr: userL){
         userMapForDpd.put(usr.Id, usr.email);
     }     
     String[]receipntTo = new String[1];
     String[] receipntCC = new String[3];
     String[] receipntCC1 = new String[5];
     
     String dpdEmail;
     String gpEmail;
     String pzEmail;
     String pmEmail;
     String evpEmail;
     String dofEmail;
     String mofEmail;
      
     //Preparing Subject
     String Subject;
     String HtmlBody;
     String toMail;
     if (clprjt.size() > 0) {
        List<Messaging.SingleEmailMessage> email = new List<Messaging.SingleEmailMessage>();
        for (Client_Project__c cp : clprjt){
           system.debug ('cp.id = ' +  cp.id);
           dpdEmail = userMapForDpd.get(cp.Director_of_Project_Delivery__c);
           gpEmail = userMapForDpd.get(cp.General_Partner__c);
           pzEmail = userMapForDpd.get(cp.Global_Project_Analyst__c);
           pmEmail = userMapForDpd.get(cp.Global_Project_Manager_Director__c);
           If (dpdEmail != null) {
               receipntTo[0] = dpdEmail;
           }else{
               receipntTo[0] = pmEmail;
           }
           if (pzEmail != null) {
               toMail = pzEmail;
           }else{
               toMail = pmEmail;
           }
           if (gpEmail != null ) {toMail += ':' + gpEmail;} 
           if (pmEmail != null ) {toMail += ':' + pmEmail;}           
           
              receipntCC = toMail.split(':', 0);  
              
              system.debug ('dpdEmail  = ' +  dpdEmail);  
              system.debug ('pzEmail  = ' +  pzEmail);  
              system.debug ('gpEmail= ' +  gpEmail);  
              system.debug ('pmEmail= ' +  pmEmail); 
              //Preparing Subject
              Subject='Project '+ cp.Name+' Requires your Approval';
                               
              //Preparing HTML Body with Link
              HtmlBody='Project <b>'+ cp.Name+'</b> is being escalated for DPD review and approval due to non-approval by the PM <p>'+'  '+'To view the project please login to PM Console. '+'<p>';  
             
              Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
              mail.setToAddresses(receipntTo);                         
              mail.setCcAddresses(receipntCC);              
              mail.setSubject(Subject);
              mail.setHtmlBody(HTMLBody);
              email.add(mail);
                                  
             // PRA_EmailHelper.sendEmail(receipntTo,receipntCC,Subject,HtmlBody);
        system.debug ('PM deadline emails ' + clprjt+email);
        }
        Messaging.sendEmail(email);
     }
     
       
    List <Client_Project__c> clprjt1 = [Select Id, Name, Director_of_Project_Delivery__c, Executive_Vice_President__c, General_Partner__c,
                                               Global_Project_Analyst__c, Global_Project_Manager_Director__c, Manager_Director_of_Operations_Finance__c,
                                               Vice_President_of_Operations_Finance__c, Manager_of_Operations_Finance__c
                                        from Client_Project__c
                                        where id in :cpneedEVPapp];
  
     if (clprjt1.size() > 0){ 
     List<Messaging.SingleEmailMessage> email1 = new List<Messaging.SingleEmailMessage>();
         for (Client_Project__c cp1 : clprjt1){
             system.debug ('cp1.id = ' +  cp1.id);
             dpdEmail = userMapForDpd.get(cp1.Director_of_Project_Delivery__c);
             gpEmail = userMapForDpd.get(cp1.General_Partner__c);
             pzEmail = userMapForDpd.get(cp1.Global_Project_Analyst__c);
             pmEmail = userMapForDpd.get(cp1.Global_Project_Manager_Director__c);
             evpEmail = userMapForDpd.get(cp1.Executive_Vice_President__c);
             dofEmail = userMapForDpd.get(cp1.Manager_Director_of_Operations_Finance__c);
             mofEmail = userMapForDpd.get(cp1.Manager_of_Operations_Finance__c);
             
             if(evpEmail!=null){                    
                receipntTo[0] = evpEmail;
             }else{
                receipntTo[0] = pmEmail; 
             }
     
              String rcptMail;  
              if (dpdEmail != null) {
                  rcptMail = dpdEmail;
              }else{
                  rcptMail = pmEmail; 
             }
              if (pzEmail != null) {rcptMail +=  ':' + pzEmail;}
              if (gpEmail != null) {rcptMail += ':' + gpEmail;} 
              if (pmEmail != null) {rcptMail += ':' + pmEmail;}
              if (dofEmail != null) {rcptMail += ':' + dofEmail;}
              if (mofEmail != null) {rcptMail += ':' + mofEmail;}
              //receipntCC1[1] = pzEmail;
              //receipntCC1[2] = gpEmail;
              //receipntCC1[3] = pmEmail;
              //receipntCC1[4] = dofEmail;
              
              if(rcptMail!=null)                          
                 receipntCC1 = rcptMail.split(':', 0);
    
              //Preparing Subject
              Subject='Project '+ cp1.Name+' Requires your Approval';
                               
              //Preparing HTML Body with Link
              HtmlBody='Project <b>'+ cp1.Name+'</b> is being escalated for EVP review and approval due to non-approval by the DPD <p>'+'  '+'To view the project please login to PM Console. '+'<p>'; 
                                   
              Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage(); 
              mail1.setToAddresses(receipntTo);               
              mail1.setCcAddresses(receipntCC1); 
              mail1.setSubject(Subject);
              mail1.setHtmlBody(HTMLBody);
              email1.add(mail1);              
              
              //PRA_EmailHelper.sendEmail(receipntTo,receipntCC1,Subject,HtmlBody);
               system.debug ('DPD deadline emails ' + clprjt);
         }
         Messaging.sendEmail(email1);
     }
      
      if (PZnotReleasebyPM.size() > 0) {
        List<Messaging.SingleEmailMessage> email2 = new List<Messaging.SingleEmailMessage>();
        for (Client_Project__c cp : [Select Id, Name, Director_of_Project_Delivery__c, Executive_Vice_President__c, General_Partner__c,
                                            Global_Project_Analyst__c, Global_Project_Manager_Director__c, Manager_Director_of_Operations_Finance__c,
                                                Vice_President_of_Operations_Finance__c from Client_Project__c where id in :PZnotReleasebyPM]){
           system.debug ('cp.id = ' +  cp.id);
           dpdEmail = userMapForDpd.get(cp.Director_of_Project_Delivery__c);
           gpEmail = userMapForDpd.get(cp.General_Partner__c);
           pzEmail = userMapForDpd.get(cp.Global_Project_Analyst__c);
           pmEmail = userMapForDpd.get(cp.Global_Project_Manager_Director__c);
           If (dpdEmail != null) {
               receipntTo[0] = dpdEmail;
           }else{
               receipntTo[0] = pmEmail;
           }
           if (pzEmail != null) {
               toMail = pzEmail;
           }else{
               toMail = pmEmail;
           }
           if (gpEmail != null ) {toMail += ':' + gpEmail;} 
           if (pmEmail != null ) {toMail += ':' + pmEmail;}           
           
              receipntCC = toMail.split(':', 0);  
              
              system.debug ('dpdEmail  = ' +  dpdEmail);  
              system.debug ('pzEmail  = ' +  pzEmail);  
              system.debug ('gpEmail= ' +  gpEmail);  
              system.debug ('pmEmail= ' +  pmEmail); 
               //Preparing Subject
              Subject='Project '+ cp.Name+' requires your review';
                               
              //Preparing HTML Body with Link
              HtmlBody='Project <b>'+ cp.Name+'</b> is being escalated for DPD review due to the forecast not being finalized.<p>'+'  '+'Please follow up with the project PM and PZ. '+'<p>'; 
               
              Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage(); 
              mail.setToAddresses(receipntTo);                         
              mail.setCcAddresses(receipntCC);              
              mail.setSubject(Subject);
              mail.setHtmlBody(HTMLBody);
              email2.add(mail);
                                  
             // PRA_EmailHelper.sendEmail(receipntTo,receipntCC,Subject,HtmlBody);
        system.debug ('PZ misses the deadline either PM  deadline emails ' + cp+email2);
        }
        Messaging.sendEmail(email2);
     }
     
      
     if (PZnotReleasebyDPD.size() > 0){ 
     List<Messaging.SingleEmailMessage> email3 = new List<Messaging.SingleEmailMessage>();
         for (Client_Project__c cp1 : [Select Id, Name, Director_of_Project_Delivery__c, Executive_Vice_President__c, General_Partner__c,
                                              Global_Project_Analyst__c, Global_Project_Manager_Director__c, Manager_Director_of_Operations_Finance__c,
                                              Vice_President_of_Operations_Finance__c, Manager_of_Operations_Finance__c
                                       from Client_Project__c 
                                       where id in :PZnotReleasebyDPD]){
             system.debug ('cp1.id = ' +  cp1.id);
             dpdEmail = userMapForDpd.get(cp1.Director_of_Project_Delivery__c);
             gpEmail = userMapForDpd.get(cp1.General_Partner__c);
             pzEmail = userMapForDpd.get(cp1.Global_Project_Analyst__c);
             pmEmail = userMapForDpd.get(cp1.Global_Project_Manager_Director__c);
             evpEmail = userMapForDpd.get(cp1.Executive_Vice_President__c);
             dofEmail = userMapForDpd.get(cp1.Manager_Director_of_Operations_Finance__c);
             mofEmail = userMapForDpd.get(cp1.Manager_of_Operations_Finance__c);
             
             if(evpEmail!=null){                    
                receipntTo[0] = evpEmail;
             }else{
                receipntTo[0] = pmEmail; 
             }
     
              String rcptMail;  
              if (dpdEmail != null) {
                  rcptMail = dpdEmail;
              }else{
                  rcptMail = pmEmail; 
             }
              if (pzEmail != null) {rcptMail +=  ':' + pzEmail;}
              if (gpEmail != null) {rcptMail += ':' + gpEmail;} 
              if (pmEmail != null) {rcptMail += ':' + pmEmail;}
              if (dofEmail != null) {rcptMail += ':' + dofEmail;}
              if (mofEmail != null) {rcptMail += ':' + mofEmail;}
              //receipntCC1[1] = pzEmail;
              //receipntCC1[2] = gpEmail;
              //receipntCC1[3] = pmEmail;
              //receipntCC1[4] = dofEmail;
              
              if(rcptMail!=null)                          
                 receipntCC1 = rcptMail.split(':', 0);
    
              //Preparing Subject
              Subject='Project '+ cp1.Name+' requires your review';
                               
              //Preparing HTML Body with Link
              HtmlBody='Project <b>'+ cp1.Name+'</b> is being escalated for EVP review due to the forecast not being finalized.<p>'+'  '+'Please follow up with the project PM and PZ. '+'<p>'; 
                                   
              Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage(); 
              mail1.setToAddresses(receipntTo);               
              mail1.setCcAddresses(receipntCC1); 
              mail1.setSubject(Subject);
              mail1.setHtmlBody(HTMLBody);
              email3.add(mail1);              
              
              //PRA_EmailHelper.sendEmail(receipntTo,receipntCC1,Subject,HtmlBody);
               system.debug ('PZ misses the deadline either  DPD deadline emails ' + cp1+email3);
         }
         Messaging.sendEmail(email3);
     }     
  }
}