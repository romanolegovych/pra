global class LMS_UpdateMappings implements Schedulable {
    
/*
CRON string : seconds/minutes/hours/day/month/year
LMS_UpdateMappings mappings = new LMS_UpdateMappings();
String str = '0 0 0 * * ?';
System.schedule('LMS_Update_Mappings', str, mappings);
*/ 
        
    global void execute(SchedulableContext cxt) {
        //Methods to update role_course/role_employee mapping data
        LMS_NightlyJobs.checkRoleCourseMappings();
        LMS_NightlyJobs.checkRoleEmployeeStartDate();
        
        //Method to send role_employee data to SABA
        LMS_NightlyJobs.sendRoleEmployeeMappings();
                   
        //Batch process used to send role_course data to SABA
        String rcAddQuery = 'SELECT Role_Id__c, Course_Id__c, Status__c, Sync_Status__c, SABA_Role_Course_PK__c, Assigned_On__c, Assigned_By__c, ' + 
            'Course_Id__r.SABA_ID_PK__c, Role_Id__r.SABA_Role_PK__c FROM LMS_Role_Course__c WHERE ' + 
			'(Status__c = \'Pending - Add\' AND Course_Id__r.Available_From__c <= TODAY AND Commit_Date__c = null) OR ' + 
            '(Status__c = \'Pending - Add\' AND Commit_Date__c <= TODAY) ORDER BY Role_Id__c';
        String rcAddParams = Json.serialize(new List<String>{rcAddQuery, 'A'});
        PRA_Batch_Queue__c rcAddBatch = 
        new PRA_Batch_Queue__c(Batch_Class_Name__c = 'LMS_NightlyBatch', Parameters_JSON__c = rcAddParams, 
            Status__c = 'Waiting', Priority__c = 5, Scope__c = 200);
        
        String rcDeleteQuery = 'SELECT Role_Id__c, Course_Id__c, Status__c, Sync_Status__c, SABA_Role_Course_PK__c, Assigned_On__c, Assigned_By__c,' +
            'Course_Id__r.SABA_ID_PK__c, Role_Id__r.SABA_Role_PK__c FROM LMS_Role_Course__c WHERE ' +  
            'Status__c = \'Removed\' OR (Status__c = \'Pending - Delete\' AND Commit_Date__c <= TODAY) ORDER BY Course_Id__c';
        String rcDeleteParams = Json.serialize(new List<String>{rcDeleteQuery, 'D'});
        PRA_Batch_Queue__c rcDeleteBatch =
        new PRA_Batch_Queue__c(Batch_Class_Name__c = 'LMS_NightlyBatch', Parameters_JSON__c = rcDeleteParams, 
            Status__c = 'Waiting', Priority__c = 5, Scope__c = 75);
            
        insert rcAddBatch;
        insert rcDeleteBatch;
    }
}