public class PAWS_ProjectCVO
{
	public ID RecordID { get; private set; }
	public String ProjectID { get; private set; }

	public PAWS_ProjectCVO(ecrf__c inEcrfProject)
	{ 
		RecordID = inEcrfProject.ID;
		ProjectID = inEcrfProject.Project_ID__r.Name;
	}
}