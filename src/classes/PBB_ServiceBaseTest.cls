@isTest
private class PBB_ServiceBaseTest {

	private static final String WORD_VALUE = 'Model';
	private static final String ERROR_MESSAGE = 'Some message';
	
	private static ServiceBaseExt ext;
	
	private static Service_Model__c sm;
	
	private static PBB_TestUtils testUtils = new PBB_TestUtils();
	
	@isTest
	private static void testConstructor1(){
		Test.startTest();
			ext = new ServiceBaseExt();
		Test.stopTest();
	}
	
	@isTest
	private static void ServiceBase2(){
		sm = testUtils.CreateServiceModelAttributes();
		Test.startTest();
			ext = new ServiceBaseExt(sm.Id);
		Test.stopTest();
		System.assertEquals( sm.Id, ext.parentServiceId );
		System.assertEquals( sm.Id, ext.previousServiceId );
	}
	
	@isTest
	private static void removeServiceTest(){
		ext = new ServiceBaseExt();
		sm = testUtils.CreateServiceModelAttributes();
		ext.serviceRecord = sm;
		ext.serviceRecords = new List<Service_Model__c> { sm };
		Test.setCurrentPage( Page.PBB_LayoutModel );
		ApexPages.currentPage().getParameters().put( 'editServiceModelId', sm.Id );
		Test.startTest();
			ext.removeService();
		Test.stopTest();
		System.assertEquals( 0, ApexPages.getMessages().size() );
		System.assertEquals( null, ext.serviceRecord );
		
	}
	
	@isTest
	private static void removeServiceTestNullPointer(){
		ext = new ServiceBaseExt();
		Test.setCurrentPage( Page.PBB_LayoutModel );
		Test.startTest();
			ext.removeService();
		Test.stopTest();
		System.assertEquals( 1, ApexPages.getMessages().size() );
	}
	
	@isTest
	private static void getDetailServiceTest(){
		ext = new ServiceBaseExt();
		sm = testUtils.CreateServiceModelAttributes();
		ext.serviceRecords = new List<Service_Model__c>{ sm };
		ext.serviceId = sm.Id;
		Test.startTest();
			ext.getDetailService();
		Test.stopTest();
		System.assertEquals( sm.Id, ext.serviceRecord.Id );
		
	}
	
	@isTest
	private static void isValidServiceTestPositive(){
		ext = new ServiceBaseExt();
		ext.serviceForUpdate = testUtils.CreateServiceModelAttributes();
		ext.serviceForUpdate.put('Name', 'Some value');
		Test.startTest();
			System.assert( ext.isValidService() );
		Test.stopTest();
		System.assertEquals( 0, ApexPages.getMessages().size() );
	}
	
	@isTest
	private static void isValidServiceTestNegative(){
		ext = new ServiceBaseExt();
		ext.serviceForUpdate = testUtils.CreateServiceModelAttributes();
		ext.serviceForUpdate.put('Name', null);
		Test.startTest();
			System.assert( !ext.isValidService() );
		Test.stopTest();
		System.assertEquals( 1, ApexPages.getMessages().size() );
	}
	
	@isTest
	private static void createServiceTestPositive(){
		ext = new ServiceBaseExt();
		ext.serviceForUpdate = new Service_Model__c();
		ext.serviceForUpdate.put('Name', 'Some value');
		Test.startTest();
			ext.createService();
		Test.stopTest();
		System.assertEquals( 0, ApexPages.getMessages().size() );
		System.assertEquals( 1, ext.serviceRecords.size() );
	}
	
	@isTest
	private static void createServiceTestNotValid(){
		ext = new ServiceBaseExt();
		ext.serviceForUpdate = new Service_Model__c();
		ext.serviceForUpdate.put('Name', null);
		Test.startTest();
			ext.createService();
		Test.stopTest();
		System.assertEquals( 1, ApexPages.getMessages().size() );
		System.assertEquals( 0, ext.serviceRecords.size() );
	}
	
	@isTest
	private static void saveServiceTestPositive(){
		ext = new ServiceBaseExt();
		ext.serviceForUpdate = new Service_Model__c();
		ext.serviceForUpdate.put('Name', 'Some value');
		Test.startTest();
			ext.saveService();
		Test.stopTest();
		System.assertEquals( 0, ApexPages.getMessages().size() );
		System.assert( ext.isValid );
	}
	
	@isTest
	private static void saveServiceNegative(){
		ext = new ServiceBaseExt();
		ext.serviceForUpdate = null;
		Test.setCurrentPage( Page.PBB_LayoutModel );
		Test.startTest();
			ext.saveService();
		Test.stopTest();
		System.assertEquals( 1, ApexPages.getMessages().size() );
		System.assert( !ext.isValid );
	}
	
	@isTest
	private static void cancelServiceTestPositive(){
		testUtils.CreateServiceModelAttributes();
		ext = new ServiceBaseExt();
		Test.startTest();
			ext.cancelService();
		Test.stopTest();
		System.assertEquals( 1, ext.serviceRecords.size() );
	}
	
	@isTest
	private static void showErrorMessageOnDeleteTest(){
		Test.startTest();
			PBB_ServiceBase.showErrorMessageOnDelete( new PBB_ServiceBase.ServiceModelException(ERROR_MESSAGE) );
		Test.stopTest();
		System.assertEquals( 1, ApexPages.getMessages().size() );
		System.assertEquals( ERROR_MESSAGE, ApexPages.getMessages()[0].getSummary() );
	}
	
	@isTest
	private static void showErrorMessageTest(){
		Test.startTest();
			PBB_ServiceBase.showErrorMessage( new PBB_ServiceBase.ServiceModelException(ERROR_MESSAGE) );
		Test.stopTest();
		System.assertEquals( 1, ApexPages.getMessages().size() );
		System.assertEquals( ERROR_MESSAGE, ApexPages.getMessages()[0].getSummary() );
	}
	
	@isTest
	private static void getEditServiceIdTest(){
		sm = testUtils.CreateServiceModelAttributes();
		ext = new ServiceBaseExt();
		Test.setCurrentPage( Page.PBB_LayoutModel );
		ApexPages.currentPage().getParameters().put( 'editServiceModelId', sm.Id );
		Test.startTest();
			System.assertEquals( sm.Id, ext.getEditServiceId() );
		Test.stopTest();
	}
	
	@isTest
	private static void getCloneServiceIdTest(){
		sm = testUtils.CreateServiceModelAttributes();
		ext = new ServiceBaseExt();
		Test.setCurrentPage( Page.PBB_LayoutModel );
		ApexPages.currentPage().getParameters().put( 'cloneServiceModelId', sm.Id );
		Test.startTest();
			System.assertEquals( sm.Id, ext.getCloneServiceId() );
		Test.stopTest();
	}
	
	private class ServiceBaseExt extends PBB_ServiceBase {
		
		public ServiceBaseExt(){
			
		}
		
		public ServiceBaseExt(Id parentId){
			super(parentId);
		}
		
	    protected override String getServiceWord(){
	    	return WORD_VALUE;
	    }
	    
		public override void refreshServices(){
			serviceRecords = PBB_ServiceModelServices.getServiceModels();
		}
	
	
	    public override void preparationCreateOrEditService() {
	    }
	    
	}   
	 
}