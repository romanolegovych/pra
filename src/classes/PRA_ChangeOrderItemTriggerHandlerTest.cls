@isTest (SeeAllData=false)
private class PRA_ChangeOrderItemTriggerHandlerTest {

    static testMethod void shouldUpdateRevisedUnitCostAndTotalCost() {
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        //tu.createBidContractDetails();
        
        Change_Order__c co = tu.createChangeOrder(tu.clientProject.id);
        
        // when
        List <Change_Order_Item__c> liCOItems = [select (select Id, Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, Amendment_CO_Hours_unit__c, BUF_Code__c, 
                                                                Change_Order_Item__c, Current_Contract_Hours__c, Current_Contract_Value__c, Current_Hours_Unit__c, 
                                                                Current_Total_Cost__c, Current_Unit_Cost__c, Revised_Hours_Unit__c, Revised_Total_Cost__c, 
                                                                Revised_Total_Hours__c, Revised_Unit_Cost__c, Type__c
                                                            from Change_Order_Line_Item__r), 
                                                        Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Number_Of_Contracted_Units__c,
                                                        Revised_Unit_Cost__c, Revised_Total_Cost__c
                                                from Change_Order_Item__c
                                                where Change_Order__c = :co.id];
        
        // then
        Test.startTest();
        for (Change_Order_Item__c coItem : liCOItems){
            Decimal expectedRevUnitCost = 0;
            Decimal expectedRevTotalCost = 0;
            
            system.assert(coItem.Change_Order_Line_Item__r.size()>0);
            for (Change_Order_Line_Item__c coLineItem : coItem.Change_Order_Line_Item__r){
                expectedRevUnitCost += coLineItem.Revised_Unit_Cost__c;
                expectedRevTotalCost += coLineItem.Revised_Total_Cost__c;
            }
            
            system.assertEquals(expectedRevUnitCost,coItem.Revised_Unit_Cost__c );
            system.assertEquals(expectedRevTotalCost,coItem.Revised_Total_Cost__c );
        }
        Change_Order_Item__c coUpdateItem = liCOItems.get(0);
        coUpdateItem.Amendment_CO_Number_of_Units__c = 10;
        update coUpdateItem;
        
        Decimal expectedAmUnitCost = 0;
        List <Change_Order_Item__c> liUpdatedCOItems = [select (select Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, Amendment_CO_Hours_unit__c, Current_Contract_Hours__c, 
                                                            Current_Contract_Value__c, Current_Hours_Unit__c,Revised_Unit_Cost__c, Revised_Total_Cost__c, Is_Labor_unit__c
                                                            from Change_Order_Line_Item__r
                                                            ), 
                                                        Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Number_Of_Contracted_Units__c,
                                                        Revised_Unit_Cost__c, Revised_Total_Cost__c 
                                                from Change_Order_Item__c
                                                where Change_Order__c = :co.id];
        for (Change_Order_Item__c coItem : liUpdatedCOItems){
            Decimal expectedRevUnitCost = 0;
            Decimal expectedRevTotalCost = 0;
            
            for (Change_Order_Line_Item__c coLineItem : coItem.Change_Order_Line_Item__r){
                expectedRevUnitCost += coLineItem.Revised_Unit_Cost__c;
                expectedRevTotalCost += coLineItem.Revised_Total_Cost__c;
            }
            
            system.assertEquals(expectedRevUnitCost,coItem.Revised_Unit_Cost__c );
            system.assertEquals(expectedRevTotalCost,coItem.Revised_Total_Cost__c );
        }
        
        coUpdateItem = liCOItems.get(0);
        system.assertEquals(2, coUpdateItem.Change_Order_Line_Item__r.size());
        Change_Order_Line_Item__c coUpdatedCOLineItem  = coUpdateItem.Change_Order_Line_Item__r.get(0);
        Change_Order_Item__c coCheckedItem;
        
        //for each of 2 conditions
        
        // check change of Revised Unit Cost and Revised Total Cost
        coUpdatedCOLineItem.Amendment_BUF_Code_Bill_Rate__c = 2;
        update coUpdatedCOLineItem;
        
        liUpdatedCOItems = [select (select Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, Amendment_CO_Hours_unit__c, Current_Contract_Hours__c, 
                                                            Current_Contract_Value__c, Current_Hours_Unit__c,Revised_Unit_Cost__c, Revised_Total_Cost__c, Is_Labor_unit__c
                                                            from Change_Order_Line_Item__r
                                                            ), 
                                                        Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Number_Of_Contracted_Units__c,
                                                        Revised_Unit_Cost__c, Revised_Total_Cost__c 
                                                from Change_Order_Item__c
                                                where Change_Order__c = :co.id];
        for (Change_Order_Item__c coItem : liUpdatedCOItems){
            Decimal expectedRevUnitCost = 0;
            Decimal expectedRevTotalCost = 0;
            
            for (Change_Order_Line_Item__c coLineItem : coItem.Change_Order_Line_Item__r){
            	if (coLineItem.Is_Labor_unit__c)
            	{
                expectedRevUnitCost += coLineItem.Revised_Unit_Cost__c;
                expectedRevTotalCost += coLineItem.Revised_Total_Cost__c;
            	}
            }
            
            system.assertEquals(expectedRevUnitCost,coItem.Revised_Unit_Cost__c );
            system.assertEquals(expectedRevTotalCost,coItem.Revised_Total_Cost__c );
        }
        
        
        
        // Test delete change order line items
        delete coUpdatedCOLineItem;
        
        liUpdatedCOItems = [select (select Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, Amendment_CO_Hours_unit__c, Current_Contract_Hours__c, 
                                                            Current_Contract_Value__c, Current_Hours_Unit__c,Revised_Unit_Cost__c, Revised_Total_Cost__c, Is_Labor_unit__c
                                                            from Change_Order_Line_Item__r
                                                            ), 
                                                        Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Number_Of_Contracted_Units__c,
                                                        Revised_Unit_Cost__c, Revised_Total_Cost__c
                                                from Change_Order_Item__c
                                                where Change_Order__c = :co.id];
        for (Change_Order_Item__c coItem : liUpdatedCOItems){
            Decimal expectedRevUnitCost = 0;
            Decimal expectedRevTotalCost = 0;
            
            for (Change_Order_Line_Item__c coLineItem : coItem.Change_Order_Line_Item__r){
            	if (coLineItem.Is_Labor_unit__c){
                expectedRevUnitCost += coLineItem.Revised_Unit_Cost__c;
                expectedRevTotalCost += coLineItem.Revised_Total_Cost__c;
            	}
            }
            
            system.assertEquals(expectedRevUnitCost,coItem.Revised_Unit_Cost__c );
            system.assertEquals(expectedRevTotalCost,coItem.Revised_Total_Cost__c );
        }
        
        Test.stopTest();
    }
    static testMethod void shouldUpdateAmmendmentHours() {
        // given
        Double addedUnits = 13;
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        //tu.createBidContractDetails();
        
        Change_Order__c co = tu.createChangeOrder(tu.clientProject.id);
        
        // when
        List <Change_Order_Item__c> liCOItems = [select (select Id, Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, Amendment_CO_Hours_unit__c, BUF_Code__c, 
                                                                Change_Order_Item__c, Current_Contract_Hours__c, Current_Contract_Value__c, Current_Hours_Unit__c, 
                                                                Current_Total_Cost__c, Current_Unit_Cost__c, Revised_Hours_Unit__c, Revised_Total_Cost__c, 
                                                                Revised_Total_Hours__c, Revised_Unit_Cost__c, Type__c
                                                            from Change_Order_Line_Item__r), 
                                                        Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Number_Of_Contracted_Units__c,
                                                        Revised_Unit_Cost__c, Revised_Total_Cost__c
                                                from Change_Order_Item__c
                                                where Change_Order__c = :co.id];
        system.assert (liCOItems.size()>0);
        Change_Order_Item__c coItem = liCOItems.get(0);
        system.assert (coItem.Change_Order_Line_Item__r.size()>0);
        Map<Id, Change_Order_Line_Item__c> mapCOLineItems = new Map<Id, Change_Order_Line_Item__c>();
        for(Change_Order_Line_Item__c coLineItem : coItem.Change_Order_Line_Item__r){
            mapCOLineItems.put(coLineItem.id, coLineItem);
        }
        
        Test.startTest();
        coItem.Amendment_CO_Number_of_Units__c = addedUnits;
        update coItem;
        
        // then
        Change_Order_Item__c updatedCOItem = [select (select Id, Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, Amendment_CO_Hours_unit__c, BUF_Code__c, 
                                                                Change_Order_Item__c, Current_Contract_Hours__c, Current_Contract_Value__c, Current_Hours_Unit__c, 
                                                                Current_Total_Cost__c, Current_Unit_Cost__c, Revised_Hours_Unit__c, Revised_Total_Cost__c, 
                                                                Revised_Total_Hours__c, Revised_Unit_Cost__c, Type__c
                                                            from Change_Order_Line_Item__r), 
                                                        Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Number_Of_Contracted_Units__c,
                                                        Revised_Unit_Cost__c, Revised_Total_Cost__c
                                                from Change_Order_Item__c
                                                where id = :coItem.id];
                                                
        system.assertEquals(coItem.Amendment_CO_Number_of_Units__c, updatedCOItem.Amendment_CO_Number_of_Units__c);
        
        for (Change_Order_Line_Item__c coLineItem : updatedCOItem.Change_Order_Line_Item__r){
            Change_Order_Line_Item__c oldCOLineItem = mapCOLineItems.get(coLineItem.id);
            Double expectedVal = coItem.Amendment_CO_Number_of_Units__c * oldCOLineItem.Current_Hours_Unit__c;
            system.assertEquals(expectedVal, coLineItem.Amendment_CO_Hours__c);
            system.assertEquals(oldCOLineItem.Revised_Unit_Cost__c, coLineItem.Revised_Unit_Cost__c);
            system.assertEquals(oldCOLineItem.Revised_Hours_Unit__c, coLineItem.Revised_Hours_Unit__c);
        }
        
        Test.stopTest();
    }
    static testMethod void shouldUpdateAmmendmentHoursDecreseNumberOfUnits() {
        // given
        Double addedUnits = -7;
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        //tu.createBidContractDetails();
        
        Change_Order__c co = tu.createChangeOrder(tu.clientProject.id);
        
        // when
        List <Change_Order_Item__c> liCOItems = [select (select Id, Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, Amendment_CO_Hours_unit__c, BUF_Code__c, 
                                                                Change_Order_Item__c, Current_Contract_Hours__c, Current_Contract_Value__c, Current_Hours_Unit__c, 
                                                                Current_Total_Cost__c, Current_Unit_Cost__c, Revised_Hours_Unit__c, Revised_Total_Cost__c, 
                                                                Revised_Total_Hours__c, Revised_Unit_Cost__c, Type__c
                                                            from Change_Order_Line_Item__r), 
                                                        Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Number_Of_Contracted_Units__c,
                                                        Revised_Unit_Cost__c, Revised_Total_Cost__c
                                                from Change_Order_Item__c
                                                where Change_Order__c = :co.id];
        system.assert (liCOItems.size()>0);
        Change_Order_Item__c coItem = liCOItems.get(0);
        system.assert (coItem.Change_Order_Line_Item__r.size()>0);
        Map<Id, Change_Order_Line_Item__c> mapCOLineItems = new Map<Id, Change_Order_Line_Item__c>();
        for(Change_Order_Line_Item__c coLineItem : coItem.Change_Order_Line_Item__r){
            mapCOLineItems.put(coLineItem.id, coLineItem);
        }
        
        Test.startTest();
        coItem.Amendment_CO_Number_of_Units__c = addedUnits;
        update coItem;
        
        // then
        Change_Order_Item__c updatedCOItem = [select (select Id, Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, Amendment_CO_Hours_unit__c, BUF_Code__c, 
                                                                Change_Order_Item__c, Current_Contract_Hours__c, Current_Contract_Value__c, Current_Hours_Unit__c, 
                                                                Current_Total_Cost__c, Current_Unit_Cost__c, Revised_Hours_Unit__c, Revised_Total_Cost__c, 
                                                                Revised_Total_Hours__c, Revised_Unit_Cost__c, Type__c
                                                            from Change_Order_Line_Item__r), 
                                                        Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Number_Of_Contracted_Units__c,
                                                        Revised_Unit_Cost__c, Revised_Total_Cost__c
                                                from Change_Order_Item__c
                                                where id = :coItem.id];
                                                
        system.assertEquals(coItem.Amendment_CO_Number_of_Units__c, updatedCOItem.Amendment_CO_Number_of_Units__c);
        
        for (Change_Order_Line_Item__c coLineItem : updatedCOItem.Change_Order_Line_Item__r){
            Change_Order_Line_Item__c oldCOLineItem = mapCOLineItems.get(coLineItem.id);
            Double expectedVal = coItem.Amendment_CO_Number_of_Units__c * oldCOLineItem.Current_Hours_Unit__c;
            system.assertEquals(expectedVal, coLineItem.Amendment_CO_Hours__c);
            system.assertEquals(oldCOLineItem.Revised_Unit_Cost__c, coLineItem.Revised_Unit_Cost__c);
            system.assertEquals(oldCOLineItem.Revised_Hours_Unit__c, coLineItem.Revised_Hours_Unit__c);
        }
        
        Test.stopTest();
    }
}