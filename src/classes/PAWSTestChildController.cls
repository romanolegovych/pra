public class PAWSTestChildController
{
    public Integer SelectedRecordNumber {get;set;}
    
    public List<UserRole> Roles
    {
        get
        {
            if (Roles == null)
            {
                Roles = [Select Id, Name from UserRole Order By Name];
            }
            return Roles;
        }
        set;
    }
    
    public Set<String> SelectedNames
    {
        get
        {
            if (SelectedNames == null)
            {
                SelectedNames = new Set<String>();
                for (RoleEffort obj : Records)
                {
                    SelectedNames.add(obj.Name);
                }
            }
            return SelectedNames;
        }
        set;
    }
    
    public List<RoleEffort> Records
    {
        get
        {
            if (Records == null)
            {
                Records = new List<RoleEffort>();
            }
            return Records;
        }
        set;
    }
    
    public String RecordsJson
    {
        get
        {
            List<Map<String, Object>> listMapRecords = new List<Map<String, Object>>();
            for (RoleEffort obj : Records)
            {
                listMapRecords.add(new Map<String, Object>
                    {
                        'roleName' => obj.Name,
                        'effort' => obj.Effort,
                        'unit' => obj.Unit
                    });
                
            }
            
            Map<String, Object> mapRecords = new Map<String, Object>();
            mapRecords.put('description', '');
            mapRecords.put('data', new Map<String, Object>{'efforts' => listMapRecords});
            
            RecordsJson = JSON.serialize(mapRecords);
            return RecordsJson;
        }
        set;
    }
    
    public void AddRecord()
    {       
        List<Selectoption> options = findOptions();
        Records.add(new RoleEffort(options));
        ChangeRecord();
    }
    
    public void DeleteRecord()
    {
        Records.remove(SelectedRecordNumber);
        //SelectedRecordId;
    }
    
    public void ChangeRecord()
    {
        List<Selectoption> options = findOptions();
        
        for (RoleEffort obj : Records)
        {
            obj.Options = options.clone();
            obj.Options.add(0, new Selectoption(obj.Name, obj.Name));
        }
    }
    
    public List<Selectoption> findOptions()
    {
        SelectedNames = null;
        List<Selectoption> options = new List<Selectoption>();
        for (UserRole obj : Roles)
        {
            if (SelectedNames.contains(obj.Name))
                continue;
                
            options.add(new Selectoption(obj.Name, obj.Name));
        }
        
        return options;
    }
    
    public class RoleEffort
    {
        public String Name {get;set;}
        public Integer Effort {get;set;}
        public String Unit {get;set;}       
        public List<SelectOption> Options {get;set;}
        
        public RoleEffort(List<Selectoption> opt)
        {
            Name = opt.get(0).getValue();
            Unit = 'hrs';
            Options = opt;
        }
    }
}