public class PAWS_StepProperty_RM_Query_Controller
{
    public PAWS_StepProperty_RM_Query_Controller()
    {
    
        if(String.isEmpty(ApexPages.currentPage().getParameters().get('ropertyId')) == false)
        {
            QuerySelected = '';
        }
    }
    
    public String QuerySelected {get;set;}
    public List<SelectOption> QuerySelectOptions
    {
        get
        {
            QuerySelectOptions = new List<SelectOption>{ new SelectOption('','-please select-') };
            QuerySelectOptions.add(new SelectOption('1', 'Project Managers'));
            QuerySelectOptions.add(new SelectOption('2', 'Clinical System Designers'));
            QuerySelectOptions.add(new SelectOption('3', 'Safety Data Coordinators'));
            
            return QuerySelectOptions;
        }
        set;
    }
}