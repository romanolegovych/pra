public with sharing class BillFormulaModelsController {

	/**
	* to display the list Bill Rate Formulas Names
	*/
	public List<Formula_Name__c> billFormulaList    { get; set; }

	/** 
       @description Constructor
       @author Tkachenko Oleksiy
       @date 2015   
   */
	public BillFormulaModelsController() {
		billFormulaList = PBB_DataAccessor.getAllBillFormulaNames();
	}

	/** 
        @description Function to call New Page of required Model
        @author Tkachenko Oleksiy
        @date 2015  
   */
   public PageReference getNewPageReference() {
       Schema.DescribeSObjectResult R;
       R = Formula_Name__c.SObjectType.getDescribe();
       return new PageReference('/' + R.getKeyPrefix() + '/e');
   }
}