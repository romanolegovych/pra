/**
 * Test for all view role controllers
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestViewRoleControllers {
    
    static List<Job_Class_Desc__c> jobClasses;
    static List<Job_Title__c> jobTitles;
    static List<PRA_Business_Unit__c> businessUnits;
    static List<Department__c> departments;
    static List<Region__c> regions;
    static List<Country__c> countries;
    static List<Employee_Type__c> empTypes;
    static List<Employee_Status__c> empStatus;
    static List<WFM_Client__c> clients;
    static List<WFM_Project__c> projects;
    static List<WFM_Contract__c> contracts;
    static List<Role_Type__c> roleTypes;
    static List<LMS_Role__c> roles;
    static List<Course_Domain__c> courseDomains;
    static List<LMS_Course__c> courses;
    static List<LMS_Role_Course__c> roleCourses;
    static List<LMSConstantSettings__c> constants;
    static List<LMSCustomTabSettings__c> tabs;
    static List<CourseDomainSettings__c> domains;
    static List<RoleAdminServiceSettings__c> settings;
    
    static void init() {
        constants = new LMSConstantSettings__c[] {
            new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
            new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
            new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
            new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D'),
            new LMSConstantSettings__c(Name = 'typePRA', Value__c = 'PRA'),
            new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
            new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
            new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
            new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
            new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
            new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
            new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
            new LMSConstantSettings__c(Name = 'statusToDelete', Value__c = 'Delete'),
            new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended'),
            new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
            new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
            new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
            new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete')
        };
        insert constants;
        
        tabs = new LMSCustomTabSettings__c[] {
            new LMSCustomTabSettings__c(Name = 'CourseToRole', tabId__c = 'tabIdForLink')
        };
        insert tabs;
        
        domains = new CourseDomainSettings__c[] {
            new CourseDomainSettings__c(Name = 'Internal', Domain_Id__c = 'Internal'),
            new CourseDomainSettings__c(Name = 'PST', Domain_Id__c = 'Project Specific'),
            new CourseDomainSettings__c(Name = 'Archive', Domain_Id__c = 'Archive')
        };
        insert domains;
        
        settings = new RoleAdminServiceSettings__c[] {
            new RoleAdminServiceSettings__c(Name = 'DOMAIN', Value__c = 'DOMAIN'),
            new RoleAdminServiceSettings__c(Name = 'ENDPOINT', Value__c = 'ENDPOINT'),
            new RoleAdminServiceSettings__c(Name = 'TIMEOUT', Value__c = '50000')
        };
        insert settings;
        
        jobClasses = new Job_Class_Desc__c[] {
            new Job_Class_Desc__c(Name = 'class1', Job_Class_Code__c = 'code1', Job_Class_ExtID__c = 'class1'),
            new Job_Class_Desc__c(Name = 'class2', Job_Class_Code__c = 'code2', Job_Class_ExtID__c = 'class2'),
            new Job_Class_Desc__c(Name = 'projectClass', Job_Class_Code__c = 'code3', Job_Class_ExtID__c = 'projectClass', Is_Assign__c = true)
        };
        insert jobClasses;
        
        jobTitles = new Job_Title__c[] {
            new Job_Title__c(Job_Code__c = 'Code1', Job_Title__c = 'title1', Job_Class_Desc__c = jobClasses[0].Id, Status__c='A'),
            new Job_Title__c(Job_Code__c = 'Code2', Job_Title__c = 'title2', Job_Class_Desc__c = jobClasses[1].Id, Status__c='A'),
            new Job_Title__c(Job_Code__c = 'Code3', Job_Title__c = 'title3', Job_Class_Desc__c = jobClasses[1].Id, Status__c='A')
        };
        insert jobTitles;
        
        businessUnits = new PRA_Business_Unit__c[] {
            new PRA_Business_Unit__c(Name = 'unit', Business_Unit_Code__c = 'BUC', Status__c = 'A'),
            new PRA_Business_Unit__c(Name = 'unit2', Business_Unit_Code__c = 'BU2', Status__c = 'A')
        };
        insert businessUnits;
        
        departments = new Department__c[] {
            new Department__c(Name = 'department', Department_Code__c = 'DEPTC', Status__c = 'A'),
            new Department__c(Name = 'department2', Department_Code__c = 'DEPT2', Status__c = 'A')
        };
        insert departments;
        
        regions = new Region__c[] {
            new Region__c(Region_Name__c = 'region1', Region_Id__c = 1, Status__c = 'A'),
            new Region__c(Region_Name__c = 'region2', Region_Id__c = 2, Status__c = 'A')
        };
        insert regions;
        
        countries = new Country__c[] {
            new Country__c(Name = 'country1', Region_Name__c = 'region1', Country_Code__c = 'C1', PRA_Country_ID__c = '100'),
            new Country__c(Name = 'country2', Region_Name__c = 'region2', Country_Code__c = 'C2', PRA_Country_ID__c = '200')
        };
        insert countries;
        
        empTypes = new Employee_Type__c[] {
            new Employee_Type__c(Name = 'type1'),
            new Employee_Type__c(Name = 'type2')
        };
        insert empTypes;
        
        empStatus = new Employee_Status__c[] {
            new Employee_Status__c(Employee_Status__c = 'AA', Employee_Type__c = empTypes[0].Id),
            new Employee_Status__c(Employee_Status__c = 'IN', Employee_Type__c = empTypes[1].Id)
        };
        insert empStatus;
        
        clients = new WFM_Client__c[] {
            new WFM_Client__c(Name = 'Client1', Client_Unique_Key__c = 'Client1'),
            new WFM_Client__c(Name = 'Client2', Client_Unique_Key__c = 'Client2'),
            new WFM_Client__c(Name = 'c', Client_Unique_Key__c = 'c')
        };
        insert clients;
        
        contracts = new WFM_Contract__c[] {
            new WFM_Contract__c(Name = 'contract1', Contract_Unique_Key__c = 'contract1', Client_Id__c = clients[0].Id),
            new WFM_Contract__c(Name = 'contract2', Contract_Unique_Key__c = 'contract2', Client_Id__c = clients[1].Id),
            new WFM_Contract__c(Name = 'contract3', Contract_Unique_Key__c = 'contract3', Client_Id__c = clients[2].Id)
        };
        insert contracts;
        
        projects = new WFM_Project__c[] {
            new WFM_Project__c(Name = 'Project1', Project_Unique_Key__c = 'Project1', Contract_Id__c = contracts[0].Id),
            new WFM_Project__c(Name = 'Project2', Project_Unique_Key__c = 'Project2', Contract_Id__c = contracts[1].Id),
            new WFM_Project__c(Name = 'p', Project_Unique_Key__c = 'p', Contract_Id__c = contracts[2].Id)
        };
        insert projects;
        
        roleTypes = new Role_Type__c[] {
            new Role_Type__c(Name = 'PRA'),
            new Role_Type__c(Name = 'Project Specific'),
            new Role_Type__c(Name = 'Additional Role')
        };
        insert roleTypes;
        
        courseDomains = new Course_Domain__c[] {
            new Course_Domain__c(Domain__c = 'Internal'),
            new Course_Domain__c(Domain__c = 'Archive'),
            new Course_Domain__c(Domain__c = 'Project Specific')
        };
        insert courseDomains;
                
        courses = new LMS_Course__c[] {
            new LMS_Course__c(Offering_Template_No__c = 'course123', Course_Code__c = 'C123', Title__c = 'Course 123', Domain_Id__c = 'Internal',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse1',
                              Discontinued_From__c = Date.today()-1),
            new LMS_Course__c(Offering_Template_No__c = 'course321', Course_Code__c = 'C124', Title__c = 'Course 132', Domain_Id__c = 'Internal',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse2',
                              Discontinued_From__c = Date.today()+1),
            new LMS_Course__c(Offering_Template_No__c = 'course124', Course_Code__c = 'C234', Title__c = 'Course 234', Domain_Id__c = 'Internal',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse3',
                              Discontinued_From__c = Date.today()+1),
            new LMS_Course__c(Offering_Template_No__c = 'course341', Course_Code__c = 'C231', Title__c = 'Course 243', Domain_Id__c = 'Archive',
                              Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
                              Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse4',
                              Discontinued_From__c = Date.today()+1)
        };
        insert courses;
    }
    
    static void initRoles() {
        roles = new LMS_Role__c[] {
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = jobTitles[0].Id, Business_Unit__c = businessUnits[0].Id,
                            Department__c = departments[0].Id, Region__c = regions[0].Id, Country__c = countries[0].Id, Employee_Type__c = empTypes[0].Id, 
                            SABA_Role_PK__c = 'SABAROLE00000000001', Status__c = 'Active', Sync_Status__c = 'Y', Role_Type__c = roleTypes[0].Id),
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[1].Id, Job_Title__c = jobTitles[1].Id, Business_Unit__c = businessUnits[1].Id,
                            Department__c = departments[1].Id, Region__c = regions[1].Id, Country__c = countries[1].Id, Employee_Type__c = empTypes[1].Id, 
                            SABA_Role_PK__c = 'SABAROLE00000000002', Status__c = 'Inactive', Sync_Status__c = 'Y', Role_Type__c = roleTypes[0].Id),
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[2].Id, Project_Id__c = projects[0].Id, Client_Id__c = clients[0].Id, 
                            SABA_Role_PK__c = 'SABAROLE00000000003', Status__c = 'Active', Sync_Status__c = 'Y', Role_Type__c = roleTypes[1].Id),
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[2].Id, Project_Id__c = projects[1].Id, Client_Id__c = clients[1].Id, 
                            SABA_Role_PK__c = 'SABAROLE00000000004', Status__c = 'Inactive', Sync_Status__c = 'Y', Role_Type__c = roleTypes[1].Id),
            new LMS_Role__c(Adhoc_Role_Name__c = 'TestRole1', SABA_Role_PK__c = 'SABAROLE00000000005', Status__c = 'Active', Sync_Status__c = 'Y', 
                            Role_Type__c = roleTypes[2].Id),
            new LMS_Role__c(Adhoc_Role_Name__c = 'TestRole2', SABA_Role_PK__c = 'SABAROLE00000000006', Status__c = 'Inactive', Sync_Status__c = 'Y', 
                            Role_Type__c = roleTypes[2].Id)
        };
        insert roles;
    }
    
    static void initMappings() {
        roleCourses = new LMS_Role_Course__c[] {
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft Delete', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
            new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Committed', Assigned_By__c = 'ASA')
        };
        insert roleCourses;
    }

    static testMethod void testPRAJobClassLOV() {
        init();
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        c.jobTitle = null;
        List<SelectOption> options = c.getJobFamilyList();
        System.assert(options.size() == 4);
        System.assert(options[1].getValue() == 'class1');       
    }
    
    static testMethod void testPRAJobTitleLOV() {
        init();
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        c.jobFamily = null;
        List<SelectOption> options = c.getJobTitleList();
        System.assert(options.size() == 4);
        
        c.jobFamily = 'class1';
        c.getTitleOnFamily();
        options = c.getJobTitleList();
        System.assert(options.size() == 2 && options[1].getValue() == 'title1');
        
        c.jobFamily = 'class2';
        c.getTitleOnFamily();
        options = c.getJobTitleList();
        System.assert(options.size() == 3 && options[1].getValue() == 'title2' && options[2].getValue() == 'title3');
    }
    
    static testMethod void testPRABusinessUnitLOV() {
        init();
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        c.department = null;
        List<SelectOption> options = c.getBusinessUnitList();
        System.debug('-----------------------------------'+options[0].getValue());
        System.assert(options.size() == 3);
        
        c.department = 'department';
        //c.getBUOnDepartment();
        options = c.getBusinessUnitList();
        System.assert(options.size() == 3);
        System.assert(options[1].getValue() == 'unit');
    }
    
    static testMethod void testPRADepartmentLOV() {
        init();
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        c.businessUnit = null;
        List<SelectOption> options = c.getDepartmentList();
        System.debug('-----------------------------------'+options[1].getValue());
        System.assert(options.size() == 3);
        
        c.businessUnit = 'unit';
        //c.getDepartmentOnBU();
        options = c.getDepartmentList();
        System.assert(options.size() == 3);
        System.assert(options[1].getValue() == 'department');
    }
    
    static testMethod void testPRARegionLOV() {
        init();
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        List<SelectOption> options = c.getRegionList();
        System.assert(options.size() == 3 && options[1].getValue() == 'region1');
    }
    
    static testMethod void testPRACountryLOV() {
        init();
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        List<SelectOption> options = c.getCountryList();
        System.assert(options.size() == 3 && options[1].getValue() == 'country1');
        
        c.region = 'region2';
        c.getCountryOnRegion();
        options = c.getCountryList();
        System.assert(options.size() == 2 && options[1].getValue() == 'country2');
    }
    
    static testMethod void testPRAEmployeeTypeLOV() {
        init();
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        List<SelectOption> options = c.getTypesList();
        System.assert(options.size() == 3 && options[1].getValue() == 'Contractor' && options[2].getValue() == 'Employees');
    }
    
    static testMethod void testPRAResetMethods() {
        init();
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        
        c.roleReset();
        System.assert(c.renderError == false && c.renderBlock == false && c.roleList == null);
    }
    
    static testMethod void testPRACriteriaSearchSuccess() {
        init();
        initRoles();
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        c.jobFamily = jobClasses[0].Name;
        c.jobTitle = jobTitles[0].Job_Title__c;
        c.businessUnit = businessUnits[0].Name;
        c.department = departments[0].Name;
        c.region = regions[0].Region_Name__c;
        c.country = countries[0].Name;
        c.empType = empTypes[0].Name;
        c.search();
        
        System.assert(c.renderBlock == true && c.renderError == false);
    }
    
    static testMethod void testPRACriteriaSearchFailure() {
        init();
        initRoles();
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        c.jobFamily = jobClasses[1].Name;
        c.jobTitle = jobTitles[0].Job_Title__c;
        c.businessUnit = businessUnits[0].Name;
        c.department = departments[0].Name;
        c.region = regions[0].Region_Name__c;
        c.country = countries[0].Name;
        c.empType = empTypes[0].Name;
        c.search();
        
        System.assert(c.renderError == true && c.renderBlock == false && c.roleErrorText == 'No role name found, please enter different criteria');
    }
    
    static testMethod void testPRAAutoCompleteSearchSuccess() {
        init();
        initRoles();
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        c.roleName = 'title';
        c.search();
        
        System.assert(c.renderError == false && c.renderBlock == true);
    }
    
    static testMethod void testPRAAutoCompleteSearchFailure() {
        init();
        initRoles();
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        c.roleName = 'A';
        c.search();
        
        System.assert(c.renderError == true && c.renderBlock == false && c.roleErrorText == 'Role not found, please enter a different role name');
    }
    
    static testMethod void testPRAActivateRole() {
        init();
        initRoles();
        initMappings();
        
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        c.department = departments[0].Name;
        c.region = regions[0].Region_Name__c;
        c.search();
        
        c.roleList[0].checked = true;
        c.activateRoles();
    }
    
    static testMethod void testPRAInactivateRole() {
        init();
        initRoles();
        initMappings();
        
        LMS_ViewPRAController c = new LMS_ViewPRAController();
        c.jobFamily = jobClasses[0].Name;
        c.jobTitle = jobTitles[0].Job_Title__c;
        c.businessUnit = businessUnits[0].Name;
        c.department = departments[0].Name;
        c.region = regions[0].Region_Name__c;
        c.country = countries[0].Name;
        c.empType = empTypes[0].Name;
        c.search();
        
        c.roleList[0].checked = true;
        c.disableRoles();
    }
    
    static testMethod void testPSTController() {
        init();
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        
        System.assert(c.renderBlock == false && c.bWebServiceError == false && c.clientText == '' && 
            c.projectText == '' && c.roleName == 'Enter Role Name' && c.roleNameStyle == 'WaterMarkedTextBox' && 
            c.projectFilter == ' and Client_Id__c != \'\' and Client_Id__c != null and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')' &&
            c.roleFilter == ' and Role_Type__r.Name = \'Project Specific\' and Project_Id__r.Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')');
    }
    
    static testMethod void testPSTResetMethods(){
        init();
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        
        c.roleReset();
        
        System.assert(c.renderBlock == false && c.bWebServiceError == false && c.clientText == '' && 
            c.projectText == '' && c.roleName == 'Enter Role Name' && c.roleNameStyle == 'WaterMarkedTextBox' &&
            c.projectFilter == ' and Client_Id__c != \'\' and Client_Id__c != null and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')' &&
            c.roleFilter == ' and Role_Type__r.Name = \'Project Specific\' and Project_Id__r.Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')');
            
        c.resetAC();
        System.assert(c.clientText == '' && c.projectText == '' && c.projectFilter == 'and Client_Id__c != \'\' and Client_Id__c != null and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')');
    }
    
    static testMethod void testPSTProjectSearchType(){
        init();
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        c.clientText = 'client1';
        c.projectText = '';
        c.projSearchType();
        System.assert(c.projectFilter == ' and Client_Id__c != \'\' and Client_Id__c != null' + 
            ' and Client_Id__c = \'' + c.clientText + '\' and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')');
        
        c.clientText = '';
        c.projectText = 'project1';
        c.projSearchType();
        System.debug('------------------------project filter------------------------------'+c.projectFilter);
        System.assert(c.projectFilter == ' and Client_Id__c != \'\' and Client_Id__c != null' + 
            ' and Client_Id__c = \'\' and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')');
    }
    
    static testMethod void testPSTClientSearchType(){
        init();
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        c.clientText = '';
        c.projectText = 'project1';
        c.projSearchType();
        System.debug('------------------------project filter------------------------------'+c.projectFilter);
        System.assert(c.projectFilter == ' and Client_Id__c != \'\' and Client_Id__c != null' + 
            ' and Client_Id__c = \'\' and Status_Desc__c NOT IN (\'Bid\',\'Closed\',\'Lost\')');
    }
    
    static testMethod void testPSTIsValidClientId() {
        init();
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        c.clientText = clients[0].Name;
        Boolean isValid = LMS_ViewProjectController.isValidClientId(c.clientText);
        System.assert(isValid == true);
        
        c.clientText = 'invalidClient';
        isValid = LMS_ViewProjectController.isValidClientId(c.clientText);
        System.assert(isValid == false);
    }
    
    static testMethod void testPSTIsValidProjectId() {
        init();
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        c.projectText = projects[0].Name;
        Boolean isValid = LMS_ViewProjectController.isValidProjectId(c.projectText);
        System.assert(isValid == true);
        
        c.projectText = 'invalidProject';
        isValid = LMS_ViewProjectController.isValidProjectId(c.projectText);
        System.assert(isValid == false);
    }
    
    static testMethod void testPSTIsValidClientProjectPair() {
        init();
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        Boolean isValid = LMS_ViewProjectController.isValidClientProjectPair(c.clientText, c.projectText);
        System.assert(isValid == true);
        
        c.clientText = 'invalidClient';
        c.projectText = projects[0].Name;
        isValid = LMS_ViewProjectController.isValidClientProjectPair(c.clientText, c.projectText);
        System.assert(isValid == false);
    }
    
    static testMethod void testPSTProjectRoleLOV(){
        init();
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        List<SelectOption> options = c.getProjectRoles();
        System.assert(options.size() == 2 && options[1].getValue() == 'projectClass');
    }
    
    static testMethod void testPSTCriteriaSearchSuccess() {
        init();
        initRoles();
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        c.roleName = null;
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = jobClasses[2].Name;
        c.search();
        system.debug(c.renderBlock);
        system.debug(c.renderError);
        System.assert(c.renderBlock == true && c.renderError == false);
    }
    
    static testMethod void testPSTCriteriaSearchFailure() {
        init();
        initRoles();
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = jobClasses[1].Name;
        c.search();
        
        System.assert(c.renderError == true && c.renderBlock == false && 
            c.roleErrorText == 'Role either contains a bid, lost, or closed project or could not be found, please enter different criteria');
    }
    
    static testMethod void testPSTAutoCompleteSearchSuccess() {
        init();
        initRoles();
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        c.roleName = 'client';
        c.search();
        
        System.assert(c.renderError == false && c.renderBlock == true);
    }
    
    static testMethod void testPSTAutoCompleteSearchFailure() {
        init();
        initRoles();
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        c.roleName = 'Klient';
        c.search();
        
        System.assert(c.renderError == true && c.renderBlock == false && 
            c.roleErrorText == 'Role either contains a bid, lost, or closed project or could not be found, please enter a different role name');
    }
    
    static testMethod void testPSTActivateRole() {
        init();
        initRoles();
        initMappings();
        
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        c.clientText = clients[1].Name;
        c.projectText = projects[1].Name;
        c.projectRole = jobClasses[2].Name;
        c.search();
        
        c.roleList[0].checked = true;
        c.activateRoles();
    }
    
    static testMethod void testPSTInactivateRole() {
        init();
        initRoles();
        initMappings();
        
        LMS_ViewProjectController c = new LMS_ViewProjectController();
        c.clientText = clients[0].Name;
        c.projectText = projects[0].Name;
        c.projectRole = jobClasses[2].Name;
        c.search();
        
        c.roleList[0].checked = true;
        c.disableRoles();
    }
    
    static testMethod void testAdhocController() {
        init();
        LMS_ViewAdhocController c = new LMS_ViewAdhocController();
        
        System.assert(c.renderBlock == false && c.bWebServiceError == false && c.roleName == 'Enter Role Name' &&
            c.roleNameStyle == 'WaterMarkedTextBox' && c.roleFilter == ' and Role_Type__r.Name = \'Additional Role\'');
    }
    
    static testMethod void testAdhocAutoCompleteSearchSuccess() {
        init();
        initRoles();
        LMS_ViewAdhocController c = new LMS_ViewAdhocController();
        c.roleName = 'testrole';
        c.search();
        
        System.assert(c.renderError == false && c.renderBlock == true);
    }
    
    static testMethod void testAdhocAutoCompleteSearchFailure() {
        init();
        initRoles();
        LMS_ViewAdhocController c = new LMS_ViewAdhocController();
        c.roleName = 'roletest';
        c.search();
        
        System.assert(c.renderError == true && c.renderBlock == false && c.roleErrorText == 'Role not found, please enter a different role name');
    }
    
    static testMethod void testAdhocResetMethods(){
        init();
        LMS_ViewAdhocController c = new LMS_ViewAdhocController();
        
        c.roleReset();
        System.assert(c.renderBlock == false && c.renderError == false && c.bWebServiceError == false && c.roleName == 'Enter Role Name' &&
            c.roleNameStyle == 'WaterMarkedTextBox' && c.roleFilter == ' and Role_Type__r.Name = \'Additional Role\'');
    }
    
    static testMethod void testAdhocActivateRole() {
        init();
        initRoles();
        initMappings();
        
        LMS_ViewAdhocController c = new LMS_ViewAdhocController();
        c.roleName = 'TestRole2';
        c.search();
        
        c.roleList[0].checked = true;
        c.activateRoles();
    }
    
    static testMethod void testAdhocInactivateRole() {
        init();
        initRoles();
        initMappings();
        
        LMS_ViewAdhocController c = new LMS_ViewAdhocController();
        c.roleName = 'TestRole1';
        c.search();
        
        c.roleList[0].checked = true;
        c.disableRoles();
    }
}