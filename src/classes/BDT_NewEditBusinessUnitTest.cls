@isTest 
private class BDT_NewEditBusinessUnitTest {
	public static List<Site__c> mySiteList  {get;set;}
	
	static testMethod void createBUPath() { 
		BDT_NewEditBusinessUnit newEditBU = new BDT_NewEditBusinessUnit();
		
		newEditBU.businessUnit.Abbreviation__c = 'BBB';
		newEditBU.businessUnit.Name = 'ITS MY NAME';
		
		System.assertNotEquals(null, newEditBU.save());
		
		System.currentPageReference().getParameters().put('buId', newEditBU.businessUnit.id);
		BDT_NewEditBusinessUnit newEditBU1 = new BDT_NewEditBusinessUnit();
		
		System.assertNotEquals(null, newEditBU1.cancel());
		
	}
	
	static testMethod void failEditBUPath() { 
		
		System.currentPageReference().getParameters().put('buId', '1111111111111111');
		BDT_NewEditBusinessUnit newEditBU2 = new BDT_NewEditBusinessUnit();
		
		System.assertNotEquals(null, newEditBU2.businessUnit);
		
	}
	
	static testMethod void deleteBUPath() { 
		BDT_NewEditBusinessUnit newEditBU = new BDT_NewEditBusinessUnit();
		
		newEditBU.businessUnit.Abbreviation__c = 'BBB';
		newEditBU.businessUnit.Name = 'ITS MY NAME';
		
		System.assertNotEquals(null, newEditBU.save());
		
		mySiteList = new List<Site__c>();
		for(Integer i=0; i<5; i++){
				mySiteList.add(new Site__c(Name='SI'+i, Abbreviation__c= 'SI'+i+newEditBU.businessUnit.Abbreviation__c, Address_Line_1__c = 'Netherlands '+ i, City__c= 'Assen', Business_Unit__c =newEditBU.businessUnit.id));
		}
		insert mySiteList;
		
		System.currentPageReference().getParameters().put('buId', newEditBU.businessUnit.id);
		BDT_NewEditBusinessUnit newEditBU1 = new BDT_NewEditBusinessUnit();
		
		System.assertNotEquals(null, newEditBU1.deleteSoft());
		
	}
	
}