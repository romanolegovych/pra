/** 
     * @description Test class CM_AgreementRedirectController
     * @author      Lavakusareddy Poluru
     * @date        Created: 30-Jan-2015
     */


@isTest
private class CM_AgreementRedirectControllerTest {

private static final String Agreementid = [SELECT Id from RecordType WHERE SObjectType = 'Apttus__APTS_Agreement__c' and DeveloperName = 'Agreement'].Id;
private static final String Templateid= [SELECT Id from RecordType WHERE SObjectType = 'Apttus__APTS_Agreement__c' and DeveloperName = 'Template'].Id;
private static final string testretURL = Apttus__APTS_Agreement__c.sObjectType.getDescribe().getKeyPrefix();
        
      /** 
     * @description Test method testredirectpage
     * @author      Lavakusareddy Poluru
     * @date        Created: 30-Jan-2015
     */  
      
    static testMethod void testredirectpage() {
        Apttus__APTS_Agreement__c agreement = new Apttus__APTS_Agreement__c();
        ApexPages.StandardController Cont = new ApexPages.StandardController(agreement);
        
        //Agreement Record Type 
        ApexPages.CurrentPage().GetParameters().put('RecordType',Agreementid);
        ApexPages.CurrentPage().GetParameters().put('retURL',testretURL);
        CM_AgreementRedirectController redirectcont1 = new CM_AgreementRedirectController(Cont); 
                
        test.startTest();
        redirectcont1.redirect();
        redirectcont1.docancel();
        redirectcont1.save();
        test.stoptest();
         
                        
    }
}