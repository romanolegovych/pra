/*
 * PRAFIT_DashboardController supports Visualforce Charting components on the VF page PRAFIT_DashboardPage
 * 
 *  IMPORTANT: Does NOT use the “with sharing” keyword to allow the Charting components to 
 *  display KPIs and other aggregate info across all of the PRAFIT_Action sObject records
 *  public without sharing class PRAFIT_DashboardController{
 */

public class PRAFIT_DashboardController{
    
    //variables
        public Boolean FORCEadmin {get; private set;}
        public Boolean PRAFITappAdmin {get; private set;}
        public Boolean PRAFITpraRPS {get; private set;} 
        public Boolean PRAFITpraCRI {get; private set;}
        private String recordTypeIdPraRps {get; set;}
        private String recordTypeIdPraCri {get; set;}
        private List<String> functionalTeamList {get; set;}
           
    //constants
        private static final String customSettingDashboardPageCalendarURLpraRPS = 'dashboardPageCalendarURLpraRPS';
        private static final String customSettingDashboardPageCalendarURLpraCRI = 'dashboardPageCalendarURLpraCRI';
        private static final String praCriPicklistFilter = 'CRILT:';
        private static final String permSetNameAppAdmin = 'PRAFIT:: App Admin';
        private static final String permSetNamePraRps = 'PRAFIT:: PRA - RPS Standard User';
        private static final String permSetNamePraCri = 'PRAFIT:: PRA - CRI Lifetree Standard User';
        private static final String profileNameSysAdmin = 'System Administrator';
        private static final String recordTypeNamePraRps = 'PRA - RPS';
        private static final String recordTypeNamePraCri = 'PRA - CRI Lifetree';
    
    //constructor
        public PRAFIT_DashboardController(){
        	init();
		}
		
	//initialize variables
		private void init(){
			FORCEadmin = PRAFIT_Utils.checkProfile(profileNameSysAdmin);
         	PRAFITappAdmin = PRAFIT_Utils.checkPermissionSet(permSetNameAppAdmin); 	
         	PRAFITpraRPS = PRAFIT_Utils.checkPermissionSet(permSetNamePraRps); 	
         	PRAFITpraCRI = PRAFIT_Utils.checkPermissionSet(permSetNamePraCri);
         	recordTypeIdPraRps = PRAFIT_Utils.getRecordTypeId('PRAFIT_Action__c', recordTypeNamePraRps);
         	recordTypeIdPraCri = PRAFIT_Utils.getRecordTypeId('PRAFIT_Action__c', recordTypeNamePraCri);
         	functionalTeamList = PRAFIT_Utils.getPickListValues('PRAFIT_Action__c', 'Functional_Team__c');
		}
        
    //get the public calendar URL for the RPS initiative
        public String getDashboardPageCalendarUrlRPS(){
            return getDashboardPageCalendarURL(PRAFIT_Utils.prafitCustomSetting(customSettingDashboardPageCalendarURLpraRPS));
        }
    
    //get the public calendar URL for the CRI initiative
        public String getDashboardPageCalendarUrlCRI(){
            return getDashboardPageCalendarURL(PRAFIT_Utils.prafitCustomSetting(customSettingDashboardPageCalendarURLpraCRI));
        }
    //get the id of the public calendar to be used within the PRAFIT_DashboardPage.page
        //return a string formatted as a URL for displaying standard Public Calendars
        private String getDashboardPageCalendarURL(String customSettingValue){
            String dashboardPageCalendarURL = (customSettingValue==null||customSettingValue=='')?'/00U/c?cType=1&isdtp=vw':'/00U/c?cType=1&cal_lkid='+ customSettingValue + '&isdtp=vw';
            return dashboardPageCalendarURL;
        }        
    
    //run stats for use by Apex Charting
    	public List<ChartDataItem> getPRAFITActionStatusByFunctionalTeamRPS() {
        	return PRAFIT_DashboardController.generatePRAFITActionStatusByFunctionalTeam(this.recordTypeIdPraRps, this.recordTypeIdPraRps, this.recordTypeIdPraCri, this.functionalTeamList);
    	}
    	public List<ChartDataItem> getPRAFITActionsIncompleteByFunctionalTeamRPS() {
        	return PRAFIT_DashboardController.generatePRAFITActionsIncompleteByFunctionalTeam(this.recordTypeIdPraRps, this.recordTypeIdPraRps, this.recordTypeIdPraCri, this.functionalTeamList);
    	}
    	public List<ChartDataItem> getPRAFITActionStatusByFunctionalTeamCRI() {
    		return PRAFIT_DashboardController.generatePRAFITActionStatusByFunctionalTeam(this.recordTypeIdPraCri, this.recordTypeIdPraRps, this.recordTypeIdPraCri, this.functionalTeamList);
    	}
    	public List<ChartDataItem> getPRAFITActionsIncompleteByFunctionalTeamCRI() {
    		return PRAFIT_DashboardController.generatePRAFITActionsIncompleteByFunctionalTeam(this.recordTypeIdPraCri, this.recordTypeIdPraRps, this.recordTypeIdPraCri, this.functionalTeamList);
    	}
            
    //wrapper class for use with PRAFIT Dashboard Chart
        public class ChartDataItem  implements Comparable{
            public String label1 {get; private set;}
            public Integer data1 {get; private set;}
            public Integer data2 {get; private set;}
            public Integer data3 {get; private set;} 
            public Integer data4 {get; private set;}
            public String sort1 {get; private set;}
            //make this custom class sortable in a list
            public string sortKey() { 
                return sort1;
            }
            public Integer compareTo(Object compareTo) {
                ChartDataItem compareToRow = (ChartDataItem)compareTo;
                if (sortKey() == compareToRow.sortKey()) return 0;
                if (sortKey() > compareToRow.sortKey()) return 1;
                return -1;        
            } 
        }
    
    //getter for aggregate data list of PRAFIT_Actions by Status by Functional Team
        private static List<ChartDataItem> generatePRAFITActionStatusByFunctionalTeam(string recordTypeIdFilter, string recordTypeIdPraRps, string recordTypeIdPraCri, list<String> functionalTeamList){        	
			
			//clean funcitonal team list for CRI or PRS Initiative based on recordTypeIdFilter
			List<String> localFunctionalTeamList = functionalTeamList.clone();			
			system.debug('localFunctionalTeamList for generatePRAFITActionStatusByFunctionalTeam = ' + localFunctionalTeamList);
			for (Integer i=0; i < localFunctionalTeamList.size(); i++){
        		string dataItem = localFunctionalTeamList[i];
        		if(recordTypeIdFilter == recordTypeIdPraRps && dataItem.contains(praCriPicklistFilter)){
            		localFunctionalTeamList.remove(i);
            		i = i-1;
            	}else if(recordTypeIdFilter == recordTypeIdPraCri && !dataItem.contains(praCriPicklistFilter)){
            		localFunctionalTeamList.remove(i);
            		i = i-1;
            	}
            }
            system.debug('cleaned localFunctionalTeamList for generatePRAFITActionStatusByFunctionalTeam = ' + localFunctionalTeamList);
			
        	//preload our map with one object per each functional team found
        	Map<String,ChartDataItem> dataItemMap = new Map<String,ChartDataItem>();
            for(String functionalTeam : localFunctionalTeamList){
                ChartDataItem dataItem = new ChartDataItem();
                dataItem.label1 = functionalTeam;
                dataItem.data1 = 0;
                dataItem.data2 = 0;
                dataItem.data3 = 0;
                dataItem.data4 = 0;
                dataItem.sort1 = functionalTeam;
                dataItemMap.put(functionalTeam, dataItem);
            }
            
        	//get average of Percent Complete for ALL Actions by functional team and add the values
            List<AggregateResult> avgPercent = [SELECT Functional_Team__c, avg(Percent_Complete__c) 
                                                FROM PRAFIT_Action__c
                                                WHERE Status__c != 'Canceled - 100%' AND RecordTypeId = :recordTypeIdFilter
                                                GROUP BY Functional_Team__c
                                                ORDER BY Functional_Team__c];
            for(AggregateResult ar : avgPercent) {
                 string functionalTeam = String.valueOf(ar.get('Functional_Team__c')); 
                 ChartDataItem dataItem = dataItemMap.get(functionalTeam);
                 if(dataItem != null){
                    dataItem.data1 = Integer.valueOf(ar.get('expr0'));
                 }else{
                    system.debug('>>>>>>>>>>>>>>>>>>>>> Functional Team = \'' + functionalTeam + '\' not found in meta-data for PRAFIT_Action__c.Functional_Team__c picklist field.');
                 }
            }
        	
        	//get count of Compelte Actions by Functional Team and add the values
            List<AggregateResult> countComplete = [SELECT Functional_Team__c, count(Id) 
                                                FROM PRAFIT_Action__c
                                                WHERE Status__c like 'Complete%' AND RecordTypeId = :recordTypeIdFilter
                                                GROUP BY Functional_Team__c 
                                                ORDER BY Functional_Team__c];
            for(AggregateResult ar : countComplete) {
                 string functionalTeam = String.valueOf(ar.get('Functional_Team__c')); 
                 ChartDataItem dataItem = dataItemMap.get(functionalTeam);
                 if(dataItem != null){
                    dataItem.data2 = Integer.valueOf(ar.get('expr0'));
                 }else{
                    system.debug('>>>>>>>>>>>>>>>>>>>>> Functional Team = \'' + functionalTeam + '\' not found in meta-data for PRAFIT_Action__c.Functional_Team__c picklist field.');
                 }
            }
        	
        	//get count of In Progress Actions by Functional Team and add the values
            List<AggregateResult> countInProgress = [SELECT Functional_Team__c, count(Id) 
                                                FROM PRAFIT_Action__c
                                                WHERE Status__c like 'In Progress%' AND RecordTypeId = :recordTypeIdFilter
                                                GROUP BY Functional_Team__c 
                                                ORDER BY Functional_Team__c];
            for(AggregateResult ar : countInProgress) {
                 string functionalTeam = String.valueOf(ar.get('Functional_Team__c')); 
                 ChartDataItem dataItem = dataItemMap.get(functionalTeam);
                 if(dataItem != null){
                    dataItem.data3 = Integer.valueOf(ar.get('expr0'));
                 }else{
                    system.debug('>>>>>>>>>>>>>>>>>>>>> Functional Team = \'' + functionalTeam + '\' not found in meta-data for PRAFIT_Action__c.Functional_Team__c picklist field.');
                 }
            }
        	
        	//get count of Not Started Actions by Functional Team and add the values
            List<AggregateResult> countNotStarted = [SELECT Functional_Team__c, count(Id) 
                                                FROM PRAFIT_Action__c
                                                WHERE Status__c like 'Not Started%' AND RecordTypeId = :recordTypeIdFilter
                                                GROUP BY Functional_Team__c 
                                                ORDER BY Functional_Team__c];
            for(AggregateResult ar : countNotStarted) {
                 string functionalTeam = String.valueOf(ar.get('Functional_Team__c')); 
                 ChartDataItem dataItem = dataItemMap.get(functionalTeam);
                 if(dataItem != null){
                    dataItem.data4 = Integer.valueOf(ar.get('expr0'));
                 }else{
                    system.debug('>>>>>>>>>>>>>>>>>>>>> Functional Team = \'' + functionalTeam + '\' not found in meta-data for PRAFIT_Action__c.Functional_Team__c picklist field.');
                 }
            }
        
        	//copy the map values to a list so we can sort them before sending to the charting api
    		List<ChartDataItem> dataItemList = dataItemMap.values();
            
            //sort and return list
            dataItemList.sort();
            return dataItemList;
        }
    
    //getter for aggregate data list of PRAFIT_Actions Incomplete
        private static List<ChartDataItem> generatePRAFITActionsIncompleteByFunctionalTeam(string recordTypeIdFilter, string recordTypeIdPraRps, string recordTypeIdPraCri, list<String> functionalTeamList){
			//clean funcitonal team list for CRI or PRS Initiative based on recordTypeIdFilter
            List<String> localFunctionalTeamList = functionalTeamList.clone();			
            system.debug('localFunctionalTeamList for generatePRAFITActionsIncompleteByFunctionalTeam = ' + localFunctionalTeamList);
            for (Integer i=0; i < localFunctionalTeamList.size(); i++){
        		string dataItem = localFunctionalTeamList[i];
        		if(recordTypeIdFilter == recordTypeIdPraRps && dataItem.contains(praCriPicklistFilter)){
            		localFunctionalTeamList.remove(i);
            		i = i-1;
            	}else if(recordTypeIdFilter == recordTypeIdPraCri && !dataItem.contains(praCriPicklistFilter)){
            		localFunctionalTeamList.remove(i);
            		i = i-1;
            	}
            }
            system.debug('cleaned localFunctionalTeamList for generatePRAFITActionsIncompleteByFunctionalTeam = ' + localFunctionalTeamList);
            
        	//preload our map with one object per each functional team found
            Map<String,ChartDataItem> dataItemMap = new Map<String,ChartDataItem>();
        	for(String functionalTeam : localFunctionalTeamList){
                ChartDataItem dataItem = new ChartDataItem();
                dataItem.label1 = functionalTeam;
                dataItem.data1 = 0;
                dataItem.data2 = 0;
                dataItem.data3 = 0;
                dataItem.data4 = 0;
                dataItem.sort1 = functionalTeam;
                dataItemMap.put(functionalTeam, dataItem);
            }

        	//get count of Past Due Actions by Functional Team and add the values
            List<AggregateResult> countPastDue = [SELECT Functional_Team__c, count(Id) 
                                                FROM PRAFIT_Action__c
                                                WHERE Past_Due__c = true AND RecordTypeId = :recordTypeIdFilter
                                                GROUP BY Functional_Team__c 
                                                ORDER BY Functional_Team__c];
            for(AggregateResult ar : countPastDue) {
                 string functionalTeam = String.valueOf(ar.get('Functional_Team__c')); 
                 ChartDataItem dataItem = dataItemMap.get(functionalTeam);
                 if(dataItem != null){
                    dataItem.data4 = Integer.valueOf(ar.get('expr0'));
                 }else{
                    system.debug('>>>>>>>>>>>>>>>>>>>>> Functional Team = \'' + functionalTeam + '\' not found in meta-data for PRAFIT_Action__c.Functional_Team__c picklist field.');
                 }
            }

        	//get count of Actions Due In Next 30 Days by Functional Team and add the values
            List<AggregateResult> countDueIn30 = [SELECT Functional_Team__c, count(Id) 
                                                  FROM PRAFIT_ACTION__c
                                                  WHERE Due_In_Next_30_Days__c = true AND RecordTypeId = :recordTypeIdFilter
                                                  GROUP BY Functional_Team__c
                                                  ORDER BY Functional_Team__c];
            for(AggregateResult ar : countDueIn30) {
                 string functionalTeam = String.valueOf(ar.get('Functional_Team__c')); 
                 ChartDataItem dataItem = dataItemMap.get(functionalTeam);
                 if(dataItem != null){
                    dataItem.data3 = Integer.valueOf(ar.get('expr0'));
                 }else{
                    system.debug('>>>>>>>>>>>>>>>>>>>>> Functional Team = \'' + functionalTeam + '\' not found in meta-data for PRAFIT_Action__c.Functional_Team__c picklist field.');
                 }
            }

        	//get count of Actions Due Beyond 30 Days by Functional Team and add the values
            List<AggregateResult> countDuePast30 = [SELECT Functional_Team__c, count(Id) 
                                                    FROM PRAFIT_ACTION__c
                                                    WHERE Due_Beyond_30_Days__c = true AND RecordTypeId = :recordTypeIdFilter
                                                    GROUP BY Functional_Team__c
                                                    ORDER BY Functional_Team__c];
            for(AggregateResult ar : countDuePast30) {
                 string functionalTeam = String.valueOf(ar.get('Functional_Team__c')); 
                 ChartDataItem dataItem = dataItemMap.get(functionalTeam);
                 if(dataItem != null){
                    dataItem.data2 = Integer.valueOf(ar.get('expr0'));
                 }else{
                    system.debug('>>>>>>>>>>>>>>>>>>>>> Functional Team = \'' + functionalTeam + '\' not found in meta-data for PRAFIT_Action__c.Functional_Team__c picklist field.');
                 }
            }
        	
        	//copy the map values to a list so we can sort them before sending to the charting api
    		List<ChartDataItem> dataItemList = dataItemMap.values();
            
            //sort and return list
            dataItemList.sort();
            return dataItemList;
        }
}