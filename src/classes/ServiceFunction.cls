public with sharing class ServiceFunction extends ServiceBase {
	
	@TestVisible
	private static final String FUNCTION_WORD = 'Function';
	
    public List<Service_Function__c> serviceFunctions {
    	get{
    		return (List<Service_Function__c>) serviceRecords;
    	}
    	set{
    		serviceRecords = value;
    	}
	}
    public Service_Function__c serviceFunction {
    	get{
    		return (Service_Function__c)serviceRecord;
    	}
    	set{
    		serviceRecord = value;
    	}
    }
    public Service_Function__c serviceFunctionForUpdate {
    	get{
    		return (Service_Function__c)serviceForUpdate;
    	}
    	set{
    		serviceForUpdate = value;
    	}
    }

    public ServiceFunction(String parentServiceId) {
    	super(parentServiceId);
    }
	
	@TestVisible    
    protected override String getServiceWord(){
    	return FUNCTION_WORD;
    }
    
	public override void refreshServices(){
		serviceFunctions = ServiceModelServices.getserviceFunctionsByServiceAreaId(parentServiceId);
	}

    public override void preparationCreateOrEditService() {
        if(String.isBlank(getEditServiceId())) {
            serviceFunctionForUpdate = new Service_Function__c();
            serviceFunctionForUpdate.Service_Area__c = parentServiceId;
        } else {
            Map<Id, Service_Function__c> serviceFunctionMap = new Map<Id, Service_Function__c>(serviceFunctions);
            serviceFunctionForUpdate = serviceFunctionMap.get(getEditServiceId());
        }
        if(!String.isBlank(getCloneServiceId())) {
            serviceFunctionForUpdate = serviceFunction.clone(false, true);
            serviceFunctionForUpdate.Service_Function_Unique_ID__c= null;
        }
    }
    
}