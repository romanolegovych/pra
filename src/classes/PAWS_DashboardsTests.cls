/**
*   'PAWS_DashboardsTests' is the test class for PAWS_CrossProjectPlanningController and PAWS_CrossProjectProgressController
*   Test classes are used to write the unit test and cover the code coverage of the code.
*   @author   Work Relay
*/
@isTest 
public with sharing class PAWS_DashboardsTests 
{
	/************************* CROSS PROJECT PROGRESS CONTROLLER TESTS SECTION ***********************/
    
    static testmethod void crossProjectProgressControllerTests()
    {
    	PAWS_ApexTestsEnvironment.init();

    	STSWR1__Flow__c flow = PAWS_ApexTestsEnvironment.Flow;
		
		STSWR1.AbstractTrigger.Disabled = true;
    	STSWR1__Flow_Instance__c instance = PAWS_ApexTestsEnvironment.FlowInstance;
    	STSWR1.AbstractTrigger.Disabled = false;
    	
    	PAWS_Project_Flow_Junction__c projectFlow = PAWS_ApexTestsEnvironment.ProjectFlow;

        PAWS_CrossProjectProgressController controller = new PAWS_CrossProjectProgressController();
        System.assert(controller.ReinitFlag != null);

        controller.ProjectFilter = PAWS_ApexTestsEnvironment.Project.Name;
        controller.selectedTags = 'Test';
        controller.selectedProtocols = 'Test';
        controller.selectedCountries = 'Test';
        
        System.assert(controller.RowsSetController != null);
        System.assert(controller.Rows != null);
        controller.applyFilters();
        controller.refresh();

        controller.enableObjectSpecificProgressMode();
        controller.cancelObjectSpecificProgressMode();
        
        controller.SeletedObjectId = PAWS_ApexTestsEnvironment.Project.Id;
        System.assert(controller.SeletedObject != null);
        controller.clearFilters();
        
        PAWS_CrossProjectProgressController.loadAvailableSponsors(null);
        PAWS_CrossProjectProgressController.loadAvailableProjects(null);
		PAWS_CrossProjectProgressController.loadAvailableProtocols(null);
		PAWS_CrossProjectProgressController.loadAvailableCountries(null);
		PAWS_CrossProjectProgressController.loadAvailableTags(null);
        
        System.assert(PAWS_CrossProjectProgressController.exportData(0, null, null, null, null, null, null, null, null) != null);
    } 
    
    /************************* CROSS PROJECT PLANNING CONTROLLER TESTS SECTION ***********************/
    
    static testmethod void crossProjectPlanningControllerTests()
    {
		PAWS_ApexTestsEnvironment.init();

		STSWR1__Flow__c flow = PAWS_ApexTestsEnvironment.Flow;
		
		STSWR1.AbstractTrigger.Disabled = true;
    	STSWR1__Flow_Instance__c instance = PAWS_ApexTestsEnvironment.FlowInstance;
    	STSWR1.AbstractTrigger.Disabled = false;
    	
    	STSWR1__Flow_Step_Junction__c step = (STSWR1__Flow_Step_Junction__c)PAWS_ApexTestsEnvironment.API.call('FlowApexTestsEnvironment', 'getFlowStep', null);
    	
    	STSWR1__Gantt_Step_Property__c property = (STSWR1__Gantt_Step_Property__c)PAWS_ApexTestsEnvironment.API.call('FlowApexTestsEnvironment', 'getGanttStepProperty', null);
    	property.STSWR1__Planned_Start_Date__c = DateTime.now();
    	property.STSWR1__Planned_End_Date__c = DateTime.now();
    	property.STSWR1__Revised_Start_Date__c = DateTime.now();
    	property.STSWR1__Revised_End_Date__c = DateTime.now();
    	property.STSWR1__Revised_Comment__c = 'test';
    	property.STSWR1__Parent_Flow_Id__c = instance.STSWR1__Flow__c;
    	property.STSWR1__Flow__c = instance.STSWR1__Flow__c;
    	property.Project__c = PAWS_ApexTestsEnvironment.Project.Id;
    	property.STSWR1__Level__c = 'First';
    	
    	STSWR1.AbstractTrigger.Disabled = true;
    	update property;
    	STSWR1.AbstractTrigger.Disabled = false;
    	
    	System.debug(PAWS_Utilities.makeQuery('STSWR1__Gantt_Step_Property__c', 'Id=\'' + property.Id + '\'', null));
    	System.debug(PAWS_Utilities.makeQuery('STSWR1__Flow__c', 'Id=\'' + PAWS_ApexTestsEnvironment.Flow.Id + '\'', null));
    	System.debug(PAWS_Utilities.makeQuery('STSWR1__Flow__c', 'Id=\'' + instance.STSWR1__Flow__c + '\'', null));
    	System.debug(PAWS_Utilities.makeQuery('ecrf__c', 'Id=\'' + PAWS_ApexTestsEnvironment.Project.Id + '\'', null));
    	
        PAWS_CrossProjectPlanningController controller = new PAWS_CrossProjectPlanningController();
        System.assert(controller.ReinitFlag != null);
        
        controller.populateDashboardDefaults();
        controller.ProjectFilter = PAWS_ApexTestsEnvironment.Project.Name;
        controller.FlowFilter = PAWS_ApexTestsEnvironment.Flow.Name;
        controller.StepFilter = step.Name;
        controller.StartDateFilterForStartDate = property.STSWR1__Planned_Start_Date__c.date();
        controller.EndDateFilterForEndDate = property.STSWR1__Revised_Start_Date__c.date();
        controller.OrderBy = 'STSWR1__Step__r.STSWR1__Flow__r.Name';
        controller.OrderType = 'asc';
        controller.refresh();
        System.assert(controller.RowsSetController != null);
        System.assert(controller.Rows != null);
        
        String excludeCompletedStepsFilterURLParam = controller.GetExcludeCompletedStepsFilterURLParam();
        String excludeParentStepsFilterURLParam = controller.GetExcludeParentStepsFilterURLParam();
        String startDateFilterForStartDateURLParam = controller.GetStartDateFilterForStartDateURLParam();
        String endDateFilterForStartDateURLParam = controller.GetEndDateFilterForStartDateURLParam();
        String startDateFilterForEndDateURLParam = controller.GetStartDateFilterForEndDateURLParam();
        String endDateFilterForEndDateURLParam = controller.GetEndDateFilterForEndDateURLParam();
        
        controller.applyFilters();
        controller.OrderBy = 'STSWR1__Planned_Start_Date__c';
        controller.OrderType = 'asc';        
        controller.refresh();
        System.assert(controller.RowsSetController != null);
        System.assert(controller.Rows != null);
        System.assert(controller.DefaultDateFormat != null);

        controller.enableObjectSpecificProgressMode();
        controller.cancelObjectSpecificProgressMode();
        
        controller.SeletedObjectId = PAWS_ApexTestsEnvironment.Project.Id;
        System.assert(controller.SeletedObject != null);
        controller.clearFilters();
        
        PAWS_CrossProjectPlanningController.loadAvailableSponsors(null);
        PAWS_CrossProjectPlanningController.loadAvailableProjects(null);
		PAWS_CrossProjectPlanningController.loadAvailableFlows(null);
		PAWS_CrossProjectPlanningController.loadAvailableSteps(null);
		PAWS_CrossProjectPlanningController.loadAvailableRoles(null);
		PAWS_CrossProjectPlanningController.loadAvailableUsers(null);
		PAWS_CrossProjectPlanningController.loadAvailableProtocols(null);
		PAWS_CrossProjectPlanningController.loadAvailableCountries(null);

        
        System.assert(controller.pageController != null);
        System.assert(controller.RowsJSON != null);
        controller.enableGanttMode();
        controller.cancelGanttMode();
    } 
    
    static testmethod void crossProjectPlanExportControllerTests()
    {
		PAWS_ApexTestsEnvironment.init();

		STSWR1__Flow__c flow = PAWS_ApexTestsEnvironment.Flow;
		
		STSWR1.AbstractTrigger.Disabled = true;
    	STSWR1__Flow_Instance__c instance = PAWS_ApexTestsEnvironment.FlowInstance;
    	STSWR1.AbstractTrigger.Disabled = false;
    	
    	STSWR1__Flow_Step_Junction__c step = (STSWR1__Flow_Step_Junction__c)PAWS_ApexTestsEnvironment.API.call('FlowApexTestsEnvironment', 'getFlowStep', null);
    	
    	STSWR1__Gantt_Step_Property__c property = (STSWR1__Gantt_Step_Property__c)PAWS_ApexTestsEnvironment.API.call('FlowApexTestsEnvironment', 'getGanttStepProperty', null);
    	property.STSWR1__Planned_Start_Date__c = DateTime.now();
    	property.STSWR1__Planned_End_Date__c = DateTime.now();
    	property.STSWR1__Revised_Start_Date__c = DateTime.now();
    	property.STSWR1__Revised_End_Date__c = DateTime.now();
    	property.STSWR1__Revised_Comment__c = 'Test';
    	property.STSWR1__Parent_Flow_Id__c = instance.STSWR1__Flow__c;
    	property.STSWR1__Flow__c = instance.STSWR1__Flow__c;
    	property.Project__c = PAWS_ApexTestsEnvironment.Project.Id;
    	property.STSWR1__Level__c = 'First';
    	
    	STSWR1.AbstractTrigger.Disabled = true;
    	update property;
    	STSWR1.AbstractTrigger.Disabled = false;
    	
    	System.debug(PAWS_Utilities.makeQuery('STSWR1__Gantt_Step_Property__c', 'Id=\'' + property.Id + '\'', null));
    	System.debug(PAWS_Utilities.makeQuery('STSWR1__Flow__c', 'Id=\'' + PAWS_ApexTestsEnvironment.Flow.Id + '\'', null));
    	System.debug(PAWS_Utilities.makeQuery('STSWR1__Flow__c', 'Id=\'' + instance.STSWR1__Flow__c + '\'', null));
    	System.debug(PAWS_Utilities.makeQuery('ecrf__c', 'Id=\'' + PAWS_ApexTestsEnvironment.Project.Id + '\'', null));
    	
        System.assert(PAWS_CrossProjectPlanExportController.buildQuery(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null) != null);
        System.assert(PAWS_CrossProjectPlanExportController.loadData(new List<String>{property.Id}) != null);
    } 
}