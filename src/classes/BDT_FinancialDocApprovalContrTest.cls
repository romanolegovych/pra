/** Implements the test for the controller BDT_FinancialDocApprovalController
 * @author	Dimitrios Sgourdos
 * @version	24-Jan-2014
 */
@isTest
private class BDT_FinancialDocApprovalContrTest {
	
	// Global variables
	private static FinancialDocument__c financialDocument;
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	24-Jan-2014
	 */
	static void init(){
		// Create project
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001', 'BDT Project First Experiments');
		insert currentProject;
		
		// Create financial document
		financialDocument = new FinancialDocument__c();
		financialDocument.DocumentType__c	= 'Proposal';
		financialDocument.Client_Project__c	= currentProject.Id;
		financialDocument.DocumentOwnerName__c = UserInfo.getName();
		financialDocument.DocumentOwnerEmail__c =  UserInfo.getUserEmail();
		insert financialDocument;
		
		// Create financial document owner approval
		Employee_Details__c emp = FinancialDocumentApprovalService.getEmployeeByNameAndEmail( UserInfo.getFirstName(),
																							UserInfo.getLastName(),
																							UserInfo.getUserEmail() );
		
		FinancialDocumentApproval__c approval = FinancialDocumentApprovalService.createApprovalInstance(
														financialDocument.Id,
														emp,
														NULL,
														true,
														FinancialDocumentApprovalDataAccessor.APPROVAL_CREATOR_REASON,
														1 );
		
		FinancialDocumentService.takeDocumentOwnership(financialDocument, approval, emp);
	}
	
	/** The test is only for code coverage as in controller there are only calls to functions
	 *	from other classes that are already tested.
	 * @author	Dimitrios Sgourdos
	 * @version	24-Jan-2014
	 */
	static testMethod void myUnitTest() {
		// Create data
		init();
		
		// Create the controller without financial document id in the parameters
		BDT_FinancialDocApprovalController p = new BDT_FinancialDocApprovalController();
		
		// Create the controller with given financial document in the parameters
		PageReference pageRef = New PageReference(System.Page.BDT_FinancialDocApproval.getUrl());
		pageref.getParameters().put('financialDocumentId' , financialDocument.id);
		Test.setCurrentPage(pageRef); 
		p = new BDT_FinancialDocApprovalController();
		
		// Call functions for code coverage
		p.approveRejectComment = 'Test comment for taking ownership';
		p.takeDocumentOwnership();
		
		p.showOwnershipComments();
		p.approveRejectIndex = '0';
		p.showComments();
		p.closeComments();
		
		p.updateJobAndLocIndex = '0';
		p.updateJobAndLocation();
		
		p.takeDocumentOwnership();
		p.approveRejectComment = 'Test comment for taking ownership';
		p.takeDocumentOwnership();
		
		p.recalculateApprovals();
		p.releaseDocumentForApprovals();
		
		p.updatedApprovalIndex = '0';
		p.saveAdjustmentNotificationChange();
		
		p.resetDocumentApprovals();
		p.saveDocumentApprovals();
		
		p.addApprover();
		p.releaseDocumentForApprovals();
				
		p.deletedApprovalIndex = '1';
		p.removeApproval();
		
		p.approveRejectIndex = '0';
		p.approveDocument();
		p.rejectDocument();
		p.approveRejectComment = NULL;
		p.rejectDocument();
	}
}