/**
 * @description	Implements the test for the functions of the NCM_SrvLayer_Notification class
 * @author		Kalaiyarasan Karunanithi
 * @version		Created: 10-Sep-2015, Edited: 09-Oct-2015
 */
@isTest
private class NCM_SrvLayer_NotificationTest {
	
	/**
	 * @description	Test the function getNotificationFromWrapperData.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015
	 */
	static testMethod void getNotificationFromWrapperDataTest() {
		NotificationCatalog__c notCat = new NotificationCatalog__c(	NotificationName__c = 'Test Notification',
																	ImplementationClassName__c = 'Test Class Name');
		insert notCat;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		NotificationRegistration__c registration = new NotificationRegistration__c(NotificationCatalog__c = notCat.Id);
		insert registration;
		
		NotificationEvent__c notificationEvent = new NotificationEvent__c(NotificationCatalog__c = notCat.Id);
		insert notificationEvent;
		
		Notification__c notification = new Notification__c();
		
		NCM_API_DataTypes.NotificationWrapper wrapperItem = new NCM_API_DataTypes.NotificationWrapper();
		wrapperItem.notificationEvent = notificationEvent.Id;
		wrapperItem.notificationRegistration = registration.Id;
		DateTime sourceDateTime = DateTime.now();
		wrapperItem.reminder = sourceDateTime;
		wrapperItem.status = NCM_API_DataTypes.ACKNOWLEDGED;
		
		String errorMessage = 'Error in creating a notification record from wrapper item';
		
		// Check function with invalid parameters
		Notification__c result = NCM_SrvLayer_Notification.getNotificationFromWrapperData(NULL, NULL);
		system.assertEquals(NULL, result, errorMessage);
		
		// Check function with valid parameters
		result = NCM_SrvLayer_Notification.getNotificationFromWrapperData(wrapperItem, notification);
		
		system.assertNotEquals(NULL, result, errorMessage);
		system.assertEquals(notificationEvent.Id, result.NotificationEvent__c, errorMessage);
		system.assertEquals(registration.Id, result.NotificationRegistration__c, errorMessage);
		system.assertEquals(sourceDateTime, result.Reminder__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ACKNOWLEDGED, result.Status__c, errorMessage);
		
		wrapperItem.reminder = NULL;
		wrapperItem.status = NULL;
		result = NCM_SrvLayer_Notification.getNotificationFromWrapperData(wrapperItem, notification);
		
		system.assertNotEquals(NULL, result, errorMessage);
		system.assertEquals(notificationEvent.Id, result.NotificationEvent__c, errorMessage);
		system.assertEquals(registration.Id, result.NotificationRegistration__c, errorMessage);
		system.assertNotEquals(NULL, result.Reminder__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, result.Status__c, errorMessage);
	}
	
	
	/**
	 * @description	Test the function getPendingNotificationsByUser.
	 * @author		Kalaiyarasan Karunanithi, Dimitrios Sgourdos
	 * @date		Created: 10-Sep-2015, Edited: 09-Oct-2015
	*/
	static testMethod void getPendingNotificationsByUserTest() {
		// Create data
		NotificationCatalog__c catalog = new NotificationCatalog__c(
																NotificationName__c = 'Topic A',
																ImplementationClassName__c = 'Class A',
																Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		insert catalog;
		
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'C', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		List<User> userList = NCM_Utils.buildTestUserList(2);
		
		List<NotificationRegistration__c> source = new List<NotificationRegistration__c>();
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = NULL,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[2].Id,
													Active__c = false) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		insert source;
		
		NotificationEvent__c dummyEvent = new NotificationEvent__c(NotificationCatalog__c = catalog.Id);
		insert dummyEvent;
		
		List<Notification__c> notificationList = new List<Notification__c>();
		notificationList.add(new Notification__c(	NotificationEvent__c = dummyEvent.Id,
													NotificationRegistration__c = source[0].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 1));
		notificationList.add(new Notification__c(	NotificationEvent__c = dummyEvent.Id,
													NotificationRegistration__c = source[1].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 2));
		notificationList.add(new Notification__c(	NotificationEvent__c = dummyEvent.Id,
													NotificationRegistration__c = source[2].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 3));
		notificationList.add(new Notification__c(	NotificationEvent__c = dummyEvent.Id,
													NotificationRegistration__c = source[3].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 4));
		notificationList.add(new Notification__c(	NotificationEvent__c = dummyEvent.Id,
													NotificationRegistration__c = source[4].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 5));
		insert notificationList;
		
		// Check the function
		List<NCM_API_DataTypes.NotificationWrapper> results =  NCM_SrvLayer_Notification.getPendingNotificationsByUser(
																										userList[0].Id);
		
		String errorMessage = 'Error in retrieving pending notifications by user';
		
		system.assertEquals(3, results.size(), errorMessage);
		
		Map<Id, NCM_API_DataTypes.NotificationWrapper> resultsMap = new Map<Id, NCM_API_DataTypes.NotificationWrapper>();
		for(NCM_API_DataTypes.NotificationWrapper tmpWrapperItem : results) {
			resultsMap.put(tmpWrapperItem.recordId, tmpWrapperItem);
		}
		
		NCM_API_DataTypes.NotificationWrapper tmpItem = resultsMap.remove(notificationList[0].Id);
		system.assertEquals(notificationList[0].Id, tmpItem.recordId, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, tmpItem.Status, errorMessage);
		system.assertEquals(notificationList[0].Reminder__c, tmpItem.Reminder, errorMessage);
		system.assertNotEquals(NULL, tmpItem.recordCreatedDate, errorMessage);
		system.assertEquals(accList[0].Id, tmpItem.relatedToId, errorMessage);
		system.assertEquals('Topic A', tmpItem.parentTopicData.notificationName, errorMessage);
		system.assertEquals(catalog.Id, tmpItem.parentEventData.notificationCatalogId, errorMessage);
		
		tmpItem = resultsMap.remove(notificationList[1].Id);
		system.assertEquals(notificationList[1].Id, tmpItem.recordId, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, tmpItem.Status, errorMessage);
		system.assertEquals(notificationList[1].Reminder__c, tmpItem.Reminder, errorMessage);
		system.assertNotEquals(NULL, tmpItem.recordCreatedDate, errorMessage);
		system.assertEquals(accList[1].Id, tmpItem.relatedToId, errorMessage);
		system.assertEquals('Topic A', tmpItem.parentTopicData.notificationName, errorMessage);
		system.assertEquals(catalog.Id, tmpItem.parentEventData.notificationCatalogId, errorMessage);
		
		tmpItem = resultsMap.remove(notificationList[2].Id);
		system.assertEquals(notificationList[2].Id, tmpItem.recordId, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, tmpItem.Status, errorMessage);
		system.assertEquals(notificationList[2].Reminder__c, tmpItem.Reminder, errorMessage);
		system.assertNotEquals(NULL, tmpItem.recordCreatedDate, errorMessage);
		system.assertEquals(NULL, tmpItem.relatedToId, errorMessage);
		system.assertEquals('Topic A', tmpItem.parentTopicData.notificationName, errorMessage);
		system.assertEquals(catalog.Id, tmpItem.parentEventData.notificationCatalogId, errorMessage);
	}
	
	
	/**
	 * @description	Test the function getPendingNotificationsByUserAndRelatedObject.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 11-Sep-2015, Edited: 09-Oct-2015
	*/
	static testMethod void getPendingNotificationsByUserAndRelatedObjectTest() {
		// Create data
		List<NotificationCatalog__c> catalogList = new List<NotificationCatalog__c>();
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic A',
													ImplementationClassName__c = 'Class A',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic B',
													ImplementationClassName__c = 'Class B',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic C',
													ImplementationClassName__c = 'Class C',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic D',
													ImplementationClassName__c = 'Class D',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		insert catalogList;
		
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		List<User> userList = NCM_Utils.buildTestUserList(2);
		
		List<NotificationRegistration__c> source = new List<NotificationRegistration__c>();
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = false) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[2].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[2].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[3].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[2].Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true) );
		insert source;
		
		NotificationEvent__c dummyEvent = new NotificationEvent__c(NotificationCatalog__c = catalogList[0].Id);
		insert dummyEvent;
		
		List<Notification__c> notificationList = new List<Notification__c>();
		notificationList.add(new Notification__c(	NotificationEvent__c = dummyEvent.Id,
													NotificationRegistration__c = source[0].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 1));
		notificationList.add(new Notification__c(	NotificationEvent__c = dummyEvent.Id,
													NotificationRegistration__c = source[1].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 2));
		notificationList.add(new Notification__c(	NotificationEvent__c = dummyEvent.Id,
													NotificationRegistration__c = source[2].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 3));
		notificationList.add(new Notification__c(	NotificationEvent__c = dummyEvent.Id,
													NotificationRegistration__c = source[3].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 4));
		notificationList.add(new Notification__c(	NotificationEvent__c = dummyEvent.Id,
													NotificationRegistration__c = source[4].Id,
													Status__c = NCM_API_DataTypes.ACKNOWLEDGED,
													Reminder__c = Date.today() + 5));
		notificationList.add(new Notification__c(	NotificationEvent__c = dummyEvent.Id,
													NotificationRegistration__c = source[5].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 6));
		notificationList.add(new Notification__c(	NotificationEvent__c = dummyEvent.Id,
													NotificationRegistration__c = source[6].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 7));
		insert notificationList;
		
		String errorMessage = 'Error in retrieving pending notifications by user and related to Id';
		
		// Check the function with normal related to id
		List<NCM_API_DataTypes.NotificationWrapper> results =
												NCM_SrvLayer_Notification.getPendingNotificationsByUserAndRelatedObject(
																										userList[0].Id,
																										accList[0].Id);
		
		system.assertEquals(2, results.size(), errorMessage);
		
		Map<Id, NCM_API_DataTypes.NotificationWrapper> resultsMap = new Map<Id, NCM_API_DataTypes.NotificationWrapper>();
		for(NCM_API_DataTypes.NotificationWrapper tmpWrapperItem : results) {
			resultsMap.put(tmpWrapperItem.recordId, tmpWrapperItem);
		}
		
		NCM_API_DataTypes.NotificationWrapper tmpItem = resultsMap.remove(notificationList[0].Id);
		system.assertEquals(notificationList[0].Id, tmpItem.recordId, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, tmpItem.Status, errorMessage);
		system.assertEquals(notificationList[0].Reminder__c, tmpItem.Reminder, errorMessage);
		system.assertNotEquals(NULL, tmpItem.recordCreatedDate, errorMessage);
		system.assertEquals(accList[0].Id, tmpItem.relatedToId, errorMessage);
		system.assertEquals('Topic A', tmpItem.parentTopicData.notificationName, errorMessage);
		system.assertEquals(catalogList[0].Id, tmpItem.parentEventData.notificationCatalogId, errorMessage);
		
		tmpItem = resultsMap.remove(notificationList[2].Id);
		system.assertEquals(notificationList[2].Id, tmpItem.recordId, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, tmpItem.Status, errorMessage);
		system.assertEquals(notificationList[2].Reminder__c, tmpItem.Reminder, errorMessage);
		system.assertNotEquals(NULL, tmpItem.recordCreatedDate, errorMessage);
		system.assertEquals(accList[0].Id, tmpItem.relatedToId, errorMessage);
		system.assertEquals('Topic C', tmpItem.parentTopicData.notificationName, errorMessage);
		system.assertEquals(catalogList[0].Id, tmpItem.parentEventData.notificationCatalogId, errorMessage);
		
		// Check the function with NULL related to id
		source[0].RelatedToId__c = NULL;
		source[1].RelatedToId__c = NULL;
		source[2].RelatedToId__c = NULL;
		source[4].RelatedToId__c = NULL;
		source[5].RelatedToId__c = NULL;
		update source;
		
		results = NCM_SrvLayer_Notification.getPendingNotificationsByUserAndRelatedObject(userList[0].Id, NULL);
		
		system.assertEquals(2, results.size(), errorMessage);
		
		resultsMap = new Map<Id, NCM_API_DataTypes.NotificationWrapper>();
		for(NCM_API_DataTypes.NotificationWrapper tmpWrapperItem : results) {
			resultsMap.put(tmpWrapperItem.recordId, tmpWrapperItem);
		}
		
		tmpItem = resultsMap.remove(notificationList[0].Id);
		system.assertEquals(notificationList[0].Id, tmpItem.recordId, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, tmpItem.Status, errorMessage);
		system.assertEquals(notificationList[0].Reminder__c, tmpItem.Reminder, errorMessage);
		system.assertNotEquals(NULL, tmpItem.recordCreatedDate, errorMessage);
		system.assertEquals(NULL, tmpItem.relatedToId, errorMessage);
		system.assertEquals('Topic A', tmpItem.parentTopicData.notificationName, errorMessage);
		system.assertEquals(catalogList[0].Id, tmpItem.parentEventData.notificationCatalogId, errorMessage);
		
		tmpItem = resultsMap.remove(notificationList[2].Id);
		system.assertEquals(notificationList[2].Id, tmpItem.recordId, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, tmpItem.Status, errorMessage);
		system.assertEquals(notificationList[2].Reminder__c, tmpItem.Reminder, errorMessage);
		system.assertNotEquals(NULL, tmpItem.recordCreatedDate, errorMessage);
		system.assertEquals(NULL, tmpItem.relatedToId, errorMessage);
		system.assertEquals('Topic C', tmpItem.parentTopicData.notificationName, errorMessage);
		system.assertEquals(catalogList[0].Id, tmpItem.parentEventData.notificationCatalogId, errorMessage);
	}
	
	
	/**
	 * @description	Test the function getNumberOfPendingNotificationsPerRelatedObjectByUser.
	 * @author		Kalaiyarasan Karunanithi, Dimitrios Sgourdos
	 * @date		Created: 11-Sep-2015
	*/
	static testMethod void getNumberOfPendingNotificationsPerRelatedObjectByUserTest() {
		// Create data
		List<NotificationCatalog__c> catalogList = new List<NotificationCatalog__c>();
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic A',
													ImplementationClassName__c = 'Class A',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic B',
													ImplementationClassName__c = 'Class B',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic C',
													ImplementationClassName__c = 'Class C',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		insert catalogList;
		
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		List<User> userList = NCM_Utils.buildTestUserList(2);
		
		List<NotificationRegistration__c> source = new List<NotificationRegistration__c>();
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = false) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[2].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = NULL,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = NULL,
													Active__c = false) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[2].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = NULL,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[2].Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true) );
		insert source;
		
		List<Notification__c> notificationList = new List<Notification__c>();
		notificationList.add(new Notification__c(	NotificationRegistration__c = source[0].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 1));
		notificationList.add(new Notification__c(	NotificationRegistration__c = source[1].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 2));
		notificationList.add(new Notification__c(	NotificationRegistration__c = source[2].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 3));
		notificationList.add(new Notification__c(	NotificationRegistration__c = source[3].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 4));
		notificationList.add(new Notification__c(	NotificationRegistration__c = source[4].Id,
													Status__c = NCM_API_DataTypes.ACKNOWLEDGED,
													Reminder__c = Date.today() + 5));
		notificationList.add(new Notification__c(	NotificationRegistration__c = source[5].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 6));
		notificationList.add(new Notification__c(	NotificationRegistration__c = source[6].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 6));
		notificationList.add(new Notification__c(	NotificationRegistration__c = source[7].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 6));
		notificationList.add(new Notification__c(	NotificationRegistration__c = source[8].Id,
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 6));
		insert notificationList;
		
		// Check the function
		Map<String, Integer> results = NCM_SrvLayer_Notification.getNumberOfPendingNotificationsPerRelatedObjectByUser(
																										userList[0].Id);
		
		String errorMessage = 'Error in get the number of pending notifications per related 0bject';
		
		system.assertEquals(3, results.size(), errorMessage);
		
		Integer numberofPending = results.get(accList[0].Id);
		system.assertEquals(1, numberofPending, errorMessage);
		numberofPending = results.get(accList[1].Id);
		system.assertEquals(2, numberofPending, errorMessage);
		numberofPending = results.get(NULL);
		system.assertEquals(2, numberofPending, errorMessage);
	}
	
	
	/**
	 * @description	Test the function acknowledgePendingNotifications.
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 10-Sep-2015, Edited: 09-Oct-2015
	*/
	static testMethod void acknowledgePendingNotificationsTest() {
		// Create data
		NotificationCatalog__c catalog = new NotificationCatalog__c(NotificationName__c = 'Topic A',
																ImplementationClassName__c = 'Class A',
																Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		insert catalog;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'C', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		List<NotificationRegistration__c> regList = new List<NotificationRegistration__c>();
		regList.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = testUser.Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		regList.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = testUser.Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true) );
		regList.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = testUser.Id,
													RelatedToId__c = accList[2].Id,
													Active__c = true) );
		insert regList;
		
		List<Notification__c> notificationList = new List<Notification__c>();
		notificationList.add(new Notification__c(NotificationRegistration__c = regList[0].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 1));
		notificationList.add(new Notification__c(NotificationRegistration__c = regList[1].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 2));
		notificationList.add(new Notification__c(NotificationRegistration__c = regList[2].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 3));
		insert notificationList;
		
		String errorMessage = 'Error in acknowledging notifications';
		
		// Check the function with invalid parameters
		NCM_SrvLayer_Notification.acknowledgePendingNotifications(NULL);
		
		List<Notification__c> results = [SELECT ID,
												Status__c
										FROM	Notification__c
										ORDER BY Reminder__c
										LIMIT	10];
		
		system.assertEquals(3, results.size(), errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, results[0].Status__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, results[1].Status__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, results[2].Status__c, errorMessage);
		
		// Check the function with valid parameters
		List<NCM_API_DataTypes.NotificationWrapper> source = new List<NCM_API_DataTypes.NotificationWrapper>();
		source.add( new NCM_API_DataTypes.NotificationWrapper(notificationList[0], true, true) );
		source.add( new NCM_API_DataTypes.NotificationWrapper(notificationList[1], true, true) );
		
		NCM_SrvLayer_Notification.acknowledgePendingNotifications(source);
		
		results = 	[SELECT ID,
							Status__c
					FROM	Notification__c
					ORDER BY Reminder__c
					LIMIT	10];
		
		system.assertEquals(3, results.size(), errorMessage);
		system.assertEquals(notificationList[0].Id, results[0].Id, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ACKNOWLEDGED, results[0].Status__c, errorMessage);
		system.assertEquals(notificationList[1].Id, results[1].Id, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ACKNOWLEDGED, results[1].Status__c, errorMessage);
		system.assertEquals(notificationList[2].Id, results[2].Id, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, results[2].Status__c, errorMessage);
	}
	
	
	/**
	 * @description	Test the function createNotificationInstanceFromRegistrationAndEvent.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 28-Sep-2015
	*/
	static testMethod void createNotificationInstanceFromRegistrationAndEventTest() {
		// Create data
		List<NotificationCatalog__c> catalogList = new List<NotificationCatalog__c>();
		catalogList.add( new NotificationCatalog__c(	NotificationName__c = 'Topic A',
												Acknowledgementrequired__c = true,
												ImplementationClassName__c = 'Test Class Name'));
		catalogList.add( new NotificationCatalog__c(	NotificationName__c = 'Topic B',
												Acknowledgementrequired__c = false,
												ImplementationClassName__c = 'Test Class Name'));
		insert catalogList;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		List<NotificationRegistration__c> registrationList = new List<NotificationRegistration__c>();
		registrationList.add( new NotificationRegistration__c(	Name = 'A',
																NotificationCatalog__c = catalogList[0].Id,
																UserId__c = testUser.Id,
																RelatedToId__c = acc.Id,
																ReminderPeriod__c = 10));
		registrationList.add( new NotificationRegistration__c(	Name = 'B',
																NotificationCatalog__c = catalogList[1].Id,
																UserId__c = testUser.Id,
																RelatedToId__c = acc.Id,
																ReminderPeriod__c = 20));
		insert registrationList;
		
		List<NotificationEvent__c> eventList = new List<NotificationEvent__c>();
		eventList.add( new NotificationEvent__c(NotificationCatalog__c = catalogList[0].Id, RelatedToId__c = acc.Id) );
		eventList.add( new NotificationEvent__c(NotificationCatalog__c = catalogList[1].Id, RelatedToId__c = acc.Id) );
		insert eventList;
		
		List<NotificationRegistration__c> source = [SELECT	Id,
															RelatedToId__c,
															ReminderPeriod__c,
															NotificationCatalog__c,
															NotificationCatalog__r.AcknowledgementRequired__c
													FROM	NotificationRegistration__c
													ORDER BY Name
													LIMIT	50000];
		
		String errorMessage = 'Error in creating a notification instance from given registration and event';
		
		// Check the function
		Notification__c result = NCM_SrvLayer_Notification.createNotificationInstanceFromRegistrationAndEvent(
																									source[0],
																									eventList[0].Id);
		system.assertEquals(eventList[0].Id, result.NotificationEvent__c, errorMessage);
		system.assertEquals(source[0].Id, result.NotificationRegistration__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, result.Status__c, errorMessage);
		system.assertNotEquals(NULL, result.Reminder__c, errorMessage);
		
		result = NCM_SrvLayer_Notification.createNotificationInstanceFromRegistrationAndEvent(	source[1],
																								eventList[1].Id);
		system.assertEquals(eventList[1].Id, result.NotificationEvent__c, errorMessage);
		system.assertEquals(source[1].Id, result.NotificationRegistration__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ACKNOWLEDGED, result.Status__c, errorMessage);
		system.assertNotEquals(NULL, result.Reminder__c, errorMessage);
	}
}