public with sharing class FlowchartDataAccessor {

	/** Object definition for fields used in application for Flowcharts__c
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('Flowcharts__c');		
	}
	
	
	/** Object definition for fields used in application for Flowcharts__c with the parameter referenceName as a prefix
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'ClinicalDesign__c,';
		result += referenceName + 'listOfTimeMinutes__c,';
		result += referenceName + 'Description__c,';
		result += referenceName + 'Flowchart_Number__c';
		return result;
	}
	
	/** Retrieve list of arms.
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @param	whereClause			The criteria that Laboratory Selection Lists must meet
	 * @return	List of arms
	 */
	public static List<Flowcharts__c> getFlowchartList(String whereClause) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM Flowcharts__c ' +
								'WHERE  {1} ' +
								'ORDER BY Flowchart_Number__c',
								new List<String> {
									getSObjectFieldString(),
									whereClause
								}
							);
		return (List<Flowcharts__c>) Database.query(query);
	}

}