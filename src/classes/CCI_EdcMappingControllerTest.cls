/**
 * Unit test for EDC Mapping Controller
 */
@isTest
private class CCI_EdcMappingControllerTest {
	
	static list<MuleServicesCS__c> settings;
	
	private static void init() {
		settings = new MuleServicesCS__c[]{
			new MuleServicesCS__c(Name = 'ENDPOINT', Value__c = 'ENDPOINT'),
			new MuleServicesCS__c(Name = 'TIMEOUT', Value__c = '60000'),
			new MuleServicesCS__c(Name = 'MdmProtocolService', Value__c = 'MdmProtocolService'),
			new MuleServicesCS__c(Name = 'MdmMappingService', Value__c = 'MdmMappingService')
		};
		insert settings;
	}

	static testMethod void testConstructor() {
		CCI_EdcMappingController con = new CCI_EdcMappingController();
		system.assert(con.instanceVOMap.containsKey(null));
	}
	
	static testMethod void testSearch() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_EdcMappingController con = new CCI_EdcMappingController();
		con.projId = '0';
		con.instName = 'value';
		con.protNum = 'value';
		con.studyId = 'value';
		con.search();
		con.getProtocolVOs();
		system.assert(con.protocolVOs.size() == 5);
		Test.stopTest();
	}
	
	static testMethod void testGetInstances() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_EdcMappingController con = new CCI_EdcMappingController();
		list<selectoption> options = con.getInstances();
		system.assert(options.size() == 4);
		Test.stopTest();
	}
	
	static testMethod void testSearchProtocol() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(false));
		CCI_EdcMappingController con = new CCI_EdcMappingController();
		con.saveProtNum = 'MOCK PROTOCOL';
		con.searchProtocol();
		system.assert(con.protocolVO != null && con.protocolVO.clientProtocolNum.equals('clientprot-1'));
		Test.stopTest();
	}
	
	static testMethod void testGetClientProjectListFromProtocol() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(false));
		CCI_EdcMappingController con = new CCI_EdcMappingController();
		con.saveProtNum = 'MOCK PROTOCOL';
		con.searchProtocol();
		list<selectoption> options = con.getClientProjectsFromProtocol();
		system.assert(options != null && options.size() == 6);
		Test.stopTest();
	}
	
	static testMethod void testFindProtocolSiteVOFromMap() {
		String key = 'key';
		CCI_EdcMappingController con = new CCI_EdcMappingController();
		con.protSiteVOMap = new Map<String, MdmMappingService.protocolSiteVO>();
		MdmMappingService.protocolSiteVO protocolSiteVO = new MdmMappingService.protocolSiteVO();
		protocolSiteVO.instanceName = 'test name;';
		protocolSiteVO.praStudyId = 'test pra id';
		protocolSiteVO.protocolNo = 'test prot number';
		protocolSiteVO.protProjId = 0L;
		protocolSiteVO.siteId = 0L;
		protocolSiteVO.source = 'test source';
		con.protSiteVOMap.put(key, protocolSiteVO);
		con.selectedProtSiteId = 'key';
		con.findProtSiteVO();
		system.assert(con.selectedProtSiteVO.equals(protocolSiteVO));
	}
	
	static testMethod void testAddEdcMappingToMDM() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_EdcMappingController con = new CCI_EdcMappingController();
		con.saveProtNum = 'MOCK PROTOCOL';
		con.saveInstName = 'MOCK INSTANCE NAME';
		con.saveStudyId = 'MOCK PRA ID';
		con.Add();
		Test.stopTest();
	}
	
	static testMethod void testSaveEdcMappingToMDM() {
		String key = 'key';
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_EdcMappingController con = new CCI_EdcMappingController();
		con.protSiteVOMap = new Map<String, MdmMappingService.protocolSiteVO>();
		MdmMappingService.protocolSiteVO protocolSiteVO = new MdmMappingService.protocolSiteVO();
		protocolSiteVO.instanceName = 'test name;';
		protocolSiteVO.praStudyId = 'test pra id';
		protocolSiteVO.protocolNo = 'test prot number';
		protocolSiteVO.protProjId = 0L;
		protocolSiteVO.siteId = 0L;
		protocolSiteVO.source = 'test source';
		con.protSiteVOMap.put(key, protocolSiteVO);
		con.selectedProtSiteId = 'key';
		con.findProtSiteVO();
		con.save();
		Test.stopTest();
	}
	
	static testMethod void testDeleteEdcMappingFromMDM() {
		String key = 'key';
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new MdmMappingServiceTestMocks(false));
		CCI_EdcMappingController con = new CCI_EdcMappingController();
		con.protSiteVOMap = new Map<String, MdmMappingService.protocolSiteVO>();
		MdmMappingService.protocolSiteVO protocolSiteVO = new MdmMappingService.protocolSiteVO();
		protocolSiteVO.instanceName = 'test name;';
		protocolSiteVO.praStudyId = 'test pra id';
		protocolSiteVO.protocolNo = 'test prot number';
		protocolSiteVO.protProjId = 0L;
		protocolSiteVO.siteId = 0L;
		protocolSiteVO.source = 'test source';
		con.protSiteVOMap.put(key, protocolSiteVO);
		con.selectedProtSiteId = 'key';
		con.findProtSiteVO();
		con.deleteMapping();
		Test.stopTest();
	}
}