public with sharing class BDT_NewEditPopulation {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;}
	public Population__c population {get;set;}
	public String projectId 		{get;set;}
	public String customId			{get;set;}
	public String populationId 		{get;set;}
	public String smokingPreference = null;
	public List<Criteria__c> exclusiveCriteriaList	{get;set;}
	public List<Criteria__c> inclusiveCriteriaList	{get;set;}
	public List<Criteria__c> softDeletedCriteriaList{get;set;}
	public List<Criteria__c> allCriteriaList		{get;set;}
	public Boolean showDeleted{
		get {if (showDeleted == null) {
				showDeleted = false;
			}
			return showDeleted;
		}
		set;
	}
	public BDT_NewEditPopulation(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Population');
		projectId = system.currentPageReference().getParameters().get('ProjectId');
		populationId = system.currentPageReference().getParameters().get('PopulationId');
		
		softDeletedCriteriaList = new List<Criteria__c>(); 
				
		if(populationId!=null && populationId!=''){
			try{
				population = [Select p.Smoking__c, p.Smoking_Max__c, p.Population_Code__c, p.ScreeningEfficiency__c, 
									p.Name, p.Male__c, p.IsDeleted, p.Id, p.Female__c, p.BloodPS_Min__c, 
									p.BloodPS_Max__c, p.BloodPD_Min__c, p.BloodPD_Max__c, p.BMI_Min__c, p.BMI_Max__c, 
									p.Age_Min__c, p.Age_Max__c, p.Client_Project__c, p.BDTDeleted__c 
									From Population__c p 
									where id =: populationId 
									and ((BDTDeleted__c  = :showDeleted) or (BDTDeleted__c  = false))
									LIMIT 1];
				
				if(population.Smoking__c == true){
					smokingPreference = 'Yes';
				}else{
					smokingPreference = 'No';
				}
				//retrieve exclusive and inclusive criteria
				
			}catch(Exception e){
				population = new Population__c();
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Population Id could not be found. ');
	        	ApexPages.addMessage(msg); 
			}	
		}else{
			population = new Population__c(Client_Project__c=projectId);
		}
		
		getPopulationCriteria();
	}
	
	public String getSmokingPreference() {
		return smokingPreference;
	} 
	
	public void setSmokingPreference(String smokingPreference) { this.smokingPreference = smokingPreference; }
	
	public void deleteCriteria(){
		String deleteCriteriaId =  System.CurrentPageReference().getParameters().get('deleteCriteriaId');
		for(Integer i=0; i<exclusiveCriteriaList.size();i++){
			if(exclusiveCriteriaList[i].id == deleteCriteriaId){
				softDeletedCriteriaList.add(exclusiveCriteriaList[i]);
				exclusiveCriteriaList.remove(i);
			}
		}
		for(Integer i=0; i<inclusiveCriteriaList.size();i++){
			if(inclusiveCriteriaList[i].id == deleteCriteriaId){
				softDeletedCriteriaList.add(inclusiveCriteriaList[i]);
				inclusiveCriteriaList.remove(i);
			}
		}			
	}
		
	public void getPopulationCriteria(){
		allCriteriaList = new List<Criteria__c>();
		exclusiveCriteriaList = new List<Criteria__c>();
		inclusiveCriteriaList = new List<Criteria__c>();
		
		try{
			allCriteriaList = [Select c.BDTDeleted__c, c.Populations__c, c.Name, c.Inclusive__c, c.Id, c.Exclusive__c, c.CriteriaDescription__c 
											From Criteria__c c 
											where Populations__c = :population.id
											and ((BDTDeleted__c  = :showDeleted) or (BDTDeleted__c  = false))];
		}catch(Exception e){
			system.debug('There are not Criterias assigned to Population with ID: '+ population.id);
		}
		for(Criteria__c cr: allCriteriaList){
			if(cr.Inclusive__c == true){
				inclusiveCriteriaList.add(cr);
			}else if (cr.Exclusive__c == true){
				exclusiveCriteriaList.add(cr);
			}
		}
		
		system.debug('exclusiveCriteriaList.size(): '+ exclusiveCriteriaList.size());
		if(exclusiveCriteriaList.isEmpty()){
			addExclusive();
		}
		
		if(inclusiveCriteriaList.isEmpty()){
			addInclusive();
		}
	}
	
	public void addExclusive(){
		exclusiveCriteriaList.add(new Criteria__c(CriteriaDescription__c='', Populations__c=population.id, Exclusive__c = true)); 
	}
	
	public void addInclusive(){
		inclusiveCriteriaList.add(new Criteria__c(CriteriaDescription__c='', Populations__c=population.id, Inclusive__c = true)); 
	}
	
	public List<SelectOption> getSmokingPref() {
		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('No','No')); 
		options.add(new SelectOption('Yes','Yes'));
		return options;
	}
	
	public PageReference save(){
				
		if(smokingPreference == 'Yes'){
			population.Smoking__c = true;
		}else{
			population.Smoking__c = false;
		}
		
		if(!softDeletedCriteriaList.isEmpty()){
			criteriaSoftDelete(softDeletedCriteriaList);
		}
		upsert population;
			
		saveCriteria();
		
		return BDT_Characteristics.returnToCharacteristics();
	}
	
	public void criteriaSoftDelete(List<Criteria__c> softDeletedCriteriaList){
		for(Criteria__c cr: softDeletedCriteriaList){
			cr.BDTDeleted__c = true;
		}
		update softDeletedCriteriaList;
	}
	
	public void saveCriteria(){		
		List<Criteria__c> saveList = new List<Criteria__c>();
		for(Criteria__c tmp : exclusiveCriteriaList) {
			if(tmp.CriteriaDescription__c!=null && tmp.CriteriaDescription__c!=''){
				tmp.Populations__c = population.id;
				saveList.add(tmp);
			}
		}
		for(Criteria__c tmp : inclusiveCriteriaList) {
			if(tmp.CriteriaDescription__c!=null && tmp.CriteriaDescription__c!=''){
				tmp.Populations__c = population.id;
				saveList.add(tmp);
			}
		}
		upsert saveList;
	}
	
	public PageReference cancel(){
		return BDT_Characteristics.returnToCharacteristics();
	}
	
	public PageReference deleteSoft(){	
		population.BDTDeleted__c = true;
		criteriaSoftDelete(allCriteriaList);		
		update population;
		return BDT_Characteristics.returnToCharacteristics();
	}
	
}