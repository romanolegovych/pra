/** Implements the Service Layer of the object StudyMileStone__c
 * @author	Dimitrios Sgourdos
 * @version	28-Feb-2014
 */
public with sharing class BDT_StudyMileStoneService {
	
	/** Retrieve the StudyMileStones that belong to the given study.
	 * @author	Dimitrios Sgourdos
	 * @version 26-Feb-2014
	 * @param	studyId				The id of the Study
	 * @return	The retrieved StudyMileStones.
	 */
	public static List<StudyMileStone__c> getStudyMileStonesByStudy(String studyId) {
		String whereClause = 'Study__c = \'' + studyId + '\'';
		
		return BDT_StudyMileStoneDataAccessor.getStudyMileStones(whereClause, 'DueDate__c');
	}
	
	
	/** Retrieve the StudyMileStone with the given id.
	 * @author	Dimitrios Sgourdos
	 * @version 27-Feb-2014
	 * @param	studyMilestoneId	The id of the StudyMileStone
	 * @return	The retrieved StudyMileStone.
	 */
	public static StudyMileStone__c getStudyMileStoneById(String studyMilestoneId) {
		String whereClause = 'Id = \'' + studyMilestoneId + '\'';
		List<StudyMileStone__c> sourceList = BDT_StudyMileStoneDataAccessor.getStudyMileStones(whereClause, NULL);
		
		return ( (sourceList.isEmpty())? NULL : sourceList[0] );
	}
	
	
	/** Retrieve the StudyMileStones that belong to the given studies.
	 * @author	Dimitrios Sgourdos
	 * @version 28-Feb-2014
	 * @param	studiesId				The ids of the Studies
	 * @return	The retrieved StudyMileStones.
	 */
	public static List<StudyMileStone__c> getStudyMileStonesByStudiesId(List<String> studiesId) {
		List<StudyMileStone__c> results = new List<StudyMileStone__c>();
		String studiesIdsQuery = BDT_Utils.buildSetForDynamicQueries(studiesId);
		
		if(String.isNotBlank(studiesIdsQuery)) {
			String whereClause = 'Study__c IN ' + studiesIdsQuery;
			results = BDT_StudyMileStoneDataAccessor.getStudyMileStones(whereClause, 'DueDate__c');
		}
		
		return results;
	}
	
	
	/** Create a SelectOption list with the StudyMileStones that belong to the given studies.
	 * @author	Dimitrios Sgourdos
	 * @version 28-Feb-2014
	 * @param	studiesId				The ids of the Studies
	 * @return	The created SelectOption List with the StudyMileStones.
	 */
	public static List<SelectOption> getMileStonesOptionsForPaymentTerms(List<String> studiesId) {
		// retrieve the StudyMileStones and filter them to keep the uniques ones
		List<StudyMileStone__c> source = getStudyMileStonesByStudiesId(studiesId);
		
		Set<String> definitionsIds = new Set<String>();
		List<StudyMileStone__c> stMlList = new List<StudyMileStone__c>();
		
		for(StudyMileStone__c tmpItem : source) {
			if(tmpItem.MileStoneDefinition__c == NULL) {
				stMlList.add(tmpItem);
			} else if( ! definitionsIds.contains(tmpItem.MileStoneDefinition__c) ) {
				definitionsIds.add(tmpItem.MileStoneDefinition__c);
				stMlList.add(tmpItem);
			}
		}
		
		return BDT_Utils.getSelectOptionFromSObjectList(stMlList,'Name','Name','Please Select...',false);
	}
}