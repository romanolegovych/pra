@istest private class CC_CloneTest
{
	@istest private static void testCloneChains()
	{
		Map<String, SObject> sobjectmap = CCApiTest.setup();
		new CC_Clone().execute(new Map<String, String>{sobjectMap.get('Flow').Id => sobjectMap.get('Flow').Id});
		
		System.assert([select count() from Critical_Chain__c] == 2);
	}
}