/**
* @author Kostya Hladkyi
* @date 04/11/2015
* @description Controller for CV_Help_CreateEditTopic page
*/
public with sharing class CV_Help_CreateEditTopicCtrl {
    
    public Help_Record__c record { get; private set; }
    
    public List<String> selectedProfiles{
        get{
            return CV_Utils.getValueAsList(record.User_Profiles__c);
        }
        set{
            record.User_Profiles__c = CV_Utils.getJoinedValue(value);
        }
    }
    
    public List<String> selectedSObjects{
        get{
            return CV_Utils.getValueAsList(record.Related_Objects__c);
        }
        set{
            record.Related_Objects__c = CV_Utils.getJoinedValue(value);
        }
    }
    
    public List<String> selectedRecordTypes{
        get{
            return CV_Utils.getValueAsList(record.Record_Types__c);
        }
        set{
            record.Record_Types__c = CV_Utils.getJoinedValue(value);
        }
    }
    
    public List<String> selectedPages{
        get{
            return CV_Utils.getValueAsList(record.Related_Pages__c);
        }
        set{
            System.debug('--- selectedPages: ' + selectedPages);
            record.Related_Pages__c = CV_Utils.getJoinedValue(value);
        }
    }
    
    public List<SelectOption> getSelectedPagesAsOptions(){
        try{
            List<SelectOption> pages = new List<SelectOption>();
            for (String pageName : CV_Utils.getValueAsList(record.Related_Pages__c) ){
                pages.add( new SelectOption(pageName, pageName) );
            }
            return pages;
        }catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
    }
    
    public String selectedPagesAsString{
        get{
            return record.Related_Pages__c;
        }
        set{
            record.Related_Pages__c = value != ',' ? value : null;
        }
    }
    
    
    public CV_Help_CreateEditTopicCtrl(ApexPages.StandardController stdController) {
        try{
            record = (Help_Record__c)stdController.getRecord();
            if (record == null){
                record = new Help_Record__c();
            }
            System.debug('--- record: ' + record);
            if (record.Id != null){
                List<Help_Record__c> records = [Select Related_Objects__c, User_Profiles__c, Record_Types__c, Related_Pages__c , Rich_Html_Content__c, Label__c, Active__c, Order__c From Help_Record__c Where Id = :record.Id];
                if ( records.isEmpty() ){
                    throw new CV_Utils.HelpSideBarException('Help Record not found by Id: ' + record.Id);
                }
                record = records[0];
            }
        }catch(Exception e){
            ApexPages.addMessages(e);
        }
    }
    
    public PageReference save(){
        System.debug('--- save: ' + record);
        PageReference resultRef;
        try{
            CV_Utils.upsertRecord(record);
            resultRef = getReferenceToDetailPage();
        }catch(Exception e){
            ApexPages.addMessages(e);
        }        
        return resultRef;
    }
    
    public void quickSave(){
        System.debug('--- quicksave: ' + record);
        try{
            CV_Utils.upsertRecord(record);
            ApexPages.addMessage( new ApexPages.Message(ApexPages.Severity.Confirm, 'Changes saved'));
        }catch(Exception e){
            ApexPages.addMessages(e);
        }        
    }
    
    public void cloneRecord(){
        try{
            record = (Help_Record__c)CV_Utils.cloneRecord(record);
            System.debug('--- after clone: ' + record);
        }catch(Exception e){
            ApexPages.addMessages(e);
        }
    }
    
    public PageReference deleteRecord(){
        try{
            CV_Utils.deleteRecord(record);
            return new PageReference( getTabUrl() );
        }catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }        
    }
    
    public PageReference cancel(){
        return getReferenceToDetailPage();
    }
    
    private PageReference getReferenceToDetailPage(){
        return new PageReference( '/' + ( record == null || record.Id == null ? getTabUrl() : record.Id) );
    }
    
    public List<SelectOption> getAllProfiles(){
        try{
            return CV_Utils.getAllProfiles();
        }catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
    }
    
    public List<SelectOption> getAllSObjects(){
        try{
            return CV_Utils.getAllSObjects();
        }catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
    }
    
    public List< List<SelectOption> > getAllPages(){
        try{
            return CV_Utils.getAllPages();
        }catch(Exception e){
            ApexPages.addMessages(e);
            return null;
        }
    }
    
    public List<SelectOption> getRecordTypesForSObject(){
        List<String> sobjects = selectedSObjects;
        return sobjects != null && sobjects.size() == 1 ? CV_Utils.getRecordTypesForSObject( sobjects[0] ) : null;
    }
    
    public void refreshRecordTypes(){
        
    }
    
    public Boolean getIsCloneDeleteAvailable(){
        System.debug(' --- record.Id: ' + record.Id + '. ' + (record.Id != null) );
        return record.Id != null;
    }
    
    private static String getTabUrl(){
        return Schema.Help_Record__c.getSObjectType().getDescribe().getKeyPrefix();
    }
}