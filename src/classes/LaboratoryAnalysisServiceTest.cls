/** Implements the test for the Service Layer of the object LaboratoryAnalysis__c
 * @author	Dimitrios Sgourdos
 * @version	11-Nov-2013
 */
@isTest
private class LaboratoryAnalysisServiceTest {
	
	// Global variables
	private static List<LaboratoryMethodCompound__c>	compoundList;
	private static List<Study__c>						studiesList;
	
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static void init(){
		// Create project
		Client_Project__c firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		
		// Create studies
		studiesList = BDT_TestDataUtils.buildStudies(firstProject,3);
		insert studiesList;
		
		// Create Laboratory Methods
		List<LaboratoryMethod__c> lmList = new  List<LaboratoryMethod__c>();
		lmList.add(new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
											Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood', 
											Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle'));
		lmList.add(new LaboratoryMethod__c (Name='PRA-NL-LML-0002', AnalyticalTechnique__c='ELISA', AntiCoagulant__c='N/A',
											Department__c='SML', Detection__c='N/A', Location__c='PRA-US', Matrix__c='Breast milk', 
											Proprietary__c='N', ShowOnMethodList__c='Y', Species__c='Human'));
		insert lmList; 
		
		// Create Laboratory Method Compounds
		compoundList = new List<LaboratoryMethodCompound__c>();
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[0], 'midazolam') );
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[0], '4-OH') );
		insert compoundList;
	}
	
	
	/** Test the function createLaboratoryAnalysisInstance
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void createLaboratoryAnalysisInstanceTest() {
		init();
		
		LaboratoryAnalysis__c labAnalysis = LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],
																									studiesList[0],
																									true,
																									false,
																									true
											);
		
		system.assertNotEquals(labAnalysis, NULL);
		system.assertEquals(labAnalysis.LaboratoryMethodCompound__c, compoundList[0].Id);
		system.assertEquals(labAnalysis.Study__c, 					 studiesList[0].Id);
		system.assertEquals(labAnalysis.Analysis__c, 		  		 true);
		system.assertEquals(labAnalysis.MethodDevelopment__c, 		 false);
		system.assertEquals(labAnalysis.MethodValidation__c,  		 true);
	}
	
	
	/** Test the function reorderLaboratoryAnalysisListByStudiesList
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void reorderLaboratoryAnalysisListByStudiesListTest() {
		init();
		
		List<LaboratoryAnalysis__c> initialLabAnalysisList = new List<LaboratoryAnalysis__c>();
		initialLabAnalysisList.add( LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],studiesList[2],true,false,true) );
		initialLabAnalysisList.add( LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],studiesList[0],true,false,true) );
		
		List<LaboratoryAnalysis__c> newLabAnalysisList = LaboratoryAnalysisService.reorderLaboratoryAnalysisListByStudiesList(
																								initialLabAnalysisList,
																								studiesList,
																								compoundList[0]);
																								
		system.assertEquals(newLabAnalysisList.size(), 3);
		system.assertEquals(newLabAnalysisList[0].Id, initialLabAnalysisList[1].Id);
		system.assertEquals(newLabAnalysisList[1].Id, NULL);
		system.assertEquals(newLabAnalysisList[2].Id, initialLabAnalysisList[0].Id);
	}
	
	
	/** Test the function deleteAnalysisList
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void deleteAnalysisListTest() {
		init();
		
		List<LaboratoryAnalysis__c> initialLabAnalysisList = new List<LaboratoryAnalysis__c>();
		initialLabAnalysisList.add( LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],studiesList[2],true,false,true) );
		initialLabAnalysisList.add( LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],studiesList[0],true,false,true) );
		insert initialLabAnalysisList;
		
		List<LaboratoryAnalysis__c> retrievedAnalysisList = [Select Id FROM LaboratoryAnalysis__c];
		
		// Before deletion
		system.assert(retrievedAnalysisList.size() > 0);
		
		// After deletion
		Boolean resultFlag = LaboratoryAnalysisService.deleteAnalysisList(retrievedAnalysisList);
		system.assertEquals(resultFlag, true);
		retrievedAnalysisList = [Select Id FROM LaboratoryAnalysis__c];
		system.assertEquals(retrievedAnalysisList.size(), 0);
	}
	
	
	/** Test the function getStudyAndMethodSelectOptions
	 * @author	Dimitrios Sgourdos
	 * @version	06-Nov-2013
	 */
	static testMethod void getStudyAndMethodSelectOptionsTest() {
		// Create data
		init();
		
		List<LaboratoryAnalysis__c> initialLabAnalysisList = new List<LaboratoryAnalysis__c>();
		initialLabAnalysisList.add( LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0],studiesList[0],false,true,true) );
		initialLabAnalysisList.add( LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[1],studiesList[0],false,true,true) );
		initialLabAnalysisList.add( LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[1],studiesList[1],false,true,true) );
		initialLabAnalysisList.add( LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[1],studiesList[2],false,true,true) );
		
		// Insert and retrieved so to have access in all parents fields
		insert initialLabAnalysisList;
		List<LaboratoryAnalysis__c> retrievedList = LaboratoryAnalysisDataAccessor.getByMdOrValAndUserStudies(studiesList);
		
		// Check the function
		List<SelectOption> results = LaboratoryAnalysisService.getStudyAndMethodSelectOptions(retrievedList);
		
		// Expected size 4, as the compounds belongs to the same method and there are recors assigned to all three studies
		// + the 'Please select...' option
		system.assertEquals(results.size(), 4);
		system.assertEquals(results[0].getValue(), '');
		system.assertEquals(results[0].getLabel(), 'Please select...');
		system.assertEquals(results[1].getValue(), retrievedList[0].Study__c + ':' + retrievedList[0].LaboratoryMethodCompound__r.LaboratoryMethod__r.Id);
		system.assertEquals(results[1].getLabel(), retrievedList[0].Study__r.Code__c.substringAfterLast('-') + ' / ' + retrievedList[0].LaboratoryMethodCompound__r.LaboratoryMethod__r.Name);
		system.assertEquals(results[2].getValue(), retrievedList[2].Study__c + ':' + retrievedList[0].LaboratoryMethodCompound__r.LaboratoryMethod__r.Id);
		system.assertEquals(results[2].getLabel(), retrievedList[2].Study__r.Code__c.substringAfterLast('-') + ' / ' + retrievedList[0].LaboratoryMethodCompound__r.LaboratoryMethod__r.Name);
		system.assertEquals(results[3].getValue(), retrievedList[3].Study__c + ':' + retrievedList[0].LaboratoryMethodCompound__r.LaboratoryMethod__r.Id);
		system.assertEquals(results[3].getLabel(), retrievedList[3].Study__r.Code__c.substringAfterLast('-') + ' / ' + retrievedList[0].LaboratoryMethodCompound__r.LaboratoryMethod__r.Name);
	}
	
	
	/** Test the function getLabAnalysisByMdOrValAndUserStudies.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	06-Nov-2013
	 */
	static testMethod void getLabAnalysisByMdOrValAndUserStudiesTest() {
		List<LaboratoryAnalysis__c> results = LaboratoryAnalysisService.getLabAnalysisByMdOrValAndUserStudies(new List<Study__c>());
	}
	
	
	/** Test the function getLabAnalysisByAnalysisAndUserStudies.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	11-Nov-2013
	 */
	static testMethod void getLabAnalysisByAnalysisAndUserStudiesTest() {
		List<LaboratoryAnalysis__c> results = LaboratoryAnalysisService.getLabAnalysisByAnalysisAndUserStudies(new List<Study__c>());
	}
}