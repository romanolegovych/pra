/* This Test class is for the three classes 
   PRA_Hour_ER_TriggerHandler,PRA_Unit_Effort_TriggerHandler to cover the test coverage.   
*/

@isTest
private class PRA_Hour_ER_TriggerHandlerTest {

    //Prepare the default Unit_Effort & Hour_ER records.
    static testmethod void testDefaultForecast_ER_Hour(){
  
        Profile pr = [select id, name from Profile where name = 'System Administrator'];
        User testUser = new User(alias='TestUser',Email='testPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='PZ', 
                            LocaleSidKey='en_US', ProfileId=pr.id, TimeZoneSidKey='America/New_York', Username='Test.PZ@praintl.com.unitTestPMConsole');
        insert testUser;
        
        /*Creating test data : start*/
       
        Client_Project__c project = new Client_Project__c(PRA_Project_Id__c = 'UCB');
        insert project;
        
        Monthly_Approval__c mp = new Monthly_Approval__c();
        mp.Client_Project__c=project.ID;
        mp.PZ_released_forecast__c=testUser.id;
        mp.Month_Approval_Applies_To__c= Date.today().addMonths(0).toStartOfMonth();
        String  month=(string.valueof((mp.Month_Approval_Applies_To__c).month()).length()==1)?string.valueof('0'+(mp.Month_Approval_Applies_To__c).month()):string.valueof((mp.Month_Approval_Applies_To__c).month());    
        String madate=string.valueof((mp.Month_Approval_Applies_To__c).year())+string.valueof(month)+'01';
        system.debug('------month--madate----'+month+madate);
        mp.MA_Unique_ID__c = project.Name+':'+madate;         
        insert mp;
                
        Client_Task__c task = new Client_Task__c(Project__c = project.ID,PRA_Client_Task_Id__c = 'UCB1');
        insert task;
        
        Unit_Effort__c ef1 = new Unit_Effort__c(month_applies_to__c = Date.today().addMonths(0).toStartOfMonth());
        ef1.Client_Task__c = task.id;
        ef1.Forecast_Unit__c = 1;
        ef1.Forecast_Effort__c = 2;
        ef1.Unit_Effort_ID__c = 'asdfasdfassdf1';       
        insert ef1;
       
        
        
        BUF_Code__c bc1 = new BUF_Code__c();
        insert bc1;
        
        BUF_Code__c bc2 = new BUF_Code__c();
        insert bc2;       
       
        Hour_EffortRatio__c her1 = new Hour_EffortRatio__c();
        her1.Unit_Effort__c = ef1.id;
        her1.BUF_Code__c = bc1.id;
        her1.Hour_ER_ID__c = 'asdfasdfassdf4';
        her1.Forecast_ER__c= 2;        
        insert her1;
        
        Hour_EffortRatio__c her2 = new Hour_EffortRatio__c();
        her2.Unit_Effort__c = ef1.id;
        her2.BUF_Code__c = bc2.id;
        her2.Hour_ER_ID__c= 'asdfasdfassdf5';
        her2.Forecast_ER__c= 4;        
        insert her2;
       
       
    }
    ///Prepare the default Unit_Effort & Hour_ER records and then update them to trigger those classes to fire.
    static testmethod void testUpdateForecast_ER_Hour(){
  
            
        Profile pr = [select id, name from Profile where name = 'System Administrator'];
        User testUser = new User(alias='TestUser',Email='testPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='PZ', 
                            LocaleSidKey='en_US', ProfileId=pr.id, TimeZoneSidKey='America/New_York', Username='Test.PZ@praintl.com.unitTestPMConsole');
        insert testUser;
        
        /*Creating test data : start*/
         
        Client_Project__c project = new Client_Project__c(PRA_Project_Id__c = 'UCB');
        insert project;
        
        Monthly_Approval__c mp = new Monthly_Approval__c();
        mp.Client_Project__c=project.ID;
        mp.PZ_released_forecast__c=testUser.id;
        mp.Month_Approval_Applies_To__c= Date.today().addMonths(0).toStartOfMonth();
        String  month=(string.valueof((mp.Month_Approval_Applies_To__c).month()).length()==1)?string.valueof('0'+(mp.Month_Approval_Applies_To__c).month()):string.valueof((mp.Month_Approval_Applies_To__c).month());    
        String madate=string.valueof((mp.Month_Approval_Applies_To__c).year())+string.valueof(month)+'01';
        system.debug('------month--madate----'+month+madate);
        mp.MA_Unique_ID__c = project.Name+':'+madate;         
        insert mp;
                
        Client_Task__c task = new Client_Task__c(Project__c = project.ID,PRA_Client_Task_Id__c = 'UCB1');
        insert task;
        
        Unit_Effort__c ef1 = new Unit_Effort__c(month_applies_to__c = Date.today().addMonths(0).toStartOfMonth());
        ef1.Client_Task__c = task.id;
        ef1.Forecast_Unit__c = 1;
        ef1.Forecast_Effort__c = 2;
        ef1.Worked_Unit__c=3;
        ef1.Unit_Effort_ID__c = 'asdfasdfassdf1';       
        insert ef1; 
        
         //Updating the Worked Unit record in which it calls the PRA_Unit_Effort_TriggerHandler class through PRA_Unit_Effort trigger
        ef1.Worked_Unit__c=4;
        update ef1;
        
         //Updating the Forecast Unit record in which it calls the PRA_Unit_Effort_TriggerHandlerr class through PRA_Unit_Effort trigger
        ef1.Forecast_Unit__c=4;
        update ef1;
          
        
        BUF_Code__c bc1 = new BUF_Code__c();
        insert bc1;
        
        BUF_Code__c bc2 = new BUF_Code__c();
        insert bc2;       
       
        Hour_EffortRatio__c her1 = new Hour_EffortRatio__c();
        her1.Unit_Effort__c = ef1.id;
        her1.BUF_Code__c = bc1.id;
        her1.Hour_ER_ID__c = 'asdfasdfassdf4';
        her1.Forecast_ER__c= 2;        
        insert her1;
        
        Hour_EffortRatio__c her2 = new Hour_EffortRatio__c();
        her2.Unit_Effort__c = ef1.id;
        her2.BUF_Code__c = bc2.id;
        her2.Hour_ER_ID__c= 'asdfasdfassdf5';
        her2.Forecast_ER__c= 4;        
        insert her2;
        
        mp.PZ_released_forecast__c=testUser.id;
        update mp;
        
        //Updating the Forecast Effort Ratio record in which it calls the PRA_Hour_ER_TriggerHandler class through PRA_Hour_ER trigger
        her1.Forecast_ER__c=4;
        update her1;
        
            
    }
}