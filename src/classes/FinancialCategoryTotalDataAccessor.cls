/** Implements the Selector Layer of the object FinancialCategoryTotal__c
 * @author	Dimitrios Sgourdos
 * @version	17-Dec-2013
 */
public with sharing class FinancialCategoryTotalDataAccessor {
	
	/** Object definition for fields used in application for FinancialCategoryTotal
	 * @author	Dimitrios Sgourdos
	 * @version 17-Dec-2013
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('FinancialCategoryTotal__c');
	}
	
	
	/** Object definition for fields used in application for FinancialCategoryTotal with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 17-Dec-2013
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result =	referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'FinancialDocument__c,';
		result += referenceName + 'ServiceCategory__c,';
		result += referenceName + 'Study__c';
		return result;
	}
}