@isTest
private class RegionModelsControllerTest {
	private static RegionModelsController cont;
	
	@isTest
	static void RegionModelsControllerTest() {
		Test.startTest();
    		cont = new RegionModelsController();
    	Test.stopTest();
    	System.assert(cont.Region_Models.size() == 0);
	}

	@isTest
	static void getNewPageReferenceTest() {
		cont = new RegionModelsController();
		Test.startTest();
    		PageReference pr = cont.getNewPageReference();
    	Test.stopTest();
    	Schema.DescribeSObjectResult  r = Mapping_Region_Model__c.SObjectType.getDescribe();
    	System.assertEquals(pr.getUrl(), '/'+r.getKeyPrefix()+'/e');
	}
}