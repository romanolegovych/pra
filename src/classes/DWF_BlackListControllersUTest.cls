/**
 * This test class covers the following Apex Classes:
 *	-DWF_BlackListHomeController
 *	-DWF_BlackListNewEditController
 *
 * The class DWF_FilterServiceTestMocks provides mock responses
 * to webservice requests.
 */
@isTest
private class DWF_BlackListControllersUTest {

	static list<MuleServicesCS__c> serviceSettings;
	static list<DWF_System__c> targetSystems;
	static DWF_Blacklist__c blacklist;

	static void init() {
		serviceSettings = new MuleServicesCS__c[] {
			new MuleServicesCS__c(name = 'ENDPOINT', Value__c = 'ENDPOINT'),
			new MuleServicesCS__c(name = 'TIMEOUT', Value__c = '60000'),
			new MuleServicesCS__c(name = 'DwFilterService', Value__c = 'DwFilterService')
		};
		insert serviceSettings;
		
		targetSystems = new DWF_System__c[] {
			new DWF_System__c(name = 'Salesforce'),
			new DWF_System__c(name = 'Success Factors'),
			new DWF_System__c(name = 'Databasics'),
			new DWF_System__c(name = 'AON'),
			new DWF_System__c(name = 'CSD')
		};
		insert targetSystems;
		
		blacklist = new DWF_Blacklist__c(Employee_Id__c = '14603', DWF_System__c = targetSystems.get(0).Id);
		insert blacklist;
	}
	
	/******** Start DWF_BlackListHomeController Tests ********/
	static testmethod void testHomeConstructor() {
		init();
		DWF_BlackListHomeController con = new DWF_BlackListHomeController();
		system.assertEquals(con.selectedView, 'All');
		system.assertNotEquals(con.blackLists, null);
	}
	static testmethod void testHomeDropdownLists() {
		init();
		DWF_BlackListHomeController con = new DWF_BlackListHomeController();
		list<selectoption> filterTypes = con.getFilterTypes();
		system.assertNotEquals(filterTypes, null);
		system.assert(filterTypes.size() == 3);
	}
	static testmethod void testDeleteBlacklist() {
		init();
		DWF_BlackListHomeController con = new DWF_BlackListHomeController();
		con.deleteBlacklistID = blacklist.Id;
		con.deleteBlacklist();
		system.assertEquals(0, [select count() from DWF_Blacklist__c]);
	}
	/********* End DWF_BlackListHomeController Tests *********/
	
	
	/******** Start DWF_BlackListNewEditController Tests ********/
	static testmethod void testNewEditConstructor() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_ALL_COMPANY_VO_RESPONSE));
		Apexpages.Standardcontroller stdCon = new Apexpages.Standardcontroller(new DWF_Blacklist__c());
		// Test edit constructor
		DWF_BlackListNewEditController con = new DWF_BlackListNewEditController(stdCon);
		system.assertNotEquals(con.companyNames, null);
		system.assertNotEquals(con.businessUnits, null);
		Test.stopTest();
	}
	static testmethod void testNewEditDropdowLists() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_ALL_COMPANY_VO_RESPONSE));
		Apexpages.Standardcontroller stdCon = new Apexpages.Standardcontroller(new DWF_Blacklist__c());
		DWF_BlackListNewEditController con = new DWF_BlackListNewEditController(stdCon);
		list<selectoption> systems = con.getSystemNames();
		system.assertNotEquals(systems, null);
		system.assert(systems.size() == targetSystems.size() + 1);
	}
	static testmethod void testGetCompanyAndBUFromValue() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_ALL_COMPANY_VO_RESPONSE));
		Apexpages.Standardcontroller stdCon = new Apexpages.Standardcontroller(new DWF_Blacklist__c());
		DWF_BlackListNewEditController con = new DWF_BlackListNewEditController(stdCon);
		// set company no, call get from value
		con.selectedCompanyNo = '1';
		con.getCompanyFromSelectedValue();
		// set bu name, call get from value
		con.selectedBusinessUnitName = 'MOCK BU NAME 1';
		con.getBusinessUnitFromSelectedValue();
		Test.stopTest();
	}
	static testmethod void testSearchEmployeees() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_ALL_COMPANY_VO_RESPONSE));
		Apexpages.Standardcontroller stdCon = new Apexpages.Standardcontroller(new DWF_Blacklist__c());
		DWF_BlackListNewEditController con = new DWF_BlackListNewEditController(stdCon);
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_EMPLOYEE_VO_RESPONSE));
		con.selectedCompanyNo = '100';
		con.searchEmployees();
		system.assertEquals(con.employeeWrappers.size(), 5);
		Test.stopTest();
	}
	static testmethod void testSaveWhitelists() {
		init();
		Test.startTest();
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_ALL_COMPANY_VO_RESPONSE));
		Apexpages.Standardcontroller stdCon = new Apexpages.Standardcontroller(new DWF_Blacklist__c());
		DWF_BlackListNewEditController con = new DWF_BlackListNewEditController(stdCon);
		Test.setMock(WebServiceMock.class, new DWF_FilterServiceTestMocks(DWF_FilterServiceTestMocks.GET_EMPLOYEE_VO_RESPONSE));
		con.selectedCompanyNo = '100';
		con.searchEmployees();
		for (DWF_BlackListNewEditController.EmployeeWrapper employee : con.employeeWrappers) {
			employee.isSelected = true;
		}
		con.saveBlackList();
		Test.stopTest();
	}
	/********* End DWF_BlackListNewEditController Tests *********/
}