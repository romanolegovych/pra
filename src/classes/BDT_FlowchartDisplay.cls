public with sharing class BDT_FlowchartDisplay {
	
	public ClinicalDesign__c design 						{get;set;} 
	
	public static List<armWrapper> getFlowchart(String flowchartDesignId, String projectId){
		
		List<Epoch__c> epochList = getEpochHeaderList(flowchartDesignId);
		List<Arm__c> armList = getArmHeaderList(flowchartDesignId);
		List<flowchartassignment__c> flowchartAssList = getFlowchartsAssignmentList(flowchartDesignId);
		
		List<armWrapper> armWrapperList = new List<armWrapper>();
		
		List<String> ColorList = '#6699CC:#E7A550:#999999:#7AA3CC:#E7AF5A:#B2B2B2:#8EADCC:#E7B964:#CCCCCC:#A2B7CC:#E7C36E:#E5E5E5:#B6C1CC:#E7CD78:#F9F9F9'.split(':');
	
		Set<Id> flowchartIds = new Set<Id>();
		
		Map<String,String> FlowChartColors = new MAP<String,String>();
		Integer fi = 0;
		for (Flowcharts__c f : [select id from Flowcharts__c where ClinicalDesign__c = :flowchartDesignId order by Flowchart_Number__c,name]) {
			FlowChartColors.put(f.id, ColorList[ math.mod( fi, (ColorList.size()-1) ) ]);
			fi++;
		}
		
		
		for(Arm__c arm: armList){ 
			List<flowWrapper> flowWrapperList = new List<flowWrapper>(); 
			
			for(Epoch__c ep: epochList){
				flowWrapperList.add(new flowWrapper(arm.id, ep.id, null, null)); 
			}
			
			for(flowWrapper sw: flowWrapperList){
				for(flowchartassignment__c ssa: flowchartAssList){
					if(ssa.Arm__c == sw.armId && ssa.epoch__c == sw.epochId){
							sw.flowchartDescription = ssa.flowchart__r.Description__c;
							sw.flowchartId = ssa.flowchart__c;
							sw.colorCode = FlowChartColors.get(ssa.Flowchart__c);
							
							
					}	
				}
			}
			
			armWrapperList.add(new armWrapper(arm,flowWrapperList));	
		}
		
		return armWrapperList;
	}
	
	public static List<Epoch__c> getEpochHeaderList(String aDesignID){
		List<Epoch__c> epochList; 
		 
		try{
			epochList = [Select e.Id, e.Epoch_Number__c, e.Description__c, e.Duration_days__c, e.ClinicalDesign__c, e.CreatedDate From Epoch__c e where e.ClinicalDesign__c = :aDesignID order by  Epoch_Number__c];
		}catch(Exception e){
			epochList = new List<Epoch__c>();
		}
		system.debug('epochList: '+ epochList); 
		return epochList;
	}
	
	public static List<Arm__c> getArmHeaderList(String aDesignID){ 
		List<Arm__c> armList;
		try{
			armList = [Select a.Id, a.ClinicalDesign__c, a.Description__c, a.Arm_Number__c, a.CreatedDate From Arm__c a where a.ClinicalDesign__c = :aDesignID order by Arm_Number__c];
		}catch(Exception e){
			armList = new List<Arm__c>();    
		}
		return armList;
	}
	
	public static List<flowchartassignment__c> getFlowchartsAssignmentList(String aDesignID){
		List<flowchartassignment__c> flowchartAssList;
		try{
			flowchartAssList =  [select arm__c, arm__r.description__c, epoch__c, epoch__r.Description__c, flowchart__c, flowchart__r.Description__c
									from flowchartassignment__c 
									where arm__r.ClinicalDesign__c = :aDesignID and flowchart__c!=null ];
		
		}catch(Exception e){
			flowchartAssList = new List<flowchartassignment__c>();
		}
		
		return flowchartAssList;
	}
	
	public class armWrapper{
		public Arm__c armRecord {get;set;}
		public List<flowWrapper> flowchartAss {get;set;}
		
		public armWrapper(Arm__c sArmRecord, List<flowWrapper> sFlowchartAss){
			armRecord = sArmRecord;
			flowchartAss = sFlowchartAss;
		} 
	}  
	
	public class flowWrapper{     
		public String armId	{get; set;}
		public String epochId	{get; set;}
		public String flowchartId {get; set;}
		public String flowchartDescription {get; set;}
		public String colorCode{get;set;}
		 
		
		public flowWrapper(String sArmId, String sEpochId, String sFlowchartId, String sFlowchartDescription){
			armId = sArmId;
			epochId = sEpochId;			 
			flowchartId = sFlowchartId;
			flowchartDescription = sFlowchartDescription;  
		}  

	}
	

}