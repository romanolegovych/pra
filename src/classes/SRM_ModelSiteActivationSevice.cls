/**
@author 
@date 2014
@description this class used for model site activation service
**/
public with sharing class SRM_ModelSiteActivationSevice{
    
    Integer totalDivisions = 50;
    Integer gaussianConstant = 0;
    List<Decimal> gaussianValues = new List<Decimal>();
    List<Decimal> yValuesList = new List<Decimal>();
    Decimal sumOfZValues = 0.0;
    Integer noOfWeeks = 10;
    Integer height = 5;
    Integer guassValue = 0;
    

            /**
@author 
@date 2014
@description this method to generate chart data
**/
    public List<Decimal> generateChartData(SObject siteOrSubject, String type, Boolean applyWeek){
        Integer x = 0;
        yValuesList.clear();
        gaussianValues.clear();
        sumOfZValues = 0.0;
        
        if(type.equals('SRM_Model_Site_Activation__c')){
            SRM_Model_Site_Activation__c siteActivation = (SRM_Model_Site_Activation__c)siteOrSubject;
            for(Integer i=0; i<=siteActivation.Number_of_Weeks__c; i++){
                Decimal center = ((siteActivation.Number_of_Weeks__c/2) + (siteActivation.Shift_Center_By__c));
                Decimal expVal = Math.pow(Double.valueOf((i - center)), Double.valueOf(2.0));
                Decimal divideBy = (2 * (Math.pow(Double.valueOf(siteActivation.Standard_Deviation__c), Double.valueOf(2.0))));
                Decimal zValue = (((siteActivation.Amplitude__c) * (Math.exp(-(expVal/divideBy)))) + siteActivation.Gaussian_Constant__c);
                gaussianValues.add(zValue.setScale(2));
                x++;
                sumOfZValues += zValue;
                System.debug('zValue: '+zValue.setScale(2));
            }
            System.debug('>>>gaussianValues Size>>>'+gaussianValues.size() + ' >>>sumOfZValues>>>'+sumOfZValues);
            List<Decimal> averageOfTwoConsecutive = new List<Decimal>();
            Decimal averageOfAll = 0.0;
            for(Integer i=0; i<gaussianValues.size()-1; i++){
                Decimal avg = ((gaussianValues[i]+gaussianValues[i+1])/2);
                averageOfAll += avg;
                averageOfTwoConsecutive.add(avg);
            }
            List<Decimal> weekAdjValuesList = new List<Decimal>();
            
            //Apply week adjustment
            if(applyWeek == true){
           
                Decimal averageOfAllWeek = 0.0;
                List<SRM_Week_Adjustments__c> weekAdjList = SRM_ScenarioDataAccessor.getAllWeekAdjustments(siteActivation.SRM_Model__c, siteActivation.Country__c);
                if(weekAdjList == null || weekAdjList.isEmpty()){
                    System.debug('>>> Empty week Adj list >>> ');
                    SRM_Utils.showErrorMessage('Week Adjustments are not defined for this country.');
                }
                else{
                    for(Integer i=0; i<averageOfTwoConsecutive.size(); i++){
                        Decimal adjValue = SRM_ModelSiteActivationSevice.applyWeekAdjustment(i, siteActivation.Start_Week__c, averageOfTwoConsecutive, weekAdjList);
                        if(adjValue == null){
                            averageOfAllWeek += averageOfTwoConsecutive[i];
                            weekAdjValuesList.add(averageOfTwoConsecutive[i]);
                            System.debug('>>> value added adjValue null >>> '+averageOfTwoConsecutive[i]);
                        }
                        else{
                            Decimal valueToAdd = (averageOfTwoConsecutive[i] + adjValue);
                            averageOfAllWeek += valueToAdd;
                            weekAdjValuesList.add(valueToAdd);
                            System.debug('>>> value added adjValue not null >>> '+valueToAdd);
                        }
                        System.debug('>>> averageOfAllWeek >>> '+averageOfAllWeek );
                    }
                }
                System.debug('>>> averageOfAllWeek Final >>> '+averageOfAllWeek );
                for(Integer i=0; i<weekAdjValuesList.size(); i++){
                    Decimal avg = ((weekAdjValuesList[i]/averageOfAllWeek)*100);
                    yValuesList.add(avg.setScale(2));
                    System.debug('>>>average %>>>'+avg.setScale(2));
                }
            }    
            else{
                for(Integer i=0; i<averageOfTwoConsecutive.size(); i++){
                    Decimal avg = ((averageOfTwoConsecutive[i]/averageOfAll)*100);
                    yValuesList.add(avg.setScale(2));
                    System.debug('>>>average %>>>'+avg.setScale(2));   
                }
            }
            System.debug('gaussianValues: '+gaussianValues);
            System.debug('yValuesList>> '+yValuesList);
            return  yValuesList;
        }
        else{
            SRM_Model_Subject_Enrollment__c subjectenrollment = (SRM_Model_Subject_Enrollment__c)siteOrSubject;
            for(Integer i=0; i<=noOfWeeks; i++){
                Decimal center = ((noOfWeeks/2) +(subjectenrollment.Shift_Center_By__c));
                Decimal expVal = Math.pow(Double.valueOf((i - center)), Double.valueOf(2.0));
                Decimal divideBy = (2 * (Math.pow(Double.valueOf(subjectenrollment.Standard_Deviation__c), Double.valueOf(2.0))));
                Decimal zValue = (((height) * (Math.exp(-(expVal/divideBy)))) + guassValue);
                gaussianValues.add(zValue);
                x++;
                sumOfZValues += zValue;
                System.debug('zValue: '+zValue);
            }
            System.debug('>>>gaussianValues Size>>>'+gaussianValues.size() + ' >>>sumOfZValues>>>'+sumOfZValues);
            List<Decimal> averageOfTwoConsecutive = new List<Decimal>();
            Decimal averageOfAll = 0.0;
            for(Integer i=0; i<gaussianValues.size()-1; i++){
                Decimal avg = ((gaussianValues[i]+gaussianValues[i+1])/2);
                averageOfAll += avg;
                averageOfTwoConsecutive.add(avg.setScale(2));
            }
            return averageOfTwoConsecutive;
        }
        
    }
    
    /**
    * @description method to adjust the weeks
    * @params weekNumber  WeekAdjList
    */
    public static Decimal applyWeekAdjustment(Integer weekNumber, Decimal delay, List<Decimal> averageOfTwoConsecutive, List<SRM_Week_Adjustments__c> weekAdjList){
        Decimal adjValue = null;
        for(SRM_Week_Adjustments__c wk: weekAdjList){
            System.debug('>>> weekNumber >>> '+weekNumber +' >>> wk.Week_of_Project__c >>> '+(wk.Week_of_Project__c -1));
            if(weekNumber == ((wk.Week_of_Project__c -1) - delay)){
                adjValue = (averageOfTwoConsecutive[weekNumber] * wk.Site_Act_Adjustment__c)/100;
                System.debug('>>> adjValue >>> '+adjValue);
                break;
            }
            else{
                continue;
            }
        }
        return adjValue;
    }
    
                /**
@author 
@date 2014
@description this method to get parent ID
**/
    //Method to get parent record id
    public static String getParentId(String urlToCheck){
        String parentId = '';
        if(!String.isEmpty(urlToCheck)){
            System.debug('urlToCheck: '+urlToCheck);
            Integer st = urlToCheck.indexOf('_lkid=');
            Integer ed = urlToCheck.indexOf('&', (st+6));
            parentId = urlToCheck.subString((st+6), ed);
            System.debug('>>> st >>> '+st+' >>> ed >>> '+ed + ' >>> parentId >>> '+parentId);
        }
        return parentId;
    }
    
   
               /**
@author 
@date 2014
@description this method clone child objects
**/ 
    //Method to clone all the child objects of SRM_Model object
    public static String doClone(SRM_Model__c srmModelObj){
        String newSrmModelId='';
        Map<Id,List<SRM_Model_Response__c>>  srmmodelresponsemap=new Map<Id,List<SRM_Model_Response__c>>();//to hold question id with responses
        Map<Id,Id> oldquestionidwithnewqmap=new Map<Id,Id>();//to hold old question id with new cloned id
        List<SRM_Model_Site_Activation__c>  srmsiteActivationlst=new List<SRM_Model_Site_Activation__c>();//to hold cloned site activations
        List<SRM_Model_Subject_Enrollment__c> srmsubjectenrollmentlst=new List<SRM_Model_Subject_Enrollment__c>();//To hold cloned subject enrollments
        List<SRM_Model_Questions__c> srmModelquestionlst=new List<SRM_Model_Questions__c>();//to hold cloned model questions
        List<SRM_Week_Adjustments__c> srmweekadjustmentslst=new List<SRM_Week_Adjustments__c>();//To hold cloned week adjustments
        List<SRM_Calender_Adjustments__c> srmCalenderadjustmentlst=new List<SRM_Calender_Adjustments__c>();//To hold cloned Calender adjustments
        List<SRM_Model_Response__c> srmResponselst=new List<SRM_Model_Response__c>();//To hold cloned response
        if(srmModelObj!=null && srmModelObj.id!=null){
        	
            //Starting SRM Model clone
            String srmmodelquery=SRM_ModelCloneDataAccessor.ListofSRMModels(srmModelObj.id);
            SRM_Model__c oldsrmModelObj = database.query(srmmodelquery);
            SRM_Model__c newsrmModelObj = oldsrmModelObj.clone(false,true);
            newsrmModelObj.Comments__c=srmModelObj.Comments__c;
            newsrmModelObj.Status__c='In Progress';
            insert newsrmModelObj;
            newSrmModelId=newsrmModelObj.id;
            
            //End of SRM Model Clone
            //Starting of SRM Site Activations clone
              String srmSiteActivationQuery=SRM_ModelCloneDataAccessor.ListofSiteActivations(srmModelObj);
            if(Database.query(srmSiteActivationQuery)!=null && Database.query(srmSiteActivationQuery).size()>0){
                for(SRM_Model_Site_Activation__c srmsiteactivationobj:Database.query(srmSiteActivationQuery)){
                        SRM_Model_Site_Activation__c newsrmsiteactivationobj=srmsiteactivationobj.clone(false,true);
                        newsrmsiteactivationobj.SRM_Model__c=newsrmModelObj.id;
                        newsrmsiteactivationobj.Duplicate_Record__c=null;
                        srmsiteActivationlst.add(newsrmsiteactivationobj);
                }
                if(srmsiteActivationlst!=null && srmsiteActivationlst.size()>0){
                system.debug('--srmsiteActivationlst--'+srmsiteActivationlst);
                    insert srmsiteActivationlst;
                }               
            }
            
            //End of SRM site Activations clone
            //Starting of SRM_Model_Subject_Enrollment__c clone
              String srmSubjectEnrollmentquery=SRM_ModelCloneDataAccessor.ListofSubjectEnrollments(srmModelObj);
            if(Database.query(srmSubjectEnrollmentquery)!=null && Database.query(srmSubjectEnrollmentquery).size()>0){
                for(SRM_Model_Subject_Enrollment__c srmSubjectEnrollment:Database.query(srmSubjectEnrollmentquery)){
                    SRM_Model_Subject_Enrollment__c newsrmSubjectEnrollment=srmSubjectEnrollment.clone(false,true);
                    newsrmSubjectEnrollment.SRM_Model__c=newsrmModelObj.id;
                    newsrmSubjectEnrollment.Unique_Record__c =null;
                    system.debug('--newsrmSubjectEnrollment--'+srmSubjectEnrollment.Unique_Record__c +newsrmSubjectEnrollment);
                    srmsubjectenrollmentlst.add(newsrmSubjectEnrollment);
                }   
                if(srmsubjectenrollmentlst!=null && srmsubjectenrollmentlst.size()>0){
                system.debug('--srmsubjectenrollmentlst--'+srmsubjectenrollmentlst);
                        insert srmsubjectenrollmentlst;
                }               
            }       
            
            
            //Starting of SRM_Model_Questions__c clone
                String srmQuestionsquery=SRM_ModelCloneDataAccessor.ListofsrmQuestions(srmModelObj);
                
                if(Database.query(srmQuestionsquery)!=null && Database.query(srmQuestionsquery).size()>0){
                    for(SRM_Model_Questions__c srmQuestionsObj:Database.query(srmQuestionsquery)){
                        SRM_Model_Questions__c newsrmQuestionsObj=srmQuestionsObj.clone(false,true);
                        newsrmQuestionsObj.SRM_Model__c=newsrmModelObj.id;
                        newsrmQuestionsObj.OldquestionId__c=srmQuestionsObj.id;
                        srmModelquestionlst.add(newsrmQuestionsObj);
                        srmmodelresponsemap.put(srmQuestionsObj.id,srmQuestionsObj.SRM_Model_Response__r);
                    }               
                }
                
                if(srmModelquestionlst!=null && srmModelquestionlst.size()>0){
                    insert srmModelquestionlst;
                    for(SRM_Model_Questions__c srmQuestionObj:srmModelquestionlst){
                            oldquestionidwithnewqmap.put(srmQuestionObj.OldquestionId__c,srmQuestionObj.id);                    
                    }
                    if(srmmodelresponsemap!=null && srmmodelresponsemap.size()>0){
                        for(Id oldQuestionId:srmmodelresponsemap.keyset()){
                            for(SRM_Model_Response__c srmResponseObj:srmmodelresponsemap.get(oldQuestionId)){
                                SRM_Model_Response__c newsrmResponseObj=srmResponseObj.clone(false,true);
                                newsrmResponseObj.SRM_Question_ID__c=oldquestionidwithnewqmap.get(oldQuestionId);
                                srmResponselst.add(newsrmResponseObj);                          
                            }                   
                        
                        }   
                        if(srmResponselst!=null && srmResponselst.size()>0){
                            insert srmResponselst;
                        
                        }
                    
                    }
                
                }       
            
            //End of SRM_Model_Questions__c clone
            //Starting of SRM_Week_Adjustments__c clone
            String srmWeekadjustmentquery=SRM_ModelCloneDataAccessor.ListofWeekadjustments(srmModelObj);
            if(Database.query(srmWeekadjustmentquery)!=null && Database.query(srmWeekadjustmentquery).size()>0){
                for(SRM_Week_Adjustments__c weekAdjustmentObj:Database.query(srmWeekadjustmentquery)){
                    SRM_Week_Adjustments__c newweekAdjustmentObj=weekAdjustmentObj.clone(false,true);
                    newweekAdjustmentObj.SRM_Model__c=newsrmModelObj.id;
                    newweekAdjustmentObj.Unique_Record__c = null;
                    srmweekadjustmentslst.add(newweekAdjustmentObj);
                }
                if(srmweekadjustmentslst!=null && srmweekadjustmentslst.size()>0){
                    insert srmweekadjustmentslst;           
                }
                
            }
            
            //End of SRM_Week_Adjustments__c clone
            //Starting of SRM_Calender_Adjustments__c clone
            String srmCalenderadjustmentquery=SRM_ModelCloneDataAccessor.ListofCalenderadjustments(srmModelObj);
            
            if(database.query(srmCalenderadjustmentquery)!=null && database.query(srmCalenderadjustmentquery).size()>0){
                for(SRM_Calender_Adjustments__c CalenderadjustmentObj:database.query(srmCalenderadjustmentquery)){
                    SRM_Calender_Adjustments__c newCalenderadjustmentObj=CalenderadjustmentObj.clone(false,true);
                    newCalenderadjustmentObj.SRM_Model__c=newsrmModelObj.id;
                    srmCalenderadjustmentlst.add(newCalenderadjustmentObj);             
                }
                if(srmCalenderadjustmentlst!=null && srmCalenderadjustmentlst.size()>0){
                    insert srmCalenderadjustmentlst;                
                }           
            }       
        }
        //End of SRM_Calender_Adjustments__c clone
            if(newSrmModelId!='' && newSrmModelId!=null){
                return newSrmModelId;           
            }
            else
                return newSrmModelId;   
    }
    
}