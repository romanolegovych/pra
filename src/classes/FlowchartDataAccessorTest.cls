@isTest
private class FlowchartDataAccessorTest {

    static testMethod void myUnitTest() {
        // create test data
        ClinicalDesign__c cd = new ClinicalDesign__c(name='1');
        insert cd;
        
        list<flowcharts__c> aList = new list<flowcharts__c>();
        aList.add(new flowcharts__c(ClinicalDesign__c = cd.id, Flowchart_Number__c = 1, Description__c = 'Flowchart 1'));
        aList.add(new flowcharts__c(ClinicalDesign__c = cd.id, Flowchart_Number__c = 2, Description__c = 'Flowchart 2'));
        aList.add(new flowcharts__c(ClinicalDesign__c = cd.id, Flowchart_Number__c = 3, Description__c = 'Flowchart 3'));
        insert aList;
        
        //test retrieve logic
        aList = FlowchartDataAccessor.getFlowchartList('ClinicalDesign__c = \'' + cd.id + '\'');
        system.assertEquals(3,aList.size());
    }
}