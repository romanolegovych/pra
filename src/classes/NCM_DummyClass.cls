/**
 * @description	The dummy class to test the interface(testing purpose)
 * @author		Jegadheesan Muthusamy
 * @date		Created: 11-Sep-2015, Edited: 24-Sep-2015
 */
global class NCM_DummyClass implements NCM_NotificationEventInterface {
	
	/**
	 * @description	This method used to decide if the notification occured under the given related to id
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 24-Sep-2015
	 */
	global Boolean didNotificationTopicOccur(Id notificationCatalogId, String relatedToId) {
		return true;
	}
	
	/**
	 * @description	This method used to send some dummy email subject for testing purpose
	 * @author		Jegadheesan Muthusamy
	 * @date		Created: 11-Sep-2015
	 */
	global String getNotificationSubject(Id notificationCatalogId, String relatedToId) {
		return 'Test email subject';
	}
	
	/**
	 * @description	This method used to send some dummy email body for testing purpose
	 * @author		Jegadheesan Muthusamy
	 * @date		Created: 11-Sep-2015
	 */
	global String getNotificationEmailBody(Id notificationCatalogId, String relatedToId) {
		return 'Test email body';
	}
}