/**
    The purpose of this class is to calculate the units and the cost of the scenario budgeted data.
    This class contains all the formula methods used to calculate the no of units for the service tasks.  
*/
public with sharing class PBB_FormulabasedUnitsCalcs extends PRA_BaseService {
    public PBB_FormulabasedUnitsCalcs () {
        super();
    }
    
    /** 
    * @description Method to determine the units by formula1 claculation
    * @author Devaram Bhargav
    * @date 2015
    **/
    public static Map<String,Decimal> determineFormulaMethodNames(List<String> CSTFormulaList,String PBBSID){
        Map<String,Decimal> CountryTaskUnitsMap = new Map<String,Decimal>();  
        Map<String,List<String>> FmlatoCSTIDlsit = new  Map<String,List<String>>();    
        for(Service_Task_Drivers__c std:[Select id,name,Countries_Service_Tasks__c,Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__c,
                                            Service_Task_To_Drivers__r.Service_Task__c,Service_Task_To_Drivers__r.Drivers__c,Service_Task_To_Drivers__r.name,Service_Task_To_Drivers__r.Driver_Type__c,
                                            Service_Task_To_Drivers__r.Countries__c,Service_Task_To_Drivers__r.Countries_Option__c,Service_Task_To_Drivers__r.Parameter_Value__c,
                                            Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__r.Number_of_Adjudication_Member_Payments__c,Formulas__c,Formulas__r.name,Formulas__r.Method_Name__c
                                         from Service_Task_Drivers__c
                                         where Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__c =:PBBSID 
                                         and   Countries_Service_Tasks__c in: CSTFormulaList
                                         and   Service_Task_To_Drivers__r.Drivers__c = 'Formula']){

            system.debug('--------std-------'+std.Formulas__r.Method_Name__c);   
            if (!FmlatoCSTIDlsit.containsKey(std.Formulas__r.Method_Name__c) ) {
                FmlatoCSTIDlsit.put(std.Formulas__r.Method_Name__c, new List<String>());
            }            
            (FmlatoCSTIDlsit.get(std.Formulas__r.Method_Name__c)).add(std.Countries_Service_Tasks__c);
        } 
        system.debug('-------FmlatoCSTIDlsit   -std-------'+FmlatoCSTIDlsit );  
        CountryTaskUnitsMap  =  determineFormulaMethods(FmlatoCSTIDlsit );
        system.debug('-------CountryTaskUnitsMap  -std-------'+CountryTaskUnitsMap  );           
        return  CountryTaskUnitsMap ;     
    } 
    
    public static Map<String,Decimal> determineFormulaMethods(Map<String,List<String>> FmlatoCSTIDlsit ){
         Map<String,Decimal> CountryTaskUnitsMap = new Map<String,Decimal>();  
         
         if(FmlatoCSTIDlsit.containskey('No_of_SAEs_Per_Country_v1')){
             CountryTaskUnitsMap.putall(No_of_SAEs_Per_Country_v1(FmlatoCSTIDlsit.get('No_of_SAEs_Per_Country_v1') ));
         }
         if(FmlatoCSTIDlsit.containskey('TN_of_Case_Versions_per_Country_v1')){
             CountryTaskUnitsMap.putall(TN_of_Case_Versions_per_Country_v1(FmlatoCSTIDlsit.get('TN_of_Case_Versions_per_Country_v1') ));
         }        
         
         return  CountryTaskUnitsMap ;     
    }
    
    
     /** 
    * @description Method to determine the units by No_of_SAEs_Per_Country_v1 claculation
    * @author Devaram Bhargav
    * @date 2015
    **/
    public static Map<String,Decimal> No_of_SAEs_Per_Country_v1(List<String> FmlatoCSTIDlsit ){
        Map<String,Decimal> CountryTaskUnitsMap = new Map<String,Decimal>();  
        
         for(Service_Task_Drivers__c std:[Select id,name,Countries_Service_Tasks__c,Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__c,Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__r.SAE_Rate__c,
                                            Service_Task_To_Drivers__r.Service_Task__c,Service_Task_To_Drivers__r.Drivers__c,Service_Task_To_Drivers__r.name,Service_Task_To_Drivers__r.Driver_Type__c,
                                            Service_Task_To_Drivers__r.Countries__c,Service_Task_To_Drivers__r.Countries_Option__c,Service_Task_To_Drivers__r.Parameter_Value__c,Countries_Service_Tasks__r.PBB_Scenario_Country__r.No_of_Patients__c,
                                            Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__r.Number_of_Adjudication_Member_Payments__c,Formulas__c,Formulas__r.name,Formulas__r.Method_Name__c,
                                              Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__r.No_of_case_Versions__c
                                         from Service_Task_Drivers__c
                                         where Countries_Service_Tasks__c in: FmlatoCSTIDlsit 
                                         and   Formulas__r.Method_Name__c = 'No_of_SAEs_Per_Country_v1']){
            Decimal SAERate= ((std.Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__r.SAE_Rate__c ==null)?0:std.Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__r.SAE_Rate__c);
            Decimal NP= ((std.Countries_Service_Tasks__r.PBB_Scenario_Country__r.No_of_Patients__c==null)?0:std.Countries_Service_Tasks__r.PBB_Scenario_Country__r.No_of_Patients__c);
           
            SAERate  = SAERate/100;
            Decimal  unit = SAERate *  NP;
            unit = Math.FLOOR(unit);  
            system.debug('-------SAERate  -------'+SAERate +'unit-- '+unit+'NP---'+NP);  
            CountryTaskUnitsMap.put(std.Countries_Service_Tasks__c+'-'+std.id,unit); 
        }     
        return CountryTaskUnitsMap ;    
    } 
    
     /** 
    * @description Method to determine the units by TN_of_Case_Versions_per_Country_v1 claculation
    * @author Devaram Bhargav
    * @date 2015
    **/
    public static  Map<String,Decimal> TN_of_Case_Versions_per_Country_v1(List<String> FmlatoCSTIDlsit){
        Map<String,Decimal> CountryTaskUnitsMap = new Map<String,Decimal>();  
         for(Service_Task_Drivers__c std:[Select id,name,Countries_Service_Tasks__c,Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__c,Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__r.SAE_Rate__c,
                                            Service_Task_To_Drivers__r.Service_Task__c,Service_Task_To_Drivers__r.Drivers__c,Service_Task_To_Drivers__r.name,Service_Task_To_Drivers__r.Driver_Type__c,
                                            Service_Task_To_Drivers__r.Countries__c,Service_Task_To_Drivers__r.Countries_Option__c,Service_Task_To_Drivers__r.Parameter_Value__c,
                                            Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__r.Number_of_Adjudication_Member_Payments__c,Formulas__c,Formulas__r.name,Formulas__r.Method_Name__c,
                                            Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__r.No_of_case_Versions__c,Countries_Service_Tasks__r.PBB_Scenario_Country__r.No_of_Patients__c
                                         from Service_Task_Drivers__c
                                         where Countries_Service_Tasks__c in: FmlatoCSTIDlsit 
                                         and   Formulas__r.Method_Name__c = 'TN_of_Case_Versions_per_Country_v1']){
            Decimal SAERate= ((std.Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__r.SAE_Rate__c ==null)?0:std.Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__r.SAE_Rate__c);
            Decimal CSV= ((std.Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__r.No_of_case_Versions__c==null)?0:std.Countries_Service_Tasks__r.PBB_Scenario_Country__r.PBB_Scenario__r.No_of_case_Versions__c);
            Decimal NP= ((std.Countries_Service_Tasks__r.PBB_Scenario_Country__r.No_of_Patients__c==null)?0:std.Countries_Service_Tasks__r.PBB_Scenario_Country__r.No_of_Patients__c);
           
            SAERate  = SAERate/100;
            Decimal  unit = SAERate *  NP;
            unit = Math.FLOOR(unit) * CSV  ;   
            system.debug('-------SAERate  -------'+SAERate +'unit-- '+unit+'CSV---'+CSV+'NP--'+ NP);  
            CountryTaskUnitsMap.put(std.Countries_Service_Tasks__c+'-'+std.id,unit); 
           
        }  
        return CountryTaskUnitsMap ;     
    } 
     
    
}