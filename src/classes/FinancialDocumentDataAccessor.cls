/** Implements the Selector Layer of the object FinancialDocument__c
 * @author	Dimitrios Sgourdos
 * @version	22-Jan-2014
 */
public with sharing class FinancialDocumentDataAccessor {
	
	// Global Constants
	// Document Type
	public static final String	PROPOSAL_TYPE	  = 'Proposal';
	public static final String	CHANGE_ORDER_TYPE = 'Change Order';
	// Approval Status
	public static final String	DOCUMENT_ACCEPTED = 'Accepted';
	public static final String	DOCUMENT_REJECTED = 'Rejected';
	// Document status
	public static final String	DOCUMENT_STATUS_NEW 		= 'New';
	public static final String	DOCUMENT_STATUS_UNDEFINED	= 'Undefined';
	public static final String	DOCUMENT_STATUS_IN_PROGRESS = 'In Progress';
	public static final String	DOCUMENT_STATUS_INVALID		= 'Invalid';
	public static final String	DOCUMENT_STATUS_APPROVED	= 'Approved';
	public static final String	DOCUMENT_STATUS_REJECTED	= 'Rejected';
	// Emails for document
	public static final String	ADJUSTMENTS_NOTIFICATION_EMAIL_SUBJECT	 = 'Plenbox Document Adjustments notification!';
	public static final String	RELEASE_FOR_APPROVAL_APPOINTMENT_SUBJECT = 'Plenbox Document Approval Appointment';
	
	
	/** Object definition for fields used in application for FinancialDocument
	 * @author	Dimitrios Sgourdos
	 * @version 16-Dec-2013
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('FinancialDocument__c');
	}
	
	
	/** Object definition for fields used in application for FinancialDocument with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 17-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'ApprovalStatus__c,';
		result += referenceName + 'ApprovalsUpToDate__c,';
		result += referenceName + 'BDTDeleted__c,';
		result += referenceName + 'Client_Project__c,';
		result += referenceName + 'ContentSetUpComplete__c,';
		result += referenceName + 'Description__c,';
		result += referenceName + 'DocumentOwnerEmail__c,';
		result += referenceName + 'DocumentOwnerName__c,';
		result += referenceName + 'DocumentStatus__c,';
		result += referenceName + 'DocumentType__c,';
		result += referenceName + 'FinancialDocumentParent__c,';
		result += referenceName + 'IsApprovalReleased__c,';
		result += referenceName + 'listOfStudyIds__c,';
		result += referenceName + 'PaymentTermsComplete__c,';
		result += referenceName + 'PricingComplete__c,';
		result += referenceName + 'TotalValue__c,';
		result += referenceName + 'Client_Project__r.Project_Description__c,';
		result += referenceName + 'Client_Project__r.Code__c,';
		result += referenceName + 'Client_Project__r.Client__c,';
		result += referenceName + 'Client_Project__r.Client__r.Name,';
		result += referenceName + 'FinancialDocumentParent__r.Id,';
		result += referenceName + 'FinancialDocumentParent__r.Name,';
		result += referenceName + 'FinancialDocumentParent__r.listOfStudyIds__c';
		
		return result;
	}
	
	
	/** Retrieve the Financial Document with the given id.
	 * @author	Dimitrios Sgourdos
	 * @version 16-Dec-2013
	 * @param	financialDocumentId		The id of the financial document
	 * @return	The retrieved Financial Document.
	 */
	public static FinancialDocument__c getFinancialDocumentById(String financialDocumentId) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM FinancialDocument__c ' +
								'WHERE Id = {1}',
								new List<String> {
									getSObjectFieldString(),
									'\'' + financialDocumentId + '\''
								}
							);
		
		try {
			return (FinancialDocument__c) Database.query(query);
		} catch (Exception e) {
			system.debug('Query error: ' + query);
			return null;
		}
	}
	
	
	/** Retrieve the list of Financial Document Approvals that meets the given criteria.
	 * @author	Dimitrios Sgourdos
	 * @version 20-Jan-2014
	 * @param	whereClause			The criteria that the documents must meet
	 * @param	orderByField		The field that the documents will be ordered by
	 * @return	The list of Financial Document Approvals.
	 */
	public static List<FinancialDocument__c> getFinancialDocuments(String whereClause, String orderByField) {
		// Query
		String query = String.format(
								'SELECT {0} ' +
								'FROM FinancialDocument__c ' +
								'WHERE {1} ' +
								'ORDER BY {2}',
								new List<String> {
									getSObjectFieldString(),
									whereClause,
									orderByField
								}
							);
		
		return (List<FinancialDocument__c>) Database.query(query);
	}
}