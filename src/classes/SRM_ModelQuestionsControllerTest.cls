/**
* @author Ramya Shree Edara
* @date 21-Oct-2014
* @description this is test class for scenario related classes
*/
@isTest
public with sharing class SRM_ModelQuestionsControllerTest {
    public static testMethod void testGetSRM_ModelQuestionsController(){
        //Create Country
        Country__c country = new Country__c();
        country.Name = 'India';
        country.Country_Code__c = 'IN';
        country.Daily_Business_Hrs__c = 8;
        country.Description__c = 'Test India';
        country.PRA_Country_Id__c = 'TI1';
        insert country;
                
         //Create SRM Model
        SRM_Model__c model = new SRM_MOdel__c();
        model.Status__c = 'In Progress';
        model.Comments__c = 'Test Comments';
        insert model;
        
        SRM_Model_Questions__c que1 = new SRM_Model_Questions__c();
        que1.SRM_Model__c = model.Id;
        que1.Question__c = 'This is test question';
        que1.Primary_Role__c = 'Proposal Director';
        que1.Group__c = 'Country';
        que1.Sub_Group__c = 'Site Activation';
        que1.Selecting_Country__c = 'Specific Countries'; 
        que1.Country__c = 'India';
        insert que1;
        
        SRM_Model_Response__c res1 = new SRM_Model_Response__c();
        res1.SRM_Question_ID__c = que1.Id;
        res1.Response__c = 'No';
        insert res1;
        
        
        PageReference pageRef = Page.SRM_Model_Questions;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', que1.Id);
        ApexPages.currentPage().getParameters().put('returl', model.Id);
        ApexPages.currentPage().getParameters().put('clone', '1');
        ApexPages.currentPage().getParameters().put('parentId', model.Id);   
        
        ApexPages.StandardController sc = new ApexPages.StandardController(que1);
        SRM_ModelQuestionsController srmModelQuestionsController = new SRM_ModelQuestionsController(sc);
        srmModelQuestionsController.saveModelQuestions();
        srmModelQuestionsController.saveandnewQuestions();
        srmModelQuestionsController.cancel();
        
        SRM_Model_Questions__c questions = SRM_ModelCloneDataAccessor.modelQuestions(que1.Id);
        system.assertEquals('Country',questions.Group__c);
        
    }
    
    public static testMethod void testGetSRM_ModelQuestionsControllerNullId(){
        //Create Country
        Country__c country = new Country__c();
        country.Name = 'India';
        country.Country_Code__c = 'IN';
        country.Daily_Business_Hrs__c = 8;
        country.Description__c = 'Test India';
        country.PRA_Country_Id__c = 'TI1';
        insert country;
                
         //Create SRM Model
        SRM_Model__c model = new SRM_MOdel__c();
        model.Status__c = 'In Progress';
        model.Comments__c = 'Test Comments';
        insert model;
        
        SRM_Model_Questions__c que1 = new SRM_Model_Questions__c();
        que1.SRM_Model__c = model.Id;
        que1.Question__c = 'This is test question';
        que1.Primary_Role__c = 'Proposal Director';
        que1.Group__c = 'Project';
        que1.Sub_Group__c = 'Site Activation';
        que1.Selecting_Country__c = 'Specific Countries'; 
        que1.Country__c = 'India';
        insert que1;
        
        SRM_Model_Response__c res1 = new SRM_Model_Response__c();
        res1.SRM_Question_ID__c = que1.Id;
        res1.Response__c = 'No';
        insert res1;
        
        //update model to approved state SRM Model
        model.Status__c = 'Approved';
        upsert model;
        
        PageReference pageRef = Page.SRM_Model_Questions;
        Test.setCurrentPage(pageRef);
        /* ApexPages.currentPage().getParameters().put('id', que1.Id);
        ApexPages.currentPage().getParameters().put('returl', model.Id);
        ApexPages.currentPage().getParameters().put('clone', '1');
        */
        ApexPages.currentPage().getParameters().put('parentId', model.Id);   
        
        ApexPages.StandardController sc = new ApexPages.StandardController(que1);
        SRM_ModelQuestionsController srmModelQuestionsController = new SRM_ModelQuestionsController(sc);
        srmModelQuestionsController.saveModelQuestions();
        srmModelQuestionsController.saveandnewQuestions();
        srmModelQuestionsController.cancel();
        
        SRM_Model_Questions__c questions = SRM_ModelCloneDataAccessor.modelQuestions(que1.Id);
        system.assertEquals('Project',questions.Group__c);
        
    }
    
    public static testMethod void testGetSRM_ModelQuestionsController1(){
        //Create Country
        Country__c country = new Country__c();
        country.Name = 'India';
        country.Country_Code__c = 'IN';
        country.Daily_Business_Hrs__c = 8;
        country.Description__c = 'Test India';
        country.PRA_Country_Id__c = 'TI1';
        insert country;
                
         //Create SRM Model
        SRM_Model__c model = new SRM_MOdel__c();
        model.Status__c = 'In Progress';
        model.Comments__c = 'Test Comments';
        insert model;                
        
        SRM_Model_Questions__c que = new SRM_Model_Questions__c();
        que.SRM_Model__c = model.Id;
        que.Question__c = 'This is test question';
        que.Primary_Role__c = 'Proposal Director';
        que.Group__c = 'Country';
        que.Sub_Group__c = 'Site Activation';
        que.Selecting_Country__c = 'All Countries for Scenario'; 
        que.Country__c = 'India;America;Australia';
        insert que;
        
        SRM_Model_Response__c res = new SRM_Model_Response__c();
        res.SRM_Question_ID__c = que.Id;
        res.Response__c = 'No';
        insert res;       
        
        PageReference pageRef = Page.SRM_Model_Questions;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',que.Id);
        ApexPages.currentPage().getParameters().put('returl', model.Id);
        //ApexPages.currentPage().getParameters().put('clone', '1');
        ApexPages.currentPage().getParameters().put('parentId', model.Id);   
        
        ApexPages.StandardController sc = new ApexPages.StandardController(que);
        SRM_ModelQuestionsController srmModelQuestionsController = new SRM_ModelQuestionsController(sc);
        srmModelQuestionsController.saveModelQuestions();
        srmModelQuestionsController.saveandnewQuestions();
        srmModelQuestionsController.cancel(); 
    }  
    
    public static testMethod void testGetSRM_ModelQuestionsController2(){
        //Create Country
        Country__c country = new Country__c();
        country.Name = 'India';
        country.Country_Code__c = 'IN';
        country.Daily_Business_Hrs__c = 8;
        country.Description__c = 'Test India';
        country.PRA_Country_Id__c = 'TI1';
        insert country;
                
         //Create SRM Model
        SRM_Model__c model = new SRM_MOdel__c();
        model.Status__c = 'In Progress';
        model.Comments__c = 'Test Comments';
        insert model;                
        
        SRM_Model_Questions__c que = new SRM_Model_Questions__c();
        que.SRM_Model__c = model.Id;
        que.Question__c = 'This is test question';
        que.Primary_Role__c = 'Proposal Director';
        que.Group__c = 'Country';
        que.Sub_Group__c = 'Site Activation';
        que.Selecting_Country__c = 'Specific Countries'; 
        que.Country__c = 'India;America;Australia';
        insert que;
        
        SRM_Model_Response__c res = new SRM_Model_Response__c();
        res.SRM_Question_ID__c = que.Id;
        res.Response__c = 'No';
        insert res;       
        
        PageReference pageRef = Page.SRM_Model_Questions;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',que.Id);
        ApexPages.currentPage().getParameters().put('returl', model.Id);
        //ApexPages.currentPage().getParameters().put('clone', '1');
        ApexPages.currentPage().getParameters().put('parentId', model.Id);   
        
        ApexPages.StandardController sc = new ApexPages.StandardController(que);
        SRM_ModelQuestionsController srmModelQuestionsController = new SRM_ModelQuestionsController(sc);
        srmModelQuestionsController.selectedCountries = new LIST<System.SelectOption>();
        srmModelQuestionsController.saveModelQuestions();
        srmModelQuestionsController.saveandnewQuestions();
        srmModelQuestionsController.cancel(); 
    } 
    
 
}