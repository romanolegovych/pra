public with sharing class PAWS_PBB_Helper
{
	public class PAWS_PBB_HelperException extends Exception {}

	public PAWS_PBB_Helper() { }

	public static List<WorkItem> getWorkBreakdown(ID pawsProjectId)
    {
        List<PAWS_Project_Flow_Junction__c> projectFlowJunctionList = [select Flow__c from PAWS_Project_Flow_Junction__c where Project__c = :pawsProjectId limit 1];
        if(projectFlowJunctionList.isEmpty() == true)
        {
        	throw new PAWS_PBB_HelperException('There is no flow assigned to the project.');
        }

        ID flowId = projectFlowJunctionList.get(0).Flow__c;

		List<WorkItem> workItemList = new List<WorkItem>();
		List<StepWrapper> wrappedSteps = getWrappedStepsByFlowId(flowId);
        for(StepWrapper eachStep : wrappedSteps)
        {
            STSWR1__Flow_Step_Property__c stepProperty = eachStep.Step.STSWR1__Flow_Step_Properties__r.isEmpty() ? null : eachStep.Step.STSWR1__Flow_Step_Properties__r.get(0);
            STSWR1__Gantt_Step_Property__c ganttProperty = eachStep.Step.STSWR1__Gantt_Properties__r.isEmpty() ? null : eachStep.Step.STSWR1__Gantt_Properties__r.get(0);
            if(stepProperty != null && (eachStep.ServiceTaskId != null || Test.isRunningTest()))
            {
            	Map<String, Object> stepPropertyMap = (Map<String, Object>)JSON.deserializeUntyped(stepProperty.STSWR1__Value__c);
            	for(Object eachEffort : (List<Object>)stepPropertyMap.get('Efforts'))
            	{
            		WorkItem item = new WorkItem((Map<String, Object>)eachEffort, ganttProperty);
            		FlowObject country = eachStep.getCountry();
            		item.CountryName = country == null ? null : country.CountryName;
            		item.CountryID = country == null ? null : country.CountryID;
            		item.ServiceTaskId = eachStep.ServiceTaskId;
						workItemList.add(item);
            	}
			
            }
        }

        return workItemList;
    }

    public static List<StepWrapper> getWrappedStepsByFlowId(ID flowId)
    {
    	List<StepWrapper> outWrappedSteps = new List<StepWrapper>();
    	List<FlowWrapper> result = getFlowsHierarchy(new List<ID> { flowId }, outWrappedSteps, null, null);
    	return outWrappedSteps;
    }

    private static List<FlowWrapper> getFlowsHierarchy(List<ID> flowIds, List<StepWrapper> outWrappedSteps, Map<ID, FlowWrapper> proccessedWrappedFlowMap, Map<ID, List<StepWrapper>> flowIdWrppedStepsMap)
    {
    	if(proccessedWrappedFlowMap == null)
    	{
    		proccessedWrappedFlowMap = new Map<ID, FlowWrapper>();
    	}
		if(flowIdWrppedStepsMap == null)
    	{
			flowIdWrppedStepsMap = new Map<ID, List<StepWrapper>>();
    	}

    	List<FlowWrapper> result = new List<FlowWrapper>();
    	for(STSWR1__Flow__c eachFlow : getFlowsById(flowIds))
		{
			FlowWrapper wrpdFlow = new FlowWrapper(eachFlow);
			proccessedWrappedFlowMap.put(eachFlow.ID, wrpdFlow);
			result.add(wrpdFlow);
			List<StepWrapper> wrappedStepList = flowIdWrppedStepsMap.get(eachFlow.ID);
			if(wrappedStepList != null)
			{
				for(StepWrapper eachWrappedStep : wrappedStepList)
				{
					eachWrappedStep.addSubFlow(wrpdFlow);
				}
			}
		}

    	List<STSWR1__Flow_Step_Junction__c> flowStepJunctionList = loadFlowStepJunction(flowIds);
    	Set<ID> flowIdsForSearch = new Set<ID>();
    	flowIdWrppedStepsMap = new Map<ID, List<StepWrapper>>();
    	for(STSWR1__Flow_Step_Junction__c eachStep : flowStepJunctionList)
    	{
			FlowWrapper wrappedFlow = proccessedWrappedFlowMap.get(eachStep.STSWR1__Flow__c);
			StepWrapper wrappedStep = wrappedFlow.addStep(eachStep);
			if(wrappedStep.isSkippedStep() == true)
			{
				continue;
			}
			outWrappedSteps.add(wrappedStep);

			for(STSWR1__Flow_Step_Action__c eachAction : eachStep.STSWR1__Flow_Step_Actions__r)
			{
				Map<String, Object> config = (Map<String, Object>)System.JSON.deserializeUntyped(eachAction.STSWR1__Config__c);
				if(config != null)
				{
					Object flowId = config.get('flowId');
					if(flowId != null && String.valueOf(flowId) != '')
					{
						ID flwId = (ID)String.valueOf(flowId);
						List<StepWrapper> wrappedStepList = flowIdWrppedStepsMap.get(flwId);
						if(wrappedStepList == null)
						{
							wrappedStepList = new List<StepWrapper>();
							flowIdWrppedStepsMap.put(flwId, wrappedStepList);
						}

						wrappedStepList.add(wrappedStep);

						// NOTE: don't add flow ID if it already proccessed
						if(proccessedWrappedFlowMap.containsKey(flwId) == false)
						{
							flowIdsForSearch.add(flwId);
						}
					}
				}
			}
    	}

    	if(flowIdsForSearch.isEmpty() == false)
    	{
			getFlowsHierarchy(new List<ID>(flowIdsForSearch), outWrappedSteps, proccessedWrappedFlowMap, flowIdWrppedStepsMap);
    	}
    	else
    	{
    		updateFlowObject(proccessedWrappedFlowMap);
			updateTemplateSteps(proccessedWrappedFlowMap);
			updateWRModelID(proccessedWrappedFlowMap);
			updateServiceTaskID(proccessedWrappedFlowMap);
    	}

    	return result;
    }

    public static void updateFlowObject(Map<ID, FlowWrapper> inFlowWrapperMap)
    {
        Map<ID, List<FlowWrapper>> objectIdFlowWrapperMap = new Map<ID, List<FlowWrapper>>();
        Map<String, List<String>> objectTypeObjectIdsMap = new Map<String, List<String>>();

        for(FlowWrapper eachFlowWrap : inFlowWrapperMap.values())
        {
            String objectId = eachFlowWrap.Flow.STSWR1__Source_Id__c;
            String objectType = eachFlowWrap.Flow.STSWR1__Object_Type__c;

            if(objectId != null && objectType != null)
            {
                List<FlowWrapper> flowList = objectIdFlowWrapperMap.get(objectId);
                if(flowList == null)
                {
                    flowList = new List<FlowWrapper>();
                    objectIdFlowWrapperMap.put(objectId, flowList);
                }
                flowList.add(eachFlowWrap);
                
                List<String> objectIds = objectTypeObjectIdsMap.get(objectType);
                if(objectIds == null)
                {
                    objectIds = new List<String>();
                }
                objectIds.add(objectId);
                objectTypeObjectIdsMap.put(objectType, objectIds);
            }
        }
        
        for(String eachObjectType : objectTypeObjectIdsMap.keySet())
        {
            List<String> objectIds = objectTypeObjectIdsMap.get(eachObjectType);

            String fields = 'Id, Name';
            if(eachObjectType == 'ecrf__c')
            {
				fields += ', SRM_Scenario_Name__c';
            }

            String query = 'select ' + fields + ' from ' + eachObjectType + ' where ID = :objectIds';
            List<sObject> sObjectList = Database.query(query);
            for(sObject eachFlowObject : sObjectList)
            {
                for(FlowWrapper flwWrapperEach : objectIdFlowWrapperMap.get(eachFlowObject.Id))
                {
                    flwWrapperEach.FlowObject = new FlowObject(eachFlowObject);
                }
            }

            // TODO: process countries with the same names
            if(eachObjectType == 'PAWS_Project_Flow_Country__c')
            {
            	Map<String, Set<ID>> contryNameMap = new Map<String, Set<Id>>();
            	Set<String> countryNameSet = new Set<String>();
            	for(PAWS_Project_Flow_Country__c eachCountry : (List<PAWS_Project_Flow_Country__c>)sObjectList)
		        {
		        	Set<ID> ids = contryNameMap.get(eachCountry.Name);
		        	if(ids == null)
		        	{
						ids = new Set<ID>();
						contryNameMap.put(eachCountry.Name, ids);
		        	}
		        	ids.add(eachCountry.ID);
		        	countryNameSet.add(eachCountry.Name);
		        }

		        for(Country__c eachCountry : [select ID, Name from Country__c where Name = :countryNameSet])
		        {
		        	Set<ID> countryIDs = contryNameMap.get(eachCountry.Name);
		        	if(countryIDs != null)
		        	{
		        		for(ID eachId : countryIDs)
		        		{
							for(FlowWrapper flwWrapperEach : objectIdFlowWrapperMap.get(eachId))
				            {
				                flwWrapperEach.FlowObject.CountryID = eachCountry.ID;
				                flwWrapperEach.FlowObject.CountryName = eachCountry.Name;
				            }
		        		}
		        	}
		        }
            }
        }
    }

    public static void updateWRModelID(Map<ID, FlowWrapper> inFlowWrapperMap)
    {
		Set<String> scenarioIds = new Set<String>();
		Map<String, FlowWrapper> scenarioIdWrappedFlowMap = new Map<String, FlowWrapper>();
		for(FlowWrapper eachWrappedFlow : inFlowWrapperMap.values())
		{
			if(eachWrappedFlow.Flow.STSWR1__Object_Type__c == 'ecrf__c' && eachWrappedFlow.FlowObject != null && eachWrappedFlow.FlowObject.FlowObject.get('SRM_Scenario_Name__c') != null)
			{
				String scenarioId = (String)eachWrappedFlow.FlowObject.FlowObject.get('SRM_Scenario_Name__c');
				scenarioIds.add(scenarioId);
				scenarioIdWrappedFlowMap.put(scenarioId, eachWrappedFlow);
			}
		}

		for(PBB_Scenario__c eachScenario : getScenariosById(scenarioIds))
		{
			FlowWrapper wrappedFlow = scenarioIdWrappedFlowMap.get(eachScenario.Name);
			wrappedFlow.WRModelId = eachScenario.WR_Model__c;
		}
    }

    public static void updateTemplateSteps(Map<ID, FlowWrapper> inFlowWrapperMap)
    {
    	Set<ID> parentFlowIds = new Set<ID>();
    	Map<ID, FlowWrapper> parentFlowIdWrappedFlowMap = new Map<ID, FlowWrapper>();
    	for(FlowWrapper eachWrappedFlow : inFlowWrapperMap.values())
    	{
    		if(eachWrappedFlow.Flow.STSWR1__Parent__c != null)
    		{
				parentFlowIds.add(eachWrappedFlow.Flow.STSWR1__Parent__c);
				parentFlowIdWrappedFlowMap.put(eachWrappedFlow.Flow.STSWR1__Parent__c, eachWrappedFlow);
    		}
    	}

    	if(parentFlowIds.isEmpty() == false)
    	{
    		List<STSWR1__Flow_Step_Junction__c> templateSteps = [select Name, STSWR1__Index__c, STSWR1__Flow__c from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__c = :parentFlowIds];
    		for(STSWR1__Flow_Step_Junction__c eachTemplateStep : templateSteps)
    		{
    			FlowWrapper wrappedFlow = parentFlowIdWrappedFlowMap.get(eachTemplateStep.STSWR1__Flow__c);
    			StepWrapper wrappedStep = wrappedFlow.StepKeyWrappedStepMap.get(eachTemplateStep.Name + eachTemplateStep.STSWR1__Index__c);
    			if(wrappedStep != null)
    			{
    				wrappedStep.StepTemplate = eachTemplateStep;
    			}
    		}
    	}
    }

	private static void updateServiceTaskID(Map<ID, FlowWrapper> inFlowWrapperMap)
	{
		Set<ID> stepIds = new Set<ID>();
		Set<ID> modelIds = new Set<ID>();
		Map<String, FlowWrapper> taskKeyWrappedFlowMap = new Map<String, FlowWrapper>();
		Map<ID,ID> templateStepClonedStepMap = new Map<ID,ID>();
		for(FlowWrapper eachWrappedFlow : inFlowWrapperMap.values())
		{
			ID wrModalID = eachWrappedFlow.get_WRModelID();
			if(wrModalID == null)
			{
				continue;
			}

			for(StepWrapper eachWrappedStep : eachWrappedFlow.Steps)
			{
				if(eachWrappedStep.StepTemplate != null)
				{
					stepIds.add(eachWrappedStep.StepTemplate.ID);
					modelIds.add(wrModalID);
					taskKeyWrappedFlowMap.put(wrModalID + '' + eachWrappedStep.StepTemplate.ID, eachWrappedFlow);
					templateStepClonedStepMap.put(eachWrappedStep.StepTemplate.ID, eachWrappedStep.Step.ID);
				}
			}
		}

		System.debug(LoggingLevel.ERROR, '==> stepIds' + JSON.serialize(stepIds));
		System.debug(LoggingLevel.ERROR, '==> modelIds' + JSON.serialize(modelIds));

		List<Service_Task_To_Flow_Step__c> serviceTaskList = [select Flow_Step__c, Service_Tasks__c, WR_Model__c from Service_Task_To_Flow_Step__c where Flow_Step__c = :stepIds and WR_Model__c = :modelIds];
		System.debug(LoggingLevel.ERROR, '==> serviceTaskList' + JSON.serialize(serviceTaskList));
		for(Service_Task_To_Flow_Step__c eachServiceTask : serviceTaskList)
		{
			FlowWrapper wrappedFlow = taskKeyWrappedFlowMap.get(eachServiceTask.WR_Model__c + '' + eachServiceTask.Flow_Step__c);
			StepWrapper wrappedStep = wrappedFlow.StepMap.get(templateStepClonedStepMap.get(eachServiceTask.Flow_Step__c));
			wrappedStep.ServiceTaskId = eachServiceTask.Service_Tasks__c;
 		}
	}

    private static List<STSWR1__Flow_Step_Junction__c> loadFlowStepJunction(List<ID> flowIds)
    {
        return [select
                STSWR1__Flow__c, Name, STSWR1__Index__c,
                (
                    select 
                    	name, STSWR1__Type__c, STSWR1__Status__c, STSWR1__Config__c 
                    from 
                    	STSWR1__Flow_Step_Actions__r 
                    where 
                    	STSWR1__Type__c = 'Start Sub Flow'
                ),
				(select STSWR1__Value__c from STSWR1__Flow_Step_Properties__r where STSWR1__Type__c = 'Roles & Efforts'), 
				(select STSWR1__Planned_End_Date__c, STSWR1__Planned_Start_Date__c, STSWR1__Is_Skipped__c from STSWR1__Gantt_Properties__r where STSWR1__Level__c = 'First')
            from
                STSWR1__Flow_Step_Junction__c
            where
                STSWR1__Flow__c = :flowIds];
    }

    private static List<STSWR1__Flow__c> getFlowsById(List<ID> flowIds)
    {
    	return [select STSWR1__Parent__c, ID, Name, STSWR1__Object_Type__c, STSWR1__Source_Id__c from STSWR1__Flow__c where ID = :flowIds];
    }

    private static List<PBB_Scenario__c> getScenariosById(Set<String> scenarioIds)
    {
    	return [select Name, WR_Model__c from PBB_Scenario__c where Name = :scenarioIds];
    }
    
	public static List<WrModelStepWrapper> getWRModelSteps(ID flowId)
	{
		String flowPath, flowPathWithNames;
		Set<ID> flowPathItemIds = new Set<ID>();
		List<WrModelStepWrapper> steps = new List<WrModelStepWrapper>();
		
		STSWR1__Item__c flowItem = [Select STSWR1__Parent__c, STSWR1__Path__c, STSWR1__Sub_Path__c From STSWR1__Item__c Where STSWR1__Source_Flow__c = :flowId];
		
		Map<ID, Map<String, String>> flows = new Map<ID, Map<String, String>>();
		flowPath = (flowItem.STSWR1__Path__c == null ? '' : flowItem.STSWR1__Path__c) + (flowItem.STSWR1__Sub_Path__c == null ? '' : '\\' + flowItem.STSWR1__Sub_Path__c);
		flows.put(flowId, new Map<String, String>{'pathIds' => flowPath});
		if (!String.isBlank(flowPath)) flowPathItemIds.addAll((List<ID>) flowPath.split('\\\\'));
		
		ID parentFolderId = flowItem.STSWR1__Parent__c;
		Set<ID> parentIds = new Set<ID>{parentFolderId};
		
		while(parentIds.size() > 0 && parentFolderId != null) //if parentFolderId == null, then we want to process only one flow - "flowId"
		{
			List<STSWR1__Item__c> items = [Select STSWR1__Type__c, STSWR1__Source_Flow__c, STSWR1__Path__c, STSWR1__Sub_Path__c From STSWR1__Item__c Where STSWR1__Parent__c = :parentIds];
			parentIds.clear();
			
			for (STSWR1__Item__c item : items)
			{
				if (item.STSWR1__Source_Flow__c != null)
				{
					flowPath = (item.STSWR1__Path__c == null ? '' : item.STSWR1__Path__c) + (item.STSWR1__Sub_Path__c == null ? '' : '\\' + item.STSWR1__Sub_Path__c);
					flows.put(item.STSWR1__Source_Flow__c, new Map<String, String>{'pathIds' => flowPath});
					if (!String.isBlank(flowPath)) flowPathItemIds.addAll((List<ID>) flowPath.split('\\\\'));
				}
				else
				{
					parentIds.add(item.Id);
				}
			}
		}
		
		Map<ID, STSWR1__Item__c> allItems = new Map<ID, STSWR1__Item__c>([Select STSWR1__Name_Value__c From STSWR1__Item__c Where Id = :flowPathItemIds]);
		
		for (ID fid : flows.keySet())
		{
			flowPathWithNames = flows.get(fid).get('pathIds');
			if (!String.isBlank(flowPathWithNames))
			{
				for (ID itemId : (List<ID>) flowPathWithNames.split('\\\\'))
				{
					flowPathWithNames = flowPathWithNames.replace((String) itemId, allItems.get(itemId).STSWR1__Name_Value__c);
				}
			}
			flows.get(fid).put('pathNames', flowPathWithNames);
		}
		
		List<STSWR1__Flow_Step_Junction__c> flowSteps = [Select Name, STSWR1__Flow__r.Name From STSWR1__Flow_Step_Junction__c Where STSWR1__Flow__c = :flows.keySet() Order By STSWR1__Flow__r.Name, STSWR1__Flow__r.Id, Name];
		
		for (STSWR1__Flow_Step_Junction__c step : flowSteps)
		{
			steps.add(new WrModelStepWrapper(
				step.Id,
				step.Name,
				step.STSWR1__Flow__r.Name,
				flows.get(step.STSWR1__Flow__r.Id).get('pathNames')
			));
		}
		
		return steps;
	}

    public class FlowWrapper
    {
    	public FlowWrapper(STSWR1__Flow__c inFlow, StepWrapper inParentStep)
    	{
    		this(inFlow);
    		ParentStep = inParentStep;
		}

    	public FlowWrapper(STSWR1__Flow__c inFlow)
    	{
    		Steps = new List<StepWrapper>();
    		StepKeyWrappedStepMap = new Map<String, StepWrapper>();
    		StepMap = new Map<ID, StepWrapper>();
    		Flow = inFlow;
    	}

    	public STSWR1__Flow__c Flow { get; set; }
    	public StepWrapper ParentStep { get; set; }
    	public List<StepWrapper> Steps { get; set; }
    	public Map<String, StepWrapper> StepKeyWrappedStepMap { get; set; }
    	public Map<ID, StepWrapper> StepMap { get; set; }

    	//public sObject FlowObject { get; set; }
    	public FlowObject FlowObject { get; set; }
    	
    	public String WRModelId { get; set; }

    	public StepWrapper addStep(STSWR1__Flow_Step_Junction__c step)
    	{
    		StepWrapper wrappedStep = new StepWrapper(step, this);
			Steps.add(wrappedStep);
			StepKeyWrappedStepMap.put(step.Name + step.STSWR1__Index__c, wrappedStep);
			StepMap.put(step.ID, wrappedStep);
			return wrappedStep;
    	}

    	public String get_WRModelID()
		{
			return get_WRModelID(this);
		}

		private String get_WRModelID(FlowWrapper inFlow)
		{
			if(inFlow.WRModelId != null)
			{
				return inFlow.WRModelId;
			}
			else if(inFlow.ParentStep != null)
			{
				return get_WRModelID(inFlow.ParentStep.ParentFlow);
			}
			else
			{
				return null;
			}
		}
    }

	public class StepWrapper
    {
		public StepWrapper(STSWR1__Flow_Step_Junction__c inStep, FlowWrapper flow)
		{
			SubFlows = new List<FlowWrapper>();
			ParentFlow = flow;
			Step = inStep;
		}

		public STSWR1__Flow_Step_Junction__c Step { get; set; }
		public STSWR1__Flow_Step_Junction__c StepTemplate { get; set; }
		public FlowWrapper ParentFlow { get; set; }
		public List<FlowWrapper> SubFlows { get; set; }
		public ID ServiceTaskId { get; set;}

		public FlowWrapper addSubFlow(STSWR1__Flow__c inFlow)
		{
			FlowWrapper wrappedFlow = new FlowWrapper(inFlow, this);
			SubFlows.add(wrappedFlow);
			return wrappedFlow;
		}

		public void addSubFlow(FlowWrapper inFlow)
		{
			inFlow.ParentStep = this;
			SubFlows.add(inFlow);
		}

		public FlowObject getCountry()
		{
			return getCountry(ParentFlow);
		}

		public Boolean isSkippedStep()
		{
			if(Step.STSWR1__Gantt_Properties__r.isEmpty() == false && Step.STSWR1__Gantt_Properties__r.get(0).STSWR1__Is_Skipped__c)
			{
				return true;
			}			
			else
			{
				return false;
			}
		}

		private FlowObject getCountry(FlowWrapper wrappedFlow)
		{
			if(wrappedFlow == null)
			{
				return null;
			}

			if(wrappedFlow.Flow.STSWR1__Object_Type__c == 'PAWS_Project_Flow_Country__c')
			{
				return wrappedFlow.FlowObject;
			}
			else if(wrappedFlow.ParentStep != null)
			{
				return getCountry(wrappedFlow.ParentStep.ParentFlow);
			}
			else
			{
				return null;
			}
		}
    }

    public class FlowObject
    {
    	public FlowObject(sObject inSobject)
    	{
			this.FlowObject = inSobject;
    	}

    	public sObject FlowObject { get; set; }
		public String CountryName { get; set; }
		public ID CountryID { get; set; }
    }

	public class WorkItem
	{
		public WorkItem(Map<String, Object> stepPropertyMap, STSWR1__Gantt_Step_Property__c ganttStepProperty)
		{
			if(stepPropertyMap != null)
			{
				JobClassDesc = String.valueOf(stepPropertyMap.get('RoleCode'));
				Effort = Integer.valueOf(stepPropertyMap.get('Effort'));
			}
			if(ganttStepProperty != null)
			{
				StartDate = ganttStepProperty.STSWR1__Planned_Start_Date__c == null ? null : ganttStepProperty.STSWR1__Planned_Start_Date__c.dateGMT();
				EndDate = ganttStepProperty.STSWR1__Planned_End_Date__c == null ? null :ganttStepProperty.STSWR1__Planned_End_Date__c.dateGMT();
			}
		} 

		public String JobClassDesc { get; set;}
		public Integer Effort { get; set;}
		public Date StartDate { get; set;}
		public Date EndDate { get; set;}
		public ID ServiceTaskId { get; set;}
		public String CountryName { get; set;}
		public ID CountryID { get; set;}
	}
	
	public class WrModelStepWrapper
	{
		public ID StepId {get; set;}
		public String StepName {get; set;}
		public String FlowName {get; set;}
		public String FlowPath {get; set;}
		
		public WrModelStepWrapper(ID stepId, String stepName, String flowName, String flowPath)
		{
			this.StepId = stepId;
			this.StepName = stepName;
			this.FlowName = flowName;
			this.FlowPath = flowPath;
		}
	}
}