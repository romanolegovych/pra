/**
* @author Ramya Shree Edara
* @date 1/14/2015
* @description this is test class for Project Information class
*/

@isTest
public with sharing class PBB_ProjectInformationControllerTest {

    //test method for save
    public static testMethod void testPBB_ProjectInformationController()  {
        test.startTest();
        //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils ();       
        tu.wfmp = tu.createwfmproject();
        tu.bid =tu.createbidproject();        
        tu.scen =tu.createscenarioAttributes(); 
        tu.smm = tu.createServiceModelAttributes();
        tu.smm = tu.approveServicemodel(tu.smm);
        tu.Wrm = tu.createWRModelAttributes();       
        tu.scen.WR_Model__c  = tu.Wrm.id;
        update tu.scen;
        
        tu.Therp = tu.createTherapeutic();
        tu.Indgrp = tu.createIndicationGroup();
        tu.prmyind = tu.createPrimaryIndication();        
        tu.Phase = tu.createWFMPhase();
        
        //ApexPages.currentPage().getParameters().put('bid',tu.bid.id);
        ApexPages.currentPage().getParameters().put('id',tu.scen.id);
        
        PBB_ProjectInformationController pc = new PBB_ProjectInformationController(new ApexPages.StandardController(tu.scen));
        
        string taNames = tu.bid.Therapeutic_Area__c;            
        string selectedPhase = tu.bid.Study_Phase__c; 
        string prmyinds = tu.bid.Primary_Indication__c; 
        string Indgrpvalues = tu.bid.Indication_group__c;        
        
        pc.save();
        pc.cancel();
       // PageReference pageRef = Page.PBB_ProjectInformation;
        //Test.setCurrentPage(pageRef);
        pc.getWrList();
        pc.gettherptic();
        pc.CloneScenario();
        pc.getIndicationgrp();
        pc.getprimaryInd();
        pc.getstudyPhase();
        pc.getIndicationsBasedOnTheropeticArea();
        pc.getprmyIndicationsBasedOnIndgrp();
        test.stopTest();
    }
    
    //test method for save
    public static testMethod void testNewPBBScenario()  {
        test.startTest();
        PBB_TestUtils tu = new PBB_TestUtils ();       
        tu.wfmp = tu.createwfmproject();
        tu.bid =tu.createbidproject(); 
        tu.bid.Expected_Sites__c = 10; 
        tu.bid.Expected_Patients__c = 20;
        update tu.bid;
        PBB_Scenario__c psc = new PBB_Scenario__c(Bid_Project__c = tu.bid.id);
        ApexPages.currentPage().getParameters().put('bid',tu.bid.id);
        //ApexPages.currentPage().getParameters().put('id',tu.scen.id);
        
        PBB_ProjectInformationController pc = new PBB_ProjectInformationController(new ApexPages.StandardController(psc));
        PageReference pageRef = Page.PBB_ProjectInformation;
        Test.setCurrentPage(pageRef);
        pc.save();
        test.stopTest();
    }
}