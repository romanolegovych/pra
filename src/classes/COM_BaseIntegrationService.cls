/**
 * @author Sukrut Wagh
 * @date 	06/24/2014
 * @description Abstract class providing common properties for all services
 */ 
public abstract class COM_BaseIntegrationService implements COM_IIntegrationService {

	//API name for sObjType to be integrated
	private String sObjType;
	//Integration system name
	private String intSystem; 
	
	/**
	 * @description Returns the sObject type API name 
	*/
	public String getSObjectType() {
		return this.sObjType;
	}

	/**
	 * @description Returns the integration system 
	*/
	public String getIntSystem() {
		return this.intSystem;
	}
	
	public void setSObjectType(final String sObjType) {
		this.sObjType = sObjType;
	}
	public void setIntSystem(final String intSystem) {
		this.intSystem = intSystem;
	}
	
	protected COM_BaseIntegrationService(){}
	
	/**
	* @author 	Sukrut Wagh
	* @date 	06/24/2014
	* @description Instance of COM_IIntegrationService should be retrieved only from COM_Factory.getIntegrationService
	*/
	protected COM_BaseIntegrationService(final String intSystem, final String sObjType) {
		if(null == intSystem || null == sObjType) {
			throw new COM_Exception(COM_ErrorCode.CANNOT_CREATE_INTEGRATION_SERVICE,
			'Cannot instantiate integration service for system:'+intSystem+', sObjType:'+sObjType);
		}
		this.sObjType = sObjType;
		this.intSystem = intSystem;
	}

}