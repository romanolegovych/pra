/**
 * Created by illia on 12/7/15.
 */
/**
* This class provides an interface to maintain a custom settings.
* By default it relies on custom SObject with the name <code>PAWS_Settings__c</code>,
* however you can overwrite this behaviour by creating you
*/
public virtual class PAWS_Settings
{
	/********************PROPERTIES REGION********************/
	public static final PAWS_Settings shared = new PAWS_Settings();
	public static final String ORGANIZATION_WIDE_SCOPE = '*';
	public static final List<String> SCOPE_ORDER = new List<String>{Userinfo.getUsername(), Userinfo.getProfileID(), ORGANIZATION_WIDE_SCOPE};

	public String scope {get; private set;}

	private static Map<String, Map<String, PAWS_Settings__c>> settingsMap
	{
		get
		{
			if (settingsMap == null)
			{
				settingsMap = new Map<String, Map<String, PAWS_Settings__c>>();
				for (PAWS_Settings__c each : PAWS_Settings__c.getAll().values())
				{
					String scope = (each.Scope__c != null ? each.Scope__c : ORGANIZATION_WIDE_SCOPE);
					if (!settingsMap.containsKey(each.Name__c))
					{
						settingsMap.put(each.Name__c, new Map<String, PAWS_Settings__c>());
					}

					settingsMap.get(each.Name__c).put(scope, each);
				}
			}

			return settingsMap;
		}
		set;
	}

	/********************METHODS REGION********************/
	private PAWS_Settings()
	{
		scope = ORGANIZATION_WIDE_SCOPE;
	}

	public PAWS_Settings scoped(String scope)
	{
		PAWS_Settings result = this.clone();
		result.scope = scope;

		return result;
	}

	public Boolean hit(String key)
	{
		return settingsMap.containsKey(key);
	}

	public Boolean miss(String key)
	{
		return !hit(key);
	}

	public String read(String scope, String key)
	{
		PAWS_Settings__c settingsRecord;
		Map<String, PAWS_Settings__c> scopePool = settingsMap.get(key);
		if (scopePool != null)
		{
			settingsRecord = scopePool.get(scope);
		}

		return (settingsRecord != null ? settingsRecord.Value__c : null);
	}

	/**
	* Reads setting in SCOPE_ORDER order.
	*/
	public String read(String key)
	{
		for (Integer i = 0; settingsMap.get(key) != null && i < SCOPE_ORDER.size(); i++)
		{
			if (settingsMap.get(key).containsKey(SCOPE_ORDER.get(i)))
			{
				return settingsMap.get(key).get(SCOPE_ORDER.get(i)).Value__c;
			}
		}

		return null;
	}

	public String write(String key, String value)
	{
		return write(scope, key, value);
	}

	public String write(String scope, String key, String value)
	{
		if (settingsMap.get(key) == null)
		{
			settingsMap.put(key, new Map<String, PAWS_Settings__c>());
		}

		PAWS_Settings__c settingsRecord = settingsMap.get(key).get(scope);
		if (settingsRecord == null)
		{
			settingsRecord = new PAWS_Settings__c(
					Name = EncodingUtil.convertToHex(Crypto.generateAesKey(128)).toUppercase(),
					Name__c = key,
					Scope__c = scope
			);

			settingsMap.get(key).put(scope, settingsRecord);
		}

		return (String) settingsRecord.put(PAWS_Settings__c.Value__c, value);
	}

	/**
	* Saves settings to database
	 */
	public void save()
	{
		List<PAWS_Settings__c> settings = new List<PAWS_Settings__c>();
		for (Map<String, PAWS_Settings__c> each : settingsMap.values())
		{
			if (each != null)
			{
				settings.addAll(each.values());
			}
		}

		upsert settings;
	}
}