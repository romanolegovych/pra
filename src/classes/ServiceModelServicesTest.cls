@isTest
private class ServiceModelServicesTest {

	private static Service_Model__c serviceModel;
    private static Service_Area__c serviceArea;
    private static Service_Function__c serviceFunction;
    private static Service_Task__c serviceTask;
    private static Service_Task_To_Drivers__c serviceTaskToDrivers;

    static void createTestData() {
        PBB_TestUtils testUtils = new PBB_TestUtils();
        serviceModel = testUtils.CreateServiceModelAttributes();
        serviceArea = testUtils.createServiceAreaAttributes();
        serviceFunction = testUtils.createServiceFunctionAttributes();
        serviceTask = testUtils.createServiceTaskAttributes();
        serviceTask.Global__c = true;
        update serviceTask;

        Formula_Name__c billFormula = testUtils.createBillFormulaNames();
        serviceTaskToDrivers = testUtils.createSTtoBF(serviceTask, billFormula);

    	AA_TestUtils ut = new AA_TestUtils();
        ut.createCountry();
    }

	@isTest
	static void getServiceModelsTest() {
		createTestData();
		Test.startTest();
		    List<Service_Model__c> serviceModels = ServiceModelServices.getServiceModels();
    	Test.stopTest();
    	System.assertEquals(serviceModels.get(0).Id, serviceModel.Id);
	}

	@isTest
	static void getServiceAreaByServiceModelIdTest() {
		createTestData();
		Test.startTest();
		    List<Service_Area__c> serviceAreas = ServiceModelServices.getServiceAreaByServiceModelId(serviceModel.Id);
    	Test.stopTest();
    	System.assertEquals(serviceAreas.get(0).Id, serviceArea.Id);
	}

	@isTest
	static void getserviceFunctionsByServiceAreaIdTest() {
		createTestData();
		Test.startTest();
		    List<Service_Function__c> serviceFunctions = ServiceModelServices.getserviceFunctionsByServiceAreaId(serviceArea.Id);
    	Test.stopTest();
    	System.assertEquals(serviceFunctions.get(0).Id, serviceFunction.Id);
	}

	@isTest
	static void getServiceTasksByServiceFunctionIdTest() {
		createTestData();
		Test.startTest();
		    List<Service_Task__c> serviceTasks = ServiceModelServices.getServiceTasksByServiceFunctionId(serviceFunction.Id);
    	Test.stopTest();
    	System.assertEquals(serviceTasks.get(0).Id, serviceTask.Id);
	}

	@isTest
	static void getServiceTaskByIdTest() {
		createTestData();
		Test.startTest();
		    Service_Task__c serviceT = ServiceModelServices.getServiceTaskById(serviceTask.Id);
    	Test.stopTest();
    	System.assertEquals(serviceT.Id, serviceTask.Id);
	}

	@isTest
	static void getServiceTaskToServiceImpactsByServiceTaskIdTest() {
		createTestData();
		Test.startTest();
		    List<Service_Task_To_Service_Impact__c> serviceTaskToServiceImpacts = ServiceModelServices.getServiceTaskToServiceImpactsByServiceTaskId(serviceTask.Id);
    	Test.stopTest();
    	System.assertEquals(serviceTaskToServiceImpacts.size(), 0);
	}

	@isTest
	static void getServiceTaskToDriversListByServiceTaskIdTest() {
		createTestData();
		Test.startTest();
		    List<Service_Task_To_Drivers__c> serviceTaskDs = ServiceModelServices.getServiceTaskToDriversListByServiceTaskId(serviceTask.Id);
    	Test.stopTest();
    	System.assertEquals(serviceTaskDs.get(0).Service_Task__c, serviceTask.Id);
	}

	@isTest
	static void getServiceTaskToDriversByIdTest() {
		createTestData();
		Test.startTest();
		    Service_Task_To_Drivers__c serviceTaskD = ServiceModelServices.getServiceTaskToDriversById(serviceTaskToDrivers.Id);
    	Test.stopTest();
    	System.assertEquals(serviceTaskD.Service_Task__c, serviceTask.Id);
	}

	@isTest
	static void getCountryTest() {
		createTestData();
		Test.startTest();
		    List<Country__c> countries = ServiceModelServices.getCountry();
    	Test.stopTest();
    	System.assert(countries.size() > 0);
	}
}