/** Implements the Service Layer of the object LaboratorySamples__c
 * @author	Dimitrios Sgourdos
 * @version	27-Nov-2013
 */
public with sharing class LaboratorySamplesService {
	
	/** Retrieve the Laboratory Samples for the laboratory design.
	 * @author	Dimitrios Sgourdos
	 * @version 04-Nov-2013
	 * @param	projectId			The id of the current project
	 * @param	studiesList			The project studies that are assigned with AN analysis
	 * @return	The Laboratory Samples wrapper list for the laboratory design.
	 */
	public static List<LaboratorySamplesDesignWrapper> getLaboratorySamplesDesignContainer(String projectId, List<Study__c> studiesList) {
		List<LaboratorySamplesDesignWrapper> results = new List<LaboratorySamplesDesignWrapper>();
		
		// Retrieve the Laboratory Samples of the project
		List<LaboratorySamples__c> labSamplesList = LaboratorySamplesDataAccessor.getByProjectId(projectId);
		
		// Create wrapper list
		for(LaboratorySamples__c tmpSample : labSamplesList) {
			Boolean includeFlag = false;
			LaboratorySamplesDesignWrapper newItem = new LaboratorySamplesDesignWrapper();
			newItem.labSample = tmpSample;
			newItem.shipmentNumber = String.valueOf(tmpSample.ShipmentNumber__c);
			
			for(Study__c tmpStudy : studiesList) {
				String tmpStr = tmpSample.ListOfStudies__c;
				SelectedStudyWrapper selectionItem = new SelectedStudyWrapper();
				selectionItem.studyId = tmpStudy.Id;
				selectionItem.StudyLabel = tmpStudy.Code__c.substringAfterLast('-');
				if(String.IsNotBlank(tmpStr) && tmpStr.contains(tmpStudy.Id)) {
					selectionItem.selected = true;
					includeFlag   = true;
				} else {
					selectionItem.selected = false;
				}
				newItem.studiesSelectionList.add(selectionItem);
			}
			
			// Include only the Laboratory Samples that are assigned with the selected studies
			if(includeFlag) {
				results.add(newItem);
			}
		}
		
		return results;
	}
	
	
	/** Create a LaboratorySamplesDesignWrapper Instance without study selection.
	 * @author	Dimitrios Sgourdos
	 * @version 04-Nov-2013
	 * @param	shipmentNumber		The shipment number that the instance will be associated with
	 * @param	studiesList			The project studies that are assigned with AN analysis
	 * @param	projectId			The id of the current project
	 * @return	The LaboratorySamplesDesignWrapper Instance.
	 */
	public static LaboratorySamplesDesignWrapper createLaboratorySamplesDesignWrapperInstance(String shipmentNumber,
																							List<Study__c> studiesList,
																							String projectId) {
		LaboratorySamplesDesignWrapper result = new LaboratorySamplesDesignWrapper();
		
		// Add the laboratory sample and the shipment number
		LaboratorySamples__c labSample = new LaboratorySamples__c(Project__c=projectId, shipmentNumber__c=Decimal.valueOf(shipmentNumber));
		result.labSample = labSample;
		result.shipmentNumber = shipmentNumber;
		
		// Add the studiesSelectionList
		for(Study__c tmpStudy : studiesList) {
			SelectedStudyWrapper selectionItem = new SelectedStudyWrapper();
			selectionItem.studyId	 = tmpStudy.Id;
			selectionItem.StudyLabel = tmpStudy.Code__c.substringAfterLast('-');
			selectionItem.selected	 = false;
			result.studiesSelectionList.add(selectionItem);
		}
		
		return result;
	}
	
	
	/** Count how many shipments there are in a LaboratorySamplesDesignWrapper List.
	 * @author	Dimitrios Sgourdos
	 * @version 04-Nov-2013
	 * @param	samplesWrapperList	The LaboratorySamplesDesignWrapper List
	 * @return	How many shipments there are in the LaboratorySamplesDesignWrapper List.
	 */
	public static Integer countShipmentsInLaboratorySamplesDesignWrapperList(List<LaboratorySamplesDesignWrapper> samplesWrapperList) {
		Integer result = 0;
		Decimal shipmentNum = -1;
		
		for(LaboratorySamplesDesignWrapper tmpItem : samplesWrapperList) {
			Decimal tmpDec = tmpItem.labSample.shipmentNumber__c;
			if(shipmentNum != tmpDec) {
				shipmentNum = tmpDec;
				result++;
			}
		}
		
		return result;
	}
	
	
	/** Add a sample in the sample shipments lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 31-Oct-2013
	 * @param	initialWrapperList	The initial wrapper list that holds the sample shipments lab design
	 * @param	firstSampleIndex	The index of the first sample of the shipment in the wrapper list, that we want to add a new sample
	 * @param	studiesList			The user selected studies that are assigned with AN analysis
	 * @param	projectId			The id of the current project
	 * @return	The updated wrapper list that holds the sample shipments lab design.
	 */
	public static List<LaboratorySamplesDesignWrapper> addSampleToLabDesign(List<LaboratorySamplesDesignWrapper> initialWrapperList,
																			Integer firstSampleIndex,
																			List<Study__c> studiesList,
																			String projectId) {
		// Check if the firstSampleIndex is well defined
		if(firstSampleIndex<0 || firstSampleIndex >= initialWrapperList.size()) {
			return initialWrapperList;
		}
		
		// Create the LaboratorySamplesDesignWrapper instance
		String shipmentNumber = String.valueOf(initialWrapperList[firstSampleIndex].labSample.shipmentNumber__c);
		LaboratorySamplesDesignWrapper newItem = createLaboratorySamplesDesignWrapperInstance(shipmentNumber, studiesList, projectId);
		
		// Find the position in the wrapper that the new sample must be inserted
		for(Integer i=firstSampleIndex; i<initialWrapperList.size(); i++) {
			if(initialWrapperList[firstSampleIndex].labSample.shipmentNumber__c != initialWrapperList[i].labSample.shipmentNumber__c) {
				// Add the wrapper line
				initialWrapperList.add(i, newItem);
				return initialWrapperList;
			}
		}
		
		// Add to the end
		initialWrapperList.add(newItem);
		return initialWrapperList;
	} 
	
	
	/** Add a shipment in the sample shipments lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 04-Nov-2013
	 * @param	initialWrapperList	The initial wrapper list that holds the sample shipments lab design
	 * @param	studiesList			The user selected studies that are assigned with AN analysis
	 * @param	projectId			The id of the current project
	 * @return	The updated wrapper list that holds the sample shipments lab design.
	 */
	public static List<LaboratorySamplesDesignWrapper> addShipmentToLabDesign(List<LaboratorySamplesDesignWrapper> initialWrapperList, 
																			List<Study__c> studiesList,
																			String projectId) {
		// Create the LaboratorySamplesDesignWrapper instance
		String shipmentNumber = String.valueOf(countShipmentsInLaboratorySamplesDesignWrapperList(initialWrapperList) + 1);
		
		LaboratorySamplesDesignWrapper newItem = createLaboratorySamplesDesignWrapperInstance(shipmentNumber, studiesList, projectId);
		initialWrapperList.add(newItem);
		
		return initialWrapperList;
	}
	
	
	/** Remove a laboratory sample from the sample shipments lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 11-Nov-2013
	 * @param	initialWrapperList	The initial wrapper list that holds the sample shipments lab design
	 * @param	removeSampleIndex	The index in the list of the laboratory samples that will be removed
	 * @return	The updated sample shipments lab design.
	 */
	public static List<LaboratorySamplesDesignWrapper> removeSampleFromLabdesign(List<LaboratorySamplesDesignWrapper> initialWrapperList, 
																				Integer removeSampleIndex) {
		// Check if the removeSampleIndex is valid
		if(removeSampleIndex<0 || removeSampleIndex>=initialWrapperList.size()) {
			return initialWrapperList;
		}
		
		// Update the next combination (in case it belongs to the same slot) with the inserted slot number to keep the inserted slot number 
		// if the deleted combination is first child
		Integer nextIndex = removeSampleIndex + 1;
		if(nextIndex<initialWrapperList.size()
			&& initialWrapperList[nextIndex].labSample.ShipmentNumber__c == initialWrapperList[removeSampleIndex].labSample.ShipmentNumber__c
			) {
				initialWrapperList[nextIndex].shipmentNumber = initialWrapperList[removeSampleIndex].shipmentNumber;
		}
		
		// remove laboratory sample
		initialWrapperList.remove(removeSampleIndex);
		return initialWrapperList;
	}
	
	
	/** Create a list of study Ids depend on new selection.
	 * @author	Dimitrios Sgourdos
	 * @version 04-Nov-2013
	 * @param	previousListOfStudies	The initial list of studies
	 * @param	selectionStudiesList	The new selection of studies
	 * @return	The list of study Ids.
	 */
	public static String adjustListOfStudiesForSample(List<SelectedStudyWrapper> selectionStudiesList) {
		String result = '';
		
		for(SelectedStudyWrapper tmpItem : selectionStudiesList) {
			if(tmpItem.selected) {
				result += ':' + tmpItem.studyId;
			}
		}
		
		result = result.removeStart(':');
		
		return result;
	}
	
	
	/** Adjust the samples of a samples wrapper list. The shipmentNumber of all records must match the shipment number 
	 *	of the first sample that belongs to the same shipment. The sequenceNumber__c of the samples must match the sample 
	 *	sequence in the shipment and the ListOfStudies__c must match the new selection. 
	 * @author	Dimitrios Sgourdos
	 * @version 04-Nov-2013
	 * @param	initialList			The initial wrapper list that holds the sample shipments lab design
	 * @return	The updated sample shipments lab design.
	 */
	public static List<LaboratorySamplesDesignWrapper> adjustSamplesInSamplesWrapperList(List<LaboratorySamplesDesignWrapper> initialList) {
		Decimal previousDataShipmentNum;
		String insertedShipmentNum;
		Integer counter = 1;
		
		for(LaboratorySamplesDesignWrapper tmpItem : initialList) {
			if(previousDataShipmentNum != tmpItem.labSample.ShipmentNumber__c ) {
				previousDataShipmentNum = tmpItem.labSample.ShipmentNumber__c;
				insertedShipmentNum 	= tmpItem.shipmentNumber;
				counter = 1;
			}
			tmpItem.shipmentNumber = insertedShipmentNum;
			tmpItem.labSample.SequenceNumber__c = counter;
			tmpItem.labSample.ListOfStudies__c	= adjustListOfStudiesForSample(tmpItem.studiesSelectionList);
			counter++;
		}
		
		return initialList;
	}
	
	
	/** Sort a LaboratorySamplesDesignWrapper list by the shipmentNumber and renumber the duplicates. 
	 * @author	Dimitrios Sgourdos
	 * @version 05-Nov-2013
	 * @param	initialList			The initial wrapper list that holds the sample shipments lab design
	 * @return	The updated LaboratorySamplesDesignWrapper list.
	 */
	public static List<LaboratorySamplesDesignWrapper> sortSamplesDesignWrapperByShipmentNumber(List<LaboratorySamplesDesignWrapper> initialList) {
		List<LaboratorySamplesDesignWrapper> finalList = new List<LaboratorySamplesDesignWrapper>();
		Set<Decimal> shipmentNumbersSet = new Set<Decimal>();
		Map<String, List<LaboratorySamplesDesignWrapper>> sortingMap = new Map<String, List<LaboratorySamplesDesignWrapper>>();
		
		// Create the temporary map and the shipment numbers set
		for(LaboratorySamplesDesignWrapper wrapperItem : initialList) {
			// Set
			shipmentNumbersSet.add(Decimal.valueOf(wrapperItem.shipmentNumber));
			// Map
			List<LaboratorySamplesDesignWrapper> tmpWrapperList = new List<LaboratorySamplesDesignWrapper>();
			if(sortingMap.containsKey(wrapperItem.shipmentNumber)) {
				tmpWrapperList = sortingMap.remove(wrapperItem.shipmentNumber);
			}
			tmpWrapperList.add(wrapperItem);
			sortingMap.put(wrapperItem.shipmentNumber, tmpWrapperList);
		}
		
		// Sort the shipment numbers
		List<Decimal> shipmentNumsList = new List<Decimal>();
		shipmentNumsList.addAll(shipmentNumbersSet);
		shipmentNumsList.sort();
		
		// Create the final sorted list
		Integer counter = 1;
		for(Decimal tmpStr : shipmentNumsList) {
			String key = String.valueOf(tmpStr);
			if( sortingMap.containsKey(key) ) {
				List<LaboratorySamplesDesignWrapper> tmpWrapperList = sortingMap.get(key);
				for(LaboratorySamplesDesignWrapper wrapperItem : tmpWrapperList) {
					wrapperItem.shipmentNumber = String.valueOf(counter);
					finalList.add(wrapperItem);
					counter++;
				}
			}
		}
		
		return finalList;
	}
	
	
	/** Extract the lab samples from the inserted lab design. 
	 * @author	Dimitrios Sgourdos
	 * @version 05-Nov-2013
	 * @param	initialList			The initial wrapper list that holds the sample shipments lab design
	 * @return	The lab samples from the inserted lab design.
	 */
	public static List<LaboratorySamples__c> extractLabSamplesFromLabDesign(List<LaboratorySamplesDesignWrapper> initialList) {
		List<LaboratorySamples__c> results = new List<LaboratorySamples__c>();
		
		// Adjust the wrapper list to be updated with the user's entries
		initialList = adjustSamplesInSamplesWrapperList(initialList);
		
		// Create a map with key the ShipmentNumber__c of the samples and value the list of Samples. 
		// Also retrieve the first samples of each shipment and store them to the firstSamplesOfShipmentsList
		List<LaboratorySamplesDesignWrapper> firstSamplesOfShipmentsList = new List<LaboratorySamplesDesignWrapper>();
		Map<String,List<LaboratorySamples__c>> labSamplesMap = new Map<String, List<LaboratorySamples__c>>();
		List<LaboratorySamples__c> tmpLabSamplesList = new List<LaboratorySamples__c>();	
		Decimal previousDataShipmentNum;
		
		for(LaboratorySamplesDesignWrapper wrapperItem : initialList) {
			// Add to the temporary list for creating the map
			tmpLabSamplesList.add(wrapperItem.labSample);
			// Create the list of the first samples of each shipment
			if(previousDataShipmentNum != wrapperItem.labSample.ShipmentNumber__c ) {
				previousDataShipmentNum = wrapperItem.labSample.ShipmentNumber__c;
				firstSamplesOfShipmentsList.add(wrapperItem);
			}
		}
		
		labSamplesMap = (Map<String,List<LaboratorySamples__c>>) BDT_Utils.mapSObjectListOnKey(tmpLabSamplesList, new List<String>{'ShipmentNumber__c'});
		
		// Sort the first samples of each shipment by the inserted shipment numbers and renumber the duplicates
		firstSamplesOfShipmentsList = sortSamplesDesignWrapperByShipmentNumber(firstSamplesOfShipmentsList);
		
		// Create the results from labSamplesMap and firstSamplesOfShipmentsList
		for(LaboratorySamplesDesignWrapper wrapperItem : firstSamplesOfShipmentsList) {
			String key = String.valueOf(wrapperItem.labSample.ShipmentNumber__c);
			if( labSamplesMap.containsKey(Key) ) {
				// Update firstly the renumbered shipmentNumber in the retrieved samples from the map
				List<LaboratorySamples__c> tmpList = new List<LaboratorySamples__c>();
				tmpList = labSamplesMap.get(Key);
				for(LaboratorySamples__c tmpSample : tmpList) {
					tmpSample.ShipmentNumber__c = Decimal.valueOf(wrapperItem.shipmentNumber);
				}
				// Add to the results the fully updated lab samples
				results.addAll(tmpList);
			}
		}
		
		return results;
	}
	
	
	/** Find which lab samples from the given list don't have study selection. 
	 * @author	Dimitrios Sgourdos
	 * @version 05-Nov-2013
	 * @param	initialList			The initial list of Laboratory Samples
	 * @return	The lab samples from the given list which don't have study selection.
	 */
	public static List<LaboratorySamples__c> getSamplesWithoutStudySelection(List<LaboratorySamples__c> initialList) {
		List<LaboratorySamples__c> results = new List<LaboratorySamples__c>();
		for(LaboratorySamples__c tmpSample : initialList) {
			if(String.IsBlank(tmpSample.ListOfStudies__c)) {
				results.add(tmpSample);
			}
		}
		return results;
	}
	
	
	/** Save the given LaboratorySamples in case they have study selection. 
	 * @author	Dimitrios Sgourdos
	 * @version 05-Nov-2013
	 * @param	initialList			The initial list of Laboratory Samples
	 * @return	If the save was successful or not.
	 */
	public static Boolean saveLaboratorySamples(List<LaboratorySamples__c> initialList) {
		List<LaboratorySamples__c> saveList = new List<LaboratorySamples__c>();
		
		// Save only the samples that have a study selection
		for(LaboratorySamples__c tmpSample : initialList) {
			if( ! String.IsBlank(tmpSample.ListOfStudies__c) ) {
				saveList.add(tmpSample);
			}
		}
		
		try{
			upsert saveList;
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}
	
	
	/** Delete the given LaboratorySamples in case they have id. 
	 * @author	Dimitrios Sgourdos
	 * @version 05-Nov-2013
	 * @param	initialList			The initial list of Laboratory Samples
	 * @return	If the deletion was successful or not.
	 */
	public static Boolean deleteLaboratorySamples(List<LaboratorySamples__c> initialList) {
		List<LaboratorySamples__c> deletedList = new List<LaboratorySamples__c>();
		
		// delete only the samples that have id
		for(LaboratorySamples__c tmpSample : initialList) {
			if(tmpSample.Id != NULL) {
				deletedList.add(tmpSample);
			}
		}
		
		try{
			delete deletedList;
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}
	
	
	/** Get the available shipments numbers that can be associated to the slots for analysis lab design for the given studies. 
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 * @param	projectId			The id of the current project
	 * @param	studiesList			The given syudies
	 * @return	The available shipments numbers that can be associated to the slots for analysis.
	 */
	public static List<ShipmentSelectionWrapper> getAvailableShipmentSelection(String projectId, List<Study__c> studiesList) {
		List<ShipmentSelectionWrapper> results = new List<ShipmentSelectionWrapper>();
		
		// Read the data (Firstly save to a set to get the distinct values)
		Set<String> shipmentNumbersSet = new Set<String>();
		List<LaboratorySamples__c> labSamplesList = LaboratorySamplesDataAccessor.getByProjectId(projectId);
		
		for(LaboratorySamples__c tmpItem : labSamplesList) {
			if(tmpItem.ShipmentNumber__c != NULL) {
				String tmpNumStr = String.valueOf(tmpItem.ShipmentNumber__c);
				shipmentNumbersSet.add( BDT_Utils.lPad(tmpNumStr, 2, '0') );
			}
		}
		
		// Create the wrapper
		List<String> shipmentNumbersList = new List<String>();
		shipmentNumbersList.addAll(shipmentNumbersSet);
		shipmentNumbersList.sort();
		
		for(String tmpNumStr : shipmentNumbersList) {
			ShipmentSelectionWrapper newItem = new ShipmentSelectionWrapper();
			newItem.shipmentNumber = tmpNumStr;
			newItem.selectedFlag   = false;
			results.add(newItem);
		}
		
		return results;
	}
	
	
	/** Update the available shipments numbers that can be associated to the slots for analysis lab design if they are selected
	 *  in the given slot. 
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 * @param	initialList			The available shipment numbers wrapperlist
	 * @param	labSlot				The slot that will be associated with the available shipment numbers
	 * @return	The updated available shipments numbers that can be associated to the slots for analysis.
	 */
	public static List<ShipmentSelectionWrapper> updateShipmentSelectionBySelectedInSlot(List<ShipmentSelectionWrapper> initialList, 
																						LaboratoryAnalysisSlots__c labSlot) {
		// Create a set with the selected shipment numbers in the given slot
		Set<String> shipmentNumbersSet = new Set<String>();
		
		if( String.isNotBlank(labSlot.ListOfShipments__c) ) {
			List<String> shipmentNumbersList = new List<String>();
			shipmentNumbersList = labSlot.ListOfShipments__c.split(':');
			shipmentNumbersSet.addAll(shipmentNumbersList);
		}
		
		// Update the wrapper list
		for(ShipmentSelectionWrapper tmpItem : initialList) {
			tmpItem.selectedFlag = shipmentNumbersSet.contains(tmpItem.shipmentNumber);
		}
		
		return initialList;
	}
	
	
	/** Save the selected list of shipments  the available shipments numbers that can be associated to the slots for analysis lab design if they are selected
	 *  in the given slot.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 * @param	selectionList		The available shipment numbers wrapperlist
	 * @param	labSlot				The slot that will be associated with the available shipment numbers
	 * @return	The updated LaboratoryAnalysisSlots__c with the shipment selection.
	 */
	public static LaboratoryAnalysisSlots__c saveShipmentsAssociationToSlot(List<ShipmentSelectionWrapper> selectionList, 
																			LaboratoryAnalysisSlots__c labSlot) {
		String listOfShipments = '';
		
		for(ShipmentSelectionWrapper tmpItem : selectionList) {
			if(tmpItem.selectedFlag) {
				listOfShipments += ':' + tmpItem.shipmentNumber;
			}
		}
		
		listOfShipments = listOfShipments.removeStart(':');
		labSlot.ListOfShipments__c = listOfShipments;
		
		return labSlot;
	}
	
	
	/** Update the Map of the associated shipment numbers with key the initial number and value the final number.
	 * @author	Dimitrios Sgourdos
	 * @version 14-Nov-2013
	 * @param	initialMap			The map that will be updated
	 * @param	samplesWrapperList	The wrapper list that holds the Sample Shipments Design
	 * @return	The updated map with the final associated shipment numbers.
	 */
	public static Map<String,String> updateShipmentNumbersMapWithNewNumbers(Map<String,String> initialMap, 
																			List<LaboratorySamplesDesignWrapper> samplesWrapperList) {
		for(LaboratorySamplesDesignWrapper tmpItem : samplesWrapperList) {
			String initialNumStr = BDT_Utils.lPad(String.valueOf(tmpItem.labSample.ShipmentNumber__c), 2, '0');
			String finalNumStr	 = BDT_Utils.lPad(tmpItem.shipmentNumber, 2, '0');
			initialMap.put(initialNumStr, finalNumStr);
		}
		
		return initialMap;
	}
	
	
	/** Clone the shipment numbers of LaboratorySamplesDesignWrapper list to a new list.
	 * @author	Dimitrios Sgourdos
	 * @version 14-Nov-2013
	 * @param	initialList			The wrapper list from which the shipment numbers will be cloned
	 * @return	The new wrapper list that holds only the cloned shipment numbers
	 */
	public static List<LaboratorySamplesDesignWrapper> cloneSamplesWrapperListShipmentNumbers(List<LaboratorySamplesDesignWrapper> initialList) {
		List<LaboratorySamplesDesignWrapper> results = new List<LaboratorySamplesDesignWrapper>();
		
		for(LaboratorySamplesDesignWrapper tmpItem : initialList) {
			LaboratorySamplesDesignWrapper newItem = new LaboratorySamplesDesignWrapper();
			newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=tmpItem.labSample.ShipmentNumber__c);
			newItem.shipmentNUmber = tmpItem.shipmentNumber;
			results.add(newItem);
		}
		
		return results;
	}
	
	
	/** Get the first samples records from a Sample Shipment lab design sorted and renumbered.
	 * @author	Dimitrios Sgourdos
	 * @version 14-Nov-2013
	 * @param	initialList			The wrapper list from which the new list will be created
	 * @return	The new wrapper list that holds only the first samples records from a Sample Shipment lab design sorted and renumbered
	 */
	public static List<LaboratorySamplesDesignWrapper> getUpdatedFirstSamplesRecordsFromSamplesDesign(List<LaboratorySamplesDesignWrapper> initialList) {
		List<LaboratorySamplesDesignWrapper> results = new List<LaboratorySamplesDesignWrapper>();
		
		// Create the list of the first samples of each shipment
		Decimal previousDataShipmentNum;
		for(LaboratorySamplesDesignWrapper wrapperItem : initialList) {
			if(previousDataShipmentNum != wrapperItem.labSample.ShipmentNumber__c ) {
				previousDataShipmentNum = wrapperItem.labSample.ShipmentNumber__c;
				results.add(wrapperItem);
			}
		}
		
		// Sort the first samples of each shipment by the inserted shipment numbers and renumber the duplicates
		results = sortSamplesDesignWrapperByShipmentNumber(results);
		
		return results;
	}
	
	
	/** Filter the sample shipments design resords that belong to the given desing and it has shipment numbers in the given ones,
	 *	or study selection in the given studies.
	 * @author	Dimitrios Sgourdos
	 * @version 27-Nov-2013
	 * @param	initialList			The wrapper list that keep the sample shipments design and from which the new list will be created
	 * @param	usedShipNumbers		The shipment numbers that the retrieved data must meet
	 * @param	studiesList			The studies that the retrieved data must have a selection associated with
	 * @return	The new wrapper list that holds the filtered sample shipments design.
	 */
	public static List<LaboratorySamplesDesignWrapper> filterSamplesDesignByShipmentNumberOrStudySelection(
																			List<LaboratorySamplesDesignWrapper> initialList,
																			Set<Decimal> usedShipNumbers,
																			List<Study__c> studiesList) {
		// Declare variables and initialize data
		List<LaboratorySamplesDesignWrapper> results = new List<LaboratorySamplesDesignWrapper>();
		
		Set<Object> studiesIdsSet = COM_SObjectUtils.getSetFromList (studiesList, 'Id');
		
		// Keep only the records that meet the criteria
		for(LaboratorySamplesDesignWrapper tmpItem : initialList) {
			// If there isn't meaning, skip this step
			if(tmpItem.labSample==NULL || tmpItem.labSample.ShipmentNumber__c==NULL || tmpItem.studiesSelectionList.size()==0) {
				continue;
			}
			
			// Check firstly by shipment number
			Decimal tmpDec = tmpItem.labSample.ShipmentNumber__c.setScale(0);
			if( usedShipNumbers.contains(tmpDec) ) {
				results.add(tmpItem);
			} else { // Check by study selection
				for(SelectedStudyWrapper tmpSel : tmpItem.studiesSelectionList) {
					if( tmpSel.selected  &&  studiesIdsSet.contains(tmpSel.studyId) ) {
						results.add(tmpItem);
						break; // break the interior for
					}
				}
			}
		}
		
		return results;
	}
	
	
	/**	This class keeps a laboratory sample with the study selection.
	 * @author	Dimitrios Sgourdos
	 * @version	04-Nov-2013
	 */
	public class LaboratorySamplesDesignWrapper {
		public LaboratorySamples__c			labSample 				{get;set;}
		public String						shipmentNumber			{get;set;}
		public List<SelectedStudyWrapper>	studiesSelectionList	{get;set;}
		
		// The constructor of the sub-class
		public LaboratorySamplesDesignWrapper() {
			studiesSelectionList = new List<SelectedStudyWrapper>();
		}
		
		// Calculate the total, as the field TotalNumberOfSamples is updated only if the record is saved
		public String getLabSampleTotalNumber() {
			// Correct the values in case they are null
			labSample.ActiveNumberOfSamples__c  = (labSample.ActiveNumberOfSamples__c==NULL)?  0 : labSample.ActiveNumberOfSamples__c;
			labSample.PlaceboNumberOfSamples__c = (labSample.PlaceboNumberOfSamples__c==NULL)? 0 : labSample.PlaceboNumberOfSamples__c;
			labSample.BackupNumberOfSamples__c  = (labSample.BackupNumberOfSamples__c==NULL)?  0 : labSample.BackupNumberOfSamples__c;
			labSample.OtherNumberOfSamples__c   = (labSample.OtherNumberOfSamples__c==NULL)?   0 : labSample.OtherNumberOfSamples__c;
			// Calculate Result
			return BDT_Utils.formatNumberWithSeparators(labSample.ActiveNumberOfSamples__c + labSample.PlaceboNumberOfSamples__c +
														labSample.BackupNumberOfSamples__c + labSample.OtherNumberOfSamples__c);
		}
		
		// Retrieve the label for study selection
		public String getStudySelection() {
			String results = '';
			for(SelectedStudyWrapper tmpItem : studiesSelectionList) {
				if(tmpItem.selected) {
					results += ' / ' + tmpItem.StudyLabel;
				}
			}
			results = results.removeStart(' / ');
			return results;
		}
	}
	
	
	/**	This class keeps a study Id and if it is selected by a sample in lab design.
	 * @author	Dimitrios Sgourdos
	 * @version	31-Oct-2013
	 */
	public class SelectedStudyWrapper {
		public String	studyId		{get;set;}
		public String	StudyLabel	{get;set;}
		public Boolean	selected	{get;set;}
	}
	
	
	/**	This class keeps a shipment number and if it is selected in the association with the current slot.
	 * @author	Dimitrios Sgourdos
	 * @version	13-Nov-2013
	 */
	public class ShipmentSelectionWrapper {
		public String  shipmentNumber {get;set;}
		public Boolean selectedFlag   {get;set;}
	}
}