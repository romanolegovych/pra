public with sharing class BDT_Utils {

	public static boolean getUserIsAdministrator() {
		try {
			Profile p =[Select p.Name, p.Id 
						From Profile p 
						where id = :UserInfo.getProfileId()
						and name in ('System Administrator','Application Administrator')];
			return true;
		} catch (Exception e) {}
		return false;
	}

	public static String lPad(String text,Integer length,String paddingCharacter){
		String result = text;
		while (result.length() < length) {
			result = paddingCharacter + result;
		}
		return result;
	}

	public static String rPad(String text,Integer length,String paddingCharacter){
		String result = text;
		while (result.length() < length){
			result = result + paddingCharacter;
			}
		return result;
	}

	public static String removeLeadingZerosFromCat(String Category){
		// change codes like 001.002.003 to 1.2.3
	try{
		Category = Category.replaceAll('^0+','');
		Category = Category.replaceAll('\\.0+','.');}
		catch(Exception e) {

			}
		return Category;
	}

	public static String removeTailingZerosFromDecimals(String value) {
			return value.replaceAll('[.]?(?<=[.][1-9]{0,10})[0]*$','');
	}


	public static ServiceCategory__c setServiceCategoryParent (ServiceCategory__c sc) {
		try{
			if (sc == null) {
				return sc;
			} else if (sc.code__c.lastIndexOf('.') == -1) {
				sc.ParentCategory__c = null;
			} else {
				String parentcode = sc.code__c.substring(0,sc.code__c.lastIndexOf('.'));
			ServiceCategory__c sc_parent = [select id
											from   ServiceCategory__c
											where  code__c = :parentcode
											and	BDTDeleted__c = false];
			sc.ParentCategory__c = sc_parent.id;
		}
			return sc;
		} catch (Exception e) {
			return sc;
		}
	}

	public static String addLeadingZerosToCat(String Category) {
		String[] str = Category.split('\\.');
		String result = '';
		for (Integer i=0; i < str.size(); i++) {
			if (i>0) {result=result+'.';}
			result=result+lpad(str[i],3,'0');
		}

		return result;
	}

	public static Client_Project__c getProject(String projectId, Boolean showDeleted){
		Client_Project__c singleProject;

			try{
				singleProject = [Select c.Title__c, c.Therapeutic_Indication__c, c.Therapeutic_Area__c, c.Status__c, c.Standard_rate_year__c,
									c.Project_Currency__c, c.Phase__c, c.PRA_Protocol__c, c.PRA_Project_Id__c, c.OwnerId, c.Name,
									c.Indication_Group__c, c.Id, c.Drug_Administered__c, c.Currency_exchange_lock_date__c, c.Cumulative_Cost__c,
									c.Contracted_Start_Date__c, c.Contracted_End_Date__c, c.Contract_type__c, c.Code__c,
									c.Client__c, c.Client__r.Name, c.Client__r.Sponsor__c, c.Client__r.Sponsor__r.Name,
									Financial_Status__c,Project_Description__c,
									(Select Name, Project__c, Code__c, Title__c, Legacy_Code__c, Business_Unit__c, Business_Unit__r.Name, Business_Unit__r.Abbreviation__c,Status__c, Protocol_Title__c
										From Studies__r
										where ((BDTDeleted__c  = :showDeleted) or (BDTDeleted__c  = false))
										order by Code__c asc)
									From Client_Project__c c
									Where c.id = :projectId
									and ((BDTDeleted__c  = :showDeleted) or (BDTDeleted__c  = false))
									LIMIT 1];

			}catch(QueryException e){
					singleProject = new Client_Project__c();
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Warning, 'Project Id could not be found. You can create a new Project.');
					ApexPages.addMessage(msg);
			}

		return singleProject;

	}

	public static ClinicalDesign__c getDesign(String designId, String projectId){
		ClinicalDesign__c design;

		try{
				design = [Select d.Project__c, d.Id, d.Design_Name__c, d.name,
							(Select Id, Name, ClinicalDesign__c, Arm_Number__c, Description__c From Arms__r)
							From ClinicalDesign__c d
							where id =:designId];
			}catch(Exception e){
				design = new ClinicalDesign__c(Project__c=projectId);
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Design Id could not be found. ');
				ApexPages.addMessage(msg);
		}

		return design;

	}

	public static void sortList(List<Sobject> items, String sortField, String order){

		List<Sobject> resultList = new List<Sobject>();

		//Create a map that can be used for sorting
		Map<object, List<Sobject>> objectMap = new Map<object, List<Sobject>>();

		for(Sobject ob : items){
				if(objectMap.get(ob.get(sortField)) == null){  // For non Sobject use obj.ProperyName
					objectMap.put(ob.get(sortField), new List<Sobject>());
				}
				objectMap.get(ob.get(sortField)).add(ob);
		}
		//Sort the keys
		List<object> keys = new List<object>(objectMap.keySet());
		keys.sort();

		for(object key : keys){
			resultList.addAll(objectMap.get(key));
		}

		//Apply the sorted values to the source list
		items.clear();
		if(order.toLowerCase() == 'asc'){
			for(Sobject ob : resultList){
				items.add(ob);
			}
		}else if(order.toLowerCase() == 'desc'){
			for(integer i = resultList.size()-1; i >= 0; i--){
				items.add(resultList[i]);
			}
		}
	}

	public static String getThisPageName(String url){
		String currentPage;

		String[] str= url.split('/');
		currentPage = str[2];

		if(currentPage.contains('?')){

			String[] str1 = currentPage.split('\\?');
			currentPage = str1[0];
		}

		return currentPage;

	}

	public static void cleanProjectPreferences(String projectId, boolean deleteProjectId) {
		List<String> prefToDelete = new List<String>();

		prefToDelete.add('SelectedStudies');
		prefToDelete.add('designId');
		prefToDelete.add('populationId');

		if(deleteProjectId){
			prefToDelete.add('SelectedProject');
		} else {
			// project is not deleted, then set the parameter
			setPreference('SelectedProject',projectId);
		}

		cleanPreferences(prefToDelete);
	}

	public static List<Study__c> readSelectedStudies () {
		// studies are selected in the sidebar, these are stored in user preferences
		try {
			List<String> selectedStudies =  BDT_Utils.getPreference('SelectedStudies').split(':');
			Set<ID> studieIDs = new Set<ID>();
			for (String ss:selectedStudies){studieIDs.add(ss);}
			List<Study__c> StudiesToDisplay = [select Title__c, Status__c, Sponsor_Code__c, Protocol_Title__c,
														Project__c, Name, Legacy_Code__c, BDTDeleted__c, Id, Code__c,
														Business_Unit__c, Business_Unit__r.Name
												from 	Study__c
												where 	id = :studieIDs
												and 	BDTDeleted__c = false
												order by code__c];
			return StudiesToDisplay;
		} catch (Exception e) {
			return new List<Study__c>();
		}
	}

	public static List<Study__c> readSelectedStudies (String ProjectId) {
		// studies are selected in the sidebar, these are stored in user preferences
		List<String> selectedStudies = new List<String>();
		List<Study__c> StudiesToDisplay = new List<Study__c>();
		try {
			selectedStudies =  BDT_Utils.getPreference('SelectedStudies').split(':');

			Set<ID> studieIDs = new Set<ID>();

			for(String ss:selectedStudies){
				studieIDs.add(ss);
			}

			StudiesToDisplay = [Select s.Title__c, s.Status__c, s.Sponsor_Code__c, s.Protocol_Title__c,
										s.Project__c, s.Name, s.Legacy_Code__c, s.BDTDeleted__c, s.Id, s.Code__c,
										s.Business_Unit__c, Business_Unit__r.Name
								From 	Study__c s
								where 	id = :studieIDs
								and 	Project__c=:ProjectId
								and 	BDTDeleted__c = false
								order by code__c];

		} catch (Exception e) {

		}
		return StudiesToDisplay;
	}

	public static Currency__c getCurrency(String CurrencyID) {
		try {
			return [Select c.OfficialName__c, c.Name, c.Id
					From 	Currency__c c
					where   id = :CurrencyID];
		} catch (Exception e) {return null;}
	}

	public static Currency__c getCurrencyForBusinessUnit(String BusinessUnitID) {
		try {
			return [Select c.OfficialName__c, c.Name, c.Id
					From 	Currency__c c
					where   id in (select currency__c
									from business_unit__c
									where id = :BusinessUnitID)];
		} catch (Exception e) {return null;}
	}

	public static List<SObject> sortList (List<SObject> unsorted, String fieldName) {
		List<String> sortingList = new List<String>();

		for (SObject s : unsorted) {
			sortingList.add(string.valueOf(s.get(fieldName)));
		}

		sortingList.sort();

		List<SObject> t = unsorted.clone();
		t.clear();

		Integer iteratornum;
		Integer iteratortag;

		for (String sortItem: sortingList){
			iteratornum = 0;
			for (SObject s : unsorted) {
				if (string.valueOf(s.get(fieldName)) ==  sortItem) {
					t.add(s);
					iteratortag = iteratornum;
					break;
				}
				iteratornum++;
			}
			unsorted.remove(iteratortag);
		}

		return t;
	}
	
	public static List<SelectOption> sortSelectList (List<SelectOption> values) {
		List<String> sortingList = new List<String>();
		List<SelectOption> result = new List<SelectOption>();

		for (SelectOption s : values) {
			sortingList.add(s.getLabel());
		}

		sortingList.sort();

		Integer iteratornum;
		Integer iteratortag;

		for (String sortItem: sortingList){
			iteratornum = 0;
			for (SelectOption s : values) {
				if (s.getLabel() ==  sortItem) {
					result.add(s);
					iteratortag = iteratornum;
					break;
				}
				iteratornum++;
			}
			values.remove(iteratortag);
		}

		return result;
	}

	public static List<SObject> sortListNumeric (List<SObject> unsorted, String fieldName) {
		if(unsorted == NULL || unsorted.isEmpty()) {
			return unsorted;
		}
		List<decimal> sortingList = new List<decimal>();

		for (SObject s : unsorted) {
			sortingList.add(decimal.valueOf(String.valueOf(s.get(fieldName))));
		}

		sortingList.sort();

		List<SObject> t = unsorted.clone();
		t.clear();

		Integer iteratornum;
		Integer iteratortag;

		for (decimal sortItem: sortingList){
			iteratornum = 0;
			for (SObject s : unsorted) {
				if (decimal.valueOf(String.valueOf(s.get(fieldName))) ==  sortItem) {
					t.add(s);
					iteratortag = iteratornum;
					break;
				}
				iteratornum++;
			}
			unsorted.remove(iteratortag);
		}

		return t;
	}

	public static Business_Unit__c getBusinessUnitForStudy (ID StudyID){
		// get business unit (enriched with currency details) for a study
		try {
			Business_Unit__c BU = [ Select b.Name
									, b.BDTDeleted__c
									, b.Id
									, b.Description__c
									, b.Currency__c
									, b.Currency__r.name
									, b.Currency__r.OfficialName__c
									, b.Abbreviation__c
									From Business_Unit__c b
									where id in (Select Business_Unit__C
												 From Study__c
												 where id = :StudyID)];
			return BU;
		} catch (Exception e) {
			return new Business_Unit__c();
		}
	}

	/**
	 * This method will get a default ratecardclass for a service and businessunit to be used in the studyserviceratecard
	 * @author Maurice Kremer
	 * @version dec-2012
	 * @param BusinessUnitId	Business unit the study code relates to, aka the default business unit
	 * @param ProjectServiceId	The project service
	 * @param NumberOfUnits		The number of assesments that the service has
	 * @return RateCardClass__c	The best matching ratecard for this studyservicepricing
	 */
	public static RateCardClass__c getDefaultRatecard (ID BusinessUnitID, ID ProjectServiceID, Decimal NumberOfUnits ) {
		try {

			RateCard__c RC = [Select UnitDescription__c,
									 Service__c,
									 BDTDeleted__c,
									 Id,
									 Business_Unit__c,
									(Select Id,
											Name,
											RateCard__c,
											BDTDeleted__c,
											MaximumUnits__c,
											NumericClass__c,
											UnitPrice__c,
											Currency__c
									From 	RateCardClasses__r
									where 	BDTDeleted__c != true
									order by MaximumUnits__c, UnitPrice__c)
								From RateCard__c
								where BDTDeleted__c != true
								and Service__c in (select service__c
												   from   projectservice__c
												   where  id = :ProjectServiceID
												   and	BDTDeleted__c != true)
								and Business_Unit__c = :BusinessUnitID];

			RateCardClass__c RCCresult;

			for (RateCardClass__c RCC : RC.RateCardClasses__r) {
				if (RCCresult == null) {
					RCCresult = RCC;
				} else if (RCC.NumericClass__c == true && RCC.MaximumUnits__c >= NumberOfUnits) {
					RCCresult = RCC;
				} else if (RCC.NumericClass__c == true && RCC.MaximumUnits__c < NumberOfUnits) {
					break;
				}
			}

			return RCCresult;

		} catch (Exception e) {
			return new RateCardClass__c();
		}
	}

	/* Class constructor
	 * @author	Dimitris Sgourdos, Maurice Kremer
	 * @version	16-Sep-2013
	 * @since	Jul-2013
	 */
	/* has become obsolete with version 4.6 as the timepoint__c object has been factorred out 
	*
	* public static void updateListOfMinutesInProjectServiceTimePoint(list<projectservicetimepoint__c> prSrvTimePoint) {
	*	Set<id> pIds = new Set<id>();
	*	for (projectservicetimepoint__c tmpItem:prSrvTimePoint) {
	*		pIds.add(tmpItem.projectservice__c);
	*	}
	*	list<projectservice__c> prSrvList = [select (select TimePoint__r.Minutes__c, flowchart__c
	*													From ProjectServiceTimePoint__r
	*													order by TimePoint__r.minutes__c),
	*												id
	*										from 	projectservice__c
	*										where   id in :pIds
	*										and	service__r.isdesignObject__c = true];
	*	map<string,string> timesMinMap = new map<string,string>();
	*	for (projectservice__c tmpObj : prSrvList) {
	*		for (projectservicetimepoint__c tmpItem : tmpObj.ProjectServiceTimePoint__r){
	*			string key = tmpItem.flowchart__c +':'+ tmpObj.id;
	*			string myList ='';
	*			if (timesMinMap.containsKey(key)) {
	*				myList = timesMinMap.remove(key);
	*			}
	*			if (tmpItem.timepoint__r != null && tmpItem.timepoint__r.minutes__c!=null){
	*				myList += tmpItem.timepoint__r.minutes__c + ':';
	*			}
	*			timesMinMap.put(key,myList);
	*		}
	*	}
	*	list<sObject> updateList = new list<sObject>();
	*	for (projectservicetimepoint__c tmpObj:[select projectservice__c, flowchart__c, listoftimeminutes__c, TimePointCount__c
	*											from projectservicetimepoint__c
	*											where timepoint__c = null
	*											and projectservice__c in :pIds]) {
	*		string key = tmpObj.flowchart__c +':'+ tmpObj.projectservice__c;
	*		if (timesMinMap.containsKey(key)) {
	*			// check if the record should be changed
	*			string timepoints = timesMinMap.get(key).removeEnd(':');
	*			string storedTimePointValue = (String.isEmpty(tmpObj.listoftimeminutes__c))?'':tmpObj.listoftimeminutes__c;
	*			Integer timePointCount = (timepoints=='')?0:timepoints.split(':').size();
	*			if 		(storedTimePointValue != timepoints ||
	*					tmpObj.TimePointCount__c != timePointCount ) {
	*				tmpObj.listoftimeminutes__c = timepoints;
	*				tmpObj.TimePointCount__c = timePointCount;
	*				updateList.add(tmpObj);
	*			}
	*		}
	*	}
	*	update updateList;
	*}
	*/
	
	public static void setPreference(String preferenceName,String preferenceValue) {
		String currentValue = getPreference(preferenceName);
		// do not update if value has not changed
		if (currentValue != preferenceValue) {
			// change detected
			BDT_UserPreference__c up;
			// get existing or new BDT_UserPreference__c record
			try {
				up = [
					select 	id, name, text__c
					from 	BDT_UserPreference__c
					where 	ownerid = :UserInfo.getUserId()
					and		name = :preferenceName];
			} catch (Exception e) {
				//preference could not be found, create new
				up = new BDT_UserPreference__c(name = preferenceName);
			}
			//update the value
			up.text__c = preferenceValue;
			upsert up;
		}
	}

	public static String getPreference (String preferenceName) {
		BDT_UserPreference__c up;
		try {
			up = [select 	id, name, text__c
				from 	BDT_UserPreference__c
				where 	ownerid = :UserInfo.getUserId()
				and		name = :preferenceName];
		} catch (Exception e) {
			up = new BDT_UserPreference__c();
		}
		return (up.text__c == null)?'':up.text__c;
	}

	public static void cleanPreferences(List<String> preferencesToRemove) {
			try{
				delete [select id
						from BDT_UserPreference__c
						where name in :preferencesToRemove
						and ownerid = :UserInfo.getUserId()];
			} catch (Exception e) {}
	}

	public static void cleanPreferences() {
		try{
			delete [select id
					from BDT_UserPreference__c
					where ownerid = :UserInfo.getUserId()];
		} catch (Exception e) {}
	}

	/**
	  *	Take a list of objects and map it on one or more fields. If the key of
	  *	of the map has multiple fields the key will be seperated using a colon (:).
	  * Each key will return a list of SObject.
	  *	@author   Maurice Kremer
	  *	@version  25-Sep-2013
	  *	@param	  List<SObject>				List that needs to be converted
	  *	@param	  List<String>				List of fields that key needs to consist of (can only be of field in SObject
	  *											and not of a parent object)
	  *	@return	  Map<String,List<SObject>>	List mapped on specified fields
	  */
	public static Map<string,list<sObject>> mapSObjectListOnKey (List<sObject> listToMap, list<String> keysToMap) {
		Map<string,list<sObject>> resultMap = new Map<string,list<sObject>> ();
		For (sObject listItem : listToMap) {
			String Key = '';
			For (String keyField : keysToMap) {
				Key += ':' + listItem.get(keyField);
			}
			Key = Key.removeStart(':');
			List<sObject> newList = new List<sObject>();
			If (resultMap.containsKey(Key)) {
				newList = resultMap.remove(Key);
			}
			newList.add(listItem);
			resultMap.put(Key,newList);
		}
		return resultMap;
	}

	/**
	* Take a list of objects and map it on one or more fields. If the key of
	* of the map has multiple fields the key will be seperated using a colon (:)
	* Each key will return a single SObject
	* @author   Maurice Kremer
	* @version  25-Sep-2013
	* @param	  List<SObject>			List that needs to be converted
	* @param	  List<String>			List of fields that key needs to consist of (can only be of field in SObject
	* 										and not of a parent object)
	* @return	  Map<String,SObject>	List mapped on specified fields
	*/
	public static Map<string,sObject> mapSObjectOnKey (List<sObject> listToMap, List<String> keysToMap) {
		Map<string,sObject> resultMap = new Map<string,sObject> ();
		For (sObject listItem : listToMap) {
			String Key = '';
			For (String keyField : keysToMap) {
				Key += ':' + String.valueOf(listItem.get(keyField));
			}
			Key = Key.removeStart(':');
			resultMap.put(Key,listItem);
		}
		return resultMap;
	}

	/**
	* Get a set from a list
	* @author	Maurice Kremer
	* @version	03-Oct-13
	* @param	List<sObject>	List to use for the set
	* @param	String			Field to create set of
	* @return	Set<sObject>
	*/
	public static Set<Object> getSetFromList (List<SObject> sourceList, String fieldName){
		Set<Object> result = new Set<Object>();
		For (sObject listItem : sourceList) {
			result.add(listItem.get(fieldName));
		}
		return result;
	}

	/**
	* Get a list of SelectOptions from a list
	* @author	Maurice Kremer
	* @version	09-Oct-13
	* @param	List<SObject> sourceList	List of sobjects
	* @param	String identifierField		Fieldname that holds the identifier
	* @param	String labelField			Fieldname that holds the label
	* @return	List<SelectOption>
	*/
	public static List<SelectOption> getSelectOptionFromList (List<SObject> sourceList, String identifierField, String labelField) {
		List<SelectOption> result = new List<SelectOption>();
		 
		For (SObject listItem: sourceList) {
			String key = (String.valueOf(listItem.get(identifierField))!=null)?
					String.valueOf(listItem.get(identifierField)):
					'n/a';
			String label = (String.valueOf(listItem.get(labelField))!=null)?
					String.valueOf(listItem.get(labelField)):
					'n/a';
			result.add(new SelectOption(key,label));
		}
		return result;
	}
	
	
	/** Get a list of SelectOptions from a list, with additional no selection, sorted or not
	* @author	Dimitrios Sgourdos
	* @version	10-Dec-2013
	* @param	sourceList			List of sobjects
	* @param	identifierField		Fieldname that holds the identifier
	* @param	labelField			Fieldname that holds the label
	* @param	noSelectionLabel	The label of the nothing selected of the selectoption list. In case of NULL or empty,
									this initial not-selection option is not added
	* @param	sortedFlag			A flag if the returned values are sorted or not
	* @return	The created selectOptions list
	*/
	public static List<SelectOption> getSelectOptionFromSObjectList (List<SObject> sourceList,
														String identifierField,
														String labelField,
														String noSelectionLabel,
														Boolean sortedFlag) {
		// Create the select option list
		List<SelectOption> results = new List<SelectOption>();
		
		// Add the none value in case it is needed
		if(String.isNotBlank(noSelectionLabel)) {
			results.add(new SelectOption('', noSelectionLabel));
		}
		
		// Add the list of objects in the results
		List<SelectOption> temporaryList = getSelectOptionFromList(sourceList, identifierField, labelField);
		results.addAll(temporaryList);
		
		// Sort in case it is needed
		if(sortedFlag) {
			results.sort();
		}
		
		return results;
	}
	
	
	/** Retrieve all the picklist values from a field and store them in a SelectOption list. 
	* @author	Dimitrios Sgourdos
	* @version	10-Dec-2013
	* @param	obj					The current SObject
	* @param	fieldName			The fieldname of the SObject
	* @param	noSelectionLabel	The label of the nothing selected of the selectoption list. In case of NULL or empty,
									this initial not-selection option is not added
	* @param	sortedFlag			A flag if the returned values are sorted or not
	* @return	The SelectOption list with the picklist values
	*/
	public static List<SelectOption> getSelectOptionsFromPickList(SObject obj,
														String fieldName,
														String noSelectionLabel,
														Boolean sortedFlag) {
		// Create the select option list
		List<SelectOption> results = new List<SelectOption>();
		
		// Add the none value in case it is needed
		if(String.isNotBlank(noSelectionLabel)) {
			results.add(new SelectOption('', noSelectionLabel));
		}
		
		// Get a map of fields for the SObject
		Schema.sObjectType objType = obj.getSObjectType(); 
		Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
		map<String, Schema.SObjectField> fieldMap = objDescribe.fields.getMap();
		
		// Get the list of picklist values for this field and add them to the selectoption list
		list<Schema.PicklistEntry> values =	fieldMap.get(fieldName).getDescribe().getPickListValues();
		for (Schema.PicklistEntry a : values) {
			results.add(new SelectOption(a.getLabel(), a.getValue())); 
		}
		
		// Sort in case it is needed
		if(sortedFlag) {
			results.sort();
		}
		
		return results;
	}
	
	
	public static String convertListToString (list<String> values) {
		String result = '';
		For (String value :  values) {
			result += ':' + value.replace(':',';');
		}
		result = result.removeStart(':');
		return result;
	}
	
	
	/** Concatinate the given fieldName values of a list of SObjects to a string using the delimiter ':' 
	* @author	Dimitrios Sgourdos
	* @version	11-Oct-2013
	* @param	objectList			The list of SObjects
	* @param	fieldName			The fieldname of the SObjects
	* @return	The concatinated string
	*/
	public static String createConcatinatedStringFromSobject(List<Sobject> objectList, String fieldName) {
		String result = '';
		for(SObject tmpItem : objectList) {
			String tmpStr;
			try { // If the field name is not correct
			  tmpStr = String.valueOf(tmpItem.get(fieldName));
			} catch(Exception e) {
				return '';
			}
			// If the value is not blank, add it to the concatinated string
			if( ! String.isBlank(tmpStr) ) {
				result += ':' + tmpStr.replace(':',';');
			} 
		}
		result = result.removeStart(':');
		return result;
	}


	public static String removeElementFromStringList (String value,String removeElement) {
		String result;
		result = value.replaceAll('(^|:)'+removeElement+'($|:)',':'); 
		result = result.replaceAll('(^:|:$)',''); // clean up start or end colon
		return result;
	}
	
	/**
	 * Determine is there is a shared element in two series.
	 * @author Maurice Kremer
	 * @version 18Oct13
	 * @param String seriesOne A string serie like 1:2:3 with colon as the seperator
	 * @param String seriesTwo A string serie like 1:2:3 with colon as the seperator
	 * @ return boolean
	 */
	public static Boolean haveMatchingElements (String seriesOne, String seriesTwo) {
		
		String shortest;
		String longest;
		
		if (seriesOne == null || seriesTwo == null) {
			return false;
		}
		
		// minimize number of loops by only looping the shortest series
		if (seriesOne.length() >= seriesTwo.length()) {
			shortest = seriesTwo;
			longest = seriesOne;
		} else {
			shortest = seriesOne;
			longest = seriesTwo;
		}
		
		For (String element : shortest.split(':')) {
			if (Pattern.matches('(^|.*:)'+element+'($|:.*)', longest)) {
				return true;
			}
		}
		
		return false;
		
	}

	public static Set<String> getStringSetFromList (List<SObject> inputList, String fieldName) {
		Set<String> result = new Set<String>();
		For (SObject inputItem : inputList) {
			result.add(string.valueOf(inputItem.get(fieldName)));
		}
		return result;
	}


	public class SelectOptionShuttle {

		public SelectOptionShuttle (List<SObject> listOfObjects, String keyField, String labelField, List<String> selectedItems) {
			
			Set<String> selectedSet = new Set<String>();
			selectedSet.addAll(selectedItems);
			
			allOptions = new list<SelectOption>();
			usedOptions = new list<SelectOption>();

			for (SObject item : listOfObjects) {
				allOptions.add(new SelectOption(String.valueOf(item.get(keyField)),String.valueOf(item.get(labelField))));
				if( selectedSet.contains( String.valueOf(item.get(keyField)) ) ) {
					usedOptions.add(new SelectOption(String.valueOf(item.get(keyField)),String.valueOf(item.get(labelField))));
				}
			}
		}



		public list<SelectOption> allOptions {get;set;}
		public list<SelectOption> usedOptions {get;set;}

		public list<SelectOption> getUnusedOptions() {
			List<SelectOption> result = new List<SelectOption>();
			Set<String> usedOptionsSet = new Set<String>();
			usedOptionsSet.addAll( getUsedKeys() );
			
			for(SelectOption allItem : allOptions) {
				if( ! usedOptionsSet.contains(allItem.getValue()) ) {
					result.add(new SelectOption(allItem.getValue(),allItem.getLabel()));
				}
			}
			
			return result;
		}

		public list<String> getUsedKeys() {
			list<String> result = new list<String>();
			For (SelectOption used : usedOptions) {
				result.add(used.getValue());
			}
			return result;
		}

		public list<String> getUnusedKeys() {
			list<String> result = new list<String>();
			For (SelectOption unused : getUnusedOptions()) {
				result.add(unused.getValue());
			}
			return result;
		}

		public void moveToUsed (List <String> keyList) {
			for (String keyItem : keyList) {
				for (SelectOption allItem : allOptions) {
					if (keyItem == allItem.getValue()) {
						// found the item to be added to usedOptions
						usedOptions.add(allItem);
						// this loop is complete
						break;
					}
				}
			}
			usedOptions = sortSelectList(usedOptions);
		}

		public void moveToUnused (List <String> keyList) {
			for (String keyItem : keyList) {
				// find the items in the used list and then remove them
				For (Integer i = 0; i < usedOptions.size(); i++) {
					if (usedOptions[i].getValue() == keyItem) {
						// found item that needs to be removed
						usedOptions.remove(i);
						// this loop is complete
						break;
					}
				}
			}
		}
	}
	
	
	/**	Format a number to integer and add commas.
	*	@author   Dimitrios Sgourdos
	*	@version  19-Nov-2013
	*	@param	  value					The value that will be formatted
	*	@return   The formatted number as a string.
	*/
	public static String formatNumberWithSeparators (Decimal value){
		return formatNumberWithSeparators(value,0);
	}
	
	
	public static String formatNumberWithSeparators (Decimal value,Integer decimals){
		if (value == null) return null;
		decimals = (decimals==null)?0:decimals;
		String result = String.valueOf(value.setScale(decimals));
		
		string beforeDecimalDot= result.replaceAll('[.]\\d*',''); // get 12 from 12.43
		string afterDecimalDot = result.replaceAll('^[0-9]*',''); // get .43 from 12.43
		
		String regex = '(\\d)(?=(\\d{3})+$)';
		
		return beforeDecimalDot.replaceAll(regex,'$1,') + afterDecimalDot;
	}
	
	
	/**	Create a string that holds the given list as a set for dynamic queries.
	 * @author	Dimitrios Sgourdos
	 * @version	14-Feb-2014
	 * @param	source				The source for the created string
	 * @return  The string that holds the given list as a set for dynamic queries.
	*/
	public static String buildSetForDynamicQueries(List<String> source) {
		String result = '';
		
		for (String tmpStr : source) {
			if( String.isNotBlank(tmpStr) ) {
				result += ',\''+ tmpStr +'\'';
			}
		}
		
		result = result.removeStart(',');
		
		if( String.isBlank(result) ) {
			return NULL;
		}
		
		return '(' + result + ')';
	}
	
	
	/**	Create an email instance with the given info.
	 * @author	Dimitrios Sgourdos
	 * @version	23-Jan-2014
	 * @param	receiptTo			The receipents of the email
	 * @param	emailSubject		The email subject
	 * @param	emailBodyHTML		The email body
	 * @param	attachFile			The attachment of the mail
	 * @return  The created e-mail instance.
	*/
	public static Messaging.SingleEmailMessage createEmailInstance(List<String> receiptTo,
																String emailSubject,
																String emailBodyHTML,
																Messaging.EmailFileAttachment attachFile) {
		// Create email instance
		Messaging.SingleEmailMessage mailInstance = new Messaging.SingleEmailMessage();
		
		mailInstance.setToAddresses(receiptTo);
		mailInstance.setSubject(emailSubject);
		mailInstance.setHtmlBody(emailBodyHTML);
		
		if(attachFile != NULL) {
			mailInstance.setFileAttachments(new Messaging.EmailFileAttachment[] {attachFile});
		}
		
		return mailInstance;
	}
	
	
	/**	Calculate the percentage of the number of days in the given month.
	 * @author	Dimitrios Sgourdos
	 * @version	26-Feb-2014
	 * @param	numberOfDays		The number of days
	 * @param	srcYear				The year that the given month belongs to
	 * @param	srcMonth			The given month
	 * @return  The calculated percentage.
	*/
	public static Decimal getNumberOfDaysPercentageInMonth(Integer numberOfDays, Integer srcYear, Integer srcMonth) {
		Decimal numerator = numberOfDays;
		Decimal denominator = Date.daysInMonth(srcYear, srcMonth);
		return (numerator / denominator);
	}
	
	
	/**	Calculate the number of days until the end of month of the given date.
	 * @author	Dimitrios Sgourdos
	 * @version	26-Feb-2014
	 * @param	source				The given date
	 * @return  The calculated number of days.
	*/
	public static Integer getNumberOfDaysUntilEndOfMonth(Date source) {
		return Date.daysInMonth(source.year(), source.month()) - source.day();
	}
	
	
	/**	Calculate the absolute number of months between the given dates.
	 * @author	Dimitrios Sgourdos
	 * @version	27-Feb-2014
	 * @param	startDate			The start date of the comparison
	 * @param	endDate				The end date of the comparison
	 * @return  The calculated absolute number of months.
	*/
	public static Decimal getAbsoluteMonthsBetweenDates(Date startDate, Date endDate) {
		Decimal result;
		
		// Switch the start and the end date in case the first one is later
		Date newStartDate = (startDate <= endDate)? startDate : endDate;
		Date newEndDate	  = (startDate <= endDate)? endDate : startDate;
		
		// Calculate the month difference and adjust with percentage
		Integer monthDiff = newStartDate.monthsBetween(newEndDate);
		
		// Check if the dates belong to the same month in the same year
		if(monthDiff == 0) {
			result = getNumberOfDaysPercentageInMonth(newStartDate.daysBetween(newEndDate),
													newEndDate.year(),
													newEndDate.month() );
		} else {
			Date tmpDate = newStartDate.addMonths(monthDiff);
			result = monthDiff;
			
			if(tmpDate < newEndDate) {
				result += getNumberOfDaysPercentageInMonth(tmpDate.daysBetween(newEndDate),
														newEndDate.year(),
														newEndDate.month() );
			} else if( tmpDate > newEndDate ) {
				result -= 1;
				Integer tmpInt = getNumberOfDaysUntilEndOfMonth(newStartDate);
				result += getNumberOfDaysPercentageInMonth(tmpInt, newStartDate.year(), newStartDate.month());
				result += getNumberOfDaysPercentageInMonth(newEndDate.day(), newEndDate.year(), newEndDate.month());
			}
		}
		
		return ( (startDate <= endDate)? result : -result);
	}
}