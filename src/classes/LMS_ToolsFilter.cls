public class LMS_ToolsFilter {
	
	public static String getWhitelistEmployeeFilter() {
		String filter = '';
		list<DWF_Whitelist__c> whitelists = [select Company_No__c, Business_Unit_Name__c from DWF_Whitelist__c where DWF_System__r.Name = 'Saba'];
		
		if (whitelists != null && whitelists.size() > 0) {
			filter += 'and (';
			for (DWF_Whitelist__c wl : whitelists) {
				filter += '(Home_Company__c = ' + wl.Company_No__c + ' and Business_Unit_Desc__c = \'' + wl.Business_Unit_Name__c + '\') OR ';
			}
			filter = filter.substring(0, filter.length() - 4) + ')';
		}
		
		return filter;
	}
	
	public static String getWhitelistRoleEmployeeFilter() {
		String filter = '';
		list<DWF_Whitelist__c> whitelists = [select Company_No__c, Business_Unit_Name__c from DWF_Whitelist__c where DWF_System__r.Name = 'Saba'];
		
		if (whitelists != null && whitelists.size() > 0) {
			filter += 'and (';
			for (DWF_Whitelist__c wl : whitelists) {
				filter += '(Employee_Id__r.Home_Company__c = ' + wl.Company_No__c + 
					' and Employee_Id__r.Business_Unit_Desc__c = \'' + wl.Business_Unit_Name__c + '\') OR ';
			}
			filter = filter.substring(0, filter.length() - 4) + ')';
		}
		
		return filter;
	}
	
	public static Map<Decimal, set<String>> getWhitelistCompanyBUMap() {
		Map<Decimal, set<String>> companyBUMap = new Map<Decimal, set<String>>();
		list<DWF_Whitelist__c> whitelists = [select Company_No__c, Business_Unit_Name__c from DWF_Whitelist__c where DWF_System__r.Name = 'Saba'];
		
		if (whitelists != null && whitelists.size() > 0) {
			for (DWF_Whitelist__c wl : whitelists) {
				if (!companyBUMap.containsKey(wl.Company_No__c)) {
					companyBUMap.put(wl.Company_No__c, new set<String>());
					companyBUMap.get(wl.Company_No__c).add(wl.Business_Unit_Name__c);
				} else {
					companyBUMap.get(wl.Company_No__c).add(wl.Business_Unit_Name__c);
				}
			}
		}
		
		return companyBUMap;
	}
	
	public static String getBlackListEmployeeFilter() {
		String filter = '';
		list<DWF_Blacklist__c> blacklists = [select Employee_Id__c from DWF_Blacklist__c where DWF_System__r.Name = 'Saba'];
		
		if (blacklists != null && blacklists.size() > 0) {
			filter += 'and Name not in (';
			for (DWF_Blacklist__c bl : blacklists) {
				filter += '\'' + bl.Employee_ID__c + '\',';
			}
			filter = filter.substring(0, filter.length() - 1) + ')';
		}
		
		return filter;
	}
	
	public static String getBlackListRoleEmployeeFilter() {
		String filter = '';
		list<DWF_Blacklist__c> blacklists = [select Employee_Id__c from DWF_Blacklist__c where DWF_System__r.Name = 'Saba'];
		
		if (blacklists != null && blacklists.size() > 0) {
			filter += 'and Employee_Id__r.Name not in (';
			for (DWF_Blacklist__c bl : blacklists) {
				filter += '\'' + bl.Employee_ID__c + '\',';
			}
			filter = filter.substring(0, filter.length() - 1) + ')';
		}
		
		return filter;
	}
	
	public static set<String> getBlacklistEmployeeSet() {
		set<String> empSet = new set<String>();
		list<DWF_Blacklist__c> blacklists = [select Employee_Id__c from DWF_Blacklist__c where DWF_System__r.Name = 'Saba'];
		
		if (blacklists != null && blacklists.size() > 0) {
			for (DWF_Blacklist__c bl : blacklists) {
				empSet.add(bl.Employee_Id__c);
			}
		}
		
		return empSet;
	}
	
}