/** Implements the test for the Selector Layer of the object FinancialCatTotPrice__c
 * @author	Dimitrios Sgourdos
 * @version	17-Dec-2013
 */
@isTest
private class FinancialCatTotPriceDataAccessorTest {
	
	/** Test the function getFinancialCatTotalPricesByDocumentId
	 * @author	Dimitrios Sgourdos
	 * @version	17-Dec-2013
	 */
	static testMethod void getFinancialCatTotalPricesByDocumentIdTest() {
		// Create data
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001',
																				'BDT Project First Experiments');
		insert currentProject;
	
		Study__c study = new Study__c();
		study.Project__c = currentProject.Id;
		insert study;
		
		FinancialDocument__c financialDocument = new FinancialDocument__c();
		financialDocument.DocumentType__c	= 'Proposal';
		financialDocument.Client_Project__c	= currentProject.Id; 
		insert financialDocument;
		
		ServiceCategory__c srvCat = new ServiceCategory__c();
		srvCat.Code__c = '001';
		srvCat.Name = 'Test Category';
		insert srvCat;
		
		FinancialCategoryTotal__c fct = new FinancialCategoryTotal__c();
		fct.FinancialDocument__c = financialDocument.Id;
		fct.ServiceCategory__c	 = srvCat.Id;
		fct.Study__c			 = study.Id;
		insert fct;
		
		List<FinancialCatTotPrice__c> sourceList = new List<FinancialCatTotPrice__c>();
		sourceList.add(new FinancialCatTotPrice__c(FinancialCategoryTotal__c = fct.Id, Name='Test 2'));
		sourceList.add(new FinancialCatTotPrice__c(FinancialCategoryTotal__c = fct.Id, Name='Test 1'));
		insert sourceList;
		
		// Check the function
		List<FinancialCatTotPrice__c> results = FinancialCatTotPriceDataAccessor.getFinancialCatTotalPricesByDocumentId (
																							financialDocument.Id,
																							true,
																							'Name');
		
		String errorMessage = 'Error in reading Financial Category Total Prices from the system';
		system.assertEquals(results.size(), 2, errorMessage);
		
		system.assertEquals(results[0].Id, sourceList[1].Id, errorMessage);
		system.assertEquals(results[0].FinancialCategoryTotal__r.ServiceCategory__c, srvCat.Id, errorMessage);
		
		system.assertEquals(results[1].Id, sourceList[0].Id, errorMessage);
		system.assertEquals(results[1].FinancialCategoryTotal__r.ServiceCategory__c, srvCat.Id, errorMessage);
	}
}