public with sharing class PBB_RegionModelsController {

	/** 
	* to display the list of region models
	*/
	public List <Mapping_Region_Model__c> Region_Models    {get;set;}

	/** 
       @description Constructor
       @author Niharika Reddy
       @date 2015   
   */
	public PBB_RegionModelsController() {
		Region_Models= PBB_DataAccessor.GetAllRegionModels();
	}

    /** 
        @description Function to call New Page of required Model
        @author Tkachenko Oleksiy
        @date 2015  
   */
   public PageReference getNewPageReference() {
       Schema.DescribeSObjectResult R;
       R = Mapping_Region_Model__c.SObjectType.getDescribe();
       return new PageReference('/' + R.getKeyPrefix() + '/e');
   }
}