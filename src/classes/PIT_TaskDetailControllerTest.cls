@isTest
private class PIT_TaskDetailControllerTest {

	static testMethod void myUnitTest() {
		// Create the Task
		Task tmpTask = new Task();
		insert tmpTask;
		// create the controller
		ApexPages.standardController stdCtr;
		PIT_TaskDetailController p = new PIT_TaskDetailController(stdCtr); // cannot find ID
		PageReference pageRef = New PageReference(System.Page.PIT_TaskDetail.getUrl());
		pageref.getParameters().put('id' , tmpTask.id);
		Test.setCurrentPage(pageRef); 
		p = new PIT_TaskDetailController(stdCtr); // can find ID
		System.assertEquals(p.ta.id, tmpTask.id);
		p.showAttachSelPanel();
		p.hideAttachSelPanel();
		p.showAttachSelPanel();
		p.insertAttachment();
		p.deleteAttach();
		p.viewAttach();
		p.editAttach();
    }
}