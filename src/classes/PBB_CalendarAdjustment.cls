/**
 * @author Ramya
 * @Date 2015
 * @Description This class contains the variables and methods for holding and 
 *             generating data for calendar Adjustments respectively.
 */
public class PBB_CalendarAdjustment {


    public List<SRM_Calender_Adjustments__c> calendarlist  {get;set;}
    public List<CalendarAdjWrapper> CalendardatavaluesList{get;set;}
    public PBB_Scenario__c scenario{ get; set; }
    
    // Wrapper class to display all the Calendar adjustments fields and dates in string format
    public class CalendarAdjWrapper {
        public  String    CalendarAdjustmentID          {get;set;}    
        public  String    Comments                      {get;set;}        
        public  decimal   SiteActAdjustment             {get;set;}
        public  decimal   SubjectEnrollAdjustment       {get;set;}  
        public  String    Countryname                   {get;set;}  
        public  String    Fromdate                      {get;set;}  
        public  String    ToDate                        {get;set;}  
        public  CalendarAdjWrapper (){   
        }       
    }

   //Method to Show Calendar Adjustments
    /**
    * @author Ramya Shree Edara
    * @Date 2015
    * @Description Construtor
    *
    */
    public PBB_CalendarAdjustment(ApexPages.StandardController controller) {
 
 
        calendarlist  = new List<SRM_Calender_Adjustments__c>();
        CalendardatavaluesList = new List<CalendarAdjWrapper>();
        String scenarioId = ApexPages.CurrentPage().getParameters().get('id');
        system.debug('@@@@scenarioid'+scenarioId );

        scenario =PBB_DataAccessor.getScenarioByID(scenarioId);
        system.debug('@@@@scenario'+scenario);
        
        List<SRM_Scenario_Country__c> scencoutryidsList =PBB_DataAccessor.GetPBBWeeklyCountriesbyasc(scenarioId);
        system.debug('scencoutryidsList :'+scencoutryidsList ); 
        List<Id> countryIds = new List<Id>();
        for(SRM_Scenario_Country__c coun: scencoutryidsList ){
            countryIds.add(coun.PBB_Scenario_Country__r.Country__r.Id);
            system.debug('@@@@countryIds'+countryIds);
        }
        
        if(scenarioId !=null){
        
            /** Get Data From SRM_Calender_Adjustments__c from PBB_DataAccessor Class**/
            system.debug('@@@@countryIds'+countryIds);
            calendarlist  = PBB_DataAccessor.GetcalendarAdjData(scenario.Final_Protocol_Date__c,scenario.Last_Patient_In__c,scenario.SRM_Model__c,countryIds);
            system.debug('@@@@calendarlist'+calendarlist);
            
            if(calendarlist != null){

                for(SRM_Calender_Adjustments__c  sca :calendarlist ){
                
                   CalendarAdjWrapper caw = new CalendarAdjWrapper();

                   caw.ToDate =  SRM_Utils.getDateInStringFormat(sca.To_Date__c);
                   caw.Fromdate =  SRM_Utils.getDateInStringFormat(sca.From_Date__c);
                   caw.Comments =  sca.Comments__c;
                   caw.SiteActAdjustment =  sca.Site_Act_Adjustment__c;
                   caw.SubjectEnrollAdjustment  =  sca.Subject_Enroll_Adjustment__c;
                   caw.Countryname =  sca.Country__r.name;
                   caw.CalendarAdjustmentID = sca.Name;     
                   
                   CalendardatavaluesList.add(caw);
                   system.debug('@@@@CalendardatavaluesList'+CalendardatavaluesList);          
                }                 
            }
        }  
    }
}