public with sharing class PBB_Mobile_Cont {
    public Map<String,Decimal>                          countrySummary          {get;set;}

    public List<PAWS_ProjectDetailCVO>                  projectDetailsList      {get;set;}
    public List<PBBMobileSettingWrapper>                settingList             {get;set;}
    
    public PAWS_ProjectDetailCVO                        projectDetailItem       {get;set;}
    public PBB_WR_APIs.PatientEnrollmentwrapper         patientEnrollmentItem   {get;set;}
    
    public String                                       gType                   {get;set;}
    public String                                       activeComponent         {get;set;}
    
    public Id                                           scenarioId              {get;set;}

    public Integer                                      goalInteger             {get{ if(goalInteger==null){goalInteger=0;}return goalInteger;}set;}
    
    public Date                                         siteActivatedByDate     {get{if( siteActivatedByDate == null ){siteActivatedByDate = Date.today();}return siteActivatedByDate;}set;}

    private List<PAWS_ProjectCVO>                       projectList             {get;set;}
    
    private PBB_Scenario__c                             scenarioItem            {get;set;}
    
    private String sponsorName = 'SM Test Acc1';                                                    // Dima TODO HARDCODE

    private Id projectId = ApexPages.currentPage().getParameters().get( PBBUtils.PROJECTID_PARAM_STRING );


    public PBB_Mobile_Cont() {
        try{
            settingList = PBBMobileSettingServices.getSettingList( Id.valueOf( UserInfo.getUserId() ) );
        } catch ( Exception expt ){
                                                                                                    // TODO Dima AddException
            showError( expt );
        }
System.debug( LoggingLevel.ERROR,'@@@projectId : ' + projectId );
        if( projectId == null ){
            projectDetailsList = new List<PAWS_ProjectDetailCVO>();

            try{                                                                                    // StudyStatusReview page
                projectList = PAWS_API.PBB_getActiveProjects( sponsorName );
            } catch ( Exception expt ){
                                                                                                    // TODO Dima AddException
                showError( expt );
                return;
            }
System.debug( LoggingLevel.ERROR,'@@@projectList : ' + projectList.size() );
            for( PAWS_ProjectCVO projectItem :  projectList ){
                PAWS_ProjectDetailCVO projectDetailItem;

                try{
                    projectDetailItem = PAWS_API.PBB_getProjectDetails( projectItem.RecordID );
                } catch ( Exception expt ){
                                                                                                    // TODO Dima AddException
                    showError( expt );
                    return;
                }
System.debug( LoggingLevel.ERROR,'@@@projectDetailItem : ' + projectDetailItem );
                projectDetailsList.add( projectDetailItem );

                return;                                                                             // to avoid 101 Error
            }

            return;
        } else {                                                                                    // PatientEnrollment,PatientEnrollment_1,PatientEnrollment_2,LiveGraphPOC,TrialServices,StudyGeneral,StudyStatusReview pages
        //if( projectId != null ){
            gType = 'Enrollment Status by Country';                                                 // TODO Dima HARDCODE
            countrySummary = new Map<String,Decimal>();
            // Temp init default values
            countrySummary.put( 'NumberOfSites', 0);
            countrySummary.put( 'NumberOfSitesAtRisk', 0);
            countrySummary.put( 'NumberOfSitesOnTrack', 0);
            countrySummary.put( 'NumberOfSitesDelayed', 0);
            countrySummary.put( 'NumberOfSitesCompleted', 0);
            
            try{
                projectDetailItem = PAWS_API.PBB_getProjectDetails( projectId );
            } catch ( Exception expt ){
                                                                                                    // TODO Dima AddException
                showError( expt );
                return;
            }

            try{
                scenarioItem = PBB_WR_APIs.getAprrovedScenarioID( projectDetailItem.ProjectID );
            } catch ( Exception expt ){
                                                                                                    // TODO Dima AddException
                showError( expt );
                return;
            }

            if( scenarioItem == null ){
                scenarioId = Id.valueOf( 'a0YL0000008ar8ZMAQ' );                                     // TODO Dima HARDCODE
            } else {
                scenarioId = scenarioItem.Id;
            }
            try{
                patientEnrollmentItem = PBB_WR_APIs.GetPatientEnrollmentAttributes( scenarioId );
            } catch ( Exception expt ){
                                                                                                    // TODO Dima AddException
                showError( expt );
                return;
            }

            for( PAWS_ProjectCountryCVO countryItem : projectDetailItem.Countries ){
                countrySummary.put( 'NumberOfSites',            ( countrySummary.get( 'NumberOfSites' )             != null ? countrySummary.get( 'NumberOfSites' ) : 0 )           +  ( countryItem.NumberOfSites          != null ? countryItem.NumberOfSites : 0 ) );
                countrySummary.put( 'NumberOfSitesAtRisk',      ( countrySummary.get( 'NumberOfSitesAtRisk' )       != null ? countrySummary.get( 'NumberOfSitesAtRisk' ) : 0 )     +  ( countryItem.NumberOfSitesAtRisk    != null ? countryItem.NumberOfSitesAtRisk : 0 ) );
                countrySummary.put( 'NumberOfSitesOnTrack',     ( countrySummary.get( 'NumberOfSitesOnTrack' )      != null ? countrySummary.get( 'NumberOfSitesOnTrack' ) : 0 )    +  ( countryItem.NumberOfSitesOnTrack   != null ? countryItem.NumberOfSitesOnTrack : 0 ) );
                countrySummary.put( 'NumberOfSitesDelayed',     ( countrySummary.get( 'NumberOfSitesDelayed' )      != null ? countrySummary.get( 'NumberOfSitesDelayed' ) : 0 )    +  ( countryItem.NumberOfSitesDelayed   != null ? countryItem.NumberOfSitesDelayed : 0 ) );
                countrySummary.put( 'NumberOfSitesCompleted',    ( countrySummary.get( 'NumberOfSitesCompleted' )    != null ? countrySummary.get( 'NumberOfSitesCompleted' ) : 0 )  +  ( countryItem.NumberOfSitesCompleted != null ? countryItem.NumberOfSitesCompleted : 0 ) );
            }
        }
    }

    public String saveSettings(){
        try{
            return PBBMobileSettingServices.saveSettings( settingList );
        } catch ( Exception expt ){
                                                                                                    // TODO Dima AddException
            showError( expt );
        }

        return '';
    }

    public void saveData(){
        try{
System.debug( LoggingLevel.ERROR,'@@@goalInteger : ' + goalInteger );
System.debug( LoggingLevel.ERROR,'@@@siteActivatedByDate : ' + siteActivatedByDate );
            Boolean saveBoolean = PAWS_API.PBB_setAggregatedMilestone ( projectDetailItem.RecordID, 
                                                                        goalInteger,
                                                                        siteActivatedByDate,
                                                                        'Site Activation'
                                                                        );
System.debug( LoggingLevel.ERROR,'@@@saveBoolean : ' + saveBoolean );
        } catch ( Exception expt ){
                                                                                                    // TODO Dima AddException
            showError( expt );
        }
    }

    public void showComponent(){
        gType = activeComponent;

    }


    private void showError( Exception expt ){
        System.debug( LoggingLevel.ERROR,'@@@expt : ' + String.valueOf( expt ) );
    }
}