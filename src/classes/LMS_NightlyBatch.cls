global class LMS_NightlyBatch extends PRA_BatchaQueueable implements Database.AllowsCallouts,Database.Stateful {
    
    String query;
    String action;
    global Boolean isError = false;
    global String startMessage = '<html><body>';
    global String closeMessage = '</body></html>';
    global String jobMessage = '';
    global String mappingMessage = '';
    global String finalMessage = '';
    global String emailSubect = '';
    
    global LMS_NightlyBatch() {
        
    }

    //Set the parameters over the variables
    global override void  setBatchParameters(String parametersJSON){
        List<String> params = (List<String>) Json.deserialize(parametersJSON, List<String>.class);
        query = params.get(0);
        action = params.get(1);        
     }
    
    global override Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global override void execute(Database.BatchableContext BC, List<sObject> scope) {
        isError = false;
        Set<String> errorStrings;
        List<String> errs = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();
        List<LMS_Role_Course__c> role_course_add = new List<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> role_course_remove = new List<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> addRoleCourses = new List<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> removeRoleCourses = new List<LMS_Role_Course__c>();
        Map<String, List<LMS_Role_Course__c>> sendAddMap = new Map<String, List<LMS_Role_Course__c>>();
        Map<String, Map<String, LMS_Role_Course__c>> objectsAdd = new Map<String, Map<String, LMS_Role_Course__c>>();
        
        for(sObject s : scope) {
            if(s instanceof LMS_Role_Course__c && action == 'A') {
                LMS_Role_Course__c rc = (LMS_Role_Course__c)s;
                role_course_add.add(rc);
            }
            if(s instanceof LMS_Role_Course__c && action == 'D') {
                LMS_Role_Course__c rc = (LMS_Role_Course__c)s;
                role_course_remove.add(rc);
            }
        }
        if(role_course_add.size() > 0) {
            System.debug('-----------reached rc add----------------');
            String roleId;
            Map<String, LMS_Role_Course__c> roleCourseMatch = new Map<String, LMS_Role_Course__c>();
            for(LMS_Role_Course__c rc : role_course_add) {
                if(roleId != rc.Role_Id__c) {
                    roleCourseMatch = new Map<String, LMS_Role_Course__c>();
                }
                roleCourseMatch.put(rc.Course_Id__r.SABA_ID_PK__c, rc);
                objectsAdd.put(rc.Role_Id__r.SABA_Role_PK__c, roleCourseMatch);
                addRoleCourses.add(rc);
                roleId = rc.Role_Id__c;
            }
            sendAddMap.put('A', addRoleCourses);
            addRoleCourses = LMS_ToolsService.executeCoursesToRolesWS(sendAddMap, objectsAdd);
            errors = LMS_ToolsService.getErrorMap();
            errorStrings = new Set<String>();
            if(errors != null && !errors.isEmpty()) {
                errs = errors.get('A');
            }
            if(errs != null && errs.size() > 0) {
                isError = true;
                for(String message : errs) {
                    if(!errorStrings.contains(message)) {
                        mappingMessage += message + '<br/>';
                        errorStrings.add(message);  
                    }
                }
            }
        }
        if(role_course_remove.size() > 0) {
            System.debug('-----------reached rc remove----------------');
            removeRoleCourses = LMS_ToolsService.coursesRolesBatchDeleteMany(role_course_remove);
            errors = LMS_ToolsService.getErrorMap();
            errorStrings = new Set<String>();
            if(errors != null && !errors.isEmpty()) {
                errs = errors.get('D');
            }
            if(errs != null && errs.size() > 0) {
                isError = true;
                for(String message : errs) {
                    if(!errorStrings.contains(message)) {
                        mappingMessage += message + '<br/>';
                        errorStrings.add(message);  
                    }
                }
            }
        }
        
        // Upsert and delete records based on query and web service results
        if(addRoleCourses.size() > 0) {
            System.debug('------------------------ Adding RC List size ------------------------' + addRoleCourses.size());
            upsert addRoleCourses;
        }
        if(removeRoleCourses.size() > 0) {
            System.debug('------------------------ Deleting RC List size ------------------------' + removeRoleCourses.size());
            delete removeRoleCourses;
        }
    }
    
    global override void finish(Database.BatchableContext BC) {
        System.debug('--------------errored?--------------'+isError);
        if(isError) {
            // Get the ID of the AsyncApexJob representing this batch job 
            // from Database.BatchableContext. 
            // Query the AsyncApexJob object to retrieve the current job's information. 
            AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
              TotalJobItems, CreatedBy.Email
              from AsyncApexJob where Id =:BC.getJobId()];
            
            // Get users to send failure emails
            List<User> users = new List<User>([SELECT Id FROM User 
            WHERE Username IN ('jonestodd@praintl.com.prod')]);
            
            List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();
            
            for(User u : users) {
                // Send an email to the Apex job's submitter notifying of job completion.
                jobMessage = 'PRODUCTION - CLEAR Role Course Nightly Service processed ' + a.JobItemsProcessed + 
                    ' batch with the following errors :<br/><br/>'; 
                finalMessage = startMessage + jobMessage + mappingMessage + closeMessage;
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setTargetObjectId(u.Id);
                mail.setSaveAsActivity(false);
                mail.setUseSignature(false);
                mail.setSubject('PRODUCTION  - CLEAR Role Course Nightly Service has ' + a.Status + ' with errors');
                mail.setHtmlBody(' ' + finalMessage);
                mails.add(mail);
            }
            Messaging.sendEmail(mails);
        }
    }
}