@isTest
private class BDT_ProjectServicesTest {
	
	static testMethod void myUnitTest() {
		// Create data
		ServiceCategory__c srvCat = new ServiceCategory__c(code__c='001',name='Service Category (1)');
		insert srvCat;
		
		Service__c srv = new Service__c(Name = 'Srv 1', ServiceCategory__c=srvCat.Id);
		insert srv;
		
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001',
																				'BDT Project First Experiments');
		insert currentProject;
		BDT_Utils.setPreference('SelectedProject', String.valueOf(currentProject.id));
		
		Study__c study = new Study__c();
		study.Project__c = currentProject.Id;
		insert study;
		BDT_Utils.setPreference('SelectedStudies', study.Id);
		
		ProjectService__c prSrv = new ProjectService__c(Service__c = srv.id, Client_Project__c = currentProject.id);
		insert prSrv;
		
		StudyService__c stSrv = new StudyService__c(ProjectService__c = prSrv.Id,
													NumberOfUnits__c=1,
													Study__c=study.Id);
		insert stSrv;
		
		// Create Controller
		BDT_ProjectServices p = new BDT_ProjectServices();
		
		for(BDT_ProjectServices.ServiceCategoryWrapper scw : p.StudyCategories) {
			scw.changed = true;
			// For code coverage
			scw.getServiceCategoryCode();
			scw.getPadding();
		}
		
		p.getNumberOfStudies();
		
		
		//save data
		p.saveStudyServices();
		
		// reload the data for code coverage
		p.loadData();
	
		// Copy services
		p.copyServices();
		
		// The following tests are only for code coverage as for these piece of code in controller there are only 
		// calls to functions from other classes that are already tested.
		p.serviceCatListIndex = '0';
		p.serviceListIndex = '0';
		p.studyListIndex = '0';
		p.calculationHelperItem = new StudyServiceService.CalculationHelper();
		
		p.showCalculatorHelper();
		p.calculationHelperItem.rule = p.AssessmentsForAllFcInStudyRule;
		p.changeCalculationSelection();
		p.includeOutcome();
		p.hideCalculatorHelper();
		
		p.showCalculatorHelper();
		p.calculationHelperItem.rule = p.PredefinedCategoriesAssessmentsInStudyRule;
		p.changeCalculationSelection();
		p.showServiceCategoriesShuttle();
		p.buttonMoveToUnused();
		p.buttonMoveToUsed();
		p.calculateUnits();
	}

}