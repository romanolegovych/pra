global with sharing class PRA_CompletedUnitsModuleController {
    
	// Expose PRA_ClientTaskSearchService properties as
	// belonging to the controller class.
	public String selectedProject {
		get { return ctSearch.selectedProject; }
		set { ctSearch.selectedProject = value;}
	}
	public List<String> selectedOperationalArea {
		get { return ctSearch.selectedOperationalAreas; }
		set { ctSearch.selectedOperationalAreas = value;}
	}
	public List<String> selectedGroup {
		get { return ctSearch.selectedGroups; }
		set { ctSearch.selectedGroups = value;}
	}
	public List<String> selectedTask {
		get { return ctSearch.selectedTasks; }
		set { ctSearch.selectedTasks = value;}
	}
	public List<String> selectedRegion {
		get { return ctSearch.selectedRegions; }
		set { ctSearch.selectedRegions = value;}
	}
	public List<SelectOption> regionOptions {
		get { return ctSearch.regionOptions; }
		set { ctSearch.regionOptions = value;}
	}
	public List<SelectOption> operationalAreaOptions {
		get { return ctSearch.operationalAreaOptions; }
		set { ctSearch.operationalAreaOptions = value;}
	}
	public List<SelectOption> groupOptions {
		get { return ctSearch.groupOptions; }
		set { ctSearch.groupOptions = value;}
	}
	public List<SelectOption> taskOptions {
		get { return ctSearch.taskOptions; }
		set { ctSearch.taskOptions = value;}
	}
    //  Constant representing the select value option in the picklist
    public final String ST_PLACEHOLDER = 'Enter name or project ID....';
    // Value selected by the user in the picklist
    public String selectedProjectLabel  {get;set;}
    public Date selectedProjectMigratedDate {get;set;}
    //Value to store ProjectID for VF
    public String selectedProjectName   {get;set;}
    public Boolean blocked   {get;set;}    //block the edit button
    
    //Labels
    //target month is always current month - 1
    public String labelTargetMonth          {get;set;}
    public String TargetMonthDateLabel      {get;set;}
    //previous month FROM target month 
    public String labelTargetMonthMinusOne  {get;set;} 
    public String TargetMonthMinusOneDateLabel   {get;set;} 
    //previous to previous month FROM target month
    public String labelTargetMonthMinusTwo  {get;set;}
    // SOQL variables
    public String selectedMonthStringSOQL {get;set;}
    public String selectedMonthStringMinusOneSOQL;
    public String selectedMonthStringMinusTwoSOQL;
    // Picklist
    public List<SelectOption> projectOptions {get;set;}
    // Checkbox for unconfirmed units
    public Boolean unconfirmedOnly {get;set;}
    public Boolean confirmedOnly {get;set;}
    public List<Unit_Effort__c> liUnit {get;set;}
    public Map<Id, CompletedUnitsWrapper> mapTaskIdUnits = new Map<Id, CompletedUnitsWrapper>();
    public List<CompletedUnitsWrapper> liWrappers {get;set;} 
    // Collapsed
    public Boolean isCollapsed {get;set;}
    public Boolean isFirstTime {get;set;}
    
    public PRA_ClientTaskSearchService ctSearch = new PRA_ClientTaskSearchService();
    public PRA_CompletedUnitsModuleController() {
        
        // Getting Datetime of the 1st day of the previous month
        DateTime t = Datetime.newInstance(DateTime.now().year(), DateTime.now().month()-1, 1);
        selectedMonthStringSOQL = t.format('yyyy-MM-dd');
        system.debug('=============='+selectedMonthStringSOQL);
        
        // Prepare the date for SOQL
        prepareDates();
        
        PRA_Utils.UserPreference userPref = PRA_Utils.retrieveUserPreference();
        if(userPref != null) {
            selectedProject         = userPref.projectId;
            List<Client_Project__c> clientProjectsList = 
                [SELECT Name, Is_Project_Locked__c, Migrated_Date__c FROM Client_Project__c WHERE Id = :selectedProject and Load_Status__c != 'New'];
            if(clientProjectsList.size() == 1) {
                selectedProjectLabel        = clientProjectsList.get(0).Name;
                selectedProjectName         = clientProjectsList.get(0).Name;//Assigning for display in VF page
                selectedProjectMigratedDate = clientProjectsList.get(0).migrated_date__c;
                blocked                     = blockEditButton(clientProjectsList.get(0));
                selectedOperationalArea     = userPref.operationalAreaId;
                System.debug('selectedOperationalArea: '+selectedOperationalArea);
                selectedRegion              = userPref.regionId;
                selectedTask                = userPref.clientUnitId;
                selectedGroup               = userPref.clientUnitGroupId;
                unconfirmedOnly             = userPref.unconfirmedOnly;
                confirmedOnly               = userPref.confirmedOnly;
                selectProject();
                isCollapsed = true;
                // EXECUTE THE SEARCH
                isFirstTime = true;
                searchTasks();
            } else {
                // Init of Unconfirmed
                unconfirmedOnly = false;
                confirmedOnly = false;
                isFirstTime = true;
                // Reset the filters
               
                reset();
                 
                // Set the default project to the first project in the list
                selectedProject = ctSearch.ST_NONE;
                isCollapsed = false;
            }
        } else {
            // Init of Unconfirmed
            unconfirmedOnly = false;
            confirmedOnly = false;
            isFirstTime = true;
            
            reset();
            
            //set the default project to the first project in the list
            selectedProject = ctSearch.ST_NONE;
            isCollapsed = false;
        }
    }
    
    // Preparing the necessary dates
    public void prepareDates() {
        Datetime dateSelected = Datetime.newinstance(
            Integer.valueOf(selectedMonthStringSOQL.substring(0, 4)), 
            Integer.valueOf(selectedMonthStringSOQL.substring(5, 7)), 
            Integer.valueOf(selectedMonthStringSOQL.substring(8, 10)));
        
        // Label
        labelTargetMonth = dateSelected.format('yyyy-MM');
        TargetMonthDateLabel=dateSelected.format('yyyy-MM');
        system.debug('=============='+labelTargetMonth+TargetMonthDateLabel);
        
        // Target Month -1 ready for SOQL
        Datetime tm1=dateSelected.addMonths(-1);
        tm1=tm1.addHours(tm1.hour());        
        selectedMonthStringMinusOneSOQL = (tm1).format('yyyy-MM-dd');
        labelTargetMonthMinusOne = (tm1).format('yyyy-MM');
        TargetMonthMinusOneDateLabel=(tm1).format('yyyy-MM');
        system.debug('=============='+selectedMonthStringMinusOneSOQL+labelTargetMonthMinusOne+TargetMonthMinusOneDateLabel);
        
        // Target Month -2 ready for SOQL
        Datetime tm2=dateSelected.addMonths(-2);
        tm2=tm2.addHours(tm2.hour());
        selectedMonthStringMinusTwoSOQL = tm2.format('yyyy-MM-dd');
        labelTargetMonthMinusTwo = tm2.format('yyyy-MM');
        system.debug('=============='+selectedMonthStringMinusTwoSOQL+labelTargetMonthMinusTwo);
    }
    
    // Reset every filters
    public void reset() {
        selectedProject = ctSearch.ST_NONE;
        selectedProjectName = 'Home';
        selectedProjectLabel = null;
        selectedProjectMigratedDate = null;
        selectProject();
        liWrappers = null;
        confirmedOnly = false;
        unConfirmedOnly = false;
        if(!isFirstTime)
        PRA_Utils.deleteUserPreference(); 
    }
    
    // Selecting a Project updates OA and Regions
    public void selectProject() {
    	ctSearch.initOnSelectProject();
    }
    
    
    public void searchTasksVF() {
        for(Client_Project__c c : [SELECT Name, Is_Project_Locked__c FROM Client_Project__c WHERE Id = :selectedProject limit 1]) {
            selectedProjectName     = c.Name;
            selectedProjectLabel    = c.Name;
            blocked                 = blockEditButton(c);
        }
        isFirstTime = false;
        searchTasks();
    }
    
    // Event Handlers
    public void initGroupOptions() {
		ctSearch.initGroupOptions();    	
    }
    
    public void initTaskOptions() {
    	ctSearch.initTaskOptions();
    }
    
    // Search method
    public void searchTasks() {
        mapTaskIdUnits.clear();
        liWrappers = new List<CompletedUnitsWrapper>();
        
        Date selectedMonthDate = FROMSOQLtoDate(selectedMonthStringSOQL);
        Date selectedMonthDateMinus1 = FROMSOQLtoDate(selectedMonthStringMinusOneSOQL);
        Date selectedMonthDateMinus2 = FROMSOQLtoDate(selectedMonthStringMinusTwoSOQL);
        
		ctSearch.ueStartMonth = selectedMonthStringMinusTwoSOQL;
		ctSearch.filterClosedUnits = true;
		ctSearch.filterOffset = -1;		
		List<Unit_Effort__c> unitEfforts = ctSearch.getUnitEfforts();
		
        for(Unit_Effort__c unit: unitEfforts) {
            // Set project migrated date
            selectedProjectMigratedDate =  unit.Client_Task__r.Project__r.Migrated_Date__c;
            // Init
            if(mapTaskIdUnits.get(unit.Client_Task__c) == null) {
                // Link the Wrapper to the Task Id
                mapTaskIdUnits.put(unit.Client_Task__c, new CompletedUnitsWrapper());
                // Operational Area
                mapTaskIdUnits.get(unit.Client_Task__c).operationalArea = unit.Client_Task__r.Task_Group__r.Operational_Area__r.Name;
                // Task Name
                mapTaskIdUnits.get(unit.Client_Task__c).taskName = unit.Client_Task__r.Description__c;
                // Region
                mapTaskIdUnits.get(unit.Client_Task__c).region = unit.Client_Task__r.Project_Region__r.Name;
                // Country
                mapTaskIdUnits.get(unit.Client_Task__c).country = unit.Client_Task__r.Country__r.Name;
                // Unit of measurement
                mapTaskIdUnits.get(unit.Client_Task__c).unitMeasurement = unit.Client_Task__r.Unit_of_Measurement__c;
                //Unit Number
                mapTaskIdUnits.get(unit.Client_Task__c).unitnumber = unit.Client_Task__r.Client_Unit_Number__c;
            }
            if(mapTaskIdUnits.get(unit.Client_Task__c).remainingUnits == null) {
                mapTaskIdUnits.get(unit.Client_Task__c).remainingUnits = 0;
            }
            if(unit.Last_Approved_Unit__c != null && unit.Month_Applies_To__c == selectedMonthDate) {
                system.debug('pr found a previous source worked: ' + unit);
                system.debug('val: ' + unit.Last_Approved_Unit__c.setScale(2));
                mapTaskIdUnits.get(unit.Client_Task__c).nbUnitsForecastTargetMonth = unit.Last_Approved_Unit__c.setScale(2);
            }
            if(unit.Client_Task__r.Total_Contract_Units__c != null && unit.Month_Applies_To__c == selectedMonthDate) {
                if(mapTaskIdUnits.get(unit.Client_Task__c).totalContractedUnits == null) {
                    mapTaskIdUnits.get(unit.Client_Task__c).totalContractedUnits = 0;
                }
                // Set the total contracted units
                mapTaskIdUnits.get(unit.Client_Task__c).totalContractedUnits = mapTaskIdUnits.get(unit.Client_Task__c).totalContractedUnits + unit.Client_Task__r.Total_Contract_Units__c;
                // Add the baseline units to remaining units
                mapTaskIdUnits.get(unit.Client_Task__c).remainingUnits = mapTaskIdUnits.get(unit.Client_Task__c).remainingUnits + unit.Client_Task__r.Total_Contract_Units__c;
            }
            if(unit.Source_Worked_Unit__c != null && unit.Month_Applies_To__c == selectedMonthDate) {
                if(unit.Is_Null__c || unit.Source_Worked_Unit__c == null) {
                    mapTaskIdUnits.get(unit.Client_Task__c).nbUnitsWorkedTargetMonthSource = null;
                } else {
                    mapTaskIdUnits.get(unit.Client_Task__c).nbUnitsWorkedTargetMonthSource = unit.Source_Worked_Unit__c.setScale(2);
                }                
            }
            if(unit.Worked_Unit__c != null) {
                // Subtract the baseline units to remaining units
                if(unit.Month_Applies_To__c == selectedMonthDateMinus2) {
                    mapTaskIdUnits.get(unit.Client_Task__c).nbUnitsTargetMonthMinusTwo = unit.Worked_Unit__c;
                } else if(unit.Month_Applies_To__c == selectedMonthDateMinus1) {
                    mapTaskIdUnits.get(unit.Client_Task__c).nbUnitsTargetMonthMinusOne = unit.Worked_Unit__c.setScale(2);                 
                } else if(unit.Month_Applies_To__c == selectedMonthDate) {// This record is the one that tells us we will display a line
                    // Task Id
                    mapTaskIdUnits.get(unit.Client_Task__c).sfId = unit.Id;
                    // Number of Units
                    mapTaskIdUnits.get(unit.Client_Task__c).nbUnitsWorkedTargetMonthActual = unit.Worked_Unit__c == null ? null : unit.Worked_Unit__c.setScale(2);
                    // Number of Cumulative Units
                    mapTaskIdUnits.get(unit.Client_Task__c).nbCumulativeUnitsTargetMonth = unit.Client_Task__r.Total_Worked_Units__c;
                    // Previous months Cumulative
                    mapTaskIdUnits.get(unit.Client_Task__c).nbCumulativeUnitsTargetMonthMinusOne = (unit.Client_Task__r.Total_Worked_Units__c  == null ? null : (unit.Client_Task__r.Total_Worked_Units__c - unit.Worked_Unit__c).setScale(2));
                    // Incl Adj
                    mapTaskIdUnits.get(unit.Client_Task__c).inclAdj = unit.Include_Adjustment__c;
                    // Confirmed
                    mapTaskIdUnits.get(unit.Client_Task__c).confirmed = unit.Is_Confirmed_Worked_Unit__c;
                    // Remaining
                    mapTaskIdUnits.get(unit.Client_Task__c).remainingUnits = (mapTaskIdUnits.get(unit.Client_Task__c).remainingUnits - (unit.Client_Task__r.Total_Worked_Units__c == null ? 0 : unit.Client_Task__r.Total_Worked_Units__c)).setScale(2);
                }
            }
            if(unit.Month_Applies_To__c == selectedMonthDate) {
                // Handle the coloration when Worked <> Source Worked
                if(mapTaskIdUnits.get(unit.Client_Task__c).nbUnitsWorkedTargetMonthSource != null && 
                    mapTaskIdUnits.get(unit.Client_Task__c).nbUnitsWorkedTargetMonthSource <> mapTaskIdUnits.get(unit.Client_Task__c).nbUnitsWorkedTargetMonthActual) {
                    mapTaskIdUnits.get(unit.Client_Task__c).differentFromSourceClass = 'differentFromSource';
                }
                // Check that we received a Worked Unit: do the Unconfirmed filter
                if(mapTaskIdUnits.get(unit.Client_Task__c).sfId != null) {
                    system.debug('----------------confirmed--------------------' + unit.Is_Confirmed_Worked_Unit__c);
                    system.debug('----------------only unconfirmed--------------------' + unconfirmedOnly);
                    system.debug('----------------only confirmed--------------------' + confirmedOnly);
                    if(unconfirmedOnly && !unit.Is_Confirmed_Worked_Unit__c) {
                        liWrappers.add(mapTaskIdUnits.get(unit.Client_Task__c));
                    }
                    if(confirmedOnly && unit.Is_Confirmed_Worked_Unit__c) {
                        liWrappers.add(mapTaskIdUnits.get(unit.Client_Task__c));
                    }
                    if(!unconfirmedOnly && !confirmedOnly) {
                        liWrappers.add(mapTaskIdUnits.get(unit.Client_Task__c));
                    }
                }
            }
        }
        
        System.debug('*** SIZE returned: ' + liWrappers.size());
        
        if(!isFirstTime) {
            // Save user prefs
            PRA_Utils.savePrefs(selectedProject, selectedRegion, selectedOperationalArea, selectedTask, 
                                 selectedGroup, unconfirmedOnly, confirmedOnly);
        } else {
            isFirstTime = false;
        }
    }
    
    // Export the data to excel
    public PageReference exportToExcel() {
        //re-use the search task functionality to retrive the units
        searchTasks();
        if(liWrappers != null) {
            liWrappers.sort();
        }
        return Page.PRA_CompletedUnitsModuleCSV;    
    }

    // Returns true if the Edit button should be blocked (not visible)
    private Boolean blockEditButton(Client_Project__c prj) {
        Boolean rtnVal = false;
        
        // If project is locked then no one can edit
        if (prj.Is_Project_Locked__c) {
            rtnVal = true;
        }
        // If the current user is a Generic Employee profile
        // And if the PM has already approved work units for this month
        // Then the editing is blocked.
        else {

            // Search for user profile in the PMC_ProfileGroup.  The PMC_ProfileGroup
            // Contains all PMC users EXCEPT the generic employee
            Profile p=[select name from profile where id=:UserInfo.getProfileId()]; 
            List<PMC_ProfileGroup__c> ProfileSelectList = [select name from PMC_ProfileGroup__c where name=:p.name] ;  

            // Get the Monthly Approval record for the project for the current month.
            List<Monthly_Approval__c> liMA = [select Month_Approval_Applies_To__c, Is_Approved_PM_Worked_Units__c
                                              from Monthly_Approval__c
                                              where Month_Approval_Applies_To__c = THIS_MONTH and Client_Project__r.id = :prj.id];

            // If current user profile is not in PMC_ProfileGroup
            if(ProfileSelectList.size() < 1) {
                // If Monthly Approval record exists
                if (( liMA != null) && (liMA.size() > 0)) {
                    // If PM has approved work units for this project/month
                    if ( liMA[0].Is_Approved_PM_Worked_Units__c == true ) {
                        rtnVal = true;
                    }
                }
            }
        }
        
        return rtnVal;
    }
   
    
    // JS Remoting action called when editing the number of Worked Units
    @RemoteAction
    global static void updateWorkedUnitList(List<Unit_Effort__c> updatedUnits) {
        
        Set<Id> unitIds = new Set<Id>();
        Set<Id> setUnitID = new Set<Id>(); //Set to get Task Ids
        Set<Id> setTaskIds = new Set<Id>();
        Map<Id, Decimal> workedChangedMap = new Map<Id, Decimal>();
        Map<Id, Decimal> previousUnits = new Map<Id, Decimal>(); // Map to hold changed unit
        Map<Id, Boolean> previousAdjustment = new Map<Id, Boolean>(); // Map to hold changed adjustment
        Map<Id, Boolean> previousConfirm = new Map<Id, Boolean>(); // Map to hold changed confirm
        
        for(Unit_Effort__c u : updatedUnits) {
            setUnitID.add(u.Id);
            previousUnits.put(u.Id, u.Worked_Unit__c);
            previousAdjustment.put(u.Id, u.Include_Adjustment__c);
            previousConfirm.put(u.Id, u.Is_Confirmed_Worked_Unit__c);
        }
        
        List<Unit_Effort__c> unitEffortCosts = new List<Unit_Effort__c>();
        List<Unit_Effort__c> unitEffortsUpdate = new List<Unit_Effort__c>();         
        List<Unit_Effort__c> contractedBDGUnits = new List<Unit_Effort__c>();
        Map<Id, Unit_Effort__c> unitEffortMap = new Map<Id, Unit_Effort__c>();
        
        if(setUnitID != null) {
            List<Unit_Effort__c> allUnits = 
            [SELECT Id, Client_Task__c, Worked_Unit__c, Include_Adjustment__c, Is_Confirmed_Worked_Unit__c, Total_Capped_Cost__c, 
                Total_Contracted_BDG_Cost__c FROM Unit_Effort__c WHERE Id IN :setUnitID];           
            if(allUnits.size() > 0) {
                for(Unit_Effort__c u : allUnits) {
                    unitEffortMap.put(u.Id, u);
                    Decimal newWorked = previousUnits.get(u.Id);
                    workedChangedMap.put(u.Client_Task__c, newWorked - u.Worked_Unit__c);
                    if(!setTaskIds.contains(u.Client_Task__c) && u.Worked_Unit__c != newWorked) {
                        setTaskIds.add(u.Client_Task__c);
                    }
                    if(previousUnits.get(u.Id) != u.Worked_Unit__c || previousAdjustment.get(u.Id) != u.Include_Adjustment__c || 
                        previousConfirm.get(u.Id) != u.Is_Confirmed_Worked_Unit__c) {
                        unitIds.add(u.Id);
                        unitEffortsUpdate.add(u); // Add object to list if any change was made
                    }
                    if(previousUnits.get(u.Id) != u.Worked_Unit__c) {
                        u.Worked_Unit__c = previousUnits.get(u.Id); // Update the worked unit if changed
                    }
                    if(previousAdjustment.get(u.Id) != u.Include_Adjustment__c) {
                        u.Include_Adjustment__c = previousAdjustment.get(u.Id); // Update the inc_adj if changed
                    }    
                    if(previousConfirm.get(u.Id) != u.Is_Confirmed_Worked_Unit__c) {
                        u.Is_Confirmed_Worked_Unit__c = previousConfirm.get(u.Id); // Update the is_confirmed if changed
                    } 
                } 
            }
        }
        
        contractedBDGUnits = PRA_CompletedUnitsModuleController.updateContractedBDGUnits(setTaskIds, previousUnits, workedChangedMap);
        for(Unit_Effort__c ue : contractedBDGUnits) {
            if(!unitIds.contains(ue.Id)) {
                unitIds.add(ue.Id);
                unitEffortMap.put(ue.Id, ue);
            }
        }
        
        if(contractedBDGUnits.size() > 0) {
            update contractedBDGUnits;
        }
        if(unitEffortsUpdate.size() > 0) {                    
            update unitEffortsUpdate;
        }
        if(unitIds.size() > 0) {
            for(AggregateResult ar : [SELECT Unit_Effort__c ue, sum(Capped_Cost__c) capped, sum(Contracted_BDG_Cost__c) contracted 
                from Hour_EffortRatio__c where Unit_Effort__c in :unitIds GROUP BY Unit_Effort__c ORDER BY Unit_Effort__c]) {
                Unit_Effort__c unitEffort = unitEffortMap.get(String.valueOf(ar.get('ue')));
                if(ar.get('capped') != null && 
                    Decimal.valueOf(Double.valueOf(ar.get('capped'))) != unitEffortMap.get(String.valueOf(ar.get('ue'))).Total_Capped_Cost__c)
                    unitEffort.Total_Capped_Cost__c = Decimal.valueOf(Double.valueOf(ar.get('capped')));
                if(ar.get('contracted') != null && 
                    Decimal.valueOf(Double.valueOf(ar.get('contracted'))) != unitEffortMap.get(String.valueOf(ar.get('ue'))).Total_Contracted_BDG_Cost__c)
                    unitEffort.Total_Contracted_BDG_Cost__c = Decimal.valueOf(Double.valueOf(ar.get('contracted')));                    
                if((ar.get('capped') != null && Decimal.valueOf(Double.valueOf(ar.get('capped'))) == unitEffortMap.get(String.valueOf(ar.get('ue'))).Total_Capped_Cost__c) ||
                    (ar.get('contracted') != null && Decimal.valueOf(Double.valueOf(ar.get('contracted'))) == unitEffortMap.get(String.valueOf(ar.get('ue'))).Total_Contracted_BDG_Cost__c)) {
                    unitEffortCosts.add(unitEffort);
                }
            }
        }
        if(unitEffortCosts.size() > 0) {
            update unitEffortCosts;
        }
    }
    
    // JS Remoting action called when clicking on the Incl Adj checkbox
    @RemoteAction
    global static void clickInclAdj(String recordId, String newValue) {
        System.debug('recordId: ' + recordId + ' newValue: ' + newValue);
        Unit_Effort__c un = [SELECT Id, Include_Adjustment__c FROM Unit_Effort__c WHERE Id = :recordId];
        un.Include_Adjustment__c = (newValue == 'true');
        update un;
    }
    
    // JS Remoting action called when clicking on the Confirm checkbox
    @RemoteAction
    global static void clickConfirmed(String recordId, String newValue) {
        System.debug('recordId: ' + recordId + ' newValue: ' + newValue);
        Unit_Effort__c un = [SELECT Id, Is_Confirmed_Worked_Unit__c FROM Unit_Effort__c WHERE Id = :recordId];
        un.Is_Confirmed_Worked_Unit__c = (newValue == 'true');
        update un;
    }
    
    // JS Remoting action called when looking for Project
    @RemoteAction
    global static List<Client_Project__c> searchProject(String projectName) {
        System.debug('projectName: ' + projectName );
        List<Client_Project__c> liProj = Database.query('SELECT Id, Name FROM Client_Project__c WHERE Name LIKE \'%' 
            + String.escapeSingleQuotes(projectName) + '%\' and Load_Status__c != \'New\'');
        return liProj;
    }
    
    // Takes an ISO date formated for SOQL and trasnform it to Date
    private Date FROMSOQLtoDate(String soqlDate) {
        Date retDate = Date.newinstance(
            Integer.valueOf(soqlDate.substring(0, 4)), 
            Integer.valueOf(soqlDate.substring(5, 7)), 
            Integer.valueOf(soqlDate.substring(8, 10)));
        return retDate;
    }
    
    // Method to loop through Units to modify Contracted BDG Units
    private static List<Unit_Effort__c> updateContractedBDGUnits(Set<Id> taskIds, Map<Id, Decimal> newWorked, Map<Id, Decimal> differenceMap) {
        List<Unit_Effort__c> unitEfforts = new List<Unit_Effort__c>();
        List<Unit_Effort__c> unitList = [SELECT Id, Client_Task__c, Client_Task__r.Total_Units__c, Client_Task__r.Total_Contract_Units__c, 
                                        Client_Task__r.Total_Worked_Units__c, Contracted_BDG_Unit__c, Forecast_Unit__c, Worked_Unit__c, Month_Applies_To__c 
                                        FROM Unit_Effort__c WHERE Client_Task__c IN :taskIds ORDER BY Client_Task__c, Month_Applies_To__c];
        Id clientTask;
        Decimal totalUnits = 0;
        Decimal contractUnits = 0;
        Decimal aggregateUnit = 0;
        Decimal aggregateBDG = 0;
        Boolean clearContractBDG = false;                               
        for(Unit_Effort__c ue : unitList) {
            // Initialize values for every client task
            if(clientTask != ue.Client_Task__c) {
                clientTask = ue.Client_Task__c;
                totalUnits = ue.Client_Task__r.Total_Units__c + differenceMap.get(clientTask);
                contractUnits = ue.Client_Task__r.Total_Contract_Units__c;
                aggregateUnit = 0;
                aggregateBDG = 0;
                system.debug('-----------difference units----------- ' + differenceMap.get(clientTask));
                system.debug('-----------contract units----------- ' + contractUnits);
                system.debug('-----------total units----------- ' + totalUnits);
            }
            // When total units are greater than contract units
            system.debug('-------------total units greater than contract----------------');
            // Start aggregating the forecast values this month forward
            if(ue.Month_Applies_To__c >= Date.today().toStartOfMonth()) {
                if(ue.Forecast_Unit__c != null) {
                    aggregateUnit += ue.Forecast_Unit__c;
                }
            } else {
                if(newWorked.get(ue.Id) != null) {
                    aggregateUnit += newWorked.get(ue.Id);
                } else if(ue.Worked_Unit__c != null) {
                    aggregateUnit += ue.Worked_Unit__c;
                }
            }
            Decimal calcConBDG = Math.min(aggregateUnit - aggregateBDG, contractUnits - aggregateBDG);
            system.debug('--------------aggregateUnit---------------- ' + aggregateUnit);
            // If aggregation has not exceeded contract units
            // Update Contracted BDG Unit with the updated forecast value
            if(aggregateUnit <= contractUnits) {
                if(ue.Contracted_BDG_Unit__c != calcConBDG) {
                    ue.Contracted_BDG_Unit__c = calcConBDG;
                    unitEfforts.add(ue);
                }
                clearContractBDG = false;
            } else {
                system.debug('-----------------aggregate has surpassed contract-----------------');
                // If multiple units are to be cut off set (n+1,n+2,.....) to 0 
                if(clearContractBDG) {
                    if(ue.Contracted_BDG_Unit__c != 0) {
                        ue.Contracted_BDG_Unit__c = 0;
                        unitEfforts.add(ue);
                    }
                } else {
                    // Cut off contracted BDG unit so total will equal contracted units
                    if(ue.Contracted_BDG_Unit__c != calcConBDG) {
                        ue.Contracted_BDG_Unit__c = calcConBDG;
                        system.debug('------------new con bdg unit------------- ' + ue.Contracted_BDG_Unit__c);
                        unitEfforts.add(ue);
                    }
                    clearContractBDG = true; // will make all other further units have 0 contract bdg units
                }
            }
            aggregateBDG += ue.Contracted_BDG_Unit__c;
        }
        return unitEfforts;
    }
    
    public class NoCumulativeException extends Exception {}
    
    // Wrapper to hold the data to display
    global class CompletedUnitsWrapper implements Comparable {
        public String unitnumber {get;set;}
        public String sfId {get;set;}
        public Decimal nbUnits {get;set;}
        public String operationalArea {get;set;}
        public String region {get;set;}
        public String country {get;set;}
        public String taskName {get;set;}
        public Decimal nbUnitsTargetMonthMinusOne {get;set;}
        public Decimal nbUnitsTargetMonthMinusTwo {get;set;}
        public Decimal nbCumulativeUnitsTargetMonthMinusOne {get;set;}
        public Decimal nbUnitsForecastTargetMonth {get;set;}
        public Decimal nbUnitsWorkedTargetMonthSource {get;set;}
        public Decimal nbUnitsWorkedTargetMonthActual {get;set;}
        public Decimal nbCumulativeUnitsTargetMonth {get;set;}
        public Decimal totalContractedUnits {get; set;}
        public Decimal remainingUnits {get; set;}
        public Boolean inclAdj {get;set;}
        public Boolean confirmed {get;set;}
        public String unitMeasurement{get;set;}
        public String differentFromSourceClass {get;set;}
        
        public CompletedUnitsWrapper() {
            
        }
        
        // implement the compareTo method
        global Integer compareTo(Object compareTo) {
            CompletedUnitsWrapper compareToCompUnitsWrapper = (CompletedUnitsWrapper)compareTo;
            if (taskName == compareToCompUnitsWrapper.taskName) return 0;
            if (taskName > compareToCompUnitsWrapper.taskName) return 1;
            return -1;        
        }
    }

}