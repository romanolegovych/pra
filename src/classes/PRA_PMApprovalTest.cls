@isTest(SeeAllData=false)
public with sharing class PRA_PMApprovalTest {
    private static Integer PROJECT_LENGHT = 3;
   static void initTestData (String projectName){
   
        Operational_Area__c oa = new Operational_Area__c(Name = 'Project Management', Description__c = 'Project Management');
        insert oa;
        
        Date start_date = Date.today();
        Date end_date = Date.today();
        end_date = end_date.addMonths( PROJECT_LENGHT );
        
        Client_Project__c cp = new Client_Project__c( contracted_start_date__c = start_date, contracted_end_date__c = end_date );
        if(projectName!='myT3stPr0j3ct')
        cp.Estimated_End_Date__c = end_date ;
        
        cp.PRA_Project_ID__C = projectName;
        cp.name = projectName;
        insert cp;
        
        Date thismonth = System.today().toStartOfMonth();
        Monthly_Approval__c mp =new Monthly_Approval__c();
        mp.Month_Approval_Applies_To__c=thismonth;
        mp.Client_Project__c=cp.id;
        String  month=(string.valueof(thismonth.month()).length()==1)?string.valueof('0'+thismonth.month()):string.valueof(thismonth.month());                
        String madate=string.valueof(thismonth.year())+string.valueof(month)+'01';
        system.debug('------month--madate----'+month+madate);
        mp.MA_Unique_ID__c = ProjectName+':'+madate;
        
        if(projectName=='myT3stPr0j3ct')
            insert mp;
        
        Task_Group__c tg = new Task_Group__c( Operational_Area__c = oa.id );
        insert tg;
        
        Client_Task__c ct = new Client_Task__c( project__c = cp.id, task_group__c = tg.id );
        ct.PRA_Client_Task_ID__C = projectName+'ct1';
        ct.Start_Date__c = start_date;
        ct.Original_End_date__c = end_date;
        ct.Forecast_Curve__c = 'Project Management';
        insert ct;
        
        Client_Task__c ct1 = new Client_Task__c( project__c = cp.id, task_group__c = tg.id );
        ct1.PRA_Client_Task_ID__C =  projectName+'ct2';
        ct1.Start_Date__c = start_date;
        ct1.Original_End_date__c = end_date;
        ct1.Forecast_Curve__c = 'Project change Management';
        insert ct1;
        
        
        Unit_Effort__c ef1 = new Unit_Effort__c(month_applies_to__c = Date.today().addMonths(0).toStartOfMonth());
        ef1.Client_Task__c = ct.id;
        ef1.Forecast_Unit__c = 1;
        ef1.Forecast_Effort__c = 2;
        ef1.Worked_Unit__c=3;
        ef1.Total_Forecast_Cost__c=3;
        ef1.Unit_Effort_ID__c =  projectName+'ue1';       
        insert ef1;
        
        Unit_Effort__c ef2 = new Unit_Effort__c(month_applies_to__c = Date.today().addMonths(1).toStartOfMonth());
        ef2.Client_Task__c = ct.id;
        ef2.Forecast_Unit__c = 2;
        ef2.Forecast_Effort__c = 4;
        ef2.Worked_Unit__c=3; 
        ef2.Total_Forecast_Cost__c=3;      
        ef2.Unit_Effort_ID__c =  projectName+'ue2';          
        insert ef2;
        
         Unit_Effort__c ef3 = new Unit_Effort__c(month_applies_to__c = Date.today().addMonths(2).toStartOfMonth());
        ef3.Client_Task__c = ct.id;
        ef3.Forecast_Unit__c = 3;
        ef3.Forecast_Effort__c = 6; 
        ef3.Worked_Unit__c=3;  
        ef3.Total_Forecast_Cost__c=3;      
        ef3.Unit_Effort_ID__c =  projectName+'ue3';        
        insert ef3;
       
        
        Unit_Effort__c ef4 = new Unit_Effort__c(month_applies_to__c = Date.today().addMonths(0).toStartOfMonth());
        ef4.Client_Task__c = ct.id;
        ef4.Forecast_Unit__c = 1;
        ef4.Forecast_Effort__c = 2;
        ef4.Total_Forecast_Cost__c=3;
         ef4.Worked_Unit__c=3; 
        ef4.Unit_Effort_ID__c =  projectName+'ue4';       
        insert ef4;
        
         // make 2 imaginary buff codes
        BUF_Code__c bc1 = new BUF_Code__c();
        insert bc1;
        
        BUF_Code__c bc2 = new BUF_Code__c();
        insert bc2;
        
        
        Hour_EffortRatio__c her1 = new Hour_EffortRatio__c();
        her1.Unit_Effort__c = ef1.id;
        her1.BUF_Code__c = bc1.id;
        her1.Hour_ER_ID__c =  projectName+'her1';
        her1.Forecast_ER__c= 2; 
        her1.Worked_Hours__c=5;       
        insert her1;
        
        Hour_EffortRatio__c her2 = new Hour_EffortRatio__c();
        her2.Unit_Effort__c = ef1.id;
        her2.BUF_Code__c = bc2.id;
        her2.Hour_ER_ID__c=  projectName+'her2';
        her2.Forecast_ER__c= 4;
        //her2.Worked_Hours__c=5;        
        insert her2;
        
        Hour_EffortRatio__c her3 = new Hour_EffortRatio__c();
        her3.Unit_Effort__c = ef1.id;
        her3.BUF_Code__c = bc2.id;
        her3.Hour_ER_ID__c=  projectName+'her3';
        her3.Forecast_ER__c= 4;
        her3.Worked_Hours__c=5;
        her3.Cost_Rate__c=4;
        her3.Exchange_Rate__c=4;        
        insert her3;
        
          
   }
   //prepare a test data for project with contract end date
   static testMethod void shouldUpdateLAFUnits_EffortforCED() {
        String testProjectName = 'myT3stPr0j3ct';           // should be something that's probably unique
        initTestData(testProjectName);        
        Test.startTest();        
        // PRA_PMApprovalBatch batchUnits = new PRA_PMApprovalBatch('Unit__c', 'Month_Unit_Applies_To__c', testProjectName);
        // database.executebatch(batchUnits);
        String paramsJSONStringUnit = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', testProjectName});
        String unitBatchQueueId = PRA_BatchQueue.addJob('PRA_PMApprovalBatch',paramsJSONStringUnit, null);
        
        
        String paramsJSONStringHER = Json.serialize(new List<String>{'Hour_EffortRatio__c', 'Unit_Effort__r.Month_Applies_To__c', testProjectName});
        String HERBatchQueueId = PRA_BatchQueue.addJob('PRA_PMApprovalBatch',paramsJSONStringHER, null);
        
        Test.stopTest();        
        system.assertNotEquals(null, unitBatchQueueId);   
        system.assertNotEquals(null, HERBatchQueueId);       
    }
    
    //prepare a test data for project with Estimated end date
   static testMethod void shouldUpdateLAFUnits_EffortforEED() {
        //different project
        String testProjectName= 'myT3stPr0j3ct1';           // should be something that's probably unique
        initTestData(testProjectName);        
        Test.startTest();        
        // PRA_PMApprovalBatch batchUnits = new PRA_PMApprovalBatch('Unit__c', 'Month_Unit_Applies_To__c', testProjectName);
        // database.executebatch(batchUnits);
        String paramsJSONStringUnit = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', testProjectName});
        String unitBatchQueueId = PRA_BatchQueue.addJob('PRA_PMApprovalBatch',paramsJSONStringUnit, null);
        
        
        String paramsJSONStringHER = Json.serialize(new List<String>{'Hour_EffortRatio__c', 'Unit_Effort__r.Month_Applies_To__c', testProjectName});
        String HERBatchQueueId = PRA_BatchQueue.addJob('PRA_PMApprovalBatch',paramsJSONStringHER, null);
        
        Test.stopTest();        
        system.assertNotEquals(null, unitBatchQueueId);   
        system.assertNotEquals(null, HERBatchQueueId);  
   }
   
   /*
   //prepare a test data for project with contract end date
   static testMethod void shouldUpdateLAFHour_EffortRatioCED() {
        String testProjectName = 'myT3stPr0j3ct';           // should be something that's probably unique
        initTestData(testProjectName);        
        Test.startTest();        
        // PRA_PMApprovalBatch batchUnits = new PRA_PMApprovalBatch('Unit__c', 'Month_Unit_Applies_To__c', testProjectName);
        // database.executebatch(batchUnits);
        String paramsJSONString = Json.serialize(new List<String>{'Hour_EffortRatio__c', 'Unit_Effort__r.Month_Applies_To__c', testProjectName});
        String unitBatchQueueId = PRA_BatchQueue.addJob('PRA_PMApprovalBatch',paramsJSONString, null);
        Test.stopTest();        
        system.assertNotEquals(null, unitBatchQueueId);    
   }
  
   //prepare a test data for project with Estimated end date
   static testMethod void shouldUpdateLAFHour_EffortRatioEED() {
        //different project
        String testProjectName = 'myT3stPr0j3ct1';           // should be something that's probably unique
        initTestData(testProjectName);        
        Test.startTest();        
        // PRA_PMApprovalBatch batchUnits = new PRA_PMApprovalBatch('Unit__c', 'Month_Unit_Applies_To__c', testProjectName);
        // database.executebatch(batchUnits);
        String paramsJSONString = Json.serialize(new List<String>{'Hour_EffortRatio__c', 'Unit_Effort__r.Month_Applies_To__c', testProjectName});
        String unitBatchQueueId = PRA_BatchQueue.addJob('PRA_PMApprovalBatch',paramsJSONString, null);
        Test.stopTest();        
        system.assertNotEquals(null, unitBatchQueueId);    
   }
   */
        
}