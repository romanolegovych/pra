public class IPA_IntegrationUpdateComponentController
{
    public IPA_Articles__c integrationUpdate {get; set;}
    
    public IPA_IntegrationUpdateComponentController()
    {
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        integrationUpdate = ipa_bo.returnIntegrationUpdate();
    }
}