/**
*   'PRA_ClientTasksOpenCloseController' is to display all the client tasks with open/close status for a project in P&F.
*   @author   Devaram Bhargav
*   @version  18-Nov-2013
*   @since    18-Nov-2013
*/
public with sharing class PRA_ClientTasksOpenCloseController{   
   
    /** variables to hold the ProjectName, ProjectID etc*/   
    public String ProjectText           {get;set;}       //holds the projectname to search on for.
    public String selectedProjectLabel  {get;set;}       //holds the projectname or 'Home'.    roject.
    public Client_Project__c project    {get;set;}       //client project sobject to hold the project record.
    
    /** variables to hold the select option over the select list of values*/
    public String selectedAction        {get;set;} 
    
    /** variables to hold all the select client tasks on the VF page to open/close them*/
    public String selectedTasks         {get;set;}
    
    //This value is to hold, if all the client tasks are selected or not.
    public Boolean bSelectAllTasks      {get;set;}
   
    // Constants none used in saving the userpreference.
    public final  String ST_NONE               = PRA_Constants.ST_NONE;
    
    // for user prefs
    private Integer searchCount                     {get;set;}
    private PRA_Utils.UserPreference userPreference {get;private set;} 
    
    // date variable is to close the date for the client units.
    public Date ctCloseDate             {get;set;}
    
    //Options need to display over the action picklist on VF. 
    public static String TEXT_DISPLAY_ALL_CLIENT_TASKS      {get {return PRA_ClientTaskService.TO_DISPLAY_ALL_CLIENT_TASKS;}}    
    public static String TEXT_OPEN_CLIENT_TASKS             {get {return PRA_ClientTaskService.TO_RE_OPEN_CLIENT_TASKS;}}
    public static String TEXT_CLOSE_CLIENT_TASKS            {get {return PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS;}}
    
    //Options need to display over the action picklist on VF. 
    public static String TO_DISPLAY_ALL_CLIENT_TASKS        {get {return PRA_ClientTaskService.TO_DISPLAY_ALL_CLIENT_TASKS;}}    
    public static String TO_RE_OPEN_CLIENT_TASKS            {get {return PRA_ClientTaskService.TO_RE_OPEN_CLIENT_TASKS;}}
    public static String TO_CLOSE_CLIENT_TASKS              {get {return PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS;}}    
    
    //setting up the Pick list values.
    public List<Selectoption> getActionList() {
        List<Selectoption> retVal = new List<Selectoption>();        
        retVal.add(new Selectoption (TO_DISPLAY_ALL_CLIENT_TASKS, TO_DISPLAY_ALL_CLIENT_TASKS));   //Display all client tasks.     
        retVal.add(new Selectoption (TO_CLOSE_CLIENT_TASKS, TO_CLOSE_CLIENT_TASKS));               //Displays all open client tasks to close.        
        retVal.add(new Selectoption (TO_RE_OPEN_CLIENT_TASKS, TO_RE_OPEN_CLIENT_TASKS));                 //Displays all closed client tasks to open.
        return retVal;
    }  
    
    /**
    *  Constructor that retrieves the userpreferences if, then search on them.          
    */
    public PRA_ClientTasksOpenCloseController() {
        
        selectedProjectLabel = PRA_Constants.ST_DEFAULT_PROJECT_LABEL;  //default the home label
        bSelectAllTasks = false;               
        userPreference = PRA_Utils.retrieveUserPreference();           //retrieve the userpreferences
        
        if(userPreference != null) {  
            //get the project name for the userpreference projectid.           
            project = PRA_DataAccessor.getClientProjectDetailsbyProjectID(userPreference.projectId);
            
            //if project exists assign the name to label and text.
            if(project!=null) {                
                selectedProjectLabel = project.Name;
                ProjectText          = project.Name; 
            }
        } 
        
        //do a search if project exists or not in userpreference.
        MainSearch();
    }
    
    /**
    *  MainSearch method is used to search on the client tasks for a particular project.          
    */
    public void MainSearch() {
        
        //set home as default label 
        selectedProjectlabel  = PRA_Constants.ST_DEFAULT_PROJECT_LABEL; 
        
        //get the record for the project variable ProjectText by name
        project = PRA_DataAccessor.getClientProjectDetailsbyProjectName(ProjectText);
        
        //set the labels, project Name, ID and userpreferences if the project is returned.
        if(project!=null ){
            userPreference = PRA_Utils.retrieveUserPreference();
            PRA_Utils.setUserPreferenceforProject(project.id,userPreference);
            userPreference = PRA_Utils.retrieveUserPreference();
            selectedProjectLabel = project.name;                       //set the project label
        }
        
        //default the selected action to TO_DISPLAY_ALL_CLIENT_TASKS on page load or on search.
        selectedAction = TO_DISPLAY_ALL_CLIENT_TASKS; 
    }   
    
    //reset the user preferences
    public void Reset() {
        
        //search on empty project to remove all the valeus from UI.
        MainSearch(); 
        
        //default to Home when there is no project.
        selectedProjectlabel  = PRA_Constants.ST_DEFAULT_PROJECT_LABEL;  
        
        //Delete all the user preferneces for this user.
        PRA_Utils.deleteUserPreference();    
    }
    
    /**
    *  getTaskListWrapper method is used to get the client tasks for a particular project for a particular action.          
    */
    public List<PRA_ClientTaskService.ClientTaskWrapper> getTaskListWrapper() {
        
        //Make a wrapper list to display on the page         
        List<PRA_ClientTaskService.ClientTaskWrapper> retVal = new List<PRA_ClientTaskService.ClientTaskWrapper>();
        
        //get the list for a project.
        if(project != null) {           
            retVal = PRA_ClientTaskService.getClientTaskWrapperListbyAction(project.id,selectedAction);            
        }
        return retVal;
    }
    
    /**
    *  updateClientTasksclosedate method is used to close/open the client tasks for a particular project for a particular action.          
    */
    public void updateClientTasksclosedate() {                  
        try{
            system.debug('-----selectedTasks-------'+selectedTasks);            
            String ErrorMsg;
            
            //validate for failed CO if you have any 
            ErrorMsg = PRA_ClientTaskService.ValidatefailedCO(project.id,'Failed');
            
            //get the taskIDlist from the UI that are selected
            List<String> liTaskIds = (List<String>)JSON.deserialize(selectedTasks, List<String>.class); 
            
            ctCloseDate = null;
            //assign the close date for the tasks as start of next month.
            if(selectedAction == PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS)                   
               ctCloseDate  = Date.today().addMonths(1).toStartOfMonth();
             
            //update the tasks with thier close/re-open action. 
            if(ErrorMsg==null || ErrorMsg =='')  
                ErrorMsg = PRA_ClientTaskService.updateClientTaskwithCloseDate(liTaskIds,ctCloseDate);
            
            if(ErrorMsg !=null && ErrorMsg !='')
                Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, ErrorMsg ));                         
        }
        catch(Exception ex) {
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, ex.getMessage()));             
        }
    }
    
    /**
    *  selectAllTasksmethod is used to select all the client tasks at once, but not using now might use in future.          
    */
    public void selectAllTasks() {
        bSelectAllTasks = !bSelectAllTasks;
    }
}