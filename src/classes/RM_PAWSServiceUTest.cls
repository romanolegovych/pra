/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_PAWSServiceUTest {

	static COUNTRY__c country;
    static List<WFM_Location_Hours__c> hrs;
    static List<WFM_Employee_Availability__c> avas;
    static WFM_employee_Allocations__c alloc;
    static WFM_employee_Allocations__c alloc1;
    static WFM_employee_Allocations__c allocR;
    static List<WFM_Employee_Assignment__c> assigns;
    static WFM_Employee_Assignment__c ass1; 
    
    static Employee_Details__c  empl;
    static WFM_Project__c project;
    static WFM_Project__c projectB;
    static List<string> months;
 
    
    static void init(){
       
        country = new COUNTRY__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0);
        insert country;
       
        months = RM_Tools.GetMonthsList(0, 6);
              
    
        empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='100', Employee_Unique_Key__c='100', First_Name__c='John', Last_Name__c='Smith',email_Address__c='JSmith@gmail.com', 
        Function_code__c='PR',Buf_Code__c='KCICR', Job_Class_Desc__c='A', Business_Unit__c='RDU',Business_Unit_Desc__c='Clinical Informatics', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'),  
        User__c = UserInfo.getUserID(),  FTE_Equivalent__c=0.5, location_code__c = 'TTT');
        insert empl;
        
         //need to insert project
        WFM_Client__c client = new WFM_Client__c(name='TestClient', Client_Name__c='TestClinet_Name', Client_Unique_Key__c='TestClient');
        insert client;
        
        Date projectEnd = Date.today().addMonths(10);
        
        project = new WFM_Project__c(name='TestProject', wfm_client__c=client.id, Project_Unique_Key__c='TestProject', project_status__c ='RM',  Status_Desc__c='Active', project_end_date__c = projectEnd);
        insert project;
        projectB = new WFM_Project__c(name='TestProjectB', wfm_client__c=client.id, Project_Unique_Key__c='TestProjectB', project_status__c ='BB',  Status_Desc__c='Bid');
        insert projectB;
        
        hrs = new List<WFM_Location_Hours__c>();
        for (string month:months){
            WFM_Location_Hours__c hr = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + month,YEAR_MONTH__c=month,NBR_Business_Day_In_Month__c=22);
            hrs.add(hr); 
        }
        insert hrs; 
       
        avas = new List<WFM_Employee_Availability__c>();
        
        List<WFM_employee_Allocations__c> allocs = new List<WFM_employee_Allocations__c>();
        
        Date allEndDate = Date.Today().addMonths(6);
        alloc= new WFM_employee_Allocations__c(EMPLOYEE_ID__c=empl.id, Project_Buf_Code__c='KCICR',Is_Lead__c=true,Project_Country__c='United State',  Project_ID__c=project.id, Project_Function__c='CR', 
            allocation_end_date__c =allEndDate,allocation_start_date__c =Date.Today(),
            Status__c='Confirmed', Allocation_Unique_Key__c=empl.name+project.name+'KCICRUnited State');
        allocs.add(alloc);
        alloc1= new WFM_employee_Allocations__c(EMPLOYEE_ID__c=empl.id, Project_Buf_Code__c='KCICY',Project_Country__c='United State',  Project_ID__c=project.id, Project_Function__c='CY', 
        allocation_end_date__c =allEndDate,allocation_start_date__c =Date.Today(),
            Status__c='Proposed', Allocation_Unique_Key__c=empl.name+project.name+'KCICYUnited State');  // same project different bufcode
        allocs.add(alloc1);
        
        allocR = new WFM_employee_Allocations__c(EMPLOYEE_ID__c=empl.id, Project_Buf_Code__c='KCICR',Project_Country__c='United State',  Project_ID__c=projectB.id, Project_Function__c='CRA', 
            Status__c='Proposed', Allocation_Unique_Key__c=empl.name+projectB.name+'KCICRUnited State');
        allocs.add(allocR);
        
        insert allocs;
            
        assigns = new List<WFM_Employee_Assignment__c>();
        
        for (Integer i = 0; i < months.size(); i++){
            
            WFM_Employee_Availability__c ava = new WFM_Employee_Availability__c(EMPLOYEE_ID__c=empl.id, year_month__c=months[i], Availability_Unique_Key__c=empl.name+':'+months[i], 
            Location_Hours__c = hrs[i].id);
            avas.add(ava);
        }
        insert avas;
            
        for (Integer i = 0; i < months.size(); i++){    
            WFM_Employee_Assignment__c ass =  new WFM_Employee_Assignment__c(Assignment_Unique_Key__c=avas[i].id+(string)alloc.id, EMPLOYEE_ID__c = empl.id, Availability_FK__c=avas[i].id, Allocation_Key__c=alloc.id, Year_Month__c=months[i], 
            Assigned_unit__c='FTE', assigned_value__c = 0.1);
            
            assigns.add(ass);
        }
        
        insert assigns;
        
        ass1 =  new WFM_Employee_Assignment__c(Assignment_Unique_Key__c=avas[0].id+(string)alloc1.id, EMPLOYEE_ID__c = empl.id, Availability_FK__c=avas[0].id, Allocation_Key__c=alloc1.id, Year_Month__c=months[0], 
            Assigned_unit__c='Hours', assigned_value__c = 10);
        
    }
    static testMethod void getActiveAssignmentByProjectTest() {
        init();
        Test.startTest();
        list<RM_PAWSCVO> lstA = RM_PAWSService.getActiveAssignmentByProject(project.name);
        Test.StopTest();
        system.assertEquals(lstA[0].Project_Role, alloc.Project_Function__c);
    }
    static testMethod void getActiveAssignmentByProjectinBUTest() {
        init();
        Test.startTest();
        list<RM_PAWSCVO> lstA = RM_PAWSService.getActiveAssignmentByProjectinBU(project.name, empl.Business_Unit_Desc__c);
        Test.StopTest();
        system.assertEquals(lstA[0].Project_Role, alloc.Project_Function__c);
    }
      static testMethod void getActiveAssignmentByProjectinBUwithRoleTest() {
        init();
        Test.startTest();
        list<RM_PAWSCVO> lstA = RM_PAWSService.getActiveAssignmentByProjectinBUwithRole(project.name, empl.Business_Unit_Desc__c, alloc.project_function__c);
        Test.StopTest();
        system.assertEquals(lstA[0].Project_Role, alloc.Project_Function__c);
    }
}