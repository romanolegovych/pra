public with sharing class CM_AgreementForProtocolCountry{

    //Member variables
    //Get Record Type Id of type "Template"
    public static String RECORD_ID =  [SELECT Id from RecordType WHERE SObjectType = 'Apttus__APTS_Agreement__c' and DeveloperName = 'Template'].Id;
    public Apttus__APTS_Agreement__c agreement{ get; set; }
    public Boolean isEdit{ get; set; }
    
    public CM_AgreementForProtocolCountry(ApexPages.StandardSetController controller) {
        System.debug('***** RECORD_ID ***** '+RECORD_ID );
        agreement = new Apttus__APTS_Agreement__c();
        String pcid = ApexPages.CurrentPage().getParameters().get('id');
        System.debug('***** Id ***** '+pcid);
        
        if(!String.isEmpty(RECORD_ID)){
            isEdit = false;
            agreement.RecordTypeId = RECORD_ID;
            agreement.Name = 'TemplatePC__'+DateTime.Now().format('yyyy-MM-dd_hh_mm_ss');
            agreement.Protocol_Country__c = pcid;
        }
    }
    
    /**
     * @author  Prashant Wayal
     * @date    17-July-2014 
     * @description This method is used to save the agreement for Protocol Country Object.
    */
    
    //Method to save agrement
    public pageReference saveAgreement(){
        if(agreement != null){
            try{
                upsert agreement;
                PageReference redirect = new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+agreement.Protocol_Country__c);
                redirect.setRedirect(true);
                return redirect;
            }
            catch(Exception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error while saving agreement: '+e));
                return null;
            }
        }
        return null;
    }
}