/**
 * @description	Implements the test for the functions of the NCM_PendingAcknowledgeBatch class
 * @author		Kalaiyarasan Karunanithi
 * @version		Created: 04-Sep-2015, Edited: 11-Sep-2015
 */
@isTest
private class NCM_PendingAcknowledgeBatchTest {
	
	/**
	 * @description	Check the NCM_PendingAcknowledgeBatch class: start, execute, finish methods.
	 * @author		Kalaiyarasan Karunanithi, Dimitrios Sgourdos
	 * @date		Created: 11-Sep-2015
	 */
	static testMethod void PendingAcknowledgeBatchTest() {
		// Create data
		NotificationCatalog__c catalog = new NotificationCatalog__c(NotificationName__c = 'Topic A',
													ImplementationClassName__c = 'Class A',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION,
													Acknowledgementrequired__c = true,
													SMSallowed__c = true);
		insert catalog;
		
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'C', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'D', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		List<NotificationRegistration__c> source = new List<NotificationRegistration__c>();
		source.add( new NotificationRegistration__c(Name = 'A',
													NotificationCatalog__c = catalog.Id,
													MaxNumberReminders__c = 5,
													ReminderPeriod__c = 10,
													UserId__c = testUser.Id,
													RelatedToId__c = accList[0].Id,
													Active__c = false,
													NotifyByEmail__c = true,
													NotifyBySMS__c = true) );
		source.add( new NotificationRegistration__c(Name = 'B',
													NotificationCatalog__c = catalog.Id,
													MaxNumberReminders__c = 7,
													ReminderPeriod__c = 20,
													UserId__c = testUser.Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true,
													NotifyByEmail__c = true,
													NotifyBySMS__c = false) );
		source.add( new NotificationRegistration__c(Name = 'C',
													NotificationCatalog__c = catalog.Id,
													MaxNumberReminders__c = 4,
													ReminderPeriod__c = 30,
													UserId__c = testUser.Id,
													RelatedToId__c = accList[2].Id,
													Active__c = true,
													NotifyByEmail__c = true,
													NotifyBySMS__c = true) );
		source.add( new NotificationRegistration__c(Name = 'D',
													NotificationCatalog__c = catalog.Id,
													MaxNumberReminders__c = 3,
													ReminderPeriod__c = 40,
													UserId__c = testUser.Id,
													RelatedToId__c = accList[3].Id,
													Active__c = true,
													NotifyByEmail__c = false,
													NotifyBySMS__c = true) );
		source.add( new NotificationRegistration__c(Name = 'E',
													NotificationCatalog__c = catalog.Id,
													MaxNumberReminders__c = 2,
													ReminderPeriod__c = 50,
													UserId__c = testUser.Id,
													RelatedToId__c = NULL,
													Active__c = true,
													NotifyByEmail__c = false,
													NotifyBySMS__c = true) );
		insert source;
		
		DateTime todayDate = Date.today();
		List<Notification__c> notificationList = new List<Notification__c>();
		notificationList.add(new Notification__c(NotificationRegistration__c = source[0].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = todayDate - 4));
		notificationList.add(new Notification__c(NotificationRegistration__c = source[1].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = todayDate - 3));
		notificationList.add(new Notification__c(NotificationRegistration__c = source[2].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = todayDate - 2));
		notificationList.add(new Notification__c(NotificationRegistration__c = source[3].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = todayDate + 10));
		notificationList.add(new Notification__c(NotificationRegistration__c = source[4].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = todayDate - 1));
		insert notificationList;
		
		List<NotificationHistory__c> notificationHistoryList = new List<NotificationHistory__c>();
		notificationHistoryList.add(new NotificationHistory__c(Notification__c = notificationList[0].Id, 
																EmailSent__c = true,
																SMSsent__c = true));
		notificationHistoryList.add(new NotificationHistory__c(Notification__c = notificationList[1].Id, 
																EmailSent__c = true,
																SMSsent__c = false));
		notificationHistoryList.add(new NotificationHistory__c(Notification__c = notificationList[2].Id, 
																EmailSent__c = true,
																SMSsent__c = true));
		notificationHistoryList.add(new NotificationHistory__c(Notification__c = notificationList[3].Id, 
																EmailSent__c = false,
																SMSsent__c = true));
		notificationHistoryList.add(new NotificationHistory__c(Notification__c = notificationList[4].Id, 
																EmailSent__c = false,
																SMSsent__c = true));
		insert notificationHistoryList;
		
		String errorMessage = 'Error in notify users by reminders';
		
		// Check the batchable job
		Test.startTest();
		NCM_PendingAcknowledgeBatch pendingAcknowledgeBatch = new NCM_PendingAcknowledgeBatch();
		Database.executeBatch(pendingAcknowledgeBatch);
		Test.stopTest();
		
		List<Notification__c> notificationResults = [SELECT ID,
															Reminder__c
													FROM	Notification__c
													ORDER BY NotificationRegistration__r.Name
													LIMIT	10];
														
		system.assertEquals(5, notificationResults.size(), errorMessage);
		system.assertEquals(todayDate-4, notificationResults[0].Reminder__c, errorMessage);
		system.assertEquals((todayDate-3).addHours(20), notificationResults[1].Reminder__c, errorMessage);
		system.assertEquals((todayDate-2).addHours(30), notificationResults[2].Reminder__c, errorMessage);
		system.assertEquals(todayDate+10, notificationResults[3].Reminder__c, errorMessage);
		system.assertEquals((todayDate-1).addHours(50), notificationResults[4].Reminder__c, errorMessage);
		
		List<NotificationHistory__c> historyResults =	[SELECT Id,
																Notification__c,
																EmailSent__c,
																SMSsent__c
														FROM	NotificationHistory__c
														ORDER BY Notification__r.NotificationRegistration__r.Name
														LIMIT	10];
		
		system.assertEquals(8, historyResults.size(), errorMessage);
		
		system.assertEquals(notificationList[0].Id, historyResults[0].Notification__c, errorMessage);
		system.assertEquals(true, historyResults[0].EmailSent__c, errorMessage);
		system.assertEquals(true, historyResults[0].SMSsent__c, errorMessage);
		
		system.assertEquals(notificationList[1].Id, historyResults[1].Notification__c, errorMessage);
		system.assertEquals(true, historyResults[1].EmailSent__c, errorMessage);
		system.assertEquals(false, historyResults[1].SMSsent__c, errorMessage);
		
		system.assertEquals(notificationList[1].Id, historyResults[2].Notification__c, errorMessage);
		system.assertEquals(true, historyResults[2].EmailSent__c, errorMessage);
		system.assertEquals(false, historyResults[2].SMSsent__c, errorMessage);
		
		system.assertEquals(notificationList[2].Id, historyResults[3].Notification__c, errorMessage);
		system.assertEquals(true, historyResults[3].EmailSent__c, errorMessage);
		system.assertEquals(true, historyResults[3].SMSsent__c, errorMessage);
		
		system.assertEquals(notificationList[2].Id, historyResults[4].Notification__c, errorMessage);
		system.assertEquals(true, historyResults[4].EmailSent__c, errorMessage);
		system.assertEquals(true, historyResults[4].SMSsent__c, errorMessage);
		
		system.assertEquals(notificationList[3].Id, historyResults[5].Notification__c, errorMessage);
		system.assertEquals(false, historyResults[5].EmailSent__c, errorMessage);
		system.assertEquals(true, historyResults[5].SMSsent__c, errorMessage);
		
		system.assertEquals(notificationList[4].Id, historyResults[6].Notification__c, errorMessage);
		system.assertEquals(false, historyResults[6].EmailSent__c, errorMessage);
		system.assertEquals(true, historyResults[6].SMSsent__c, errorMessage);
		
		system.assertEquals(notificationList[4].Id, historyResults[7].Notification__c, errorMessage);
		system.assertEquals(false, historyResults[7].EmailSent__c, errorMessage);
		system.assertEquals(true, historyResults[7].SMSsent__c, errorMessage);
	}
}