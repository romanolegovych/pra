@isTest
private class PIT_LogCallTaskControllerTest {

    static testMethod void myUnitTest() {
       // create the controller
		ApexPages.standardController stdCtr;
		PIT_LogCallTaskController p = new PIT_LogCallTaskController(stdCtr);
		p.downTask.Subject = 'testing';
		p.save();
		// Test the save
		List<Task> taskList = [SELECT id FROM Task];
		System.assertEquals(taskList.size(), 2); // the Call task and the 'testing' task
    }
}