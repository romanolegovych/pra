public class IPA_DAL_CategoryAndTopic{
    
    //This function returns a set of Topic Ids assigned to a given Page Widget instance
    public  Set<Id> returnPageWidgetTopics(Id pageWidgetId) {
        Set<Id> topicIds = new Set<Id>();
        List<IPA_Page_Widget_Topic__c> widgetTopicList = new List<IPA_Page_Widget_Topic__c>();
        try {
            widgetTopicList = [SELECT Topic__c 
                               FROM IPA_Page_Widget_Topic__c 
                               WHERE Page_Widget__c = : pageWidgetId AND Topic__r.Active__c = true];
        }catch(Exception e) {
            system.debug('Error fetching Widget\'s Topics. The system error is: ' + e);
        }
        if(!widgetTopicList.isEmpty()){
            for(IPA_Page_Widget_Topic__c widgetTopic : widgetTopicList){
                topicIds.add(widgetTopic.Topic__c);
            }
        }
        return topicIds;
    }
    
    //This function returns the default Category Id for the app
    public  Id returnDefaultCategory() {
        List<IPA_Page__c> pagesList = new List<IPA_Page__c>();
        Id catId;
        try{
            pagesList = [Select Category__c from IPA_Page__c Where Master_Landing_Page__c = true Limit 1];
            catId = pagesList[0].Category__c;
        }catch(Exception e) {
            system.debug('Error fetching Default Category for the App. The system error is: ' + e);
        }
        return catId;
    }
    
    //This function returns a set of Link Ids assigned to a given Page Widget instance
    public  Set<Id> returnPageWidgetLinks(Id pageWidgetId) {
        Set<Id> linkIds = new Set<Id>();
        List<IPA_Page_Widget_Link__c> widgetLinkList = new List<IPA_Page_Widget_Link__c>();
        try {
            widgetLinkList = [SELECT Link__c 
                               FROM IPA_Page_Widget_Link__c 
                               WHERE Page_Widget__c = : pageWidgetId];
        }catch(Exception e) {
            system.debug('Error fetching Widget\'s Links. The system error is: ' + e);
        }
        if(!widgetLinkList.isEmpty()){
            for(IPA_Page_Widget_Link__c widgetLink : widgetLinkList){
                linkIds.add(widgetLink.Link__c);
            }
        }
        return linkIds;
    }
    
    
    //This function returns a set of Content Ids assigned to a given Page Widget instance
    public  Set<Id> returnPageWidgetContents(Id pageWidgetId) {
        Set<Id> contentIds = new Set<Id>();
        List<IPA_Page_Widget_Content__c> widgetContentList = new List<IPA_Page_Widget_Content__c>();
        try {
            widgetContentList = [SELECT Content__c
                               FROM IPA_Page_Widget_Content__c 
                               WHERE Page_Widget__c = : pageWidgetId AND Content__r.Display_Date__c >= today AND Content__r.Published__c = true];
        }catch(Exception e) {
            system.debug('Error fetching Widget\'s Contents. The system error is: ' + e);
        }
        if(!widgetContentList.isEmpty()){
            for(IPA_Page_Widget_Content__c widgetContent : widgetContentList){
                contentIds.add(widgetContent.Content__c);
            }
        }
        return contentIds;
    }

    //This function returns all set of Content Ids assigned to a given Page Widget instance
    public  Set<Id> returnAllPageWidgetContents(Id pageWidgetId) {
        Set<Id> contentIds = new Set<Id>();
        List<IPA_Page_Widget_Content__c> widgetContentList = new List<IPA_Page_Widget_Content__c>();
        try {
            widgetContentList = [SELECT Content__c
                               FROM IPA_Page_Widget_Content__c 
                               WHERE Page_Widget__c = : pageWidgetId AND Content__r.Published__c = true];
        }catch(Exception e) {
            system.debug('Error fetching Widget\'s Contents. The system error is: ' + e);
        }
        if(!widgetContentList.isEmpty()){
            for(IPA_Page_Widget_Content__c widgetContent : widgetContentList){
                contentIds.add(widgetContent.Content__c);
            }
        }
        return contentIds;
    }
}