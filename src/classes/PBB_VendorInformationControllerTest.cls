/**
@author Niharika Reddy
@date 2015
@description this test class is to display Vendor and Other Vendor selection.
**/

@isTest
public class PBB_VendorInformationControllerTest{    
    public testMethod static void testPBB_VendorInformationController(){    	
        PBB_TestUtils tu = new PBB_TestUtils();
        WFM_Project__c wp = tu.createwfmproject();
        Bid_Project__c bidProject = tu.createbidproject();
        Vendor_Category__c vendorCategory = tu.createVendorCategory();
        Vendor__c vendor = tu.createVendor();
        Project_Vendors__c projectVendor = tu.createProjectVendors();
        Project_Vendors__c projectVendor1 = tu.createOtherProjectVendors();
        ApexPages.StandardController cont = new ApexPages.StandardCOntroller(projectVendor);
        PBB_VendorInformationController vendorInfo = new PBB_VendorInformationController(cont);
        vendorInfo.selectedValue = vendorCategory.Name;
        System.assertEquals(vendorInfo.availVendorsList.size(), 0);
        System.assertEquals(vendorInfo.selectedVendorsList.size(), 1);
        ApexPages.currentPage().getParameters().put('index','1');
        PBB_VendorInformationController.OtherVendors otherVendors = new PBB_VendorInformationController.OtherVendors('');
        vendorInfo.otherVendorsList.add(new PBB_VendorInformationController.OtherVendors('Test1'));        
        vendorInfo.savePV();        
        vendorInfo.otherVendorsList.add(new PBB_VendorInformationController.OtherVendors('OV1'));        
        vendorInfo.savePV();        
        vendorInfo.addRow();
        vendorInfo.delRow();        
        PageReference pa = vendorInfo.Cancel();        
    } 
}