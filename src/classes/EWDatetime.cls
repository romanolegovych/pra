/**
* A wrapper class for Datetime standard class, which provides a set of 
* EW-related handy methods.
*/
public class EWDatetime
{
	private Datetime actualDatetime;
	public EWDatetime(Datetime actualDatetime)
	{
		this.actualDatetime = actualDatetime;
	}
	
	public Datetime addWorkdays(Decimal days)
	{
		Integer direction = (days >= 0 ? 1 : -1);
		days = Math.abs(days);
		
		Integer i = 0;
		while (days > 0)
		{
			String day = actualDatetime.addDays(direction * i).format('EEEE', 'GMT');
			if (day != 'Saturday' && day != 'Sunday')
			{
				days--;
			}
			
			i++;
		}
		
		return actualDatetime.addDays(direction * Math.max(0, i - 1)).dateGmt();
	}
	
	public Integer getDay(Datetime value)
	{
		return Integer.valueOf(value.formatGmt('u'));
	}
	
	public Date toMonday(Date value)
	{
		return value.addDays(1 - getDay(value));
	}
	
	public Integer workdaysBetween(Datetime anotherDatetime)
	{
		if (actualDatetime > anotherDatetime)
		{
			return - new EWDatetime(anotherDatetime).workdaysBetween(actualDatetime);
		}
		//else if (getDay(actualDatetime) > 5)//in case of when starting date is on weekend
		//{
		//	return new EWDatetime(toMonday(actualDatetime.dateGmt().addDays(3))).workdaysBetween(anotherDatetime);
		//}
		
		Date startMonday = toMonday(actualDatetime.dateGmt());
		Date endMonday = toMonday(anotherDatetime.dateGmt()).addDays(7);
		
		//System.debug(System.LoggingLevel.ERROR, 'From: ' + actualDatetime + ' to: ' + anotherDatetime
		//	+ '\nStart monday: ' + startMonday + ' end monday: ' + endMonday
		//	+ '\nDays between mondays: ' + ((startMonday.daysBetween(endMonday)/7) * 5)
		//	+ '\nStart day: ' + getDay(actualDatetime) + ' end day: ' + getDay(anotherDatetime));//@TODO: Delete this line!
		
		return (startMonday.daysBetween(endMonday)/7) * 5 - Math.max(0, 5 - (getDay(anotherDatetime) - 1)) - Math.min(5, getDay(actualDatetime) - 1);
		/*
		Integer direction = (actualDatetime <= anotherDatetime ? 1 : -1);
		
		Integer i = 0;
		Integer days = 0;
		while (actualDatetime.addDays(direction * i).date() != anotherDatetime.date())
		{
			String day = actualDatetime.addDays(direction * i).format('EEEE');
			if (day != 'Saturday' && day != 'Sunday')
			{
				days++;
			}
			
			i++;
		}
		
		return direction * days;
		/**/
	}
}