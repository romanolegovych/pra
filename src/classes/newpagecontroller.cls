public with sharing class newpagecontroller {
    public String pbbScenarioID{get;set;}
    public String pbbScenarioCountryID{get;set;}
    public List<PBB_Scenario_Country_Site_Weekly_Events__c> pscswe{get;set;}
    public ID bidscenario{get;set;}
    public String scenarioCountry{get;set;}
    public Boolean isEdit{get;set;}
    
    public  newpagecontroller(ApexPages.StandardController controller){
    isEdit = false;
    
    }
    
    public List<selectOption> getscenariolist(){
    
        List<selectOption> scenariolist = new List<selectOption>();
        
        //to add blank value as default
        scenariolist.add(new SelectOption('','  ')); 
        
        for(PBB_Scenario__c ps: PBB_DataAccessor.getPbbScenarioOrderByDesc()) {
            scenariolist.add(new selectOption(ps.Id,ps.name));
        } 
        return scenariolist;
    }
    
    public List<selectOption> getcountrylist(){
    
        List<SelectOption> cntrylist = new List<SelectOption>();
        cntrylist.add(new SelectOption('',' '));
        system.debug('Selected Scenario'+bidscenario);
        for(PBB_Scenario_Country__c psc: PBB_DataAccessor.GetSelectedCountries(bidscenario)){
            cntrylist.add(new selectOption(psc.Id,psc.Country__r.Name));
        }
        return cntrylist;
    }
    
    public pageReference refreshData(){
        pscswe = [Select Id, Name, Actual_Estimated_Sites__c, Actual_Estimated_Enrollment__c, Actual_Subjects__c, Current_Record__c, Enrollment_Factor__c,
                         Expected_Subjects__c, Next_Record__c, PBB_Scenario_Country__c, Planned_Enrollment_Rate__c, Planned_Sites__c, Planned_Subjects__c, Prev_Record__c,
                         Revised_Enrollment_Rate__c, Revised_Sites__c, Revised_Subjects__c from  PBB_Scenario_Country_Site_Weekly_Events__c where PBB_Scenario_Country__c =:scenarioCountry];
        system.debug('Weekly Data'+pscswe);
        return null;
    }
    
    public pageReference editrecord(){
        isEdit = true;
        return null;
    }
}