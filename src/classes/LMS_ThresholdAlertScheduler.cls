global class LMS_ThresholdAlertScheduler implements Schedulable {
	
	private static final String EMAIL_LOAD_SUBJECT = 'CLEAR LOAD ALERT - ';
	
	global void execute(SchedulableContext cxt) {
		
		Map<String, LMSConstantSettings__c> constants = LMSConstantSettings__c.getAll();
		String thresholdNumber = constants.get('alertThreshold').Value__c;
		list<String> emailRecipients = constants.get('alertThresholdRecipients').Value__c.split(',');
		String env = constants.get('sfEnvironment').Value__c;
		
		list<Messaging.SingleEmailMessage> mail = new list<Messaging.SingleEmailMessage>();
		
		Integer roleCoursePendingMappings = LMS_ToolsDataAccessor.getPendingRoleCourseUpdates();
		Integer roleEmployeePendingMappings = LMS_ToolsDataAccessor.getPendingRoleEmployeeUpdates();
		Integer roleEmployeeTransitAddMappings = LMS_ToolsDataAccessor.getTransitAddRoleEmployees();
		Integer roleEmployeeTransitDeleteMappings = LMS_ToolsDataAccessor.getTransitDeleteRoleEmployees();
		Integer totalPendingMappings = roleCoursePendingMappings + roleEmployeePendingMappings;
		Integer totalTransitMappings = roleEmployeeTransitAddMappings + roleEmployeeTransitDeleteMappings;
		
		if (totalPendingMappings >= Integer.valueOf(thresholdNumber) || totalTransitMappings > 0) {
			String emailBody = '<html><body>This email is being sent because there are greater than ' + Integer.valueOf(thresholdNumber) 
				+ ' pending mappings or there are role-person commits that are in '
				+ 'transit-add or transit-delete status indicating a failure.<br/><br/>'
				+ 'If the pending threshold has been reached please disable scheduled CLEAR batch '
				+ 'jobs and prepare to run the mappings manually, 200 at a time.<br/><br/>'
				+ 'For transit-add and transit-delete statuses, please synchronize CLEAR and SABA.<br/><br/>'
				+ 'Pending Mappings: ' + totalPendingMappings + '<br/>'
				+ 'Transit Mappings: ' + totalTransitMappings 
				+ '</body></html>';
			mail.add(sendEmailAlert(EMAIL_LOAD_SUBJECT, emailBody, env, emailRecipients));
		}
		
		Messaging.sendEmail(mail);
	}
	
	private static Messaging.SingleEmailMessage sendEmailAlert(String subject, String body, String env, list<String> recipients) {
		
		Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
		mail.setToAddresses(recipients);
        mail.setSaveAsActivity(false);
        mail.setUseSignature(false);
        mail.setSubject(subject + env);
        mail.setHtmlBody(body);

		return mail;
	}

}