@isTest
private class FlowchartServiceTest {

	static testMethod void Test () {

		// create test data
		Client_Project__c cp = BDT_TestDataUtils.buildProject();
		insert cp;
		List<ClinicalDesign__c> designList = BDT_TestDataUtils.buildClinicDesign(cp,1);
		insert designList;

		List<Flowcharts__c> flowchartList =  BDT_TestDataUtils.buildFlowcharts(designList,1);
		insert flowchartList;
		List<Arm__c> armList = BDT_TestDataUtils.buildArms(designList,1);
		insert armList;
		List<Epoch__c> epochList = BDT_TestDataUtils.buildEpochs(designList,1);
		insert epochList;
		
		List<flowchartassignment__c> FlowchartAssignmentList = BDT_TestDataUtils.buildFlowchartAssignment (designList,
																flowchartList,
																armList,
																epochList);
		insert FlowchartAssignmentList;
		
		List<ServiceCategory__c> categoryList = BDT_TestDataUtils.buildServiceCategory(1, null);
		insert categoryList;
		List<Service__c> serviceList = BDT_TestDataUtils.buildService(categoryList, 5, 'tst', true);
		insert serviceList;
		List<ProjectService__c> projectServiceList = BDT_TestDataUtils.buildProjectService(serviceList, cp.Id);
		insert projectServiceList;
		List<TimePoint__c> timepointList = BDT_TestDataUtils.buildTimePoints(flowchartList, 5);
		for (integer i = 0; i < 5; i++){
			timepointList[i].Minutes__c = i-2;
		}
		insert timepointList;
		List<ProjectServiceTimePoint__c> projectServiceTimepointList = BDT_TestDataUtils.buildProjectServiceTimePoint(
			projectServiceList, timepointList);
		For (ProjectServiceTimePoint__c pst:projectServiceTimepointList){
			pst.listOfTimeMinutes__c = '-1:-2:0:1:2';
			pst.TimePointCount__c = 5;
		}
		insert projectServiceTimepointList;
		
		// start test RecodeOldData
		Boolean migrationNeeded = FlowchartService.RecodeOldData(cp.id, false);
		system.assert(migrationNeeded);
		timepointList = [select id from TimePoint__c];
		system.assertEquals(0,timepointList.size());
		flowchartList = [select listOfTimeMinutes__c from Flowcharts__c where id = :flowchartList[0].id];
		system.assertEquals(5,flowchartList[0].listOfTimeMinutes__c.split(':').size());
		
		// start test flowchartDisplay
		FlowchartService.flowchartDisplay display = new FlowchartService.flowchartDisplay(flowchartList[0].id,cp.id, NULL);
		
		// remove service
		system.assertEquals(5, display.FlowchartServices.size());
		display.removeService(serviceList[0].id);
		system.assertEquals(4, display.FlowchartServices.size());
		
		// add service
		display.addServiceToFlowchart(serviceList[0],categoryList[0]);
		system.assertEquals(5, display.FlowchartServices.size());

		// add timepoint
		display.addTimePoints (new List<integer>{100});
		system.assertEquals(6,display.TimePointsInFlowchart.size());
		
		// remove timepoint
		display.removeTimePoints (new List<integer>{100});
		system.assertEquals(5,display.TimePointsInFlowchart.size());
		
		// remove unused timepoints
		display.addTimePoints (new List<integer>{100,101,102,103});
		system.assertEquals(9,display.TimePointsInFlowchart.size());
		display.deleteUnusedTimepoints();
		system.assertEquals(5,display.TimePointsInFlowchart.size());
		
		display.checkTimePointsUsed ();
		
		display.saveFlowchartAndProjectServiceTimepointChanges ();
		
		
		////servicecategoriesForFlowcharts
		FlowchartService.servicecategoriesForFlowcharts b = new FlowchartService.servicecategoriesForFlowcharts(display);
		
	}
	
	
	static testMethod void copyTest () {
		BDT_BuildTestData.createAllData();
		
		Client_Project__c cp = [select id from Client_Project__c limit 1];
		List<Flowcharts__c> flowchartList = [select id from Flowcharts__c];
		
		// start test RecodeOldData
		Boolean migrationNeeded = FlowchartService.RecodeOldData(cp.id, false);
		system.assert(migrationNeeded);
		
		// start test flowchartDisplay
		FlowchartService.flowchartDisplay display = new FlowchartService.flowchartDisplay(flowchartList[0].id,cp.id, NULL);
		FlowchartService.CopyFlowchart copyHandler = new FlowchartService.CopyFlowchart(display);
		
		copyHandler.SelectedFlowchartID = flowchartList[1].id;
		
		// continue to the selection of the services and timepoint to be copied
		copyHandler.prepareFlowchartCopyDataList();
		
		// select everything
		For (FlowchartService.FlowchartCopyData fcd : copyHandler.flowchartCopyDataList) {
			fcd.shouldServiceBeCopied = true;
			fcd.shouldTimePointsBeCopied = true;
		}
		
		// run the copy
		copyHandler.copyFlowChart();
		
	}
	
}