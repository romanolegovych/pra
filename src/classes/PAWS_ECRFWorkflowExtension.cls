global without sharing class PAWS_ECRFWorkflowExtension 
{
	private ApexPages.StandardController controller;
	
	global PAWS_ECRFWorkflowExtension(ApexPages.StandardController controller) 
    {
    	this.controller = controller;
    }
    
    public String CursorsJSON
    {
    	get
    	{
    		if(CursorsJSON == null) CursorsJSON = JSON.serialize(loadCursors(controller.getRecord().Id));
    		return CursorsJSON;
    	}
    	set;
    }
    
    public Boolean IsOldVersion
    {
    	get
    	{
    		try 
    		{
    			return [select Id from ApexClass where Name = 'FlowStepSkipConditionEditExtension'].size() == 0;
    		}
    		catch (Exception e)
    		{
    			return false;
    		}
    	}
    }
    
    @RemoteAction
    global static Map<String, String> loadCursors(ID recordId)
    {
    	STSWR1.API wrApi = new STSWR1.API();
    	List<STSWR1__Flow_Instance_Cursor__c> cursors = (List<STSWR1__Flow_Instance_Cursor__c>)wrApi.call('WorkflowService', 'getFlowInstanceCursorsByObjectId', recordId);
        Map<ID, List<STSWR1__Flow_Instance_History__c>> historyMap = getHistory(cursors);
        
        Map<String, String> result = new Map<String, String>();
        for(ID cursorId : historyMap.keySet())
        {
        	List<STSWR1__Flow_Instance_History__c> history = historyMap.get(cursorId);
        	if(history.size() > 0) 
        	{
        		String startDate = (history[history.size() - 1].STSWR1__Actual_Start_Date__c != null ? PAWS_Utilities.formatDate(history[history.size() - 1].STSWR1__Actual_Start_Date__c, 'M/d/yyyy') : PAWS_Utilities.formatDate(Date.today(), 'M/d/yyyy'));
        		result.put((String)cursorId, startDate);
        	}       
        }
        
        return result;
    }
    
    private static Map<ID, List<STSWR1__Flow_Instance_History__c>> getHistory(List<STSWR1__Flow_Instance_Cursor__c> records)
    {
    	Map<ID, List<STSWR1__Flow_Instance_History__c>> result = new Map<ID, List<STSWR1__Flow_Instance_History__c>>();
    	
    	Set<ID> ids = new Set<ID>();
    	for(STSWR1__Flow_Instance_Cursor__c record : records)
	        ids.add(record.Id);

    	List<String> fields = new List<String>();
    	Schema.DescribeSObjectResult describe = STSWR1__Flow_Instance_History__c.getSObjectType().getDescribe();
    	fields.addAll(describe.fields.getMap().keySet());
    	
        List<STSWR1__Flow_Instance_History__c> history = (List<STSWR1__Flow_Instance_History__c>)Database.query('select ' + String.join(fields, ',') + ' from STSWR1__Flow_Instance_History__c where STSWR1__Cursor__c in :ids order by STSWR1__Cursor__c, CreatedDate, Name');
		for(STSWR1__Flow_Instance_History__c record : history)
		{
			if(!result.containsKey(record.STSWR1__Cursor__c)) result.put(record.STSWR1__Cursor__c, new List<STSWR1__Flow_Instance_History__c>());
			result.get(record.STSWR1__Cursor__c).add(record);
		}
    	
        return result;
    }
}