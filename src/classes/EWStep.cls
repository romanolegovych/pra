public class EWStep
{
	public Datetime plannedStart { get { return gantt.STSWR1__Planned_Start_Date__c != null ? gantt.STSWR1__Planned_Start_Date__c : expectedStart; } }
	public Datetime plannedEnd { get { return gantt.STSWR1__Planned_End_Date__c != null ? gantt.STSWR1__Planned_End_Date__c : expectedEnd; } }
	public Datetime revisedStart { get { return gantt.STSWR1__Revised_Start_Date__c; } }
	public Datetime revisedEnd { get { return gantt.STSWR1__Revised_End_Date__c; } }
	public Datetime challengingStart;
	public Datetime challengingEnd;
	public Datetime expectedStart;
	public Datetime expectedEnd;
	public Integer index;
	
	public Datetime actualStart
	{
		get
		{
			//if(!result.containsKey(history.STSWR1__Step__c) || history.STSWR1__Step__r.STSWR1__Flow_Branch__c == (ID)history.STSWR1__Cursor__r.STSWR1__Flow_Branch__c)
			//result.put(history.STSWR1__Step__c, history);
			Datetime result;
			for (STSWR1__Flow_Instance_History__c history : extendedStep.STSWR1__Flow_Instance_History__r)
			{
				if (result == null || history.STSWR1__Step__r.STSWR1__Flow_Branch__c == (ID) history.STSWR1__Cursor__r.STSWR1__Flow_Branch__c)
				{
					result = (history.STSWR1__Step__r.STSWR1__Is_System_Value__c == true ? history.STSWR1__Actual_Start_Date_Value__c : history.STSWR1__Actual_Start_Date__c);
				}
			}
			
			return result;
			/*
			for (STSWR1__Flow_Instance_History__c history : extendedStep.STSWR1__Flow_Instance_History__r)
			{
				return history.STSWR1__Actual_Start_Date__c;
			}
			
			return null;
			*/
		}
	}
	
	public Datetime actualEnd
	{
		get
		{
			Datetime result;
			for (STSWR1__Flow_Instance_History__c history : extendedStep.STSWR1__Flow_Instance_History__r)
			{
				if (result == null || history.STSWR1__Step__r.STSWR1__Flow_Branch__c == (ID) history.STSWR1__Cursor__r.STSWR1__Flow_Branch__c)
				{
					result = (history.STSWR1__Step__r.STSWR1__Is_System_Value__c == true ? history.STSWR1__Actual_Complete_Date_Value__c : history.STSWR1__Actual_Complete_Date__c);
				}
			}
			
			return result;
			/*
			for (STSWR1__Flow_Instance_History__c history : extendedStep.STSWR1__Flow_Instance_History__r)
			{
				return history.STSWR1__Actual_Complete_Date__c;
			}
			
			return null;
			*/
		}
	}
	
	public Decimal plannedDuration { get { return gantt.STSWR1__Step__r.STSWR1__Duration_Planned__c; } }
	
	public Decimal remainingTime
	{
		get
		{
			Decimal result = (ccJunction.Remaining_Time__c != null ? ccJunction.Remaining_Time__c : ccJunction.Challenging_Time__c);
			return (actualEnd == null ? result : 0);
		}
	}
	
	public Decimal challengingTime { get { return ccJunction.Challenging_Time__c; } }
	public Decimal bufferTime { get { return ccJunction.Buffer_Time__c; } }
	
	public STSWR1__Flow_Step_Junction__c extendedStep;
	public STSWR1__Gantt_Step_Property__c gantt;
	public Critical_Chain_Step_Junction__c ccJunction;
	
	public EWStep(STSWR1__Flow_Step_Junction__c extendedStep, STSWR1__Gantt_Step_Property__c gantt, Critical_Chain_Step_Junction__c ccJunction)
	{
		this.extendedStep = extendedStep;
		this.gantt = gantt;
		this.ccJunction = ccJunction;
	}
	
	/**
	* Sets expectedStart and expectedDate values, and returns either a passed expectedStart parameter,
	* or a new expectedEnd value
	*/
	public void setExpectedDates(Datetime expectedStart)
	{
		/*Expected Dates calculation*/
		if (actualEnd == null)
		{
			Decimal expectedDuration = (ccJunction.Remaining_Time__c != null ? ccJunction.Remaining_Time__c : ccJunction.Challenging_Time__c);
			
			this.expectedStart = expectedStart;
			expectedEnd = new EWDatetime(expectedStart).addWorkdays(expectedDuration);
		}
	}
	
	public void setChallengingDates(Decimal bufferShift)
	{
		challengingStart = new EWDatetime(revisedStart != null ? revisedStart : plannedStart).addWorkdays(-(bufferShift + 1));
		challengingEnd = new EWDatetime(challengingStart).addWorkdays(ccJunction.Challenging_Time__c);
	}
	
	public Map<String, Object> toDTO()
	{
		Map<String, Object> result = (Map<String, Object>) System.Json.deserializeUntyped(System.Json.serialize(ccJunction));
		result.putAll(
			new Map<String, Object>{
				'Planned_Duration__c' => plannedDuration,
				'Planned_Start_Date__c' => plannedStart,
				'Planned_End_Date__c' => plannedEnd,
				'Challenging_Start_Date__c' => challengingStart,
				'Challenging_End_Date__c' => challengingEnd,
				'Revised_Start_Date__c' => revisedStart,
				'Revised_End_Date__c' => revisedEnd,
				'Expected_Start_Date__c' => expectedStart,
				'Expected_End_Date__c' => expectedEnd,
				'Actual_Start_Date__c' => actualStart,
				'Actual_End_Date__c' => actualEnd,
				'index' => index,
				'Remaining_Time__c' => remainingTime//overrides remaining time with 0 if step is completed
			});
		
		return result;
	}
}