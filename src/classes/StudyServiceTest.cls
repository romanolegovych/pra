/** Implements the test for the Service Layer of the object Study__c
 * @author	Dimitrios Sgourdos
 * @version	18-Nov-2013
 */
@isTest
private class StudyServiceTest {
	
	/** Test the function getStudiesByUserPreference.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Oct-2013
	 */
	static testMethod void getStudiesByUserPreferenceTest() {
		List<Study__c> results = StudyService.getStudiesByUserPreference();
	}
	
	
	/** Test the function getStudiesByProjectId.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	05-Nov-2013
	 */
	static testMethod void getStudiesByProjectIdTest() {
		List<Study__c> results = StudyService.getStudiesByProjectId('');
	}
	
	
	/** Test the function getStudiesByUsedInAN.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Oct-2013
	 */
	static testMethod void getStudiesByUsedInANTest() {
		List<Study__c> results = StudyService.getStudiesByUsedInAN('');
	}
	
	
	/** Test the function getStudiesByUsedInMdOrVal.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	18-Nov-2013
	 */
	static testMethod void getStudiesByUsedInMdOrValTest() {
		List<Study__c> results = StudyService.getStudiesByUsedInMdOrVal('');
	}
	
	
	/** Test the function getStudiesByUsedInMd.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	18-Nov-2013
	 */
	static testMethod void getStudiesByUsedInMdTest() {
		List<Study__c> results = StudyService.getStudiesByUsedInMd('');
	}
	
	
	/** Test the function getStudiesByUsedInVal.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	18-Nov-2013
	 */
	static testMethod void getStudiesByUsedInValTest() {
		List<Study__c> results = StudyService.getStudiesByUsedInVal('');
	}
}