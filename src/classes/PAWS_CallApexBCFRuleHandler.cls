public with sharing class PAWS_CallApexBCFRuleHandler implements STSWR1.WorkflowCallApexRuleInterface 
{
	public Boolean run(sObject context, STSWR1__Flow_Instance_Cursor__c flowInstanceCursor, String parameters)
	{
		Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(parameters);
		return new PAWS_CallApexRuleWrapper().checkBackCrossFlowRule(context, flowInstanceCursor, paramsMap);
	}
}