/**
 *	This class looks for employees that do not have any PRA Roles Assigned
 *	It will determine if they are elligible for a PRA Role and assign them if possible
 *	Author: Andrew Allen
 *	Date: 4/5/2013
 */
public with sharing class LMS_MappingUtil {
	
	static String outSync = 'N';
    static String statusDraft = 'Draft';
    static String statusPendingAdd = 'Pending - Add';
    static String excludeEmployeeFilter = '';
    static String includeCompanyBUFilter = '';
	private static map<String, String> jobTitleMap{get;set;}
	private static map<String, set<String>> statusMap{get;set;}
	private static set<Employee_Status__c> statusTerminated{get;set;}


	static {
		jobTitleMap = initJobTitleMap();
		statusTerminated = initStatusSet();
		statusMap = initStatusMap();
		excludeEmployeeFilter = LMS_ToolsFilter.getBlackListEmployeeFilter();
		includeCompanyBUFilter = LMS_ToolsFilter.getWhitelistEmployeeFilter();
	}

	private static set<Employee_Status__c> initStatusSet() {
		set<Employee_Status__c> status = 
			new set<Employee_Status__c>([select Employee_Status__c from Employee_Status__c where Employee_Type__c = 'Terminated']);
		return status;
	}

	private static map<String, String> initJobTitleMap() {
		map<String, String> titleMap = new map<String, String>();
		for(Job_Title__c jt : [select Job_Title__c, Job_Code__c from Job_Title__c]) {
			titleMap.put(jt.Job_Code__c, jt.Job_Title__c);
		}
		return titleMap;
	}

	private static map<String, set<String>> initStatusMap() {
		set<String> employeeType = new set<String>();
		set<String> contractorType = new set<String>();
		set<String> terminatedType = new set<String>();
		map<String, set<String>> status = new map<String, set<String>>();
		for(Employee_Status__c s : [select Employee_Status__c, Employee_Type__c from Employee_Status__c order by Employee_Type__c]) {
			if(s.Employee_Type__c == 'Employees') employeeType.add(s.Employee_Status__c);
			if(s.Employee_Type__c == 'Contractor') contractorType.add(s.Employee_Status__c);
			if(s.Employee_Type__c == 'Terminated') terminatedType.add(s.Employee_Status__c);
		}
		status.put('Employees', employeeType);
		status.put('Contractor', contractorType);
		status.put('Terminated', terminatedType);
		return status;
	}

	public static void processRoles(list<LMS_Role__c> roles) {
		list<LMS_Role_Employee__c> newMappings = new list<LMS_Role_Employee__c>();
		for(LMS_Role__c r : roles) {
			Id roleId = r.Id;
			String jobClass = r.Job_Class_Desc__r.Name;
			String jobTitle = r.Job_Title__r.Job_Code__c;
			String businessUnit = r.Business_Unit__r.Name;
			String department = r.Department__r.Name;
			String region = r.Region__r.Region_Name__c;
			String country = r.Country__c;
			String empType = r.Employee_Type__r.Name;
			set<Id> employeeIds = getCurrentEmployeeIds(r.Id);
			list<Employee_Details__c> newEmployees = getNewEmployeeIds(jobClass, jobTitle, businessUnit, department, region, country, empType, employeeIds);

			for(Employee_Details__c emp : newEmployees) {
				if(emp.Date_Hired__c > Date.today()) {
					newMappings.add(new LMS_Role_Employee__c(Role_Id__c = r.Id, Employee_Id__c = emp.Id, Status__c = statusPendingAdd,
	                    	Created_On__c = System.today(), Updated_On__c = System.today(), Sync__c = outSync));
				} else {
					newMappings.add(new LMS_Role_Employee__c(Role_Id__c = r.Id, Employee_Id__c = emp.Id, Status__c = statusDraft,
	                    	Created_On__c = System.today(), Updated_On__c = System.today(), Sync__c = outSync));
				}
			}
		}

		if(newMappings != null && newMappings.size() > 0) {
			insert newMappings;
		}
	}

	private static set<Id> getCurrentEmployeeIds(Id roleId) {
		set<Id> employeeIds = new set<Id>();
		list<LMS_Role_Employee__c> roleEmps = [select Employee_Id__c from LMS_Role_Employee__c where Role_Id__c = :roleId];
		for(LMS_Role_Employee__c re : roleEmps) {
			employeeIds.add(re.Employee_Id__c);
		}
		if(employeeIds.size() == 0) employeeIds.clear();
		return employeeIds;
	}

	private static list<Employee_Details__c> getNewEmployeeIds(String jobClass, String jobTitle, String businessUnit, String department, 
		String region, String country, String empType, set<Id> excludeIds) {
		String queryParam = '';
		String query = 'select Id, Date_Hired__c from Employee_Details__c where Name != null ';
		if(excludeIds != null) {
			for(String s : excludeIds) {
				if(s != null) queryParam += '\'' + s + '\',';
			}
			if(queryParam.length() > 0) queryParam = queryParam.substring(0, queryParam.length() - 1);
			else queryParam = '\'\'';
			query += 'and Id not in (' + queryParam + ') ';
		}
		if(jobClass != null) query+= 'and Job_Class_Desc__c = \'' + jobClass + '\' ';
		if(jobTitle != null) query+= 'and Job_Code__c = \'' + jobTitle + '\' ';
		if(businessUnit != null) query+= 'and Business_Unit_Desc__c = \'' + businessUnit + '\' ';
		if(department != null) query+= 'and Department__c = \'' + department + '\' ';
		if(region != null) query+= 'and Region__c = \'' + region + '\' ';
		if(country != null) query+= 'and Country_Name__c = \'' + country + '\' ';
		if(empType != null) {
			queryParam = '';
			for(String s : statusMap.get(empType)) {
				if(s != null) queryParam += '\'' + s + '\',';
			}
			if(queryParam.length() > 0) queryParam = queryParam.substring(0, queryParam.length() - 1);
			else queryParam = '\'\'';
			query+= 'and Status__c in (' + queryParam + ') ';
		}
		queryParam = '';
		for(Employee_Status__c s : statusTerminated) {
			queryParam += '\'' + s.Employee_Status__c + '\',';
		}
		if(queryParam.length() > 0) queryParam = queryParam.substring(0, queryParam.length() - 1);
		else queryParam = '\'\'';
		query += 'and (Term_Date__c > TODAY or Term_Date__c = null) and Status__c not in (' + queryParam + ') ';
		query += excludeEmployeeFilter + ' ' + includeCompanyBUFilter;
		system.debug('-----------------------query-------------------------' + query);
		list<Employee_Details__c> employeeIds = Database.query(query);
		return employeeIds;
	}
}