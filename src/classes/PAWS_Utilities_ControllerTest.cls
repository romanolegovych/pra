@isTest
private class PAWS_Utilities_ControllerTest
{
	static testMethod void testAll()
	{
		PAWS_ApexTestsEnvironment.init();
		STSWR1__Gantt_Step_Property__c prop = PAWS_ApexTestsEnvironment.GanttStepProperty;
		
		PAWS_Utilities_Controller controller = new PAWS_Utilities_Controller();
		system.assert(controller.Bug_99873_Projects != null);
		
		PAWS_Utilities_Controller.updateGanttRecords(new List<Map<String, String>>{
			new Map<String, String>{'STSWR1__Revised_History__c' => 'test'}
		});
		
		PAWS_Utilities_Controller.updateGanttRecords(new List<Map<String, String>>{
			new Map<String, String>{'Id' => prop.Id},
			new Map<String, String>{'STSWR1__Revised_History__c' => 'test'}
		});
		
		PAWS_ApexTestsEnvironment.FlowSwimlane.Name = 'Lead Safety Manager';
		update PAWS_ApexTestsEnvironment.FlowSwimlane;
		
		PAWS_Utilities_Controller.updateSwimlaneLabels();
	}
}