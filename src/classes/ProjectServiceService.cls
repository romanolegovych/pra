public with sharing class ProjectServiceService {

	
	/** Get a map of projectservices for a set of services and create missing records
	 * @author	Maurice Kremer
	 * @version 09-Jan-2014
	 * @param	ServiceIdSet - Set of services
	 * @param	ProjectId - Project ID
	 * @param	CreateMissing - Boolean to indicate if missing should be created
	 * @return	Map of projectservices with service as key
	 */
	public static map<id,ProjectService__c> getProjectServiceForService (Set<Id> ServiceIdSet,Id ProjectId,Boolean CreateMissing) {
		
		// put services in a string list for SOQL
		String services = '';
		For (Id ServiceId:ServiceIdSet) {
			if (ServiceId != null) {
				services += ',\''+ServiceId+'\'';
			}
		}
		services = services.removeStart(',');
		
		// read current projectservices
		map<id,ProjectService__c> result = new map<id,ProjectService__c>();
		
		
		For (ProjectService__c ps:
			ProjectServiceDataAccessor.getProjectServiceList('BDTDeleted__c = false and Client_Project__c = \''+ProjectId+'\' and Service__c in ('+services+')')
		){
			result.put(ps.service__c,ps);
		}
		
		if (CreateMissing) {
			// find missing projectservice
			list<ProjectService__c> newRecords = new list<ProjectService__c>();
			For (Id ServiceId:ServiceIdSet) {
			
				if (result.containsKey(ServiceId)) {
					// this service has a projectservice associated, continue to check next serviceId
					continue;
				}
				
				// for this service a projectservice needs to be created
				newRecords.add(new ProjectService__c(Client_Project__c = ProjectId, Service__c = ServiceId));
			}
			
			insert newRecords;
			For (ProjectService__c ps:newRecords){
				result.put(ps.service__c,ps);
			}
		}
		
		return result;
		
	}


}