public with sharing class PBB_ServiceTask extends PBB_ServiceBase {
    
    @TestVisible
    private static final String TASK_WORD = 'Task';
        
    public List<Service_Task__c> serviceTasks {
        get{
            return (List<Service_Task__c>) serviceRecords;
        }
        set{
            serviceRecords = value;
        }
    }
    public Service_Task__c serviceTaskForUpdate {
        get{
            return (Service_Task__c)serviceForUpdate;
        }
        set{
            serviceForUpdate = value;
        }
    }
    public Service_Task__c serviceTask {
        get{
            return (Service_Task__c)serviceRecord;
        }
        set{
            serviceRecord = value;
        }
    }
    
    public List<String> selectedBusinessSegmentTypes {get;set;}
    /*public String serviceTaskId {
        get;
        set{
            serviceTaskId = value;
            currentTab = ServiceTaskTab.ST_TO_SI;
        }
    }*/
    public ServiceTaskTab currentTab{
        get{
            if (currentTab == null){
                currentTab = ServiceTaskTab.ST_TO_SI;
            }
            return currentTab;
        }
        set;
    }
    
    public PBB_ServiceTask(String parentServiceId) {
        super(parentServiceId);
    }
    
    @TestVisible
    protected override String getServiceWord(){
        return TASK_WORD;
    }
    
    public override void createService() {
        prepData();
        super.createService();
    }

    public override void refreshServices(){
        serviceTasks = parentServiceId != null ? PBB_ServiceModelServices.getserviceTasksByServiceFunctionId(parentServiceId) : new List<Service_Task__c>();
    }
    
    public override void getDetailService() {
        super.getDetailService();
        if(serviceTask != null && !String.isBlank(serviceTask.Business_Segment__c)) {
            selectedBusinessSegmentTypes = serviceTask.Business_Segment__c.split(';');
        }
    }
    
    public override void preparationCreateOrEditService() {
        if(String.isBlank(getEditServiceId())) {
            serviceTaskForUpdate = new Service_Task__c();
            serviceTaskForUpdate.Service_Function__c = parentServiceId;
            selectedBusinessSegmentTypes = new List<String>();
        } else {
            refreshServices();
            Map<Id, Service_Task__c> serviceTaskMap = new Map<Id, Service_Task__c>(serviceTasks);
            serviceTaskForUpdate = serviceTaskMap.get(getEditServiceId());
            selectedBusinessSegmentTypes = !String.isBlank(serviceTaskForUpdate.Business_Segment__c) ? serviceTaskForUpdate.Business_Segment__c.split(';') : new List<String>();
        }
        if(!String.isBlank(getCloneServiceId())) {
            serviceTaskForUpdate = serviceTask.clone(false, true);
            serviceTaskForUpdate.Service_Task_Unique_ID__c = null;
            selectedBusinessSegmentTypes = !String.isBlank(serviceTaskForUpdate.Business_Segment__c) ? serviceTaskForUpdate.Business_Segment__c.split(';') : new List<String>();
        }
    }
    
    private void prepData() {
        serviceTaskForUpdate.Business_Segment__c = String.join(selectedBusinessSegmentTypes, ';');
    }

    public List<selectOption> getBusinessSegmentTypes(){
        List<SelectOption> options = new List<SelectOption>(); 
        for(PRA_Business_Unit__c bs:PBB_DataAccessor.getPRABusinessUnit()){
            options.add(new SelectOption(bs.name,bs.Business_Unit_Code__c+'-'+bs.name));
        }  
        return options;
    }
    
    protected override Boolean isValidService() {
        isValid = false;
        Boolean isValidServiceTask = true;
        if(String.isBlank(serviceTaskForUpdate.Name)){
            isValidServiceTask = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Service Task Name: You must enter a value'));
        }

        if(String.isBlank(serviceTaskForUpdate.Business_Segment__c)) {
            isValidServiceTask = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Business Segment: You must select a value'));
        }
        return isValidServiceTask;
    }
    
    public enum ServiceTaskTab{
        ST_TO_SI, ST_TO_D
    }
}