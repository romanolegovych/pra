public class ChartController {

    public Date mydate {get;set;}
    public Date mydate2 {get;set;}
    list<date>           enddate = new list<date>();
    list<date>           srtdate = new list<date>();
    map<date,date>       sted = new map<date,date>();
    list<date>           enddate1 = new list<date>();    
    static list<integer>        newticket = new list<integer>();
    static list<integer>        closedticket = new list<integer>();    
    static list<integer>        averageopen = new list<integer>();
    static list<integer>        closedage = new list<integer>();
    static list<integer>        closedmedian = new list<integer>();
    date startDate;
    date startDate1;
    date myDate1;
    datetime dt;
    datetime dt1;
    datetime dt2;
    Integer sizeOfList;
    Integer index;
    Integer median ;   
    
    Integer noOfDays=0 ;
    integer noOfDays1 =0;
    integer count=0;

    
    public void startdate(date date1,date date2){
        if(date1 != null && date2 != null){ 
        date Datas = date1.toStartofWeek()+1;
        date dataE=Datas +6;
        sted.put(Datas,dataE);
        srtdate.add(datas);
        enddate.add(dataE);
         do{ 
             datas=datae+1;
             dataE=Datas+6; 
             sted.put(Datas,dataE);
             srtdate.add(datas);
             enddate.add(dataE);
         }while(dataE <= date2);
    }
   }
 

    public void getreporting (){
        
        startdate(mydate,mydate2);
        if(sted.size() != 0){      
                BMCServiceDesk__Status__c BMCStatus=[select Id from BMCServiceDesk__Status__c where Name='Open' limit 1];
           for(integer i=0;i<sted.size();i++){
               list<BMCServiceDesk__Incident__c>  studentdetail=[ select  
                                                                            BMCServiceDesk__openDateTime__c,
                                                                            BMCServiceDesk__FKStatus__c,
                                                                            createddate,
                                                                            Lastmodifieddate ,
                                                                            BMCServiceDesk__closeDateTime__c
                                                                    from 
                                                                            BMCServiceDesk__Incident__c
                                                                    where 
                                                                        (BMCServiceDesk__FKStatus__c =:BMCStatus.Id  or 
                                                                        Lastmodifieddate >: enddate[i] ) and 
                                                                        CreatedDate <: enddate[i]
                                                                ]; 
                 newticket.add([select 
                                    count() 
                                from  
                                    BMCServiceDesk__Incident__c 
                                where 
                                    CreatedDate>:srtdate[i] 
                               ]);
               
                 for(BMCServiceDesk__Incident__c st : studentdetail){
                     dt1=st.BMCServiceDesk__openDateTime__c;
                    startDate = date.newinstance(dT1.year(), dT1.month(), dT1.day());    
                       
                      noOfDays = startDate.daysBetween(enddate[i]);                      
                      noOfDays1=noOfDays1+noOfDays;                      
                      count=count+1;
                 }
                 if(count == 0){
                     averageopen.add(0);
                 }
                 else{
                     averageopen.add(noOfDays1/count);  
                 }  
               
                list<BMCServiceDesk__Incident__c> studentdetail1=[ select     
                                                                        BMCServiceDesk__openDateTime__c,
                                                                        BMCServiceDesk__FKStatus__c,
                                                                        createddate,
                                                                        Lastmodifieddate ,
                                                                        BMCServiceDesk__closeDateTime__c
                                                                   from 
                                                                        BMCServiceDesk__Incident__c
                                                                   where  
                                                                        Lastmodifieddate >: srtdate[i]  
                                                                        and  Lastmodifieddate <: enddate[i]
                                                                 ];
                                                     
                closedticket.add([select 
                                        count() 
                                  from
                                        BMCServiceDesk__Incident__c     
                                  where 
                                         BMCServiceDesk__FKStatus__c  =:[select Id from BMCServiceDesk__Status__c where Name='Closed' limit 1] 
                                         and Lastmodifieddate>: srtdate[i] 
                                         and Lastmodifieddate <: enddate[i]
                                 ]);                
                 sizeOfList =0;
                 index =0;
             
                 for(BMCServiceDesk__Incident__c st1 : studentdetail1){   
                     dt2=st1.BMCServiceDesk__openDateTime__c;
                     startDate1 = date.newinstance(dT2.year(), dT2.month(), dT2.day());                        
                     dt=st1.Lastmodifieddate;
                     myDate1 = date.newinstance(dT.year(), dT.month(), dT.day());                
                     noOfDays = startDate1.daysBetween(mydate1);
                     closedage.add(noofdays);
                 }        
               
                  sizeOfList = closedage.size();
                  index = sizeOfList - 1;
                  if(sizeOfList ==0){
                     closedmedian.add(0);
                  }
                  else{
                      if (Math.mod(sizeOfList, 2) == 0) {
                           median = (closedage[(index-1)/2] + closedage[(index/2)+1])/2;
                      }
                      else{
                           median = closedage[(index+1)/2];
                      }           
                       closedmedian.add(median);
                  }
                closedage.clear(); 
            } 
        } 
        
    }
  
    // Return a list of data points for a chart
    public List<Data> getData() {
        return ChartController.getChartData();
        
    }
    
    public static List<Data> getChartData() {
        List<Data> data = new List<Data>();
        for(Integer i=0;i<newticket.size();i++){
            data.add(new Data(newticket.get(i),closedticket.get(i),averageopen.get(i),closedmedian.get(i)));}
        return data;
    }
    
    // Wrapper class
    public class Data {
        public String name { get; set; }
        public Integer data1 { get; set; }
        public Integer data2 { get; set; }
        public Integer data3 { get; set; }
        public Integer data4 { get; set; }
        public Data(String name, Integer data1,Integer data2,Integer data3,Integer data4) {
            this.name = name;
            this.data1 = data1;
            this.data2 = data2;
            this.data3 = data3;
            this.data4 = data4;
         }
         
      public Data(Integer data1,Integer data2,Integer data3,Integer data4) {
            this.name = '15/3/15';
            this.data1 = data1;
            this.data2 = data2;
            this.data3 = data3;
            this.data4 = data4;
         }
    }
}