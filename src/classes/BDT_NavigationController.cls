public with sharing class BDT_NavigationController {
	public boolean ShowNavigation {get;set;}
	public string NavigationFunction {get;set;}
	public List<SelectOption> NavigationMainCategories {get;set;}
    public List<BDT_NavigationConfiguration.navigationMainCategoryWrapper> navigationData;
    public list<BDT_NavigationConfiguration.navigationCategoryWrapper> navigationCategories {get;set;}
	public list<BDT_NavigationConfiguration.navigationItemWrapper> navigationItems 			{get;set;}
	public string selectedCategory													 		{get;set;}
	public string selectedItem																{get;set;}
	public string selectedMainCategory														{get;set;}
	public string targetPage																{get;set;}
	public string currentPageName;
	
	public BDT_NavigationConfiguration.navigationMainCategoryWrapper activeFunction;
	public BDT_NavigationConfiguration.navigationCategoryWrapper activeCategory;
	
	public BDT_NavigationController(){
		BDT_NavigationConfiguration navData = new BDT_NavigationConfiguration();
		
		navigationData = new List<BDT_NavigationConfiguration.navigationMainCategoryWrapper>();
		navigationData = navData.navigation;
		
		/*retrieve current page from the system */
		currentPageName = getCurrentPageName();

		/* search the active page in the wrappers and set the items accordingly */
		selectedItem = navData.retrieveItemName( currentPageName );
		selectedCategory = navData.retrieveCategoryName( currentPageName );
		selectedMainCategory = navData.retrieveMainCategoryName( currentPageName );
		NavigationFunction = selectedMainCategory;
		
		if(selectedItem!=null&&NavigationFunction!=''){
			retrieveNavigationCategories();
		}
		
		if(selectedItem!=null&&selectedCategory!=''){
			displayNavigationItems();
		}
		if(selectedItem!=null) {
			ShowNavigation = true;
			buildMainCategories();
		} else {ShowNavigation=false;}
	}
	
	public boolean getUserIsAdministrator() {
		return BDT_Utils.getUserIsAdministrator();
	}
	
	public string getCurrentPageName(){
	
  		String pageName = ApexPages.CurrentPage().getUrl();     
  		pageName = pageName.replaceFirst('/apex/','');
  		pageName = EncodingUtil.urlEncode(pageName, 'UTF-8');
         
  		string[] pageNameExtra = pageName.split('%3F',0);
                                                         
  		pageName = pageNameExtra[0];                     
        
  		return pageName.toLowerCase(pageName);
	}
	
	public void buildMainCategories(){
		NavigationMainCategories = new List<SelectOption>();
		for(BDT_NavigationConfiguration.navigationMainCategoryWrapper fnc: navigationData){
			NavigationMainCategories.add(new selectOption(fnc.Name, fnc.Name));
		}
	}
	public PageReference buildNavigationCategories(){
		//this method is invoked when the main navigation selection changes (e.g. from administration to single project)
		selectedCategory='';
		retrieveNavigationCategories();
		//rebuild navigation items, since the category has changed.
		selectedCategory=navigationCategories[0].Name;
		//displayNavigationItems();
		buildNavigationItems();  
		
		return gotoTargetPage();
	}
	
	public void retrieveNavigationCategories(){
		navigationCategories = new list<BDT_NavigationConfiguration.navigationCategoryWrapper>();
		
		for(BDT_NavigationConfiguration.navigationMainCategoryWrapper fnc: navigationData){
			if(fnc.Name == NavigationFunction ){
				activeFunction = fnc;
				navigationCategories = activeFunction.NavigationCategoryList;
				break;
			}
		}
	}
	
	public void buildNavigationItems(){
		selectedItem='';
		displayNavigationItems();
		targetPage = navigationItems[0].MainPage; 
	}
	
	public PageReference displayNavigationItems(){
		navigationItems = new List<BDT_NavigationConfiguration.navigationItemWrapper>();
		for(BDT_NavigationConfiguration.navigationCategoryWrapper cat: activeFunction.NavigationCategoryList){
			if(cat.Name == selectedCategory){
				activeCategory = cat;
				navigationItems = cat.NavigationItemList;
			    targetPage = navigationItems[0].MainPage;
				return gototargetPage();
			}	
		}
		targetPage = currentPageName;
		return gototargetPage();
	}
	
	public pageReference gotoTargetPage(){
		return new PageReference( '/apex/'+ targetPage );
				
	}
	
}