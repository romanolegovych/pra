@isTest(SeeAllData=false)
private class PRA_ForecastUpdateTest {

	static final String testValue = 'Remove after Migration';

    static testMethod void shouldUpdateOfficialForecastUnits() {
        // given
        initTestData();
        
        Date firstDayOfCurrentMonth = Date.today().toStartOfMonth();
        
        List <Unit_Effort__c> oldForecast = [select id,Month_Applies_To__c,Is_Historical__c                                       
                                    from Unit_Effort__c where  
                                                  Month_Applies_To__c < :firstDayOfCurrentMonth.addMonths(0) order by Month_Applies_To__c];
        
        for (Unit_Effort__c oldOfficial :  oldForecast){
           
           system.assertEquals(oldOfficial.Is_Historical__c,false);
        }
        // when
        Test.startTest();
        PRA_ForecastUpdateScheduler scheduler = new  PRA_ForecastUpdateScheduler();
        Datetime dt = System.now().addSeconds(2);
        // String cromeStr = prepareCromeExpresion(dt);        
        scheduler.execute(null);
        Test.stopTest();
        
        // then        
        // check that there are no Units from previous months of Official Forecast
        List <Unit_Effort__c> ForecastOldUnits = [select id,Month_Applies_To__c,Is_Historical__c                                       
                                                    from Unit_Effort__c where 
                                                     Month_Applies_To__c < :firstDayOfCurrentMonth order by Month_Applies_To__c];
        
        for (Unit_Effort__c oldOfficial : ForecastOldUnits){
           
           system.assertEquals(oldOfficial.Is_Historical__c,true);
        }
    }
    
    static testMethod void shouldTestOldClasses() {
    	PRA_ForecastUpdateSchedulerEffort effortCon = new PRA_ForecastUpdateSchedulerEffort();
    	system.assert(effortCon.testValue == testValue);
    	
    	PRA_ForecastUpdateSchedulerHours hourCon = new PRA_ForecastUpdateSchedulerHours();
    	system.assert(hourCon.testValue == testValue);
    	
    	PRA_ForecastUpdateSchedulerUnits unitCon = new PRA_ForecastUpdateSchedulerUnits();
    	system.assert(unitCon.testValue == testValue);
    }
    
    
    static void initTestData (){
        PRA_TestUtils testUtil = new PRA_TestUtils();
        testUtil.initAll();
        
    }   
  
}