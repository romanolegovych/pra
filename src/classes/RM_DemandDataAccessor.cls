public with sharing class RM_DemandDataAccessor {
 /**
 	DataAccessor for Demand Objects
 */
  Public RM_DemandDataAccessor(){
  	
  }
   public static list<AggregateResult> getAssignedlist( String projName, List<String> graphMonthList, String selectedRole ){  	
   	
   	List<AggregateResult> aggreResult = [Select allocation_key__r.project_function__c, Year_Month__c, sum(hours__c) hours, sum(fte__c) FT
                                            From WFM_Employee_Assignment__c 
                                            where allocation_key__r.project_id__r.name = :projName 
                                            and Year_Month__c in :graphMonthList
                                            and allocation_key__r.project_function__c = :selectedRole
                                            group by allocation_key__r.project_function__c, Year_Month__c ];
   	
   
   return aggreResult;
   }
   public static List<AggregateResult> getAssignedByMonthList(String projName, List<string> yrMthList){
   	List<AggregateResult> assignedList = new List<AggregateResult>([Select allocation_key__r.project_function__c role, Year_Month__c, 
                                                                        sum(hours__c) hours, sum(fte__c) fte 
                                                                        From WFM_Employee_Assignment__c 
                                                                        where allocation_key__r.project_id__r.name = :projName 
                                                                        and Year_Month__c in :yrMthList
                                                                        group by allocation_key__r.project_function__c, Year_Month__c]);
   	return assignedList;
   	
   }
   public static list<AggregateResult> getPlannedList(String projName, List<String> graphMonthList, String selectedRole){
   	
   	List<AggregateResult> arList = new List<AggregateResult>([Select Project_Role__r.Job_Class_Desc__r.Name roleName, Year_Month__c, 
                                                                    Sum(planned_hours__c) plannedHours, Sum(planned_fte__c) plannedFte, sum(Demand_Hours__c) forecastHours, sum(Demand_fte__c) forecastFte
                                                                From WFM_Project_Demand__c
                                                                Where Project_Role__r.Project_Id__c =:projName
                                                                and Project_Role__r.Project_Role__c = :selectedRole
                                                                and Year_Month__c in :graphMonthList
                                                                Group by Project_Role__r.Job_Class_Desc__r.Name,Year_Month__c]);
   return arList;	
   }
   
   public static List<WFM_Project_Role__c> getProjectRolesValues (List<String> monthList , String projId ,String previewRole, String previewCntry, String previewLocation){
   	List<WFM_Project_Role__c> prjectRoles = new List<WFM_Project_Role__c>([Select Id, Project_Role_Id__c, WFM_Project__c, Business_Unit__c,
                                                                                    Job_CLass_Desc__c, Country__c, Project_Id__c, Project_BU_Location__c,
                                                                                    Project_Role__c, Project_Country__c,
                                                                                    (Select Country_Hour__c, Year_Month__c, Demand_Hours__c, 
                                                                                        Demand_fte__c,Project_Demand_Id__c, Project_Role__c, 
                                                                                        Planned_Hours__c, 
                                                                                        Planned_Fte__c 
                                                                                    From Project_Demands__r
                                                                                    Where Year_Month__c = : monthList)
                                                                                From WFM_Project_Role__c
                                                                                Where WFM_Project__c = : projId
                                                                                and Project_Role__c = : previewRole
                                                                                and Project_Country__c = : previewCntry
                                                                                and Project_BU_Location__c =: previewLocation]);
	return prjectRoles;                                                                          
   }
   
   public static List<AggregateResult> getPlannedValuesbyYrMthList(String projName,List<String> yrMthList){
   	List<AggregateResult> arList = new List<AggregateResult>([Select Project_Role__r.Job_Class_Desc__r.Name roleName, Year_Month__c, 
                                                                    Sum(planned_hours__c) plannedHours, Sum(planned_fte__c) plannedFte, sum(Demand_Hours__c) forecastHours, sum(Demand_fte__c) forecastFte
                                                                From WFM_Project_Demand__c
                                                                Where Project_Role__r.Project_Id__c =:projName
                                                                and Year_Month__c in :yrMthList
                                                                Group by Project_Role__r.Job_Class_Desc__r.Name,Year_Month__c]);
   	
   	return arList;
   }
   public static List<AggregateResult> getAssignedvaluesBySelectedRole (string ProjName,String SelectedRole, List<String> yrMthList ){
   	
   	List<AggregateResult> childAssignedList = new List<AggregateResult>([Select allocation_key__r.project_function__c role, 
                                                                            allocation_key__r.Project_Country__c country,
                                                                            allocation_key__r.Project_Buf_Code__c location,
                                                                            Year_Month__c, 
                                                                            sum(hours__c) hours, sum(fte__c) fte 
                                                                            From WFM_Employee_Assignment__c 
                                                                            where allocation_key__r.project_id__r.name = :projName
                                                                            and allocation_key__r.project_function__c = :selectedRole
                                                                            and Year_Month__c in :yrMthList
                                                                            group by allocation_key__r.project_function__c,
                                                                                    allocation_key__r.Project_Country__c,
                                                                                    allocation_key__r.Project_Buf_Code__c,
                                                                                    Year_Month__c]);
	return   childAssignedList;                                                                                  
   }
   
   public static List<AggregateResult> getPlannedValuesBySelectedRole(string ProjName,String SelectedRole, List<String> yrMthList){
   List<AggregateResult> childPlannedList = new List<AggregateResult>([Select Project_Role__r.Job_Class_Desc__r.Name roleName,
                                                                        Project_Role__r.Country__r.Name country,
                                                                        Project_Role__r.Business_Unit__r.Name location,
                                                                        Year_Month__c, 
                                                                        Sum(planned_hours__c) plannedHours, Sum(planned_fte__c) plannedFte, sum(Demand_Hours__c) forecastHours, sum(Demand_fte__c) forecastFte
                                                                    From WFM_Project_Demand__c
                                                                    Where Project_Role__r.Project_Id__c =:projName
                                                                    and Project_Role__r.Project_Role__c = :selectedRole
                                                                    and Year_Month__c in :yrMthList
                                                                    Group by Project_Role__r.Job_Class_Desc__r.Name,
                                                                            Project_Role__r.Country__r.Name,
                                                                            Project_Role__r.Business_Unit__r.Name,
                                                                            Year_Month__c]);
	return   childPlannedList;                                                                          
   }                                                                            
}