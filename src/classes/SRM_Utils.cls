/**
@author 
@date 2014
@description this class to have all methods that are generic
**/
public with sharing class SRM_Utils{
    
    //Method to show error Message
    public static void showErrorMessage(String errorMessage){
        if(ApexPages.CurrentPage() != null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
        }
        else{
            System.debug('>>> '+errorMessage+' >>>');
        }
    }
    
     /**
@author 
@date 2014
@description this Method to show message
**/
    //Method to show message
    public static void showMessage(String message){
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, message));
    }
    
         /**
@author 
@date 2014
@description this Method to show message
**/
    //Method to get week start date
    public static Date getWeekStartDate(Date dateToFind){
        Date weekStartDate = null;
        if(dateToFind == null){
            return null;
        }
        else{
            Date weekStart = dateToFind.toStartofWeek();
            if(weekStart.daysBetween(dateToFind) >= 4){
                weekStartDate = dateToFind.addDays(7).toStartOfWeek();
            }
            else{
                weekStartDate = weekStart;
            }
        }
        //add 2 days because week start in US is on Sunday as per salesforce document and one day for
        //getting correct date selected by user.
        return weekStartDate.addDays(2);
    }
    
 
      /**
@author 
@date 2014
@description this Method to get week start date
**/   
    //Method to get week start date
    public static Date getWeekStartDateByUser(Date dateToFind){
        Date weekStartDate = null;
        if(dateToFind == null){
            return null;
        }
        else{
            Date weekStart = dateToFind.toStartofWeek();
            if(weekStart.daysBetween(dateToFind) >= 4){
                weekStartDate = dateToFind.addDays(7).toStartOfWeek();
            }
            else{
                weekStartDate = weekStart;
            }
        }
        //add 2 days because week start in US is on Sunday as per salesforce document and one day for
        //getting correct date selected by user.
        return weekStartDate.addDays(1);
    }
    
  
       /**
@author 
@date 2014
@description this Method to get number of weeks
**/  
    //Method to get number of weeks
    public static Integer getNumberOfWeeks(Date startDate, Date endDate){
        Integer numberDaysDue = (getWeekStartDate(startDate)).daysBetween(getWeekStartDate(endDate));
        System.debug('>>> numberDaysDue >>> '+numberDaysDue);
        Integer numberOfWeeks = Integer.valueOf(numberDaysDue/7) + 1;
        return numberOfWeeks;
    }
    

     /**
@author 
@date 2014
@description this Method to return all the fields from obejct in comma separated string
**/    
    //Method to return all the fields from obejct in comma separated string
    public static String getAllFieldsFromObject(String ObjectType){
        String fieldsString = '';
        Map<String, Schema.SObjectField> objFields = Schema.getGlobalDescribe().get(objectType).getDescribe().fields.getMap();
        Set<String> fieldSet = objFields.keySet();
        for(String s: fieldSet){
            fieldsString += ' '+s+',';
        }
        if(fieldsString.endsWith(',')){
            fieldsString = fieldsString.removeEnd(',');
        }
        System.debug('>>> '+objectType +' fields >>> '+fieldsString);
        return fieldsString;
    }
    
       /**
@author Niharika Reddy
@date 2015
@description thisMethod to generate date from String date in the format of dd-Mmm-yyyy
**/  
    
    public static Date getDate(String dateInStringFormat){
        Date newDate = null;
        Integer monthInteger;
        if(!String.isEmpty(dateInStringFormat)){
            
            System.debug('>>> dateInStringFormat>>> '+dateInStringFormat);
            
            Integer monthInd = dateInStringFormat.indexOf('-');
            System.debug('monthInd : '+monthInd );
            
            Integer len = dateInStringFormat.length();
            System.debug('len : '+len );
            
            Integer day = Integer.valueOf(dateInStringFormat.subString(0, monthInd));
            System.debug('day : '+ day );
            
            String month = String.valueOf(dateInStringFormat.subString(monthInd+1, dateInStringFormat.lastIndexOf('-')));
            System.debug('month : '+ month );
            
            if(month.toLowerCase() == 'jan' ){
                month='Jan';
                monthInteger = 1;
            }else if(month.toLowerCase() == 'feb' ){
                month='Feb';
                monthInteger = 2;
            }else if(month.toLowerCase() == 'mar' ){
                month='Mar';
                monthInteger = 3;
            }else if(month.toLowerCase() == 'apr' ){
                month='Apr';
                monthInteger = 4;
            }else if(month.toLowerCase() == 'may' ){
                month='May';
                monthInteger = 5;
            }else if(month.toLowerCase() == 'jun' ){
                month='Jun';
                monthInteger = 6;
            }else if(month.toLowerCase() == 'jul' ){
                month='Jul';
                monthInteger = 7;
            }else if(month.toLowerCase() == 'aug' ){
                month='Aug';
                monthInteger = 8;
            }else if(month.toLowerCase() == 'sep' ){
                month='Sep';
                monthInteger = 9;
            }else if(month.toLowerCase() == 'oct' ){
                month='Oct';
                monthInteger = 10;
            }else if(month.toLowerCase() == 'nov' ){
                month='Nov';
                monthInteger = 11;
            }else if(month.toLowerCase() == 'dec' ){
                month='Dec';
                monthInteger = 12;
            }
            
            Integer year = Integer.valueOf(dateInStringFormat.subString(dateInStringFormat.lastIndexOf('-')+1, len));
            System.debug('year : '+year );
            
            newDate = Date.newInstance(year,monthInteger,day);
        }
        return newDate;
    }
    
         /**
@author Niharika Reddy
@date 2015
@description this Method to generate date from String date in the format of dd-Mmm-yyyy
**/
    
    public static String getDateInStringFormat(Date dateToConvert){
        String convertedDate = '';
        String datelabel = '';
        if(dateToConvert != null){
            Integer year = dateToConvert.year();
            Integer month = dateToConvert.month();
            Integer day = dateToConvert.day();
            
            datelabel = DateTime.newInstance(year, month, day).format('dd-MMM-yyyy');
        }
        return datelabel;
    }
    
  /**
@author Niharika Reddy
@date 2015
@description this Method to generate date from String date in the format of dd-Mmm-yyyy
**/
    
    public static String getDateInStringFormatcoded(Date dateToConvert){
        String convertedDate = '';
        String datelabel = '';
        if(dateToConvert != null){
            Integer year = dateToConvert.year();
            Integer month = dateToConvert.month();
            Integer day = dateToConvert.day();
            
            datelabel = DateTime.newInstance(year, month, day).format('dd-MMM-20yy');
        }
        return datelabel;
    } 
    
    public Static Date getStartOfWeek(Date weekDay){
        Datetime dt = DateTime.newInstance(weekDay.toStartOfWeek(), Time.newInstance(0, 0, 0, 0));
        System.debug('******TODATE*******'+dt);
        String day = dt.format('EEEE');
        System.debug('******* DAY NAME ********'+day);
        weekDay = weekDay.toStartOfWeek();
        If(day == 'Sunday'){
            weekDay = weekDay.addDays(1);
            System.debug('*****After adding one day*******'+weekDay);
        }
        return weekDay;
    }
    
}