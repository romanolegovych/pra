public class LMS_ToolsModifyAssignments {
	
	private static String statusDraft{get;set;}
	private static String statusCommitted{get;set;}
	private static String statusDraftDelete{get;set;}
	private static String statusPendingAdd{get;set;}
	private static String statusPendingDelete{get;set;}
	private static String statusTransitAdd{get;set;}
	private static String statusTransitDelete{get;set;}
	private static String statusRemoved{get;set;}
	private static String inSync{get;set;}
	private static String outSync{get;set;}
	private static String actionAdd{get;set;}
	private static String actionRemove{get;set;}
	private static Date currentDate{get;set;}
	private static List<String> sfdcErrors{get;set;}
	private static List<String> errorList{get;set;}
	private static Map<String, List<String>> errorMap{get;set;}
	
	/**
	 * Initialize instance variables
	 */
	static {
		Map<String, LMSConstantSettings__c> constants = LMSConstantSettings__c.getAll();
		inSync = constants.get('inSync').Value__c;
		outSync = constants.get('outSync').Value__c;
		actionAdd = constants.get('addMappings').Value__c;
		actionRemove = constants.get('removeMappings').Value__c;
		statusDraft = constants.get('statusDraft').Value__c;
		statusCommitted = constants.get('statusCommitted').Value__c;
		statusDraftDelete = constants.get('statusDraftDelete').Value__c;
		statusPendingAdd = constants.get('statusPendingAdd').Value__c;
		statusPendingDelete = constants.get('statusPendingDelete').Value__c;
		statusTransitAdd = constants.get('statusTransitAdd').Value__c;
		statusTransitDelete = constants.get('statusTransitDelete').Value__c;
		statusRemoved = constants.get('statusRemoved').Value__c;
		currentDate = Date.today();
	}
	
	public static Map<String, List<String>> getErrorMap() {
		return errorMap;
	}
	
	/**
	 * Add course mappings to a given role
	 */
	public static Map<String, List<String>> addCourses(List<LMS_CourseList> courseList, List<LMS_CourseAssignments> assignments, 
		Map<String, LMS_Role_Course__c> mappings, String roleId) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
        List<LMS_Role_Course__c> assignList = new List<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> deleteList = new List<LMS_Role_Course__c>();
        Map<String, String> assignMap = new Map<String,String>();
        Map<String, String> statusMap = new Map<String,String>();
        
        for(LMS_CourseAssignments assign : assignments) {
        	assignMap.put(assign.courseId, assign.assignmentId);
        	statusMap.put(assign.courseId, assign.assignmentStatus);
        }
        
        for(LMS_CourseList cList : courseList) {
        	if(!assignMap.containsKey(cList.courseId) && cList.selected == true) {
        		assignList.add(new LMS_Role_Course__c(Course_Id__c = cList.courseId, Role_Id__c = roleId, Assigned_By__c = UserInfo.getName() , 
        			Status__c = statusDraft, Sync_Status__c = outSync));
        	}
        	if(assignMap.containsKey(cList.courseId) && statusMap.get(cList.courseId) == statusDraft && cList.selected == false) {
        		deleteList.add(mappings.get(assignMap.get(cList.courseId)));
        	}
        }

		try {
        	insert assignList;
        	delete deleteList;
		} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
		}
        
        return errorMap;
	}
	
	/**
	 * Revert assignment statuses to previous state
	 */
	public static Map<String, List<String>> cancelDraftCourses(String roleId) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
        List<LMS_Role_Course__c> rc = [SELECT Status__c,Previous_Status__c,Commit_Date__c FROM LMS_Role_Course__c WHERE Role_Id__c = :roleId];
        List<LMS_Role_Course__c> toDelete = new List<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> toUpdate = new List<LMS_Role_Course__c>();
        
        if(rc.size() > 0) { 
	        for(LMS_Role_Course__c assignment : rc) {
	            if(assignment.Status__c == statusDraft) {
	                toDelete.add(assignment);  
	            } else if(assignment.Status__c == statusDraftDelete) {
	                assignment.Status__c = statusCommitted;
	                toUpdate.add(assignment);
	            } else if(assignment.Status__c == statusPendingAdd) {
	            	assignment.Status__c = assignment.Previous_Status__c;
	            	assignment.Commit_Date__c = null;
	            	toUpdate.add(assignment);
	            } else if(assignment.Status__c == statusPendingDelete) {
	            	assignment.Status__c = assignment.Previous_Status__c;
	            	assignment.Commit_Date__c = null;
	            	toUpdate.add(assignment);
	            }
	        }
        }
		
		try {
	        delete toDelete;
	        upsert toUpdate;
		} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
		}
		
        return errorMap;
	}
	
	/**
	 * Removes draft statuses, or toggles between committed and draft delete
	 */
	public static Map<String, List<String>> removeCourses(List<LMS_CourseAssignments> assignments, Map<String, LMS_Role_Course__c> mappings) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
        List<LMS_Role_Course__c> toDelete = new List<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> toUpdate = new List<LMS_Role_Course__c>();

		for(LMS_CourseAssignments a : assignments) {
			if(a.selected) {
				LMS_Role_Course__c rc = mappings.get(a.assignmentId);
		        if(rc.Status__c == statusDraft) {
		        	toDelete.add(rc);
				} else if(rc.Status__c == statusCommitted) {
					rc.Previous_Status__c = rc.Status__c;
		            rc.Status__c = statusDraftDelete;
		            toUpdate.add(rc);
		        } else if(rc.Status__c == statusDraftDelete) {
		            rc.Status__c = statusCommitted;
		            toUpdate.add(rc);
		        } else if(rc.Status__c == statusPendingAdd || rc.Status__c == statusPendingDelete) {
		        	rc.Status__c = rc.Previous_Status__c;
		        	rc.Commit_Date__c = null;
		        	toUpdate.add(rc);
		        }
			}
		}
		
		try {
    		delete toDelete;
        	upsert toUpdate;
		} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
		}
		
        return errorMap;
	}
	
	/**
	 * Set Commit date for current assignments
	 */
	public static Map<String, List<String>> setCommitDateCourses(List<LMS_CourseAssignments> assignments, Map<String, LMS_Role_Course__c> mappings, Date commitDate) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
        List<LMS_Role_Course__c> toPend = new List<LMS_Role_Course__c>();
        
        if(null != assignments) {
	        for(LMS_CourseAssignments a : assignments) {
	        	if(a.selected && null != commitDate && a.assignmentStatus != statusCommitted && 
	        		a.assignmentStatus != statusTransitAdd && a.assignmentStatus != statusTransitDelete) {
		        	LMS_Role_Course__c rc = mappings.get(a.assignmentId);
		        	if(rc.Status__c != statusPendingAdd && rc.Status__c != statusPendingDelete) {
		        		rc.Previous_Status__c = rc.Status__c;
		        	}
		        	System.debug('------------------------rc------------------------'+rc);
		        	if(a.assignmentStatus == statusDraftDelete) {
		        		rc.Commit_Date__c = commitDate;
						rc.Status__c = statusPendingDelete;
						toPend.add(rc);
		        	} else if(a.assignmentStatus == statusPendingAdd || a.assignmentStatus == statusPendingDelete) {
		        		rc.Commit_Date__c = commitDate;
		        		toPend.add(rc);
		        	} else {
						rc.Commit_Date__c = commitDate;
						rc.Status__c = statusPendingAdd;
						toPend.add(rc);
		        	}
	        	}
		    }
        }
        
    	try {
    		update toPend;
    	} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
    	}
        
        return errorMap;
	}
	
	/**
	 * Commit current assignents
	 */
	public static Map<String, List<String>> commitCourses(List<LMS_CourseAssignments> assignments, Map<String, LMS_Role_Course__c> mappings) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
        List<LMS_Role_Course__c> toDelete = new List<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> toUpdate = new List<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> toPend = new List<LMS_Role_Course__c>();
		List<LMS_Role_Course__c> updateRoleCourse = new List<LMS_Role_Course__c>();
		List<LMS_Role_Course__c> deleteRoleCourse = new List<LMS_Role_Course__c>();
        Map<String, LMS_Role_Course__c> objectsAdd = new Map<String, LMS_Role_Course__c>();
        Map<String, List<LMS_Role_Course__c>> sendAddMap = new Map<String, List<LMS_Role_Course__c>>();
        
        if(null != assignments) {
	        for(LMS_CourseAssignments a : assignments) {
	        	if(a.selected) {
		        	LMS_Role_Course__c rc = mappings.get(a.assignmentId);
		        	System.debug('------------------------rc------------------------'+rc);
		            if(rc.Status__c == statusDraft && 
		              (rc.Course_Id__r.Available_From__c <= Date.today() || rc.Course_Id__r.Available_From__c == null)){
		                objectsAdd.put(rc.Course_Id__r.SABA_ID_PK__c, rc);
		                toUpdate.add(rc);
		            } else if(rc.Status__c == statusDraft && rc.Course_Id__r.Available_From__c > Date.today()){ 
		            	rc.Previous_Status__c = statusDraft;
		            	rc.Status__c = statusPendingAdd;
		            	toPend.add(rc);
		            } else if(rc.Status__c == statusDraftDelete) {
		                toDelete.add(rc);
		            }
	        	}
		    }
	        if(null != toUpdate && toUpdate.size() > 0) {
	        	sendAddMap.put(actionAdd, toUpdate);
	        	updateRoleCourse = LMS_ToolsService.executeCoursesToRoleWS(sendAddMap, objectsAdd);
	        }
	        if(null != toDelete && toDelete.size() > 0) {
				deleteRoleCourse = LMS_ToolsService.courseRoleBatchDelete(toDelete);	        
	        }
        }
        
        if(LMS_ToolsService.getErrorMap() != null)
        errorMap = LMS_ToolsService.getErrorMap();
        
        if(null != updateRoleCourse && updateRoleCourse.size() > 0) {
			try {
        		upsert updateRoleCourse;
			} catch(Exception e) {
				System.debug(e.getMessage());
				sfdcErrors.add(e.getMessage());
				errorMap.put('SFDCEX', sfdcErrors);
			}
        }
        if(null != toPend && toPend.size() > 0) {
        	try {
        		update toPend;
        	} catch(Exception e) {
				System.debug(e.getMessage());
				sfdcErrors.add(e.getMessage());
				errorMap.put('SFDCEX', sfdcErrors);
        	}
        }
        if(null != deleteRoleCourse && deleteRoleCourse.size() > 0) {
			try {
        		delete deleteRoleCourse;
			} catch(Exception e) {
				System.debug(e.getMessage());
				sfdcErrors.add(e.getMessage());
				errorMap.put('SFDCEX', sfdcErrors);
			}
        }
        
        return errorMap;
	}
	
	/**
	 * Adds selected roles to a course with assignment status of Draft
	 * Deletes courses if course is not selected and assignment status is Draft
	 */
	public static Map<String, List<String>> addRoles(List<LMS_RoleList> roleList, List<LMS_RoleAssignment> assignments, 
		Map<String, LMS_Role_Course__c> mappings, String courseId) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
        List<LMS_Role_Course__c> assignList = new List<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> deleteList = new List<LMS_Role_Course__c>();
        Map<String, String> assignMap = new Map<String, String>();
        Map<String, String> statusMap = new Map<String, String>();
        
        for(LMS_RoleAssignment ra : assignments) {
        	assignMap.put(ra.name, ra.assignmentId);
        	statusMap.put(ra.name, ra.assignmentStatus);
        }
        
        for(LMS_RoleList rList : roleList) {
        	if(!assignMap.containsKey(rList.name) && rList.selected == true) {
        		assignList.add(new LMS_Role_Course__c(Course_Id__c = courseId, Role_Id__c = rList.roleId, Assigned_By__c = UserInfo.getName(), 
        			Status__c = statusDraft, Sync_Status__c = outSync));
        	}
        	if(assignMap.containsKey(rList.name) && statusMap.get(rList.name) == statusDraft && rList.selected == false ) {
        		deleteList.add(mappings.get(assignMap.get(rList.name)));
        	}
        }
        
		try {
        	insert assignList;
        	delete deleteList;
		} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
		}
        
        return errorMap;
	}
	
	/**
	 * Cancels all changes made to assignments
	 */
	public static Map<String, List<String>> cancelDraftRoles(string courseId) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
        List<LMS_Role_Course__c> rc = [SELECT Status__c,Previous_Status__c,Commit_Date__c FROM LMS_Role_Course__c WHERE Course_Id__c = :courseId];
        List<LMS_Role_Course__c> toDelete = new list<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> toUpdate = new list<LMS_Role_Course__c>();
        
        if(rc.size() > 0) {
	        for(LMS_Role_Course__c assignment : rc) {
	            if(assignment.Status__c == statusDraft) {
	                toDelete.add(assignment);  
	            } else if(assignment.Status__c == statusDraftDelete) {
	                assignment.Status__c = statusCommitted;
	                toUpdate.add(assignment);
	            } else if(assignment.Status__c == statusPendingAdd) {
	            	assignment.Status__c = assignment.Previous_Status__c;
	            	assignment.Commit_Date__c = null;
	            	toUpdate.add(assignment);
	            } else if(assignment.Status__c == statusPendingDelete) {
	            	assignment.Status__c = assignment.Previous_Status__c;
	            	assignment.Commit_Date__c = null;
	            	toUpdate.add(assignment);
	            }
	        }
        }
        
		try {
	        delete toDelete;
	        upsert toUpdate;
		} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
		}
		
        return errorMap;
	}
	
	/**
	 * Removes role from course : Draft -> Delete, Committed <-> Draft Delete
	 */
	public static Map<String, List<String>> removeRoles(List<LMS_RoleAssignment> assignments, Map<String, LMS_Role_Course__c> mappings) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
        List<LMS_Role_Course__c> toDelete = new List<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> toUpdate = new List<LMS_Role_Course__c>();
        
        for(LMS_RoleAssignment a : assignments) {
        	if(a.selected) {
				LMS_Role_Course__c rc = mappings.get(a.assignmentId);
		        if(rc.Status__c == statusDraft) {
		        	toDelete.add(rc);
		        } else if(rc.Status__c == statusCommitted) {
					rc.Previous_Status__c = rc.Status__c;
		            rc.Status__c = statusDraftDelete;
		            toUpdate.add(rc);
		        } else if(rc.Status__c == statusDraftDelete) {
		            rc.Status__c = statusCommitted;
		            toUpdate.add(rc);
		        } else if(rc.Status__c == statusPendingAdd || rc.Status__c == statusPendingDelete) {
		        	rc.Status__c = rc.Previous_Status__c;
		        	rc.Commit_Date__c = null;
		            toUpdate.add(rc);
		        }
        	}
        }
        
		try {
    		delete toDelete;
        	upsert toUpdate;
		} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
		}
		
        return errorMap;
        
	}
	
	/**
	 * Set Commit date for current assignments
	 */
	public static Map<String, List<String>> setCommitDateRoles(List<LMS_RoleAssignment> assignments, Map<String, LMS_Role_Course__c> mappings, Date commitDate) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
        List<LMS_Role_Course__c> toPend = new List<LMS_Role_Course__c>();
        
        if(null != assignments) {
	        for(LMS_RoleAssignment a : assignments) {
	        	if(a.selected && null != commitDate && a.assignmentStatus != statusCommitted && 
	        		a.assignmentStatus != statusTransitAdd && a.assignmentStatus != statusTransitDelete) {
	        		LMS_Role_Course__c rc = mappings.get(a.assignmentId);
		        	if(rc.Status__c != statusPendingAdd && rc.Status__c != statusPendingDelete) {
		        		rc.Previous_Status__c = rc.Status__c;
		        	}
		        	System.debug('------------------------rc------------------------'+rc);
		        	if(a.assignmentStatus == statusDraftDelete) {
		        		rc.Commit_Date__c = commitDate;
						rc.Status__c = statusPendingDelete;
						toPend.add(rc);
		        	} else if(a.assignmentStatus == statusPendingAdd || a.assignmentStatus == statusPendingDelete) {
		        		rc.Commit_Date__c = commitDate;
		        		toPend.add(rc);
		        	} else {
						rc.Commit_Date__c = commitDate;
						rc.Status__c = statusPendingAdd;
						toPend.add(rc);
		        	}
	        	}
		    }
        }
        
    	try {
    		update toPend;
    	} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
    	}
        
        return errorMap;
	}
	
	/**
	 * Sends all updated assignments to SABA
	 */
	public static Map<String, List<String>> commitRoles(List<LMS_RoleAssignment> assignments, Map<String, LMS_Role_Course__c> mappings) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
		List<LMS_Role_Course__c> batchList = new List<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> toDelete = new List<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> toUpdate = new List<LMS_Role_Course__c>();
        List<LMS_Role_Course__c> toPend = new List<LMS_Role_Course__c>();
		List<LMS_Role_Course__c> updateRoleCourse = new List<LMS_Role_Course__c>();
		List<LMS_Role_Course__c> deleteRoleCourse = new List<LMS_Role_Course__c>();
        Map<String, LMS_Role_Course__c> objectsAdd = new Map<String, LMS_Role_Course__c>();
        Map<String, LMS_Role_Course__c> objectsDelete = new Map<String, LMS_Role_Course__c>();
        Map<String, List<LMS_Role_Course__c>> sendAddMap = new Map<String, List<LMS_Role_Course__c>>();
        Map<String, List<LMS_Role_Course__c>> sendDeleteMap = new Map<String, List<LMS_Role_Course__c>>();
        
        if(null != assignments) {
	        for(LMS_RoleAssignment a : assignments) {
	        	if(a.selected) {
		        	LMS_Role_Course__c rc = mappings.get(a.assignmentId);
		            if(rc.Status__c == statusDraft && rc.Course_Id__r.Available_From__c <= Date.today()) {
		            	objectsAdd.put(rc.Role_Id__r.SABA_Role_PK__c, rc);
		                toUpdate.add(rc);  
		            } else if(rc.Status__c == statusDraft && rc.Course_Id__r.Available_From__c > Date.today()) {
		            	rc.Previous_Status__c = statusDraft;
		                rc.Status__c = statusPendingAdd;
		                toPend.add(rc);
		            } else if(rc.Status__c == statusDraftDelete) {
		                toDelete.add(rc);
		            }
	        	}
	        }
	        
	        if(null != toUpdate && toUpdate.size() > 0) {
	        	sendAddMap.put(actionAdd, toUpdate);
	        	updateRoleCourse = LMS_ToolsService.executeRolesToCourseWS(sendAddMap, objectsAdd);
	        }
	        if(null != toDelete && toDelete.size() > 0) {
	        	deleteRoleCourse = LMS_ToolsService.roleCourseBatchDelete(toDelete);	
	        }
	        
        }
        
        if(LMS_ToolsService.getErrorMap() != null)
        errorMap = LMS_ToolsService.getErrorMap();
        
        if(null != updateRoleCourse && updateRoleCourse.size() > 0) {
			try {
        		upsert updateRoleCourse;
			} catch(Exception e) {
				System.debug(e.getMessage());
				sfdcErrors.add(e.getMessage());
				errorMap.put('SFDCEX', sfdcErrors);
			}
        }
        if(null != toPend && toPend.size() > 0) {
        	try {
        		update toPend;
        	} catch(Exception e) {
				System.debug(e.getMessage());
				sfdcErrors.add(e.getMessage());
				errorMap.put('SFDCEX', sfdcErrors);
        	}
        }
        if(null != deleteRoleCourse && deleteRoleCourse.size() > 0) {
			try {
        		delete deleteRoleCourse;
			} catch(Exception e) {
				System.debug(e.getMessage());
				sfdcErrors.add(e.getMessage());
				errorMap.put('SFDCEX', sfdcErrors);
			}
        }
        
        return errorMap;
	}
	
	/**
	 * Add employee mappings given a certain role
	 */
	public static Map<String, List<String>> addEmployees(List<LMS_PersonList> personList, List<LMS_PersonAssignment> assignments, 
		Map<String, LMS_Role_Employee__c> mappings, String roleId) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
        List<LMS_Role_Employee__c> assignList = new List<LMS_Role_Employee__c>();
        List<LMS_Role_Employee__c> deleteList = new List<LMS_Role_Employee__c>();
        Map<String, String> assignMap = new Map<String, String>();
        Map<String, String> statusMap = new Map<String, String>();
         
        for(LMS_PersonAssignment pAssign : assignments) {
        	assignMap.put(pAssign.employeeCode, pAssign.assignmentId);
        	statusMap.put(pAssign.employeeCode, pAssign.assignmentStatus);
        }
        for(LMS_PersonList pList : personList) {
        	if(!assignMap.containsKey(pList.employeeId) && pList.selected == true) {
        		assignList.add(new LMS_Role_Employee__c(Role_Id__c = roleId, Employee_Id__c = pList.id, Assigned_By__c = UserInfo.getName(), 
        			Created_On__c = System.today(), Updated_On__c = System.today(), Status__c = statusDraft, Sync__c = outSync));
        	}
        	if(assignMap.containsKey(pList.employeeId) && statusMap.get(pList.employeeId) == statusDraft && pList.selected == false ) {
        		deleteList.add(mappings.get(assignMap.get(pList.employeeId)));
        	}
        }

		try {
        	insert assignList;
        	delete deleteList;
		} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
		}
        
        return errorMap;
	}
	
	/**
	 * Revert all changes made to role to employee mappings
	 */
	public static Map<String, List<String>> cancelDraftEmployees(String roleId) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
		List<LMS_Role_Employee__c> re = [SELECT Status__c,Previous_Status__c,Commit_Date__c FROM LMS_Role_Employee__c WHERE Role_Id__c = :roleId];
        List<LMS_Role_Employee__c> toDelete = new List<LMS_Role_Employee__c>();
        List<LMS_Role_Employee__c> toUpdate = new List<LMS_Role_Employee__c>();
        
        if(re.size() > 0) {
	        for(LMS_Role_Employee__c assignment : re) {
	            if(assignment.Status__c == statusDraft) {
	                toDelete.add(assignment);  
	            } else if(assignment.Status__c == statusDraftDelete) {
	                assignment.Status__c = statusCommitted;
	                toUpdate.add(assignment);
	            } else if(assignment.Status__c == statusPendingAdd) {
	            	assignment.Status__c = assignment.Previous_Status__c;
	            	assignment.Commit_Date__c = null;
	            	toUpdate.add(assignment);
	            } else if(assignment.Status__c == statusPendingDelete) {
	            	assignment.Status__c = assignment.Previous_Status__c;
	            	assignment.Commit_Date__c = null;
	            	toUpdate.add(assignment);
	            }
	        }
        }
        
		try {
	        delete toDelete;
	        upsert toUpdate;
		} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
		}
		
        return errorMap;
	}
	
	/**
	 * Change status for role to employee mappings
	 */
	public static Map<String, List<String>> removeEmployees(List<LMS_PersonAssignment> assignments, Map<String, LMS_Role_Employee__c> mappings) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
        List<LMS_Role_Employee__c> toDelete = new List<LMS_Role_Employee__c>();
        List<LMS_Role_Employee__c> toUpdate = new List<LMS_Role_Employee__c>();
        
		for(LMS_PersonAssignment a : assignments) {
			if(a.selected) {
				LMS_Role_Employee__c re = mappings.get(a.assignmentId);
		        if(re.Status__c == statusDraft) {
		        	toDelete.add(re);
		        } else if(re.Status__c == statusCommitted) {
		       		re.Previous_Status__c = re.Status__c;
		            re.Status__c = statusDraftDelete;
		        	toUpdate.add(re);
		        } else if(re.Status__c == statusDraftDelete) {
		            re.Status__c = statusCommitted;
		        	toUpdate.add(re);
		        } else if(re.Status__c == statusPendingAdd || re.Status__c == statusPendingDelete) {
		        	re.Status__c = re.Previous_Status__c;
		        	re.Commit_Date__c = null;
		        	toUpdate.add(re);
		        }
			}
		}
		
		try {
    		delete toDelete;
        	upsert toUpdate;
		} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
		}
		
        return errorMap;
	}
	
	/**
	 * Set Commit date for current assignments
	 */
	public static Map<String, List<String>> setCommitDateEmployees(List<LMS_PersonAssignment> assignments, Map<String, LMS_Role_Employee__c> mappings, Date commitDate) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
        List<LMS_Role_Employee__c> toPend = new List<LMS_Role_Employee__c>();
        
        if(null != assignments) {
	        for(LMS_PersonAssignment a : assignments) {
	        	if(a.selected && a.assignmentStatus != statusCommitted && null != commitDate) {
	        		LMS_Role_Employee__c re = mappings.get(a.assignmentId);
		        	if(re.Status__c != statusPendingAdd && re.Status__c != statusPendingDelete) {
		        		re.Previous_Status__c = re.Status__c;
		        	}
		        	System.debug('------------------------re------------------------'+re);
		        	if(a.assignmentStatus == statusDraftDelete) {
		        		re.Commit_Date__c = commitDate;
						re.Status__c = statusPendingDelete;
						toPend.add(re);
		        	} else if(a.assignmentStatus == statusPendingAdd || a.assignmentStatus == statusPendingDelete) {
		        		re.Commit_Date__c = commitDate;
		        		toPend.add(re);
		        	} else {
						re.Commit_Date__c = commitDate;
						re.Status__c = statusPendingAdd;
						toPend.add(re);
		        	}
	        	}
		    }
        }
        
        if(null != toPend && toPend.size() > 0) {
        	try {
        		update toPend;
        	} catch(Exception e) {
				System.debug(e.getMessage());
				sfdcErrors.add(e.getMessage());
				errorMap.put('SFDCEX', sfdcErrors);
        	}
        }
        
        return errorMap;
	}
	
	/**
	 * Commit changes made to role to employee mappings
	 */
	public static Map<String, List<String>> commitEmployees(List<LMS_PersonAssignment> assignments, Map<String, LMS_Role_Employee__c> mappings) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
        List<LMS_Role_Employee__c> toRemove = new List<LMS_Role_Employee__c>();
        List<LMS_Role_Employee__c> toUpdate = new List<LMS_Role_Employee__c>();
        List<LMS_Role_Employee__c> toPend = new List<LMS_Role_Employee__c>();
        List<LMS_Role_Employee__c> updateList = new List<LMS_Role_Employee__c>();
        Map<String, LMS_Role_Employee__c> objectsUpdate = new Map<String, LMS_Role_Employee__c>();
        Map<String, List<LMS_Role_Employee__c>> mapUpdate = new Map<String, List<LMS_Role_Employee__c>>();
        
        if(null != assignments) {
	        for(LMS_PersonAssignment a : assignments) {
	        	if(a.selected) {
		        	LMS_Role_Employee__c re = mappings.get(a.assignmentId);
		            if(re.Status__c == statusDraft && re.Employee_Id__r.Date_Hired__c <= Date.today()) {
		                toUpdate.add(re);  
	        			objectsUpdate.put(re.Employee_Id__r.Name, re);
		            } else if(re.Status__c == statusDraft && re.Employee_Id__r.Date_Hired__c > Date.today()) {
		            	re.Previous_Status__c = statusDraft;
		            	re.Status__c = statusPendingAdd;
		            	toPend.add(re);
		            } else if(re.Status__c == statusDraftDelete) {
		                toRemove.add(re);
	        			objectsUpdate.put(re.Employee_Id__r.Name, re);
		            }
	        	}
	        }
	        if(null != toUpdate && toUpdate.size() > 0) {
	       		mapUpdate.put(actionAdd, toUpdate);
	        }
	        if(null != toRemove && toRemove.size() > 0) {
	        	mapUpdate.put(actionRemove, toRemove);
	        }
	        if(!mapUpdate.isEmpty()) {
	        	updateList = LMS_ToolsService.executeEmployeesToRoleWS(mapUpdate, objectsUpdate);
	        }
        }
        
        if(LMS_ToolsService.getErrorMap() != null)
        errorMap = LMS_ToolsService.getErrorMap();
        
        if(updateList.size() > 0) {
			try {
        		upsert updateList;
			} catch(Exception e) {
				System.debug(e.getMessage());
				sfdcErrors.add(e.getMessage());
				errorMap.put('SFDCEX', sfdcErrors);
			}
        }
        if(toPend != null && toPend.size() > 0) {
        	try {
        		update toPend;
        	} catch(Exception e) {
        		System.debug(e.getMessage());
				sfdcErrors.add(e.getMessage());
				errorMap.put('SFDCEX', sfdcErrors);
        	}
        }
        
        return errorMap;
	}

}