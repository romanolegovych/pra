public class LMS_RoleWrapper {
    
    public Boolean checked{get;set;}
    public String roleId{get;private set;}
    public String roleName{get;private set;}
    public String roleType{get;private set;}
    public String sync{get;private set;}
    public String status{get;private set;}
    public Integer roleCourses{get;private set;}
    public Integer employeeCount{get;private set;}        
    public String CreatedBy{get;private set;}
    public String LastModifiedBy{get;private set;}
    public String CreatedDate{get;private set;}
    public String LastModifiedDate{get;private set;} 
   
   
    public LMS_RoleWrapper(LMS_Role__c role) {
        checked = false;
        roleId = role.Id;
        roleName = role.Role_Name__c;
        roleType = role.Role_Type__r.Name;
        sync = role.Sync_Status__c;
        status = role.Status__c;
        roleCourses = role.RoleCourses__r.size();
        employeeCount = Integer.valueOf(role.Employee_Count__c);        
        CreatedBy = role.CreatedBy.Name;
        LastModifiedBy = role.LastModifiedBy.Name;       
        CreatedDate = role.CreatedDate.format();
        LastModifiedDate = role.LastModifiedDate.format();
        
    }
}