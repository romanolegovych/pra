public with sharing class AA_EditBillRateExtension{

    public AA_EditBillRateExtension(ApexPages.StandardController controller) {}
    
    public AA_EditBillRateExtension(){}
    
    public PageReference redirect(){
        String billRateId = ApexPages.CurrentPage().getParameters().get('Id');
        Bill_rate__c billRate = AA_BillRateDataAccessor.getBillRate(billRateId);
        //Bill_rate__c billRate = [select Id, Name, Model_Id__c, Job_Position__c, Country__c, Schedule_Year__c from Bill_Rate__c where Id=: billRateId limit 1];
        PageReference pageRef = Page.AA_BillRate_InflationOverride; 
        pageRef.getParameters().put('jobPosition',billRate.Job_Position__c);
        pageRef.getParameters().put('country',billRate.Country__c);
        pageRef.getParameters().put('scheduledYear',billRate.Schedule_Year__c);
        pageRef.getParameters().put('modelId',billRate.Model_Id__c);
        return pageRef;
    }
}