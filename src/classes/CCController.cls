public with sharing class CCController
{
	public ID selectedStep {get; set;}
	public List<Critical_Chain_Step_Junction__c> chainSteps
	{
		get
		{
			if (chainSteps == null)
			{
				chainSteps = [select Flow_Step__r.STSWR1__Duration__c, Flow_Step__c, Flow_Step__r.Name, Flow_Step__r.STSWR1__Flow__r.Name from Critical_Chain_Step_Junction__c where Critical_Chain__c = :chainID order by Flow_Step__r.STSWR1__Flow__r.Name, Flow_Step__r.Name];
			}
			
			return chainSteps;
		}
		set;
	}
	
	public List<SelectOption> flowOptions
	{
		get
		{
			if (flowOptions == null)
			{
				flowOptions = new List<SelectOption>{new SelectOption(chain.Flow__c, 'Master Flow')};
				for (STSWR1__Flow__c each : [select Name from STSWR1__Flow__c where Id in :selectDescendants(chain.Flow__c) order by Name])
				{
					flowOptions.add(new SelectOption(each.Id, each.Name));
				}
			}
			
			return flowOptions;
		}
		private set;
	}
	
	public ID selectedFlow
	{
		get;
		set
		{
			if (selectedFlow != value)
			{
				stepOptions = null;
			}
			
			selectedFlow = value;
		}
	}
	
	public List<SelectOption> stepOptions
	{
		get
		{
			if (stepOptions == null)
			{
				stepOptions = new List<SelectOption>();
				for (STSWR1__Flow_Step_Junction__c each : [select Name from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__c = :selectedFlow and Id not in (select Flow_Step__c from Critical_Chain_Step_Junction__c where Critical_Chain__c = :chainID) and Id in (select STSWR1__Flow_Step__c from STSWR1__Flow_Step_Property__c where STSWR1__Type__c = 'Tag' and Name = 'Critical') order by Name])
				{
					stepOptions.add(new SelectOption(each.Id, each.Name));
				}
			}
			
			return stepOptions;
		}
		private set;
	}
	
	public Critical_Chain_Step_Junction__c newJunction {get; set;}
	
	public Critical_Chain__c chain {get; set;}
	private ID chainID;
	
	public CCController(ApexPages.StandardController controller)
	{
		chainID = controller.getID();
		chain = (Critical_Chain__c) controller.getRecord();
		selectedFlow = chain.Flow__c;
		newJunction = new Critical_Chain_Step_Junction__c(Critical_Chain__c = chainID);
	}
	
	/**
	* Adds a step to the current Critical Chain
	*/
	public void addStep()
	{
		insert newJunction.clone(true, true);
		chainSteps = null;
		stepOptions = null;
		newJunction = new Critical_Chain_Step_Junction__c(Critical_Chain__c = chainID);
	}
	
	public static List<ID> selectDescendants(ID flowID)
	{
		List<ID> descendantIDs = new List<ID>();
		List<ID> childIDs = new List<ID>{flowID};
		Boolean keepGoing = true;
		while (keepGoing)
		{
			keepGoing = false;
			for (STSWR1__Flow_Step_Action__c action : [select STSWR1__Flow_Step__c, STSWR1__Flow_Step__r.STSWR1__Flow__c, STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Flow_Step__r.STSWR1__Flow__c in :childIDs and STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active'])
			{
				try
				{
					Map<String, Object> config = (Map<String, Object>) JSON.deserializeUntyped(action.STSWR1__Config__c);
					if (config != null && !String.isEmpty((String) config.get('flowId'))) 
					{
						if (keepGoing == false)
						{
							childIDs.clear();
						}
						
						keepGoing = true;
						childIDs.add((ID) config.get('flowId'));
					}
				}
				catch (Exception ex)
				{
					//do nothing
				}
			}
			
			descendantIDs.addAll(childIDs);
		}
		
		return descendantIDs;
	}
	
	/**
	* Use this method to refresh/re-render page
	*/
	public PageReference refresh()
	{
		stepOptions = null;
		return null;
	}
	
	public void save()
	{
		upsert chainSteps;
	}
	
	public void deleteStep()
	{
		delete [select Name from Critical_Chain_Step_Junction__c where Id = :selectedStep];
		chainSteps = null;
		stepOptions = null;
	}
	/*
	public List<ID> buildCriticalPath(ID chainID)
	{
		List<ID> result = new List<ID>();
		//Retrieve critical steps
		for (Critical_Chain__c chain : [select Flow__c from Critical_Chain__c where Id = :chainID])
		{
			List<ID> descendantIDs = selectDescendants(chain.Flow__c);
			List<STSWR1__Flow_Step_Junction__c> criticalSteps = selectCriticalSteps(chainID);
			List<STSWR1__Flow_Step_Junction__c> allSteps = selectAllSteps(chain.Flow__c);
		}
		
		//Build all possible paths 
		//Calculate durations for all paths
		
		return result;
	}
	
	private List<STSWR1__Flow_Step_Junction__c> selectCriticalSteps(ID chainID)
	{
		return [select Name from STSWR1__Flow_Step_Junction__c where Id in (select Flow_Step__c from Critical_Chain_Step_Junction__c where Critical_Chain__c = :chainID)];
	}
	
	private List<STSWR1__Flow_Step_Junction__c> selectAllSteps(ID flowID)
	{
		List<ID> descendantIDs = selectDescendants(chain.Flow__c);
		descendantIDs.add(flowID);
		
		return [select Name, (select Name, STSWR1__Flow_Step_Source__c, STSWR1__Flow_Step_Target__c from STSWR1__Flow_Step_Input_Connections__r), (select STSWR1__Flow_Step__c, STSWR1__Flow_Step__r.STSWR1__Flow__c, STSWR1__Config__c from STSWR1__Flow_Step_Actions__r where STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active') from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__c in :descendantIDs];
	}
	
	public String criticalPath
	{
		get
		{
			String result = '';
			List<String> names = new List<String>();
			Decimal duration = 0;
			for (Critical_Chain_Step_Junction__c each : chainSteps)
			{
				names.add('"' + each.Flow_Step__r.Name + '"');
				duration += (each.Flow_Step__r.STSWR1__Duration__c != null ? each.Flow_Step__r.STSWR1__Duration__c : 0);
			}
			
			return String.join(names, '->') + ' Total duration is ' + duration.intValue() + ' day(s).';
		}
	}
	
	public class Graph
	{
		private List<STSWR1__Flow_Step_Junction__c> steps = new List<STSWR1__Flow_Step_Junction__c>();
		
		public Graph(ID flowID)
		{
			this.steps = selectSteps(flowID);
		}
		
		public Graph(List<STSWR1__Flow_Step_Junction__c> steps)
		{
			this.steps = steps;
		}
		
		private List<STSWR1__Flow_Step_Junction__c> selectSteps(ID flowID)
		{
			List<ID> descendantIDs = getDescendantFlowIDs(flowID);
			descendantIDs.add(flowID);
			
			return [select Name, (select Name, STSWR1__Flow_Step_Source__c, STSWR1__Flow_Step_Target__c from STSWR1__Flow_Step_Input_Connections__r), (select STSWR1__Flow_Step__c, STSWR1__Flow_Step__r.STSWR1__Flow__c, STSWR1__Config__c from STSWR1__Flow_Step_Actions__r where STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active') from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__c in :descendantIDs];
		}
		
		private List<ID> getDescendantFlowIDs(ID flowID)
		{
			List<ID> descendantIDs = new List<ID>();
			List<ID> childIDs = new List<ID>{flowID};
			Boolean keepGoing = true;
			while (keepGoing)
			{
				keepGoing = false;
				for (STSWR1__Flow_Step_Action__c action : [select STSWR1__Flow_Step__c, STSWR1__Flow_Step__r.STSWR1__Flow__c, STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Flow_Step__r.STSWR1__Flow__c in :childIDs and STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active'])
				{
					try
					{
						Map<String, Object> config = (Map<String, Object>) JSON.deserializeUntyped(action.STSWR1__Config__c);
						if (config != null && !String.isEmpty((String) config.get('flowId'))) 
						{
							if (keepGoing == false)
							{
								childIDs.clear();
							}
							
							keepGoing = true;
							childIDs.add((ID) config.get('flowId'));
						}
					}
					catch (Exception ex)
					{
						//do nothing
					}
				}
				
				descendantIDs.addAll(childIDs);
			}
			
			return descendantIDs;
		}
		
		public List<Node> nodes = new List<Node>();
		
		public Node root
		{
			get
			{
				if (root == null)
				{
					for (Node each : nodes)
					{
						if (each.parents == null)
						{
							root = each;
							break;
						}
					}
				}
				
				return root;
			}
			private set;
		}
		
		public List<List<Node>> buildPaths(Node a, Node b)
		{
			List<Node> aDescendants = a.getDescendants();
			List<Node> aAncestors = a.getAncestors();
			
			List<Node> bDescendants = b.getDescendants();
			List<Node> bAncestors = b.getAncestors();
			
			Node start;
			Node finish;
			for (Node each : aDescendants)
			{
				if (each == b)
				{
					start = a;
					finish = b;
				}
			}
			
			for (Node each : bDescendants)
			{
				if (each == a)
				{
					start = b;
					finish = a;
				}
			}
			
			System.debug(System.LoggingLevel.ERROR, 'Start is : ' + start.name);
			
			List<List<Node>> paths = new List<List<Node>>();
			if (start != null)
			{
				Set<Node> intersection = new Set<Node>(start.getDescendants());
				intersection.retainAll(finish.getAncestors());
				
				log(new List<Node>(intersection));//@TODO: Remove this line!
				
				List<Node> leafs = start.children;
				Set<Node> visited = new Set<Node>(leafs);
				for (Node leaf : leafs)
				{
					paths.add(new List<Node>{start, leaf});
				}
				
				Boolean keepGoing = false;
				do
				{
					List<Node> nextLeafs = new List<Node>();
					for (Node leaf : leafs)
					{
						List<List<Node>> forks = new List<List<Node>>();
						for (Node child : leaf.children)
						{
							if (!visited.add(child) || !intersection.contains(child))
							{
								continue;
							}
							
							nextLeafs.addAll(child.children);
							keepGoing = true;
							
							//Multiple existing paths if there are more then one ways to continue
							for (List<Node> path : paths)
							{
								if (path.get(path.size() - 1) != child)
								{
									continue;
								}
								
								List<Node> fork = path.clone();
								fork.add(child);
								forks.add(fork);
							}
						}
						
						paths = forks;
					}
					
					leafs = nextLeafs;
				} while (keepGoing);
			}
			
			return paths;
		}
	}
	
	public class Node
	{
		public String name;
		public List<Node> children = new List<Node>();
		public List<Node> parents = new List<Node>();
		private Graph container;
		
		public Node(String name)
		{
			this.name = name;
		}
		
		public Node addChild(Node child)
		{
			this.children.add(child);
			child.parents.add(this);
			
			return this;
		}
		
		public List<Node> getDescendants()
		{
			List<Node> descendants = this.children.clone();
			for (Node child : children)
			{
				descendants.addAll(child.getDescendants());
			}
			
			return descendants;
		}
		
		public List<Node> getAncestors()
		{
			List<Node> ancestors = new List<Node>();
			for (Node parent : parents)
			{
				ancestors.addAll(parent.getAncestors());
			}
			
			return ancestors;
		}
	}
	public static void log(List<Node> nodes)
	{
		List<String> names = new List<String>();
		for (Node each : nodes)
		{
			names.add(each.name);
		}
		
		System.debug(System.LoggingLevel.ERROR, String.join(names, '->'));
	}
	/**/
}