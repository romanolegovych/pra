@isTest
private class ArmDataAccessorTest {

    static testMethod void myUnitTest() {
        // create test data
        ClinicalDesign__c cd = new ClinicalDesign__c(name='1');
        insert cd;
        
        list<arm__c> aList = new list<arm__c>();
        aList.add(new arm__c(ClinicalDesign__c = cd.id, Arm_Number__c = 1, Description__c = 'Arm 1'));
        aList.add(new arm__c(ClinicalDesign__c = cd.id, Arm_Number__c = 2, Description__c = 'Arm 2'));
        aList.add(new arm__c(ClinicalDesign__c = cd.id, Arm_Number__c = 3, Description__c = 'Arm 3'));
        insert aList;
        
        //test retrieve logic
        aList = ArmDataAccessor.getArmList('ClinicalDesign__c = \'' + cd.id + '\'');
        system.assertEquals(3,aList.size());
    }
}