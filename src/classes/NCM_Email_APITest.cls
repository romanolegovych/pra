/**
 * @description	Implements the test for the functions of the NCM_Email_API class
 * @author		Dimitrios Sgourdos
 * @version		Created: 01-Oct-2015, Edited: 02-Oct-2015
 */
@isTest
private class NCM_Email_APITest {
	
	/**
	 * @description	The test is only for the code coverage, as the API contains only calls to methods of the
	 *				Service Layers
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Oct-2015, Edited: 02-Oct-2015
	*/
	static testMethod void myUnitTest() {
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		NCM_Email_API.getOrgWideEmailAddressIdFromAddressName(NULL);
		NCM_Email_API.createEmailInstance(testUser.Id, NULL, 'Test Subject', 'Test mail body', NULL);
		NCM_Email_API.sendEmailInstances(new List<Messaging.SingleEmailMessage>());
	}
}