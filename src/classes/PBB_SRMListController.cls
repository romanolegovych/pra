/* 
@author Kondal Reddy
@date 2014  
@description Controller for Listing SRM Scenarios on PBB Scenario SRM tab
*/
public with sharing class PBB_SRMListController {

   //to hide/show the main tabs in the component.
   public  List<String> HiddenHomeTabsList{get;set;}
   
   //to hide/show the subtabs in the component.
   public  List<String> HiddenSubTabsList{get;set;}

    public String pbbScenarioID{get;set;}  
    public String srmScenarioId{get;set;}
    public String bidProjectId{ get; set; }
    public List<SRM_Scenario__c> sclist{get;set;}
    public PBB_Scenario__c pbbobj{get;set;}
    
    public PBB_SRMListController(ApexPages.StandardController controller) {
        HiddenHomeTabsList = new List<String>();
        HiddenSubTabsList = new List<String>();
       
        pbbScenarioID = ApexPages.currentPage().getParameters().get('PBBScenarioID');
        system.debug(pbbScenarioID);
        
        if(pbbScenarioID!=null && pbbScenarioID!=''){
            system.debug(pbbScenarioID);
            pbbobj= PBB_DataAccessor.getScenarioByID(pbbScenarioID);
            
            if(pbbobj.Bid_Project__c!=null){
                system.debug(pbbobj.Bid_Project__c);
                scList = new List<SRM_Scenario__c>();
                sclist = PBB_DataAccessor.getSRMScenarioListByBidProjectId(pbbobj.Bid_Project__c);
                system.debug(sclist);
            }
        }
        //HiddenSubTabsList.add('SDpage'); 
    }
    
    /*
    @author Kondal Reddy
    @date 2014  
    @description Constructor
    */
    public PBB_SRMListController(){
                
    }
    
    /*
    @author Kondal Reddy
    @date 2014  
    @description Method to Attach SRM Scenario to PBB Scenario
    */
    public PageReference attachSRMScenario(){
        System.debug('Scenario Id '+srmScenarioId);
        pbbobj.SRM_Scenario__c = srmScenarioId;
        upsert pbbobj;
        //PBB_Services service = new PBB_Services();
        PBB_Services.createPBBScenarioCountries(pbbobj, pbbobj.SRM_Scenario__c);
        return null;
    }
    
    /*
    @author Kondal Reddy
    @date 2014  
    @description Method to Detach SRM Scenario from PBB Scenario
    */
    public PageReference detachSRMScenario(){
        pbbobj.SRM_Scenario__c = null;
        upsert pbbobj;
        //PBB_Services service = new PBB_Services();
        PBB_Services.deleteWeeklyEvents(pbbobj);
        return null;
    }
    
    /*
    @author Niharika Reddy
    @date 2014  
    @description Method to create new SRM Scenario 
    */
    public PageReference newSrmScenario(){
        PageReference pgref = new PageReference('/apex/SRM_Scenario?bid='+ pbbobj.Bid_Project__c);
        pgref.setRedirect(true);
        return pgref;
    }
    
    /*
    @author Niharika Reddy
    @date 2014  
    @description Method to add edit functionality for the SRM Records 
    */
    public PageReference srmEdit(){
        PageReference pgref = new PageReference('/apex/SRM_Scenario?id='+ srmScenarioId);
        pgref.setRedirect(true);
        return pgref;
    }
    
}