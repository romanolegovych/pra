@isTest
private class ServiceTaskToServiceImpactTest {

	private static Service_Model__c serviceModel;
    private static Service_Area__c serviceArea;
    private static Service_Function__c serviceFunction;
    private static Service_Task__c serviceTask;
    private static Service_Task_To_Drivers__c serviceTaskToDrivers;
    private static Service_Task_To_Service_Impact__c serviceTaskToServiceImpact;

    private static ServiceTaskToServiceImpact serviceTaskToSI;

    static void createTestData() {
        PBB_TestUtils testUtils = new PBB_TestUtils();
        serviceModel = testUtils.CreateServiceModelAttributes();
        serviceArea = testUtils.createServiceAreaAttributes();
        serviceFunction = testUtils.createServiceFunctionAttributes();
        serviceTask = testUtils.createServiceTaskAttributes();
        Formula_Name__c billFormula = testUtils.createBillFormulaNames();
        serviceTaskToDrivers = testUtils.createSTtoBF(serviceTask, billFormula);
        Service_Impact_Question_Name__c siqn = testUtils.createSIQNAttributes();
        serviceTaskToServiceImpact = testUtils.createSTtoSI(serviceTask, siqn);

        serviceTaskToSI = new ServiceTaskToServiceImpact(serviceTask.Id);
        serviceTaskToSI.serviceTaskToServiceImpacts = new List<Service_Task_To_Service_Impact__c>{serviceTaskToServiceImpact};
    	serviceTaskToSI.serviceTaskToServiceImpact = serviceTaskToServiceImpact;
    }

    @isTest
    static void refreshServicesTest() {
    	createTestData();
    	Test.startTest();
    	serviceTaskToSI.refreshServices();
    	Test.stopTest();
    	System.assert(serviceTaskToSI.serviceTaskToServiceImpacts.size() > 0);
    }

	@isTest
    static void removeServiceTest() {
    	createTestData();
    	ApexPages.currentPage().getParameters().put('editServiceTaskToServiceImpactId', serviceTaskToServiceImpact.Id);
    	Test.startTest();
    		serviceTaskToSI.removeService();
    	Test.stopTest();
    	System.assert(serviceTaskToSI.serviceTaskToServiceImpacts.size() == 0);
    }

	@isTest
    static void preparationCreateOrEditServiceNewTest() {
    	createTestData();
    	Test.startTest();
    		serviceTaskToSI.preparationCreateOrEditService();
    	Test.stopTest();
    	System.assertEquals(serviceTaskToSI.serviceTaskToServiceImpact.Id, null);
    	System.assertEquals(serviceTaskToSI.serviceTaskToServiceImpact.Service_Task__c, serviceTask.Id);
    }

	@isTest
    static void preparationCreateOrEditServiceEditTest() {
    	createTestData();
    	ApexPages.currentPage().getParameters().put('editServiceTaskToServiceImpactId', serviceTaskToServiceImpact.Id);
    	Test.startTest();
    		serviceTaskToSI.preparationCreateOrEditService();
    	Test.stopTest();
    	System.assertEquals(serviceTaskToSI.serviceTaskToServiceImpact.Id, serviceTaskToServiceImpact.Id);
    }

	@isTest
    static void isValidServiceTrueTest() {
    	createTestData();
    	Test.startTest();
    		Boolean isValidService = serviceTaskToSI.isValidService();
    	Test.stopTest();
    	System.assertEquals(isValidService, true);
    }

    @isTest
    static void isValidServiceFalseTest() {
    	createTestData();
    	serviceTaskToSI.serviceTaskToServiceImpact = new Service_Task_To_Service_Impact__c();
    	Test.startTest();
    		Boolean isValidService = serviceTaskToSI.isValidService();
    	Test.stopTest();
    	System.assertEquals(isValidService, false);
    }

    @isTest
    static void propertiesTest() {
    	createTestData();
    	Test.startTest();
    		serviceTaskToSI.getDetailService();
    	Test.stopTest();
    	System.assertEquals(serviceTaskToSI.getServiceWord(), 'TaskToServiceImpact');
    }
}