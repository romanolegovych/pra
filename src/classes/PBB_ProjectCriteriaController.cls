/**
@author Bhargav Devaram
@date 2014
@description this controller class is to display the sub tab menus related to a Project Criteria
**/
public class PBB_ProjectCriteriaController {
   
  /**
  * to hide/show the main tabs in the component.
  */
   public  List<String> HiddenHomeTabsList        {get;set;} 
   
   /** 
   * to hide/show the subtabs in the component.
   */
   public  List<String> HiddenSubTabsList         {get;set;}
   
   //PBB scenarios ID  
   Public String PBBScenarioID                    {get;set;}
   
   //Default it to project info tab
   public static final string Defaulttoprojectintotab  = '1';
   public string screenState                      {get;private set;}
   
   /** 
   @author Bhargav Devaram
   @date 2014  
   @description Constructor 
   */ 
   Public PBB_ProjectCriteriaController(){
       
       PBBScenarioID = ApexPages.currentPage().getParameters().get('PBBScenarioID');
       HiddenHomeTabsList = new List<String>();
       HiddenSubTabsList = new List<String>();  
       //HiddenSubTabsList.add('SDpage'); 
       
       screenState = PBB_ProjectCriteriaController.Defaulttoprojectintotab;
           
   }
   
}