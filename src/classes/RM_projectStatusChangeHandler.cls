public with Sharing Class RM_projectStatusChangeHandler {
    private boolean m_isExecuting = false;
    private integer BatchSize = 0;
  private string strNote = ' This assignment was ended by the system based on an update to the project status';
    public RM_projectStatusChangeHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;   
        //
    }
    
    public void statusClosedOrLost(Set<Id> projId){
        Set<Id> closePIDs = projId;
        if (closePIDs.size() > 0){
          list<string> aIDs = new list<string>();
          list<string> lstLostID = new list<string>();
          list<string> lstCloseID = new list<string>();
          try{
            
              for (WFM_employee_Allocations__c a: RM_AssignmentsService.GetAllocationByProjIDsDate(closePIDs, system.today())){
                aIDs.add(a.id);
                if (a.Project_ID__r.Status_Desc__c == 'Lost')
                  lstLostID.add(a.id);
                else if (a.Project_ID__r.Status_Desc__c == 'Closed')
                  lstCloseID.add(a.id);
              }
               system.debug('---lstLostID--'+lstLostID);
               system.debug('---lstCloseID--'+lstCloseID);
              if (lstCloseID.size() > 0){
                RM_AssignmentsService.endAssignment(lstCloseID, system.today());
                // Add note
                
                RM_AssignmentsService.SaveBatchNote(lstCloseID, strNote);
              }
              if (lstLostID.size() > 0){
                system.debug('---end Date--'+RM_Tools.GetFirstDayDatefromDate(system.today()).addDays(-1));
                RM_AssignmentsService.endAssignment(lstLostID, RM_Tools.GetFirstDayDatefromDate(system.today()).addDays(-1));
                // Add note
                
                RM_AssignmentsService.SaveBatchNote(lstLostID, strNote);
              }
           }
           catch (DmlException e){
                System.debug(e.getMessage());
           }
        }
    }
    public void projStatusChanges(Set<Id> projId){
     Set<Id> projectIDs = projId;
     if (projectIDs.size() > 0){
        list<WFM_Employee_Assignment__c> lsAssigns = RM_AssignmentsService.GetAssignmentByProjectIDs(projectIDs);
        for (WFM_Employee_Assignment__c ass :lsAssigns){
            if (ass.Allocation_Key__r.Project_ID__r.Status_Desc__c == 'Bid')
                ass.type__c = 'Reservation';
            else if (ass.Allocation_Key__r.Project_ID__r.Status_Desc__c == 'Active')
                ass.type__c = 'Assignment';
        }
        try{
            update lsAssigns;
            system.debug('---lsAssigns--'+lsAssigns);
        }
        catch (DmlException e) {
            System.debug(e.getMessage());
        }
     }
    }
    public void notifychangeproject(String pID,String oldValue,String newValue,String ChangeType)
    {  
        WFM_Notification__c nc = new WFM_Notification__c();
        if (ChangeType == 'Status')
            nc.Change_Type__c = 'Project Status Change'; 
        else
            nc.Change_Type__c = 'Project End date Change'; 
       
        //nc.New_Value__c = newValue;
        //nc.Old_Value__c = oldValue;
        string strDate = newValue;
        string strDate2 = oldValue;        
        if(strDate!=null){
            if(strDate.contains('-')){
                String String1 = strDate.substring(0,4);
                String String2 = strDate.substring(5,7);
                String String3 = strDate.substring(8,10);
               
                strDate = string2+'/'+string3+'/'+String1; 
                system.debug(newValue+' '+String3+' '+String2+' '+String1+' '+strDate);     
            } 
        }
        if(strDate2!=null){
            if(strDate2.contains('-')){
                String String1 = strDate2.substring(0,4);
                String String2 = strDate2.substring(5,7);
                String String3 = strDate2.substring(8,10);
               
                strDate2 = string2+'/'+string3+'/'+String1; 
                system.debug(newValue+' '+String3+' '+String2+' '+String1+' '+strDate);     
            }
        }   
        nc.New_Value__c = strDate;  
        nc.Old_Value__c = strDate2 ;
        nc.WFM_Project__c = pID;
        insert nc;
   
    }
}