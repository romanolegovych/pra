/**
*   'HR_DataAccessor' class is to access all the SOQL queries and also to perform DML operations on HR application objects.
*   @author   Devaram Bhargav
*   @version  26-Sep-2013
*   @since    17-Sep-2013
*/
public with sharing class HR_DataAccessor {
    /**
    *   Method retrieve the  Questionnaire data with the predefined attributes value (FormType).   
    *   @param    FormType   The type of the form like "Exit Interview" form.
    *   @return   The Questionnaire data for the form type.
    */   
    public static List<Questionnaire__c> getQuestionnaireListbyFormtype(String FormType){
        return [Select id,
                       name,
                       Form_Type__c,
                       Section__c,
                       Section_Order__c,
                       Required__c,
                       Question_Text__c,
                       Question_Type__c,
                       Question_Choices__c,
                       Active__c,
                       Question_Order__c 
                FROM   Questionnaire__c
                WHERE  Form_Type__c=:FormType and Active__c=true
                ORDER BY Question_Order__c,Section_Order__c];
    }
    
    /**
    *   Method retrieve the HR_Exit_Interview_Questionnaire__c data with the predefined attributes value (EXIid).   
    *   @param    EXIid   This is ID of the Exit Interview record.
    *   @return   The HR_Exit_Interview_Questionnaire__c data for the Exit Interview record.
    */ 
    public static List<HR_Exit_Interview_Questionnaire__c> getEXIQuestionnaireListbyEXIid(ID EXIid){
        return [Select id,
                       name,
                       HR_Exit_Interview__c,
                       Question_ID__c,
                       Questions__c,
                       Response__c,                       
                       Comments_notes__c
                FROM   HR_Exit_Interview_Questionnaire__c
                WHERE  HR_Exit_Interview__c=:EXIid 
                ORDER BY Question_ID__c ];
    }
    
    /**
    *   Method upserts the HR_Exit_Interview_Questionnaire__c data with the predefined attributes value (HREIQList).   
    *   @param    HREIQList   This is the list of the HR_Exit_Interview_Questionnaire__c object.
    *   @return   The errors message.
    */ 
    public static String upsertHRQuestionnairebyList(List<HR_Exit_Interview_Questionnaire__c> HREIQList){
        String ErrorMsg;
        try{
            
            UPSERT  HREIQList;            
                
        }catch(Exception ex) {
            ErrorMsg=ex.getMessage();                
        }
        return ErrorMsg;
    }
    
    /**
    *   Method deletes the HR_Exit_Interview_Questionnaire__c data with the predefined attributes value (HREIQList).   
    *   @param    HREIQList   This is the list of the HR_Exit_Interview_Questionnaire__c object.
    *   @return   The errors message.
    */    
    public static String deleteHRQuestionnairebyList(List<HR_Exit_Interview_Questionnaire__c> HREIQList){
        String ErrorMsg;
        try{
            DELETE  HREIQList;                
        }catch(Exception ex) {
            ErrorMsg=ex.getMessage();                
        }
        return ErrorMsg;
    } 
}