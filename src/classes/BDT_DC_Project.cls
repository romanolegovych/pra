public with sharing class BDT_DC_Project {

	// create an empty project
	public static Client_Project__c createEmptyProject () {
		Client_Project__c cp = new Client_Project__c();
		return cp;
	}
	
	public static String getNewProjectCode(){

		date myDate = date.today();

		//current year
		String year = String.valueOf(Datetime.now().year()).substring(2,4); 
		//current month
		String month = String.valueOf(Datetime.now().month()).length()<2 ? '0'+String.valueOf(Datetime.now().month()): String.valueOf(Datetime.now().month());
		
		String startOfCode = 'PRA'+year+month+'-';
		// get perviously assigned followup number
		integer followUpNumber;
		try{
			Client_Project__c latestProject  = [Select 	code__c
												from 	Client_project__c 
												where	Code__c like :startOfCode +'%'
												order by createddate desc Limit 1];
			followUpNumber = Integer.valueOf(latestProject.code__c.right(3))+1;
		} catch(Exception e) {
			// no previous record found
			followUpNumber = 1;
		}
		
		return startOfCode + BDT_Utils.lPad(String.valueOf(followUpNumber), 3, '0');
	}

}