/** 
@Author Bhargav Devaram
@Date 2014
@Description this class is for selection of countries for the PBB Scenario and its services.
*/
public class PBB_CountryServicesController {
    
    
    public String PBBSID{ get; set; } 
    public String PBBScenarioID{ get; set; }               //PBB Scenario object ID
    public PBB_Scenario__c PBBScenario{ get; set; }        //PBB Scenario object 
    
    public static String PROJECT_LEVEL{get {return PBB_Constants.PROJECT_LEVEL;}}
    
    //Picklist to hold all available countries
    public SelectOption[] availableCountriesList { get; set; } 
    
    //Picklist to hold all selected countries for PBB Scenario
    public SelectOption[] selectedCountriesList { get; set; } 
    
    //lists to hold old and new selection of countries for PBB Scenario 
    public Set<String> oldSelectedSet{ get; set; } 
    public Set<String> NewSelectedSet{ get; set; }
    
    public Map<String,List<String>> PBBSMap{ get; set; } 
    
    //to hide/show the main tabs in the component.
    public  List<String> HiddenHomeTabsList{get;set;}
    
    //to hide/show the subtabs in the component.
    public  List<String> HiddenSubTabsList{get;set;}
    
    //to display all Service Areas,Function and Tasks for PBBscenario for a country.
    public List<PBB_Services.ServiceAreaWrapper> SAWrapperList{get;set;}
    
    //Service Tasks Ids selected for a country.    
    public String  selectedServicetasks{get;set;}
    
    //Country selected to map the service task.    
    public String  CountrySelect{get;set;}    
    
    
    //Constructor
    public PBB_CountryServicesController() { 
        
        //get PBB Scenario ID from URL
        PBBScenarioID= ApexPages.currentPage().getParameters().get('PBBScenarioID');
        if(PBBScenarioID!=null && PBBScenarioID!=''){
            PBBScenario = PBB_DataAccessor.getScenarioByID(PBBScenarioID);
            //get the picklists ready for the PBB Scenario countries selection
            CountrySelect = PROJECT_LEVEL;
            getAvailableandSelectedCountries();
            if( PBBScenario.Bid_Project__r.Business_Segment__c!=null){ 
            getAllServiceTasks();
        }        
            else{
                SRM_Utils.showErrorMessage('In order to view the list of available services a Business Segment must be selected for this project.');
            }
        } 
        
        //condition to check WR model has not been selected/Saved by the user 
        if(PBBScenario.WR_Model__r.name == null){
            SRM_Utils.showErrorMessage(' Please select a Work Relay Model in order to define the Services for this BID Project.');
        
        }      
        
        HiddenHomeTabsList = new List<String>();
        HiddenSubTabsList = new List<String>();
        //HiddenSubTabsList.add('SDpage');  
        
    }
    
    
    /** 
@Author Bhargav Devaram
@Date 2014
@Description this method display the countries available and selected for PBB Scenario
*/
    public void getAvailableandSelectedCountries() {
        
        //Selected countries for the PBB Scenario 
        oldSelectedSet = new Set<String>();
        selectedCountriesList = new List<SelectOption>(); 
        for(PBB_Scenario_Country__c  c:PBB_DataAccessor.GetSelectedCountries(PBBScenarioID)){
            //skip the country selected for cloning as they might remove it.
            if(CountrySelect !=c.Country__r.name){
                selectedCountriesList.add(new SelectOption(c.Country__r.name, c.Country__r.name));
                selectedCountriesList.sort();
                
            }
            oldSelectedSet.add(c.Country__r.name);
        }
        
        //check if no countries in the list and default to project level.
        if(!oldSelectedSet.contains(CountrySelect)){
            CountrySelect =PROJECT_LEVEL;
            
        }
        
        //Available countries for the PBB Scenario 
        availableCountriesList = new List<SelectOption>(); 
        for(Country__c c:PBB_DataAccessor.getCountriesListbyNot(oldSelectedSet)){
            availableCountriesList.add(new SelectOption(c.Name, c.Name)); 
            availableCountriesList.sort();
            
        }    
    }
    
    
    /** 
@Author Bhargav Devaram
@Date 2014
@Description this method saves the selection of countries for PBB Scenario
*/
    public void saveSelectedCountries() { 
        PBB_Services.savePBBSelectedCountries(selectedCountriesList,oldSelectedSet,PBBScenarioID,CountrySelect);         
        getAllServiceTasks();
    }
    
    
    /** 
@Author Bhargav Devaram
@Date 2014
@Description get all Service Areas,Function and Tasks for PBBscenario for a country
*/
    
    public void getAllServiceTasks() {
        
        getAvailableandSelectedCountries();
        //get all Service Areas,Function and Tasks for PBBscenario for a country.
        system.debug('----countries ---'+CountrySelect);
        SAWrapperList = new List<PBB_Services.ServiceAreaWrapper>();
        Map<String,PBB_Services.ServiceAreaWrapper> SAWrapper = new Map<String,PBB_Services.ServiceAreaWrapper>();  
        SAWrapper = PBB_Services.getServicetaskforSelectedCountry(PBBScenarioID,CountrySelect); 
        SAWrapperList.addall(SAWrapper.values());
        system.debug('----SAWrapperList---'+SAWrapperList);
    } 
    
    
    /** 
@Author Bhargav Devaram
@Date 2014
@Description maps the slected tasks for the country for PBB Scenario.
*/
    public void save() {
        //Deserializing the list from the UI(used this bcoz the JQuery datatable is behaving weird             
        Set<String> TaskIdsSet = (Set<String>)JSON.deserialize(selectedServicetasks, Set<String>.class);   
        //system.debug('------TaskIdsSet--------------'+TaskIdsSet);
        Map<String,String> TaskIdsMap = new Map<String,String>();
        for(String s:TaskIdsSet){
            String[] task = s.split('=');
            //system.debug('------task--------------'+task[0]+task[1]);
            TaskIdsMap.put(task[0], task[1]);
        }
        system.debug('----TaskIdsMap ---'+TaskIdsMap+'--TaskIdsSet-'+TaskIdsSet);
        system.debug('----countries ---'+CountrySelect);
        PBB_Services.saveServicetaskforSelectedCountry(PBBScenarioID,CountrySelect,TaskIdsMap);
        getAllServiceTasks();         
    }
    
    
    /** 
@Author Bhargav Devaram
@Date 2014
@Description get the countries list for PBB Scenario
*/      
    public List<SelectOption> getCountriesSelectedforPBBS() {
        
        List<SelectOption> options = new List<SelectOption>(); 
        //we even add the non country selection           
        options.add(new SelectOption(PROJECT_LEVEL,PROJECT_LEVEL));
        
        List<SelectOption> options1 = new List<SelectOption>();
        
        //to avoid deletions if the same country is selected for clone mappings.
        if(CountrySelect!= (PROJECT_LEVEL))
            options1.add(new SelectOption(CountrySelect,CountrySelect));
        
        //Selected countries for the PBB Scenario
        if(selectedCountriesList!=null )
            options1.addall(selectedCountriesList);
        options1.sort();
        options.addall(options1); 
        
        return options;
    }    
    
    /** 
@Author Bhargav Devaram
@Date 2014
@Description get the types of service tasks
*/
    public List<selectOption> getServiceTaskType(){
        List<SelectOption> options = new List<SelectOption>(); 
        options.add(new SelectOption('PRA','PRA'));
        options.add(new SelectOption('Sponsor','Sponsor'));
        options.add(new SelectOption('Vendor','Vendor'));        
        return options;
    } 
}