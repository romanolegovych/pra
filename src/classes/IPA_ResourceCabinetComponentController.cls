public class IPA_ResourceCabinetComponentController
{
    public IPA_Page_Widget__c pageWidgetObj {get; set;}
    public List<IPA_Links__c> getLstMediaLibraries() 
    {
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        if(pageWidgetObj != null)
            return ipa_bo.returnMediaLibraries(pageWidgetObj);
        else
            return new List<IPA_Links__c>();
    }
}