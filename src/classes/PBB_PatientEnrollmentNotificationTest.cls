@isTest
private class PBB_PatientEnrollmentNotificationTest{
    static testMethod void testGetScenarioByID(){
        PBB_TestUtils tu = new PBB_TestUtils();
        tu.wfmp  = tu.createWfmProject();
        tu.protocol = tu.createProtocol();
        tu.site = tu.createSite(); 
        tu.notificationCatalogProto = tu.createnotifiCatalogProtocol();
        tu.notifiRegistratn =tu.createNotificationRegistration();
      
        PBB_PatientEnrollmentNotification pen = new PBB_PatientEnrollmentNotification();
        pen.getNotificationSubject(tu.notificationCatalogProto.id,String.valueOf(tu.protocol.id));
        pen.getNotificationEmailBody(tu.notificationCatalogProto.id,String.valueOf(tu.protocol.id));
        pen.didNotificationTopicOccur(tu.notificationCatalogProto.id,String.valueOf(tu.protocol.id));
        
        pen.getNotificationSubject(tu.notificationCatalogProto.id,String.valueOf(tu.protocol.id));
        pen.getNotificationEmailBody(tu.notificationCatalogProto.id,String.valueOf(tu.protocol.id));
        pen.didNotificationTopicOccur(tu.notificationCatalogProto.id,String.valueOf(tu.protocol.id));
     }
}