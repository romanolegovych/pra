/**
 * @description	Implements the test for the functions of the NCM_PendingAcknowledgeSchedule class
 * @author		Kalaiyarasan Karunanithi
 * @version		Created: 04-Sep-2015
 */
@isTest
private class NCM_PendingAcknowledgeScheduleTest {
	
	/**
	 * @description	Test the function scheduleMe.
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 04-Sep-2015
	 */
	static testMethod void scheduleMeTest() {
		Test.startTest();
		NCM_PendingAcknowledgeSchedule.scheduleMe();
		Test.stopTest();
	}
}