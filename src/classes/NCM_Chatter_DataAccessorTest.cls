/**
 * @description	Implements the test for the functions of the NCM_Chatter_DataAccessor class
 * @author		Dimitrios Sgourdos
 * @version		Created: 09-Sep-2015
 */
@isTest
private class NCM_Chatter_DataAccessorTest {
	
	/**
	 * @description	Test the function getSubscriptionByFeedAndUser
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sep-2015
	 */
	static testMethod void getSubscriptionByFeedAndUserTest() {
		// Create data
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		String errorMessage = 'Error in retrieving subscription by feed and user';
		
		// Check the function with the user not subscribed to the created account
		List<EntitySubscription> results = NCM_Chatter_DataAccessor.getSubscriptionByFeedAndUser(acc.Id, testUser.Id);
		system.assertEquals(0, results.size(), errorMessage);
		
		// Check the function with the user subscribed to the created account
		EntitySubscription subscr = new EntitySubscription(parentId = acc.Id, subscriberId = testUser.Id);
		insert subscr;
		
		results = NCM_Chatter_DataAccessor.getSubscriptionByFeedAndUser(acc.Id, testUser.Id);
		system.assertEquals(1, results.size(), errorMessage);
	}
	
	
	/**
	 * @description	Test the function getSubscriptionsByFeedSetAndUserSet
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sep-2015
	 */
	static testMethod void getSubscriptionsByFeedSetAndUserSetTest() {
		// Created Data
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'C', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		List<User> userList = NCM_Utils.buildTestUserList(2);
		
		List<EntitySubscription> subscriptions = new List<EntitySubscription>();
		subscriptions.add( new EntitySubscription(parentId = accList[0].Id, subscriberId = userList[0].Id) );
		subscriptions.add( new EntitySubscription(parentId = accList[0].Id, subscriberId = userList[1].Id) );
		subscriptions.add( new EntitySubscription(parentId = accList[1].Id, subscriberId = userList[0].Id) );
		subscriptions.add( new EntitySubscription(parentId = accList[2].Id, subscriberId = userList[1].Id) );
		insert subscriptions;
		
		String errorMessage = 'Error in retrieving subscriptions by feeds and users';
		
		// Check the function with valid parameters
		Set<Id> feedIds = new Set<Id>{accList[0].Id, accList[1].Id};
		Set<Id> userIds = new Set<Id>{userList[0].Id};
		
		List<EntitySubscription> results = NCM_Chatter_DataAccessor.getSubscriptionsByFeedSetAndUserSet(feedIds,
																										userIds);
		
		system.assertEquals(2, results.size(), errorMessage);
		
		Set<Id> resultsIds = new Set<Id>{subscriptions[0].Id, subscriptions[2].Id};
		
		system.assertEquals(true, resultsIds.contains(results[0].Id), errorMessage);
		system.assertEquals(true, resultsIds.contains(results[1].Id), errorMessage);
	}
	
	
	
	/**
	 * @description	Test the function getFeedItemsByParentFeed
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sep-2015
	 */
	static testMethod void getFeedItemsByParentFeedTest() {
		// Create data
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		FeedItem source = new FeedItem(parentId = acc.Id, Body = 'Test Feed');
		insert source;
		
		String errorMessage = 'Error in getting feed items';
		
		// Check the function
		List<feedItem> results = NCM_Chatter_DataAccessor.getFeedItemsByParentFeed(acc.Id);
		
		system.assertEquals(1, results.size(), errorMessage);
		system.assertEquals(source.Id, results[0].Id, errorMessage);
		system.assertEquals('Test Feed', results[0].Body, errorMessage);
		system.assertEquals(acc.Id, results[0].parentId, errorMessage);
	}
	
	
	/**
	 * @description	Test the function getFeedItemsByParentFeedAndRelatedTopic
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sep-2015
	 */
	static testMethod void getFeedItemsByParentFeedAndRelatedTopicTest() {
		// Create data
		Account acc = new Account(Name = 'A',BillingCountry='NL', BillingCity='Groningen');
		insert acc;
		
		List<FeedItem> source = new List<FeedItem>();
		source.add( new FeedItem(parentId = acc.Id, Body = '#TestTopic Test Feed A') );
		source.add( new FeedItem(parentId = acc.Id, Body = '#TestTopic Test Feed B') );
		source.add( new FeedItem(parentId = acc.Id, Body = 'Test Feed B') );
		insert source;
		
		Set<Id> feedIds = new Set<Id>{source[0].Id, source[1].Id};
		
		String errorMessage = 'Error in getting feed items';
		
		// Check the function
		List<FeedItem> results = NCM_Chatter_DataAccessor.getFeedItemsByParentFeedAndRelatedTopic(	acc.Id,
																								'Not a present topic');
		system.assertEquals(0, results.size(), errorMessage);
		
		results = NCM_Chatter_DataAccessor.getFeedItemsByParentFeedAndRelatedTopic(acc.Id, 'TestTopic');
		system.assertEquals(2, results.size(), errorMessage);
		system.assertEquals(true, feedIds.contains(results[0].Id), errorMessage);
		system.assertEquals(true, feedIds.contains(results[1].Id), errorMessage);
	}
}