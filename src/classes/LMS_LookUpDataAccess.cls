public with sharing class LMS_LookUpDataAccess {

    public LMS_LookUpDataAccess(){
        
    }
   
    public static List<SelectOption> getRoles(Boolean bAssign, Boolean bBlank){
        string strSQL = '';
        transient set<String> setItems=new set<string>();
        
        transient List<SelectOption> options = new List<SelectOption>(); 
        if (bBlank)       
            options.add(new SelectOption('',''));
        if (bAssign)
            strSQL = 'select name from  Job_Class_Desc__c where Is_Assign__c = true order by name asc';
        else 
            strSQL = 'select name from  Job_Class_Desc__c order by name asc';
        for(Job_Class_Desc__c temp:database.query(strSQL))
            if(!setItems.contains(temp.name)){              
                options.add(new SelectOption(temp.name,temp.name));
               setItems.add(temp.name);
            }
   
       
        return options; 
    }
    public static List<SelectOption> getClinicalRoles(Boolean bClinical, Boolean bBlank){
        string strSQL = '';
        transient set<String> setItems=new set<string>();
        
        transient List<SelectOption> options = new List<SelectOption>(); 
        if (bBlank)       
            options.add(new SelectOption('',''));
        if (bClinical)
            strSQL = 'select name from  Job_Class_Desc__c where IsClinical__c=true order by name asc';
        else 
            strSQL = 'select name from  Job_Class_Desc__c order by name asc';
        for(Job_Class_Desc__c temp:database.query(strSQL))
            if(!setItems.contains(temp.name)){              
                options.add(new SelectOption(temp.name,temp.name));
               setItems.add(temp.name);
            }
   
       
        return options; 
    }
    public static List<selectoption> getAllPSTRoles(boolean bBlank) {
    	string sqlPST = '';
    	string sqlPSTAdhoc = '';
        transient set<String> setItems=new set<string>();
        transient List<String> listItems=new List<String>();
        
        transient List<SelectOption> options = new List<SelectOption>(); 
        if (bBlank)       
            options.add(new SelectOption('',''));
        sqlPST = 'select name from  Job_Class_Desc__c where Is_Assign__c=true order by name asc';
        sqlPSTAdhoc = 'select PST_Adhoc_Role__c from PST_Adhoc_Role__c order by PST_Adhoc_Role__c asc';
        for(Job_Class_Desc__c temp:database.query(sqlPST)) {
            if(!setItems.contains(temp.name)) {
               setItems.add(temp.name);
               listItems.add(temp.Name);
            }
        }
        for(PST_Adhoc_Role__c temp : database.query(sqlPSTAdhoc)) {
        	if(!setItems.contains(temp.PST_Adhoc_Role__c)) {
               setItems.add(temp.PST_Adhoc_Role__c);
               listItems.add(temp.PST_Adhoc_Role__c);
            }
        }
        listItems.sort();
        for(String s : listItems) {
        	options.add(new SelectOption(s,s));
        }
   
       
        return options; 
    }
    public static List<selectoption> getTitles(boolean bBlank){
        transient set<string> setItems = new set<string>();
        transient list<selectoption> options = new list<selectoption>();
        
        if(bBlank)
            options.add(new selectoption('',''));
        for(Job_Title__c temp : [select Job_Title__c from Job_Title__c where Status__c = 'A' order by Job_Title__c asc])
        if(!setItems.contains(temp.Job_Title__c)){
            options.add(new selectoption(temp.Job_Title__c,temp.Job_Title__c));
            setItems.add(temp.Job_Title__c);
        }
        
        return options;
    }
    public static List<selectoption> getBusinessUnits(boolean bBlank){
        transient set<string> setItems = new set<string>();
        transient list<selectoption> options = new list<selectoption>();
        
        if(bBlank)
            options.add(new selectoption('',''));
        for(PRA_Business_Unit__c temp : [select Name from PRA_Business_Unit__c where Status__c = 'A' order by name asc])
        if(!setItems.contains(temp.name)){
            options.add(new selectoption(temp.name,temp.name));
            setItems.add(temp.name);
        }
        
        return options;
    }
    public static List<selectoption> getDepartments(boolean bBlank){
        transient set<string> setItems = new set<string>();
        transient list<selectoption> options = new list<selectoption>();
        
        if(bBlank)
            options.add(new selectoption('',''));
        for(Department__c temp : [select name from Department__c where Status__c = 'A' order by name asc])
        if(!setItems.contains(temp.name)){
            options.add(new selectoption(temp.name,temp.name));
            setItems.add(temp.name);
        }
        
        return options;
    }
    public static List<selectoption> getStatusGroups(boolean bBlank){
        transient set<string> setItems = new set<string>();
        transient list<selectoption> options = new list<selectoption>();
        
        if(bBlank)
            options.add(new selectoption('',''));
        for(Employee_Type__c temp : [select name from Employee_Type__c order by name asc])
        if(!setItems.contains(temp.name)){
            options.add(new selectoption(temp.name,temp.name));
            setItems.add(temp.name);
        }
        
        return options;
    }
    public static List<SelectOption> getCountry(Boolean bBlank){ 
        transient set<String> setItems=new set<string>();
       
         transient List<SelectOption> options = new List<SelectOption>();
         
            if (bBlank)       
                options.add(new SelectOption('',''));
            for(Country__c temp:[select name from Country__c order by name asc])
            if(!setItems.contains(temp.name)){
               options.add(new SelectOption(temp.name,temp.name));
               setItems.add(temp.name);
            } 
        return options;
    }
    public static List<SelectOption> getRegion(Boolean bBlank){ 
        transient set<String> setItems=new set<string>();
        transient List<SelectOption> options = new List<SelectOption>();
            if (bBlank)       
                options.add(new SelectOption('',''));
            for(Region__c temp:[select Region_Name__c from Region__c where Status__c = 'A' order by Region_Name__c asc])
            if(!setItems.contains(temp.Region_Name__c)){
               options.add(new SelectOption(temp.Region_Name__c,temp.Region_Name__c));
               setItems.add(temp.Region_Name__c);
            } 
        return options;
    }
}