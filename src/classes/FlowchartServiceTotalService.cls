/** Implements the Service Layer of the object FlowchartServiceTotal__c
 * @author	Dimitrios Sgourdos
 * @version	03-Feb-2014
 */
public with sharing class FlowchartServiceTotalService {
	
	/** Deserialize a string to the ExplanationJSONWrapper sub-class.
	 * @author	Dimitrios Sgourdos
	 * @version 03-Feb-2014
	 * @return	The created sub-class.
	 */
	public static List<ExplanationJSONWrapper> deserializeFromStringToExplanationJSONWrapperList(String jsonValue) {
		List<ExplanationJSONWrapper> results = new List<ExplanationJSONWrapper>();
		
		if( String.isNotBlank(jsonValue) ) {
			try {
				results = (List<ExplanationJSONWrapper>) JSON.deserialize(jsonValue, List<ExplanationJSONWrapper>.class);
			} catch (Exception e) {
				results = new List<ExplanationJSONWrapper>();
			}
		}
		
		return results;
	}
	
	
	/**	This class keeps a calculation for the number of units field of StudyService object.
	 * @author	Dimitrios Sgourdos
	 * @version	03-Feb-2014
	 */
	public class ExplanationJSONWrapper {
		public string populationName;
		public string groupname;
		public decimal groupsize;
		public string designname;
		public string armname;
		public string flowchartname;
		public string epochname;
		public decimal timepoints;
		public decimal totalunits;
	}
}