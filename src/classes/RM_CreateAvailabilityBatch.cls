global class RM_CreateAvailabilityBatch extends PRA_BatchaQueueable{
	global String parameter;
	global RM_CreateAvailabilityBatch() {
        
    }

    //Set the parameters over the variables
    global override void  setBatchParameters(String parametersJSON){
        List<String> params = (List<String>) Json.deserialize(parametersJSON, List<String>.class);
        parameter = params.get(0);    
     }
    
    global override Database.QueryLocator start(Database.BatchableContext BC) {
    	
	    
	    String query = 'SELECT id FROM Employee_details__c WHERE  status_desc__c!= \'Terminated\'';  
	    
	    
	  	system.debug('---------query---------'+query  ); 
        return Database.getQueryLocator(query);
    }
    
    global override void execute(Database.BatchableContext BC, List<sObject> scope) {
    	string endMonth = RM_Tools.GetYearMonth(Integer.ValueOf(parameter));
	    system.debug('---------endMonth---------'+endMonth  );
	    list<string> lstEmpl = new list<string>();
	    for(Sobject s : scope) {
	    	system.debug('---------s---------'+s  );
		      if(s instanceof Employee_details__c) {
		        Employee_details__c a = (Employee_details__c)s;
		        	lstEmpl.add(a.ID);
		      }
	    }
	    map<string, string> mapAva = new map<string, string>();
    	list<string> lstNeedMore = new list<string>();
	    
	    for (AggregateResult ar :  [SELECT Employee_ID__c, max(year_month__c) lastAvMonth FROM WFM_Employee_Availability__c 
	    WHERE  employee_id__r.status_desc__c != 'Terminated' and employee_id__c in :lstEmpl
	    group by employee_id__c having max(Year_Month__c) < :endMonth ]){
	    	mapAva.put((string)ar.get('Employee_ID__c'), (string)ar.get('lastAvMonth'));
	    	lstNeedMore.add((string)ar.get('Employee_ID__c'));
	    }
	    system.debug('---------mapAva---------'+mapAva  );
	    if(mapAva != null && mapAva.size() > 0) {
	     	RM_AssignmentsService.InsertAvailabilityByEmployees(lstNeedMore, Integer.valueOf(parameter));
	    }
    }
    
    global override void finish(Database.BatchableContext BC) {
    
    }
}