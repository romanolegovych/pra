/** 
@Author Guo Grace 
@Date 04/15/2015
@Description this class is inserting the Service Impact Response.
*/
public with sharing class PBB_ServiceImpactResponsesController {

    public  Boolean                    saveDefault          {get;set;}
    public  ID                         sirID                {get;set;}                 //to get the Service Impact Response ID
    public  Service_Impact_Response__c sir                  {get;set;}                 //to get the Service Impact Response Sobject for edit or new   
      
     public PBB_ServiceImpactResponsesController(ApexPages.StandardController controller){        
        //check if the sir is for edit
        sir = (Service_Impact_Response__c)controller.getRecord();
        sirID = ApexPages.currentPage().getParameters().get('id');        
        string clone =ApexPages.currentPage().getParameters().get('clone');          
        //check if record exists or not
        if(sirID !=null){      
            sir  = PBB_DataAccessor.getSIResponse(sirID); 
            //Cloning Service Impact Response
            if(clone !=null){
                sir  =sir.clone(false, true);
            }                       
        }
        else{         
            sir.Default__c=false;   
        }
    }  
    
    /** 
    @Author Guo Grace 
    @Date 04/15/2015
    @Description this class is inserting the Service Impact Question.
    */
    public PageReference saveSIRQuestions(){        
        PageReference pageRef =null;           
        
        try{ 
            List<Service_Impact_Response__c > SIRListDefault = PBB_DataAccessor.getSIAllResponse(sir.Service_Impact_Questions__c);
            if(SIRListDefault.size() != 0){               
                if (SIRListDefault[0].Service_Impact_Questions__r.Status__c == 'Approved' || SIRListDefault[0].Service_Impact_Questions__r.Status__c == 'Retired' )   {     
                        System.debug('--Question Status--'+SIRListDefault[0].Service_Impact_Questions__r.Status__c);                                
                        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'You cannot edit or create response once question is Approved or Retired.');
                        ApexPages.addMessage(myMsg);
                        return pageRef;  
                }   
            } 
           
            // Save the current response    
            upsert sir ; 
            System.debug('--sir-'+sir);  
            // Get the all responses for the same question to make sure only one default response             
            // If current response as default, make make sure only one default response
            if(sir.Default__c == true){            
                for(Service_Impact_Response__c curr : SIRListDefault){                    
                    //Only insert once for all the response
                    if(curr.id != sir.id && curr.Default__c == true ){                    
                        curr.Default__c = false;
                        upsert curr;
                    }
                }   
            }            
            pageRef = new PageReference('/'+sir.id);
            pageRef.setRedirect(true);                 
         }
        catch(Exception e){
            ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0) );
            ApexPages.addMessage(msg);        
        } 
        return pageRef;  
    }
}