/**
*   This class contains variables and methods to help initialize unit test data
*/
@isTest
public class PRA_TestUtils {
    public Account client;
    public Client_Project__c clientProject;
    public Operational_Area__c operationalArea;
    public Task_Group__c taskGroup;
    public Business_Unit__c businessUnit;
    public Map<String, Country__c> countries = new Map<String, Country__c>();
    public Map<String, Project_Client_Region__c> projectRegions = new Map<String, Project_Client_Region__c>();
    public Client_Task__c clientTask;
    public List<Client_Task__c> clientTasks = new List<Client_Task__c> ();
    public List<BUF_Code__c> bufCodes = new List<BUF_Code__c>();
    public List<Unit_Effort__c> unitEfforts = new List<Unit_Effort__c>();
    public List<Hour_EffortRatio__c> effortRations = new List<Hour_EffortRatio__c>();
    public List<Regional_Contract_Value__c> regionalContractValues = new List<Regional_Contract_Value__c> ();
    public WBS_Project__c wbsProject;
    public WBS_Company__c wbsCompany;
    public Region__c region;
    
    public void initAll() {
        initClientSetUp();
        initUnitSetUp(clientTasks);
        initCostSetUp(clientTasks);
        
        effortRations = [Select Id, Unit_Effort__r.Client_Task__c, BUF_Code__c, Baseline_ER__c, Forecast_ER__c, Last_Approved_ER__c, Worked_ER__c, 
                        Unit_Effort__r.Month_Applies_To__c, Unit_Effort__r.Month_Forecast_Created__c
                        from Hour_EffortRatio__c
                        where Unit_Effort__r.Client_Task__c in :clientTasks];
    }
    
    public void initClientSetUp() {
        Date curentDate = System.today();
        insert getAccount('Acme Inc.');
        insert getClientProject(client.id, 'Human Canonball', ' HC', curentDate.addMonths(-6), curentDate.addMonths(6));        
        insert getRegion();
        insert getWbsCompany();
        insert getWbsProject();
        insert getOperationalArea('Therapeutic Expertise', 'HC');
        insert getTaskGroup('Wild coyote', operationalArea.id, 'WC');
        insert getBusinessUnit('Road runner hunters', 'RRH');
        insert getCountries(businessUnit.id).values();
        insert getProjectRegions().values();
        insert getClientTask(taskGroup.id, clientProject.id, 'Rocket power', 'Rocket power rolls',  curentDate.addMonths(-4), curentDate.addMonths(4),
                            countries.get('USA').id, projectRegions.get('NA').id);
        insert getClientTask(taskGroup.id, clientProject.id, 'Rockets powers', 'Rockets powers rolls',  curentDate.addMonths(-4), curentDate.addMonths(4),
                            countries.get('PL').id, projectRegions.get('EMEA').id);  
        clientTasks = [select Id, Description__c, Unit_of_Measurement__c, Total_Units__c from Client_Task__c];
        insert getRegionalContractValues(clientTasks);
    }
    
    /*** Added by : Andrew Allen ***/
    public void initUnitSetUp(List<Client_Task__c> taskList) {
        List<Unit_Effort__c> unitEfforts = new List<Unit_Effort__c>();
        List<Client_Task__c> tasks = [select Id, Original_End_Date__c, Start_Date__c, Description__c 
                                                    from Client_Task__c where Id in :taskList];   
        
        // Go through each task and set up the units                                      
        for(Integer i = 0; i < tasks.size(); i++) {
            Date applyTo;
            // Increase the month to apply 12 different unit records for each task
            for(Integer j = 0; j < 12; j++) {
                if(j == 0) {
                    applyTo = tasks.get(j).Start_Date__c.toStartOfMonth();
                } else {
                    applyTo = applyTo.addMonths(1).toStartOfMonth();
                }
                unitEfforts.add(getUnitEfforts(tasks[i].Id, 2.0, applyTo));
            }
        }
        insert unitEfforts;
        System.debug('------------inserting unitEfforts------------------'+unitEfforts.size());
    }
    
    public void initCostSetUp(List<Client_Task__c> taskList) {
        List<Client_Task__c> tasks = [select Id, Original_End_Date__c, Start_Date__c, Description__c 
                                                    from Client_Task__c where Id in :taskList];
        List<Unit_Effort__c> unitEfforts = [select Id, Client_Task__r.Start_Date__c from Unit_Effort__c where Client_Task__c IN :tasks];
            
        // create 2 buf codes for each task, with one extra
        insert getBufCodes(3);
        
        // create effort ration between each crct and buf code
        for (Integer i = 0; i < unitEfforts.size(); i++) {
            //Values are spreaded by bid trigger which needs only initial values
            if(i < 3) {
                effortRations.add(getEffortRatio(unitEfforts[i].Id, bufCodes.get(0).Id, 1, true));
                effortRations.add(getEffortRatio(unitEfforts[i].Id, bufCodes.get(1).Id, 1, true));
            } else {
                effortRations.add(getEffortRatio(unitEfforts[i].Id, bufCodes.get(0).Id, 1, false));
                effortRations.add(getEffortRatio(unitEfforts[i].Id, bufCodes.get(1).Id, 1, false));
            }
        }
        insert effortRations;
        
        createBidContractDetails();
        
        System.debug('--------------clientTasks-----------------'+clientTasks);
        System.debug('--------------clientTasks size-----------------'+clientTasks.size());
        System.debug('--------------Inserting effortRatios-----------------'+effortRations.size());
    }
    
    public Account getAccount(String clientName) {
        client = new Account(name = clientName);
        return client;
    }
    
    public Client_Project__c getClientProject(Id accountId, String projName, String praProjectId, Date startDate, Date endDate) {
        clientProject = new Client_Project__c(Client__c = accountId, name=projName, PRA_project_id__c = praProjectId);
        if(startDate != null) {
            clientProject.Contracted_Start_Date__c = startDate;
        }
        if(endDate != null) {
            clientProject.Contracted_End_Date__c = endDate;
            clientProject.estimated_end_date__c = endDate;
        }
        clientProject.Currency_exchange_lock_date__c=date.today();
        return clientProject;
    }
    
    public Region__c getRegion() {
        region = new Region__c(Region_Id__c=1, Region_Name__c='US/Canada', Status__c='A');
        return region;  
    }
    
    public WBS_Company__c getWbsCompany() {
        WbsCompany = new WBS_Company__c(Name= clientProject.id+'100', Description__c= clientProject.id+'100Company', Division__c= 'Product Registration',
                                        Global_Region__c = region.id);
        
        return WbsCompany ;      
    }
    
    public WBS_Project__c getWbsProject() {
        wbsProject = new WBS_Project__c(Client_Project__c = clientProject.id, Activity_Group__c = 'EXELIXIS', Company_number__c = WbsCompany.id,
                                        Contract_Level_Address__c = 17, Project_Level_Address__c = 1, Invoice_Method__c = 'UNIT');
        
        return wbsProject;      
    }

    public Operational_Area__c getOperationalArea (String oaName, String praOperationalAreaId) {
        operationalArea = new Operational_Area__c(name=oaName, PRA_Operational_Area_Id__c=praOperationalAreaId);
        return operationalArea;
    }
    
    public Task_Group__c getTaskGroup(String taskGroupName, Id operationalAreaId, String praTaskGroupId) {
        taskGroup = new Task_Group__c(name=taskGroupName, Operational_Area__c=operationalAreaId, PRA_Task_Group_Id__c=praTaskGroupId);
        return taskGroup;
    }
    
    public Task_Group__c getTaskGroup(String taskGroupName, Id operationalAreaId, String praTaskGroupId, String activityCode,
                                      String lawsonShortDesc, String lawson30Desc) {
        taskGroup = new Task_Group__c(name=taskGroupName, Operational_Area__c=operationalAreaId, PRA_Task_Group_Id__c=praTaskGroupId,
                                      Activity_Code__c=activityCode, Lawson_Short_Desc__c=lawsonShortDesc,
                                      Lawson_30_Char_Desc__c=lawson30Desc);
        return taskGroup;
    }
    
    public Business_Unit__c getBusinessUnit(String buName, String praBusinessUnitId) {
        businessUnit = new Business_Unit__c (name=buName, PRA_Business_Unit_Id__c=praBusinessUnitId);
        return businessUnit;
    }
    
    public Country__c getCountry (String countryName, String praCountryId, String businessUnitId) {
        Country__c country = new Country__c (name=countryName, PRA_Country_Id__c=praCountryId);
        if (businessUnitId!=null && businessUnitId!='') {
            country.Default_Business_Unit__c = businessUnitId;
        }
        return country;
    }
    
    public Map<String, Country__c> getCountries(String businessUnitId) {
        countries.put('PL', getCountry('Poland', 'PL', businessUnitId));
        countries.put('UK', getCountry('United Kingdom', 'UK', businessUnitId));
        countries.put('USA', getCountry('USA', 'USA', businessUnitId));
        countries.put('ID', getCountry('India', 'ID', businessUnitId));
        return countries;
    }
    
    public Project_Client_Region__c getProjectClientRegion(String projectClientRegionName) {
        Project_Client_Region__c pcr = new Project_Client_Region__c(name = projectClientRegionName);
        return pcr;
    }
    
    public Map<String, Project_Client_Region__c> getProjectRegions() {
        projectRegions.put('EMEA', getProjectClientRegion('EMEA'));
        projectRegions.put('NA', getProjectClientRegion('NA'));
        projectRegions.put('APAC', getProjectClientRegion('APAC'));
        projectRegions.put('WE', getProjectClientRegion('WE'));
        return projectRegions;
    }
    
    public Client_Task__c getClientTask(Id taskGroupId, Id projectId, String praClientTaskId, String desciption, Date startDate, Date endDate, Id countryId, 
        Id projectRegionId) {
        clientTask = new Client_Task__c(Task_Group__c=taskGroupId, Project__c=projectId, PRA_Client_Task_Id__c=praClientTaskId, Description__c=desciption,
                                            start_date__c=startDate, original_end_date__c=endDate, Country__c = countryId, 
                                            Project_Region__c = projectRegionId, Total_Contract_Units__c = 1000.00,
                                            Total_Contract_Hours__c = 10000.00, Contract_Value__c = 10000.00, CVPU_Asia_Pacific__c = .25,
                                            CVPU_Europe_Africa__c = .25, CVPU_Latin_America__c = .25, CVPU_US_Canada__c = .25);
        clientTask.Forecast_Curve__c = 'Project Management';
        return clientTask;
    }
    
    // BCD Inserts for NEW schema only
    public Bid_Contract_Detail__c getBidContractDetail(Id clientTaskId, Id bufCode, Decimal contractedHours, Decimal contractValue) {
        Bid_Contract_Detail__c bidDetail = new Bid_Contract_Detail__c(Client_Task__c = clientTaskId, BUF_Code__c = bufCode, 
                                                                        Contracted_Hours__c = contractedHours, Contracted_Value__c = contractValue);
        return bidDetail;
    }
    
    public BUF_Code__c getBufCode(String name, String businessUnit, String functionalCode, String praRegion) {
        BUF_Code__c bufCode = new BUF_Code__c(Name = name, Business_Unit__c = businessUnit, Function_Code__c = functionalCode, PRA_Region__c = praRegion);
        return bufCode;
    }  
    
    public Unit_Effort__c getUnitEfforts(String taskId, Decimal amountToApply, Date monthAppliesTo) {
        Unit_Effort__c unitEffort = new Unit_Effort__c(Client_Task__c = taskId, Forecast_Unit__c = amountToApply, Baseline_Unit__c = amountToApply,
                                        Worked_Unit__c = amountToApply, Last_Approved_Unit__c = amountToApply, Forecast_Effort__c = amountToApply, 
                                        Baseline_Effort__c = amountToApply, Last_Approved_Effort__c = amountToApply, Month_Applies_To__c = monthAppliesTo, 
                                        Month_Forecast_Created__c = Date.today().toStartOfMonth(), Unit_Effort_Id__c = taskId + '-' + monthAppliesTo,
                                        LA_Total_Contracted_BDG_Hours__c = 20.0, Total_Contracted_BDG_Cost__c = 1000.0, Total_Forecast_Cost__c = 1000.0,
                                        Contracted_BDG_Unit__c = 20.0, Total_Capped_Cost__c = 1000.0);
        return unitEffort;
    }
    
    public Hour_EffortRatio__c getEffortRatio(String unitEffortId, String bufCode, Decimal percentOfEffort, Boolean createWorked) {
        Hour_EffortRatio__c effortRatio = new Hour_EffortRatio__c(Unit_Effort__c = unitEffortId, BUF_Code__c = bufCode, Baseline_ER__c = percentOfEffort,
                                        Forecast_ER__c = percentOfEffort, Last_Approved_ER__c = percentOfEffort, Hour_ER_Id__c = unitEffortId + '-' + bufCode,
                                        Cost_Rate__c = 5.0, Exchange_Rate__c = .5);
        if(createWorked) {
            effortRatio.Worked_Hours__c = 10.0;
        }
        return effortRatio;
    }
    
    public List<BUF_Code__c> getBufCodes (Integer noOfBufCodes) {
        List<String> praRegions = new List<String>{'Asia Pacific', 'Europe Africa', 'Latin America', 'US Canada'};
        
        for (Integer i=0; i<noOfBufCodes; i++) {
            bufCodes.add(getBufCode('BudCode-'+i, null, null, praRegions.get(math.mod(i, 4))));
        }
        return bufCodes;
    }    
    
    // RCV Inserts for NEW schema only
    public Regional_Contract_Value__c getRegionalContractValue(Id taskId, Date effectiveDate, Decimal usCanada, Decimal europeAfrica, Decimal latin,
                                                                Decimal asiaPacific) {
        Regional_Contract_Value__c retVal = 
            new Regional_Contract_Value__c(Client_Task__c = taskId, Effective_Date__c = effectiveDate, 
                US_Canada__c = usCanada, Europe_Africa__c = europeAfrica, Latin_America__c = latin, Asia_Pacific__c = asiaPacific, 
                Contract_Value_Per_Unit__c = 100);
        
        return retVal;                                                              
    }
    
    // RCV Inserts for NEW schema only
    public List<Regional_Contract_Value__c> getRegionalContractValues (List<Client_Task__c> tasks) {
        regionalContractValues = new List<Regional_Contract_Value__c> ();
        Date currentDate = System.today();
        for(Client_Task__c task : tasks) {
            regionalContractValues.add(getRegionalContractValue(task.Id, currentDate, 2, 2, 2, 2));
        }
        
        return regionalContractValues;
    }
    
    // BCD Inserts for NEW schema only
    public void createBidContractDetails() {
        Map<String, Bid_Contract_Detail__c> inDetails =  new Map<String, Bid_Contract_Detail__c> ();
        List<Hour_EffortRatio__c> hrErs = [select BUF_Code__c, Baseline_ER__c, Unit_Effort__r.Client_Task__r.Total_Units__c,
                                            Unit_Effort__r.Client_Task__r.Contract_Date__c,
                                            Unit_Effort__r.Client_Task__r.Average_Contract_Effort__c,Unit_Effort__r.Client_Task__r.Total_Contract_Hours__c,
                                            Unit_Effort__r.Client_Task__r.Total_Contract_Units__c, Unit_Effort__r.Client_Task__c
                                            from Hour_EffortRatio__c];
        
        for(Hour_EffortRatio__c hrEr : hrErs) {
            String key = hrEr.Unit_Effort__r.Client_Task__c + '-' + hrEr.BUF_Code__c;
            if(!inDetails.containsKey(key)) {
                Bid_Contract_Detail__c bd = new Bid_Contract_Detail__c();
                bd.Client_Task__c = hrER.Unit_Effort__r.Client_Task__c;
                bd.BUF_Code__c = hrER.BUF_Code__c;
                bd.Contracted_Hours__c = hrER.Unit_Effort__r.Client_Task__r.Total_Contract_Hours__c;
                bd.Contracted_Value__c = hrER.Unit_Effort__r.Client_Task__r.Total_Contract_Units__c;
                inDetails.put(key, bd);
            }
        }
        insert inDetails.values();
    }
    
    // method create change order for projects all crcts
    public Change_Order__c createChangeOrder(Id projectId) {
        Change_Order__c retVal = new Change_Order__c(Name='Unit Test Change Order', status__c= 'Active', Client_Project__c = projectId); 
        insert retVal;
        addSelectedTasksToChangeOrder(retVal);
        
        return retVal;
    }
    
    private void addSelectedTasksToChangeOrder(Change_Order__c changeOrder) {
        
        Map<Id, Change_Order_Item__c> mapChangeOrderItems = new Map<Id, Change_Order_Item__c>();
        List<Client_Task__c> tasks = [select  Id, Description__c, Client_Unit_Number__c, Project_Region__r.Name,
                    Project_Region__c, Total_Worked_Units__c, Total_Worked_Hours__c, Forecast_Curve__c, Combo_Code__c, 
                    Start_Date__c, Original_End_Date__c, Task_Group__c, Unit_of_Measurement__c, Contract_Value__c, Total_Contract_Units__c
                from Client_Task__c 
                where  Project__c = :changeOrder.Client_Project__c
                order by Description__c, Project_Region__r.Name];
        for(Client_Task__c task : tasks) {
            Decimal totalUnits = 0;
            Decimal contractCost = 0;
            Decimal unitCost = 0;
            if(task.Total_Contract_Units__c != null) {
                totalUnits += task.Total_Contract_Units__c;
            }
            if(task.Contract_Value__c != null) {
                contractCost += task.Contract_Value__c;
            }
            if(totalUnits != 0 && contractCost != 0) {
                unitCost = contractCost / totalUnits;
            }
            
            Change_Order_Item__c coItem = new Change_Order_Item__c(Change_Order__c = changeOrder.Id, Client_Task__c = task.Id, Type__c = 'Modified');
            coItem.Client_Unit_Number__c    = task.Client_Unit_Number__c;
            coItem.Description__c           = task.Description__c;
            coItem.Project_Region__c        = task.Project_Region__c;
            coItem.Unit_of_Measurement__c   = task.Unit_of_Measurement__c;
            coItem.Combo_Code__c            = task.Combo_Code__c;
            coItem.Start_Date__c            = task.Start_Date__c;
            coItem.End_Date__c              = task.Original_End_Date__c;
            coItem.Forecast_Curve__c        = task.Forecast_Curve__c;
            coItem.Task_Group__c            = task.Task_Group__c;
            coItem.Number_of_Contracted_Units__c = totalUnits;
            coItem.Worked_Units__c          = task.Total_Worked_Units__c;
            coItem.Worked_Hours__c          = task.Total_Worked_Hours__c;
            coItem.Unit_Cost__c             = unitCost;
            coItem.Total_Cost__c            = totalUnits * unitCost;
            coItem.Amendment_CO_Number_of_Units__c = 0;
            
            mapChangeOrderItems.put(task.id, coItem);
        }
        insert mapChangeOrderItems.values();
        
        // Change Order Line Items
        List<Change_Order_Line_Item__c> liCOLineItems = new List<Change_Order_Line_Item__c>();
        
        List<Bid_Contract_Detail__c> liBidCOntractsDetails = [select Id, Client_Task__c, Client_Task__r.Additional_Units_With_No_Contract_Value__c, 
                                                 Client_Task__r.Contract_Value__c, Client_Task__r.Name, 
                                                Client_Task__r.Contracted_Number_of_Units__c, Client_Task__r.Contract_Status__c, Client_Task__r.Average_Contract_Effort__c,
                                                Client_Task__r.Total_Contract_Hours__c, Client_Task__r.Total_Contract_Units__c, BUF_Code__c, 
                                                BUF_Code__r.Name, BUF_Code__r.PRA_BUF_Code_Id__c, BUF_Code__r.PRA_Region__c, Contracted_Hours__c, 
                                                Contracted_Value__c
                                            from Bid_Contract_Detail__c
                                            where Client_Task__r.Project__c = :changeOrder.Client_Project__c];
        
        system.debug('-------------------tuUtil bidContractDetail COLI size-------------------------' + liBidCOntractsDetails.size());
        system.debug('-------------------tuUtil bidContractDetail COLI-------------------------' + liBidCOntractsDetails);
        
        for(Bid_Contract_Detail__c bidDetails : liBidCOntractsDetails) {
            Change_Order_Line_Item__c coLineItem = new Change_Order_Line_Item__c();
            
            coLineItem.Change_Order_Item__c         = mapChangeOrderItems.get(bidDetails.Client_Task__c).Id;
            coLineItem.BUF_Code__c                  = bidDetails.BUF_Code__c;
            coLineItem.Current_Contract_Hours__c    = bidDetails.Contracted_Hours__c;
            coLineItem.Current_Contract_Value__c    = bidDetails.Contracted_Value__c;
            
            if(bidDetails.Contracted_Hours__c != 0 && bidDetails.Client_Task__r.Total_Contract_Units__c != 0) {
                coLineItem.Current_Hours_Unit__c = bidDetails.Contracted_Hours__c / bidDetails.Client_Task__r.Total_Contract_Units__c;
            } else {
                coLineItem.Current_Hours_Unit__c = 0;
            }
            
            if(bidDetails.Contracted_Value__c != 0 && bidDetails.Client_Task__r.Total_Contract_Units__c != 0) {
                coLineItem.Current_Unit_Cost__c = bidDetails.Contracted_Value__c / bidDetails.Client_Task__r.Total_Contract_Units__c;
            } else {
                coLineItem.Current_Unit_Cost__c = 0;
            }
            coLineItem.Current_Total_Cost__c = coLineItem.Current_Unit_Cost__c * bidDetails.Client_Task__r.Total_Contract_Units__c;
            coLineItem.Type__c = 'Modified';
            coLineItem.Is_Labor_unit__c = true;
            system.debug('-------------colineitem-------------'+coLineItem);
            
            liCOLineItems.add(coLineItem);
        }
        insert liCOLineItems;
        system.debug('------------- inserted colineitems -------------' + liCOLineItems.size());
        system.debug('------------- inserted colineitems -------------' + liCOLineItems);
    }
    
}