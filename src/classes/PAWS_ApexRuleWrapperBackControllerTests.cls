/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PAWS_ApexRuleWrapperBackControllerTests {
    
    static final String sourceObjectType = 'paws_project_flow_agreement__c';
    static final String destinationObjectType = 'paws_project_flow_site__c';
    static final String destinationObjectName = 'PAWS Project Flow Site';
    static final String destinationStepName = 'Test Destination Step 2 gkuw824jkbfk7y';
    static final String[] destinationStatuses = new String[]{'Not Started', 'Pending'};
    
    static testMethod void runPositiveTestCases() {
        // PREPARE SOURCE DATA
        // Prepare source Flow
        STSWR1__Flow__c sourceFlow = new STSWR1__Flow__c();
        sourceFlow.Name = 'Test Source Flow gkuw824jkbfk7y';
        sourceFlow.STSWR1__Object_Type__c = sourceObjectType;
        sourceFlow.STSWR1__Type__c='Template';
        insert sourceFlow;
        
        // Prepare source Step
        STSWR1__Flow_Step_Junction__c sourceStep = new STSWR1__Flow_Step_Junction__c();
        sourceStep.Name = 'Test Source Step gkuw824jkbfk7y';
        sourceStep.STSWR1__Flow__c = sourceFlow.Id;
        sourceStep.STSWR1__Is_First_Step__c = True;
        sourceStep.STSWR1__Index__c = 1;
        insert sourceStep;
        
        // PREPARE DESTINATION DATA
        // Prepare destination Flow
        STSWR1__Flow__c destinationFlow = new STSWR1__Flow__c();
        destinationFlow.Name = 'Test Destination Flow gkuw824jkbfk7y';
        destinationFlow.STSWR1__Object_Type__c = destinationObjectType;
        destinationFlow.STSWR1__Type__c='Template';
        insert destinationFlow;
        
        // Prepare destintion folder
        STSWR1__Item__c destinationFolder = new STSWR1__Item__c();
        destinationFolder.Name = 'Test Destination Folder gkuw824jkbfk7y';
        // destinationFolder.STSWR1__Name_Value__c = destinationFolder.Name;
        // destinationFolder.STSWR1__Type__c = 'Folder';
        insert destinationFolder;
        
        // Prepare destintion file
        STSWR1__Item__c destinationFile = new STSWR1__Item__c();
        destinationFile.Name = destinationFlow.Name;
        // destinationFile.STSWR1__Name_Value__c = destinationFolder.Name;
        // destinationFile.STSWR1__Type__c = 'File';
        // destinationFile.STSWR1__Source_Type__c = 'Flow__c';
        destinationFile.STSWR1__Source_Flow__c = destinationFlow.Id;
        destinationFile.STSWR1__Path__c = destinationFolder.Id;
        destinationFile.STSWR1__Parent__c = destinationFolder.Id;
        insert destinationFile;
        
        // Prepare destintion Steps
        STSWR1__Flow_Step_Junction__c destintionStep1 = new STSWR1__Flow_Step_Junction__c();
        destintionStep1.Name = 'Test Destination Step 1 gkuw824jkbfk7y';
        destintionStep1.STSWR1__Flow__c = destinationFlow.Id;
        destintionStep1.STSWR1__Is_First_Step__c = true;
        destintionStep1.STSWR1__Index__c = 1;
        insert destintionStep1;
        
        STSWR1__Flow_Step_Junction__c destintionStep2 = new STSWR1__Flow_Step_Junction__c();
        destintionStep2.Name = destinationStepName;
        destintionStep2.STSWR1__Flow__c = destinationFlow.Id;
        destintionStep2.STSWR1__Is_First_Step__c = false;
        destintionStep2.STSWR1__Index__c = 2;
        insert destintionStep2;
        
        STSWR1__Flow_Step_Junction__c destintionStep3 = new STSWR1__Flow_Step_Junction__c();
        destintionStep3.Name = 'Test Destination Step 3 gkuw824jkbfk7y';
        destintionStep3.STSWR1__Flow__c = destinationFlow.Id;
        destintionStep3.STSWR1__Is_First_Step__c = false;
        destintionStep3.STSWR1__Index__c = 3;
        insert destintionStep3;
        
        // Make a PageReference
        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        System.currentPageReference().getParameters().put('sourceid', sourceStep.Id);
        
        PAWS_ApexRuleWrapperBackController testPage = new PAWS_ApexRuleWrapperBackController();
        
        // Select Object
        List<SelectOption> objectTypes = testPage.ObjectTypes;
        Boolean foundObjectType = false;
        for (SelectOption currentObjectType : objectTypes) {
            if (currentObjectType.getValue() == destinationObjectName) {
                foundObjectType = true;
                break;
            }
        }
        if (!foundObjectType) {
            System.debug('Destination Object not found!');
            System.assertEquals(1, 2);
            return;
        }
        
        // Set Object
        testPage.dataobject = destinationObjectName;
        
        // Select Flow
        List<SelectOption> flows = testPage.flows;
        // Set Flow
        testPage.flow = destinationFlow.Id;
        
        // Select Step
        List<SelectOption> steps = testPage.steps;
        Boolean foundStep = false;
        for (SelectOption currentStep : steps) {
            if (currentStep.getLabel() == destinationStepName) {
                foundObjectType = true;
                System.assertEquals(currentStep.getValue(), destintionStep2.Id);
                break;
            }
        }
        if (!foundObjectType) {
            System.debug('Destination Step not found!');
            System.assertEquals(1, 2);
            return;
        }
        
        // Set Step
        testPage.step = destintionStep2.Id;
        
        // Set Statuses
        testPage.setStatuses(destinationStatuses);
        
       	// Receive result
        String resultJson = testPage.result_json;
        
        System.debug('Result json:' + resultJson);
        
        // Save json
        STSWR1__Flow_Step_Rule__c newRule = new STSWR1__Flow_Step_Rule__c();
        
        Map<String, String> resultMap = new Map<String, String>{};
        resultMap.put('params', resultJson);
        String final_json = JSON.serialize(resultMap);
        
        newRule.STSWR1__Flow_Step__c = sourceStep.Id;
        newRule.STSWR1__Conditions__c = final_json;
        newRule.STSWR1__Object_Type__c = sourceObjectType;
        insert newRule;
        
        // CHECK LOADING
        // Make a PageReference
        Test.setCurrentPageReference(new PageReference('Page.myPage'));
        System.currentPageReference().getParameters().put('sourceid', newRule.Id);
        
        PAWS_ApexRuleWrapperBackController testPageLoad = new PAWS_ApexRuleWrapperBackController();
        
        // Select Object
        List<SelectOption> newObjectTypes = testPageLoad.ObjectTypes;
        // Select Flow
        List<SelectOption> newFlows = testPageLoad.flows;
        // Select Step
        List<SelectOption> newSteps = testPageLoad.steps;
        
        String resultJsonAfterLoad = testPageLoad.result_json;
        System.assertEquals(resultJsonAfterLoad, resultJson);
        
        
    }
}