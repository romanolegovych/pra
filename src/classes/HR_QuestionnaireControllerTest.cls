/**
*   'HR_QuestionnaireControllerTest' is the test class for HR_QuestionnaireController
*   Test classes are used to write the unit test and cover the code coverage of the code.
*   @author   Devaram Bhargav
*   @version  26-Sep-2013
*   @since    17-Sep-2013
*/
@isTest (SeeAllData=false)
private class HR_QuestionnaireControllerTest {
    /** This method is used to create the test data 
    *   and test the methods in the controller.*/
    static testMethod void createHRData(){
        //Create the test data from the test utils
        HR_TestUtils tu=new HR_TestUtils ();
        tu.HR_TestUtils();       
        
        //Stating the the test.
        Test.startTest();
            //Instantiate the controller by paasing the HR Exit Interview record ID.
            HR_QuestionnaireController HRQCntr = new HR_QuestionnaireController(new Apexpages.Standardcontroller(tu.hrei));
            
            System.assertEquals(HRQCntr.PAGE_MODE, HRQCntr.PAGE_MODE_EDIT);
            
            //Making two list according to section's
            List<HR_QuestionnaireCVO> HRQCVOList1 = HRQCntr.ListofHRQCVOList[1];
            List<HR_QuestionnaireCVO> HRQCVOList0 = HRQCntr.ListofHRQCVOList[0];
            system.debug('-------HRQCntr.ListofHRQCVOList-----------'+HRQCntr.ListofHRQCVOList);
            
            //Assigning the picklist value
            HRQCVOList1[0].PicklistResponse='good';
            
            //making the Multi- picklist values
            string[] multistr = new List<string>();
            multistr.add('good');
            multistr.add('better');
            HRQCVOList0[0].getPicklistoptions();
            HRQCVOList0[0].MultiPicklistResponse=multistr;
            System.assertEquals(multistr, HRQCVOList0[0].MultiPicklistResponse);
           
            //Removing both the lists from the List of List
            HRQCntr.ListofHRQCVOList.remove(0);
            HRQCntr.ListofHRQCVOList.remove(0);
            System.assertEquals((HRQCntr.ListofHRQCVOList).size(),0);
            
            //Add the both lists into the List of List again with responses 
            HRQCntr.ListofHRQCVOList.add(HRQCVOList1);
            HRQCntr.ListofHRQCVOList.add(HRQCVOList0);
            System.assertEquals((HRQCntr.ListofHRQCVOList).size(),2);
            
            //Save the Questionnaire
            HRQCntr.SaveHRQuestionnaire();        
            
            //Edit the Questionnaire 
            HRQCntr.EditHRQuestionnaire();
            
            //Get the List's back from List of Lists
            HRQCVOList1=HRQCntr.ListofHRQCVOList[1];
            HRQCVOList0=HRQCntr.ListofHRQCVOList[0];
            System.assertEquals('good', HRQCVOList1[0].PicklistResponse);
            
            //pick null values into the picklists to tets the error condition for required questionns.
            HRQCVOList1[0].PicklistResponse     =null;          
            HRQCVOList0[0].MultiPicklistResponse=null;
                        
            //Removing both the lists from the List of List
            HRQCntr.ListofHRQCVOList.remove(0);
            HRQCntr.ListofHRQCVOList.remove(0);
            System.assertEquals((HRQCntr.ListofHRQCVOList).size(),0);
            
            //Add the both lists into the List of List again with responses as null values
            HRQCntr.ListofHRQCVOList.add(HRQCVOList1);
            HRQCntr.ListofHRQCVOList.add(HRQCVOList0);
            System.assertEquals((HRQCntr.ListofHRQCVOList).size(),2); 
            
            //Saving back with selecting the null values in the picklist 
            HRQCntr.SaveHRQuestionnaire(); 
            system.debug('-------HRQCntr.ListofHRQCVOList-----------'+HRQCntr.ListofHRQCVOList);
            
            //Refreshing the page with new instance
            HR_QuestionnaireController HRQCntr1 = new HR_QuestionnaireController(new Apexpages.Standardcontroller(tu.hrei));
            System.assertEquals((HRQCntr1.ListofHRQCVOList).size(),2); 
            
            //Deleteing the questionnaire list.
            HRQCntr1.DeleteHRQuestionnaire();
            
        Test.stopTest();
    }
    

}