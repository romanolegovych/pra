public with sharing class PAWS_ProjectFlowSubmissionTrigger extends STSWR1.AbstractTrigger
{
	public override void beforeInsert(List<SObject> records)
	{
		new PAWS_API().beforeUpsert(records);
	}
	
	public override void beforeUpdate(List<SObject> records, List<SObject> oldRecords)
	{
		new PAWS_API().beforeUpsert(records);
	}
	
	public override void afterInsert(List<SObject> records)
	{
		new PAWS_API().activateFlows(records);
	}
	
	public override void afterUpdate(List<SObject> records, List<SObject> oldRecords)
	{
		new PAWS_API().activateFlows(records);
	}
	
	public override void afterDelete(List<SObject> records)
    {
        new PAWS_API().afterDelete(records);
    }
}