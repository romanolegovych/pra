public without sharing class PAWS_LimitsTest implements STSWR1.WorkflowCallApexActionInterface
{
  public class LimitsException extends Exception {}
  
  public void run (SObject context, STSWR1__Flow_Instance_Cursor__c cursor, String operationType, String parameter)
  {
    Integer queriesNumber = 40;
    Integer dmlsNumber = 40;
    
    if (Limits.getQueries() + queriesNumber >= Limits.getLimitQueries()) throw new LimitsException('Not enough queries to execute this class.');
    if (Limits.getDMLStatements() + dmlsNumber >= Limits.getLimitDMLStatements()) throw new LimitsException('Not enough DMLs to execute this class.');

    try
    {
      for (Integer i = 0; i < queriesNumber; i++)
      {
        List<ecrf__c> projects = [Select Id From ecrf__c limit 1];
      }
    }
    catch(Exception ex)
    {
      throw ex;
    }
  }
}