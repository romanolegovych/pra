public class LMS_CourseAssignments {
	
	public Boolean selected{get;set;}
	public String assignmentId{get;private set;}
    public String courseId{get;private set;}
    public String courseCode{get;private set;}
    public String courseTitle{get;private set;}
    public String duration{get;private set;}
    public String status{get;private set;}
    public String assignmentStatus{get;private set;}
    public String sync{get;private set;}
    public String assignedOn{get;private set;}
    public String assignedBy{get;private set;}
    public String processed{get;private set;}
    public Integer targetDays{get;private set;}
    public Date availFrom{get;private set;}
    public Date disconFrom{get;private set;}
    public Date commitDate{get;set;}
    public Datetime modifiedDate{get;private set;}
    public String style{get;private set;}
    public String bgStyle{get;private set;}
    public String commitStyle{get;private set;}
    public String roleId{get;private set;}
    
    public LMS_CourseAssignments(LMS_Role_Course__c r) {
    	selected = false;
        roleId = r.Role_Id__c;
        assignmentId = r.Id;
        courseId = r.Course_Id__r.Id;
        courseCode = r.Course_Id__r.Course_Code__c;
        courseTitle = r.Course_Id__r.Title__c;
        duration = r.Course_Id__r.Duration_Display__c;
        targetDays = Integer.valueOf(r.Course_Id__r.Target_Days__c);
        status = r.Course_Id__r.Status__c; 
        assignmentStatus = r.Status__c;
        sync = r.Sync_Status__c;
        availFrom = r.Course_Id__r.Available_From__c;
        disconFrom = r.Course_Id__r.Discontinued_From__c;
        if(r.Status__c == 'Committed') {
        	processed = 'Processed';
        } else {
        	if(r.Commit_Date__c != null) {
        		commitDate = r.Commit_Date__c;
        	}
        }
        if(r.Assigned_On__c != null) {
            assignedOn = r.Assigned_On__c.format('MMMM dd, yyyy');
        }
        assignedBy = r.Assigned_By__c;
        modifiedDate = r.LastModifiedDate;
        
        if(assignmentStatus == 'Draft') {
            style = 'draftCourse';
            bgStyle = 'draftRow';
        } else if(assignmentStatus == 'Draft Delete') {
            style = 'deleteCourse';
            bgStyle = 'draftDeleteRow';
        } else if(assignmentStatus == 'Committed') {
        	commitStyle = 'commitCourse';
        }
    }
}