/** Implements the Selector Layer of the object Service__c
 * @author	Maurice Kremer, Dimitrios Sgourdos
 * @version	06-Feb-2014
 */
public with sharing class ServiceDataAccessor {

	/** Object definition for fields used in application for Service__c
	 * @author	Maurice Kremer
	 * @version 09-Jan-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('Service__c');
	}
	
	
	/** Object definition for fields used in application for Service__c with the parameter referenceName as a prefix
	 * @author	Maurice Kremer, Dimitrios Sgourdos
	 * @version 07-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 * @change	31-Jan-2014 added parentcategory and grantparent category to fields
	 * @change	06-Feb-2014 added ShortName to fields
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'ShortName__c,';
		result += referenceName + 'ServiceCategory__c,';
		result += referenceName + 'InformationalText__c,';
		result += referenceName + 'IsDesignObject__c,';
		result += referenceName + 'SequenceNumber__c,';
		result += referenceName + 'IsExpense__c,';
		result += referenceName + 'ServiceCategory__r.ParentCategory__c,';
		result += referenceName + 'ServiceCategory__r.ParentCategory__r.ParentCategory__c';
		return result;
	}
	
	/** Retrieve list of Services.
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @param	whereClause			The criteria that Service__c Lists must meet
	 * @return	List of Services
	 */
	public static List<Service__c> getServiceList(String whereClause) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM Service__c ' +
								'WHERE {1} ' +
								'ORDER BY ServiceCategory__r.Code__c, SequenceNumber__c',
								new List<String> {
									getSObjectFieldString(),
									whereClause
								}
							);
		return (List<Service__c>) Database.query(query);
	}

}