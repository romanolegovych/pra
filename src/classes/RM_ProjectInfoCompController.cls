public with sharing class RM_ProjectInfoCompController {
	public string projectID{get; set;}
	public WFM_Project__c project{get; private set;}
	public string protocol{get; private set;}
	public string theraArea{get; private set;}
    public string phase{get; private set;}
    public Boolean bBid{get;private set;}
    
    public RM_ProjectInfoCompController(){
    	getProjectfromID(projectID);
    }
    public void getProjectInfo(){
    	getProjectfromID(projectID);
    	//return project;
    }
    private  void getProjectfromID(string pID){
        system.debug('---project---' + pID);
        
        if (pID <> null){        
            if (pID.Length() > 0)  {
                List<WFM_Project__c> lstProj = RM_ProjectService.GetProjectbyrID(projectID);
				if (lstProj.size() > 0){
					project = lstProj[0]; 
                    system.debug('---project---' + project);
                    if (project.protocols__r != null && project.protocols__r.size() > 0){
                        theraArea = project.Protocols__r[0].Therapeutic_Area__c;
                        phase = project.Protocols__r[0].phase__c;
                        protocol = project.Protocols__r[0].name;
                    }
                    else{
                        theraArea = ''; 
                        phase='';
                        protocol = '';
                    }
                    if (Project.Bid_Defense_Date__c==null)
                        bBid = false;
                    else
                        bBid = true;
                    
                }                
            }
        }
        
    }
}