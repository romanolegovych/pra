/** 
@Author Bhargav Devaram
@Date 2015
@Description this class is to lookup all active, approved and global or Non global questions for Service_Task_To_Service_Impact__c  object.
*/
public class PBB_ActiveQuestionsforServiceTasksCntrl {
    
    public string QuestionName{get;set;}
    public List<Service_Impact_Questions__c> results{get;set;} // search results
    
    //Standard Constructor
    public PBB_ActiveQuestionsforServiceTasksCntrl () {
    
        results = new List<Service_Impact_Questions__c>();
        QuestionName = System.currentPageReference().getParameters().get('lksrch');
        String isGlobal  = System.currentPageReference().getParameters().get('Global');  

        //get all approved questions with active status and global or non global 
        String soql = 'select id, name,Service_Impact_Question__r.name,Service_Impact_Question__c,Status__c from Service_Impact_Questions__c';        
        soql = soql +  ' where Status__c=\'Approved\'  and Service_Impact_Question__r.isActive__c=true';
        
        //check if the questions are global or not by service tasks
        if(isGlobal =='true')
            soql = soql +  ' and Question_Applies_to__c=\'Project\'';
        else
             soql = soql +  ' and Question_Applies_to__c!=\'Project\'';
        soql = soql + ' limit 999 ';
        System.debug(soql);
        results = database.query(soql); 
        System.debug(results );
    }
    
    // used by the visualforce page to send the link to the right dom element
    public string getFormTag() {
        return System.currentPageReference().getParameters().get('frm');
    }
    
    // used by the visualforce page to send the link to the right dom element for the text box
    public string getTextBox() {
        return System.currentPageReference().getParameters().get('txt');
    }
}