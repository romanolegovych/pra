global class BDT_AutoComplete {
	
	public BDT_AutoComplete(BDT_FinancialDocPricingController tmpContr) {
		
	}
	
	public BDT_AutoComplete(BDT_ApprovalRulesController tmpContr) {
		
	}
	
	public BDT_AutoComplete(BDT_GeneralTemplate tmp){}
	
	public BDT_AutoComplete() {
		
	}
	
	
@RemoteAction	
	global static List<Sobject> getAutocompleteData(string ObjectList, string FieldList, string SearchString, boolean showdeleted ){
	
	   List<String> objectsList = ObjectList.deleteWhitespace().split(',');
	   List<String> fieldsList = FieldList.deleteWhitespace().split(',');

		//it is possible to pass object names that don't exist. clean up objectslist first.
        List<string> realObjects = new List<string>();
        Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();
        for(string objName: objectsList){
        	Schema.SObjectType sot = gd.get(objName);
    		if(sot != null){
    			realObjects.add(objName);
    		}
        }
		
		//validate field names for the first object
		List<String> realFieldnames = new List<String>();
		for(string fldname: fieldsList){
			if(FieldExists( realObjects[0], fldname )){
				realFieldnames.add(fldname);	
			}
		}
		string Filter = ' like \'%' + String.escapeSingleQuotes(SearchString) + '%\'';
		string query = 'Select Id';
		for(string fld: realFieldnames){
			query +=', '+fld;
		}
		query += ' from '+realObjects[0]+' where ('; 	
    	integer i=0;
    	
    	for( string fld: realFieldnames){
    		if(i==0){
    			query += fld + filter;
    		}
    		else{
    			query+= ' or '+fld+filter;
    		}
    		i++;
    	}
    	query +=')';
    	if(!showdeleted){
    		if(FieldExists( realObjects[0],'BDTDeleted__c' )){
    			query += ' and BDTDeleted__c=false';
    		}
    	}
    	
    	List<sObject> L = new List<sObject>();
    	
    	L = Database.query(query);
    	
    	return L;
    
	}
	public static boolean FieldExists(string objectName, string fieldName ){
		
		Map<String, Schema.SObjectType> m = Schema.getGlobalDescribe() ;
        boolean returnvalue = false;
		//if the fieldname is Id, return false, in order not to get 2 Id query's
		if(fieldname!='Id'){
			Schema.SObjectType sot = m.get(objectName);
			if(sot !=null){
				schema.DescribeSObjectResult r = sot.getDescribe();
	
				Map<string, schema.SObjectField> n = r.fields.getMap();
			
				schema.sObjectField sof = n.get(fieldName);
			
				if(sof!=null){
					returnvalue = true;
				}	
			}
		}
		
		return returnvalue;
	}
	
	
	/** Retrieve the plenbox users that are suited in the user entry in an auto complete component.
	 * @author	Dimitrios Sgourdos
	 * @version 14-Jan-2014
	 * @param	FieldList			The list of the fields that the user entry will be searched
	 * @param	searchString		The user entry for searching
	 * @return	The Users that are plenbox users and suit to the user entry.
	 */
	@RemoteAction
	global static List<sObject> getAutocompleteDataPlenboxUsers(String FieldList, String searchString){
		// Find plenbox users
		List<User> plenboxUsers = BDT_UserRolesService.getBDTusers();
		Set<String> plenboxUsersIds = BDT_Utils.getStringSetFromList(plenboxUsers, 'Id');
		
		// Validate field names
		List<String> fieldsList = FieldList.deleteWhitespace().split(',');
		List<String> realFieldnames = new List<String>();
		for(string fldname: fieldsList){
			if(FieldExists( 'User', fldname )){
				realFieldnames.add(fldname);
			}
		}
		
		// Create query
		String Filter = ' like \'%' + String.escapeSingleQuotes(SearchString) + '%\'';
		string query = 'Select Id';
		for(string fld: realFieldnames){
			query +=', '+fld;
		}
		query += ' from User where (';
		Integer i=0;
		
		for( string fld: realFieldnames){
			query += ( (i==0)? '' : ' or ');
			query += fld + filter;
			i++;
		}
		query +=')';
		query += ' and Id in :plenboxUsersIds';
		
		return Database.query(query);
	}
}