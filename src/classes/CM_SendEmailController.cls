public class CM_SendEmailController {

    public String subject { get; set; }
    public String fromAddress {get; set;}
    Public String toAddress {get; set;}
    
    public String body { get; set; }

    private final Account account;
    //public Apttus__APTS_Agreement__c AttachmentListA {get; set;}
    
    // Create a constructor that populates the Account object
    public CM_SendEmailController() {
        //AttachmentListA = [SELECT Id,Name,(SELECT Id,Name FROM Attachments) FROM Apttus__APTS_Agreement__c WHERE id='a7qL0000000030Y' limit 1];
        //account = [select Name, (SELECT Contact.Name, Contact.Email FROM Account.Contacts) 
            //  from Account where id = :ApexPages.currentPage().getParameters().get('id')];
    }
    
    //ATTACHMENT BLOCK    
    public List<wrAttachment> AttachmentList {get; set;}
    
    public List<wrAttachment> getAttachments(){
        if(AttachmentList == null){
            AttachmentList= new List<wrAttachment>();
            for(Attachment a:[Select id, name, BodyLength, LastModifiedDate from Attachment where ParentId='a7qL0000000030Y']){
                   AttachmentList.add(new wrAttachment(a));
            }
        }
        return AttachmentList;
    }
    
    // wrapper class for attachments
    public class wrAttachment{
        public Attachment att{get; set;}
        public Boolean Selected {get; set;}
        
        public wrAttachment(Attachment add){
            att=add;
            Selected=false;           
        }
    }
    
    //TEMPLATE BLOCK
    public List<wrTemplate> TemplateList {get; set;}
    
    public List<wrTemplate> getTemplates(){
        if(TemplateList == null){
            TemplateList= new List<wrTemplate>();
            for(EmailTemplate a:[Select Id, Name, Subject, TemplateType, FolderId, Folder.Name, Description From EmailTemplate Where  FolderId='00lL0000000gCZ0IAM']){
                   TemplateList.add(new wrTemplate(a));             
            }
        }
        return TemplateList;
    }
    
    // wrapper class for Templates
    public class wrTemplate{
        public EmailTemplate Etemp{get; set;}
        public Boolean TSelected {get; set;}
        
        public wrTemplate(EmailTemplate add){
            Etemp=add;
            TSelected=false;           
        }
        
    }
    
    public Apttus__APTS_Agreement__c gettoValue(){
        return [select Apttus__Requestor__c from Apttus__APTS_Agreement__c limit 1];
    }
        
    public Account getAccount() {
        return account;
    }

    public PageReference send() {
        // Define the email
        Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage(); 
        
        String addresses;
        
    if (account.Contacts[0].Email != null)
    {
        addresses = account.Contacts[0].Email;
        // Loop through the whole list of contacts and their emails
        for (Integer i = 1; i < account.Contacts.size(); i++) 
        {
            if (account.Contacts[i].Email != null)
            {
                addresses += ':' + account.Contacts[i].Email;
            }
        }
    }

        String[] toAddresses = addresses.split(':', 0);

        // Sets the paramaters of the email
        email.setSubject( subject );
        email.setToAddresses( toAddresses );
        email.setPlainTextBody( body );
    
        // Sends the email
        Messaging.SendEmailResult [] r = 
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});   
        
        return null;
    }
}