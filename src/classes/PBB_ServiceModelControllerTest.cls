/**
@author
@date 2015
@description this is PBB_Services's test class
**/
@isTest   
private class PBB_ServiceModelControllerTest {
    static testMethod void testPBB_ServiceModelController(){
       test.startTest(); 
            //Prepare the test data
            PBB_TestUtils tu = new PBB_TestUtils ();       
            tu.smm = tu.createServiceModelAttributes();
            PageReference pageRef = Page.PBB_ServiceModelController;
            Test.setCurrentPage(pageRef);
            ApexPages.currentPage().getParameters().put('id',tu.smm.Id);  
            
            ApexPages.StandardController sc = new ApexPages.StandardController(tu.smm);
            PBB_ServiceModelController modelController = new PBB_ServiceModelController(sc);
            modelController.saveSMQuestions();
            
            Service_model__c smclone = new Service_model__c();
            ApexPages.currentPage().getParameters().put('clone', '1'); 
            ApexPages.StandardController sc1 = new ApexPages.StandardController(smclone);
            PBB_ServiceModelController modelController1 = new PBB_ServiceModelController(sc1);
            system.assertEquals('1',ApexPages.currentPage().getParameters().get('clone'));
            modelController1.saveSMQuestions();
            
       test.stopTest();      
    }   
}