@isTest
private class AA_NewCostRateExtensionTest{   
    
    /* Test BUF Code Selection options */
    static testMethod void getbufselectoptionTest() {
    
         initTestData ();
         
         Test.startTest();

         AA_NewCostRateExtension extension = new AA_NewCostRateExtension(new ApexPages.StandardController(new Cost_Rate__c()));
         List<SelectOption> result = extension.getbufselectoption();
         
         System.assertNotEquals(result.size(),null);
         
         Test.stopTest();
    }
    
    /* Test Currency Selection options */
    static testMethod void getcurrencyselectoptionTest() {
    
         initTestData ();
         
         Test.startTest();

         AA_NewCostRateExtension extension = new AA_NewCostRateExtension(new ApexPages.StandardController(new Cost_Rate__c()));
         List<SelectOption> result = extension.getcurrencyselectoption();
         
         System.assertNotEquals(result.size(),null);
         
         Test.stopTest();
    }
    
    /* Test Create button */
    static testMethod void CreateTest() {
    
         initTestData ();
         
         Test.startTest();

         AA_NewCostRateExtension extension = new AA_NewCostRateExtension(new ApexPages.StandardController(new Cost_Rate__c()));

         
         //BUF Code Data
         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
          
         extension.yearValue = '2011';
         extension.initialRateValue = 200;
         extension.selectedBufCode = BufCode1.Id;
         extension.selectedCurrency = currency1.Id;
         
         PageReference pageRef = extension.Create();
         
          PageReference reqPageRef = Page.AA_Inflation_Override;
          reqPageRef.getParameters().put('functionId', BufCode1.Id);
          reqPageRef.getParameters().put('scheduledYear','2011');

         System.assertEquals(pageRef.geturl(), reqPageRef.geturl());
         
         Test.stopTest();
    }
    
     /* Test Create Error in Year button */
    static testMethod void CreateErrorTest() {
    
         initTestData ();
         
         Test.startTest();

         AA_NewCostRateExtension extension = new AA_NewCostRateExtension(new ApexPages.StandardController(new Cost_Rate__c()));

         
         //BUF Code Data
         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
          
         extension.yearValue = '1990';
         extension.initialRateValue = 200;
         extension.selectedBufCode = BufCode1.Id;
         extension.selectedCurrency = currency1.Id;
         
         PageReference pageRef = extension.Create();
         
          PageReference reqPageRef = Page.AA_Inflation_Override;
          reqPageRef.getParameters().put('functionId', BufCode1.Id);
          reqPageRef.getParameters().put('scheduledYear','2011');

         System.assertEquals(pageRef, null);
         
         Test.stopTest();
    }
    
     /* Test Create Exception in Year button */
    static testMethod void CreateExceptionTest() {
    
         initTestData ();
         
         Test.startTest();

         AA_NewCostRateExtension extension = new AA_NewCostRateExtension(new ApexPages.StandardController(new Cost_Rate__c()));

         
         //BUF Code Data
         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
          
         extension.yearValue = 'A';
         extension.initialRateValue = 200;
         extension.selectedBufCode = BufCode1.Id;
         extension.selectedCurrency = currency1.Id;
         
         PageReference pageRef = extension.Create();
         
          PageReference reqPageRef = Page.AA_Inflation_Override;
          reqPageRef.getParameters().put('functionId', BufCode1.Id);
          reqPageRef.getParameters().put('scheduledYear','2011');

         System.assertEquals(pageRef, null);
         
         Test.stopTest();
    }
    
    /* Test Back button */
    static testMethod void backTest() {
    
         initTestData ();
         
         Test.startTest();

         AA_NewCostRateExtension extension = new AA_NewCostRateExtension(new ApexPages.StandardController(new Cost_Rate__c()));
         PageReference pageRef = extension.back();
         
         System.assertEquals(pageRef.geturl(), Page.AA_Cost_Rate.geturl());
         
         Test.stopTest();
    }
    
    /* Test Create New Records Test */
    static testMethod void createNewRecordsTest() {
    
         initTestData ();
         
         Test.startTest();

         AA_NewCostRateExtension extension = new AA_NewCostRateExtension(new ApexPages.StandardController(new Cost_Rate__c()));
         
         //BUF Code Data
         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
          
         extension.yearValue = '2011';
         extension.initialRateValue = 200;
         extension.selectedBufCode = BufCode1.Id;
         extension.selectedCurrency = currency1.Id;
         
         
         extension.createNewRecords();
         
         System.assertEquals(1, 1);
         
         Test.stopTest();
    }
    /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }  
}