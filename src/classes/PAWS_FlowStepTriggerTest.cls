@isTest
private class PAWS_FlowStepTriggerTest
{
	@istest private static void coverAll()
	{
		PAWS_ApexTestsEnvironment.init();
		
		STSWR1__Flow_Step_Junction__c step = PAWS_ApexTestsEnvironment.FlowStep;
		
		Test.startTest();
		
		delete step;
		
		Test.stopTest();
	}
}