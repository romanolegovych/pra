public with sharing class CV_CreateIncidentController {

    public static final String INCIDENT_CONTACT_TYPE_WEB_FORM = 'Web request form';
    
    public BMCServiceDesk__Incident__c incident {
        get{
            if(incident == null){
                incident = new BMCServiceDesk__Incident__c();
            }
            return incident;
        }
        set;
    }   
    
    public void saveAction(){
        try{
            incident.BMCServiceDesk__FKClient__c = UserInfo.getUserId();
            incident.BMCServiceDesk__contactType__c = INCIDENT_CONTACT_TYPE_WEB_FORM;
            insert incident;
        } catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }    
    
}