/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_MyResourcesChartControllerUTest {
 
 	 static list<Country__c> lstCountry;	
    
    static list<Employee_Details__c> lstEmpl; 
    static list<Employee_Status__c> lstEmplStatus;
    static Employee_Details__c supervisor;
    static list<WFM_Client__c> lstClient;
    static list<WFM_Contract__c>  lstContract;
    static list<WFM_Project__c> lstProject;
    static list<WFM_Protocol__c> lstProt;
    static list<WFM_Site_Detail__c> lstSite;
    

    
    static List<WFM_Location_Hours__c> lstHrs;
    static List<WFM_Location_Hours__c> hrs1;
    static List<WFM_Employee_Availability__c> lstAvas;
    static WFM_employee_Allocations__c alloc; 
    static WFM_employee_Allocations__c alloc1;
   
    static List<WFM_Employee_Assignment__c> lstAssigns;
    static List<WFM_Employee_Assignment__c> lstAssigns1;
  	static list<WFM_EE_Work_History__c> lstHistory;
  	static list<WFM_EE_Work_History__c> lstHistory1;
  	static list<Job_Class_Desc__c> lstJob;
    
  
    static RM_ConstantSetting__c emailGroup;
    
    static RM_UnitTestData init(){
    	RM_UnitTestData testData = new RM_UnitTestData();
    	lstCountry = testData.lstCountry;
    	lstEmpl = testData.lstEmpl;
    	lstEmplStatus = testData.lstEmplStatus;
    	lstClient = testData.lstClient;
    	lstContract = testData.lstContract;
    	lstProject = testData.lstProject;
    	lstJob = testData.lstJobs;
    	
    	lstHrs = testData.insertLocationHr(-4, 6, 'TTT'); 
    	hrs1 = testData.insertLocationHr(-4, 6, 'TTS');
    	lstAvas = testData.insertAvailability(lstEmpl[0], lstHrs);
    	alloc = testData.insertAllocation(lstEmpl[0].id, lstProject[0].id, RM_Tools.GetStringfromDate(RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(-4)), 'mm/dd/yyyy'), RM_Tools.GetStringfromDate(RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(5)), 'mm/dd/yyyy'),  'Project Manager', lstCountry[0].Name);
    	alloc1 = testData.insertAllocation(lstEmpl[0].id, lstProject[0].id, RM_Tools.GetStringfromDate(RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(-4)), 'mm/dd/yyyy'), RM_Tools.GetStringfromDate(RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(5)), 'mm/dd/yyyy'),  'Clinical Research Associate',lstCountry[0].Name);
    	lstAssigns = testData.insertAssignment(lstEmpl[0].id, alloc, lstAvas, 0.2);
    	lstAssigns1 = testData.insertAssignment(lstEmpl[0].id, alloc1, lstAvas, 0.5);
    	system.debug('---------Test lstCountry---------'+lstCountry  ); 
    	system.debug('---------Test lstHrs---------'+lstHrs  ); 
    	//reset email to tester's email
    	user u = RM_OtherService.GetUserInfo(UserInfo.getUserId());
    	
    	
 		supervisor = new Employee_Details__c(COUNTRY_name__c = lstCountry[0].id, name='1120', Employee_Unique_Key__c='1120', First_Name__c='John', Last_Name__c='supervisor',email_Address__c=u.Email, Job_Class_Desc__c='Clinical Research Associate',
        Function_code__c='TT',Buf_Code__c='TSTTT', Location__c = 'TestLoc', Business_Unit__c='BUC', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), Manager_Last_Name__c='TestManager',  Manager_First_Name__c='TestFirst', 
        FTE_Equivalent__c = 0.8, NBR_PRA_Monitoring_Yrs__c = 5.0, NBR_Non_PRA_Monitoring_Yrs__c = 6.0, NBR_AT_PRA_Yrs__c=5.0);
        insert supervisor;
        lstEmpl[0].Supervisor_ID__c = supervisor.name;
        lstEmpl[1].Supervisor_ID__c = supervisor.name;
        
        update lstEmpl;    
    	lstHistory = testData.insertWorkHist(lstProject[0].id, lstEmpl[0].id, lstAvas);
        system.debug('---------Test lstHistory---------'+lstHistory  ); 
      	lstHistory1 = testData.insertWorkHist(lstProject[1].id, lstEmpl[0].id, lstAvas);
        system.debug('---------Test lstHistory1---------'+lstHistory1  ); 
    	emailGroup = testData.emailGroup;
    	return testData;
    }
    static testMethod void myUnitTest() {
        init();
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/RM_MyResourcesChart?sid=' + supervisor.name);
        Test.setCurrentPage(pageRef);
        RM_MyResourcesChartController  v = new RM_MyResourcesChartController();
      
       
        system.assert(v.nView == 'All');
    }
    static testMethod void TestDropdowns() {
        init();
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/RM_MyResourcesChart?sid=' + supervisor.name);
        Test.setCurrentPage(pageRef);
        RM_MyResourcesChartController  v = new RM_MyResourcesChartController();
      	List<SelectOption> listMonths = v.getTimeRangeItems();
       
        system.assert(listMonths.size() > 0);
        List<SelectOption> listProjTime = v.getProjTimeItems();
        system.assert(listProjTime.size() > 0);
        
        list<SelectOption> lstEmplName = v.getEmpNameItems();
        system.assert(lstEmpl.size() > 0);
        
        list<SelectOption> lstItem = v.getViewItems();
        system.assert(lstItem.size() > 0);
        v.nView = '1';
      	v.ViewChange();
      	string emplList = v.getempList();
      	system.assert(emplList.length() > 0);
        
    }
    static testMethod void TestDropdownsChange() {
        init();
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/RM_MyResourcesChart?sid=' + supervisor.name);
        Test.setCurrentPage(pageRef);
        RM_MyResourcesChartController  v = new RM_MyResourcesChartController();
      	v.TimeRangeChange();
      	string seriesData = v.getseriesData();
      	system.assert(seriesData.length() > 0);
      	
      	v.ViewChange();
      	string emplList = v.getempList();
      	system.assert(emplList.length() > 0);  
    }
    static testMethod void TestNext() {
        init();
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/RM_MyResourcesChart?sid=' + supervisor.name);
        Test.setCurrentPage(pageRef);
        RM_MyResourcesChartController  v = new RM_MyResourcesChartController();
        v.n = 2;
      	v.Next();
      	
      	system.assert(v.bPrev == true);
      	
      	v.n = 2;
      	v.Previous();
      	system.assert(v.bPrev == false);  
    }
     static testMethod void TestEmpNameChange() {
        init();
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/RM_MyResourcesChart?sid=' + supervisor.name);
        Test.setCurrentPage(pageRef);
        RM_MyResourcesChartController  v = new RM_MyResourcesChartController();
        v.empRID = lstEmpl[0].id;
        v.pMonth = RM_Tools.GetYearMonth(-1);
      	v.EmpNameChange();
      	string lineCat = v.getLineCat();
      	system.assert(lineCat.length() > 0);
      	string lineData = v.getLineData();
      	system.assert(lineData.length() > 0);
      	
      	v.ProjTimeChange();
      	string pieDate = v.getPieData();
      	system.assert(pieDate.length() > 0); 
      	string pieHistDate = v.getPieDataHist();
      	system.assert(pieHistDate.length() > 0);  
    }
    static testMethod void TestClickAllocation() {
        init();
        Test.startTest();
        string strMonth = RM_Tools.GetYearMonth(-1);
        PageReference pageRef = new PageReference('/apex/RM_MyResourcesChart?sid=' + supervisor.name + '&resourceName=' + lstEmpl[0].Last_Name__c + ', ' + lstEmpl[0].First_Name__c + '&monthYear=' + strMonth);
        Test.setCurrentPage(pageRef);
        RM_MyResourcesChartController  v = new RM_MyResourcesChartController();
        
      	v.OnClickAllocation();
      	string lineCat = v.getLineCat();
      	system.assert(lineCat.length() > 0);
      	string lineData = v.getLineData();
      	system.assert(lineData.length() > 0);
      	
      	v.OnClickResourceAvail();
      	string pieDate = v.getPieData();
      	system.assert(pieDate.length() > 0); 
      	string pieHistDate = v.getPieDataHist();
      	system.assert(pieHistDate.length() > 0);  
    }
}