global class PRA_ForecastUpdateBatch  extends PRA_BatchaQueueable {


  private String oName;
  private String fMonthAppliesTo;
  private String fValue;
  private Boolean serialExecution;
  
  private String additionalField; // additional field queried - buf_code for hours
  private String additionalFieldQuery;
  
  private String query;
  
  global PRA_ForecastUpdateBatch (){
  }
  
  global override void setBatchParameters(String parametersJSON){
    system.debug('---Set parameters: ' + parametersJSON);
    // set paramters from Json string
    List<String> params = (List<String>)Json.deserialize(parametersJSON, List<String>.class);
    oName      = params.get(0);
    fMonthAppliesTo  = params.get(1);
    fValue      = params.get(2);
    
    // do rest stuff from contructor
    additionalFieldQuery = '';
    if (oName == 'Hour_EffortRatio__c'){
     //future use
      //additionalField = 'BUF_Code__c';
      //additionalFieldQuery = ', BUF_Code__c ';
    }
  }
  
  
  global override Database.QueryLocator start(Database.BatchableContext BC){
    // lock all projects
    List<Client_Project__c> allProjects = [select Id, Is_Project_Locked__c from Client_Project__c where Is_Project_Locked__c = false];
    for (Client_Project__c project : allProjects){
      project.Is_Project_Locked__c = true;
    }
    update allProjects;
    
    // get string with first day of current month
    Date fdm = System.today().toStartOfMonth();
    Datetime dt = Datetime.newInstance(fdm.year(), fdm.month(), fdm.day(), 0,0, 0);
    String firstDayOfCurrentMonth = dt.format('yyyy-MM-dd');
    
    
    query = 'select Id '  +
             'from ' + oName + ' ' +
             'where ' ; 
             //Check if object is unit_Effort 
             if(oName == 'Unit_Effort__c'){   
                query += '' + fMonthAppliesTo + '<' + firstDayOfCurrentMonth + ' ' +
                         'and Is_Historical__c=false' ;
                       
             }else{
             //future use
             //Check if object is Hour_EffortRatio 
               //query +=  '' + fMonthAppliesTo + '<' + firstDayOfCurrentMonth + ' ' ;
                         
             
             }  
         
    if (Test.isRunningTest()){
      query += ' Limit 200';
    }
    system.debug('---' + query + '---');
    return Database.getQueryLocator(query);
  }
  global override void execute(Database.BatchableContext BC, List<Sobject> scope){
  
    Set <String> crctIds = new Set <String> ();
    Set <Date> monthsAppliesTo = new Set <Date>();
    Date firstDayOfCurrentMonth = Date.today().toStartOfMonth();
    
    Set <id> setobjIDs = new Set <id> ();
    List<Unit_Effort__c>  uniteffortlist = new List<Unit_Effort__c>(); 
    List<Hour_EffortRatio__c>  HourEffortRatiolist = new List<Hour_EffortRatio__c>();
    
    //Collect all the object record id's
     for (Sobject sObj : scope){  
       system.debug('-----sobj---'+sObj);       
       setobjIDs.add((ID)sObj.get('Id'));      
      
     }
     system.debug('-----setobjIDs---'+setobjIDs.size()); 
     
     //update Is_Historical__c=true for all previous months 
     if(oName == 'Unit_Effort__c'){           
           uniteffortlist = [select id,Month_Applies_To__c,Is_Historical__c                                       
                                    from Unit_Effort__c where id in :setobjIDs order by Month_Applies_To__c];
           for(Unit_Effort__c uf:uniteffortlist){ 
               if(uf.Month_Applies_To__c<firstDayOfCurrentMonth && uf.Is_Historical__c!=true){ 
                  system.debug('-----uf---'+uf);                      
                  uf.Is_Historical__c=true;
               } 
           }
           //Update UnitEffortlist 
           if(uniteffortlist.size()>0)
             update uniteffortlist;   
     }
     system.debug('-----uniteffortlist---'+uniteffortlist);
    
    
  }
    
  global override  void finish(Database.BatchableContext BC){
  }
 
}