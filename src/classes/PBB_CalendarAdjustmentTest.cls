/***
@author 
@date 2015
@description this is PBB_CalendarAdjustment test class
**/

@isTest
private class PBB_CalendarAdjustmentTest {
    static testMethod void testPBB_CalendarAdjustment()    {    
        test.startTest();   
            
        //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils ();
        tu.wfmp = tu.createWfmProject();        
        tu.bid = tu.createBidProject();  
        tu.sm = tu.createSRMModelAttributes();    
        tu.scen =tu.createscenarioAttributes(); 
        tu.country = tu.createCountryAttributes();
        tu.psc = tu.createPBBScenarioCountry();
        tu.scecont = tu.createSrmCountryAttributes();
        system.debug('@@@@@@tu.sm'+tu.sm);
        tu.SCA= tu.createSRMCalendarAdj();
        SRM_Scenario_Country__c scencont1 = tu.createSrmCountryAttributes();  

        PBB_CalendarAdjustment.CalendarAdjWrapper calwrap = new  PBB_CalendarAdjustment.CalendarAdjWrapper();  
        
        SRM_Calender_Adjustments__c caladjst = [select id from SRM_Calender_Adjustments__c];
        PBB_Scenario__c pscen =new PBB_Scenario__c();      
        System.assertEquals(null,PBB_DataAccessor.getScenarioByID(pscen.id)); 
        Apexpages.currentPage().getParameters().put('Id',tu.scen.id);
        pscen =PBB_DataAccessor.getScenarioByID(tu.scen.id); 
        system.debug('@@@@@@pscen '+pscen);
        
        List<SRM_Scenario_Country__c> scencoutryidsList =PBB_DataAccessor.GetPBBWeeklyCountriesbyasc(pscen.id);
        system.debug('@@@@@@scencoutryidsList'+scencoutryidsList);
        List<Id> countryIds = new List<Id>();

        for(SRM_Scenario_Country__c coun: scencoutryidsList ){
            countryIds.add(coun.PBB_Scenario_Country__r.country__c);
            system.debug('@@@@countryIds'+countryIds);
        } 
        List<SRM_Calender_Adjustments__c> calendarlist  = PBB_DataAccessor.GetcalendarAdjData(pscen.Final_Protocol_Date__c,pscen.Last_Patient_In__c,pscen.SRM_Model__c,countryIds);
        system.debug('@@@@@@calendarlist  '+calendarlist);
        PBB_CalendarAdjustment Cal = new PBB_CalendarAdjustment(new Apexpages.Standardcontroller(caladjst)); 
        PBB_CalendarAdjustment.CalendarAdjWrapper calwrap1 = new  PBB_CalendarAdjustment.CalendarAdjWrapper();  
        test.stopTest();
   }       
}