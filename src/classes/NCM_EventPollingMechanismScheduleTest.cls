/**
 * @description	Implements the test for the functions of the NCM_EventPollingMechanismScheduleTest class
 * @author		Kalaiyarasan Karunanithi
 * @version		Created: 30-Sep-2015
 */
@isTest
private class NCM_EventPollingMechanismScheduleTest {
	
	/**
	 * @description	Test the function scheduleMe.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 30-Sep-2015
	 */
	static testMethod void scheduleMeTest() {
		Test.startTest();
		NCM_EventPollingMechanismSchedule.scheduleMe();
		Test.stopTest();
	}
}