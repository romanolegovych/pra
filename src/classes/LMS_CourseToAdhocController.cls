public class LMS_CourseToAdhocController {

    //Instance Variables
    public String commitDate{get;set;}
    public String courseUpdateStart{get;set;}
    public String courseUpdateEnd{get;set;}
    public String roleText{get;set;}
    public String roleFilter{get;set;}
    public String roleTextStyle{get;private set;}
    public String roleResultId{get;private set;}
    public String roleNameDisplay{get;private set;}
    public String courseText{get;set;}
    public String courseTextPackage{get;set;}
    public String courseTextStyle{get;private set;}
    public String courseTextStylePackage{get;private set;}
    public String searchFilter{get;private set;}
    public String searchFilterPackage{get;private set;}
    
    public String assignment{get;set;}
    public String courseType{get;set;}
    public String roleErrorText{get;private set;}
    public String courseErrorText{get;private set;}
    public String webServiceError{get;private set;}
    public Boolean allAdhoc{get;set;}
    public Boolean blnUpdated{get;set;}
    public Boolean allChecked{get;set;}
    public Boolean doSendRole{get;set;}
    public Boolean allCheckedCourse{get;set;}
    public Boolean bCreateRole{get;set;}
    public Boolean bWebServiceError{get;private set;}

    //For impact
    public String impactChange{get;set;}
    public Integer employeeCount{get;set;}
    public Decimal impactStart{get;set;}
    public Decimal impactEnd{get;set;}
    private Decimal impactNum{get;set;}

    //Collections
    public List<LMS_CourseAssignments> courses{get;set;}
    public List<LMS_CourseList> selCourses{get;set;}
    public List<LMS_Role__c> roles{get;set;}
    private Map<String,String> roleResult{get;set;}
    private Map<String,String> roleCommitted{get;set;}
    private Map<String,String> roleDraft{get;set;}
    public List<String> allRoles{get;private set;}
    private Map<String, LMS_Role_Course__c> mappings{get;private set;}
    private Map<String, CourseDomainSettings__c> settings{get;set;}
    private Map<String, LMSConstantSettings__c> constants{get;set;}

    // Error Varaibles
    public List<String> addErrors{get;private set;}
    public List<String> createErrors{get;private set;}
    public List<String> deleteErrors{get;private set;}
    public List<String> calloutErrors{get;private set;}
    public List<String> sfdcErrors{get;private set;}

    /**
     *  Constructor
     *  Initializes all data
     */
    public LMS_CourseToAdhocController() {
        initSettings();
        init();
        checkParams();
    }

    /**
     *  Initialize all lists and other data
     */
    private void init() {
        String internalDomain = settings.get('Internal').Domain_Id__c;
        roleText = 'Enter part of the role name';
        roleFilter = ' and Role_Type__r.Name = \'Additional Role\' and Status__c = \'Active\'';
        searchFilter = 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
    'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
    'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';
       
        searchFilterPackage = 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
    'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
    'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';

        roleTextStyle = 'WaterMarkedTextBox';
        courseText = 'Enter course title';
        courseTextPackage ='Enter package title';
        courseTextStyle = 'WaterMarkedTextBox';
        courseTextStylePackage ='WaterMarkedTextBox';
        impactChange = '0';
        employeeCount = 0;
        impactStart = 0;
        impactEnd = 0;
        doSendRole = true;
        bWebServiceError = false;
    }

    /**
     *  Get constants from custom settings
     */
    private void initSettings() {
        settings = CourseDomainSettings__c.getAll();
        constants = LMSConstantSettings__c.getAll();
    }

    /**
     *  Initialize course search data
     */
    private void initCourseVals() {
        impactChange = '0';
        employeeCount = 0;
        impactStart = 0;
        impactEnd = 0;
    }

    /**
     *  Look for page parameters, set adhoc role name when found
     */
    private void checkParams() {
        String adhocName = ApexPages.currentPage().getParameters().get('adhocName');
        String roleId = ApexPages.currentPage().getParameters().get('roleId');
        if(adhocName != null) { 
            roleText = adhocName;
            search();
        }

        if(roleId != null) {
            LMS_Role__c role = 
            [SELECT Adhoc_Role_Name__c from LMS_Role__c where Id = :roleId];

            if(null != role.Adhoc_Role_Name__c)roleText = role.Adhoc_Role_Name__c;
            allAdhoc = false;
            search();
        }
    }

    /**
     *  Course Types LOV
     */
    public list<selectoption> getCourseTypes() {
        list<selectoption> cType = new list<selectoption>();
            cType.add(new selectoption('Select Course Type','Select Course Type'));
            cType.add(new selectoption('All','All'));
            cType.add(new selectoption('Courses Only','Courses Only'));
            cType.add(new selectoption('SOPs Only','SOPs Only'));
        return cType;
    }

    /**
     *  Closes the list for course search
     */
    public PageReference closeCourseSearch() {
        selCourses = null;
        return null;
    }

    /**
     *  Method to reset role information
     */
    public PageReference roleReset() { 
        initCourseVals();
        roles = null;
        courses = null;
        selCourses = null;
        bWebServiceError = false;
        return null;
    }

    /**
     *  Resets course search data
     */
    public PageReference courseReset() {
        selCourses = null;
        initCourseVals();
        return null;        
    }

    /**
     *  Changes autocomplete filter from drop-down selection
     */
    public PageReference searchType() {
        String internalDomain = settings.get('Internal').Domain_Id__c;
        if(courseType == 'Select Course Type' || courseType == 'All') {
            searchFilter = 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
    'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
    
    searchFilterPackage = 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
    'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';

     
        } else if(courseType == 'Courses Only') {
            searchFilter = 'and Type__c NOT IN (\'PST\',\'SOP\') and Domain_Id__c = \'' + internalDomain + '\' ' + 
    'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
    
    searchFilterPackage = 'and Type__c NOT IN (\'PST\',\'SOP\') and Domain_Id__c = \'' + internalDomain + '\' ' + 
    'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
    
        } else if(courseType == 'SOPs Only') {
            searchFilter = 'and Type__c = \'SOP\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
    'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
    
    searchFilterPackage  = 'and Type__c = \'SOP\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
    'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\')';
    
        }
        return null;
    }

    /**
     *  Role search logic
     */
    public void search() {
        String joinQry,roleQry = '';
        bWebServiceError = false;
        roleResult = new Map<String, String>();
        roleCommitted = new Map<String, String>();
        roleDraft = new Map<String, String>();
        mappings = new Map<String, LMS_Role_Course__c>();

        String draft = constants.get('statusDraft').Value__c;
        String draftDelete = constants.get('statusDraftDelete').Value__c;
        String committed = constants.get('statusCommitted').Value__c;
        String removed = constants.get('statusRemoved').Value__c;

        //Query strings to pull the assignment data for a specific role
        joinQry = 'select Role_Id__r.SABA_Role_PK__c,Role_Id__r.Id,Course_Id__r.Name,Course_Id__r.Course_Code__c,Course_Id__r.Title__c,' + 
    'Course_Id__r.Available_From__c,Course_Id__r.Discontinued_From__c,Course_Id__r.Status__c,Course_Id__r.Duration__c,'  + 
    'Course_Id__r.Target_Days__c,Course_Id__r.Duration_Display__c,Course_Id__r.Id,Course_Id__r.SABA_ID_PK__c,Assigned_On__c,Assigned_By__c,' + 
    'Commit_Date__c,Status__c,Previous_Status__c,Sync_Status__c,LastModifiedDate ' + 
    'from Rolecourses__r WHERE Status__c != :removed ORDER BY Status__c,Course_Id__r.Title__c';
        roleQry = 'select Adhoc_Role_Name__c,SABA_Role_PK__c,Status__c,Sync_Status__c,Employee_Count__c,('+joinQry+') from LMS_Role__c where Adhoc_Role_Name__c != null ' + 
    'and Role_Type__r.Name = \'Additional Role\'';

        if((roleText != '' && roleText != 'Enter part of the role name') || allAdhoc == false) {
            roleQry += ' and Adhoc_Role_Name__c=:roleText';
        }
        roleQry += ' ORDER BY Role_Name__c';
        System.debug('--------------------------query------------------------'+roleQry);
        allChecked = false;
        //Query the database
        roles = Database.query(roleQry);
        if(roles.size() == 0) {
            bCreateRole = true;
            roleErrorText = 'No role name found, Would you like to create role?';
        } else if(roles.size() == 1 && allAdhoc == false) {
            courses = new List<LMS_CourseAssignments>();
            for(LMS_Role__c r : roles) {
                roleNameDisplay = r.Adhoc_Role_Name__c;
                roleResultId = r.Id;
                for(LMS_Role_Course__c c : r.roleCourses__r) {
                    System.debug('--------------------mappings---------------------'+c);
                    mappings.put(c.Id, c);
                    roleResult.put(c.Course_Id__c, c.Course_Id__c);
                    if(c.Status__c == committed || c.Status__c == draftDelete) {
                        roleCommitted.put(c.Course_Id__c, c.Course_Id__c);
                    } else if(c.Status__c == draft) {
                        roleDraft.put(c.Course_Id__c, c.Course_Id__c);
                    }
                    courses.add(new LMS_CourseAssignments(c));
                }
            }
            if(roles[0].SABA_Role_PK__c != '') {
                bCreateRole = false;
                roleErrorText = 'Role is out of Sync with SABA, courses can be added but not committed until sync is resolved';
            }
            if(roles[0].Status__c == 'Inactive') {
                bCreateRole = false;
                roleErrorText = 'Role is inactive, please enter different role name';
            }
        } else if(roles.size() > 1) {
            bCreateRole = false;
            allRoles = new List<String>();
            for(LMS_Role__c r : roles) {
                allRoles.add(r.Adhoc_Role_Name__c);
            }
        }
        viewImpact(); 
    }

    /**
     *  Person search logic
     */
    public void courseSearch() {
        //Build the query for the course search
        String courseSearchQry='';
        Datetime updateStart, updateEnd;
        Set<String> cAssigned = new Set<String>();
        String internalDomain = settings.get('Internal').Domain_Id__c;

        if(roleCommitted != null) {
            cAssigned = roleCommitted.keySet();
        }

        courseSearchQry = 'select Name,Course_Code__c,Title__c,Duration__c,Target_Days__c,Duration_Display__c,Available_From__c,Discontinued_From__c,' + 
            'Status__c,SABA_ID_PK__c from LMS_Course__c where Id not in:cAssigned and Domain_Id__c = \'' + String.escapeSingleQuotes(internalDomain) + 
            '\' and (Discontinued_From__c > TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
            'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')';
        allCheckedCourse = false;

        // Parse dates from UI if present
        if(courseUpdateStart != null && courseUpdateStart != '') {
            System.debug('--------------------start date-------------------'+courseUpdateStart);
            Date updatedDate = Date.parse(courseUpdateStart);
            updateStart = Datetime.newInstance(updatedDate, Time.newInstance(0,0,0,0));
            System.debug('-------------------start datetime---------------'+updateStart);
            courseSearchQry += ' and LastModifiedDate > :updateStart';
        }
        if(courseUpdateEnd != null && courseUpdateEnd != '') {
            System.debug('--------------------end date-------------------'+courseUpdateEnd);
            Date updatedDate = Date.parse(courseUpdateEnd);
            updateEnd = Datetime.newInstance(updatedDate, Time.newInstance(0,0,0,0));
            System.debug('-------------------start datetime---------------'+updateEnd);
            courseSearchQry += ' and LastModifiedDate < :updateEnd';
        }

        //Determine if search is based on SOPs, Courses or All courses
        if(courseType == 'Select Course Type' || courseType == 'All') {
            courseSearchQry += ' and Type__c != \'PST\'';
        } else if(courseType == 'Courses Only') {
            courseSearchQry += ' and Type__c NOT IN (\'PST\',\'SOP\')';
        } else if(courseType == 'SOPs only') {
            courseSearchQry += ' and Type__c = \'SOP\'';
        }

        //Search on Course Text
        if(courseText != 'Enter course title' && courseText != '') {
            courseSearchQry += ' and Title__c like \'' + String.escapeSingleQuotes(courseText.trim()) + '%\'';
        }
        
        //Search on Package Text
        if(courseTextPackage != 'Enter package title' && courseTextPackage != '') {
            courseSearchQry += ' and Course_Package__c like \'' + String.escapeSingleQuotes(courseTextPackage.trim()) + '%\'';
        }
        

        //Search on Updated within 30 days
        if(blnUpdated != false) {
            Datetime updated = System.now()-30;
            courseSearchQry += ' and Updated_On__c >:updated';
        }

        System.debug('------------------query------------------------'+courseSearchQry);
        //Run query and perform changes to error messages based on results
        List<LMS_Course__c> selectCourses = Database.query(courseSearchQry);
        if(selectCourses.size() > 1000) {
            selectCourses = null;
            courseErrorText = 'The result contains too many records. Please refine your search criteria and run the search again.';
        } else if(selectCourses.size() == 0) {
            selectCourses = null;
            courseErrorText = 'The result does not contain any records. Please refine your search criteria and run the search again.';
        } else if(selectCourses.size() > 0) {
            selCourses = new List<LMS_CourseList>();
            for(LMS_Course__c c : selectCourses){
                selCourses.add(new LMS_CourseList(c, roleResult));
            }
        }
    }

    /**
     *  Create role with selected criteria
     */
    public PageReference createRole() {
        sfdcErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();
        List<String> values = new List<String>{roleText.trim()};

        if(isRoleDataValid() && LMS_ToolsDataAccessor.isRoleValid('Additional Role', values)){
            errors = LMS_ToolsModifyRoles.createRole(values, 3);
            doSendRole = true;
            sfdcErrors = errors.get('SFDCEX');
        } else {
            webServiceError = 'You cannot change the search criteria during the role creation process.  Please click reset button and search again.';
            doSendRole = false;
            bWebServiceError = true;
        }

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        }

        if(doSendRole) {
            search();
            viewImpact();
        } 
        return null;
    }

    private Boolean isRoleDataValid() {
        Boolean isValid = true;
        if(roleText == null || roleText == '' || roleText == 'Enter part of the role name')
            return !isValid;
        return isValid;
    }

    public PageReference cancelCreateRole() {
        roles = null;
        return null;
    }

    /**
     *  Send role data to SABA
     */
    public PageReference sendRoleData() {
        createErrors = new List<String>();
        calloutErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        if(doSendRole) {
            LMS_Role__c role = [SELECT Id, Role_Name__c, Status__c, SABA_ROLE_PK__c, Role_Type__r.Name FROM LMS_Role__c where Id =:roleResultId];
            errors = LMS_ToolsService.sendRoleData(role);
            createErrors = errors.get('ROLE');
            calloutErrors = errors.get('CALLOUT');        
            System.debug('---------------------errors-----------------------'+errors);
            System.debug('---------------------create errors-----------------------'+createErrors);
            System.debug('---------------------callout errors-----------------------'+calloutErrors);
        }

        if((null != createErrors && 0 < createErrors.size()) || (null != calloutErrors && 0 < calloutErrors.size())) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        }

        if(doSendRole)
            search();
        return null;
    }

    /**
     *  Add course to role with
     *  an assignment status of Draft
     */
    public PageReference addCourse() {
        sfdcErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.addCourses(selCourses, courses, mappings, roleResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while adding course to role.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        courseSearch();
        viewImpact(); 
        return null;
    }

    /**
     *  Remove person from the selected role
     */
    public PageReference removeCourses(){
        sfdcErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.removeCourses(courses, mappings);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred while removing or reverting courses from role.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact(); 
        return null;
    }

    /**
     *  Remove all changes made to role assignments
     */
    public PageReference cancel() {
        sfdcErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.cancelDraftCourses(roleResultId);
        sfdcErrors = errors.get('SFDCEX');

        if((null != sfdcErrors && sfdcErrors.size() > 0)) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }

        search();
        courseSearch();
        viewImpact(); 
        return null;
    }

    public PageReference setCommitDate() {

        sfdcErrors = new List<String>();
        addErrors = new List<String>();
        deleteErrors = new List<String>();
        calloutErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        Date dateCommit = Date.parse(commitDate);
        errors = LMS_ToolsModifyAssignments.setCommitDateCourses(courses, mappings, dateCommit);

        sfdcErrors = errors.get('SFDCEX');
        System.debug('-------------errors------------'+errors);
        System.debug('-------------sfdcErrors-----------'+sfdcErrors);

        if(null != sfdcErrors && sfdcErrors.size() > 0) {
            webServiceError = 'Errors occurred while applying commit date.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact();
        return null;
    }

    /**
     *  Commit changes, send data to SABA
     */
    public PageReference commitCourses() {
        sfdcErrors = new List<String>();
        addErrors = new List<String>();
        deleteErrors = new List<String>();
        calloutErrors = new List<String>();
        Map<String, List<String>> errors = new Map<String, List<String>>();

        errors = LMS_ToolsModifyAssignments.commitCourses(courses, mappings);
        sfdcErrors = errors.get('SFDCEX');
        addErrors = errors.get('A');
        deleteErrors = errors.get('D');
        calloutErrors = errors.get('CALLOUT');

        System.debug('-------------errors------------'+errors);
        System.debug('-------------sfdcErrors------------'+sfdcErrors);
        System.debug('-------------addErrors------------'+addErrors);
        System.debug('-------------deleteErrors------------'+deleteErrors);
        System.debug('-------------calloutErrors------------'+calloutErrors);

        if((null != sfdcErrors && sfdcErrors.size() > 0) || (null != addErrors && 0 < addErrors.size()) || 
            (null != deleteErrors && 0 < deleteErrors.size()) || (null != calloutErrors && 0 < calloutErrors.size())) {
            webServiceError = 'Errors occurred during communication to SABA.  Click link to view error log.';
            bWebServiceError = true;
        } else {
            bWebServiceError = false;
        }
        
        boolean didError = bWebServiceError;

        search();
        bWebServiceError = didError;
        viewImpact(); 
        return null;
    }

    /**
     *  View the impact of adding people to the role
     */
    public PageReference viewImpact() {
        impactNum = 0;
        impactEnd = 0;   
        impactStart = 0;    
        impactChange = '0';

        List<String> courseIds = new List<String>();
        // Get employee count and total hour count
        if(roles != null && roles.size() > 0) {
            employeeCount = Integer.valueOf(roles[0].Employee_Count__c);
        } else {
            employeeCount = 0;
        }
        if(roleResultId != null && roleResultId != '') {
            Decimal duration = LMS_ToolsDataAccessor.getTotalCourseDurationForCommittedMappings(roleResultId);
            if(duration != null) {
                impactStart = (duration*employeeCount/60).setScale(2);
            } else {
                impactStart = 0;
            }
        }
        // Get the course Ids of the current select courses
        if(selCourses != null) {
            for(LMS_CourseList c : selCourses) {
                if(c.selected && !roleResult.containsKey(c.courseId)) {
                    System.debug('-----------------c.selected--------------'+c.courseId);
                    System.debug('-----------------roleDraftKey--------------'+roleResult.get(c.courseId));
                    courseIds.add(c.courseId);
                }
            }
        }
        if(courses != null) {            
            // Get the durations for the adding and removal of courses
            Double clToAdd = LMS_ToolsDataAccessor.getTotalCourseDurationForAddMappings(roleResultId);
            Double clToRemove = LMS_ToolsDataAccessor.getTotalCourseDurationForDeleteMappings(roleResultId);
            Double cToAdd = LMS_ToolsDataAccessor.getTotalCourseDurationFromCourseList(courseIds);
            impactNum = 0;

            if(clToAdd != null) {
                impactNum += clToAdd;
                System.debug('---------total additions from course list--------------'+clToAdd);
            }
            if(courseIds.size() > 0) {
                if(cToAdd != null) {
                    impactNum += cToAdd;
                    System.debug('---------total from selCourse list--------------'+cToAdd);
                }
            }
            if(clToRemove != null) {
                impactNum -= clToRemove;
                System.debug('---------total deletions from course list--------------'+clToRemove);
            }
            impactNum *= employeeCount;
            impactNum = (impactNum/60).setScale(2);

            if(impactNum > 0) {
                impactChange = '+ '+impactNum;
            } else {
                impactChange = String.valueOf(impactNum);
            }

            impactEnd = (impactStart + impactNum).setScale(2);
        } else {
            employeeCount = 0;
            impactStart = 0;
            impactNum = 0;
            impactChange = String.valueOf(impactNum);
            impactEnd = impactStart + impactNum;
        }

        return null;
    }

    public PageReference showErrors() {
        PageReference pr = new PageReference('/apex/LMS_CourseAdhocError');
        pr.setRedirect(false);
        return pr;
    }

}