@isTest
private class BDT_FinancialDocDownloadCOControllerTest {
	
	public static FinancialDocument__c changeOrder;
	
	static void init(){
		//servicecategories
		List<ServiceCategory__c> categoryList = BDT_CreateTestData.buildServiceCategories();
		//services
		List<Service__c> serviceList = BDT_CreateTestData.buildServices(categoryList);
		// Create project
		Client_Project__c firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		// Create studies
		List<Study__c>  studiesList = BDT_TestDataUtils.buildStudies(firstProject,1);
		insert studiesList;
		// create a set of project services
		List<ProjectService__c> prosSrv = new List<ProjectService__c>();
		prosSrv.add(new ProjectService__c(Service__c = serviceList[0].id, Client_Project__c = firstProject.id));
		upsert prosSrv;
		// Select Data for the test
		BDT_Utils.setPreference('SelectedProject', String.valueOf(firstProject.id));
		String StudyIDs = '';
		for (Study__c st : studiesList) {
			StudyIDs = StudyIDs + ':' + st.id;
		}
		BDT_Utils.setPreference('SelectedStudies', StudyIDs.removeStart(':'));
		// add services
		List<StudyService__c> ss = new List<StudyService__c>();
		for (ProjectService__c ps:prosSrv){
			StudyService__c s = new StudyService__c();
			s.ProjectService__c = ps.id;
			s.NumberOfUnits__c = 2;
			s.Study__c = StudiesList[0].id;
			ss.add(s);
		}
		upsert ss;
    	// Create the new parent proposal, with the predefined attributes values   	
		FinancialDocument__c financialDocument = new FinancialDocument__c();
		financialDocument.Name = 'Test name';
		financialDocument.DocumentStatus__c = ( StudiesList.size()>0 )? 'New' : 'Undefined';
		financialDocument.DocumentType__c   = 'Proposal';
		financialDocument.Client_Project__c = firstproject.id;
		financialDocument.listOfStudyIds__c = StudyIds;
		insert financialDocument; 
		// Create the child change order
		changeOrder = new FinancialDocument__c();
		changeOrder.Name = 'Change Order Test name';
		changeOrder.DocumentStatus__c = ( StudiesList.size()>0 )? 'New' : 'Undefined';
		changeOrder.DocumentType__c   = 'Change Order';
		changeOrder.Client_Project__c = firstproject.id;
		changeOrder.listOfStudyIds__c = StudyIds;
		changeOrder.FinancialDocumentParent__c = financialDocument.Id;
		insert changeOrder;
		// Associate with study services
		list<StudyServicePricing__c> sspList = [Select s.UnitPricePRAProposal__c, s.UnitPriceNegotiated__c, s.SystemModstamp, s.StudyService__c, s.Service__c, s.RateCardPrice__c, s.RateCardCurrency__c, s.RateCardClass__c, s.NumberOfUnits__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsDeleted, s.Id, s.FinancialDocumentIDs__c, s.DiscountOnTotal__c, s.CreatedDate, s.CreatedById, s.CalculatedTotalPrice__c, s.CalculatedDiffToRateCard__c, s.ApprovedFinancialDocument__c From StudyServicePricing__c s limit 1];
		for (StudyServicePricing__c ssp:sspList){
			ssp.FinancialDocumentIDs__c = financialDocument.id + ':' + changeOrder.id;
		}
		upsert sspList;
	}

	static testMethod void myUnitTest() {
		init();
		// no document
		BDT_FinancialDocDownloadCOController pageController = new BDT_FinancialDocDownloadCOController();
		// document set
		PageReference b = new pagereference(System.Page.BDT_FinancialDocDownload.getURL() );
		b.getParameters().put('financialDocumentID',''+changeOrder.id);
		test.setCurrentPage(b);
		pageController = new BDT_FinancialDocDownloadCOController();
	}
	
}