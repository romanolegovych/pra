public with sharing class PAWS_FlowInstanceCursorTrigger extends STSWR1.AbstractTrigger
{
	public override void beforeInsert(List<SObject> records)
	{
		try
		{
			fillAssignedToName((List<STSWR1__Flow_Instance_Cursor__c>)records, null);
		}catch(Exception ex)
		{
			onError(records, ex.getMessage());
		}
	}
	
	public override void afterInsert(List<SObject> records)
	{
		System.debug('IGNORE_PAWS_TRIGGERS: ' + PAWS_Utilities.IGNORE_PAWS_TRIGGERS);
		if(PAWS_Utilities.IGNORE_PAWS_TRIGGERS == true) return;
		
		new PAWS_StepProperty_LinkedStepHelper().CursorAfterInsert((List<STSWR1__Flow_Instance_Cursor__c>) records);
	}
	
	public override void beforeUpdate(List<SObject> records, List<SObject> oldRecords)
	{
		try
		{
			fillAssignedToName((List<STSWR1__Flow_Instance_Cursor__c>)records, (List<STSWR1__Flow_Instance_Cursor__c>)oldRecords);
		}catch(Exception ex)
		{
			onError(records, ex.getMessage());
		}
	}
	
	public override void afterUpdate(List<SObject> records, List<SObject> oldRecords)
	{
		System.debug('IGNORE_PAWS_TRIGGERS: ' + PAWS_Utilities.IGNORE_PAWS_TRIGGERS);
		if(PAWS_Utilities.IGNORE_PAWS_TRIGGERS == true) return;

		new PAWS_StepProperty_LinkedStepHelper().CursorAfterUpdate((List<STSWR1__Flow_Instance_Cursor__c>) records, (List<STSWR1__Flow_Instance_Cursor__c>) oldRecords);
	}
	
	private void fillAssignedToName(List<STSWR1__Flow_Instance_Cursor__c> records, List<STSWR1__Flow_Instance_Cursor__c> oldrecords)
	{
		Boolean needUpdate = false;
		for(Integer i = 0; i < records.size(); i++)
		{
			if(oldrecords == null || records[i].STSWR1__Step_Assigned_To__c != oldrecords[i].STSWR1__Step_Assigned_To__c)
			{
				needUpdate = true;break;
			}
		}
		
		if(!needUpdate) return;
		
		Map<ID, String> assigners = PAWS_Utilities.loadAssigners(records);
		for(Integer i = 0; i < records.size(); i++)
		{
			if(records[i].STSWR1__Step_Assigned_To__c instanceof ID) records[i].Step_Assigned_To_Name__c = assigners.get(records[i].STSWR1__Step_Assigned_To__c);
		}
	}
	
	public void onError(List<SObject> records, String message)
	{
		for (SObject each : records)
		{
			each.addError(message);
		}
	}
}