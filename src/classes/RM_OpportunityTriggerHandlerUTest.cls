/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_OpportunityTriggerHandlerUTest {
	static WFM_Client__c client;
	static WFM_Project__c project;
	static WFM_Project__c projectB;
	
	static void init(){
		 //need to insert project
        client = new WFM_Client__c(name='TestSpouser1', Client_Name__c='TestSpouser1', Client_Unique_Key__c='TestSpouser1');
        insert client;
        
        project = new WFM_Project__c(name='12345678-123125', wfm_client__c=client.id, Project_Unique_Key__c='12345678-123125', project_status__c ='RM',  Status_Desc__c='Active');
        insert project;
        projectB = new WFM_Project__c(name='12345678-123126', wfm_client__c=client.id, Project_Unique_Key__c='12345678-123126', project_status__c ='BB',  Status_Desc__c='Bid');
        insert projectB;
	}
    static testMethod void TestInsertOpp() {
         
         Account oneAccount = new Account(name='TestSpouser');
        insert oneAccount;
         Test.startTest();
         
    	 Opportunity opp = new Opportunity();
		    opp.Name='Testing Opportunity';
		    opp.AccountID= oneAccount.id;
		    opp.StageName='Ballpark Sent';
		    opp.Type='New Business';
		    opp.PRA_Project_ID__c='12345678-123123';
		    opp.Business_Unit__c='PR-EU';
		    opp.Phase__c='Phase I';
		    opp.Therapeutic_Area__c='Dentistry';
		    opp.Indication_Group__c='Dentistry Group';
		    opp.Study_Start_Date__c= Date.today()+60;
		    opp.CloseDate= Date.today()+30;
		    system.debug('------------opp before insert------------'+opp);
		    insert opp;
		    
		Test.stopTest();  
		   
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj[0].name, opp.PRA_Project_ID__c);
        list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstB--------------'+lstB);
         system.assertEquals(lstB[0].name, opp.PRA_Project_ID__c);
    }
    static testMethod void TestInsertOppWithExistClient() {
         init();
         Account oneAccount = new Account(name='TestSpouser1');
        insert oneAccount;
         Test.startTest();
         
    	 Opportunity opp = new Opportunity();
		    opp.Name='Testing Opportunity';
		    opp.AccountID= oneAccount.id;
		    opp.StageName='Ballpark Sent';
		    opp.Type='New Business';
		    opp.PRA_Project_ID__c='12345678-123123';
		    opp.Business_Unit__c='PR-EU';
		    opp.Phase__c='Phase I';
		    opp.Therapeutic_Area__c='Dentistry';
		    opp.Indication_Group__c='Dentistry Group';
		    opp.Study_Start_Date__c= Date.today()+60;
		    opp.CloseDate= Date.today()+30;
		    system.debug('------------opp before insert------------'+opp);
		    insert opp;
		    
		Test.stopTest();  
		   
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj[0].name, opp.PRA_Project_ID__c);
        list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstB--------------'+lstB);
         system.assertEquals(lstB[0].name, opp.PRA_Project_ID__c);
        
    }
    static testMethod void TestInsertOppTypeNotProject() {
        
         Account oneAccount = new Account(name='TestSpouser');
        insert oneAccount;
         Test.startTest();
         
    	 Opportunity opp = new Opportunity();
		    opp.Name='Testing Opportunity';
		    opp.AccountID= oneAccount.id;
		    opp.StageName='Ballpark Sent';
		    opp.Type='Change Order';
		    opp.PRA_Project_ID__c='12345678-123123';
		    opp.Business_Unit__c='PR-EU';
		    opp.Phase__c='Phase I';
		    opp.Therapeutic_Area__c='Dentistry';
		    opp.Indication_Group__c='Dentistry Group';
		    opp.Study_Start_Date__c= Date.today()+60;
		    opp.CloseDate= Date.today()+30;
		    system.debug('------------opp before insert------------'+opp);
		    insert opp;
		    
		  
		 Test.stopTest();   
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj.size(), 0);
        list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstB--------------'+lstB);
        system.assertEquals(lstB.size(), 0);
    }
     static testMethod void TestInsertOppNoProjNameNotProject() {
         
         Account oneAccount = new Account(name='TestSpouser');
        insert oneAccount;
         Test.startTest();
         
    	 Opportunity opp = new Opportunity();
		    opp.Name='Testing Opportunity';
		    opp.AccountID= oneAccount.id;
		    opp.StageName='Ballpark Sent';
		    opp.Type='New Business';
		   // opp.PRA_Project_ID__c='12345678-123123';
		    opp.Business_Unit__c='PR-EU';
		    opp.Phase__c='Phase I';
		    opp.Therapeutic_Area__c='Dentistry';
		    opp.Indication_Group__c='Dentistry Group';
		    opp.Study_Start_Date__c= Date.today()+60;
		    opp.CloseDate= Date.today()+30;
		    system.debug('------------opp before insert------------'+opp);
		    insert opp;
		    
		 Test.stopTest();  
		   
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj.size(), 0);
        list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstB--------------'+lstB);
        system.assertEquals(lstB.size(), 0);
        
    }
      static testMethod void TestInsertOppNotinPR() {
         
         Account oneAccount = new Account(name='TestSpouser');
        insert oneAccount;
         Test.startTest();
         
         Opportunity opp = new Opportunity();
            opp.Name='Testing Opportunity';
            opp.AccountID= oneAccount.id;
            opp.StageName='Ballpark Sent';
            opp.Type='New Business';
            opp.PRA_Project_ID__c='12345678-123123';
            opp.Business_Unit__c='SS-EU';
            opp.Phase__c='Phase I';
            opp.Therapeutic_Area__c='Dentistry';
            opp.Indication_Group__c='Dentistry Group';
            opp.Study_Start_Date__c= Date.today()+60;
            opp.CloseDate= Date.today()+30;
            system.debug('------------opp before insert------------'+opp);
            insert opp;
            
         Test.stopTest();  
           
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj.size(), 1);
        list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstB--------------'+lstB);
        system.assertEquals(lstB.size(), 0);
        
    }
     static testMethod void TestInsertOppNoProjNameThenUpdateProject() {
         
         Account oneAccount = new Account(name='TestSpouser');
        insert oneAccount;
         
         
         Opportunity opp = new Opportunity();
            opp.Name='Testing Opportunity';
            opp.AccountID= oneAccount.id;
            opp.StageName='Ballpark Sent';
            opp.Type='New Business';
           // opp.PRA_Project_ID__c='12345678-123123';
            opp.Business_Unit__c='PR-EU';
            opp.Phase__c='Phase I';
            opp.Therapeutic_Area__c='Dentistry';
            opp.Indication_Group__c='Dentistry Group';
            opp.Study_Start_Date__c= Date.today()+60;
            opp.CloseDate= Date.today()+30;
            system.debug('------------opp before insert------------'+opp);
            insert opp;
            Test.startTest();
            opp.PRA_Project_ID__c='12345678-123124';
            update opp;
            Test.stopTest();  
           
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj[0].name, opp.PRA_Project_ID__c);
        list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstB--------------'+lstB);
         system.assertEquals(lstB[0].name, opp.PRA_Project_ID__c);
        
    }
     static testMethod void TestOppUpdatewithNewProjName() {
         
         Account oneAccount = new Account(name='TestSpouser');
        insert oneAccount;
       
    	 Opportunity opp = new Opportunity();
		    opp.Name='Testing Opportunity';
		    opp.AccountID= oneAccount.id;
		    opp.StageName='Ballpark Sent';
		    opp.Type='New Business';
		  	opp.PRA_Project_ID__c='12345678-123126';
		    opp.Business_Unit__c='PR-EU';
		    opp.Phase__c='Phase I';
		    opp.Therapeutic_Area__c='Dentistry';
		    opp.Indication_Group__c='Dentistry Group';
		    opp.Study_Start_Date__c= Date.today()+60;
		    opp.CloseDate= Date.today()+30;
		    
		    insert opp;
		    system.debug('------------opp after insert------------'+opp);
		Test.startTest();  
		opp.PRA_Project_ID__c='12345678-123124';
		update opp;
		Test.stopTest();
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj[0].name, opp.PRA_Project_ID__c);
       	list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstB--------------'+lstB);
         system.assertEquals(lstB[0].name, opp.PRA_Project_ID__c);
    }
     static testMethod void TestOppUpdateNoProject() {
         
         Account oneAccount = new Account(name='TestSpouser');
        insert oneAccount;
       
         Opportunity opp = new Opportunity();
            opp.Name='Testing Opportunity';
            opp.AccountID= oneAccount.id;
            opp.StageName='Ballpark Sent';
            opp.Type='New Business';
            opp.PRA_Project_ID__c='12345678-123126';
            opp.Business_Unit__c='PR-EU';
            opp.Phase__c='Phase I';
            opp.Therapeutic_Area__c='Dentistry';
            opp.Indication_Group__c='Dentistry Group';
            opp.Study_Start_Date__c= Date.today()+60;
            opp.CloseDate= Date.today()+30;
            
            insert opp;
            system.debug('------------opp after insert------------'+opp);
        Test.startTest();  
        list<WFM_Project__c> lstProjD = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProjD--------------'+lstProjD);
        delete lstProjD;
       
        opp.PRA_Project_ID__c='12345678-123124';
        update opp;
        Test.stopTest();
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj[0].name, opp.PRA_Project_ID__c);
        list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstB--------------'+lstB);
         system.assertEquals(lstB[0].name, opp.PRA_Project_ID__c);
    }
     static testMethod void TestOppUpdateNoBidProject() {
         
         Account oneAccount = new Account(name='TestSpouser');
        insert oneAccount;
       
         Opportunity opp = new Opportunity();
            opp.Name='Testing Opportunity';
            opp.AccountID= oneAccount.id;
            opp.StageName='Ballpark Sent';
            opp.Type='New Business';
            opp.PRA_Project_ID__c='12345678-123126';
            opp.Business_Unit__c='PR-EU';
            opp.Phase__c='Phase I';
            opp.Therapeutic_Area__c='Dentistry';
            opp.Indication_Group__c='Dentistry Group';
            opp.Study_Start_Date__c= Date.today()+60;
            opp.CloseDate= Date.today()+30;
            
            insert opp;
            system.debug('------------opp after insert------------'+opp);
        Test.startTest();  
        list<Bid_Project__c> lstBD = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProjD--------------'+lstBD);
        delete lstBD;
       
        opp.PRA_Project_ID__c='12345678-123124';
        update opp;
        Test.stopTest();
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj[0].name, opp.PRA_Project_ID__c);
        list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstB--------------'+lstB);
         system.assertEquals(lstB[0].name, opp.PRA_Project_ID__c);
    }
    static testMethod void TestUpdateOppChangeStatus() {
         
         Account oneAccount = new Account(name='TestSpouser');
        insert oneAccount;
         
         
    	 Opportunity opp = new Opportunity();
		    opp.Name='Testing Opportunity';
		    opp.AccountID= oneAccount.id;
		    opp.StageName='Ballpark Sent';
		    opp.Type='New Business';
		    opp.PRA_Project_ID__c='12345678-123123';
		    opp.Business_Unit__c='PR-EU';
		    opp.Phase__c='Phase I';
		    opp.Therapeutic_Area__c='Dentistry';
		    opp.Indication_Group__c='Dentistry Group';
		    opp.Study_Start_Date__c= Date.today()+60;
		    opp.CloseDate= Date.today()+30;
		    system.debug('------------opp before insert------------'+opp);
		    insert opp;
		    
		Test.startTest();  
		opp.StageName='Closed Lost';   
		opp.CloseDate = Date.today();
		opp.Description='Lost';
    	opp.X1_Reason__c='PRA Project Team';
		update opp;
		  Test.stopTest();
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj[0].Status_Desc__c, 'Lost'); 
        list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstB--------------'+lstB);
        system.assertEquals(lstB[0].BID_Stage__c, opp.StageName);
    }
    static testMethod void TestUpdateOppChangeProjectName() {
       
         Account oneAccount = new Account(name='TestSpouser');
        insert oneAccount;
         
         
    	 Opportunity opp = new Opportunity();
		    opp.Name='Testing Opportunity';
		    opp.AccountID= oneAccount.id;
		    opp.StageName='Ballpark Sent';
		    opp.Type='New Business';
		    opp.PRA_Project_ID__c='12345678-123123';
		    opp.Business_Unit__c='PR-EU';
		    opp.Phase__c='Phase I';
		    opp.Therapeutic_Area__c='Dentistry';
		    opp.Indication_Group__c='Dentistry Group';
		    opp.Study_Start_Date__c= Date.today()+60;
		    opp.CloseDate= Date.today()+30;
		    system.debug('------------opp before insert------------'+opp);
		    insert opp;
		    
		Test.startTest();  
		opp.PRA_Project_ID__c='12345678-New123';   
		
		update opp;
		 Test.stopTest();
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj[0].name, opp.PRA_Project_ID__c); 
        list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstB--------------'+lstB);
         system.assertEquals(lstB[0].name, opp.PRA_Project_ID__c);
    }
     static testMethod void TestUpdateOppChangeOther() {
         
         Account oneAccount = new Account(name='TestSpouser');
        insert oneAccount;
         
         
    	 Opportunity opp = new Opportunity();
		    opp.Name='Testing Opportunity';
		    opp.AccountID= oneAccount.id;
		    opp.StageName='Ballpark Sent';
		    opp.Type='New Business';
		    opp.PRA_Project_ID__c='12345678-123123';
		    opp.Business_Unit__c='PR-EU';
		    opp.Phase__c='Phase I';
		    opp.Therapeutic_Area__c='Dentistry';
		    opp.Indication_Group__c='Dentistry Group';
		    opp.Primary_Indication__c = 'Primary1';
		    opp.Bid_Defense_Date__c = Date.today() +40;
		    opp.Native_Contract_Amount__c = 10;
		    opp.Study_Start_Date__c= Date.today()+60;
		    opp.CloseDate= Date.today()+30;
		    system.debug('------------opp before insert------------'+opp);
		    insert opp;
		    
		Test.startTest();  
		opp.Therapeutic_Area__c='Gastroenterology';  
		opp.Indication_Group__c='Indication Group1';
		
		opp.Study_End_Date__c = Date.today() + 180;
		opp.split_Sites_EAPA__c = 10;
		update opp;
		 Test.stopTest();
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj[0].project_End_Date__c, opp.Study_End_Date__c); 
         list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstB--------------'+lstB);
        system.assertEquals(lstB[0].split_Sites_EAPA__c, opp.split_Sites_EAPA__c);
    }
     static testMethod void TestUpdateOppChangeforBidProject() {
         
         Account oneAccount = new Account(name='TestSpouser');
        insert oneAccount;
         
         
         Opportunity opp = new Opportunity();
            opp.Name='Testing Opportunity';
            opp.AccountID= oneAccount.id;
            opp.StageName='Ballpark Sent';
            opp.Type='New Business';
            opp.PRA_Project_ID__c='12345678-123123';
            opp.Business_Unit__c='PR-EU';
            opp.Phase__c='Phase I';
            opp.Therapeutic_Area__c='Dentistry';
            opp.Indication_Group__c='Dentistry Group';
            opp.Primary_Indication__c = 'Primary1';
            opp.Bid_Defense_Date__c = Date.today() +40;
            opp.Native_Contract_Amount__c = 10;
            opp.Study_Start_Date__c= Date.today()+60;
            opp.CloseDate= Date.today()+30;
            opp.split_Sites_EAPA__c = 10;
            opp.split_Sites_NA__c = 10;
            opp.Expected_Patients__c = 10;
            opp.Expected_Sites__c = 10;
            opp.RFP_1st_Patient_In_Date__c = Date.today() + 100;
            opp.RFP_Last_Patient_In_Date__c = Date.today() + 200;
            opp.Proposal_Due_Date__c = Date.today() +10;
            opp.RFP_Expected_Date__c = Date.today() + 20;
            opp.RFP_Received_Date__c = Date.today() + 30;
            opp.Protocol_Number__c = 'TEST';
            system.debug('------------opp before insert------------'+opp);
            insert opp;
            
        Test.startTest();  
        
        opp.split_Sites_EAPA__c = 30;
        update opp;
         Test.stopTest();
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj[0].project_End_Date__c, opp.Study_End_Date__c); 
         list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstB--------------'+lstB);
        system.assertEquals(lstB[0].split_Sites_EAPA__c, opp.split_Sites_EAPA__c);
    }
   
    static testMethod void TestInsertDoubleOppWithTheSameAccount() {
        
         WFM_Client__c client1 = new WFM_Client__c(name='TestSpouser', Client_Name__c='TestSpouser', Client_Unique_Key__c='TestSpouser');
         insert client1; 
         Account oneAccount = new Account(name='TestSpouser');
        insert oneAccount;
         Test.startTest();
         
    	 Opportunity opp = new Opportunity();
		    opp.Name='Testing Opportunity';
		    opp.AccountID= oneAccount.id;
		    opp.StageName='Ballpark Sent';
		    opp.Type='New Business';
		    opp.PRA_Project_ID__c='12345678-123123';
		    opp.Business_Unit__c='PR-EU';
		    opp.Phase__c='Phase I';
		    opp.Therapeutic_Area__c='Dentistry';
		    opp.Indication_Group__c='Dentistry Group';
		    opp.Study_Start_Date__c= Date.today()+60;
		    opp.CloseDate= Date.today()+30;
		    system.debug('------------opp before insert------------'+opp);
		    insert opp;
		Opportunity opp1 = new Opportunity();
		    opp1.Name='Testing Opportunity1';
		    opp1.AccountID= oneAccount.id;
		    opp1.StageName='Ballpark Sent1';
		    opp1.Type='New Business';
		    opp1.PRA_Project_ID__c='12345678-123124';
		    opp1.Business_Unit__c='PR-EU';
		    opp1.Phase__c='Phase II';
		    opp1.Therapeutic_Area__c='Dentistry';
		    opp1.Indication_Group__c='Dentistry Group';
		    opp1.Study_Start_Date__c= Date.today()+60;
		    opp1.CloseDate= Date.today()+30;
		    system.debug('------------opp before insert 2------------'+opp);
		    insert opp1;    
		Test.stopTest();  
		   
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj[0].name, opp.PRA_Project_ID__c);
        
    }
     static testMethod void TestDeleteOpp() {
         init();     
         Account oneAccount = new Account(name='TestSpouser');
         insert oneAccount;
         
        
         Opportunity opp = new Opportunity();
            opp.Name='Testing Opportunity';
            opp.AccountID= oneAccount.id;
            opp.StageName='Ballpark Sent';
            opp.Type='New Business';
            opp.PRA_Project_ID__c='12345678-123126';
            opp.Business_Unit__c='PR-EU';
            opp.Phase__c='Phase I';
            opp.Therapeutic_Area__c='Dentistry';
            opp.Indication_Group__c='Dentistry Group';
            opp.Study_Start_Date__c= Date.today()+60;
            opp.CloseDate= Date.today()+30;
            system.debug('------------opp before insert------------'+opp);
            insert opp;
            system.debug('------------opp after insert------------'+opp);
        Test.startTest();       
        
        
        delete opp;
        Test.stopTest();
        system.debug('------------opp--------------'+opp);
        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        system.debug('------------lstProj--------------'+lstProj);
        system.assertEquals(lstProj[0].Status_desc__c, 'Deleted'); 
        
    }
     static testMethod void TestOperationalOwner() {
     	 
        
     	 
         Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
         
         User operationalOwner = new User(Alias = 'newU', Email='newuser@prahs.com',firstname='t1', 
         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = p.Id,EmployeeNumber='1234', 
         TimeZoneSidKey='America/Los_Angeles', UserName='opOwnerT@prahs.com');
         insert operationalOwner;
         User operationalOwnerUpperdate = new User(Alias = 'newU1', Email='newuser1@prahs.com',firstname='t1New', 
         EmailEncodingKey='UTF-8', LastName='TestingNew', LanguageLocaleKey='en_US', 
         LocaleSidKey='en_US', ProfileId = p.Id,EmployeeNumber='1235', 
         TimeZoneSidKey='America/Los_Angeles', UserName='opOwnerTU@prahs.com');
         insert operationalOwnerUpperdate;
        System.runAs ( operationalOwner ) {
	         Account oneAccount = new Account(name='TestSpouser');
	        insert oneAccount;
	         
	         
	         Opportunity opp = new Opportunity();
	            opp.Name='Testing Opportunity';
	            opp.AccountID= oneAccount.id;
	            opp.StageName='Ballpark Sent';
	            opp.Type='New Business';
	            opp.PRA_Project_ID__c='12345678-123123';
	            opp.Business_Unit__c='PR-EU';
	            opp.Phase__c='Phase I';
	            opp.Therapeutic_Area__c='Dentistry';
	            opp.Indication_Group__c='Dentistry Group';
	            opp.Primary_Indication__c = 'Primary1';
	            opp.Bid_Defense_Date__c = Date.today() +40;
	            opp.Native_Contract_Amount__c = 10;
	            opp.Study_Start_Date__c= Date.today()+60;
	            opp.CloseDate= Date.today()+30;
	            opp.split_Sites_EAPA__c = 10;
	            opp.split_Sites_NA__c = 10;
	            opp.Expected_Patients__c = 10;
	            opp.Expected_Sites__c = 10;
	            opp.RFP_1st_Patient_In_Date__c = Date.today() + 100;
	            opp.RFP_Last_Patient_In_Date__c = Date.today() + 200;
	            opp.Proposal_Due_Date__c = Date.today() +10;
	            opp.RFP_Expected_Date__c = Date.today() + 20;
	            opp.RFP_Received_Date__c = Date.today() + 30;
	            opp.Protocol_Number__c = 'TEST';
	            opp.Operational_Owner__c = operationalOwner.id;
	            system.debug('------------opp before insert------------'+opp);
	            insert opp;
             
	        Test.startTest();  
	        
	        opp.Operational_Owner__c = operationalOwnerUpperdate.id;
	        update opp;
	         Test.stopTest();
	        system.debug('------------opp--------------'+opp);
	        list<WFM_Project__c> lstProj = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
	        system.debug('------------lstProj--------------'+lstProj);
	        system.assertEquals(lstProj[0].DPD_Employee_ID__c, operationalOwnerUpperdate.EmployeeNumber); 
	         list<Bid_Project__c> lstB = RM_ProjectsDataAccessor.getBidProjectByProjectName(opp.PRA_Project_ID__c);
	        system.debug('------------lstB--------------'+lstB);
	        system.assertEquals(lstB[0].Project_Director__c, opp.Operational_Owner__c);
        }
    }
   
}