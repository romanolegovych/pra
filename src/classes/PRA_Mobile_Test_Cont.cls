global virtual without sharing class PRA_Mobile_Test_Cont {
    public PRA_Mobile_Test_Cont(){
    }
    
    @RemoteAction
    global static List<PAWS_ProjectDetailCVO> getProjectsDetails(){
        String sponsorName = 'SM Test Acc1';
 
        return PAWS_API.PBB_getActiveProjectList( sponsorName );
    }

    @RemoteAction
    global static PBB_NumbersWrapper saveData( Integer goalInteger,
                                    Date siteActivatedByDate,
                                    Id projectId ){
        try{
System.debug( LoggingLevel.ERROR,'@@@goalInteger : ' + goalInteger );
System.debug( LoggingLevel.ERROR,'@@@siteActivatedByDate : ' + siteActivatedByDate );
            Boolean saveBoolean = PAWS_API.PBB_setAggregatedMilestone ( projectId, 
                                                                        goalInteger,
                                                                        siteActivatedByDate,
                                                                        'Site Activation'
                                                                        );
System.debug( LoggingLevel.ERROR,'@@@saveBoolean : ' + saveBoolean );
        } catch ( Exception expt ){
                                                                                                    // TODO Dima AddException
            //showError( expt );
        }
        
        PAWS_ProjectDetailCVO projectDetailItem = PAWS_API.PBB_getProjectDetails( projectId );

        PBB_NumbersWrapper numbersWrapper = new PBB_NumbersWrapper();
        numbersWrapper.NumberOfSitesCompleted = projectDetailItem.NumberOfSitesCompleted;
        numbersWrapper.NumberOfSitesOnTrackEstimated = projectDetailItem.NumberOfSitesOnTrackEstimated;
        numbersWrapper.NumberOfSitesDelayedEstimated = projectDetailItem.NumberOfSitesDelayedEstimated;
        numbersWrapper.NumberOfSitesAtRiskEstimated = projectDetailItem.NumberOfSitesAtRiskEstimated;

        return numbersWrapper;
    }

    @RemoteAction
    global static PAWS_ProjectDetailCVO getProjectDetail( Id projectId ){
        return PAWS_API.PBB_getProjectDetails( projectId );
    }

    global class PBB_NumbersWrapper{
        public Decimal NumberOfSitesCompleted { get; set; }
        public Decimal NumberOfSitesOnTrackEstimated { get; set; }
        public Decimal NumberOfSitesDelayedEstimated { get; set; }
        public Decimal NumberOfSitesAtRiskEstimated { get; set; }

        public PBB_NumbersWrapper(){}
    }
}