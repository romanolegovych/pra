public with sharing class PAWS_FlowsDashboardV2Controller
{
    public class PAWS_FlowsDashboardControllerException extends Exception {}
    private PAWS_DashboardService dashboardService = new PAWS_DashboardService();
    
    public PAWS_FlowsDashboardV2Controller() {}
    
    public String SelectedFlow { get; set; }
    public List<SelectOption> FlowOptionList
    {
        get
        {
            if(FlowOptionList == null)
            {
                FlowOptionList = new List<SelectOption> { new SelectOption('', '- Please Select -') };
                List<ID> parentFlowIds = new List<ID>();
                for(AggregateResult each : [select STSWR1__Flow__r.STSWR1__Parent__c ParentId from STSWR1__Flow_Instance__c where STSWR1__Flow__r.STSWR1__Parent__c != null and STSWR1__Flow__r.STSWR1__Object_Type__c = 'ecrf__c' group by STSWR1__Flow__r.STSWR1__Parent__c])
                {
                    parentFlowIds.add(String.valueOf(each.get('ParentId')));
                }
                for(STSWR1__Flow__c each : [select Name from STSWR1__Flow__c where Id = :parentFlowIds order by Name])
                {
                    FlowOptionList.add(new SelectOption(each.Id, each.Name));
                }
            }
            return FlowOptionList;
        }
        set;
    }
    
    public String BoardJson
    { 
        get
        { 
            Map<String, Object> result = new Map<String, Object>{'flows' => null, 'objects' => null};
            //if(SelectedFlow != null)
            {
                List<STSWR1__Flow__c> flowList = dashboardService.findFlowByParentIds(new List<String> {SelectedFlow});
                result.put('flows', loadFlowList(flowList));
                result.put('objects', dashboardService.getAllRelatedObjects(FlowWrapperMap));
            }

            return System.JSON.serialize(result).replace('\'', '\\\''); 
        } 
    }

    @RemoteAction 
    public static void getData(){ }
    public void applyAction() { }
    
    public Map<ID, FlowWrapper> FlowWrapperMap = new Map<ID, FlowWrapper>();
    private Map<ID, List<FlowStepWrapper>> FlowIdStepWrapperListMap = new Map<ID, List<FlowStepWrapper>>();
    private Map<ID, FlowStepWrapper> StepWrapperMap = new Map<ID, FlowStepWrapper>();
    
    public List<FlowWrapper> loadFlowList(List<STSWR1__Flow__c> inFlowList)
    {
        List<FlowWrapper> flowWrapperList = new List<FlowWrapper>();
        Set<String> cursorIds = new Set<String>();
        Set<String> flowIds = new Set<String>();
        
        for(STSWR1__Flow__c eachFlow : inFlowList)
        {
            FlowWrapper flwWrapper = new FlowWrapper(eachFlow);
            FlowWrapperMap.put(eachFlow.ID, flwWrapper);
            flowIds.add(eachFlow.ID);
            List<FlowStepWrapper> stepWrapperList = FlowIdStepWrapperListMap.get(eachFlow.ID);
            if(stepWrapperList != null)
            {
                for(FlowStepWrapper eachStepWrapper : stepWrapperList)
                {
                    eachStepWrapper.SubFlows.add(flwWrapper);
                }
            }
            else
            {
                flowWrapperList.add(flwWrapper);
            }
        }
        
        Set<String> flowIdsForSearch = new Set<String>();
        for(STSWR1__Flow_Step_Junction__c eachStep : dashboardService.loadFlowStepJunction(new List<String>(flowIds)))
        {
            FlowWrapper flwWrapper = FlowWrapperMap.get(eachStep.STSWR1__Flow__c);
            FlowStepWrapper stepWrapper = flwWrapper.addStep(eachStep);
            StepWrapperMap.put(eachStep.ID, stepWrapper);
            
            for(STSWR1__Flow_Step_Action__c eachAction : eachStep.STSWR1__Flow_Step_Actions__r)
            {
                Map<String, Object> config = (Map<String, Object>)System.JSON.deserializeUntyped(eachAction.STSWR1__Config__c);
                if(config != null)
                {
                    Object flowId = config.get('flowId');
                    if(flowId != null && String.valueOf(flowId) != '')
                    {
                        String flowIdStr = String.valueOf(flowId);
                        List<FlowStepWrapper> wrapList = FlowIdStepWrapperListMap.get(flowIdStr);
                        if(wrapList == null)
                        {
                            wrapList = new List<FlowStepWrapper> { stepWrapper };
                        }
                        else
                        {
                            wrapList.add(stepWrapper);
                        }
                        FlowIdStepWrapperListMap.put(flowIdStr, wrapList);
                        flowIdsForSearch.add(flowIdStr);
                    }
                }
            }
        }
        
        for(STSWR1__Flow_Instance__c eachInstance : dashboardService.loadFlowInstanceByID(new List<String>(flowIds)))
        {
            FlowWrapper flwWrapper = FlowWrapperMap.get(eachInstance.STSWR1__Flow__c);
            flwWrapper.FlowInstance = eachInstance;
            
            for(STSWR1__Flow_Instance_Cursor__c cursorEach : eachInstance.STSWR1__Flow_Instance_Cursors__r)
            {
                cursorIds.add(cursorEach.Id);
            }
        }
        
        if(flowIdsForSearch.isEmpty() == false)
        {
            loadFlowList(dashboardService.findFlowByIDs(new List<String>(flowIdsForSearch)));
        }
        else
        {
            // NOTE: all flows were found
            dashboardService.updateFlowObject(FlowWrapperMap);
            //Map<ID, ProjectWrapper> ProjectWrapperMap = dashboardService.getAllRelatedObjects(FlowWrapperMap);
        }
        
        for(STSWR1__Flow_Instance_Cursor__c eachCursor : dashboardService.loadCursorInstanceWithHistoryByIds(new List<String>(cursorIds)))
        {
            FlowWrapper flwWrapper = FlowWrapperMap.get(eachCursor.STSWR1__Flow__c);
            flwWrapper.FlowInstanceCursorList.add(eachCursor);   
            for(STSWR1__Flow_Instance_History__c eachHistory : eachCursor.STSWR1__Flow_Instance_History__r)
            {
                // NOTE: we need only last step history
                flwWrapper.setStepHistory(eachHistory);
            }
        }
        
        for(STSWR1__Gantt_Step_Property__c eachGanttProp : dashboardService.getFlowPropertyByStepAndObject(StepWrapperMap.keySet()).values())
        {
            FlowStepWrapper eachStep = StepWrapperMap.get(eachGanttProp.STSWR1__Step__c);
            eachStep.GanttPropery = eachGanttProp;
        }
        
        return flowWrapperList;
    }
    
    public class FlowWrapper
    {   
        public FlowWrapper()
        {
            FlowInstanceCursorList = new List<STSWR1__Flow_Instance_Cursor__c>();
            Steps = new List<FlowStepWrapper>();
            StepsWraperIdMap = new Map<String, FlowStepWrapper>();
        }
        
        public FlowWrapper(STSWR1__Flow__c inputFlow)
        {
            this();
            Flow = inputFlow;
        }
        
        public FlowWrapper(STSWR1__Flow_Instance__c inputFlowInstance)
        {
            this();
            FlowInstance = inputFlowInstance;
        }
        
        public String Name { get { return Flow.Name; } }
        public String FlowID { get { return Flow.ID; } }
        public String ObjectType { get { return FlowInstance == null ? Flow.STSWR1__Object_Type__c : FlowInstance.STSWR1__Object_Type__c; } }
        
        public Transient STSWR1__Flow__c Flow { get; set; }
        public Transient STSWR1__Flow_Instance__c FlowInstance { get; set; }
        public Transient List<STSWR1__Flow_Instance_Cursor__c> FlowInstanceCursorList { get; set; }
        public sObject FlowObject { get; set; }
        public List<FlowStepWrapper> Steps { get; set; }
        public Transient FlowStepWrapper ParentStep { get; set; }
        public Transient Map<String, FlowStepWrapper> StepsWraperIdMap { get; set; }
        
        public FlowStepWrapper addStep(STSWR1__Flow_Step_Junction__c stepJunction)
        {
            FlowStepWrapper stepWrap = new FlowStepWrapper(stepJunction, this);
            StepsWraperIdMap.put(stepJunction.ID, stepWrap);
            Steps.add(stepWrap);
            return stepWrap;
        }
        
        public void setStepHistory(STSWR1__Flow_Instance_History__c inputHistory)
        {
            FlowStepWrapper stepWrap = StepsWraperIdMap.get(inputHistory.STSWR1__Step__c);
            if(stepWrap != null)
            {
                stepWrap.FlowInstanceHistory = inputHistory;
            }
        }
    }
    
    public class FlowStepWrapper
    {
        public FlowStepWrapper()
        { 
            SubFlows = new List<FlowWrapper>();
        }
        public FlowStepWrapper(STSWR1__Flow_Step_Junction__c inputStepJunction, FlowWrapper parentFlowWrapper)
        {
            this();
            FlowStepJunction = inputStepJunction;
            ParentFlow = parentFlowWrapper;
        }
        
        public Transient STSWR1__Flow_Step_Junction__c FlowStepJunction { get; set; }
        public Transient STSWR1__Flow_Instance_History__c FlowInstanceHistory { get; set; }
        public Transient STSWR1__Gantt_Step_Property__c GanttPropery { get; set; }
        public Transient FlowWrapper ParentFlow;
        public List<FlowWrapper> SubFlows { get; set; }
        
        public String Name { get { return FlowStepJunction.Name; } }
        public String ObjectType { get { return ParentFlow == null ? null : ParentFlow.ObjectType; } }
        
        public String MilestoneName { get { return FlowStepJunction.STSWR1__Flow_Milestone__r.Name; } }
        public String MilestoneKey
        {
            get
            {
                if(MilestoneKey == null)
                {
                    String MilestoneName = FlowStepJunction.STSWR1__Flow_Milestone__r.Name;
                    Integer MilestoneIndex = Integer.valueOf(FlowStepJunction.STSWR1__Flow_Milestone__r.STSWR1__Index__c);
                    MilestoneKey = (MilestoneIndex + '_' + MilestoneName).replace(' ', '_');
                }
                return MilestoneKey;
            }
            set;
        }
        
        public String StepKey
        {
            get
            {   
                if(StepKey == null)
                {
                    String Name = FlowStepJunction.Name;
                    Integer Index = Integer.valueOf(FlowStepJunction.STSWR1__Index__c);
                    StepKey = (ParentFlow.Flow.STSWR1__Object_Type__c + '_' + MilestoneKey + '_' + Name).replace(' ', '_');
                }
                return StepKey;
            }
            set;
        }
        
        public Integer StatusId
        {
            get
            {
                if(Status == 'late')
                {
                    return 0;
                }
                if(Status == 'notStarted')
                {
                    return 1;
                }
                if(Status == 'pending')
                {
                    return 2;
                }
                if(Status == 'inProgress')
                {
                    return 3;
                }
                if(Status == 'completed')
                {
                    return 4;
                }
                if(Status == 'skipped')
                {
                    return 5;
                }
                return 6;
            }
        }
        
        // Status could be: completed, late, inProgress, pending, notStarted
        public String Status
        {
            get
            {
                if(Status == null)
                {   
                    if(FlowInstanceHistory != null)
                    {
                        DateTime expirationDate;
                    	STSWR1__Flow_Instance_History__c history = FlowInstanceHistory;
                        
                        if(history.STSWR1__Step__r.STSWR1__Duration__c != null && GanttPropery != null && GanttPropery.STSWR1__Planned_Start_Date__c != null)
                        {
                            Integer duration = history.STSWR1__Step__r.STSWR1__Duration__c == 0 ? 1 : Integer.valueOf(history.STSWR1__Step__r.STSWR1__Duration__c);
                            expirationDate = GanttPropery.STSWR1__Planned_Start_Date__c.addDays(duration);
                        }
                        
                        if(history.STSWR1__Status__c == 'In Progress' && expirationDate != null && expirationDate < DateTime.now())
                        {
                            Status = 'late';
                        }
                        else if(history.STSWR1__Status__c == 'In Progress')
                        {
                            Status = 'inProgress';
                        }
                        else if(history.STSWR1__Status__c == 'Completed' || history.STSWR1__Status__c == 'Approved' || history.STSWR1__Status__c == 'Rejected')
                        {
                            Status = 'completed';
                        }
                        else if(history.STSWR1__Status__c == 'Pending')
                        {
                            Status = 'pending';
                        }
                    }
                    else if(FlowStepJunction != null)
                    {
                        Status = 'notStarted';
                    }
                    else
                    {
                        Status = 'hasNoStep';
                    }
                }
                return Status;
            }
            set;
        }
        
        public void addSubFlow(FlowWrapper inputFlow)
        {
            inputFlow.ParentStep = this; 
            SubFlows.add(inputFlow);
        }
    }
    
    public virtual class ObjectWrapper
    {
        public Transient sObject FlowObject { get; set; }
        public Transient ObjectWrapper Parent { get; set; }
        public Object FlowObjectName { get { return FlowObject == null ? null : FlowObject.get('Name'); } }
        public ID FlowObjectID { get { return FlowObject == null ? null : FlowObject.ID; } }
        
        public ObjectWrapper(sObject inSobject)
        {
            FlowObject = inSobject;
        }
    }

    public class ProjectWrapper extends ObjectWrapper
    {
        public List<CountryWrapper> Countries { get; set; }
        
        public ProjectWrapper(ecrf__c inProject)
        {
            super(inProject);
            Countries = new List<CountryWrapper>();
        }
        
        public CountryWrapper addCountry(PAWS_Project_Flow_Country__c inCountry)
        {
            CountryWrapper country = new CountryWrapper(inCountry, this);
            Countries.add(country);
            return country;
        }
    }
    
    public class CountryWrapper extends ObjectWrapper
    {
        public List<SiteWrapper> Sites { get; set; }
        
        public CountryWrapper(PAWS_Project_Flow_Country__c inCountry, ProjectWrapper inParent)
        {
            super(inCountry);
            Parent = inParent;
            Sites = new List<SiteWrapper>();
        }
        
        public SiteWrapper addSite(PAWS_Project_Flow_Site__c inSite)
        {
            SiteWrapper site = new SiteWrapper(inSite, this);
            Sites.add(site);
            return site;
        }
    }
    
    public class SiteWrapper extends ObjectWrapper
    {   
        public List<AgreementWrapper> Agreements { get; set; }
        public List<DocumentWrapper> Documents { get; set; }
        public List<SubmissionWrapper> Submissions { get; set; }

        public SiteWrapper(PAWS_Project_Flow_Site__c inSite, CountryWrapper inParent)
        {
            super(inSite);
            Parent = inParent;
            Agreements = new List<AgreementWrapper>();
            Documents = new List<DocumentWrapper>();
            Submissions = new List<SubmissionWrapper>();
            
            for(PAWS_Project_Flow_Agreement__c eachAgreement : inSite.PAWS_Agreements__r)
            {
                Agreements.add(new AgreementWrapper(eachAgreement, this));
            }
            for(PAWS_Project_Flow_Document__c eachDoc : inSite.PAWS_Documents__r)
            {
                Documents.add(new DocumentWrapper(eachDoc, this));
            }
            for(PAWS_Project_Flow_Submission__c eachSubm : inSite.PAWS_Submissions__r)
            {
                Submissions.add(new SubmissionWrapper(eachSubm, this));
            }
        }
    }
    
    public class DocumentWrapper extends ObjectWrapper
    {
        public DocumentWrapper(PAWS_Project_Flow_Document__c inDocument, SiteWrapper inParent)
        {
            super(inDocument);
            Parent = inParent;
        }
    }
    
    public class SubmissionWrapper extends ObjectWrapper
    {
        public SubmissionWrapper(PAWS_Project_Flow_Submission__c inSubmission, SiteWrapper inParent)
        {
            super(inSubmission);
            Parent = inParent;
        }
    }
    
    public class AgreementWrapper extends ObjectWrapper
    {
        public AgreementWrapper(PAWS_Project_Flow_Agreement__c inAgreement, SiteWrapper inParent)
        {
            super(inAgreement);
            Parent = inParent;
        }
    }
}