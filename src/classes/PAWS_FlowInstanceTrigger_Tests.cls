@isTest
private class PAWS_FlowInstanceTrigger_Tests
{
	private static STSWR1__Flow__c parentFlow {get; set;}
	private static STSWR1__Flow_Step_Junction__c parentFlowStep {get; set;}
	
	private static PAWS_Project_Flow_Junction__c parentProjectFlow {get; set;}
	
	private static STSWR1__Flow_Instance__c parentFlowInstance {get; set;}
	private static STSWR1__Flow_Instance_Cursor__c parentFlowInstanceCursor {get; set;}
	private static STSWR1__Flow_Instance_History__c parentFlowInstanceHistory {get; set;}
	
	private static STSWR1__Flow__c subFlow {get; set;}
	private static STSWR1__Flow_Branch__c subFlowBranch {get; set;}
	private static STSWR1__Flow_Swimlane__c subFlowSwimlane {get; set;}
	private static STSWR1__Flow_Milestone__c subFlowMilestone {get; set;}
	private static STSWR1__Flow_Step_Junction__c subFlowStep {get; set;}
	
	private static STSWR1__Flow_Instance__c subFlowInstance {get; set;}
	private static STSWR1__Flow_Instance_Cursor__c subFlowInstanceCursor {get; set;}
	private static STSWR1__Flow_Instance_History__c subFlowInstanceHistory {get; set;}
	
	static testMethod void testPAWSFlowInstanceTrigger()
	{
		PAWS_ApexTestsEnvironment.init();
		
		parentFlow = PAWS_ApexTestsEnvironment.Flow;
		parentFlowStep = PAWS_ApexTestsEnvironment.FlowStep;
		parentProjectFlow = PAWS_ApexTestsEnvironment.ProjectFlow;
		
		parentFlowInstance = new STSWR1__Flow_Instance__c(STSWR1__Flow__c=parentFlow.Id, STSWR1__Is_Active__c=true, STSWR1__Object_Id__c=PAWS_ApexTestsEnvironment.Project.Id, STSWR1__Object_Name__c=PAWS_ApexTestsEnvironment.Project.Name);
		insert parentFlowInstance;
		//update parentFlowInstance;
		
		parentFlowInstanceCursor = new STSWR1__Flow_Instance_Cursor__c(STSWR1__Flow_Instance__c=parentFlowInstance.Id, STSWR1__Step__c=parentFlowStep.Id, STSWR1__Step_Assigned_To__c=UserInfo.getUserId(), STSWR1__Status__c='In Progress');
		insert parentFlowInstanceCursor;
		
		parentFlowInstanceHistory = new STSWR1__Flow_Instance_History__c(STSWR1__Cursor__c=parentFlowInstanceCursor.Id, STSWR1__Step__c=parentFlowStep.Id, STSWR1__Status__c='In Progress');
		insert parentFlowInstanceHistory;
		
		Test.startTest();
		
		createSubFlow();
		
		Test.stopTest();
	}
	
	private static void createSubFlow()
	{
		subFlow = new STSWR1__Flow__c(Name='Test Sub Flow', STSWR1__Type__c = 'One Time', STSWR1__Object_Type__c = 'ecrf__c', STSWR1__Status__c = 'Active');
		insert subFlow;
		
		subFlowBranch = new STSWR1__Flow_Branch__c(Name='Test Branch', STSWR1__Flow__c=subFlow.Id);
		insert subFlowBranch;
		
		subFlowSwimlane = new STSWR1__Flow_Swimlane__c(Name='Test Swimlane', STSWR1__Flow__c=subFlow.Id, STSWR1__Assign_Type__c='User',STSWR1__Assign_To__c=UserInfo.getUserId());
		insert subFlowSwimlane;
		
		subFlowMilestone = new STSWR1__Flow_Milestone__c(Name='Test Milestone', STSWR1__Flow__c=subFlow.Id);
		insert subFlowMilestone;
		
		subFlowStep = new STSWR1__Flow_Step_Junction__c(Name='Test Step', STSWR1__Flow__c=subFlow.Id, STSWR1__Flow_Swimlane__c=subFlowSwimlane.Id, STSWR1__Flow_Milestone__c=subFlowMilestone.Id, STSWR1__Flow_Branch__c=subFlowBranch.Id, STSWR1__Type__c='Manual', STSWR1__Is_First_Step__c = true);
		insert subFlowStep;
		
		subFlowInstance = new STSWR1__Flow_Instance__c(STSWR1__Flow__c=subFlow.Id, STSWR1__Parent__c=parentFlowInstance.Id, STSWR1__Lock_Parent_Step__c=parentFlowStep.Id, STSWR1__Parent_Step__c=parentFlowStep.Id, STSWR1__Is_Active__c=true, STSWR1__Object_Id__c=PAWS_ApexTestsEnvironment.Project.Id, STSWR1__Object_Name__c=PAWS_ApexTestsEnvironment.Project.Name);
		insert subFlowInstance;
		
		subFlowInstanceCursor = new STSWR1__Flow_Instance_Cursor__c(STSWR1__Flow_Instance__c=subFlowInstance.Id, STSWR1__Step__c=subFlowStep.Id, STSWR1__Step_Assigned_To__c=UserInfo.getUserId(), STSWR1__Status__c='In Progress');
		insert subFlowInstanceCursor;
		
		subFlowInstanceHistory = new STSWR1__Flow_Instance_History__c(STSWR1__Cursor__c=subFlowInstanceCursor.Id, STSWR1__Step__c=subFlowStep.Id, STSWR1__Status__c='In Progress', STSWR1__Actual_Start_Date__c=date.today());
		insert subFlowInstanceHistory;
		
		subFlowInstanceHistory.STSWR1__Actual_Complete_Date__c = date.today();
		update subFlowInstanceHistory;
		
		subFlowInstanceCursor.STSWR1__Status__c = 'Complete';
		update subFlowInstanceCursor;
	}
}