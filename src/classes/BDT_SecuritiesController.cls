public with sharing class BDT_SecuritiesController {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Administration Securities
	public list<BDT_RoleService.displayRoleSelector> displayRoles {get;set;}
	public BDT_Role__c AddEditRole {get;set;}

	public list<user> bdtusers {get;set;}
	public BDT_UserRoles__c bdtuserroles {get;set;}
	public String userName {get;set;}
	public String userNameError {get;set;}
	public BDT_Utils.SelectOptionShuttle shuttle {get;set;}
	public list<String> shuttleUsed {get;set;}
	public list<String> shuttleUnused {get;set;}
	public list<SelectOption> shuttleUsedSelectOption {get;set;}
	public list<SelectOption> shuttleUnusedSelectOption {get;set;}

	public list<BDT_Role__c> selectedRoles {get;set;}
	public list<BDT_SecuritiesService.SecuritiesDisplay> rolesDisplay {get;set;}

	public boolean showRoles {get;set;}
	public boolean showUsers {get;set;}
	public boolean showAddEditRoleName {get;set;}
	public boolean showAddUser {get;set;}
	public boolean showConfigureUserRoles {get;set;}
	public boolean showConfigureRoles {get;set;}
	public boolean showConfigureSecuritySettings {get;set;}

	public BDT_SecuritiesController() {
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Securities');
		setInitialState();
	}

	public void setInitialState () {
		displayRoles = BDT_RoleService.getDisplayRoles();
		bdtusers = BDT_UserRolesService.getBDTusers();
		AddEditRole = null;
		bdtuserroles = null;
		shuttleUsed = null;
		shuttleUnused = null;
		shuttleUsedSelectOption = null;
		shuttleUnusedSelectOption = null;
		userNameError=null;
		
		// default boolean settings
		disableAllShowBooleans();
		showRoles = true;
		showUsers = true;
	}

	public void disableAllShowBooleans () {
		showRoles = false;
		showUsers = false;
		showAddEditRoleName = false;
		showAddUser = false;
		showConfigureUserRoles = false;
		showConfigureRoles = false;
		showConfigureSecuritySettings = false;
	}

	public void buttonAddRole () {
		AddEditRole = new BDT_Role__c();
		disableAllShowBooleans();
		showAddEditRoleName = true;
	}

	public void buttonEditRole () {
		String RoleId = system.currentPageReference().getParameters().get('RoleId');
		AddEditRole = BDT_RoleService.getRole(RoleId);
		disableAllShowBooleans();
		showAddEditRoleName = true;
		bdtusers = BDT_UserRolesService.getUsersByRole (String.valueOf(AddEditRole.Role_Number__c));
	}

	public void buttonSaveRole () {
		List<BDT_Role__c> roleList = new List<BDT_Role__c>();
		roleList.add(AddEditRole);
		BDT_RoleService.saveRoles(roleList);
		setInitialState();
	}

	public void buttonDeleteRole () {
		BDT_RoleService.deleteRole(AddEditRole.Role_Number__c);
		setInitialState();
	}

	public void buttonCancelRole () {
		setInitialState();
	}

	public void buttonAddUser () {
		userName = null;
		disableAllShowBooleans();
		showAddUser = true;
	}

	public void buttonSaveNewUser () {
		try{
			bdtuserroles = BDT_UserRolesService.createFindBDTUser(userName);
		} catch (Exception e) {
			userNameError = 'Enter a valid SalesForce username! New SalesForce users can not be created here.';
			userName = null;
		}
		// run this when there was a vild username
		if (userName != null) {
			if (bdtuserroles == null) {
				// invalid data entry retry, let the user retry
				buttonAddUser();
			}
			// reload initial data after save occured
			setInitialState ();
			
			disableAllShowBooleans();
			showUserEditRoles();
		}
	}

	public void buttonCancelAddUser () {
		setInitialState();
	}

	public void buttonEditUserRoles() {
		String UserId = system.currentPageReference().getParameters().get('UserId');
		userName = BDT_UserRolesService.getUserName(UserId);
		showUserEditRoles();
	}
		
	public void showUserEditRoles() {
		if (userName != null) {
			bdtuserroles = BDT_UserRolesService.createFindBDTUser(userName);
			shuttle = BDT_UserRolesService.createShuttle(bdtuserroles.Name);
			shuttleUsed = new List<String>();
			shuttleUnused = new List<String>();
			shuttleUsedSelectOption = shuttle.usedOptions;
			shuttleUnusedSelectOption = shuttle.getUnusedOptions();
			disableAllShowBooleans();
			showConfigureUserRoles = true;
		} else {
			setInitialState();
		}
		
	}

	public void buttonUserRolesMoveToUsed () {
		shuttle.moveToUsed(shuttleUnused);
		shuttleUnused = new List<String>();
		shuttleUsedSelectOption = shuttle.usedOptions;
		shuttleUnusedSelectOption = shuttle.getUnusedOptions();
	}

	public void buttonUserRolesMoveToUnused () {
		shuttle.moveToUnused(shuttleUsed);
		shuttleUsed = new List<String>();
		shuttleUsedSelectOption = shuttle.usedOptions;
		shuttleUnusedSelectOption = shuttle.getUnusedOptions();
	}
	
	public void buttonSaveConfigureUserRoles () {
		BDT_UserRolesService.saveRoles(bdtuserroles,shuttleUsedSelectOption);
		setInitialState();
	}
	
	public void buttonDeleteConfigureUserRoles () {
		BDT_UserRolesService.removeAllUsersRolesFromPlenbox(bdtuserroles.id);
		setInitialState();
	}
	
	public void buttonCancelConfigureUserRoles () {
		setInitialState ();
	}

	public void buttonEditRoleSecurities () {
		// determine selected roles
		selectedRoles = new list<BDT_Role__c>();
		for (BDT_RoleService.displayRoleSelector displayRole : displayRoles) {
			if (displayRole.roleSelected) {
				selectedRoles.add(displayRole.role);
			}
		}
		rolesDisplay = BDT_SecuritiesService.getSecurities (selectedRoles);
		disableAllShowBooleans();
		showConfigureSecuritySettings = true;
	}

	public void buttonSaveConfigureRole(){
		BDT_SecuritiesService.saveSecurities (rolesDisplay);
		setInitialState ();
	}

	public void buttonCancelConfigureRole(){
		setInitialState ();
	}

}