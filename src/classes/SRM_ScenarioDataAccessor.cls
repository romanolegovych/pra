/**
* @author Kondal Reddy
* @date 21-Oct-2014
* @description this is controller for scenario data accessor
*/

public with sharing class SRM_ScenarioDataAccessor{
    
    
            /**
    * @description Method to get SRM Project by WFM Project Name
    * @params sceanrioId and countryName
    */
    //Method to get SRM Project by WFM Project Name
    public static List<Bid_Project__c> getSRMProjectByWFMProjectName(String wfmProjectName){
        return [select Id, Name, PRA_Project_ID__c, PRA_Project_ID__r.Name from Bid_Project__c where PRA_Project_ID__r.Name =: wfmProjectName];
    }
    
    
            /**
    * @description Method to get SRM Project
    * @params sceanrioId and countryName
    */
    //Method to get SRM Project
    public static Bid_Project__c getSRMProject(String qry, String projectId){
        Bid_Project__c srmProj = (Bid_Project__c)Database.query(qry)[0];
        return srmProj;
    }
    
    
            /**
    * @description Method to get SRM Project by id=:scenario.Bid_Project__c
    * @params sceanrioId and countryName
    */
       //Method to get SRM Project by id=:scenario.Bid_Project__c
    public static Bid_Project__c getSRMProjectbyscenario(String project){
        Bid_Project__c projectObj = [select id,name,Proposal_Director__c,Regulatory_Expert__c,Startup_Expert__c,Therapeutic_Expert__c,Project_Manager__c,Project_Director__c,Contracts_Expert__c,Business_Dev_Expert__c,
                                     Proposal_Manager__c from Bid_Project__c where id=:project];
        return projectObj;
    }
    
    
            /**
    * @description Method to get scenario countries
    * @params sceanrioId and countryName
    */
    //Method to get scenario countries
    public static List<SRM_Scenario_Country__c> getScenarioCountries(String query, String scenarioId){
        return Database.query(query);
    }
    
    
            /**
    * @description Method to get countries
    * @params sceanrioId and countryName
    */
    //Method to get countries
    public static List<Country__c> getCountries(){
       List<Country__c> countryList = [select id, Name from Country__c limit 50000];
        return countryList;
    }
    
    
            /**
    * @description Method to get countries
    * @params sceanrioId and countryName
    */
     //Method to get countries
    public static List<Country__c> getCountriesByIds(List<Id> countryIds){
       List<Country__c> countryList = [select id,Name from Country__c where id IN :countryIds];
        return countryList;
    }
    
    
    
    
            /**
    * @description Method to return all the fields from obejct in comma separated string
    * @params sceanrioId and countryName
    */
    //Method to return all the fields from obejct in comma separated string
    public static String getAllFieldsFromObject(String ObjectType){
        String fieldsString = SRM_Utils.getAllFieldsFromObject(ObjectType);
        return fieldsString;
    }
    
    
            /**
    * @description Method to return all the fields from obejct in comma separated string
    * @params sceanrioId and countryName
    */
    //Method to return all the fields from obejct in comma separated string
    public static List<Group> getusergrpids(){
        List<Group> groupList = [select id from Group where DeveloperName = 'SRM_ScenarioModeler' limit 1] ;
        return groupList;
    }
    
    
            /**
    * @description Method to return all the fields from obejct in comma separated string
    * @params sceanrioId and countryName
    */
   //Method to return all the fields from obejct in comma separated string
    public static List<GroupMember> getusergrpmemids(String grouplistid){
        List<GroupMember> groupMemberList = [Select Id, UserOrGroupId From GroupMember Where GroupId = :grouplistid];
        return groupMemberList;
    }
    

            /**
    * @description Method to get all records defined for child objects of the Model
    * @params sceanrioId and countryName
    */
    //Method to get all records defined for child objects of the Model
    public static List<sObject> getAllCountries(String query, String activeModelId){
        List<sObject> countryList = Database.query(query);
        return countryList;
    }
    
    
            /**
    * @description method to find approved/active model
    * @params sceanrioId and countryName
    */
    //method to find approved/active model
    public static List<SRM_Model__c> findRecentActiveModel(){
        List<SRM_Model__c> activeModelList = [select Id, Name, Status__c from SRM_Model__c where Status__c = 'Approved' order by Name desc limit 1];
        return activeModelList;
    }
    
    
            /**
    * @description method to find Scenario Model id
    * @params sceanrioId and countryName
    */
    //method to find Scenario Model id
    public static String findScenarioSelectedModelId(String name){
        List<SRM_Model__c> SelectedModelIdList = [select Id, Name, Status__c,Comments__c from SRM_Model__c where Name=:name limit 1];
        String id;
        for(SRM_Model__c modelIdsList :SelectedModelIdList){
            id=modelIdsList.Id;
        }        
        return id;
    } 
    
    
            /**
    * @description get all the questionsresponse
    * @params sceanrioId and countryName
    */
    public static String findScenarioSelectedModelName(String Id){
        List<SRM_Model__c> SelectedModelNameList = [select Id, Name, Status__c,Comments__c from SRM_Model__c where Id=:Id limit 1];
        String name;
        for(SRM_Model__c modelnameList :SelectedModelNameList){
            name=modelnameList.Name;
        }        
        return name;
    } 
    
    
            /**
    * @description method to find Scenario Model
    * @params sceanrioId and countryName
    */
    //method to find Scenario Model
    public static List<SRM_Model__c> findScenarioModels(){
        List<SRM_Model__c> scenarioModelList = [select Id, Name, Status__c,Comments__c from SRM_Model__c where Status__c = 'Approved' OR Status__c = 'Validation' order by Status__c ASC];
        return scenarioModelList;
    }
    
    
    /**
    * @description Method to return all questions and answers records for selected model
    * @params modelId
    */
    public static List<SRM_Model_Questions__c> getQuestionAndAnswersForModel(String modelId){
        List<SRM_Model_Questions__c> questionAnsList = [select id,name,Group__c,Primary_Role__c ,information__c,Country__c,(select id, name, SRM_Question_ID__c,DefaultResponse__c, Response__c, SRM_Model_Response__c.SRM_Question_ID__r.Country__c,  
                                                        SRM_Model_Response__c.SRM_Question_ID__r.Selecting_Country__c,Adjust_Center__c, Adjust_Width__c, Adjust_Weeks__c, Adjust_Height__c, Impact_Start_Week_By__c, Adjust_FPI__c, Adjust_LPI__c, Subject_Enroll_Adjustment__c
                                                        from SRM_Model_Response__r),Sub_Group__c,Question__c from SRM_Model_Questions__c where SRM_Model__c=:modelId];
        return questionAnsList;
    }
    
       /**
    * @description Method to return all questions and answers records for selected model
    * @params modelId
    */
    public static List<SRM_Model_Questions__c> getModelQuestions(String model){
        List<SRM_Model_Questions__c> questionList = [select id,name,Group__c,Required_for_Approval__c, Unique__c, Primary_Role__c ,Selecting_Country__c,information__c,Country__c,(select id, name, SRM_Question_ID__c,DefaultResponse__c, Response__c from SRM_Model_Response__r),Sub_Group__c,Question__c from SRM_Model_Questions__c  where SRM_Model__c=:model]; 
        return questionList;
    }
    
    
           /**
    * @description Method to return all questions and answers records for selected model
    * @params modelId
    */
    public static List<SRM_Questions_Response__c> getModelQuestionsbyres(List<Id> quesIds ,string scenId ){
        List<SRM_Questions_Response__c> quesResList = [select id, Country__c,Comments__c,SRM_Question_ID__c, SRM_Question_ID__r.Unique__c, SRM_Response_Id__c, IsChecked__c from SRM_Questions_Response__c where SRM_Question_ID__c IN: quesIds]; 
        return quesResList;
    }
    
    
    /**
    * @description get all the week adjustment for model and country
    * @params modelId and countryName
    */
    public static List<SRM_Week_Adjustments__c> getAllWeekAdjustments(String modelId, String country){
        List<SRM_Week_Adjustments__c> weekList = [select Id, Name, Country__c, Country__r.Name, Site_Act_Adjustment__c, SRM_Model__c, Week_of_Project__c from SRM_Week_Adjustments__c where SRM_Model__c =: modelId and Country__c =: country];
        return weekList;
    }
    
    
        /**
    * @description get all the questionsresponse
    * @params sceanrioId and countryName
    */
    public static List<SRM_Questions_Response__c> getQuesResponse(String selCountry, String scenarioId){
        List<SRM_Questions_Response__c> delQuesResponseList = [select id,Country__r.Name from SRM_Questions_Response__c where Country__r.Name =: selCountry];
        return delQuesResponseList;
    }
    
    
            /**
    * @description get all the questionsresponse
    * @params sceanrioId and countryName
    */
    public static List<SRM_Questions_Response__c> getQuestions(String scenarioId){
        List<SRM_Questions_Response__c> srmQueResList = [select id, QuestionResponseUnique__c, SRM_Question_ID__c,SRM_Question_ID__r.Unique__c,Comments__c,SRM_Response_Id__r.Response__c, SRM_Response_Id__c, IsChecked__c from SRM_Questions_Response__c ];
        return srmQueResList;
    }
    
    
     /** 
    * @description Method to get export details by project
    * @author Ramya
    * @date 2014
    **/
    public static List<SRM_Questions_Response__c> getexporttbyproject(String scenarioId ){
 
         List<SRM_Questions_Response__c> detailsbyprojectList= [SELECT Id,
                                                                       Name,
                                                                       SRM_Question_ID__c,
                                                                       SRM_Response_Id__c, 
                                                                       SRM_Question_ID__r.Name,
                                                                       SRM_Question_ID__r.Country__c,
                                                                       SRM_Question_ID__r.Question__c,
                                                                       SRM_Question_ID__r.Group__c,
                                                                       SRM_Question_ID__r.Primary_Role__c,
                                                                       SRM_Response_Id__r.Alert_Message__c ,
                                                                       SRM_Response_Id__r.Response__c,
                                                                       SRM_Response_Id__r.SRM_Question_ID__c  ,
                                                                       SRM_Response_Id__r.Alert_Flag__c  ,
                                                                       LastModifiedDate,
                                                                       LastModifiedBY.Name, 
                                                                       LastModifiedBYId,
                                                                       Comments__c,
                                                                       SRM_Response_Id__r.LastModifiedBYId 
                                                                 FROM  SRM_Questions_Response__c 
                                                                 where SRM_Question_ID__r.Group__c = 'Project'];
                                                                 
           return detailsbyprojectList;
    }
    
   
      /** 
    * @description Method to get export details by sponser 
    * @author Ramya
    * @date 2014
    **/
    public static List<SRM_Questions_Response__c> getexporttbysponser(String scenarioId ){
 
         List<SRM_Questions_Response__c> detailsbysponserList= [SELECT Id,
                                                                       Name,
                                                                       SRM_Question_ID__c,
                                                                       SRM_Response_Id__c, 
                                                                       SRM_Question_ID__r.Name,
                                                                       SRM_Question_ID__r.Country__c,
                                                                       SRM_Question_ID__r.Question__c,
                                                                       SRM_Question_Id__r.Group__c,
                                                                       SRM_Question_ID__r.Primary_Role__c,
                                                                       SRM_Response_Id__r.Alert_Message__c ,
                                                                       SRM_Response_Id__r.Response__c,
                                                                       SRM_Response_Id__r.SRM_Question_ID__c  ,
                                                                       SRM_Response_Id__r.Alert_Flag__c  ,
                                                                       LastModifiedDate,
                                                                       LastModifiedBYId,
                                                                       LastModifiedBY.Name, 
                                                                       Comments__c,
                                                                       SRM_Response_Id__r.LastModifiedBYId 
                                                                 FROM  SRM_Questions_Response__c 
                                                                 where SRM_Question_ID__r.Group__c = 'Sponsor'];
                                                                 
           return detailsbysponserList;
    }
    
    
        /** 
    * @description Method to get export details by country
    * @author Ramya
    * @date 2014
    **/
    public static List<SRM_Questions_Response__c> getexporttbycountry(String scenarioId ){
 
         List<SRM_Questions_Response__c> detailsbycountryList= [SELECT Id,
                                                                       Name,
                                                                       SRM_Question_ID__c,
                                                                       Country__r.Name,
                                                                       SRM_Response_Id__c, 
                                                                       SRM_Question_ID__r.Name,
                                                                       SRM_Question_ID__r.Country__c,
                                                                       SRM_Question_ID__r.Question__c,
                                                                       SRM_Question_ID__r.Group__c,
                                                                       SRM_Question_ID__r.Primary_Role__c,
                                                                       SRM_Response_Id__r.Alert_Message__c ,
                                                                       SRM_Response_Id__r.Response__c,
                                                                       SRM_Response_Id__r.SRM_Question_ID__c  ,
                                                                       SRM_Response_Id__r.Alert_Flag__c  ,
                                                                       LastModifiedDate,
                                                                       LastModifiedBY.Name, 
                                                                       LastModifiedBYId,
                                                                       Comments__c,
                                                                       SRM_Response_Id__r.LastModifiedBYId 
                                                                 FROM  SRM_Questions_Response__c 
                                                                 where SRM_Question_ID__r.Group__c = 'Country'];
                                                                 
           return detailsbycountryList;
    }
    
          /** 
    * @description Method to get export details by contract
    * @author Ramya
    * @date 2014
    **/
    public static List<SRM_Questions_Response__c> getexporttbycontract(String scenarioId ){
 
         List<SRM_Questions_Response__c> detailsbycontractList= [SELECT Id,
                                                                       Name,
                                                                       SRM_Question_ID__c,
                                                                       Country__r.Name,
                                                                       SRM_Response_Id__c, 
                                                                       SRM_Question_ID__r.Name,
                                                                       SRM_Question_ID__r.Country__c,
                                                                       SRM_Question_ID__r.Question__c,
                                                                       SRM_Question_ID__r.Group__c,
                                                                       SRM_Question_ID__r.Primary_Role__c,
                                                                       SRM_Response_Id__r.Alert_Message__c ,
                                                                       SRM_Response_Id__r.Response__c,
                                                                       SRM_Response_Id__r.SRM_Question_ID__c  ,
                                                                       SRM_Response_Id__r.Alert_Flag__c  ,
                                                                       LastModifiedDate,
                                                                       LastModifiedBY.Name, 
                                                                       LastModifiedBYId,
                                                                       Comments__c,
                                                                       SRM_Response_Id__r.LastModifiedBYId 
                                                                 FROM  SRM_Questions_Response__c 
                                                                 where SRM_Question_ID__r.Group__c = 'Contract'];
                                                                 
           return detailsbycontractList;
    }
    
    
    /**
    * @description get all the site activation for recent approved model
    */
    public static List<SRM_Model_Site_Activation__c> getAllSiteActivations(String modelId){
        String query = 'select '+getAllFieldsFromObject('SRM_Model_Site_Activation__c')+' from SRM_Model_Site_Activation__c where SRM_Model__c =: modelId limit 10000';
        List<SRM_Model_Site_Activation__c> siteList = Database.query(query);
        return siteList;
    }
    
    
    /**
    @author Kondal Reddy 
    @date 2014
    @description method to get Subject Enrollment Record
    */
    public static SRM_Model_Subject_Enrollment__c getSubEnrollById(String SubEnrollId){
        return [Select  Id,
                        Name,
                        default__c,
                        SRM_Model__c,
                        Shift_Center_By__c,
                        Standard_Deviation__c,
                        Comments__c
                FROM    SRM_Model_Subject_Enrollment__c
                Where   Id =: SubEnrollId
                LIMIT   1];
    }
    
    /**
    @author Kondal Reddy 
    @date 2014
    @description method to get Site Activation Record
    */
    public static SRM_Model_Site_Activation__c getSiteActivationById(String SiteActId){
        return [Select  Id,
                        Name,
                        SRM_Model__c,
                        Number_of_Weeks__C,
                        Country__c,
                        Country__r.Name,
                        Amplitude__c,
                        //Therapeutic__c,
                		Therapeutic_Area__c ,
                        Shift_Center_By__c,
                        Site_Activation_Formula__c,
                        Standard_Deviation__c,
                        Comments__c,
                        Start_Week__c,
                        Gaussian_Constant__c,
                        sponsor__c,
                        IRB_Type__c
                FROM    SRM_Model_Site_Activation__c
                Where   Id =:SiteActId
                LIMIT   1];
    }
    
    /**
    * @description 
    * @params
    */ 
    public static List<SRM_Model_Subject_Enrollment__c> getAllSubEnrollments(String modelId){
        return [select Id, 
               Name,default__c, 
               Comments__c, 
               Shift_Center_By__c, 
               Standard_Deviation__c, 
               SRM_Model__c, 
               Unique_Record__c 
        from   SRM_Model_Subject_Enrollment__c
        where  SRM_MOdel__c =: modelId
       ORDER BY default__c DESC];
    }
}