public class IPA_EventsHolidaysComponentController
{
    public List<IPA_Articles__c> lstEvents {get; private set;}
    public List<IPA_Articles__c> lstHolidays {get; private set;}
    public IPA_Page_Widget__c pageWidgetObj {get; set;}
    
    public void getFetchContent() {
        
        system.debug('in getEvent');        
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        
        if(pageWidgetObj != null){
            lstEvents = ipa_bo.returnTop3Events(pageWidgetObj);
        }else{
            lstEvents = ipa_bo.returnTop3Events();
        }
        lstHolidays = ipa_bo.returnTop3Holidays();
    }
}