/**
@author Bhargav Devaram
@date 2015
@description this is PBB_ScenarioQuestionnaireCntrltest class
**/
@isTest   
private class PBB_ScenarioQuestionnaireCntrlTest{
    static testMethod void testPBB_ScenarioQuestionnaireCntrl(){        
        test.startTest();   
            PBB_ScenarioQuestionnaireCntrl csc = new PBB_ScenarioQuestionnaireCntrl();
            csc.getQuestionnairebyServiceArea();
            csc.SaveQuestionnairebyServiceArea();
        test.stopTest();
    }   
}