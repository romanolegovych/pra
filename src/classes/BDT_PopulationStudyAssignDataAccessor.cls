public with sharing class BDT_PopulationStudyAssignDataAccessor {

	public static String getSObjectFieldString() {
		return getSObjectFieldString('PopulationStudyAssignment__c');		
	}

	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = '';
		result += referenceName + 'AggregateNumberOfBackups__c,';
		result += referenceName + 'AggregateNumberOfSubjects__c,';
		result += referenceName + 'Population__c,';
		result += referenceName + 'Name,';
		result += referenceName + 'Id,';
		result += referenceName + 'Study__c';
		return result;
	}

	public static List<PopulationStudyAssignment__c> getList(String whereClause) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM PopulationStudyAssignment__c ' +
								'WHERE  {1}',
								new List<String> {
									getSObjectFieldString(),
									whereClause
								}
							);
		return (List<PopulationStudyAssignment__c>) Database.query(query);
	}

}