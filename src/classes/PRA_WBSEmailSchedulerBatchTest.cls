/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PRA_WBSEmailSchedulerBatchTest {
    
    //lets test the scheduler first
    static testMethod void testPRA_WBSEmailScheduler() {
        
        Test.startTest();
        PMC_ProfileGroup__c Plist = new PMC_ProfileGroup__c();
        plist.Profile__c='FATestPRAUnitTest';
        plist.Name='FATestPRAUnitTest';
        insert plist;
        PRA_WBSEmailScheduler scheduler = new  PRA_WBSEmailScheduler();
        Datetime dt = System.now().addSeconds(2);                
        scheduler.execute(null);
        Test.stopTest();
        
        List<PRA_Batch_Queue__c> BQ = [select id,name from PRA_Batch_Queue__c];
        system.assertEquals(BQ.size(),1);        
    }
    
     //then test the batch next
    static testMethod void testPRA_WBSEmailBatch() {  
        
        // Init Test Utils 
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        
        User testUser = new User(alias='TestFAUr',Email='testFAPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='FA', 
                            LocaleSidKey='en_US', ProfileId=p.id, TimeZoneSidKey='America/New_York', Username='Test.FA@praintl.com.unitTestPMConsole');
        insert testUser;
        
        PermissionSet ps=new PermissionSet();
        ps.name='FATestPRAUnitTest';
        ps.label='FATestPRAUnitTest';
        insert ps;
        
        PermissionSetAssignment psa =new PermissionSetAssignment();
        psa.AssigneeId= testUser.id; 
        psa.PermissionSetId = ps.id;
        insert psa;
        
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        PMC_ProfileGroup__c Plist = new PMC_ProfileGroup__c();
        plist.Profile__c='FATestPRAUnitTest';
        plist.Name='FATestPRAUnitTest';
        insert plist; 
        
        // Zero out all effort ratios
        List<Client_Task__c> ctList = [select id,project__r.name,Project__c,status__c
                                          from Client_Task__c
                                          where Project__r.id = :tu.clientProject.id
                                          ];
        ctList[0].status__c=PRA_Constants.CU_STATUS_READY;
        ctList[1].status__c=PRA_Constants.CU_STATUS_SENT;
        update ctList;        
        
        System.runAs(testUser){            
            test.startTest(); 
                 //make a map with all the values in the custom setting object , which also has the permission set names.
                Map<String,PMC_ProfileGroup__c> ProfileSelectList = PMC_ProfileGroup__c.getall() ;
                system.debug('-------ProfileSelectList----'+ProfileSelectList);        
                Set<String> ProfileSelectSet =  ProfileSelectList.keyset();
                
                //Pass all the permissionset labels to batch queue object as s JSON string.
                String paramsJSONString ='["'+ProfileSelectSet+'"]';                 
                String unitBatchQueueId = PRA_BatchQueue.addJob('PRA_WBSEmailBatch',paramsJSONString, null);            
            test.stopTest();
            System.assertNotEquals(null, unitBatchQueueId);             
        }        
    }
}