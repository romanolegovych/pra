/**
 * @author Konstantin Ermolenko
 * @Date 12/24/2014
 * @Description VF page controller which contains all logic needed for configuring a Linked Step 
 */
public with sharing class PAWS_StepProperty_LinkedStepController
{
	public STSWR1__Flow_Step_Junction__c Step {get; set;}
	public STSWR1__Flow_Step_Property__c Property {get; set;}
	
	public Map<String, Set<ID>> StepsToExclude {get; set;}
	
	public String LinkedStepType {get; set;}
	public String FolderId {get; set;}
	public String LinkedFlowId {get; set;}
	public String LinkedStepId {get; set;}
	public String SatellitePropertyId {get; set;}
	public String SelectedItemId {get; set;}
	
	public Boolean IsInvalidConfig {get; set;}
	public Boolean IsSaveSuccessfull {get; set;}
	public String LinkedStepDataJSON {get; set;}
	
	public void Init()
	{
		IsInvalidConfig = false;
		IsSaveSuccessfull = false;
		LinkedStepType = 'Master'; //Slave
		FolderId = '-1';
		LinkedFlowId = '-1';
		LinkedStepId = '-1';
		SelectedItemId = '-1';
		
		try
		{
			String stepId = ApexPages.currentPage().getParameters().get('stepId');
			String propertyId = ApexPages.currentPage().getParameters().get('propertyId');
			
			Step = [Select STSWR1__Flow__c, STSWR1__Flow__r.STSWR1__Object_Type__c From STSWR1__Flow_Step_Junction__c Where Id = :stepId];
			LinkedFlowId = Step.STSWR1__Flow__c;
			
			if (!String.isEmpty(propertyId))
			{
				Property = [Select STSWR1__Value__c, STSWR1__Type__c, STSWR1__Key__c From STSWR1__Flow_Step_Property__c Where Id = :propertyId];
				LinkedStepData stepData;
				Map<String, String> keyData = new Map<String, String>();
				
				try
				{
					stepData = (LinkedStepData) JSON.deserialize(Property.STSWR1__Value__c, LinkedStepData.class);
					keyData = (Map<String, String>) JSON.deserialize(Property.STSWR1__Key__c, Map<String, String>.class);
				}
				catch(Exception ex){/*do nothing*/}
				
				IsInvalidConfig = (keyData.get('invalid') == 'true');
				if (stepData != null && !IsInvalidConfig)
				{
					LinkedStepType = stepData.Config.get('type');
					SatellitePropertyId = keyData.get('satelliteID');
					
					List<STSWR1__Flow_Step_Junction__c> linkedSteps = [Select STSWR1__Flow__c From STSWR1__Flow_Step_Junction__c Where Id = :stepData.Config.get('stepID')];
					LinkedStepId = (linkedSteps.size() > 0 ? (String) linkedSteps.get(0).Id : '-1');
					LinkedFlowId = (linkedSteps.size() > 0 ? (String) linkedSteps.get(0).STSWR1__Flow__c : LinkedFlowId);
					IsInvalidConfig = linkedSteps.IsEmpty();
				}
			}
			
			StepsToExclude = new Map<String, Set<ID>>{
				'Master' => new Set<ID>{Step.Id},
				'Slave' => new Set<ID>{Step.Id}
			};
			
			for (STSWR1__Flow_Step_Property__c p : [Select STSWR1__Value__c, STSWR1__Key__c From STSWR1__Flow_Step_Property__c Where STSWR1__Type__c = 'Linked Step' And STSWR1__Flow_Step__c = :Step.Id])
			{
				Map<String, String> keyData = (Map<String, String>) JSON.deserialize(p.STSWR1__Key__c, Map<String, String>.class);
				if (keyData.get('invalid') != 'true')
				{
					LinkedStepData stepData = (LinkedStepData) JSON.deserialize(p.STSWR1__Value__c, LinkedStepData.class);
					if (Property == null || Property.Id != p.Id)
					{
						StepsToExclude.get(stepData.Config.get('type')).add(stepData.Config.get('stepID'));
						StepsToExclude.get(stepData.Config.get('type') == 'Master' ? 'Slave' : 'Master').add(stepData.Config.get('stepID'));
					}
				}
			}
			
			STSWR1__Item__c item = [Select STSWR1__Parent__c From STSWR1__Item__c Where STSWR1__Source_Flow__c = :LinkedFlowId];
			FolderId = item.STSWR1__Parent__c;
			if (String.isEmpty(FolderId)) FolderId = '-1';
			
			SelectedItemId = item.Id;
		}
		catch(Exception ex)
		{
			ApexPages.addMessages(ex);
		}
	}
	
	public List<SelectOption> Items
	{
		get
		{
			if (Items == null)
			{
				Items = new List<SelectOption>();
				
				Items.add(new SelectOption('-1', '--Select Flow--'));
				
				try
				{
					if (FolderId != null)
					{
						STSWR1__Item__c parentItem = [Select STSWR1__Parent__c, STSWR1__Parent__r.STSWR1__Name_Value__c, STSWR1__Name_Value__c From STSWR1__Item__c Where Id = :FolderId];
						Items.add(new SelectOption('folder-' + (parentItem.STSWR1__Parent__c == null ? 'root' : parentItem.STSWR1__Parent__c), '[..] - go up to "' + (parentItem.STSWR1__Parent__c == null ? 'root' : parentItem.STSWR1__Parent__r.STSWR1__Name_Value__c) + '" (current folder: "' + parentItem.STSWR1__Name_Value__c + '")'));
					}
					
					List<STSWR1__Item__c> folders = [Select STSWR1__Name_Value__c From STSWR1__Item__c Where STSWR1__Type__c = 'Folder' And STSWR1__Parent__c = :FolderId Order By STSWR1__Name_Value__c];
					for (STSWR1__Item__c folder : folders)
					{
						Items.add(new SelectOption('folder-' + folder.Id, '[' + folder.STSWR1__Name_Value__c + ']'));
					}
					
					List<STSWR1__Item__c> flows = [Select STSWR1__Name_Value__c From STSWR1__Item__c Where STSWR1__Type__c = 'File' And STSWR1__Parent__c = :FolderId And STSWR1__Source_Flow__r.STSWR1__Object_Type__c = :Step.STSWR1__Flow__r.STSWR1__Object_Type__c Order By STSWR1__Name_Value__c];
					for (STSWR1__Item__c flow : flows)
					{
						Items.add(new SelectOption(flow.Id, flow.STSWR1__Name_Value__c));
					}
				}
				catch(Exception ex)
				{
					ApexPages.addMessages(ex);
				}
			}
			return Items;
		}
		set;
	}
	
	public List<SelectOption> Steps
	{
		get
		{
			if (Steps == null)
			{
				Steps = new List<SelectOption>();
				
				Steps.add(new SelectOption('-1', '--Select Step--'));
				
				try
				{
					if (LinkedFlowId != '-1')
					{
						for (STSWR1__Flow_Step_Junction__c flowStep : [Select Name From STSWR1__Flow_Step_Junction__c Where STSWR1__Flow__c = :LinkedFlowId Order By STSWR1__Index__c])
						{
							Steps.add(new SelectOption(flowStep.Id, flowStep.Name, StepsToExclude.get(LinkedStepType).contains(flowStep.Id)));
						}
					}
				}
				catch(Exception ex)
				{
					ApexPages.addMessages(ex);
				}
			}
			return Steps;
		}
		set;
	}
	
	public void LinkedStepTypeOnChange()
	{
		Steps = null;
		LinkedStepId = '-1';
		IsInvalidConfig = false;
	}
	
	public void SelectedItemOnChange()
	{
		Steps = null;
		LinkedStepId = '-1';
		
		try
		{
			if (SelectedItemId.startsWith('folder-'))
			{
				Items = null;
				FolderId = SelectedItemId.split('-')[1];
				FolderId = (FolderId == 'root' ? null : FolderId);
				LinkedFlowId = '-1';
			}
			else
			{
				LinkedFlowId = (SelectedItemId == '-1' ? '-1' : [Select STSWR1__Source_Flow__c From STSWR1__Item__c Where Id = :SelectedItemId].get(0).STSWR1__Source_Flow__c);
			}
			
			IsInvalidConfig = false;
		}
		catch(Exception ex)
		{
			ApexPages.addMessages(ex);
		}
	}
	
	public void Save()
	{
		try
		{
			if (String.isEmpty(LinkedStepId) || LinkedStepId == '-1') throw new PAWS_API.PAWS_APIException('Step is not selected.');
			Set<ID> stepsToCheck = new Set<ID>{Step.Id};
			if (LinkedStepType == 'Master') stepsToCheck.add(LinkedStepId);
			if (CheckForLoop(stepsToCheck, new Set<ID>()) == true) throw new PAWS_API.PAWS_APIException('Linked Steps loop detected. Please select another step.');
			
			STSWR1__Flow_Step_Junction__c flowStep = [Select Name, STSWR1__Guid__c, STSWR1__Flow__r.Name From STSWR1__Flow_Step_Junction__c Where Id = :LinkedStepId];
			LinkedStepDataJSON = new LinkedStepData(flowStep, LinkedStepType).toJson();
			IsSaveSuccessfull = true;
		}
		catch(Exception ex)
		{
			IsSaveSuccessfull = false;
			ApexPages.addMessages(ex);
		}
	}
	
	private Boolean CheckForLoop(Set<ID> stepsToCheck, Set<ID> checkedSteps)
	{
		Boolean isLoop = false;
		
		for (ID stepId : stepsToCheck)
		{
			if (checkedSteps.contains(stepId))
			{
				isLoop = true;
				break;
			}
		}
		
		if (!isLoop)
		{
			Set<ID> masterStepIds = new Set<ID>();
			List<STSWR1__Flow_Step_Property__c> properties = [Select STSWR1__Key__c, STSWR1__Value__c From STSWR1__Flow_Step_Property__c Where STSWR1__Type__c = 'Linked Step' And STSWR1__Flow_Step__c in :stepsToCheck];
			for (STSWR1__Flow_Step_Property__c p : properties)
			{
				if (p.Id != SatellitePropertyId)
				{
					Map<String, String> keyData = (Map<String, String>) JSON.deserialize(p.STSWR1__Key__c, Map<String, String>.class);
					if (keyData.get('invalid') != 'true')
					{
						PAWS_StepProperty_LinkedStepController.LinkedStepData linkedStepData = (PAWS_StepProperty_LinkedStepController.LinkedStepData) JSON.deserialize(p.STSWR1__Value__c, PAWS_StepProperty_LinkedStepController.LinkedStepData.class);
						if (linkedStepData.Config.get('type') == 'Master')
						{
							masterStepIds.add(linkedStepData.Config.get('stepID'));
						}
					}
				}
			}
			
			if (masterStepIds.size() > 0)
			{
				checkedSteps.addAll(stepsToCheck);
				isLoop = CheckForLoop(masterStepIds, checkedSteps);
			}
		}
		
		return isLoop;
	}
	
	public class LinkedStepData
	{
		public Map<String, String> Config {get; set;}
		public String Description {get; set;}
		
		public LinkedStepData(){/*do nothing*/}
		
		public LinkedStepData(STSWR1__Flow_Step_Junction__c step, String linkedStepType)
		{
			this.LoadLinkedStepData(step, linkedStepType, true);
		}
		
		public LinkedStepData(STSWR1__Flow_Step_Junction__c step, String linkedStepType, Boolean escapeDescription)
		{
			this.LoadLinkedStepData(step, linkedStepType, escapeDescription);
		}
		
		public void LoadLinkedStepData(STSWR1__Flow_Step_Junction__c step, String linkedStepType, Boolean escapeDescription)
		{
			this.Config = new Map<String, String>{
				'type' => linkedStepType,
				'stepGUID' => step.STSWR1__Guid__c,
				'stepID' => step.Id
			};
			
			this.Description = generateDescription(step, linkedStepType, escapeDescription);
		}
		
		public String toJson()
		{
			return JSON.serialize(this);
		}
		
		public String generateDescription(STSWR1__Flow_Step_Junction__c step, String linkedStepType, Boolean escape)
		{
			String description = (linkedStepType == 'Master' ? 'Controls "' : 'Is controlled by "') + step.Name + '" step in "' + step.STSWR1__Flow__r.Name + '" flow.';
			return (escape ? description.replace('\\', '\\\\').replace('"', '\\"') : description);
		}
	}
}