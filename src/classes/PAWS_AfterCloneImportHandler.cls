/**
 * @author Konstantin Ermolenko
 * @description contains logic that is executed after a WR flow is cloned or imported
*/
public with sharing class PAWS_AfterCloneImportHandler implements STSWR1.FlowPostImportInterface
{
    public void Execute(Map<String, String> oldNewFlowIdMap)
    {
        Set<String> newFlowIds = new Set<String>(oldNewFlowIdMap.values());
        new PAWS_StepProperty_LinkedStepHelper().AfterCloneImport(newFlowIds);
        
        //Clones Critical Chains
        //new CC_Clone(OldNewFlowIdMap).cloneChains();
        System.debug(System.LoggingLevel.ERROR, 'PAWS_AfterCloneImportHandler.Map: ' + oldNewFlowIdMap);//@TODO: Delete this line!
        //* Code moved to PAWS_FlowTrigger.createEWs method
        if (Type.forName('CC_Clone') != null)
        {
        	Object ccHandler = Type.forName('CC_Clone').newInstance();
        	if (ccHandler instanceof STSWR1.FlowPostImportInterface)
        	{
         		((STSWR1.FlowPostImportInterface) ccHandler).execute(oldNewFlowIdMap);
        	}
        }
        /**/
    }
}