/** 
@Author Guo Grace 
@Date 04/15/2015
@Description this class is inserting the Service Model.
*/
public with sharing class PBB_ServiceModelController {

    public  ID  smID                {get;set;}                 //to get the Service Model ID
    public  Service_Model__c sm     {get;set;}                 //to get the Service Model Sobject for edit or new   
	String clone;
    
     public PBB_ServiceModelController(ApexPages.StandardController controller){
        
         
        //check if the SM is for edit
        sm = (Service_Model__c)controller.getRecord();
        smID = ApexPages.currentPage().getParameters().get('id');
		       
        clone =ApexPages.currentPage().getParameters().get('clone');
         
        //check if record exists or not
        if(smID !=null){      
            sm  = PBB_DataAccessor.getServiceModel(smID);  
      
            //Cloning Service Model
            if(clone !=null){
                sm  =sm.clone(false, true);
                sm.Status__c='In-Process';
            }                       
        }
        else
        {  
            sm.Status__c='In-Process';   
        }
    }     
	
    /** 
	@Author Guo Grace 
	@Date 04/15/2015
	@Description this class is inserting the Service Model.
	*/
    public PageReference saveSMQuestions(){
    	
    	PageReference pageRef =null;   
    	
        
        try{   
            
            upsert sm ;
            if(clone !=null)
           		PBB_Services.cloneServiceModelChildRecords(smID,sm.id);
            pageRef = new PageReference('/'+sm.id);
            pageRef.setRedirect(true);                 
         }
        catch(DMLException e){
             ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0) );
             ApexPages.addMessage(msg);        
        }     
        
        return pageRef;  
    }
}