@istest public with sharing class PAWS_WFMSiteTriggerTest
{
	@istest private static void testTrigger()
	{
		for (AsyncApexJob pawsJob : [select Id from AsyncApexJob where ApexClass.Name = 'PAWS_Scheduler'])
		{
			System.abortJob(pawsJob.Id);
		}
		
		for (CronTrigger cronJob : [select Id from CronTrigger where CronJobDetailId in (select Id from CronJobDetail where Name = 'PAWS_Scheduler')])
		{
			System.abortJob(cronJob.Id);
		}
		
		PAWS_APITests.testAssociatePAWSProjectSiteWith();
		update [select Name from WFM_Site_Detail__c];
		
		System.assertEquals(1, [select count() from PAWS_Project_Flow_Site__c where WFM_Site__c != null]);
	}
}