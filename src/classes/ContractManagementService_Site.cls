//Generated by wsdl2apex

public class ContractManagementService_Site {
    public class site {
        public String id;
        public String city;
        public String country;
        public String investigatorFirstName;
        public String investigatorLastName;
        public String sponsorId;
        public String sponsor;
        public String protocol;
        public String region;
        public String accountId;
        public String account;
        public String siteNumber;
        public String status;
        private String[] id_type_info = new String[]{'id','http://www.siebel.com/xml/PRACTASite',null,'0','1','false'};
        private String[] city_type_info = new String[]{'city','http://www.siebel.com/xml/PRACTASite',null,'0','1','false'};
        private String[] country_type_info = new String[]{'country','http://www.siebel.com/xml/PRACTASite',null,'0','1','false'};
        private String[] investigatorFirstName_type_info = new String[]{'investigatorFirstName','http://www.siebel.com/xml/PRACTASite',null,'0','1','false'};
        private String[] investigatorLastName_type_info = new String[]{'investigatorLastName','http://www.siebel.com/xml/PRACTASite',null,'0','1','false'};
        private String[] sponsorId_type_info = new String[]{'sponsorId','http://www.siebel.com/xml/PRACTASite',null,'0','1','false'};
        private String[] sponsor_type_info = new String[]{'sponsor','http://www.siebel.com/xml/PRACTASite',null,'0','1','false'};
        private String[] protocol_type_info = new String[]{'protocol','http://www.siebel.com/xml/PRACTASite',null,'0','1','false'};
        private String[] region_type_info = new String[]{'region','http://www.siebel.com/xml/PRACTASite',null,'0','1','false'};
        private String[] accountId_type_info = new String[]{'accountId','http://www.siebel.com/xml/PRACTASite',null,'0','1','false'};
        private String[] account_type_info = new String[]{'account','http://www.siebel.com/xml/PRACTASite',null,'0','1','false'};
        private String[] siteNumber_type_info = new String[]{'siteNumber','http://www.siebel.com/xml/PRACTASite',null,'0','1','false'};
        private String[] status_type_info = new String[]{'status','http://www.siebel.com/xml/PRACTASite',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.siebel.com/xml/PRACTASite','true','false'};
        private String[] field_order_type_info = new String[]{'id','city','country','investigatorFirstName','investigatorLastName','sponsorId','sponsor','protocol','region','accountId','account','siteNumber','status'};
    }
    public class sites {
        public ContractManagementService_Site.site[] site;
        private String[] site_type_info = new String[]{'site','http://www.siebel.com/xml/PRACTASite',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.siebel.com/xml/PRACTASite','true','false'};
        private String[] field_order_type_info = new String[]{'site'};
    }
    public class sitesTopElmt {
        public ContractManagementService_Site.sites sites;
        private String[] sites_type_info = new String[]{'sites','http://www.siebel.com/xml/PRACTASite',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.siebel.com/xml/PRACTASite','true','false'};
        private String[] field_order_type_info = new String[]{'sites'};
    }
}