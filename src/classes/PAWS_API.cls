public without sharing class PAWS_API
{
    public class PAWS_APIException extends Exception {}
    
    /*
    public class PBB_WR_Step
    {
        public ID WRStepId {get; set;}
        public String CountryName {get; set;}
        public String RoleCode {get; set;}
        public String Effort {get; set;}
        public Date StartDate {get; set;}
        public Date EndDate {get; set;}
    }
    
    public static List<PBB_WR_Step> PBB_GetWRSteps(ID pawsProjectId)
    {
        return new List<PBB_WR_Step>();
    }
    */

    /******************** SEARCH TEMPLATE SECTION *************************/
    
    public List<ID> getTemplate(sObject record)
    {
        List<sObject> records = (List<sObject>)Type.forName('List<' + record.getSObjectType() + '>').newInstance();
        records.add(record);
        
        Map<ID, List<ID>> result = getTemplates(records);
        return result.get(record.Id);
    }
    
    public Map<ID, List<ID>> getTemplates(List<sObject> records)
    {
        return PAWS_APIHelper.getTemplates(records);
    }
    
    /******************** ACTIVATE FLOW SECTION *************************/
    
    public void activateFlows(List<sObject> records)
    {
        List<sObject> actualRecords = (List<sObject>)Type.forName('List<' + records.getSObjectType() + '>').newInstance();
        for(sObject record : records)
        {
            if(record.get('Status__c') != 'Inactive') continue;
            actualRecords.add(record);
        }
        
        if(actualRecords.size() == 0) return;
        
        Set<ID> activatingRecordsIds = new Set<ID>();
        Map<ID, List<ID>> templatesMap = getTemplates(actualRecords);
        List<Map<String, String>> tasks = new List<Map<String, String>>();
        for(sObject record : actualRecords)
        {
            List<ID> templateIds = templatesMap.get(record.Id);
            if(templateIds == null) continue;
            
            for(ID templateId : templateIds)
            {
                if(!String.isEmpty(templateId)) 
                {
                	activatingRecordsIds.add(record.Id);
                    tasks.add(new Map<String, String>{
                        'Type__c' => 'Activate Flow',
                        'Data__c' => JSON.serialize(new Map<String, String>{'sourceId' => record.Id, 'templateId' => templateId})
                    });
                }
            }
        }
        
        if(activatingRecordsIds.size() > 0)
        {
        	PAWS_Queue.addTasks(tasks);
        
	        actualRecords = Database.query('select Status__c from ' + records.getSObjectType() + ' where Id in :activatingRecordsIds');
	        for(sObject record : actualRecords)
	      		record.put('Status__c', 'Activating');
	
	        STSWR1.AbstractTrigger.Disabled = true;
	        update actualRecords;
	        STSWR1.AbstractTrigger.Disabled = false;
        }
    }

    public sObject activateFlow(ID sourceId, ID templateId)
    {
        sObject source;
        Map<String, Object> history;
        Map<String, Object> templateHistory;
        
        try
        {
            if (!Test.isRunningTest()) new STSWR1.API().call('LimitsService', 'setEnabled', true);
            
            source = PAWS_APIHelper.loadSource(sourceId);
            if(source == null) throw new PAWS_APIException('Can not find Source Object with ID \'' + sourceId + '\'!');

            history = PAWS_APIHelper.getHistory(source, templateId);
            templateHistory = (Map<String, Object>)history.get(templateId);
            
            if(templateHistory != null && templateHistory.get('status') == 'Active') return source;
            templateHistory = PAWS_APIHelper.prepareHistory(templateId, history);
            
            String connectionPoint = PAWS_APIHelper.findConnectionPoint(templateId);
            if(String.isEmpty(connectionPoint)) throw new PAWS_APIException('Can not find Connection Point in template with ID \'' + templateId + '\'!');
            
            List<String> connectionPointItems = connectionPoint.split(':');
            String parentFlowName = connectionPointItems[0];
            String parentStepName = connectionPointItems[1];
            
            PAWS_Project_Flow_Junction__c projectFlow = PAWS_APIHelper.loadProjectFlow((String)source.get('PAWS_Project__c'), parentFlowName);
            if(projectFlow == null) throw new PAWS_APIException('Can not find flow for Project with Id \'' + (String)source.get('PAWS_Project__c') + '\'!');

            ID sourceFolderId = PAWS_APIHelper.setupSourceFolder(source, projectFlow.Folder__c);
            if(String.isEmpty(sourceFolderId)) throw new PAWS_APIException('Can not find folder for cloning!');

            ID parentFlowId = PAWS_APIHelper.findParentFlow(projectFlow.Folder__c, sourceFolderId, parentFlowName);
            if(String.isEmpty(parentFlowId)) throw new PAWS_APIException('Can not find flow with name "' + parentFlowName + '" in folder "' + sourceFolderId +'"!');

            STSWR1__Flow__c clonedFlow = PAWS_APIHelper.cloneTemplates(sourceFolderId, source, templateHistory);
            while(clonedFlow != null)
            {
                clonedFlow = PAWS_APIHelper.cloneTemplates(sourceFolderId, source, templateHistory);
            }

            if (!Test.isRunningTest()) PAWS_Utilities.checkLimits(new Map<String, Integer> 
            {
                'Queries' => 10,
                'DMLStatements' => 10
            });
            
            if (!Test.isRunningTest()) new STSWR1.API().call('LimitsService', 'checkLimits', new Map<String, Integer> 
            {
                'Queries' => 20,
                'DMLStatements' => 30
            });

            STSWR1__Flow__c sourceFlow = PAWS_APIHelper.loadSourceFlow(templateId, source, templateHistory);
            if(sourceFlow == null) throw new PAWS_APIException('Can not find source flow after cloning!');

            PAWS_APIHelper.fixActions(templateHistory);
            STSWR1__Flow_Step_Action__c startSubFlowAction = PAWS_APIHelper.createStartSubFlowAction(sourceFlow, source, parentFlowId, parentStepName, templateHistory);
            STSWR1__Flow_Instance__c sourceFlowInstance = PAWS_APIHelper.startWorkflow(sourceFlow, source, parentFlowId, startSubFlowAction);
            
            templateHistory.clear();
            templateHistory.put('flow', sourceFlow.Id);
            if(startSubFlowAction != null) templateHistory.put('action', startSubFlowAction.Id);
            if(sourceFlowInstance != null) templateHistory.put('flowInstance', sourceFlowInstance.Id);
            templateHistory.put('status', 'Active');
        }
        catch(Exception ex)
        {
            if(!PAWS_Utilities.isLimitException(ex))
                PAWS_APIHelper.rollBack(source, templateHistory, ex);
        }
        
        return PAWS_APIHelper.commitHistory(source, history);
    }       
    
    /******************** COMPLETE SUB-FLOW PARENT STEP SECTION *************************/
    
    public void completeParentStep(sObject context, STSWR1__Flow_Instance_Cursor__c flowInstanceCursor)
    {
        try
        {
            STSWR1__Flow_Instance__c flowInstance = flowInstanceCursor.STSWR1__Flow_Instance__r;
            if(flowInstance.STSWR1__Number_of_Active_Cursors__c > 1) return;
            
            Integer totalLocked = [select count() from STSWR1__Flow_Instance__c where Id != :flowInstance.Id and STSWR1__Parent__c = :flowInstance.STSWR1__Parent__c and STSWR1__Is_Active__c = true and STSWR1__Is_Completed__c != 'Yes' and STSWR1__Lock_Parent__c = true and STSWR1__Lock_Parent_Step__c = :flowInstance.STSWR1__Lock_Parent_Step__c];
            if(totalLocked > 0) return;
            
            List<STSWR1__Flow_Instance_Cursor__c> parentCursors = [select STSWR1__Actions_History__c from STSWR1__Flow_Instance_Cursor__c where STSWR1__Flow_Instance__c = :flowInstance.STSWR1__Parent__c and STSWR1__Step__c = :flowInstance.STSWR1__Lock_Parent_Step__c and STSWR1__Status__c != 'Complete'];
            if(parentCursors.size() > 0) completeCursor(parentCursors[0]);
        }catch(Exception ex)
        {
            throw ex;
        }
    }
    
    public void completeCursor(STSWR1__Flow_Instance_Cursor__c parentCursor)
    {
        completeCursor(parentCursor, true);
    }
    
    public void completeCursor(STSWR1__Flow_Instance_Cursor__c parentCursor, Boolean ignoreLockedBySubFlows)
    {
        if (ignoreLockedBySubFlows)
        {
           //Ignore LockedBySubFlows Rule
           {
               Map<String, Object> cache = new Map<String, Object>();
               if(!String.isEmpty(parentCursor.STSWR1__Actions_History__c)) cache = (Map<String, Object>)JSON.deserializeUntyped(parentCursor.STSWR1__Actions_History__c);
               cache.put('LockedByChilds', DateTime.now().getTime());
               parentCursor.STSWR1__Actions_History__c = JSON.serialize(cache);
               
               STSWR1.AbstractTrigger.Disabled = true;
               update parentCursor;
               STSWR1.AbstractTrigger.Disabled = false;
           }
        }
        
        STSWR1.API wrapi = new STSWR1.API();
        parentCursor = (STSWR1__Flow_Instance_Cursor__c)wrapi.call('WorkflowService', 'getFlowInstanceCursorById', parentCursor.Id);
        
        //save initial CommonObject values
        Object comments = wrapi.call('WorkflowService', 'getCommonObject', 'Comments');
        Object actualStartDate = wrapi.call('WorkflowService', 'getCommonObject', 'ActualStartDate');
        Object actualCompleteDate = wrapi.call('WorkflowService', 'getCommonObject', 'ActualCompleteDate');
        
        system.debug('----ACTION: COMPLETE STEP, comments = ' + comments);
        system.debug('----ACTION: COMPLETE STEP, actualStartDate = ' + actualStartDate);
        system.debug('----ACTION: COMPLETE STEP, actualCompleteDate = ' + actualCompleteDate);
        
        //remove CommonObject values in order to avoid setting current comments and actuals to parent step
        wrapi.call('WorkflowService', 'removeCommonObject', 'Comments');
        wrapi.call('WorkflowService', 'removeCommonObject', 'ActualStartDate');
        wrapi.call('WorkflowService', 'removeCommonObject', 'ActualCompleteDate');
        
        Boolean complete = false;
        while(!complete)
        {
            complete = (Boolean)wrapi.call('WorkflowService', 'completestep', new Map<String, Object>
                {
                    'flowInstanceCursor' => parentCursor,
                    'connectorType' => (parentCursor.STSWR1__Step_Type__c == 'Approval' ? 'Approve' : 'Proceed')
                });
        }
        
        //restore CommonObject values so that they are saved correctly to current step that is being proceeded
        wrapi.call('WorkflowService', 'setCommonObject', new Map<String, Object>{'name' => 'Comments', 'value' => comments});
        wrapi.call('WorkflowService', 'setCommonObject', new Map<String, Object>{'name' => 'ActualStartDate', 'value' => actualStartDate});
        wrapi.call('WorkflowService', 'setCommonObject', new Map<String, Object>{'name' => 'ActualCompleteDate', 'value' => actualCompleteDate});
    }
    
    /*************************** TRIGGER METHODS *****************************/
    
    public void beforeUpsert(List<SObject> records)
    {
        Schema.DescribeSObjectResult describe = records.getSObjectType().getDescribe();
        
        for (SObject record : records)
        {
            record.put('Project__c', record.get('Project_ID__c'));
            record.put('Protocol__c', record.get('Protocol_ID__c'));
            if(describe.fields.getMap().containsKey('PAWS_Project_ID__c')) record.put('PAWS_Project__c', record.get('PAWS_Project_ID__c'));
            if(describe.fields.getMap().containsKey('PAWS_Country_ID__c')) record.put('PAWS_Country__c', record.get('PAWS_Country_ID__c'));
        }

        if(describe.name == 'PAWS_Project_Flow_Site__c')
        {
            PAWS_PBB_ProjectHelper.beforeUpsert((List<PAWS_Project_Flow_Site__c>)records);
        }
    }
    
    public void afterDelete(List<SObject> records)
    {
        Set<ID> folderIds = new Set<ID>();
        for(SObject record : records)
        {
            if(!String.isEmpty((ID)record.get('Folder__c'))) folderIds.add((ID)record.get('Folder__c'));
        }
        
        PAWS_Utilities.checkLimits(new Map<String, Integer> 
        {
            'Queries' => 1,
            'DMLStatements' => 1
        });
        
        delete [select Id from STSWR1__Item__c where Id in :folderIds];
    }

    /*************************** PBB METHODS *****************************/

    public static List<PAWS_PBB_Helper.WorkItem> PBB_getWorkBreakdown(ID pawsProjectId)
    {
        return PAWS_PBB_Helper.getWorkBreakdown(pawsProjectId);
    }
    
	public static List<PAWS_PBB_Helper.WrModelStepWrapper> PBB_getWRModelSteps(ID flowId)
	{
		return PAWS_PBB_Helper.getWRModelSteps(flowId);
	}

    /*************************** PBB MOBILE METHODS *********************/
    
    public static List<PAWS_ProjectCVO> PBB_getActiveProjects(String sponsorId)
    {
        return new PAWS_PBB_ProjectHelper().getActiveProjects(sponsorId);
    }

    public static PAWS_ProjectDetailCVO PBB_getProjectDetails(ID projectRecordID)
    {
        return new PAWS_PBB_ProjectHelper().getProjectDetails(projectRecordID);
    }

    public static List<PAWS_ProjectDetailCVO> PBB_getActiveProjectList(String sponsorId)
    {
        return new PAWS_PBB_ProjectHelper().getActiveProjectList();
    }

    public static List<PAWS_ProjectDetailCVO> PBB_getActiveProjectList()
    {
        return new PAWS_PBB_ProjectHelper().getActiveProjectList();
    }

    public static Boolean PBB_setAggregatedMilestone(ID projectRecordID, Integer goal, Date targetDate, String milestoneName)
    {
        Boolean success = false;
        List<EW_Aggregated_Milestone__c> milestoneList = [select Name from EW_Aggregated_Milestone__c where PAWS_Project_Flow__c = :projectRecordID and Name = :milestoneName];
        if(milestoneList.isEmpty() == false)
        {
            EW_Aggregated_Milestone__c milestone = milestoneList.get(0);
            milestone.Milestone_Target_Date__c = targetDate;
            milestone.Goal__c = goal;
            update milestone;
            
            success = true;
        }
        return success;
    }
    
    /*************************** WR API METHODS *****************************/
    
    /*
    * Description: API method for proceeding simple WR flows that do not have parallel branches
    *
    * @recordId - ID of your Salesforce record that has a running WR flow on it
    * @flowId - ID of WR Flow that you want to proceed
    * @actionType - how do you want to proceed a flow. Accepts: Approve, Reject, Proceed. If step is not of "approval" type and "Approve" action is provided, it will automatically replaced with "Proceed" action
    * @comments - optional. If you don't want to provide a comment just pass an empty string or null.
    */
    public static FlowProceedResult proceedFlow(ID recordId, ID flowId, String actionType, String comments)
    {
        FlowProceedResult result = new FlowProceedResult();
        
        try
        {
            List<STSWR1__Flow_Instance_Cursor__c> cursorList = [select STSWR1__Step__r.STSWR1__Type__c from STSWR1__Flow_Instance_Cursor__c where STSWR1__Is_Completed__c = 'No' And STSWR1__Flow_Instance__r.STSWR1__Flow__c = :flowId And STSWR1__Flow_Instance__r.STSWR1__Object_Id__c = :recordId];
            
            if (cursorList.size() == 0) throw new PAWS_APIException('Nothing to proceed was found.');
            if (cursorList.size() > 1) throw new PAWS_APIException('Flow has more than one active branch. Use "proceedStep" API method instead.');
            
            result = proceedStep(recordId, cursorList.get(0).STSWR1__Step__r.Id, (actionType == 'Approve' && cursorList.get(0).STSWR1__Step__r.STSWR1__Type__c != 'Approval' ? 'Proceed' : actionType), comments);
        }
        catch(Exception ex)
        {
            result.errors.add(ex.getMessage());
        }
        
        return result;
    }
    
    /*
    * Description: API method for proceeding WR flows that have parallel branches
    *
    * @recordId - ID of your Salesforce record that has a running WR flow on it
    * @stepId - ID of WR Step that you want to proceed. Step always belongs to specific Branch and Flow which are unique in bounds of your record
    * @actionType - how do you want to proceed a step. Accepts: Approve, Reject, Proceed. If step type is not "approval" you need to pass "Proceed" instead of "Approve"
    * @comments - optional. If you don't want to provide a comment just pass an empty string or null.
    */
    public static FlowProceedResult proceedStep(ID recordId, ID stepId, String actionType, String comments)
    {
        return proceedStep(recordId, stepId, new Map<String, Object>{
            'comments' => comments,
            'connectorType' => actionType
        });
    }
    
    
    /*
    * Description: generic API method for proceeding WR flows that have parallel branches
    *
    * @recordId - ID of your Salesforce record that has a running WR flow on it
    * @stepId - ID of WR Step that you want to proceed. Step always belongs to specific Branch and Flow which are unique in bounds of your record
    * @params - various parameters needed for proceed. You can find an example of such map in "proceedStep" method.
    */
    public static FlowProceedResult proceedStep(ID recordId, ID stepId, Map<String, Object> params)
    {
        FlowProceedResult result = new FlowProceedResult();
        result.params = params;
        
        try
        {
            List<STSWR1__Flow_Instance_Cursor__c> cursorList = [select Id from STSWR1__Flow_Instance_Cursor__c where STSWR1__Is_Completed__c = 'No' And STSWR1__Step__c = :stepId And STSWR1__Flow_Instance__r.STSWR1__Object_Id__c = :recordId];
            if(cursorList.isEmpty() == true)
            {
                throw new PAWS_APIException('Step is not found or is not active.');
            }
            
            STSWR1.API wrapi = new STSWR1.API();
            STSWR1__Flow_Instance_Cursor__c cursor = (STSWR1__Flow_Instance_Cursor__c) wrapi.call('WorkflowService', 'getFlowInstanceCursorById', cursorList.get(0).Id);
            
            params.put('flowInstanceCursor', cursor);
            
            proceedStep(result);
        }
        catch(Exception ex)
        {
            result.errors.add(ex.getMessage());
        }
        
        return result;
    }
    
    public static void proceedFlow(FlowProceedResult proceedResult)
    {
        proceedStep(proceedResult);
    }
    
    public static void proceedStep(FlowProceedResult proceedResult)
    {
        try
        {
            STSWR1.API wrapi = new STSWR1.API();
            proceedResult.isDone = (Boolean) wrapi.call('WorkflowService', 'completeorskipstep', proceedResult.params);
        }
        catch(Exception ex)
        {
            proceedResult.errors.add(ex.getMessage());
        }
    }
    
    public class FlowProceedResult
    {
        public Boolean isDone {get; set;}
        public List<String> errors {get; set;}
        public Map<String, Object> params {get; set;}
        
        public FlowProceedResult()
        {
            errors = new List<String>();
            isDone = false;
            params = new Map<String, Object>();
        }
    }
    
    /*************************** RM METHODS *****************************/
    
    public static void updateLookupIDsForPAWSProjectFlows (String projectName, String ecrfId) {
    
        List <RM_PAWSCVO> lstAss = RM_PAWSService.getActiveAssignmentByProject(projectName);
        List <String> requiredRoles = new List <String> {'Project Manager', 'Clinical Informatics Manager', 'Clinical Team Manager', 'Start-Up Lead'};
        
        Map<String, String> rolesAndUsers = new Map<String, String>();
        for (RM_PAWSCVO a : lstAss) {
            for (String b : requiredRoles) {
                if (a.Project_Role == b && a.UserID != null) {
                    rolesAndUsers.put(a.Project_Role, a.UserID);
                }
            }
        }
        
        ecrf__c projectToUpdate = [SELECT Project_Manager__c, Clinical_Informatics_Manager__c, CTM__c, Study_Start_Up_Manager__c, Escalation_Executive__c 
                                    FROM ecrf__c WHERE id = :ecrfId LIMIT 1];
        projectToUpdate.Project_Manager__c = (rolesAndUsers.containsKey('Project Manager') ? rolesAndUsers.get('Project Manager') : projectToUpdate.Project_Manager__c);
        projectToUpdate.Clinical_Informatics_Manager__c = (rolesAndUsers.containsKey('Clinical Informatics Manager') ? rolesAndUsers.get('Clinical Informatics Manager') : projectToUpdate.Clinical_Informatics_Manager__c);
        projectToUpdate.CTM__c = (rolesAndUsers.containsKey('Clinical Team Manager') ? rolesAndUsers.get('Clinical Team Manager') : projectToUpdate.CTM__c);
        projectToUpdate.Study_Start_Up_Manager__c = (rolesAndUsers.containsKey('Start-Up Lead') ? rolesAndUsers.get('Start-Up Lead') : projectToUpdate.Study_Start_Up_Manager__c);
        
        update projectToUpdate; 
    }
    
    public static void updateLookupIDsForPAWSProjectFlowsSwimlanes (String projectName, List <String> FlowIDs) {
    
        List <RM_PAWSCVO> lstAss = RM_PAWSService.getActiveAssignmentByProject(projectName);
        List <STSWR1__Flow_Swimlane__c> swimlanesToUpdate = [SELECT Name, STSWR1__Assign_To__c, STSWR1__Assign_Type__c FROM STSWR1__Flow_Swimlane__c WHERE STSWR1__Flow__c IN :FlowIDs];
        
        Map<String, String> swimlanesAndUsers = new Map<String, String>();
        for (STSWR1__Flow_Swimlane__c s : swimlanesToUpdate) {
            for (RM_PAWSCVO a : lstAss) {
                if (s.Name == a.Project_Role) {
                    swimlanesAndUsers.put(s.Name, a.UserID);
                }
            }
        }

        for (STSWR1__Flow_Swimlane__c s : swimlanesToUpdate) {
            
            if (swimlanesAndUsers.get(s.Name) == null) continue;
            
            s.STSWR1__Assign_Type__c = 'User';
            s.STSWR1__Assign_To__c = swimlanesAndUsers.get(s.Name);
        }
        
        update swimlanesToUpdate;    
    }
	
	public static void associatePAWSProjectSiteWith(WFM_Site_Detail__c site)
	{
		associatePAWSProjectSiteWith(new List<WFM_Site_Detail__c>{site});
	}
	
	public static void associatePAWSProjectSiteWith(List<WFM_Site_Detail__c> sites)
	{
		List<WFM_Site_Detail__c> wfmSites = [select Project_Protocol__r.Project_ID__c, Project_Protocol__c, Country__c from WFM_Site_Detail__c where Id in :sites and Id not in (select WFM_Site__c from PAWS_Project_Flow_Site__c where WFM_Site__c in :sites)];
		List<ID> projectIDs = new List<ID>();
		List<ID> protocolIDs = new List<ID>();
		List<String> countryNames = new List<String>();
		for (WFM_Site_Detail__c wfmSite : wfmSites)
		{
			projectIDs.add(wfmSite.Project_Protocol__r.Project_ID__c);
			protocolIDs.add(wfmSite.Project_Protocol__c);
			countryNames.add(wfmSite.Country__c);
		}
		
		List<PAWS_Project_Flow_Site__c> pawsSites = new List<PAWS_Project_Flow_Site__c>();

		for (PAWS_Project_Flow_Site__c pawsSite : [select WFM_Site__c, PAWS_Country__r.Name, PAWS_Country__r.PAWS_Project__r.Project_ID__c, PAWS_Country__r.PAWS_Project__r.Protocol_ID__c from PAWS_Project_Flow_Site__c
				where PAWS_Country__r.PAWS_Project__r.Project_ID__c in :projectIDs
				and PAWS_Country__r.PAWS_Project__r.Protocol_ID__c in :protocolIDs
				and PAWS_Country__r.Name in :countryNames
				and WFM_Site__c = null
				order by Planned_Start_Date__c])
		{
			Integer i = 0;
			for (Integer n = wfmSites.size(); i < n; i++)
			{

				if (wfmSites.get(i).Project_Protocol__r.Project_ID__c != pawsSite.PAWS_Country__r.PAWS_Project__r.Project_ID__c
						|| wfmSites.get(i).Project_Protocol__c != pawsSite.PAWS_Country__r.PAWS_Project__r.Protocol_ID__c
						|| wfmSites.get(i).Country__c != pawsSite.PAWS_Country__r.Name)
				{
					continue;
				}
				
				pawsSite.WFM_Site__c = wfmSites.remove(i).Id;
				pawsSites.add(pawsSite);
				
				break;
			}
		}
		
		//if (wfmSites.size() > 0) throw new PAWS_APIException('ERROR: Unable to match ' + wfmSites.size() + ' site(s) against PAWS Sites!');
		
		update pawsSites;
	}
}