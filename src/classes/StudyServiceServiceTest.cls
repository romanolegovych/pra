/** Implements the test for the Service Layer of the object StudyService__c
 * @author	Dimitrios Sgourdos
 * @version	27-Feb-2014
 */
@isTest
private class StudyServiceServiceTest {
	
	/** Test the function getStudyServiceById.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	28-Jan-2014
	 */
	static testMethod void getStudyServiceByIdTest() {
		StudyService__c result = StudyServiceService.getStudyServiceById('');
	}
	
	
	/** Test the function createSelectOptionsForCalculations.
	 * @author	Dimitrios Sgourdos
	 * @version	25-Feb-2014
	 */
	static testMethod void createSelectOptionsForCalculationsTest() {
		String errorMessage = 'Error in creating select options list for Calculation Helper';
		
		List<SelectOption> results = new List<SelectOption>();
		results = StudyServiceService.createSelectOptionsForCalculations('Please Select...');
		
		system.assertEquals(results.size(), 4, errorMessage);
		
		system.assertEquals('', results[0].getValue(), errorMessage);
		system.assertEquals('Please Select...', results[0].getLabel(), errorMessage);
		
		system.assertEquals(StudyServiceDataAccessor.TIME_PERIOD_BETWEEN_MILESTONES, results[1].getValue(), errorMessage);
		system.assertEquals(StudyServiceDataAccessor.TIME_PERIOD_BETWEEN_MILESTONES, results[1].getLabel(), errorMessage);
		
		system.assertEquals(StudyServiceDataAccessor.ASSESSMENTS_FOR_ALL_FC_IN_STUDY_RULE, results[2].getValue(), errorMessage);
		system.assertEquals(StudyServiceDataAccessor.ASSESSMENTS_FOR_ALL_FC_IN_STUDY_RULE, results[2].getLabel(), errorMessage);
		
		system.assertEquals(StudyServiceDataAccessor.PREDEFINED_CATEGORIES_ASSESSMENTS_IN_STUDY_RULE, results[3].getValue(), errorMessage);
		system.assertEquals(StudyServiceDataAccessor.PREDEFINED_CATEGORIES_ASSESSMENTS_IN_STUDY_RULE, results[3].getLabel(), errorMessage);
	}
	
	
	/** Test the function calculateAssessmentForAllFlowchartsInStudy.
	 * @author	Dimitrios Sgourdos
	 * @version	29-Jan-2014
	 */
	static testMethod void calculateAssessmentForAllFlowchartsInStudyTest() {
		 // create test data
		list<sobject> initList = new list<sobject>();
		initList.add(new ClinicalDesign__c(name='1')); // 0
		initList.add( BDT_BuildTestData.buildClientProject('PRA1312-001','BDT Project First Experiments') ); // 1
		initList.add(new ServiceCategory__c(name='test',code__c='001')); // 2
		insert initList;
		
		list<sobject> aList = new list<sobject>();
		aList.add(new arm__c(ClinicalDesign__c = initList[0].id, Arm_Number__c = 1, Description__c = 'Arm 1')); //0
		aList.add(new arm__c(ClinicalDesign__c = initList[0].id, Arm_Number__c = 2, Description__c = 'Arm 2')); //1
		aList.add(new Epoch__c(ClinicalDesign__c = initList[0].id, Epoch_Number__c = 1, Description__c = 'Epoch 1')); //2
		aList.add(new Epoch__c(ClinicalDesign__c = initList[0].id, Epoch_Number__c = 2, Description__c = 'Epoch 2')); //3
		aList.add(new flowcharts__c(ClinicalDesign__c = initList[0].id, Flowchart_Number__c = 1, Description__c = 'Flowchart 1')); //4
		aList.add(new flowcharts__c(ClinicalDesign__c = initList[0].id, Flowchart_Number__c = 2, Description__c = 'Flowchart 2')); //5
		aList.add(new Study__c(Project__c = initList[1].Id)); // 6
		aList.add(new Service__c(ServiceCategory__c = initList[2].id, Name = 'a', SequenceNumber__c = 1, IsDesignObject__c = true)); // 7
		insert aList;
		
		list<FlowchartAssignment__c> fList = new list<FlowchartAssignment__c>();
		fList.add(new FlowchartAssignment__c(
			Arm__c 			= (Id)(aList[0].get('ID')),
			Epoch__c 		= (Id)(aList[2].get('ID')),
			Flowchart__c 	= (Id)(aList[4].get('ID'))
		));
		fList.add(new FlowchartAssignment__c(
			Arm__c 			= (Id)(aList[0].get('ID')),
			Epoch__c 		= (Id)(aList[3].get('ID')),
			Flowchart__c 	= (Id)(aList[5].get('ID'))
		));
		insert fList;

		ProjectService__c ps = new ProjectService__c(Client_Project__c = initList[1].id, Service__c = aList[7].id);
		insert ps;
		
		List<FlowchartServiceTotal__c> sourceList = new List<FlowchartServiceTotal__c>();
		sourceList.add(new FlowchartServiceTotal__c(flowchartassignment__c=fList[0].Id,
													TotalUnits__c=10,
													ProjectService__c = ps.Id,
													Study__c = aList[6].Id) );
		sourceList.add(new FlowchartServiceTotal__c(flowchartassignment__c=fList[1].Id,
													TotalUnits__c=20,
													ProjectService__c = ps.Id,
													Study__c = aList[6].Id) );
		insert sourceList;

		// Check the function
		String errorMessage = 'Error in calculating Assessments for all flowcharts in the given study';
		
		Decimal result = StudyServiceService.calculateAssessmentForAllFlowchartsInStudy(aList[6].Id);
		system.assertEquals(2, result, errorMessage);
		
		sourceList[1].TotalUnits__c = 0;
		update sourceList[1];
		result = StudyServiceService.calculateAssessmentForAllFlowchartsInStudy(aList[6].Id);
		system.assertEquals(1, result, errorMessage);
	}
	
	
	/** Test the function calculateAssessmentForPredefCategoriesInStudy.
	 * @author	Dimitrios Sgourdos
	 * @version	31-Jan-2014
	 */
	static testMethod void calculateAssessmentForPredefCategoriesInStudyTest() {
		 // create test data
		list<sobject> initList = new list<sobject>();
		initList.add(new ClinicalDesign__c(name='1')); // 0
		initList.add( BDT_BuildTestData.buildClientProject('PRA1312-001','BDT Project First Experiments') ); // 1
		insert initList;
		
		List<ServiceCategory__c> srvCatList = new List<ServiceCategory__c>();
		srvCatList.add(new ServiceCategory__c(name='test 1',code__c='001'));
		srvCatList.add(new ServiceCategory__c(name='test 2',code__c='002'));
		insert srvCatList;
		
		list<sobject> aList = new list<sobject>();
		aList.add(new arm__c(ClinicalDesign__c = initList[0].id, Arm_Number__c = 1, Description__c = 'Arm 1')); //0
		aList.add(new arm__c(ClinicalDesign__c = initList[0].id, Arm_Number__c = 2, Description__c = 'Arm 2')); //1
		aList.add(new Epoch__c(ClinicalDesign__c = initList[0].id, Epoch_Number__c = 1, Description__c = 'Epoch 1')); //2
		aList.add(new Epoch__c(ClinicalDesign__c = initList[0].id, Epoch_Number__c = 2, Description__c = 'Epoch 2')); //3
		aList.add(new flowcharts__c(ClinicalDesign__c = initList[0].id, Flowchart_Number__c = 1, Description__c = 'Flowchart 1')); //4
		aList.add(new flowcharts__c(ClinicalDesign__c = initList[0].id, Flowchart_Number__c = 2, Description__c = 'Flowchart 2')); //5
		aList.add(new Study__c(Project__c = initList[1].Id)); // 6
		aList.add(new Service__c(ServiceCategory__c = srvCatList[0].id, Name = 'a', SequenceNumber__c = 1, IsDesignObject__c = true)); // 7
		aList.add(new Service__c(ServiceCategory__c = srvCatList[1].id, Name = 'b', SequenceNumber__c = 2, IsDesignObject__c = true)); // 8
		insert aList;
		
		list<FlowchartAssignment__c> fList = new list<FlowchartAssignment__c>();
		fList.add(new FlowchartAssignment__c(
			Arm__c 			= (Id)(aList[0].get('ID')),
			Epoch__c 		= (Id)(aList[2].get('ID')),
			Flowchart__c 	= (Id)(aList[4].get('ID'))
		));
		fList.add(new FlowchartAssignment__c(
			Arm__c 			= (Id)(aList[0].get('ID')),
			Epoch__c 		= (Id)(aList[3].get('ID')),
			Flowchart__c 	= (Id)(aList[5].get('ID'))
		));
		insert fList;
		
		List<ProjectService__c> prSrvList = new List<ProjectService__c>();
		prSrvList.add( new ProjectService__c(Client_Project__c = initList[1].id, Service__c = aList[7].id));
		prSrvList.add( new ProjectService__c(Client_Project__c = initList[1].id, Service__c = aList[8].id));
		insert prSrvList;
		
		// Create FlowchartServiceTotal
		String explan_1 = '[{"totalunits":8,"timepoints":10,"populationName":"Diabetic Patients","groupsize":8,"groupname":"A",';
		explan_1 += '"flowchartname":"Fc 1","epochname":"Epoch 1","designname":"Test design","armname":"Arm 1"}]';
		
		String explan_2 = '[{"totalunits":16,"timepoints":5,"populationName":"Diabetic Patients","groupsize":8,';
		explan_2 += '"groupname":"A","flowchartname":"Fc 1","epochname":"Epoch 2","designname":"Test design","armname":"Arm 2"}]';
		
		List<FlowchartServiceTotal__c> sourceList = new List<FlowchartServiceTotal__c>();
		sourceList.add(new FlowchartServiceTotal__c(flowchartassignment__c=fList[0].Id,
													TotalUnits__c=10,
													ProjectService__c = prSrvList[0].Id,
													Study__c = aList[6].Id,
													ExplanationJSON__c = explan_1) );
		sourceList.add(new FlowchartServiceTotal__c(flowchartassignment__c=fList[1].Id,
													TotalUnits__c=20,
													ProjectService__c = prSrvList[1].Id,
													Study__c = aList[6].Id,
													ExplanationJSON__c = explan_2) );
		insert sourceList;
		
		String errorMessage = 'Error in calculating Assessments for predefined categories in the given study';
		
		// Check the function with both service categories selected
		List<String> srvCatIds = new List<String>();
		srvCatIds.add(srvCatList[0].Id);
		srvCatIds.add(srvCatList[1].Id);
		
		Decimal result = StudyServiceService.calculateAssessmentForPredefCategoriesInStudy(aList[6].Id, srvCatIds);
		system.assertEquals(15, result, errorMessage);
		
		// Check the function with only the first service category selected
		srvCatIds.remove(1);
		result = StudyServiceService.calculateAssessmentForPredefCategoriesInStudy(aList[6].Id, srvCatIds);
		system.assertEquals(10, result, errorMessage);
	}
	
	
	/** Test the function createOptionsForEwpressTypeOfMilestoneCalculation.
	 * @author	Dimitrios Sgourdos
	 * @version	25-Feb-2014
	 */
	static testMethod void createOptionsForExpressTypeOfMilestoneCalculationTest() {
		String errorMessage = 'Error in creating select options list for Express Type of time period between milestones';
		
		List<SelectOption> results = new List<SelectOption>();
		results = StudyServiceService.createOptionsForExpressTypeOfMilestoneCalculation('Please Select...');
		
		system.assertEquals(results.size(), 5, errorMessage);
		
		system.assertEquals('', results[0].getValue(), errorMessage);
		system.assertEquals('Please Select...', results[0].getLabel(), errorMessage);
		
		system.assertEquals(StudyServiceDataAccessor.EXPRESS_IN_DAYS, results[1].getValue(), errorMessage);
		system.assertEquals(StudyServiceDataAccessor.EXPRESS_IN_DAYS, results[1].getLabel(), errorMessage);
		
		system.assertEquals(StudyServiceDataAccessor.EXPRESS_IN_WEEKS, results[2].getValue(), errorMessage);
		system.assertEquals(StudyServiceDataAccessor.EXPRESS_IN_WEEKS, results[2].getLabel(), errorMessage);
		
		system.assertEquals(StudyServiceDataAccessor.EXPRESS_IN_MONTHS, results[3].getValue(), errorMessage);
		system.assertEquals(StudyServiceDataAccessor.EXPRESS_IN_MONTHS, results[3].getLabel(), errorMessage);
		
		system.assertEquals(StudyServiceDataAccessor.EXPRESS_IN_YEARS, results[4].getValue(), errorMessage);
		system.assertEquals(StudyServiceDataAccessor.EXPRESS_IN_YEARS, results[4].getLabel(), errorMessage);
	}
	
	
	/** Test the function calculateTimePeriodBetweenMileStones.
	 * @author	Dimitrios Sgourdos
	 * @version	27-Feb-2014
	 */
	static testMethod void calculateTimePeriodBetweenMileStonesTest() {
		// Create data
		Client_Project__c cp = BDT_BuildTestData.buildClientProject('PRA1312-001', 'BDT Project First Experiments');
		insert cp;
		
		Study__c study = new Study__c(Project__c = cp.Id);
		insert study;
		
		List<StudyMileStone__c> source = new List<StudyMileStone__c>();
		source.add( new StudyMileStone__c(Study__c = study.Id,	DueDate__c = Date.newInstance(2014,6,9)) );
		source.add( new StudyMileStone__c(Study__c = study.Id, DueDate__c = Date.newInstance(2014,6,18)) );
		source.add( new StudyMileStone__c(Study__c = study.Id, DueDate__c = Date.newInstance(2014,6,23)) );
		source.add( new StudyMileStone__c(Study__c = study.Id, DueDate__c = Date.newInstance(2015,6,18)) );
		source.add( new StudyMileStone__c(Study__c = study.Id,	DueDate__c = Date.newInstance(2015,6,9)) );
		insert source;
		
		String errorMessage = 'Error in calculating the time period between two milestones';
		
		// Check the function with not valid parameters
		Decimal result = StudyServiceService.calculateTimePeriodBetweenMileStones(NULL, NULL, NULL);
		system.assertEquals(0, result, errorMessage);
		
		// Check the function with one not existent milestone
		result = StudyServiceService.calculateTimePeriodBetweenMileStones(source[0].Id,
																		cp.Id,
																		StudyServiceDataAccessor.EXPRESS_IN_DAYS);
		system.assertEquals(0, result, errorMessage);
		
		// Check the function with result express in days
		result = StudyServiceService.calculateTimePeriodBetweenMileStones(source[0].Id,
																		source[1].Id,
																		StudyServiceDataAccessor.EXPRESS_IN_DAYS);
		system.assertEquals(9, result, errorMessage);
		
		// Check the function with result express in weeks
		result = StudyServiceService.calculateTimePeriodBetweenMileStones(source[0].Id,
																		source[2].Id,
																		StudyServiceDataAccessor.EXPRESS_IN_WEEKS);
		system.assertEquals(2, result, errorMessage);
		
		// Check the function with result express in mpnths
		result = StudyServiceService.calculateTimePeriodBetweenMileStones(source[0].Id,
																		source[3].Id,
																		StudyServiceDataAccessor.EXPRESS_IN_MONTHS);
		system.assertEquals(12.3, result, errorMessage);
		
		// Check the function with result express in years
		result = StudyServiceService.calculateTimePeriodBetweenMileStones(source[0].Id,
																		source[4].Id,
																		StudyServiceDataAccessor.EXPRESS_IN_YEARS);
		system.assertEquals(1, result, errorMessage);
	}
	
	
	/** Test the function deserializeFromStringToCalculationHelper.
	 * @author	Dimitrios Sgourdos
	 * @version	25-Feb-2014
	 */
	static testMethod void deserializeFromStringToCalculationHelperTest() {
		String jsonValue = '{"rule":"test rule","outcome":10,"categoriesIds":"TestId1:TestId2"}';
		String errorMessage = 'Error in deserializing the json';
		
		StudyServiceService.CalculationHelper result = StudyServiceService.deserializeFromStringToCalculationHelper(
																											jsonValue);
		
		system.assertNotEquals(NULL, result, errorMessage);
		system.assertEquals('test rule', result.rule, errorMessage);
		system.assertEquals(10, result.outcome, errorMessage);
		system.assertEquals('TestId1:TestId2', result.categoriesIds, errorMessage);
		system.assertEquals(NULL, result.startMileStone, errorMessage);
		system.assertEquals(NULL, result.endMileStone, errorMessage);
		system.assertEquals(NULL, result.timeExpressIn, errorMessage);
		system.assertEquals(NULL, result.commentMileStone, errorMessage);
	}
	
	
	/** Test the function serializeToString of sub class CalculationHelper.
	 * @author	Dimitrios Sgourdos
	 * @version	26-Feb-2014
	 */
	static testMethod void serializeToStringTest() {
		// Create data
		StudyServiceService.CalculationHelper source = new StudyServiceService.CalculationHelper();
		source.rule = 'test rule';
		source.outcome = 10;
		source.categoriesIds = 'TestId1:TestId2';
		source.startMileStone = 'MileStone1';
		source.endMileStone = 'Milestone2';
		source.timeExpressIn = 'days';
		source.commentMileStone = 'TestComment';
		
		// Check the function
		String errorMessage = 'Error in serializing the class';
		
		String result = source.serializeToString();
		system.assertNotEquals(NULL, result, errorMessage);
	}
	
	
	/** Test the function getOutcomeValue of sub class CalculationHelper.
	 * @author	Dimitrios Sgourdos
	 * @version	27-Feb-2014
	 */
	static testMethod void getOutcomeValueTest() {
		// Create data
		StudyServiceService.CalculationHelper source = new StudyServiceService.CalculationHelper();
		
		String errorMessage = 'Error in formatting the calculated outcome';
		
		// Check the function with NULL outcome
		String result = source.getOutcomeValue();
		system.assertEquals(NULL, result, errorMessage);
		
		// Check the function with a calculated outcome and ASSESSMENTS_FOR_ALL_FC_IN_STUDY_RULE as a rule
		source.rule = StudyServiceDataAccessor.ASSESSMENTS_FOR_ALL_FC_IN_STUDY_RULE;
		source.outcome = 50000.45;
		result = source.getOutcomeValue();
		system.assertEquals('50,000', result, errorMessage);
		
		// Check the function with a calculated outcome and TIME_PERIOD_BETWEEN_MILESTONES as a rule
		source.rule = StudyServiceDataAccessor.TIME_PERIOD_BETWEEN_MILESTONES;
		source.timeExpressIn = StudyServiceDataAccessor.EXPRESS_IN_WEEKS;
		source.outcome = 50000.45;
		result = source.getOutcomeValue();
		system.assertEquals('50,000.4', result, errorMessage);
	}
	
	
	/** Test the function getSelectedCategoriesIds of sub class CalculationHelper.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Jan-2014
	 */
	static testMethod void getSelectedCategoriesIdsTest() {
		// Create data
		StudyServiceService.CalculationHelper source = new StudyServiceService.CalculationHelper();
		
		String errorMessage = 'Error in getting the selected service categories';
		
		// Check the function with categoriesIds equal to Null
		List<String> results = source.getSelectedCategoriesIds();
		system.assertEquals(0, results.size(), errorMessage);
		
		// Check the function with categoriesIds not equal to NULL
		source.categoriesIds = 'TestId1:TestId2';
		results = source.getSelectedCategoriesIds();
		
		system.assertEquals(2, results.size(), errorMessage);
		system.assertEquals('TestId1', results[0], errorMessage);
		system.assertEquals('TestId2', results[1], errorMessage);
	}
	
	
	/** Test the function cleanUnusedData of sub class CalculationHelper.
	 * @author	Dimitrios Sgourdos
	 * @version	27-Feb-2014
	 */
	static testMethod void cleanUnusedDataTest() {
		StudyServiceService.CalculationHelper source = new StudyServiceService.CalculationHelper();
		source.rule = StudyServiceDataAccessor.ASSESSMENTS_FOR_ALL_FC_IN_STUDY_RULE;
		source.categoriesIds  = 'TestId1:TestId2';
		source.startMileStone = 'TestMilestoneId';
		
		source.cleanUnusedData();
		system.assertEquals('', source.categoriesIds, 'Error in cleaning unused data');
		system.assertEquals('', source.startMileStone, 'Error in cleaning unused data');
	}
}