public with sharing class StudyStatusReviewCont {
    public List<PAWS_ProjectDetailCVO> projectDetailsList {get;set;}

    private List<PAWS_ProjectCVO> projectList {get;set;}
    private String sponsorName = 'SM Test Acc1';  // Dima TODO HARDCODE

    public List<PBBMobileSettingWrapper> settingList {get;set;}

    public StudyStatusReviewCont() {
        settingList = PBBMobileSettingServices.getSettingList( Id.valueOf(UserInfo.getUserId() ) );

        projectList = PAWS_API.PBB_getActiveProjects( sponsorName );
        projectDetailsList = new List<PAWS_ProjectDetailCVO>();
        
        for( PAWS_ProjectCVO projectItem :  projectList ){
            PAWS_ProjectDetailCVO projectDetailItem = PAWS_API.PBB_getProjectDetails( projectItem.RecordID );
            projectDetailsList.add( projectDetailItem );
        }
    }

    public String saveSettings(){
        return PBBMobileSettingServices.saveSettings( settingList );
    }
}