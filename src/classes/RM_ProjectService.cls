public with sharing class RM_ProjectService {

    public static WFM_Project__c GetProjectwithProtocolbyPName(string projectName){
    	return RM_ProjectsDataAccessor.GetProjectwithProtocolbyPName(projectName);
    }
   	public static List<WFM_Project__c> GetProjectbyrID(string rID){      
   		return RM_ProjectsDataAccessor.GetProjectbyrID(rID);   
   	}   
   	public static Set<ID> GetProjectSetByEmplRIDYearMonth(string EmplRID, Integer startMonth, Integer endMonth){
   		return  RM_ProjectsDataAccessor.GetProjectSetByEmplRIDYearMonth(EmplRID, startMonth, EndMonth);
   	}
   	public static list<WFM_Project__c> GetProjectListBySetProject(set<ID>projectIDs){
		
        return RM_ProjectsDataAccessor.GetProjectListBySetProject(projectIDs);
	}
	public static list<WFM_Project__c> GetPMProject(string employeeRID, string employeeID){
		return RM_ProjectsDataAccessor.GetPMProject(employeeRID, employeeID);
	}
	public static  list<WFM_Protocol__c> GetProtocolByProjectID(string projectRID){
		return RM_ProjectsDataAccessor.GetProtocolByProjectID(projectRID);
	}
	public static List<WFM_Site_Detail__c> GetSitesInfobyProtocolRID(string protRID){
	 	return RM_ProjectsDataAccessor.GetSitesInfobyProtocolRID(protRID);
	}
	public static List<AggregateResult> GetSitesLocationbyProtocolRID(string protRID){
	  	return RM_ProjectsDataAccessor.GetSitesLocationbyProtocolRID(protRID);
	}
	public static List<AggregateResult> GetActiveSitesLocationbyProtocolRID(string protRID){
	 	return RM_ProjectsDataAccessor.GetActiveSitesLocationbyProtocolRID(protRID);
	}
 }