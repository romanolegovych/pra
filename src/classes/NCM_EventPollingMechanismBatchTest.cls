/**
 * @description	Implements the test for the functions of the NCM_EventPollingMechanismBatch class
 * @author		Dimitrios Sgourdos
 * @version		Created: 01-Oct-2015
 */
@isTest
private class NCM_EventPollingMechanismBatchTest {
	
	/**
	 * @description	Check the NCM_EventPollingMechanismBatch class: start, execute, finish methods.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Oct-2015
	 */
	static testMethod void EventPollingMechanismBatchTest() {
		// Create data
		List<NotificationCatalog__c> catalogList = new List<NotificationCatalog__c>();
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic A',
													ImplementationClassName__c = 'NCM_DummyClass',
													AcknowledgementRequired__c = true,
													ChatterEnabled__c = true,
													TriggeredByBatchJob__c = true,
													SMSallowed__c = true,
													Active__c = true) );
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic B',
													ImplementationClassName__c = 'NCM_DummyClass',
													AcknowledgementRequired__c = false,
													ChatterEnabled__c = false,
													TriggeredByBatchJob__c = true,
													SMSallowed__c = false,
													Active__c = true) );
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic C',
													ImplementationClassName__c = 'InvalidClass',
													AcknowledgementRequired__c = true,
													ChatterEnabled__c = true,
													TriggeredByBatchJob__c = true,
													SMSallowed__c = true,
													Active__c = true) );
		insert catalogList;
		
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		List<User> userList = NCM_Utils.buildTestUserList(2);
		
		List<NotificationRegistration__c> regList = new List<NotificationRegistration__c>();
		// First catalog
		regList.add( new NotificationRegistration__c(	Name = 'A',
														NotificationCatalog__c = catalogList[0].Id,
														UserId__c = userList[0].Id,
														RelatedToId__c = accList[0].Id,
														ReminderPeriod__c = 10,
														NotifyByEmail__c = true,
														NotifyBySMS__c = true,
														NotifyByChatter__c = true) );
		regList.add( new NotificationRegistration__c(	Name = 'B',
														NotificationCatalog__c = catalogList[0].Id,
														UserId__c = userList[0].Id,
														RelatedToId__c = accList[1].Id,
														ReminderPeriod__c = 20,
														NotifyByEmail__c = true,
														NotifyBySMS__c = false,
														NotifyByChatter__c = true) );
		regList.add( new NotificationRegistration__c(	Name = 'C',
														NotificationCatalog__c = catalogList[0].Id,
														UserId__c = userList[1].Id,
														RelatedToId__c = accList[0].Id,
														ReminderPeriod__c = 30,
														NotifyByEmail__c = false,
														NotifyBySMS__c = false,
														NotifyByChatter__c = true) );
		regList.add( new NotificationRegistration__c(	Name = 'D',
														NotificationCatalog__c = catalogList[0].Id,
														UserId__c = userList[1].Id,
														RelatedToId__c = accList[1].Id,
														ReminderPeriod__c = 40,
														NotifyByEmail__c = false,
														NotifyBySMS__c = true,
														NotifyByChatter__c = false) );
		// Second catalog
		regList.add( new NotificationRegistration__c(	Name = 'E',
														NotificationCatalog__c = catalogList[1].Id,
														UserId__c = userList[0].Id,
														RelatedToId__c = accList[0].Id,
														ReminderPeriod__c = 50,
														NotifyByEmail__c = true,
														NotifyBySMS__c = true,
														NotifyByChatter__c = true) );
		regList.add( new NotificationRegistration__c(	Name = 'F',
														NotificationCatalog__c = catalogList[1].Id,
														UserId__c = userList[0].Id,
														RelatedToId__c = accList[1].Id,
														ReminderPeriod__c = 60,
														NotifyByEmail__c = false,
														NotifyBySMS__c = false,
														NotifyByChatter__c = true) );
		regList.add( new NotificationRegistration__c(	Name = 'G',
														NotificationCatalog__c = catalogList[1].Id,
														UserId__c = userList[1].Id,
														RelatedToId__c = accList[0].Id,
														ReminderPeriod__c = 70,
														NotifyByEmail__c = false,
														NotifyBySMS__c = true,
														NotifyByChatter__c = false) );
		regList.add( new NotificationRegistration__c(	Name = 'H',
														NotificationCatalog__c = catalogList[1].Id,
														UserId__c = userList[1].Id,
														RelatedToId__c = accList[1].Id,
														ReminderPeriod__c = 80,
														NotifyByEmail__c = true,
														NotifyBySMS__c = true,
														NotifyByChatter__c = false) );
		// Third catalog
		regList.add( new NotificationRegistration__c(	Name = 'I',
														NotificationCatalog__c = catalogList[2].Id,
														UserId__c = userList[0].Id,
														RelatedToId__c = accList[0].Id,
														ReminderPeriod__c = 90,
														NotifyByEmail__c = false,
														NotifyBySMS__c = true,
														NotifyByChatter__c = true) );
		regList.add( new NotificationRegistration__c(	Name = 'J',
														NotificationCatalog__c = catalogList[2].Id,
														UserId__c = userList[0].Id,
														RelatedToId__c = accList[1].Id,
														ReminderPeriod__c = 100,
														NotifyByEmail__c = false,
														NotifyBySMS__c = true,
														NotifyByChatter__c = false) );
		regList.add( new NotificationRegistration__c(	Name = 'K',
														NotificationCatalog__c = catalogList[2].Id,
														UserId__c = userList[1].Id,
														RelatedToId__c = accList[0].Id,
														ReminderPeriod__c = 110,
														NotifyByEmail__c = true,
														NotifyBySMS__c = false,
														NotifyByChatter__c = true) );
		regList.add( new NotificationRegistration__c(	Name = 'L',
														NotificationCatalog__c = catalogList[2].Id,
														UserId__c = userList[1].Id,
														RelatedToId__c = accList[1].Id,
														ReminderPeriod__c = 120,
														NotifyByEmail__c = true,
														NotifyBySMS__c = true,
														NotifyByChatter__c = false) );
		insert regList;
		
		String errorMessage = 'Error in the Notification Topic Polling Mechanism';
		
		// Check the batchable job
		Test.startTest();
		NCM_EventPollingMechanismBatch pollingMechanismBatch = new NCM_EventPollingMechanismBatch();
		Database.executeBatch(pollingMechanismBatch);
		Test.stopTest();
		
		// Events
		List<NotificationEvent__c> eventResults = 	[SELECT	Name,
															NotificationCatalog__c,
															RelatedToId__c,
															Subject__c,
															EmailBody__c
													FROM	NotificationEvent__c
													LIMIT	5000];
		
		system.assertEquals(4, eventResults.size(), errorMessage);
		
		Map<String, NotificationEvent__c> eventMap = new Map<String, NotificationEvent__c>();
		for(NotificationEvent__c tmpEvent : eventResults) {
			eventMap.put('' + tmpEvent.NotificationCatalog__c + ':' + tmpEvent.relatedToId__c, tmpEvent);
		}
		
		String key = catalogList[0].Id + ':' + accList[0].Id;
		system.assertEquals(true, eventMap.containsKey(key), errorMessage);
		system.assertNotEquals(NCM_API_DataTypes.EVENT_UNDER_BATCH_PROCEDURE, eventMap.get(key).Name, errorMessage);
		
		key = catalogList[0].Id + ':' + accList[1].Id;
		system.assertEquals(true, eventMap.containsKey(key), errorMessage);
		system.assertNotEquals(NCM_API_DataTypes.EVENT_UNDER_BATCH_PROCEDURE, eventMap.get(key).Name, errorMessage);
		
		key = catalogList[1].Id + ':' + accList[0].Id;
		system.assertEquals(true, eventMap.containsKey(key), errorMessage);
		system.assertNotEquals(NCM_API_DataTypes.EVENT_UNDER_BATCH_PROCEDURE, eventMap.get(key).Name, errorMessage);
		
		key = catalogList[1].Id + ':' + accList[1].Id;
		system.assertEquals(true, eventMap.containsKey(key), errorMessage);
		system.assertNotEquals(NCM_API_DataTypes.EVENT_UNDER_BATCH_PROCEDURE, eventMap.get(key).Name, errorMessage);
		
		// Notifications
		List<Notification__c> notificationResults =	[SELECT	NotificationEvent__c,
															NotificationRegistration__c,
															Status__c
													FROM	Notification__c
													ORDER BY NotificationRegistration__r.Name
													LIMIT 50000];
		
		system.assertEquals(8, notificationResults.size(), errorMessage);
		
		system.assertEquals(regList[0].Id, notificationResults[0].NotificationRegistration__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, notificationResults[0].Status__c, errorMessage);
		key = catalogList[0].Id + ':' + accList[0].Id;
		system.assertEquals(eventMap.get(key).Id, notificationResults[0].NotificationEvent__c, errorMessage);
		
		system.assertEquals(regList[1].Id, notificationResults[1].NotificationRegistration__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, notificationResults[1].Status__c, errorMessage);
		key = catalogList[0].Id + ':' + accList[1].Id;
		system.assertEquals(eventMap.get(key).Id, notificationResults[1].NotificationEvent__c, errorMessage);
		
		system.assertEquals(regList[2].Id, notificationResults[2].NotificationRegistration__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, notificationResults[2].Status__c, errorMessage);
		key = catalogList[0].Id + ':' + accList[0].Id;
		system.assertEquals(eventMap.get(key).Id, notificationResults[2].NotificationEvent__c, errorMessage);
		
		system.assertEquals(regList[3].Id, notificationResults[3].NotificationRegistration__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, notificationResults[3].Status__c, errorMessage);
		key = catalogList[0].Id + ':' + accList[1].Id;
		system.assertEquals(eventMap.get(key).Id, notificationResults[3].NotificationEvent__c, errorMessage);
		
		system.assertEquals(regList[4].Id, notificationResults[4].NotificationRegistration__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ACKNOWLEDGED, notificationResults[4].Status__c, errorMessage);
		key = catalogList[1].Id + ':' + accList[0].Id;
		system.assertEquals(eventMap.get(key).Id, notificationResults[4].NotificationEvent__c, errorMessage);
		
		system.assertEquals(regList[5].Id, notificationResults[5].NotificationRegistration__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ACKNOWLEDGED, notificationResults[5].Status__c, errorMessage);
		key = catalogList[1].Id + ':' + accList[1].Id;
		system.assertEquals(eventMap.get(key).Id, notificationResults[5].NotificationEvent__c, errorMessage);
		
		system.assertEquals(regList[6].Id, notificationResults[6].NotificationRegistration__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ACKNOWLEDGED, notificationResults[6].Status__c, errorMessage);
		key = catalogList[1].Id + ':' + accList[0].Id;
		system.assertEquals(eventMap.get(key).Id, notificationResults[6].NotificationEvent__c, errorMessage);
		
		system.assertEquals(regList[7].Id, notificationResults[7].NotificationRegistration__c, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ACKNOWLEDGED, notificationResults[7].Status__c, errorMessage);
		key = catalogList[1].Id + ':' + accList[1].Id;
		system.assertEquals(eventMap.get(key).Id, notificationResults[7].NotificationEvent__c, errorMessage);
		
		// Notification History
		List<NotificationHistory__c> historyResults =	[SELECT	Notification__c,
																EmailSent__c,
																SMSsent__c
														FROM	NotificationHistory__c
														ORDER BY Notification__r.NotificationRegistration__r.Name
														LIMIT	50000];
							
		system.assertEquals(5, historyResults.size(), errorMessage);
		
		system.assertEquals(notificationResults[0].Id, historyResults[0].Notification__c, errorMessage);
		system.assertEquals(true, historyResults[0].EmailSent__c, errorMessage);
		system.assertEquals(true, historyResults[0].SMSsent__c, errorMessage);
		
		system.assertEquals(notificationResults[1].Id, historyResults[1].Notification__c, errorMessage);
		system.assertEquals(true, historyResults[1].EmailSent__c, errorMessage);
		system.assertEquals(false, historyResults[1].SMSsent__c, errorMessage);
		
		system.assertEquals(notificationResults[3].Id, historyResults[2].Notification__c, errorMessage);
		system.assertEquals(false, historyResults[2].EmailSent__c, errorMessage);
		system.assertEquals(true, historyResults[2].SMSsent__c, errorMessage);
		
		system.assertEquals(notificationResults[4].Id, historyResults[3].Notification__c, errorMessage);
		system.assertEquals(true, historyResults[3].EmailSent__c, errorMessage);
		system.assertEquals(false, historyResults[3].SMSsent__c, errorMessage);
		
		system.assertEquals(notificationResults[7].Id, historyResults[4].Notification__c, errorMessage);
		system.assertEquals(true, historyResults[4].EmailSent__c, errorMessage);
		system.assertEquals(false, historyResults[4].SMSsent__c, errorMessage); // as SMSallowed = false
		
		// Chatter posts
		List<FeedItem> chatterPostsResults = [SELECT Id, parentId, Body FROM FeedItem LIMIT 10];
		
		system.assertEquals(7, chatterPostsResults.size(), errorMessage);	// 1 generic (second account, as event for
																			// first account was manually created)
																			// + 5 for subscribed users
		
		Map<String, List<FeedItem> > feedItemMap = new Map<String, List<FeedItem>>();
		for(FeedItem tmpItem : chatterPostsResults) {
			List<FeedItem> tmpList = new List<FeedItem>();
			if( feedItemMap.containsKey(tmpItem.parentId) ) {
				tmpList = feedItemMap.remove(tmpItem.parentId);
			}
			tmpList.add(tmpItem);
			feedItemMap.put(tmpItem.parentId, tmpList);
		}
		
		List<FeedItem> feedResults = feedItemMap.remove(accList[0].Id);
		system.assertEquals(1, feedResults.size(), errorMessage);
		system.assertEquals('Test email subject', feedResults[0].Body, errorMessage);
		
		feedResults = feedItemMap.remove(accList[1].Id);
		system.assertEquals(1, feedResults.size(), errorMessage);
		system.assertEquals('Test email subject', feedResults[0].Body, errorMessage);
		
		feedResults = feedItemMap.remove(userList[0].Id);
		system.assertEquals(4, feedResults.size(), errorMessage);
		for(FeedItem tmpItem : feedResults) {
			system.assertEquals('Test email subject', tmpItem.Body, errorMessage);
		}
		
		feedResults = feedItemMap.remove(userList[1].Id);
		system.assertEquals(1, feedResults.size(), errorMessage);
		system.assertEquals('Test email subject', feedResults[0].Body, errorMessage);
	}
}