public class IPA_QuickLinksComponentController{
    //member vars
    public IPA_Page_Widget__c pageWidgetObj {get; set;}
    public List<IPA_Links__c> getLstQuickLinks() {
        system.debug('component controller page widget obj = ' + pageWidgetObj);
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        if(pageWidgetObj != null){
            return ipa_bo.returnQuickLinks(pageWidgetObj);
        }else{
            return ipa_bo.returnQuickLinks();
        }
    }
    //constructor
    /*public void IPA_QuickLinksComponentController(){
    }*/
}