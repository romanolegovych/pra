@isTest
private class BDT_DesignCopyControllerTest {

	public static List<Client_Project__c> projectList {get;set;}
	public static List<Study__c> stydiesList {get;set;}	

	public static List<ClinicalDesign__c> designList {get;set;}
	public static List<Flowcharts__c> flowchartList  {get;set;}
	public static List<TimePoint__c> timepointList  {get;set;}
	public static List<Service__c> serviceList	{get;set;}
	public static List<ProjectService__c> projectService {get;set;}
	public static List<ProjectServiceTimePoint__c> projectServiceTimePointList {get;set;}

	static void init(){
		projectList = new List<Client_Project__c>();
		projectList.add(BDT_TestDataUtils.buildProject());
		projectList.add(BDT_TestDataUtils.buildProject());
		insert projectList;
		
		stydiesList = BDT_TestDataUtils.buildStudies(projectList[0]);
		stydiesList.addAll(BDT_TestDataUtils.buildStudies(projectList[1]));
		insert stydiesList;
		
		designList = BDT_TestDataUtils.buildClinicDesign(projectList[0]); 
		designList.addAll(BDT_TestDataUtils.buildClinicDesign(projectList[1]));
		insert designList;		
				 
		flowchartList = BDT_TestDataUtils.buildFlowchart(designList[0].id);
		
		insert flowchartList;
		
		timepointList = BDT_TestDataUtils.buildTimePoints(flowchartList[0].id, 10);
		insert timepointList;
				
		List<ServiceCategory__c> myServiceCat1 = BDT_TestDataUtils.buildServiceCategory(3,null);
		insert myServiceCat1 ;
	    
	    List<ServiceCategory__c> myServiceCat2 = BDT_TestDataUtils.buildServiceCategory(3,myServiceCat1);
	    insert myServiceCat2 ;
	    
	    serviceList = BDT_TestDataUtils.buildService(myServiceCat2, 1, 'myTimePointTest',true);
	    insert serviceList;
	     
	    projectService = BDT_TestDataUtils.buildProjectService(serviceList, projectList[0].id);
	    insert projectService;
	    
	    projectServiceTimePointList = BDT_TestDataUtils.buildProjectServiceTimePoint(projectService, timepointList);
	    insert projectServiceTimePointList; 
	    
	    system.assertEquals(99,  [select count() from ProjectServiceTimePoint__c where flowchart__c =: flowchartList[0].id]);
	    
	}
	
    static testMethod void copyTest() {
    	init();
    	
    	PageReference p;
    	
		BDT_Utils.setPreference('SelectedProject', projectList[1].id);
		
		BDT_DesignCopyController DCC = new BDT_DesignCopyController();
		
		DCC.ProjectSearchTerm = 'Something we will never find.';
		DCC.ButtonSearchProject();
		
		DCC.ProjectSearchTerm = projectList[0].Project_Description__c;
		DCC.ButtonSearchProject();
		
		DCC.SelectedProjectID = projectList[0].id;
		DCC.SelectedDesignID = designList[0].id;
		
		DCC.CreateBasicDesignParts();
		
		DCC.NewDesignName = 'My New Copy';
		p = DCC.ButtonCopyDesign();
		DCC.NewDesignName = 'My New Copy Duplicate';
		p = DCC.ButtonCopyDesign();
		
		p = DCC.ButtonCancel();
    }
}