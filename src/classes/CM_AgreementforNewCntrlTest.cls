/** 
     * @description Test class CM_AgreementforNewCntrlTest 
     * @author      LavakusaReddy Poluru
     * @date        Created: 30-Jan-2015
     */


@isTest
public with sharing class CM_AgreementforNewCntrlTest {
      private static final List<RecordType> Agreement =  [SELECT Name from RecordType WHERE Name = 'Contract Agreement' LIMIT 1];
      private static final List<RecordType> Template =  [SELECT Name from RecordType WHERE Name = 'Contract Template' LIMIT 1];
      
        /** 
     * @description Test method testnewAgreement
     * @author      LavakusaReddy Poluru
     * @date        Created: 30-Jan-2015
     */
     
    static testMethod void testnewAgreement() {
    
        WFM_Client__c testClient = new WFM_Client__c();
        WFM_Contract__c testContract = new WFM_Contract__c();       
        WFM_Project__c testProject = new WFM_Project__c();
        Protocol_Country__c testProtocolCountry = new Protocol_Country__c();
        WFM_Protocol__c testProtocol = new WFM_Protocol__c();
        WFM_Site_Detail__c testSite = new WFM_Site_Detail__c();
        Country__c testcountry = new Country__c();
        
        list<Apttus__APTS_Agreement__c> testlist = new list<Apttus__APTS_Agreement__c>();
        Apttus__APTS_Agreement__c testagreement = new Apttus__APTS_Agreement__c();
        Apttus__APTS_Agreement__c testtemplate_protocol = new Apttus__APTS_Agreement__c();
        Apttus__APTS_Agreement__c testtemplate_protocol_country = new Apttus__APTS_Agreement__c();
        list<Apttus__APTS_Agreement__c> testlist1 = new list<Apttus__APTS_Agreement__c>();
        
        //Test Client, Contract, Project, Protocol, and Protocol Country and agreement records
        testClient.Name = 'Ranbaxytest';                
        testClient.Client_Unique_Key__c = 'RBX001';
        insert testClient;
        
        testContract.Name = 'testcontract';
        testContract.Contract_Unique_Key__c = 'crnttest1';
        testContract.Client_ID__c = testClient.Id;
        insert testContract;
        
        testProject.Name = 'LiverCancer';
        testProject.Project_Unique_Key__c = 'LVC1';
        insert testProject;
        
        testProtocol.Name = 'testprotocol';
        testProtocol.Protocal_Unique_Key__c = 'test1';
        testProtocol.Project_ID__c = testProject.Id;
        insert testProtocol;
        
        testSite.Project_Protocol__c = testProtocol.Id;
        testSite.Site_ID__c = '010';
        testSite.Name = 'testsite1';
        insert testSite;
        
        testcountry.Name = 'ALBANIA';
        insert testcountry;
        
        testProtocolCountry.Name = 'Netherlands';
        testProtocolCountry.Client_Protocol__c = testProtocol.id;
        testProtocolCountry.Region__c = testcountry.id;
        insert testProtocolCountry;
                
        
        testtemplate_protocol.name='Test Template';
        testtemplate_protocol.PRA_Status__c = 'Budget Template Initiated';   
        testtemplate_protocol.Contract_Type__c='Master CTA'; 
        testtemplate_protocol.Protocol__c = testProtocol.id;
        testtemplate_protocol.Protocol_Country__c = testProtocolCountry.id;
        testtemplate_protocol.Site__c = testSite.id;
        testtemplate_protocol.First_Draft_Sent_Date__c = Date.newInstance(2015, 01, 31);
        testtemplate_protocol.Planned_Final_Exec_Date__c = Date.newInstance(2015, 12, 31);
        insert testtemplate_protocol;
        testlist.add(testtemplate_protocol);
        
       //New Controller Instances for CM_AgreementforNewCntrl
        PageReference pageRef1 = New PageReference(System.Page.CM_AgreementForNew.getUrl());
        Pageref1.getParameters().put('RecordType',Template[0].Name);
        System.test.setCurrentPage(pageRef1);
        ApexPages.StandardsetController Cont2 = new ApexPages.StandardsetController(testlist);
        CM_AgreementforNewCntrl testController2 = new CM_AgreementforNewCntrl(Cont2);
        testController2.saveAgreement();
        testController2.getStatusList();
        
        PageReference pageRef = New PageReference(System.Page.CM_AgreementForNew.getUrl());
        pageref.getParameters().put('RecordType',Template[0].Name);
        System.test.setCurrentPage(pageRef);
        ApexPages.StandardsetController Cont1 = new ApexPages.StandardsetController(testlist);
        CM_AgreementforNewCntrl testController1 = new CM_AgreementforNewCntrl(Cont1);
        testController1.agreement.name='Test Template';
        testController1.agreement.PRA_Status__c = 'Budget Template Initiated';   
        testController1.agreement.Contract_Type__c='Master CTA'; 
        testController1.agreement.Protocol__c = testProtocol.id;
        testController1.agreement.Protocol_Country__c = testProtocolCountry.id;
        testController1.agreement.Site__c = testSite.id;
        testController1.agreement.First_Draft_Sent_Date__c = Date.newInstance(2015, 01, 31);
        testController1.agreement.Planned_Final_Exec_Date__c = Date.newInstance(2015, 12, 31); 
        testController1.saveAgreement();
         
        
        PageReference pageRef2 = New PageReference(System.Page.CM_AgreementForNew.getUrl());
        pageref2.getParameters().put('Id' , testSite.id);
        pageref2.getParameters().put('RecordType',Agreement[0].Name);
        System.test.setCurrentPage(pageRef2);
        ApexPages.StandardsetController Cont3 = new ApexPages.StandardsetController(testlist);
        CM_AgreementforNewCntrl testController3 = new CM_AgreementforNewCntrl(Cont3);
        testController3.saveAgreement();
        testController3.getStatusList();
                
        PageReference pageRef3 = New PageReference(System.Page.CM_AgreementForNew.getUrl());
        pageref3.getParameters().put('Id' , testProtocol.id);
        pageref3.getParameters().put('RecordType',Agreement[0].Name);
        System.test.setCurrentPage(pageRef3);
        ApexPages.StandardsetController Cont4 = new ApexPages.StandardsetController(testlist);
        CM_AgreementforNewCntrl testController4 = new CM_AgreementforNewCntrl(Cont4);
        testController4.saveAgreement();
        
        PageReference pageRef4 = New PageReference(System.Page.CM_AgreementForNew.getUrl());
        pageref4.getParameters().put('Id' , testProtocolCountry.id);
        pageref4.getParameters().put('RecordType',Agreement[0].Name);
        System.test.setCurrentPage(pageRef4);
        ApexPages.StandardsetController Cont5 = new ApexPages.StandardsetController(testlist);
        CM_AgreementforNewCntrl testController5 = new CM_AgreementforNewCntrl(Cont5);
        testController5.saveAgreement();
        
        
        PageReference pageRef5 = New PageReference(System.Page.CM_AgreementForNew.getUrl());
        pageref5.getParameters().put('RecordType',Template[0].Name);
        System.test.setCurrentPage(pageRef5);
        ApexPages.StandardsetController Cont6 = new ApexPages.StandardsetController(testlist);
        CM_AgreementforNewCntrl testController6 = new CM_AgreementforNewCntrl(Cont6);
        testController6.agreement.name='Test Template';
        testController6.agreement.PRA_Status__c = 'Budget Template Initiated';   
        testController6.agreement.Contract_Type__c='Master CTA'; 
        testController6.agreement.Protocol_Country__c = testProtocolCountry.id;
        testController6.agreement.First_Draft_Sent_Date__c = Date.newInstance(2015, 01, 31);
        testController6.agreement.Planned_Final_Exec_Date__c = Date.newInstance(2015, 12, 31); 
        testController6.saveAgreement();
        
        
        PageReference pageRef6 = New PageReference(System.Page.CM_AgreementForNew.getUrl());
        pageref6.getParameters().put('RecordType',Agreement[0].Name);
        System.test.setCurrentPage(pageRef6);
        ApexPages.StandardsetController Cont7 = new ApexPages.StandardsetController(testlist);
        CM_AgreementforNewCntrl testController7 = new CM_AgreementforNewCntrl(Cont7);
        testController7.agreement.name='Test Template';
        testController7.agreement.PRA_Status__c = 'Template Available';   
        testController7.agreement.Contract_Type__c='Master CTA'; 
        testController7.agreement.Site__c = testSite.id;
        testController7.agreement.First_Draft_Sent_Date__c = Date.newInstance(2015, 01, 31);
        testController7.agreement.Planned_Final_Exec_Date__c = Date.newInstance(2015, 12, 31); 
        testController7.saveAgreement();
                
    }
}