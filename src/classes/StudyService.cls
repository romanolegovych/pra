/** Implements the Service Layer of the object Study__c
 * @author	Dimitrios Sgourdos
 * @version	18-Nov-2013
 */
public with sharing class StudyService {
	
	/** Retrieve the List of Studies that are selected by the current user.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 30-Oct-2013
	 * @return	The list of Studies
	 */
	public static List<Study__c> getStudiesByUserPreference() {
		return  StudyDataAccessor.getByUserPreference();
	}
	
	
	/** Retrieve the List of Studies that belong to the given project and they are not deleted.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 05-Nov-2013
	 * @return	The list of Studies
	 */
	public static List<Study__c> getStudiesByProjectId(String projectId) {
		return  StudyDataAccessor.getByProjectId(projectId);
	}
	
	
	/** Retrieve the List of Studies that are described in the string listOfStudiesIds and they have
	 *	LaboratoryAnalysis with Analysis__c selected assigned to them.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 05-Nov-2013
	 * @param	listOfStudiesIds	A string that holds the desired studies separated with the delimiter ':'
	 * @return	The list of Studies
	 */
	public static List<Study__c> getStudiesByUsedInAN (String listOfStudiesIds) {
		return StudyDataAccessor.getByUsedInAN(listOfStudiesIds);
	}
	
	
	/** Retrieve the List of Studies that are described in the string listOfStudiesIds and they have
	 *	LaboratoryAnalysis with MethodDevelopment__c or MethodValidation__c selected assigned to them.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 18-Nov-2013
	 * @param	listOfStudiesIds	A string that holds the desired studies separated with the delimiter ':'
	 * @return	The list of Studies
	 */
	public static List<Study__c> getStudiesByUsedInMdOrVal (String listOfStudiesIds) {
		return StudyDataAccessor.getByUsedInMdOrVal(listOfStudiesIds);
	}
	
	
	/** Retrieve the List of Studies that are described in the string listOfStudiesIds and they have
	 *	LaboratoryAnalysis with MethodDevelopment__c selected assigned to them.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 18-Nov-2013
	 * @param	listOfStudiesIds	A string that holds the desired studies separated with the delimiter ':'
	 * @return	The list of Studies
	 */
	public static List<Study__c> getStudiesByUsedInMd (String listOfStudiesIds) {
		return StudyDataAccessor.getByUsedInMd(listOfStudiesIds);
	}
	
	
	/** Retrieve the List of Studies that are described in the string listOfStudiesIds and they have
	 *	LaboratoryAnalysis with MethodValidation__c selected assigned to them.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 18-Nov-2013
	 * @param	listOfStudiesIds	A string that holds the desired studies separated with the delimiter ':'
	 * @return	The list of Studies
	 */
	public static List<Study__c> getStudiesByUsedInVal (String listOfStudiesIds) {
		return StudyDataAccessor.getByUsedInVal(listOfStudiesIds);
	}
}