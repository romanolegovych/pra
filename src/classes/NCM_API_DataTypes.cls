/**
 * @description	This class holds the wrapper data types that are used in the 'Notification & Communication Module' API
 * @author		Dimitrios Sgourdos
 * @date		Created: 01-Sep-2015, Edited: 09-Oct-2015
 */
global with sharing class NCM_API_DataTypes {
	
// *********************************************************************************************************************
//				Definition of constants
// *********************************************************************************************************************
	// Constants for Notification Category
	global static final String	PROTOCOL_NOTIFICATION	= 'Protocol';
	global static final String	COUNTRY_NOTIFICATION	= 'Country';
	global static final String	SITE_NOTIFICATION		= 'Site';
	
	// Constants for Notification Type
	global static final String	ALERT_TYPE	= 'Alert';
	global static final String	INFO_TYPE	= 'Info';
	
	// Constants for Notification Priority
	global static final String	LOW_PRIORITY	= 'Low';
	global static final String	NORMAL_PRIORITY	= 'Normal';
	global static final String	HIGH_PRIORITY	= 'High';
	
	//Constants for Reminders
	global static final Decimal	DEFAULT_MAX_NUMBER_OF_REMINDERS = 2;
	
	// Constants for Pending Acknowledgement
	global static final String	PENDING_ACKNOWLEDGEMENT	= 'Pending Acknowledgement';
	global static final String	ACKNOWLEDGED			= 'Acknowledged';
	
	// Constants for Notification Events
	global static final String	EVENT_UNDER_BATCH_PROCEDURE	= 'Event is under batch procedure';
	
	// Constants for Batch jobs Names
	global static final String BATCH_JOB_FOR_REMINDER = 'NCM module: Reminder for Pending Acknowledgement Notifications';
	global static final String BATCH_JOB_FOR_POLLING_TOPICS = 'NCM module: Polling mechanism for notification topics';
	
	// Constants for setting the from address in the emails
	global static final String ORG_WIDE_EMAIL_ADDRESS = 'noreply_sfdc@prahs.com';
	
	
// *********************************************************************************************************************
//				Definition of wrapper classes
// *********************************************************************************************************************
	
	/**
	 * @description	This sub-class holds the wrapper data that corresponds to the attributes of the 
	 *				'Notification Registration' object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015, Edited: 04-Sep-2015
	 */
	global class NotificationRegistrationWrapper {
		global ID							recordId				{get;set;}
		global ID 							notificationCatalogId	{get;set;}
		global ID 							userId					{get;set;}
		global Decimal						reminderPeriod			{get;set;}
		global Decimal						maxNumberReminders		{get;set;}
		global String						relatedToId				{get;set;}
		global String						relatedToIdName			{get;set;}
		global Boolean						active					{get;set;}
		global Boolean						notifyByEmail			{get;set;}
		global Boolean						notifyBySMS				{get;set;}
		global Boolean						notifyByChatter			{get;set;}
		global NotificationCatalogWrapper	parentTopicData			{get;set;}
		
		// Standard constructor
		global NotificationRegistrationWrapper() {
			
		}
		
		// Constructor from a Notification Registration instance
		global NotificationRegistrationWrapper(NotificationRegistration__c reg, Boolean parentDataFlag) {
			recordId				= reg.Id;
			notificationCatalogId	= reg.NotificationCatalog__c;
			userId					= reg.UserId__c;
			reminderPeriod			= reg.ReminderPeriod__c;
			maxNumberReminders		= reg.MaxNumberReminders__c;
			relatedToId				= reg.RelatedToId__c;
			active					= reg.Active__c;
			notifyByEmail			= reg.NotifyByEmail__c;
			notifyBySMS				= reg.NotifyBySMS__c;
			notifyByChatter			= reg.NotifyByChatter__c;
			if(parentDataFlag) {
				addParentNotificationCatalogData(reg);
			}
		}
		
		// Add the Notification Catalog parent data
		@testVisible private void addParentNotificationCatalogData(NotificationRegistration__c reg) {
			parentTopicData = new NotificationCatalogWrapper(reg);
		}
	}
	
	
	/**
	 * @description	This sub-class holds the wrapper data that corresponds to the attributes of the 
	 *				'Notification Catalog' object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015, Edited: 08-Oct-2015
	 */
	global class NotificationCatalogWrapper {
		global ID		recordId				{get;set;}
		global String 	notificationName		{get;set;}
		global String 	notificationDescription	{get;set;}
		global String	notificationCategory	{get;set;}
		global String	notificationType		{get;set;}
		global String	comments				{get;set;}
		global Decimal	defaultReminderPeriod	{get;set;}
		global Boolean	active					{get;set;}
		global String	priority				{get;set;}
		global Boolean	acknowledgementRequired	{get;set;}
		global Boolean	chatterEnabled			{get;set;}
		global Boolean	SMSallowed				{get;set;}
		global Boolean	triggeredByBatchJob		{get;set;}
		global String	implementationClassName	{get;set;}
		
		// Standard constructor
		global NotificationCatalogWrapper() {
			
		}
		
		// Constructor from a Notification Notification instance
		global NotificationCatalogWrapper(NotificationCatalog__c notCat) {
			recordId				= notCat.Id;
			notificationName		= notCat.NotificationName__c;
			notificationDescription	= notCat.NotificationDescription__c;
			notificationCategory	= notCat.Category__c;
			notificationType		= notCat.Type__c;
			comments				= notCat.Comments__c;
			defaultReminderPeriod	= notCat.DefaultReminderPeriod__c;
			active					= notCat.Active__c;
			priority				= notCat.Priority__c;
			acknowledgementRequired	= notCat.AcknowledgementRequired__c;
			chatterEnabled			= notCat.ChatterEnabled__c;
			SMSallowed				= notCat.SMSallowed__c;
			triggeredByBatchJob		= notCat.TriggeredByBatchJob__c;
			implementationClassName	= notCat.ImplementationClassName__c;
		}
		
		// Constructor from a Notification Registration instance
		global NotificationCatalogWrapper(NotificationRegistration__c reg) {
			recordId				= reg.NotificationCatalog__c;
			notificationName		= reg.NotificationCatalog__r.NotificationName__c;
			notificationDescription	= reg.NotificationCatalog__r.NotificationDescription__c;
			notificationCategory	= reg.NotificationCatalog__r.Category__c;
			notificationType		= reg.NotificationCatalog__r.Type__c;
			comments				= reg.NotificationCatalog__r.Comments__c;
			defaultReminderPeriod	= reg.NotificationCatalog__r.DefaultReminderPeriod__c;
			active					= reg.NotificationCatalog__r.Active__c;
			priority				= reg.NotificationCatalog__r.Priority__c;
			acknowledgementRequired	= reg.NotificationCatalog__r.AcknowledgementRequired__c;
			chatterEnabled			= reg.NotificationCatalog__r.ChatterEnabled__c;
			SMSallowed				= reg.NotificationCatalog__r.SMSallowed__c;
			triggeredByBatchJob		= reg.NotificationCatalog__r.TriggeredByBatchJob__c;
			implementationClassName	= reg.NotificationCatalog__r.ImplementationClassName__c;
		}
		
		// Constructor from a Notification instance
		global NotificationCatalogWrapper(Notification__c notification) {
			recordId				= notification.NotificationRegistration__r.NotificationCatalog__c;
			notificationName		= notification.NotificationRegistration__r.NotificationCatalog__r.NotificationName__c;
			notificationDescription	= notification.NotificationRegistration__r.NotificationCatalog__r.NotificationDescription__c;
			notificationCategory	= notification.NotificationRegistration__r.NotificationCatalog__r.Category__c;
			notificationType		= notification.NotificationRegistration__r.NotificationCatalog__r.Type__c;
			comments				= notification.NotificationRegistration__r.NotificationCatalog__r.Comments__c;
			defaultReminderPeriod	= notification.NotificationRegistration__r.NotificationCatalog__r.DefaultReminderPeriod__c;
			active					= notification.NotificationRegistration__r.NotificationCatalog__r.Active__c;
			priority				= notification.NotificationRegistration__r.NotificationCatalog__r.Priority__c;
			acknowledgementRequired	= notification.NotificationRegistration__r.NotificationCatalog__r.AcknowledgementRequired__c;
			chatterEnabled			= notification.NotificationRegistration__r.NotificationCatalog__r.ChatterEnabled__c;
			SMSallowed				= notification.NotificationRegistration__r.NotificationCatalog__r.SMSallowed__c;
			triggeredByBatchJob		= notification.NotificationRegistration__r.NotificationCatalog__r.TriggeredByBatchJob__c;
			implementationClassName	= notification.NotificationRegistration__r.NotificationCatalog__r.ImplementationClassName__c;
		}
	}
	
	
	/**
	 * @description	This sub-class holds the wrapper data that corresponds to the attributes of the
	 *				'Notification Event' object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Oct-2015
	 */
	global class NotificationEventWrapper {
		global ID		recordId				{get;set;}
		global ID 		notificationCatalogId	{get;set;}
		global String	relatedToId				{get;set;}
		global String	notificationType		{get;set;}
		global String	notificationSubject		{get;set;}
		global String	emailBody				{get;set;}
		global DateTime	recordCreatedDate		{get;set;}
		
		// Standard constructor
		global NotificationEventWrapper() {
			
		}
		
		// Constructor from a Notification Notification instance
		global NotificationEventWrapper(NotificationEvent__c notificationEvent) {
			recordId				= notificationEvent.Id;
			notificationCatalogId	= notificationEvent.NotificationCatalog__c;
			relatedToId				= notificationEvent.RelatedToId__c;
			notificationType		= notificationEvent.Type__c;
			notificationSubject		= notificationEvent.Subject__c;
			emailBody				= notificationEvent.EmailBody__c;
			recordCreatedDate		= notificationEvent.CreatedDate;
		}
		
		// Constructor from a Notification instance
		global NotificationEventWrapper(Notification__c notification) {
			recordId				= notification.NotificationEvent__c;
			notificationCatalogId	= notification.NotificationEvent__r.NotificationCatalog__c;
			relatedToId				= notification.NotificationEvent__r.RelatedToId__c;
			notificationType		= notification.NotificationEvent__r.Type__c;
			notificationSubject		= notification.NotificationEvent__r.Subject__c;
			emailBody				= notification.NotificationEvent__r.EmailBody__c;
			recordCreatedDate		= notification.NotificationEvent__r.CreatedDate;
		}
	}
	
	
	/**
	 * @description	This sub-class holds the wrapper data that corresponds to the attributes of the
	 *				'Notification' object
	 * @author		Kalaiyarasan Karunanithi, Dimitrios Sgourdos
	 * @date		Created: 10-Sep-2015, Edited: 09-Oct-2015
	 */
	global class NotificationWrapper {
		global Id							recordId				 	{get;set;}
		global Id							notificationEvent		 	{get;set;}
		global Id							notificationRegistration 	{get;set;}
		global String 						status					 	{get;set;}
		global DateTime						reminder				 	{get;set;}
		global DateTime						recordCreatedDate			{get;set;}
		global String						relatedToId				 	{get;set;}
		global String						relatedToIdName				{get;set;}
		global NotificationCatalogWrapper	parentTopicData				{get;set;}
		global NotificationEventWrapper		parentEventData				{get;set;}
		
		// Standard constructor
		global NotificationWrapper() {
			
		}
		
		// Constructor from a Notification instance
		global NotificationWrapper(Notification__c notifi, Boolean parentCatalogDataFlag, Boolean parentEventDataFlag) {
			recordId				 = notifi.Id;
			notificationEvent		 = notifi.NotificationEvent__c;
			notificationregistration = notifi.NotificationRegistration__c;
			status					 = notifi.Status__c;
			reminder				 = notifi.Reminder__c;
			recordCreatedDate		 = notifi.CreatedDate;
			relatedToId				 = notifi.NotificationRegistration__r.RelatedToId__c;
			if(parentCatalogDataFlag) {
				addParentNotificationCatalogData(notifi);
			}
			if(parentEventdataFlag) {
				addParentNotificationEventData(notifi);
			}
		}
		
		// Add the Notification Catalog parent data
		@testVisible private void addParentNotificationCatalogData(Notification__c notification) {
			parentTopicData = new NotificationCatalogWrapper(notification);
		}
		
		// Add the Notification Event parent data
		@testVisible private void addParentNotificationEventData(Notification__c notification) {
			parentEventData = new NotificationEventWrapper(notification);
		}
	}
}