public with sharing class PRA_ChangeOrderController {
    
    // Page variables
    public String selectedProject;
    public String selectedProjectLabel  {get;set;}
    public String selectedProjectId     {get;set;}
    public Client_Project__c project    {get;set;}
    public String selectedAction        {get;set;}
    public String selectedTasks         {get;set;}
    public String changeOrderName       {get;set;}  // name for new change order
    public String selectedChangeOrder   {get;set;}
    public String changeOrderId         {get;set;} // id of extisting change order 
    public Boolean bSelectAllTasks      {get;set;}
    // Change order wrapper
    public List<ChangeOrderWrapper> selectedChangeOrdersWrappers {get;set;}
    // Constants
    public final String ST_NONE = 'None'; 
    public static String TEXT_NEW_CHANGE_ORDER              {get {return 'Create a New Change Order';}}
    public static String TEXT_ADD_UNITS                     {get {return 'Add Existing Client Unit to an Existing Change Order';}}
    public static String TEXT_MODIFY_END_DATE               {get {return 'Modify Estimated End Date of Project';}}
    public static String TEXT_MODIFY_CHANGE_ORDER           {get {return 'Display / Modify a Change Order';}}
    public static String TEXT_DELETE_CHANGE_ORDER           {get {return 'Delete Change Order';}}
    public static String CHANGE_ORDER_STATUS_ACTIVE         {get {return 'Active';}}
    public static String CHANGE_ORDER_STATUS_FAILED         {get {return 'Failed';}}
    public static String CHANGE_ORDER_STATUS_EXECUTED       {get {return 'Executed';}}
    public static String CHANGE_ORDER_ITEM_STATUS_NEW       {get {return 'New';}}
    public static String CHANGE_ORDER_ITEM_STATUS_MODIFIED  {get {return 'Modified';}}
    // for user prefs
    private Integer searchCount{get;set;}
    private PRA_Utils.UserPreference userPreference{get;private set;}
    
    public PRA_ChangeOrderController() {
        bSelectAllTasks = false;
        searchCount = -1;
        if(Apexpages.currentPage().getParameters().get('id') != null) {
            setSelectedProject(Apexpages.currentPage().getParameters().get('id'));
            selectedProjectLabel = project.Name;
            selectedAction = TEXT_MODIFY_CHANGE_ORDER;
        }
        selectedProjectId='';
        userPreference = PRA_Utils.retrieveUserPreference();
        if(userPreference != null) {
            selectedProject = userPreference.projectId;
            List<Client_Project__c> clientProjectsList = 
                [select Name, Is_Project_Locked__c, Migrated_Date__c,Total_Contracted_Value__c from Client_Project__c where Id = :selectedProject and Load_Status__c != 'New'];
            if(clientProjectsList.size() == 1) {
                setSelectedProject(userPreference.projectId);
                selectedProjectLabel = clientProjectsList.get(0).Name;
                selectedProjectId=selectedProjectLabel;
            }
        } else {
            selectedProjectLabel = 'Home';
            selectedProjectId='';
            searchCount++;
            
        }
    }
    
    public void doNothingJustSubmitForm() {
        saveUserPreferences();
    }
    
    // set selected project
    public void setSelectedProject(String projectId) {
        searchCount++;
        system.debug('Init project ' + projectId);
        selectedAction = TEXT_MODIFY_CHANGE_ORDER;
        if(projectId != null && projectId != '' && (project == null || (project.id != projectId))) {
            selectedProject = projectId;
            if(userPreference != null) {
                userPreference.projectId = selectedProject;
            } else { 
                userPreference = new PRA_Utils.UserPreference();
                userPreference.projectId = selectedProject;
                userPreference.regionId = new String[]{ST_NONE};
                userPreference.countryId = new String[]{ST_NONE};
                userPreference.clientUnitId = new String[]{ST_NONE};
                userPreference.clientUnitGroupId = new String[]{ST_NONE};
                userPreference.operationalAreaId = new String[]{ST_NONE};
                userPreference.unconfirmedOnly = false;
                userPreference.confirmedOnly = false;
                userPreference.crctId = '';
                userPreference.userId = UserInfo.getUserId();
            }  
            project = [select Client__c,Total_Contracted_Value__c, Global_Project_Analyst__c, Global_Project_Manager_Director__c, Name, Project_Currency__c, 
                Contract_type__c, Migrated_Date__c from Client_Project__c where Id = :projectId];
            initSelectableChangeOrders();
        }
    }  
    
    // get selected project
    public String getSelectedProject() {
        return selectedProject;
    }
    
    public List<Selectoption> getActionList() {
        List<Selectoption> retVal = new List<Selectoption>();
        //retVal.add(new Selectoption (TEXT_SELECT_OPTION, TEXT_SELECT_OPTION));
        retVal.add(new Selectoption (TEXT_NEW_CHANGE_ORDER, TEXT_NEW_CHANGE_ORDER));
        retVal.add(new Selectoption (TEXT_ADD_UNITS, TEXT_ADD_UNITS));
        retVal.add(new Selectoption (TEXT_MODIFY_END_DATE, TEXT_MODIFY_END_DATE));
        retVal.add(new Selectoption (TEXT_MODIFY_CHANGE_ORDER, TEXT_MODIFY_CHANGE_ORDER));
        retVal.add(new Selectoption (TEXT_DELETE_CHANGE_ORDER, TEXT_DELETE_CHANGE_ORDER));
        return retVal;
    }
    // TODO: Resert functionality
    
    public List<TaskWrapper> getTaskListWrapper() {
        List<TaskWrapper> retVal = new List<TaskWrapper>();
        system.debug('Init task list');
        if(project != null) {
            List<Client_Task__c> tasks = new List<Client_Task__c>();
            
            if(selectedAction == TEXT_NEW_CHANGE_ORDER) { 
                    tasks = PRA_DataAccessor.getClientTaskbyProjectOpenClosedStatus(project.Id,PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS);
                    /*tasks = [select Id, Description__c, Client_Unit_Number__c, Project_Region__r.Name, Total_Contract_Units__c, Total_Baseline_Units__c, 
                            Unit_of_Measurement__c, Contract_Value__c, Total_Units__c from Client_Task__c  where Project__c = :project.Id
                            and Description__c != 'Migrated Worked Hours' // task with migrated worked hours should never be displayed to user
                                                                                         // this is not my design. I was forced to do it! This comment is to silence my conscience
                                                                                         // and inform futher developers maintaing code, that reason for ghost tasks was design.
                            order by Description__c, Project_Region__r.Name];
                   */         
            }
            if(selectedAction == TEXT_ADD_UNITS && changeOrderId != '') {
                Set<Id> changeOrderTasks = new Set<Id>();
                for(Change_Order_Item__c liCOItem : [select Id, Change_Order__c, Client_Task__c from Change_Order_Item__c where Change_Order__c = :changeOrderId]) {
                    changeOrderTasks.add(liCOItem.Client_Task__c);
                }
                
                tasks =  PRA_DataAccessor.getClientTaskbyProjectandNotInCOItems(project.Id,PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS,changeOrderTasks);
               /* tasks = [select  Id, Description__c, Client_Unit_Number__c, Project_Region__r.Name, 
                        Total_Units__c, Total_Baseline_Units__c, Unit_of_Measurement__c, Contract_Value__c, Total_Contract_Units__c
                        from Client_Task__c  where Project__c = :project.Id and Id not in :changeOrderTasks
                        and Description__c != 'Migrated Worked Hours'   // task with migrated worked hours should never be displayed to user
                                                                        // this is not my design. I was forced to do it! This comment is to silence my conscience
                                                                        // and inform futher developers maintaing code, that reason for ghost tasks was design.
                        order by Description__c, Project_Region__r.Name];
                        
                        */
            }
            for(Client_Task__c task : tasks) {
                Decimal totalUnits = 0;
                Decimal contractCost = 0;
                Decimal unitCost = 0;
                if(task.Total_Contract_Units__c != null) {
                    totalUnits += task.Total_Contract_Units__c;
                }
                if(task.Contract_Value__c != null) {
                    contractCost += task.Contract_Value__c;
                }
                if (totalUnits != 0 && contractCost != 0) {
                    unitCost = contractCost / totalUnits;
                }
                retVal.add(new TaskWrapper(task.Id, task.Client_Unit_Number__c, task.Description__c, task.Project_Region__r.Name,
                    task.Unit_of_Measurement__c, totalUnits, unitCost, contractCost, bSelectAllTasks));
            }
        }
        return retVal;
    }
    
    public List<Change_Order__c> getProjectChangeOrders() {
        List<Change_Order__c> retVal = new List<Change_Order__c>();
        
        if(project != null) {
            if(selectedAction == TEXT_ADD_UNITS || selectedAction == TEXT_MODIFY_END_DATE) { //return only active and failed CO
                retVal = [select Id, Name, Client_Project__c, Status__c,createddate,lastmodifieddate,ExecutedBy__r.name,Owner.FirstName,Owner.LastName,Executeddate__c from Change_Order__c
                 where Client_Project__c = :project.Id 
                    and Status__c in (:CHANGE_ORDER_STATUS_ACTIVE, :CHANGE_ORDER_STATUS_FAILED)];
            } else {
                retVal = [select Id, Name, Client_Project__c, Status__c,createddate,lastmodifieddate,ExecutedBy__r.name,Owner.FirstName,Owner.LastName,Executeddate__c from Change_Order__c
                 where Client_Project__c = :project.Id];
            }
        }
        return retVal;
    }
    
    public void initSelectableChangeOrders() {
        selectedChangeOrdersWrappers = new List<ChangeOrderWrapper>();
        if(project != null) { //return only active, failed CO
            for(Change_Order__c co :[select Id, Name, Client_Project__c, Status__c,createddate,lastmodifieddate,ExecutedBy__r.name,Owner.FirstName,Owner.LastName,Executeddate__c from Change_Order__c 
                where Client_Project__c = :project.Id and Status__c in (:CHANGE_ORDER_STATUS_ACTIVE, :CHANGE_ORDER_STATUS_FAILED)]) {
                selectedChangeOrdersWrappers.add(new ChangeOrderWrapper(co));
                system.debug('--co=' + co);
            }
        }
    }
    
    public Pagereference createNewChangeOrder() {
        Pagereference retVal;
        
        if(changeOrderName != null && changeOrderName != '') {
            Change_Order__c changeOrder = new Change_Order__c(Name = changeOrderName, Status__c = CHANGE_ORDER_STATUS_ACTIVE, Client_Project__c = project.Id);           
            changeOrder.Total_Contracted_Value__c=project.Total_Contracted_Value__c;
            insert changeOrder;
            changeOrderId = changeOrder.Id;
            addSelectedTasksToChangeOrder();
            
            retVal = Page.PRA_Change_Order_Display_Modify;
            retVal.getParameters().put('id', changeOrderId);
        }
        return retVal;
    }
    
    public Pagereference deleteSelectedChangeOrders() {
        Pagereference retVal;
        
        Set<ID> setDelChangeOrders = new Set<Id>();
        
        for(ChangeOrderWrapper cow : selectedChangeOrdersWrappers) {
            if(cow.isSelected == true) {
                setDelChangeOrders.add(cow.changeOrder.Id);
            }
        }
        if(setDelChangeOrders.size() > 0) {
            delete [select Id from Change_Order__c where Id in :setDelChangeOrders];
            initSelectableChangeOrders();
        }
        
        return retVal;
    }
    
    public Pagereference addChangeOrderItems() {
        Pagereference retVal;
        
        if(changeOrderId != null && changeOrderId != '') {
            addSelectedTasksToChangeOrder();
            
            retVal = Page.PRA_Change_Order_Display_Modify;
            retVal.getParameters().put('id', changeOrderId);
        }
        return retVal;
    }
    
    public Pagereference selectChangeOrder() {
        Pagereference retVal;
        
        if(selectedAction == TEXT_MODIFY_END_DATE) {
            retVal = Page.PRA_Change_Order_EndDate;
            retVal.getParameters().put('id', changeOrderId);
        }
        if(selectedAction == TEXT_MODIFY_CHANGE_ORDER) {
            retVal = Page.PRA_Change_Order_Display_Modify;
            retVal.getParameters().put('id', changeOrderId);
        }
        return retVal;
    }
    
    public Pagereference reset() {
        Pagereference redirect = Page.PRA_Change_Order;
        if(getSelectedProject() != null && getSelectedProject() != '') {
            PRA_Utils.deleteUserPreference();
        }
        redirect.setRedirect(true);
        return redirect;
    }
     
    public void selectAllTasks() {
        bSelectAllTasks = !bSelectAllTasks;
    }
    
    // ---------- Static methods (for remote calls) ---------------------
    // autocomplete
    @RemoteAction
    public static List<Client_Project__c> searchProject(String projectName) {
        System.debug('projectName: ' + projectName );
        List<Client_Project__c> liProj = Database.query('select Id, Name from Client_Project__c where Name LIKE \'%' 
            + String.escapeSingleQuotes(projectName) + '%\' and Load_Status__c != \'New\'');
        //PRA_ChangeOrderController.saveUserPreferences();
        return liProj;
    }
    
    public void saveUserPreferences() { 
        if(searchCount > 0) {
            // Save user prefs
            PRA_Utils.saveUserPreference(userPreference);
        } 
    } 
    
    private void addSelectedTasksToChangeOrder() {
        List<String> liTaskIds = (List<String>)JSON.deserialize(selectedTasks, List<String>.class);
        if(liTaskIds != null && liTaskIds.size() > 0) {
            Map<Id,Change_Order_Item__c> mapChangeOrderItems = new Map<Id, Change_Order_Item__c>();
            List<Client_Task__c> coTasks = [select Id, Description__c, Client_Unit_Number__c, Project_Region__r.Name, Project_Region__c, 
                Total_Worked_Units__c, Total_Worked_Hours__c, Forecast_Curve__c, Combo_Code__c, Start_Date__c, Original_End_Date__c,Revised_End_Date__c, Task_Group__c, 
                Unit_of_Measurement__c, Contract_Value__c, Total_Contract_Units__c, Total_Units__c from Client_Task__c 
                where Id in :liTaskIds ORDER BY Description__c, Project_Region__r.Name];
            for(Client_Task__c task : coTasks) {
                Decimal totalUnits = 0;
                Decimal contractCost = 0;
                Decimal unitCost = 0;
                system.debug('-----------combo code-----------' + task.Combo_Code__c);
                if(task.Total_Contract_Units__c != null) {
                    totalUnits += task.Total_Contract_Units__c;
                }
                if(task.Contract_Value__c != null) {
                    contractCost += task.Contract_Value__c;
                }
                if(totalUnits != 0 && contractCost != 0) {
                    unitCost = contractCost / totalUnits;
                }
                
                Change_Order_Item__c coItem = 
                    new Change_Order_Item__c(Change_Order__c = changeOrderId, Client_Task__c = task.Id, Type__c = CHANGE_ORDER_ITEM_STATUS_MODIFIED);
                
                coItem.Client_Unit_Number__c    = task.Client_Unit_Number__c;
                coItem.Description__c           = task.Description__c;
                coItem.Project_Region__c        = task.Project_Region__c;
                coItem.Unit_of_Measurement__c   = task.Unit_of_Measurement__c;
                coItem.Combo_Code__c            = task.Combo_Code__c;
                coItem.Start_Date__c            = task.Start_Date__c;
                if(task.Revised_End_Date__c!=null)
                    coItem.End_Date__c          = task.Revised_End_Date__c;
                else
                    coItem.End_Date__c          = task.Original_End_Date__c;
                coItem.Forecast_Curve__c        = task.Forecast_Curve__c;
                coItem.Task_Group__c            = task.Task_Group__c;
                coItem.Number_of_Contracted_Units__c = totalUnits;
                coItem.Worked_Units__c          = task.Total_Worked_Units__c;
                coItem.Worked_Hours__c          = task.Total_Worked_Hours__c;
                coItem.Unit_Cost__c             = unitCost;
                coItem.Total_Cost__c            = contractCost;
                coItem.Amendment_CO_Number_of_Units__c = 0;
                        
                mapChangeOrderItems.put(task.id, coItem);
            }
            insert mapChangeOrderItems.values();
                
            // Change Order Line Items
            List<Change_Order_Line_Item__c> liCOLineItems = new List<Change_Order_Line_Item__c>();
            List<Bid_Contract_Detail__c> liBidCOntractsDetails = 
            [select Id, Client_Task__c, Client_Task__r.Additional_Units_With_No_Contract_Value__c,
                Client_Task__r.Contract_Value__c, Client_Task__r.Name, Client_Task__r.Contracted_Number_of_Units__c, 
                Client_Task__r.Status__c, Client_Task__r.Average_Contract_Effort__c, Client_Task__r.Total_Worked_Hours__c, 
                Client_Task__r.Total_Contract_Units__c, Contracted_Hours__c, Contracted_Value__c, 
                BUF_Code__c, BUF_Code__r.Id, BUF_Code__r.Name, BUF_Code__r.PRA_BUF_Code_Id__c, BUF_Code__r.PRA_Region__c
                from Bid_Contract_Detail__c where Client_Task__c in :liTaskIds];
                
                
            for(Bid_Contract_Detail__c bidDetails : liBidCOntractsDetails) {
                Change_Order_Line_Item__c coLineItem = new Change_Order_Line_Item__c();
                
                coLineItem.Change_Order_Item__c         = mapChangeOrderItems.get(bidDetails.Client_Task__c).Id;
                coLineItem.BUF_Code__c                  = bidDetails.BUF_Code__c;
                coLineItem.Current_Contract_Hours__c    = bidDetails.Contracted_Hours__c;
                coLineItem.Current_Contract_Value__c    = bidDetails.Contracted_Value__c;
                if(bidDetails.Contracted_Hours__c != 0 && bidDetails.Client_Task__r.Total_Contract_Units__c != 0) {
                    coLineItem.Current_Hours_Unit__c    = bidDetails.Contracted_Hours__c / bidDetails.Client_Task__r.Total_Contract_Units__c;
                } else {
                    coLineItem.Current_Hours_Unit__c = 0;
                }
                
                if(bidDetails.Contracted_Value__c != 0 && bidDetails.Client_Task__r.Total_Contract_Units__c != 0){
                    coLineItem.Current_Unit_Cost__c = bidDetails.Contracted_Value__c / bidDetails.Client_Task__r.Total_Contract_Units__c;
                } else {
                    coLineItem.Current_Unit_Cost__c = 0;
                }
                coLineItem.Current_Total_Cost__c = bidDetails.Contracted_Value__c;
                coLineItem.Type__c = CHANGE_ORDER_ITEM_STATUS_MODIFIED;
                
                if(bidDetails.Contracted_Hours__c==0 && bidDetails.Contracted_Value__c!=0)
                    coLineItem.Is_Labor_Unit__c=false;
                
                liCOLineItems.add(coLineItem);
            }
            insert liCOLineItems;   
        }
    }
    
    // ---------- wrapers --------
    public class TaskWrapper {
        public Id taskId                {get;set;}
        public String clientUnitNumber  {get;set;}
        public String description       {get;set;}
        public String clientRegion      {get;set;}
        public String unitDriver        {get;set;}
        public Decimal contractedUnits  {get;set;}
        public Decimal unitCost         {get;set;}
        public Decimal totalCost        {get;set;}
        public Boolean selected         {get;set;} 
        
        public TaskWrapper() {
            
        }
        public TaskWrapper(Id clientTaskId, String unitNumber, String unitName, String region, String driver,
            Decimal contractedUn, Decimal cost, Decimal total, Boolean checked) {
            this.taskId             = clientTaskId;
            this.clientUnitNumber   = unitNumber;
            this.description        = unitName;
            this.clientRegion       = region;
            this.unitDriver         = driver;
            this.contractedUnits    = contractedUn;
            this.unitCost           = cost;
            this.totalCost          = total; 
            this.selected           = checked;
        }
    }
    
    public class ChangeOrderWrapper {
        public Change_Order__c  changeOrder {get;set;}
        public Boolean          isSelected  {get;set;}
        
        public ChangeOrderWrapper (Change_Order__c co) {
            this.changeOrder = co;
            this.isSelected = false;
        }
    }
}