public with sharing class BDT_SubcontractorController {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} //Administration Subcontractors
	public list<Subcontractor__c> SubcontractorList{get;set;}
	public list<SubcontractorActivity__c> SubcontractorActivityList{get;set;}
    public String SubcontractorId {get;set;}
    public String SubcontractorName {get;set;}
	public String SubcontractorActivityId {get;set;}
	
	public BDT_SubcontractorController(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Subcontractors');
		SubcontractorId = System.currentPageReference().getParameters().get('SubcontractorId');
		string ScId = System.currentPageReference().getParameters().get('ScId');
		loadSubcontractors();
		if(ScId != null){
			for(Subcontractor__c sc: SubcontractorList){
				if(sc.Id ==ScId){
					SubcontractorName = sc.Name;
				}
			}
			loadSubcontractorActivities(ScId);
			SubcontractorId = ScId;
		}
	   
	}
	
	public void loadSubcontractors(){
		SubcontractorList = [Select Name, Address__c, ZipCode__c, City__c,country__c, country__r.Name 
		                     from Subcontractor__c
		                     where BDTDeleted__c =false];
	}
	
	public void showSubcontractorActivities(){
		SubcontractorId = System.currentPageReference().getParameters().get('SubcontractorId');
		SubcontractorName = System.currentPageReference().getParameters().get('SubcontractorName');
		loadSubcontractorActivities( SubcontractorId );
	}
	
	public void loadSubcontractorActivities( String subcontractorId ){
		
		
		SubcontractorActivityList = [Select Name, QualificationStatus__c, QualificationStartDate__c, QualificationEndDate__c, Subcontractor__c
		                             from SubcontractorActivity__c
		                             where Subcontractor__c = :subcontractorId
		                             and BDTDeleted__c=false];
	}
	
	public PageReference editSubcontractor(){
		SubcontractorId = System.currentPageReference().getParameters().get('SubcontractorId');
		PageReference editSubcontractorPage = new PageReference(System.Page.BDT_NewEditSubcontractor.getUrl());
		editSubcontractorPage.getParameters().put('SubcontractorId',SubcontractorId);
		
		return editSubcontractorPage;
	}
	
	public PageReference editSubcontractorActivity(){
		SubcontractorActivityId = System.currentPageReference().getParameters().get('SubcontractorActivityId');
		SubcontractorId = system.currentPageReference().getParameters().get('SubcontractorId');
		
		PageReference editSubcontractorActivityPage = new PageReference(System.Page.BDT_NewEditSubcontractorActivity.getUrl());
		editSubcontractorActivityPage.getParameters().put('SubcontractorActivityId',SubcontractorActivityId);
		editSubcontractorActivityPage.getParameters().put('SubcontractorId',SubcontractorId);
		return editSubcontractorActivityPage;
	}
	
	public PageReference createSubcontractor(){
		PageReference createSubcontractorPage = new PageReference(System.Page.BDT_NewEditSubcontractor.getUrl());
		
		return createSubcontractorPage;
	}
	
	public PageReference createSubcontractorActivity(){
	    	
		PageReference createSubcontractorActivityPage = new PageReference(System.Page.BDT_NewEditSubcontractorActivity.getUrl());
		createSubcontractorActivityPage.getParameters().put('SubcontractorId',SubcontractorId);
		
		return createSubcontractorActivityPage;
	}
	
}