/**
 * @description	Implements the test for the functions of the NCM_Utils class
 * @author		Dimitrios Sgourdos
 * @version		Created: 04-Sep-2015, Edited: 25-Sep-2015
 */
@isTest
private class NCM_APITest {
	
	/**
	 * @description	The test is only for the code coverage, as the API contains only calls to methods of the
	 *				Service Layers
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	*/
	static testMethod void myUnitTest() {
		// Create dummy data
		NotificationCatalog__c notCat = new NotificationCatalog__c(	NotificationName__c = 'Topic A',
																	ImplementationClassName__c = 'Class Name A',
																	Category__c=NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		insert notCat;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		NotificationRegistration__c registration = new NotificationRegistration__c( UserId__c = testUser.Id,
																					NotificationCatalog__c = notCat.Id,
																					RelatedToId__c = acc.Id,
																					ReminderPeriod__c = 10,
																					Active__c = true);
																					
		NotificationRegistration__c tmpregistration = new NotificationRegistration__c( UserId__c = testUser.Id,
																					NotificationCatalog__c = notCat.Id,
																					RelatedToId__c = acc.Id,
																					ReminderPeriod__c = 10,
																					Active__c = true);
		insert tmpregistration;
																					
		Notification__c notification = new Notification__c(NotificationRegistration__c = tmpregistration.Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today());

		insert notification;
		
		// Call the functions
		//Part 'Build / Search for Notification Registrations'
		NCM_API.buildAllRegistrationsForNotifications(testUser.Id, acc.Id, NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		NCM_API.buildAllRegistrationsForNotifications(testUser.Id, NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		NCM_API.buildActiveRegistrationsForNotifications(testUser.Id, acc.Id, NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		NCM_API.buildActiveRegistrationsForNotifications(testUser.Id, NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		NCM_API.buildNonSubscribedRegistrationsForNotifications(testUser.Id,
																acc.Id,
																NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		NCM_API.buildNonSubscribedRegistrationsForNotifications(testUser.Id, NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		NCM_API.getNumberOfActiveRegistrationsPerRelatedObjectByUser(testUSer.Id);
		
		// Part 'Register for Notification'
		NCM_API.registerForNotifications(new List<NCM_API_DataTypes.NotificationRegistrationWrapper>());
		
		// Part 'Disable Notification Registration'
		NCM_API.disableRegistrationsByWrapperData(new List<NCM_API_DataTypes.NotificationRegistrationWrapper>());
		NCM_API.disableRegistrationsByIds (new Set<ID>{registration.ID});
		NCM_API.disableRegistrationsByUserAndRelatedToId(testUser.Id, acc.Id);
		NCM_API.disableRegistrationsByUserAndNotificationTopic(testUser.Id, notCat.Id);
		NCM_API.disableRegistrationsByUser(testUser.Id);
		NCM_API.disableRegistrationsByRelatedToId(acc.Id);
		
		// Part 'Insert, update, disable Notification Catalog entries'
		NCM_API.insertNotificationCatalogEntries(new List<NCM_API_DataTypes.NotificationCatalogWrapper>());
		NCM_API.updateNotificationCatalogEntries(new List<NCM_API_DataTypes.NotificationCatalogWrapper>());
		NCM_API.disableNotificationCatalogEntriesByName(new Set<String>());
		NCM_API.disableNotificationCatalogEntries(new List<NCM_API_DataTypes.NotificationCatalogWrapper>());
		
		// Part 'Retrieve and acknowledge notifications'
		NCM_API.getPendingNotificationsByUser(testUser.Id);
		NCM_API.getPendingNotificationsByUserAndRelatedObject(testUser.Id, acc.Id);
		NCM_API.getNumberOfPendingNotificationsPerRelatedObjectByUser(testUser.Id);
		NCM_API.acknowledgePendingNotifications(new List<NCM_API_DataTypes.NotificationWrapper>());
		
		// Part 'Create notification events'
		NCM_API.activateNotificationEventById(notCat.Id, acc.Id);
		NCM_API.activateNotificationEventByName('Topic A', acc.Id);
		
		// Part 'Transformations between DataTypes and objects'
		NCM_API.getNotificationRegistrationFromWrapperData(	new NCM_API_DataTypes.NotificationRegistrationWrapper(),
															registration);
		NCM_API.getNotificationCatalogFromWrapperData(new NCM_API_DataTypes.NotificationCatalogWrapper(), notCat);
		NCM_API.getNotificationFromWrapperData(new NCM_API_DataTypes.NotificationWrapper(), notification);
	}
}