public with sharing class CM_AgreementforNewCntrl {   
    
    
    public ID PID{ get; set; }
    public String ObjName { get; set; }
    public String RecordTypeID{ get; set; }
    public String RecordType{ get; set; }
    
    public Apttus__APTS_Agreement__c agreement{ get; set; }    
    public WFM_Protocol__c protocol { get; set; }
    public Protocol_Country__c protocolcntry{ get; set; }
    public WFM_Site_Detail__c site { get; set; }
    public static String statuschangedate {get;set;}
    public Map<string,string> statusMap {get;set;}
    
    public CM_AgreementforNewCntrl(ApexPages.StandardSetController controller) {
        agreement = new Apttus__APTS_Agreement__c();
        statuschangedate = '';
        PID=(ApexPages.CurrentPage().getParameters().get('id'));
        RecordType = (ApexPages.CurrentPage().getParameters().get('RecordType'));
        statusMapData();
        if(PID!=null)
            ObjName = PID.getSObjectType().getDescribe().getName();
            system.debug('--------ObjName ------'+ObjName );
        
        if(ObjName == 'WFM_Protocol__c'){
            RecordType = 'Contract Template';
            RecordTypeID = [SELECT Id from RecordType WHERE Name =:RecordType ].id;  
            agreement.RecordTypeId = RecordTypeID ;  
            protocol = [select id,name,Project_ID__r.Project_Unique_Key__c,Project_ID__r.Client_ID__c,Protocol_Title__c 
                        from WFM_Protocol__c where id=:PID];  
            agreement.Protocol__c  = protocol.id;
            agreement.Name = protocol.Project_ID__r.Client_ID__c+ '_' + protocol.Project_ID__r.Project_Unique_Key__c+ '_'+ agreement.Contract_Type__c  ;  
            agreement.Project_id__c =  protocol.Project_ID__r.Project_Unique_Key__c;
            agreement.Protocol_ID__c = protocol.Name;    
            agreement.Protocol_Title__c = protocol.Protocol_Title__c ;                 
        }
        
        if(ObjName =='Protocol_Country__c'){
            RecordType = 'Contract Template';
            RecordTypeID = [SELECT Id from RecordType WHERE Name =:RecordType ].id;
            protocolcntry = [select id,name,Client_Protocol__r.Project_ID__r.Project_Unique_Key__c,Client_Protocol__r.Protocol_Title__c,Client_Protocol__r.Project_ID__r.Client_ID__c,Client_Protocol__r.Name
                                from Protocol_Country__c where id=:PID]; 
            agreement.RecordTypeId = RecordTypeID ;
            agreement.Protocol_Country__c  = protocolcntry.id;
            agreement.Name = protocolcntry.Client_Protocol__r.Project_ID__r.Client_ID__c+ '_' + protocolcntry.Client_Protocol__r.Project_ID__r.Project_Unique_Key__c+ '_'+ agreement.Contract_Type__c  ;  
            agreement.Project_id__c =  protocolcntry.Client_Protocol__r.Project_ID__r.Project_Unique_Key__c; 
            agreement.Protocol_ID__c = protocolcntry.Client_Protocol__r.Name;     
            agreement.Protocol_Title__c = protocolcntry.Client_Protocol__r.Protocol_Title__c ;   
        }
        
        if(ObjName =='WFM_Site_Detail__c'){
            
            RecordType = 'Contract Agreement';
            RecordTypeID = [SELECT Id from RecordType WHERE Name =:RecordType ].id;
            agreement.RecordTypeId = RecordTypeID ;
            site = [select id,name,Project_Protocol__r.Project_ID__r.Project_Unique_Key__c,Project_Protocol__r.Project_ID__r.Client_ID__c,Project_Protocol__r.Name,Project_Protocol__r.Protocol_Title__c
                     from WFM_Site_Detail__c where id=:PID]; 
            agreement.site__c=site.id;
            agreement.Name = site.Project_Protocol__r.Project_ID__r.Client_ID__c+ '_' + site.Project_Protocol__r.Project_ID__r.Project_Unique_Key__c + '_'+ agreement.Contract_Type__c  ;  
            agreement.Project_id__c =  site.Project_Protocol__r.Project_ID__r.Project_Unique_Key__c;   
            agreement.Protocol_ID__c = site.Project_Protocol__r.Name;
            agreement.Protocol_Title__c = site.Project_Protocol__r.Protocol_Title__c ;     
        } 
        system.debug('--------agreement ------'+agreement+RecordType);
    }   
    
    //Method to save agreement   
    public PageReference  saveAgreement(){   
        try{
             if(agreement.Protocol__c ==null && agreement.Protocol_Country__c==null && agreement.site__c==null ){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Required fields missing'));
                    return null;
             }else{
                   
                   if((ApexPages.CurrentPage().getParameters().get('id'))==null){
                        getAgreement();
                        upsert agreement;
                        system.debug('--------agreement 11------'+agreement);            
                        PageReference pageRef=new PageReference('/'+agreement.id);
                        pageRef.setRedirect(true);
                        return pageRef; 
                   }else{
                        getAgreementbyPID();
                        upsert agreement;
                        system.debug('--------agreement 11------'+agreement);            
                        PageReference pageRef=new PageReference('/'+agreement.id);
                        pageRef.setRedirect(true);
                        return pageRef; 
                   }
            
            }
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            return null;
        }
        return null;
    }   
    
    //Method to insert status with their field api names in statusMap
    public void statusMapData(){
        statusMap = new Map<string,string>();
        statusMap.put('Template Sent to Site','Template_Sent_Date__c');
        statusMap.put('Negotiation Complete','Neg_Complete_Date__c');
        statusMap.put('Partially Executed','Partially_Exec_Date__c');
        
    }
    
    public void getAgreement(){  
        if( agreement.Protocol__c !=null ) {
            ObjName = (agreement.Protocol__c).getSObjectType().getDescribe().getName();
            PID = agreement.Protocol__c;
            RecordTypeID = [SELECT Id from RecordType WHERE Name =:RecordType ].id;  
            agreement.RecordTypeId = RecordTypeID ;  
            protocol = [select id,name,Project_ID__r.Project_Unique_Key__c,Project_ID__r.Client_ID__c,Protocol_Title__c 
                        from WFM_Protocol__c where id=:PID ];  
            agreement.Protocol__c  = protocol.id;
            agreement.Name = protocol.Project_ID__r.Client_ID__c+ '_' + protocol.Project_ID__r.Project_Unique_Key__c+ '_'+ agreement.Contract_Type__c  ;  
            agreement.Project_id__c =  protocol.Project_ID__r.Project_Unique_Key__c;      
            agreement.Protocol_ID__c = protocol.Name;    
            agreement.Protocol_Title__c = protocol.Protocol_Title__c ;  
        }
        else
        if(agreement.Protocol_Country__c!=null){
            PID = agreement.Protocol_Country__c;
            ObjName = (agreement.Protocol_Country__c).getSObjectType().getDescribe().getName();
            RecordTypeID = [SELECT Id from RecordType WHERE Name =:RecordType ].id;
            protocolcntry = [select id,name,Client_Protocol__r.Name,Client_Protocol__r.Project_ID__r.Project_Unique_Key__c,Client_Protocol__r.Project_ID__r.Client_ID__c,Client_Protocol__r.Protocol_Title__c 
                                from Protocol_Country__c where id=:PID]; 
            agreement.RecordTypeId = RecordTypeID ;
            agreement.Protocol_Country__c  = protocolcntry.id;
            agreement.Name = protocolcntry.Client_Protocol__r.Project_ID__r.Client_ID__c+ '_' + protocolcntry.Client_Protocol__r.Project_ID__r.Project_Unique_Key__c+ '_'+ agreement.Contract_Type__c  ;  
            agreement.Project_id__c =  protocolcntry.Client_Protocol__r.Project_ID__r.Project_Unique_Key__c; 
            agreement.Protocol_ID__c = protocolcntry.Client_Protocol__r.Name; 
            agreement.Protocol_Title__c = protocolcntry.Client_Protocol__r.Protocol_Title__c ;       
        } else
        if(agreement.site__c!=null){
            PID = agreement.site__c;
            ObjName = (agreement.site__c).getSObjectType().getDescribe().getName(); 
            RecordTypeID = [SELECT Id from RecordType WHERE Name =:RecordType ].id;
            agreement.RecordTypeId = RecordTypeID ;
            site = [select id,name,Project_Protocol__r.Project_ID__r.Project_Unique_Key__c,Project_Protocol__r.Project_ID__r.Client_ID__c,Project_Protocol__r.Protocol_Title__c,Project_Protocol__r.Name
                     from WFM_Site_Detail__c where id=:PID]; 
            agreement.site__c=site.id;
            agreement.Name = site.Project_Protocol__r.Project_ID__r.Client_ID__c+ '_' + site.Project_Protocol__r.Project_ID__r.Project_Unique_Key__c + '_' + agreement.Contract_Type__c  ;  
            agreement.Project_id__c =  site.Project_Protocol__r.Project_ID__r.Project_Unique_Key__c; 
            agreement.Protocol_ID__c = site.Project_Protocol__r.Name;
            agreement.Protocol_Title__c = site.Project_Protocol__r.Protocol_Title__c ; 
                       
           	if(statusMap.get(agreement.PRA_Status__c )!=null){
                if(statuschangedate!='')
                    agreement.put(statusMap.get(agreement.PRA_Status__c ),dateFormat(statuschangedate));
                else 
                    agreement.put(statusMap.get(agreement.PRA_Status__c ),system.today());
            }
        }
        system.debug('--------agreement------'+agreement);
            
    }
    
    public void getAgreementbyPID(){         
        
            if(ObjName =='WFM_Protocol__c'){
                agreement.Name = protocol.Project_ID__r.Client_ID__c+ '_' + protocol.Project_ID__r.Project_Unique_Key__c+ '_'+ agreement.Contract_Type__c  ;  
                agreement.Project_ID__c=protocol.Project_ID__r.Project_Unique_Key__c;   
                agreement.Protocol__c = protocol.id; 
                agreement.Protocol_ID__c = protocol.Name;  
                agreement.Protocol_Title__c = protocol.Protocol_Title__c ;  
            }
            if(ObjName =='Protocol_Country__c'){
                agreement.Name = protocolcntry.Client_Protocol__r.Project_ID__r.Client_ID__c+ '_' + protocolcntry.Client_Protocol__r.Project_ID__r.Project_Unique_Key__c+ '_'+ agreement.Contract_Type__c  ;  
                agreement.Project_ID__c = protocolcntry.Client_Protocol__r.Project_ID__r.Project_Unique_Key__c;   
                agreement.Protocol_Country__c  = protocolcntry.id;
                agreement.Protocol_ID__c = protocolcntry.Client_Protocol__r.Name; 
                agreement.Protocol_Title__c = protocolcntry.Client_Protocol__r.Protocol_Title__c ;   
            }
            if(ObjName =='WFM_Site_Detail__c'){
                agreement.Name = site.Project_Protocol__r.Project_ID__r.Client_ID__c+ '_' + site.Project_Protocol__r.Project_ID__r.Project_Unique_Key__c+ '_' + agreement.Contract_Type__c  ;  
                agreement.Project_ID__c=site.Project_Protocol__r.Project_ID__r.Project_Unique_Key__c;
                agreement.site__c = site.id;
                agreement.Protocol_ID__c = site.Project_Protocol__r.Name; 
                agreement.Protocol_Title__c = site.Project_Protocol__r.Protocol_Title__c ; 
            }           
            system.debug('--------agreement------'+agreement);
            
        
    }
    
     public list<selectoption> getStatusList(){
         list<selectoption> options = new list<selectoption>();
        
         if(RecordType == 'Contract Agreement'){           
             options.add(new SelectOption('Template Available', 'Template Available')); 
             options.add(new SelectOption('Template Sent to Site','Template Sent to Site'));
             options.add(new SelectOption('At Site-Review','At Site-Review')); 
             options.add(new SelectOption('At PRA-CA','At PRA-CA')); 
             options.add(new SelectOption('At PRA-Signature','At PRA-Signature'));
             options.add(new SelectOption('At Sponsor-Review','At Sponsor-Review')); 
             options.add(new SelectOption('At Agency or Review Board','At Agency or Review Board'));
             options.add(new SelectOption('Negotiation Complete','Negotiation Complete'));
             options.add(new SelectOption('At Site-Signature', 'At Site-Signature'));
             options.add(new SelectOption('At Sponsor-Signature', 'At Sponsor-Signature'));
             options.add(new SelectOption('Partially Executed','Partially Executed'));
             options.add(new SelectOption('Fully Executed','Fully Executed'));
             options.add(new SelectOption('At PRA-Clinical','At PRA-Clinical'));
             options.add(new SelectOption('At PRA-PM/PZ','At PRA-PM/PZ'));
             options.add(new SelectOption('At Translator','At Translator'));
             options.add(new SelectOption('At PRA-Legal Counsel','At PRA-Legal Counsel'));
             options.add(new SelectOption('Escalated','Escalated'));
             options.add(new SelectOption('On Hold','On Hold'));
             options.add(new SelectOption('Dropped','Dropped'));
             options.add(new SelectOption('Sent to Payments','Sent to Payments'));
         }
         if(RecordType == 'Contract Template'){ 
             options.add(new SelectOption('Final Budget Received','Final Budget Received'));             
             options.add(new SelectOption('Template Drafted','Template Drafted'));
             options.add(new SelectOption('Sent for Translation','Sent for Translation'));
             options.add(new SelectOption('Translation Complete','Translation Complete'));
             options.add(new SelectOption('At Sponsor - Template Review','At Sponsor - Template Review'));
             options.add(new SelectOption('At PRA - Sponsor Feedback','At PRA - Sponsor Feedback'));
             options.add(new SelectOption('At PRA - Template Final','At PRA - Template Final'));
             options.add(new SelectOption('Budget Template Initiated','Budget Template Initiated'));
         }          
         return options;
    }  
    
   //Method to convert string to Date format 
    public Date dateFormat(String statusdate){
                 String[] dateOnly = statusdate.split(' ');
                String[] changeDate = dateOnly[0].split('/');
                Integer myIntDate = integer.valueOf(changeDate[1]);
                Integer myIntMonth = integer.valueOf(changeDate[0]);
                Integer myIntYear = integer.valueOf(changeDate[2]);
                return Date.newInstance(myIntYear, myIntMonth, myIntDate);
          } 
    
}