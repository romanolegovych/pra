public with sharing class AA_BufCodeService{
     
    public static List<Business_Unit__c> getBusinessUnitCodes()
    {
        return AA_BUFCodeDataAccessor.getBuCodes();
    }
    
    public static List<Function_Code__c> getFunctionCodes()
    {
        return AA_BUFCodeDataAccessor.getFCodes();
    }
    
    public static BUF_Code__c getBufCodeDetails(String val){
        return AA_BUFCodeDataAccessor.getBufCodeDetails(val);
    }
    
    public static List<BUF_Code__c> getBufCodeName(String val){
        return AA_BUFCodeDataAccessor.getBufCodeName(val);    
    }
    
    public static void insertBufCode(BUF_Code__c bufCode){
        AA_BUFCodeDataAccessor.insertBufCode(bufCode);
    }
}