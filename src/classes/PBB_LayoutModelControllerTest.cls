@isTest
private class PBB_LayoutModelControllerTest {
	private static PBB_LayoutModelController cont;

	@isTest
	static void setComponentControllerTest() {
		cont = new PBB_LayoutModelController();
		cont.setComponentController(new PBB_ComponentControllerBase());
		cont.getMyComponentController();
	}

	@isTest
	static void instanceServiceModelControllerServiceModelsTest() {
		cont = new PBB_LayoutModelController();
		cont.instanceModelName = 'Service_Models';
		Test.startTest();
    		cont.instanceServiceModelController();
    	Test.stopTest();
    	System.assertNotEquals(cont.serviceModelController, null);
	}

	@isTest
	static void instanceServiceModelControllerSRMModelsTest() {
		cont = new PBB_LayoutModelController();
		cont.instanceModelName = 'SRM_Models';
		Test.startTest();
    		cont.instanceServiceModelController();
    	Test.stopTest();
    	System.assertNotEquals(cont.sRMModelController, null);
	}

	@isTest
	static void instanceServiceModelControllerRegionModelsTest() {
		cont = new PBB_LayoutModelController();
		cont.instanceModelName = 'Region_Models';
		Test.startTest();
    		cont.instanceServiceModelController();
    	Test.stopTest();
    	System.assertNotEquals(cont.regionModelsController, null);
	}

	@isTest
	static void instanceServiceModelControllerWRModelsTest() {
		cont = new PBB_LayoutModelController();
		cont.instanceModelName = 'WR_Models';
		Test.startTest();
    		cont.instanceServiceModelController();
    	Test.stopTest();
    	System.assertNotEquals(cont.wRModelsController, null);
	}

	@isTest
	static void instanceServiceModelControllerBillRateCardModelsTest() {
		cont = new PBB_LayoutModelController();
		cont.instanceModelName = 'Bill_Rate_Card_Models';
		Test.startTest();
    		cont.instanceServiceModelController();
    	Test.stopTest();
    	System.assertNotEquals(cont.billRateCardModelsController, null);
	}

	@isTest
	static void instanceServiceModelControllerBillFormulaModelsTest() {
		cont = new PBB_LayoutModelController();
		cont.instanceModelName = 'Bill_Formula_Models';
		Test.startTest();
    		cont.instanceServiceModelController();
    	Test.stopTest();
    	System.assertNotEquals(cont.billFormulaModelsController, null);
	}
}