/**
@author Niharika Reddy
@date 2015
@description this is controller for Bill Rate page
*/
public class AA_BillRateInflationController {
    Public String Id {get;set;}
    Public String scheduledYear{get;set;}
    public String country     {get;set;}
    public String jobPosition{get;set;}
    Public List<BillRateInflationWrapper> result {get;set;}
    
    Public String InflationRate{get;set;}
    public Decimal applyInflationStartYear{ get; set; }
    
    public String modelId{ get; set; }
    Public Boolean inflation_applied =False;
    
    public Bill_Rate_Card_Model__c billModelRecord {get;set;}
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this is the constructor for billrate 
    */
    public AA_BillRateInflationController () {
        country     = System.currentPagereference().getParameters().get('country');
        jobPosition = System.currentPagereference().getParameters().get('jobPosition');
        scheduledYear = System.currentPagereference().getParameters().get('scheduledYear');
        modelId = System.currentPagereference().getParameters().get('modelId');
        billModelRecord = [select Name,Status__c,Year__c,Bill_Rate_Model_Unique_Id__c from Bill_Rate_Card_Model__c where id =: modelId];
        getresult();
    }
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this the method for holding bill rate records
    */
    Public List<BillRateInflationWrapper> getresult(){
        result =  new List<BillRateInflationWrapper>();
        if(!inflation_applied){
            List<Bill_Rate__c> billRatesList = AA_BillRateDataAccessor.getRateDetails(modelId, jobPosition, country, scheduledYear, 'true');
            
            system.debug('------billRatesList ------'+billRatesList);
         
            for(Bill_Rate__c r : billRatesList){
                system.debug('------r------'+r);
                InflationRate = r.Inflation_Rate__c; 
                //applyInflationStartYear = r.Apply_Inflation_Start_Year__c;
                BillRateInflationWrapper cost = new BillRateInflationWrapper(r,false);
                result.add(cost);
            }
              
        }
        return result;
    }
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this the method for applying inflation
    */
    Public Pagereference apply(){
        inflation_applied = true;
        List<BillRateInflationWrapper> tempresult = result; 
        if(((String.isEmpty(inflationRate.trim())) || inflationRate == null) || (Decimal.valueOf(inflationRate) < 0)){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Inflation rate cannot be negative or blank.If you dont want to apply inflation please enter 0.'));
            return null;
        }
        /* If inflation year entered */
        if(applyInflationStartYear != 0){
            if(applyInflationStartYear >= Decimal.valueOf(scheduledYear) && applyInflationStartYear <= Decimal.valueOf(scheduledYear) + 15){
                for(Integer i = 1 ; i < tempresult.size(); i ++){         
                    if(Decimal.valueOf(tempresult[i-1].costrate.Year_Rate_Applies_To__c) >= applyInflationStartYear){
                        Decimal unScaled = (Decimal) tempresult[i-1].costrate.Bill_Rate__c + (tempresult[i-1].costrate.Bill_Rate__c)*(Decimal.valueOf(InflationRate))/100;
                        tempresult[i].costrate.Bill_Rate__c = unScaled.setScale(2);
                    }
                }
            }else{
                ApexPages.Message errormsg = new ApexPages.Message(ApexPages.severity.ERROR,' Invalid Inflation Start Year ');
                ApexPages.addMessage(errormsg);
            }
        }else{
             for(Integer i = 1 ; i < tempresult.size(); i ++){
                Decimal unScaled = (Decimal) tempresult[i-1].costrate.Bill_Rate__c + (tempresult[i-1].costrate.Bill_Rate__c)*(Decimal.valueOf(InflationRate))/100;
                tempresult[i].costrate.Bill_Rate__c = unScaled.setScale(2);
             }
        }
            result = tempresult;
            return null;
        }
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this the method to Save bill rate records
    */
    Public Pagereference save(){
        List<Bill_Rate__c> updatelist = new List<Bill_Rate__c>();
        
        System.debug('*** inflation Rate *** '+inflationRate +' *** applyInflationStartYear *** '+applyInflationStartYear+ ' *** scheduledYear *** '+scheduledYear);
        for(BillRateInflationWrapper c : result){
            c.costrate.Inflation_Rate__c = (inflationRate == null || String.isEmpty(inflationRate))? '0': InflationRate;
            //c.costrate.Apply_Inflation_Start_Year__c = (applyInflationStartYear == 0)? Integer.valueOf(scheduledYear): applyInflationStartYear;
            updatelist.add(c.costrate);
        }
        update updatelist;
        return null;
    }
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this the method to redirect user to the bill rate's model
    */
    public PageReference back(){
        String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + modelId ;
        PageReference returnPage = new PageReference(baseURL);
        returnPage.setRedirect(true);    
        return returnPage;    
    }
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this the method for deleting bill rate records(soft delete)
    */    
    Public Pagereference deleterecords(){
        List<Bill_Rate__c> deletelist= new List<Bill_Rate__c>();
        for(BillRateInflationWrapper c : result){
                c.costrate.IsActive__c  = false;
                deletelist.add(c.costrate);
        }
        update deletelist;
        getresult();
        return null;
    }
    
    /**
    @author Niharika Reddy
    @date 2015
    @description this the wrapper class for holding bill rate and selected value
    */
    public class BillRateInflationWrapper {
        public Bill_Rate__c costrate {get;set;}
        public Boolean selected {get;set;}
        public BillRateInflationWrapper (Bill_Rate__c c, Boolean sel) {
            this.costrate = c;
            this.selected = sel;
        }
    }    
}