/** Implements the Selector Layer of the object StudyMileStone__c
 * @author	Dimitrios Sgourdos
 * @version	25-Feb-2014
 */
public with sharing class BDT_StudyMileStoneDataAccessor {
	
	/** Object definition for fields used in application for StudyMileStone
	 * @author	Dimitrios Sgourdos
	 * @version 25-Feb-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('StudyMileStone__c');
	}
	
	
	/** Object definition for fields used in application for StudyMileStone with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 25-Feb-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'DueDate__c,';
		result += referenceName + 'MileStoneDefinition__c,';
		result += referenceName + 'Study__c';
		
		return result;
	}
	
	
	/** Retrieve the list of StudyMileStones that meets the given criteria.
	 * @author	Dimitrios Sgourdos
	 * @version 25-Feb-2014
	 * @param	whereClause			The criteria that the StudyMileStones must meet
	 * @param	orderByField		The field that the StudyMileStones will be ordered by
	 * @return	The list of StudyMileStones
	 */
	public static List<StudyMileStone__c> getStudyMileStones(String whereClause, String orderByField) {
		// Check parmateres
		String tmpWhereClause  = String.IsBlank(whereClause)?  '' : 'WHERE ' + whereClause;
		String tmpOrderByField = String.IsBlank(orderByField)? '' : 'ORDER BY ' + orderByField;
		
		String query = String.format(
								'SELECT {0}, {1}, {2} ' +
								'FROM StudyMileStone__c ' +
								'{3} {4}',
								new List<String> {
									getSObjectFieldString(),
									StudyDataAccessor.getSObjectFieldString('Study__r'),
									BDT_MileStoneDefinitionDataAccessor.getSObjectFieldString('MileStoneDefinition__r'),
									tmpWhereClause,
									tmpOrderByField
								}
							);
		return (List<StudyMileStone__c>) Database.query(query);
	}
}