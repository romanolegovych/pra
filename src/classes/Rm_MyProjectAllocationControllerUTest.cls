/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Rm_MyProjectAllocationControllerUTest {

  
	static list<Country__c> lstCountry;	
    
    static list<Employee_Details__c> lstEmpl; 
    static list<Employee_Status__c> lstEmplStatus;
    
    static list<WFM_Client__c> lstClient;
    static list<WFM_Contract__c>  lstContract;
    static list<WFM_Project__c> lstProject;
    static list<WFM_Protocol__c> lstProt;
    static list<WFM_Site_Detail__c> lstSite;
    

    
    static List<WFM_Location_Hours__c> lstHrs;
    static List<WFM_Location_Hours__c> hrs1;
    static List<WFM_Employee_Availability__c> lstAvas;
    static WFM_employee_Allocations__c alloc; 
    static WFM_employee_Allocations__c alloc1;
   
    static List<WFM_Employee_Assignment__c> lstAssigns;
    static List<WFM_Employee_Assignment__c> lstAssigns1;
  	static list<WFM_EE_Work_History__c> lstHistory;
  	static list<Job_Class_Desc__c> lstJob;
    
    
    static RM_ConstantSetting__c emailGroup;
    
    static RM_UnitTestData init(){
    	RM_UnitTestData testData = new RM_UnitTestData();
    	lstCountry = testData.lstCountry;
    	lstEmpl = testData.lstEmpl;
    	lstEmplStatus = testData.lstEmplStatus;
    	lstClient = testData.lstClient;
    	lstContract = testData.lstContract;
    	lstProject = testData.lstProject;
    	lstJob = testData.lstJobs;
    	system.debug('-----lstJob------'+lstJob); 
    	lstHrs = testData.insertLocationHr(-4, 6, 'TTT'); 
    	hrs1 = testData.insertLocationHr(-4, 6, 'TTS');
    	lstAvas = testData.insertAvailability(lstEmpl[0], lstHrs);
    	alloc = testData.insertAllocation(lstEmpl[0].id, lstProject[0].id, RM_Tools.GetStringfromDate(system.today(), 'mm/dd/yyyy'), RM_Tools.GetStringfromDate(RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(5)), 'mm/dd/yyyy'),  lstJob[0].name, lstCountry[0].Name);
    	system.debug('-----alloc------'+alloc); 
    	alloc1 = testData.insertAllocation(lstEmpl[0].id, lstProject[0].id, RM_Tools.GetStringfromDate(system.today(), 'mm/dd/yyyy'), RM_Tools.GetStringfromDate(RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(5)), 'mm/dd/yyyy'),  lstJob[1].name,lstCountry[0].Name);
    	system.debug('-----alloc1------'+alloc1); 
    	lstAssigns = testData.insertAssignment(lstEmpl[0].id, alloc, lstAvas, 0.2);
    	lstAssigns1 = testData.insertAssignment(lstEmpl[0].id, alloc1, lstAvas, 0.5);
    	system.debug('---------Test lstCountry---------'+lstCountry  ); 
    	system.debug('---------Test lstHrs---------'+lstHrs  ); 
    	//reset email to tester's email
    	user u = RM_OtherService.GetUserInfo(UserInfo.getUserId());
    	lstEmpl[0].Email_Address__c = u.Email;
    	//lstEmpl[0].Job_Class_Desc__c = 'Clinical Research Associate';
    	update lstEmpl[0];
    	   // four month history
    	   
    	lstHistory = testData.insertWorkHist(lstProject[0].id, lstEmpl[0].id, lstAvas);
        system.debug('---------Test lstHistory---------'+lstHistory  ); 
      
    	emailGroup = testData.emailGroup;
    	return testData;
    }
      
    static testMethod void TestMonthList() {
    	init();
    	
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/Rm_MyProjectAllocation?id=' + lstProject[0].id);
     
        Test.setCurrentPage(pageRef);
        Rm_MyProjectAllocationController flg = new Rm_MyProjectAllocationController();
        list<string>  lstMonth = flg.getMonthLab();
       
       
        system.assert(lstMonth.size() > 0 );
        Test.stopTest(); 
    }
    static testMethod void TestAllocationList() {
    	init();
    
        Test.startTest();
         PageReference pageRef = new PageReference('/apex/Rm_MyProjectAllocation?id=' + lstProject[0].id);
     
        Test.setCurrentPage(pageRef);
        Rm_MyProjectAllocationController flg = new Rm_MyProjectAllocationController();
        list<RM_AssignmentOp.ProjectAllocation> lstOp = flg.getProAllocationList();
       
       
      //  system.assert(lstOp.size() > 0 );
        Test.stopTest(); 
    }
}