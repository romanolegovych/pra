@isTest
public class PAWS_OperationalizeProjectCntrlTests
{
	@isTest private static void testOperationalizeProjectCntrl()
	{
		PAWS_ApexTestsEnvironment.init();
		ecrf__c project = PAWS_ApexTestsEnvironment.Project;
		PAWS_Project_Flow_Junction__c projectFlow = PAWS_ApexTestsEnvironment.ProjectFlow;
		
		Test.startTest();
		
		PAWS_OperationalizeProjectController controller = new PAWS_OperationalizeProjectController();
		
		controller.Init();
		
		PageReference pageRef = Page.PAWS_OperationalizeProject;
		pageRef.getParameters().put('id', String.valueOf(project.Id));
		Test.setCurrentPage(pageRef);
		
		controller.Init();
		system.assert(controller.GetActionsListJSON() != null);
		controller.InvokeRM_API();
		controller.Finalize();
		
		Test.stopTest();
	}
}