public with sharing class BDT_FinancialDocStudySelController {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} //Financial Documents Study Selection
	public  List<Study__c>			proposalStudies		{get;set;}
	public  FinancialDocument__c 	financialDocument	{get;set;}
	public  String 					financialDocumentId	{get;set;}
	public  Boolean					showPage 			{get;set;}
	public	Boolean					invalidDocFlag		{get;set;}
	public	Boolean					approvedDocFlag		{get;set;}
	private String					selStudiesInAcceptedDoc;
	public  List<BDT_DC_FinancialDocument.studySelectionInFinDocProcessWrapper>	studiesWrapperList{get;set;}
	
	
	/**
	*	The constructor of the class.
	*	@author   Dimitrios Sgourdos
	*	@version  11-Sep-2013
	*/
	public BDT_FinancialDocStudySelController() {
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Financial Documents Study Selection');
		if(initializeGlobalVariables()) {
			if(financialDocument.DocumentType__c == 'Proposal') {
				studiesWrapperList = BDT_DC_FinancialDocument.createStudySelectionRecordsForProposal(financialDocument, proposalStudies, selStudiesInAcceptedDoc);
			} else { // to be changed
				studiesWrapperList = BDT_DC_FinancialDocument.createStudySelectionRecordsForChangeOrder(financialDocument, proposalStudies);
			}
			
		}
	}
	
	
	/**
	*	Initialize the global variables of the class.
	*	@author   Dimitrios Sgourdos
	*	@version  11-Sep-2013
	*	@return   If everything went well
	*/
	private Boolean initializeGlobalVariables() {
		studiesWrapperList = new List<BDT_DC_FinancialDocument.studySelectionInFinDocProcessWrapper>();
		showPage = true;
		financialDocumentID = system.currentPageReference().getParameters().get('financialDocumentID');
		if(String.isBlank(financialDocumentID)) {
			errorOnReadingFinancialDocument();
			return false;
		} else {
			financialDocument = FinancialDocumentService.getFinancialDocumentById(financialDocumentId);
			invalidDocFlag = (financialDocument.DocumentStatus__c == FinancialDocumentDataAccessor.DOCUMENT_STATUS_INVALID);
			approvedDocFlag = (financialDocument.DocumentStatus__c == FinancialDocumentDataAccessor.DOCUMENT_STATUS_APPROVED);
			proposalStudies = BDT_DC_FinancialDocument.readProjectStudies(financialDocument.Client_Project__c);
			if( String.isBlank(financialDocument.Name) ) {
				errorOnReadingFinancialDocument();
				return false;
			}
			selStudiesInAcceptedDoc = BDT_DC_FinancialDocument.studiesIDsOfAcceptedProposals(financialDocument.Client_Project__c);
		}
		return true;
	}
	
	
	/**
	*	Give an error message in case the financial document is not found.
	*	@author   Dimitrios Sgourdos
	*	@version  09-Sep-2013
	*/
	private void errorOnReadingFinancialDocument() {
		showPage = false;
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected financial document.Please, select a document and reload the page.');
		ApexPages.addMessage(msg);
	}
	
	
	/**
	*	Cancel the study selection
	*	@author   Dimitrios Sgourdos
	*	@version  11-Sep-2013
	*/
	public void closePage() {
		showPage = false;
	}
	
	
	/**	Save the study selection
	* @author	Dimitrios Sgourdos
	* @since	11-Sep-2013
	* @version  16-Jan-2014
	*/
	public void save() {
		String oldDocumentStatus = financialDocument.DocumentStatus__c;
		
		String listOfSelStudies = BDT_DC_FinancialDocument.readSelectedStudiesForFinancialDocument(studiesWrapperList);
		financialDocument.listOfStudyIds__c = (String.isBlank(financialDocument.listOfStudyIds__c))?
													'' :
													financialDocument.listOfStudyIds__c;
		
		if( ! listOfSelStudies.equalsIgnoreCase(financialDocument.listOfStudyIds__c) ) {
			String oldStudiesString = financialDocument.listOfStudyIds__c;
			
			financialDocument.listOfStudyIds__c = listOfSelStudies;
			if( oldDocumentStatus == FinancialDocumentDataAccessor.DOCUMENT_STATUS_NEW
					|| oldDocumentStatus == FinancialDocumentDataAccessor.DOCUMENT_STATUS_UNDEFINED) {
				financialDocument.DocumentStatus__c = FinancialDocumentDataAccessor.DOCUMENT_STATUS_IN_PROGRESS;
			}
			update financialDocument;
			
			// Update the StudyServicePricing object 
			BDT_DC_FinancialDocument.updateStudyServicePricingWithStudySelection(financialDocumentId,
																				oldStudiesString,
																				listOfSelStudies);
			
			// Capture action to the history (retrieve the document again to have the adjusted price)
			financialDocument = FinancialDocumentService.getFinancialDocumentById(financialDocumentId);
			
			String totalValueString = '';
			if(financialDocument.TotalValue__c != null) {
				totalValueString = financialDocument.TotalValue__c;
			}
			
			FinancialDocumentHistoryService.saveHistoryObject(financialDocument.id,
														'Study Selection',
														'Study selection was adjusted',
														totalValueString,
														FinancialDocumentHistoryDataAccessor.STUDY_SELECTION_PROCESS);
			
			// Send e-mail notifications for adjustments
			FinancialDocumentApprovalService.sendAdjustmentsEmailNotifications(financialDocument,
														FinancialDocumentHistoryDataAccessor.STUDY_SELECTION_PROCESS,
														userInfo.getName());
		}
		
		// Check if the approvals are up to date
		Boolean oldValueFlag = financialDocument.ApprovalsUpToDate__c;
		Boolean upToDateFlag = FinancialDocumentApprovalService.areDocumentApprovalsUpToDate(financialDocument.id);
		if(oldValueFlag != upToDateFlag) {
			financialDocument.ApprovalsUpToDate__c = upToDateFlag;
			update financialDocument;
		}
		
		showPage = false;
	}
}