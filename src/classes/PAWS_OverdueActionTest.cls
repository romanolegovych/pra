/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@istest
private class PAWS_OverdueActionTest
{
	static @istest void coverOverdueAction()
	{
		PAWS_ApexTestsEnvironment.init();
		System.debug(PAWS_ApexTestsEnvironment.FlowInstanceCursor);
		insert new STSWR1__Gantt_Step_Property__c(STSWR1__Planned_End_Date__c = System.today().addYears(-1), STSWR1__Flow__c = PAWS_ApexTestsEnvironment.Flow.Id, STSWR1__Step__c = PAWS_ApexTestsEnvironment.FlowStep.Id);
		
		PAWS_OverdueAction testee = new PAWS_OverdueAction();
		try
		{
			testee.run(new PAWS_Project_Flow_Junction__c(Flow__c = PAWS_ApexTestsEnvironment.Flow.Id), new STSWR1__Flow_Instance_Cursor__c(), null, null);
			System.assert(false, 'Expecting exception above!');
		}
		catch (Exception e)
		{
			System.assert(true, 'Expected exception caught: ' + e.getMessage());
		}
		
		testee.clearHistory(PAWS_ApexTestsEnvironment.FlowInstanceCursor);
		testee.sendEmail('test', new List<String>{'test@test.com'});
	}
}