@isTest
private class COM_FactoryUTest {

    static testMethod void getIntegrationServiceTest() {
        COM_IIntegrationService iService = null;
        String intSystem = 'ETMF';
        String apiType = 'Apttus__Agreement_Document__c';
        
        try {
            iService = COM_Factory.getIntegrationService(null, null);
        } catch(COM_Exception ex) {
            system.assertEquals(COM_ErrorCode.INTEGRATION_SERVICE_NOT_FOUND, ex.getCode());
        }
        try {
            iService = COM_Factory.getIntegrationService(intSystem, null);
        } catch(COM_Exception ex) {
            system.assertEquals(COM_ErrorCode.INTEGRATION_SERVICE_NOT_FOUND, ex.getCode());
        }
        try {
            iService = COM_Factory.getIntegrationService(null, apiType);
        } catch(COM_Exception ex) {
            system.assertEquals(COM_ErrorCode.INTEGRATION_SERVICE_NOT_FOUND, ex.getCode());
        }
        try {
            iService = COM_Factory.getIntegrationService('Invalid System', 'Invalid Type');
        } catch(COM_Exception ex) {
            system.assertEquals(COM_ErrorCode.INTEGRATION_SERVICE_NOT_FOUND, ex.getCode());
        }
        /*
        The below code will fail since the service implementations do not exist in this branch.
        iService = COM_Factory.getIntegrationService(intSystem, apiType);
        system.assertNotEquals(null, iService);
        system.assertEquals(intSystem, iService.getIntSystem());
        system.assertEquals(apiType, iService.getSObjectType());
        */
    }
}