@isTest
private class BDT_FinancialDocPricingControllerTest {

	public static FinancialDocument__c financialDocument;
	
	
static void init(){
		
        List<Study__c> StudiesList = new List<Study__c>();
		// Create test data
		BDT_CreateTestData.createall();
		Client_Project__c project = [select id from Client_Project__c limit 1];
		BDT_Utils.setPreference('SelectedProject', String.valueOf(project.id));
		String StudyIDs = '';
		for (Study__c st : [select id from Study__c where Project__c = :project.id]) {
			StudyIDs = StudyIDs + ':' + st.id;
			StudiesList.add(st);
		}
    	BDT_Utils.setPreference('SelectedStudies', StudyIDs.removeStart(':'));

		// add services
		List<StudyService__c> ss = new List<StudyService__c>();
		For (ProjectService__c ps:[select id from ProjectService__c]){
			StudyService__c s = new StudyService__c();
			s.ProjectService__c = ps.id;
			s.NumberOfUnits__c = 2;
			s.Study__c = StudiesList[0].id;
			ss.add(s);
		}
		upsert ss;

    	// Create the new financial document, with the predefined attributes values   	
		financialDocument = new FinancialDocument__c();
		financialDocument.Name = 'Test name';
		financialDocument.DocumentStatus__c = ( StudiesList.size()>0 )? 'New' : 'Undefined';
		financialDocument.DocumentType__c   = 'Proposal';
		financialDocument.Client_Project__c = project.id;
		financialDocument.listOfStudyIds__c = StudyIDs.removeStart(':');
		insert financialDocument; 
		
		list<StudyServicePricing__c> sspList = [Select s.UnitPricePRAProposal__c, s.UnitPriceNegotiated__c, s.SystemModstamp, s.StudyService__c, s.Service__c, s.RateCardPrice__c, s.RateCardCurrency__c, s.RateCardClass__c, s.NumberOfUnits__c, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsDeleted, s.Id, s.FinancialDocumentIDs__c, s.DiscountOnTotal__c, s.CreatedDate, s.CreatedById, s.CalculatedTotalPrice__c, s.CalculatedDiffToRateCard__c, s.ApprovedFinancialDocument__c From StudyServicePricing__c s];
		for (StudyServicePricing__c ssp:sspList){
			ssp.FinancialDocumentIDs__c = financialDocument.id;
		}
		upsert sspList;
		
	}

	static testMethod void myFirstUnitTest() {
		init();
		// no document
		BDT_FinancialDocPricingController pageController = new BDT_FinancialDocPricingController();
	}
	
	static testMethod void mySecondUnitTest() {
		init();
		
		PageReference b = new pagereference(System.Page.BDT_FinancialDocPricing.getURL() );
		b.getParameters().put('financialDocumentID',''+financialDocument.id);
		test.setCurrentPage(b);
		
		// document set
		BDT_FinancialDocPricingController pageController = new BDT_FinancialDocPricingController();
		
		pageController.fakeSave();
		pageController.save();
    }
    
    static testMethod void myThirdUnitTest() {
		init();
		
		PageReference b = new pagereference(System.Page.BDT_FinancialDocPricing.getURL() );
		b.getParameters().put('financialDocumentID',''+financialDocument.id);
		test.setCurrentPage(b);
		
		// document set
		BDT_FinancialDocPricingController pageController = new BDT_FinancialDocPricingController();
		
		pageController.editExchangeRateStudy = pageController.Studies[0].id;
		pageController.showExchangeRateForStudy();
		pageController.exchangeRates[0].TargetCurrency__c = 'ANG';
		pageController.exchangeRates[0].ExchangeRate__c = 2.3;
		pageController.applyExchangeRate();
		
    }
    
    static testMethod void myFourthUnitTest() {
    	List<StudyService__c> sList = [select id,NumberOfUnits__c from StudyService__c];
    	For (StudyService__c s:sList){
			s.NumberOfUnits__c = 5;
		}
		upsert sList;
    }
    
    static testMethod void myFifthUnitTest() {
		init();
		
		PageReference b = new pagereference(System.Page.BDT_FinancialDocPricing.getURL() );
		b.getParameters().put('financialDocumentID',''+financialDocument.id);
		test.setCurrentPage(b);
		
		// document set
		BDT_FinancialDocPricingController pageController = new BDT_FinancialDocPricingController();
		pageController.financialDocument.PricingComplete__c = true;
		pageController.save();

    }

    static testMethod void mySixthUnitTest() {
		init();
		
		PageReference b = new pagereference(System.Page.BDT_FinancialDocPricing.getURL() );
		b.getParameters().put('financialDocumentID',''+financialDocument.id);
		test.setCurrentPage(b);
		
		// document set
		BDT_FinancialDocPricingController pageController = new BDT_FinancialDocPricingController();
		pageController.financialDocument.PricingComplete__c = true;
		pageController.comment = 'Happy to be here';
		pageController.save();

    }


}