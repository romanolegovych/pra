/** Implements the test for the Selector Layer of the object LaboratoryAnalysis__c
 * @author	Dimitrios Sgourdos
 * @version	11-Nov-2013
 */
@isTest
private class LaboratoryAnalysisDataAccessorTest {
	
	// Global variables
	private static List<LaboratoryMethodCompound__c>	compoundList;
	private static List<Study__c>						studiesList;
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static void init(){
		// Create project
		Client_Project__c firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;
		
		// Create studies
		studiesList = BDT_TestDataUtils.buildStudies(firstProject,3);
		insert studiesList;
		
		// Create Laboratory Methods
		List<LaboratoryMethod__c> lmList = new  List<LaboratoryMethod__c>();
		lmList.add(new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
											Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood',
											Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle'));
		lmList.add(new LaboratoryMethod__c (Name='PRA-NL-LML-0002', AnalyticalTechnique__c='ELISA', AntiCoagulant__c='N/A',
											Department__c='SML', Detection__c='N/A', Location__c='PRA-US', Matrix__c='Breast milk',
											Proprietary__c='N', ShowOnMethodList__c='Y', Species__c='Human'));
		insert lmList; 
		
		// Create Laboratory Method Compounds
		compoundList = new List<LaboratoryMethodCompound__c>();
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[0], 'midazolam') );
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lmList[0], '4-OH') );
		insert compoundList;
	}
	
	
	/** Test the function getByMdOrValAndUserStudies
	 * @author	Dimitrios Sgourdos
	 * @version	06-Nov-2013
	 */
	static testMethod void getByMdOrValAndUserStudiesTest() {
		init();
		
		// Create analysis
		List<LaboratoryAnalysis__c> analysisList = new List<LaboratoryAnalysis__c>();
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[0], false,  false, false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[1], false,  true,  false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[1], false,  false, true));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[2], false,  true,  true));
		insert analysisList;
		
		// Query for the first two studies
		List<Study__c> tmpStudiesList = new List<Study__c>();
		tmpStudiesList.add(studiesList[0]);
		tmpStudiesList.add(studiesList[1]);
		List<LaboratoryAnalysis__c> results = LaboratoryAnalysisDataAccessor.getByMdOrValAndUserStudies(tmpStudiesList);
		
		// Expected results 2, as 2 from the 3 analysis, which are assigned with the first two studies, have MethodDevelopment__c or MethodValidation__c equal to TRUE  
		system.assertEquals(results.size(), 2);
	}
	
	
	/** Test the function getByAnalysisAndUserStudies
	 * @author	Dimitrios Sgourdos
	 * @version	11-Nov-2013
	 */
	static testMethod void getByAnalysisAndUserStudiesTest() {
		init();
		
		// Create analysis
		List<LaboratoryAnalysis__c> analysisList = new List<LaboratoryAnalysis__c>();
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[0], false, false, false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[1], true,  false, false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[1], true,  false, false));
		analysisList.add(LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compoundList[0], studiesList[2], true,  true,  false));
		insert analysisList;
		
		// Query for the first two studies
		List<Study__c> tmpStudiesList = new List<Study__c>();
		tmpStudiesList.add(studiesList[0]);
		tmpStudiesList.add(studiesList[1]);
		List<LaboratoryAnalysis__c> results = LaboratoryAnalysisDataAccessor.getByAnalysisAndUserStudies(tmpStudiesList);
		
		// Expected results 2, as 2 from the 3 analysis, which are assigned with the first two studies, have Analysis__c equal to TRUE  
		system.assertEquals(results.size(), 2);
	}
}