@isTest
private class BDT_NewEditSponsorTest {
	
	static testMethod void createBUPath() { 
		BDT_NewEditSponsor newEditSpon = new BDT_NewEditSponsor();
		
		newEditSpon.sponsor.Name = 'mysponsor';
		newEditSpon.sponsor.Abbreviation__c = 'MINE';
		
		System.assertNotEquals(null, newEditSpon.save());
		
		System.currentPageReference().getParameters().put('sponsorId', newEditSpon.sponsor.id);
		BDT_NewEditSponsor newEditSpon1 = new BDT_NewEditSponsor();
		
		System.assertNotEquals(null, newEditSpon1.cancel());
		
	}
	
	static testMethod void failEditSponsorPath() { 
		
		System.currentPageReference().getParameters().put('sponsorId', '1111111111111111');
		BDT_NewEditSponsor newEditSponFail = new BDT_NewEditSponsor();
		
		System.assertNotEquals(null, newEditSponFail.sponsor);
		
	}
	
	static testMethod void deleteSponsorPath() { 
		
		BDT_NewEditSponsor newEditSpon = new BDT_NewEditSponsor();
		
		newEditSpon.sponsor.Name = 'mysponsor';
		newEditSpon.sponsor.Abbreviation__c = 'MINE';
		
		System.assertNotEquals(null, newEditSpon.save());
		
		System.currentPageReference().getParameters().put('sponsorId', newEditSpon.sponsor.id);
		BDT_NewEditSponsor newEditSpon1 = new BDT_NewEditSponsor();
		
		System.assertNotEquals(null, newEditSpon1.deleteSoft());
		
	}

}