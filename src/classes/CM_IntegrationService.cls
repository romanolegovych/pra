/**
*   Represents the Business Logic Service layer for Contract Management integrations
*/

public with sharing class CM_IntegrationService {

    public static void upsertSiteContracts(Apttus__APTS_Agreement__c contract) {
        
        if(null != contract) {
            CM_CtmsService_SiteContract.contract cont = new CM_CtmsService_SiteContract.contract();
            cont.ctaId = contract.Id;
            cont.account = contract.Site__r.Account_Institution__r.Name;
            //cont.accountId = contract.Site__r.Account__c; //-- Ext Id
            cont.budgetApprovedDate = contract.Budget_Approved_Date__c.format();
            cont.budgetReceivedDate = contract.Budget_Rec_Date__c.format();
            cont.budgetSentDate = contract.Budget_Sent_Date__c.format();
            cont.comments = contract.Comments__c;
            cont.contractId = contract.Site__r.Project_Protocol__r.Project_ID__r.Project_Unique_Key__c;
            cont.contractName = contract.Name;
            cont.contractType = contract.Contract_Type__c;
            cont.firstDraftSentDate = contract.First_Draft_Sent_Date__c.format();
            cont.firstSiteCommentsDate = contract.First_Site_Comments_Rec_Date__c.format();
            cont.fullyExecutedDate = contract.Fully_Exec_Date__c.format();
            cont.id = contract.Integration_ID__c;
            cont.negotiationCompleteDate = contract.Neg_Complete_Date__c.format();
            cont.partiallyExecutedDate = contract.Partially_Exec_Date__c.format();
            cont.plannedFinalExecutionDate = contract.Planned_Final_Exec_Date__c.format();
            cont.sponsorName = contract.Site__r.Project_Protocol__r.Sponsor_ID__r.Name;
            cont.contractHolder = contract.Contract_Holder__c;
            cont.team = contract.Team__r.Username;
            
            CM_CtmsService_SiteContract.status status = new CM_CtmsService_SiteContract.status();
            status.status = contract.Apttus__Status__c;
            status.date_x = Datetime.now().format('YYYY-MM-DDThh:mm:ss+hh:mm'); 
            
            CM_CtmsService_SiteContract.statuses statuses = new CM_CtmsService_SiteContract.statuses();
            statuses.status = new CM_CtmsService_SiteContract.status[]{status};
            cont.statuses = statuses;
            
            CM_CtmsService_SiteContract.site site = new CM_CtmsService_SiteContract.site();
            site.id = contract.Site__r.Site_ID__c;
            CM_CtmsService_SiteContract.contracts contracts = new CM_CtmsService_SiteContract.contracts();
            contracts.contract = new CM_CtmsService_SiteContract.contract[]{cont};
            site.contracts = contracts;
            CM_CtmsService.WriteSiteContract_Output_element response = null;
            //CM_CtmsServiceWrapper.upsertSiteContracts(site, Userinfo.getUserName());
            if(null != response && null != response.sites && null != response.sites.site) {
                    for(CM_CtmsService_SiteContract.site siteResponse : response.sites.site) {
                    if (null != siteResponse  && null != siteResponse.contracts && null != siteResponse.contracts.contract) {
                        for(CM_CtmsService_SiteContract.contract contResp : siteResponse.contracts.contract) {
                            contract.Integration_ID__c = contResp.id;
                            system.debug('upsertSiteContracts:Updated integrationId for siteID:'+siteResponse.id+', contractID:'+contResp.id);
                        }
                    }
                    }
                }
            
        }
    }
}