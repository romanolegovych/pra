@isTest(SeeAllData=false)
private class PRA_MonthlyApprovalBatchTest {
    
    ///*ASA 
    static testMethod void testMonthlyApproval() {
        Test.startTest();   
    
        Date startOfMonth = Date.today().toStartOfMonth();    
        system.debug ('startOfMonth  = ' +  startOfMonth ); 
        if(date.today()!=startOfMonth){
            Approval_Calendar__c ac = new Approval_Calendar__c(Month_Approval_Applies_To__c= Date.today().addMonths(0).toStartOfMonth(),
                                                                Forecast_Approved_by_DPD__c= (Date.today()-1),Forecast_Approved_by_Project_Manager__c= (Date.today()-1),Worked_Units_Approved_by_Project_Manager__c= Date.today()-1
                                                                ,Worked_Units_Confirmed_by_project_Team__c= Date.today()-1,Worked_Units_Screen_Available_for_Update__c= Date.today()-1);        
        
            insert ac; 
        }else{
        
          Approval_Calendar__c ac = new Approval_Calendar__c(Month_Approval_Applies_To__c= Date.today().addMonths(-1).toStartOfMonth(),
                                                                Forecast_Approved_by_DPD__c= (Date.today()-1),Forecast_Approved_by_Project_Manager__c= (Date.today()-1),Worked_Units_Approved_by_Project_Manager__c= Date.today()-1
                                                                ,Worked_Units_Confirmed_by_project_Team__c= Date.today()-1,Worked_Units_Screen_Available_for_Update__c= Date.today()-1);        
        
            insert ac; 
        
        }    
     
             
        //System.assertEquals(date.today().toStartOfMonth(), ac.Month_Approval_Applies_To__c);
        //System.assertEquals(date.today()-1, ac.Forecast_Approved_by_Project_Manager__c);  
      
        Client_Project__c project = new Client_Project__c(PRA_Project_Id__c = 'UCB');
        insert project;
        Profile pr = [select id, name from Profile where name = 'System Administrator'];
        User u = new User(alias='TestUser',Email='testPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='PZ', 
                            LocaleSidKey='en_US', ProfileId=pr.id, TimeZoneSidKey='America/New_York', Username='Test.PZ@praintl.com.unitTestPMConsole');
        insert u;
        
        Monthly_Approval__c mp = new Monthly_Approval__c(Client_Project__c=project.ID,Month_Approval_Applies_To__c= Date.today().addMonths(0).toStartOfMonth());
        if(Date.today()== Date.today().addMonths(0).toStartOfMonth())
        mp.Month_Approval_Applies_To__c= Date.today().addMonths(-1).toStartOfMonth();       
        mp.PM_Approved_Worked_Units__c=u.id; 
        String  month=(string.valueof((mp.Month_Approval_Applies_To__c).month()).length()==1)?string.valueof('0'+(mp.Month_Approval_Applies_To__c).month()):string.valueof((mp.Month_Approval_Applies_To__c).month());                
        String madate=string.valueof((mp.Month_Approval_Applies_To__c).year())+string.valueof(month)+'01';
        system.debug('------month--madate----'+month+madate);
        mp.MA_Unique_ID__c = Project.Name+':'+madate;
        insert mp;
        
        mp.Is_approval_complete__c =false;
        mp.PM_Approved_Forecast__c = null;
        mp.DPD_approved_forecast__c = null;
        mp.PZ_released_forecast__c =u.id; 
        update mp;
        
        (project.Director_of_Project_Delivery__c)=u.id;
        (project.General_Partner__c)=u.id;
        (project.Global_Project_Analyst__c)=u.id;
        (project.Global_Project_Manager_Director__c)=u.id;
        (project.Executive_Vice_President__c)=u.id;
        (project.Manager_Director_of_Operations_Finance__c)=u.id;      
         update project;
        
           
         PRA_MonthlyApprovalBatch scheduler = new PRA_MonthlyApprovalBatch();
         
         Datetime dt = System.now().addSeconds(2);        
         scheduler.execute(null);
        Test.stopTest();
    }
    
     ///*ASA 
    static testMethod void testMonthlyApprovalforPZNotRelease() {
        Test.startTest();   
    
        Date startOfMonth = Date.today().toStartOfMonth();    
        system.debug ('startOfMonth  = ' +  startOfMonth ); 
        if(date.today()!=startOfMonth){
            Approval_Calendar__c ac = new Approval_Calendar__c(Month_Approval_Applies_To__c= Date.today().addMonths(0).toStartOfMonth(),
                                                                Forecast_Approved_by_DPD__c= (Date.today()-1),Forecast_Approved_by_Project_Manager__c= (Date.today()-1),Worked_Units_Approved_by_Project_Manager__c= Date.today()-1
                                                                ,Worked_Units_Confirmed_by_project_Team__c= Date.today()-1,Worked_Units_Screen_Available_for_Update__c= Date.today()-1);        
        
            insert ac; 
        }else{
        
          Approval_Calendar__c ac = new Approval_Calendar__c(Month_Approval_Applies_To__c= Date.today().addMonths(-1).toStartOfMonth(),
                                                                Forecast_Approved_by_DPD__c= (Date.today()-1),Forecast_Approved_by_Project_Manager__c= (Date.today()-1),Worked_Units_Approved_by_Project_Manager__c= Date.today()-1
                                                                ,Worked_Units_Confirmed_by_project_Team__c= Date.today()-1,Worked_Units_Screen_Available_for_Update__c= Date.today()-1);        
        
            insert ac; 
        
        }    
     
             
        //System.assertEquals(date.today().toStartOfMonth(), ac.Month_Approval_Applies_To__c);
        //System.assertEquals(date.today()-1, ac.Forecast_Approved_by_Project_Manager__c);  
      
        Client_Project__c project = new Client_Project__c(PRA_Project_Id__c = 'UCB');
        insert project;
        Profile pr = [select id, name from Profile where name = 'System Administrator'];
        User u = new User(alias='TestUser',Email='testPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='PZ', 
                            LocaleSidKey='en_US', ProfileId=pr.id, TimeZoneSidKey='America/New_York', Username='Test.PZ@praintl.com.unitTestPMConsole');
        insert u;
        
        Monthly_Approval__c mp = new Monthly_Approval__c(Client_Project__c=project.ID,Month_Approval_Applies_To__c= Date.today().addMonths(0).toStartOfMonth());
        if(Date.today()== Date.today().addMonths(0).toStartOfMonth())
        mp.Month_Approval_Applies_To__c= Date.today().addMonths(-1).toStartOfMonth();       
        mp.PM_Approved_Worked_Units__c=u.id; 
        String  month=(string.valueof((mp.Month_Approval_Applies_To__c).month()).length()==1)?string.valueof('0'+(mp.Month_Approval_Applies_To__c).month()):string.valueof((mp.Month_Approval_Applies_To__c).month());                
        String madate=string.valueof((mp.Month_Approval_Applies_To__c).year())+string.valueof(month)+'01';
        system.debug('------month--madate----'+month+madate);
        mp.MA_Unique_ID__c = Project.Name+':'+madate;
        
        insert mp;
        
        mp.Is_approval_complete__c =false;
        mp.PM_Approved_Forecast__c = null;
        mp.DPD_approved_forecast__c = null;
        //mp.PZ_released_forecast__c =u.id; 
        update mp;
        
        //(project.Director_of_Project_Delivery__c)=u.id;
        (project.General_Partner__c)=u.id;
        //(project.Global_Project_Analyst__c)=u.id;
        (project.Global_Project_Manager_Director__c)=u.id;
        //(project.Executive_Vice_President__c)=u.id;
        (project.Manager_Director_of_Operations_Finance__c)=u.id;      
         update project;
        
           
         PRA_MonthlyApprovalBatch scheduler = new PRA_MonthlyApprovalBatch();
         
         Datetime dt = System.now().addSeconds(2);        
         scheduler.execute(null);
        Test.stopTest();
    }
    
    
    
    //ASA*/
}