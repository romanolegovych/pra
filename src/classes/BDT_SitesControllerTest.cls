@isTest 
private class BDT_SitesControllerTest {
	
	public static List<Site__c> mySiteList {get;set;}
	public static List<Business_Unit__c> myBUList {get;set;}
	
	static void init(){
		
		myBUList = BDT_TestDataUtils.buildBusinessUnit(1);
		insert myBUList;
		
		mySiteList = BDT_TestDataUtils.buildSite(myBUList);
		insert mySiteList;
	
	}
	
	static testMethod void editBUPath() { 
		init();
		System.currentPageReference().getParameters().put('buId', myBUList[0].id);
		BDT_SitesController buEdit = new BDT_SitesController();
		buEdit.getSitesPerBU();
	}
	
	static testMethod void editSitePath() { 
		init();
		System.currentPageReference().getParameters().put('buId', myBUList[0].id);		
		BDT_SitesController siteEdit = new BDT_SitesController();
		
		System.assertNotEquals(null, siteEdit.EditSite());
	}
	
	static testMethod void noSuccessfullPath() { 
		init();	
		BDT_SitesController siteEdit = new BDT_SitesController();
		
		System.assertNotEquals(null, siteEdit.EditSite());
	}
	
	static testMethod void getSitePerBUPath() { 
		init();
		System.currentPageReference().getParameters().put('selectedbuId', myBUList[0].id);
		BDT_SitesController siteController = new BDT_SitesController();
		siteController.getSitesPerBU();
		System.assertEquals(5,siteController.siteList.size());
	}

}