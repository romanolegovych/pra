/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PRA_ClientTaskSearchServiceTest {

    static testMethod void nullSearch() {
        PRA_TestUtils tu = new PRA_TestUtils();
        initTestData(tu);

		Test.startTest();
		
		PRA_ClientTaskSearchService ctSrch = new PRA_ClientTaskSearchService();
		
		List<Client_Task__c> ctl = ctSrch.getTasks();
		System.assertEquals(0, ctl.size());
		
		List<Unit_Effort__c> uel = ctSrch.getUnitEfforts();
		System.assertEquals(0, uel.size());
		
		List<Regional_Contract_Value__c> rcv = ctSrch.getRegionalContractValues();
		System.assertEquals(0, rcv.size());
		
		Test.stopTest();
    }

    static testMethod void selectProjSearch() {
        PRA_TestUtils tu = new PRA_TestUtils();
        initTestData(tu);

		Test.startTest();
		
		PRA_ClientTaskSearchService ctSrch = new PRA_ClientTaskSearchService();
		
		ctSrch.selectedProject = tu.clientProject.id;
		
		List<Client_Task__c> ctl = ctSrch.getTasks();
		System.assertEquals(5, ctl.size());

		Datetime minusTwoMonths = Datetime.newInstance(DateTime.now().year(), DateTime.now().month()-2, 1);
		ctSrch.ueStartMonth = minusTwoMonths.format('yyyy-MM-dd');
		List<Unit_Effort__c> uel = ctSrch.getUnitEfforts();
		System.assertEquals(12, uel.size());
		
		List<Regional_Contract_Value__c> rcv = ctSrch.getRegionalContractValues();
		System.assertEquals(4, rcv.size());

		Test.stopTest();
    }

    static testMethod void noMigratedSearch() {
        PRA_TestUtils tu = new PRA_TestUtils();
        initTestData(tu);

		Test.startTest();
		
		PRA_ClientTaskSearchService ctSrch = new PRA_ClientTaskSearchService();
		
		ctSrch.selectedProject = tu.clientProject.id;
		ctSrch.filterMigrated = true;
		
		List<Client_Task__c> ctl = ctSrch.getTasks();
		
		System.assertEquals(4, ctl.size());
		
		Test.stopTest();
    }

    static testMethod void initSelectOptions() {
        PRA_TestUtils tu = new PRA_TestUtils();
        initTestData(tu);

		Test.startTest();
		
		PRA_ClientTaskSearchService ctSrch = new PRA_ClientTaskSearchService();
		
		ctSrch.selectedProject = tu.clientProject.id;
		ctSrch.initOnSelectProject();
		
		List<Client_Task__c> ctl = ctSrch.getTasks();
		
		System.assertEquals(5, ctl.size());
		
		Test.stopTest();
    }

    static testMethod void testSelections() {
        PRA_TestUtils tu = new PRA_TestUtils();
        initTestData(tu);

		Test.startTest();
		
		PRA_ClientTaskSearchService ctSrch = new PRA_ClientTaskSearchService();
		
		ctSrch.selectedProject = tu.clientProject.id;
		ctSrch.initOnSelectProject();
		
		List<String> selList = new List<String>();
		List<Task_Group__c> tgList = [SELECT id FROM Task_Group__c];
		selList.add(tgList[0].id);
		
		ctSrch.selectedGroups = selList;
		system.debug('----- selectedGroups before: ' + ctSrch.selectedGroups);
		ctSrch.initGroupOptions();
		system.debug('----- selectedGroups after: ' + ctSrch.selectedGroups);
		
		List<Client_Task__c> ctl = ctSrch.getTasks();
		
		System.assertEquals(3, ctl.size());
		
		Test.stopTest();
    }

	static testMethod void testOrder() {
        PRA_TestUtils tu = new PRA_TestUtils();
        initTestData(tu);

		Test.startTest();
		
		PRA_ClientTaskSearchService ctSrch = new PRA_ClientTaskSearchService();

		ctSrch.selectedProject = tu.clientProject.id;

		// Default order is by description		
		List<Client_Task__c> ctl = ctSrch.getTasks();

		system.assertEquals('3a', ctl[0].Client_Unit_Number__c);
		
		// Set order to Client Unit Num
		ctSrch.orderBy = ctSrch.CT_ORDERBY_CLIENT_UNIT;
		List<Client_Task__c> ctl2 = ctSrch.getTasks();
		
		system.assertEquals('1a', ctl2[0].Client_Unit_Number__c);
		
		Test.stopTest();
	}
	
	static testMethod void testClosedUnits() {
        PRA_TestUtils tu = new PRA_TestUtils();
        initTestData(tu);

		Test.startTest();
		
		PRA_ClientTaskSearchService ctSrch = new PRA_ClientTaskSearchService();

		ctSrch.selectedProject = tu.clientProject.id;
		
		ctSrch.filterClosedUnits = true;

		List<Client_Task__c> ctl = ctSrch.getTasks();
		System.assertEquals(3, ctl.size());
		
		Datetime minusTwoMonths = Datetime.newInstance(DateTime.now().year(), DateTime.now().month()-2, 1);
		ctSrch.ueStartMonth = minusTwoMonths.format('yyyy-MM-dd');
		List<Unit_Effort__c> uel = ctSrch.getUnitEfforts();
		System.assertEquals(6, uel.size());
		
		List<Regional_Contract_Value__c> rcv = ctSrch.getRegionalContractValues();
		System.assertEquals(2, rcv.size());

		Test.stopTest();
	}
	
    static void initTestData(PRA_TestUtils testUtils) {
        Date curentDate = System.today();
        Datetime closeDate = (Datetime.newInstance(Date.today().year(), Date.today().month(), 1)).addMonths(-1);
        
        insert testUtils.getAccount('Acme Inc.');
        insert testUtils.getClientProject(testUtils.client.id, 'ELEELEXL-FEASXL', 'ELEELEXL-FEASXL', curentDate.addMonths(-6), curentDate.addMonths(6));
		insert testUtils.getRegion();
        insert testUtils.getWbsCompany();
        insert testUtils.getWbsProject();
        insert testUtils.getOperationalArea('Clinical Operations', 'CO');
        insert testUtils.getBusinessUnit('Road runner hunters', 'RRH');
        insert testUtils.getCountries(testUtils.businessUnit.id).values();
        insert testUtils.getProjectRegions().values();
        
        List<Task_Group__c> tgList = new List<Task_Group__c>();
        tgList.add(testUtils.getTaskGroup('Clin ops - startup', testUtils.operationalArea.id, 'TG0', '201', 'CO-START', 'Clin ops - startup'));
        tgList.add(testUtils.getTaskGroup('Clin Ops - Conduct', testUtils.operationalArea.id, 'TG1', '202', 'CO-CONDT', 'Clin Ops - Conduct'));
        tgList.add(testUtils.getTaskGroup('Project Management', testUtils.operationalArea.id, 'TG2', '203', 'PROJ MAN', 'Project Management'));
        tgList.add(testUtils.getTaskGroup('Interim Monitoring Visits', testUtils.operationalArea.id, 'TG3', '208', 'INTR-VIS', 'Interim Monitoring Visits'));
        insert tgList;
        
        List<Client_Task__c> ctList = new List<Client_Task__c>();       
        Client_Task__c ct;
        ct = testUtils.getClientTask(tgList[2].id, testUtils.clientProject.id, 'CT0', 'Project Management Start Up Phase',  curentDate.addMonths(-4), curentDate.addMonths(4),
                            testUtils.countries.get('USA').id, testUtils.projectRegions.get('NA').id);
        ct.status__c = PRA_Constants.CU_STATUS_SENT;
        ct.Combo_Code__c = '010';
        ct.Client_Unit_Number__c = '1a';
        ct.Contract_Value__c = 1000;
        ct.Close_Date__c = closeDate.date();
        ctList.add(ct);
                                    
        ct = testUtils.getClientTask(tgList[2].id, testUtils.clientProject.id, 'CT1', 'Project Management Financial Suport',  curentDate.addMonths(-4), curentDate.addMonths(4),
                            testUtils.countries.get('USA').id, testUtils.projectRegions.get('NA').id);
        ct.status__c = PRA_Constants.CU_STATUS_SENT;
        ct.Combo_Code__c = '20';
        ct.Client_Unit_Number__c = '1b';
        ct.Close_Date__c = closeDate.date();
        ctList.add(ct);
        
        ct = testUtils.getClientTask(tgList[0].id, testUtils.clientProject.id, 'CT2', 'Clinical Team Management',  curentDate.addMonths(-4), curentDate.addMonths(4),
                            testUtils.countries.get('USA').id, testUtils.projectRegions.get('NA').id);
        ct.status__c = PRA_Constants.CU_STATUS_SENT;
        ct.Combo_Code__c = '070';
        ct.Client_Unit_Number__c = '3a';
        ctList.add(ct);

        ct = testUtils.getClientTask(tgList[0].id, testUtils.clientProject.id, 'CT3', 'Site recruitment and qualification',  curentDate.addMonths(-4), curentDate.addMonths(4),
                            testUtils.countries.get('USA').id, testUtils.projectRegions.get('NA').id);
        ct.status__c = PRA_Constants.CU_STATUS_SENT;
        ct.Combo_Code__c = '080';
        ct.Client_Unit_Number__c = '3b';
        ctList.add(ct);
        
        ct = testUtils.getClientTask(tgList[0].id, testUtils.clientProject.id, 'CT4', 'Migrated Worked Hours',  curentDate.addMonths(-4), curentDate.addMonths(4),
                            testUtils.countries.get('USA').id, testUtils.projectRegions.get('NA').id);
        ct.status__c = PRA_Constants.CU_STATUS_SENT;
        ct.Combo_Code__c = 'Migrated Worked';
        ct.Client_Unit_Number__c = 'Migrated Worked Hours';
        ctList.add(ct);
        
        insert ctList;
        
        List<Client_Task__c> ctlSel = [SELECT id FROM Client_Task__c];
        testUtils.initUnitSetUp(ctlSel);
        testUtils.initCostSetUp(ctlSel);
        
        List<Regional_Contract_Value__c> rcvList = testUtils.getRegionalContractValues(ctlSel);
        system.debug('---- rcvList: ' + rcvList);
        insert rcvList;
        
        List<Regional_Contract_Value__c> temp = [SELECT ID FROM Regional_Contract_Value__c]; 

		system.debug('----- temp ' + temp.size());         
		
		List<Client_Task__c> temp2 = [SELECT Contract_Value__c FROM Client_Task__c];
		
		system.debug('----- temp2 ' + temp2);                                        
    }

}