/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_ViewResourceControllerExtUTest {

    static COUNTRY__c country;
    static list<WFM_Location_Hours__c> hrs;
    static Employee_Details__c empl;
    static list<WFM_Employee_Availability__c> avas;
    
    static list<WFM_EE_Therapeutic_Exp__c> theras;
    static list<WFM_EE_LANGUAGE__c> lang;
    static list<WFM_EE_Education__c> edu;
    static list<WFM_EE_CERTIFICATION__c> cer;
    static list<WFM_EE_SYSTEM_EXPERIENCE__c> sys;
    static list<WFM_EE_PHASE_EXPERIENCE__c> phase;
    static WFM_Client__c client;
    static WFM_Contract__c  contract;
    static WFM_Project__c project;
    static list<WFM_EE_Work_History__c> history;
    static List<WFM_Employee_Assignment__c> assigns;
    static WFM_employee_Allocations__c alloc;
    static WFM_EE_Work_History_Summary__c histSummary;
    
    static void init(){
        list<string> months = RM_Tools.GetMonthsList(-4, 5);
        country = new COUNTRY__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0);
        insert country;
        
        hrs = new List<WFM_Location_Hours__c>();
        for (string month:months){
            WFM_Location_Hours__c hr = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + month,YEAR_MONTH__c=month,NBR_Business_Day_In_Month__c=22);
            hrs.add(hr); 
        }
        insert hrs;  
        empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='1000', Employee_Unique_Key__c='1000', First_Name__c='John', Last_Name__c='Smith',email_Address__c='JSmith@gmail.com', RM_Comment__c='Test',
        Function_code__c='PR',Buf_Code__c='KCIPR', Business_Unit__c='BUC', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), FTE_Equivalent__c=1 );
        insert empl;
        avas = new list<WFM_Employee_Availability__c>();
      
        for (integer i = 0; i <months.size(); i++){
            WFM_Employee_Availability__c a = new WFM_Employee_Availability__c(EMPLOYEE_ID__c=empl.id, year_month__c=months[i], Availability_Unique_Key__c=empl.id+months[i],
            location_Hours__c=hrs[i].id);
            avas.add(a);
        }
        insert avas;
        //allocation
        
      
       
        theras = new WFM_EE_Therapeutic_Exp__c[]{
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Colorectal', employee_id__c=empl.id, Therapeutic_area__c='Oncology',Indication_Group__c='Solid Tumors',NBR_Total_Monitoring_Exp__c=4.29, NBR_PRA_Monitoring_Exp_Yrs__c=4.29),
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Colorectal', employee_id__c=empl.id, Therapeutic_area__c='Oncology',Indication_Group__c='Solid Tumors',Role__c='UNDEFINED', NBR_Role_Experience_Yrs__c=0.43), 
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Colorectal', employee_id__c=empl.id, Therapeutic_area__c='Oncology',Indication_Group__c='Solid Tumors',Study_Drug_Type__c='UNDEFINED', NBR_Study_Drug_Yrs__c=0.43),   
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Colorectal', employee_id__c=empl.id, Therapeutic_area__c='Oncology',Indication_Group__c='Solid Tumors',Subject_Population__c='UNDEFINED'),                                  
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Sedation', employee_id__c=empl.id, Therapeutic_area__c='Anesthesiology',Indication_Group__c='Anesthesia',Role__c='Clinical Team Manager', NBR_Role_Experience_Yrs__c=1.4),
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Sedation', employee_id__c=empl.id, Therapeutic_area__c='Anesthesiology',Indication_Group__c='Anesthesia',NBR_Total_Monitoring_Exp__c=4.29, NBR_PRA_Monitoring_Exp_Yrs__c=4.29),
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Surgical Anesthesia', employee_id__c=empl.id, Therapeutic_area__c='Anesthesiology',Indication_Group__c='Anesthesia',Role__c='UNDEFINED', NBR_Role_Experience_Yrs__c=1.92),
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Gout', employee_id__c=empl.id, Therapeutic_area__c='Endocrinology',Indication_Group__c='Metabolic',Role__c='Clinical Team Manager', NBR_Role_Experience_Yrs__c=0.15)
            
        
        };
        insert theras;
        Edu = new WFM_EE_Education__c[]{
            new WFM_EE_Education__c(Degree_Type__c='BA', employee_id__c=empl.id,  Institution__c='Instituation', Month_Obtained__c= Date.parse('4/1/2010'))       
        };
        insert Edu;
        lang = new WFM_EE_LANGUAGE__c[]{
            new WFM_EE_LANGUAGE__c(Language__c='English',employee_id__c=empl.id, Level_of_Med_Term__c='Low',   Level_of_Speak__c='High',  Level_of_Write__c='High')
        };
        insert lang;
        
        cer = new WFM_EE_CERTIFICATION__c[]{
            new WFM_EE_CERTIFICATION__c(Certification_Affilication_Name__c='Certification', employee_id__c=empl.id, Effective_Date__c = Date.parse('4/1/2010'),Granting_Institution__c='Granting')
        };
        insert cer;
        
       sys = new WFM_EE_SYSTEM_EXPERIENCE__c[]{
            new WFM_EE_SYSTEM_EXPERIENCE__c(CRS_System__c='Sys', employee_id__c=empl.id,Proficiency_Level__c = 1)
        };
        insert sys;
        
        phase = new WFM_EE_PHASE_EXPERIENCE__c[]{
            new WFM_EE_PHASE_EXPERIENCE__c(phase__c='IV', employee_id__c=empl.id,Phase_experience_yrs__c=3)
        };
        insert phase;
        client = new WFM_Client__c(name='TestClient', Client_Name__c='TestClinet_Name', Client_Unique_Key__c='TestClient');
        insert client;
        contract = new WFM_Contract__c(name='TestContract', Contract_Status__c='AA', Client_ID__c=client.id, Contract_Unique_Key__c='TestContract');
        insert contract;
        project = new WFM_Project__c(name='TestProject', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProject');
        insert project;
        alloc= new WFM_employee_Allocations__c(EMPLOYEE_ID__c=empl.id, Project_Buf_Code__c='KCIPR',Project_Country__c='United State', Project_ID__c=project.id, Project_Function__c='Clinical Research Associate', 
            Status__c='Confirmed', Allocation_Unique_Key__c=empl.name+project.name+'KCICRUnited State');
      
        insert alloc;
        //assignment
        assigns = new List<WFM_Employee_Assignment__c>();
        for (Integer i = 0; i < months.size(); i++){    
            WFM_Employee_Assignment__c ass =  new WFM_Employee_Assignment__c(Assignment_Unique_Key__c=avas[i].id+(string)alloc.id, EMPLOYEE_ID__c = empl.id, Availability_FK__c=avas[i].id, Allocation_Key__c=alloc.id, Year_Month__c=months[i], 
            Assigned_value__c=20, assigned_unit__c = 'Hours');
            
            assigns.add(ass);
        }
        insert assigns;
      
        // four month history
        list <string> monthHistry = RM_Tools.GetMonthsList(-4, -1);
        history = new list<WFM_EE_Work_History__c>();
    
        for (integer i = 0; i < monthHistry.size(); i++){
            WFM_EE_Work_History__c h = new WFM_EE_Work_History__c(Project_ID__c=project.id, employee_id__c=empl.id, Availability__c = avas[i].id, Work_History_Unique_Key__c=empl.id+(string)project.id+monthHistry[i]+'KCIPR',Buf_Code__c='KCIPR',year_month__c=monthHistry[i], Hours__c=20);
            history.add(h);
        }
        insert history;
        //history summary
        histSummary = new WFM_EE_Work_History_Summary__c(Project__c=project.id, employee_id__c=empl.id, Buf_Code__c='KCIPR', Start_Year_Month__c='07/2011', End_Year_Month__c='07/2012', Sum_Worked_Hours__c=200, Work_History_Summary_Unique_Key__c=empl.id+(string)project.id+'KCIPR');
        insert histSummary;
    }
        
      static testMethod void TestTherapeutic() {
        init();
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(empl);
        RM_ViewResourceControllerExt detailExt = new RM_ViewResourceControllerExt(sc);   
        List<WFM_EE_Therapeutic_Exp__c> th = detailExt.getTherapeuticExp();
        system.assert(th[0].role__c == 'UNDEFINED');
        system.assert(th[1].NBR_Total_Monitoring_Exp__c==4.29);
        Test.stopTest();
        
      }
      static testMethod void TestOthers() {
        init(); 
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(empl);
        RM_ViewResourceControllerExt detailExt = new RM_ViewResourceControllerExt(sc);   
        List<WFM_EE_Education__c> th = detailExt.getEducation();
        system.assert(th[0].Degree_Type__c == 'BA');
       
        List<WFM_EE_LANGUAGE__c> l = detailExt.getLanguage();
        system.assert(l[0].Language__c=='English');
        
        
        List<WFM_EE_CERTIFICATION__c> c = detailExt.getCertification();
        system.assert(c[0].Certification_Affilication_Name__c=='Certification');
        
        List<WFM_EE_SYSTEM_EXPERIENCE__c> s = detailExt.getSysExp();
        system.assert(S[0].CRS_System__c=='Sys');
        
        List<WFM_EE_PHASE_EXPERIENCE__c> p = detailExt.getPhaseExp();
        system.assert(p[0].Phase__c=='IV');
        Test.stopTest();
      }
      
     
       
      
      /* static testMethod void TestExportAssignment(){
      	init();
       
        Test.startTest();
        
        PageReference pageRef = new PageReference('/apex/RM_ExcelExportAssignments?Hr=false');
     
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.StandardController(empl);
        RM_ViewResourceControllerExt detailExt = new RM_ViewResourceControllerExt(sc);   
       
        List<RM_ViewAssignmentsCompController.SixMonthAssignment> assignments = detailExt.getAssignments();
        system.debug('---assignments---' + assignments);
        system.assert(assignments.size() > 0);
       
        Test.stopTest();
      
      }*/
}