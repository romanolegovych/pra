public class IPA_DepContactsComponentController
{
    public IPA_Page_Widget__c pageWidgetObj {get; set;}
    public List<IPA_Articles__c> getLstDepContacts() 
    {
        List<IPA_Articles__c> formattedArticlesList = new List<IPA_Articles__c>();
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        if(pageWidgetObj != null){
            formattedArticlesList = ipa_bo.returnDepartmentContacts(pageWidgetObj).clone();
            /*for(IPA_Articles__c articleObj : formattedArticlesList){
            	if(articleObj.Email__c != null && articleObj.Email__c.length()>15){
            		articleObj.Email__c = articleObj.Email__c.replace('@','<br>@');
            	}
            }*/
            return formattedArticlesList;
        }else{
            return formattedArticlesList;
        }
    }
}