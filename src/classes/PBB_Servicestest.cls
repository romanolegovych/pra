/**
@author Bhargav Devaram
@date 2015
@description this is PBB_Services's test class
**/
@isTest   
private class PBB_Servicestest {
    static testMethod void testsavePBBSelectedCountries()    {
        test.startTest();   
        
            PBB_TestUtils tu = new PBB_TestUtils ();  
            tu.wfmp = tu.createwfmproject();
            tu.bid =tu.createbidproject();
            tu.sm = tu.createSRMModelAttributes();
            //tu.srmScen = tu.createSRMScenario();
            tu.scen =tu.createscenarioAttributes();
            tu.country =tu.createCountryAttributes();
            //tu.psc =tu.createPBBScenarioCountry();   
            
            tu.smm = tu.createServiceModelAttributes();
            tu.sa = tu.createServiceAreaAttributes();
            tu.sf = tu.createServiceFunctionAttributes();
            tu.St = tu.createServiceTaskAttributes();
            
            tu.siqn = tu.createSIQNAttributes();  
            tu.siqs = tu.createSIQSAttributes('Project');  
            tu.sir = tu.createSIRAttributes(); 
            tu.STtoSI = tu.createSTtoSI(tu.st,tu.siqn); 
            tu.siqs.status__c ='Approved';            
            update tu.siqs;
            
            tu.billFormulaName = tu.createBillFormulaNames(); 
            tu.billFormula = tu.createBillFormula(); 
            tu.STtoBF = tu.createSTtoBF(tu.st,tu.billFormulaName ); 
            tu.smm = tu.approveServicemodel(tu.smm);
            
            tu.Wrm = tu.createWRModelAttributes();
            tu.scen.WR_Model__c  = tu.Wrm.id;
            update tu.scen;
            
        PBB_Services pbbs = new PBB_Services();
        Map<String,String> TaskIdsMap = new Map<String,String>();
        TaskIdsMap.put(tu.st.id,'PRA');  
           
        //insert   
        PBB_Services.saveServicetaskforSelectedCountry(tu.scen.id,PBB_Constants.PROJECT_LEVEL,TaskIdsMap);        
        
        Set<String> oldSelectedSet = new Set<String>();
        List<SelectOption> selectedCountriesList = new List<SelectOption>();
        selectedCountriesList.add(new SelectOption(tu.country.name,tu.country.name));
        PBB_Services.savePBBSelectedCountries(selectedCountriesList,oldSelectedSet,tu.scen.id,tu.country.name);
        
        //upsert
        PBB_Services.saveServicetaskforSelectedCountry(tu.scen.id,tu.country.name,TaskIdsMap); 
        PBB_Services.savePBBSelectedCountries(selectedCountriesList,oldSelectedSet,tu.scen.id,PBB_Constants.PROJECT_LEVEL);   
        System.assertEquals(PBB_DataAccessor.GetSelectedServiceTask(tu.scen.id,tu.country.id).size(),0);
       
        Map<String,PBB_Services.ServiceAreaWrapper> SAWrapper = new Map<String,PBB_Services.ServiceAreaWrapper>();        
        SAWrapper = PBB_Services.getServicetaskforSelectedCountry(tu.scen.id,tu.country.name);
        System.assertEquals(SAWrapper.size(),1);
        System.assertEquals(PBB_DataAccessor.GetSelectedCountries(tu.scen.id).size(),2);
        //tu.scecont=tu.createSrmCountryAttributes();  
        //PBB_Services.createPBBWeeklyEvents(tu.scen);
        PBB_Scenario__c pbbsclone = new PBB_Scenario__c ();
        pbbsclone = PBB_Services.clone(tu.scen.id);
        PBB_Services.showErrorMessage('errorMessage');
        
        Set<String> SRMCSet = new Set<String>();
        //SRMCSet.add(tu.scecont.id);
        //PBB_Services.addPreviousWeeklyEvents(SRMCSet);
        
        Service_Model__c smNew = new Service_Model__c();
        smNew = tu.smm.clone(false, true);
        smNew.Name = 'Test Method 684654';  
        smNew.Status__c = 'In-Process';
        smNew.Description__c = ' This is a test method';
        upsert smNew;         
        PBB_Services.cloneServiceModelChildRecords(tu.smm.id, smNew.id);
        test.stopTest(); 
    }     
}