/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
@isTest
private class PBB_SiteandSubjectGraphsCntrlTest {  
     static testMethod void constructorTest() {
         PBB_TestUtils tu = new PBB_TestUtils();
         WFM_Project__c wf = tu.createWfmProject();
         SRM_Model__c smm = tu.createSRMModelAttributes();
         Bid_Project__c b = tu.createBidProject();
         Country__c  c = tu.createCountryAttributes();
         PBB_Scenario__c pb = tu.createScenarioAttributes();
         pb.protocol_name__c = 'Protocol1';
         upsert pb;
         wfm_protocol__c protocol = new WFM_Protocol__c();
         protocol.name = 'Protocol1';
         protocol.Protocal_Unique_Key__c = '23';
         protocol.Project_ID__c = wf.id;
         insert protocol;
         
         
         
         ApexPages.currentPage().getParameters().put('ProtocolUniqueKey','23');
         PBB_SiteandSubjectGraphsCntrl cls = new PBB_SiteandSubjectGraphsCntrl();
     }
}