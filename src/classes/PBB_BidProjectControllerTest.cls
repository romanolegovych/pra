/**
@author Ramya
@date 2015
@description this is PBB_BidProjectController test class
**/

@isTest
private class PBB_BidProjectControllerTest {
    
    static testMethod void testPBB_BidProjectController(){
        
        test.startTest();       
        PBB_TestUtils tu = new PBB_TestUtils ();
        
        //Prepare the test data
        tu.prabu =  tu.createPRABusinessUnits();
        tu.wfmp = tu.createWfmProject();        
        tu.bid =tu.createBidProject();        
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(tu.bid);
        
        //set the pbb bid project id.  
        ApexPages.currentPage().getParameters().put('Id',tu.bid.id);
        PBB_BidProjectController BPC= new PBB_BidProjectController(new Apexpages.Standardcontroller(tu.bid));
        BPC.getBusinessSegmentTypes();
        BPC.SelectBSs = 'Product Registration';        
        BPC.saveBP();
        System.assertEquals(BPC.bp.id, tu.bid.id);
        System.assertEquals(BPC.bp.Business_Segment__c, 'Product Registration');
        test.stopTest();
    }
}