public with sharing class IPA_HeroGraphicComponentController
{
    public IPA_Articles__c content {get; set;}
    public IPA_Page_Widget__c pageWidgetObj {get; set;}
    
    public void getFetchContent()
    {
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        content = ipa_bo.returnHeroGraphicContent(pageWidgetObj);
    }
}