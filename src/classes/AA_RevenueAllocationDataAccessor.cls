public with sharing class AA_RevenueAllocationDataAccessor{
    
    public static List<Business_Unit__c> getBuCodes(){
         return [SELECT  Id,Name,Description__c FROM Business_Unit__c order by Name];
    }
    
    public static List<Function_Code__c> getFCodes(){
         return [SELECT  Id,Name,Description__c FROM Function_Code__c order by Name];
    }
    public static List<AggregateResult> getValidYears(){
        return [SELECT Year__c FROM Revenue_Allocation_Rate__c GROUP BY Year__c ORDER BY Year__c];
    }
    
    public static List<Country__c> getCountries(){
         return [SELECT  Id,Name FROM Country__c order by Name];
    }
    
    public static BUF_Code__c getBufCodeDetails(String val){
        return [SELECT  Name FROM BUF_Code__c where Name =: val];
    }
    
    public static List<BUF_Code__c> getBufCodeName(String val){
        return [SELECT  Name FROM BUF_Code__c where Name=: val];    
    }
    
    public static void insertBufCode(BUF_Code__c bufCode){
        insert bufCode;
    }
    
    public static BUF_Code__c getBufCodeId(String name){
        return [SELECT Id from BUF_Code__c WHERE Name=:name];
    }
    
    public static Currency__c getCurrencyId(String name){
        return [SELECT Id from Currency__c WHERE Name=:name];
    }
    public static List<Revenue_Allocation_Rate__c> getRateDetails(String Id){
        return [SELECT Id,CreatedBy.Name,CreatedDate,LastModifiedBy.Name,LastModifiedDate,Function__c,Year__c,Rate__c,Currency__c,IsActive__c  from Revenue_Allocation_Rate__c WHERE Id=:Id];
    }
    public static Revenue_Allocation_Rate__c checkExistingRecord(BUF_Code__c function, String year){
        return [SELECT Id,Rate__c from Revenue_Allocation_Rate__c WHERE Function__c=:function.Id AND Year__c=:year];
    }
    public static List<Revenue_Allocation_Rate__c> checkExisting(String function, String year){
        return [SELECT Id,Rate__c from Revenue_Allocation_Rate__c WHERE Function__c=:function AND Year__c=:year];
    }
    public static Revenue_Allocation_Rate__c getRARId(){
        return [SELECT Id from Revenue_Allocation_Rate__c LIMIT 1];
    }
    
    public static List<Revenue_Allocation_Rate__c> getRecordsByFilter(String year,String bucode,String functioncode,String country,String active){
         String query = String.format('SELECT Id,Function__c,Year__c,Currency__c,Rate__c,CreatedBy.Name,CreatedDate,LastModifiedBy.Name,LastModifiedDate,IsActive__c from Revenue_Allocation_Rate__c WHERE Year__c = \'\'{0}\'\' ', new String[]{year});    
         if(bucode !=null)
             query = query + String.format(' AND Function__r.Business_Unit__c=\'\'{0}\'\' ', new String[]{bucode});  
         if(functioncode !=null)
             query = query + String.format(' AND Function__r.Function_Code__c=\'\'{0}\'\' ', new String[]{functioncode});  
         if(country !=null)
             query = query + String.format(' AND Revenue_Allocation_Rate__c.Function__r.Business_Unit__r.Default_Country__c=\'\'{0}\'\' ', new String[]{country});
         if(active !='A'){
             if(active == 'Y')
                 query = query + ' AND Revenue_Allocation_Rate__c.IsActive__c= true ';
             else
                 query = query + ' AND Revenue_Allocation_Rate__c.IsActive__c = false '; 
         }
         query = query + 'ORDER BY Function__r.Name';
         return Database.query(query);
      }
      
    public static List<Revenue_Allocation_Rate__c> runQuery(String query){
       return  Database.query(query);
    }
    }