/**
* @author Sukrut Wagh
* @date 06/24/2014
* @description This class is PRA's implementation of ITriggerDispatcher. 
*			   It dynamically searches & instantiates the trigger handler implemented by different projects.
*/
public class COM_TriggerDispatcherBase implements ITriggerDispatcher { 
	
	private static final COM_Logger LOGGER = new COM_Logger('COM_TriggerDispatcherBase');
	
	/*	Lists are used to maintain the order of execution of the trigger event handlers	*/
	private List<String> handlerPrefix;
	protected Schema.sObjectType soType;
	protected String soName;
	
	private List<COM_TriggerHandlerMeta> beforeInsertHandlers = new List<COM_TriggerHandlerMeta>();
	private List<COM_TriggerHandlerMeta> beforeUpdateHandlers = new List<COM_TriggerHandlerMeta>();
	private List<COM_TriggerHandlerMeta> beforeDeleteHandlers = new List<COM_TriggerHandlerMeta>();
	private List<COM_TriggerHandlerMeta> afterInsertHandlers = new List<COM_TriggerHandlerMeta>();
	private List<COM_TriggerHandlerMeta> afterUpdateHandlers = new List<COM_TriggerHandlerMeta>();
	private List<COM_TriggerHandlerMeta> afterDeleteHandlers = new List<COM_TriggerHandlerMeta>();
	private List<COM_TriggerHandlerMeta> afterUndeleteHandlers = new List<COM_TriggerHandlerMeta>();
	
	public virtual void setObjectType(Schema.sObjectType soType){
		this.soType = soType;
		this.soName = COM_Utils.getObjectName(soType);
	}
	
	public void setHandlerPrefix(List<String> handlerPrefix)
	{
		this.handlerPrefix = handlerPrefix;
		setHandlers(this.soName,this.handlerPrefix);
	}
	
	/**
	* @author Sukrut Wagh
	* @date 06/24/2014
	* @description Derives the handler class names, initializes them & sets the handler properties
	* @param	soName - sObject Name	
	* @param	handlerPrefix - list of handler class name prefixes used to derive the handler classes
	*/
	private void setHandlers(final String soName, final List<String> handlerPrefix) {
		if(COM_Utils.isNotEmpty(soName) && COM_Utils.isNotEmpty(handlerPrefix)) {
			ITriggerHandler handler = null;
			COM_TriggerHandlerMeta handlerMeta = null;
			for(String prefix : handlerPrefix) {
				handler = getTriggerHandler(prefix,soName,TriggerParameters.TriggerEvent.BeforeInsert);
				if(null != handler) {
					handlerMeta = new COM_TriggerHandlerMeta();
					handlerMeta.handler = handler;
					beforeInsertHandlers.add(handlerMeta);
				}
				
				handler = getTriggerHandler(prefix,soName,TriggerParameters.TriggerEvent.BeforeUpdate);
				if(null != handler) {
					handlerMeta = new COM_TriggerHandlerMeta();
					handlerMeta.handler = handler;
					beforeUpdateHandlers.add(handlerMeta);
				}
				
				handler = getTriggerHandler(prefix,soName,TriggerParameters.TriggerEvent.BeforeDelete);
				if(null != handler) {
					handlerMeta = new COM_TriggerHandlerMeta();
					handlerMeta.handler = handler;
					beforeDeleteHandlers.add(handlerMeta);
				}
				
				handler = getTriggerHandler(prefix,soName,TriggerParameters.TriggerEvent.AfterInsert);
				if(null != handler) {
					handlerMeta = new COM_TriggerHandlerMeta();
					handlerMeta.handler = handler;
					afterInsertHandlers.add(handlerMeta);
				}
				
				handler = getTriggerHandler(prefix,soName,TriggerParameters.TriggerEvent.AfterUpdate);
				if(null != handler) {
					handlerMeta = new COM_TriggerHandlerMeta();
					handlerMeta.handler = handler;
					afterUpdateHandlers.add(handlerMeta);
				}
				
				handler = getTriggerHandler(prefix,soName,TriggerParameters.TriggerEvent.AfterDelete);
				if(null != handler) {
					handlerMeta = new COM_TriggerHandlerMeta();
					handlerMeta.handler = handler;
					afterDeleteHandlers.add(handlerMeta);
				}
				
				handler = getTriggerHandler(prefix,soName,TriggerParameters.TriggerEvent.AfterUndelete);
				if(null != handler) {
					handlerMeta = new COM_TriggerHandlerMeta();
					handlerMeta.handler = handler;
					afterUndeleteHandlers.add(handlerMeta);
				}
			}
		}
	}
	
	private static ITriggerHandler getTriggerHandler(final String prefix, final String soName, final TriggerParameters.TriggerEvent event) {
		String className = getHandlerClassName(prefix,soName,event);
		Type obType = Type.forName(className);
		ITriggerHandler handler = (obType == null) ? null : (ITriggerHandler)obType.newInstance();
		if(null != handler) {
			LOGGER.info('TriggerHandler found for className:'+className);
		} else {
			LOGGER.debug('TriggerHandler not found for className:'+className);
		}
		
		return handler;
	}
	
	private static String getHandlerClassName(final String prefix, final String soName, final TriggerParameters.TriggerEvent event) {
		String className = null;
		if(COM_Utils.isNotEmpty(prefix) && COM_Utils.isNotEmpty(soName) && COM_Utils.isNotEmpty(event)) {
			className = prefix+'_'+soName+event.name()+'Handler';
		}
		return className;
	}
	
    /** 
	* @author Hari Krishnan
	* @date 07/16/2013
	* @description This method is called prior to execution of a before trigger event. If you want 
	*				to load any lookup data or cache the data, then this is the place that you need 
	*				to put your code. 
	*/
    public virtual void bulkBefore() {}

    /** 
	* @author Hari Krishnan
	* @date 07/16/2013
	* @description This method is called prior to execution of an after trigger event. 
	*/
    public virtual void bulkAfter() {}

    /** 
	* @author Hari Krishnan
	* @date 07/16/2013
	* @description This method is called for records to be inserted during a BEFORE trigger.
	*/
    public void beforeInsert(TriggerParameters tp) {
    	if(COM_Utils.isNotEmpty(beforeInsertHandlers)) {
    		execute(beforeInsertHandlers,tp);
    	}
    }

    /** 
	* @author Hari Krishnan
	* @date 07/16/2013
	* @description This method is called for records to be updated during a BEFORE trigger.
	*/
    public void beforeUpdate(TriggerParameters tp) {
    	if(COM_Utils.isNotEmpty(beforeUpdateHandlers)) {
    		execute(beforeUpdateHandlers,tp);
    	}
    }

    /** 
	* @author Hari Krishnan
	* @date 07/16/2013
	* @description This method is called for records to be deleted during a BEFORE trigger.
	*/
    public void beforeDelete(TriggerParameters tp) {
    	if(COM_Utils.isNotEmpty(beforeDeleteHandlers)) {
    		execute(beforeDeleteHandlers,tp);
    	}
    }

    /** 
	* @author Hari Krishnan
	* @date 07/16/2013
	* @description This method is called for records inserted during an AFTER trigger. Always put field validation 
	*				in the 'After' methods in case another trigger has modified any values. The record is 'read only' 
	*				at this point.
	*/
    public void afterInsert(TriggerParameters tp) {
    	if(COM_Utils.isNotEmpty(afterInsertHandlers)) {
    		execute(afterInsertHandlers,tp);
    	}
    } 

    /** 
	* @author Hari Krishnan
	* @date 07/16/2013
	* @description This method is called iteratively for each record updated during an AFTER trigger.
	*/
    public void afterUpdate(TriggerParameters tp) {
    	if(COM_Utils.isNotEmpty(afterUpdateHandlers)) {
    		execute(afterUpdateHandlers,tp);
    	}
    }

    /** 
	* @author Hari Krishnan
	* @date 07/16/2013
	* @description This method is called iteratively for each record deleted during an AFTER trigger.
	*/
    public void afterDelete(TriggerParameters tp) {
    	if(COM_Utils.isNotEmpty(afterDeleteHandlers)) {
    		execute(afterDeleteHandlers,tp);
    	}
    }
    
    /** 
	* @author Hari Krishnan
	* @date 07/16/2013
	* @description This method is called prior to execution of a AFTER UNDELETE trigger. 
	*/
    public void afterUnDelete(TriggerParameters tp) {
    	if(COM_Utils.isNotEmpty(afterUndeleteHandlers)) {
    		execute(afterUndeleteHandlers,tp);
    	}
    }
    
    public virtual void andFinally() {}
    
   /** 
	* @author Sukrut Wagh
	* @date 06/24/2014
	* @description This is a dispatcher lifecycle method called from the different dispatcher event handler methods .
	*			   It in turn invokes the lifecycle methods (mainEntry,updateObjects) on the trigger handlers
	* @param handlers list of trigger handlers in the order of execution
	* @param tp trigger parameters passed from the trigger context		   
	*/
   	public void execute(final List<COM_TriggerHandlerMeta> handlers, final TriggerParameters tp) {
   		if(COM_Utils.isNotEmpty(handlers) && COM_Utils.isNotEmpty(tp)) {
   			for(COM_TriggerHandlerMeta meta : handlers) {
   				if(null != meta) {
   					if(null == meta.isProcessing || !meta.isProcessing) {
						meta.isProcessing = true;
						meta.handler.mainEntry(tp);
    					meta.handler.updateObjects();
						meta.isProcessing = false;
					} else {
						meta.handler.inProgressEntry(tp);
   					}
   				}
   			}
   		}
    }
}