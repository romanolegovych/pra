/**
 * Created by illia on 12/4/15.
 */

public with sharing class EWCategoryTrigger extends STSWR1.AbstractTrigger
{
	public override void beforeInsert(List<SObject> records)
	{
		updateBufferIndex((List<EW_Category__c>) records);
	}

	public override void afterInsert(List<SObject> records)
	{
		updateEWCategoryJunctions((List<EW_Category__c>) records);
		new EWCategoryHelper().associateCategories((List<EW_Category__c>) records);
	}

	public override void beforeUpdate(List<SObject> records, List<SObject> priors)
	{
		updateBufferIndex((List<EW_Category__c>) records);
	}

	public override void afterUpdate(List<SObject> records, List<SObject> priors)
	{
		updateEWCategoryJunctions((List<EW_Category__c>) records);
		updateSites((List<EW_Category__c>) records);
		new EWCategoryHelper().associateCategories((List<EW_Category__c>) records);
	}

	public static final Map<String, Schema.SObjectField> STRATEGY_TO_SOURCE_FIELD_MAP = new Map<String, Schema.SObjectField>{
		'Min' => EW_Category__c.Min_Buffer_Index__c,
		'Max' => EW_Category__c.Max_Buffer_Index__c
	};

	public static final Schema.SObjectField DEFAULT_BUFFER_INDEX_SOURCE_FIELD = EW_Category__c.Min_Buffer_Index__c;

	private void updateBufferIndex(List<EW_Category__c> categories)
	{
		for (EW_Category__c categoryRecord : categories)
		{
			Schema.SObjectField sourceField = chooseBISourceField(categoryRecord);
			categoryRecord.put(EW_Category__c.Buffer_Index__c, categoryRecord.get(sourceField));
		}
	}

	private Schema.SObjectField chooseBISourceField(EW_Category__c categoryRecord)
	{
		return STRATEGY_TO_SOURCE_FIELD_MAP.get(categoryRecord.Buffer_Index_Strategy__c) != null
				? STRATEGY_TO_SOURCE_FIELD_MAP.get(categoryRecord.Buffer_Index_Strategy__c)
				: DEFAULT_BUFFER_INDEX_SOURCE_FIELD;
	}

	/**
    * Updates Buffer_Index__c value in all associated EW_Category_EW_Junction__c records
     */
	private void updateEWCategoryJunctions(List<EW_Category__c> categories)
	{
		update [select Id from EW_Category_EW_Junction__c where Child_Category__c in :categories];

//		Map<ID, EW_Category__c> categoryMap = new Map<ID, EW_Category__c>(categories);
//		List<EW_Category_EW_Junction__c> categoryJunctions = [select Id, Buffer_Index__c, Child_Category__c from EW_Category_EW_Junction__c where Child_Category__c in :categories];
//		for (EW_Category_EW_Junction__c categoryJunction : categoryJunctions)
//		{
//			EW_Category__c categoryRecord = categoryMap.get(categoryJunction.Child_Category__c);
//			categoryJunction.Buffer_Index__c = categoryRecord.Buffer_Index__c;
//		}
//
//		update categoryJunctions;
	}

	public static final String ESSENTIAL_DOCUMENTS_CATEGORY_SETTING_NAME = 'Essential Documents Category Name';
	public static final String ESSENTIAL_DOCUMENTS_CATEGORY_NAME = PAWS_Settings.shared.read(ESSENTIAL_DOCUMENTS_CATEGORY_SETTING_NAME);
	public static final String ESSENTIAL_DOCUMENTS_EXPECTED_DATE_FIELD_NAME = PAWS_Settings.shared.read('Essential Documents Expected Date Field');

	public static final List<String> SITE_ACTIVATION_CATEGORIES = new List<String>{
			'Essential Documents', 'Start Up Requirements', 'Site Approvals', 'Site Contracts'
	};

	public static final String CATEGORY_SETTINGS_PREFIX = ' Category Name';

	public static final Map<String, String> SITE_ACTIVATION_FIELD_NAMES_TO_SOURCE_FIELD_MAP = new Map<String, String>{
			' Expected Date' => 'Expected Date',
			' Target Date' => 'Target Date',
			' Expected Date BI 0' => 'Expected Date BI 0',
			' Expected Date BI 0.5' => 'Expected Date BI 0.5',
			' Expected Date BI 1' => 'Expected Date BI 1',
			' Completed Date' => 'Completed Date'
	};

	public static final String CATEGORY_SOURCE_FIELD_SETTING_SUFFIX = ' Category Field Name';
	public static final String SITE_FIELD_EW_TARGET_FIELD_SUFFIX = ' Field Name';

	private void updateSites(List<EW_Category__c> categories)
	{
		List<EW_Category__c> extendedCategories = selectExtendedCategories(categories);
		Map<ID, PAWS_Project_Flow_Site__c> affectedSites = new Map<ID, PAWS_Project_Flow_Site__c>();
		for (EW_Category__c extendedCategory : extendedCategories)
		{
			affectedSites.putAll(updateSiteEWData(extendedCategory));
		}

		update affectedSites.values();
	}

	private List<EW_Category__c> selectExtendedCategories(List<EW_Category__c> categories)
	{
		String query = 'select ' + String.join(new List<String>(SObjectType.EW_Category__c.Fields.getMap().keyset()), ', ')
				+ ', (select Early_Warning__r.PAWS_Project_Flow_Site__c, Early_Warning__r.PAWS_Project_Flow_Site__r.' + String.join(new List<String>(SObjectType.PAWS_Project_Flow_Site__c.Fields.getMap().keyset()), ', Early_Warning__r.PAWS_Project_Flow_Site__r.')
				+ ' from Early_Warnings__r where Early_Warning__r.PAWS_Project_Flow_Site__c != null)'
				+ ' from EW_Category__c where Id in :categories';

		return Database.query(query);
	}

	private Map<ID, PAWS_Project_Flow_Site__c> updateSiteEWData(EW_Category__c categoryRecord)
	{
		Map<ID, PAWS_Project_Flow_Site__c> processedSites = new Map<ID, PAWS_Project_Flow_Site__c>();
		for (String category : SITE_ACTIVATION_CATEGORIES)
		{
			String categoryName = PAWS_Settings.shared.read(category + CATEGORY_SETTINGS_PREFIX);
			System.debug(System.LoggingLevel.ERROR, 'Category Name [Settings: ' + categoryName + '], [Actual: ' + categoryRecord.Name + ']');//@TODO: Delete this line!
			if (categoryRecord.Name != categoryName)
			{
				continue;
			}


			for (EW_Category_EW_Junction__c ewJunction : categoryRecord.Early_Warnings__r)
			{
				if (ewJunction.Early_Warning__r.PAWS_Project_Flow_Site__c == null
						|| processedSites.containsKey(ewJunction.Early_Warning__r.PAWS_Project_Flow_Site__c))
				{
					continue;
				}

				PAWS_Project_Flow_Site__c pawsSite = ewJunction.Early_Warning__r.PAWS_Project_Flow_Site__r;
				processedSites.put(pawsSite.Id, pawsSite);

				for (String key : SITE_ACTIVATION_FIELD_NAMES_TO_SOURCE_FIELD_MAP.keyset())
				{
					String targetField = PAWS_Settings.shared.read(category + key + SITE_FIELD_EW_TARGET_FIELD_SUFFIX);
					String sourceField = PAWS_Settings.shared.read(SITE_ACTIVATION_FIELD_NAMES_TO_SOURCE_FIELD_MAP.get(key) + CATEGORY_SOURCE_FIELD_SETTING_SUFFIX);

					System.debug(System.LoggingLevel.ERROR, 'Source Field: ' + sourceField + ', Target Field: ' + targetField);//@TODO: Delete this line!
					if (targetField == null || sourceField == null)
					{
						continue;
					}

					try
					{
						pawsSite.put(targetField, categoryRecord.get(sourceField));
					}
					catch (Exception e)
					{
						//do nothing
					}
				}

			}
		}

		return processedSites;
	}
}