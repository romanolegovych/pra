@isTest
private class ServiceTaskTest {
	
	private static final String TEST_VALUE = 'TEST_VALUE';
		
	private static ServiceTask st;
	
	private static List<Service_Task__c> serviceTasks;
	private static PBB_TestUtils testUtils = new PBB_TestUtils();
	
	static{
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		testUtils.sa = testUtils.CreateServiceAreaAttributes();
		testUtils.sf = testUtils.CreateServiceFunctionAttributes();
	}
	
	@isTest
	private static void testConstructor(){
		Test.startTest();
			st = new ServiceTask(testUtils.sf.Id);
		Test.stopTest();
		System.assertEquals( testUtils.sf.Id, st.parentServiceId );
		System.assertEquals( testUtils.sf.Id, st.previousServiceId );
	}
	
	@isTest
	private static void testProperties(){
		st = new ServiceTask(testUtils.sf.Id);
		ServiceTask.ServiceTaskTab currentTab;
		Test.startTest();
			st.serviceTasks = new List<Service_Task__c>{ new Service_Task__c() };
			st.serviceTask = new Service_Task__c();
			st.serviceTaskForUpdate = new Service_Task__c();
			currentTab = st.currentTab;
		Test.stopTest();
		System.assertEquals( ServiceTask.TASK_WORD, st.getServiceWord() );
		System.assertEquals( 1, st.serviceTasks.size() );
		System.assertNotEquals( null, st.serviceTask );
		System.assertNotEquals( null, st.serviceTaskForUpdate );
		System.assertNotEquals(null, currentTab);
	}
	
	@isTest
	private static void refreshServicesTest(){
		st = new ServiceTask(testUtils.sf.Id);
		testUtils.CreateServiceTaskAttributes();
		Test.startTest();
			st.refreshServices();
		Test.stopTest();
		System.assertEquals( 1, st.serviceTasks.size() );
	}
	
	@isTest
	private static void preparationCreateOrEditServiceTestCreateNew(){
		st = new ServiceTask(testUtils.sf.Id);
		Test.setCurrentPage( Page.LayoutModel );
		Test.startTest();
			st.preparationCreateOrEditService();
		Test.stopTest();
		System.assertEquals( null, st.serviceTaskForUpdate.Id );
		System.assertEquals( testUtils.sf.Id, st.serviceTaskForUpdate.Service_Function__c );
		System.assertEquals( 0, st.selectedBusinessSegmentTypes.size() );
	}
	
	@isTest
	private static void preparationCreateOrEditServiceTestUpdate(){
		st = new ServiceTask(testUtils.sf.Id);
		st.serviceTask = testUtils.CreateServiceTaskAttributes();
		st.refreshServices();
		Test.setCurrentPage( Page.LayoutModel );
		ApexPages.currentPage().getParameters().put( 'editServiceTaskId', st.serviceTask.Id );
		Test.startTest();
			st.preparationCreateOrEditService();
		Test.stopTest();
		System.assertEquals( st.serviceTask.Id, st.serviceTaskForUpdate.Id );
	}

	@isTest
	private static void createService(){
		st = new ServiceTask(testUtils.sf.Id);
		st.serviceTask = testUtils.CreateServiceTaskAttributes();
		st.refreshServices();
		Test.setCurrentPage( Page.LayoutModel );
		ApexPages.currentPage().getParameters().put( 'editServiceTaskId', st.serviceTask.Id );
		st.preparationCreateOrEditService();
		Test.startTest();
			st.createService();
		Test.stopTest();
		System.assertNotEquals( st.serviceTaskForUpdate.Business_Segment__c, null);
	}

	@isTest
	private static void createServiceNameNull(){
		st = new ServiceTask(testUtils.sf.Id);
		st.serviceTask = testUtils.CreateServiceTaskAttributes();
		st.refreshServices();
		Test.setCurrentPage( Page.LayoutModel );
		ApexPages.currentPage().getParameters().put( 'editServiceTaskId', st.serviceTask.Id );
		st.preparationCreateOrEditService();
		st.serviceTaskForUpdate.Name = null;
		Test.startTest();
			st.createService();
		Test.stopTest();
		System.assertEquals( st.isValid, false);
	}

	@isTest
	private static void createServiceBusinessSegmentNull(){
		st = new ServiceTask(testUtils.sf.Id);
		st.serviceTask = testUtils.CreateServiceTaskAttributes();
		st.refreshServices();
		Test.setCurrentPage( Page.LayoutModel );
		ApexPages.currentPage().getParameters().put( 'editServiceTaskId', st.serviceTask.Id );
		st.preparationCreateOrEditService();
		st.serviceTaskForUpdate.Name = 'new Name';
		st.serviceTaskForUpdate.Business_Segment__c = null;
		Test.startTest();
			st.createService();
		Test.stopTest();
		//System.assertEquals(st.isValid, false);
	}

	@isTest
	private static void getBusinessSegmentTypes(){
		List<selectOption> businessSegmentTypes;
		st = new ServiceTask(testUtils.sf.Id);
		st.serviceTask = testUtils.CreateServiceTaskAttributes();
		st.refreshServices();
		Test.setCurrentPage( Page.LayoutModel );
		ApexPages.currentPage().getParameters().put( 'editServiceTaskId', st.serviceTask.Id );
		Test.startTest();
			businessSegmentTypes = st.getBusinessSegmentTypes();
		Test.stopTest();
		System.assert(businessSegmentTypes.size() == 0);
	}
	
	@isTest
	private static void preparationCreateOrEditServiceTestClone(){
		st = new ServiceTask(testUtils.sf.Id);
		st.serviceTask = testUtils.CreateServiceTaskAttributes();
		st.serviceTask.Name = TEST_VALUE;
		Test.setCurrentPage( Page.LayoutModel );
		ApexPages.currentPage().getParameters().put( 'cloneServiceTaskId', st.serviceTask.Id );
		Test.startTest();
			st.preparationCreateOrEditService();
		Test.stopTest();
		System.assertEquals( null, st.serviceTaskForUpdate.Id );
		System.assertEquals( TEST_VALUE, st.serviceTaskForUpdate.Name );
	}
	
	@isTest
	private static void removeServiceTest(){
		st = new ServiceTask(testUtils.sf.Id);
		st.serviceTask = testUtils.CreateServiceTaskAttributes();
		Test.startTest();
			st.removeService();
		Test.stopTest();
		System.assertEquals( 0, st.serviceTasks.size() );
	}
	
	@isTest
	private static void removeServiceTestNullPointer(){
		st = new ServiceTask(testUtils.sf.Id);
		Test.setCurrentPage( Page.LayoutModel );
		Test.startTest();
			st.removeService();
		Test.stopTest();
		System.assertEquals( 1, ApexPages.getMessages().size() );
	}
				
}