public with sharing class RM_ProjectsDataAccessor {
/**
 	DataAccessor for Project Realated Objects
 */
  	public static list<WFM_Project__c> getProjectByProjectName(string projectName){
  		 return [select name, id, Client_ID__c, Project_Start_Date__c, Project_End_Date__c, Bid_Defense_Date__c , Total_Budget__c, 
  		 	Director_Project_Delivery__c, Global_Project_Manager__c, Project_Status__c, Status_Desc__c,DPD_Employee_ID__c,GPM_Employee_ID__c,
                 Therapeutic_Area__c, phase__c, Indication_Group__c, Primary_Indication__c, Currency_Code__c, Owning_Business_Unit__c, wfm_client__c, Opportunity__c
                from WFM_Project__c where name =:projectName];
  	}
  	public static list<WFM_Project__c> getProjectBySetProjectName(set<string> setProjectName){
  		 return [select name, id, Client_ID__c, Project_Start_Date__c, Project_End_Date__c, Bid_Defense_Date__c , Total_Budget__c, 
  		 	Director_Project_Delivery__c, Global_Project_Manager__c, Project_Status__c, Status_Desc__c,DPD_Employee_ID__c,GPM_Employee_ID__c,
                 Therapeutic_Area__c, phase__c, Indication_Group__c, Primary_Indication__c, Currency_Code__c, Owning_Business_Unit__c, wfm_client__c, Opportunity__c 
                from WFM_Project__c where name in:setProjectName];
  	}
  	public static list<WFM_Project__c> getProjectBySetProjectNameStatus(set<string> setProjectName, string statusDesc){
  		 return [select name, id, Client_ID__c, Project_Start_Date__c, Project_End_Date__c, Bid_Defense_Date__c , Total_Budget__c, 
  		 	Director_Project_Delivery__c, Global_Project_Manager__c, Project_Status__c, Status_Desc__c,DPD_Employee_ID__c,GPM_Employee_ID__c,
                 Therapeutic_Area__c, phase__c, Indication_Group__c, Primary_Indication__c, Currency_Code__c, Owning_Business_Unit__c, wfm_client__c, Opportunity__c
                from WFM_Project__c where name in:setProjectName and status_desc__c =:statusDesc];
  	}
     public static WFM_Project__c GetProjectwithProtocolbyPName(string projectName){
        return [select name, id, Client_ID__c, Project_Start_Date__c, Project_End_Date__c, Bid_Defense_Date__c , Total_Budget__c, 
        	Director_Project_Delivery__c, Global_Project_Manager__c, Project_Status__c, Status_Desc__c,DPD_Employee_ID__c,GPM_Employee_ID__c,
                (select Therapeutic_Area__c, phase__c from protocols__r) 
                from WFM_Project__c where name =:projectName]; 
    }
    public static List<WFM_Project__c> GetProjectbyrID(string rID){
        return [select name, id, client_id__c, Project_Start_Date__c, Project_End_Date__c,  Total_Budget__c, Currency_code__c, 
        	Director_Project_Delivery__c, Global_Project_Manager__c, Project_Status__c, Status_Desc__c,Bid_Defense_Date__c,DPD_Employee_ID__c,
        	GPM_Employee_ID__c, Therapeutic_Area__c, phase__c, Protocol_ID__c, Primary_Indication__c, Owning_Business_Unit__c, Indication_Group__c
                from WFM_Project__c where id =:rID limit 1]; 
    }
    public static list<WFM_Project__c> GetProjectListBySetProject(set<string>projectIDs){
    
        list<WFM_Project__c> ProjAlloList = [SELECT name, id, client_id__c, Project_Start_Date__c, 
        Project_End_Date__c,  Total_Budget__c, Currency_code__c, Director_Project_Delivery__c, Global_Project_Manager__c, Project_Status__c, 
        Status_Desc__c,Bid_Defense_Date__c,DPD_Employee_ID__c,GPM_Employee_ID__c, 
        Therapeutic_Area__c, phase__c, Indication_Group__c, Primary_Indication__c, Owning_Business_Unit__c, wfm_client__c         
        FROM WFM_Project__c 
        where id in :ProjectIDs order  by Name];
        
        return ProjAlloList;
    }
    public static Date getMaxProjectEndDateBySetProject(set<string>stProjID){ 
    	
    	
   		AggregateResult[] maxEndDate = [select max(project_End_Date__c) endDate from WFM_Project__c where id in :stProjID  ];
    	if (maxEndDate.size() > 0)
    	 	return (Date)maxEndDate[0].get('endDate');
    	 return null;
    }
    public static  list<WFM_Protocol__c> GetProtocolByProjectID(string projectRID){
        return [SELECT Actual_First_Site_Init__c,Actual_First_Subj_Enroll__c,Actual_Last_Site_Closed__c,Actual_Last_Subj_Enroll__c,
        Actual_Last_Subj_Off_Study__c,Contracted_DB_Lock_DT__c,Contracted_First_Site_Init__c,Contracted_First_Subj_Enroll__c,
        Contracted_Last_Site_Closed__c,Contracted_Last_Subj_Enroll__c,Contracted_Last_Subj_Off_Study__c,DB_Lock_DT__c,Id,
        Indication_Group__c,Name,Phase__c,Primary_Indication__c,Project_ID__c,Revised_First_Site_Init__c,
        Revised_First_Subj_Enroll__c,Revised_Last_Site_Closed__c,Revised_Last_Subj_Enroll__c,Revised_Last_Subj_Off_Study__c,
        Therapeutic_Area__c 
        FROM WFM_Protocol__c where project_id__c =:projectRID];
    }

    public static List<WFM_Site_Detail__c> GetSitesInfobyProtocolRID(string protRID){
        return [SELECT Actual_Subj_Enroll__c,Country__c,CRF_Pages_Expected__c,Id,Name,Project_Protocol__c,Region_Name__c,
        Site_ID__c,State_Province__c,Status__c, Total_Forecasted_Visits__c 
        FROM WFM_Site_Detail__c where Project_Protocol__c =:protRID
        order by Status__c, Country__c, name];
    }
    
    public static Set<ID> GetProjectSetByEmplRIDYearMonth(string EmplRID, Integer startMonth, Integer endMonth){
        Set<ID> ProjectIDs = new Set<ID>();     
        
        for (WFM_employee_Allocations__c a: [Select Project_ID__c 
            FROM WFM_employee_Allocations__c WHERE Employee_ID__c =:EmplRID         
            and allocation_end_date__c >= :RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(startMonth)) 
            and allocation_start_date__c  <= :RM_Tools.GetDatefromYearMonth(RM_Tools.GetYearMonth(endMonth))])
            ProjectIDs.add(a.Project_ID__c);
            
        
        for (WFM_EE_Work_History__c h: [select Project_ID__c from WFM_EE_Work_History__c  
        where Employee_ID__c= :EmplRID and year_month__c in :RM_Tools.GetMonths(startMonth, endMonth)])
            ProjectIDs.add(h.Project_ID__c);
        return projectIDs;
    }
    public static list<WFM_Project__c> GetProjectListBySetProject(set<ID>projectIDs){
    
        list<WFM_Project__c> ProjAlloList = [SELECT id, Name, client_id__c
        
        FROM WFM_Project__c 
        where id in :ProjectIDs order  by Name];
        
        return ProjAlloList;
    }
    public static List<AggregateResult> GetSitesLocationbyProtocolRID(string protRID){
        AggregateResult[] siteSummary =
        [SELECT count(id) num_site, sum(Actual_Subj_Enroll__c) subjEnroll, Country__c,Region_Name__c 
        	FROM WFM_Site_Detail__c where Project_Protocol__c=:protRID
        	group by country__c, region_name__c order by region_name__c, country__c]; 
         
        return siteSummary;
    }
    public static List<AggregateResult> GetActiveSitesLocationbyProtocolRID(string protRID){
        AggregateResult[] siteSummary =
        [SELECT count(id) num_site,Country__c,Region_Name__c 
        	FROM WFM_Site_Detail__c 
        	where Project_Protocol__c=:protRID and status__c = 'Activated'
        group by country__c, region_name__c order by region_name__c, country__c];
         
        return siteSummary;
    }
    /*---  PM--*/
    public static list<WFM_Project__c> GetPMProject(string employeeRID, string employeeID){
        list <string> roles = new list <string>();
        roles.add('Project Manager');
        roles.add('Project Director');
        //Get current assigned PM and PD role
        list<string> pList = new list<string>();
        for (WFM_employee_Allocations__c a: [SELECT EMPLOYEE_ID__c,allocation_end_Date__c,Name,Is_lead__c, Is_IEDR__c, 
        Is_Unblinded__c, Project_Function__c,Project_ID__r.name
        FROM WFM_employee_Allocations__c 
        WHERE Project_Function__c in :roles and employee_id__c = :employeeRID and allocation_end_date__c > :system.today()]){
            pList.add(a.Project_ID__r.name);
        } 
        //Get GPM from PMC client Project
        
        //for (Client_Project__c pmc: [SELECT Name, Global_Project_Manager_Director__r.name FROM Client_Project__c where Global_Project_Manager_Director__c = :UserInfo.getUserId()])
        //  pList.add(pmc.name);
        
        
        list <string> statusList = new list <string> ();
        statusList.add('Bid');
        statusList.add('Active');
        //Get current GPM project
        return [select id, name, Client_ID__c, Status_Desc__c from WFM_Project__c  
        where (GPM_Employee_id__c = :employeeID or DPD_Employee_ID__c = :employeeID or name in :pList ) and Status_Desc__c in :statusList  order by status_desc__c,  name];
        
    }
    /*-- Opportunity & Account ----*/
      /**
    * @author   Min Chen
    * @date     August 2014   
    * @description  get project by set of opportunity ID
    **/  
    public static list<WFM_Project__c> getProjectByOpportunityID(set<ID> OppIDs){
    	return [select id, name,  Opportunity__c, Project_Unique_Key__c, Therapeutic_Area__c, Indication_Group__c, Primary_Indication__c,
    	Phase__c, Bid_Defense_Date__c, Project_Start_Date__c, Project_End_Date__c, Status_Desc__c, Currency_Code__c, 
    	Total_Budget__c, Owning_Business_Unit__c, WFM_Client__c
    	 from wfm_project__c where Opportunity__c in :OppIDs limit 1000 ];
    }
      /**
    * @author   Min Chen
    * @date     Nov. 2014   
    * @description  get bid project by set of opportunity ID
    **/  
    public static list<Bid_Project__c> getBidProjectByOpportunityID(set<ID> OppIDs){
        return [select id, name,  BID_Stage__c, Opportunity_Type__c, split_Sites_EAPA__c, split_Sites_NA__c, Expected_Patients__c,
        Expected_Sites__c, First_Patient_In__c, Last_Patient_In__c, Proposal_Due_Date__c, RFP_Expected__c, RFP_Received__c, 
        PRA_Project_ID__c, PRA_Project_ID__r.Status_Desc__c, PRA_Project_ID__r.Opportunity__c, Account__c, Opportunity_Owner__c,
        Project_Director__c, Protocol_Number__c
         from Bid_Project__c where PRA_Project_ID__r.Opportunity__c in :OppIDs limit 1000 ];
            
           
    }
      /**
    * @author   Min Chen
    * @date     Nov 2014   
    * @description  get bid project by project Name
    **/  
     public static list<Bid_Project__c> getBidProjectByProjectName(string  projName){
        return [select id, name,  BID_Stage__c, Opportunity_Type__c, split_Sites_EAPA__c, split_Sites_NA__c, Expected_Patients__c,
        Expected_Sites__c, First_Patient_In__c, Last_Patient_In__c, Proposal_Due_Date__c, RFP_Expected__c, RFP_Received__c, 
        PRA_Project_ID__c, PRA_Project_ID__r.Status_Desc__c, PRA_Project_ID__r.Opportunity__c, Account__c, Opportunity_Owner__c,
        Project_Director__c, Protocol_Number__c
         from Bid_Project__c where name = :projName limit 1000 ];
            
           
    }
      /**
    * @author   Min Chen
    * @date     Nov 2014   
    * @description  get bid project by set of project Name
    **/  
     public static list<Bid_Project__c> getBidProjectByProjectName(set<string>  projNames){
        return [select id, name,  BID_Stage__c, Opportunity_Type__c, split_Sites_EAPA__c, split_Sites_NA__c, Expected_Patients__c,
        Expected_Sites__c, First_Patient_In__c, Last_Patient_In__c, Proposal_Due_Date__c, RFP_Expected__c, RFP_Received__c, 
        PRA_Project_ID__c, PRA_Project_ID__r.Status_Desc__c, PRA_Project_ID__r.Opportunity__c, Account__c, Opportunity_Owner__c,
        Project_Director__c, Protocol_Number__c
         from Bid_Project__c where name in :projNames limit 1000 ];
            
           
    }
      /**
    * @author   Min Chen
    * @date     August 2014   
    * @description  get account by set of account ID
    **/  
    public static list<Account> getAccountByaccountID(set<string> accountID){
    	return [select id, name 
    		from account where id in: accountID limit 1000];
    }
      /**
    * @author   Min Chen
    * @date     August 2014   
    * @description  get opportunity by set of opportunity IDs
    **/  
    public static list<Opportunity> getOpportunityByOppRID(set<ID> OppRID){
    	return [select id, name,Type, PRA_Project_ID__c, Therapeutic_Area__c, Indication_Group__c, Primary_Indication__c, Phase__c, 
    	   Bid_Defense_Date__c, Study_Start_Date__c, Study_End_Date__c,	Native_Currency__c, Native_Contract_Amount__c, 
    	   Business_Unit__c, StageName, accountID, Operational_Owner__c, OwnerId, split_Sites_EAPA__c, split_Sites_NA__c,
    	   Expected_Patients__c,Expected_Sites__c, RFP_1st_Patient_In_Date__c, RFP_Last_Patient_In_Date__c, Proposal_Due_Date__c,
    	   RFP_Expected_Date__c, RFP_Received_Date__c, Protocol_Number__c
    		from opportunity where id in: OppRID];
    }
      /**
    * @author   Min Chen
    * @date     August 2014   
    * @description  get client by set of client unique key
    **/  
    public static list<WFM_Client__c> getClientByclientUniqueKey(set<string> nameSet){
    	return [select id, name, client_name__c, client_unique_key__c, account__c
    	from wfm_client__c where client_unique_key__c in : nameset];
    }
       /**
    * @author   Min Chen
    * @date     Nov. 2014   
    * @description  get client by set of client unique key when there is no account
    **/  
    public static list<WFM_Client__c> getClientByclientUniqueKeyNoAccount(set<string> nameSet){
        return [select id, name, client_name__c, client_unique_key__c, account__c
        from wfm_client__c where client_unique_key__c in : nameset and account__c = null];
    }
      /**
    * @author   Min Chen
    * @date     August 2014   
    * @description  get client by set of account ID
    **/  
    public static list<WFM_Client__c>getClientByAccountID(set<ID> accountID){
    	return [select id, name, client_name__c, client_unique_key__c, account__c 
    	from wfm_client__c where account__c in :accountID limit 1000];
    }
   	
}