/**
* @author Hari Krishnan
* @date 07/17/2013
* @description This factory creates the correct dispatcher and dispatches the trigger event(s) to the appropriate 
*				event handler(s). The dispatchers are automatically created using the Type API, hence dispatcher 
*				registration is not required for each dispatchers.
* 				Sukrut Wagh
* 				06/24/24 - version 2
* 				Updated the factory with additional methods to support multiple trigger event handlers that might be defined
*				per TFS project basis
*/
public with sharing class TriggerFactory
{
	public static final String DEFAULT_DISPATCHER = 'COM_TriggerDispatcherBase'; 
	
    /** 
	* @author Hari Krishnan
	* @date 07/17/2013
	* @description Creates the appropriate dispatcher and dispatches the trigger event to the dispatcher's event handler method.
	* @param Schema.sObjectType Object type to process (SObject.sObjectType)
	*/
    public static void createTriggerDispatcher(Schema.sObjectType soType)
    {
        ITriggerDispatcher dispatcher = getTriggerDispatcher(soType);
        if (dispatcher == null)
            throw new COM_Exception(COM_ErrorCode.TRIGGER_DISPATCHER_NOT_FOUND,'No Trigger dispatcher registered for Object Type: ' + soType);
        execute(dispatcher,null);
    }
    
    /** 
	* @author Sukrut Wagh
	* @date 06/23/2014
	* @description 	Creates the appropriate dispatcher and dispatches the trigger event to the dispatcher's event handler method.
	*			   	A default dispatcher implementation - COM_TriggerDispatcherBase is used when the dispatcherClass is not provided.
	*			   	Use handlerPrefix param when you have multiple trigger event handler classes with name format:
	*		 		<PREFIX>_<SOBJECTNAME><EVENT>Handler
	*		 		Eg: RM_AccountAfterInsertHandler, CM_AccountAfterInsertHandler
	*		 		This param is added to support the way Force.com code is managed in PRA's source code control (TFS)
	*		 		based on project name. These projects may choose to implement trigger event handlers
	* @param dispatcherClass (optional) Dispatcher class name
	* @param Schema.sObjectType Object type to process (SObject.sObjectType)
	* @param handlerPrefix of handler apex class prefix. Handlers will be called per the order of the list.
	*/
    public static void createTDWithMultipleHandlers(String dispatcherClass, Schema.sObjectType soType,
    		 List<String> handlerPrefix) {
    	if (soType == null)
            throw new COM_Exception(COM_ErrorCode.TRIGGER_DISPATCHER_SO_TYPE_NULL,'Trigger dispatcher soType cannot be null');
            
    	if(COM_Utils.isEmpty(dispatcherClass)) {
    		dispatcherClass = DEFAULT_DISPATCHER;
    	}
        ITriggerDispatcher dispatcher = getTriggerDispatcherByName(dispatcherClass);
        if (dispatcher == null)
            throw new COM_Exception(COM_ErrorCode.TRIGGER_DISPATCHER_NOT_FOUND,'No Trigger dispatcher registered for dispatcherClass: ' + dispatcherClass);
        dispatcher.setObjectType(soType);
        dispatcher.setHandlerPrefix(handlerPrefix);
        execute(dispatcher,soType.getDescribe().getName());
    }

    /** 
	* @author Hari Krishnan
	* @date 07/17/2013
	* @description Dispatches to the dispatcher's event handlers.
	* @param ITriggerDispatcher A Trigger dispatcher that dispatches to the appropriate handlers
	*/ 
    private static void execute(ITriggerDispatcher dispatcher, final String soTypeName)
    {
    	if(Trigger.isExecuting && COM_Utils.isNotEmpty(soTypeName)) {
    		TriggerParameters tp = new TriggerParameters(Trigger.old, Trigger.new, Trigger.oldMap, Trigger.newMap,
									Trigger.isBefore, Trigger.isAfter, Trigger.isDelete, 
									Trigger.isInsert, Trigger.isUpdate, Trigger.isUnDelete, Trigger.isExecuting,soTypeName);
	        // Handle before trigger events
	        if (Trigger.isBefore) {
	            dispatcher.bulkBefore();
	            if (Trigger.isDelete)
	                dispatcher.beforeDelete(tp);
	            else if (Trigger.isInsert)
	                dispatcher.beforeInsert(tp);
	            else if (Trigger.isUpdate)
	                dispatcher.beforeUpdate(tp);         
	        }
	        else	// Handle after trigger events
	        {
	            dispatcher.bulkAfter();
	            if (Trigger.isDelete)
	                dispatcher.afterDelete(tp);
	            else if (Trigger.isInsert)
	                dispatcher.afterInsert(tp);
	            else if (Trigger.isUpdate)
	                dispatcher.afterUpdate(tp);
	        }
	        dispatcher.andFinally();
    	}
    } 

    /** 
	* @author Hari Krishnan
	* @date 07/17/2013
	* @description Gets the appropriate dispatcher based on the SObject. It constructs the instance of the dispatcher
	*				dynamically using the Type API. The name of the dispatcher has to follow this format:
	*				<ObjectName>TriggerDispatcher. For e.g. for the Feedback__c object, the dispatcher has to be named
	*				as FeedbackTriggerDispatcher.
	* @param Schema.sObjectType Object type to create the dispatcher
	* @return ITriggerDispatcher A trigger dispatcher  if one exists or null.
	*/
    private static ITriggerDispatcher getTriggerDispatcher(Schema.sObjectType soType)
    {
		ITriggerDispatcher dispatcher = getTriggerDispatcherByName(getTriggerDispatcherClassName(soType));
    	return dispatcher;
    }
    
    private static String getTriggerDispatcherClassName(Schema.sObjectType soType) {
    	String dispatcherTypeName = COM_Utils.getObjectName(soType);
    	if(COM_Utils.isNotEmpty(dispatcherTypeName)) {
	    	dispatcherTypeName += 'TriggerDispatcher';
    	}
    	return dispatcherTypeName;
    }
    
    /** 
	* @author Sukrut Wagh
	* @date 06/24/2014
	* @description 	Constructs the instance of the dispatcher
	*				dynamically using the Type API. The name of the dispatcher typically follows this format:
	*				<ObjectName>TriggerDispatcher. For e.g. for the Feedback__c object, the dispatcher has to be named
	*				as FeedbackTriggerDispatcher.
	*				In case the Base dispatcher is used, it's class name will be passed
	* @param dispatcherClass Apex class name to create the dispatcher
	* @return ITriggerDispatcher A trigger dispatcher  if one exists or null.
	*/
    private static ITriggerDispatcher getTriggerDispatcherByName(String dispatcherClass) {
		Type obType = Type.forName(dispatcherClass);
		ITriggerDispatcher dispatcher = (obType == null) ? null : (ITriggerDispatcher)obType.newInstance();
    	return dispatcher;
    }
}