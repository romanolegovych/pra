/** Implements the Service Layer of the object FinancialDocumentApprRule__c
 * @author	Dimitrios Sgourdos
 * @version	19-Dec-2013
 */
public with sharing class FinancialDocumentApprRuleService {
	
	/** Retrieve all the Financial Document Approval Rules that have been set in the system.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 10-Dec-2013
	 * @return	The list of all the Financial Document Approval Rules
	 */
	public static List<FinancialDocumentApprRule__c> getAllFinancialDocumentApprRules() {
		return FinancialDocumentApprRuleDataAccessor.getFinancialDocumentApprRules();
	}
	
	
	/** Retrieve all the Financial Document Approval Rules that have been set in the system and analyze
	 *	them in an approvalRuleWrapper list.
	 * @author	Dimitrios Sgourdos
	 * @version 16-Dec-2013
	 * @return	The created approvalRuleWrapper list.
	 */
	public static List<approvalRuleWrapper> getApprovalRuleWrapperData() {
		List<approvalRuleWrapper> results = new List<approvalRuleWrapper>();
		
		List<FinancialDocumentApprRule__c> rulesList = FinancialDocumentApprRuleDataAccessor.getFinancialDocumentApprRules();
		
		for(FinancialDocumentApprRule__c tmpRule : rulesList) {
			approvalRuleWrapper newItem = new approvalRuleWrapper();
			newItem.rule = tmpRule;
			
			Boolean applicableFlag = (tmpRule.SelectedRule__c != FinancialDocumentApprRuleDataAccessor.TOTAL_VALUE_MORE
										&& tmpRule.SelectedRule__c != FinancialDocumentApprRuleDataAccessor.PAYMENT_TERMS);
			
			if(applicableFlag && String.isNotBlank(tmpRule.ListOfServiceCategories__c) ) {
				newItem.serviceCategoriesIdsList.addAll(tmpRule.ListOfServiceCategories__c.split(':'));
			}
			
			results.add(newItem);
		}
		
		return results; 
	}
	
	
	/** Add a Financial Document Approval Rule to the given list of rules.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Dec-2013
	 * @param	sourceList			The initial wrapper list that holds the approval rules
	 * @return	The updated wrapper list that holds the approval rules.
	 */
	public static List<approvalRuleWrapper> addRuleToApprovalRuleWrapperList(List<approvalRuleWrapper> sourceList) {
		approvalRuleWrapper newItem = new approvalRuleWrapper();
		newItem.rule = new FinancialDocumentApprRule__c(SequenceNumber__c = sourceList.size()+1);
		sourceList.add(newItem);
		return sourceList;
	}
	
	
	/**	Reset the first and second parameter of the given Financial Document Approval Rule.
	 * @author	Dimitrios Sgourdos
	 * @version	12-Dec-2013
	 */
	public static approvalRuleWrapper resetApprovalRuleParameters(approvalRuleWrapper ruleWrapperItem) {
		// Reset the selected service categories list
		ruleWrapperItem.serviceCategoriesIdsList = new List<String>();
		
		// Check for illegal parameter
		if(ruleWrapperItem.rule == NULL) {
			return ruleWrapperItem;
		}
		
		// Reset the parameters
		ruleWrapperItem.rule.RuleFirstParameter__c  = '';
		ruleWrapperItem.rule.RuleSecondParameter__c = ''; 
		
		return ruleWrapperItem; 
	}
	
	
	/** Remove a Financial Document Approval Rule from the given list of rules.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Dec-2013
	 * @param	sourceList			The initial wrapper list that holds the approval rules
	 * @param	ruleIndex			The index of the approval rule in the list of rules that will be removed
	 * @return	The updated wrapper list that holds the approval rules.
	 */
	public static List<approvalRuleWrapper> removeRuleFromApprovalRuleWrapperList(List<approvalRuleWrapper> sourceList,
																				Integer ruleIndex) {
		// Check if the ruleIndex is valid
		if(ruleIndex < 0 || ruleIndex >= sourceList.size()) {
			return sourceList;
		}
		
		// remove approval rule
		sourceList.remove(ruleIndex);
		return sourceList;
	}
	
	
	/** Delete the given Financial Document Approval Rules in case they have id. 
	 * @author	Dimitrios Sgourdos
	 * @version 12-Dec-2013
	 * @param	sourceList			The initial list of Approval Rules
	 * @return	If the deletion was successful or not.
	 */
	public static Boolean deleteApprovalRules(List<FinancialDocumentApprRule__c> sourceList) {
		List<FinancialDocumentApprRule__c> deletedList = new List<FinancialDocumentApprRule__c>();
		
		// delete only the samples that have id
		for(FinancialDocumentApprRule__c tmpRule : sourceList) {
			if(tmpRule.Id != NULL) {
				deletedList.add(tmpRule);
			}
		}
		
		try{
			delete deletedList;
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}
	
	
	/** Validate if the given approval rules have all the neccessary fields inserted or not.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Dec-2013
	 * @param	sourceList			The wrapper list that holds the approval rules
	 * @return	If the approval rules list is valid or not.
	 */
	public static Boolean validateApprovalRules(List<approvalRuleWrapper> sourceList) {
		// Check all the rules
		for(approvalRuleWrapper tmpItem : sourceList) {
			// Check for general fields
			if( String.isBlank(tmpItem.rule.Description__c)
					|| String.isBlank(tmpItem.rule.SelectedRule__c)
					|| String.isBlank(tmpItem.rule.RuleFirstParameter__c) ) {
				return false;
			}
			
			// Check for 'Applied to' Service Category depends on the selected rule
			Boolean emptySrvCatSelection = (tmpItem.serviceCategoriesIdsList.size()==1
												&& String.isBlank(tmpItem.serviceCategoriesIdsList[0]) );
			
			Boolean applicableFlag = (tmpItem.rule.SelectedRule__c != FinancialDocumentApprRuleDataAccessor.TOTAL_VALUE_MORE
									 && tmpItem.rule.SelectedRule__c != FinancialDocumentApprRuleDataAccessor.PAYMENT_TERMS);
			
			if( applicableFlag && (tmpItem.serviceCategoriesIdsList.isEmpty() || emptySrvCatSelection) ) {
				return false;
			}
			
			// Check for second parameter depends on the selected rule
			applicableFlag = (tmpItem.rule.SelectedRule__c==FinancialDocumentApprRuleDataAccessor.PROPOSAL_VALUE_MORE
								|| tmpItem.rule.SelectedRule__c==FinancialDocumentApprRuleDataAccessor.TOTAL_VALUE_MORE);
			
			if(applicableFlag && String.isBlank(tmpItem.rule.RuleSecondParameter__c) ) {
				return false;
			}
		}
		
		// All the rules are valid
		return true;
	}
	
	
	/** Renumber the sequence number of the given approval rules.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Dec-2013
	 * @param	sourceList			The list that holds the approval rules
	 * @return	The updated approval rules.
	 */
	public static List<FinancialDocumentApprRule__c> renumberApprovalRulesList(List<FinancialDocumentApprRule__c> sourceList){
		// Sort approval rules
		sourceList = BDT_Utils.sortListNumeric(sourceList, 'SequenceNumber__c'); 
		
		// Renumber approval rules
		Decimal val=1;
		for (FinancialDocumentApprRule__c tmpItem : sourceList) {
			tmpItem.SequenceNumber__c = val;
			val++;
		}
		
		return sourceList;
	}
	
	
	/** Adjust and extract the approval rules from an approvalRuleWrapper list.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Dec-2013
	 * @param	sourceList			The wrapper list that holds the approval rules
	 * @return	The adjusted approval rules.
	 */
	public static List<FinancialDocumentApprRule__c> extractApprovalRules(List<approvalRuleWrapper> sourceList) {
		List<FinancialDocumentApprRule__c> results = new List<FinancialDocumentApprRule__c>();
		
		for(approvalRuleWrapper tmpItem : sourceList) {
			// Check the sequence number
			tmpItem.rule.SequenceNumber__c = (tmpItem.rule.SequenceNumber__c == NULL)? 0 : tmpItem.rule.SequenceNumber__c;
			
			// Update rule in case its type containsservice categories selection
			Boolean applicableFlag = (tmpItem.rule.SelectedRule__c != FinancialDocumentApprRuleDataAccessor.TOTAL_VALUE_MORE
									 && tmpItem.rule.SelectedRule__c != FinancialDocumentApprRuleDataAccessor.PAYMENT_TERMS);
			
			if(applicableFlag) {
				If(tmpItem.serviceCategoriesIdsList.size()>0 && String.isBlank(tmpItem.serviceCategoriesIdsList[0]) ) {
					tmpItem.serviceCategoriesIdsList.remove(0);
				}
				tmpItem.rule.ListOfServiceCategories__c = BDT_Utils.convertListToString(tmpItem.serviceCategoriesIdsList);
			}
			
			results.add(tmpItem.rule);
		}
		
		results = renumberApprovalRulesList(results);
		
		return results;
	}
	
	
	/** Save the given approval rules.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Dec-2013
	 * @param	source			The initial list of approval rules
	 * @return	If the save was successful or not.
	 */
	public static Boolean saveApprovalRules(List<FinancialDocumentApprRule__c> sourceList) {
		try{
			upsert sourceList;
		} catch(Exception e) {
			return false;
		}
		return true;
	}
	
	
	/** Check if the given approval rule (type NUM_OF_CURRENCIES) is applicable for the given financial document.
	 * @author	Dimitrios Sgourdos
	 * @version 19-Dec-2013
	 * @param	ruleData			The approval rule
	 * @param	documentData		The financial document
	 * @return	If the approval rule is applicable or not.
	 */
	public static Boolean isNumOfCurrenciesRuleApplicable(approvalRuleWrapper ruleData,
													FinancialDocumentService.financialDocumentGroupWrapper documentData) {
		// Initialize variables
		Set<String> currenciesSet = new Set<String>();
		Integer numOfCurrencies = 0;
		
		try {
			numOfCurrencies = Integer.valueOf(ruleData.rule.RuleFirstParameter__c);
		} catch(Exception e) {
			return false;
		}
		
		// Iterate through selected service categories for the rule
		for(String tmpId : ruleData.serviceCategoriesIdsList) {
			// If there is no meaning skip this step
			if( ! documentData.pricesPerServiceCategoryMap.containsKey(tmpId) ) {
				continue;
			}
			
			// Iterate through the Financial Category Total Prices
			List<FinancialCatTotPrice__c> pricesList = documentData.pricesPerServiceCategoryMap.get(tmpId);
			for(FinancialCatTotPrice__c tmpPrice : pricesList) {
				currenciesSet.add(tmpPrice.OriginalCurrency__c);
				if(currenciesSet.size() > numOfCurrencies) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	
	/** Check if the given approval rule (type PROPOSAL_VALUE_MORE) is applicable for the given financial document.
	 * @author	Dimitrios Sgourdos
	 * @version 19-Dec-2013
	 * @param	ruleData			The approval rule
	 * @param	documentData		The financial document
	 * @return	If the approval rule is applicable or not.
	 */
	public static Boolean isValueOfProposalRuleApplicable(approvalRuleWrapper ruleData,
													FinancialDocumentService.financialDocumentGroupWrapper documentData) {
		// Initialize variables
		Decimal documentValue  = 0;
		Decimal ruleValueLimit = 0;
		String  ruleCurrency   = ruleData.rule.RuleSecondParameter__c;
		
		try {
			ruleValueLimit = Decimal.valueOf(ruleData.rule.RuleFirstParameter__c);
		} catch (Exception e) {
			return false;
		}
		
		// Iterate through selected service categories for the rule
		for(String tmpId : ruleData.serviceCategoriesIdsList) {
			// If there is no meaning skip this step
			if( ! documentData.pricesPerServiceCategoryMap.containsKey(tmpId) ) {
				continue;
			}
			
			// Iterate through the Financial Category Total Prices
			List<FinancialCatTotPrice__c> pricesList = documentData.pricesPerServiceCategoryMap.get(tmpId);
			for(FinancialCatTotPrice__c tmpPrice : pricesList) {
				if(tmpPrice.OriginalCurrency__c == ruleCurrency && tmpPrice.TotalPrice__c != NULL) {
					documentValue += tmpPrice.TotalPrice__c;
				}
				
				if(documentValue > ruleValueLimit) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	
	/** Check if the given approval rule (type TOTAL_VALUE_MORE) is applicable for the given financial document.
	 * @author	Dimitrios Sgourdos
	 * @version 19-Dec-2013
	 * @param	ruleData			The approval rule
	 * @param	documentData		The financial document
	 * @return	If the approval rule is applicable or not.
	 */
	public static Boolean isTotalValueOfProposalRuleApplicable(approvalRuleWrapper ruleData,
													FinancialDocumentService.financialDocumentGroupWrapper documentData) {
		// Initialize variables
		Decimal documentValue  = 0;
		Decimal ruleValueLimit = 0;
		String  ruleCurrency   = ruleData.rule.RuleSecondParameter__c;
		
		try {
			ruleValueLimit = Decimal.valueOf(ruleData.rule.RuleFirstParameter__c);
		} catch (Exception e) {
			return false;
		}
		
		// Iterate through all the Financial Category Total Prices
		for(String tmpId : documentData.pricesPerServiceCategoryMap.keySet() ) {
			List<FinancialCatTotPrice__c> pricesList = documentData.pricesPerServiceCategoryMap.get(tmpId);
			for(FinancialCatTotPrice__c tmpPrice : pricesList) {
				if(tmpPrice.OriginalCurrency__c == ruleCurrency && tmpPrice.TotalPrice__c != NULL) {
					documentValue += tmpPrice.TotalPrice__c;
				}
				
				if(documentValue > ruleValueLimit) {
					return true;
				}
			}
		}
		
		return false;
	}
	
	
	/** Check if the given approval rule (type DELTA_MORE) is applicable for the given financial document.
	 * @author	Dimitrios Sgourdos
	 * @version 19-Dec-2013
	 * @param	ruleData			The approval rule
	 * @param	documentData		The financial document
	 * @return	If the approval rule is applicable or not.
	 */
	public static Boolean isDeltaRuleApplicable(approvalRuleWrapper ruleData,
												FinancialDocumentService.financialDocumentGroupWrapper documentData) {
		// Initialize variables
		Decimal ruleDelta = 0;
		
		try {
			ruleDelta = Decimal.valueOf(ruleData.rule.RuleFirstParameter__c);
		} catch (Exception e) {
			return false;
		}
		
		// Iterate through selected service categories for the rule
		for(String tmpId : ruleData.serviceCategoriesIdsList) {
			// If there is no meaning skip this step
			if( ! documentData.pricesPerServiceCategoryMap.containsKey(tmpId) ) {
				continue;
			}
			
			// Iterate through the Financial Category Total Prices
			List<FinancialCatTotPrice__c> pricesList = documentData.pricesPerServiceCategoryMap.get(tmpId);
			for(FinancialCatTotPrice__c tmpPrice : pricesList) {
				if(tmpPrice.PriceDelta__c != NULL) {
					Decimal deltaPercentage = math.abs(tmpPrice.PriceDelta__c);
					if(deltaPercentage > ruleDelta) {
						return true;
					}
				}
			}
		}
		
		return false;
	}
	
	
	/** Get all the applicable approval rules for the given financial document.
	 * @author	Dimitrios Sgourdos
	 * @version 19-Dec-2013
	 * @param	financialDocumentId		The id of the financial document
	 * @return	The applicable approval rules.
	 */
	public static List<FinancialDocumentApprRule__c> getApplicableApprovalRulesForFinancialDocument(
																						String financialDocumentId) {
		// Declare variables
		List<FinancialDocumentApprRule__c> results = new List<FinancialDocumentApprRule__c>();
		
		// Read all the approval rules
		List<approvalRuleWrapper> rulesDataList = getApprovalRuleWrapperData();
		
		// Read all financial group data
		FinancialDocumentService.financialDocumentGroupWrapper documentData =
									FinancialDocumentService.getFinancialDocumentGrouWrapperData(financialDocumentId);
		
		// Filter the approval rules
		for(approvalRuleWrapper tmpItem : rulesDataList) {
			Boolean applicableFlag = false;
			
			if(tmpItem.rule.SelectedRule__c == FinancialDocumentApprRuleDataAccessor.TOTAL_VALUE_MORE) {
				applicableFlag = isTotalValueOfProposalRuleApplicable(tmpItem, documentData);
			} else if(tmpItem.rule.SelectedRule__c == FinancialDocumentApprRuleDataAccessor.PROPOSAL_VALUE_MORE) {
				applicableFlag = isValueOfProposalRuleApplicable(tmpItem, documentData);
			} else if(tmpItem.rule.SelectedRule__c == FinancialDocumentApprRuleDataAccessor.DELTA_MORE) {
				applicableFlag = isDeltaRuleApplicable(tmpItem, documentData);
			} else if(tmpItem.rule.SelectedRule__c == FinancialDocumentApprRuleDataAccessor.NUM_OF_CURRENCIES) {
				applicableFlag = isNumOfCurrenciesRuleApplicable(tmpItem, documentData);
			} //else if(tmpItem.rule.SelectedRule__c == FinancialDocumentApprRuleDataAccessor.PAYMENT_TERMS ) {
				// This rule will be implemented in a future release
			//}
			
			if(applicableFlag) {
				results.add(tmpItem.rule);
			}
		}
		
		return results;
	}
	
	
	/**	This class keeps a Financial Document Approval Rule with its parameters analyzed.
	 * @author	Dimitrios Sgourdos
	 * @version	13-Dec-2013
	 */
	public class approvalRuleWrapper {
		public FinancialDocumentApprRule__c 	rule  					 {get;set;}
		public List<String>						serviceCategoriesIdsList {get;set;}
		
		public approvalRuleWrapper() {
			rule = new FinancialDocumentApprRule__c();
			serviceCategoriesIdsList = new List<String>();
		}
	}
}