/**
@author Fahad Arif
@date 2015
@description this is PBB_ServiceTasktoFlowController test class
**/

@isTest
public class PBB_ServiceTasktoFlowControllerTest {
    static testMethod void PBB_ServiceTasktoFlowController(){
        
        STSWR1__Flow__c f = new STSWR1__Flow__c();
        f.name = 'This is test record for Flow - Fahad';
        insert f;
        
        Service_Model__c sm = new Service_Model__c();
        sm.Description__c = 'bvjhvkampvfa dfadg';
        sm.Status__c = 'In-Process';
        insert sm;

		sm.Status__c = 'Approved';
		upsert sm;        
        
        WR_Model__c wr = new WR_Model__c();
        wr.Description__c = 'Thsis is a test description jndai ijfaig';
        wr.Flow__c = f.id;
        wr.Service_Model__c = sm.id;
        insert wr;
        
        Service_Task_To_Flow_Step__c st = new Service_Task_To_Flow_Step__c();
		st.WR_Model__c = wr.id;
        insert st;
        
        STSWR1__Flow_Step_Junction__c sj = new STSWR1__Flow_Step_Junction__c();
        sj.name = 'nbfahuvanoijav4vag';
        sj.STSWR1__Flow__c = f.id;
        insert sj;
        
        Service_Model__c sm1 = new Service_Model__c();
        sm1.Description__c = 'bvjhvkampvfa';
        sm1.Status__c = 'In-Process';
        insert sm1;
        
        Service_Area__c sa = new Service_Area__c();
        sa.name = 'fbdanfkaga';
        sa.Service_Model__c = sm1.id;
        insert sa;
        
        Service_Function__c srf = new Service_Function__c();
        srf.name = 'janvoanoganvav51vag';
        srf.Service_Area__c = sa.id;
        insert srf;
		        
        Service_Task__c srt = new Service_Task__c();
        srt.Name = 'afyanfuahgag';
        srt.Service_Function__c = srf.id;
        insert srt;
        
        Service_Task_To_Flow_Step__c st1 = new Service_Task_To_Flow_Step__c();
		st1.Flow_Step__c = sj.id;
        st1.Service_Tasks__c = srt.id;
        st1.WR_Model__c = wr.id;
		insert st1;
        
        ApexPages.StandardController sc = new ApexPages.standardController(st);
        
        PBB_DataAccessor.getWrModelRecord(wr.id);
        PBB_ServiceTasktoFlowController sf = new PBB_ServiceTasktoFlowController(sc);
        
		List<SelectOption> FlowStepPickList = sf.getFlowStepPickList(); 
        System.assertEquals(FlowStepPickList.size(),2); 
        
        List<SelectOption> ServiceTaskPickList = sf.getServiceTaskPickList();
        System.assertEquals(ServiceTaskPickList.size(),1); 
        sf.saveSTtoFS();
        
        ApexPages.StandardController sc1 = new ApexPages.standardController(st1);
        PBB_ServiceTasktoFlowController sf1 = new PBB_ServiceTasktoFlowController(sc1);
        sf1.saveSTtoFS();
        
    }
}