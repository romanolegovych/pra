public class AA_BufCodeGenerator{

    public Boolean saveReady{get;set;}
    public Boolean entryReady{get;set;}
    private final List<Business_Unit__c> bu;
    private final List<Function_Code__c> fc;
    public String selectedBU{get;set;}
    public String[] selectedF{get;set;}
    public BUF_Code__c bufCode;
    public List<BUF_Code__c> bufcodeDetails = new List<BUF_Code__c>();  
    public Map<String, String> mapSelectListBUValues = new Map<String, String>();
    public Map<String, String> mapSelectListFCValues = new Map<String, String>();
    
    
    public AA_BufCodeGenerator(ApexPages.StandardSetController controller) {
        bu = AA_BufCodeService.getBusinessUnitCodes();
        fc = AA_BufCodeService.getFunctionCodes();
        saveReady = false;
        entryReady = false;
    }


    /* Constructor to initalize Business Unit Codes and Function Codes */
    public AA_BufCodeGenerator() {
        bu = AA_BufCodeService.getBusinessUnitCodes();
        fc = AA_BufCodeService.getFunctionCodes();
        saveReady = false;
        entryReady = false;
    }
    
    public List<BUF_Code__c> getbufcodeDetails(){
        return bufcodeDetails;
    }
    
    /* Populate Business Unit Codes in Select List */
    public List<SelectOption> getBuselectoption() {
        List<SelectOption> buselectoption = new List<SelectOption>();
        for(Business_Unit__c entry : bu){
            buselectoption.add(new SelectOption(entry.Id, entry.Name));
            mapSelectListBUValues.put(entry.Id, entry.Name);
        }
        return buselectoption;
    }
    
    /* Populate Function Codes in Select List */
    public List<SelectOption> getfcselectoption() {
         List<SelectOption> fcselectoption = new List<SelectOption>();
         for(Function_Code__c entry : fc){
            fcselectoption.add(new SelectOption(entry.Id, entry.Name));
            mapSelectListFCValues.put(entry.Id, entry.Name);
        }
        return fcselectoption;
    }
    
    /* Save BUF codes */
    public PageReference save() {
        Integer error = 0;
        bufcodeDetails.clear();
        for ( String s : selectedF){ 
            String tempFC = mapSelectListFCValues.get(s);
            bufCode = new BUF_Code__c();
            String temp  = mapSelectListBUValues.get(selectedBU) + tempFC ; 
            List<BUF_Code__c> bufCodeTemp;
            bufCodeTemp=AA_BufCodeService.getBufCodeName(temp);
            /* Check if BUF Code already Exists */
                if(bufCodeTemp.size() > 0){
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'BUF Code : ' + temp + ' already exists'));
                     error = 1;
                }else{
                    bufCode.name =   temp;
                    bufCode.Business_Unit__c = selectedBU;
                    bufCode.Function_Code__c = s;
                    bufcodeDetails.add(bufCode);
                    AA_BufCodeService.insertBufCode(bufCode);
                    error = 2;
                }
            }
            /* Display success message */
            if(error == 2)
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.INFO,' BUF Codes Created Succesfully'));
                saveReady = false;
                entryReady = false;
            }
        return null;
    }
    
    /* Generate BUF codes */
    public PageReference generate() {
        Integer error = 0;
        bufcodeDetails.clear();
        if(selectedF.size() > 0){
        for ( String s : selectedF){ 
            String tempFC = mapSelectListFCValues.get(s);
            bufCode = new BUF_Code__c();
            String temp  = mapSelectListBUValues.get(selectedBU) + tempFC ; 
            List<BUF_Code__c> bufCodeTemp;
            bufCodeTemp=AA_BufCodeService.getBufCodeName(temp);
            /* Check if BUF Code already Exists */
                if(bufCodeTemp.size() > 0){
                     ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'BUF Code : ' + temp + ' already exists'));
                     error = 1;
                }else{
                    bufCode.name =   temp;
                    bufCode.Business_Unit__c = selectedBU;
                    bufCode.Function_Code__c = s;
                    bufcodeDetails.add(bufCode);
                }
            }
            if(error==0){
                saveReady = true;
                entryReady = true;
                }
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Select Function Codes'));
             error = 1;
        }
        return null;
    }
    public String getselectedBU() {
        return selectedBU;
    }
 }