@isTest
private class PRA_ContractValueModuleControllerTest {

    static testMethod void shouldSearchProject() {
        // given
        PRA_ContractValueModuleController contractedValCtr = new PRA_ContractValueModuleController();
        PRA_TestUtils tu = new PRA_TestUtils();
        insert tu.getAccount('Test Account');
        Date curentDate = System.today();
        Client_Project__c pr1 = tu.getClientProject(tu.client.id, 'Test project1', 'praProjectId1', curentDate.addMonths(-6), curentDate.addMonths(6));
        Client_Project__c pr2 = tu.getClientProject(tu.client.id, 'Test 2', 'praProjectId2', curentDate.addMonths(-6), curentDate.addMonths(6));
        insert new List<Client_Project__c>{pr1,pr2};      
        
        // when
        contractedValCtr.selectProject();
        contractedValCtr.getSelectedProject();
        List<Client_Project__c> projects = PRA_ContractValueModuleController.searchProject('Some crezzy query');
        system.assertEquals(0, projects.size());
        projects = PRA_ContractValueModuleController.searchProject('Test');
        system.assertEquals(2, projects.size());
        contractedValCtr.setSelectedProject(pr1.id);
        
        //then
        system.assertEquals(pr1.name, contractedValCtr.project.name);
        
    }
    static testMethod void shouldDisplayCalculationPage() {
        // given
        PRA_ContractValueModuleController contractedValCtr = new PRA_ContractValueModuleController();
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        // set Client_Unit_Number__c in one of tasks
        Client_Task__c updateCrct = tu.clientTasks.get(0);
        update updateCrct;
        
        List<Unit_Effort__c> unitOfficialForecast = [select Id, Forecast_Unit__c from Unit_Effort__c where Client_Task__r.project__c = :tu.clientProject.Id];
        // increase number of one unit - to introduce diffrence between bid contract and actual total units
        Unit_Effort__c un = unitOfficialForecast.get(0);
        un.Forecast_Unit__c = un.Forecast_Unit__c + 7;
        update un;
        
        // when
        test.startTest();
        
        contractedValCtr.setSelectedProject(tu.clientProject.Id);
        List<PRA_ContractValueModuleController.SummaryValuesWraper> summaryValues = contractedValCtr.getSummaryValues();
        List<PRA_ContractValueModuleController.ClientUnitContractValue> contractUnitDetail = contractedValCtr.getDetailClientUnitContractValues();
        
        List<Client_Task__c> tasks = [select Id, Description__c, Project_Region__r.Name, 
                        Total_Units__c, Total_Baseline_Units__c, Contract_Value__c, Total_Contract_Units__c, 
                        Total_CON_BDG_Value__c, Total_CON_BDG_Value_APAC__c, Total_CON_BDG_Value_EA__c, Total_CON_BDG_Value_LA__c,
            			Total_CON_BDG_Value_US_Ca__c, Contract_Value_APAC__c, Contract_Value_EA__c, Contract_Value_LA__c, 
            			Contract_Value_US_CA__c, Contract_Value_Per_Unit__c, CVPU_US_Canada__c, CVPU_Europe_Africa__c, 
            			CVPU_Asia_Pacific__c, CVPU_Latin_America__c                                                    
	                    from Client_Task__c 
	                    where Project__c = :tu.clientProject.Id
	                    order by Description__c, Project_Region__r.Name];
        Map<String, Regional_Contract_Value__c> regionContractValues = getRegionContractValue(tu.clientProject);
        test.stopTest();
        
        // then
        // check summary
        system.assertEquals(6, summaryValues.size());
        // check contract value details
        system.assertEquals(tasks.size(), contractUnitDetail.size());
      
        //Map<ID, Bid_Contract__c> bidContracts = tu.getBidContractPerCrct();
        Map<ID, PRA_ContractValueModuleController.ClientUnitContractValue> unitContractedValuesPerCrct = new Map<ID, PRA_ContractValueModuleController.ClientUnitContractValue>();
        for(PRA_ContractValueModuleController.ClientUnitContractValue cvmc : contractUnitDetail) {
            unitContractedValuesPerCrct.put(cvmc.taskId, cvmc);
        }        
        
        Decimal expectedValNa       = 0;
        Decimal expectedValEMEA     = 0;
        Decimal expectedValLatin    = 0;
        Decimal expectedValApac     = 0;
        
        Decimal expectedTotal       = 0;
        Decimal expectedTotalNa     = 0;
        Decimal expectedTotalEmea   = 0;
        Decimal expectedTotalLatin  = 0;
        Decimal expectedTotalApac   = 0;
        
        for(Client_Task__c task : tasks) {
            Decimal regValNa    = 0;
            Decimal regValEmea  = 0;
            Decimal regValLatin = 0;
            Decimal regValApac  = 0;
            
            regValNa = regionContractValues.get(task.id).US_Canada__c;
            regValEmea = regionContractValues.get(task.id).Europe_Africa__c;
            regValLatin = regionContractValues.get(task.id).Latin_America__c;
            regValApac = regionContractValues.get(task.id).Asia_Pacific__c;
            
            // check task/units details
            //Bid_Contract__c bid = bidContracts.get(task.id);
            PRA_ContractValueModuleController.ClientUnitContractValue cucv = unitContractedValuesPerCrct.get(task.id);            
   
            system.assertEquals(task.Total_CON_BDG_Value_US_Ca__c == null ? 0 : task.Total_CON_BDG_Value_US_Ca__c, cucv.na);
            system.assertEquals(task.Total_CON_BDG_Value_EA__c == null ? 0 : task.Total_CON_BDG_Value_EA__c, cucv.emea);
            system.assertEquals(task.Total_CON_BDG_Value_LA__c == null ? 0 : task.Total_CON_BDG_Value_LA__c, cucv.latin);
            system.assertEquals(task.Total_CON_BDG_Value_APAC__c == null ? 0 : task.Total_CON_BDG_Value_APAC__c, cucv.apac);
            system.assertEquals(task.Contract_Value_Per_Unit__c, cucv.unitPrice);
            system.assertEquals(task.Total_CON_BDG_Value__c == null ? 0 : task.Total_CON_BDG_Value__c, cucv.total);            
            
            expectedTotal       += task.Total_CON_BDG_Value__c == null ? 0 : task.Total_CON_BDG_Value__c;
            expectedTotalNa     += task.Total_CON_BDG_Value_US_Ca__c == null ? 0 : task.Total_CON_BDG_Value_US_Ca__c;
            expectedTotalEmea   += task.Total_CON_BDG_Value_EA__c == null ? 0 : task.Total_CON_BDG_Value_EA__c;
            expectedTotalLatin  += task.Total_CON_BDG_Value_LA__c == null ? 0 : task.Total_CON_BDG_Value_LA__c;
            expectedTotalApac   += task.Total_CON_BDG_Value_APAC__c == null ? 0 : task.Total_CON_BDG_Value_APAC__c;
            
        }
        
        system.assertEquals(contractedValCtr.totalContractValues, expectedTotal);
        system.assertEquals(contractedValCtr.totalNa, expectedTotalNa);
        system.assertEquals(contractedValCtr.totalEmea, expectedTotalEmea);
        system.assertEquals(contractedValCtr.totalLatin, expectedTotalLatin);
        system.assertEquals(contractedValCtr.totalApac, expectedTotalApac);
        
        // check export to excel
        Pagereference expretedRef = Page.PRA_ContractValueModuleCSV;
        expretedRef.getParameters().put('id', contractedValCtr.project.id);
        system.assertEquals(expretedRef.getURL(), contractedValCtr.exportToExcel().getURL());
    }
    static testMethod void shouldTestWraperClasses() {
        
        // given
        PRA_ContractValueModuleController.SummaryValuesWraper sumWrap = new PRA_ContractValueModuleController.SummaryValuesWraper();
        PRA_ContractValueModuleController.ClientUnitContractValue clientUnitWrap = new PRA_ContractValueModuleController.ClientUnitContractValue();
        
        // when
        sumWrap = new PRA_ContractValueModuleController.SummaryValuesWraper ('test name');
        sumWrap.na = 1;
        sumWrap.emea = 2;
        sumWrap.latin = 3;
        sumWrap.apac = 4;
        
        clientUnitWrap = new PRA_ContractValueModuleController.ClientUnitContractValue('CC-1', 'UnitName', 'WE', 2, 3, 4, 5, 6, 21);

        //then
        //system.assertEquals(10, sumWrap.getAll());
        
        system.assertEquals('CC-1', clientUnitWrap.unitNumber);
        system.assertEquals('UnitName', clientUnitWrap.unitName);
        system.assertEquals('WE', clientUnitWrap.region);
        system.assertEquals(2, clientUnitWrap.unitPrice);
        system.assertEquals(3, clientUnitWrap.na);
        system.assertEquals(4, clientUnitWrap.emea);
        system.assertEquals(5, clientUnitWrap.latin);
        system.assertEquals(6, clientUnitWrap.apac);
        system.assertEquals(21, clientUnitWrap.total);
    }
    static testMethod void shouldSetProjectFromURL() {
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        insert tu.getAccount('Test Account');
        Date curentDate = System.today();
        Client_Project__c pr1 = tu.getClientProject(tu.client.id, 'Test project1', 'praProjectId1', curentDate.addMonths(-6), curentDate.addMonths(6));
        Client_Project__c pr2 = tu.getClientProject(tu.client.id, 'Test 2', 'praProjectId2', curentDate.addMonths(-6), curentDate.addMonths(6));
        insert new List<Client_Project__c>{pr1,pr2};      
        
        // when
        Pagereference pageRef = Page.PRA_ContractValueModule;
        pageRef.getParameters().put('id', pr1.id);
        Test.setCurrentPage(pageRef);
        PRA_ContractValueModuleController contractedValCtr = new PRA_ContractValueModuleController();
        
        //then
        system.assertEquals(pr1.name, contractedValCtr.project.name);
        
    }
    
    private static Map<String, Regional_Contract_Value__c> getRegionContractValue(Client_Project__c project) {
       	Map<String, Regional_Contract_Value__c> regionContractValue = new Map<String, Regional_Contract_Value__c> ();
        
        if(project != null) {
            List<Regional_Contract_Value__c> regionCtrValList = [select Id, Client_Task__c, Asia_Pacific__c, Effective_Date__c, Europe_Africa__c, 
                        Latin_America__c, US_Canada__c 
                        from Regional_Contract_Value__c 
                        where Client_Task__r.Project__c = :project.Id];
            for(Regional_Contract_Value__c rcv : regionCtrValList) {
                if(!regionContractValue.containsKey(rcv.Client_Task__c)) {
                    regionContractValue.put(rcv.Client_Task__c, rcv);
                } else if(regionContractValue.get(rcv.Client_Task__c).Effective_Date__c < rcv.Effective_Date__c) {
                    regionContractValue.put(rcv.Client_Task__c, rcv);
                }
            }
        }
        return regionContractValue;
    }
}