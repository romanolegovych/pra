/**
 * @description	The Service Layer for the Notification Event
 * @author		Jegadheesan Muthusamy, Dimitrios Sgourdos
 * @date		Created: 09-Sep-2015, Edited: 02-Oct-2015
 */
public with sharing class NCM_SrvLayer_NotificationEvent {
	
	/**
	 * @description	Create the notification event based on the notification catalog id and the related to id and then 
	 				create notification for each active notification registration and then notification history for each
	 				notication. Also, it sends the selected per user communication (email, sms or chatter post).
	 * @author		Jegadheesan Muthusamy, Dimitrios Sgourdos
	 * @date		Created: 10-Sep-2015, Edited: 14-Sep-2015
	 * @param		notificationCatalogId    The id of the notification topic that occurs
	 * @param		relatedToId              The related object id that triggers the notification topic
	*/
	public static void activateNotificationEventById(Id notificationCatalogId, String relatedToId ) {
		// Check for invalid parameters
		if( notificationCatalogId == NULL ) {
			return;
		}
		
		// Retrieve corresponding notification catalog record
		NotificationCatalog__c notificationCatalog;
		
		try{
			notificationCatalog = NCM_DataAccessor.getNotificationTopicById( notificationCatalogId );
		} catch(Exception e) {
			// Notification catalog was not found
			system.debug(e.getMessage());
			return;
		}
		
		// Create Notification Event and inform subscribed users
		createNotificationEventAndSendNotifications(notificationCatalog, relatedToId);
	}
	
	
	/**
	 * @description	Create the notification event based on the notification catalog name and the related to id and then
	 *				create notification for each active notification registration and then notification history for each
	 *				notication. Also, it sends the selected per user communication (email, sms or chatter post).
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015
	 * @param		notificationCatalogName    The name of the notification topic that occurs
	 * @param		relatedToId                The related object id that triggers the notification topic
	*/
	public static void activateNotificationEventByName(String notificationCatalogName, String relatedToId ) {
		// Check for invalid parameters
		if( String.isBlank(notificationCatalogName)) {
			return;
		}
		
		// Retrieve corresponding notification catalog record
		NotificationCatalog__c notificationCatalog;
		
		try{
			notificationCatalog = NCM_DataAccessor.getNotificationTopicByName(notificationCatalogName);
		} catch(Exception e) {
			// Notification catalog was not found
			system.debug(e.getMessage());
			return;
		}
		
		// Create Notification Event and inform subscribed users
		createNotificationEventAndSendNotifications(notificationCatalog, relatedToId);
	}
	
	
	/**
	 * @description	Create the notification event record for the given notification catalog and the related to id and
	 *				and then create notification for each active notification registration and then notification
	 *				history for each notication.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015
	 * @param		notificationCatalogName    The notification topic that occurs
	 * @param		relatedToId                The related object id that triggers the notification topic
	*/
	@testVisible private static void createNotificationEventAndSendNotifications(
																			NotificationCatalog__c notificationCatalog,
																			String relatedToId) {
		// Check for invalid parameters
		if( notificationCatalog == NULL ) {
			return;
		}
		
		// Build the notification subject and email body
		String notificationSubject = '';
		String notificationEmailBody = '';
		
		try {
			// Get the Type corresponding to the class name
			Type t = Type.forName(notificationCatalog.ImplementationClassName__c);
			// Instantiate the type.
			// The type of the instantiated object is the interface.
			NCM_NotificationEventInterface classInstance = (NCM_NotificationEventInterface) t.newInstance();
			notificationSubject = classInstance.getNotificationSubject( notificationCatalog.Id, relatedToId );
			notificationEmailBody = classInstance.getNotificationEmailBody( notificationCatalog.Id, relatedToId );
			// If class not found don't do anything just return
		} catch( Exception ex ) {
			return;
		}
		
		// If enabled in the notification topic, post in the chatter of the related to object
		if(notificationCatalog.ChatterEnabled__c) {
			try{
				NCM_Chatter_API.postFeedItem(relatedToId, notificationSubject);
			} catch(Exception e) {
				// Chatter post failed, probably because related object is not enabled for chatter
				system.debug(e.getMessage());
			}
		}
		
		// Create the 'Notification Event' record to capture the corresponding event
		NotificationEvent__c newNotificationEvent = createAndInsertNewNotificationEvent( notificationCatalog,
																						 relatedToId,
																						 notificationSubject,
																						 notificationEmailBody );
		
		// Retrieve the list of the active notification registration
		List<NotificationRegistration__c> notificationRegistrationList =
																	NCM_DataAccessor.getListOfNotificationRegistration(
																								notificationCatalog.Id,
																								relatedToId );
		
		// Check for empty results, and map registrations in their id, and notify users by chatter (the ones that they
		// have selected it)
		if( notificationRegistrationList.isEmpty() ) {
			return; // not necessary to continue, if not active subscriptions exists
		}
		
		Map<Id, NotificationRegistration__c> notificationRegistrationMap =
												new Map<Id, NotificationRegistration__c>(notificationRegistrationList);
		
		
		Set<Id> userIds = NCM_SrvLayer_NotificationRegistration.getUserIdsToBeNotififiedByChatter(
																						notificationRegistrationList);
		try {
			postInUsersChatter(userIds, notificationSubject);
		} catch(Exception e) {
			// Chatter post failed, probably because related object is not enabled for chatter
			system.debug(e.getMessage());
		}
		
		// Create and insert the Notification records for each registration
		List<Notification__c> newNotificationsList = createAndInsertNewNotifications(	notificationRegistrationList,
																						newNotificationEvent.Id );
		
		// Communicate with users and capture the communication in the notification history
		List<NotificationHistory__c> newNotificationsHistoryList = createAndInsertNotificationHistoryRecords(
																					newNotificationsList,
																					notificationRegistrationMap,
																					notificationSubject,
																					notificationEmailBody);
	}
	
	
	/**
	 * @description	Create and insert the new notification event record for the notification catalog and related to id.
	 * @author		Jegadheesan Muthusamy
	 * @date		Created: 10-Sep-2015, Edited: 11-Sep-2015
	 * @param		notificationCatalog    The notification catalog record
	 * @param		relatedToId            The related object id that is connected with the registrations
	 * @param		notificationSubject    The subject of the event(subject in emails, text in SMS and Chatter)
	 * @param		emailBody              The email body of the event
	 * @return		The notification event record
	*/
	@TestVisible private static NotificationEvent__c createAndInsertNewNotificationEvent(
																			 NotificationCatalog__c notificationCatalog,
																			 String relatedToId,
																			 String notificationSubject,
																			 String emailBody ) {
		// Initialize instance
		NotificationEvent__c newNotificationEvent = new NotificationEvent__c(
																NotificationCatalog__c	= notificationCatalog.Id,
																RelatedToId__c			= relatedToId,
																Subject__c				= notificationSubject,
																EmailBody__c			= emailBody,
																Type__c					= notificationCatalog.Type__c
															);
		
		// Insert into DB
		insert newNotificationEvent;
		return newNotificationEvent;
	}
	
	
	/**
	 * @description	Create and insert a notification record for each given notification registration.
	 * @author		Jegadheesan Muthusamy
	 * @date		Created: 10-Sep-2015, Edited:11-Sep-2015
	 * @param		notificaionRegistrationList    The active notification registrations to the notification topic
	 * @param		newNotificationEvent           The notification event that occurs
	 * @return		The list of notifications records
	*/
	@TestVisible private static List<Notification__c> createAndInsertNewNotifications(
														List<NotificationRegistration__c> notificationRegistrationList,
														String newNotificationEventId ) {
		// Initializ data and create notification for each registration
		List<Notification__c> newNotificationsList = new List<Notification__c>();
		
		for( NotificationRegistration__c notificationRegistration : notificationRegistrationList ) {
			Notification__c newNotification = new Notification__c();
			
			newNotification.NotificationEvent__c		= newNotificationEventId;
			newNotification.NotificationRegistration__c = notificationRegistration.Id;
			
			// Remainder is now + remainder period in the registration
			Decimal tmpReminderPeriodDec = (notificationRegistration.ReminderPeriod__c == NULL)?
																			24 :
																			notificationRegistration.ReminderPeriod__c;
			Integer reminderPeriodInt = Integer.valueOf(tmpReminderPeriodDec);
			
			newNotification.Reminder__c = DateTime.now().addHours( reminderPeriodInt );
			
			// For statuses if Acknowledgement Required true in catalog then it is pending acknowledgement otherwise
			// acknowledged
			newNotification.Status__c = (notificationRegistration.NotificationCatalog__r.AcknowledgementRequired__c) ?
																		NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT :
																		NCM_API_DataTypes.ACKNOWLEDGED;
			newNotificationsList.add( newNotification );
		}
		
		// Insert into DB
		insert newNotificationsList;
		return newNotificationsList;
	}
	
	
	/**
	 * @description	Create and insert notification history records to capture the communication with the subscribed
	 *				users, when notification records are created.
	 * @author		Jegadheesan Muthusamy, Dimitrios Sgourdos
	 * @date		Created: 10-Sep-2015, Edited:02-Oct-2015
	 * @param		newNotificationsList			The newly created notification list
	 * @param		notificationRegistrationMap		The notification registration Map
	 * @param		notificationSubject             The subject of the event(subject in emails, text in SMS)
	 * @param		notificationEmailBody           The email body of the event
	 * @return		The list of notifications history
	*/
	@TestVisible private static List<NotificationHistory__c> createAndInsertNotificationHistoryRecords(
													List<Notification__c> newNotificationsList,
													Map<Id, NotificationRegistration__c> notificationRegistrationMap,
													String notificationSubject,
													String notificationEmailBody) {
		
		// Initialize data and check for invalid parameter
		List<NotificationHistory__c> notificationsHistoryList = new List<NotificationHistory__c>();
		
		if(newNotificationsList == NULL
				|| newNotificationsList.isEmpty()
				|| notificationRegistrationMap == NULL
				|| notificationRegistrationMap.isEmpty() ){
			return notificationsHistoryList;
		}
		
		List<Messaging.SingleEmailMessage> notificationEmailsList = new List<Messaging.SingleEmailMessage>();
		
		// Find the org wide email id for setting up the from address in the emails
		Id orgWideFromAddressId = NCM_Email_API.getOrgWideEmailAddressIdFromAddressName(
																			NCM_API_DataTypes.ORG_WIDE_EMAIL_ADDRESS);
		
		// Create notification history for each notification
		for( Notification__c notification : newNotificationsList ) {
			// Check if the registration is included in the map, to double check that parameters are valid
			if( ! notificationRegistrationMap.containsKey(notification.NotificationRegistration__c) ) {
				continue;
			}
			
			NotificationRegistration__c registration = notificationRegistrationMap.remove(
																			notification.NotificationRegistration__c );
			
			Boolean smsOkFlag = registration.NotifyBySMS__c && registration.NotificationCatalog__r.SMSallowed__c;
			
			if( (!registration.NotifyByEmail__c) && (!smsOkFlag) ) {
				continue;
			}
			
			// Create Notification History instance
			NotificationHistory__c newHistory = new NotificationHistory__c(
																	Notification__c	= notification.Id,
																	EmailSent__c	= registration.NotifyByEmail__c,
																	SMSsent__c		= smsOkFlag
																);
			notificationsHistoryList.add( newHistory );
			
			if(newHistory.EmailSent__c) {
				notificationEmailsList.add( NCM_Email_API.createEmailInstance(	registration.UserId__c,
																				orgWideFromAddressId,
																				notificationSubject,
																				notificationEmailBody,
																				NULL)
										);
			}
		}
		
		// Insert into DB and send emails
		insert notificationsHistoryList;
		NCM_Email_API.sendEmailInstances(notificationEmailsList);
		return notificationsHistoryList;
	}
	
	
	/**
	 * @description	Post the feeds under the given corresponding users
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 10-Sept-2015, Edited: 11-Sep-2015
	 * @param		userIds     The user ids that will be notified by chatter for the notification event
	 * @param		postText    The text of the chatter post
	*/
	@TestVisible private static void postInUsersChatter(Set<Id> userIds, String postText) {
		List<NCM_Chatter_API_DataTypes.ChatterPostWrapper> chatterWrapperData =
															new List<NCM_Chatter_API_DataTypes.ChatterPostWrapper>();
		
		for(Id tmpId : userIds) {
			chatterWrapperData.add( new NCM_Chatter_API_DataTypes.ChatterPostWrapper(tmpId, postText) );
		}
		
		NCM_Chatter_API.postFeedItem(chatterWrapperData);
	}
	
	
	/**
	 * @description	Find all the events that ware marked as NCM_API_DataTypes.EVENT_UNDER_BATCH_PROCEDURE and
	 *				unmark them.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 25-Sep-2015
	*/
	public static void releaseNotificationEventsFromBatchProcedure() {
		List<NotificationEvent__c> eventsList = NCM_DataAccessor.getNotificationEventUnderBatchProcedure();
		
		for(NotificationEvent__c tmpEvent : eventsList) {
			tmpEvent.Name = tmpEvent.Id;
		}
		
		update eventsList;
	}
	
	
	/**
	 * @description	Implement the polling mechanism for triggering topics, create events, notifications and sending
	 *				the notifications to the subscribed users.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 25-Sep-2015, Edited: 30-Sep-2015
	 * @param		registrationList    The registrations assigned with the topics to be decided if occured or not
	*/
	public static void createEventsFromPollingMechanism(List<NotificationRegistration__c> registrationList) {
		// Check for invalid parameters
		if(registrationList == NULL || registrationList.isEmpty()) {
			return;
		}
		
		// Declare variables
		List<NotificationEvent__c>	 eventToBeInsertedList		  = new List<NotificationEvent__c>();
		List<Notification__c>		 notificationToBeInsertedList = new List<Notification__c>();
		
		Map<String, List<NotificationRegistration__c>> registrationsOnCatalogAndRelatedIdMap =
																	new Map<String, List<NotificationRegistration__c>>();
		
		List<NCM_Chatter_API_DataTypes.ChatterPostWrapper> chatterWrapperData =
															new List<NCM_Chatter_API_DataTypes.ChatterPostWrapper>();
		
		Id previousCatalogId		= NULL;
		String previousRelatedToId	= NULL;
		Boolean eventHasOccured		= false;
		Type t;
		NCM_NotificationEventInterface classInstance;
		String notificationSubject;
		String notificationEmailBody;
		
		// Select all the events that are under batch procedure and map them on their catalog id:related to id
		Map<String, NotificationEvent__c> eventOnCatalogAndRelatedIdMap = mapBatchProceededEventsOnCatalogIdAndRelateToId();
		
		Set<String> usedCatalogAndRelatedToIdSet = new Set<String>( eventOnCatalogAndRelatedIdMap.keySet() );
		
		// The data coming from batch job are supposed to be ordered by Notification Catalog and related to id
		for(NotificationRegistration__c reg : registrationList) {
			// Check if there is an implementation class name, else skip this record without updating previous values
			if(reg.NotificationCatalog__r.ImplementationClassName__c == NULL) {
				continue;
			}
			
			// Check if the assigned event has already been triggered in previous batch, and then create a notification
			// instance and proceed to the next iteration
			String key = '' + reg.NotificationCatalog__c + ':' + reg.RelatedToId__c;
			if( eventOnCatalogAndRelatedIdMap.containsKey(key) ) {
				Notification__c tmpNotif = NCM_SrvLayer_Notification.createNotificationInstanceFromRegistrationAndEvent(
																			reg,
																			eventOnCatalogAndRelatedIdMap.get(key).Id );
				notificationToBeInsertedList.add(tmpNotif);
				continue;
			}
			
			// Untill here the event hasn't been triggered in previous iteration of for or batch chunk
			// If the catalog is different then re-instantiate the implementation class
			if(reg.NotificationCatalog__c != previousCatalogId) {
				try {
					// Get the Type corresponding to the class name
					// Instantiate the type.
					t = Type.forName(reg.NotificationCatalog__r.ImplementationClassName__c);
					// The type of the instantiated object is the interface.
					classInstance = (NCM_NotificationEventInterface) t.newInstance();
				} catch( Exception ex ) {
					// Update iteration helper variables
					previousCatalogId	= reg.NotificationCatalog__c;
					previousRelatedToId	= reg.RelatedToId__c;
					eventHasOccured		= false;
					t = NULL;
					classInstance = NULL;
					continue;
				}
			}
			
			// If the related to id is different or the catalog is different, then check again if the event occured
			if(reg.RelatedToId__c != previousRelatedToId || reg.NotificationCatalog__c != previousCatalogId) {
				// Need to check if event occured
				try {
					eventHasOccured = classInstance.didNotificationTopicOccur(	reg.NotificationCatalog__c,
																				reg.RelatedToId__c);
					
					if(eventHasOccured) {
						notificationSubject = classInstance.getNotificationSubject(	reg.NotificationCatalog__c,
																					reg.RelatedToId__c );
						notificationEmailBody = classInstance.getNotificationEmailBody(	reg.NotificationCatalog__c,
																						reg.RelatedToId__c );
					} else {
						// Update iteration helper variables and continue to next iteration
						previousCatalogId	= reg.NotificationCatalog__c;
						previousRelatedToId	= reg.RelatedToId__c;
						eventHasOccured		= false;
						continue;
					}
				} catch( Exception ex ) {
					// If implementation class creates exception, iterate to the next record
					previousCatalogId	= reg.NotificationCatalog__c;
					previousRelatedToId	= reg.RelatedToId__c;
					eventHasOccured		= false;
					t = NULL;
					classInstance = NULL;
					continue;
				}
			}
			
			// If event had occured, proceed with the steps of creating event, notifications, notification history
			if( eventHasOccured ) {
				// If the event instance hasn't been created in a previous iteration of the for loop
				if( ! usedCatalogAndRelatedToIdSet.contains(key) ) {
					// Event instance
					eventToBeInsertedList.add( new NotificationEvent__c(
												Name					= NCM_API_DataTypes.EVENT_UNDER_BATCH_PROCEDURE,
												NotificationCatalog__c	= reg.NotificationCatalog__c,
												RelatedToId__c			= reg.RelatedToId__c,
												Subject__c				= notificationSubject,
												EmailBody__c			= notificationEmailBody,
												Type__c					= reg.NotificationCatalog__r.Type__c
											) );
					usedCatalogAndRelatedToIdSet.add(key);
					
					// Chatter instance if needed
					if(reg.NotificationCatalog__r.ChatterEnabled__c) {
						chatterWrapperData.add(new NCM_Chatter_API_DataTypes.ChatterPostWrapper(reg.RelatedToId__c,
																								notificationSubject)
												);
					}
				}
				
				// Capture the registration for creating notifications (with the same key could be that many
				// registrations exist under different users)
				List<NotificationRegistration__c> tmpRegList = new List<NotificationRegistration__c>();
				if( registrationsOnCatalogAndRelatedIdMap.containsKey(key) ) {
					tmpRegList = registrationsOnCatalogAndRelatedIdMap.remove(key);
				}
				tmpRegList.add(reg);
				registrationsOnCatalogAndRelatedIdMap.put(key, tmpRegList);
			}
			
			// Refresh the temporary variables for next iteration
			previousCatalogId = reg.NotificationCatalog__c;
			previousRelatedToId = reg.RelatedToId__c;
		}
		
		// Add the chatter posts
		try {
			NCM_Chatter_API.postFeedItem(chatterWrapperData);
		} catch(Exception e) {
			// Chatter post failed, probably because related object is not enabled for chatter
			system.debug(e.getMessage());
		}
		
		// Add events and and map them in their Catalogid:relatedToId
		insert eventToBeInsertedList;
		
		// Add notifications
		notificationToBeInsertedList = createAndInsertNewNotificationsFromEventsAndRegistrations(
																				notificationToBeInsertedList,
																				eventToBeInsertedList,
																				registrationsOnCatalogAndRelatedIdMap);
						
		
		// For the inserted notifications retrieve them from the database as additional info from parent objects is
		// needed for creating history records, creating chatter posts and send emails/sms
		Map<Id, Notification__c> tmpMap = new Map<Id, Notification__c>(notificationToBeInsertedList);
		notificationToBeInsertedList = NCM_DataAccessor.getNotificationsById( tmpMap.keySet() );
		
		communicateNotificationsAndCaptureInHistory(notificationToBeInsertedList);
	}
	
	
	/**
	 * @description	Find all the events that were marked as NCM_API_DataTypes.EVENT_UNDER_BATCH_PROCEDURE and map them
	 *				on their Catalog id and related to id.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 25-Sep-2015
	 * @return		The map of the retrieved events on their Catalog id and Related to Id
	*/
	@TestVisible private static Map<String, NotificationEvent__c> mapBatchProceededEventsOnCatalogIdAndRelateToId() {
		Map<String, NotificationEvent__c> results = new Map<String, NotificationEvent__c>();
		
		List<NotificationEvent__c> eventsList = NCM_DataAccessor.getNotificationEventUnderBatchProcedure();
		
		for(NotificationEvent__c tmpEvent : eventsList) {
			results.put( '' + tmpEvent.NotificationCatalog__c + ':' + tmpEvent.RelatedToId__c,
						tmpEvent);
		}
		
		return results;
	}
	
	
	/**
	 * @description	Create and insert new notifications from given notifications, events and mapped registrations
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 30-Sep-2015
	 * @param		notificationToBeInsertedList             The already created notifications that should be inserted
	 * @param		eventList                                The events for creation additional notifications
	 * @param		registrationsOnCatalogAndRelatedIdMap    The mapped registrations to assign notifications to events
	 * @return		The inserted notifications
	*/
	@TestVisible private static List<Notification__c> createAndInsertNewNotificationsFromEventsAndRegistrations(
						List<Notification__c> notificationToBeInsertedList,
						List<NotificationEvent__c> eventList,
						Map<String, List<NotificationRegistration__c>> registrationsOnCatalogAndRelatedIdMap) {
		// Map the given events in their Catalogid:relatedToId
		Map<String, NotificationEvent__c> eventOnCatalogAndRelatedToIdMap = new Map<String, NotificationEvent__c>();
		
		for(NotificationEvent__c tmpEvent : eventList) {
			eventOnCatalogAndRelatedToIdMap.put(
												'' + tmpEvent.NotificationCatalog__c + ':' + tmpEvent.RelatedToId__c,
												tmpEvent);
		}
		
		// Add the notifications that were not instatiated from assigned registrations
		for(String tmpKey : eventOnCatalogAndRelatedToIdMap.keySet()) {
			if( ! registrationsOnCatalogAndRelatedIdMap.containsKey(tmpKey) ) {
				continue;
			}
			
			Id eventId = eventOnCatalogAndRelatedToIdMap.remove(tmpKey).Id;
			List<NotificationRegistration__c> tmpRegList = registrationsOnCatalogAndRelatedIdMap.remove(tmpKey);
			
			for(NotificationRegistration__c tmpReg : tmpRegList) {
				Notification__c tmpNotification = NCM_SrvLayer_Notification.createNotificationInstanceFromRegistrationAndEvent(
																			tmpReg,
																			eventId );
				notificationToBeInsertedList.add(tmpNotification);
			}
		}
		
		insert notificationToBeInsertedList;
		
		return notificationToBeInsertedList;
	}
	
	
	/**
	 * @description	Send chatter posts, emails or SMS for the given notifications and in case of email or sms sending,
	 *				capture the communication to the notification history object.
	 *				users, when notification records are created.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 30-Sep-2015, Edited: 08-Oct-2015
	 * @param		newNotificationsList    The newly created notifications
	*/
	@TestVisible private static void communicateNotificationsAndCaptureInHistory(
																			List<Notification__c> notificationList) {
		// Initialize data and check for invalid parameter
		if(notificationList == NULL || notificationList.isEmpty() ){
			return;
		}
		
		List<NotificationHistory__c> notificationsHistoryList = new List<NotificationHistory__c>();
		List<Messaging.SingleEmailMessage> notificationEmailsList = new List<Messaging.SingleEmailMessage>();
		List<NCM_Chatter_API_DataTypes.ChatterPostWrapper> chatterWrapperData =
															new List<NCM_Chatter_API_DataTypes.ChatterPostWrapper>();
		
		// Find the org wide email id for setting up the from address in the emails
		Id orgWideFromAddressId = NCM_Email_API.getOrgWideEmailAddressIdFromAddressName(
																			NCM_API_DataTypes.ORG_WIDE_EMAIL_ADDRESS);
		
		// Create notification history for each notification
		for( Notification__c tmpNotification : notificationList ) {
			// Check if a chatter post should be created
			if(tmpNotification.Notificationregistration__r.NotifyByChatter__c) {
				chatterWrapperData.add(	new NCM_Chatter_API_DataTypes.ChatterPostWrapper(
															tmpNotification.Notificationregistration__r.UserId__c,
															tmpNotification.NotificationEvent__r.Subject__c)
										);
			}
			
			// Check if an email or an sms must be sent, and if not continue to next iteration
			Boolean smsOkFlag = tmpNotification.NotificationRegistration__r.NotifyBySMS__c
								&& tmpNotification.NotificationRegistration__r.NotificationCatalog__r.SMSallowed__c;
			
			if( (!tmpNotification.NotificationRegistration__r.NotifyByEmail__c) && (!smsOkFlag) ) {
				continue;
			}
			
			// Create Notification History instance and email instance
			NotificationHistory__c newHistory = new NotificationHistory__c(
										Notification__c	= tmpNotification.Id,
										EmailSent__c	= tmpNotification.NotificationRegistration__r.NotifyByEmail__c,
										SMSsent__c		= smsOkFlag
									);
			notificationsHistoryList.add( newHistory ); 
			
			if(newHistory.EmailSent__c) {
				notificationEmailsList.add( NCM_Email_API.createEmailInstance(
																tmpNotification.Notificationregistration__r.UserId__c,
																orgWideFromAddressId,
																tmpNotification.NotificationEvent__r.Subject__c,
																tmpNotification.NotificationEvent__r.EmailBody__c,
																NULL)
										);
			}
		}
		
		// Insert into DB, send chatter posts and emails
		insert notificationsHistoryList;
		
		try {
			NCM_Chatter_API.postFeedItem(chatterWrapperData);
		} catch(Exception e) {
			system.debug(e.getMessage());
		}
		
		NCM_Email_API.sendEmailInstances(notificationEmailsList);
	}
}