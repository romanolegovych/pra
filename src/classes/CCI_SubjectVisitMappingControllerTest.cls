@isTest
public class CCI_SubjectVisitMappingControllerTest {
    
    static list<MuleServicesCS__c> serviceSettings;
    
    static void init() {
        serviceSettings = new MuleServicesCS__c[] {
            new MuleServicesCS__c(name = 'ENDPOINT', Value__c = 'ENDPOINT'),
            new MuleServicesCS__c(name = 'TIMEOUT', Value__c = '60000'),
            new MuleServicesCS__c(name = 'MdmSvtService', Value__c = 'MdmSvtService'),
            new MuleServicesCS__c(name = 'MdmProtocolService', Value__c = 'MdmProtocolService')
        };
        insert serviceSettings;
    }
    
    static testMethod void testResetAllData() {
        String TEST_VALUE = 'test value';
        CCI_SubjectVisitMappingController controller = new CCI_SubjectVisitMappingController();
        controller.praStudyId = TEST_VALUE;
        controller.protocolText = TEST_VALUE;
        controller.keySvtId = TEST_VALUE;
        controller.dummyBoolean = true;
        controller.svtEcrfWrapper = new map<String, CCI_SubjectVisitMappingController.SvtEcrfMappingWrapper>();
        controller.shouldRenderMapping = new map<String, Boolean>();
        
        controller.resetAllData();
        system.assert(controller.praStudyId == null && controller.protocolText == null && controller.keySvtId == null && !controller.dummyBoolean);
    }

    static testMethod void testLikeProtocolsSearch() {
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(false));
        list<Object> results = CCI_AutocompleteController.retreiveWSObjects('getProtocols', 'MOCK');
        system.assert(results.size() == 5);
        Test.stopTest();    
    }
    
    static testMethod void testProtocolSearch() {
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(false));
        CCI_SubjectVisitMappingController controller = new CCI_SubjectVisitMappingController();
        callProtocolSearch(controller);
        system.assert(controller.protocolVO != null);
        system.assert(controller.protocolVO.clientProtocolNum.equals(controller.protocolText));
        Test.stopTest();
    }
    
    static testMethod void testPopulateClientProjectsDropdown() {
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(false));
        CCI_SubjectVisitMappingController controller = new CCI_SubjectVisitMappingController();
        callProtocolSearch(controller);
        system.assert(controller.protocolVO != null);
        system.assert(controller.protocolVO.clientProtocolNum.equals(controller.protocolText));
        
        list<selectoption> options = controller.getClientProjectsFromProtocol();
        system.assert(options.size() == 6 && options.get(1).getValue().equals('PRA ID 1'));
        Test.stopTest();
    }
    
    static testMethod void testSearchAllData() {
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(false));
        CCI_SubjectVisitMappingController controller = new CCI_SubjectVisitMappingController();
        callProtocolSearch(controller);
        system.assert(controller.protocolVO != null);
        system.assert(controller.protocolVO.clientProtocolNum.equals(controller.protocolText));
        
        Test.setMock(WebServiceMock.class, new MdmSvtServiceTestMocks(false));
        controller.praStudyId = 'MOCK STUDYVO';
        controller.searchAllData();
        system.assert(controller.svtVOs.size() == 5);
        for (MdmSvtService.svtVO svt : controller.svtVOs) {
            system.assert(svt.svtItems.size() == 10);
            system.assert(controller.svtEcrfWrapper.get(svt.systemId).ecrfItemVOs.size() == 20);
            system.assert(controller.svtEcrfWrapper.get(svt.systemId).visitMappings.size() == 20);
        }
        Test.stopTest();
    }
    
    static testMethod void testSearchAllDataNullResponse() {
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(false));
        CCI_SubjectVisitMappingController controller = new CCI_SubjectVisitMappingController();
        callProtocolSearch(controller);
        system.assert(controller.protocolVO != null);
        system.assert(controller.protocolVO.clientProtocolNum.equals(controller.protocolText));
        
        Test.setMock(WebServiceMock.class, new MdmSvtServiceTestMocks(true));
        controller.praStudyId = 'MOCK STUDYVO';
        controller.searchAllData();
        system.assert(controller.svtVOs == null);
        Test.stopTest();
    }
    
    static testMethod void testAddMappings() {
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(false));
        CCI_SubjectVisitMappingController controller = new CCI_SubjectVisitMappingController();
        // Search Protocol
        callProtocolSearch(controller);
        system.assert(controller.protocolVO != null);
        system.assert(controller.protocolVO.clientProtocolNum.equals(controller.protocolText));
        
        // Search mappings
        Test.setMock(WebServiceMock.class, new MdmSvtServiceTestMocks(false));
        controller.praStudyId = 'MOCK STUDYVO';
        controller.searchAllData();
        system.assert(controller.svtVOs.size() == 5);
        String svtId;
        for (MdmSvtService.svtVO svt : controller.svtVOs) {
            svtId = svt.systemId;
            system.assert(svt.svtItems.size() == 10);
            system.assert(controller.svtEcrfWrapper.get(svt.systemId).ecrfItemVOs.size() == 20);
            system.assert(controller.svtEcrfWrapper.get(svt.systemId).visitMappings.size() == 20);
        }
        
        // Add new mapping data
        controller.keySvtId = svtId;
        list<selectoption> svtOptions = controller.svtEcrfWrapper.get(svtId).svtItemOptions;
        list<selectoption> ecrfOptions = controller.svtEcrfWrapper.get(svtId).ecrfItemOptions;
        controller.svtEcrfWrapper.get(svtId).selectedSVT = svtOptions.get(1).getValue();
        controller.svtEcrfWrapper.get(svtId).selectedECRF = ecrfOptions.get(1).getValue();
        controller.addScheduledMappingData();
        Test.stopTest();
    }
    
    static testMethod void testRemoveMappings() {
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(false));
        CCI_SubjectVisitMappingController controller = new CCI_SubjectVisitMappingController();
        // Search Protocol
        callProtocolSearch(controller);
        system.assert(controller.protocolVO != null);
        system.assert(controller.protocolVO.clientProtocolNum.equals(controller.protocolText));
        
        // Search mappings
        Test.setMock(WebServiceMock.class, new MdmSvtServiceTestMocks(false));
        controller.praStudyId = 'MOCK STUDYVO';
        controller.searchAllData();
        system.assert(controller.svtVOs.size() == 5);
        String svtId;
        for (MdmSvtService.svtVO svt : controller.svtVOs) {
            svtId = svt.systemId;
            system.assert(svt.svtItems.size() == 10);
            system.assert(controller.svtEcrfWrapper.get(svt.systemId).ecrfItemVOs.size() == 20);
            system.assert(controller.svtEcrfWrapper.get(svt.systemId).visitMappings.size() == 20);
        }
        
        // Add new mapping data
        controller.keySvtId = svtId;
        list<selectoption> svtOptions = controller.svtEcrfWrapper.get(svtId).svtItemOptions;
        list<selectoption> ecrfOptions = controller.svtEcrfWrapper.get(svtId).ecrfItemOptions;
        controller.svtEcrfWrapper.get(svtId).selectedSVT = svtOptions.get(1).getValue();
        controller.svtEcrfWrapper.get(svtId).selectedECRF = ecrfOptions.get(1).getValue();
        controller.addScheduledMappingData();
        
        // Remove mapping data
        for (CCI_SubjectVisitMappingController.MappingWrapper mappingWrapper : controller.svtEcrfWrapper.get(svtId).visitMappings) {
            mappingWrapper.isSelected = true;
        }
        controller.removeScheduledMappingData();
        Test.stopTest();
    }
    
    static testMethod void testSaveMappings_POSITIVE() {
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(false));
        CCI_SubjectVisitMappingController controller = new CCI_SubjectVisitMappingController();
        // Search Protocol
        callProtocolSearch(controller);
        system.assert(controller.protocolVO != null);
        system.assert(controller.protocolVO.clientProtocolNum.equals(controller.protocolText));
        
        // Search mappings
        Test.setMock(WebServiceMock.class, new MdmSvtServiceTestMocks(false));
        controller.praStudyId = 'MOCK STUDYVO';
        controller.searchAllData();
        system.assert(controller.svtVOs.size() == 5);
        String svtId;
        for (MdmSvtService.svtVO svt : controller.svtVOs) {
            svtId = svt.systemId;
            system.assert(svt.svtItems.size() == 10);
            system.assert(controller.svtEcrfWrapper.get(svt.systemId).ecrfItemVOs.size() == 20);
            system.assert(controller.svtEcrfWrapper.get(svt.systemId).visitMappings.size() == 20);
        }
        
        // Add new mapping data
        controller.keySvtId = svtId;
        list<selectoption> svtOptions = controller.svtEcrfWrapper.get(svtId).svtItemOptions;
        list<selectoption> ecrfOptions = controller.svtEcrfWrapper.get(svtId).ecrfItemOptions;
        controller.svtEcrfWrapper.get(svtId).selectedSVT = svtOptions.get(1).getValue();
        controller.svtEcrfWrapper.get(svtId).selectedECRF = ecrfOptions.get(1).getValue();
        controller.addScheduledMappingData();
        
        // Save Mappings
        Test.setMock(WebServiceMock.class, new MdmSvtServiceTestMocks(false));
        /*for (CCI_SubjectVisitMappingController.MappingWrapper mappingWrapper : controller.svtEcrfWrapper.get(svtId).visitMappings) {
            mappingWrapper.isSelected = true;
        }*/
        controller.saveScheduledMappingData();
        Test.stopTest();
    }
    
    static testMethod void testSaveMappings_NEGATIVE() {
        init();
        Test.startTest();
        Test.setMock(WebServiceMock.class, new MdmProtocolServiceTestMocks(false));
        CCI_SubjectVisitMappingController controller = new CCI_SubjectVisitMappingController();
        // Search Protocol
        callProtocolSearch(controller);
        system.assert(controller.protocolVO != null);
        system.assert(controller.protocolVO.clientProtocolNum.equals(controller.protocolText));
        
        // Search mappings
        Test.setMock(WebServiceMock.class, new MdmSvtServiceTestMocks(false));
        controller.praStudyId = 'MOCK STUDYVO';
        controller.searchAllData();
        system.assert(controller.svtVOs.size() == 5);
        String svtId;
        for (MdmSvtService.svtVO svt : controller.svtVOs) {
            svtId = svt.systemId;
            system.assert(svt.svtItems.size() == 10);
            system.assert(controller.svtEcrfWrapper.get(svt.systemId).ecrfItemVOs.size() == 20);
            system.assert(controller.svtEcrfWrapper.get(svt.systemId).visitMappings.size() == 20);
        }
        
        // Add new mapping data
        controller.keySvtId = svtId;
        list<selectoption> svtOptions = controller.svtEcrfWrapper.get(svtId).svtItemOptions;
        list<selectoption> ecrfOptions = controller.svtEcrfWrapper.get(svtId).ecrfItemOptions;
        controller.svtEcrfWrapper.get(svtId).selectedSVT = svtOptions.get(1).getValue();
        controller.svtEcrfWrapper.get(svtId).selectedECRF = ecrfOptions.get(1).getValue();
        controller.addScheduledMappingData();
        
        // Remove mapping data
        for (CCI_SubjectVisitMappingController.EcrfMappingWrapper mappingWrapper : controller.svtEcrfWrapper.get(svtId).visitMappings) {
            mappingWrapper.ecrfMappingVO.primary = true;
        }
        controller.saveScheduledMappingData();
        Test.stopTest();
    }
    
    static void callProtocolSearch(CCI_SubjectVisitMappingController controller) {
        controller.protocolText = 'clientprot-1';
        controller.searchProtocol();
    }
}