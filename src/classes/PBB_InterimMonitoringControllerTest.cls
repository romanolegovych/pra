/***
@author 
@date 2015
@description this is PBB_ActualDisplayController test class
**/

@isTest
private class PBB_InterimMonitoringControllerTest {
    static testMethod void testPBB_InterimMonitoringController()    {    
        test.startTest();   
            
        //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils ();
        tu.wfmp = tu.createWfmProject();        
        tu.bid =tu.createBidProject();      
        tu.scen =tu.createscenarioAttributes();        
        
        PBB_Scenario__c pscen =new PBB_Scenario__c();      
        System.assertEquals(null,PBB_DataAccessor.getScenarioByID(pscen.id)); 
        PBB_InterimMonitoringController IMC1 = new PBB_InterimMonitoringController(new Apexpages.Standardcontroller(pscen)); 
        Apexpages.currentPage().getParameters().put('PBBScenarioID', tu.scen.id);
        pscen =PBB_DataAccessor.getScenarioByID(tu.scen.id);        
        String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + pscen.Bid_Project__c ;       
        PBB_InterimMonitoringController IMC = new PBB_InterimMonitoringController(new Apexpages.Standardcontroller(pscen));      
        System.assertEquals(IMC.PBBScenarioID ,pscen.id);
        IMC.save();
        IMC.cancel();       
        test.stopTest();
   }       
}