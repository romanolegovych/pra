public class IPA_UI_NewsArchive
{
    //public List<IPA_Links__c> lstQuickLinks {get; set;}
    public List<IPA_Articles__c> newsArchive;
    public Boolean init_pg {get; set;}
    public Boolean last_pg {get; set;}
    public List<IPA_Articles__c> N3RecordsOfNews {get; set;} 
    public integer N3TotalRecord {get; set;}
    public integer N3LeftTd {get; set;}
    public integer N3RightTd {get; set;}  
    public integer N3Startind;  
    public boolean n3init_pg {get; set;} 
    public boolean n3last_pg {get; set;} 
    public List<IPA_Articles__c> LeftList{get; set;}
    public List<IPA_Articles__c> RightList{get; set;}
    public integer MaxRecordsonPage {get; set;}
        
    public IPA_UI_NewsArchive()
    {               
        MaxRecordsonPage = 12;
        IPA_BO_NewsArchive bo_news  = new IPA_BO_NewsArchive();
        //lstQuickLinks = bo_news.returnQuickLinks();
        
        try
        {
            //Local initialization
            string Type = ApexPages.currentPage().getParameters().get('Type');
            system.debug('****Type :' +Type);
            init_pg = true;
            last_pg = false;
            
            if(Type.equals('Industry Watch'))
            {
                N3RecordsOfNews = new List<IPA_Articles__c >();
                LeftList = new List<IPA_Articles__c >();
                RightList = new List<IPA_Articles__c >();
                newsArchive = bo_news.returnNewsOf3TypesArchive(Type);
                N3Startind = 1;
                commonWatch();
            }
        }
        catch(Exception e) {}
        
       //return null;
    }
    
    public void nextWatch()
    {
        N3Startind = N3Startind + N3totalrecord;
        N3RecordsOfNews.clear();
        n3init_pg = false;
        commonwatch();
    }
    
    public void preWatch()
    {
        N3Startind = N3Startind - N3totalrecord - 12;
        N3RecordsOfNews.clear();
        n3last_pg = false;
        commonwatch();
    }
    
    public void commonWatch()
    {
        integer N3iteretion = 1;
        integer countloop = 1;
        for(IPA_Articles__c looprecord : newsArchive)
        {
            if(countloop >= N3Startind){
            if(N3iteretion <= MaxRecordsonPage){
            N3RecordsOfNews.add(looprecord);
            N3iteretion++;}}
            countloop++;
        }
        
        N3totalrecord = N3RecordsOfNews.size();
        N3LeftTD = N3totalrecord/2;
        N3rightTD = N3totalrecord/2;        
        if(math.mod(N3totalrecord,2) == 1) N3LeftTD += 1;  
        
        N3iteretion = 1;
        countloop = 1;
        for(IPA_Articles__c looprecord : newsArchive)
        {
            if(countloop >= N3Startind){
            if(N3iteretion <= N3leftTD){
            LeftList.add(looprecord);
            N3iteretion++;}}
            countloop++;
        }
        system.debug('****MaxRecordsonPage: '+MaxRecordsonPage);
        system.debug('****N3leftTD  : '+N3leftTD);
        system.debug('****N3leftTD size   : '+LeftList.size());
        system.debug('****N3rightTD : '+N3rightTD);
        
        N3iteretion = 1;
        countloop = 1;
        for(IPA_Articles__c looprecord : newsArchive)
        {
            if(countloop >= N3Startind){
            if(N3iteretion > N3leftTD && N3iteretion <= MaxRecordsonPage){
            RightList.add(looprecord);
            }
            N3iteretion++;
            }
            countloop++;
        }
                       
        if(N3RecordsOfNews.Get(0).Id == newsArchive.Get(0).Id) n3init_pg = true;
        if(N3RecordsOfNews.Get(N3RecordsOfNews.size()-1).Id == newsArchive.Get(newsArchive.size()-1).Id) n3last_pg = true;
        
    }
}