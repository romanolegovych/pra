global with sharing class CCApi
//global with sharing class GantBuilderApiController extends Base
{
    public static Map<String, EWChain> earlyWarningsMap = new Map<String, EWChain>();
    public static String biFilter {get; set;}
    public Critical_Chain__c chain {get; set;}
    
    private transient List<SelectOption> availableCountries;
    public ID selectedCountry {get; set;}
    
    public List<SelectOption> biFilterOptions
    {
        get
        {
            return new List<SelectOption>{
                new SelectOption('', 'All'),
                new SelectOption('On Track', 'On Track'),
                new SelectOption('At Risk', 'At Risk'),
                new SelectOption('Delayed', 'Delayed')
            };
        }
    }
    
    public List<SelectOption> getAvailableCountries()
    {
        if (availableCountries == null)
        {
            availableCountries = new List<SelectOption>{new SelectOption('', '-- none --')};
            for (PAWS_Project_Flow_Country__c country : [select Name from PAWS_Project_Flow_Country__c where PAWS_Project__c = :pawsProject.Id order by Name])
            {
                availableCountries.add(new SelectOption(country.Id, country.Name));
            }
        }
        
        return availableCountries;
    }
    
    
    public ID selectedSite {get; set;}
    private transient List<SelectOption> availableSites;
    
    public List<SelectOption> getAvailableSites()
    {
        if (availableSites == null)
        {
            availableSites = new List<SelectOption>{new SelectOption('', '-- none --')};
            for (PAWS_Project_Flow_Site__c site : [select Name from PAWS_Project_Flow_Site__c where PAWS_Country__c = :selectedCountry order by Name])
            {
                availableSites.add(new SelectOption(site.Id, site.Name));
            }
        }
        
        return availableSites;
    }
    
    public EW_Aggregated_Milestone__c aggregatedMilestone
    {
        get
        {
            if (aggregatedMilestone == null)
            {
                aggregatedMilestone = new EW_Aggregated_Milestone__c();
                for (EW_Aggregated_Milestone__c each : [select Name, Milestone_Target_Date__c from EW_Aggregated_Milestone__c where Id = :milestoneID])
                {
                    aggregatedMilestone = each;
                }
            }
            
            return aggregatedMilestone;
        }
        set;
    }
    
    public ecrf__c pawsProject {get; set;}
    public String instanceID
    {
        get
        {
            if (instanceID == null)
            {
                instanceID = ApexPages.currentPage().getParameters().get('instanceID') != null
                    ? ApexPages.currentPage().getParameters().get('instanceID')
                    : ApexPages.currentPage().getParameters().get('id');
            }
            
            return instanceID;
        }
        private set;
    }
    
    public String milestoneID {get; set;}
    
    public List<SelectOption> aggregatedMilestoneOptions
    {
        get
        {
            if (aggregatedMilestoneOptions == null)
            {
                aggregatedMilestoneOptions = new List<SelectOption>{new SelectOption('', 'none selected')};
                for (EW_Aggregated_Milestone__c aggregatedMilestone : [select Name from EW_Aggregated_Milestone__c where PAWS_Project_Flow__c = :pawsProject.Id])
                {
                    aggregatedMilestoneOptions.add(new SelectOption(('' + aggregatedMilestone.Id).substring(0, 15), aggregatedMilestone.Name));
                }
            }
            
            return aggregatedMilestoneOptions;
        }
        private set;
    }
    
    private static Map<String, Decimal> chainIDBufferIndex;
    private static Map<String, Datetime> milestoneDates = new Map<String, Datetime>();
    private static String aggregatedMilestoneID;
    public CCApi(ApexPages.StandardController controller)
    {
        if (controller.getRecord() instanceof Critical_Chain__c)
        {
            chain = (Critical_Chain__c) controller.getRecord();
        }
        else if (controller.getRecord() instanceof ecrf__c)
        {
            pawsProject = (ecrf__c) controller.getRecord();
        }
        
        if (milestoneID == null && ApexPages.currentPage() != null)
        {
            milestoneID = ApexPages.currentPage().getParameters().get('milestoneID');
        }
        if (ApexPages.currentPage().getParameters().get('instanceID') != null)
        {
            selectFilters();
        } 
    }
    
    public void selectFilters() 
    {
        for (PAWS_Project_Flow_Site__c siteObj : [select ID, Name, PAWS_Country__c from PAWS_Project_Flow_Site__c where id =: instanceID])
        {
            selectedCountry = siteObj.PAWS_Country__c;
            onCountryChange();
            selectedSite = siteObj.ID;
        }     
    } 
    
    public void onMilestoneChange()
    {
        //do nothing
        aggregatedMilestone = null;
    }
    
    public void changeMilestoneTargetDate()
    {
        update aggregatedMilestone;
    }
    
    public void onCountryChange()
    {
        availableSites = null;
        //do nothing
    }
    
    public void onFilter()
    {
        if (selectedSite != null)
        {
            instanceID = selectedSite;
        }
        else if (selectedCountry != null)
        {
            instanceID = selectedCountry;
        }
        else
        {
            instanceID = pawsProject.Id;
        }
    }
    
    public CCApi() {/*just a constructor*/}
    public String response {get; private set;}
    public void execute()
    {
        response = loadFlowHierarchy(ApexPages.currentPage().getParameters().get('flowID')).Data;
    }
    
    @RemoteAction
    global static Object loadFlows(List<String> IDs)
    {
        return STSWR1.GantBuilderApiController.loadFlows(IDs);
    }
    
    @RemoteAction
    global static Object loadDashboard(String instanceID/*projectID*/, String milestoneID, Map<String, Object> additionalParams)
    {
        aggregatedMilestoneID = milestoneID;
        AsyncResult result = new AsyncResult(true);
        
        String projectID;
        for (Critical_Chain__c eachChain : [select PAWS_Project_Flow__c from Critical_Chain__c where PAWS_Project_Flow__c = :instanceID or PAWS_Project_Flow_Country__c = :instanceID or PAWS_Project_Flow_Site__c = :instanceID or PAWS_Project_Flow_Site__r.PAWS_Country__c = :instanceID limit 1])
        {
            projectID = eachChain.PAWS_Project_Flow__c;
        }
        
        for (ecrf__c eachProject : [select Id, Name from ecrf__c where Id = :projectID])
        {
            result.project = eachProject;
            List<String> chainIDs = new List<String>();
            //for (Critical_Chain__c eachChain : [select Id, Flow_Instance__r.STSWR1__Flow__c from Critical_Chain__c where Flow_Instance__c in (select Id from STSWR1__Flow_Instance__c where PAWS_Project_Flow__c = :instanceID)])
            for (Critical_Chain__c eachChain : milestoneID != null && milestoneID != '' ? [select Id, Flow__c from Critical_Chain__c where PAWS_Project_Flow__c = :projectID
                        and Id in (select Early_Warning__c from EW_Aggregated_Milestone_EW_Junction__c where Aggregated_Milestone__c = :milestoneID and Aggregated_Milestone__r.PAWS_Project_Flow__c = :projectID)]
                    : [select Id, Flow__c from Critical_Chain__c where PAWS_Project_Flow__c = :instanceID or PAWS_Project_Flow_Country__c = :instanceID or PAWS_Project_Flow_Site__c = :instanceID or PAWS_Project_Flow_Site__r.PAWS_Country__c = :instanceID/*PAWS_Project_Flow__c = :projectID*/])
            {
                chainIDs.add(eachChain.Id);
                if (result.data == null)
                {
                    result.data = System.Json.serialize(loadHierarchy(eachChain.Id, false));
                }
            }
            
            Map<String, Object> ccData = loadCCData(chainIDs);
            
            result.chains = new List<Object>();
            List<Critical_Chain__c> chains = selectChains(chainIDs);
            for (Critical_Chain__c chain : chains)
            {
                EWChain ew = earlyWarningsMap.get(chain.Id);
                if (ew == null)
                {
                    continue;
                }
                
                Map<String, Object> jsonChain = (Map<String, Object>) System.Json.deserializeUntyped(System.Json.serialize(chain));
                //jsonChain.put('Buffer_Index__c', chainIDBufferIndex.get(chain.Id));
                Decimal bufferIndex = 0;
                Datetime milestoneDate = ew.bufferEnd;
                if (milestoneID != null && milestoneID != '' && milestoneDates.get(chain.Milestone__c) != null)
                {
                    milestoneDate = milestoneDates.get(chain.Milestone__c);
                    //bufferIndex = ew.calculateBufferIndex(milestoneDate);
                    ew.applyMilestoneDate(milestoneDate);
                    bufferIndex = ew.calculateBufferIndex();
                }
                else
                {
                    bufferIndex = ew.calculateBufferIndex();
                }
                
                jsonChain.putAll(new Map<String, Object>{
                    'Buffer_Index__c' => bufferIndex,
                    'Milestone_Target_Date__c' => '' + milestoneDate
                });
                
                if (includeChain(jsonChain, additionalParams.get('biFilter')))
                {
                    result.chains.add(jsonChain);
                }
            }
            
            result.ccData = System.Json.serialize(cleanupCCData(ccData, new Map<ID, Critical_Chain__c>(chains).keyset()));
        }
        /**/
        
        System.debug(System.LoggingLevel.ERROR, 'Result: ' + System.Json.serialize(result));//@TODO: Delete this line!
        
        return result;
    }
    
    private static Boolean includeChain(Map<String, Object> jsonChain, Object filter)
    {
        Decimal bi = (Decimal) jsonChain.get('Buffer_Index__c');
        return filter == 'Delayed' && bi < 0
                || filter == 'At Risk' && bi >=0 && bi < 1
                || filter == 'On Track' && bi >= 1
                || filter == '' || filter == null;
    }
    
    private static Map<String, Object> cleanupCCData(Map<String, Object> ccData, Set<ID> chainIDs)
    {
        Set<String> keys = ccData.keyset();
        for (String key : keys)
        {
            String ccID = (key != null ? key.substring(0, Math.min(18, key.length())) : null);
            if (!chainIDs.contains(ccID))
            {
                ccData.remove(key);
            }
        }
        
        return ccData;
    }
    
    private static List<Critical_Chain__c> selectChains(List<String> chainIDs)
    {
        return [select Name, Flow__r.Name, Flow__c, Milestone__c, Milestone__r.Name, Flow_Instance__c, Flow_Instance__r.PAWS_Project_Flow__c, Flow_Instance__r.PAWS_Project_Flow__r.Name, Flow_Instance__r.STSWR1__Flow__c, Buffer_Index__c, Aggregated_Challenging_Time__c, Aggregated_Buffer_Time__c, Chain_Owner__c, BI_Explanation__c
                from Critical_Chain__c where Id in :chainIDs];
    }
    
    public static String vfLoadFlow(ID flowID)
    {
        PageReference apiPage = Page.CC_Request;
        apiPage.getParameters().putAll(new Map<String, String>{'flowID' => flowID});
        
        String result = (Test.isRunningTest() ? null : apiPage.getContent().toString());
        return (result == null || result == '' ? '{}' : result);
    }
    
    private static Map<String, Object> loadHierarchy(ID chainID, Boolean hierarchyOnly)
    {
        Map<String, Object> hierarchy = new Map<String, Object>();
        for (Critical_Chain__c chain : selectChains(new List<String>{chainID}))
        {
            hierarchy = (Map<String, Object>) System.Json.deserializeUntyped(vfLoadFlow(chain.Flow__c));
            continue;//Do not load subhierarchy yet.
            /*
            for (Critical_Chain__c subchain : [select Flow__c from Critical_Chain__c where Parent__c = :chain.Id])
            {
                Map<String, Object> subHierarchy = loadHierarchy(subchain.Id, true);
                String sourcestepName;
                String sourcestepID;
                for (STSWR1__Flow_Step_Property__c customProperty : [select STSWR1__Value__c from STSWR1__Flow_Step_Property__c where Name = 'Connection Point'
                        and STSWR1__Flow_Step__r.STSWR1__Flow__c = :subchain.Flow__c])
                {
                    sourcestepName = customProperty.STSWR1__Value__c.split(':').get(1);
                }
                
                for (Object parentStep : (List<Object>) new XMap(hierarchy).get('hierarchy.steps'))
                {
                    if (((Map<String, Object>) parentStep).get('Name') == sourcestepName)
                    {
                        sourcestepID = '' + ((Map<String, Object>) parentStep).get('Id');
                        break;
                    }
                }
                
                subHierarchy.put('sourcestep', new List<String>{sourcestepID});
                
                ((List<Object>) ((Map<String, Object>) hierarchy.get('hierarchy')).get('childflows')).add(subHierarchy);
            }
            /**/
        }
        
        return (hierarchyOnly ? (Map<String, Object>) hierarchy.get('hierarchy') : hierarchy);
    }
    
    @RemoteAction
    global static Object loadChain(String chainID)
    {
        AsyncResult result = new AsyncResult(true);
        
        for (Critical_Chain__c chain : selectChains(new List<String>{chainID}))
        {
            result.chain = chain;
            result.ccData = System.Json.serialize(loadCCData(new List<String>{chainID}));
            result.Data = System.Json.serialize(loadHierarchy(chainID, false));
        }
        
        return result;
    }
    
    @RemoteAction
    global static AsyncResult saveChainChanges(String objectJSON) 
    {
        try
        {
            Critical_Chain__c chain = (Critical_Chain__c)JSON.Deserialize(objectJSON, Critical_Chain__c.class);
            update chain;
            
            return new AsyncResult(true);
        }catch(Exception ex)
        {
            return new AsyncResult(ex);
        }
    }
    
    @RemoteAction
    global static AsyncResult saveCCJunctionChanges(String objectJSON) 
    {
        try
        {
            Critical_Chain_Step_Junction__c CCJunction = (Critical_Chain_Step_Junction__c)JSON.Deserialize(objectJSON, Critical_Chain_Step_Junction__c.class);
            update CCJunction;
            
            return new AsyncResult(true);
        }catch(Exception ex)
        {
            return new AsyncResult(ex);
        }
    }
    
    @RemoteAction
    global static Map<String, Object> doAssigneeSearch(String text, List <String> userIDs) 
    {     
        if(userIDs != null) 
        {
            String param = String.Join(userIDs, '\',\'');
            return STSWR1.LookupInputController.doSearch('User', 'Name', text, '', 'ID IN (\'' + param + '\')', '', false);
        }
        else 
        {
            return STSWR1.LookupInputController.doSearch('User', 'Name', text, '', '', '', false);
        }
    }
    
    
    
    @testvisible private static Map<String, Object> loadCCDataOptimized(List<String> chainIDs)
    {
        chainIDBufferIndex = new Map<String, Decimal>();
        List<EWChain> ewChains = EWChain.buildEWChains(chainIDs);
        Map<String, Object> tempMap = new Map<String, Object>();
        EW_Aggregated_Milestone__c aggregatedMilestone = selectAggregatedMilestone(aggregatedMilestoneID);
        
        for (EWChain earlyWarningChain : ewChains)
        {
            earlyWarningsMap.put(earlyWarningChain.chainRecord.Id, earlyWarningChain);
            
            for (EWStep earlyWarningStep : earlyWarningChain.steps)
            {
                tempMap.put('' + earlyWarningStep.ccJunction.Critical_Chain__c + earlyWarningStep.ccJunction.Flow_Step__c, earlyWarningStep.toDTO());
            }
            
            if (aggregatedMilestone != null && aggregatedMilestone.Milestone_Target_Date__c != null)
            {
                milestoneDates.put(earlyWarningChain.chainRecord.Milestone__c, aggregatedMilestone.Milestone_Target_Date__c);
            }
            else if (milestoneDates.get(earlyWarningChain.chainRecord.Milestone__c) == null
                || milestoneDates.get(earlyWarningChain.chainRecord.Milestone__c) < earlyWarningChain.bufferEnd)
            {
                milestoneDates.put(earlyWarningChain.chainRecord.Milestone__c, earlyWarningChain.bufferEnd);
            }
        }
        
        return tempMap;
    }
    
    private static EW_Aggregated_Milestone__c selectAggregatedMilestone(String milestoneID)
    {
        for (EW_Aggregated_Milestone__c aggregatedMilestone : [select Name, Milestone_Target_Date__c from EW_Aggregated_Milestone__c where Id = :milestoneID])
        {
            return aggregatedMilestone;
        }
        
        return null;
    }
    
    @testvisible private static Map<String, Object> loadCCData(List<String> chainIDs)
    {
        return loadCCDataOptimized(chainIDs);
    }
    
    @RemoteAction
    global static Object saveChain(AsyncResult chainData)
    {
        AsyncResult result = new AsyncResult(true);
        System.Savepoint transactionSavepoint = Database.setSavepoint();
        try
        {
            Map<String, Critical_Chain_Step_Junction__c> junctionMap =
                (Map<String, Critical_Chain_Step_Junction__c>) System.Json.deserialize(chainData.ccData, Map<String, Critical_Chain_Step_Junction__c>.class);
            
            List<Critical_Chain_Step_Junction__c> junctions = junctionMap.values();
            for (Critical_Chain_Step_Junction__c junction : junctions)
            {
                junction.Critical_Chain__c = chainData.chain.Id;
            }
            
            upsert junctions;
            
            delete [select Name from Critical_Chain_Step_Junction__c where Critical_Chain__c = :chainData.chain.Id and Id not in :new Map<ID, Critical_Chain_Step_Junction__c>(junctions).keyset()];
        }
        catch (Exception e)
        {
            result.Errors = new List<String>{e.getMessage()};
            Database.rollback(transactionSavepoint);
        }
        
        return result;
    }
    
    global class AsyncResult
    {
        public AsyncResult()
        {
            this.Complete = false;
            this.Errors = new List<String>();
        }
        
        public AsyncResult(Boolean complete)
        {
            this.Complete = complete;
            this.Errors = new List<String>();
        }
        
        public AsyncResult(String error)
        {
            this.Complete = true;
            this.Errors = new List<String>{error};
        }
        
        public AsyncResult(Exception ex)
        {
            this.Complete = true;
            this.Errors = new List<String>{ex.getMessage()};
        }
        
        public Boolean Complete {get;set;}
        public List<String> Errors {get;set;}
        public List<String> ProcessIds {get;set;}
        public List<String> AllProcessIds {get;set;}
        public String Data {get;set;}
        public String ccData {get; set;}
        public Critical_Chain__c chain {get; set;}
        public List<Object> chains {get; set;}
        public ecrf__c project {get; set;}
    }
    
    public class CCException extends Exception {}
    /*
    public class XMap
    {
        public Map<String, Object> holder {get; private set;}
        public String separator {get; private set;}
        
        public XMap(Map<String, Object> holder, String separator)
        {
            if (holder == null) {
                throw new CCException('XMap.<constructor>(): Argument holder:Map<String, Object> must not be null: ' + holder);
            }
            
            this.holder = holder;
            this.separator = separator;
        }
        
        public XMap(Map<String, Object> holder)
        {
            this(holder, '\\.');
        }
        
        public Object get(String key)
        {
            Map<String, Object> source = holder;
            for (List<String> parts = ('' + key).split(separator, 2); key != null && source != null && parts.size() == 2; parts = ('' + key).split(separator, 2))
            {
                if (!(source.get(parts.get(0)) instanceof Map<String, Object>)) {
                    throw new CCException('Unable to found Map<String, Object> at: ' + parts.get(0));
                }
                
                source = (Map<String, Object>) source.get(parts.get(0));
                key = parts.get(1);
            }
            
            return source.get(key);
        }
        
        public void put(String key, Object value)
        {
            holder.put(key, value);
        }
    }
    /**/
    //Deprecated method fix
    public static AsyncResult loadFlowHierarchy(String recordId)
    {
        try
        {
            STSWR1__Flow__c flow = [select STSWR1__Object_Type__c, STSWR1__Type__c, STSWR1__Admins__c, STSWR1__Source_Id__c from STSWR1__Flow__c where Id = :recordId];
            
            Set<ID> flowsIds = new Set<ID>();
            Map<ID, Map<ID, ID>> hierarchyDown = new Map<ID, Map<ID, ID>>();
            Map<ID, Map<ID, ID>> hierarchyUp = new Map<ID, Map<ID, ID>>();
            
            ID rootFlowId = flow.Id;
            if(flow.STSWR1__Type__c == 'Template') 
            {
                flowsIds.add(rootFlowId);
                loadFlowHierarchyByConnectionPoint(flow.Id, flowsIds, hierarchyDown, hierarchyUp);
                loadFlowHierarchyBySubFlows(flowsIds, hierarchyDown, hierarchyUp);

                while(hierarchyUp.containsKey(rootFlowId))
                    rootFlowId = new List<ID>(hierarchyUp.get(rootFlowId).keySet())[0];
            }else 
            {
                rootFlowId = getParentLevelFlow(flow);
                flowsIds.add(rootFlowId);
                loadFlowHierarchyBySubFlows(flowsIds, hierarchyDown, hierarchyUp);
                
                ID tempParentId = flow.Id;
                ID tempChildId = flow.Id;
                while(hierarchyUp.containsKey(tempParentId))
                {
                    tempChildId = tempParentId;
                    tempParentId = new List<ID>(hierarchyUp.get(tempParentId).keySet())[0];

                    if(hierarchyDown.containsKey(tempParentId))
                    {
                        Map<ID, ID> mp = hierarchyDown.get(tempParentId);
                        ID parentStepId = mp.get(tempChildId);
                        for(ID childId : mp.keySet())
                        {
                            if(childId != tempChildId && mp.get(childId) == parentStepId)
                                mp.remove(childId);
                        }
                    }
                }
            }

            flowsIds = removeInvalidIds(flowsIds);
            Set<ID> usedIds = new Set<ID>{rootFlowId};
            Map<String, Object> resultData = new Map<String, Object>{
                'ids' => flowsIds,
                'hierarchy' => new Map<String, Object>{
                    'id' => rootFlowId,
                    'childflows' => loadFlowsHierarchyDown(hierarchyDown.get(rootFlowId), usedIds, hierarchyDown, flowsIds),
                    'parentflows' => loadFlowsHierarchyUp(hierarchyUp.get(rootFlowId), usedIds, hierarchyUp, flowsIds)
                }
            };
            AsyncResult result = new AsyncResult(true);
            result.Data = JSON.serialize(resultData).stripHtmlTags();
            return result;
        }
        catch(Exception ex)
        {
            if(Test.isRunningTest()) throw ex;
            return new AsyncResult(ex);
        }
    }
    
    @testvisible private static List<Object> loadFlowsHierarchyDown(Map<ID, ID> ids, Set<ID> usedIds, Map<ID, Map<ID, ID>> hierarchyDown, Set<ID> allFlowsIds)
    {
        List<Object> result = new List<Object>();
        if(ids == null || ids.size() == 0) return result;
        for(ID flowId : ids.keySet())
        {
            if(usedIds.contains(flowId) || !allFlowsIds.contains(flowId)) continue;
            usedIds.add(flowId);
            result.add(new Map<String, Object>{
                'id' => flowId,
                'sourcestep' => new List<ID> { ids.get(flowId) },
                'childflows' => loadFlowsHierarchyDown(hierarchyDown.get(flowId), usedIds, hierarchyDown, allFlowsIds)
            });
        }
        return result;
    }
    
    @testvisible private static List<Object> loadFlowsHierarchyUp(Map<ID, ID> ids, Set<ID> usedIds, Map<ID, Map<ID, ID>> hierarchyUp, Set<ID> allFlowsIds)
    {
        List<Object> result = new List<Object>();
        if(ids == null || ids.size() == 0) return result;
        for(ID flowId : ids.keySet())
        {
            if(usedIds.contains(flowId) || !allFlowsIds.contains(flowId)) continue;
            usedIds.add(flowId);
            result.add(new Map<String, Object>{
                'id' => flowId,
                'sourcestep' => new List<ID> { ids.get(flowId) },
                'parentflows' => loadFlowsHierarchyUp(hierarchyUp.get(flowId), usedIds, hierarchyUp, allFlowsIds)
            });
        }
        return result;
    }
    private static Set<ID> removeInvalidIds(Set<ID> ids)
    {
        Map<ID, STSWR1__Flow__c> flowsMap = new Map<ID, STSWR1__Flow__c>([select Id from STSWR1__Flow__c where Id in :ids]);
        return flowsMap.keySet();
    }
    
    public static void loadFlowHierarchyByConnectionPoint(String recordId, Set<ID> flowIds, Map<ID, Map<ID, ID>> hierarchyDown, Map<ID, Map<ID, ID>> hierarchyUp)
    {
        STSWR1__Item__c flowItem = [select STSWR1__Path__c, STSWR1__Parent__c, STSWR1__Parent__r.STSWR1__Parent__c, STSWR1__Parent__r.STSWR1__Parent__r.STSWR1__Parent__c, STSWR1__Source_Flow__r.STSWR1__Object_Type__c, STSWR1__Source_Flow__r.STSWR1__Type__c from STSWR1__Item__c where STSWR1__Source_Flow__c = :recordId];
        if(flowItem.STSWR1__Source_Flow__r.STSWR1__Type__c != 'Template') return;
        
        String projectFolderId = null, countryFolderId = null, siteFolderId = null, documentFolderId = null;
        if(flowItem.STSWR1__Source_Flow__r.STSWR1__Object_Type__c == 'ecrf__c')
        {
            projectFolderId = countryFolderId = siteFolderId = documentFolderId = flowItem.STSWR1__Parent__c;
        }else{
            if(flowItem.STSWR1__Parent__r != null && flowItem.STSWR1__Parent__r.STSWR1__Parent__r != null) projectFolderId = flowItem.STSWR1__Parent__r.STSWR1__Parent__r.STSWR1__Parent__c;
            countryFolderId = siteFolderId = documentFolderId = flowItem.STSWR1__Parent__c;
        }
        
        if(projectFolderId == null) return;
            
        String query = 'select STSWR1__Source_Flow__c, STSWR1__Source_Flow__r.Name from STSWR1__Item__c where';
        query += ' (STSWR1__Source_Flow__r.STSWR1__Object_Type__c = \'ecrf__c\' and STSWR1__Path__c like \'%' + projectFolderId + '%\')';
        if(countryFolderId != null) query += ' or (STSWR1__Source_Flow__r.STSWR1__Object_Type__c = \'paws_project_flow_country__c\' and STSWR1__Path__c like \'%' + countryFolderId + '%\')';
        if(siteFolderId != null) query += ' or (STSWR1__Source_Flow__r.STSWR1__Object_Type__c = \'paws_project_flow_site__c\' and STSWR1__Path__c like \'%' + siteFolderId + '%\')';
        if(documentFolderId != null) query += ' or (STSWR1__Source_Flow__r.STSWR1__Object_Type__c in (\'paws_project_flow_document__c\',\'paws_project_flow_agreement__c\',\'paws_project_flow_submission__c\') and STSWR1__Path__c like \'%' + documentFolderId + '%\')';
        
        System.debug(query);
        Map<String, ID> flowsNameIdsMap = new Map<String, ID>();
        for(STSWR1__Item__c record : (List<STSWR1__Item__c>)Database.query(query))
            flowsNameIdsMap.put(record.STSWR1__Source_Flow__r.Name, record.STSWR1__Source_Flow__c);
            
        Map<String, ID> stepsNameIdsMap = new Map<String, ID>();
        for(STSWR1__Flow_Step_Junction__c record : [select Name, STSWR1__Flow__c from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__c in :flowsNameIdsMap.values()])
            stepsNameIdsMap.put(record.STSWR1__Flow__c + record.Name, record.Id);

        for(STSWR1__Flow_Step_Property__c record : [select STSWR1__Flow_Step__c, STSWR1__Flow_Step__r.STSWR1__Flow__c, STSWR1__Value__c from STSWR1__Flow_Step_Property__c where STSWR1__Flow_Step__r.STSWR1__Flow__c in :flowsNameIdsMap.values() and Name = 'Connection Point' and STSWR1__Flow_Step__r.STSWR1__Is_First_Step__c = true])
        {
            List<String> items = record.STSWR1__Value__c.split(':');
            if(items.size() < 2) continue;
            
            ID flowId = flowsNameIdsMap.get(items[0]);
            if(flowId == null) continue;
            
            ID flowStepId = stepsNameIdsMap.get(flowId + items[1]);
            if(flowStepId == null) continue;
            
            if(!hierarchyDown.containsKey(flowId)) hierarchyDown.put(flowId, new Map<ID, ID>());
            hierarchyDown.get(flowId).put(record.STSWR1__Flow_Step__r.STSWR1__Flow__c, flowStepId);

            if(!hierarchyUp.containsKey(record.STSWR1__Flow_Step__r.STSWR1__Flow__c)) hierarchyUp.put(record.STSWR1__Flow_Step__r.STSWR1__Flow__c, new Map<ID, ID>());
            hierarchyUp.get(record.STSWR1__Flow_Step__r.STSWR1__Flow__c).put(flowId, flowStepId);
            
            flowIds.add(record.STSWR1__Flow_Step__r.STSWR1__Flow__c);
            flowIds.add(flowId);
        }
    }
    
    public static void loadFlowHierarchyBySubFlows(Set<ID> flowIds, Map<ID, Map<ID, ID>> hierarchyDown, Map<ID, Map<ID, ID>> hierarchyUp)
    {
        Set<ID> subFlowsIds = new Set<ID>(flowIds);
        Set<ID> temp = new Set<ID>();
        while(true)
        {
            temp.clear();

            for(STSWR1__Flow_Step_Action__c action : [select STSWR1__Flow_Step__c, STSWR1__Flow_Step__r.STSWR1__Flow__c, STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Flow_Step__r.STSWR1__Flow__c in :subFlowsIds and STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active'])
            {
                try
                {
                    Map<String, Object> config = (Map<String, Object>)JSON.deserializeUntyped(action.STSWR1__Config__c);
                    if(config != null && !String.isEmpty((String)config.get('flowId')) && !flowIds.contains((ID)config.get('flowId'))) 
                    {
                        ID flowId = (ID)config.get('flowId');
                        ID flowStepId = (config.get('lockParent') == 'true' && !String.isEmpty((String)config.get('lockStepId')) ? (String)config.get('lockStepId') : action.STSWR1__Flow_Step__c);
                        temp.add(flowId);
                        
                        if(!hierarchyDown.containsKey(action.STSWR1__Flow_Step__r.STSWR1__Flow__c)) hierarchyDown.put(action.STSWR1__Flow_Step__r.STSWR1__Flow__c, new Map<ID, ID>());
                        hierarchyDown.get(action.STSWR1__Flow_Step__r.STSWR1__Flow__c).put(flowId, flowStepId);
                        
                        if(!hierarchyUp.containsKey(flowId)) hierarchyUp.put(flowId, new Map<ID, ID>());
                        hierarchyUp.get(flowId).put(action.STSWR1__Flow_Step__r.STSWR1__Flow__c, flowStepId);
                    }
                }catch(Exception ex){}
            }
                
            if(temp.size() == 0) break;
                
            flowIds.addAll(temp);
            subFlowsIds = new Set<ID>(temp);
        }
    }
    
    @testvisible private static ID getParentLevelFlow(STSWR1__Flow__c flow)
    {
        if(String.isEmpty(flow.STSWR1__Source_Id__c)) return flow.Id;

        ID sourceId = (ID)flow.STSWR1__Source_Id__c;
        Set<ID> sourcesIds = new Set<ID>();
        if(sourceId.getSObjectType() == PAWS_Project_Flow_Site__c.getSObjectType())
        {
            List<PAWS_Project_Flow_Site__c> records = [select PAWS_Project__c, PAWS_Country__c from PAWS_Project_Flow_Site__c where Id = :sourceId];
            if(records.size() > 0) sourcesIds.addAll(new Set<ID>{records[0].PAWS_Country__c, records[0].PAWS_Project__c});
        }else if(sourceId.getSObjectType() == PAWS_Project_Flow_Country__c.getSObjectType())
        {
            List<PAWS_Project_Flow_Country__c> records = [select PAWS_Project__c from PAWS_Project_Flow_Country__c where Id = :sourceId];
            if(records.size() > 0) sourcesIds.add(records[0].PAWS_Project__c);
        }
        
        Map<ID, ID> childParentMap = new Map<ID, ID>();
        for(STSWR1__Flow_Step_Action__c action : [select STSWR1__Flow_Step__c, STSWR1__Flow_Step__r.STSWR1__Flow__c, STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Flow_Step__r.STSWR1__Flow__r.STSWR1__Source_Id__c in :sourcesIds and STSWR1__Flow_Step__r.STSWR1__Flow__r.STSWR1__Type__c = 'One Time' and STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active'])
        {
            try
            {
                Map<String, Object> config = (Map<String, Object>)JSON.deserializeUntyped(action.STSWR1__Config__c);
                if(config != null && !String.isEmpty((String)config.get('flowId'))) 
                    childParentMap.put((ID)config.get('flowId'), action.STSWR1__Flow_Step__r.STSWR1__Flow__c);
            }catch(Exception ex){}
        }
        
        ID rootFlowId = flow.Id;
        while(childParentMap.containsKey(rootFlowId))
            rootFlowId = childParentMap.get(rootFlowId);
            
        return rootFlowId;
    }
}