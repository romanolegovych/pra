public class CCI_EdcMappingController {

	public String instName { get; set; }
	public String protNum { get; set; }
	public String studyId { get; set; }
	public String projId { get; set; }
	public String saveStudyId {get; set;}
	public String saveProtNum {get;set;}
	public String saveInstName {get;set;}
	public String selectedButton { get; set; }
	public MdmProtocolService.protocolVO protocolVO {get;set;}	
	public String selectedProtProjId {get;set;}	
	public String selectedProtSiteId {get;set;}
	public List<MdmMappingService.protocolSiteVO> protocolVOs {get;set;}
	public MdmMappingService.protocolSiteVO selectedProtSiteVO {get;set;}
	public Map<String, MdmMappingService.protocolSiteVO> protSiteVOMap {get;set;}
	public Map<String, MdmMappingService.instanceVO> instanceVOMap {get;set;}
	
	public CCI_EdcMappingController() {
		instanceVOMap = new Map<String, MdmMappingService.instanceVO>();
		instanceVOMap.put(null, null);
	}
	
	public List<MdmMappingService.protocolSiteVO> getProtocolVOs() {
		return protocolVOs;
	}
	
	public void findProtSiteVO() {
		if (selectedProtSiteId != null) {
			selectedProtSiteVO = new MdmMappingService.protocolSiteVO();
			selectedProtSiteVO = protSiteVOMap.get(selectedProtSiteId);
		}
	}
	
	public void searchProtocol() {
		MdmProtocolService.protocolResponse response = MdmProtocolServiceWrapper.getProtocolVOByClientProtNum(String.escapeSingleQuotes(saveProtNum));
		protocolVO = response.protocolVO;
	}
	
	public list<selectoption> getClientProjectsFromProtocol() {
		transient List<selectoption> options = new List<SelectOption>(); 
		if (protocolVO != null && protocolVO.studyVOs != null) {
			list<MdmProtocolService.studyVO> studyVOs = protocolVO.studyVOs;
			options.add(new selectoption('', '--- Select a Study Identifier ---'));
			for (MdmProtocolService.studyVO cp : studyVOs) {
				options.add(new selectoption(cp.praId, cp.praId));
			}
		} else {
			options.add(new selectoption('',''));
		}
		return options;
	}

	public list<selectoption> getInstances()
	{
		MdmMappingService.valueListResposne resp=  MdmMappingServiceWrapper.getAllCrsInstances();
		List<SelectOption> options = new List<SelectOption>();
		options.add(new selectoption('', '--- Select an Instance Name ---'));
		for(String c : resp.values)
		{
			options.add(new selectoption(c,c));
		}
		return options;
	}

	public PageReference reset() {
		return null;
	}

	public void search() {
		Long projectId = 0L;
		
		if(projId != NULL && projId != '')
		{
			projectId=Long.valueOf(projId);
		} 
		MdmMappingService.protocolSiteListResponse resp= MdmMappingServiceWrapper.getProtocolMappingSitesSearch(projectId ,studyId,protNum,instName);
		protocolVOs=new List<MdmMappingService.protocolSiteVO>();
		protocolVOs=resp.protocolSiteVOs;       
		if (protocolVOs != null && protocolVOs.size() > 0) {
			protSiteVOMap = new Map<String, MdmMappingService.protocolSiteVO>();
			for (MdmMappingService.protocolSiteVO protSiteVO : protocolVOs) {
				protSiteVOMap.put(String.valueOf(protSiteVO.siteId), protSiteVO);
				instanceVOMap.put(String.valueOf(protSiteVO.siteId), protSiteVO.instanceVO);
			}
		}
	}
	
	public void Add()
	{
		ApexPages.getMessages().clear();
		MdmMappingService.protocolSiteVO protVO;
		
		if(saveStudyId != NULL && saveProtNum != NULL && saveInstName!=NULL )
		{
			protVO=new MdmMappingService.protocolSiteVO();
			protVO.siteId = 0;
			protVO.protProjId = 0;
			protVO.protocolNo = saveProtNum;
			protVO.praStudyId = saveStudyId;			
			protVO.instanceName=saveInstName;
			protVO.source= UserInfo.getName();
		}
		MdmMappingService.mdmCrudResponse response = MdmMappingServiceWrapper.saveCrsInstClientProjFromVO(protVO);    
		if (response.operationStatus.equals('0')) {
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, 'Protocol Mapping saved successful'));
		} else {
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Protocol Mapping save failed with errors: ' + response.errors));
		}
	}
	
	public void save() {
		ApexPages.getMessages().clear();
		if (selectedProtSiteVO != null) {
			selectedProtSiteVO.source = UserInfo.getName();
			MdmMappingService.mdmCrudResponse response = MdmMappingServiceWrapper.saveCrsInstClientProjFromVO(selectedProtSiteVO);
			if (response.operationStatus.equals('0')) {
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, 'Protocol Mapping saved successful'));
			} else {
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Protocol Mapping save failed with errors: ' + response.errors));
			}
		}
	}
	
	public void deleteMapping() {
		if (selectedProtSiteVO != null) {
			MdmMappingService.mdmCrudResponse response = MdmMappingServiceWrapper.deleteCrsInstClientProjFromVO(selectedProtSiteVO);
			if (response.operationStatus.equals('0')) {
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, 'Protocol Mapping delete successful'));
			} else {
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Protocol Mapping delete failed with errors: ' + response.errors));
			}
		}
	}

}