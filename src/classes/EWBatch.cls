global class EWBatch implements Database.Batchable<SObject>, Database.Stateful, Database.AllowsCallouts, Schedulable
{
	public static String BASE_QUERY = 'select Id from Critical_Chain__c where Flow__c not in (select STSWR1__Flow__c from STSWR1__Flow_Instance__c where (STSWR1__Is_Completed__c = \'Yes\' and LastModifiedDate < YESTERDAY))';
	public static Integer NUMBER_OF_IDLE_DAYS = 3;
	
	public static void scheduleMe()
	{
		System.schedule('Early Warnings Batch', '0 0 0 * * ?', new EWBatch(BASE_QUERY));
	}
	
	global void execute(SchedulableContext context)
	{
		Database.executeBatch(this, 1);
		//executeBatch();
	}
	
	public String query;
	
	public static void executeBatch()
	{
		Database.executeBatch(new EWBatch(BASE_QUERY), 1);
	}
	
	public EWBatch(String query)
	{
		this.query = query;
	}
	
	global virtual Database.QueryLocator start(Database.BatchableContext bc)
	{
		return Database.getQueryLocator(query);
	}
	
	private void handleException(Exception e)
	{
		System.debug(System.LoggingLevel.ERROR, 'EWBatch.execute: ' + e.getMessage() + '\nStacktrace: ' + e.getStacktraceString());
	}
	
	global virtual void execute(Database.BatchableContext BC, List<SObject> scope)
	{
		List<ID> chainIDs = new List<ID>();
		for (SObject each : scope)
		{
			chainIDs.add(each.Id);
		}
		
		try
		{
			updateRemainingTime((List<Critical_Chain__c>) scope);
		}
		catch (Exception e)
		{
			handleException(e);
		}
		
		try
		{
			List<EWChain> ewChains = EWChain.buildEWChains(chainIDs);
			updateEWs(ewChains);
		}
		catch (Exception e)
		{
			handleException(e);
		}
	}
	
	private Object choose(List<Object> variants)
	{
		for (Object variant : variants)
		{
			if (variant != null)
			{
				return variant;
			}
		}
		
		return null;
	}
	
	private void updateRemainingTime(List<Critical_Chain__c> chains)
	{
		String yesterday = System.now().addDays(-1).format('EEEE');
		//It not test is running and yesterday was a working day
		if (!Test.isRunningTest() && (yesterday == 'Saturday' || yesterday == 'Sunday'))
		{
			return;
		}
		
		//update only active steps!
		/**
		List<ID> stepIDs = new List<ID>();
		List<Critical_Chain_Step_Junction__c> junctions = [select Remaining_Time__c, Challenging_Time__c, Duration__c, Last_Known_Remaining_Time__c, Remaining_Time_Change_Date__c, LastModifiedDate from Critical_Chain_Step_Junction__c
				where Critical_Chain__c in :new Map<ID, Critical_Chain__c>(chains).keyset() 
				and (Remaining_Time_Change_Date__c = NULL or Remaining_Time_Change_Date__c < YESTERDAY)];
		for (Critical_Chain_Step_Junction__c junction : junctions)
		{
			stepIDs.add(junction.Flow_Step__c);
		}/**/
		
			//Looping through all steps and filtering active ones
		List<Critical_Chain_Step_Junction__c> junctionsToUpdate = new List<Critical_Chain_Step_Junction__c>();
		for (STSWR1__Flow_Step_Junction__c step : [select Id, Name,
				(select STSWR1__Step__r.STSWR1__Flow_Branch__c, STSWR1__Cursor__r.STSWR1__Flow_Branch__c, STSWR1__Status__c,
					STSWR1__Cursor__r.STSWR1__Step__c, STSWR1__Step__c
					from STSWR1__Flow_Instance_History__r
					where STSWR1__Obsolete__c != true order by CreatedDate, Name),
				(select Remaining_Time__c, Challenging_Time__c, Duration__c, Last_Known_Remaining_Time__c, Remaining_Time_Change_Date__c
					from Critical_Chain_Junctions__r
					where (Remaining_Time_Change_Date__c = NULL or Remaining_Time_Change_Date__c < YESTERDAY))
				//order by CreatedDate desc limit 1)
				from STSWR1__Flow_Step_Junction__c
				where Id in (select Flow_Step__c from Critical_Chain_Step_Junction__c
					where Critical_Chain__c in :new Map<ID, Critical_Chain__c>(chains).keyset() 
					and (Remaining_Time_Change_Date__c = NULL or Remaining_Time_Change_Date__c < YESTERDAY))])
				//from STSWR1__Flow_Step_Junction__c where Id in :stepIDs])
		{
			Boolean isActive = false;
			for (STSWR1__Flow_Instance_History__c history : step.STSWR1__Flow_Instance_History__r)
			{
				if (history.STSWR1__Cursor__r.STSWR1__Step__c == history.STSWR1__Step__c)
				{
					isActive = true;
				}
				
				//if (isActive == null || history.STSWR1__Step__r.STSWR1__Flow_Branch__c == (ID) history.STSWR1__Cursor__r.STSWR1__Flow_Branch__c)
				//{
				//	isActive = (history.STSWR1__Status__c == 'In Progress');
				//}
			}
			
			//If step is active - looping through it's Critical Chain Junctions and update the one which haven't been updated properly
			for (Integer i = 0, n = step.Critical_Chain_Junctions__r.size(); isActive == true && i < n; i++)
			{
				Critical_Chain_Step_Junction__c chainJunction = step.Critical_Chain_Junctions__r.get(i);
				if (chainJunction.Remaining_Time_Change_Date__c == null)
				{
					chainJunction.Remaining_Time_Change_Date__c = System.today();
				}
				
				Decimal remainingTime = (Decimal) choose(new List<Decimal>{chainJunction.Remaining_Time__c, chainJunction.Challenging_Time__c, 0});
				Decimal lastKnown = (Decimal) choose(new List<Decimal>{chainJunction.Last_Known_Remaining_Time__c, remainingTime});
				//Integer idleDays = chainJunction.Remaining_Time_Change_Date__c.daysBetween(System.today());
				Integer idleDays = new EWDatetime(chainJunction.Remaining_Time_Change_Date__c).workdaysBetween(System.today());
				
				//If we haven't decreased it by required number of days or haven't decreased it for required number of days
				//if (lastKnown - remainingTime < NUMBER_OF_IDLE_DAYS
				//		&& idleDays <= NUMBER_OF_IDLE_DAYS + 2 /*week-end when we don't decrease values*/)
				if (idleDays <= NUMBER_OF_IDLE_DAYS)
				{
					remainingTime--;
				}
				else
				{
					remainingTime = lastKnown;
				}
				
				chainJunction.Last_Known_Remaining_Time__c = lastKnown;
				chainJunction.Remaining_Time__c = Math.max(remainingTime, 0);
				junctionsToUpdate.add(chainJunction);
			}
		}
		
		update junctionsToUpdate;
		return;
		
		/*
		for (List<Critical_Chain_Step_Junction__c> stepJunctions : [select Remaining_Time__c, Challenging_Time__c, Duration__c, Last_Known_Remaining_Time__c, Remaining_Time_Change_Date__c, LastModifiedDate from Critical_Chain_Step_Junction__c
				where Critical_Chain__c in :new Map<ID, Critical_Chain__c>(chains).keyset() 
				and (Remaining_Time_Change_Date__c = NULL or Remaining_Time_Change_Date__c < YESTERDAY)])
		{
			List<ID> stepIDs = new List<ID>();
			for (Critical_Chain_Step_Junction__c each : stepJunctions)
			{
				stepIDs.add(each.Flow_Step__c);
			}
			
			for (List<Critical_Chain_Step_Junction__c> futureSteps : [select Remaining_Time__c, Challenging_Time__c, Duration__c, Last_Known_Remaining_Time__c, Remaining_Time_Change_Date__c, LastModifiedDate from Critical_Chain_Step_Junction__c where Id in :stepJunctions
					and Flow_Step__c in (select STSWR1__Step__c from STSWR1__Gantt_Step_Property__c where STSWR1__Step__c in :stepIDs
						//STSWR1__Planned_Start_Date__c, STSWR1__Planned_End_Date__c, STSWR1__Revised_Start_Date__c,
						//STSWR1__Revised_End_Date__c
						and ((STSWR1__Revised_Start_Date__c = null and STSWR1__Planned_Start_Date__c > TODAY) or (STSWR1__Revised_Start_Date__c > TODAY)))])
			{
				
			}
			
			for (Critical_Chain_Step_Junction__c stepJunction : stepJunctions)
			{
				if (stepJunction.Remaining_Time_Change_Date__c == null)
				{
					stepJunction.Remaining_Time_Change_Date__c = stepJunction.LastModifiedDate.date();
				}
				
				if (stepJunction.Remaining_Time_Change_Date__c.daysBetween(System.today()) < NUMBER_OF_IDLE_DAYS + 1)
				{
					stepJunction.Remaining_Time__c = Math.max(0, (Decimal) choose(new List<Decimal>{
						stepJunction.Remaining_Time__c,
						stepJunction.Challenging_Time__c,
						stepJunction.Duration__c,
						0
					}) - 1);
				}
				else
				{
					stepJunction.Remaining_Time__c = (Decimal) choose(new List<Decimal>{
						stepJunction.Last_Known_Remaining_Time__c,
						stepJunction.Challenging_Time__c,
						stepJunction.Duration__c,
						stepJunction.Remaining_Time__c,
						0
					});
				}
			}
			
			update stepJunctions;
		}
		/**/
	}
	
	private void updateEWs(List<EWChain> ewChains)
	{
		List<Critical_Chain__c> chainRecords = new List<Critical_Chain__c>();
		for (EWChain eachEW : ewChains)
		{
			chainRecords.add(eachEW.toRecord());
			//new EWDIABatch().updateDIAData(eachEW);//@TODO: Turn on unless Category-driven Site EW data implemented!
		}
		
		update chainRecords;
	}
	
	global virtual void finish(Database.BatchableContext BC)
	{
		//chaining Actuals Batch
		//EWSiteActualsBatch.executeBatch();
	}
}