public with sharing class PAWS_AdminUtilitiesController 
{
	public class PAWS_AdminUtilitiesControllerException extends Exception {}
	
	public String Operation {get;set;}
	
	private STSWR1.API API
	{
		get
		{
			if(API == null) API = new STSWR1.API();
			return API;
		}
		set;
	}
	
	public void operationChanged()
	{
		if (Operation == 'Restart Workflow Branch') rwbRefresh();
	}

	public void execute()
	{
		if(Operation == 'Restart Workflow Branch') rwbExecute();
	}
	
	/********************* RESTART WORKFLOW BRANCH SECTION *************************/
	
	public String RWBSourceId {get;set;}
	public String RWBFlowId {get;set;}
	public String RWBBranchId {get;set;}
	
	public List<SelectOption> RWBAvailableFlows
	{
		get
		{
			if(RWBAvailableFlows == null && !String.isEmpty(RWBSourceId))
			{
				RWBAvailableFlows = new List<SelectOption>();
				RWBAvailableFlows.add(new SelectOption('', '--None--'));
				
				Map<ID, SelectOption> optionsMap = new Map<ID, SelectOption>();
				String sourceId = RWBSourceId.length() > 15 ? RWBSourceId.substring(0, 15) : RWBSourceId;
				for(STSWR1__Flow_Instance__c record : [select STSWR1__Flow__c, STSWR1__Flow__r.Name from STSWR1__Flow_Instance__c where STSWR1__Object_Id__c like :(sourceId + '%')])
				{
					optionsMap.put(record.STSWR1__Flow__c, new SelectOption(record.STSWR1__Flow__c, record.STSWR1__Flow__r.Name));
				}
				
				RWBAvailableFlows.addAll(optionsMap.values());
			}
			return RWBAvailableFlows;
		}
		set;
	}
	
	public List<SelectOption> RWBAvailableBranches
	{
		get
		{
			if(RWBAvailableBranches == null && !String.isEmpty(RWBFlowId))
			{
				RWBAvailableBranches = new List<SelectOption>();
				RWBAvailableBranches.add(new SelectOption('', '--None--'));
				
				for(STSWR1__Flow_Branch__c record : [select Name from STSWR1__Flow_Branch__c where STSWR1__Flow__c = :RWBFlowId])
				{
					RWBAvailableBranches.add(new SelectOption(record.Id, record.Name));
				}
			}
			return RWBAvailableBranches;
		}
		set;
	}
	
	private void rwbRefresh()
	{
		RWBSourceId = null;
		RWBAvailableFlows = null;
		RWBAvailableBranches = null;
		RWBFlowId = null;
		RWBBranchId = null;
	}
	
	public void rwbSourceChanged()
	{
		RWBAvailableFlows = null;
		RWBAvailableBranches = null;
		RWBFlowId = null;
		RWBBranchId = null;
	}
	
	public void rwbFlowChanged()
	{
		RWBAvailableBranches = null;
		RWBBranchId = null;
	}
	
	public void rwbExecute()
	{
		try
		{
			if(rwbRestartBranch(RWBSourceId, RWBFlowId, RWBBranchId) != null) 
				ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The branch was successfully restarted!'));
		}catch(Exception ex)
		{
			ApexPages.addMessages(ex);
		}
	}
	
	private STSWR1__Flow_Instance_Cursor__c rwbRestartBranch(ID sourceId, ID flowId, ID branchId)
    {
    	Savepoint sp = Database.setSavepoint();
    	try
    	{
    		List<STSWR1__Flow_Instance__c> instances = [select STSWR1__Parent__c, STSWR1__Parent_Step__c, STSWR1__Lock_Parent__c, STSWR1__Lock_Parent_Step__c, STSWR1__Flow__c, 
    						STSWR1__Flow__r.Name, STSWR1__Flow__r.STSWR1__Type__c, STSWR1__Flow__r.STSWR1__Object_Type__c, 
    						STSWR1__Flow__r.STSWR1__Enable_User_Chatter_Notifications__c, STSWR1__Flow__r.STSWR1__Enable_Object_Chatter_Notifications__c, 
    						STSWR1__Object_Id__c, STSWR1__Object_Type__c from STSWR1__Flow_Instance__c where STSWR1__Object_Id__c = :sourceId and STSWR1__Flow__c = :flowId];
    		if(instances.size() == 0) throw new PAWS_AdminUtilitiesControllerException('There are no available instances!');
    		
    		STSWR1__Flow_Instance__c flowInstance = instances[0];
    		sObject source = (sObject)API.call('WorkflowService', 'getSObjectByFlowInstance', flowInstance);
    		if(source == null) throw new PAWS_AdminUtilitiesControllerException('Can not find source record!');
    		
    		

    		List<STSWR1__Flow_Step_Junction__c> steps = [select STSWR1__Is_First_Step__c, STSWR1__Flow_Branch__r.STSWR1__Parent_Branch__c, (select Id from STSWR1__Flow_Step_Input_Connections__r where STSWR1__Start_Parallel_Branch__c = 'Yes') from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__c = :flowInstance.STSWR1__Flow__c and STSWR1__Flow_Branch__c = :branchId];
    		Map<ID, STSWR1__Gantt_Step_Property__c> propertiesMap = (Map<ID, STSWR1__Gantt_Step_Property__c>)API.call('GanttStepPropertyService', 'getFlowsPropertiesMap', new Map<String, Object> {'flowIds' => new Set<ID>{flowInstance.STSWR1__Flow__c}});
    
    		ID actualStepId = null;
    		for(STSWR1__Flow_Step_Junction__c step : steps)
    		{
    			if(step.STSWR1__Is_First_Step__c == true) 
    			{
    				actualStepId = step.Id;
    				break;
    			}else if(step.STSWR1__Flow_Branch__r.STSWR1__Parent_Branch__c != null && step.STSWR1__Flow_Step_Input_Connections__r.size() > 0)
    			{
    				actualStepId = step.Id;
    				break;
    			}    			
    		}
    		
    		if(actualStepId == null)  throw new PAWS_AdminUtilitiesControllerException('Can not find first step!');
    		STSWR1__Flow_Step_Junction__c actualStep = (STSWR1__Flow_Step_Junction__c)API.call('WorkflowService', 'getFlowStepById', actualStepId);

			List<STSWR1__Flow_Instance_Cursor__c> cursors = [select Id from STSWR1__Flow_Instance_Cursor__c where STSWR1__Flow_Instance__c = :flowInstance.Id and STSWR1__Step__r.STSWR1__Flow_Branch__c = :branchId];
    		ID cursorId = (cursors.size() > 0 ? cursors[0].Id : null);
    		
    		STSWR1__Flow_Instance_Cursor__c cursor = rwbBuildCursor(actualStep, source, propertiesMap, cursorId);
    		if(cursor.Id == null) cursor.STSWR1__Flow_Instance__c = flowInstance.Id;
			upsert cursor;
			
			delete [select Id from STSWR1__Flow_Instance_History__c where STSWR1__Cursor__c = :cursor.Id];
			insert new STSWR1__Flow_Instance_History__c(STSWR1__Cursor__c=cursor.Id, STSWR1__Step__c = cursor.STSWR1__Step__c,STSWR1__Connector__c = cursor.STSWR1__Step_Connection__c,STSWR1__Assigned_To__c = cursor.STSWR1__Step_Assigned_To__c,STSWR1__Status__c = cursor.STSWR1__Status__c);
    		
    		cursor = (STSWR1__Flow_Instance_Cursor__c)API.call('WorkflowService', 'getFlowInstanceCursorById', cursor.Id);
    		
			API.call('WorkflowService', 'postTasksToChatter', new List<Map<String, Object>>{
    			new Map<String, Object> {
	    			'cursor' => cursor,
	    			'source' => source
	    		}
    		});

    		return cursor;
    	}catch(Exception ex)
    	{
    		Database.rollback(sp);
    		throw ex;
    	}
    }
    
    private STSWR1__Flow_Instance_Cursor__c rwbBuildCursor(STSWR1__Flow_Step_Junction__c step, sObject record, Map<ID, STSWR1__Gantt_Step_Property__c> propertiesMap, ID cursorId)
    {
		String status = 'In Progress', statusReason = null;
		if(step.STSWR1__Number_of_Before_Rules__c > 0)
		{
			status = 'Pending';
			statusReason = 'Because of before Step Rules';
		}else if(step.STSWR1__Number_of_Before_Actions__c > 0)
		{
			status = 'Pending';
			statusReason = 'Bacause of before Step Actions';
		}
		
		STSWR1__Flow_Instance_Cursor__c cursor = new STSWR1__Flow_Instance_Cursor__c(Id=cursorId);
		cursor.STSWR1__Status__c = status;
		cursor.STSWR1__Status_Reason__c = statusReason;
		cursor.STSWR1__Step__c = step.Id;
		cursor.STSWR1__Step_Changed_Date__c = DateTime.now();
        cursor.STSWR1__Step_Assigned_To__c = (String)API.call('WorkflowService', 'buildAssignTo', new Map<String, Object> {
        		'assignType' => step.STSWR1__Assign_Type_Value__c,
        		'assignTo' => step.STSWR1__Assign_To_Value__c,
        		'record' => record
        });

		cursor.STSWR1__Is_Skipped__c = (Boolean)API.call('WorkflowService', 'isStepSkipped', new Map<String, Object> {
        		'stepId' => step.Id,
        		'cursor' => null,
        		'property' => propertiesMap.get(step.Id),
        		'history' => null
        });

		cursor.STSWR1__Step__r = step;

		return cursor;
    }
}