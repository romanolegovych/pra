public class LMS_ViewPRAController{
        
    /************************************** 
     *  Code for : CourseToPRAController  *
     *  Project : LMS                     *    
     *  Author : Andrew Allen             * 
     *  Last Updated Date : 6/7/2012     * 
     *************************************/
     
    // Instance Variables
    public String roleName{get;set;}
    public String roleNameStyle{get;private set;}
    public String roleFilter{get;private set;}
    public String businessUnit{get;set;}
    public String department{get;set;}
    public String jobTitle{get;set;}
    public String jobFamily{get;set;}
    public String region{get;set;}
    public String country{get;set;}
    public String empType{get;set;}
    public String searchFilter{get;set;}
    public String assignment{get;set;}
    public String roleErrorText{get;private set;}
    public String courseToRoleId{get;private set;}
    public String webServiceError{get;private set;}
    public Boolean allChecked{get;set;}
    public Boolean renderBlock{get;set;}
    public Boolean renderError{get;set;}
    public Boolean disableRole{get;set;}
    public Boolean renderExport{get;set;}
    public Boolean bWebServiceError{get;private set;}
    public Boolean bSyncAction{get;private set;}
    
    // Collections
    public list<LMS_RoleWrapper> roleList{get;set;}
    private list<LMS_Role__c> roles{get;set;}
    private map<String, LMS_Role__c> roleMap{get;set;}
    private map<String, CourseDomainSettings__c> settings{get;set;}
    private map<String, LMSConstantSettings__c> constants{get;set;}
    private map<String, LMSCustomTabSettings__c> tabIds{get;set;}
    private Integer currentJTSLIndex {get;set;}
    private map<Integer, list<selectoption>> jtSelectoptionMap {get;private set;}
    
    // Error Varaibles
    public list<String> sfdcErrors{get;private set;}
    public list<String> roleError{get;private set;}
    public list<String> calloutErrors{get;private set;}
    
    // Constants
    private static final String NEXT_PAGINATE = '------- Next Page -------';
    private static final String PREV_PAGINATE = '----- Previous Page -----';
    
    /**
     *  Constructor
     *  Initializes all data
     */
    public LMS_ViewPRAController() { 
        initSettings();
        initVals();
    }
    
    /**
     *  Initialize course search data
     */
    private void initVals() {        
        currentJTSLIndex = 1;
        jtSelectoptionMap = new map<Integer, list<selectoption>>();
        renderBlock = false;
        bWebServiceError = false;
        roleName = 'Enter Role Name';
        roleNameStyle = 'WaterMarkedTextBox';
        roleFilter = ' and Role_Type__r.Name = \'PRA\'';
        courseToRoleId = tabIds.get('CourseToRole').tabId__c;
    }
    
    /**
     *  Get constants from custom settings
     */
    private void initSettings() {
        settings = CourseDomainSettings__c.getAll();
        constants = LMSConstantSettings__c.getAll();
        tabIds = LMSCustomTabSettings__c.getAll();
    }
    
    /**
     *  Business Unit LOV
     */
    public list<selectoption> getBusinessUnitList() {
        list<selectoption> business = new list<selectoption>();
        business = LMS_LookUpDataAccess.getBusinessUnits(false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        for(integer i=0; i<business.size(); i++)
            options.add(business[i]);
        
        return options;
    }
    
    /**
     *  Department LOV w/ filtering based on Business Unit
     */
    public list<selectoption> getDepartmentList() {
        list<selectoption> department = new list<selectoption>();
            department = LMS_LookUpDataAccess.getDepartments(false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        for(integer i=0; i<department.size(); i++)
            options.add(department[i]);
            
        return options;
    }
    
    /**
     *  Job Family LOV
     */
    public list<selectoption> getJobFamilyList() {
        list<selectoption> family = new list<selectoption>();
        family = LMS_LookUpDataAccess.getRoles(false, false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        for(integer i=0; i<family.size(); i++)
            options.add(family[i]);
        return options;
    }
    
    /**
     *  Job Title LOV w/ filtering based on Job Family
     */
    public list<selectoption> getJobTitleList() {
    	Integer mapIndex = 1;
    	jtSelectoptionMap.clear();
        list<selectoption> title = new list<selectoption>();
        title = LMS_LookUpDataAccess.getTitles(false);
    	list<selectoption> options = new list<selectoption>();
    	options.add(new selectoption('',''));
        if(jobFamily == null) {
            for(Integer i = 0; i < title.size(); i++){
            	if(options.size() == 999) {
            		options.add(new selectoption(NEXT_PAGINATE,NEXT_PAGINATE));
            		jtSelectoptionMap.put(mapIndex, options);
            		mapIndex++;
            		options = new list<selectoption>();
            		options.add(new selectoption('',''));
            		options.add(new selectoption(PREV_PAGINATE,PREV_PAGINATE));
            		options.add(title[i]);
            	} else {
                	options.add(title[i]);
            	}
            }
            jtSelectoptionMap.put(mapIndex, options);
        } else {
            for(Job_Title__c obj : [select Job_Title__c,Job_Class_Desc__r.Name from Job_Title__c where Job_Class_Desc__r.Name =:jobFamily 
                                    and Status__c != 'I' order by Job_Title__c asc]){
                options.add(new selectoption(obj.Job_Title__c,obj.Job_Title__c));
            }
            currentJTSLIndex = mapIndex;
            jtSelectoptionMap.put(mapIndex, options);
        }
        return jtSelectoptionMap.get(currentJTSLIndex);
    }
    
    public Pagereference updateJTList() {
    	if (jobTitle != null && jobTitle != '') {
	    	if (jobTitle.equals(NEXT_PAGINATE)) {
	    		currentJTSLIndex++;
	    	} else if (jobTitle.equals(PREV_PAGINATE)) {
	    		currentJTSLIndex--;
	    	}
    	}
    	getJobTitleList();
    	return null;
    }
    
    /**
     *  Function to filter down Job Title
     */
    public pageReference getTitleOnFamily() {
        getJobTitleList();
        return null;
    }
    
    /**
     *  Region LOV
     */
    public list<selectoption> getRegionList() {
        list<selectoption> region = new list<selectoption>();
        region = LMS_LookUpDataAccess.getRegion(false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        for(integer i=0; i<region.size(); i++) {
            options.add(region[i]);
        }
        return options;
    }
    
    /**
     *  Country LOV w/ filtering based on Region
     */
    public list<selectoption> getCountryList() {
        list<selectoption> country = new list<selectoption>();
        country = LMS_LookUpDataAccess.getCountry(false);
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
        if(region == null) {
            for(integer i=0; i<country.size(); i++) {
                options.add(country[i]);
            }
        } else {
            for(Country__c obj : [select Name from Country__c where Region_Name__c =:region order by Name asc]) {
                options.add(new selectoption(obj.Name, obj.Name));
            }
        }
        return options;
    }
    
    /**
     *  Function to filter down Country
     */
    public pagereference getCountryOnRegion() {
        getCountryList();
        return null;
    }
    
    /**
     *  Employee Types LOV
     */
    public list<selectoption> getTypesList() {
        list<selectoption> options = new list<selectoption>();
            options.add(new selectoption('',''));
            options.add(new selectoption('Contractor','Contractor'));
            options.add(new selectoption('Employees','Employee'));
        return options;
    }
    
    /**
     *  Method to reset role information
     */
    public void roleReset() {
        //Reset LOVs
        getTitleOnFamily();
        getCountryOnRegion();
        
        renderError = false;
        renderBlock = false;
        bWebServiceError = false;
        
        initVals();
    }
    
    /** 
     *  Search Implementation method :
     *  This method uses the UI role criteria to search for the role
     *  and the courses that have been assigned to that role 
     */
    public void search() {
        Boolean acSearch;
        roleMap = new map<String, LMS_Role__c>();
        
        String draft = constants.get('statusDraft').Value__c;
        String committed = constants.get('statusCommitted').Value__c;
        String draftDelete = constants.get('statusDraftDelete').Value__c;
        String removed = constants.get('statusRemoved').Value__c;
        
        //Join query to get course information on a certain role
        String courseQry = 'select Name from Rolecourses__r where Status__c != :removed';
        //Join query to get employee information for course
        String employeeQry = 'select Name from RoleEmployees__r where Status__c != :removed';
        //Main query to pull the role based on criteria in UI               
        String roleQry = 'select CreatedBy.Name,CreatedDate, LastModifiedBy.Name, LastModifiedDate,  Role_Name__c,Role_Type__r.Name,Job_Class_Desc__r.Name,Job_Title__r.Job_Title__c,' +
            'Business_Unit__r.Name,Department__r.Name,Region__r.Region_Name__c,Employee_Type__r.Name,Employee_Count__c,Sync_Status__c,Status__c,' +
            'SABA_Role_PK__c,(' + courseQry + '),(' + employeeQry + ') from LMS_Role__c where Role_Type__r.Name = \'PRA\' ';
        
        
        if(null != roleName && roleName != '' && roleName != 'Enter Role Name') {
            roleQry += ' and Role_Name__c LIKE \'' + String.escapeSingleQuotes(roleName) + '%\'';
            acSearch = true;
        } else {
            acSearch = false;
            if(jobFamily != null) { roleQry += 'and Job_Class_Desc__r.Name = :jobFamily ';} 
            if(jobTitle != null) { roleQry += 'and Job_Title__r.Job_Title__c = :jobTitle ';}  
            if(businessUnit != null) { roleQry += 'and Business_Unit__r.Name = :businessUnit ';} 
            if(department != null) { roleQry += 'and Department__r.Name = :department ';} 
            if(region != null) { roleQry += 'and Region__r.Region_Name__c =: region ';}
            if(country != null) { roleQry += 'and Country__r.Name = :country ';}
            if(empType != null) { roleQry += 'and Employee_Type__r.Name = :empType ';}
        }
        roleQry += ' order by Role_Name__c';
        allChecked = false;
        
        roles = Database.query(roleQry);
        roleList = new list<LMS_RoleWrapper>();
        ///If no role exists do not render table & display message
        for(LMS_Role__c r : roles) {
            roleList.add(new LMS_RoleWrapper(r));
            roleMap.put(r.Id, r);
        }
        if(roles.size() == 0) {
            if(acSearch) {
                renderError = true;
                renderBlock = false;
                roleErrorText = 'Role not found, please enter a different role name';
            } else {
                renderError = true;
                renderBlock = false;
                roleErrorText = 'No role name found, please enter different criteria';
            }
        } else {
            renderBlock = true;
            renderError = false;
        }
    }
    
    public PageReference activateRoles() {
        LMS_ToolsService.activateRole(roleList, roleMap);
        search();
        return null;
    }
    
    public PageReference disableRoles() {
        LMS_ToolsService.inactivateRole(roleList, roleMap);
        search();
        return null;
    }
    
    public PageReference showErrors() {
        PageReference pr = new PageReference('/apex/LMS_ViewPRAError');
        pr.setRedirect(false);
        return pr;
    }
    
    public PageReference invokeExportRoles(){
    System.debug('****exp1***'+roleList);
   // LMS_ToolsService.exportHelper(roleList);
    
    PageReference pr = new PageReference('/apex/LMS_PRAExportRoles');
    pr.setRedirect(false);
    return pr;
    }
    public list<LMS_RoleWrapper> getexportRole() {
    System.debug('****exp2***'+roleList);
        return roleList;
        
    }
    
}