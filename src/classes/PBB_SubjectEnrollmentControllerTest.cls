/**
@author Niharika Reddy
@date 2015
@description this test class is to display the Subject Enrollment graph and its Grid.
**/

@isTest
private class PBB_SubjectEnrollmentControllerTest {
    
    static testMethod void testConstructor() {
        PBB_TestUtils utils = new PBB_TestUtils();
        WFM_Project__c wfmp = utils.createwfmproject();
        Bid_Project__c bid = utils.createbidproject();
        SRM_Model__c sm = utils.createSRMModelAttributes();
        //SRM_Scenario__c srmScen = utils.createSRMScenario();
        PBB_Scenario__c scen = utils.createscenarioAttributes();
        PBB_Scenario__c pb = utils.createscenarioAttributes();
        ApexPages.currentPage().getParameters().put('PBBScenarioID',pb.id);
        PBB_SiteActivationController obj = new PBB_SiteActivationController();
        //System.assertEquals(pb.SRM_Scenario__c,null);
        //pb.SRM_Scenario__c = null;
        upsert pb;
        PBB_SubjectEnrollmentController objj = new PBB_SubjectEnrollmentController();
        //System.assertEquals(pb.SRM_Scenario__c,null);
    }   

}