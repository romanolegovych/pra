/** Implements the test for the Selector Layer of the object MileStoneDenition__c
 * @author	Dimitrios Sgourdos
 * @version	25-Feb-2014
 */
@isTest
private class BDT_MileStoneDefinitionDataAccessorTest {
	
	/** Test the function getMileStoneDefinitions
	 *	The function will be used for calls from Service Layer. By just call the function in the test, we can check
	 *	that its syntax in the query is correct.
	 * @author	Dimitrios Sgourdos
	 * @version	25-Feb-2014
	 */
	static testMethod void getMileStoneDefinitionsTest() {
		List<MileStoneDefinition__c> results = BDT_MileStoneDefinitionDataAccessor.getMileStoneDefinitions(NULL, NULL);
	}
}