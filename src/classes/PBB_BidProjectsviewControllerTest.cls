/**
@author Bhargav Devaram
@date 2015
@description this is PBB_CountryServicesController's test class
**/

@isTest   
private class PBB_BidProjectsviewControllerTest {    
    static testMethod void testPBB_BidProjectsviewController(){        
        
        test.startTest(); 
         
        PBB_TestUtils tu = new PBB_TestUtils ();
        
        //Prepare the test data
        tu.prabu =  tu.createPRABusinessUnits();
        tu.wfmp = tu.createWfmProject();        
        tu.bid =tu.createBidProject();      
        tu.scen =tu.createscenarioAttributes();  
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(tu.bid);
        
        //set the pbb bid project id.  
        ApexPages.currentPage().getParameters().put('Id',tu.bid.id);
        
        PBB_BidProjectsviewController pvc = new PBB_BidProjectsviewController(new Apexpages.Standardcontroller(tu.bid));  
        pvc.deleteID = null;
        pvc.deleteRelatedObjectRecord(); 
        System.assertNotEquals(pvc.ErrorMsg,null);
        
        pvc.deleteID = tu.scen.id;   
        pvc.deleteRelatedObjectRecord();
        System.assertEquals(0,PBB_DataAccessor.getSRMScenarios(pvc.deleteID).size());
        
        
        test.stopTest();
    }    
}