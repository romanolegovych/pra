@isTest
private class BDT_MainRightNavigationBarTest {
	
	static testMethod void  testPagePath() {
		PageReference pdfPage = new PageReference( '/apex/BDT_SingleProject' );
		Test.setCurrentPage(pdfPage); 
		
		BDT_MainRightNavigationBar bar = new BDT_MainRightNavigationBar();
		System.assertEquals(true, bar.show_MainRightStudyDetails);
		
	}

}