/** Implements the test for the Selector Layer of the object StudyServicePricing__c
 * @author	Dimitrios Sgourdos
 * @version	20-Jan-2014
 */
@isTest
private class StudyServicePricingDataAccessorTest {
	
	/** Test the function getStudyServicePricings
	 * @author	Dimitrios Sgourdos
	 * @version	20-Jan-20134
	 */
	static testMethod void getStudyServicePricingsTest() {
		// Create data
		ServiceCategory__c srvCat = new ServiceCategory__c(code__c='001',name='Service Category (1)');
		insert srvCat;
		
		Service__c srv = new Service__c(Name = 'Srv 1', ServiceCategory__c=srvCat.Id);
		insert srv;
		
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001',
																				'BDT Project First Experiments');
		insert currentProject;
		
		Study__c study = new Study__c();
		study.Project__c = currentProject.Id;
		insert study;
		
		ProjectService__c prSrv = new ProjectService__c(Service__c = srv.id, Client_Project__c = currentProject.id);
		insert prSrv;
		
		StudyService__c stSrv = new StudyService__c(ProjectService__c = prSrv.Id,
													NumberOfUnits__c=1,
													Study__c=study.Id);
		insert stSrv;
		
		String errorMessage = 'Error in retrieving the Study Service Pricings';
		
		// Find the automated created (by trigger) study service pricings
		String whereClause = 'StudyService__c = ' + '\'' + stSrv.Id + '\'';
		List<StudyServicePricing__c> results = StudyServicePricingDataAccessor.getStudyServicePricings(
																						whereClause,
																						'NumberOfUnits__c');
		
		system.assert(results.size() > 0, errorMessage);
	}
}