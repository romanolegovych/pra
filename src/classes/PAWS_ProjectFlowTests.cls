/**
*   'PAWS_ProjectFlowTests' is the test class for PAWS ProjectFlow (PAWS)
*   Test classes are used to write the unit test and cover the code coverage of the code.
*   @author   Work Relay
*/
@isTest 
public with sharing class PAWS_ProjectFlowTests {

    static testMethod void projectFlowTriggerTests()
    {
        STSWR1__Item__c item = new STSWR1__Item__c(Name='Test Item');
        insert item;
        
        WFM_Client__c client = new WFM_Client__c(name='TestClient', Client_Name__c='TestClinet_Name', Client_Unique_Key__c='TestClient');
        insert client;
        
        WFM_Contract__c contract = new WFM_Contract__c(name='TestContract', Contract_Status__c='AA', Client_ID__c=client.id, Contract_Unique_Key__c='TestContract');
        insert contract;
        
        WFM_Project__c wfmProject = new WFM_Project__c(name='TestProject', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProject', project_status__c ='RM',  Status_Desc__c='Active');
        insert wfmProject ;
        
        ecrf__c project = new ecrf__c(Name='Test', Project_Id__c=wfmProject.Id, Type__c = 'Regular');
        insert project;
        
        STSWR1__Flow__c flow= new STSWR1__Flow__c(Name='Test', STSWR1__Object_Type__c='Stub__c');
        insert flow;
        
        PAWS_Project_Flow_Junction__c flowJunction = new PAWS_Project_Flow_Junction__c();
        flowJunction.Folder__c = item.Id;
        flowJunction.Flow__c = flow.Id;
        flowJunction.Project__c = project.Id;
        insert flowJunction;
        
        project.Type__c = 'Proposal';
        update project;
        
        delete flowJunction;
    }
    
    static testMethod void flowInstanceHistoryTriggerTests()
    {
        PAWS_ApexTestsEnvironment.init();

    	PAWS_ApexTestsEnvironment.FlowInstanceHistory.STSWR1__Actual_Complete_Date__c = Date.today();
		update PAWS_ApexTestsEnvironment.FlowInstanceHistory;
		
		PAWS_ApexTestsEnvironment.FlowInstanceHistory.STSWR1__Actual_Complete_Date__c = null;
		update PAWS_ApexTestsEnvironment.FlowInstanceHistory;
    }
    
    static testMethod void ganttStepPropertyTriggerTests()
    {
        PAWS_ApexTestsEnvironment.init();
        
        PAWS_Project_Flow_Junction__c projectFlow = PAWS_ApexTestsEnvironment.ProjectFlow;
        
        PAWS_ApexTestsEnvironment.GanttStepProperty.STSWR1__Revised_Start_Date__c = Date.today();
        PAWS_ApexTestsEnvironment.GanttStepProperty.STSWR1__Revised_End_Date__c = Date.today().addDays(2);
        PAWS_ApexTestsEnvironment.GanttStepProperty.STSWR1__Revised_Comment__c = 'Test';
		update PAWS_ApexTestsEnvironment.GanttStepProperty;
    }
    
    static testMethod void flowStepActionTriggerTests()
    {
        PAWS_ApexTestsEnvironment.init();

    	delete PAWS_ApexTestsEnvironment.FlowStepAction;
    }
    
	static testMethod void testPAWS_LimitsTest()
	{
		STSWR1__Stub__c stub = new STSWR1__Stub__c();
		insert stub;
		
		PAWS_LimitsTest inst = new PAWS_LimitsTest();
		inst.run(null, null, null, null);
	}
    
    @isTest(SeeAllData=true)
	static void objectTriggerTestsPart1()
    {
    	List<String> objectsNames = new List<String>((Set<String>)new STSWR1.API().call('SObjectService', 'loadAllowedObjects', null));
    	
    	STSWR1.AbstractTrigger.Disabled = true;
    	
    	Integer i = 0, j = 0; 
    	while(i < objectsNames.size() && j < 10)
    	{
    		Type objectType = Type.forName(objectsNames[i]);
    		try{ insert (sObject)objectType.newInstance(); }catch(Exception ex){}
    		
    		j++;i++;
    	}
    }

	@isTest(SeeAllData=true)
    static void objectTriggerTestsPart2()
    {
    	List<String> objectsNames = new List<String>((Set<String>)new STSWR1.API().call('SObjectService', 'loadAllowedObjects', null));
    	
    	STSWR1.AbstractTrigger.Disabled = true;
    	
    	Integer i = 10, j = 0; 
    	while(i < objectsNames.size() && j < 10)
    	{
    		Type objectType = Type.forName(objectsNames[i]);
    		try{ insert (sObject)objectType.newInstance(); }catch(Exception ex){}
    		
    		j++;i++;
    	}
    }

	@isTest(SeeAllData=true)
    static void objectTriggerTestsPart3()
    {
    	List<String> objectsNames = new List<String>((Set<String>)new STSWR1.API().call('SObjectService', 'loadAllowedObjects', null));
    	
    	STSWR1.AbstractTrigger.Disabled = true;
    	
    	Integer i = 20, j = 0; 
    	while(i < objectsNames.size() && j < 10)
    	{
    		Type objectType = Type.forName(objectsNames[i]);
    		try{ insert (sObject)objectType.newInstance(); }catch(Exception ex){}
    		
    		j++;i++;
    	}
    }

	@isTest(SeeAllData=true)
    static void objectTriggerTestsPart4()
    {
    	List<String> objectsNames = new List<String>((Set<String>)new STSWR1.API().call('SObjectService', 'loadAllowedObjects', null));
    	
    	STSWR1.AbstractTrigger.Disabled = true;
    	
    	Integer i = 30, j = 0; 
    	while(i < objectsNames.size() && j < 10)
    	{
    		Type objectType = Type.forName(objectsNames[i]);
    		try{ insert (sObject)objectType.newInstance(); }catch(Exception ex){}
    		
    		j++;i++;
    	}
    }

	@isTest(SeeAllData=true)
    static void objectTriggerTestsPart5()
    {
    	List<String> objectsNames = new List<String>((Set<String>)new STSWR1.API().call('SObjectService', 'loadAllowedObjects', null));
    	
    	STSWR1.AbstractTrigger.Disabled = true;
    	
    	Integer i = 40, j = 0; 
    	while(i < objectsNames.size() && j < 10)
    	{
    		Type objectType = Type.forName(objectsNames[i]);
    		try{ insert (sObject)objectType.newInstance(); }catch(Exception ex){}
    		
    		j++;i++;
    	}
    }
}