/** Implements the test for the Selector Layer of the object FinancialDocumentApproval__c
 * @author	Dimitrios Sgourdos
 * @version	29-Jan-2014
 */
@isTest
private class FlowchartServiceTotalDataAccessorTest {
	
	/** Test the function getFlowchartServiceTotalList
	 * @author	Dimitrios Sgourdos
	 * @version	29-Jan-2014
	 */
	static testMethod void getFlowchartServiceTotalListTest() {
		 // create test data
		list<sobject> initList = new list<sobject>();
		initList.add(new ClinicalDesign__c(name='1'));
		initList.add( BDT_BuildTestData.buildClientProject('PRA1312-001','BDT Project First Experiments') );
		initList.add(new ServiceCategory__c(name='test',code__c='001'));
		insert initList;
		
		list<sobject> aList = new list<sobject>();
		aList.add(new arm__c(ClinicalDesign__c = initList[0].id, Arm_Number__c = 1, Description__c = 'Arm 1')); //0
		aList.add(new arm__c(ClinicalDesign__c = initList[0].id, Arm_Number__c = 2, Description__c = 'Arm 2')); //1
		aList.add(new Epoch__c(ClinicalDesign__c = initList[0].id, Epoch_Number__c = 1, Description__c = 'Epoch 1')); //2
		aList.add(new Epoch__c(ClinicalDesign__c = initList[0].id, Epoch_Number__c = 2, Description__c = 'Epoch 2')); //3
		aList.add(new flowcharts__c(ClinicalDesign__c = initList[0].id, Flowchart_Number__c = 1, Description__c = 'Flowchart 1')); //4
		aList.add(new flowcharts__c(ClinicalDesign__c = initList[0].id, Flowchart_Number__c = 2, Description__c = 'Flowchart 2')); //5
		aList.add(new Study__c(Project__c = initList[1].Id)); // 6
		aList.add(new Service__c(ServiceCategory__c = initList[2].id, Name = 'a', SequenceNumber__c = 1, IsDesignObject__c = true)); // 7
		insert aList;
		
		list<FlowchartAssignment__c> fList = new list<FlowchartAssignment__c>();
		fList.add(new FlowchartAssignment__c(
			Arm__c 			= (Id)(aList[0].get('ID')),
			Epoch__c 		= (Id)(aList[2].get('ID')),
			Flowchart__c 	= (Id)(aList[4].get('ID'))
		));
		fList.add(new FlowchartAssignment__c(
			Arm__c 			= (Id)(aList[0].get('ID')),
			Epoch__c 		= (Id)(aList[3].get('ID')),
			Flowchart__c 	= (Id)(aList[4].get('ID'))
		));
		insert fList;

		ProjectService__c ps = new ProjectService__c(Client_Project__c = initList[1].id, Service__c = aList[7].id);
		insert ps;
		
		List<FlowchartServiceTotal__c> sourceList = new List<FlowchartServiceTotal__c>();
		sourceList.add(new FlowchartServiceTotal__c(flowchartassignment__c=fList[0].Id,
													TotalUnits__c=10,
													ProjectService__c = ps.Id,
													Study__c = aList[6].Id) );
		sourceList.add(new FlowchartServiceTotal__c(flowchartassignment__c=fList[1].Id,
													TotalUnits__c=20,
													ProjectService__c = ps.Id,
													Study__c = aList[6].Id) );
		insert sourceList;

		// Check the function
		String errorMessage = 'Error in retrieving flowchart service totals from the system';
		
		List<FlowchartServiceTotal__c> results = FlowchartServiceTotalDataAccessor.getFlowchartServiceTotalList(
																	'flowchartassignment__c = \'' + fList[0].Id + '\'',
																	NULL
												);
		
		system.assertEquals(results.size(), 1, errorMessage);
		system.assertEquals(results[0].Id, sourceList[0].Id);
		
		// Check deletion of orphaned Flowchart Service Totals
		sourceList[0].Study__c = NULL;
		update sourceList[0];
		
		results = FlowchartServiceTotalDataAccessor.getFlowchartServiceTotalList(
																	'flowchartassignment__c = \'' + fList[0].Id + '\'',
																	NULL
												);
		
		system.assertEquals(results.size(), 0, errorMessage);
	}
}