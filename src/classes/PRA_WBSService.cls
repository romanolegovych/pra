/**
*   'PRA_WBSService' class is to access the bussiness logic involved for Work Breakdown Structure(WBS) of a project.
*   @author   Devaram Bhargav
*   @version  18-Oct-2013
*   @since    18-Oct-2013
*/
public with sharing class PRA_WBSService {
    /**
    *   This Method converts all the projects WBS list into a Map with the no predefined attributes value . 
    *   @return   The Map of PRA_ProjectWBSCVO.ProjectWBS type  with project as a key in the Map.
    */
    public static Map<string, PRA_ProjectWBSCVO.ProjectWBS> getMapfromAllProjectsbyClientTaskStatus(){
        //Create a map that needs to return.
        Map<String,PRA_ProjectWBSCVO.ProjectWBS> projectWBSMap = new Map<String,PRA_ProjectWBSCVO.ProjectWBS>();
        
        //Loop over the results and map it into MAP
        for(Aggregateresult ar:PRA_DataAccessor.getClientTaskCountbyNullStatus(PRA_Constants.CP_ALL)){
            //Make a variable of the PRA_ProjectWBSCVO.ProjectWBS record type.
            PRA_ProjectWBSCVO.ProjectWBS pwbs=new PRA_ProjectWBSCVO.ProjectWBS();            
            
            /**
            *   Verifies if the Map contains the project already in it or not.  
            *   if so, they updates the map with status of the project.
            *   if not, create one in the map with the statuses.
            */
            if(projectWBSMap.containsKey(string.valueof(ar.get('p')))) {
                pwbs=projectWBSMap.get(string.valueof(ar.get('p'))); 
            }
            
            //Assign the Project Name
            pwbs.ProjectName=string.valueof(ar.get('p'));
            
            //Assign to the Map for all different status.
            if(ar.get('s')==PRA_Constants.CU_STATUS_NEW){
               pwbs.InitiateWBS  =  true;                //Assign true if you have client task with status New
            }else
            if(ar.get('s')==PRA_Constants.CU_STATUS_READY){
               pwbs.ExportWBS    =  true;                //Assign true if you have client task with status Ready 
            }else
            if(ar.get('s')==PRA_Constants.CU_STATUS_SENT){
               pwbs.ConfirmWBS   =  true;                //Assign true if you have client task with status Sent
            }
            
            //Make a map with Project as a key and its status as a value.
            projectWBSMap.put(string.valueof(ar.get('p')),pwbs);        
        }        
        return projectWBSMap;
   }
   
   /**
   *   This Method reurns the Lwbs_project__c record with the predefined attributes value (ProjectName).   
   *   @param    ProjectName   This is name of the Project.
   *   @return   wbs_project__c record, it creates even if its not there.
   */
   public static wbs_project__c getWBSProjectbyProjectName(String ProjectName) {
       //Make a list to return the WBS for the Project.
       List<wbs_project__c> WBSProjectList = new List<wbs_project__c>();
       
       //get the WBS project data by project name
       WBSProjectList  =   PRA_DataAccessor.getWBSprojectbyProjectName(ProjectName);
       system.debug('-----WBSProject -------' + WBSProjectList); 
      
       //If there are no records found for the project, create one
       if(WBSProjectList.isEmpty()){
       
           //get the Project ID from the project Name
           client_project__c cp= PRA_DataAccessor.getClientProjectDetailsbyProjectName(ProjectName);          
          
           //creating a record for this project.
           wbs_project__c wsbp=new wbs_project__c();
           wsbp.Client_Project__c=cp.id;  
           WBSProjectList.add(wsbp);
           
           //Performing upsert for the project.                
           String ErrorMsg=PRA_DataAccessor.upsertWBSProject(WBSProjectList);
       }
                    
       //system.debug('-----WBSProjectList -------' + WBSProjectList); 
       return WBSProjectList[0];
   }
   
    /**
    *   Method retrieve the  Aggregateresult data with the predefined attributes value (Project).
    *   @return   The Aggregateresult data for the client task whose status is not null for either one project or all.
    */
    public static List<Aggregateresult> getClientTaskCountbyStatus(String Project){
        return PRA_DataAccessor.getClientTaskCountbyNullStatus(Project);
    }
    
    /**
    *   Method upserts the  client tasks with different statuses attributes value (Project,status).
    *   @return  error message..
    */
    public static String saveClientTaskbyStatus(String Project,String status,String toStatus){
        
        String ErrorMsg;
        
        // make a list to perform upsert.
        List<client_task__c> ctList = new List<client_task__c>();
        
        //loop through the collected list and change the status and make a list.
        for(client_task__c ct:PRA_DataAccessor.getClientTaskbyProjectStatus(Project,status)){
            ct.status__c   =  toStatus;
            if(ct.Client_Unit_Number__c == PRA_Constants.MIGRATED_CLIENT_TASK)
                ct.status__c   =  '';       
            ctList.add(ct);
        }
                
        //Performing upsert for the client tasks.                
        ErrorMsg = PRA_DataAccessor.upsertClientTaskList(ctList);        
        return ErrorMsg;
    }
    
    
     /**
   *   This Method send the emails to FA's with the predefined attributes value.   
   *   @param   UserEmailList This is users email list of FA's.
   *   @param   ExportProjectSet is list of projects lsit need to export to lawson.
   *   @param   ConfirmProjectSet is list of projects lsit need to confirm .
   */
   public static void SendEmailbyWBSProjectsList(Set<String> UserEmailList ,Set<String> ExportProjectSet,Set<String> ConfirmProjectSet) {
           
       system.debug('-------UserEmailList ----'+UserEmailList+'---ExportProjectSet-----'+ExportProjectSet+'---ConfirmProjectSet----'+ConfirmProjectSet );
       
       //prepare the subject, body, to send list.
       String[] receipntTo = new String[0];                
       //Preparing Subject
       String Subject;
       String HtmlBody;
       String toMail; 
       
       receipntTo.addall(UserEmailList);  //to send list
        
       Subject='Planning and Forecasting: WBS available for download/confirmation.';   //Subject 
       
       //Preparing HTML Body with Link  if there are projects to export      
       if(ExportProjectSet.size()>0 ){
           HtmlBody='<br></br><br></br>There is a new WBS is available for Export for the following projects:<br></br>';  
           for(String s:ExportProjectSet){
               HtmlBody+=s+'<br></br>';
           }
       }
       
       //Preparing HTML Body with Link  if there are projects to confirm   
       if(ConfirmProjectSet.size()>0){
           HtmlBody+='<br></br><br></br>Please confirm that the WBS has been loaded to Lawson for the following projects:<br></br>';  
           for(String s:ConfirmProjectSet){
               HtmlBody+=s+'<br></br>';
           }
       }        
        
       HtmlBody+='<br></br><br></br>Please login to ';
       String ref = URL.getSalesforceBaseUrl().toExternalForm();
       HtmlBody+='<a href="'+ref +'/apex/PRA_WBSProject">Planning and Forecasting</a>'; 
       HtmlBody+=' to Export the WBS or Confirm the WBS.<br></br><br></br><br></br>'; 
          
       HtmlBody+='Thank You!.<br></br>';   
       HtmlBody+='PRA Information Technology.<br></br>';   
       
       //New instance of a single email message     
       Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();      
       mail.setToAddresses(receipntTo);                
       mail.setSubject(Subject);
       mail.setHtmlBody(HTMLBody);
       system.debug('----------mail----------'+mail);
       Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
   
   
    /**
    *   PZblock method is to show the PZ block in the VF page. 
    *   @return  true only if the profile name in the custom seeting object PMC_ProfileGroup__c
    */
    public static boolean getPZblock(String profileID){
        
        Boolean  retVal=false;
        
        //get the profile name by profile ID
        Profile p=PRA_DataAccessor.getProfilebyId(profileID); 
        
        //check the profile name is in the custom setting object PMC_ProfileGroup__c
        PMC_ProfileGroup__c ProfileSelectList = PMC_ProfileGroup__c.getInstance(p.Name);  
        system.debug('-------ProfileSelectList----'+ProfileSelectList);
        
        //if the record is found in the custom setting object return true otherwise return false
        if((ProfileSelectList!=Null))
            retVal=true;
        return retVal;
    }
    
    /**
    *   FAblock method is to show the FA block in the VF page. 
    *   @return  true only if the user is assigned to a permsiion set that exit in the custom setting object PMC_ProfileGroup__c
    */
    public static boolean getFAblock(String userID){
        
        Boolean  retVal=false;
        
        //make a map with all the values in the custom setting object , which also has the permission set names.
        Map<String,PMC_ProfileGroup__c> ProfileSelectList = PMC_ProfileGroup__c.getall() ;
        system.debug('-------ProfileSelectList----'+ProfileSelectList);
        
        //check the profile name is in the custom setting object PMC_ProfileGroup__c
        List<PermissionSetAssignment> psaList = PRA_DataAccessor.getPermissionSetAssignmentbyUserIdandProfiles(userID, ProfileSelectList.keyset());  
        system.debug('--------psaList----'+psaList);
        
        //if the record is found in the psaList lsit return true otherwise return false
        if(psaList.size()>0)
            retVal=true;
        return retVal;
    }
    
     /**
    *   getUserEmailbyUser method is to get users email list by user 
    *   @return  users email list
    */
    public static Set<String> getUserEmailbyUser(Set<String> UserIDList){
       
       Set<String> UserEmailList = new Set<String>();
       //loop through to get the email over the user list
       for (User u :PRA_DataAccessor.getUserListbyIds(UserIDList)){
             UserEmailList.add((string)u.email);  
       }         
       system.debug('-----UserEmailList---'+UserEmailList+'--UserIDList---'+UserIDList);
       return UserEmailList;
    }
}