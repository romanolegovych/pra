@isTest 
private class PBB_ServiceTaskToDriverTriggerTest {
    
    private static final Integer NUMBER_OF_RECORDS = 2;
    
    private static List<Service_Task_To_Drivers__c> drivers;
    
    static testMethod void validate() { 
        test.startTest();
        //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils ();
       
        tu.smm = tu.createServiceModelAttributes();
        tu.sa = tu.createServiceAreaAttributes();
        tu.sf = tu.createServiceFunctionAttributes();
        tu.St = tu.createServiceTaskAttributes();
        tu.billFormulaName  = tu.createBillFormulaNames();        
        tu.billFormula  = tu.createBillFormula();
        Service_Task_To_Drivers__c sttsi1  = tu.createSTtoBF(tu.St,tu.billFormulaName );         
      
        try{
            delete sttsi1;        
            Service_Task_To_Drivers__c sttsi2  = tu.createSTtoBF(tu.St,tu.billFormulaName ); 
            tu.smm = tu.approveServicemodel(tu.smm);
            delete sttsi2;
        }
        catch(Exception e) {
            system.assert(e.getMessage().contains('UNABLE TO DELETE RECORD BECAUSE SERVICE MODEL IS APPROVED/RETIRED!'));
        } 
        test.stopTest();                 
    }
    
    private static testMethod void checkForApprovedServicesTestServiceInProgress(){
        createDrivers('Service', 'In-Progress');
        Test.startTest();
            insert drivers;
        Test.stopTest();
    }
    
    private static testMethod void checkForApprovedServicesTestFeeInProgress(){
        createDrivers('Fee', 'In-Progress');
        Test.startTest();
            insert drivers;
        Test.stopTest();
    }
    
    private static testMethod void checkForApprovedServicesTestFeeApproved(){
        createDrivers('Service', 'Approved');
        Test.startTest();
            try{
                insert drivers;
                System.assert(false);
            }catch(Exception e){
            }
        Test.stopTest();
        System.assertEquals( NUMBER_OF_RECORDS, drivers.size() );
        for (Service_Task_To_Drivers__c driver : drivers){
            System.assertEquals( null, driver.Id );
        }
    }
    
    private static void createDrivers(String typeValue, String statusValue){
        PBB_TestUtils testUtils = new PBB_TestUtils();
        testUtils.smm = testUtils.CreateServiceModelAttributes();
        testUtils.sa = testUtils.CreateServiceAreaAttributes();
        testUtils.sf = testUtils.CreateServiceFunctionAttributes();
        testUtils.st = testUtils.CreateServiceTaskAttributes();
        
        drivers = new List<Service_Task_To_Drivers__c>();
        for (Integer i = 0; i < NUMBER_OF_RECORDS; i ++ ){
            drivers.add( new Service_Task_To_Drivers__c(
                Service_Task__c = testUtils.st.Id, 
                Driver_Type__c = typeValue,
                Frequency__c= 'Study',
                Times_Frequency__c=1, 
                Per_Frequency__c =1,
                Status__c = statusValue
            ) );
        }
    }
}