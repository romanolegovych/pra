global class PRA_ForecastUpdateScheduler implements Schedulable{
    global void execute(SchedulableContext sc) {
       
       
        // Units
        String paramsJSONString = Json.serialize(new List<String>{'Unit_Effort__c', 'Month_Applies_To__c', 'Forecast_Unit__c'});
        String unit=PRA_BatchQueue.addJob('PRA_ForecastUpdateBatch',paramsJSONString, null);
        
       

    }
}