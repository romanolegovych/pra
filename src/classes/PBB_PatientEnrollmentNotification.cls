/**
 * @description Patient Enrollment Notification class
 * @author      Niharika Reddy
 * @date        Created: 23-Sep-2015, Edited: 23-Sep-2015
 */
public class PBB_PatientEnrollmentNotification implements NCM_NotificationEventInterface {
    
    public Integer notificationCount {get;set;}
    public WFM_Protocol__c protocol  {get;set;}
    
    /**
     * @description Notification Subject is implemented in this method
     * @author      Niharika Reddy
     * @date        Created: 23-Sep-2015
     * @param       notificationCatalogName    The name of the notification topic that occurs
     * @param       relatedToId                The related object id that triggers the notification topic
     * @return      Returns a String that has notification subject
    */
    public String getNotificationSubject(Id notificationCatalogId, String relatedToId) {
        protocol = PBB_DataAccessor.getProtocolById(relatedToId);
        String site = protocol.Total_Subj_Enroll__c+ ' ' + 'Patients Enrolled' ;
        return site;
     }
    
    /**
     * @description Notification body is implemented in this method
     * @author      Niharika Reddy
     * @date        Created: 23-Sep-2015
     * @param       notificationCatalogName    The name of the notification topic that occurs
     * @param       relatedToId                The related object id that triggers the notification topic
     * @return      Returns a string with notification body
    */
    public String getNotificationEmailBody(Id notificationCatalogId,String relatedToId){
        protocol = PBB_DataAccessor.getProtocolById(relatedToId);
        String bdy = 'Notification Date:'+ ' ' +PBB_Utils.getDateInStringFormat(Date.today()) + '<br>' + 
                      '<br/>' +
                      'Protocol:' + ' ' + protocol.name + '<br>' + 
                      '<br/>' +
                       protocol.Total_Subj_Enroll__c+ ' ' + 'Patients Enrolled' + '<br>' + 
                     '<br/>' +
                     '<br/>' +
                     '<br/>' +
                     'Regards'+ ','+ '<br>' +
                     'Predictivv Connect Team' + '<br>' +
                     '_________________________________________________________________________'+ ' <br> ' +
                     '<i>Disclaimer: You are receiving this email due to Notification Subscription settings.</i>' + '<br>' +
                     '<i>To stop these emails please update your Notification Settings.</i>' + '<br>' +
                     '<i>Please do not reply to this email.</i>';             
        return bdy;
    }
    
    /**
     * @description This method checks whether the notification topics has occured or not . 
                    If it occurs it would create and send the email for every ten patients registered.
     * @author      Niharika Reddy
     * @date        Created: 23-Sep-2015
     * @param       notificationCatalogName    The name of the notification topic that occurs
     * @param       relatedToId                The related object id that triggers the notification topic
     * @return      Returns notification body
    */
    public Boolean didNotificationTopicOccur(Id notificationCatalogId, String relatedToId){
        protocol = new WFM_Protocol__c();
        protocol = PBB_DataAccessor.getProtocolById(relatedToId);
        System.debug('******didMethod 1**********'+protocol);
        if(protocol.Notification_Patient_Count__c== null &&   protocol.Total_Subj_Enroll__c >= 10){
            System.debug('***********'+protocol);
            protocol.Notification_Patient_Count__c= protocol.Total_Subj_Enroll__c;
            upsert protocol;
            System.debug('******didMethod 2**********'+protocol);
            //NCM_API.activateNotificationEventById(notificationCatalogId,relatedToId);
            return true;
         }
        else if(protocol.Notification_Patient_Count__c!= null && Integer.valueOf(protocol.Notification_Patient_Count__c/10) <  Integer.valueOf((protocol.Total_Subj_Enroll__c)/10)){
            protocol.Notification_Patient_Count__c= protocol.Total_Subj_Enroll__c;
            upsert protocol;
            System.debug('******didMethod 3**********'+protocol);
            //NCM_API.activateNotificationEventById(notificationCatalogId,relatedToId);
            return true;
        }
        else{
            return false;
        } 
    }
    
}