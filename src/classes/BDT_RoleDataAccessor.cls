public with sharing class BDT_RoleDataAccessor {

	/** Object definition for fields used in application, can be extended when needed
	 * @author Maurice Kremer
	 * @version 8-oct-13
	 * @return list<String> Return used fields
	 */
	public static String getSObjectFieldString() {
		return 
			'BDT_Role__c.Name,'+
			'BDT_Role__c.Id,'+
			'BDT_Role__c.Role_Number__c';
	}
	
	public static list<BDT_Role__c> getAllRoles() {
		return getAllRoles('Name');
	}
	
	/** Return all defined roles
	 * @author Maurice Kremer
	 * @version 8-oct-13
	 * @return list<BDT_Role> All defined roles
	 */
	public static list<BDT_Role__c> getAllRoles(String orderBy) {
		String query = String.format(
			'select {0} from BDT_Role__c order by {1}',
			new list<string> {
				getSObjectFieldString(),
				orderBy
			}
		);
		return (List<BDT_Role__c>) Database.query(query);
	}

	/** Return single roles
	 * @author Maurice Kremer
	 * @version 8-oct-13
	 * @return list<BDT_Role> All defined roles
	 */
	public static BDT_Role__c getRole(String roleId) {
		String query = String.format(
			'select {0} from BDT_Role__c where id = {1}',
			new list<string> {
				getSObjectFieldString(),
				'\'' + roleId + '\''
			}
		);
		try {
			return (BDT_Role__c) Database.query(query);
		} catch (Exception e) {
			system.debug('Query error: ' + query);
			return null;
		}
	}


}