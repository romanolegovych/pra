@istest public with sharing class PAWS_EW_RemainingTimeEditControllerTest
{
	@testsetup public static void setup()
	{
		EWBatchTest.setup();
	}
	
	@istest private static void coverController()
	{
		Test.setCurrentPage(Page.PAWS_EW_RemainingTimeEdit);
		for (Critical_Chain_Step_Junction__c chainJunction : [select Flow_Step__c from Critical_Chain_Step_Junction__c limit 1])
		{
			//ApexPages.currentPage().getParameters().put('step', chainJunction.Flow_Step__c);
			
			ApexPages.currentPage().getParameters().put('signed_request', EncodingUtil.base64Encode(Blob.valueOf(System.Json.serialize(
				new Map<String, Object>{
					'context' => new Map<String, Object>{
						'environment' => new Map<String, Object>{
							'parameters' => new Map<String, Object>{
								'step' => chainJunction.Flow_Step__c
							}
						}
					}
				}
			))));
		}
		
		PAWS_EW_RemainingTimeEditController controller = new PAWS_EW_RemainingTimeEditController();
		controller.init();
		
		controller.EW_Days = 2;
		controller.save();
	}
}