public with sharing class SiteAgreementCreation {

    public RecordType RT_Agreement { get; set; }
    public String sValues { get; set;}
    public List<WFM_Site_Detail__c> mySiteRecords { get; set; }
    
    
    public SiteAgreementCreation(){
          
          sValues='a1cL0000000DstF,a1cL0000000DstA';
          String[] splitString=sValues.split('\\,');
          mySiteRecords=[Select id, Name, Project_Protocol__c  From WFM_Site_Detail__c where id in :splitString];
          System.Debug('My Site Records' + mySiteRecords);
          RT_Agreement = [SELECT Id from RecordType WHERE Name = 'Agreement' limit 1];
          System.Debug(RT_Agreement);
    }

}