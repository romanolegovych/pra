global with sharing class RM_ProjectDetailsController {
	public string projectRID{get; private set;}
	public WFM_Project__c project{get; private set;}
	public string protocol{get; private set;}
	public string theraArea{get; private set;}
    public string phase{get; private set;}
    public Boolean bBid{get;private set;}
  //  public string assignType{get; private set;}
    public list<string> lstBU{get; private set;}
    public string destLink{get; private set;}
    
    private map<string, List<string>> emplListMap;
    
    
	public RM_ProjectDetailsController(){
		projectRID = ApexPages.currentPage().getParameters().get('ID');
		//userName = UserInfo.getName();
        user u = RM_OtherService.GetUserInfo(UserInfo.getUserId());
        if (u != null){ 
        	lstBU = RM_OtherService.getBUinGroup();
        }
		getProjectfromID(projectRID);
		//GetEmployeeListByProject(projectRID);
        
	}
	@RemoteAction
   	global static list<string> getRoleData( string pRID, string BU){
    	
		list<string>lstRole = new list<string>();
		
		
		List<AggregateResult> roleList =  RM_AssignmentsService.GetProjectRoleActiveEmplAccountInBU(pRID, BU);
       
        for(Integer i = 0; i < roleList.size(); i++){
            string sId = string.valueof(roleList[i].get('Project_Function__c'));
           	string nCount = string.valueOf(roleList[i].get('nEmpl'));
           	string role = sID +' ('+nCount + ')';
           	lstRole.add(role);
        }
        return lstRole;
   	 }
   	@RemoteAction 
   	global static list<string> getEmplData(string pRID, string projectRole, string bu){
		
		list<string> lstEmp = new list<string>();
		system.debug('---projectRole---' + projectRole);  
  		list<WFM_employee_Allocations__c> emplList = RM_AssignmentsService.GetActiveEmployeeListByRoleProject(pRID,  projectRole, bu);
        for(WFM_employee_Allocations__c a : emplList){
        	string empl = a.EMPLOYEE_ID__c + '_' + a.employee_id__r.full_name__c;
        	lstEmp.add(empl);
        }
        
       return lstEmp;
        	
	}
	private  void getProjectfromID(string pID){
        system.debug('---project---' + pID);
        
        if (pID <> null){        
            if (pID.Length() > 0)  {
                
                List<WFM_Project__c> lstProj = RM_ProjectService.GetProjectbyrID(projectRID);
				if (lstProj.size() > 0){
					project = lstProj[0]; 
                
                    system.debug('---project---' + project);
                    if (project.protocols__r != null && project.protocols__r.size() > 0){
                        theraArea = project.Protocols__r[0].Therapeutic_Area__c;
                        phase = project.Protocols__r[0].phase__c;
                        protocol = project.Protocols__r[0].name;
                    }
                    else{
                        theraArea = ''; 
                        phase='';
                        protocol = '';
                    }
                    if (Project.Bid_Defense_Date__c==null)
                        bBid = false;
                    else
                        bBid = true;
                 
                }                
            }
        }
        
    }
   
    
   /* public string getBUJsonData(){
    	list<string> buList = RM_OtherService.getBUinGroup();
    	
		string buJsonStr = '';
		for (string s:buList){
			buJsonStr += '{ label: "' + s + '", icon: "needurl", expanded: false, items:[' + getRoleData(s) +'] },';
		}
		if (buList.size() > 0)
        	
        	return buJsonStr;
        else 
        	return null;
    }
    private string getRoleData(string BU){
    	
		string roleData = '';
		
		
		List<AggregateResult> roleList =  RM_AssignmentsService.GetProjectRoleActiveEmplAccountInBU(projectRID, BU);
       
        for(Integer i = 0; i < roleList.size(); i++){
            string sId = string.valueof(roleList[i].get('Project_Function__c'));
           	string nCount = string.valueOf(roleList[i].get('nEmpl'));
           	string role = sID +' ('+nCount + ')';
           	//expanded: false, items:[
            roleData += '{label : "' + role + '", value: "rm_myprojectAlloRoleChart?role=' + sId + '&id=' + projectRID + '&bu='+ bu + '",'
            + 'expanded: false, items:['+ getEmplData(sID)+ ' ]},';//, icon: "{!URLFOR($Resource.RM_Images,\'group.jpg\')" },';            
                  
        } 
         system.debug('---roleData---' + roleData);
        if (roleList.size() > 0)
        	
        	return roleData.substring(0, roleData.length()-1);
        else 
        	return null;
	}
	private string getEmplData(string projectRole){
		
		string jsonEmpl = '';    
		system.debug('---projectRole---' + projectRole);  
      	if (emplListMap.containsKey(projectRole)){
      		
	        list<string> empList = emplListMap.get(projectRole);
	        system.debug('---empList---' + empList);  
	        if (empList.size() > 0){
	        	
		        for (string e: empList){
		        	 system.debug('---e---' + e); 
		        	jsonEmpl += e + ',';
		        }
	        }
        
      	}
      	 if (jsonEmpl.length() > 0)
        	
        	return jsonEmpl.substring(0, jsonEmpl.length()-1);
        else 
        	return null;
        	
	}
	private void GetEmployeeListByProject(string projectRID){
	 	map<string, List<string>> mapRole = new map<string, List<string>>();
	 	list<string> EmpList = new list<string>();
	 	set<string> emplSet = new set<string>();  //remove duplicate
	 	string role = '';
	 	
	 	for (WFM_employee_Allocations__c a: RM_AssignmentsDataAccessor.GetActiveEmployeeListByProject(projectRID)){
	 		string jsonTreeNote = '{label : "' + a.employee_id__r.full_name__c + '", value: "rm_viewResource?id=' +  a.employee_id__c + '"}';
	 	
	 		if (role != a.Project_Function__c){
	 			if (role != '' && EmpList.size() > 0){//make map
	 				
	 				mapRole.put(role, EmpList);	 				
	 				EmpList = new list<string>();
	 				emplSet = new set<string>();
	 			}
	 			role = a.Project_Function__c;
	 			if(!emplSet.contains(a.employee_id__r.full_name__c)){ 
	 				EmpList.add(jsonTreeNote);
	 				emplSet.add(a.employee_id__r.full_name__c);
	 			}
	 		}
	 		else{
	 			if(!emplSet.contains(a.employee_id__r.full_name__c)){ 
	 				emplSet.add(a.employee_id__r.full_name__c);
	 				EmpList.add(jsonTreeNote);
	 			}
	 		}
	 			
		}
		mapRole.put(role, EmpList);
		system.debug('---mapRole---' + mapRole);
	 	EmplListMap = mapRole;
	}*/
}