/** Implements the test for the Selector Layer of the object LaboratorySamples__c
 * @author	Dimitrios Sgourdos
 * @version	29-Oct-2013
 */	
@isTest
private class LaboratorySamplesDataAccessorTest {
	
	/** Test the function getByProjectId
	 * @author	Dimitrios Sgourdos
	 * @version	29-Oct-2013
	 */
	static testMethod void getByProjectIdTest() {
		// Create projects
		List<Client_Project__c> cpList = new List<Client_Project__c>();
		cpList.add(BDT_TestDataUtils.buildProject());
		cpList.add(BDT_TestDataUtils.buildProject());
		insert cpList;
		
		// Create Laboratory Samples
		List<LaboratorySamples__c> lsList = new List<LaboratorySamples__c>();
		lsList.add(new LaboratorySamples__c(ShipmentNumber__c=3, SequenceNumber__c=1, Project__c=cpList[0].Id));
		lsList.add(new LaboratorySamples__c(ShipmentNumber__c=1, SequenceNumber__c=2, Project__c=cpList[0].Id));
		lsList.add(new LaboratorySamples__c(ShipmentNumber__c=1, SequenceNumber__c=1, Project__c=cpList[0].Id));
		lsList.add(new LaboratorySamples__c(ShipmentNumber__c=2, SequenceNumber__c=1, Project__c=cpList[1].Id));
		insert lsList;
		
		// Query for the fist dummy Id
		List<LaboratorySamples__c> results = LaboratorySamplesDataAccessor.getByProjectId(cpList[0].Id);
		system.assertEquals(results.size(), 3);
		
		// Check if the results are ordered by ShipmentNumber__c
		system.assertEquals(results[0].Id, lsList[2].Id);
		system.assertEquals(results[0].ShipmentNumber__c, 1);
		system.assertEquals(results[0].SequenceNumber__c, 1);
		system.assertEquals(results[1].Id, lsList[1].Id);
		system.assertEquals(results[1].ShipmentNumber__c, 1);
		system.assertEquals(results[1].SequenceNumber__c, 2);
		system.assertEquals(results[2].Id, lsList[0].Id);
		system.assertEquals(results[2].ShipmentNumber__c, 3);
		system.assertEquals(results[2].SequenceNumber__c, 1);
	}
}