/**
* @author       Sukrut Wagh
* @date         06/24/24
* @description  Utility class providing boiler plate reusable methods
*/

global class COM_Utils {
    
    global static final String EMPTY = '';
    global static final String PROD_ID = '00D80000000Kwgb';
    global static final String DOMAIN_NAME = '@prahs.com';

    /**
     * @author      Sukrut Wagh
     * @date        06/24/24
     * @description Determins if the object is null/empty. 
     *              Supported object types: String, List.
     *              Note: Set<String> is not recognized as an instanceof Set<object> and 
     *              Map<String,String> is not recognized as an instanceof Map<Object,Object>
     * @param obj
     * @return boolean
     */
    global static Boolean isNotEmpty(final Object obj) {
        if(null != obj) {
            if(obj instanceof String) {
                return String.isBlank((String)obj) ? false : true;
            } else if(obj instanceof List<Object>) {
                return (((List<Object>)obj).isEmpty()) ? false : true;
            } /*else if(obj instanceof Set<Object>) {
                return (((Set<Object>)obj).isEmpty()) ? false : true;
            } else if(obj instanceof Map<Object,Object>) {
                return (((Map<Object,Object>)obj).isEmpty()) ? false : true;
            }*/
            return true;
        }
        return false;
    }
    
    global static Boolean isEmpty(final Object obj) {
        return !isNotEmpty(obj);
    }
    
    /**
    * @description Converts the list to a String. Values are separated by the separator character.
    * @param l          list of string
    * @param separator  separator character
    * @param wrapStr    optional. In case the value has to be wrappep/surronded by a string. 
    *                   e.g: when wrapStr=quote & separator=comma, returned string will be in format 'val 1','val 2'
    */
    global static String toString(final List<String> l, final String separator, String wrapStr) {
        String resp = '';
        if(COM_Utils.isEmpty(wrapStr)){
            wrapStr = '';
        }
        if(COM_Utils.isNotEmpty(l) && COM_Utils.isNotEmpty(separator)) {
            for(String val : l) {
                resp += wrapStr+val+wrapStr+separator;
            }
            if(resp.endsWith(separator)) {
                resp = resp.removeEndIgnoreCase(separator);
            }
        }
        return resp;
    }
    
    /*
        Copied from https://code.google.com/p/apex-lang/source/browse/tags/1.10/src/classes/StringUtils.cls
        http://commons.apache.org/proper/commons-lang/javadocs/api-release/index.html
        Gets the substring after the last occurrence of a separator. The separator is not returned.
        A null string input will return null. An empty ("") string input will return the empty string. 
        An empty or null separator will return the empty string if the input string is not null.
        
        If nothing is found, the empty string is returned.
        
         StringUtils.substringAfterLast(null, *)      = null
         StringUtils.substringAfterLast("", *)        = ""
         StringUtils.substringAfterLast(*, "")        = ""
         StringUtils.substringAfterLast(*, null)      = ""
         StringUtils.substringAfterLast("abc", "a")   = "bc"
         StringUtils.substringAfterLast("abcba", "b") = "a"
         StringUtils.substringAfterLast("abc", "c")   = ""
         StringUtils.substringAfterLast("a", "a")     = ""
         StringUtils.substringAfterLast("a", "z")     = ""
            
    */
    
    /**
    * @description  Gets the substring after the last occurrence of a separator. The separator is not returned.
    *               A null string input will return null. An empty ("") string input will return the empty string. 
    *               An empty or null separator will return the empty string if the input string is not null.
    *               If nothing is found, the empty string is returned. 
    * @param str original string.
    * @param separator separator string.
    * @return String Substring after the last occurrence of a separator.    
    */
    global static String substringAfterLast(final String str, final String separator) {
        if (isEmpty(str)) {
            return str;
        }
        if (isEmpty(separator)) {
            return EMPTY;
        }
        Integer pos = str.lastIndexOf(separator);
        if (pos == -1 || pos == (str.length() - separator.length())) {
            return EMPTY;
        }
        return str.substring(pos + separator.length());
    }
    
    /**
    * @description  Returns the sObject type API name without __c suffix
    * @param soType The sObjectType for which the name to be obtained.
    * @return String - The API name without __c suffix.    
    */
    global static String getObjectName(Schema.sObjectType soType) {
        String originalTypeName = soType.getDescribe().getName();
        String typeName = originalTypeName;
        if (originalTypeName.toLowerCase().endsWith('__c')) {
            Integer index = originalTypeName.toLowerCase().indexOf('__c');
            typeName = originalTypeName.substring(0, index);
        }
        return typeName;
    }
    
    /**
    * @description Returns the sObject type API name.
    * @param SObject The SObject for which the name to be obtained.
    * @return String - The type name.
    */
    global static String getSObjectTypeName(SObject so) {
        return so.getSObjectType().getDescribe().getName();
    }
    
    /**
    * @description Enum for Sfdc environments.
    */
    //global Enum Env { DEV, CI, TEST, UAT, PROD }
    public static Set <string> ListofEnvs= new Set <string> {'PERSONAL','CI','TEST','UAT','PRA.MY.'};
    
    /**
    * @author   Yatish Tandon
    * @date     March 2015   
    * @description  Detects the Environment Name from the Host URL
    **/
    /* In future if any of the Org Name, Host URL and the Org Id  changes then we need to update the string value */
    global static String getEnv() {
        // Org ID for  Production (00D80000000Kwgb)
         if(UserInfo.getOrganizationId().contains('00D80000000Kwgb')){
            return 'PRA.MY.';
        }         
        String EnvUrl= (Test.isRunningTest()) ?'https://pra--ci1.cs7.my.salesforce.com' :URL.getSalesforceBaseUrl().toExternalForm();                                        
        string ENVIRONMENT = 'DEV';         
        for(String s: ListofEnvs){
            if(EnvUrl.containsIgnoreCase(s)){
                ENVIRONMENT = s;                    
            }
        }
        system.debug('------ENVIRONMENT ------------'+ENVIRONMENT );  
        return ENVIRONMENT;   
    }
    
    /**
    * @description  Determines if the organization is a sandbox
    * @return isSandbox.    
    */
    global static Boolean isSandbox() {
        Boolean isSandbox = true;
        isSandbox= ListofEnvs.contains(getEnv());        
        /**
        * URL.getSalesforceBaseUrl().getHost().left(2).equalsIgnoreCase('cs');
        * The above version will not work as PRA uses custom domain url's.
        * TODO: select Id, IsSandbox from Organization limit 1  [In Summer '14, (version 31.0)]
        * Change to the above implementation once we are at version 31.0
        */
        return isSandbox;
    }
    
    global static String generateRandomEmail() {         
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(DateTime.Now().format('ddmmyyHHmmss'));
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;

        String randomEmail = uniqueName + DOMAIN_NAME;
        system.debug('==========>>>'+ randomEmail);

        return randomEmail;     
    }        
}