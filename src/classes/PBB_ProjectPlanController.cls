/**
@author Bhargav Devaram
@date 2014
@description this controller class is to display the Project Plan to a scenario results
**/
public class PBB_ProjectPlanController {
    
    /** 
* to hide/show the main tabs in the component.
*/
    public  List<String> HiddenHomeTabsList{get;set;}
    
    /** 
* to hide/show the subtabs in the component.
*/
    public  List<String> HiddenSubTabsList{get;set;}
    
    /** 
* URL for iframe to generate the project plan by WR model
*/    
    public string dynamicURL{get; set;}   
    
    /** 
* select ScenarioID
*/
    public String ScenarioID{get;set;}
    
    /** 
* to show iframe if the scenario is valid
*/
    public Boolean ShowIframe { get; set; }
    
    /** 
* PBB Scenario object ID
*/
    public String PBBScenarioID{ get; set; }  
    
    /** 
* PBB Scenario object 
*/            
    public PBB_Scenario__c PBBScenario{ get; set; }         
    
    /** 
@author Bhargav Devaram
@date 2014  
@description Constructor 
*/ 
    Public PBB_ProjectPlanController () {
        HiddenHomeTabsList = new List<String>();
        HiddenSubTabsList = new List<String>();
        
//        HiddenSubTabsList.add('CUGpage');
        HiddenSubTabsList.add('RDpage');
        HiddenSubTabsList.add('Ppage'); 
        
        /** 
* For displaying section header
*/
        PBBScenarioID= ApexPages.currentPage().getParameters().get('PBBScenarioID');
        if(PBBScenarioID!=null && PBBScenarioID!='')
            PBBScenario = PBB_DataAccessor.getScenarioByID(PBBScenarioID);
        
        dynamicURL = URL.getSalesforceBaseUrl().toExternalForm();       
        system.debug('---dynamicURL ---'+dynamicURL ); 
        
        ShowIframe =false;
        ScenarioID = ApexPages.currentPage().getParameters().get('PBBScenarioID');  
        if(ScenarioID!=null && ScenarioID!=''){
            generateProjectPlan();
        } 
    }
    
    /** 
@author Bhargav Devaram
@date 2014 
@description to generate the URL to generate the plan for the project scenario  
*/
    public void generateProjectPlan() {         
        PBB_Scenario__c srmObj = PBB_DataAccessor.getScenarioByID(ScenarioID);
        if(srmObj!=null ) {
            Bid_Project__c bidObjId = PBB_DataAccessor.getBidProjectByID(srmObj.Bid_Project__c);
            dynamicURL = URL.getSalesforceBaseUrl().toExternalForm();       
            system.debug('---dynamicURL ---'+dynamicURL );      
            dynamicURL+='/apex/PAWS_ProjectFlowGenerator?scenarioId='+srmObj.Id;
            ShowIframe =true;                   
        }             
        else {            
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Invalid ScenarioID'));         
            ShowIframe =false;
        }
    }
}