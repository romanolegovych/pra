@isTest
private class AA_EditRevenueRateExtensionTest{   

    /* Test Search Code Function */
    static testMethod void searchCodeTest() {
    
       Revenue_Allocation_Rate__c revObj = getDummyRevRate();
       
       PageReference oPage = new PageReference('/apex/AA_Edit_Revenue_Allocation_Rate?functionId='+revObj.Id);
       Test.setCurrentPage(oPage);
       
       
       ApexPages.StandardController stController = new ApexPages.StandardController(revObj);
       
       AA_EditRevenueRateExtension controller = new AA_EditRevenueRateExtension(stController);
         
       List<BUF_Code__c > listResult= AA_EditRevenueRateExtension.searchCode('BU1F1');
         
       System.assertNotEquals(listResult.size(),0);
    }
    
    
    /* Test BUF Select Options Function */
    static testMethod void getbufselectoptionTest() {
    
       Revenue_Allocation_Rate__c revObj = getDummyRevRate();
       
       PageReference oPage = new PageReference('/apex/AA_Edit_Revenue_Allocation_Rate?functionId='+revObj.Id);
       Test.setCurrentPage(oPage);
       
       
       ApexPages.StandardController stController = new ApexPages.StandardController(revObj);
       
       AA_EditRevenueRateExtension controller = new AA_EditRevenueRateExtension(stController);
         
       List<SelectOption> listResult = controller.getbufselectoption();
         
       System.assertNotEquals(listResult.size(),0);
    }
    
        
    /* Test Currency Select Options Function */
    static testMethod void getcurrencyselectoptionTest() {
    
       Revenue_Allocation_Rate__c revObj = getDummyRevRate();
       
       PageReference oPage = new PageReference('/apex/AA_Edit_Revenue_Allocation_Rate?functionId='+revObj.Id);
       Test.setCurrentPage(oPage);
       
       
       ApexPages.StandardController stController = new ApexPages.StandardController(revObj);
       
       AA_EditRevenueRateExtension controller = new AA_EditRevenueRateExtension(stController);
         
       List<SelectOption> listResult = controller.getcurrencyselectoption();
         
       System.assertNotEquals(listResult.size(),0);
    }
    
    /* Test Back Button Pressed Function */
    static testMethod void saveTest() {
    
       Revenue_Allocation_Rate__c revObj = getDummyRevRate();
       
       PageReference oPage = new PageReference('/apex/AA_Edit_Revenue_Allocation_Rate?functionId='+revObj.Id);
       Test.setCurrentPage(oPage);
       
       
       ApexPages.StandardController stController = new ApexPages.StandardController(revObj);
       
       AA_EditRevenueRateExtension controller = new AA_EditRevenueRateExtension(stController);
         
       PageReference pageRef = controller.Save();
         
       System.assertEquals(pageRef.geturl(),Page.AA_Revenue_Allocation_Rate.geturl());
    }
    
    
    /* Test Back Button Pressed Function */
    static testMethod void backTest() {
    
       Revenue_Allocation_Rate__c revObj = getDummyRevRate();
       
       PageReference oPage = new PageReference('/apex/AA_Edit_Revenue_Allocation_Rate?functionId='+revObj.Id);
       Test.setCurrentPage(oPage);
       
       
       ApexPages.StandardController stController = new ApexPages.StandardController(revObj);
       
       AA_EditRevenueRateExtension controller = new AA_EditRevenueRateExtension(stController);
         
       PageReference pageRef = controller.back();
         
       System.assertEquals(pageRef.geturl(),Page.AA_Revenue_Allocation_Rate.geturl());
    }
    
    /* Test Delete Button Pressed Function */
    static testMethod void deactivateTest() {
    
       Revenue_Allocation_Rate__c revObj = getDummyRevRate();
       
       PageReference oPage = new PageReference('/apex/AA_Edit_Revenue_Allocation_Rate?functionId='+revObj.Id);
       Test.setCurrentPage(oPage);
       
       
       ApexPages.StandardController stController = new ApexPages.StandardController(revObj);
       
       AA_EditRevenueRateExtension controller = new AA_EditRevenueRateExtension(stController);
         
       PageReference pageRef = controller.DeActivate();
         
       System.assertEquals(pageRef.geturl(),Page.AA_Revenue_Allocation_Rate.geturl());
    }    
    
    /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }   
    
    /*Get Dummy BUFCode */
    static BUF_Code__c getDummyBUFCode(){
        AA_TestUtils testUtil = new AA_TestUtils();
        return testUtil.getBufCode();
    }  
    
    /*Get Dummy Rev Rate*/
    static Revenue_Allocation_Rate__c getDummyRevRate(){
        AA_TestUtils testUtil = new AA_TestUtils();
        return testUtil.getRevRate();
    } 
 }