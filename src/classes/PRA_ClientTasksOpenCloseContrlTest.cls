/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

/**
*   'PRA_ClientTasksOpenCloseContrlTest' class is to cover the test coverage for 'PRA_ClientTasksOpenCloseController' class.
*   @author   Devaram Bhargav
*   @version  11-Nov-2013
*   @since    11-Nov-2013
*/
@isTest
private  class PRA_ClientTasksOpenCloseContrlTest {
    
    static testMethod void testconstructor() {
        
        // given 
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        // Zero out all effort ratios
        List<Client_Task__c> ctList = [select id,project__r.name,Project__c,status__c
                                          from Client_Task__c
                                          where Project__r.id = :tu.clientProject.id
                                          ];
        
        // when
        test.startTest();
           
            PRA_ClientTasksOpenCloseController prctoc = new PRA_ClientTasksOpenCloseController();              
            System.assertEquals(null, prctoc.project);
            System.assertEquals(prctoc.selectedAction, PRA_ClientTaskService.TO_DISPLAY_ALL_CLIENT_TASKS);
            System.assertNotEquals(prctoc.selectedAction, PRA_ClientTaskService.TO_RE_OPEN_CLIENT_TASKS);
            System.assertNotEquals(prctoc.selectedAction, PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS);
            //prctoc.selectedAction=prctoc.TO_DISPLAY_ALL_CLIENT_TASKS;
            prctoc.getTaskListWrapper();
            prctoc.getActionList();
            prctoc.Reset();
            System.assertEquals(prctoc.selectedProjectlabel, PRA_Constants.ST_DEFAULT_PROJECT_LABEL);            
            prctoc.ProjectText=tu.clientProject.Name;
            prctoc.MainSearch();
            System.assertNotEquals(null, prctoc.project);
            prctoc.getTaskListWrapper();
            System.assertEquals(prctoc.selectedProjectLabel, prctoc.ProjectText);
            prctoc.selectAllTasks();
            prctoc.selectedTasks='["'+ctList[0].id+'"]';
            system.debug('-----selectedTasks1-------'+prctoc.selectedTasks);
            prctoc.selectedAction=PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS;
            prctoc.updateClientTasksclosedate();
            PRA_ClientTasksOpenCloseController prctoc1 = new PRA_ClientTasksOpenCloseController();
            
            
            
        test.stopTest();        
    } 

}