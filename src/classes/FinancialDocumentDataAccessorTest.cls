/** Implements the test for the Selector Layer of the object FinancialDocument__c
 * @author	Dimitrios Sgourdos
 * @version	16-Dec-2013
 */
@isTest
private class FinancialDocumentDataAccessorTest {
	
	/** Test the function getFinancialDocumentById
	 * @author	Dimitrios Sgourdos
	 * @version	16-Dec-2013
	 */
	static testMethod void getFinancialDocumentByIdTest() {
		// Create data
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001',
																				'BDT Project First Experiments');
		insert currentProject;
		
		FinancialDocument__c financialDocument = BDT_BuildTestData.buildFinancialDocument(currentProject.Id,
																							new List<Study__c>());
		insert financialDocument;
		
		// Check the function
		FinancialDocument__c result = FinancialDocumentDataAccessor.getFinancialDocumentById(financialDocument.Id);
		system.assertEquals(result.Id, financialDocument.Id, 'Error in reading financial document from the system');
	}
	
	
	/** Test the function getFinancialDocuments
	 * @author	Dimitrios Sgourdos
	 * @version	20-Jan-2013
	 */
	static testMethod void getFinancialDocumentsTest() {
		// Create data
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001',
																				'BDT Project First Experiments');
		insert currentProject;
		
		FinancialDocument__c financialDocument = new FinancialDocument__c();
		financialDocument.DocumentType__c	= 'Proposal';
		financialDocument.Client_Project__c	= currentProject.Id;
		insert financialDocument;
		
		// Check the function
		String errorMessage = 'Error in retrieving the financial documents';
		String whereClause = 'Client_Project__c = ' + '\'' + currentProject.Id + '\'';
		
		List<FinancialDocument__c> results = FinancialDocumentDataAccessor.getFinancialDocuments( whereClause,
																								'DocumentType__c');
		system.assertEquals(results.size(), 1, errorMessage);
		system.assertEquals(results[0].Id, financialDocument.Id, errorMessage);
	}
}