/** 
    @ author Niharika Reddy
    @ date 2014
    @ Test class for all APIs used in PBB
    */
 
@isTest
Public class PBB_WR_APIsTest {
    static testMethod void testPBB_WR_APIsTest() {
   
        test.startTest();
         //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils ();
        tu.jcd = tu.createJobClass();
        tu.wfmp = tu.createwfmproject();
        tu.bid =tu.createbidproject();        
        tu.scen =tu.createscenarioAttributes();
        tu.country =tu.createCountryAttributes();
        tu.psc =tu.createPBBScenarioCountry();   
        tu.smm = tu.createServiceModelAttributes();
        tu.sa = tu.createServiceAreaAttributes();
        tu.sf = tu.createServiceFunctionAttributes();
        tu.St = tu.createServiceTaskAttributes();
        tu.smm = tu.approveServicemodel(tu.smm);
        tu.Wrm = tu.createWRModelAttributes();
        tu.Fl= tu.createFlowAttributes();
        tu.Fsj= tu.createFlowStepAttributes();
        tu.stfs= tu.createTaskToFlow();
        tu.scen.WR_Model__c  = tu.Wrm.id;
        tu.scecont = tu.createSrmCountryAttributes();
        tu.pweList = tu.createPBBWeeklyEvents(tu.scecont,5);
        update tu.scen;
        tu.CSt= tu.createCountryServiceTaskAttributes();
        tu.protocol = tu.createProtocol();
        tu.site = tu.createSite();
        tu.protocolCountry = tu.createProtocolCountry();
        tu.countryWeekly = tu.createCountryWeeklyEvents();
        
        
        PBB_WR_APIs.GetScenarioAttributes(tu.scen.id);
        PBB_WR_APIs.GetSelectedCountries(tu.scen.id);
        PBB_WR_APIs.GetSelectedProjectCountries(tu.scen.id);
        PBB_WR_APIs.GetSelectedServiceTasks(tu.scen.id,tu.country.id);
        PBB_WR_APIs.GetJobPositionList ();
        Set<ID>  wrStepId = new Set<ID>();
        PBB_WR_APIs.GetAssociatedServiceTasksToStep(tu.Wrm.id,wrStepId );
        PBB_WR_APIs.LinkScenarioToPAWSProject(tu.scen.id,tu.Fl.id);
        PBB_WR_APIs.PAWSProjectAfterUpdate(tu.scen.id);
        PBB_WR_APIs.countryServiceTasksGroupbyServicetask(tu.scen.id);
        PBB_WR_APIs.GetPatientEnrollmentAttributes(tu.protocol.Protocal_Unique_Key__c);
        PBB_WR_APIs.getAprrovedScenarioID(tu.bid.PRA_Project_ID__r.Name);
        PBB_WR_APIs.getPbbWeeklyEvntsByCountryId(tu.scen.id,tu.country.id);
        PBB_WR_APIs.getPatientEnrollmntStatusByCountry(tu.protocol.Protocal_Unique_Key__c);
        PBB_WR_APIs.getPatientEnrollmntStatusBySite(tu.protocol.Protocal_Unique_Key__c);
        PBB_WR_APIs.weekEventsByProject(tu.protocol.Protocal_Unique_Key__c);
        PBB_WR_APIs.getcountryWeeklyevents(tu.protocol.Protocal_Unique_Key__c);
        //PBB_WR_APIs.setWeeklyEventsByProtocolUniqKey(tu.protocol.Protocal_Unique_Key__c,null);
        
        test.stopTest();
   }    
}