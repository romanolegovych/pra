@isTest
private class AA_RevenueRateControllerTest{   
    
 /* Test Year Select options */
    static testMethod void getyearSelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_RevenueRateController controller = new AA_RevenueRateController();
         List < SelectOption > selection = controller.getyearSelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
     /* Test BU Select options */
    static testMethod void getbuSelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_RevenueRateController controller = new AA_RevenueRateController();
         List < SelectOption > selection = controller.getbuSelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
    /* Test FC Select options */
    static testMethod void getfcSelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_RevenueRateController controller = new AA_RevenueRateController();
         List < SelectOption > selection = controller.getfcSelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
    /* Test Country Select options */
    static testMethod void getcountrySelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_RevenueRateController controller = new AA_RevenueRateController();
         List < SelectOption > selection = controller.getcountrySelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
     /* Test Is Active Select options */
    static testMethod void getisactiveSelectoptionTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_RevenueRateController controller = new AA_RevenueRateController();
         List < SelectOption > selection = controller.getisactiveSelectoption();
         
         System.assertNotEquals(selection.size(),0);
    }
    
    /* Test goToRateDetail() */
    static testMethod void goToRateDetailTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_RevenueRateController controller = new AA_RevenueRateController();
         
         PageReference pageRef = Page.AA_Edit_Revenue_Allocation_Rate;

         PageReference thisPage = controller.goToRateDetail();
         
         System.assertEquals(thisPage.geturl() ,pageRef.geturl() );
    }
    
     /* Test Create Page redirection */
    static testMethod void createTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_RevenueRateController controller = new AA_RevenueRateController();
         PageReference pageRef= controller.create();
         
         System.assertEquals(pageRef.getUrl(),Page.AA_New_Revenue_Allocation_Rate.getUrl());
    }
    
    /* Test upload Page redirection */
    static testMethod void uploadTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_RevenueRateController controller = new AA_RevenueRateController();
         PageReference pageRef= controller.upload();
         
         System.assertEquals(pageRef.getUrl(),Page.AA_Revenue_Allocation_Rate_Upload.getUrl());
    }
    
    /* Test submit button */
    static testMethod void submitTest() {
    
         initTestData ();
         
         Test.startTest();
         
         AA_RevenueRateController controller = new AA_RevenueRateController();
         PageReference pageRef = controller.submit();
         
         System.assertEquals(pageRef,null);
    }
    
    /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }   
       
 }