@isTest
private class ServiceFunctionTest {
	
	private static final String TEST_VALUE = 'TEST_VALUE';
		
	private static ServiceFunction sf;
	
	private static List<Service_Function__c> serviceFunctions;
	private static PBB_TestUtils testUtils = new PBB_TestUtils();
	
	@isTest
	private static void testConstructor(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		testUtils.sa = testUtils.CreateServiceAreaAttributes();
		Test.startTest();
			sf = new ServiceFunction(testUtils.sa.Id);
		Test.stopTest();
		System.assertEquals( testUtils.sa.Id, sf.parentServiceId );
		System.assertEquals( testUtils.sa.Id, sf.previousServiceId );
	}

	@isTest
	private static void testProperties(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		testUtils.sa = testUtils.CreateServiceAreaAttributes();
		sf = new ServiceFunction(testUtils.sa.Id);
		Test.startTest();
			sf.serviceFunctions = new List<Service_Function__c>{ new Service_Function__c() };
			sf.serviceFunction = new Service_Function__c();
			sf.serviceFunctionForUpdate = new Service_Function__c();
		Test.stopTest();
		System.assertEquals( ServiceFunction.FUNCTION_WORD, sf.getServiceWord() );
		System.assertEquals( 1, sf.serviceFunctions.size() );
		System.assertNotEquals( null, sf.serviceFunction );
		System.assertNotEquals( null, sf.serviceFunctionForUpdate );
	}
	
	@isTest
	private static void refreshServicesTest(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		testUtils.sa = testUtils.CreateServiceAreaAttributes();
		sf = new ServiceFunction(testUtils.sa.Id);
		testUtils.CreateServiceFunctionAttributes();
		Test.startTest();
			sf.refreshServices();
		Test.stopTest();
		System.assertEquals( 1, sf.serviceFunctions.size() );
	}
	
	@isTest
	private static void preparationCreateOrEditServiceTestCreateNew(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		testUtils.sa = testUtils.CreateServiceAreaAttributes();
		sf = new ServiceFunction(testUtils.sa.Id);
		Test.setCurrentPage( Page.LayoutModel );
		Test.startTest();
			sf.preparationCreateOrEditService();
		Test.stopTest();
		System.assertEquals( null, sf.serviceFunctionForUpdate.Id );
		System.assertEquals( testUtils.sa.Id, sf.serviceFunctionForUpdate.Service_Area__c );
	}
	
	@isTest
	private static void preparationCreateOrEditServiceTestUpdate(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		testUtils.sa = testUtils.CreateServiceAreaAttributes();
		sf = new ServiceFunction(testUtils.sa.Id);
		sf.serviceFunction = testUtils.CreateServiceFunctionAttributes();
		sf.refreshServices();
		Test.setCurrentPage( Page.LayoutModel );
		ApexPages.currentPage().getParameters().put( 'editServiceFunctionId', sf.serviceFunction.Id );
		Test.startTest();
			sf.preparationCreateOrEditService();
		Test.stopTest();
		System.assertEquals( sf.serviceFunction.Id, sf.serviceFunctionForUpdate.Id );
	}
	
	@isTest
	private static void preparationCreateOrEditServiceTestClone(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		testUtils.sa = testUtils.CreateServiceAreaAttributes();
		sf = new ServiceFunction(testUtils.sa.Id);
		sf.serviceFunction = testUtils.CreateServiceFunctionAttributes();
		sf.serviceFunction.Name = TEST_VALUE;
		Test.setCurrentPage( Page.LayoutModel );
		ApexPages.currentPage().getParameters().put( 'cloneServiceFunctionId', sf.serviceFunction.Id );
		Test.startTest();
			sf.preparationCreateOrEditService();
		Test.stopTest();
		System.assertEquals( null, sf.serviceFunctionForUpdate.Id );
		System.assertEquals( TEST_VALUE, sf.serviceFunctionForUpdate.Name );
	}
	
	@isTest
	private static void removeServiceTest(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		testUtils.sa = testUtils.CreateServiceAreaAttributes();
		sf = new ServiceFunction(testUtils.sa.Id);
		sf.serviceFunction = testUtils.CreateServiceFunctionAttributes();
		Test.startTest();
			sf.removeService();
		Test.stopTest();
		System.assertEquals( 0, sf.serviceFunctions.size() );
	}
	
	@isTest
	private static void removeServiceTestNullPointer(){
		testUtils.smm = testUtils.CreateServiceModelAttributes();
		testUtils.sa = testUtils.CreateServiceAreaAttributes();
		sf = new ServiceFunction(testUtils.sa.Id);
		Test.setCurrentPage( Page.LayoutModel );
		Test.startTest();
			sf.removeService();
		Test.stopTest();
		System.assertEquals( 1, ApexPages.getMessages().size() );
	}
			
}