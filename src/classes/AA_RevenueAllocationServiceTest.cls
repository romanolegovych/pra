@isTest
private class AA_RevenueAllocationServiceTest{   
    
    /* Test getting all buf codes */
    static testMethod void getBuCodesTest() {
    
         initTestData ();
         
         Test.startTest();
         
         List<Business_Unit__c> result = AA_RevenueAllocationService.getBusinessUnitCodes();
         
         System.assertNotEquals(result.size(),0);
         
         Test.stopTest();
    }
    
    /* Test getting all FC codes */
    static testMethod void getFCodesTest() {
    
         initTestData ();
         
         Test.startTest();
         
         List<Function_Code__c> result = AA_RevenueAllocationService.getFunctionCodes();
         
         System.assertNotEquals(result.size(),0);
         
         Test.stopTest();
    }
    
    
    /* Test getting all Aggregate Year Result */
    static testMethod void getValidYearsTest() {
    
         initTestData ();
         
         Test.startTest();
         
         List<AggregateResult> result = AA_RevenueAllocationService.getValidYears();
         
         System.assertNotEquals(result.size(),0);
         
         Test.stopTest();
    }
    
    /* Test getting all Countries */
    static testMethod void getCountriesTest() {
    
         initTestData ();
         
         Test.startTest();
         
         List<Country__c> result = AA_RevenueAllocationService.getCountries();
         
         System.assertNotEquals(result.size(),0);
         
         Test.stopTest();
    }

    
    
     /* Test inserting bufcode */
    static testMethod void getRateDetailsTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);

         
         System.assertNotEquals(AA_RevenueAllocationService.getRateDetails(BufCode1.Id),null);
         
         Test.stopTest();
    }
    
    
    /* Test existing rates */
    static testMethod void checkExistingTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);

         
         System.assertNotEquals(AA_RevenueAllocationService.checkExisting(BufCode1.Id,'2011'),null);
         
         Test.stopTest();
    }
    
    /* Test getting records by filter */
    static testMethod void getRecordsByFilterTest() {
    
         initTestData ();
         
         Test.startTest();
         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
         
         
         AA_RevenueAllocationService service = new AA_RevenueAllocationService();
         List<Revenue_Allocation_Rate__c> result = AA_RevenueAllocationService.getRecordsByFilter('2011',businessUnit1.Id,functionCode1.Id,country.Id,'Y');   
         
         System.assertEquals(result.size(),0);

         Test.stopTest();
    }
    
    /* Test getting bufcode details by name */
    static testMethod void getBufCodeIdTest() {
    
         initTestData ();
         
         Test.startTest();
         
         BUF_Code__c result = AA_RevenueAllocationService.getBufCodeId('BU1F1');
         
         System.assertEquals(result.Id,AA_RevenueAllocationService.getBufCodeId('BU1F1').Id);
         
         Test.stopTest();
    }
    
     /* Test getting Currency Id by name */
    static testMethod void getCurrencyIdTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Currency__c result = AA_RevenueAllocationService.getCurrencyId('USD');
         
         System.assertEquals(result.Id,AA_RevenueAllocationService.getCurrencyId('USD').Id);
         
         Test.stopTest();
    }
    
     /* Test check existing details */
    static testMethod void checkExistingRecord() {
    
         initTestData ();
         
         Test.startTest();
         
         
         Revenue_Allocation_Rate__c result = AA_RevenueAllocationService.checkExistingRecord(AA_RevenueAllocationService.getBufCodeId('BU1F1'),'2011');
         
         System.assertNotEquals(result,null);
         
         Test.stopTest();
    }
    
       
    /* Test getting rar id*/
    static testMethod void getRARIdTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Revenue_Allocation_Rate__c result = AA_RevenueAllocationService.getRARId();
         
         System.assertNotEquals(result,null);
         
         Test.stopTest();
    }
    
    
    /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }   
       
 }