public class LMS_CourseList {
    
    public Boolean selected{get;set;}
    public String courseId{get;private set;}
    public String courseCode{get;private set;}
    public String courseTitle{get;private set;}
    public String duration{get;private set;}
    public String courseStatus{get;private set;}
    public Integer targetDays{get;private set;}
    public Date available{get;private set;}
    public Date discontinued{get;private set;}
    
    public LMS_CourseList(LMS_Course__c c, Map<String,String> m){
        if(m.containsKey(c.Id))
            selected = true;
        else
            selected = false;
        courseId = c.Id;
        courseCode = c.Course_Code__c;
        courseTitle = c.Title__c;
        duration = c.Duration_Display__c;
        targetDays = Integer.valueOf(c.Target_Days__c);
        if(c.Available_From__c != null)
            available = c.Available_From__c;
        if(c.Discontinued_From__c != null)
            discontinued = c.Discontinued_From__c;
        courseStatus = c.Status__c;
    }
}