public class LMS_RoleAssignment {

	public Boolean selected{get;set;}
	public Date commitDate{get;set;}
	public String assignmentId{get;private set;}
    public String roleId{get;private set;}
    public String name{get;private set;}
    public String roleStatus{get;private set;}
    public String assignmentStatus{get;private set;}
    public String sync{get;private set;}
    public String assignedOn{get;private set;}
    public String assignedBy{get;private set;}
    public String processed{get;private set;}
    public String style{get;private set;}
    public String bgStyle{get;private set;}
    public String commitStyle{get;private set;}
    
    public LMS_RoleAssignment(LMS_Role_Course__c c) {
    	selected = false;
    	assignmentId = c.Id;
    	roleId = c.Role_Id__c;
    	name = c.Role_Id__r.Role_Name__c;
    	roleStatus = c.Role_Id__r.Status__c;
    	sync = c.Sync_Status__c;
    	assignmentStatus = c.Status__c;
	    assignedBy = c.Assigned_By__c; 
	    if(c.Status__c == 'Committed') {
        	processed = 'Processed';
        } else {
        	if(c.Commit_Date__c != null) {
        		commitDate = c.Commit_Date__c;
        	}
        }
    	if(c.Assigned_On__c != null) {
	        assignedOn = c.Assigned_On__c.format('MMMM dd, yyyy');
    	}
	    if(assignmentStatus == 'Draft'){
	        style = 'draftCourse';
	        bgStyle = 'draftRow';
	    } else if(assignmentStatus == 'Draft Delete') {
	        style = 'deleteCourse';
	        bgStyle = 'draftDeleteRow';
	    } else if(assignmentStatus == 'Committed') {
        	commitStyle = 'commitCourse';
        }
    }
}