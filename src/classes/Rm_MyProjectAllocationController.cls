public with sharing class Rm_MyProjectAllocationController {
	public boolean bVisHr{get; set;}
	private list<RM_AssignmentOp.ProjectAllocation> proAllList;
	private list<string> MonthLabList;
	
	private string projectRID;
	private Integer iStart = -3;
	private Integer iEnd = 5;
	public Rm_MyProjectAllocationController(){
		projectRID = ApexPages.currentPage().getParameters().get('id');
		MonthLabList = RM_Tools.GetMonthsList(iStart, iEnd);
		string unit = RM_OtherService.getUserDefaultUnit(UserInfo.getUserId());
    	if (unit == 'FTE')
    		 bVisHr = false;
    	else
    		 bVisHr = true;
		proAllList = getAllocation();
	}
	public list<string> getMonthLab(){
		return MonthLabList;
	}
	public PageReference refreshData(){
    	system.debug('---------refreshData---------');	
    	proAllList = getAllocation();
    	return null;
    }
	public list<RM_AssignmentOp.ProjectAllocation> getProAllocationList(){
		return proAllList;
	}
	private list<RM_AssignmentOp.ProjectAllocation> getAllocation(){
		return  RM_AssignmentOp.GetProjectAllocationList(projectRID, iStart, iEnd, bVisHr);
   			
   		
	}
}