@istest public with sharing class EWDIABatchTest
{
	@testsetup public static void setup()
	{
		EWBatchTest.setup();
		for (String chainName : EWDIABatch.DIA_EW_NAMES)
		{
			Critical_Chain__c chainRecord = new Critical_Chain__c(
				Name = chainName,
				PAWS_Project_Flow_Site__c = PAWS_ApexTestsEnvironment.ProjectSite.Id,
				Flow__c = PAWS_ApexTestsEnvironment.Flow.Id);
			
			insert chainRecord;
			
			List<Critical_Chain_Step_Junction__c> chainJunctions = new List<Critical_Chain_Step_Junction__c>();
			for (STSWR1__Flow_Step_Junction__c stepJunction : [select Name from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__c = :PAWS_ApexTestsEnvironment.Flow.Id])
			{
				chainJunctions.add(new Critical_Chain_Step_Junction__c(Flow_Step__c = stepJunction.Id, Critical_Chain__c = chainRecord.Id, Remaining_Time_Change_Date__c = System.today().addDays(-2)));
			}
			
			insert chainJunctions;
		}
	}
	
	@istest private static void coverBatch()
	{
		for (EWChain ew : EWChain.buildEWChains(new List<ID>(new Map<ID, Critical_Chain__c>([select Name from Critical_Chain__c]).keyset())))
		{
			new EWDIABatch().copyDIAData(ew, new PAWS_Project_Flow_Site__c());
		}
		
		new EWDIABatch().updateDIAData(new Critical_Chain__c(PAWS_Project_Flow_Site__c = [select Id from PAWS_Project_Flow_Site__c limit 1].get(0).Id));
		
		try
		{
			new EWDIABatch().execute(null, new List<SObject>{new Critical_Chain__c()});
		
		}
		catch (Exception e)
		{
			//
		}
		
		try
		{
			Test.startTest();
				EWDIABatch.executeBatch();
			Test.stopTest();
		
		}
		catch (Exception e)
		{
			//
		}
	}
}