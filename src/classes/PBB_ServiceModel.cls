public with sharing class PBB_ServiceModel extends PBB_ServiceBase {
    
    @TestVisible
    private static final String MODEL_WORD = 'Model';
    
    public List<Service_Model__c> serviceModels {
        get{
            return (List<Service_Model__c>) serviceRecords;
        }
        set{
            serviceRecords = value;
        }
    }
    
    public Service_Model__c serviceModel {
        get{
            return (Service_Model__c)serviceRecord;
        }
        set{
            serviceRecord = value;
        }
    }
    
    public Service_Model__c serviceModelForUpdate {
        get{
            return (Service_Model__c)serviceForUpdate;
        }
        set{
            serviceForUpdate = value;
        }
    }
    
    @TestVisible
    protected override String getServiceWord(){
        return MODEL_WORD;
    }
    
    public override void refreshServices(){
        serviceModels = PBB_ServiceModelServices.getServiceModels();
    }

    public override void preparationCreateOrEditService() {
        if(String.isBlank(getEditServiceId())) {
            serviceModelForUpdate = new Service_Model__c();
            serviceModelForUpdate.Status__c = 'In-Process';
        } else {
            serviceModelForUpdate = serviceModel;
        }
        if(!String.isBlank(getCloneServiceId())) {
            serviceModelForUpdate = serviceModel.clone(false, true);
            serviceModelForUpdate.Status__c = 'In-Process';
            serviceModelForUpdate.Service_Model_Unique_ID__c = null;
        }
    }
    
    public override void removeService() {
        try {
            delete serviceModel;
            refreshServices();
            //getDetailService();
        } catch(Exception e) {
            showErrorMessageOnDelete(e);
        }
    }   
    
}