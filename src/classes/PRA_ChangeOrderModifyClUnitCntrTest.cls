@isTest (SeeAllData=false)
private class PRA_ChangeOrderModifyClUnitCntrTest {

    static testMethod void shouldDisplayChangeOrderItem() {
        PRA_TestUtils tu = new PRA_TestUtils();
        
        tu.initAll();
         //client_project__c c=[select id,Currency_exchange_lock_date__c from client_project__c where id=:tu.clientProject.Id ];
        tu.clientProject.Standard_rate_year__c=date.today();
        update tu.clientProject;
        
        Change_Order__c co = tu.createChangeOrder(tu.clientProject.Id);
        Change_Order_Item__c coItem = 
        [select Id, Change_Order__r.Client_Project__r.Estimated_End_Date__c,
         Change_Order__r.Client_Project__r.Standard_rate_year__c,Change_Order__r.Client_Project__r.Currency_exchange_lock_date__c,Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Change_Order__c, Client_Unit_Number__c, 
        	Combo_Code__c, Description__c, End_Date__c, Forecast_Curve__c, Number_Of_Contracted_Units__c, Project_Region__c, Revised_Number_of_Units__c, 
        	Revised_Total_Cost__c, Revised_Unit_Cost__c, Start_Date__c, Task_Group__c, Total_Cost__c, Type__c, Unit_Completed_Event_Lookup__c, Unit_Cost__c, 
            Unit_of_Measurement__c, Worked_Hours__c, Worked_Hours_Done__c, Worked_Units__c, Worked_Units_Done__c,
            (select Id, Type__c from Change_Order_Line_Item__r)
            from Change_Order_Item__c 
            where Change_Order__c = :co.id
            limit 1];
        
        test.startTest();
        PRA_ChangeOrderModifyClUnitController coItemCntr = new PRA_ChangeOrderModifyClUnitController(new Apexpages.Standardcontroller(coItem));
        
        // check count of change order line items
        system.assertEquals(coItem.Change_Order_Line_Item__r.size(), (coItemCntr.liCOLineItemsNonLabor.size()+coItemCntr.liCOLineItemsLabor.size()));
        
        // check adding new change order line item
        coItemCntr.addChangeOrderLineItemLabor();
        system.assertEquals(coItem.Change_Order_Line_Item__r.size()+1, (coItemCntr.liCOLineItemsNonLabor.size()+coItemCntr.liCOLineItemsLabor.size()));
        
        test.stopTest();
    }
    
    static testMethod void shouldAddAndUpdateChangeOrderLineItems() {
        PRA_TestUtils tu = new PRA_TestUtils();
        
        tu.initAll();
        tu.clientProject.Standard_rate_year__c=date.today();
        update tu.clientProject;
        Change_Order__c co = tu.createChangeOrder(tu.clientProject.Id);
        Change_Order_Item__c coItem = 
        [select Id,Change_Order__r.Client_Project__r.Estimated_End_Date__c,
         Change_Order__r.Client_Project__r.Standard_rate_year__c,Change_Order__r.Client_Project__r.Currency_exchange_lock_date__c, Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Change_Order__c, 
	        Client_Unit_Number__c, Combo_Code__c, Description__c, End_Date__c, Forecast_Curve__c, Number_Of_Contracted_Units__c, Project_Region__c, 
	        Revised_Number_of_Units__c, Revised_Total_Cost__c, Revised_Unit_Cost__c, Start_Date__c, Task_Group__c, Total_Cost__c, Type__c, 
	        Unit_Completed_Event_Lookup__c, Unit_Cost__c, Unit_of_Measurement__c, Worked_Hours__c, Worked_Hours_Done__c, Worked_Units__c, 
	        Worked_Units_Done__c,
	        (select Id, Type__c, Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, Amendment_CO_Hours_unit__c, BUF_Code__c, 
	        Change_Order_Item__c, Current_Contract_Hours__c, Current_Contract_Value__c, Current_Hours_Unit__c, Current_Total_Cost__c, 
	        Current_Unit_Cost__c from Change_Order_Line_Item__r)
		    from Change_Order_Item__c 
		    where Change_Order__c = :co.id
		    limit 1];
        
        test.startTest();
        PRA_ChangeOrderModifyClUnitController coItemCntr = new PRA_ChangeOrderModifyClUnitController(new Apexpages.Standardcontroller(coItem));
        Map<Id, Change_Order_Line_Item__c> mapOrginalLineItems = new Map<Id,Change_Order_Line_Item__c>();
        for (Change_Order_Line_Item__c coLineItem : coItem.Change_Order_Line_Item__r) {
            mapOrginalLineItems.put(coLineItem.id, coLineItem);
        }
        system.debug('-----------------co-----------------' + coItem.Change_Order_Line_Item__r.size());
        // check count of change order line items
        system.assertEquals(coItem.Change_Order_Line_Item__r.size(), (coItemCntr.liCOLineItemsNonLabor.size()+coItemCntr.liCOLineItemsLabor.size()));
        
        // check adding new change order line item
        //coItemCntr.addChangeOrderLineItem();
        coItemCntr.addChangeOrderLineItemLabor();
        coItemCntr.addChangeOrderLineItemNonLabor();
        system.assertEquals(coItem.Change_Order_Line_Item__r.size()+2, (coItemCntr.liCOLineItemsNonLabor.size()+coItemCntr.liCOLineItemsLabor.size()));
        Change_Order_Line_Item__c coNewLineItem;
        
        for (Change_Order_Line_Item__c coLineItem : coItemCntr.liCOLineItemsLabor){
            // check if new records
            if (coLineItem.id == null){
                coNewLineItem = coLineItem;
            }
            else{
                // check orginal value
                system.assertEquals(mapOrginalLineItems.get(coLineItem.id).Current_Contract_Hours__c, coLineItem.Current_Contract_Hours__c);
                system.assertEquals(mapOrginalLineItems.get(coLineItem.id).Current_Total_Cost__c, coLineItem.Current_Total_Cost__c);
                system.assertEquals(mapOrginalLineItems.get(coLineItem.id).Amendment_BUF_Code_Bill_Rate__c, coLineItem.Amendment_BUF_Code_Bill_Rate__c);
            }
        }
        
        coNewLineItem.BUF_Code__c = tu.bufCodes.get(2).id;
        // update change order line items
        for (Change_Order_Line_Item__c coLineItem : coItemCntr.liCOLineItemsLabor){
            coLineItem.Current_Contract_Hours__c = 20;
            coLineItem.Current_Total_Cost__c = 1000;
            coLineItem.Amendment_BUF_Code_Bill_Rate__c = 7;
        }
        coItemCntr.saveChangeOrderLineItemslabor();
        coItemCntr.saveChangeOrderLineItemsNonlabor();
        mapOrginalLineItems = 
        	new Map<Id, Change_Order_Line_Item__c>([select Id, Type__c, Amendment_BUF_Code_Bill_Rate__c, Amendment_CO_Hours__c, Amendment_CO_Hours_unit__c, 
        		BUF_Code__c, Change_Order_Item__c, Current_Contract_Hours__c, Current_Contract_Value__c, Current_Hours_Unit__c, Current_Total_Cost__c, 
                Current_Unit_Cost__c 
	            from Change_Order_Line_Item__c
	            where Change_Order_Item__c = :coItem.id]);
        
        system.assertEquals(coItem.Change_Order_Line_Item__r.size() + 1, mapOrginalLineItems.size());
        for (Change_Order_Line_Item__c coLineItem : mapOrginalLineItems.values()){
        	if(coLineItem.BUF_Code__c == tu.bufCodes.get(0).Id) {
	            system.assertEquals(20, coLineItem.Current_Contract_Hours__c);
	            system.assertEquals(1000, coLineItem.Current_Total_Cost__c);
	            system.assertEquals(7, coLineItem.Amendment_BUF_Code_Bill_Rate__c);
        	}
        }
        
        test.stopTest();
    }
    
    static testMethod void shouldDisplayOrginalCrctValues() {
        PRA_TestUtils tu = new PRA_TestUtils();
        
        tu.initAll();
        client_project__c c=[select id,Currency_exchange_lock_date__c from client_project__c where id=:tu.clientProject.Id ];
        c.Standard_rate_year__c=date.today();
        update c;
        Change_Order__c co = tu.createChangeOrder(tu.clientProject.Id);
        Change_Order_Item__c coItem = [select Id, Client_Task__c, (select Id, Type__c from Change_Order_Line_Item__r),Change_Order__r.Client_Project__r.Currency_exchange_lock_date__c,
                                            Change_Order__r.Client_Project__r.Standard_rate_year__c,Change_Order__r.Client_Project__r.Estimated_End_Date__c,Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Change_Order__c, 
                                            Client_Unit_Number__c, Combo_Code__c, Description__c, End_Date__c, Forecast_Curve__c, 
                                            Number_Of_Contracted_Units__c, Project_Region__c, Revised_Number_of_Units__c, Revised_Total_Cost__c, 
                                            Revised_Unit_Cost__c, Start_Date__c, Task_Group__c, Total_Cost__c, Type__c, Unit_Completed_Event_Lookup__c, Unit_Cost__c, 
                                            Unit_of_Measurement__c, Worked_Hours__c, Worked_Hours_Done__c, Worked_Units__c, Worked_Units_Done__c 
                                        from Change_Order_Item__c 
                                        where Change_Order__c = :co.id
                                        limit 1];
        
        test.startTest();
        PRA_ChangeOrderModifyClUnitController coItemCntr = new PRA_ChangeOrderModifyClUnitController(new Apexpages.Standardcontroller(coItem));
        
        PRA_ChangeOrderModifyClUnitController.TaskWrapper taskWrapper = coItemCntr.getOrginalTaskValues();
        Client_Task__c expecterTask = [select Id, Description__c, Client_Unit_Number__c, Project_Region__r.Name, Total_Units__c, Total_Baseline_Units__c, 
        								Unit_of_Measurement__c, Total_Worked_Hours__c, Total_Worked_Units__c, Unit_completed_event_id__c, Combo_Code__c, 
        								Forecast_Curve__c , Task_Group__r.Name, Contract_Value__c, Total_Contract_Units__c
				                        from Client_Task__c 
				                        where id = :coItem.Client_Task__c];
        
        PRA_ChangeOrderModifyClUnitController.TaskWrapper expectedTaskWrapper = 
        new PRA_ChangeOrderModifyClUnitController.TaskWrapper(expecterTask.Id, 
            expecterTask.Client_Unit_Number__c, expecterTask.Description__c, expecterTask.Project_Region__r.Name,
            expecterTask.Unit_of_Measurement__c, expecterTask.Total_Contract_Units__c, 
            expecterTask.Total_Worked_Units__c, expecterTask.Total_Worked_Hours__c);
        
        expectedTaskWrapper.comboCode = expecterTask.Combo_Code__c;
        expectedTaskWrapper.forecastCurve = expecterTask.Forecast_Curve__c;
        expectedTaskWrapper.taksGroup = expecterTask.Task_Group__r.Name;
        expectedTaskWrapper.unitCompletedEvent = expecterTask.Unit_completed_event_id__c;
                
        system.assertEquals(expectedTaskWrapper.clientRegion, taskWrapper.clientRegion);
        system.assertEquals(expectedTaskWrapper.clientUnitNumber, taskWrapper.clientUnitNumber);
        system.assertEquals(expectedTaskWrapper.comboCode, taskWrapper.comboCode);
        system.assertEquals(expectedTaskWrapper.contractedUnits, taskWrapper.contractedUnits);
        system.assertEquals(expectedTaskWrapper.description, taskWrapper.description);
        system.assertEquals(expectedTaskWrapper.forecastCurve, taskWrapper.forecastCurve);
        system.assertEquals(expectedTaskWrapper.taksGroup, taskWrapper.taksGroup);
        system.assertEquals(expectedTaskWrapper.unitCompletedEvent, taskWrapper.unitCompletedEvent);
        system.assertEquals(expectedTaskWrapper.unitDriver, taskWrapper.unitDriver);
        system.assertEquals(expectedTaskWrapper.workdedHours, taskWrapper.workdedHours);
        system.assertEquals(expectedTaskWrapper.workedUnits, taskWrapper.workedUnits);
        
        test.stopTest();
    }
    
    static testMethod void shouldselectregionandtaskgroup() {
        PRA_TestUtils tu = new PRA_TestUtils();
        
        tu.initAll();
        client_project__c c=[select id,Currency_exchange_lock_date__c from client_project__c where id=:tu.clientProject.Id ];
        c.Project_Currency__c='USD';
        c.Currency_exchange_lock_date__c=date.today();
        c.Standard_rate_year__c=date.today();
        c.Estimated_End_Date__c=date.today();      
       
        update c;
        
        BUF_Code__c bc1 = new BUF_Code__c();        
        bc1.name='ABCDE';      
        insert bc1;
        
        Valid_Project_Buf_Code__c vpbf= new Valid_Project_Buf_Code__c();
        vpbf.BUF_Code__c=bc1.id;
        //vpbf.Convert_To_Code__c='USD';
        //datetime CELDT=(date.today());
        //CELDT=CELDT.addHours(CELDT.hour());
        //String  CELDate=CELDT.format('yyyyMMdd');
        //vpbf.Currency_Date_Key__c=CELDate;
        vpbf.Standard_Rate_Year__c=Decimal.valueof(date.today().year());
        vpbf.Cost_Rate_End_Year__c=Decimal.valueof(date.today().year());
        //vpbf.Currency_End_Date_Key__c=CELDate;      
        vpbf.Currency_Code__c='USD';
        insert vpbf;
        system.debug('---------------------------------'+vpbf); 
        
        Change_Order__c co = tu.createChangeOrder(tu.clientProject.Id);
        Change_Order_Item__c coItem = [select Id, Client_Task__c, (select Id, Type__c from Change_Order_Line_Item__r),Change_Order__r.Client_Project__r.Currency_exchange_lock_date__c,
                                            Change_Order__r.Client_Project__r.Standard_rate_year__c,Change_Order__r.Client_Project__r.Estimated_End_Date__c,Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Change_Order__c, 
                                            Client_Unit_Number__c, Combo_Code__c, Description__c, End_Date__c, Forecast_Curve__c, 
                                            Number_Of_Contracted_Units__c, Project_Region__c, Revised_Number_of_Units__c, Revised_Total_Cost__c, 
                                            Revised_Unit_Cost__c, Start_Date__c, Task_Group__c, Total_Cost__c, Type__c, Unit_Completed_Event_Lookup__c, Unit_Cost__c, 
                                            Unit_of_Measurement__c, Worked_Hours__c, Worked_Hours_Done__c, Worked_Units__c, Worked_Units_Done__c 
                                        from Change_Order_Item__c 
                                        where Change_Order__c = :co.id
                                        limit 1];
        
        test.startTest();
        PRA_ChangeOrderModifyClUnitController coItemCntr = new PRA_ChangeOrderModifyClUnitController(new Apexpages.Standardcontroller(coItem));
        
        coItemCntr.selectProject();
        coItemCntr.getProjectRegionList();
        coItemCntr.ProjectRegionListAll();
        coItemCntr.getAllProjectRegionList();
        coItemCntr.getTaskGroupList();
        coItemCntr.updateChangeOrderItem();
       
        coItemCntr.addChangeOrderLineItemNonLabor();
        system.debug('-----------------coItemCntr.liCOLineItemsnonlabor----------------' + coItemCntr.liCOLineItemsnonlabor.size());
        coItemCntr.liCOLineItemsnonlabor[(coItemCntr.liCOLineItemsnonlabor.size()-1)].BUF_Code__c=bc1.id;
        coItemCntr.pageAddMode='Add';
        coItemCntr.saveChangeOrderLineItemsNonlabor();
        
        Change_Order_Item__c coit= new Change_Order_Item__c();
        coit.Change_Order__c=co.id;
        insert coit;
        
        coItem.Client_Unit_Number__c='ghd';
        coItem.Combo_Code__c='g4e';
        
        update coitem;
        coItemCntr.updateChangeOrderItem();
        coItemCntr.CloneChangeOrderItem();
        coItemCntr.SaveCloneChangeOrderItem();
        
        test.stopTest();
    }
    
    static testMethod void shouldTestConstructor() {
        
        PRA_TestUtils tu = new PRA_TestUtils();
        
        tu.initAll();
        client_project__c c=[select id,Currency_exchange_lock_date__c from client_project__c where id=:tu.clientProject.Id ];
        c.Standard_rate_year__c=date.today();
        update c;
        Change_Order__c co = tu.createChangeOrder(tu.clientProject.Id);
        Change_Order_Item__c coItem = [select Id, Client_Task__c, (select Id, Type__c from Change_Order_Line_Item__r),Change_Order__r.Client_Project__r.Currency_exchange_lock_date__c,
                                            Change_Order__r.Client_Project__r.Standard_rate_year__c,Change_Order__r.Client_Project__r.Estimated_End_Date__c,Amendment_CO_Number_of_Units__c, Amendment_Total_Cost_c__c, Amendment_Unit_Cost__c, Change_Order__c, 
                                            Client_Unit_Number__c, Combo_Code__c, Description__c, End_Date__c, Forecast_Curve__c, 
                                            Number_Of_Contracted_Units__c, Project_Region__c, Revised_Number_of_Units__c, Revised_Total_Cost__c, 
                                            Revised_Unit_Cost__c, Start_Date__c, Task_Group__c, Total_Cost__c, Type__c, Unit_Completed_Event_Lookup__c, Unit_Cost__c, 
                                            Unit_of_Measurement__c, Worked_Hours__c, Worked_Hours_Done__c, Worked_Units__c, Worked_Units_Done__c 
                                        from Change_Order_Item__c 
                                        where Change_Order__c = :co.id
                                        limit 1];
        
        
        
        test.startTest();
        
       	PRA_ChangeOrderModifyClUnitController coItemCntr = new PRA_ChangeOrderModifyClUnitController(new Apexpages.standardcontroller(coItem));
        coItemCntr.addChangeOrderLineItemNonLabor();
        //coItemCntr.liCOLineItemsnonlabor[(coItemCntr.liCOLineItemsnonlabor.size()-1)].BUF_Code__c=bc1.id;
        coItemCntr.pageAddMode='Add';
        coItemCntr.saveChangeOrderLineItemsNonlabor();
        
        coItemCntr.liCOLineItemsnonlabor[(coItemCntr.liCOLineItemsnonlabor.size()-1)].Type__c='True';
        coItemCntr.saveChangeOrderLineItemsNonlabor();        
        
        test.stopTest();
        
    }
    
    static testMethod void TestPopUp() {
        test.startTest();
      
       PRA_TestUtils tu = new PRA_TestUtils();
       tu.initAll();
       tu.clientProject.Project_Currency__c='USD';
       tu.clientProject.Currency_exchange_lock_date__c=date.today();
       tu.clientProject.Standard_rate_year__c=date.today();
       tu.clientProject.Estimated_End_Date__c=date.today();      
       update tu.clientProject;
       
       Change_Order__c coObj=tu.createChangeOrder(tu.clientProject.Id);
       Change_Order_Item__c coItemObj=new Change_Order_Item__c();
       coItemObj.Change_Order__c=coObj.Id;       
       coItemObj.Client_Unit_Number__c='123';
       coItemObj.End_Date__c=Date.today().addMonths(3).toStartOfMonth();
       insert coItemObj;
      
       Function_Code__c fc=new Function_Code__c();
       fc.name='DE';
       insert fc;
      
       Function_Code__c fc1=new Function_Code__c();
       fc1.name='FG';
       insert fc1;
      
       Function_Code__c fc2=new Function_Code__c();
       fc2.name='HI';
       insert fc2;
      
       Business_Unit__c bu=new Business_Unit__c();          
       bu.name='ABC';
       insert bu;
      
       BUF_Code__c bc1 = new BUF_Code__c();
       bc1.Function_Code__c=fc.id;
       bc1.Business_Unit__c=bu.id;
       bc1.name='ABCDE';      
       insert bc1;
      
       BUF_Code__c bc2 = new BUF_Code__c();
       bc2.Function_Code__c=fc1.id;
       bc2.Business_Unit__c=bu.id;
       bc2.name='ABCFG';      
       insert bc2;
      
       BUF_Code__c bc3 = new BUF_Code__c();
       bc3.Function_Code__c=fc2.id;
       bc3.Business_Unit__c=bu.id;
       bc3.name='ABCHI';      
       insert bc3;
        
       Change_Order_Line_Item__c coLineItemObj=new Change_Order_Line_Item__c(Change_Order_Item__c=coItemObj.id,BUF_Code__c=bc1.id);
       coLineItemObj.Type__c='New';
       coLineItemObj.Is_Labor_Unit__c=true;
       coLineItemObj.Current_Contract_Hours__c=0; 
        coLineItemObj.Current_Contract_Value__c=0;
        coLineItemObj.Current_Hours_Unit__c=0;
        coLineItemObj.Current_Unit_Cost__c=0;
        coLineItemObj.Current_Total_Cost__c=0;
        coLineItemObj.Amendment_BUF_Code_Bill_Rate__c=0;
        coLineItemObj.Amendment_CO_Hours__c=0;
        coLineItemObj.Amendment_CO_Hours_unit__c=0;
        
         coLineItemObj.Discount_Price__c=0;
        coLineItemObj.Discount_Unit_Price__c=0;
        
        
       insert coLineItemObj;
       
      
       Client_task__c ct=[select id,name from client_task__c where project__c=:tu.clientProject.id limit 1];
      
       Change_Order_Line_Item__c coLineItemObj1=new Change_Order_Line_Item__c(Change_Order_Item__c=coItemObj.id,BUF_Code__c=bc2.id);
       coLineItemObj1.Type__c='Existing';
        coLineItemObj1.Current_Contract_Hours__c=0; 
        coLineItemObj1.Current_Contract_Value__c=0;
        coLineItemObj1.Current_Hours_Unit__c=0;
        coLineItemObj1.Current_Unit_Cost__c=0;
        coLineItemObj1.Current_Total_Cost__c=0;
        coLineItemObj1.Amendment_BUF_Code_Bill_Rate__c=0;
        coLineItemObj1.Amendment_CO_Hours__c=0;
        coLineItemObj1.Amendment_CO_Hours_unit__c=0;
        
         coLineItemObj1.Discount_Price__c=0;
        coLineItemObj1.Discount_Unit_Price__c=0;
       coLineItemObj1.Is_Labor_Unit__c=false;
       insert coLineItemObj1;
      
       Valid_Project_Buf_Code__c vpbf= new Valid_Project_Buf_Code__c();
       vpbf.BUF_Code__c=bc3.id;
       //vpbf.Convert_To_Code__c='USD';
       //datetime CELDT=(date.today());
       //CELDT=CELDT.addHours(CELDT.hour());
       //String  CELDate=CELDT.format('yyyyMMdd');
       //vpbf.Currency_Date_Key__c=CELDate;
       vpbf.Standard_Rate_Year__c=Decimal.valueof(date.today().year());
       vpbf.Cost_Rate_End_Year__c=Decimal.valueof(date.today().year());
       //vpbf.Currency_End_Date_Key__c=CELDate;      
       vpbf.Currency_Code__c='USD';
       insert vpbf;
       system.debug('---------------------------------'+vpbf);
        PRA_ChangeOrderModifyClUnitController coControllerObj = new PRA_ChangeOrderModifyClUnitController(new Apexpages.standardcontroller(coItemobj));
        system.debug('----'+coControllerObj.COIid);
       
        coControllerObj.unit='labor';
        coControllerObj.COIid=coItemobj.id;
        coControllerObj.SearchBUFCodes();
        coControllerObj.getselectedBUFCode();
       coControllerObj.getselectedAvailableBUFCode();
      
       coControllerObj.getBusinessUnitOptions();      
       coControllerObj.getFunctionCodeOptions();
      
       coControllerObj.getAvailableBUFCodeList();
       coControllerObj.getselectedBUFCodeList();
      
       coControllerObj.SelectedBusinessUnit='ABC';      
       coControllerObj.doFilter();
       coControllerObj.SelectedBusinessUnit=null;
       coControllerObj.SelectedFunctionCode='HI';
       coControllerObj.doFilter();
      
       coControllerObj.SelectedBusinessUnit='ABC';
       coControllerObj.SelectedFunctionCode='HI';
       coControllerObj.doFilter();
      
       string[] ab = new List<string>();
       ab.add('ABCHI');
       coControllerObj.setselectedAvailableBUFCode(ab);
       coControllerObj.doadd();
      
       string[] sb = new List<string>();
       sb.add('ABCDE');
       coControllerObj.setselectedBUFCode(sb);
       coControllerObj.doremove();      
       coControllerObj.apply();  
        
       coControllerObj.movebufcode='Move';
        
        string[] ab1 = new List<string>();
        ab1.add('ABCHI');
        coControllerObj.setselectedAvailableBUFCode(ab1);
        coControllerObj.doadd();
        
        
        string[] sb1 = new List<string>();
        sb1.add('ABCFG');
        coControllerObj.setselectedBUFCode(sb1);
        coControllerObj.doremove();      
        
        
        bid_contract_detail__c bcd=new bid_contract_detail__c();
        bcd.Client_Task__c=ct.id;
        bcd.buf_code__c=bc2.id;
        insert bcd;
        
        coItemObj.client_task__c=ct.id;
        update coitemobj;
        
        bid_contract_detail__c bb=[select id, buf_code__c from bid_contract_detail__c where client_task__c=:coitemobj.client_task__c limit 1];
        system.debug('--------------------'+coitemobj+ct+bcd+bb);
        coControllerObj.move();
        coControllerObj.clear();
        test.stopTest(); 
    }
}