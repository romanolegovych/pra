/*********************************************************************************
 Name         :   DC_reInstateImagesUpdate 
 Author       :   Ramya Shree Edara
 Created Date :   Feb 21,2014
 Purpose      :   DC_reInstateImagesUpdate Batch class
*********************************************************************************/
Global class DC_reInstateImagesUpdate implements Database.Batchable < sObject > {

       global Database.QueryLocator start(
            Database.BatchableContext BC) {
            String query ='SELECT Id FROM IPA_Articles__c WHERE Featured_Image__c != null OR Image_1__c != null LIMIT 5000';
            return Database.getQueryLocator(query);
        }

        global void execute(Database.BatchableContext BC,
            List < sObject > scope) {
            List < IPA_Articles__c >updateArticlesList = (List <IPA_Articles__c > ) scope;
            System.debug('updateArticlesList.size() is : ' +updateArticlesList.size());
            update updateArticlesList;
        }

        global void finish(Database.BatchableContext BC) {
            AsyncApexJob a = [select id,NumberOfErrors,JobItemsProcessed,TotalJobItems from AsyncApexJob where id = :BC.getJobId()];
            System.debug('a.id is : ' +a.id);
            System.debug('a.id is : ' +a.NumberOfErrors);
            System.debug('a.id is : ' +a.JobItemsProcessed);
            System.debug('a.id is : ' +a.TotalJobItems);
        }
}