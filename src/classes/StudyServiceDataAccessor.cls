/** Implements the Selector Layer of the object StudyService__c
 * @author	Dimitrios Sgourdos
 * @version	25-Feb-2014
 */
public with sharing class StudyServiceDataAccessor {
	
	// Global Constants
	//-------------------------------------
	// Calculation Helper types
	public static final String	ASSESSMENTS_FOR_ALL_FC_IN_STUDY_RULE = 'Unique assessments per flowchart';
	public static final String	PREDEFINED_CATEGORIES_ASSESSMENTS_IN_STUDY_RULE = 'Total count of time points of included assessments';
	public static final String	TIME_PERIOD_BETWEEN_MILESTONES = 'Time period between the following milestones';
	
	// Express in types for milestones calculation helper
	public static final String	EXPRESS_IN_DAYS   = 'Days';
	public static final String	EXPRESS_IN_WEEKS  = 'Weeks';
	public static final String	EXPRESS_IN_MONTHS = 'Months';
	public static final String	EXPRESS_IN_YEARS  = 'Years';
	
	
	/** Object definition for fields used in application for StudyService
	 * @author	Dimitrios Sgourdos
	 * @version 28-Jan-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('StudyService__c');
	}
	
	
	/** Object definition for fields used in application for StudyService with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 28-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'CalculationHelperJSON__c,';
		result += referenceName + 'explanationJSON__c,';
		result += referenceName + 'NumberOfUnits__c,';
		result += referenceName + 'ProjectService__c,';
		result += referenceName + 'Study__c,';
		result += referenceName + 'Study_ProjectService_Combination__c,';
		result += referenceName + 'Unique__c';
		
		return result;
	}
	
	
	/** Retrieve the list of StudyServices that meets the given criteria.
	 * @author	Dimitrios Sgourdos
	 * @version 27-Jan-2014
	 * @param	whereClause			The criteria that the StudyServices must meet
	 * @param	orderByField		The field that the StudyServices will be ordered by
	 * @return	The list of StudyServices
	 */
	public static List<StudyService__c> getStudyServiceList(String whereClause, String orderByField) {
		// Check parmateres
		String tmpWhereClause  = String.IsBlank(whereClause)?  '' : 'WHERE ' + whereClause;
		String tmpOrderByField = String.IsBlank(orderByField)? '' : 'ORDER BY ' + orderByField;
		
		String query = String.format(
								'SELECT {0} ' +
								'FROM StudyService__c ' +
								'{1} {2}',
								new List<String> {
									getSObjectFieldString(),
									tmpWhereClause,
									tmpOrderByField
								}
							);
		return (List<StudyService__c>) Database.query(query);
	}
	
	
	/** Retrieve the StudyService with the given id.
	 * @author	Dimitrios Sgourdos
	 * @version 28-Jan-2014
	 * @param	studyServiceId		The id of the StudyService
	 * @return	The retrieved StudyService.
	 */
	public static StudyService__c getStudyServiceById(String studyServiceId) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM StudyService__c ' +
								'WHERE Id = {1}',
								new List<String> {
									getSObjectFieldString(),
									'\'' + studyServiceId + '\''
								}
							);
		
		try {
			return (StudyService__c) Database.query(query);
		} catch (Exception e) {
			system.debug('Query error: ' + query);
			return null;
		}
	}
}