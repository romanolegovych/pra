/**
 * @description	The class for the Pending Acknowledgement Schedule
 * @author		Kalaiyarasan Karunanithi
 * @date		Created: 04-Sep-2015
 */
global class NCM_PendingAcknowledgeSchedule implements Schedulable  {
	
	// Cron string for every 1 hour (at the 30 minutes of each hour)
	public static String cronSched = '0 30 0/6 * * ?';
	
	/**
	* @description Constructor of the class
	* @author      Kalaiyarasan Karunanithi, DImitrios Sgourdos
	* @date        Created: 04-Sep-2015, Edited: 30-Sep-2015
	**/
	global static String scheduleMe() {
		return System.schedule(	NCM_API_DataTypes.BATCH_JOB_FOR_REMINDER,
								cronSched,
								new NCM_PendingAcknowledgeSchedule());
	}
	
	/**
	* @description Execute the Pending Acknowledgement Batch
	* @author      Kalaiyarasan Karunanithi  
	* @date        Created: 04-Sep-2015, Edited: 14-Sep-2015
	**/
	global void execute(SchedulableContext sc) {
		Database.executeBatch(new NCM_PendingAcknowledgeBatch());
	}
	
}