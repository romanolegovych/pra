@isTest(SeeAllData=false)
private class PRA_EffortModuleControllerTest {

    static testMethod void shouldSearchProject() {
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        PRA_EffortModuleController emc = new PRA_EffortModuleController();
        
        // when
        Test.startTest();
        List<Client_Project__c> liProj = PRA_EffortModuleController.searchProject(tu.clientProject.Name);
        Test.stopTest();
        
        // then
        system.assertEquals(tu.clientProject.Name, liProj.get(0).Name);
        system.assertEquals(tu.clientProject.Id, liProj.get(0).Id);
    }
    
    static testMethod void shouldSearchTask() {
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        PRA_EffortModuleController emc = new PRA_EffortModuleController();
        
        // when
        Test.startTest();
        emc.selectedProject = tu.clientProject.Id;
        emc.selectedProjectLabel = tu.clientProject.Id;
        emc.selectedTask = tu.clientTasks.get(0).id;
        emc.selectProject();
        emc.searchTasks();
        Test.stopTest();
        
        // then
        //check month label
        List<String> liMonthLabels = (List<String>)Json.deserialize(emc.jsonMonthLabels, List<String>.class);
        for(Integer i = -3; i < 6; i++ ) {
                Date monthInContext = Date.today().addMonths(i).toStartOfMonth();
                String strMonth = Datetime.newInstance(monthInContext.year(), monthInContext.month(), 1).format('MMM yyyy');
                system.assertEquals(strMonth, liMonthLabels.get(i+3));
         }
        String selectedTask = tu.clientTasks.get(0).id;
        // check units
        List<PRA_EffortModuleController.ValueWrapper> unitsWrapperList = (List<PRA_EffortModuleController.ValueWrapper>)Json.deserialize(emc.jsonUnitValues, List<PRA_EffortModuleController.ValueWrapper>.class );
        List<Unit_Effort__c> currentMonthForecastUnits = [Select Id, Client_Task__c, Forecast_Unit__c from Unit_Effort__c where Client_Task__c = :selectedTask
                                       and (Month_Applies_To__c >=:Date.today().toStartOfMonth()  and Month_Applies_To__c <= :tu.clientTask.Original_End_Date__c)
                                       order by Month_Applies_To__c];
        //system.debug('-----' + emc.jsonUnitValues);
        //system.debug('-----size:' + currentMonthForecastUnits.size());
        for (Integer i =0; i<currentMonthForecastUnits.size(); i++){
            system.assertEquals(currentMonthForecastUnits.get(i).Id, unitsWrapperList.get(i+3).id);
            system.assertEquals(currentMonthForecastUnits.get(i).Forecast_Unit__c, unitsWrapperList.get(i+3).value);
        }
        
        // check effort values
        List<PRA_EffortModuleController.ValueWrapper> effortWrapperList = (List<PRA_EffortModuleController.ValueWrapper>)Json.deserialize(emc.jsonEffortValues, List<PRA_EffortModuleController.ValueWrapper>.class );
        List<Unit_Effort__c> currentMonthEfforts = [Select Id, Client_Task__c, Forecast_Effort__c from Unit_Effort__c where Client_Task__c = :selectedTask
                                       and (Month_Applies_To__c >= :Date.today().toStartOfMonth() and Month_Applies_To__c <= :tu.clientTask.Original_End_Date__c)
                                       order by Month_Applies_To__c];
        for (Integer i =0; i<currentMonthEfforts.size(); i++){
            system.assertEquals(currentMonthEfforts.get(i).Id, effortWrapperList.get(i+3).id);
            system.assertEquals(currentMonthEfforts.get(i).Forecast_Effort__c, effortWrapperList.get(i+3).value);
        }
        
        // table wrapper - liWrappers
        system.assertEquals(tu.clientTasks[0].Description__c, emc.liWrappers[0].taskName);
        system.assertEquals(tu.clientTasks[0].Unit_of_Measurement__c, emc.liWrappers[0].unitOfMeasurement);
        Client_Task__c task = [select Id, Total_Units__c from Client_Task__c where Id = :tu.clientTasks.get(0).Id];
        system.assertEquals(task.Total_Units__c, emc.liWrappers[0].ETC);
    }
    static testMethod void testSearch_POSITIVE(){
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        PRA_EffortModuleController emc = new PRA_EffortModuleController();
        
        emc.selectedProject = tu.clientProject.id;
        emc.selectProject();
        emc.searchTasks();
        System.assertNotEquals(emc.liWrappers, null);
        System.assertNotEquals(emc.liWrappers.size(), 0);
        System.assertEquals(3, emc.liWrappers.size());
        
        emc.selectedRegion = new List<String>{tu.projectRegions.get('NA').id};
        emc.selectedOperationalArea = new List<String>{tu.operationalArea.id};
        emc.selectedGroup = new List<String>{tu.taskGroup.Id};
        emc.selectedTasks = new List<String>{tu.clientTask.id};
        emc.confirmedOnly = true;
        emc.searchTasks();
        System.assertNotEquals(emc.liWrappers, null);
        System.assertNotEquals(emc.liWrappers.size(), 0);
        
        // Testing unconfirmed
        emc.unconfirmedOnly = true;
        emc.searchTasksVF();
        System.assertEquals(false, emc.isFirstTime);
    }
    // Test save User prefs
    static testMethod void testsavePrefs(){
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        PRA_EffortModuleController emc = new PRA_EffortModuleController();

        emc.selectedProject = tu.clientProject.id;
        emc.selectProject();
        emc.selectedRegion = new List<String>{tu.projectRegions.get('NA').id};
        PRA_Utils.savePrefs(emc.selectedProject, emc.selectedRegion, emc.selectedOperationalArea, emc.selectedTasks, 
                         emc.selectedGroup, emc.unconfirmedOnly, emc.confirmedOnly);
        String regionId =  [Select region_id__c from User_Preference__c where user_id__c = :UserInfo.getUserId() ].region_id__c.split(',').get(0);
        System.assertEquals(regionId, tu.projectRegions.get('NA').id);
        
        emc = new PRA_EffortModuleController();
        System.assertEquals(emc.selectedRegion[0], tu.projectRegions.get('NA').id);
    }
    static testMethod void shouldGetCorrectEffortTypes() {
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        PRA_EffortModuleController emc = new PRA_EffortModuleController();
        
        // when
        Test.startTest();
        emc.selectedProject = tu.clientProject.Id;
        emc.selectedProjectLabel = tu.clientProject.Id;
        emc.selectedTask = tu.clientTasks.get(0).Id;
        emc.selectProject();
        emc.searchTasks();
        Test.stopTest();
        
        // then
        List <Selectoption> soEffortType = emc.getEffortTypes();
        List<PRA_EffortModuleController.EffortTypeWrapper> wrapedEffortTypes = emc.getEffortTypeList();
        // expected
        List<Decimal> effortTypes = getEffortTypeList(tu.clientTasks.get(0).Id);
        
        System.assertEquals('Select Effort', soEffortType.get(0).getLabel());
        System.assertEquals(PRA_EffortModuleController.EFFORT_TYPE_CONTRACTED, soEffortType.get(1).getLabel());
        System.assertEquals(PRA_EffortModuleController.EFFORT_TYPE_WORKED_TO_DATE, soEffortType.get(2).getLabel());
        System.assertEquals(PRA_EffortModuleController.EFFORT_TYPE_LAST_APPROVED, soEffortType.get(3).getLabel());
        System.assertEquals(PRA_EffortModuleController.EFFORT_TYPE_ETC_LAST_APPROVED, soEffortType.get(4).getLabel());
        System.assertEquals(PRA_EffortModuleController.EFFORT_TYPE_PROPOSED, soEffortType.get(5).getLabel());

        System.assertEquals(effortTypes.get(0), Decimal.valueOf(soEffortType.get(1).getValue()));
        //System.assertEquals(effortTypes.get(1), Decimal.valueOf(soEffortType.get(2).getValue()));
        System.assertEquals(effortTypes.get(2), Decimal.valueOf(soEffortType.get(3).getValue()));
        
        System.assertEquals(effortTypes.get(0), wrapedEffortTypes.get(0).contracted);
        //System.assertEquals(effortTypes.get(1), wrapedEffortTypes.get(0).workedToDate);
        System.assertEquals(effortTypes.get(2), wrapedEffortTypes.get(0).lastApproved);
        
        
    }
    
    static testMethod void shouldSaveUpdatedEffort() {
         // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        PRA_EffortModuleController emc = new PRA_EffortModuleController();
        String selectedTask = tu.clientTasks.get(0).Id;
        List<Unit_Effort__c> currentMonthEfforts = [Select Id, Client_Task__c, Forecast_Effort__c from Unit_Effort__c where Client_Task__c = :selectedTask
                                       and (Month_Applies_To__c >= :Date.today().toStartOfMonth() and Month_Applies_To__c <= :tu.clientTask.Original_End_Date__c)
                                       order by Month_Applies_To__c];
        
        // when
        Test.startTest();
        //List<ValueWrapper>
        //List<Client_Project__c> liProj = PRA_EffortModuleController.searchProject(tu.clientProject.Name);
        List<PRA_EffortModuleController.ValueWrapper> effWrapedList = new List<PRA_EffortModuleController.ValueWrapper>();
        for(Unit_Effort__c ef : currentMonthEfforts){
            effWrapedList.add(new PRA_EffortModuleController.ValueWrapper(ef.Id, ef.Forecast_Effort__c + 2));
        }
        String jsonEffortString = Json.serialize(effWrapedList);
        PRA_EffortModuleController.saveEfforts(jsonEffortString);
        Test.stopTest();
        
        // then
        Map<Id, Unit_Effort__c> newMonthEfforts = new Map<Id, Unit_Effort__c>([Select Id, Client_Task__c, Forecast_Effort__c from Unit_Effort__c where Client_Task__c = :selectedTask
                                       and (Month_Applies_To__c >= :Date.today().toStartOfMonth() and Month_Applies_To__c <= :tu.clientTask.Original_End_Date__c)
                                       order by Month_Applies_To__c]);
        for(Unit_Effort__c ef : currentMonthEfforts){
            system.assertEquals(ef.Forecast_Effort__c + 2, newMonthEfforts.get(ef.Id).Forecast_Effort__c);
        }
    }
    
    private static List<Decimal> getEffortTypeList(String selectedTask) {
        //Contracted
        Decimal etContracted = 0;
        Client_Task__c task = [select Id, Average_Contract_Effort__c, Average_Last_Approved_Effort__c, Average_Worked_Effort__c
                                                from Client_Task__c where Id = :selectedTask];
        if(task.Average_Contract_Effort__c != 0) {
            etContracted = task.Average_Contract_Effort__c;
        }
        
        Decimal etWorked = 0; // there no wokred units
        
        // Last Approved
        Decimal etLastApproved = 0;
        if(task.Average_Last_Approved_Effort__c != 0) {
            etLastApproved = task.Average_Last_Approved_Effort__c;
        }
        
        return new List<Decimal>{etContracted, etWorked, etLastApproved};
    }
}