@isTest
public with sharing class BDT_ProjectPassThroughControllerTest {
	public static Client_Project__c firstProject 	{get;set;}
	public static List<Study__c> stydiesList 		{get;set;}
	public static List<Business_Unit__c> myBUList  	{get;set;}
	public static List<Site__c> mySiteList  		{get;set;}
	public static ServiceCategory__c PTCategory		{get;set;}
	public static PassThroughService__c PTService	{get;set;}
	
	static void init(){
		firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;
		
		myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert myBUList;
		
		
		stydiesList = BDT_TestDataUtils.buildStudies(firstProject);
		stydiesList[0].Business_Unit__c = myBUList[0].Id; 
		stydiesList[1].Business_Unit__c = myBUList[1].Id; 
		stydiesList[2].Business_Unit__c = myBUList[2].Id;
		stydiesList[3].Business_Unit__c = myBUList[3].Id; 
		stydiesList[4].Business_Unit__c = myBUList[4].Id;
		insert stydiesList;
		string selectedStudies ='';
		for(Study__c st: stydiesList){
			selectedStudies += st.Id+':';
		}
		
						
		BDT_Utils.setPreference('SelectedProject', firstProject.id);
		BDT_Utils.setPreference('SelectedStudies',selectedStudies);
	}

	static testmethod void testPassThrough(){
		//first run without any data,  to catch the exeptions being tested
		init();
						
		PTCategory = new ServiceCategory__c(Code__c = '001.001.001', Name = 'Test Category');
		insert PTCategory;
		
		PTService = new PassThroughService__c(ServiceCategory__c = PTCategory.Id, Name = 'Test Pass Through Service', DisplaySequence__c=1 );
		insert PTService;
		
		BDT_ProjectPassThroughController b = new BDT_ProjectPassThroughController();
		b.buildProjectPassthroughWrapper();
				
		b.save();
		b.ProjectPTServices[0].studyPTS[0].selected=true;
		b.ProjectPTServices[0].studyPTS[1].selected=true;
		b.save();
		b.ProjectPTServices[0].studyPTS[0].selected=false;
		b.save();
		
		//deleting all assignments for PTS [0]
		b.ProjectPTServices[0].studyPTS[1].selected=false;
		b.save();
		
	}

}