public with sharing class PAWS_FlowStepActionTrigger extends STSWR1.AbstractTrigger 
{
	//IMPORTANT: this method is called from PAWS_FlowStepTrigger.cls where not all fields are selected for Action. Consider this.
	public override void afterDelete(List<SObject> records)
	{
		updateGanttProperties((List<STSWR1__Flow_Step_Action__c>)records);
	}
	
	private void updateGanttProperties(List<STSWR1__Flow_Step_Action__c> records)
	{
		Set<String> flowIds = new Set<String>();
		
		for(STSWR1__Flow_Step_Action__c record : records)
    	{
    		try
			{
	    		Map<String, Object> config = (Map<String, Object>)JSON.deserializeUntyped(record.STSWR1__Config__c);
				if(config != null && !String.isEmpty((String)config.get('flowId')))
				{
					flowIds.add((String)config.get('flowId'));
				}
			}catch(Exception ex){}
    	}
    	
		Set<String> subFlowsIds = new Set<String>(flowIds);
		Set<String> temp = new Set<String>();
		
		PAWS_Utilities.checkLimit('Queries', 15);
		while(true)
		{
			temp.clear();
			for(STSWR1__Flow_Step_Action__c action : [select STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Flow_Step__r.STSWR1__Flow__c in :subFlowsIds and STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active'])
			{
				try
				{
					Map<String, Object> config = (Map<String, Object>)JSON.deserializeUntyped(action.STSWR1__Config__c);
					if(config != null && !String.isEmpty((String)config.get('flowId')) && !flowIds.contains((String)config.get('flowId'))) 
					{
						temp.add((String)config.get('flowId'));
					}
				}catch(Exception ex){}
			}
				
			if(temp.size() == 0) break;
				
			flowIds.addAll(temp);
			subFlowsIds = new Set<String>(temp);
		}
		
		PAWS_Utilities.checkLimit('Queries', 1);
		List<STSWR1__Gantt_Step_Property__c> properties = [select STSWR1__Parent_Flow_Id__c, Project__c from STSWR1__Gantt_Step_Property__c where STSWR1__Flow__c in :flowIds];
		for(STSWR1__Gantt_Step_Property__c property : properties)
		{
			property.STSWR1__Parent_Flow_Id__c = null;
			property.Project__c = null;
		}
		
		PAWS_Utilities.checkLimit('DMLStatements', 1);
		STSWR1.AbstractTrigger.Disabled = true;
		update properties;
		STSWR1.AbstractTrigger.Disabled = false;
	}
}