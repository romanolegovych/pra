/**
 * @description	The API that implements email functionalities
 * @author		Dimitrios Sgourdos
 * @date		Created: 01-Oct-2015, Edited: 02-Oct-2015
 */
public class NCM_Email_API {
	
	/**
	 * @description	Find the id of the org wide email address that corresponds to the given address.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 02-Oct-2015
	 * @param		emailAddress      The email address that its id will be found
	 * @return		The id of the retrieved org wide email address.
	*/
	public static Id getOrgWideEmailAddressIdFromAddressName(String emailAddress) {
		return NCM_SrvLayer_Email.getOrgWideEmailAddressIdFromAddressName(emailAddress);
	}
	
	
	/**
	 * @description	Create an email instance with the given info.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Oct-2015, Edited: 02-Oct-2015
	 * @param		receiptToId             The id of the user that will be the recipient of the email
	 * @param		orgWideFromAddressId    The from address of the mail (if different than NULL)
	 * @param		emailSubject            The email subject
	 * @param		emailBodyHTML           The email body
	 * @param		attachFile              The attachment of the mail (if different than NULL)
	 * @return		The created e-mail instance.
	*/
	public static Messaging.SingleEmailMessage createEmailInstance(	Id receiptToId,
																	Id orgWideFromAddressId,
																	String emailSubject,
																	String emailBodyHTML,
																	Messaging.EmailFileAttachment attachFile) {
		return NCM_SrvLayer_Email.createEmailInstance(	receiptToId,
														orgWideFromAddressId,
														emailSubject,
														emailBodyHTML,
														attachFile);
	}
	
	
	/**
	 * @description	Send the given email instances
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Oct-2015
	 * @param		notificationEmailsList    The given email instances to be sent
	*/
	public static void sendEmailInstances(List<Messaging.SingleEmailMessage> notificationEmailsList) {
		NCM_SrvLayer_Email.sendEmailInstances(notificationEmailsList);
	}
}