@isTest
private class BDT_FinancialDocOverviewContrTest {

	static testMethod void myUnitTest() {
		FinancialDocument__c  	 financialDocument;
		// Create test data
		BDT_CreateTestData.createall();
		Client_Project__c project = [select id from Client_Project__c limit 1];
		BDT_Utils.setPreference('SelectedProject', String.valueOf(project.id));
		// Create the controller
		BDT_FinancialDocOverviewController p = new BDT_FinancialDocOverviewController();
		p.showInvalidFlag = true;
		p.readFinancialDocuments();
		p.showInvalidFlag = false;
		p.readFinancialDocuments();
		system.assertEquals(p.financialDocList.size(), 0);
		// check adding a financial document
		p.addDocument();
		p.cancel();
		p.addDocument();
		p.save(); // name is missing
		system.assertEquals(p.financialDocList.size(), 0);
		p.financialDocument.Name = 'financial document 1';
		p.financialDocument.DocumentType__c = 'Change Order';
		p.save(); // link to proposal is missing
		system.assertEquals(p.financialDocList.size(), 0);
		p.financialDocument.DocumentType__c = 'Proposal';
		p.save();
		system.assertEquals(p.financialDocList.size(), 1);
		// Check the edit
		p.financialDocumentId = p.financialDocList[0].Id;
		p.editDocument();
		p.financialDocument.Name = 'proposal 1';
		p.save();
		system.assertEquals(p.financialDocList.size(), 1);
		system.assertEquals(p.financialDocList[0].Name, 'proposal 1');
		// Create change orders
		p.financialDocList[0].ApprovalStatus__c = 'Accepted';
		p.financialDocList[0].DocumentStatus__c = 'Approved';
		update p.financialDocList[0];
		String tmpStr = p.financialDocList[0].Id;
		BDT_DC_FinancialDocument.createAcceptedProposalsSelectionList(project.id);
		p.addDocument();
		p.financialDocument.Name = 'change order 1';
		p.financialDocument.DocumentType__c = 'Change Order';
		p.financialDocument.FinancialDocumentParent__c = tmpStr;
		p.save(); 
		p.addDocument();
		p.financialDocument.Name = 'change order 2';
		p.financialDocument.DocumentType__c = 'Change Order';
		p.financialDocument.FinancialDocumentParent__c = tmpStr;
		p.save(); 
		system.assertEquals(p.financialDocList.size(), 3);
		// Go to workflow
		p.goToWorkFlow();
		// Try to delete proposal 1
		p.financialDocList[0].ApprovalStatus__c = 'Rejected';
		p.financialDocList[0].DocumentStatus__c = 'Rejected';
		update p.financialDocList[0];
		p.financialDocumentId = p.financialDocList[0].Id;
		p.editDocument();
		p.deleteSoft();
		system.assertEquals(p.financialDocList.size(), 0);
    }
}