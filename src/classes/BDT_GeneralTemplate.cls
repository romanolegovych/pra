public with sharing class BDT_GeneralTemplate {
	
	public String rightBarPreference {get; set;}
	
	public BDT_GeneralTemplate(){
	   	rightBarPreference = BDT_Utils.getPreference('RightBarDisplay');
    	if (rightBarPreference==null||rightBarPreference==''){rightBarPreference='false';}
	}
		
	public void setRightBarPreference(String value){
		BDT_Utils.setPreference('RightBarShown', value);
	}
	
	@RemoteAction
	public static String setRightNavigationPreferences(String preferenceValue) {
		BDT_Utils.setPreference('RightBarDisplay', preferenceValue);
		return preferenceValue;
	}

}