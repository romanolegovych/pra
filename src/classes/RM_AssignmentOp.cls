public with sharing class RM_AssignmentOp { 
    public RM_AssignmentOp(){ 
         
    }
  
    
    
  
   
    /* public static Boolean FTEAllocation(string aID, decimal newFTE, date effDate, date endDate){
        List<WFM_Employee_Assignment__c> assignments = new list<WFM_Employee_Assignment__c>();
        if (endDate == null) // all assignment after effDate
            assignments = RM_DataAccessor.GetAssignementByAllocationYearMonth(aID, RM_Tools.GetYearMonthFromDate(effDate));
        else
            assignments = RM_DataAccessor.GetAssignementByAllocationYearMonth(aID, RM_Tools.GetYearMonthFromDate(effDate),RM_Tools.GetYearMonthFromDate(endDate));
        system.debug('---------assignments---------'+assignments);
        try{
            //need to insert holes
            list<string> yMonths = RM_Tools.GetMonthsListFromDate(effDate, endDate);
            WFM_employee_Allocations__c eall=RM_DataAccessor.GetAllocationByRecID(aID);
            Employee_Details__c empl = RM_DataAccessor.GetEmployeeDetailByEmployee(eall.employee_id__c);    
            List<WFM_Employee_Availability__c> avas = RM_DataAccessor.InsertAvailability(empl,  yMonths); 
         
            Map<string, string> mapAva= new Map<string, string>();             
            for (WFM_Employee_Availability__c av:avas){                  
                mapAva.put(av.year_month__c, av.id);                 
            }
            
            map<string, ID> mapAss= new map<string, ID>();
            for (WFM_Employee_Assignment__c a:assignments ){
                mapAss.put(a.allocation_key__c + a.year_month__c, a.id);
            }
            system.debug('--------- mapAss---------'+mapAss);
            List<WFM_Employee_Assignment__c> newAssign = new List<WFM_Employee_Assignment__c>();
            
            for (string aMonth: yMonths){
                if (mapAss.get(eall.id+aMonth) == null){
                    system.debug('--------- mapAss---------'+eall.id+aMonth);
                    WFM_Employee_Assignment__c aAssign = new WFM_Employee_Assignment__c(Year_Month__c=aMonth, Assignment_Unique_Key__c=eall.id+mapAva.get(aMonth), Availability_FK__c= mapAva.get(aMonth),Allocation_Key__c=eall.id, 
                                Assigned_value__c=newFTE,  type__c = eall.type__c,  Status__c=eall.status__c, EMPLOYEE_ID__c =eall.employee_id__c);
                             
                    newAssign.add(aAssign);
                    system.debug('--------- aAssign---------'+aAssign);
                }                       
            }
            
            system.debug('---------Insert Assignment---------'+newAssign);
            insert newAssign;
            for (WFM_Employee_Assignment__c a:assignments ){
                a.Assigned_value__c = newFTE;
                
            }    
            system.debug('---------updateAssignments---------'+assignments);
            update assignments;
            UpdateAllocationStartEndDates(eall, effDate, endDate);
            return true;
        }
        catch (DMLException e) {
            return false;
        }
    }*/ 
   
    
    /*private static void UpdateAllocationStartEndDates(WFM_employee_Allocations__c alloc, date effDate, date endDate){
        
        if (effDate < alloc.Allocation_Start_Date__c)
            alloc.Allocation_Start_Date__c = effDate;
        
        /*if (alloc.First_Assignment_Date__c < effDate){
            if (alloc.First_Assignment_Date__c.monthsBetween(effDate)== 0 )         
                alloc.Allocation_Start_Date__c = effDate;
            
                
        }*/
   
    /*    if (endDate > alloc.Allocation_End_Date__c)
            alloc.Allocation_End_Date__c = endDate;
        
        try{
            update alloc;
        }
        catch (DMLException e) {
            
        }
        
    }*/
   
    
    public class ProjectAllocation{
        public string lastName{get; private set;}
        public string firstName{get; private set;}
        public string emplName{get; private set;}
        public string role{get; private set;}
        public string status{get; private set;}
        public string startDate{get; private set;}
        public string endDate{get; private set;}
    
        public string employeeRID{get; private set;} 
        public string country{get; private set;}
        
        public list<string> histList{get; private set;}
        public list<string> assignList{get; private set;}
        public integer iStart{get; private set;}
        public integer iEnd{get; private set;}
        public boolean bAssign{get; private set;}
        
        
        public projectAllocation(Employee_Details__c empl, Integer iStartMonth, Integer iEndMonth){
            lastName = empl.Last_Name__c;
            firstName = empl.First_Name__c;
            emplName = lastName + ', ' + firstName;
            country = empl.country_name__r.name;
            employeeRID = empl.id;
            bAssign = false;
            role = empl.job_class_desc__c;
            histList = new string[iEndMonth - iStartMonth + 1];
            assignList = new string[iEndMonth - iStartMonth + 1];
            iStart = iStartMonth;
            iEnd = iEndMonth;
            for (integer i = 0; i < iEndMonth - iStartMonth + 1; i++){
                if (i < -iStartMonth )
                    histList[i] = '';
                assignList[i] = '';
                
            }
        }
        
        
    }
    public static list<ProjectAllocation> GetProjectAllocationList(string projectRID, Integer iStartMonth, Integer iEndMonth, boolean bHr){
        
        list<ProjectAllocation> projAllList = new list<ProjectAllocation>();
        Set<ID> employeeIDs = RM_EmployeeService.GetEmployeeSetByProjectYearMonth(projectRID, iStartMonth, iEndMonth);
        system.debug('-----employeeIDs------'+employeeIDs); 
        list<Employee_Details__c> empList = RM_EmployeeService.GetEmployeeListBySetEmployeeRID(employeeIDs);
        system.debug('-----empList------'+empList); 
        map<ID, AggregateResult> mapHist = getMapWorkHistory(employeeIDs, projectRID);
        system.debug('-----mapHist------'+mapHist); 
        map<string, string> mapWorkFTE = getMapWorkFTE(employeeIDs, projectRID, iStartMonth, 0, bHr);
        system.debug('-----mapWorkFTE------'+mapWorkFTE);
        map<string, AggregateResult> mapAssignedFTE = getMapAssignFTE(employeeIDs,  projectRID, iStartMonth, iEndMonth);
        system.debug('-----mapAssignedFTE------'+mapAssignedFTE); 
        for (Employee_Details__c e: empList){       
                        
            ProjectAllocation pA;               
            pA = new ProjectAllocation(e, iStartMonth, iEndMonth);                      
            pA = getAssignmentinPA(pA, mapAssignedFTE, 'Confirmed', bHr);
            if (pA.bAssign)   
                pA = getHistoryPA(pA,mapWorkFTE);
            else{
                pA = getAssignmentinPA(pA, mapAssignedFTE, 'Proposed', bHr);
                if (pA.bAssign)   
                    pA = getHistoryPA(pA,mapWorkFTE);
                else{
                    AggregateResult ar = mapHist.get(pA.employeeRID);
                    
                    if (ar!= null){
                        system.debug('-----ar------'+RM_Tools.GetLastDayDatefromYearMonth((string)ar.get('endMonth'))); 
                        pA.startDate = RM_Tools.GetStringfromDate(RM_Tools.GetDatefromYearMonth((string)ar.get('startMonth')), 'mm/dd/yyyy');                   
                        pA.endDate = RM_Tools.GetStringfromDate(RM_Tools.GetLastDayDatefromYearMonth((string)ar.get('endMonth')), 'mm/dd/yyyy');
                        system.debug('-----pA------'+pA); 
                    }
                    pA = getHistoryPA(pA,mapWorkFTE);
                }
                    
            }
                
            projAllList.add(pA);            
        }   
        system.debug('-----projAllList------'+projAllList); 
        return projAllList;
    }
    private static map<ID, AggregateResult> getMapWorkHistory(Set<ID> employeeIDs, string projectRID){
        map<ID, AggregateResult> mapHist = new map<ID, AggregateResult>();
        
        for (AggregateResult ar :RM_EmployeeService.GetEmployeeWorkHistrySummarybyEmployeesProject(employeeIDs, projectRID)){
            mapHist.put((ID)ar.get('Employee_ID__c'), ar);
        }
        return mapHist;
    }
    private static map<string, string> getMapWorkFTE(set<ID> employeeIDs, string projectRID, integer startMonth, integer endMonth, boolean bHr){
         map<string, string> mapWorkFTE = new map<string, string>();
         for (AggregateResult ar :RM_EmployeeService.GetSumEmployeeWorkHistByEmployeesProjectYearMonth(employeeIDs,  projectRID,  startMonth,  endMonth)){
         	if (bHr)
         		mapWorkFTE.put((string)ar.get('Employee_ID__c') + (string)ar.get('year_month__c'), string.valueOf(((decimal)ar.get('totalHr')).divide(1,1)));
         	else
            	mapWorkFTE.put((string)ar.get('Employee_ID__c') + (string)ar.get('year_month__c'), string.valueOf(((decimal)ar.get('totalFTE')).divide(1,2)));
         }
         return mapWorkFTE;
    }
    private static map<string, AggregateResult> getMapAssignFTE(set<ID> employeeIDs, string projectRID, integer startMonth, integer endMonth){
        map<string, AggregateResult> mapAssign = new map<string, AggregateResult>();
        for (AggregateResult ar :RM_AssignmentsService.GetEmployeeAllocationSummaryByEmployeesProjectYearMonth(employeeIDs,  projectRID,  startMonth,  endMonth))
            
            	mapAssign.put((string)ar.get('Employee_ID__c') + (string)ar.get('year_month__c') + (string)ar.get('status__c'), ar);
           
        return mapAssign;
    }
    
    private static ProjectAllocation getAssignmentinPA(ProjectAllocation p, map<string, AggregateResult> mapAssignedFTE, string status, boolean bHr){
        
        integer iIndex = 0;
        for (integer i = p.iStart; i<=p.iEnd; i++){
            string mapKey = p.employeeRID + RM_Tools.GetYearMonth(i)+status;
            system.debug('-----mapKey------'+mapKey); 
            AggregateResult arAssigned =  mapAssignedFTE.get(mapKey);   
            system.debug('-----arAssigned------'+arAssigned); 
            if (arAssigned != null){
                p.bAssign = true;
                decimal assignFTE = 0.0;
                if (bHr){
                	if (arAssigned.get('assignHr') != null )                   
                    	assignFTE = (decimal)arAssigned.get('assignHr');
                    p.assignList[iIndex] = string.valueOf(assignFTE.divide(1, 1));
                }
                else{
                	if (arAssigned.get('assignFTE') != null )                   
                    	assignFTE = (decimal)arAssigned.get('assignFTE');
                    p.assignList[iIndex] = string.valueOf(assignFTE.divide(1, 2));
                }
                
                p.startDate = RM_Tools.GetStringfromDate((Date)arAssigned.get('startDate'), 'mm/dd/yyyy');
            
                
                p.endDate = RM_AssignmentsService.getAllocationEndDate((Date)arAssigned.get('endDate'), (Date)arAssigned.get('lastAssignDate'));
                p.status = (string)arAssigned.get('Status__c');
            
            }
            iIndex++;
                    
        }   
        return p;
    }
    private static  ProjectAllocation getHistoryPA(ProjectAllocation p, map<string, string> mapWorkFTE ){
        integer iIndex = 0;
        for (integer i = p.iStart; i<0; i++){
            string mapKey = p.employeeRID + RM_Tools.GetYearMonth(i);
            string histFTE =  mapWorkFTE.get(mapKey);   
            if (histFTE != null){
                system.debug('-----iIndex------'+iIndex + histFTE); 
                p.histList[iIndex] = histFTE;
                
            }
            iIndex++;           
        
        }
    
    return p;
    }
}