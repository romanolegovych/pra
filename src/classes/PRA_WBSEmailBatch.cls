/**
*   'PRA_WBSEmailBatch' class is to send the Emails to Financial Analysts (FA's) for Work Breakdown Structure(WBS) of a project.
*   @author   Devaram Bhargav
*   @version  04-Dec-2013
*   @since    04-Dec-2013
*/
global class PRA_WBSEmailBatch extends PRA_BatchaQueueable implements Database.Stateful{ 
    
    //get the variables 
    private Set<String> PermissionSetSet; 
    private Set<String> UserList=new Set<string>();
    global Set<String> UserEmailList;   //assigned as global bcoz we want the list in finish method.
    private String query;
    
    //Constructor
    global PRA_WBSEmailBatch (){
        UserEmailList=new Set<string>();  //initiate the global variable.
    }
    
    //Set  the parameters over the variables
    global override  void  setBatchParameters(String parametersJSON){
        
        List<String> params = (List<String>) Json.deserialize(parametersJSON, List<String>.class);        
        
        //get the string of permissionset labels
        String PermissionSetString = (params.get(0)).Substring(1,(params.get(0)).length()-1);
         
        //get the List of permissionset labels from the string by splitting 
        PermissionSetSet= New Set<String>();
        List<string> PermissionSetList= PermissionSetString.Split(', ');
        
        //get the list into set
        PermissionSetSet.addall(PermissionSetList);                
    }
     
    //prepare the query with permission set
    global override Database.QueryLocator start(Database.BatchableContext BC){
        string query='SELECT AssigneeId ,PermissionSet.Id,PermissionSet.label,PermissionSet.Name,PermissionSetID';
               query+=' FROM   PermissionSetAssignment';
               query+=' WHERE  PermissionSet.label in:PermissionSetSet ';
        
        // MLH: added a date filter here on month applies to        
        if (Test.isRunningTest()){
            query += ' Limit 200';
        }
        system.debug('-----query---'+query+'---'+PermissionSetSet);    
        return Database.getQueryLocator(query);
    }
      
    //Execute the query, which gets the list of users assigned to the permission set set. 
    global override void execute(Database.BatchableContext BC, List<Sobject> scope){    
       
        //Collect all the user object record User id's
        for(Sobject sObj : scope){  
            system.debug('-----sobj---'+sObj);        
            UserList.add((String)sObj.get('AssigneeId'));  
        }
        system.debug('-----UserList---'+UserList.size());
         
        //get the Email Set using the user ID's
        UserEmailList = PRA_WBSService.getUserEmailbyUser(UserList);        
        system.debug('-----UserEmailList---'+UserEmailList.size());
    }
     
    //finish method is to get the list of projects that require the WBS Export/Confirm action and send the list to all FA users in P&F.
    global override void finish(Database.BatchableContext BC){
            
        //check for projects only if you have FA users in the system.
        if(UserEmailList.size()>0){
        
            //get the list of projects.
            List<PRA_ProjectWBSCVO.ProjectWBS> ProjectsWBSList=new List<PRA_ProjectWBSCVO.ProjectWBS>();
           
            //Initiate the map to collect the Project and its WBS.        
            Map<String,PRA_ProjectWBSCVO.ProjectWBS> projectWBSMap=new Map<String,PRA_ProjectWBSCVO.ProjectWBS> ();
            
            //get the Map for all the projects with the client tasks statuses.
            projectWBSMap=PRA_WBSService.getMapfromAllProjectsbyClientTaskStatus();        
            system.debug('----------projectWBSMap----------'+projectWBSMap); 
            
            //adding the values from the MAP to a list .
            ProjectsWBSList=projectWBSMap.values(); 
            
            //make a set of project's that need to Export.
            Set<String> ExportProjectSet = new Set<string>();
            
            //make a set of project's that need to Confirm.
            Set<String> ConfirmProjectSet = new Set<string>();
             
            //Loop through the list to get all individual projects list for export and confirm
            for(PRA_ProjectWBSCVO.ProjectWBS pwbs:ProjectsWBSList){
             
                //check if any export is needed
                if(pwbs.ExportWBS == true){
                   ExportProjectSet.add(pwbs.ProjectName);                //Assign project Name to a list
                }
                
                //check if any Confirm is needed
                if(pwbs.ConfirmWBS == true){
                   ConfirmProjectSet.add(pwbs.ProjectName);               //Assign project Name to a list
                }
                system.debug('----------ProjectWBS----------'+PWBS ); 
            }             
            
            //once if any of the collected set are not empty, then you can send emails based on userslist and projects list's
            if(ExportProjectSet.size()>0 || ConfirmProjectSet.size()>0  ){                
                PRA_WBSService.SendEmailbyWBSProjectsList(UserEmailList,ExportProjectSet,ConfirmProjectSet);    
            }    
        }      
    }
}