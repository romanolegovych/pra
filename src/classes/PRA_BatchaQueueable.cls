global  virtual class PRA_BatchaQueueable implements Database.Batchable <sObject>{
    global virtual void  setBatchParameters(String parametersJSON){
        }
    //String query='select id  from account limit 1';
    global String query;
    global virtual Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    global virtual void execute(Database.BatchableContext BC, List<sObject> scope){
        
    }
    global virtual void finish(Database.BatchableContext BC){
    }
    

}