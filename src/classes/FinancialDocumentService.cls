/** Implements the Service Layer of the object FinancialDocument__c
 * @author	Dimitrios Sgourdos
 * @version	23-Jan-2014
 */
public with sharing class FinancialDocumentService {
	
	/** Retrieve the Financial Document with the given id.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 06-Jan-2014
	 * @param	financialDocumentId		The id of the financial document
	 * @return	The retrieved Financial Document.
	 */
	public static FinancialDocument__c getFinancialDocumentById(String financialDocumentId) {
		return FinancialDocumentDataAccessor.getFinancialDocumentById(financialDocumentId);
	}
	
	
	/** Get the financialDocumentGroupWrapper data for the given financial document.
	 * @author	Dimitrios Sgourdos
	 * @version 17-Dec-2013
	 * @param	financialDocumentId		The id of the financial document
	 * @return	The financialDocumentGroupWrapper instance.
	 */
	public static financialDocumentGroupWrapper getFinancialDocumentGrouWrapperData(String financialDocumentId) {
		// Initialize variables
		financialDocumentGroupWrapper result = new financialDocumentGroupWrapper();
		
		// retrieve Fiancial Document
		result.FinancialDocument = FinancialDocumentDataAccessor.getFinancialDocumentById(financialDocumentId);
		
		// Retrieve and map Financial Category Total Prices
		List<FinancialCatTotPrice__c> pricesList = FinancialCatTotPriceDataAccessor.getFinancialCatTotalPricesByDocumentId(
																financialDocumentId,
																true,
																'FinancialCategoryTotal__r.ServiceCategory__r.Code__c');
		
		result.pricesPerServiceCategoryMap = FinancialCatTotPriceService.mapFinancialCatTotPriceListOnServiceCategory(
																										pricesList);
		
		return result;
	}
	
	
	/** Checks if the user with the given e-mail end fullname(FirstName LastName) is the owner of the given financial 
	 *	document.
	 * @author	Dimitrios Sgourdos
	 * @version 09-Jan-2014
	 * @param	financialDocument	The given financial document
	 * @param	userFullName		The full name (FirstName LastName) of the user
	 * @param	userEmail			The email of the user
	 * @return	If the user is the financial document owner.
	 */
	public static Boolean isTheFinancialDocumentOwner(FinancialDocument__c financialDocument,
													String userFullName,
													String userEmail) {
		return (financialDocument.DocumentOwnerName__c == userFullName
				&& financialDocument.DocumentOwnerEmail__c == userEmail);
	}
	
	
	/** Release the given financial document for approvals in case the given approvals are valid.
	 * @author	Dimitrios Sgourdos
	 * @version 09-Jan-2014
	 * @param	financialDocument	The given financial document
	 * @param	approvals			The financial document approvals that are assigned with the document
	 * @return	If the document was released for approvals successfully or not.
	 */
	public static Boolean releaseFinancialDocumentForApprovals(FinancialDocument__c financialDocument,
															List<FinancialDocumentApproval__c> approvals){
		// Check if the approvals are valid
		if( financialDocument == NULL
				|| approvals == NULL
				|| (! FinancialDocumentApprovalService.isApprovalsValidForRelease(approvals) )
				|| approvals.size()==0) {
			return false;
		}
		
		// Release the document
		financialDocument.IsApprovalReleased__c = true;
		approvals[0].Status__c = FinancialDocumentApprovalDataAccessor.APPROVAL_APPROVED;
		try {
			upsert financialDocument;
			upsert approvals[0];
		} catch(Exception e) {
			// For rerendering reasons
			financialDocument.IsApprovalReleased__c = false;
			approvals[0].Status__c = NULL;
			return false;
		}
		
		return true;
	}
	
	
	/** Give the ownership of the given financial document to the given employee.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Jan-2014
	 * @param	financialDocument	The given financial document
	 * @param	approval			The financial document approval that is assigned with the document ownership
	 * @param	employee			The employee that will take the ownership of the financial document
	 * @return	If the procedure was successfully or not.
	 */
	public static Boolean takeDocumentOwnership(FinancialDocument__c financialDocument,
												FinancialDocumentApproval__c approval,
												Employee_Details__c employee) {
		// Check for invalid data
		if(financialDocument == NULL || approval == NULL || employee == NULL) {
			return false;
		}
		
		// Update document and approval
		FinancialDocumentApproval__c previousDataApproval = new FinancialDocumentApproval__c(
															ApproverName__c = approval.ApproverName__c,
															ApproverJobCode__c = approval.ApproverJobCode__c,
															ApproverLocationCode__c = approval.ApproverLocationCode__c,
															ApproverEmail__c = approval.ApproverEmail__c);
		
		String fullName = FinancialDocumentApprovalService.getEmployeeFullName(employee);
		
		financialDocument.DocumentOwnerName__c  = fullName;
		financialDocument.DocumentOwnerEmail__c = employee.Email_Address__c;
		
		approval.ApproverName__c		 = fullName;
		approval.ApproverEmail__c		 = employee.Email_Address__c;
		approval.ApproverJobCode__c		 = employee.Job_Class_Desc__c;
		approval.ApproverLocationCode__c = employee.Location_Code__c;
		
		try {
			upsert financialDocument;
			upsert approval;
		} catch (Exception e) {
			// For rerendering reasons
			financialDocument.DocumentOwnerName__c  = previousDataApproval.ApproverName__c;
			financialDocument.DocumentOwnerEmail__c = previousDataApproval.ApproverEmail__c;
			approval.ApproverName__c  = previousDataApproval.ApproverName__c;
			approval.ApproverEmail__c = previousDataApproval.ApproverEmail__c;
			approval.ApproverJobCode__c = previousDataApproval.ApproverJobCode__c;
			approval.ApproverLocationCode__c = previousDataApproval.ApproverLocationCode__c;
			return false;
		}
		
		return true;
	}
	
	
	/** Update the Approval and Document statuses of the given financial document. If the financial document is fully
	 *	approved, then it finds and update all the invalid documents and it locks the study services prices.
	 * @author	Dimitrios Sgourdos
	 * @version 21-Jan-2014
	 * @param	financialDocument	The given financial document
	 * @return	The updated financial document.
	 */
	public static FinancialDocument__c updateDocumentApprovalStatus(FinancialDocument__c financialDocument) {
		// Check the parameter validity
		if(financialDocument == NULL || financialDocument.Id==NULL) {
			return financialDocument;
		}
		
		// Store initial info for comparisons
		String oldApprovalStatus = financialDocument.ApprovalStatus__c;
		String oldDocumentStatus = financialDocument.DocumentStatus__c;
		
		// Read the document approvals
		List<FinancialDocumentApproval__c> approvalsList = FinancialDocumentApprovalService.getApprovalsByDocumentId(
																							financialDocument.Id,
																							'SequenceNumber__c');
		
		// Check the status
		if( FinancialDocumentApprovalService.isAnApprovalRejected(approvalsList) ) {
			
			financialDocument.ApprovalStatus__c = FinancialDocumentDataAccessor.DOCUMENT_REJECTED;
			financialDocument.DocumentStatus__c = FinancialDocumentDataAccessor.DOCUMENT_STATUS_REJECTED;
			// If there is meaning for update (happened here to be updated before other triggers run)
			if(oldApprovalStatus != financialDocument.ApprovalStatus__c
					|| oldDocumentStatus != financialDocument.DocumentStatus__c ) {
				update financialDocument;
			}
			
		} else if( approvalsList.size() > 1
					&& financialDocument.ApprovalsUpToDate__c
					&& FinancialDocumentApprovalService.isAllApprovalsApproved(approvalsList) ) {
			
			financialDocument.ApprovalStatus__c = FinancialDocumentDataAccessor.DOCUMENT_ACCEPTED;
			financialDocument.DocumentStatus__c = FinancialDocumentDataAccessor.DOCUMENT_STATUS_APPROVED;
			// If there is meaning for update (happened here to be updated before other triggers run)
			if(oldApprovalStatus != financialDocument.ApprovalStatus__c
					|| oldDocumentStatus != financialDocument.DocumentStatus__c ) {
				update financialDocument;
				// Make other documents invalid + update StudyServicePricing to lock records To be implemented
				if(financialDocument.DocumentType__c == FinancialDocumentDataAccessor.PROPOSAL_TYPE){
					makeOtherDocumentsInvalid(financialDocument);
				}
				lockStudyServicePricingRecords(financialDocument.Id);
			}
			
		} else {
			
			financialDocument.ApprovalStatus__c = NULL;
			financialDocument.DocumentStatus__c = FinancialDocumentDataAccessor.DOCUMENT_STATUS_IN_PROGRESS;
			// If there is meaning for update (happened here to be updated before other triggers run)
			if(oldApprovalStatus != financialDocument.ApprovalStatus__c
					|| oldDocumentStatus != financialDocument.DocumentStatus__c ) {
				update financialDocument;
			}
			
		}
		
		return financialDocument;
	}
	
	
	/** Retrieve the list of the Proposals that belong to the same project as the given financial document.
	 * @author	Dimitrios Sgourdos
	 * @version 20-Jan-2014
	 * @param	financialDocument	The given financial document
	 * @return	The list of Financial Documents.
	 */
	public static List<FinancialDocument__c> findOtherProposalsOfTheProject(FinancialDocument__c financialDocument,
																			String orderField) {
		String whereClause = 'BDTDeleted__c != true ';
		whereClause += ' AND Client_Project__c = ' + '\'' + financialDocument.Client_Project__c + '\' ';
		whereClause += ' AND DocumentType__c = ' + '\'' + FinancialDocumentDataAccessor.PROPOSAL_TYPE + '\' ';
		whereClause += ' AND Id != '  + '\'' + financialDocument.Id + '\' ';
		
		return FinancialDocumentDataAccessor.getFinancialDocuments(whereClause, orderField);
	}
	
	
	/** Determines if the given financial document is assigned with at least one of the given studies.
	 * @author	Dimitrios Sgourdos
	 * @version 20-Jan-2014
	 * @param	financialDocument	The given financial document
	 * @param	studiesIds			The studies Ids that must be discovered
	 * @return	If the given financial document is assigned with at least one of the given studies.
	 */
	public static Boolean hasTheDocumentOneOfTHeStudies(FinancialDocument__c financialDocument,
														Set<String> studiesIds) {
		// Check the validity of the parameters
		if( financialDocument == NULL || String.isBlank(financialDocument.listOfStudyIds__c) ) {
			return false;
		}
		
		// Initialize variables
		Set<String> docStudiesSet = new Set<String>();
		List<String> docStudiesList = financialDocument.listOfStudyIds__c.split(':');
		docStudiesSet.addAll(docStudiesList);
		
		Integer initialSize = docStudiesSet.size();
		
		// Remove the given studies and check if the size decreases
		docStudiesSet.removeAll(studiesIds);
		
		return (docStudiesSet.size() < initialSize);
	}
	
	
	/** Make the proposals, that belong to the same project with the given proposal and have at least one common study
	 *	selected, invalid.
	 * @author	Dimitrios Sgourdos
	 * @version 20-Jan-2014
	 * @param	financialDocument	The given financial document
	 */
	public static void makeOtherDocumentsInvalid(FinancialDocument__c financialDocument) {
		// If there is meaning in the search
		if( String.isBlank(financialDocument.listOfStudyIds__c) ) {
			return;
		}
		
		// Initialize variables
		List<FinancialDocument__c> otherDocumentsList = new List<FinancialDocument__c>();
		List<FinancialDocument__c> updateList		  = new List<FinancialDocument__c>();
		
		String currentListOfStudies = financialDocument.listOfStudyIds__c;
		
		Set<String> studiesSet = new Set<String>();
		List<String> studiesList = financialDocument.listOfStudyIds__c.split(':');
		studiesSet.addAll(studiesList);
		
		// Find the other documents
		otherDocumentsList = findOtherProposalsOfTheProject(financialDocument, 'Name');
		
		// Check the validity of the other documents
		for(FinancialDocument__c tmpDoc : otherDocumentsList) {
			if( hasTheDocumentOneOfTHeStudies(tmpDoc, studiesSet) ) {
				Boolean rejectFlag = (tmpDoc.ApprovalStatus__c == FinancialDocumentDataAccessor.DOCUMENT_REJECTED);
				tmpDoc.ApprovalStatus__c = (rejectFlag)? tmpDoc.ApprovalStatus__c : NULL;
				tmpDoc.DocumentStatus__c = FinancialDocumentDataAccessor.DOCUMENT_STATUS_INVALID;
				updateList.add(tmpDoc);
			}
		}
		
		// Update the invalid documents
		if( ! updateList.isEmpty()) {
			upsert updateList;
		}
	}
	
	
	/**	Lock the StudyServicePricing__c records by giving value to the field ApprovedFinancialDocument__c.
	 * @author	Dimitrios Sgourdos
	 * @version	20-Jan-2014
	 * @param	financialDocumentId		The Id that will be saved in the field ApprovedFinancialDocument__c
	*/
	public static void lockStudyServicePricingRecords(String financialDocumentId) {
		String whereClause = 'FinancialDocumentIDs__c like ' + '\'' +  '%' + financialDocumentId + '%' + '\' ';
		
		List<StudyServicePricing__c> sspList = StudyServicePricingDataAccessor.getStudyServicePricings(
																								whereClause,
																								'Name');
		
		for(StudyServicePricing__c tmpSSP : sspList) {
			tmpSSP.ApprovedFinancialDocument__c = financialDocumentId;
		}
		update sspList;
	}
	
	
	/**	Determine the page reference of the given process step.
	 * @author	Dimitrios Sgourdos
	 * @version	22-Jan-2014
	 * @param	processStep			The given process section of the document
	 * @return	The page reference
	*/
	public static String getDocumentSectionPageRef(String processStep) {
		String result = '';
		
		if(processStep == FinancialDocumentHistoryDataAccessor.STUDY_SELECTION_PROCESS) {
			result = '/apex/bdt_financialdocstudysel?financialDocumentId=';
		} else if(processStep == FinancialDocumentHistoryDataAccessor.PRICE_PROCESS) {
			result = '/apex/bdt_financialdocpricing?financialDocumentId=';
		} else if(processStep == FinancialDocumentHistoryDataAccessor.PAYMENT_PROCESS) {
			result = '/apex/bdt_financialdocpayment?financialDocumentId=';
		} else if(processStep == FinancialDocumentHistoryDataAccessor.APPROVAL_PROCESS) {
			result = '/apex/bdt_financialdocapproval?financialDocumentId=';
		}
		
		return result;
	}
	
	
	/**	Create the HTML body of the emails that will be sent to the users when a financial document is changed.
	 * @author	Dimitrios Sgourdos
	 * @version	22-Jan-2014
	 * @param	financialDocument	The financial document that was changed
	 * @param	processStep			The process section that the change happened
	 * @param	plenboxUserName		The plenbox user that changed the document
	 * @param	changeDate			The date and time that the change happened
	 * @param	tz					The timezone of the user that made the change
	 * @return	The html body of the email
	*/
	public static String createHtmlBodyForEmailNotifications(FinancialDocument__c financialDocument,
													String processStep,
													String plenboxUserName,
													DateTime changeDate,
													TimeZone tz) {
		// Initialize variables
		String mainRef = URL.getSalesforceBaseUrl().toExternalForm();
		String pageRef = getDocumentSectionPageRef(processStep) + financialDocument.Id;
		
		String openSpan = '<span style="font-style:italic;">';
		String closeSpan = '</span>';
		
		String HTMLBody = '<p style="font-family:Arial,Helvetica,sans-serif; font-size:13px;">';
		
		// Create main body
		HTMLBody += 'A change has been made for:<br/>';
		HTMLBody += 'Project ID: ' + openSpan + financialDocument.Client_Project__r.Code__c + closeSpan + '<br/>';
		HTMLBody += 'Document name: ' + openSpan + financialDocument.Name + closeSpan + '<br/>';
		HTMLBody += 'Affected section: ';
		HTMLBody += '<a style="font-style:italic;" href="'+ mainRef + pageRef + '">';
		HTMLBody += processStep + '</a>' + '<br/>';
		HTMLBody += 'By: ' + openSpan + plenboxUserName + closeSpan;
		HTMLBody += ' on ' + openSpan + changeDate.format('dd MMM yyyy, HH:mm');
		HTMLBody += '  (' + tz.getDisplayName() + ' - ' + tz.getID() + ')' + closeSpan + '<br/><br/><br/>';
		
		// Create link in the body footer
		HTMLBody += '<span style="color:#808080;">';
		HTMLBody += '* Remove notifications for this document:<br/>';
		pageRef = getDocumentSectionPageRef(FinancialDocumentHistoryDataAccessor.APPROVAL_PROCESS) + financialDocument.Id;
		HtmlBody +='<a style="color:#808080;" href="'+ mainRef + pageRef + '">';
		HTMLBody += 'Approval Section';
		HTMLBody += '</a>' + closeSpan;
		
		HTMLBody += '</p>';
		
		return HTMLBody;
	}
	
	
	/**	Create the HTML body of the emails that will be sent to the users when a financial document is released for
	 *	approval.
	 * @author	Dimitrios Sgourdos
	 * @version	23-Jan-2014
	 * @param	financialDocument	The financial document that was released
	 * @param	approvalReason		The reason that the document must be approved/rejected
	 * @param	deadlineDate		The deadline of the approval
	 * @return	The html body of the email
	*/
	public static String createHtmlBodyForApprovalEmailAppointment(FinancialDocument__c financialDocument,
																String approvalReason,
																DateTime deadlineDate) {
		// Initialize variables
		String mainRef = URL.getSalesforceBaseUrl().toExternalForm();
		String pageRef = getDocumentSectionPageRef(FinancialDocumentHistoryDataAccessor.APPROVAL_PROCESS);
		pageRef += financialDocument.Id;
		
		String openSpan = '<span style="font-style:italic;">';
		String closeSpan = '</span>';
		
		// Create email body
		String HTMLBody = '<p style="font-family:Arial,Helvetica,sans-serif; font-size:13px;">';
		
		HTMLBody += 'You have been specified as an approver:<br/>';
		HTMLBody += 'Project ID: ' + openSpan + financialDocument.Client_Project__r.Code__c + closeSpan + '<br/>';
		HTMLBody += 'Document name: ' + openSpan + financialDocument.Name + closeSpan + '<br/>';
		HTMLBody += 'Reason for approval: ' + openSpan + approvalReason + closeSpan + '<br/>';
		HTMLBody += 'Deadline: ' + openSpan + deadlineDate.format('dd MMM yyyy') + closeSpan + '<br/><br/>';
		
		HTMLBody += '1) Accept the appointment in the attachment.<br/>';
		HTMLBody += '2) Go to ' + openSpan + '"Plenbox"' + closeSpan + ' and approve/reject the document<br/>';
		HTMLBody += '<a href="'+ mainRef + pageRef + '">Approval Section</a><br/>';
		
		HTMLBody += '</p>';
		
		return HTMLBody;
	}
	
	
	/**	Create a document approval appointment Instance.
	 * @author	Dimitrios Sgourdos
	 * @version	23-Jan-2014
	 * @param	organizerName		The name of the organizer of the appointment
	 * @param	organizerEmail		The email of the organizer of the appointment
	 * @param	organizerTimeZoneId	The time zone id of the organizer of the appointment
	 * @param	attendeeName		The name of the attendee of the appointment
	 * @param	attendeeEmail		The email of the attendee of the appointment
	 * @param	deadlineDate		The deadline of the approval
	 * @return	The document approval appointment Instance
	*/
	public static Blob createDocumentApprovalAppointmentInstance(String organizerName,
															String organizerEmail,
															String organizerTimeZoneId,
															String attendeeName,
															String attendeeEmail,
															DateTime deadlineDate) {
		// Adjust the organizer email in case it is the same with the attendee name
		if( organizerEmail.equalsIgnoreCase(attendeeEmail) ) {
			organizerEmail = 'donotreply@praIntl.com';
		}
		
		// Adjust the dates
		String startDateStr = deadlineDate.format('yyyyMMdd') + 'T000100';
		String endDateStr	= deadlineDate.format('yyyyMMdd') + 'T235900';
				
		DateTime creationDate = DateTime.Now();
		String creationDateStr = creationDate.format('yyyyMMdd') + 'T' + creationDate.format('hhmmss');
		
		// Create the appointment
		String attachStr = 'BEGIN:VCALENDAR' + '\n';
		attachStr += 'PRODID:-//Force.com Labs//iCalendar Export//EN\n';
		attachStr += 'VERSION:2.0\n';
		attachStr += 'METHOD:REQUEST\n';
		attachStr += 'BEGIN:VTIMEZONE\n';
		attachStr += 'TZID:' + organizerTimeZoneId + '\n';
		attachStr += 'END:VTIMEZONE\n';
		attachStr += 'BEGIN:VEVENT\n';
		attachStr += 'DTSTART;TZID=' + organizerTimeZoneId + ':' + startDateStr + '\n';
		attachStr += 'DTEND;TZID=' + organizerTimeZoneId + ':' + endDateStr + '\n';
		attachStr += 'DTSTAMP;TZID=' + organizerTimeZoneId + ':' + creationDateStr + '\n';
		attachStr += 'ORGANIZER;CN="' + organizerName + '":mailto:' + organizerEmail + '\n';
		attachStr += 'UID:' + organizerEmail + '\n';
		attachStr += 'ATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN="'
						+ attendeeName + '":MAILTO:' + attendeeEmail + '\n';
		attachStr += 'CREATED;TZID=' + organizerTimeZoneId + ':' + creationDateStr + '\n';
		attachStr += 'DESCRIPTION: \n';
		attachStr += 'SEQUENCE:0\n';
		attachStr += 'LOCATION;LANGUAGE=en-US:Plenbox\n';
		attachStr += 'STATUS:CONFIRMED\n';
		attachStr += 'SUMMARY:' + FinancialDocumentDataAccessor.RELEASE_FOR_APPROVAL_APPOINTMENT_SUBJECT + '\n';
		attachStr += 'TRANSP:OPAQUE\n';
		attachStr += 'END:VEVENT\n';
		attachStr += 'END:VCALENDAR';
		
		blob result = blob.valueOf(attachStr);
		return result;
	}
	
	
	/**	This class keeps a Financial Document with all the info that is usefull to set the applicable approvals
	 *	for this Financial Document.
	 * @author	Dimitrios Sgourdos
	 * @version	17-Dec-2013
	 */
	public class financialDocumentGroupWrapper {
		public FinancialDocument__c 						financialDocument			{get;set;}
		public Map<String, List<FinancialCatTotPrice__c>>	pricesPerServiceCategoryMap	{get;set;}
		
		public financialDocumentGroupWrapper() {
			financialDocument = new FinancialDocument__c();
			pricesPerServiceCategoryMap  = new Map<String, List<FinancialCatTotPrice__c>>();
		}
	}
}