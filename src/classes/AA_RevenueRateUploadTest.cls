@isTest
private class AA_RevenueRateUploadTest{   
    
    
    /* Test Read File Button options */
    static testMethod void ReadFileErrorTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Attachment attach=new Attachment();    
         attach.Name='Unit Test Attachment';
         Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
         
 
         AA_RevenueRateUpload upload = new AA_RevenueRateUpload();
         
         String blobCreator = 'Row 1' + '\r\n' + 'BU1F1,2011,100,CUR1,Y' 
                                      + '\r\n' + 'BU1F1,2011,100,CUR1,Y' 
                                      + '\r\n' + 'BU,2013,100,CUR1,Y' 
                                      + '\r\n' + 'BU1F1,1990,100,CUR1,Y' 
                                      + '\r\n' + 'BU1F1,2015,100,C,Y' 
                                      + '\r\n' + 'BU1F1,2016,100,CUR1,N'  
                                      + '\r\n' + 'BU1F1,2017,A,CUR1,Y' 
                                      + '\r\n' + 'BU1F1,A,100,CUR1,Y' 
                                      + '\r\n' + 'BU1F1,2019,100,CUR1,Y' 
                                      + '\r\n' + 'BU1F1,2020,100,CUR1,Y' ;
         upload.contentFile = blob.valueof(blobCreator);
         upload.ReadFile();
         upload.closeUpdatePopup();
         upload.closeConfirmationPopup();
         upload.continueButton();
         upload.getUpdateRecords();
         System.assertEquals(upload.ReadFile(),null);
         
         Test.stopTest();
    }
    
    /* Test Read File Button options */
    static testMethod void ReadFilePassTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Attachment attach=new Attachment();    
         attach.Name='Unit Test Attachment';
         Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
         
 
         AA_RevenueRateUpload upload = new AA_RevenueRateUpload();
         
         String blobCreator = 'Row 1' + '\r\n' + 'BU1F1,2011,123,USD,YE'; 

         upload.contentFile = blob.valueof(blobCreator);
         upload.ReadFile();
         upload.closeUpdatePopup();
         upload.closeConfirmationPopup();
         upload.continueButton();
         upload.getuploadedRates();
         System.assertEquals(upload.ReadFile(),null);
         
         Test.stopTest();
    }
    
    
    /* Test Back Button */
    static testMethod void backpressedTest() {
    	 AA_RevenueRateUpload upload = new AA_RevenueRateUpload();
    	 upload.backpressed();
         System.assertEquals(1,1);
    }
    /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }  
    
    
}