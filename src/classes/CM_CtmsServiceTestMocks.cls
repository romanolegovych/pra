/**
 * Mocks for Contract Management Web Service Service Callout 
 *
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 * http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_callouts_wsdl2apex_testing.htm
 */
@isTest
public class CM_CtmsServiceTestMocks implements WebServiceMock {
    
    //This is just for throwing an Runtime Exception
    public boolean shouldCreateError {get;set;}
    
    private static final String GET_SITES = 'CM_CtmsService.ReadSite_Output_element';
    private static final String GET_SITE_CONTRACTS = 'CM_CtmsService.ReadSiteContract_Output_element';
    private static final String GET_LOVS = 'CM_CtmsService.ReadLOV_Output_element';
    private static final String GET_ACCOUNTS = 'CM_CtmsService.ReadAccount_Output_element';
    private static final String GET_CONTACTS = 'CM_CtmsService.ReadContact_Output_element';
    private static final String GET_SITE_CONTACTS = 'CM_CtmsService.ReadSiteContact_Output_element';
    private static final String GET_SITE_TEAMS = 'CM_CtmsService.ReadSiteTeam_Output_element';
    private static final String UPSERT_SITE_ACTIVITIES = 'CM_CtmsService.WriteSiteActivity_Output_element';
    private static final String UPSERT_SITE_CONTARCTS = 'CM_CtmsService.WriteSiteContract_Output_element';
    private static final String dummyDate = '2013-03-13T21:43:19';
    
    private static final String WS_SUCCESS = 'Success';
    
    public CM_CtmsServiceTestMocks(boolean doError) {
        this.shouldCreateError = doError;
        system.debug('----------- Mocking a CM_CtmsServiceTestMocks method request ----------------- doError:'+doError);
    }
    
    public void doInvoke(Object stub, Object request, Map<String, Object> response, String endpoint, String soapAction,
        String requestName, String responseNS, String responseName, String responseType) {
        if (responseType.equals(GET_SITES)) {
            response.put('response_x', getSites());
        } else if(responseType.equals(GET_SITE_CONTRACTS)) {
            response.put('response_x', getSiteContracts());
        } else if(responseType.equals(GET_LOVS)) {
            response.put('response_x', getLOVs());
        } else if(responseType.equals(GET_ACCOUNTS)) {
            response.put('response_x', getAccounts());
        } else if(responseType.equals(GET_CONTACTS)) {
            response.put('response_x', getContacts());
        } else if(responseType.equals(GET_SITE_CONTACTS)) {
            response.put('response_x', getSiteContacts());
        } else if(responseType.equals(GET_SITE_TEAMS)) {
            response.put('response_x', getSiteTeams());
        } else if(responseType.equals(UPSERT_SITE_ACTIVITIES)) {
            response.put('response_x', upsertSiteActivities());
        } else if(responseType.equals(UPSERT_SITE_CONTARCTS)) {
            response.put('response_x', upsertSiteContracts());
        }
        system.debug('----------------------------request------------------------' + request);
        system.debug('----------------------------response------------------------' + response);
    }
    
    /**
    *   Returns the list of Sites for the input criteria
    */
    private CM_CtmsService.ReadSite_Output_element getSites() {
        if (shouldCreateError) {
            throwException();
        }
        CM_CtmsService.ReadSite_Output_element response = new CM_CtmsService.ReadSite_Output_element();
        response.status = WS_SUCCESS;
        response.sites = getDummySites(2);
        return response;
    }
    
    /**
    *   Returns the list of Site Contracts for the input criteria
    */
    private CM_CtmsService.ReadSiteContract_Output_element getSiteContracts() {
        if (shouldCreateError) {
            throwException();
        }
        CM_CtmsService.ReadSiteContract_Output_element response = new CM_CtmsService.ReadSiteContract_Output_element();
        response.status = WS_SUCCESS;
        response.sites = getDummySiteContracts(2);
        return response;
    }
    
    /**
     * Returns the LOV - List of values
     */
    private CM_CtmsService.ReadLOV_Output_element getLOVs() {
        if (shouldCreateError) {
            throwException();
        }
        CM_CtmsService.ReadLOV_Output_element response = new CM_CtmsService.ReadLOV_Output_element();
        response.status = WS_SUCCESS;
        response.lovs = getDummyLOVs(2);
        return response;
    }
    
    
    /**
    *   Returns the list of Accounts for the input criteria
    */
    private CM_CtmsService.ReadAccount_Output_element getAccounts() {
        if (shouldCreateError) {
            throwException();
        }
        CM_CtmsService.ReadAccount_Output_element response = new CM_CtmsService.ReadAccount_Output_element();
        response.status = WS_SUCCESS;
        response.accounts = getDummyAccounts(2);
        return response;
    }
    
    /**
    *   Returns the list of Contacts for the input criteria
    */
    private CM_CtmsService.ReadContact_Output_element getContacts() {
        if (shouldCreateError) {
            throwException();
        }
        CM_CtmsService.ReadContact_Output_element response = new CM_CtmsService.ReadContact_Output_element();
        response.status = WS_SUCCESS;
        response.contacts = getDummyContacts(2);
        return response;
    }
    
    /**
    *   Returns the list of Site Contacts for the input criteria
    */
    private CM_CtmsService.ReadSiteContact_Output_element getSiteContacts() {
        if (shouldCreateError) {
            throwException();
        }
        CM_CtmsService.ReadSiteContact_Output_element response = new CM_CtmsService.ReadSiteContact_Output_element();
        response.status = WS_SUCCESS;
        response.sites = getDummySiteContacts(2);
        return response;
    }
    
    /**
    *   Returns the list of Site Teams for the input criteria
    */
    private CM_CtmsService.ReadSiteTeam_Output_element getSiteTeams() {
        if (shouldCreateError) {
            throwException();
        }
        CM_CtmsService.ReadSiteTeam_Output_element response = new CM_CtmsService.ReadSiteTeam_Output_element();
        response.status = WS_SUCCESS;
        response.sites = getDummySiteTeams(2);
        return response;
    }
    
    /**
    *   Upserts the list of Site Activities
    */
    public CM_CtmsService.WriteSiteActivity_Output_element upsertSiteActivities() {
        if (shouldCreateError) {
            throwException();
        }
        CM_CtmsService.WriteSiteActivity_Output_element response = new CM_CtmsService.WriteSiteActivity_Output_element();
        response.status = WS_SUCCESS;
        response.sites = getDummySiteActivities(2);
        return response;
    }
    
    /**
    *   Upserts the list of Site Contracts
    */
    private CM_CtmsService.WriteSiteContract_Output_element upsertSiteContracts() {
        if (shouldCreateError) {
            throwException();
        }
        CM_CtmsService.WriteSiteContract_Output_element response = new CM_CtmsService.WriteSiteContract_Output_element();
        response.status = WS_SUCCESS;
        response.sites = getDummySiteContracts(2);
        return response;
    }
    
    /********************** Methods to build objects in responses ******************************/
    
    private static void throwException() {
        Integer i = Integer.valueOf('s');
    }
    
    private static CM_CtmsService_Site.sites getDummySites(Integer size) {
        CM_CtmsService_Site.sites sites = new CM_CtmsService_Site.sites();
        sites.site = new list<CM_CtmsService_Site.site>();
        CM_CtmsService_Site.site site = null;
        for (Integer i = 1; i <= size; i++) {
            site = new CM_CtmsService_Site.site();
            site.id = 'SITE_ID_'+i;
            site.city = 'CITY_'+i;
            site.country = 'COUNTRY_'+i;
            site.investigatorFirstName = 'INV_F_'+i;
            site.investigatorLastName = 'INV_L_'+i;
            site.sponsorId = 'SPONSOR_ID_'+i;
            site.sponsor = 'SPONSOR_'+i;
            site.protocol = 'PROTOCOL_'+i;
            site.region = 'REGION_'+i;
            site.accountId = 'ACC_ID_'+i;
            site.account = 'ACC_'+i;
            site.siteNumber = 'SITE_NUM_'+i;
            site.status = 'STATUS_'+i;
            sites.site.add(site);
        }
        return sites;
    }
    
    private static CM_CtmsService_SiteContract.sites getDummySiteContracts(Integer size) {
        CM_CtmsService_SiteContract.sites sites = new CM_CtmsService_SiteContract.sites();
        sites.site = new list<CM_CtmsService_SiteContract.site>();
        CM_CtmsService_SiteContract.site site = new CM_CtmsService_SiteContract.site();
        site.id = 'SITE_ID';
        sites.site.add(site);
        site.contracts = new CM_CtmsService_SiteContract.contracts();
        site.contracts.contract = new list<CM_CtmsService_SiteContract.contract>();
        
        CM_CtmsService_SiteContract.contract contract = null;
        CM_CtmsService_SiteContract.status status = null;
        
        for (Integer i = 1; i <= size; i++) {
            contract = new CM_CtmsService_SiteContract.contract();
            contract.id = 'CONTRACT_ID_'+i;
            contract.ctaId = 'CTA_ID_'+i;
            contract.fullyExecutedDate = dummyDate;
            contract.contractType = 'CONTRACT_TYPE_'+i;
            contract.plannedFinalExecutionDate = dummyDate;
            contract.account = 'ACCOUNT_'+i;
            contract.accountId = 'ACCOUNT_ID_'+i;
            contract.negotiationCompleteDate = dummyDate;
            contract.budgetApprovedDate = dummyDate;
            contract.budgetReceivedDate = dummyDate;
            contract.budgetSentDate = dummyDate;
            contract.comments = 'COMMENTS_'+i;
            contract.contractHolder = 'CONTRACT_HOLDER_'+i;
            contract.firstDraftSentDate = dummyDate;
            contract.firstSiteCommentsDate = dummyDate;
            contract.contractName = 'CONTRACT_NAME_'+i;
            contract.partiallyExecutedDate = dummyDate;
            contract.sponsorName = 'SPONSOR_NAME_'+i;
            contract.team = 'TEAM_'+i;
            contract.statuses = new CM_CtmsService_SiteContract.statuses();
            contract.statuses.status = new list<CM_CtmsService_SiteContract.status>();
            for (Integer j = 1; j <= size; j++) {
                status = new CM_CtmsService_SiteContract.status();
                status.id = 'STATUS_ID_'+i+'_'+j;
                status.comment = 'COMMENT_'+i+'_'+j;
                status.status = 'STATUS_'+i+'_'+j;
                status.date_x = dummyDate;
                contract.statuses.status.add(status);
            }
            site.contracts.contract.add(contract);
        }
        return sites;
    }
    
    private static CM_CtmsService_LOV.lovs getDummyLOVs(Integer size) {
        CM_CtmsService_LOV.lovs lovs = new CM_CtmsService_LOV.lovs();
        lovs.lov = new list<CM_CtmsService_LOV.lov>(); 
        CM_CtmsService_LOV.lov lov = null;
        for (Integer i = 1; i <= size; i++) {
            lov = new CM_CtmsService_LOV.lov();
            lov.active = 'Y';
            lov.id = 'ID_'+i;
            lov.name = 'NAME_'+i;
            lov.parent = 'PARENT_'+i;
            lov.type_x = 'TYPE_'+i;
            lov.value = 'VALUE_'+i;
            lovs.lov.add(lov);
        }
        return lovs;
    }
    
    private static CM_CtmsService_Account.accounts getDummyAccounts(Integer size) {
        CM_CtmsService_Account.accounts accounts = new CM_CtmsService_Account.accounts();
        accounts.account = new list<CM_CtmsService_Account.account>(); 
        CM_CtmsService_Account.account account = null;
        for (Integer i = 1; i <= size; i++) {
            account = new CM_CtmsService_Account.account();
            account.id = 'ID_'+i;
            account.addressLine1 = 'ADD1_'+i;
            account.addressLine2 = 'ADD2_'+i;
            account.addressLine3 = 'ADD3_'+i;
            account.city = 'CITY_'+i;
            account.province = 'PROV_'+i;
            account.postalCode = 'POSTAL_CODE_'+i;
            account.country = 'COUNTRY_'+i;
            account.accountType = 'ACC_TYPE_'+i;
            account.location = 'LOCATION_'+i;
            account.account = 'ACCOUNT_'+i;
            account.status = 'STATUS_'+i;
            account.accountClass = 'ACC_CLASS_'+i;
            accounts.account.add(account);
        }
        return accounts;
    }
    
    private static CM_CtmsService_Contact.contacts getDummyContacts(Integer size) {
        CM_CtmsService_Contact.contacts contacts = new CM_CtmsService_Contact.contacts();
        contacts.contact = new list<CM_CtmsService_Contact.contact>(); 
        CM_CtmsService_Contact.contact contact = null;
        for (Integer i = 1; i <= size; i++) {
            contact = new CM_CtmsService_Contact.contact();
            contact.id = 'ID_'+i;
            contact.email = 'EMAIl_'+i;
            contact.firstName = 'FNAME_'+i;
            contact.lastName = 'LNAME_'+i;
            contact.title = 'TITLE_'+i;
            contact.mainPhone = 'MAIN_PHONE_'+i;
            contact.addressLine1 = 'ADD1_'+i;
            contact.city = 'CITY_'+i;
            contact.stateProvince = 'STATE_'+i;
            contact.postalCode = 'POSTAL_CODE_'+i;
            contact.country = 'COUNTRY_'+i;
            contacts.contact.add(contact);
        }
        return contacts;
    }
    
    private static CM_CtmsService_SiteContact.sites getDummySiteContacts(Integer size) {
        CM_CtmsService_SiteContact.sites sites = new CM_CtmsService_SiteContact.sites();
        sites.site = new list<CM_CtmsService_SiteContact.site>();
        CM_CtmsService_SiteContact.site site = new CM_CtmsService_SiteContact.site();
        CM_CtmsService_SiteContact.contacts contacts = new CM_CtmsService_SiteContact.contacts();
        site.id = 'SITE_ID';
        site.contacts = contacts;
        contacts.contact = new list<CM_CtmsService_SiteContact.contact>(); 
        CM_CtmsService_SiteContact.contact contact = null;
        for (Integer i = 1; i <= size; i++) {
            contact = new CM_CtmsService_SiteContact.contact();
            contact.id = 'ID_'+i;
            contact.mobilePhone = 'MOBILE_'+i;
            contact.city = 'CITY_'+i;
            contact.country = 'COUNTRY_'+i;
            contact.email = 'EMAIl_'+i;
            contact.mainFax = 'MAIN_FAX_'+i;
            contact.firstName = 'FNAME_'+i;
            contact.lastName = 'LNAME_'+i;
            contact.endDate = dummyDate;
            contact.startDate = dummyDate;
            contact.pager = 'PAGER_'+i;
            contact.postalCode = 'POSTAL_CODE_'+i;
            contact.province = 'PROVINCE_'+i;
            contact.role = 'ROLE_'+i;
            contact.addressLine1 = 'ADD1_'+i;
            contact.addressLine2 = 'ADD2_'+i;
            contact.addressLine3 = 'ADD3_'+i;
            contact.mainPhone = 'MAIN_PHONE_'+i;
            contacts.contact.add(contact);
        }
        sites.site.add(site);
        return sites;
    }
    
    private static CM_CtmsService_SiteTeam.sites getDummySiteTeams(Integer size) {
        CM_CtmsService_SiteTeam.sites sites = new CM_CtmsService_SiteTeam.sites();
        sites.site = new list<CM_CtmsService_SiteTeam.site>();
        CM_CtmsService_SiteTeam.site site = new CM_CtmsService_SiteTeam.site();
        CM_CtmsService_SiteTeam.positions positions = new CM_CtmsService_SiteTeam.positions();
        site.id = 'SITE_ID';
        site.positions = positions;
        positions.position = new list<CM_CtmsService_SiteTeam.position>(); 
        CM_CtmsService_SiteTeam.position position = null;
        for (Integer i = 1; i <= size; i++) {
            position = new CM_CtmsService_SiteTeam.position();
            position.id = 'ID_'+i;
            position.email = 'EMAIl_'+i;
            position.firstName = 'FNAME_'+i;
            position.jobTitle = 'JOB_TITLE_'+i;
            position.lastName = 'LNAME_'+i;
            position.userId = 'USER_ID_'+i;
            position.phone = 'PHONE_'+i;
            position.division = 'DIVISION_'+i;
            position.position = 'POSITOIN_'+i;
            position.role = 'ROLE_'+i;
            positions.position.add(position);
        }
        sites.site.add(site);
        return sites;
    }
    
    private static CM_CtmsService_SiteActivity.sites getDummySiteActivities(Integer size) {
        CM_CtmsService_SiteActivity.sites sites = new CM_CtmsService_SiteActivity.sites();
        sites.site = new list<CM_CtmsService_SiteActivity.site>();
        CM_CtmsService_SiteActivity.site site = new CM_CtmsService_SiteActivity.site();
        site.id = 'SITE_ID';
        sites.site.add(site);
        site.activities = new CM_CtmsService_SiteActivity.activities();
        site.activities.activity = new list<CM_CtmsService_SiteActivity.activity>();
        
        CM_CtmsService_SiteActivity.activity activity = null;
        CM_CtmsService_SiteActivity.contact contact = null;
        
        for (Integer i = 1; i <= size; i++) {
            activity = new CM_CtmsService_SiteActivity.activity();
            activity.id = 'ID_'+i;
            activity.activity = 'ACTIVITY_'+i;
            activity.comment = 'COMMENT_'+i;
            activity.createdBy = 'CREATED_BY_'+i;
            activity.description = 'DESC_'+i;
            activity.completedDate = dummyDate;
            activity.completed = 'COMPLETED_'+i;
            activity.subType = 'SUB_TYPE_'+i;
            activity.callerEmployeeFlag = 'CALLER_EMP_FLAG_'+i;
            activity.phoneCallerFirstName = 'PHONE_CALLER_FNAME_'+i;
            activity.phoneCallerLastName = 'PHONE_CALLER_LNAME_'+i;
            activity.phoneCallerId = 'PHONE_CALLER_ID_'+i;
            activity.planned2 = 'PLANNED2_'+i;
            activity.plannedCompletion = 'PLANNED_COMPLETION_'+i;
            activity.started = 'STARTED_'+i;
            activity.status = 'STATUS_'+i;
            activity.activityType = 'ACT_TYPE_'+i;
            activity.contacts = new CM_CtmsService_SiteActivity.contacts();
            activity.contacts.contact = new list<CM_CtmsService_SiteActivity.contact>();
            for (Integer j = 1; j <= size; j++) {
                contact = new CM_CtmsService_SiteActivity.contact();
                contact.id = 'CONTACT_ID_'+i+'_'+j;
                contact.firstName = 'FNAME_'+i+'_'+j;
                contact.lastName = 'LNAME_'+i+'_'+j;
                contact.employeeFlag = 'EMP_FLAG_'+i+'_'+j;
                activity.contacts.contact.add(contact);
            }
            site.activities.activity.add(activity);
        }
        return sites;
    }
    
}