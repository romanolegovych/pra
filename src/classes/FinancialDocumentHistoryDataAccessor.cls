/** Implements the Selector Layer of the object FinancialDocumentHistory__c
 * @author	Dimitrios Sgourdos
 * @version	17-Jan-2014
 */
public with sharing class FinancialDocumentHistoryDataAccessor {
	
	// Global Constants
	public static final String	STUDY_SELECTION_PROCESS = 'Study Selection';
	public static final String	PRICE_PROCESS 			= 'Price';
	public static final String	PAYMENT_PROCESS 		= 'Payment';
	public static final String	CONTENT_PROCESS 		= 'Content';
	public static final String	APPROVAL_PROCESS 		= 'Approval';
	public static final String	OUTPUT_PROCESS 			= 'Output';
	
	
	/** Object definition for fields used in application for FinancialDocumentHistory
	 * @author	Dimitrios Sgourdos
	 * @version 17-Jan-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('FinancialDocumentHistory__c');
	}
	
	
	/** Object definition for fields used in application for FinancialDocumentHistory with the parameter referenceName 
	 *	as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 17-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'LastModifiedDate,';
		result += referenceName + 'LastModifiedBy.Name,';
		result += referenceName + 'ChangeDescription__c,';
		result += referenceName + 'FinancialDocument__c,';
		result += referenceName + 'ProcessStep__c,';
		result += referenceName + 'TotalValue__c,';
		result += referenceName + 'UserComments__c';
		return result;
	}
	
	
	/**	Filter the history of the selected financial document depends on the selected filter method.
	* @author	Dimitrios Sgourdos
	* @version	17-Jan-2014
	* @param	financialDocumentID		The id of the selected financial document
	* @param	selectedFilterMethod	The selected method for filtering the history of the financial document
	* @return	The records of the financial document history that meets the search criteria
	*/
	public static List<FinancialDocumentHistory__c> getFinancialDocumentHistoryByProcessStep(String financialDocumentId,
																							String processStep,
																							String orderByField,
																							Boolean descendingFlag ) {
		// Check if a process step is selected or all the history is retrieved
		String addedConditon = '';
		if(processStep != NULL) {
			addedConditon = 'AND ProcessStep__c = ' + '\'' + processStep + '\'';
		}
		
		String query = String.format(
								'SELECT {0} ' +
								'FROM FinancialDocumentHistory__c ' +
								'WHERE FinancialDocument__c = {1} {2}' +
								'ORDER BY {3} {4}',
								new List<String> {
									getSObjectFieldString(),
									'\'' + financialDocumentId + '\'',
									addedConditon,
									orderByField,
									(descendingFlag)? 'DESC' : ''
								}
							);
		
		return (List<FinancialDocumentHistory__c>) Database.query(query);
	}
	
}