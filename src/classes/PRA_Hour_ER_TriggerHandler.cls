/*
* Trigger Handler for the Hour trigger
*/
public class PRA_Hour_ER_TriggerHandler {

 
    private boolean isExecuting = false;
    private integer batchSize = 0;
    
    public PRA_Hour_ER_TriggerHandler(boolean triggerIsExecuting, integer size){
        isExecuting = triggerIsExecuting;
        batchSize = size;
    }
    
    //Commenting out as we dont need for now
    /*
    public void onBeforeInsert(List<Hour_EffortRatio__c> lstHourER){
    }
    
    public void onBeforeUpdate(Map<ID,Hour_EffortRatio__c> oldHourER,Map<ID,Hour_EffortRatio__c> newHourER){
    }
    
    public void onAfterInsert(Map<ID,Hour_EffortRatio__c> newHourER){
    }
    */
    
    public void onAfterUpdate(Map<ID,Hour_EffortRatio__c> oldHourER,Map<ID,Hour_EffortRatio__c> newHourER){
        // Update the Monthly Approval fields after the official forecast unit is changed
       updateMontlyApprovals(oldHourER, newHourER);
    }
    
    // Update monthly approvals
    private void updateMontlyApprovals(Map<ID, Hour_EffortRatio__c> oldValues, Map<ID, Hour_EffortRatio__c> newValues) {
        // Check if the OF Hour confirm or number value has changed
        // If it has, then we need to revert the approval for the PZ released only if the project is not approved
        
        // changed unit value set
        Set<Id> changedHourERSet = new Set<Id>();                
        for(Hour_EffortRatio__c oldHourER: oldValues.values()) {
            if(
            //(oldHourER.Forecast_Hours__c  != newValues.get(oldHourER.Id).Forecast_Hours__c) ||
             (oldHourER.Forecast_ER__c != newValues.get(oldHourER.Id).Forecast_ER__c)  ){           
            
                // So someone has changed the value of a official forecast Hour
                // How dare they? Let us teach them a lesson by updating the Review Approval flags
                
                // Add it to the changed Hour set
                changedHourERSet.add(oldHourER.Id);
            } // end if
        } // end for
        
        if(changedHourERSet.size() > 0) {
            // Get the Hour record from the db so that you have a ref to the client project id
            List<Hour_EffortRatio__c> hoursER = [Select Id, Unit_Effort__r.Client_Task__r.Project__r.Id
                            from Hour_EffortRatio__c where Id = :changedHourERSet];
            // set to hold the project id
            Set<Id> projectIdSet = new Set<Id>();
            
            // go through the HourER and add it to the respective maps           
            for(Hour_EffortRatio__c u: hoursER) {
                projectIdSet.add(u.Unit_Effort__r.Client_Task__r.Project__r.Id);
            }        
            
            if(projectIdSet.size() > 0)  {                  
                List<Monthly_Approval__c> monthlyApprovals = new List<Monthly_Approval__c>();
                
                // get the monthly approvals for the current month and which haven't been approved by the PM    
                monthlyApprovals = [select Id,Month_Approval_Applies_To__c, Approval_Date__c, Client_Project__c,DPD_Approval_Date__c, DPD_approved_forecast__c,EVP_Approval_Date__c,DOF_Approval_Date__c,EVP_Approved__c,DOF_Approved__c, Is_approval_complete__c, 
                                        Is_Approved_DPD_forecast__c,Is_Approved_EVP__c,Is_Approved_DOF__c,Is_Approved_PM_Forecast__c,Is_Approved_PM_Worked_Units__c,Is_Approved_PZ_released_forecast__c,
                                        Need_DOF_EVP_Approval__c,Need_DPD_Approval__c,PM_Forecast_App_Date__c,PM_WU_Approval_Date__c, PZ_Release_Date__c,PM_Approved_Forecast__c, PM_Approved_Worked_Units__c, PZ_released_forecast__c,PZ_Released_Forecast__r.Email
                                      from Monthly_Approval__c where Month_Approval_Applies_To__c = :Date.today().toStartOfMonth()
                                      and Client_Project__c in :projectIdSet
                                      and (PM_Approved_Forecast__c = null or PM_Approved_Forecast__c = '')
                                      and (PZ_released_forecast__c != null or PZ_released_forecast__c != '')];
                
                for(Monthly_Approval__c app : monthlyApprovals) {
                    if(app.PZ_released_forecast__c != null || app.PZ_released_forecast__c != '') {
                        //app.PZ_released_forecast__c = null;
                        
                         app.PZ_Release_Date__c=null; 
                            app.PZ_released_forecast__c=null; 
                            app.Is_Approved_PZ_released_forecast__c=false;
                            
                            app.PM_Forecast_App_Date__c=null; 
                            app.PM_Approved_Forecast__c=null;   
                            app.Is_Approved_PM_Forecast__c=false;
                           
                            app.DPD_approved_forecast__c=null;
                            app.DPD_Approval_Date__c=null;
                            app.Is_Approved_DPD_forecast__c=false;
                              
                            app.EVP_Approved__c=null;
                            app.Is_Approved_EVP__c=false;
                            app.EVP_Approval_Date__c=null;
                            
                            app.DOF_Approval_Date__c=null; 
                            app.Is_Approved_DOF__c=false;
                            app.DOF_Approved__c=null;
                            
                            app.Need_DOF_EVP_Approval__c=false;
                            app.Need_DPD_Approval__c=false;
                            app.Approval_Date__c=null; 
                            app.Is_approval_complete__c=false; 
                    }
                }                
                update monthlyApprovals;                
            } // end if projectIdSet
        }// end if update project released         
    }    
}