@isTest
private class ProjectServiceTimePointDataAccessorTest {

	static testMethod void myUnitTest() {
		// create test data
		Client_Project__c cp = new Client_Project__c(name='test');
		insert cp;

		ServiceCategory__c sc = new ServiceCategory__c(name='test',code__c='001');
		insert sc;

		list<Service__c> sList = new list<Service__c>();
		sList.add(new Service__c(ServiceCategory__c = sc.id, Name = 'a', SequenceNumber__c = 1));
		sList.add(new Service__c(ServiceCategory__c = sc.id, Name = 'b', SequenceNumber__c = 2));
		sList.add(new Service__c(ServiceCategory__c = sc.id, Name = 'c', SequenceNumber__c = 3));
		insert sList;

		ProjectService__c ps = new ProjectService__c(Client_Project__c = cp.id, Service__c = sList[0].id);
		insert ps;

		// create test data
		ClinicalDesign__c cd = new ClinicalDesign__c(name='1');
		insert cd;

		list<flowcharts__c> aList = new list<flowcharts__c>();
		aList.add(new flowcharts__c(ClinicalDesign__c = cd.id, Flowchart_Number__c = 1, Description__c = 'Flowchart 1'));
		aList.add(new flowcharts__c(ClinicalDesign__c = cd.id, Flowchart_Number__c = 2, Description__c = 'Flowchart 2'));
		aList.add(new flowcharts__c(ClinicalDesign__c = cd.id, Flowchart_Number__c = 3, Description__c = 'Flowchart 3'));
		insert aList;

		list<ProjectServiceTimePoint__c> pstList = new list<ProjectServiceTimePoint__c>();
		pstList.add(new ProjectServiceTimePoint__c(ProjectService__c = ps.id, flowchart__c = aList[0].id));
		pstList.add(new ProjectServiceTimePoint__c(ProjectService__c = ps.id, flowchart__c = aList[1].id));
		pstList.add(new ProjectServiceTimePoint__c(ProjectService__c = ps.id, flowchart__c = aList[2].id));
		insert pstList;

		//test retrieve logic
		pstList = ProjectServiceTimePointDataAccessor.getProjectServiceTimePointList('ProjectService__r.Client_Project__c = \'' + cp.id + '\'');
		system.assertEquals(3,pstList.size());
	}
}