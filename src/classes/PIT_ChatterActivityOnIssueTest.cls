@isTest
private class PIT_ChatterActivityOnIssueTest {
    static testMethod void PIT_ChatterActivityOnIssue_TestMethod() {
         WFM_Client__c cnt=new WFM_Client__c();
         cnt.Name='AMGEN INC1';
         cnt.Client_Name__c='AMGEN INC1';
         cnt.Client_Unique_Key__c='AMGEN INC1';
         //cnt.source__c='XYZ';
         Insert cnt;
         
         WFM_Contract__c contr=new WFM_Contract__c();
         contr.name='AMGGRA1X';
         contr.Client_ID__c=cnt.id;
         contr.Contract_Status__c='RN';
         contr.Contract_Unique_Key__c='AMGGRA1X';
         insert contr;
         
         WFM_Project__c proj=new WFM_Project__c();
         proj.Name='AMGGRA1X-AMGRA';
         proj.Contract_ID__c=contr.id;
         proj.Project_Unique_Key__c='AMGGRA1X-AMGRA';
         insert proj;
         
         Issue__c issu = new Issue__c();
         issu.Category__c='Data Quality';
         issu.Description__c='Testing the Feeds';
         issu.Subcategory__c='Site Level';
         issu.Project__c=proj.id;       
         insert issu;
        
         issu.Description__c='Update Description__c';
         update issu;         
         
         feeditem fpost =New Feeditem();
         fpost.type='Textpost';
         fpost.ParentId=issu.id;
         fpost.Title=issu.name;
         fpost.Body=issu.Description__c;
         insert fpost;
         
         delete issu;   
    }
}