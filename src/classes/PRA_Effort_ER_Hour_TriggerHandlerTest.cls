/* This Test class is for the three classes 
   PRA_EffortTriggerHandler,PRA_EffortRatioTriggerHandler,PRA_HourTriggerHandler to cover the test coverage.   
*/

@isTest
private class PRA_Effort_ER_Hour_TriggerHandlerTest {

    static final String testValue = 'Remove after Migration';
	
	static testMethod void shouldTestOldClasses() {
    	PRA_EffortRatioTriggerHandler erCon = new PRA_EffortRatioTriggerHandler();
    	system.assert(erCon.testValue == testValue);
    	
    	PRA_EffortTriggerHandler effCon = new PRA_EffortTriggerHandler();
    	system.assert(effCon.testValue == testValue);
    	
    	PRA_HourTriggerHandler hourCon = new PRA_HourTriggerHandler();
    	system.assert(hourCon.testValue == testValue);
    } 
}