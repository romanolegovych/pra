/** Implements the test for the Selector Layer of the object FinancialDocumentContent__c
 * @author	Dimitrios Sgourdos
 * @version	11-Feb-2014
 */
@isTest
private class BDT_FinancialDocumentContentDataAcceTest {
	
	/** Test the function getContentByDocumentId
	 * @author	Dimitrios Sgourdos
	 * @version	11-Feb-2014
	 */
	static testMethod void getContentByDocumentIdTest() {
		// Create data
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001',
																				'BDT Project First Experiments');
		insert currentProject;
		
		FinancialDocument__c financialDocument = BDT_BuildTestData.buildFinancialDocument(currentProject.Id,
																							new List<Study__c>());
		insert financialDocument;
		
		FinancialDocumentContent__c source = new FinancialDocumentContent__c(FinancialDocument__c=financialDocument.Id);
		insert source;
		
		// Check the function
		FinancialDocumentContent__c result = BDT_FinancialDocumentContentDataAccessor.getContentByDocumentId(financialDocument.Id);
		system.assertEquals(result.Id, source.Id, 'Error in reading financial document content from the system');
	}
}