public with sharing class BDT_DesignCopyController {

	public string ClientProjectID;
	
	// search variables
	public string ProjectSearchTerm {get;set;}
	public string ProjectSearchError {get;set;}
	
	// project selection variables
	public string SelectedProjectID {get;set;}
	public List<SelectOption> ProjectSelectList {get;set;}
	public boolean EnableProjectSelect {get;set;}
	
	// design selection variables
	public string SelectedDesignID {get;set;}
	public List<SelectOption> DesignSelectList {get;set;} 
	public boolean EnableDesignSelect {get;set;}
	
	// target information
	public string NewDesignName {get;set;}
	public string SelectedTargetDesignID {get;set;}
	public List<SelectOption> DesignTargetSelectList {get;set;} 
	
	public BDT_DesignCopyController(){
		ClientProjectID = BDT_Utils.getPreference('SelectedProject');
		if (String.isNotEmpty(ClientProjectID)) {
			ReadProjectSelectList();
			
			SelectedTargetDesignID = 'NA';
			DesignTargetSelectList = new List<SelectOption>();
			For (ClinicalDesign__c c:[select id,name from ClinicalDesign__c where Project__c = :ClientProjectID]){
				DesignTargetSelectList.add(new SelectOption(c.id,c.name));
			}
			DesignTargetSelectList.add(new SelectOption('NA','** Create New Design **'));
			
			
		}
	}
	
	public void ButtonSearchProject(){
		ReadProjectSelectList();
	}
	
	public void ReadProjectSelectList () {
		
		/* if there is a search string then parse the search string else
		 * else get the project code for the current project
		 */
		ProjectSelectList = new List<SelectOption>();
		SelectedProjectID = '';
		EnableProjectSelect = true;
		
		if (String.isEmpty(ProjectSearchTerm)) {
			// get the current project and creat a single item in the dropdownlist
			Client_Project__c cp = BDT_Utils.getProject(ClientProjectID, false);
			ProjectSelectList.add(new SelectOption(cp.id, (cp.Code__c!=null)?cp.Code__c:'No Code'));
			SelectedProjectID = cp.id;
			EnableProjectSelect = false;
		} else {
			// search for matching projects
			List<Client_Project__c> projects = searchProjects();
			
			if (projects!=null && projects.size()>0) {
				// create dropdown values for query results
				For (Client_Project__c project:projects){
					ProjectSelectList.add(new SelectOption(project.id,project.Code__c));
				}
				SelectedProjectID = projects[0].id;
			} else {
				// no search results found
				ProjectSelectList.add(new SelectOption('0','Query returned no results.'));
				EnableProjectSelect = false;
			}
		}
		ReadDesignSelectList ();
	}
	
	public List<Client_Project__c> searchProjects () {
		ProjectSearchError = '';
		try {
				String searchquery = 'FIND \''+ProjectSearchTerm+'*\' IN ALL FIELDS RETURNING';
				searchquery += ' Client_Project__c(Code__c, Title__c, Project_Description__c, id WHERE code__c != null and bdtdeleted__c = false ORDER BY Code__c LIMIT 100)';
				List<List<SObject>> searchList = search.query(searchquery);
				List<Client_Project__c> projectSearchResultList = (List<Client_Project__c>)searchList[0];
				return projectSearchResultList;
				
		}catch (Exception e) {				
			ProjectSearchError='Your search could not be completed. Please rephrase you search term.';
		}
		return null;
	}
	
	public void ReadDesignSelectList () {
		SelectedDesignID = '';
		DesignSelectList = new List<SelectOption>();
		EnableDesignSelect = false;
		List<ClinicalDesign__c> designs;
		try{
			string targetId = (SelectedTargetDesignID=='NA')?null:SelectedTargetDesignID;
			designs = [Select id,name from ClinicalDesign__c where Project__c = :SelectedProjectID and id != :targetId];
		} catch (Exception e) {
			designs = null;
		}
		if (designs!=null && designs.size()>0) {
			// create dropdown values for query results
			For (ClinicalDesign__c design:designs){
				DesignSelectList.add(new SelectOption(design.id,design.name));
			}
			EnableDesignSelect = true;
			SelectedDesignID = designs[0].id;
		} else {
			// no search results found
			DesignSelectList.add(new SelectOption('0','No designs available.'));
			EnableDesignSelect = false;
		}
		
		if (String.isNotEmpty(SelectedDesignID)) {
			showClinicDesign();
		} else {
			hideDesignContainer();
		}
	}
	
	public pageReference ButtonCancel(){
		return new pagereference(System.Page.BDT_Design.getURL() );
	}
	
	public pageReference ButtonCopyDesign() {
		if (String.isNotEmpty(SelectedDesignID) && String.isNotEmpty(NewDesignName)) {
			CreateBasicDesignParts();
			return new pagereference(System.Page.BDT_Design.getURL() );
		} else {
			apexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Specify a design to copy and a new design name.');
			ApexPages.addMessage(msg); 
		}
		return null;
	}
	
	public void CreateBasicDesignParts(){
		
		ClinicalDesign__c newDesign;
		if (SelectedTargetDesignID=='NA') {
			/****** create design *******/
			newDesign = new ClinicalDesign__c(name = NewDesignName, Project__c = ClientProjectID);
			insert newDesign;
		} else {
			newDesign = [select id, name from ClinicalDesign__c where id = :SelectedTargetDesignID];
		}
		
		
		/****** create arms, flowcharts and epochs *******/
		//Map<id,sObject> newDesignElements = new Map<id,sObject>();
		Map<id,sObject> newArmElements = new Map<id,sObject>();
		Map<id,sObject> newEpochElements = new Map<id,sObject>();
		Map<id,sObject> newFlowchartElements = new Map<id,sObject>();
		
		For (ClinicalDesign__c cd : [
				Select 	(Select Id, Description__c, Flowchart_Number__c, listOfTimeMinutes__c From Flowcharts__r)
				, 		(Select Id, Description__c, Duration_days__c, Epoch_Number__c From Epochs__r)
				, 		(Select Id, Arm_Number__c, Description__c From Arms__r) 
				From 	ClinicalDesign__c
				Where   id = :SelectedDesignID
				]){
			
			For (Flowcharts__c f:cd.Flowcharts__r){
				newFlowchartElements.put(f.id,new Flowcharts__c(
					Description__c = f.Description__c,
					Flowchart_Number__c = f.Flowchart_Number__c,
					listOfTimeMinutes__c = f.listOfTimeMinutes__c,
					ClinicalDesign__c = newDesign.id
				));
			}
			For (Arm__c a:cd.Arms__r){
				newArmElements.put(a.id,new Arm__c(
					Description__c = a.Description__c,
					Arm_Number__c = a.Arm_Number__c,
					ClinicalDesign__c = newDesign.id
				));
			}
			For (Epoch__c e:cd.Epochs__r){
				newEpochElements.put(e.id,new Epoch__c(
					Description__c = e.Description__c,
					Duration_days__c = e.Duration_days__c,
					Epoch_Number__c = e.Epoch_Number__c,
					ClinicalDesign__c = newDesign.id
				));
			}
		}

		
		insert newArmElements.values();
		insert newEpochElements.values();
		insert newFlowchartElements.values();
		
		/*** create flowchart assignments ******/
		list<Flowchartassignment__c> newFlowchartAss = new list<Flowchartassignment__c>();
		for (Flowchartassignment__c f: [
			select id, 
				arm__c, 
				flowchart__c, 
				epoch__c 
			from Flowchartassignment__c
			where Flowchart__r.ClinicalDesign__c = :SelectedDesignID]){
			
			newFlowchartAss.add(new Flowchartassignment__c(
				arm__c = newArmElements.get(f.arm__c).id,
				flowchart__c = newFlowchartElements.get(f.flowchart__c).id,
				epoch__c = newEpochElements.get(f.epoch__c).id
			));
			
		}
		
		insert newFlowchartAss;
		
		// create project services
		map<id,projectservice__c> newProjectServices = getProjectServices ();
		
		// create timepoints
		map<id,TimePoint__c> newTimePoints = new map<id,TimePoint__c>();
		For(TimePoint__c t: [Select Minutes__c, 
									Id, 
									Flowchart__c 
								From TimePoint__c 
								Where Flowchart__r.ClinicalDesign__c = :SelectedDesignID]){
		
			newTimePoints.put(t.id,new TimePoint__c(
				Minutes__c = t.Minutes__c,
				Flowchart__c = newFlowchartElements.get(t.Flowchart__c).id
			));
		}
		insert newTimePoints.values();
		
		//create projectservicetimepoints
		list<ProjectServiceTimePoint__c> newPSTP = new list<ProjectServiceTimePoint__c>();
		for(ProjectServiceTimePoint__c p:[Select listOfTimeMinutes__c, 
												TimePoint__c, 
												ProjectService__c, 
												Id, 
												Flowchart__c 
										 From ProjectServiceTimePoint__c 
										 where Flowchart__r.ClinicalDesign__c = :SelectedDesignID]){
										 	
			String Timepoint = (newTimePoints.get(p.TimePoint__c)!=null)?newTimePoints.get(p.TimePoint__c).id:null;
			String ProjectService = newProjectServices.get(p.ProjectService__c).id;
			
			newPSTP.add(new ProjectServiceTimePoint__c(
				listOfTimeMinutes__c = p.listOfTimeMinutes__c,
				TimePoint__c = Timepoint,
				ProjectService__c = ProjectService,
				Flowchart__c = newFlowchartElements.get(p.Flowchart__c).id 
			));
		}
		insert newPSTP;
		
	}
	
	public map<id,projectservice__c> getProjectServices () {
		// read the project services to copy and create or read the projectservices in the target project
		map<id,projectservice__c> psMap = new map<id,projectservice__c>();
		
		list<ProjectService__c> psSourceList = [select service__c, client_project__c, id 
												from projectservice__c 
												where (bdtdeleted__c = false or bdtdeleted__c = null)
												and id in (select projectservice__c
																from projectservicetimepoint__c
																where flowchart__r.clinicaldesign__c = :SelectedDesignID )];
		
		list<ProjectService__c> psTargetList = [select service__c, client_project__c, id 
												from projectservice__c 
												where (bdtdeleted__c = false or bdtdeleted__c = null)
												and client_project__c = :ClientProjectID];
		
		For (ProjectService__c psSource:psSourceList) {
			boolean psFound = false;
			For (integer i = 0 ; i < psTargetList.size() ; i++){
				// find matching ps
				if (psSource.Service__c == psTargetList[i].Service__c) {
					psMap.put(psSource.id,psTargetList[i]);
					psTargetList.remove(i);
					psFound = true;
					break;
				}
			}
			if (!psFound) {
				// no match was found create a new ps item
				psMap.put(psSource.id,new ProjectService__c(Service__c = psSource.Service__c, Client_Project__c = ClientProjectID));
			}
		}
		
		upsert psMap.values();
		
		return psMap;
		
	}
	
	/****************************Clinical design layout display****************************/

	public boolean 									renderDesignDisplay {get;set;}
	public List<Epoch__c> 							epochList 			{get;set;}
	public List<BDT_FlowchartDisplay.armWrapper> 	armWrapperList 		{get;set;}
	public ClinicalDesign__c 						design				{get;set;}

	public void 	showClinicDesign() {
		renderDesignDisplay = true;
		armWrapperList = BDT_FlowchartDisplay.getFlowchart(SelectedDesignID, SelectedProjectID);   
		epochList = BDT_FlowchartDisplay.getEpochHeaderList(SelectedDesignID);
		design = BDT_Utils.getDesign(SelectedDesignID, SelectedProjectID);		
	}
		
	public void 	hideDesignContainer() {
		renderDesignDisplay = false;
	}

}