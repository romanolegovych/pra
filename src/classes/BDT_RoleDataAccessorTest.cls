@isTest
public with sharing class BDT_RoleDataAccessorTest {
	
	static testMethod void getAllRolesTest () {
		//create a record
		BDT_Role__c r = new BDT_Role__c (name='a',Role_Number__c=1);
		insert r;
		
		//retrieve the record
		list<BDT_Role__c> rList = BDT_RoleDataAccessor.getAllRoles('Name');
		system.assertEquals(1,rList.size());
		
		//retrieve specified record
		r = BDT_RoleDataAccessor.getRole(r.id);
		system.assertNotEquals(null,r);
		
		//make an error
		r = BDT_RoleDataAccessor.getRole('not an id');
		system.assertEquals(null,r);
	}
}