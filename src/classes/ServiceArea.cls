public with sharing class ServiceArea extends ServiceBase {
	
	@TestVisible
	private static final String AREA_WORD = 'Area';
	
    public List<Service_Area__c> serviceAreas {
    	get{
    		return (List<Service_Area__c>) serviceRecords;
    	}
    	set{
    		serviceRecords = value;
    	}
    }
    public Service_Area__c serviceArea {
    	get{
    		return (Service_Area__c)serviceRecord;
    	}
    	set{
    		serviceRecord = value;
    	}
    }

    public Service_Area__c serviceAreaForUpdate {
    	get{
    		return (Service_Area__c) serviceForUpdate;
    	}
    	set{
    		serviceForUpdate = value;
    	}
    }
    
    public ServiceArea(String parentServiceId) {
    	super(parentServiceId);
    }
    
    public ServiceArea() {
    }
    
    @TestVisible
    protected override String getServiceWord(){
    	return AREA_WORD;
    }
    
	public override void refreshServices(){
		serviceAreas = ServiceModelServices.getServiceAreaByServiceModelId(parentServiceId);
	}
	
    public override void preparationCreateOrEditService() {
        if(String.isBlank(getEditServiceId())) {
            serviceAreaForUpdate = new Service_Area__c();
            serviceAreaForUpdate.Service_Model__c = parentServiceId;
        } else {
            Map<Id, Service_Area__c> serviceAreaMap = new Map<Id, Service_Area__c>(serviceAreas);
            serviceAreaForUpdate = serviceAreaMap.get(getEditServiceId());
        }
        
        if(!String.isBlank(getCloneServiceId())) {
            serviceAreaForUpdate = serviceArea.clone(false, true);
            serviceAreaForUpdate.Service_Area_Unique_ID__c = null;
        }
    }
    
}