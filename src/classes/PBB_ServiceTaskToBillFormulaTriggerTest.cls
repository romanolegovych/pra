@isTest 
private class PBB_ServiceTaskToBillFormulaTriggerTest {
    static testMethod void validate() {
        
        service_model__c smm = new service_model__c(Name='name16',Description__c ='desc0016',Status__c ='In-Process');
        insert smm;
        System.debug('----smm-inprogress-------'+smm);
        
        service_area__c sa = new service_area__c(Description__c = 'Description01',Service_Model__c = smm.Id);
        insert sa;
        System.debug('----sa--------'+sa);
        
        service_function__c sf = new service_function__c(Name='Service Function1',Description__c = 'Description02',Service_Area__c = Sa.Id);
        insert sf;
        System.debug('----sf--------'+sf);
        
        Service_Task__c st = new Service_Task__c(Name='ServiceTask1',Description__c = 'Description03',Business_Segment__c='B1-BU1',Service_Function__c = Sf.Id);
        insert st;
        System.debug('----st--------'+st);
        
        Bill_Formula_Names__c siqn = new Bill_Formula_Names__c(Name='bill formula name1',Description__c='test descri');
        insert siqn;
        System.debug('----st--------'+siqn);
        
        Service_Task_To_Bill_Formula__c sttsi = new Service_Task_To_Bill_Formula__c(Bill_Formula_Names__c=siqn.id,Service_Task__c = st.id);
        insert sttsi;
        System.debug('----st--------'+sttsi);
        
        delete sttsi;
        
        Service_Task_To_Bill_Formula__c sttsi1 = new Service_Task_To_Bill_Formula__c(Bill_Formula_Names__c=siqn.id,Service_Task__c = st.id);
        insert sttsi1;
        System.debug('----st--------'+sttsi1);
        
        smm.status__c = 'Approved';
        upsert smm;
        System.debug('----smm-Approved-------'+smm);
        
        try{
        delete sttsi1;
        }
        catch(Exception e) {
                system.assert(e.getMessage().contains('UNABLE TO DELETE RECORD BECAUSE SERVICE MODEL IS APPROVED/RETIRED!'));
            }
        
    }
}