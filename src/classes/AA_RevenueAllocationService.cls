public class AA_RevenueAllocationService{

    public static List<Business_Unit__c> getBusinessUnitCodes()
    {
        return AA_RevenueAllocationDataAccessor.getBuCodes();
    }
    
    public static List<Function_Code__c> getFunctionCodes()
    {
        return AA_RevenueAllocationDataAccessor.getFCodes();
    }
    
    public static List<Country__c> getCountries()
    {
        return AA_RevenueAllocationDataAccessor.getCountries();
    }
    public static List<AggregateResult> getValidYears(){
        return AA_RevenueAllocationDataAccessor.getValidYears();
    }
    public static BUF_Code__c getBufCodeId(String name){
        return AA_RevenueAllocationDataAccessor.getBufCodeId(name);
    }
    
    public static Currency__c getCurrencyId(String name){
        return AA_RevenueAllocationDataAccessor.getCurrencyId(name);
    }
    
    public static Revenue_Allocation_Rate__c checkExistingRecord(BUF_Code__c function, String year){
        return AA_RevenueAllocationDataAccessor.checkExistingRecord(function,year);
    }
    
    public static List<Revenue_Allocation_Rate__c> checkExisting(String function, String year){
        return AA_RevenueAllocationDataAccessor.checkExisting(function, year);
    }
    
    public static List<Revenue_Allocation_Rate__c> getRateDetails(String Id){
        return AA_RevenueAllocationDataAccessor.getRateDetails(Id);
    }
    
    public static Revenue_Allocation_Rate__c getRarId(){
        return AA_RevenueAllocationDataAccessor.getRARId();
    }
    public static List<Revenue_Allocation_Rate__c> getRecordsByFilter(String year,String bucode,String functioncode,String country,String active){
         return AA_RevenueAllocationDataAccessor.getRecordsByFilter(year,bucode,functioncode,country,active);
      }
}