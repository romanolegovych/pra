/**
* @author Devaram Bhargav
* @date 06/30/2015
* @description this is test class for PBB_OBIEE_ReportsCntrl  class
*/

 @istest(seealldata=true)
public with sharing class PBB_OBIEE_ReportsCntrlTest {

    //test method for save
    public static testMethod void testPBB_OBIEE_ReportsCntrl ()  {
        test.startTest();       
        PBB_OBIEE_ReportsCntrl  obiee = new PBB_OBIEE_ReportsCntrl ();
        obiee.endpoint='SampleProjectID==SampleGraphType';
        obiee.generateURL();
        test.stopTest();
    }   
}