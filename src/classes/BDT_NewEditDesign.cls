public with sharing class BDT_NewEditDesign {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // clinical design
	public boolean showDragAndDrop {get;set;}
	public String projectId 							{get;set;}
	public String designId								{get;set;}
	public ClinicalDesign__c design						{get;set;}
	public List<Arm__c> armList							{get;set;}
	public List<Epoch__c> epochList 					{get;set;}
	public List<Flowcharts__c> flowchartList 			{get;set;}
	public List<Arm__c> armTODELETEList					{get;set;}
	public List<Epoch__c> epochTODELETEList 			{get;set;}
	public List<Flowcharts__c> flowchartTODELETEList	{get;set;}
	
	public String Jsondata 								{get;set;}
	public List<flowchartassignment__c> fcaList;
	
	public BDT_NewEditDesign(BDT_Design controller){
		//initializeNewDesign();
	}
	
	public BDT_NewEditDesign(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Clinical Design');
		initializeNewEditDesign();
		if (armList.size()>0&&armList[0].id!=null&&
			epochList.size()>0&&epochList[0].id!=null&&
			flowchartList.size()>0&&flowchartList[0].id!=null&&
			designId!=null&&designId!='') {
			
			showDragAndDrop = true;
		} else {
			showDragAndDrop = false;
		}
	}
	
	public void initializeNewEditDesign(){
		
		projectId = BDT_Utils.getPreference('SelectedProject');
		designId = System.CurrentPageReference().getParameters().get('designId'); 
				
		if(designId!=null && designId!=''){			
			design = BDT_Utils.getDesign(designId, projectId);	
			
		}else{
			design = new ClinicalDesign__c(Project__c=projectId);
		}
		
		getDesignRelatedItems();
	}
	
	
	public void getDesignRelatedItems(){

		armTODELETEList = new List<Arm__c>();
		epochTODELETEList = new List<Epoch__c>();
		flowchartTODELETEList = new List<Flowcharts__c>();
		
		/* Load flowchart assignment */
		JSONload();
		
		try{
			armList = [Select a.Id, a.ClinicalDesign__c, a.Description__c, a.Arm_Number__c, a.CreatedDate From Arm__c a where a.ClinicalDesign__c = :design.id order by Arm_Number__c];
		}catch(Exception e){
			armList = new List<Arm__c>();
		}
		
		try{
			epochList = [Select e.Id, e.Epoch_Number__c, e.Description__c, e.Duration_days__c, e.ClinicalDesign__c, e.CreatedDate From Epoch__c e where e.ClinicalDesign__c = :design.id order by  Epoch_Number__c];
		}catch(Exception e){
			epochList = new List<Epoch__c>();
		}
		
		try{
			flowchartList = [Select f.Id, f.Flowchart_Number__c, f.ClinicalDesign__c, f.Description__c, f.CreatedDate From Flowcharts__c f where f.ClinicalDesign__c = :design.id order by Flowchart_Number__c];
		}catch(Exception e){
			flowchartList = new List<Flowcharts__c>();
		}
		
		if(armList.isEmpty()){
			addArm();
		}
		
		if(epochList.isEmpty()){
			addEpoch();
		}
		
		if(flowchartList.isEmpty()){
			addFlowchart();
		}
		
		Decimal val=1;
		for (Arm__c a:armList) {
			a.Arm_Number__c = val; 
			val++; 
		}
		val=1;
		for (Epoch__c a:epochList) {
			a.EPOCH_Number__c = val; 
			val++; 
		}
		val=1;
		for (Flowcharts__c a:flowchartList) {
			a.Flowchart_Number__c = val; 
			val++; 
		}
	
	}
	
	public void renumberArm(){
		armList = BDT_Utils.sortListNumeric(armList, 'Arm_Number__c');
		Decimal val=1;
		for (Arm__c a:armList) {
			a.Arm_Number__c = val; 
			val++; 
		}
	}		
	public void addArm(){
		renumberArm();
		armList.add(new Arm__c(Description__c='',Arm_Number__c=armList.size()+1));/*, ClinicalDesign__c=design.id*/
		showDragAndDrop = false;
	}
	
	public void renumberEpoch(){
		epochList = BDT_Utils.sortListNumeric(epochList, 'Epoch_Number__c');
		Decimal val=1;
		for (Epoch__c a:epochList) {
			a.Epoch_Number__c = val; 
			val++; 
		}
	}	
	
	public void addEpoch(){
		renumberEpoch();
		epochList.add(new Epoch__c(Description__c='',Epoch_Number__c = epochList.size()+1));
		showDragAndDrop = false;
	}
	
	public void renumberFlowchart(){
		flowchartList = BDT_Utils.sortListNumeric(flowchartList, 'Flowchart_Number__c');
		Decimal val=1;
		for (Flowcharts__c a:flowchartList) {
			a.Flowchart_Number__c = val; 
			val++; 
		}
	}
	
	public void addFlowchart(){
		renumberFlowchart();
		flowchartList.add(new Flowcharts__c(Description__c='',Flowchart_Number__c = flowchartList.size()+1));
		showDragAndDrop = false;
	}
	
	public void deleteArm(){
		String deleteArmId =  System.CurrentPageReference().getParameters().get('deleteArmId');

		for(Integer i=0; i<armList.size();i++){
			if(armList[i].id == deleteArmId){
				armTODELETEList.add(armList.remove(i));
			}
		}
		showDragAndDrop = false;
		
	}
	
	public void deleteEpoch(){
		
		String deleteEpochId =  System.CurrentPageReference().getParameters().get('deleteEpochId');
		
		for(Integer i=0; i<epochList.size();i++){
			if(epochList[i].id == deleteEpochId){
				epochTODELETEList.add(epochList[i]);
				epochList.remove(i);
			}
		}
		showDragAndDrop = false;
	}
	
	public void deleteFlowchart(){
		
		String deleteFlowchartId =  System.CurrentPageReference().getParameters().get('deleteFlowchartId');
		
		for(Integer i=0; i<flowchartList.size();i++){
			if(flowchartList[i].id == deleteFlowchartId){
				flowchartTODELETEList.add(flowchartList[i]);
				flowchartList.remove(i);
			}
		}
		showDragAndDrop = false;
		
	}
	
	public PageReference save(){
		
		PageReference u = updatedata();
		return cancel();
		
	}
	
	
	public PageReference updatedata(){
		upsert design;		
		List<Arm__c>        saveArmList       = new List<Arm__c>();
		List<Epoch__c>      saveEpochList     = new List<Epoch__c>();
		List<Flowcharts__c> saveFlowchartList = new List<Flowcharts__c>();		
		for(Arm__c tmp : armList) {
			if(tmp.Description__c!=null && tmp.Description__c!=''){
				tmp.ClinicalDesign__c = design.id;
				saveArmList.add(tmp);
			}
		}
		for(Epoch__c tmp : epochList) {
			if(tmp.Description__c!=null && tmp.Description__c!=''){
				tmp.ClinicalDesign__c = design.id;
				saveEpochList.add(tmp);
			}
		}
		for(Flowcharts__c tmp : flowchartList) {
			if(tmp.Description__c!=null && tmp.Description__c!=''){
				tmp.ClinicalDesign__c = design.id;
				saveFlowchartList.add(tmp);
			}
		}
		renumberArm();
		renumberEpoch();
		renumberFlowchart();
		upsert saveArmList;
		upsert saveEpochList;
		upsert saveFlowchartList;
		
		if(!armTODELETEList.isEmpty()){
			delete [select id from grouparmassignment__c where arm__c = :armTODELETEList];
			delete [select id from flowchartassignment__c where arm__c = :armTODELETEList];
			delete armTODELETEList;
		}
		
		if(!epochTODELETEList.isEmpty()){
			delete [select id from flowchartassignment__c where epoch__c = :epochTODELETEList];
			delete epochTODELETEList;
		}
		
		if(!flowchartTODELETEList.isEmpty()){
			delete [select id from projectservicetimepoint__c where flowchart__c = :flowchartTODELETEList];
			delete [select id from timepoint__c where flowchart__c = :flowchartTODELETEList];
			delete [select id from flowchartassignment__c where flowchart__c = :flowchartTODELETEList];
			delete flowchartTODELETEList;
		}
		
		// save the drag-n-drop design
		JSONsave();
		
		PageReference newEditDesignPage = new PageReference(System.Page.bdt_neweditdesign.getUrl());
		newEditDesignPage.getParameters().put('designId',design.id); 		
		newEditDesignPage.setRedirect(true);
		return newEditDesignPage;
	}
	
	public PageReference cancel(){ 
		PageReference designPage = new PageReference(System.Page.bdt_Design.getUrl());
		designPage.getParameters().put('designId',design.id); 		
		designPage.setRedirect(true);
		return designPage;
	}
	
	public PageReference copy(){ 
		PageReference copyDesign = new PageReference(System.Page.BDT_NewEditDesignCopy.getUrl());		
		if(designId!='' && designId!=null){//this is only available when creating a new project
			copyDesign.getParameters().put('designId', design.id);
		}
		copyDesign.setRedirect(true);
		return copyDesign;
		
	}
	
	public PageReference deleteD(){

		try{
			
			delete [select id 
					from Flowchartassignment__c
					where Flowchart__r.ClinicalDesign__c = :design.id];
			
			delete design; 
		}catch(DMLException e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'All related design items MUST be deleted before deleting a Design. ');
	        ApexPages.addMessage(msg);  
	        return null;
		}
		PageReference designPage = new PageReference(System.Page.bdt_design.getUrl()); 
		designPage.setRedirect(true);
		return designPage;
	}
	
	
	public void JSONsave() {
		try {
			String keyValue;
	
			// read JSON and make it a list
			List<flowchartassignment__c> fcaJSON = (List<flowchartassignment__c>) JSON.deserialize(Jsondata, List<flowchartassignment__c>.class);
			
			// create a map for matching the data to the database
			Map<String,flowchartassignment__c> JSONmap = new Map<String,flowchartassignment__c>();
			for (flowchartassignment__c fca:fcaJSON){
				keyValue = fca.Arm__c+'-'+fca.epoch__c;
				JSONmap.put(keyValue,fca);
			}
			
			// get database
			List<flowchartassignment__c> fcaSFC = ReadFlowChart();
			
			// update records
			for (flowchartassignment__c fca:fcaSFC){
				keyValue = fca.Arm__c+'-'+fca.epoch__c;
				if (JSONmap.containsKey(keyValue)) {
					try {fca.Flowchart__c = JSONmap.get(keyValue).Flowchart__c;
					}catch(Exception e){}
					
					// unload item from map, remaining items will be new items that need to be inserted
					JSONmap.remove(keyValue);
				}
			}
			
			try{upsert fcaSFC;}catch(Exception e){}
			
			// remaining items are new and should be inserted
			try{insert JSONmap.values();}catch(Exception e){}
		
		}catch(Exception e){}		
		
	}
	
	public void JSONload() {
		try {
			List<flowchartassignment__c> fca =  [select arm__c, epoch__c, flowchart__c from flowchartassignment__c where arm__r.ClinicalDesign__c = :designId];
			Jsondata = JSON.serialize(fca);
		} catch (Exception e) {Jsondata = '';}
	}
	
	public List<flowchartassignment__c> ReadFlowChart() {
		
		List<flowchartassignment__c> fca = new List<flowchartassignment__c>();
		
		try {
			fca = [select id,epoch__c,arm__c,flowchart__c from flowchartassignment__c where arm__r.ClinicalDesign__c = :designId];
		}
		catch (Exception e) {}
		
		return fca;
		
		
	}

}