/**
@author 
@date 2014
@description this controller used to clone
**/
public with sharing class SRMModelCloneController{  
    
    //Member variables
    String srmModelId='';
    public SRM_Model__c srmModelObj{get;set;} 
    public SRM_Model__c srmModelObjClone{get;set;} 
    public String returnUrl{get;set;}
       
    public SRMModelCloneController(ApexPages.standardController stdctrl){           
        srmModelObj=new SRM_Model__c();          
        srmModelId =ApexPages.CurrentPage().getParameters().get('Id');        
        returnUrl = ApexPages.CurrentPage().getParameters().get('retUrl');
        String clone = ApexPages.CurrentPage().getParameters().get('clone');
        
        if(srmModelId!=null && srmModelId!=''){
            String srmmodelquery=SRM_ModelCloneDataAccessor.ListofSRMModels(srmModelId);
            srmModelObj=database.query(srmmodelquery);
        }
        if(!String.isEmpty(clone)){
                          
             SRM_Model__c temp = [select Id, Status__c, Comments__c from SRM_Model__c where Id =: srmModelId limit 1 ];
             srmModelObjClone = temp.clone(false,true);
             srmModelObjClone.Status__c = 'In Progress';
             srmModelObjClone.Comments__c = temp.Comments__c;                                                              
        }   
    }
    
     /**
@author 
@date 2014
@description Method to save
**/
    //Method to save
    public pagereference doSRMClone(){
        String newSRMId=SRM_ModelSiteActivationSevice.doClone(srmModelObj);
        if(newSRMId!=null && newSRMId!='')
            return new pagereference('/'+newSRMId);
        else
            return null;
    
    }
    
     /**
@author 
@date 2014
@description Method to save and new
**/
    //Method to save and new
    public pagereference SaveandNew(){
        String newSRMId=SRM_ModelSiteActivationSevice.doClone(srmModelObj);
        if(newSRMId!=null && newSRMId!='')
            return new pagereference('/'+ String.valueof(newSRMId).substring(0,3)+'/e?retURL=%2F'+String.valueof(newSRMId).substring(0,3)+'%2Fo');
        else
            return null; 
        
    }
    
     /**
@author 
@date 2014
@description Method to return
**/
    //Method to return
    public pagereference Cancel(){
        return new pagereference('/'+srmModelId);
    }

}