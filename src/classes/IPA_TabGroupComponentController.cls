public class IPA_TabGroupComponentController
{       
    public string widgetId {get; set;}
    public IPA_Page_Widget__c pageWidgetObj { get; set; }
    public string renderTabs {get; set;}
    
    public List<IPA_Page_Widget__c> getlstWidgets()
    {   
        IPA_BO_LinksManagement bo = new IPA_BO_LinksManagement();
        List<IPA_Page_Widget__c> lstWidgets = bo.returnChildWidgets(pageWidgetObj);
        if(lstWidgets.size() > 0)
            renderTabs = '';
        else
            renderTabs = 'no';
        return lstWidgets;
    }
        
    /*public Component.Apex.OutputPanel getChildTabGroupWidgets() 
    {           
        Component.Apex.OutputPanel op = new Component.Apex.OutputPanel();
        
        Component.c.IPA_QuickLinksComponent com = new Component.c.IPA_QuickLinksComponent();
        op.childComponents.add(com);
        compCnt = compCnt + 1;
        
        Component.c.IPA_TimeZoneComponent com2 = new Component.c.IPA_TimeZoneComponent();
        op.childComponents.add(com2);
        compCnt = compCnt + 1;
        
        //return factory.createOutputPanelForTabGroup(pageWidgetObj);
        return op;
    }*/
}