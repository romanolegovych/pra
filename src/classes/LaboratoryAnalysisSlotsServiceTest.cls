/** Implements the test for the Service Layer of the object LaboratoryAnalysisSlots__c
 * @author	Dimitrios Sgourdos
 * @version	26-Nov-2013
 */
@isTest
private class LaboratoryAnalysisSlotsServiceTest {
	
	// Global variables
	private static List<Study__c>	studiesList;
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	06-Nov-2013
	 */
	static void init(){
		// Create project
		Client_Project__c firstProject = BDT_TestDataUtils.buildProject(); 
		insert firstProject;
		
		// Create studies
		studiesList = BDT_TestDataUtils.buildStudies(firstProject,3);
		insert studiesList;
	}
	
	
	/** Test the function getLabSlotTotalNumber for sub-class LaboratorySlotsDesignWrapper
	 * @author	Dimitrios Sgourdos
	 * @version	19-Nov-2013
	 */
	static testMethod void getLabSlotTotalNumberTest() {
		LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper wrapperItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		system.assertEquals(wrapperItem.labSlot, NULL);
		
		String result = wrapperItem.getLabSlotTotalNumber();
		system.assertNotEquals(wrapperItem.labSlot, NULL);
		system.assertEquals(result, '0');
		
		wrapperItem.labSlot.ActiveNumberOfSamples__c  = 1000;
		wrapperItem.labSlot.PlaceboNumberOfSamples__c = 2000;
		wrapperItem.labSlot.BackupNumberOfSamples__c  = 3000;
		wrapperItem.labSlot.OtherNumberOfSamples__c   = 4000;
		
		result = wrapperItem.getLabSlotTotalNumber();
		system.assertEquals(result, '10,000');
	}
	
	
	/** Test the function getLabSlotListOfShipments for sub-class LaboratorySlotsDesignWrapper
	 * @author	Dimitrios Sgourdos
	 * @version	26-Nov-2013
	 */
	static testMethod void getLabSlotListOfShipmentsTest() {
		LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper wrapperItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		system.assertEquals(wrapperItem.labSlot, NULL);
		
		List<String> results = wrapperItem.getLabSlotListOfShipments();
		system.assertEquals(results.size(), 0);
		
		wrapperItem.labSlot = new LaboratoryAnalysisSlots__c(ListOfShipments__c = '32:01:23');
		results = wrapperItem.getLabSlotListOfShipments();
		system.assertEquals(results.size(), 3);
		system.assertEquals(results[0], 'Ship-1');
		system.assertEquals(results[1], 'Ship-23');
		system.assertEquals(results[2], 'Ship-32');
	}
	
	
	/** Test the function getLaboratorySlotsForLabDesigns
	 * @author	Dimitrios Sgourdos
	 * @version	11-Nov-2013
	 */
	static testMethod void getLaboratorySlotsForLabDesignsTest() {
		// Create data
		init();
		
		LaboratoryMethod__c labMethod = new LaboratoryMethod__c (
												Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
												Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood',
												Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle');
		insert labMethod;
		
		List<LaboratoryAnalysisSlots__c> initialList = new List<LaboratoryAnalysisSlots__c>();
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=1, SequenceNumber__c=2, MdValSlot__c=false,Study__c=studiesList[0].Id, LaboratoryMethod__c=labMethod.Id));
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=1, SequenceNumber__c=1, MdValSlot__c=true, Study__c=studiesList[1].Id, LaboratoryMethod__c=labMethod.Id));
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=2, SequenceNumber__c=1, MdValSlot__c=true, Study__c=studiesList[0].Id, LaboratoryMethod__c=labMethod.Id));
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=1, SequenceNumber__c=3, MdValSlot__c=true, Study__c=studiesList[2].Id, LaboratoryMethod__c=labMethod.Id));
		insert initialList;
		
		// Check the function
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> results = LaboratoryAnalysisSlotsService.getLaboratorySlotsForLabDesigns(
																														studiesList, 
																														true
																					);
		system.assertEquals(results.size(), 3);
		system.assertEquals(results[0].labSlot.Id, initialList[1].Id);
		system.assertEquals(results[0].slotNumber,  '1');
		system.assertEquals(results[0].studyMethodCombination, initialList[1].Study__c + ':' + initialList[1].LaboratoryMethod__c);
		system.assertEquals(results[1].labSlot.Id, initialList[3].Id);
		system.assertEquals(results[1].slotNumber,  '1');
		system.assertEquals(results[1].studyMethodCombination, initialList[3].Study__c + ':' + initialList[3].LaboratoryMethod__c);
		system.assertEquals(results[2].labSlot.Id, initialList[2].Id);
		system.assertEquals(results[2].slotNumber,  '2');
		system.assertEquals(results[2].studyMethodCombination, initialList[2].Study__c + ':' + initialList[2].LaboratoryMethod__c);
		
		results = LaboratoryAnalysisSlotsService.getLaboratorySlotsForLabDesigns(studiesList, false);
		system.assertEquals(results.size(), 1);
		system.assertEquals(results[0].labSlot.Id, initialList[0].Id);
		system.assertEquals(results[0].slotNumber,  '1');
		system.assertEquals(results[0].studyMethodCombination, initialList[0].Study__c + ':' + initialList[0].LaboratoryMethod__c);
	}
	
	
	/** Test the function countSlotsInLabSlotsFoMdAndValDesignWrapperList
	 * @author	Dimitrios Sgourdos
	 * @version	07-Nov-2013
	 */
	static testMethod void countSlotsInLabSlotsFoMdAndValDesignWrapperListTest() {
		// Create data
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> slotsWrapperList = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNumber__c=1);
		slotsWrapperList.add(newItem);
		
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNumber__c=1);
		slotsWrapperList.add(newItem);
		
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNumber__c=7);
		slotsWrapperList.add(newItem);
		
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNumber__c=2);
		slotsWrapperList.add(newItem);
		
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNumber__c=5);
		slotsWrapperList.add(newItem);
		
		// Four different slot numbers
		Integer result = LaboratoryAnalysisSlotsService.countSlotsInLabSlotsFoMdAndValDesignWrapperList(slotsWrapperList);
		system.assertEquals(result, 4);
	}
	
	
	/** Test the function createLaboratorySlotsDesignWrapperInstance
	 * @author	Dimitrios Sgourdos
	 * @version	12-Nov-2013
	 */
	static testMethod void createLaboratorySlotsDesignWrapperInstanceTest() {
		LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper result = LaboratoryAnalysisSlotsService.createLaboratorySlotsDesignWrapperInstance(
																															'1',
																															true
																			);
		system.assertNotEquals(result, null);
		system.assertEquals(result.slotNumber, '1');
		system.assertEquals(result.labSlot.slotNumber__c, 1);
		system.assertEquals(result.studyMethodCombination, NULL);
	}
	
	
	/** Test the function addSlotToLabDesign
	 * @author	Dimitrios Sgourdos
	 * @version	12-Nov-2013
	 */
	static testMethod void addSlotToLabDesignTest() {
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> results = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		
		// Add slot
		results = LaboratoryAnalysisSlotsService.addSlotToLabDesign(results, true);
		system.assertEquals(results.size(), 1);
		system.assertEquals(results[0].labSlot.SlotNumber__c, 1);
		
		// Add second slot
		results = LaboratoryAnalysisSlotsService.addSlotToLabDesign(results, true);
		system.assertEquals(results.size(), 2);
		system.assertEquals(results[0].labSlot.SlotNumber__c, 1);
		system.assertEquals(results[1].labSlot.SlotNumber__c, 2);
	}
	
	
	/** Test the function addSlotCombinationLabDesign
	 * @author	Dimitrios Sgourdos
	 * @version	12-Nov-2013
	 */
	static testMethod void addSlotCombinationLabDesignTest() {
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> results = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		results.add(LaboratoryAnalysisSlotsService.createLaboratorySlotsDesignWrapperInstance('1', true));
		results.add(LaboratoryAnalysisSlotsService.createLaboratorySlotsDesignWrapperInstance('2', true));
		results[0].studyMethodCombination = 'Test 1';
		results[1].studyMethodCombination = 'Test 2';
		
		// Use illegal indexes
		results = LaboratoryAnalysisSlotsService.addSlotCombinationLabDesign(results, -1, true);
		system.assertEquals(results.size(), 2);
		results = LaboratoryAnalysisSlotsService.addSlotCombinationLabDesign(results, 2, true);
		system.assertEquals(results.size(), 2);
		
		// Add wrapper record to the first slot
		results = LaboratoryAnalysisSlotsService.addSlotCombinationLabDesign(results, 0, true);
		system.assertEquals(results.size(), 3);
		system.assertEquals(results[0].labSlot.SlotNumber__c, 1);
		system.assertEquals(results[0].studyMethodCombination, 'Test 1');
		system.assertEquals(results[1].labSlot.SlotNumber__c, 1);
		system.assertEquals(results[2].labSlot.SlotNumber__c, 2);
		system.assertEquals(results[2].studyMethodCombination, 'Test 2');
		
		// Add again wrapper record to the first shipment
		results[1].studyMethodCombination = 'Test 1 new a';
		results = LaboratoryAnalysisSlotsService.addSlotCombinationLabDesign(results, 0, true);
		system.assertEquals(results.size(), 4);
		system.assertEquals(results[0].labSlot.SlotNumber__c, 1);
		system.assertEquals(results[0].studyMethodCombination, 'Test 1');
		system.assertEquals(results[1].labSlot.SlotNumber__c, 1);
		system.assertEquals(results[1].studyMethodCombination, 'Test 1 new a');
		system.assertEquals(results[2].labSlot.SlotNumber__c, 1);
		system.assertEquals(results[3].labSlot.SlotNumber__c, 2);
		system.assertEquals(results[3].studyMethodCombination, 'Test 2');
		
		// Add to the second shipment number
		results = LaboratoryAnalysisSlotsService.addSlotCombinationLabDesign(results, 3, true);
		system.assertEquals(results.size(), 5);
		for(Integer i=0; i<3; i++) {
			system.assertequals(results[i].labSlot.SlotNumber__c, 1);
		}
		system.assertEquals(results[3].studyMethodCombination, 'Test 2');
		system.assertEquals(results[3].labSlot.SlotNumber__c, 2);
		system.assertequals(results[4].labSlot.SlotNumber__c, 2);
	}
	
	
	/** Test the function removeSlotCombinationFromLabDesign
	 * @author	Dimitrios Sgourdos
	 * @version	12-Nov-2013
	 */
	static testMethod void removeSlotCombinationFromLabDesignTest() {
		// Create a dummy list
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> results = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNUmber__c=1, DaysMdOrVal__c=10);
		newItem.slotNumber = '1';
		results.add(newItem);
		
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNUmber__c=2, DaysMdOrVal__c=20);
		newItem.slotNumber = '2';
		results.add(newItem);
		
		// Check the function with not valid indexes
		results = LaboratoryAnalysisSlotsService.removeSlotCombinationFromLabDesign(results,-1);
		system.assertEquals(results.size(), 2);
		results = LaboratoryAnalysisSlotsService.removeSlotCombinationFromLabDesign(results,2);
		system.assertEquals(results.size(), 2);
		
		// Remove the first wrapper Item from the list
		system.assertEquals(results[0].SlotNumber, '1');
		results = LaboratoryAnalysisSlotsService.removeSlotCombinationFromLabDesign(results,0);
		system.assertEquals(results.size(), 1);
		system.assertEquals(results[0].SlotNumber, '2');
		
		// Check if the inserted slot number is transfered to the second child in case we delete the first child of a slot
		results[0].slotNumber = '8';
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNUmber__c=2, DaysMdOrVal__c=30);
		newItem.slotNumber = '2';
		results.add(newItem);
		results = LaboratoryAnalysisSlotsService.removeSlotCombinationFromLabDesign(results,0);
		system.assertEquals(results.size(), 1);
		system.assertEquals(results[0].SlotNumber, '8');
		system.assertEquals(results[0].labSlot.DaysMdOrVal__c, 30);
	}
	
	
	/** Test the function validateLabSlotsMdAndValDesign
	 * @author	Dimitrios Sgourdos
	 * @version	08-Nov-2013
	 */
	static testMethod void validateLabSlotsMdAndValDesignTest() {
		// Create a dummy list
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> wrapperList = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.LabSlot = new LaboratoryAnalysisSlots__c();
		newItem.slotNumber = '1';
		newItem.studyMethodCombination = 'Test combination 1';
		wrapperList.add(newItem);
		
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.LabSlot = new LaboratoryAnalysisSlots__c();
		newItem.slotNumber = '2';
		newItem.studyMethodCombination = 'Test combination 2';
		wrapperList.add(newItem);
		
		// Validate without Slot Type selection
		Boolean result = LaboratoryAnalysisSlotsService.validateLabSlotsMdAndValDesign(wrapperList);
		system.assertEquals(result, false);
		
		// Validate without studyMethodCombination
		for(LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper tmpItem : wrapperList) {
			tmpItem.studyMethodCombination = '';
			tmpItem.labSlot.SlotType__c = 'Feasibility';
		}
		result = LaboratoryAnalysisSlotsService.validateLabSlotsMdAndValDesign(wrapperList);
		system.assertEquals(result, false);
		
		// Validate with everything inserted
		for(LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper tmpItem : wrapperList) {
			tmpItem.studyMethodCombination = 'Test Combination';
		}
		result = LaboratoryAnalysisSlotsService.validateLabSlotsMdAndValDesign(wrapperList);
		system.assertEquals(result, true);
	}
	
	
	/** Test the function adjustSlotsInSlotsWrapperListForLabDesigns
	 * @author	Dimitrios Sgourdos
	 * @version	13-Nov-2013
	 */
	static testMethod void adjustSlotsInSlotsWrapperListForLabDesignsTest() {
		// Create dummy IDs
		List<Account> acc = new List<Account>();
		acc.add(new Account(Name = 'A',BillingCountry='NL'));
		acc.add(new Account(Name = 'B',BillingCountry='NL'));
		acc.add(new Account(Name = 'C',BillingCountry='US'));
		acc.add(new Account(Name = 'D',BillingCountry='US'));
		acc.add(new Account(Name = 'E',BillingCountry='US'));
		acc.add(new Account(Name = 'F',BillingCountry='US'));
		insert acc;
		
		// Create a dummy list
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> results = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		
		// First Added slot
		LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '1';
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNumber__c=2);
		newItem.studyMethodCombination = acc[0].Id + ':' + acc[1].Id;
		results.add(newItem);
		
		// Second added slot
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '2';
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNumber__c=2);
		newItem.studyMethodCombination = acc[2].Id + ':' + acc[3].Id;
		results.add(newItem);
		
		// Third added slot
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '3';
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNumber__c=4);
		newItem.studyMethodCombination = acc[4].Id + ':' + acc[5].Id;
		results.add(newItem);
		
		// Check the function
		results = LaboratoryAnalysisSlotsService.adjustSlotsInSlotsWrapperListForLabDesigns(results);
		system.assertEquals(results[0].slotNumber, '1');
		system.assertEquals(results[0].labSlot.SequenceNumber__c, 1);
		system.assertEquals(results[0].labSlot.Study__c, acc[0].Id);
		system.assertEquals(results[0].labSlot.LaboratoryMethod__c, acc[1].Id);
		system.assertEquals(results[1].slotNumber, '1');
		system.assertEquals(results[1].labSlot.SequenceNumber__c, 2);
		system.assertEquals(results[1].labSlot.Study__c, acc[2].Id);
		system.assertEquals(results[1].labSlot.LaboratoryMethod__c, acc[3].Id);
		system.assertEquals(results[2].slotNumber, '3');
		system.assertEquals(results[2].labSlot.SequenceNumber__c, 1);
		system.assertEquals(results[2].labSlot.Study__c, acc[4].Id);
		system.assertEquals(results[2].labSlot.LaboratoryMethod__c, acc[5].Id);
	}
	
	
	/** Test the function sortSlotsDesignWrapperBySlotNumber
	 * @author	Dimitrios Sgourdos
	 * @version	08-Nov-2013
	 */
	static testMethod void sortSlotsDesignWrapperBySlotNumberTest() {
		// Create a dummy list
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> results = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		
		// First Added slot
		LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '3';
		newItem.studyMethodCombination = 'First added';
		results.add(newItem);
		
		// Second added slot
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '3';
		newItem.studyMethodCombination = 'Second added';
		results.add(newItem);
		
		// Third added slot
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '2';
		newItem.studyMethodCombination = 'Third added';
		results.add(newItem);
		
		// check the function
		results = LaboratoryAnalysisSlotsService.sortSlotsDesignWrapperBySlotNumber(results);
		system.assertEquals(results[0].studyMethodCombination, 'Third added');
		system.assertEquals(results[0].slotNumber, '1');
		system.assertEquals(results[1].studyMethodCombination, 'First added');
		system.assertEquals(results[1].slotNumber, '2');
		system.assertEquals(results[2].studyMethodCombination, 'Second added');
		system.assertEquals(results[2].slotNumber, '3');
	}
	
	
	/** Test the function extractLabAnalysisSlotsFromLabDesign
	 * @author	Dimitrios Sgourdos
	 * @version	08-Nov-2013
	 */
	static testMethod void extractLabAnalysisSlotsFromLabDesignTest() {
		// Create a dummy list with mixed inserted slotNumbers
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> initialWrapperList = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		
		// First added combination (first combination of slot 5)
		LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '3';
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNumber__c=5, DaysMdOrVal__c=10);
		initialWrapperList.add(newItem);
		
		// Second added combination (second combination of slot 5)
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '5';
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNumber__c=5, DaysMdOrVal__c=20);
		initialWrapperList.add(newItem);
		
		// Third added combination (first combination of slot 3)
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '3';
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNumber__c=3, DaysMdOrVal__c=30);
		initialWrapperList.add(newItem);
		
		// Fourth added combination (first combination of slot 4)
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '2';
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNumber__c=4, DaysMdOrVal__c=40);
		initialWrapperList.add(newItem);
		
		// Fifth added combination (second combination of slot 4)
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '4';
		newItem.labSlot = new LaboratoryAnalysisSlots__c(SlotNumber__c=4, DaysMdOrVal__c=50);
		initialWrapperList.add(newItem);
		
		// check the function
		List<LaboratoryAnalysisSlots__c> results = new List<LaboratoryAnalysisSlots__c>();
		results = LaboratoryAnalysisSlotsService.extractLabAnalysisSlotsFromLabDesign(initialWrapperList);
		system.assertEquals(results.size(),5);
		system.assertEquals(results[0].SlotNumber__c, 1);
		system.assertEquals(results[0].DaysMdOrVal__c, 40);
		system.assertEquals(results[1].SlotNumber__c, 1);
		system.assertEquals(results[1].DaysMdOrVal__c, 50);
		system.assertEquals(results[2].SlotNumber__c, 2);
		system.assertEquals(results[2].DaysMdOrVal__c, 10);
		system.assertEquals(results[3].SlotNumber__c, 2);
		system.assertEquals(results[3].DaysMdOrVal__c, 20);
		system.assertEquals(results[4].SlotNumber__c, 3);
		system.assertEquals(results[4].DaysMdOrVal__c, 30);
	}
	
	
	/** Test the function saveLaboratoryAnalysisSlots
	 * @author	Dimitrios Sgourdos
	 * @version	08-Nov-2013
	 */
	static testMethod void saveLaboratoryAnalysisSlotsTest() {
		// Create data
		init();
		
		LaboratoryMethod__c labMethod = new LaboratoryMethod__c (
												Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
												Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood',
												Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle');
		insert labMethod;
		
		List<LaboratoryAnalysisSlots__c> initialList = new List<LaboratoryAnalysisSlots__c>();
		initialList.add(new LaboratoryAnalysisSlots__c(SequenceNumber__c=1, MdValSlot__c=true, Study__c=studiesList[0].Id, LaboratoryMethod__c=labMethod.Id));
		initialList.add(new LaboratoryAnalysisSlots__c(SequenceNumber__c=2, MdValSlot__c=true, Study__c=studiesList[1].Id, LaboratoryMethod__c=labMethod.Id));
		
		// Check the function without giving a SlotNumber(that will create an exception)
		Boolean resultFlag = LaboratoryAnalysisSlotsService.saveLaboratoryAnalysisSlots(initialList);
		system.assertEquals(resultFlag, false);
		List<LaboratoryAnalysisSlots__c> retrievedList = [SELECT Id FROM LaboratoryAnalysisSlots__c];
		system.assertEquals(retrievedList.size(), 0);
		
		// Check the function with giving a slot number
		for(LaboratoryAnalysisSlots__c tmpSlot : initialList) {
			tmpSlot.SlotNumber__c = 1;
		}
		
		resultFlag = LaboratoryAnalysisSlotsService.saveLaboratoryAnalysisSlots(initialList);
		system.assertEquals(resultFlag, true);
		
		retrievedList = [SELECT Id, SlotNumber__c, SequenceNumber__c
						 FROM	LaboratoryAnalysisSlots__c
						 ORDER BY SlotNumber__c, SequenceNumber__c];
						 
		system.assertEquals(retrievedList.size(), 2);
		system.assertEquals(retrievedList[0].Id, initialList[0].Id);
		system.assertEquals(retrievedList[1].Id, initialList[1].Id); 
	}
	
	
	/** Test the function deleteLaboratoryAnalysisSlotsTest
	 * @author	Dimitrios Sgourdos
	 * @version	08-Nov-2013
	 */
	static testMethod void deleteLaboratoryAnalysisSlotsTest() {
		// Create data
		init();
		
		LaboratoryMethod__c labMethod = new LaboratoryMethod__c (
												Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
												Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood',
												Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle');
		insert labMethod;
		
		List<LaboratoryAnalysisSlots__c> initialList = new List<LaboratoryAnalysisSlots__c>();
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=1, LaboratoryMethod__c=labMethod.Id, Study__c=studiesList[0].Id));
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=2, LaboratoryMethod__c=labMethod.Id, Study__c=studiesList[0].Id));
		insert initialList;
		
		List<LaboratoryAnalysisSlots__c> retrievedList = [Select Id FROM LaboratoryAnalysisSlots__c];
		
		// Before deletion
		system.assert(retrievedList.size() > 0);
		
		// After deletion
		Boolean resultFlag = LaboratoryAnalysisSlotsService.deleteLaboratoryAnalysisSlots(retrievedList);
		system.assertEquals(resultFlag, true);
		retrievedList = [Select Id FROM LaboratoryAnalysisSlots__c];
		system.assertEquals(retrievedList.size(), 0);
	}
	
	
	/** Test the function summarizeLabSlotsDesignByStudyMethodCombination
	 * @author	Dimitrios Sgourdos
	 * @version	11-Nov-2013
	 */
	static testMethod void summarizeLabSlotsDesignByStudyMethodCombination() {
		// Create dummy IDs
		List<Account> acc = new List<Account>();
		acc.add(new Account(Name = 'Study A',BillingCountry='NL'));
		acc.add(new Account(Name = 'Method A',BillingCountry='US'));
		acc.add(new Account(Name = 'Method B',BillingCountry='US'));
		insert acc;
		
		// Create a dummy list
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> initialList = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		
		// First Added slot
		LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c(ActiveNumberOfSamples__c= 1, PlaceboNumberOfSamples__c= 2, BackupNumberOfSamples__c= 3, OtherNumberOfSamples__c= 4, Repeat__c= 5);
		newItem.studyMethodCombination = acc[0].Id + ':' + acc[1].Id;
		initialList.add(newItem);
		
		// Second added slot
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c(ActiveNumberOfSamples__c= 11, PlaceboNumberOfSamples__c= 22, BackupNumberOfSamples__c= 33, OtherNumberOfSamples__c= 44, Repeat__c= 50);
		newItem.studyMethodCombination = acc[0].Id + ':' + acc[1].Id;
		initialList.add(newItem);
		
		// Third added slot
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c(ActiveNumberOfSamples__c= 21, PlaceboNumberOfSamples__c= 22, BackupNumberOfSamples__c= 23, OtherNumberOfSamples__c= 24, Repeat__c= 100);
		newItem.studyMethodCombination = acc[0].Id + ':' + acc[2].Id;
		initialList.add(newItem);
		
		// Create Select options list
		List<SelectOption> studyMethodOptionList = new List<SelectOption>();
		studyMethodOptionList.add(new SelectOption(acc[0].id+':'+acc[1].id, acc[0].Name+ ' / ' +acc[1].Name ));
		studyMethodOptionList.add(new SelectOption(acc[0].id+':'+acc[2].id, acc[0].Name+ ' / ' +acc[2].Name ));
		
		// Check the function
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> results = LaboratoryAnalysisSlotsService.summarizeLabSlotsDesignByStudyMethodCombination(
																											initialList,
																											studyMethodOptionList
																					);
		system.assertEquals(results.size(), 2);
		system.assertEquals(results[0].labSlot.ActiveNumberOfSamples__c, 12);
		system.assertEquals(results[0].labSlot.PlaceboNumberOfSamples__c, 24);
		system.assertEquals(results[0].labSlot.BackupNumberOfSamples__c, 36);
		system.assertEquals(results[0].labSlot.OtherNumberOfSamples__c, 48);
		system.assertEquals(results[0].getLabSlotTotalNumber(), '120');
		system.assertEquals(results[0].labSlot.Repeat__c, 55);
		system.assertEquals(results[0].studyMethodCombination, acc[0].Name+' / '+acc[1].Name);
		system.assertEquals(results[1].labSlot.ActiveNumberOfSamples__c, 21);
		system.assertEquals(results[1].labSlot.PlaceboNumberOfSamples__c, 22);
		system.assertEquals(results[1].labSlot.BackupNumberOfSamples__c, 23);
		system.assertEquals(results[1].labSlot.OtherNumberOfSamples__c, 24);
		system.assertEquals(results[1].getLabSlotTotalNumber(), '90');
		system.assertEquals(results[1].labSlot.Repeat__c, 100);
		system.assertEquals(results[1].studyMethodCombination, acc[0].Name+' / '+acc[2].Name);
	}
	
	
	/** Test the function validateLabSlotsAnDesign
	 * @author	Dimitrios Sgourdos
	 * @version	13-Nov-2013
	 */
	static testMethod void validateLabSlotsAnDesignTest() {
		// Create a dummy list
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> wrapperList = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		
		LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.LabSlot = new LaboratoryAnalysisSlots__c();
		newItem.slotNumber = '1';
		newItem.studyMethodCombination = 'Test combination 1';
		wrapperList.add(newItem);
		
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.LabSlot = new LaboratoryAnalysisSlots__c();
		newItem.slotNumber = '2';
		newItem.studyMethodCombination = 'Test combination 2';
		wrapperList.add(newItem);
		
		// Validate without ListOfShipments__c selection
		Boolean result = LaboratoryAnalysisSlotsService.validateLabSlotsAnDesign(wrapperList);
		system.assertEquals(result, false);
		
		// Validate without studyMethodCombination
		for(LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper tmpItem : wrapperList) {
			tmpItem.studyMethodCombination = '';
			tmpItem.labSlot.ListOfShipments__c = '01';
		}
		
		result = LaboratoryAnalysisSlotsService.validateLabSlotsAnDesign(wrapperList);
		system.assertEquals(result, false);
		
		// Validate with everything inserted
		for(LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper tmpItem : wrapperList) {
			tmpItem.studyMethodCombination = 'Test Combination';
		}
		
		result = LaboratoryAnalysisSlotsService.validateLabSlotsAnDesign(wrapperList);
		system.assertEquals(result, true);
	}
	
	
	/** Test the function getAssociatedShipmentNumbersToSlots
	 * @author	Dimitrios Sgourdos
	 * @version	14-Nov-2013
	 */
	static testMethod void getAssociatedShipmentNumbersToSlotsTest() {
		List<LaboratoryAnalysisSlots__c> labSlotsList = new List<LaboratoryAnalysisSlots__c>();
		labSlotsList.add(new LaboratoryAnalysisSlots__c(ListOfShipments__c='01:03:04'));
		labSlotsList.add(new LaboratoryAnalysisSlots__c(ListOfShipments__c='02:01:04'));
		labSlotsList.add(new LaboratoryAnalysisSlots__c(ListOfShipments__c='01:02:04'));
		
		Set<String> results = LaboratoryAnalysisSlotsService.getAssociatedShipmentNumbersToSlots(labSlotsList);
		
		system.assertEquals(results.size(), 4);
		system.assertEquals(results.contains('01'), true);
		system.assertEquals(results.contains('02'), true);
		system.assertEquals(results.contains('03'), true);
		system.assertEquals(results.contains('04'), true);
	}
	
	
	/** Test the function updateLabSlotsByShipmentNumbers
	 * @author	Dimitrios Sgourdos
	 * @version	14-Nov-2013
	 */
	static testMethod void updateLabSlotsByShipmentNumbersTest() {
		// Create initial map
		Map<String, String> numbersMap = new Map<String, String>();
		numbersMap.put('01', '08');
		numbersMap.put('02', NULL);
		numbersMap.put('03', '09');
		numbersMap.put('04', '10');
		
		// Create slots list
		List<LaboratoryAnalysisSlots__c> initialList = new List<LaboratoryAnalysisSlots__c>();
		initialList.add(new LaboratoryAnalysisSlots__c(ListOfShipments__c='01:03:04'));
		initialList.add(new LaboratoryAnalysisSlots__c(ListOfShipments__c='02:01:04'));
		initialList.add(new LaboratoryAnalysisSlots__c(ListOfShipments__c='01:02:04:03'));
		
		// Check the function
		List<LaboratoryAnalysisSlots__c> results = LaboratoryAnalysisSlotsService.updateLabSlotsByShipmentNumbers(initialList, numbersMap);
		system.assertEquals(results[0].ListOfShipments__c, '08:09:10');
		system.assertEquals(results[1].ListOfShipments__c, '08:10');
		system.assertEquals(results[2].ListOfShipments__c, '08:10:09');
	}
	
	
	/** Test the function updateListOfShipmentsInLabSlots
	 * @author	Dimitrios Sgourdos
	 * @version	14-Nov-2013
	 */
	static testMethod void updateListOfShipmentsInLabSlotsTest() {
		// Create general data
		init();
		
		LaboratoryMethod__c labMethod = new LaboratoryMethod__c (
												Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
												Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood',
												Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle');
		insert labMethod;
		
		// Create LaboratoryAnalysisSlots__c data
		List<LaboratoryAnalysisSlots__c> initialList = new List<LaboratoryAnalysisSlots__c>();
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=1, SequenceNumber__c=1, MdValSlot__c=false, ListOfShipments__c='01:03:04', Study__c=studiesList[0].Id, LaboratoryMethod__c=labMethod.Id));
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=1, SequenceNumber__c=2, MdValSlot__c=false, ListOfShipments__c='02:01:04', Study__c=studiesList[1].Id, LaboratoryMethod__c=labMethod.Id));
		initialList.add(new LaboratoryAnalysisSlots__c(SlotNumber__c=2, SequenceNumber__c=1, MdValSlot__c=false, ListOfShipments__c='01:02:04:03',Study__c=studiesList[1].Id, LaboratoryMethod__c=labMethod.Id));
		insert initialList;
		
		// Create dummy Sample Shipments lab design
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> samplesWrapperList = new List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
		LaboratorySamplesService.LaboratorySamplesDesignWrapper newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=1);
		newItem.shipmentNumber = '7';
		samplesWrapperList.add(newItem);
		
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=3);
		newItem.shipmentNumber = '8';
		samplesWrapperList.add(newItem);
		
		newItem = new LaboratorySamplesService.LaboratorySamplesDesignWrapper();
		newItem.labSample = new LaboratorySamples__c(ShipmentNumber__c=4);
		newItem.shipmentNumber = '9';
		samplesWrapperList.add(newItem);
		
		// Check the function
		LaboratoryAnalysisSlotsService.updateListOfShipmentsInLabSlots(studiesList, samplesWrapperList);
		List<LaboratoryAnalysisSlots__c> results = LaboratoryAnalysisSlotsDataAccessor.getSlotsByMdValFlagAndStudies(studiesList, false);
		
		system.assertEquals(results.size(), 3);
		system.assertEquals(results[0].SlotNumber__c, 1);
		system.assertEquals(results[0].SequenceNumber__c, 1);
		system.assertEquals(results[0].ListOfShipments__c, '07:08:09');
		system.assertEquals(results[1].SlotNumber__c, 1);
		system.assertEquals(results[1].SequenceNumber__c, 2);
		system.assertEquals(results[1].ListOfShipments__c, '07:09');
		system.assertEquals(results[2].SlotNumber__c, 2);
		system.assertEquals(results[2].SequenceNumber__c, 1);
		system.assertEquals(results[2].ListOfShipments__c, '07:09:08');
	}
	
	
	/** Test the function groupSlotsDesignWrapperByStudyMethod
	 * @author	Dimitrios Sgourdos
	 * @version	26-Nov-2013
	 */
	static testMethod void groupSlotsDesignWrapperByStudyMethodTest() {
		// Create data
		init();
		
		// Create Laboratory Method, compound and analysis
		LaboratoryMethod__c lm = new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
											Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood', 
											Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle');
		insert lm; 
		
		LaboratoryMethodCompound__c compound = LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(lm, 'midazolam');
		insert compound;
		
		List<LaboratoryAnalysis__c> initialLabAnalysisList = new List<LaboratoryAnalysis__c>();
		initialLabAnalysisList.add( LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compound,studiesList[0],false,true,true) );
		initialLabAnalysisList.add( LaboratoryAnalysisService.createLaboratoryAnalysisInstance(compound,studiesList[1],false,true,true) );
		
		// Insert and retrieved so to have access in all parents fields
		insert initialLabAnalysisList;
		List<LaboratoryAnalysis__c> retrievedList = LaboratoryAnalysisDataAccessor.getByMdOrValAndUserStudies(studiesList);
		
		// Create a dummy list
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> results = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		
		// First Added slot
		LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '3';
		newItem.studyMethodCombination = studiesList[0].Id + ':' + lm.Id;
		results.add(newItem);
		
		// Second added slot
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '1';
		newItem.studyMethodCombination = studiesList[1].Id + ':' + lm.Id;
		results.add(newItem);
		
		// Third added slot
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.slotNumber = '2';
		newItem.studyMethodCombination = studiesList[0].Id + ':' + lm.Id;
		results.add(newItem);
		
		// check the function
		results = LaboratoryAnalysisSlotsService.groupSlotsDesignWrapperByStudyMethod(results, studiesList, retrievedList);
		system.assertEquals(results[0].studyMethodCombination, studiesList[0].Code__c.substringAfterLast('-') + ' / ' + lm.Name);
		system.assertEquals(results[0].slotNumber, '3');
		system.assertEquals(results[1].studyMethodCombination, studiesList[0].Code__c.substringAfterLast('-') + ' / ' + lm.Name);
		system.assertEquals(results[1].slotNumber, '2');
		system.assertEquals(results[2].studyMethodCombination, studiesList[1].Code__c.substringAfterLast('-') + ' / ' + lm.Name);
		system.assertEquals(results[2].slotNumber, '1');
	}
	
	
	/** Test the function getUsedShipmentsNumbersInSlotsForAnalysisDesign
	 * @author	Dimitrios Sgourdos
	 * @version	26-Nov-2013
	 */
	static testMethod void getUsedShipmentsNumbersInSlotsForAnalysisDesignTest() {
		// Create a dummy list
		List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> initialList = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		
		// First Added slot
		LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c(ListOfShipments__c='02:03:09');
		initialList.add(newItem);
		
		// Second added slot
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c();
		initialList.add(newItem);
		
		// Third added slot
		newItem = new LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper();
		newItem.labSlot = new LaboratoryAnalysisSlots__c(ListOfShipments__c='02:03:04');
		initialList.add(newItem);
		
		// Check the function
		Set<Decimal> results = LaboratoryAnalysisSlotsService.getUsedShipmentsNumbersInSlotsForAnalysisDesign(initialList);
		system.assertEquals(results.size(), 4);
		system.assert(results.contains(2));
		system.assert(results.contains(3));
		system.assert(results.contains(4));
		system.assert(results.contains(9));
	}
}