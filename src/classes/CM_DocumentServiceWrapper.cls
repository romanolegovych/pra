/**
*   @author Sukrut Wagh
*   @date       07/07/2014
*   @description    Wrapper class for Document Web Service (WS) client.
*/
public with sharing class CM_DocumentServiceWrapper {
    
    private static final COM_Logger LOGGER = new COM_Logger('CM_DocumentServiceWrapper');
    private static String ENDPOINT = '';
    private static String SERVICENAME = '';
    private static Integer TIMEOUT = 0;
    private static List<String> errors;
    
    static {
        initSettings();
    }  
    
    public static List<String> getErrors() {
        return errors;
    }

    /**
    *   @description    Configures the WS endpoint
    */
    private static void initSettings() {
        Map<String, MuleServicesCS__c> settings = MuleServicesCS__c.getAll();
        ENDPOINT = settings.get('ENDPOINT').Value__c;
        SERVICENAME = settings.get('DocumentService').Value__c;
        Integer to = Integer.valueOf(settings.get('TIMEOUT').Value__c);
        if (to != null && to > 0) {
            TIMEOUT = to;
        } 
        LOGGER.info('Using following Custom Settings for CM_DocumentServiceWrapper');
        LOGGER.info('ENDPOINT:'+ENDPOINT);
        LOGGER.info('SERVICENAME:'+SERVICENAME);
        LOGGER.info('TIMEOUT:'+TIMEOUT);
    }
    
    /**
    *   @description    Returns the service
    */
    private static CM_DocumentService.IDocumentServicePort getService() {
        CM_DocumentService.IDocumentServicePort service = new CM_DocumentService.IDocumentServicePort();
        service.endpoint_x = ENDPOINT + SERVICENAME;
        service.timeout_x = TIMEOUT;
        return service;
    }
    
    /**
    *   @description    Posts documents to be processed asynchronously.
    *   @param  requesterId - A valid requester id. The service will process requests coming from a valid requester
    *   @param  agrDocs     - Documents to be posted
    */
    public static void postDocumentsAsync(final String requesterId, final List<Apttus__Agreement_Document__c> agrDocs) {
        if(COM_Utils.isNotEmpty(requesterId) && COM_Utils.isNotEmpty(agrDocs)) {
            try {
                CM_DocumentService.IDocumentServicePort service = getService();
                List<CM_DocumentService.documentRequest> requests = new List<CM_DocumentService.documentRequest>(); 
                CM_DocumentService.documentRequest request = new CM_DocumentService.documentRequest();
                CM_DocumentService.requester requester = new CM_DocumentService.requester();
                requester.id = requesterId;
                request.agreements = getAgrsFromAgrDocs(agrDocs);
                request.documents = getDocsFromAgrDocs(agrDocs);
                request.requester = requester;
                
                requests.add(request);
                service.postDocumentsAsync(requesterId, requests);
            } catch(Exception e) {
                LOGGER.error('Error occured in postDocumentsAsync',e);
            }
        }
    }
    
    /** @description    Converts Apttus__Agreement_Document__c to CM_DocumentService.agreement
    */
    private static List<CM_DocumentService.agreement> getAgrsFromAgrDocs(final List<Apttus__Agreement_Document__c> agrDocs) {
        List<CM_DocumentService.agreement> agrs = null;
        CM_DocumentService.agreement agr = null;
        if(COM_Utils.isNotEmpty(agrDocs)) {
            agrs = new List<CM_DocumentService.agreement>();
            for(Apttus__Agreement_Document__c agrDoc : agrDocs) {
                if(null != agrDoc) {
                    agr = new CM_DocumentService.agreement();
                    agr.id = agrDoc.Apttus__Agreement__r.Id;
                    agr.agrDocId = agrDoc.Id;
                    agr.clientProtocolNum = agrDoc.Apttus__Agreement__r.Site__r.Project_Protocol__r.Name;
                    agr.sponsor = agrDoc.Apttus__Agreement__r.Site__r.Project_Protocol__r.Sponsor_ID__r.Name;
                    agr.country = agrDoc.Apttus__Agreement__r.Site__r.Country__c;
                    agr.invFirstName = agrDoc.Apttus__Agreement__r.Site__r.Investigator__r.First_Name__c;
                    agr.invLastName = agrDoc.Apttus__Agreement__r.Site__r.Investigator__r.Last_Name__c;
                    agr.siteIntegrationId = agrDoc.Apttus__Agreement__r.Site__r.Site_ID__c;
                    agr.type_x = agrDoc.Apttus__Agreement__r.Contract_Type__c;
                    agrs.add(agr);
                }
            }
        }
        return agrs;
    }
    
    /** @description    Converts Apttus__Agreement_Document__c to CM_DocumentService.document
    */
    private static List<CM_DocumentService.document> getDocsFromAgrDocs(final List<Apttus__Agreement_Document__c> agrDocs) {
        List<CM_DocumentService.document> docs = null;
        CM_DocumentService.document doc = null;
        if(COM_Utils.isNotEmpty(agrDocs)) {
            docs = new List<CM_DocumentService.document>();
            for(Apttus__Agreement_Document__c agrDoc : agrDocs) {
                if(null != agrDoc) {
                    doc = new CM_DocumentService.document();
                    /* Example: Apttus__URL__c = /servlet/servlet.FileDownload/AgreementProtocol %231001_Site0125_2014-06-05_v1_Redlines.doc?file=015L00000006frYIAQ
                     * TODO: Replace the url param extraction. Use UrlUtils.getParams(url) 
                     * https://code.google.com/p/apex-lang/source/browse/tags/1.9/eclipse-project/src/classes/UrlUtils.cls
                    */
                    doc.id = COM_Utils.substringAfterLast(agrDoc.Apttus__URL__c,'=') ;
                    docs.add(doc);
                }
            }
        }
        return docs;
    }
    
    

}