public with sharing class BDT_SingleProject {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;}
	public BDT_SecuritiesService.pageSecurityWrapper securitiesB {get;set;}

	public Client_Project__c singleProject 			{get;set;}
	public String sponsorEntitySelected				{get;set;}
	public String sponsorSelected					{get;set;}
	public String projectId 						{get;set;}
	public String concSponsorSponsorEntity  		{get;set;}
	public String SponsorEntityId					{get;set;}
	public string concSponsorSponsorEntitylkold		{get;set;}
	public boolean showProtocolTitle				{get;set;}

	private BDT_UserPreferenceService UserPreferences;
	public List<Study__c> studiesList 				{get;set;}

	public Boolean showDeleted{
		get {if (showDeleted == null) {
				showDeleted = false;
			}
			return showDeleted;
		}
		set;
	}

	public BDT_SingleProject(){

		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Project');
		securitiesB = new BDT_SecuritiesService.pageSecurityWrapper('Study');

		concSponsorSponsorEntity = '';


		// USER PREFERENCES
		UserPreferences = new BDT_UserPreferenceService();
		projectId = UserPreferences.getPreference(BDT_UserPreferenceDataAccessor.PROJECT);
		String stringShowProtocolTitle = UserPreferences.getPreference(BDT_UserPreferenceDataAccessor.SHOWPROTOCOL);

		stringShowProtocolTitle = (stringShowProtocolTitle==null||stringShowProtocolTitle=='') ? 'true':stringShowProtocolTitle;
		showProtocolTitle = (stringShowProtocolTitle == 'true')?true:false;

			if(projectId!=null && projectId!=''){
				singleProject = BDT_Utils.getProject(projectId, showDeleted);
				readSelectedStudies ();

			}else{//there is no user preferences set for first time, therefore, need to present singleProject page with all objects initialized
				singleProject = BDT_DC_Project.createEmptyProject();
			}

			if (singleProject.Client__c != null ) {

					concSponsorSponsorEntity = '['+singleProject.Client__r.Sponsor__r.Name+'] '+singleProject.Client__r.Name;
					SponsorEntityId= singleProject.Client__r.Id;
			}
	}

	public void readSelectedStudies () {
		studiesList = new List<Study__c>();
		try{
			Set<ID> studieIDs = new Set<ID>();
			List<String> selectedStudies = UserPreferences.getPreference(BDT_UserPreferenceDataAccessor.STUDIES).split(':');
			for (String ss:selectedStudies){studieIDs.add(ss);}
			studiesList = [select Title__c, Status__c, Sponsor_Code__c, Protocol_Title__c,
														Project__c, Name, Legacy_Code__c, BDTDeleted__c, Id, Code__c,
														Business_Unit__c, Business_Unit__r.Name
												from 	Study__c
												where 	id = :studieIDs
												and 	BDTDeleted__c = false
												order by code__c];
		} Catch (Exception e) {}
	}

	public void changeShowProtocolTitle () {
		String stringShowProtocolTitle = (showProtocolTitle) ? 'true':'false';
		UserPreferences.setPreference(BDT_UserPreferenceDataAccessor.SHOWPROTOCOL, stringShowProtocolTitle);
		UserPreferences.saveUserPreferences();
	}

	public list<SelectOption> getTherapeuticAreas() {
		String currentTerapeuticArea = singleProject.Therapeutic_Area__c;
		Boolean currentInList = false;

		List<SelectOption> options = new List<SelectOption>();
		options.add(new SelectOption('',''));

		for (TherapeuticArea__c ta : [Select Name From TherapeuticArea__c])
		{
			options.add(new SelectOption(ta.name,ta.name));
			if (!currentInList && currentTerapeuticArea != null && currentTerapeuticArea == ta.name) {
				currentInList = true;
			}
		}

		if (!currentInList && currentTerapeuticArea != null) {
			options.add(new SelectOption(currentTerapeuticArea,currentTerapeuticArea));
		}
		if (singleProject.Therapeutic_Area__c==null) {
			singleProject.Therapeutic_Area__c = '';
		}
		return options;
	}

	public PageReference save(){
		/********************prepare sponsor entity for saving* ************************************/
		//only save sponsor entity if the SponsorEntityId field is filled.
		//generate error message when SponsorEntityId field is empty and current sponsor name and stored sponsor name do not match(not selected from list)
		//the SponsorEntityId field is only filled by the JS, and is reset upon save.

		if(concSponsorSponsorEntity==null || concSponsorSponsorEntity==''){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'You did not specify a valid sponsor entity. ');
			ApexPages.addMessage(msg);
		}else{
			if(SponsorEntityId==null || SponsorEntityId==''){
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'The Sponsor you have specified is not recognized, please contact your Administrator.');
				ApexPages.addMessage(msg);
			}else{
				if(SponsorEntityId!= singleProject.Client__c){
					singleProject.Client__c = Id.valueOf( sponsorEntityId );
				}
			}
		}

		// generate a project code when the record is first saved
		if (string.isEmpty(singleproject.id)) {
			singleproject.code__c = BDT_DC_Project.getNewProjectCode();
		}

		try{
			if(singleProject.id!=null){
				update singleproject;
			}else{
				insert singleProject;
			}
		}catch(QueryException e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to save Project');
			ApexPages.addMessage(msg);
		}

		concSponsorSponsorEntitylkold ='';
		//TherapeuticArealkold='';

		// clear studie list when inserting a new project ID
		if (singleProject.id!=null && String.valueOf(singleproject.id) != UserPreferences.getPreference(BDT_UserPreferenceDataAccessor.PROJECT)) {
			UserPreferences.setPreference(BDT_UserPreferenceDataAccessor.PROJECT, singleproject.id);
			UserPreferences.setPreference(BDT_UserPreferenceDataAccessor.STUDIES, '');
			UserPreferences.saveUserPreferences();
		}

		PageReference singleprojectPage = new PageReference(System.Page.BDT_singleproject.getUrl());
		singleprojectPage.setRedirect(true);

		if(ApexPages.hasMessages()){
			return null;
		}else{
			return singleprojectPage;
		}
	}

	public PageReference edit(){
		String studyId =  System.CurrentPageReference().getParameters().get('studyId');
		PageReference newEditStydyPage = new PageReference(System.Page.bdt_neweditstudy.getUrl());

		if(studyId!=null && studyId!=''){
			newEditStydyPage.getParameters().put('studyId', studyId);
		}
		newEditStydyPage.setRedirect(true);
		return newEditStydyPage;
	}

	public String returnCount(){
		date myDate = date.today();

		//current year
		String year = String.valueOf(Datetime.now().year()).substring(2,4);
		//current month
		String month = String.valueOf(Datetime.now().month()).length()<2 ? '0'+String.valueOf(Datetime.now().month()): String.valueOf(Datetime.now().month());

		Integer mycount = 0;
		String newProjectCode;
		Client_Project__c lastcreatedproject;
		try{
			//search for the latest project that has been saved, if there is none then the count starts in 1
			lastcreatedproject = [Select id, code__c, createddate from Client_project__c where (Code__c!=null and Code__c!='') order by createddate desc Limit 1];

		}catch(QueryException e){
			lastcreatedproject = new Client_Project__c();
			System.debug('There are no client Projects, this record is the first');
			mycount = 1;
		}

		//if Code__c is not empty then we have to compare it to today's year month and check if this exists if not then we should create the first one
		if(lastcreatedproject.Code__c!='' && lastcreatedproject.Code__c!=null){
			List<String> countIndex =lastcreatedproject.Code__c.split('-');
			//check today's year month against the last created record
			String projectCountString;
			if(countIndex[0]=='PRA'+year+month){
				//last project for this yearmonth, get this last value and add one, this is if there is a previous project for this yearmonth

				mycount = Integer.valueOf(countIndex[1])+1;
				projectCountString = BDT_Utils.addLeadingZerosToCat(String.valueOf(mycount));

			}else{
				projectCountString = BDT_Utils.addLeadingZerosToCat('1');
			}
			newProjectCode = 'PRA'+year+month+'-'+projectCountString;
		}else{
			//If there is no project whatsoever
			newProjectCode = 'PRA'+year+month+'-'+BDT_Utils.addLeadingZerosToCat('1');
		}


		return newProjectCode;
	}

	public PageReference cancel(){

		PageReference singleprojectPage = new PageReference(System.Page.BDT_singleproject.getUrl());
		singleprojectPage.setRedirect(true);
		return singleprojectPage;
	}

	public PageReference deleteSoft(){
		//system.debug('This is delete method');
		//set BU to deleted
		singleProject.BDTDeleted__c = true;
		try{
			List<Study__c> studyList = [Select s.Id, BDTDeleted__c
						From Study__c s
						Where s.Project__c = : singleProject.id];

			//set Sites related to this particular BU are also set to deleted
			for(Study__c siL: studyList){
				siL.BDTDeleted__c = true;
			}
			update studyList;
		}catch(QueryException e){

		}

		try{
			update singleProject;
		}catch(QueryException e){
			system.debug('Unable to update Single Project.');
		}

		// remove from quicklist
		UserPreferences.removeProjectFromQuickList (singleProject.id);

		return referToSingleProjectEmpty();//when deleting the current record fromuser preference, then create a new SingleProject page should display

	}

	public PageReference referToSingleProjectEmpty(){

		//clean all preferences mentione on this project
		BDT_Utils.cleanProjectPreferences(singleProject.id, true);
		PageReference singleprojectPage = new PageReference(System.Page.BDT_singleproject.getUrl());
		singleprojectPage.setRedirect(true);
		return singleprojectPage;
	}

 	@RemoteAction
	public static List<Account> getJson(String searchTerm){
		//system.debug('Debug: searchTerm -> '+searchTerm);

		List<Account> liAcc = new List<Account>();

		String query = 'select id, name, Sponsor__r.id, Sponsor__r.name from Account ' +
			'where (name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\' ' +
			'or 	Sponsor__r.name like \'%' + String.escapeSingleQuotes(searchTerm) + '%\') '+
			'and Sponsor__r.name !=null '+
			'order by Sponsor__r.name,name limit 10';
		liAcc = Database.query(query);

		for (Account a : liAcc) {
			a.name = '['+a.sponsor__r.name+'] ' +a.name;
		}

		return liAcc;
	}
}