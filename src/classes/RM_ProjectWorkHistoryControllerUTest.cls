/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_ProjectWorkHistoryControllerUTest {
	 static list<Country__c> lstCountry;	
    
    static list<Employee_Details__c> lstEmpl; 
    static list<Employee_Status__c> lstEmplStatus;
    static Employee_Details__c supervisor;
    static list<WFM_Client__c> lstClient;
    static list<WFM_Contract__c>  lstContract;
    static list<WFM_Project__c> lstProject;
    static WFM_Protocol__c aProt;
    static list<WFM_Site_Detail__c> lstSite;
    

    
    static List<WFM_Location_Hours__c> lstHrs;
    static List<WFM_Location_Hours__c> hrs1;
    static List<WFM_Employee_Availability__c> lstAvas;
 
    
  	static list<WFM_EE_Work_History__c> lstHistory;
  	static list<WFM_EE_Work_History_Summary__c> lstHistorySummary;
  	static list<Job_Class_Desc__c> lstJob;
    
   
    
    static RM_UnitTestData init(){
    	RM_UnitTestData testData = new RM_UnitTestData();
    	lstCountry = testData.lstCountry;
    	lstEmpl = testData.lstEmpl;
    	lstEmplStatus = testData.lstEmplStatus;
    	lstClient = testData.lstClient;
    	lstContract = testData.lstContract;
    	lstProject = testData.lstProject;
    	lstJob = testData.lstJobs;
    	aProt = testData.insertProt(lstProject[0].ID);
    	lstSite = testData.insertSite(aProt.ID); 
    	
    	lstHrs = testData.insertLocationHr(-4, 6, 'TTT'); 
    	hrs1 = testData.insertLocationHr(-4, 6, 'TTS');
    	lstAvas = testData.insertAvailability(lstEmpl[0], lstHrs);
    	
    	lstHistory = testData.insertWorkHist(lstProject[0].id, lstEmpl[0].id, lstAvas);
    	lstHistorySummary = testData.insertWorkHistSummary(lstProject[0].id, lstEmpl[0].id);
    	system.debug('---------Test lstCountry---------'+lstCountry  ); 
    	system.debug('---------Test lstHrs---------'+lstHrs  ); 

    	return testData;
    }
      static testMethod void TestHistory() {
        init();
       
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/RM_projectworkhistory?ID='+ lstProject[0].id );
        Test.setCurrentPage(pageRef);
        
        RM_ProjectWorkHistoryController detailExt = new RM_ProjectWorkHistoryController();   
        List<RM_ProjectWorkHistoryController.WorkHistory> hist = detailExt.GetHistory();
        system.debug('---hist---' + hist);
        system.assert(hist.size() > 0);
       
        Test.stopTest();
      }
    
}