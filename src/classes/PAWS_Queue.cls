global with sharing class PAWS_Queue implements Database.Batchable<sObject>, Database.Stateful, PAWS_IScheduler
{
	public class PAWS_QueueException extends Exception {}
	
	/******************** STATIC SECTION *************************/
	
	public static PAWS_Queue__c addTask(String typeOfTask, String data)
	{
		PAWS_Queue__c record = new PAWS_Queue__c(Type__c=typeOfTask, Data__c=data, Status__c='Pending');
		insert record;
		
		startBatch();
		
		return record;
	}
	
	public static List<PAWS_Queue__c> addTasks(List<Map<String, String>> tasks)
	{
		List<PAWS_Queue__c> records = new List<PAWS_Queue__c>();
		for(Map<String, String> task : tasks)
		{
			records.add(new PAWS_Queue__c(Type__c=task.get('Type__c'), Data__c=task.get('Data__c'), Status__c='Pending'));
		}

		insert records;
		
		startBatch();
		
		return records;
	}
	
	/******************** SCHEDULER SECTION *************************/

	public void schedulerExecute(Object ctx)
	{
		PAWS_Queue.startBatch();
	}  

	/******************** BATCH SECTION *************************/
	
	//*********************** STATIC ******************************/
	
	public static void startBatch()
	{
		if(!canStartBatch(false)) return;
		
    	Database.executeBatch(new PAWS_Queue(), 1);
    	PAWS_Scheduler.startScheduler('PAWS_Queue');
	}
	
	private static Boolean canStartBatch(Boolean force)
   	{
   		Integer total = [select count() from PAWS_Queue__c where Status__c in ('Pending', 'In Progress')];
   		if(total == 0) return false;
   		
   		if(force == true) return true;
   		
   		total = [select count() from AsyncApexJob where ApexClass.Name = 'PAWS_Queue' and Status in ('Processing', 'Queued', 'Preparing') and JobType = 'BatchApex' limit 1];
   		return total == 0;
   	}

	//*********************** PUBLIC ******************************/
	
	global Database.QueryLocator start(Database.BatchableContext BC)
    {
    	if(Test.isRunningTest())
    	{
    		return Database.getQueryLocator([select Type__c, Data__c, Status__c from PAWS_Queue__c where Status__c in ('Pending', 'In Progress') limit 1]);
    	}else{	
    		return Database.getQueryLocator([select Type__c, Data__c, Status__c from PAWS_Queue__c where Status__c in ('Pending', 'In Progress')]);
    	}
    }

	global void execute(Database.BatchableContext info, List<sObject> scope)
	{	
		PAWS_Queue__c record = (PAWS_Queue__c)scope[0];
		if (record.Type__c == 'Activate Flow')
		{
			activateFlow(record);
		}
		else if (record.Type__c == 'Linked Step Proceed')
		{
			linkedStepProceed(record);
		}
		else if (record.Type__c == 'Linked Step Sync Dates')
		{
			linkedStepSyncDates(record);
		}
		
		update record;
   	}     
   	
   	global void finish(Database.BatchableContext info)
   	{   
   		clearQueue();  
   		restartBatch(info);
   	} 
   	
   	//*********************** PRIVATE ******************************/
   	
   	private void clearQueue()
   	{
   		delete [select Id from PAWS_Queue__c where CreatedDate < :Date.today().addDays(-3)];
   	}
   	
   	private void restartBatch(Database.BatchableContext info)
   	{
   		AsyncApexJob job = [select NumberOfErrors from AsyncApexJob where Id = :info.getJobId()];
   		if(job.NumberOfErrors > 0 || !canStartBatch(true)) return;
   		
    	Database.executeBatch(new PAWS_Queue(), 1);
   	}
   	
   	
   	/******************** ACTIVATE FLOW SECTION *************************/
   	
   	private void activateFlow(PAWS_Queue__c record)
   	{
   		try
   		{
   			record.Status__c = 'In Progress';
   			
   			Map<String, String> dataMap = (Map<String, String>)JSON.deserialize(record.Data__c, Map<String, String>.class);
	   		sObject result = new PAWS_API().activateFlow((ID)dataMap.get('sourceId'), (ID)dataMap.get('templateId'));
	   		if(result == null) throw new PAWS_QueueException('Invalid source id: ' + String.valueOf(dataMap.get('sourceId')));
	   		
	   		ID templateId = (ID)dataMap.get('templateId');
	   		Map<String, Object> history = PAWS_APIHelper.getHistory(result, templateId);
	   		history = (Map<String, Object>)history.get(templateId);
	   		
	   		String status = (String)history.get('status');
	   		if(status == 'Error') 
	   		{
	   			record.Status__c = 'Error';
	   			record.Error_Details__c = (String)history.get('errors');
	   		}else if(status == 'Active') 
	   		{
	   			record.Status__c = 'Complete';
	   		}
   		}catch(Exception ex)
   		{
   			record.Status__c = 'Error';
	   		record.Error_Details__c = ex.getMessage() + '\n' + ex.getLineNumber();
   		}
   	}
   	
	private void linkedStepProceed(PAWS_Queue__c record)
	{
		try
		{
			record.Status__c = 'In Progress';
			Map<String, String> dataMap = (Map<String, String>) JSON.deserialize(record.Data__c, Map<String, String>.class);
			
			new PAWS_StepProperty_LinkedStepHelper().ProceedLinkedStep(dataMap);
			
			record.Status__c = 'Complete';
		}
		catch(Exception ex)
		{
			record.Status__c = 'Error';
			record.Error_Details__c = ex.getMessage() + '\n' + ex.getLineNumber();
		}
	}
	
	private void linkedStepSyncDates(PAWS_Queue__c record)
	{
		try
		{
			record.Status__c = 'In Progress';
			Map<String, String> dataMap = (Map<String, String>) JSON.deserialize(record.Data__c, Map<String, String>.class);
			
			new PAWS_StepProperty_LinkedStepHelper().SyncDates(record, dataMap);
			
			record.Status__c = 'Complete';
		}
		catch(Exception ex)
		{
			record.Status__c = 'Error';
			record.Error_Details__c = ex.getMessage() + '\n' + ex.getLineNumber();
		}
	}
}