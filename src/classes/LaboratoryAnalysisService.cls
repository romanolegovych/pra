/** Implements the Service Layer of the object LaboratoryAnalysis__c
 * @author	Dimitrios Sgourdos
 * @version	06-Nov-2013
 */
public with sharing class LaboratoryAnalysisService {
	
	/** Create a LaboratoryAnalysis__c instance with the given attributes
	 * @author	Dimitrios Sgourdos
	 * @version 10-Oct-2013
	 * @param	labMethodCompound	The compound that the Laboratory Analysis will be connected
	 * @param	study				The study that the Laboratory Analysis will be connected
	 * @param	ANFlag				The analysis selection
	 * @param	MDFlag				The method development selection
	 * @param	VALFlag				The method validation selection
	 * @return	The LaboratoryAnalysis__c instance
	 */
	public static LaboratoryAnalysis__c createLaboratoryAnalysisInstance(LaboratoryMethodCompound__c labMethodCompound, 
																		Study__c study,
																		Boolean ANFlag,
																		Boolean MDFlag,
																		Boolean VALFlag) {
		// Create and return LaboratoryAnalysis__c
		return ( new LaboratoryAnalysis__c( LaboratoryMethodCompound__c = labMethodCompound.Id,
											Study__c = study.Id,
											Analysis__c = ANFlag,
											MethodDevelopment__c = MDFlag,
											MethodValidation__c = VALFlag) );
	}
	
	
	/** Reorder a list of laboratory analysis to be correctly associated with a list of studies
	 * @author	Dimitrios Sgourdos
	 * @version 14-Oct-2013
	 * @param	analysisList	The laboratory analysis list that will be reordered
	 * @param	studiesList		The list of studies that the reordering will be based on
	 * @return	The reordered list of laboratory analysis
	 */
	public static List<LaboratoryAnalysis__c> reorderLaboratoryAnalysisListByStudiesList(List<LaboratoryAnalysis__c> analysisList,
																						List<Study__c> studiesList,
																						LaboratoryMethodCompound__c labCompound) {
		// Declare variables
		List<LaboratoryAnalysis__c> newAnalysisList = new List<LaboratoryAnalysis__c>();
		
		// Create a map with key the Study id and value the LaboratoryAnalysis__c instance
		Map<String,SObject> studyLabAnalysisMap = BDT_Utils.mapSObjectOnKey (analysisList, new List<String>{'Study__c'});
		
		// Create the new Laboratory Analysis List
		for(Study__c tmpStudy : studiesList) {
			LaboratoryAnalysis__c newAnalysisItem = new LaboratoryAnalysis__c();
			if( studyLabAnalysisMap.containsKey(tmpStudy.Id) ) {
				newAnalysisItem = (LaboratoryAnalysis__c) studyLabAnalysisMap.get(tmpStudy.Id);
			} else {
				newAnalysisItem.Study__c = tmpStudy.Id;
				newAnalysisItem.LaboratoryMethodCompound__c = labCompound.Id;
			}
			newAnalysisList.add(newAnalysisItem);
		}
		
		return newAnalysisList;
	}
	
	
	/** Delete a list of laboratory analysis.
	 * @author	Dimitrios Sgourdos
	 * @version 05-Nov-2013
	 * @param	analysisList		The laboratory analysis list that will be reordered
	 * @return	If the deletion was successful or not
	 */
	public static Boolean deleteAnalysisList(List<LaboratoryAnalysis__c> analysisList) {
		// Declare variables
		List<LaboratoryAnalysis__c> deletedList = new List<LaboratoryAnalysis__c>();
		
		// Delete only the analysis that has an id
		for(LaboratoryAnalysis__c tmpAnalysis : analysisList) {
			if(tmpAnalysis.Id != NULL) {
				deletedList.add(tmpAnalysis);
			}
		}
		
		try{
			delete deletedList;
		} catch(Exception e) {
			return false;
		}
		
		return true;
	}
	
	
	/** Create a select options list which have the distinct combination of the studies and methods
	 *	that can be met in the given laboratory analysis list.
	 * @author	Dimitrios Sgourdos
	 * @version 06-Nov-2013
	 * @param	labAnalysisList		The laboratory analysis list that will be reordered
	 * @return	The select options list with the distinct combination of the studies and methods.
	 */
	public static List<SelectOption> getStudyAndMethodSelectOptions(List<LaboratoryAnalysis__c> labAnalysisList) {
		// Read the picklist values
		List<SelectOption> valuesList = new List<SelectOption>();
		valuesList.add(new SelectOption('', 'Please select...'));
		
		// Create the distinct SelectOption list
		Set<String> distinctCombineSet = new Set<String>();
		for(LaboratoryAnalysis__c tmpItem : labAnalysisList) {
			
			if(tmpItem.LaboratoryMethodCompound__r.LaboratoryMethod__r.Id!=NULL  &&  tmpItem.Study__c!=NULL ) {
				// Combine study and method
				String key = tmpItem.Study__c + ':' + tmpItem.LaboratoryMethodCompound__r.LaboratoryMethod__r.Id;
				
				// If the combination is not yet in the list, add it
				if( ! distinctCombineSet.contains(key) ) {
					String tmpValue = tmpItem.Study__r.Code__c.substringAfterLast('-')
											+ ' / '
											+ tmpItem.LaboratoryMethodCompound__r.LaboratoryMethod__r.Name;
					distinctCombineSet.add(key);
					valuesList.add(new SelectOption(key, tmpValue));
				}
			}
			
		}
		
		return valuesList;
	}
	
	
	/** Retrieve the Laboratory analysis that the MethodDevelopment or MethodValidation flag is true 
	 *	for the given studies.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 06-Nov-2013
	 * @return	The list of Studies
	 */
	public static List<LaboratoryAnalysis__c> getLabAnalysisByMdOrValAndUserStudies(List<Study__c> studiesList) {
		return LaboratoryAnalysisDataAccessor.getByMdOrValAndUserStudies(studiesList);
	}
	
	
	/** Retrieve the Laboratory analysis that the Analysis__c flag is true for the given studies.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 11-Nov-2013
	 * @param	userStudies			The given studies
	 * @return	The Laboratory analysis list.
	 */
	public static List<LaboratoryAnalysis__c> getLabAnalysisByAnalysisAndUserStudies(List<Study__c> studiesList) {
		return LaboratoryAnalysisDataAccessor.getByAnalysisAndUserStudies(studiesList);
	}
}