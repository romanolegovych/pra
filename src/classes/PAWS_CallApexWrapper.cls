public with sharing class PAWS_CallApexWrapper implements STSWR1.WorkflowCallApexActionInterface 
{
	public void run(sObject context, STSWR1__Flow_Instance_Cursor__c flowInstanceCursor, String operationType, String parameter)
    {
    	system.debug('---- EXECUTING ACTION: ' + parameter);
    	if(parameter == 'CompleteParentStep') new PAWS_API().completeParentStep(context, flowInstanceCursor);
    }
}