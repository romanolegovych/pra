public with sharing class PAWS_WFMSiteTrigger extends STSWR1.AbstractTrigger
{
	public override void afterInsert(List<SObject> records)
	{
		//associatePAWSSites((List<WFM_Site_Detail__c>) records);
		afterSave(records, new List<WFM_Site_Detail__c>());
	}
	
	public override void afterUpdate(List<SObject> records, List<SObject> oldRecords)
	{
		afterSave(records, oldRecords);
		//associatePAWSSites((List<WFM_Site_Detail__c>) records);
		//updateFlowNames((List<WFM_Site_Detail__c>) records, (List<WFM_Site_Detail__c>) oldRecords);
		//updateAssociatedPAWSSiteNames(selectExtendedWFMSites((List<WFM_Site_Detail__c>) records));
	}

	private void afterSave(List<SObject> records, List<SObject> oldRecords)
	{
		associatePAWSSites((List<WFM_Site_Detail__c>) records);
		updateAssociatedPAWSSiteNames(selectExtendedWFMSites((List<WFM_Site_Detail__c>) records));
		updateFlowNames((List<WFM_Site_Detail__c>) records, (List<WFM_Site_Detail__c>) oldRecords);
	}
	
	private void associatePAWSSites(List<WFM_Site_Detail__c> wfmSites)
	{
		PAWS_API.associatePAWSProjectSiteWith(wfmSites);
	}

	private void updateAssociatedPAWSSiteNames(List<WFM_Site_Detail__c> wfmSites)
	{
		List<PAWS_Project_Flow_Site__c> pawsSites = selectAssociatedPAWSSites(wfmSites);
		List<PAWS_Project_Flow_Site__c> updatedPAWSSites = new List<PAWS_Project_Flow_Site__c>();
		Map<ID, WFM_Site_Detail__c> wfmSiteMap = new Map<ID, WFM_Site_Detail__c>(wfmSites);

		String siteName, piName, institutionName;
		for (PAWS_Project_Flow_Site__c pawsSite : pawsSites)
		{
			siteName = wfmSiteMap.get(pawsSite.WFM_Site__c).Name;
			institutionName = wfmSiteMap.get(pawsSite.WFM_Site__c).Account_Institution__r.Name;
			piName = wfmSiteMap.get(pawsSite.WFM_Site__c).Investigator__r.Name;

			if (wfmSiteMap.get(pawsSite.WFM_Site__c) != null &&
				(pawsSite.Name != siteName || pawsSite.PI_Name__c != piName || pawsSite.Institution_Name__c != institutionName))
			{
				pawsSite.Name = wfmSiteMap.get(pawsSite.WFM_Site__c).Name;
				pawsSite.PI_Name__c = piName;
				pawsSite.Institution_Name__c = institutionName;

				updatedPAWSSites.add(pawsSite);
			}

			Boolean haveValuesChanged = false;
			if (wfmSiteMap.get(pawsSite.WFM_Site__c) != null)
			{
				if (pawsSite.Name != wfmSiteMap.get(pawsSite.WFM_Site__c).Name)
				{
					pawsSite.Name = wfmSiteMap.get(pawsSite.WFM_Site__c).Name;
					haveValuesChanged = true;
				}
			}
		}

		update updatedPAWSSites;
	}

	private void updateFlowNames(List<WFM_Site_Detail__c> currentRecords, List<WFM_Site_Detail__c> priorRecords)
	{
		List<ID> pawsSiteIDs = new List<ID>();
		Map<ID, PAWS_Project_Flow_Site__c> pawsSiteByWFMSiteID = new Map<ID, PAWS_Project_Flow_Site__c>();
		for (PAWS_Project_Flow_Site__c each : [select WFM_Site__c, Name from PAWS_Project_Flow_Site__c where WFM_Site__c in :currentRecords])
		{
			pawsSiteByWFMSiteID.put(each.WFM_Site__c, each);
			pawsSiteIDs.add(each.Id);
		}

		Map<ID, List<STSWR1__Flow__c>> flowByPAWSSiteID = new Map<ID, List<STSWR1__Flow__c>>();
		for (STSWR1__Flow__c each : [select STSWR1__Source_Id__c, Name from STSWR1__Flow__c where STSWR1__Source_Id__c in :pawsSiteIDs])
		{
			if (flowByPAWSSiteID.get(each.STSWR1__Source_Id__c) == null)
			{
				flowByPAWSSiteID.put(each.STSWR1__Source_Id__c, new List<STSWR1__Flow__c>());
			}

			flowByPAWSSiteID.get(each.STSWR1__Source_Id__c).add(each);
		}

		List<STSWR1__Flow__c> updatedFlows = new List<STSWR1__Flow__c>();
		for (Integer i = 0, n = currentRecords.size(); i < n; i++)
		{
			WFM_Site_Detail__c currentRecord = currentRecords.get(i);
			WFM_Site_Detail__c priorRecord = (priorRecords == null || priorRecords.isEmpty() ? new WFM_Site_Detail__c() : priorRecords.get(i));
			PAWS_Project_Flow_Site__c pawsSite = pawsSiteByWFMSiteID.get(currentRecord.Id);

			if (currentRecord.Name == priorRecord.Name || pawsSite == null)
			{
				continue;
			}

			for (Integer j = 0; flowByPAWSSiteID.get(pawsSite.Id) != null && j < flowByPAWSSiteID.get(pawsSite.Id).size(); j++)
			{
				STSWR1__Flow__c flowRecord = flowByPAWSSiteID.get(pawsSite.Id).get(j);

				flowRecord.Name = flowRecord.Name.replaceAll('\\(.+\\)$', '') + '(' + currentRecord.Name + ')';
				updatedFlows.add(flowRecord);
			}
		}

		update updatedFlows;

		update [select Name from Critical_Chain__c where Flow__c in :updatedFlows];
	}
	/*
	private void updateAssociatedEWs(List<WFM_Site_Detail__c> currentRecords, List<WFM_Site_Detail__c> priorRecords)
	{
		Map<ID, PAWS_Project_Flow_Site__c> pawsSiteByWFMSiteID = new Map<ID, PAWS_Project_Flow_Site__c>();
		Map<ID, Critical_Chain__c> ewByPAWSSiteID = new Map<ID, Critical_Chain__c>();

		for (PAWS_Project_Flow_Site__c each : [select WFM_Site__c from PAWS_Project_Flow_Site__c where WFM_Site__c in :currentRecords])
		{
			pawsSiteByWFMSiteID.put(each.WFM_Site__c, each);
		}

		for (Critical_Chain__c each : [select PAWS_Project_Flow_Site__c, Name, Cloned_From_Early_Warning__r.Name from Critical_Chain__c where PAWS_Project_Flow_Site__c in :pawsSiteByWFMSiteID.values()])
		{
			ewByPAWSSiteID.put(each.PAWS_Project_Flow_Site__c, each);
		}

		List<PAWS_Project_Flow_Site__c> updatedSites = new List<PAWS_Project_Flow_Site__c>();
		List<Critical_Chain__c> updatedChains = new List<Critical_Chain__c>();
		for (Integer i = 0, n = currentRecords.size(); i < n; i++)
		{
			WFM_Site_Detail__c currentRecord = currentRecords.get(i);
			WFM_Site_Detail__c priorRecord = (priorRecords == null || priorRecords.isEmpty() ? new WFM_Site_Detail__c() : priorRecords.get(i));

			if (currentRecord.Name != priorRecord.Name)
			{
				continue;
			}

			PAWS_Project_Flow_Site__c pawsSite = pawsSiteByWFMSiteID.get(currentRecord.Id);
			if (pawsSite == null)
			{
				continue;
			}

			pawsSite.Name = currentRecord.Name;
			updatedSites.add(pawsSite);

			Critical_Chain__c ewRecord = ewByPAWSSiteID.get(pawsSite.Id);
			if (ewRecord == null)
			{
				continue;
			}

			if (ewRecord.Cloned_From_Early_Warning__r.Name != null)
			{
				//ewRecord.Name = ;
			//}
		}
	}
	/**/

	private List<PAWS_Project_Flow_Site__c> selectAssociatedPAWSSites(List<WFM_Site_Detail__c> wfmSites)
	{
		return [select Name, PI_Name__c, Institution_Name__c, WFM_Site__c from PAWS_Project_Flow_Site__c where WFM_Site__c in :wfmSites];
	}

	private List<WFM_Site_Detail__c> selectExtendedWFMSites(List<WFM_Site_Detail__c> wfmSites)
	{
		return [select Name, Account_Institution__r.Name, Investigator__r.Name from WFM_Site_Detail__c where Id in :wfmSites];
	}
}