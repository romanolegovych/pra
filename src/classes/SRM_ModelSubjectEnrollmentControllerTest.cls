/**
@author 
@date 2014
@description this test class for srm subject enrollement class
**/
@isTest
public with sharing class  SRM_ModelSubjectEnrollmentControllerTest{

    public static testMethod void testNewSubEnrollment(){
    
    SRM_TestUtils tu = new SRM_TestUtils();
    
    SRM_MOdel__c model = new SRM_MOdel__c();
    model = tu.createSrmModel();
    SRM_Model_Subject_Enrollment__c se = new SRM_Model_Subject_Enrollment__c();
    se = tu.createSubjectEnrollment();
    
    SRM_ModelSubjectEnrollmentcontroller subenroll = new SRM_ModelSubjectEnrollmentcontroller(new ApexPages.StandardController(se));
    subenroll.savesubjectEnrollment();
    subenroll.cancel();
    
    }
    
    public static testMethod void testEditSubEnrollment(){
        SRM_TestUtils tu = new SRM_TestUtils();
        
        SRM_MOdel__c model = new SRM_MOdel__c();
        model = tu.createSrmModel();
        SRM_Model_Subject_Enrollment__c se = new SRM_Model_Subject_Enrollment__c();
        se = tu.createSubjectEnrollment();
        
        PageReference pageRef = Page.SRM_ModelSiteActivation;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', se.Id);
        ApexPages.currentPage().getParameters().put('returl', se.Id);
             
        ApexPages.StandardController sc = new ApexPages.StandardController(se);
        SRM_ModelSubjectEnrollmentcontroller subenroll = new SRM_ModelSubjectEnrollmentcontroller(sc);
        
        subenroll.savesubjectEnrollment();
        subenroll.generateChartData();
        subenroll.cancel();
    }
    
    public static testMethod void testcloneSubEnrollment(){
        SRM_TestUtils tu = new SRM_TestUtils();
        
        SRM_MOdel__c model = new SRM_MOdel__c();
        model = tu.createSrmModel();
        SRM_Model_Subject_Enrollment__c se = new SRM_Model_Subject_Enrollment__c();
        se = tu.createSubjectEnrollment();
        
        PageReference pageRef = Page.SRM_ModelSiteActivation;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', se.Id);
        ApexPages.currentPage().getParameters().put('returl', se.Id);
        ApexPages.currentPage().getParameters().put('clone', '1');   
             
        ApexPages.StandardController sc = new ApexPages.StandardController(se);
        SRM_ModelSubjectEnrollmentcontroller subenroll = new SRM_ModelSubjectEnrollmentcontroller(sc);
        
        subenroll.savesubjectEnrollment();
        subenroll.generateChartData();
        subenroll.cancel();
    }
    
    public static testMethod void testvalidation(){
        SRM_TestUtils tu = new SRM_TestUtils();
        
        SRM_MOdel__c model = new SRM_MOdel__c();
        model = tu.createSrmModel();
        SRM_Model_Subject_Enrollment__c se = new SRM_Model_Subject_Enrollment__c();
        se = tu.createSubjectEnrollment();
        
        SRM_Model_Subject_Enrollment__c se2 = SRM_ScenarioDataAccessor.getSubEnrollById(se.id);
        
        se2.Standard_Deviation__c = -1;
        update se2;
        
        PageReference pageRef = Page.SRM_ModelSiteActivation;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('Id', se2.Id);
             
        ApexPages.StandardController sc = new ApexPages.StandardController(se2);
        SRM_ModelSubjectEnrollmentcontroller subenroll = new SRM_ModelSubjectEnrollmentcontroller(sc);
        
        subenroll.savesubjectEnrollment();
        subenroll.generateChartData();
        subenroll.cancel();
    }
}