public with sharing class PRA_ClientTaskSearchService {
	// Constant representing the select value option in the picklist
	public final String ST_NONE = 'None'; 
	
	// OrderBy constants
	public final Integer CT_ORDERBY_DESCRIPTION = 1;
	public final Integer CT_ORDERBY_CLIENT_UNIT = 2;
	
	// Constant representing description text for migrated worked hours
	public final String CT_MIGRATED_WORK_HOURS = 'Migrated Worked Hours';
	
	public String selectedProject {get;set;}

	public List<String> selectedOperationalAreas {get;set;}
	public List<String> selectedGroups {get;set;}
	public List<String> selectedTasks {get;set;}
	public List<String> selectedRegions {get;set;}

	public List<SelectOption> regionOptions {get;set;}
	public List<SelectOption> operationalAreaOptions {get;set;}
	public List<SelectOption> groupOptions {get;set;}
	public List<SelectOption> taskOptions {get;set;}

	// Filter "Migrated Work Hours" when true
	public Boolean filterMigrated = false;
	
	// OrderBy property
	public Integer orderBy = CT_ORDERBY_DESCRIPTION;
	
	// SOQL Date string representing the earliest month
	// of the data range for rettrieving UnitEffort records
	public String ueStartMonth = 'THIS_MONTH'; 
	
	// Filter Closed Client Units
	public Boolean filterClosedUnits = false;
	// Filter Offset represents the number of months of offset from
	// the current month to consider when determining the closed status of 
	// Client Task records.
	// For instance an offset of -1 calculates closed status from the perspective
	// of last month (month - 1)
	public Integer filterOffset = 0;
	
	private Datetime startOfMonth;
	private String startOfMonthSQQL;
	 	
	public PRA_ClientTaskSearchService() {
		selectedProject = ST_NONE;
		selectedOperationalAreas = new String[]{ST_NONE};
		selectedGroups = new String[]{ST_NONE};
		selectedTasks = new String[]{ST_NONE};
		selectedRegions = new String[]{ST_NONE};
		
		startOfMonth = Datetime.newInstance(Date.today().year(), Date.today().month(), 1);
		startOfMonthSQQL = startOfMonth.format('yyyy-MM-dd');
	}

	// Retrieve the list of CRCT tasks which form the basis of the rows created
	public List<Client_Task__c> getTasks() {
		startOfMonthSQQL = (startOfMonth.addMonths(filterOffset)).format('yyyy-MM-dd');
		system.debug('---- startOfMonthSQQL: ' + startOfMonthSQQL);
		
		String queryTasks = 'SELECT Id, Description__c, Client_Unit_Number__c,Combo_Code__c, Project__r.Name,' 
			+' Unit_of_Measurement__c, Project__r.Estimated_End_Date__c, Task_Group__r.Operational_Area__c,'
			+' Task_Group__r.Operational_Area__r.Name, Total_Worked_Units__c, Total_Units__c,'
			+' Project_Region__r.Name,Project__r.Migrated_Date__c'
			+' FROM Client_Task__c'
			+' WHERE Project__c = \'' + selectedProject +'\' AND Project__r.Estimated_End_Date__c >= ' + startOfMonthSQQL;

		if(selectedOperationalAreas[0] != ST_NONE) {
			queryTasks += ' AND Task_Group__r.Operational_Area__c IN :selectedOperationalAreas';
		}
		if(selectedGroups[0] != ST_NONE) {
			queryTasks += ' AND Task_Group__c IN :selectedGroups';
		}
		if(selectedTasks[0] != ST_NONE) {
			queryTasks += ' AND Id IN :selectedTasks';
		}
		if(selectedRegions[0] != ST_NONE) {
			queryTasks += ' AND Project_Region__c IN :selectedRegions';
		}
		if (filterMigrated) {
			queryTasks += ' AND Description__c != :CT_MIGRATED_WORK_HOURS';
		}
		if (filterClosedUnits) {
			queryTasks += ' AND (Close_Date__c = NULL OR Close_Date__c > ' + startOfMonthSQQL + ')'; 
		}
		
		if (orderBy == CT_ORDERBY_DESCRIPTION) {
			queryTasks += ' ORDER BY Description__c';
		} else if (orderBy == CT_ORDERBY_CLIENT_UNIT) {
			queryTasks += ' ORDER BY Client_Unit_Number__c';
		}
		
		System.debug('queryTasks: ' + queryTasks);
		
		List<Client_Task__c> tasks;
		
		try {
			tasks = (List<Client_Task__c>)Database.query(queryTasks);
		}
		catch (Exception e) {
			system.debug('----- Exception in getTasks(): ' + e);
			tasks = new List<Client_Task__c>();
		}
		
		System.debug('ctList size()' + tasks.size());
		System.debug('ctList' + tasks);
		
		return tasks;
	}

	// Returns a list of Unit_Effort__c records based on
	// client task filters
	public List<Unit_Effort__c> getUnitEfforts() {
		startOfMonthSQQL = (startOfMonth.addMonths(filterOffset)).format('yyyy-MM-dd');
		system.debug('---- startOfMonthSQQL: ' + startOfMonthSQQL);

		List<Unit_Effort__c> unitEfforts;
		
		String queryUnit = 'SELECT Is_null__c, Month_Applies_To__c, Month_Forecast_Created__c, Is_Confirmed_Worked_Unit__c, Include_Adjustment__c,' 
			+ ' Worked_Unit__c, Source_Worked_Unit__c, Last_Approved_Unit__c, Client_Task__r.Total_Contract_Units__c,'
			+ ' Client_Task__r.Description__c, Client_Task__c, Client_Task__r.Total_Worked_Units__c, Client_Task__r.Total_Baseline_Units__c,'
			+ ' Client_Task__r.Unit_of_Measurement__c, Client_Task__r.Client_Unit_Number__c,'
			+ ' Client_Task__r.Task_Group__r.Operational_Area__c,'
			+ ' Client_Task__r.Task_Group__r.Operational_Area__r.Name, Client_Task__r.Country__r.Name,'
			+ ' Client_Task__r.Project__r.Migrated_Date__c,'
			+ ' Client_Task__r.Project_Region__r.Name'
			+ ' FROM Unit_Effort__c'
			+ ' WHERE Client_Task__r.Project__c = \'' + selectedProject +'\''
			+ ' and Client_Task__r.Client_Unit_Number__c!=\'' + CT_MIGRATED_WORK_HOURS +'\'';

		if(selectedOperationalAreas[0] != ST_NONE) {
			queryUnit += ' and Client_Task__r.Task_Group__r.Operational_Area__c IN :selectedOperationalAreas';
		}
		if(selectedGroups[0] != ST_NONE) {
			queryUnit += ' and Client_Task__r.Task_Group__c IN :selectedGroups';
		}
		if(selectedTasks[0] != ST_NONE) {
			queryUnit += ' and Client_Task__c IN :selectedTasks';
		}
		if(selectedRegions[0] != ST_NONE) {
			queryUnit += ' and Client_Task__r.Project_Region__c IN :selectedRegions';
		}
		if (filterClosedUnits) {
			queryUnit += ' AND (Client_Task__r.Close_Date__c = NULL OR Client_Task__r.Close_Date__c > ' + startOfMonthSQQL + ')'; 
		}
		queryUnit += ' AND Month_Applies_To__c >= ' + ueStartMonth
				  +  ' and Month_Applies_To__c <= THIS_MONTH';
		queryUnit += ' order by Client_Task__r.Description__c';

		try {
			unitEfforts = (List<Unit_Effort__c>)Database.query(queryUnit);
		}
		catch (Exception e) {
			system.debug('----- Exception in getUnitEfforts(): ' + e);
			unitEfforts = new List<Unit_Effort__c>();
		}

		return unitEfforts;
	}

	// Returns a list of Regional_Contract_Value__c records based on
	// client task filters
	public List<Regional_Contract_Value__c> getRegionalContractValues() {
		startOfMonthSQQL = (startOfMonth.addMonths(filterOffset)).format('yyyy-MM-dd');
		system.debug('---- startOfMonthSQQL: ' + startOfMonthSQQL);

		List<Regional_Contract_Value__c> regionalContractVals;

		String rcv = 'select Id,Client_Task__r.Project__r.Project_Currency__c,Client_Task__r.Description__c,Client_Task__r.Client_Unit_Number__c,'
			+ ' Client_Task__r.Unit_of_Measurement__c, Client_Task__r.Project_Region__r.name, Contract_Value_Per_Unit__c,'
			+ ' Client_Task__c, Effective_Date__c, Europe_Africa__c, Latin_America__c, US_Canada__c, Asia_Pacific__c'
			+ ' from Regional_Contract_Value__c'
			+ ' where Client_Task__r.Project__c=\'' + selectedProject +'\'  and Client_Task__r.Client_Unit_Number__c!=\''+ CT_MIGRATED_WORK_HOURS +'\''
			+ ' and Client_Task__r.Contract_Value__c != 0 and Is_Active__c = true';
		if(selectedOperationalAreas[0] != ST_NONE) {
			rcv += ' and Client_Task__r.Task_Group__r.Operational_Area__c IN :selectedOperationalAreas';
		}
		if(selectedGroups[0] != ST_NONE) {
			rcv += ' and Client_Task__r.Task_Group__c IN :selectedGroups';
		}
		if(selectedTasks[0] != ST_NONE) {
			rcv += ' and Client_Task__c IN :selectedTasks';
		}
		if(selectedRegions[0] != ST_NONE) {
			rcv += ' and Client_Task__r.Project_Region__c IN :selectedRegions';            
		}
		if (filterClosedUnits) {
			rcv += ' AND (Client_Task__r.Close_Date__c = NULL OR Client_Task__r.Close_Date__c > ' + startOfMonthSQQL + ')'; 
		}
		rcv +=' order by Effective_Date__c desc';

		try {
			regionalContractVals = (List<Regional_Contract_Value__c>)Database.query(rcv);
		}
		catch (Exception e) {
			system.debug('----- Exception in getRegionalContractValues(): ' + e);
			regionalContractVals = new List<Regional_Contract_Value__c>();
		}
		
		return regionalContractVals;
	}
	
	// Selecting a Project updates OA and Regions
	public void initOnSelectProject() {
		system.debug('----- initOnSelectProject().selectedProject: '+selectedProject);
		if(selectedProject == ST_NONE) {
			selectedOperationalAreas = new String[]{ST_NONE};
			selectedGroups = new String[]{ST_NONE};
			selectedTasks = new String[]{ST_NONE};
			selectedRegions = new String[]{ST_NONE};
		}
		initRegionOptions();
		initOperationalAreaOptions();
	}

	// Init of Regions
	public void initRegionOptions() {
		regionOptions = new List<SelectOption>();
		regionOptions.add(new SelectOption(ST_NONE, ST_NONE));
		if(selectedRegions == null) {
			selectedRegions = new String[]{ST_NONE};
		}

		// Return if no project is selected
		if(selectedProject == ST_NONE) {
			return;
		}

		Set<String> setUniqueRegion = new Set<String>();
		setUniqueRegion.add(ST_NONE);
		for(Client_Task__c pr : [SELECT Project_Region__c, Project_Region__r.Name FROM Client_Task__c
			WHERE Project__c = :selectedProject ORDER BY Project_Region__r.Name asc]) {
			if(!setUniqueRegion.contains(pr.Project_Region__c)) {
				setUniqueRegion.add(pr.Project_Region__c);
				regionOptions.add(new SelectOption(pr.Project_Region__c, pr.Project_Region__r.Name));
			}
		}

		// Make sure prior selections are still valid with the new selection list
		List<String> newSelect = new List<String>();
		for (String sel : selectedRegions) {
			if (setUniqueRegion.contains(sel)) {
				newSelect.add(sel);
			}
		}
		selectedRegions = newSelect;
		if(selectedRegions.size() < 1) {
			selectedRegions = new String[]{ST_NONE};
		}
	}

	// Init of the Operational Area Picklist
	public void initOperationalAreaOptions() {
		operationalAreaOptions = new List<SelectOption>();
		operationalAreaOptions.add(new SelectOption(ST_NONE, ST_NONE));
		if(selectedOperationalAreas == null) {
			selectedOperationalAreas = new String[]{ST_NONE};
		}
		if(selectedProject == ST_NONE) {
			initGroupOptions();
			return;
		}
		Set<String> setUniqueOA = new Set<String>();
		setUniqueOA.add(ST_NONE);
		for(Client_Task__c oa: [SELECT Task_Group__r.Operational_Area__r.Name FROM Client_Task__c WHERE Project__c = :selectedProject 
			ORDER BY Task_Group__r.Operational_Area__r.Name asc]) {
			if(!setUniqueOA.contains(oa.Task_Group__r.Operational_Area__c)) {
				setUniqueOA.add(oa.Task_Group__r.Operational_Area__c);
				operationalAreaOptions.add(new SelectOption(oa.Task_Group__r.Operational_Area__c, oa.Task_Group__r.Operational_Area__r.Name));
			}
		}

		// Make sure prior selections are still valid with the new selection list
		List<String> newSelect = new List<String>();
		for (String sel : selectedOperationalAreas) {
			if (setUniqueOA.contains(sel)) {
				newSelect.add(sel);
			}
		}
		selectedOperationalAreas = newSelect;
		if(selectedOperationalAreas.size() < 1) {
			selectedOperationalAreas = new String[]{ST_NONE};
		}
		initGroupOptions();
	}

	// Init of Group
	public void initGroupOptions() {
		groupOptions = new List<SelectOption>();
		groupOptions.add(new SelectOption(ST_NONE, ST_NONE));
		if(selectedGroups == null) {
			selectedGroups = new String[]{ST_NONE};
		}

		if(selectedProject == ST_NONE) {
			initTaskOptions();
			return;
		}

		Set<String> setUniqueGroup = new Set<String>();
		setUniqueGroup.add(ST_NONE);
		String req = 'SELECT Task_Group__r.Name'
			 + ' FROM Client_Task__c'
			 + ' WHERE Project__c = \'' +selectedProject + '\'';
		if(!selectedOperationalAreas[0].equals(ST_NONE)) {
			req += ' AND Task_Group__r.Operational_Area__c IN :selectedOperationalAreas ';
		}
		req += ' ORDER BY Task_Group__r.Name asc';
		for(Client_Task__c ct : Database.query(req)) {
			if(!setUniqueGroup.contains(ct.Task_Group__c)) {
				setUniqueGroup.add(ct.Task_Group__c);
				groupOptions.add(new SelectOption(ct.Task_Group__c, ct.Task_Group__r.Name));
			}
		}

		// Make sure prior selections are still valid with the new selection list
		List<String> newSelect = new List<String>();
		for (String sel : selectedGroups) {
			if (setUniqueGroup.contains(sel)) {
				newSelect.add(sel);
			}
		}
		selectedGroups = newSelect;
		if(selectedGroups.size() < 1) {
			selectedGroups = new String[]{ST_NONE};
		}
		initTaskOptions();
	}

	// Init of Task
	public void initTaskOptions() {
		taskOptions = new List<SelectOption>();
		taskOptions.add(new SelectOption(ST_NONE, ST_NONE));
		if(selectedTasks == null) {
			selectedTasks = new String[]{ST_NONE};
		}
		
		if(selectedProject == ST_NONE) {
			return;
		}

		Set<String> setUniqueTasks = new Set<String>();
		setUniqueTasks.add(ST_NONE);
		String req = 'SELECT Id, Client_Unit_Number__c, Description__c'
			 + ' FROM Client_Task__c'
			 + ' WHERE Project__c = \'' +selectedProject + '\'';
		if(!selectedOperationalAreas[0].equals(ST_NONE)) {
			req += ' AND Task_Group__r.Operational_Area__c IN :selectedOperationalAreas ';
		}
		if(!selectedGroups[0].equals(ST_NONE)) {
			req += ' AND Task_Group__c IN :selectedGroups';
		}
		req += ' ORDER BY Client_Unit_Number__c asc';
		for(Client_Task__c ct : Database.query(req)){
			if(!ct.Description__c.contains('Migrated Worked Hours')) {
				setUniqueTasks.add(ct.Id);
				taskOptions.add(new SelectOption(ct.Id, ct.Client_Unit_Number__c + ' - ' + ct.Description__c));
			}
		}

		// Make sure prior selections are still valid with the new selection list
		List<String> newSelect = new List<String>();
		for (String sel : selectedTasks) {
			if (setUniqueTasks.contains(sel)) {
				newSelect.add(sel);
			}
		}
		selectedTasks = newSelect;
		if(selectedTasks.size() < 1) {
			selectedTasks = new String[]{ST_NONE};
		}
	}
}