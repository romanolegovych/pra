@RestResource(urlMapping='/CV_ConnectPendingNotifications/*')
global with sharing class CV_ConnectPendingNotifications{
	
    @HttpGet
    global static NotificationsResult getPendingNotificationsByUser(){
        try{
            List<NCM_API_DataTypes.NotificationWrapper> notifications = NCM_API.getPendingNotificationsByUser( UserInfo.getUserId() );
            return new NotificationsResult( PC_Utils.populateNotificationsWithProtocolNames(notifications) );
        }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return new NotificationsResult( e.getMessage() );
        }
    }
        
	global class NotificationsResult{
	    
	        global String errorMessage { get; private set; }
	        
	        global List<NCM_API_DataTypes.NotificationWrapper> returnedValues { get; private set; }
	        
	        public NotificationsResult(List<NCM_API_DataTypes.NotificationWrapper> returnedValues ){
	            this.returnedValues = returnedValues ;
	        }
	        
	        public NotificationsResult(String errorMessage){
	            this.errorMessage = errorMessage;
	        }
	}
}