public with sharing class RM_ViewMyResourcesController {
	
	public string userName{get;private set;}
	public string folderId{get; set;}
	public string folderText{get;set;}
	public list<Employee_Details__c> resourceJsonData{get;set;}
	//public string resourceId{get; private set;}
	public string destLink{get; private set;}
	private string resources{get; private set;}
	
	public RM_ViewMyResourcesController() {
                
        userName = UserInfo.getName();
        user u = RM_OtherService.GetUserInfo(UserInfo.getUserId());
        if (u != null){        
        	resourceJsonData= getListResource(u.email); 
        	 system.debug('-------resourceJsonData---------'+resourceJsonData);
        }
    } 
  
    
    private list<Employee_Details__c> getListResource(string userEmail){
    	list<Employee_Details__c> resourcesList = new list<Employee_Details__c>();
    	list<Employee_Details__c> supervisor = RM_EmployeeService.GetEmployeeDetailByEmployeeEmail(userEmail);
    	  system.debug('-------supervisor---------'+supervisor);
    	if (supervisor.size() > 0){
    		folderID = supervisor[0].name;
    		//Map<string, integer> mapPropose = getProposedAllocation(supervisor[0].name);
    		resourcesList = RM_EmployeeService.GetEmployeeDetailByEmployeeSupervisor(supervisor[0].name);
    		
    		
    		//lstName = RM_Tools.GetListStrfromListObject('Full_Name__c', resourcesList);
    	}
    	return resourcesList;
    }
   
    /*private Map<string, integer> getProposedAllocation(string supervisorID){
    	AggregateResult[] proposedAlls = RM_AssignmentsService.GetProposedAllocationCountByEmployeeSupervisor(supervisorID);
    	Map<string, integer> mapPropose = new Map<string, integer>();
    	for(AggregateResult a: proposedAlls)
            mapPropose.put((string)a.get('EMPLOYEE_ID__c'), (integer)a.get('nProposed'));
        return mapPropose;
    }*/
}