/**
 * @description	Implements the Interface for checking if a topic occured under a related to id and for creating the
 *				email subject(SMS text and Chatter post) and email body.
 *				for the notification event
 * @author		Dimitrios Sgourdos
 * @date		Created: 10-Sep-2015, Edited: 24-Sep-2015
 */
global interface NCM_NotificationEventInterface {
	
	/**
	 * @description	Check if the notification topic occured under the given related to id
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 10-Sep-2014
	 * @param		notificationCatalogId    The id of the notification topic that is checked
	 * @param		relatedToId              The related object id that possible triggers the notification topic
	 * @return		The result if the notification topic occured under the given related to id.
	*/
	Boolean didNotificationTopicOccur(Id notificationCatalogId, String relatedToId);
	
	
	/**
	 * @description	Get the notification event subject. This subject will be used as an email subject, as a SMS text
	 *				and as a Chatter post in the communication with the subscribed users
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 10-Sep-2014
	 * @param		notificationCatalogId    The id of the notification topic that was triggered
	 * @param		relatedToId              The related object id that triggered the notification topic
	 * @return		The notification event subject.
	*/
	String getNotificationSubject(Id notificationCatalogId, String relatedToId);
	
	
	/**
	 * @description	Get the notification event email body. This will be used as the email body in the communication
	 *				with the subscribed users
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 10-Sep-2014
	 * @param		notificationCatalogId    The id of the notification topic that was triggered
	 * @param		relatedToId              The related object id that triggered the notification topic
	 * @return		The notification event email body.
	*/
	String getNotificationEmailBody(Id notificationCatalogId, String relatedToId);
}