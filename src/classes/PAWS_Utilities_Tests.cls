@isTest
public with sharing class PAWS_Utilities_Tests
{
	@istest private static void testPAWSUtilities()
	{
		PAWS_ApexTestsEnvironment.init();
		STSWR1__Flow_Instance_Cursor__c flowInstanceCursor = PAWS_ApexTestsEnvironment.FlowInstanceCursor;
		
		//test "formatDate" method
		String formatedDate = PAWS_Utilities.formatDate(null, '');
		formatedDate = PAWS_Utilities.formatDate(date.today(), 'yyyy-mm-dd');
		
		//test "getDefaultDateFormat" method
		String defaultDateFormat = PAWS_Utilities.getDefaultDateFormat();
		
		//test "getDefaultDateTimeFormat" method
		String defaultDateTimeFormat = PAWS_Utilities.getDefaultDateTimeFormat();
		
		//test "isLimitException" method
		Boolean isLimitEx = PAWS_Utilities.isLimitException(new PAWS_Utilities.PAWSException('test'));
		
		//test check limit...
		PAWS_Utilities.checkLimits(new Map<String, Integer>());
		PAWS_Utilities.checkLimit('Queries', 1);
		
		//test "loadAssigners" method
		Map<ID, String> assigners = PAWS_Utilities.loadAssigners(new List<STSWR1__Flow_Instance_Cursor__c>{flowInstanceCursor});
		
		//test "getSObjectTypeById" method
		sObjectType sObjectTypeById = PAWS_Utilities.getSObjectTypeById('123');
		sObjectTypeById = PAWS_Utilities.getSObjectTypeById((String) flowInstanceCursor.Id);
		
		//test "extractValue" method
		STSWR1__Flow_Instance_Cursor__c cursor = [Select STSWR1__Flow_Instance__r.STSWR1__Flow__r.Name From STSWR1__Flow_Instance_Cursor__c Where Id = :flowInstanceCursor.Id];
		Object extractedValue = PAWS_Utilities.extractValue(cursor, 'STSWR1__Flow_Instance__r.STSWR1__Flow__r.Name');
		
		//test "extractFieldDescribe" method
		Schema.DescribeSObjectResult dsr = STSWR1__Flow_Instance_Cursor__c.sObjectType.getDescribe();
		Schema.DescribeFieldResult fieldResult = PAWS_Utilities.extractFieldDescribe(dsr, 'stswr1__flow_instance__r.stswr1__flow__r.name');
		
		//test "makeQuery"/"updatePAWSFlag" method
		List<sObject> results = PAWS_Utilities.makeQuery('stswr1__flow_instance_cursor__c', 'Id = \'' + flowInstanceCursor.Id + '\'', new Set<String>{'stswr1__flow_instance__r.name'});
		PAWS_Utilities.updatePAWSFlag((List<STSWR1__Flow_Instance_Cursor__c>) results);
	}
	
	@istest(seealldata=true) private static void testLoadAssigners()
	{
		Map<ID, String> results;
		
		Map<ID, User> users = new Map<ID, User>([Select Id From User limit 1]);
		Map<ID, UserRole> userRoles = new Map<ID, UserRole>([Select Id From UserRole limit 1]);
		Map<ID, Group> groups = new Map<ID, Group>([Select Id From Group limit 1]);
		Map<ID, CollaborationGroup> callaborationGroups = new Map<ID, CollaborationGroup>([Select Id From CollaborationGroup limit 1]);
		Map<ID, Profile> profiles = new Map<ID, Profile>([Select Id From Profile limit 1]);
		
		results = PAWS_Utilities.loadAssigners(new Map<String, Set<ID>>{'Field' => users.keySet()});
		results = PAWS_Utilities.loadAssigners(new Map<String, Set<ID>>{'User' => users.keySet()});
		results = PAWS_Utilities.loadAssigners(new Map<String, Set<ID>>{'Role' => userRoles.keySet()});
		results = PAWS_Utilities.loadAssigners(new Map<String, Set<ID>>{'Group' => groups.keySet()});
		results = PAWS_Utilities.loadAssigners(new Map<String, Set<ID>>{'Chatter Group' => callaborationGroups.keySet()});
		results = PAWS_Utilities.loadAssigners(new Map<String, Set<ID>>{'Profile' => profiles.keySet()});
	}
}