@isTest
private class BDT_NewEditSiteTest {
	
	public static List<Business_Unit__c> myBUList {get;set;}
	
	static void init(){
		myBUList = BDT_TestDataUtils.buildBusinessUnit(1);
		insert myBUList;
	}
	

    static testMethod void createSiteBUPath() { 
    	init();

		System.currentPageReference().getParameters().put('buId', myBUList[0].id);
		BDT_NewEditSite newSite = new BDT_NewEditSite();
		
		newSite.site.Abbreviation__c = 'BBB';
		newSite.site.Address_Line_1__c = 'Some address 1';
		
		System.assertNotEquals(null, newSite.save());
		
		System.currentPageReference().getParameters().put('siteId', newSite.site.id);
		BDT_NewEditSite editSite = new BDT_NewEditSite();
		
		System.assertNotEquals(null, editSite.cancel());
		
	}
	
	static testMethod void failEditBUPath() { 
		
		BDT_NewEditBusinessUnit newEditBU2 = new BDT_NewEditBusinessUnit();
		
		System.assertNotEquals(null, newEditBU2.businessUnit);
		
	}
	
	static testMethod void failEditBUPath2() { 
		System.currentPageReference().getParameters().put('siteId', '111111111111');
		BDT_NewEditBusinessUnit newEditBU2 = new BDT_NewEditBusinessUnit();
		
		System.assertNotEquals(null, newEditBU2.businessUnit);
		
	}
	
	static testMethod void deleteSitePath() { 
    	init();

		System.currentPageReference().getParameters().put('customId', myBUList[0].id);
		BDT_NewEditSite newSite = new BDT_NewEditSite();
		
		newSite.site.Abbreviation__c = 'BBB';
		newSite.site.Address_Line_1__c = 'Some address 1';
		
		System.assertNotEquals(null, newSite.save());
		
		System.currentPageReference().getParameters().put('siteId', newSite.site.id);
		BDT_NewEditSite editSite = new BDT_NewEditSite();
		
		System.assertNotEquals(null, editSite.deleteSoft());
		
	}
}