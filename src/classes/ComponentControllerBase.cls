public with sharing virtual class ComponentControllerBase {

  public LayoutModelController layoutModelController { get; 
    set {
      if (value != null) {
	layoutModelController = value;
	layoutModelController.setComponentController(this);
      }
    }
  }
}