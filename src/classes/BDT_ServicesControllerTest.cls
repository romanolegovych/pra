@isTest
private class BDT_ServicesControllerTest {

    static testMethod void myUnitTest() {
       BDT_ServicesController a = new BDT_ServicesController();
       System.assertEquals(false, (null == a));
       
       a.getServicesPerServiceCategory();
       System.assertEquals(true, (null != a.myServices)); 
       
       
       System.assertEquals(false, (a.showDeletedServices)); 
       System.assertEquals(false, (a.showDeletedCategories));
       a.showDeletedServices = true;
       a.showDeletedCategories = true;
       System.assertEquals(true, (a.showDeletedServices)); 
       System.assertEquals(true, (a.showDeletedCategories));
       
       ServiceCategory__c sc = new ServiceCategory__c(name = 'Test',code__c = '001.001');
       upsert sc;
       
       a.serviceCategoryId = sc.id;
       System.assertEquals(true, (sc.id == a.serviceCategoryId));
       
       Service__c s = new Service__c(name = 'Test', ServiceCategory__c = sc.id);
       upsert s;
       
       a.buildServiceList( sc.id );
       System.assertEquals(false, (null == a.myServices));  
       
       List<ServiceCategory__c> scl =  a.getServiceCatReplaced();
       System.assertEquals(false, (null == scl));
        
       System.assertEquals(true, (null == a.getServiceCategoryName()));
       
       
       System.assertEquals(false, (null == a.editServiceCategory()));
       System.assertEquals(false, (null == a.editService()));
       System.assertEquals(false, (null == a.getServices()));
       System.assertEquals(false, (null == a.getShowDetails()));
       
       
       System.currentPageReference().getParameters().put('ScId', s.id);
       a = new BDT_ServicesController();
       System.currentPageReference().getParameters().put('ScId', sc.id);
       a = new BDT_ServicesController();
       
       PageReference p = a.createService();
    }
}