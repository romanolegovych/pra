global with sharing class BDT_DeleteAttachments implements Schedulable {
	public String attachID;
		
	global BDT_DeleteAttachments(String attachID) {
		this.attachID = attachID;
	}
		
	global void execute(SchedulableContext SC) {
		List<Attachment> listAttach = [SELECT id, ParentId  FROM Attachment WHERE id = :attachID];
		delete listAttach;	
	}
}