/** Implements the Service Layer of the object FinancialCatTotPrice__c
 * @author	Dimitrios Sgourdos
 * @version	17-Dec-2013
 */
public with sharing class FinancialCatTotPriceService {
	
	/** Map the given list of Financial Category Total Prices on Service Category.
	 * @author	Dimitrios Sgourdos
	 * @version 17-Dec-2013
	 * @param	sourceList			The given list of Financial Category Total Prices
	 * @return	The created map.
	 */
	public static Map<String, List<FinancialCatTotPrice__c>> mapFinancialCatTotPriceListOnServiceCategory (
																			List<FinancialCatTotPrice__c> sourceList) {
		// Declare variables
		Map<string,list<FinancialCatTotPrice__c>> results = new Map<string,list<FinancialCatTotPrice__c>> ();
		
		for (FinancialCatTotPrice__c tmpItem : sourceList) {
			// Create key
			String key = '';
			if(tmpItem.FinancialCategoryTotal__r.ServiceCategory__c != NULL) {
				key = tmpItem.FinancialCategoryTotal__r.ServiceCategory__c;
			}
			
			// Update value
			List<FinancialCatTotPrice__c> newList = new List<FinancialCatTotPrice__c>();
			if (results.containsKey(Key) ) {
				newList = results.remove(Key);
			}
			newList.add(tmpItem);
			
			results.put(key, newList);
		}
		
		return results;
	}
}