/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_EmployeeWorkHistoryUTest {

   static COUNTRY__c country;
    static list<WFM_Location_Hours__c> hrs;
    static Employee_Details__c empl;
    static list<WFM_Employee_Availability__c> avas;
    
   
    static WFM_Client__c client;
    static WFM_Contract__c  contract;
    static WFM_Project__c project;   
   
 
    static WFM_EE_Work_History_Summary__c histSummary;
    
    static void init(){
        list<string> months = RM_Tools.GetMonthsList(-4, 5);
        country = new COUNTRY__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0);
        insert country;
        
        hrs = new List<WFM_Location_Hours__c>();
        for (string month:months){
            WFM_Location_Hours__c hr = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + month,YEAR_MONTH__c=month,NBR_Business_Day_In_Month__c=22);
            hrs.add(hr); 
        }
        insert hrs; 
        empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='1000', Employee_Unique_Key__c='1000', First_Name__c='John', Last_Name__c='Smith',email_Address__c='JSmith@gmail.com', RM_Comment__c='Test',
        Function_code__c='PR',Buf_Code__c='KCIPR', Business_Unit__c='BUC', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), FTE_Equivalent__c=1 );
        insert empl;
        avas = new list<WFM_Employee_Availability__c>();
      
        for (integer i = 0; i <months.size(); i++){
            WFM_Employee_Availability__c a = new WFM_Employee_Availability__c(EMPLOYEE_ID__c=empl.id, year_month__c=months[i], Availability_Unique_Key__c=empl.id+months[i],  
            Location_Hours__c=hrs[i].id);
            avas.add(a);
        }
        insert avas;
        //allocation
        
        client = new WFM_Client__c(name='TestClient', Client_Name__c='TestClinet_Name', Client_Unique_Key__c='TestClient');
        insert client;
        contract = new WFM_Contract__c(name='TestContract', Contract_Status__c='AA', Client_ID__c=client.id, Contract_Unique_Key__c='TestContract');
        insert contract;
        project = new WFM_Project__c(name='TestProject', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProject');
        insert project;
       
        //history summary
        histSummary = new WFM_EE_Work_History_Summary__c(Project__c=project.id, employee_id__c=empl.id, Buf_Code__c='KCIPR', Start_Year_Month__c='07/2011', End_Year_Month__c='07/2012', Sum_Worked_Hours__c=200, Work_History_Summary_Unique_Key__c=empl.id+(string)project.id+'KCIPR');
        insert histSummary;
    }
    static testMethod void TestHistory() {

        
     
        //need to insert project
        init();
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/RM_EmployeeWorkhistory?ID='+ empl.id );
        Test.setCurrentPage(pageRef);
        RM_EmployeeWorkHistoryController detail = new RM_EmployeeWorkHistoryController();
        
        List<RM_EmployeeWorkHistoryController.WorkHistory> hist = detail.GetHistory();
        system.assert(hist[0].Total_work_hr == 200);
        system.debug('---Sponsor---' + hist[0].Sponsor);
        system.assert(hist[0].Sponsor == 'TestClient');
        
        Test.stopTest();
        
      }
}