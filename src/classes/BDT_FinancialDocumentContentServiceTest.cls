/** Implements the test for the Service Layer of the object FinancialDocumentContent__c
 * @author	Dimitrios Sgourdos
 * @version	11-Feb-2014
 */
@isTest
private class BDT_FinancialDocumentContentServiceTest {
	
	/** Test the function getContentByDocumentId.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	11-Feb-2014
	 */
	static testMethod void getContentByDocumentIdTest() {
		FinancialDocumentContent__c result = BDT_FinancialDocumentContentService.getContentByDocumentId('');
	}
}