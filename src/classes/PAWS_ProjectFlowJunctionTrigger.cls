public with sharing class PAWS_ProjectFlowJunctionTrigger extends STSWR1.AbstractTrigger
{
	public override void afterDelete(List<SObject> records)
	{
		Set<ID> folderIds = new Set<ID>();
		for(PAWS_Project_Flow_Junction__c record : (List<PAWS_Project_Flow_Junction__c>)records)
		{
			if(!String.isEmpty(record.Folder__c)) folderIds.add(record.Folder__c);
		}
		
		PAWS_Utilities.checkLimits(new Map<String, Integer> 
		{
			'Queries' => 1,
			'DMLStatements' => 1
		});
		
		delete [select Id from STSWR1__Item__c where Id in :folderIds];
	}
}