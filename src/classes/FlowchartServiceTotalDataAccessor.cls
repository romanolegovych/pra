/** Implements the Selector Layer of the object FlowchartServiceTotal__c
 * @author	Dimitrios Sgourdos
 * @version	29-Jan-2014
 */
public with sharing class FlowchartServiceTotalDataAccessor {
	
	/** Object definition for fields used in application for FlowchartServiceTotal
	 * @author	Dimitrios Sgourdos
	 * @version 29-Jan-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('FlowchartServiceTotal__c');
	}
	
	
	/** Object definition for fields used in application for FlowchartServiceTotal with the parameter referenceName 
	 *	as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 29-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'ExplanationJSON__c,';
		result += referenceName + 'flowchartassignment__c,';
		result += referenceName + 'ProjectService__c,';
		result += referenceName + 'Study__c,';
		result += referenceName + 'Study_ProjectService_Combination__c,';
		result += referenceName + 'TotalUnits__c';
		return result;
	}
	
	
	/** Retrieve the list of FlowchartServiceTotals that meets the given criteria.
	 * @author	Dimitrios Sgourdos
	 * @version 29-Jan-2014
	 * @param	whereClause			The criteria that the StudyServices must meet
	 * @param	orderByField		The field that the StudyServices will be ordered by
	 * @return	The list of FlowchartServiceTotals
	 */
	public static List<FlowchartServiceTotal__c> getFlowchartServiceTotalList(String whereClause, String orderByField) {
		// Check parmateres
		String tmpWhereClause  = String.IsBlank(whereClause)?  '' : 'WHERE ' + whereClause;
		String tmpOrderByField = String.IsBlank(orderByField)? '' : 'ORDER BY ' + orderByField;
		
		String query = String.format(
								'SELECT {0}, {1}, {2}, {3} ' +
								'FROM FlowchartServiceTotal__c ' +
								'{4} {5}',
								new List<String> {
									getSObjectFieldString(),
									FlowchartAssignmentDataAccessor.getSObjectFieldString('flowchartassignment__r'),
									ProjectServiceDataAccessor.getSObjectFieldString('ProjectService__r'),
									StudyDataAccessor.getSObjectFieldString('Study__r'),
									tmpWhereClause,
									tmpOrderByField
								}
							);
		return (List<FlowchartServiceTotal__c>) Database.query(query);
	}
}