/** Implements the Service Layer of the object LaboratorySelectionLists__c
 * @author	Dimitrios Sgourdos
 * @version	10-Dec-2013
 */
public with sharing class LaboratorySelectionListsService {
	
	/** Split the laboratory selection lists values to Map with key the list category and value the list of values for the category
	 * @author	Dimitrios Sgourdos
	 * @version 17-Oct-2013
	 * @param	lslList				The laboratory selection lists values list
	 * @return	The values splitted in a map
	 */
	public static Map<String, List<LaboratorySelectionLists__c>> splitLaboratorySelectionListsValuesPerCategoryInMap(List<LaboratorySelectionLists__c> lslList) {
		return  ( (Map<String,List<LaboratorySelectionLists__c>>) BDT_Utils.mapSObjectListOnKey(lslList, new List<String>{'Name'}) );
	}
	
	
	/** Retieve the values for a given laboratory selection list category.
	 * @author	Dimitrios Sgourdos
	 * @version 10-Dec-2013
	 * @param	lslMap				The map with the laboratory selection lists values lists per category
	 * @param	categoryName		The name of the laboratory selection list category
	 * @param	noneValueFlag		A flag if a none value is added in the returned values
	 * @param	sortedFlag			A flag if the returned values are sorted or not
	 * @return	The values for the given category
	 */
	public static List<SelectOption> retrieveValuesForCategoryFromMap(Map<String, List<LaboratorySelectionLists__c>> lslMap, 
																	String categoryName,
																	Boolean noneValueFlag,
																	Boolean sortedFlag) {
		List<SelectOption> results = new List<SelectOption>();
		
		if( lslMap.containsKey(categoryName) ) {
			results = BDT_Utils.getSelectOptionFromSObjectList(lslMap.get(categoryName),
																'listFirstvalue__c',
																'listFirstvalue__c',
																(noneValueFlag)? '----': NULL,
																sortedFlag
					);
		}
		
		return results;
	}
	
	
	/** Determine if a value is inside a given set, so then to return an enabling flag.
	 * @author	Dimitrios Sgourdos
	 * @version 17-Oct-2013
	 * @param	enablingValuesSet	The values that the enabling flag is true
	 * @param	currentValue		The current value that we want to check if it enables or not the enabling flag
	 * @return	If the enabling flag is true or false
	 */
	public static Boolean enableAssociatedSelectOption(Set<String> enablingValuesSet, String currentValue) {
		return enablingValuesSet.contains(currentValue);
	}
	
	
	/** Get the Matrix values that an Anti-coagulant can be assigned.
	 * @author	Dimitrios Sgourdos
	 * @version 21-Oct-2013
	 * @return	The Set of Matrix values
	 */
	public static Set<String> readMatrixValuesEnablingAnticoagulant () {
		Set<String> valuesSet = new Set<String>();
		valuesSet.addAll(LaboratorySelectionListsDataAccessor.getMatrixValuesEnablingAnticoagulant());
		return valuesSet;
	}
	
	
	/** Get the Analytical technique values that a Detection can be assigned.
	 * @author	Dimitrios Sgourdos
	 * @version 21-Oct-2013
	 * @return	The Set of Analytical technique values
	 */
	public static Set<String> readAnalyticalTechniqueValuesEnablingDetection () {
		Set<String> valuesSet = new Set<String>();
		valuesSet.addAll(LaboratorySelectionListsDataAccessor.getAnalyticalTechniqueValuesEnablingDetection());
		return valuesSet;
	}
	
	
	/** Retrieve all the Laboratory Selection Lists Values for all categories.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 30-Oct-2013
	 * @return	The Laboratory Selection Lists values list.
	 */
	public static List<LaboratorySelectionLists__c> getAllSelectionListValues () {
		return LaboratorySelectionListsDataAccessor.getAllListValues();
	}
	
}