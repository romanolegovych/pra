/** Purpose : This will update the WFM_Project__C fields i.e. Director_Project_Delivery__c ,Global_Project_Manager__c 
              If the fields on the client_project__c i.e. Global_Project_Manager_Director__c,Director_of_Project_Delivery__c changes during any update.\
              It also updates the respective employeeNumber in the WFM_Project also. 
*/
public with Sharing Class RM_CPNameChangeHandler{

    private boolean m_isExecuting = false;
    private integer BatchSize = 0;

    public RM_CPNameChangeHandler(boolean isExecuting, integer size){
        m_isExecuting = isExecuting;
        BatchSize = size;
    }
    
     public void OnAfterUpdate(Map<Id,Client_Project__c> clientProjectOldMap, Map<ID, Client_Project__c> clientProjectNewMap){         
        
         set<Id> uniqUserIds = new set<Id>();
         set<string> uniqCPNames = new set<string>();
         for (Client_Project__c cp: clientProjectNewMap.values()){
             uniqCPNames.add(cp.Name);                                     //getting the name of the Project
             if (cp.Global_Project_Manager_Director__c != null){        
                 uniqUserIds.add(cp.Global_Project_Manager_Director__c);   //getting the set of userIds
             }    
             if (cp.Director_of_Project_Delivery__c != null)  {  
                 uniqUserIds.add(cp.Director_of_Project_Delivery__c);      //getting the set of userIds
             }    
         }         
         Map<Id,User> UserNamesById = new Map<Id,User>([Select Id, Name ,EmployeeNumber From User where Id in :uniqUserIds]);
         Map<String,WFM_Project__c> wfmProjectByName = new Map<string,WFM_Project__c>();
         SYSTEM.DEBUG('=========UserNamesById ================'+UserNamesById);
         
         for(WFM_Project__c wp: [select Id,Name,Global_Project_Manager__c,Director_Project_Delivery__c,DPD_Employee_ID__c,GPM_Employee_ID__c from WFM_Project__c where Name in :uniqCPNames]){
             wfmProjectByName.put(wp.Name,wp);
         }   
          SYSTEM.DEBUG('=========wfmProjectByName================'+wfmProjectByName);
         
         if(wfmProjectByName.size()>0){      
         List<WFM_Project__c> updWFMProjects = new List<WFM_Project__c>();   
         for(ID id : clientProjectOldMap.keySet()){
             if (clientProjectNewMap.containsKey(id)){
             WFM_Project__c wfmp = new WFM_Project__c();
             //wfmp =wfmProjectByName.get(clientProjectNewMap.get(id).Name);
             if((clientProjectNewMap.get(id).Global_Project_Manager_Director__c != clientProjectOldMap.get(id).Global_Project_Manager_Director__c )
             ||( clientProjectNewMap.get(id).Director_of_Project_Delivery__c != clientProjectOldMap.get(id).Director_of_Project_Delivery__c)){
                  wfmp =wfmProjectByName.get(clientProjectNewMap.get(id).Name);
                  
                  
                  if(UserNamesById.get(clientProjectNewMap.get(id).Global_Project_Manager_Director__c)!=null ){
                         User u1 =  UserNamesById.get(clientProjectNewMap.get(id).Global_Project_Manager_Director__c);
                          SYSTEM.DEBUG('========================='+wfmp);
                          wfmp.Global_Project_Manager__c = u1.Name;
                          wfmp.GPM_Employee_ID__c=u1.EmployeeNumber;
                  }
                  if( UserNamesById.get(clientProjectNewMap.get(id).Director_of_Project_Delivery__c)!=null){   
                      User u2 =  UserNamesById.get(clientProjectNewMap.get(id).Director_of_Project_Delivery__c);
                      wfmp.Director_Project_Delivery__c = u2.Name;
                      wfmp.DPD_Employee_ID__c= u2.EmployeeNumber;                      
                  }
                  updWFMProjects.add(wfmp);
               } 
             }
         }
         if(updWFMProjects.size()>0)
         update updWFMProjects ;
         }        
    }
}