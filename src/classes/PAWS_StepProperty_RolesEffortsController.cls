public with sharing class PAWS_StepProperty_RolesEffortsController
{
    public Boolean IsAllEffortsValid { get; set; }
        
    public Integer SelectedRecordNumber {get;set;}
    
    public List<Job_Class_Desc__c> Roles
    {
        get
        {
            if (Roles == null)
            {
                //Roles = PBB_WR_APIs.GetJobPositionList();
                Roles = [select     id,
                           Job_Position_Long__c,
                           Job_Code__c,
                           Job_Class_Code__c,
                           IsClinical__c
                FROM       Job_Class_Desc__c
                WHERE      IsClinical__c=true and Status__c = 'Active'];
            }
            return Roles;
        }
        set;
    }
    
    public Map<String, String> RolesMap
    {
        get
        {
            if (RolesMap == null)
            {
                RolesMap = new Map<String, String>();
                for (Job_Class_Desc__c role : Roles)
                {
                    RolesMap.put(role.Job_Class_Code__c, role.Job_Position_Long__c);
                }
            }
            return RolesMap;
        }
        set;
    }
    
    public Map<String, List<SelectOption>> RoleOptionMap 
    {
        get
        {
            if(RoleOptionMap == null)
            {
                RoleOptionMap = new Map<String, List<SelectOption>>();
            }
            return RoleOptionMap;
        }
        set;
    }
    
    public Set<String> SelectedNames
    {
        get
        {
            if (SelectedNames == null)
            {
                SelectedNames = new Set<String>();
                for (RoleEffort obj : Records)
                {
                    SelectedNames.add(obj.RoleCode);
                }
            }
            return SelectedNames;
        }
        set;
    }
    
    public List<RoleEffort> Records
    {
        get
        {
            if (Records == null)
            {
                Records = new List<RoleEffort>();
                
                String propId = ApexPages.currentPage().getParameters().get('propertyId');
                if(String.isEmpty(propId) == false)
                {
                    try
                    {
                        STSWR1__Flow_Step_Property__c stepProp = [select STSWR1__Value__c from STSWR1__Flow_Step_Property__c where ID = :propId];
                        if(stepProp != null && stepProp.STSWR1__Value__c != null)
                        {
                            Records = ((EffortData)JSON.deserialize(stepProp.STSWR1__Value__c, EffortData.class)).Efforts;
                            ChangeRecord();
                        }
                    }     
                    catch(Exception ex){ /* do nothing */ }
                }
                
            }
            return Records;
        }
        set;
    }
    
    public String RecordsJson
    {
        get
        {
            return RecordsJson = new EffortData(Records).toJson();
        }
        set;
    }
    
    public void AddRecord()
    {       
        List<Selectoption> options = findOptions();
        Records.add(new RoleEffort(options));
        ChangeRecord();
    }
    
    public void DeleteRecord()
    {
        Records.remove(SelectedRecordNumber);
        //SelectedRecordId;
    }
    
    public void ChangeRecord()
    {
        List<Selectoption> options = findOptions();
        
        for (RoleEffort obj : Records)
        {
            List<SelectOption> opt = RoleOptionMap.get(obj.RoleCode) == null ? new List<SelectOption>() : RoleOptionMap.get(obj.RoleCode);
            opt = options.clone();
            if(RolesMap.get(obj.RoleCode) != null)
            {
                opt.add(0, new Selectoption(obj.RoleCode, RolesMap.get(obj.RoleCode)));
            }
            RoleOptionMap.put(obj.RoleCode, opt);
            /*
            obj.Options = options.clone();
            obj.Options.add(0, new Selectoption(obj.Name, obj.Name));
            */
        }
    }
    
    public void save()
    {
        IsAllEffortsValid = true;
        
        for(RoleEffort each : Records)
        {
            if(each.getIsEffortValid() == false)
            {
                IsAllEffortsValid = false;
                break;
            }
        }
    }
    
    public List<Selectoption> findOptions()
    {
        SelectedNames = null;
        List<Selectoption> options = new List<Selectoption>();
        for (Job_Class_Desc__c obj : Roles)
        {
            if (SelectedNames.contains(obj.Job_Class_Code__c))
                continue;
                
            options.add(new Selectoption(obj.Job_Class_Code__c, obj.Job_Position_Long__c));
        }
        
        return options;
    }
    
    public class RoleEffort
    {
        public String RoleCode {get;set;}
        public String Effort { get; set; }
        public Boolean getIsEffortValid()
        {
            try
            {
                Integer.valueOf(Effort);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        //public String Unit {get;set;}
        
        public RoleEffort(List<Selectoption> opt)
        {
            this.RoleCode = opt != null && opt.isEmpty() == false ? opt.get(0).getValue() : '';
            this.Effort = '0';
            //this.Unit = 'hrs';
        }
    }
    
    public class EffortData
    {
        public List<RoleEffort> Efforts { get ; set; }
        public String Description
        { 
            get
            {
                if(Description == null)
                {
                    Integer rowsCount = Efforts == null ? 0 : Efforts.size();
                    Description = rowsCount + (rowsCount == 1 ? ' Role' : ' Roles');
                }
                return Description;
            }
            set;
        }
        
        public EffortData(List<RoleEffort> inputEfforts)
        {
            Efforts = inputEfforts;
        }
        
        public String toJson()
        {
            return JSON.serialize(this);
        }
    }
}