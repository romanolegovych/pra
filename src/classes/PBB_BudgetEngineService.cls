/**
    The purpose of this class is to create and populate the PBB Scenario Budget data records.
    This class also includes any methods that query and/or modify Scenario Budget data.
    
    Dependencies:  PBB_BudgetBillRateService
                   PAWS_PBB_Helper & PAWS_API
                   
*/
public with sharing class PBB_BudgetEngineService extends PRA_BaseService {

    @TestVisible private PBB_Scenario__c pbbScenario;
    @TestVisible private Scenario_Budget__c budget;
    @TestVisible private map<ID, Scenario_Budget_Task__c> budgetTasks;
    @TestVisible private List<Scenario_Budget_RoleCountry__c> budgetRoleCountries; 

    @TestVisible private List<Countries_Service_Tasks__c> lstCountryServiceTasks;

    @TestVisible private List<String> lstErrors = new List<String>();
    
    @TestVisible private IPawsData pawsDataContext;
    @TestVisible private PBB_BudgetBillRateService.IBudgetBillRateService billRateContext;
    
    /* Class Constants */
    protected final String COUNTRY_CODE_GLOBAL = 'GLOBAL';
    
    public PBB_BudgetEngineService() {
        super();
    }

    /**
      @description  This is a static entry point to the functions of this class.  This method
                    is designed to be called by a controller.
                    This method also serves as an example of the proper way to instantiate,
                    setup, and execute the budget creation process.
    */
    public static void createScenarioBudget(ID aPbbScenarioID) {
        PBB_BudgetEngineService theService = new PBB_BudgetEngineService();
        IPawsData thePawsContext = new PbbPawsData();
        PBB_BudgetBillRateService.IBudgetBillRateService theBillRateContext = new PBB_BudgetBillRateService();
        
        theService.setPawsDataContext(thePawsContext);
        theService.setBudgetBillRateContext(theBillRateContext);
        theService.createBudget(aPbbScenarioID);
    }
    
    @TestVisible private void setPawsDataContext(IPawsData aContext) {
        pawsDataContext = aContext;
    }
    
    @TestVisible private void setBudgetBillRateContext(PBB_BudgetBillRateService.IBudgetBillRateService aContext) {
        billRateContext = aContext;
    }
    
    @TestVisible private void createBudget(ID aPbbScenarioID) {
        // Deactive any existing budgets for this scenario
        deactivateBudgets(aPbbScenarioID); 
        
        getScenarioBudgetData(aPbbScenarioID);
        if (lstErrors.size() == 0) {
            createScenarioBudget();
            createScenarioBudgetTasks();
            createScenarioBudgetRoleCountries();
        }

        if (lstErrors.size() > 0) {
            PRAServiceException ex = new PRAServiceException('Budget Validation Failure!');
            lstErrors.Sort();
            ex.setErrorList(lstErrors);
            System.Debug('---- ErrorList: ' + lstErrors);
            throw ex;
        }

        PBB_BudgetDataAccessor dataAccessor = new PBB_BudgetDataAccessor();
        dataAccessor.createBudgetDataStruct(budget, 
                                            budgetTasks.values(), 
                                            budgetRoleCountries);       
        
    }
    
    @TestVisible private void deactivateBudgets(ID aPbbScenarioID) {
        list<Scenario_Budget__c> lstSb = PBB_BudgetDataAccessor.getActiveBudgets(aPbbScenarioID);
        for (Scenario_Budget__c sb : lstSb) {
            sb.active__c = false;
        }
        update lstSb;
    }
    
    @TestVisible private void getScenarioBudgetData(ID aPbbScenarioID) {
        pbbScenario = PBB_DataAccessor.getScenarioByID(aPbbScenarioID);
        lstCountryServiceTasks = PBB_DataAccessor.GetCountryServiceTasksByScenario(aPbbScenarioID);
        
        // Validations
        if (pbbScenario.Bill_Rate_Card_Model__c == null) {
            lstErrors.add('PBB Scenario does not have a Billing Rate Card.');
        }
    }
        
    @TestVisible private void createScenarioBudget() {
        budget = new Scenario_Budget__c();
        
        budget.Active__c = true;
        budget.PBB_Scenario__c = pbbScenario.ID;
        budget.Budget_ID__c = pbbScenario.name + ':' + uniqueTimeBasedKey();
    }
    
    @TestVisible private void createScenarioBudgetTasks() {
        budgetTasks = new map<ID, Scenario_Budget_Task__c>();
        map<ID, Countries_Service_Tasks__c> services = new map<ID, Countries_Service_Tasks__c>();
        Scenario_Budget_Task__c newTask;
        
        // Create a set of Service Task ids.  This will be the unique set
        // of items that will drive the creation of the Scenario_Budget_Task__c
        // records
        for (Countries_Service_Tasks__c cst : lstCountryServiceTasks) {
            if (!services.containsKey(cst.Service_Task__c)) {
                services.put(cst.Service_Task__c, cst);
            }
        }
        
        for (Countries_Service_Tasks__c uniqueServiceTask : services.values()) {
            newTask = new Scenario_Budget_Task__c();
            newTask.Service_Task__c = uniqueServiceTask.Service_Task__c;
            newTask.Budget_Task_ID__c = budget.Budget_ID__c + ':' + uniqueServiceTask.Service_Task__r.ST_AN__c;
            
            /* NOTE: MAYBE TEMPORARY
                     We don't have an engine to calculate the number of units for a given task.
                     At this time the number of units is being entered in the service model.
                     Ummmmmm... I don't think it works like this but what the heck!
                     
                     I expect we'll have a complete service class devoted to calculating units, and 
                     when we do we'll do the assignment here.
            */                     
          //  newTask.Number_of_Units__c = uniqueServiceTask.Service_Task__r.No_of_Units__c;
            
            budgetTasks.put(newTask.Service_Task__c, newTask);
        }
    }
    
    @TestVisible private void createScenarioBudgetRoleCountries() {
        budgetRoleCountries = new List<Scenario_Budget_RoleCountry__c>();
        Scenario_Budget_RoleCountry__c newRoleCountry;
        
        List<Job_Class_Desc__c> lstJobPositions;
        
        // Build a set of selected countries and retrieve the country codes
        set<ID> countryIDs = new set<ID>();
        for (Countries_Service_Tasks__c cst : lstCountryServiceTasks)
        {
            countryIDs.add(cst.PBB_Scenario_Country__r.Country__c);
        }
        /* NOTE: TEMPORARY!!!
                 We don't have requirements for mapping global tasks to a country for lookup
                 in the bill rate table.  For now all global tasks will get the US rate.
                 Adding US to the country set for populating the BillRateEngine service.
        */
        Country__c globalCountry = [SELECT id FROM Country__c WHERE Country_Code__c = 'US' LIMIT 1];
        countryIDs.add(globalCountry.id);
        
        map<ID, Country__c> mapCountryCodes = PBB_BudgetDataAccessor.getCountryCodes(countryIDs);
        
        // Get work breakdown from PAWS and organize by country and task
        PawsWorkBreakdown workBreakdown = new PawsWorkBreakdown(pawsDataContext,pbbScenario.Paws_Project_ID__c);        
        
        /* NOTE: TEMPORARY!!!
                 We don't yet have a designated currency for bidding.  For now we will
                 default to USD.
                 I imagine that we'll have the user select a currency a the PBB Scenario or
                 the Bid Project level.  When that happens we'll select the proper currency
                 record here.
        */
        Currency__c defaultCurrency = [SELECT id FROM Currency__c WHERE name = 'USD'];
        billRateContext.setBaseParameters(pbbScenario.Bill_Rate_Card_Model__c, defaultCurrency.id);
        billRateContext.setCountryParameters(countryIDs);
        billRateContext.setJobParameters(workBreakdown.getJobSet());
        billRateContext.loadBillRates();
        
        
        for (Countries_Service_Tasks__c cst : lstCountryServiceTasks)
        {
            Scenario_Budget_Task__c theBudgetTask = budgetTasks.get(cst.Service_Task__c);
            
            // Create country code for use later
            string strCountryCode;
            if (cst.PBB_Scenario_Country__r.Country__c == null) {
                strCountryCode = COUNTRY_CODE_GLOBAL;
            }
            else {
                strCountryCode = mapCountryCodes.get(cst.PBB_Scenario_Country__r.Country__c).Country_Code__c; 
            }

            // Acquire List of Roles for this Service Task and Country
            List<PawsWorkItem> lstRoles = workBreakdown.getRoles(cst.PBB_Scenario_Country__r.Country__c, cst.Service_Task__c);
            
            // Validate!  There should be roles returned for every selected service task and
            // country combination.
            if (lstRoles.size() < 1) {
                String strErr = String.format('Service task "{0}" has no roles for country {1}', new List<String> {cst.Service_Task__r.Name, strCountryCode} );
                lstErrors.add(strErr); 
            }
            
            for (PawsWorkItem workItem : lstRoles)
            {
                newRoleCountry = new Scenario_Budget_RoleCountry__c();
                
                newRoleCountry.Countries_Service_Tasks__c = cst.ID;
                newRoleCountry.Country__c = cst.PBB_Scenario_Country__r.Country__c;
                newRoleCountry.Job_Position__c = workBreakdown.getJobClassDescId(workItem.JobCode);

                // Build human readable ID
                newRoleCountry.Budget_RoleCountry_ID__c =  theBudgetTask.Budget_Task_ID__c + ':' + (String.isBlank(strCountryCode) ? '' : strCountryCode) + ':' + (String.isBlank(workItem.JobCode) ? '' : workItem.JobCode);
                newRoleCountry.Country_Code__c = strCountryCode;
                                
                // Budget impacts
                newRoleCountry.Effort_Impact_Percent__c = cst.Total_Effort_Impact_by_Percentage__c;
                newRoleCountry.Effort_Impact_Hours__c = cst.Total_Effort_Impact_by_Hours__c;
                
                newRoleCountry.Base_Effort__c = workItem.Effort;
                newRoleCountry.Start_Date__c = workItem.StartDate;
                newRoleCountry.End_Date__c = workItem.EndDate;
                
                /* NOTE: Currently the only type of task we can handle is one where the hours
                         are evenly distributed across a date range.  When new types are added, then
                         we'll need to get creative here.  I'm suggest that we put the method selection
                         logic into the PBB_BudgetBillRateService and replace this method call with 
                         a single entry point to the service.
                */
                try {
                    newRoleCountry.Weighted_Avg_Bill_Rate__c = billRateContext.getWeightedRateEven((cst.PBB_Scenario_Country__r.Country__c == null ? globalCountry.id : cst.PBB_Scenario_Country__r.Country__c), 
                                                                                             workBreakdown.getJobClassDescId(workItem.JobCode),
                                                                                             workItem.StartDate,
                                                                                             workItem.EndDate);
                }
                catch (PRAServiceException ex) {
                    List<String> errList = ex.getErrorList();
                    List<String> errParameters = new List<String> { mapCountryCodes.get(errList[0]).Country_Code__c,
                                                                    workBreakdown.getJobClassById(errList[1]).Name,
                                                                    errList[2] };  
                    String strErr = String.format('Missing Bill Rate for Country: {0}, Job: {1}, Year: {2}', errParameters);
                    lstErrors.add(strErr); 
                }
                                
                budgetRoleCountries.add(newRoleCountry);   
            }
        }
    }
    
        
    /*
     @description  This method generates a string of the current time stamp.
                   It is used to create a unique external key or component of
                   a unique external key for SObjects that have no natural
                   unique key
    */
    private string uniqueTimeBasedKey()    {
        DateTime keyTime = DateTime.now();
        return keyTime.formatGMT('yyyy-MM-dd HH-mm-ss-SSS');
    }

    /*
      @description This class abstracts the work breakdown data from PAWS.
                   Its purpose is to keep the "dirty details" out of the main
                   area of logic to aid readability.
    */
    private class PawsWorkBreakdown {
        /* Class Constants */
        protected final String COUNTRY_CODE_GLOBAL = 'GLOBAL';
        
        private map<String, List<PawsWorkItem>> organizedItems = new map<String, List<PawsWorkItem>>();
        private map<String, Job_Class_Desc__c> mapJobcodeToJob = new map<String, Job_Class_Desc__c>(); 
        private map<ID, Job_Class_Desc__c> mapIdToJob = new map<ID, Job_Class_Desc__c>();
        
        PawsWorkBreakdown(IPawsData aPawsDataContext, ID aPawsProjectId) {
            List<PawsWorkItem> workItems = aPawsDataContext.getWorkBreakdown(aPawsProjectId);
            Set<String> jobCodes = new Set<String>();
            String key;
            
            for (PawsWorkItem item : workItems) {
                if (item.ServiceTaskId != null) {
                    key = (item.CountryID == null ? COUNTRY_CODE_GLOBAL : item.CountryID) + item.ServiceTaskId;
                
                    if (organizedItems.containsKey(key) == false) {
                        organizedItems.put(key, new List<PawsWorkItem>());
                    }
                    
                    (organizedItems.get(key)).add(item);
                    
                    jobCodes.add(item.JobCode);
                }
            }
            PBB_BudgetDataAccessor.getMappedJobs(jobCodes, mapIdToJob, mapJobcodeToJob);
        }
        
        public List<PawsWorkItem> getRoles(ID aCountryId, ID aTaskId) {
            List<PawsWorkItem> rtnList;
            String key = (aCountryId == null ? COUNTRY_CODE_GLOBAL : aCountryId) + aTaskId;

            rtnList = organizedItems.get(key);
            if(rtnList == null) {
                rtnList = new List<PawsWorkItem>();
            }
            return rtnList; 
        }
        
        public ID getJobClassDescId(String aJobCode) {
            return mapJobcodeToJob.get(aJobCode).id;
        }
        
        public Job_Class_Desc__c getJobClassById(ID aID) {
            return mapIdToJob.get(aID);
        }
        
        public Set<ID> getJobSet() {
            Set<ID> rtnSet = new Set<ID>();
            for (Job_Class_Desc__c jcd : mapJobcodeToJob.values()) {
                rtnSet.add(jcd.id);
            } 
            return rtnSet;
        }
    }

    /**
     @description Value object (wrapper) used in the IPawsData interface
    */
    public class PawsWorkItem
    {
        public String JobCode;
        public Integer Effort;
        public Date StartDate;
        public Date EndDate;
        public ID ServiceTaskId;
        public String CountryName;
        public ID CountryID;

        public PawsWorkItem(String aJobCode,
                            Integer aEffort,
                            Date aStartDate,
                            Date aEndDate,
                            ID aServiceTaskId,
                            String aCountryName,
                            ID aCountryID) {
        
            JobCode = (aJobCode == null ? null : aJobCode);
            Effort = (aEffort == null ? null : aEffort);
            StartDate = (aStartDate == null ? null : aStartDate);
            EndDate = (aEndDate == null ? null : aEndDate);
            ServiceTaskId = (aServiceTaskId == null ? null : aServiceTaskId);
            CountryName = (aCountryName == null ? null : aCountryName);
            CountryID = (aCountryID == null ? null : aCountryID);
        }
    }

    /**
     @description  This interface defines the class used to retrieve PAWS
                   data via the PAWS API.
                   This interface is used in conjunction with dependency
                   injection to isolate the dependency on PAWS data. The
                   purpose is to allow creation of mock data for units tests.
    */
    public interface IPawsData {
        List<PawsWorkItem> getWorkBreakdown(ID aPawsProjectId);
    }
    
    /**
     @description  This class is an adaptor that implements a
                   PAWS API method via the IPawsData interface.
    */
    private class PbbPawsData implements IPawsData {
        public PbbPawsData() {}

        public List<PawsWorkItem> getWorkBreakdown(ID aPawsProjectId) {
            List<PawsWorkItem> pawsWorkItems = new List<PawsWorkItem>(); 
            List<PAWS_PBB_Helper.WorkItem> workItems = PAWS_API.PBB_getWorkBreakdown(aPawsProjectId);

            for (PAWS_PBB_Helper.WorkItem wi : workItems) {
                pawsWorkItems.add( 
                   new PawsWorkItem(wi.JobClassDesc,
                                    wi.Effort,
                                    wi.StartDate, 
                                    wi.EndDate,
                                    wi.ServiceTaskId,
                                    wi.CountryName,
                                    wi.CountryID)
                );
            }
            
            // Since we've copied the list of PAWS work items to a list of
            // locally defined PawsWorkItem objects we no long need to keep 
            // this list in memory.  This should help to prevent wasting heap
            // space and avoid hitting the heap space governor limit.
            workItems.Clear();
            return pawsWorkItems;                       
        }
    }
    
    /**
    * @author Ramya
    * @date 2015
    * @description Wrapper class to display by functional and country grouping   
    * @return  map<String,List<ClientUnitGridCVO>> returns region and its Client Units and its data. 
    */   
    
    // Wrapper class to display on the pageblock table for the Client Unit grid
    public class ClientUnitGridCVO {   
         
        public  List<ResourceClientUnitGridCVO>  ResourceList{get;set;}
        public  Scenario_Budget_Task__c      UGTRU      {get;set;}    
        public  ClientUnitGridCVO (){
            ResourceList =  new List<ResourceClientUnitGridCVO>();
        }  
        
    }
    
     // Wrapper class to display all the resources for a Client Unit on the pageblock table
    public class ResourceClientUnitGridCVO {
        public  String    Resource       {get;set;}        
        public  Decimal   Rates          {get;set;}
        public  Decimal   Hours          {get;set;} 
        public  Decimal   Budget         {get;set;}        
        public  ResourceClientUnitGridCVO (){   
        }       
    }
    
    /**
    * @author Ramya
    * @date 2015
    * @description method gets the data from Work Allocation object based on selection the CR/PRA region-Band     
    * @return  map<String,List<ClientUnitGridCVO>> returns region and its Client Units and its data. 
    */   
    public static map<String,ClientUnitGridCVO> getClientGridByRolectry(ID PBBScenarioID){
    
        /** Get Data From Scenario_Budget_RoleCountry__c from PBB_BudgetDataAccessor Class**/
        map<String,Scenario_Budget_RoleCountry__c> UFTaskMap = new map<String,Scenario_Budget_RoleCountry__c>( );
        system.debug('---UFTaskMap--'+UFTaskMap); 
    
        map<String,ClientUnitGridCVO> ClientUnitMap = new map<String,ClientUnitGridCVO>();
        map<String,List<ResourceClientUnitGridCVO >> RegionClientUnitListMap = new map<String,List<ResourceClientUnitGridCVO >>(); 
        
        /** Get Data From Scenario_Budget_RoleCountry__c from PBB_BudgetDataAccessor Class**/ 
        for(Scenario_Budget_RoleCountry__c sbrc:PBB_BudgetDataAccessor.getFunctionalCntrygriddata(PBBScenarioID)){
            List<ResourceClientUnitGridCVO> rcvList = new List<ResourceClientUnitGridCVO> ();
             
            if(RegionClientUnitListMap.containsKey(String.valueof(sbrc.Scenario_Budget_Task__r.Service_Task__c)))  
                    rcvList  = RegionClientUnitListMap.get(String.valueof(sbrc.Scenario_Budget_Task__r.Service_Task__c));
                    
                ResourceClientUnitGridCVO rcv = new ResourceClientUnitGridCVO ();
                rcv.Resource = (String.valueof(sbrc.Job_Position__r.Name))+'-'+ (String.valueof(sbrc.Country__r.name)) ;
                rcv.hours =  (((Decimal) sbrc.get('Budgeted_Effort__c') == null) ? 0 : (Decimal) sbrc.get('Budgeted_Effort__c')).setscale(2); 
                rcv.Rates =  ((((Decimal) sbrc.get('Weighted_Avg_Bill_Rate__c') == null) ? 0 : (Decimal) sbrc.get('Weighted_Avg_Bill_Rate__c'))).setscale(2); 
                rcv.Budget =  ((((Decimal) sbrc.get('BA_Bill_Amount__c') == null) ? 0 : (Decimal) sbrc.get('BA_Bill_Amount__c'))).setscale(2);        
                rcvList.add(rcv);
                system.debug('---rcvList--'+rcvList);
                RegionClientUnitListMap.put(sbrc.Scenario_Budget_Task__r.Service_Task__c,rcvList);
        }   
        system.debug('---RegionClientUnitListMap--'+RegionClientUnitListMap);
            
            /** Get Data From Scenario_Budget_Task__c  from PBB_BudgetDataAccessor Class**/
            for(Scenario_Budget_Task__c bt : PBB_BudgetDataAccessor.getclientunitgriddata(PBBScenarioID)){ 
            system.debug('---bt--'+bt);
            
                ClientUnitGridCVO rcuCvo = new ClientUnitGridCVO();
                
                if(RegionClientUnitListMap.containsKey(String.valueof(bt.Service_Task__c)))
                    rcuCvo.ResourceList = RegionClientUnitListMap.get(String.valueof(bt.Service_Task__c));
                    
                rcuCvo.UGTRU  = bt;
                ClientUnitMap.put(String.valueof(bt.Service_Task__c),rcuCvo);                   
            }  
            system.debug('---ClientUnitMap--'+ClientUnitMap);       
        return ClientUnitMap;
    }
     
    /**
    * @author Ramya
    * @date 2015
    * @description method gets the data from Work Allocation object based on selection the CR/PRA region-Band     
    * @return  map<String,List<ClientUnitGridCVO>> returns region and its Client Units and its data. 
    */   
    public static map<String,ClientUnitGridCVO> getClientgridbyRole(ID PBBScenarioID){
    
        /** Get Data From Scenario_Budget_RoleCountry__c from PBB_BudgetDataAccessor Class**/ 
        map<String,Scenario_Budget_RoleCountry__c> UFTaskMap = new map<String,Scenario_Budget_RoleCountry__c>( );
        system.debug('---UFTaskMap--'+UFTaskMap); 
    
        map<String,ClientUnitGridCVO> ClientUnitMap = new map<String,ClientUnitGridCVO>();
        map<String,List<ResourceClientUnitGridCVO >> RegionClientUnitListMap = new map<String,List<ResourceClientUnitGridCVO >>(); 
        
        /** Get Data From Scenario_Budget_RoleCountry__c from PBB_BudgetDataAccessor Class**/  
        for(AggregateResult  sbrc:PBB_BudgetDataAccessor.getAggFunctionalCntrygriddata(PBBScenarioID)){
            List<ResourceClientUnitGridCVO> rcvList = new List<ResourceClientUnitGridCVO> ();
             
            if(RegionClientUnitListMap.containsKey(String.valueof(sbrc.get('sertask')))) 
                rcvList  = RegionClientUnitListMap.get(String.valueof(sbrc.get('sertask')));
                   
            ResourceClientUnitGridCVO rcv = new ResourceClientUnitGridCVO ();
            
            rcv.Resource = (String.valueof(sbrc.get('JobpName')));
            rcv.hours =  (((Decimal) sbrc.get('budgtedeff') == null) ? 0 : (Decimal) sbrc.get('budgtedeff')).setscale(2); 
            rcv.Rates =  ((rcv.hours == 0) ? 0 : ((((Decimal) sbrc.get('billamt') == null) ? 0 : (Decimal) sbrc.get('billamt'))/rcv.hours)).setscale(2); 
            rcv.Budget =  ((((Decimal) sbrc.get('billamt') == null) ? 0 : (Decimal) sbrc.get('billamt'))).setscale(2);  
            rcvList.add(rcv);
            system.debug('---rcvList--'+rcvList);
            RegionClientUnitListMap.put(String.valueof(sbrc.get('sertask')),rcvList);
        }   
        system.debug('---RegionClientUnitListMap--'+RegionClientUnitListMap);
            
            /** Get Data From Scenario_Budget_Task__c  from PBB_BudgetDataAccessor Class**/ 
            for(Scenario_Budget_Task__c bt : PBB_BudgetDataAccessor.getclientunitgriddata(PBBScenarioID)){ 
            
                ClientUnitGridCVO rcuCvo = new ClientUnitGridCVO();
                if(RegionClientUnitListMap.containsKey(String.valueof(bt.Service_Task__c)))
                    rcuCvo.ResourceList = RegionClientUnitListMap.get(String.valueof(bt.Service_Task__c));
                system.debug('---rcuCvo--'+rcuCvo);
                rcuCvo.UGTRU  = bt;
                ClientUnitMap.put(String.valueof(bt.Service_Task__c),rcuCvo);                    
            }  
            system.debug('---ClientUnitMap--'+ClientUnitMap);
        return ClientUnitMap;
    }

    /**
        ============================================================================
          Method for updating Effort Impact in scenario_budget_rolecountry records
        ============================================================================ 
        @description Updates Scenario Budget RoleCountry records based on a list of 
                     Service Task Responses.  This method should be called immediately
                     after the service task response records have been updated.
                     
        @arguments   List<Service_Task_Responses__c> - List of responses.
                     The Service_Task_Responses__c object must contain the object's
                     Countries_Service_Tasks__c field.             
    */
    public static void updateImpact(List<Service_Task_Responses__c> aListResponses) {
        if (aListResponses != null) {
            // Unique countries_service_tasks ids
            Set<ID> cstIds = new Set<ID>();
            for (Service_Task_Responses__c response : aListResponses) {
                if (response.Countries_Service_Tasks__c != null) {
                    cstIds.add(response.Countries_Service_Tasks__c);
                }
            }
            
            // Get list of budget rolecountry records with related service countries tasks records
            list<Scenario_Budget_RoleCountry__c> lstRoleCntryTasks = PBB_BudgetDataAccessor.getRoleCountriesForCountryTasks(cstIds);
            
            // Update rolecountry records with latest budget impacts
            if (lstRoleCntryTasks.size() > 0) {
                for(Scenario_Budget_RoleCountry__c sbrc : lstRoleCntryTasks) {
                    sbrc.Effort_Impact_Hours__c = sbrc.Countries_Service_Tasks__r.Total_Effort_Impact_by_Hours__c;
                    sbrc.Effort_Impact_Percent__c = sbrc.Countries_Service_Tasks__r.Total_Effort_Impact_by_Percentage__c;
                }
                update lstRoleCntryTasks;
            }
        }
    }

}