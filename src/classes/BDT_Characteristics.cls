public with sharing class BDT_Characteristics {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} //population securities
	public BDT_SecuritiesService.pageSecurityWrapper securitiesB {get;set;} //study population securities
	public BDT_SecuritiesService.pageSecurityWrapper securitiesC {get;set;} // methods and compounds to study securities
	public List<Study__c> studiesList {	
		get{
			if(projectId!=null && projectId!=''){
				studiesList = BDT_Utils.readSelectedStudies(projectId);
			}else{
				studiesList = new List<Study__c>();	
			}
			return studiesList; 
		}
		set;}
	public String projectId {get;set;}
	public String siteId {get;set;}
	public Client_Project__c singleProject {get;set;}
	public List<Population__c> populationList {get;set;}
	public List<Site__c> siteList {get;set;}
	public Set<String> buIdSet {get;set;}
	
	public Boolean showDeleted{
		get {if (showDeleted == null) {
				showDeleted = false;
			}
			return showDeleted;
		}
		set;
	}
	
	public List<BDT_Wrapper.parentWrapper> populationWrapperList {get;set;}
	public Map<String, PopulationStudyAssignment__c> existentPopulationStudyAssignment {get;set;}
	Set<Id> populationIds = new Set<Id>();
	
	public List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer> methodCompStudyList {get;set;}
	public String laboratoryMethodId {get;set;}
	
		
	public BDT_Characteristics(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Population');
		securitiesB = new BDT_SecuritiesService.pageSecurityWrapper('Study Population');
		securitiesC = new BDT_SecuritiesService.pageSecurityWrapper('Methods, Compounds to Studies');
		projectId = BDT_Utils.getPreference('SelectedProject');
		studiesList = new List<Study__c>();
		buIdSet = new Set<String>();
		siteList = new List<Site__c>();
		populationWrapperList = new List<BDT_Wrapper.parentWrapper>();
		methodCompStudyList = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		
		if(projectId!=null && projectId!=''){
			singleProject = BDT_Utils.getProject(projectId, showDeleted);

			studiesList = BDT_Utils.readSelectedStudies(singleProject.id);
			for(Study__c stu:studiesList){
				if(!buIdSet.contains(stu.Business_Unit__c)){
					buIdSet.add(stu.Business_Unit__c);
				}
			}
			//get the population for a particular project
			populationList = getPopulationList(singleProject.id, showDeleted);
			//get list of Population Ids for further processing
			for(Population__c pop:populationList){
				populationIds.add(pop.id);
			}

			
		}else{
			singleProject = new Client_Project__c();
		}
		
		getStudyPopulationAssignment();
		buildSubcontractorAssignments();
		buildStudySiteAssignments();
		getMethodCompoundStudyAssignmentContainerData();
	}
/****************************************** Population Study Assignment ******************************************/ 	
	public void getStudyPopulationAssignment(){ 
		existentPopulationStudyAssignment  = new Map<String, PopulationStudyAssignment__c>();
		
		if(populationIds.size()>0){		
			for(PopulationStudyAssignment__c popass: [Select p.Study__c, p.Population__c, p.Name, p.Id From PopulationStudyAssignment__c p where p.Population__c in: populationIds]){//RESTRICT TO RETURN ONLY POPULATION MAPKEY
				existentPopulationStudyAssignment.put(popass.Population__c+':'+popass.Study__c, popass);
			}
			//build the populationWrapperList for display
			BDT_Wrapper.buildParentWrapperList(populationWrapperList, populationList, studiesList, existentPopulationStudyAssignment);
		}
	}
	
	public void saveStudyPopulationAss(){
		
		BDT_Wrapper.saveChildWrapper(populationWrapperList, existentPopulationStudyAssignment);
 
	}
/****************************************** Study Site Assignment ******************************************/	
	public class siteWrapper{
		public string studyId{get;set;}
		public boolean selected{get;set;}
		public boolean disabled{get;set;}
		public StudySiteAssignment__c StudySiteAssignment {get;set;}
		
		public sitewrapper( string pStudyId, boolean pSelected, boolean pDisabled, StudySiteAssignment__c pStudySiteAssignment ){
			studyId = pStudyId;
			selected = pSelected;
			disabled = pDisabled;
			StudySiteAssignment= pStudySiteAssignment;
		}
	}
	
	public class studyWrapper{
		public ProjectSite__c projectSite{get;set;}
		public List<siteWrapper> StudySiteAssignments{get;set;}
			
		public studyWrapper( ProjectSite__c pProjectSite, List<SiteWrapper> pStudySiteAssignments ){
			projectSite = pProjectSite;
			StudySiteAssignments = pStudySiteAssignments;			
		}
	}
	
	public List<studyWrapper> studySiteAssignments	{get;set;}
	
	public void buildStudySiteAssignments(){
		studySiteAssignments = new List<studyWrapper>();
		
		List<ProjectSite__c> availableSites = new List<ProjectSite__c>();
		List<StudySiteAssignment__c> existingSSA = new List<StudySiteAssignment__c>();
		
		availableSites = [Select id, Name, Site__r.Name, site__r.Business_Unit__c 
							from ProjectSite__c
							where client_project__c = :projectId];
		
		try{ existingSSA = [select id, name, study__r.Project__c, ProjectSite__c, study__r.Business_Unit__c
									from StudySiteAssignment__c 
									where study__r.Project__c = :projectId];
		}
		catch( exception e ){
			existingSSA = new List<StudySiteAssignment__c>();
		}
		
		for(ProjectSite__c ps: availableSites){
			List<siteWrapper> siteWrapperList = new List<siteWrapper>();
			for(Study__c st: studiesList){
				siteWrapper siteWrapperItem = new siteWrapper( st.Id, false, false, null );  //default not selected, but enabled
				if(st.Business_Unit__c != ps.Site__r.Business_Unit__c ){ //disable the checkbox if the bu's do not match
						siteWrapperItem.disabled = true;
				}
				for(StudySiteAssignment__c ssa: existingSSA){
					if(ssa.Study__c==st.Id && ssa.ProjectSite__c == ps.Id){ //set the item to selected if projectsite and study match
						siteWrapperItem.selected =true;
						siteWrapperItem.StudySiteAssignment = ssa;
					}	
				}			
				siteWrapperList.add(siteWrapperItem);
			}
			studySiteAssignments.add(new StudyWrapper( ps, siteWrapperList ) );
		}
		
		
	}

	public void saveStudySiteAssignments(){
		List<StudySiteAssignment__c> SSAtoSave = new List<StudySiteAssignment__c>();
		List<StudySiteAssignment__c> SSAtoDelete = new List<StudySiteAssignment__c>();
	
		for(studyWrapper sw: studySiteAssignments){
			for(siteWrapper ssw: sw.StudySiteAssignments){
				if( ssw.selected && ssw.StudySiteAssignment == null  ){
					SSAtoSave.add(new StudySiteAssignment__c(ProjectSite__c = sw.projectSite.Id, Study__c = ssw.studyId ));
				} else if(!ssw.selected && ssw.StudySiteAssignment != null ){
					SSAtoDelete.add(new StudySiteAssignment__c(Id = ssw.StudySiteAssignment.Id ));
				}	
			}
		}
		
		if(!SSAtoSave.isEmpty()){
			insert SSAtoSave;
		}
		if(!SSAtoDelete.isEmpty()){
			delete SSAtoDelete;
		}
		//after saving the assignments, rebuild the wrapper to display the latest changes
		//this ensures each assignment has an id, and saving will not cause duplicate records to be created
		buildStudySiteAssignments();
	}
		

/****************************************** Other links ******************************************/
	public PageReference edit(){
		string populationId = system.currentPageReference().getParameters().get('PopulationId');
		
		PageReference newEditPopulationPage = new PageReference(System.Page.bdt_neweditpopulation.getUrl()); 
		newEditPopulationPage.getParameters().put('PopulationId',populationId);
		newEditPopulationPage.getParameters().put('ProjectId',projectId);
		
		return newEditPopulationPage;
	}
	
	public PageReference cancel(){
		return returnToCharacteristics();
	}
	 	
  	public static List<Population__c> getPopulationList(String selectedProjectId, Boolean showDeleted){
		List<Population__c> populationList;
		try{
				populationList = [Select p.Smoking__c, p.Smoking_Max__c, p.Population_Code__c, 
								p.Name, p.Male__c, p.IsDeleted, p.Id, p.Female__c, p.BloodPS_Min__c, 
								p.BloodPS_Max__c, p.BloodPD_Min__c, p.BloodPD_Max__c, p.BMI_Min__c, p.BMI_Max__c, 
								p.Age_Min__c, p.Age_Max__c, ScreeningEfficiency__c, 
								(Select Id, Name, Population__c, Study__c, Study__r.name From PopulationStudyAssignments__r order by Study__r.name) 
								From Population__c p 
								where Client_Project__c =: selectedProjectId 
								and ((BDTDeleted__c  = :showDeleted) or (BDTDeleted__c  = false))]; 
			}catch(Exception e){
				populationList = new List<Population__c>();
			}				
	
		return populationList;
	} 

	public static PageReference returnToCharacteristics(){
		PageReference characteristicsPage = new PageReference(System.Page.bdt_characteristics.getUrl()); 
				
		characteristicsPage.setRedirect(true);
		return characteristicsPage;
	}
	public PageReference createPopulation(){
		PageReference createPopulationPage = new PageReference(system.page.BDT_NewEditPopulation.getUrl() );
		
		createPopulationPage.getParameters().put('ProjectId',projectId);
		
		return createPopulationPage;
		
	}
	/***********************************************************************************************/
	/*   Subcontractor overview section   														   */
	/***********************************************************************************************/
	public PageReference assignSubcontractors(){
		String ProjectId = system.currentPageReference().getParameters().get('ProjectId');
		PageReference assignsSubcontractorPage = new PageReference( system.page.BDT_NewEditStudySubcontractorActivity.getUrl() );
		assignsSubcontractorPage.getParameters().put('ProjectId',ProjectId);
		
		return assignsSubcontractorPage;
	}
	public List<SubcontractorWrapper> subcontractorAssignmentList		{get;set;}
	public List<SubcontractorActivityStudyAssignment__c> selectedSASA; 
	
	
	public class SSAWrapper{
		public string StudyId 		{get;set;}
		public boolean IsSelected	{get;set;}
		public SubcontractorActivityStudyAssignment__c SubcontractorActivityStudyAssignment {get;set;}
		
		public SSAWrapper(String pStudyId, boolean pIsSelected,  SubcontractorActivityStudyAssignment__c pSubcontractorActivityStudyAssignment ){
			StudyId= pStudyId;
			IsSelected = pIsSelected;
			SubcontractorActivityStudyAssignment = pSubcontractorActivityStudyAssignment;
		}
	}
	
	public class SubcontractorWrapper{
	 	public ProjectSubcontractorActivity__c PSA{get;set;}
	 	public List<SSAWrapper> StudySubcAss{get;set;}
	 	
	 	
	 	public SubcontractorWrapper( ProjectSubcontractorActivity__c pProjectSubcontractorActivity, List<SSAWrapper> pStudySubcAss ){
	 		PSA = pProjectSubcontractorActivity;
	 		StudySubcAss = pStudySubcAss;
	 	}
	}

	public void buildSubcontractorAssignments(){
		subcontractorAssignmentList = new List<SubcontractorWrapper>();
		
		List<ProjectSubcontractorActivity__c> activitiesList = new List<ProjectSubcontractorActivity__c>();	
		
		activitiesList = [select id, name, SubcontractorActivity__r.Name, SubcontractorActivity__r.Subcontractor__r.Name
							from ProjectSubcontractorActivity__c 
							where ClientProject__c = :ProjectId];
		
		try {selectedSASA = [select id, name, study__r.Project__c, ProjectSubcontractorActivity__c 
						from SubcontractorActivityStudyAssignment__c
						where study__r.Project__c = :ProjectId];
			}
		catch(QueryException e){
			selectedSASA = new List<SubcontractorActivityStudyAssignment__c>();
		}

		for(ProjectSubcontractorActivity__c psca: activitiesList ){
			List<SSAWrapper> SSAWrapperList = new List<SSAWrapper>();
			for(Study__c st: studiesList ){
				// for every study add a record to the list.
				// if the study has been selected, store the id of the corresponding record
				SSAWrapper SSAWrapperItem = new SSAWrapper(st.id, false, null );
				for(SubcontractorActivityStudyAssignment__c sasa: selectedSASA){
					if(sasa.Study__c == st.id && sasa.ProjectSubcontractorActivity__c == psca.Id){
						SSAWrapperItem.isSelected = true;
						SSAWrapperItem.SubcontractorActivityStudyAssignment = sasa;
					}
							
				}
				SSAWrapperList.add(SSAWrapperItem);
			}
			subcontractorAssignmentList.add( new SubcontractorWrapper(psca, SSAWrapperList ));
		}
	}
	
	public void saveSubcontractorAssignments(){
		List<SubcontractorActivityStudyAssignment__c> sasaToSave = new List<SubcontractorActivityStudyAssignment__c>();
		List<SubcontractorActivityStudyAssignment__c> sasaToDelete = new List<SubcontractorActivityStudyAssignment__c>();
		
		for(SubcontractorWrapper scw: subcontractorAssignmentList){
			for(SSAWrapper ssaw: scw.StudySubcAss){
				if(ssaw.IsSelected && ssaw.SubcontractorActivityStudyAssignment == null){
					sasaToSave.add(new SubcontractorActivityStudyAssignment__c(Study__c = ssaw.StudyId, ProjectSubcontractorActivity__c=scw.PSA.Id ));
				} else if (!ssaw.IsSelected && ssaw.SubcontractorActivityStudyAssignment != null) {
					sasaToDelete.add(new SubcontractorActivityStudyAssignment__c(Id = ssaw.SubcontractorActivityStudyAssignment.Id, Study__c = ssaw.StudyId, ProjectSubcontractorActivity__c=scw.PSA.Id ));
				}
			}
		}
		if(!sasaToSave.isEmpty()){
			insert sasaToSave;	
		}
		if(!sasaToDelete.isEmpty()){
			delete sasaToDelete;	
		}
		
		//after saving the assignments, rebuild the wrapper to display the latest changes
		//this ensures each assignment has an id, and saving will not cause duplicate records to be created
		buildSubcontractorAssignments();
	}
	
	
	/**	Get the data about laboratory method - laboratory method compound assignment with studies.
	 * @author	Dimitrios Sgourdos
	 * @version	11-Oct-2013
	 * @return	The list of the wrapper that holds the data.
	 */
	public void getMethodCompoundStudyAssignmentContainerData() {
		List<Study__c> selectedStudiesList = StudyService.getStudiesByUserPreference();
		methodCompStudyList = LaboratoryMethodCompoundService.getMethodCompoundStudyAssignmentContainerData(selectedStudiesList);
	}
	
	
	/**	Call the page BDT_NewEditLabAnalysisToStudy in edit mode giving as a parameter the id of the selected laboratory method.
	 * @author	Dimitrios Sgourdos
	 * @version	14-Oct-2013
	 * @return	The page BDT_NewEditLabAnalysisToStudy in edit mode.
	 */
	public PageReference editLabMethodAssignment(){
		return callBDT_NewEditLabAnalysisToStudyPage('TRUE');
	}
	
	
	/**	Call the page BDT_NewEditLabAnalysisToStudy in new mode.
	 * @author	Dimitrios Sgourdos
	 * @version	14-Oct-2013
	 * @return	The page BDT_NewEditLabAnalysisToStudy in new mode.
	 */
	public PageReference addLabMethodAssignment(){
		return callBDT_NewEditLabAnalysisToStudyPage('FALSE');
	}
	
	
	/**	Call the page BDT_NewEditLabAnalysisToStudy in new/edit mode depends on if the parameter selectedLabMethodId is NULL or not.
	 * @author	Dimitrios Sgourdos
	 * @version	14-Oct-2013
	 * @return	The page BDT_NewEditLabAnalysisToStudy in new/edit mode.
	 */
	private PageReference callBDT_NewEditLabAnalysisToStudyPage(String editMode) {
		PageReference BDT_NewEditLabAnalysisToStudyPage = new PageReference(System.Page.BDT_NewEditLabAnalysisToStudy.getUrl());
		BDT_NewEditLabAnalysisToStudyPage.getParameters().put('editMode', editMode);
		return BDT_NewEditLabAnalysisToStudyPage;
	}
	
}