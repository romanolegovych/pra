public with sharing class EWChainEditController
{
	private ApexPages.StandardController controller;
	public String path
	{
		get
		{
			if (path == null)
			{
				path = priorSelection.getLabel();
			}
			
			return path;
		}
		private set;
	}
	
	public STSWR1__Item__c currentItem
	{
		get
		{
			if (currentItem == null)
			{
				currentItem = new STSWR1__Item__c();
				for (STSWR1__Item__c each : [select Name from STSWR1__Item__c where STSWR1__Source_Flow__c = :controller.getID()])
				{
					selectedOption = each.Id;
				}
			}
			
			return currentItem;
		}
		private set;
	}
	
	public SelectOption currentSelection
	{
		get
		{
			for (SelectOption each : folderOptions)
			{
				if (each.getValue() == selectedOption)
				{
					return each;
				}
			}
			
			return new SelectOption('', '/');
		}
	}
	
	private List<SelectOption> priorSelections = new List<SelectOption>();
	private SelectOption priorSelection
	{
		get
		{
			return priorSelections.isEmpty() ? new SelectOption('' , '') : priorSelections.get(priorSelections.size() - 1);
		}
	}
	
	public String selectedOption {get; set;}
	
	public List<SelectOption> folderOptions
	{
		get
		{
			if (folderOptions == null)
			{
				folderOptions = new List<SelectOption>{new SelectOption('', (priorSelections.isEmpty() ? '-- choose a folder --' : path))};
				//folderOptions = new List<SelectOption>{new SelectOption((selectedOption == null ? '' : selectedOption), path)};
				
				for (STSWR1__Item__c item : [select Name from STSWR1__Item__c where STSWR1__Parent__c = :selectedOption and STSWR1__Source_Flow__c = null order by Name])
				{
					folderOptions.add(new SelectOption(item.Id, path + '/' + item.Name));
				}
				
				if (folderOptions.size() == 1)
				{
					folderOptions.add(new SelectOption('', '/'));
				}
			}
			
			return folderOptions;
		}
		private set;
	}
	
	public List<SelectOption> availableFlows
	{
		get
		{
			if (availableFlows == null)
			{
				availableFlows = new List<SelectOption>{new SelectOption('', '-- choose a flow --')};
				for (STSWR1__Item__c item : [select STSWR1__Source_Flow__c, STSWR1__Source_Flow__r.Name from STSWR1__Item__c where STSWR1__Parent__c = :selectedOption and STSWR1__Source_Flow__c != null order by STSWR1__Source_Flow__r.Name])
				{
					availableFlows.add(new SelectOption(item.STSWR1__Source_Flow__c, item.STSWR1__Source_Flow__r.Name));
				}
			}
			
			return availableFlows;
		}
		private set;
	}
	
	public EWChainEditController(ApexPages.StandardController controller)
	{
		this.controller = controller;
		if (controller.getID() != null)
		{
			for (STSWR1__Item__c each : [select STSWR1__Parent__c from STSWR1__Item__c where STSWR1__Source_Flow__c = :((Critical_Chain__c) controller.getRecord()).Flow__c])
			{
				selectedOption = each.STSWR1__Parent__c;
			}
		}
	}
	
	public void onFolderChange()
	{
		path = null;
		//if (selectedOption == '' && !priorSelections.isEmpty())
		if (selectedOption == '' || selectedOption == null)
		{
			priorSelections.clear();
		}
		else
		{
			priorSelections.add(currentSelection);
		}
		
		folderOptions = null;
		availableFlows = null;
	}
}