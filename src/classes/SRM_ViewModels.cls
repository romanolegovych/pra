/**
* @author Kondal Reddy
* @date 30/9/2014
* @description This class used to redirect the user to list of models
*/
public with sharing class SRM_ViewModels{

    public SRM_ViewModels(ApexPages.StandardController controller) {

    }

    private String baseQuery = 'Select ID, Name FROM SRM_Model__c ORDER BY NAME ASC';
    
    public ApexPages.StandardSetController modelSetController {
        get{
            if(modelSetController == null){
                modelSetController = new ApexPages.StandardSetController(Database.getQueryLocator(baseQuery));
            }
            return modelSetController ;
        }set;
    }
    
    //Get all available list view for SRM Model Object
    public SelectOption[] getModelExistingViews(){
        return modelSetController.getListViewOptions();
    }
    
    public SRM_ViewModels(){
        
    }
    
    /**
    * @description Method to redirect to view of all models
    *              This method forms a URL based on view Id and object Id prefix
    * @params no parameters
    */
    public PageReference redirect(){
        Schema.DescribeSObjectResult R = SRM_Model__c.SObjectType.getDescribe();
        String objPrefix = r.getKeyPrefix();
        String redirectUrl = URL.getSalesforceBaseURL().toExternalForm()+'/'+objPrefix;
        String viewId = '';
        SelectOption[] soList = getModelExistingViews();
        for(SelectOption so: soList){
            System.debug('>>> view >>> '+so.getLabel());
            if((so.getLabel()).equalsIgnoreCase('All Models Approved/Retired')){
                viewId = so.getValue().subString(0, (so.getValue().length()-3));
            }
        }
        redirectUrl += '?fcf='+viewId;
        System.debug('redirectUrl: '+redirectUrl);
        PageReference ref = new PageReference(redirectUrl);
        ref.setRedirect(true);
        return ref;
    }

    
}