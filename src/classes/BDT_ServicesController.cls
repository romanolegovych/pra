public with sharing class BDT_ServicesController {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Administration Services
  public List<ServiceCategory__c> myServiceCategory {get;set;}
  public List<Service__c>         myServices        {get;set;}   
  public String                   serviceCategoryId {get;set;}
  public String 				  serviceId			{get;set;}
  public String                   serviceCategoryName;
  public Boolean                  showDetails;
  public String 				  tmId 				{get;set;}   
  public Boolean showDeletedCategories {
		get {if (showDeletedCategories == null) {
				showDeletedCategories = false;
			}
			return showDeletedCategories;
		}
		set;
	}
  
  public Boolean showDeletedServices {
		get {
			return  (showDeletedServices == null) ? false : showDeletedServices;
		}
		set;
	}  
   
  public BDT_ServicesController() {
  	securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Services');
  		
  	string ScId = system.currentPageReference().getParameters().get('ScId');
  	  	
  	if (ScId == null ) {
  		showDetails = false;
  	} else {
		showDetails = true;
		try {
			servicecategory__c sc = [select name from servicecategory__c where id = :ScId and ((BDTDeleted__c  = :showDeletedCategories) or (BDTDeleted__c  = false)) limit 1];
			serviceCategoryName = sc.name;
		} catch(Exception e) {}
		buildServiceList( ScId );
		serviceCategoryId = ScId;
	}
  	myServiceCategory = new List<ServiceCategory__c>();
  	
  	try {
  		myServiceCategory = [SELECT Id
  		                          , Name
  		                          , Code__c
  		                          , (select code__c From ServiceCategories__r where ((BDTDeleted__c  = :showDeletedCategories) or (BDTDeleted__c  = false))) 
  		                     FROM ServiceCategory__c
  		                     Where ((BDTDeleted__c  = :showDeletedCategories) or (BDTDeleted__c  = false))
  		                     order by Code__c];  		
  	} catch(Exception e) {}
  }
   
  public void getServicesPerServiceCategory() {
  	serviceCategoryId   = System.currentPageReference().getParameters().get('serCatId');
  	serviceCategoryName = System.currentPageReference().getParameters().get('serCatName');
  	buildServiceList( serviceCategoryId );
  }
  	
  public void buildServiceList(String serviceCategoryId ){
  	myServices = new List<Service__c>();
  	try {
  		showDetails = True;
  		myServices = [select id,name, ShortName__c, IsDesignObject__c,SequenceNumber__c,IsExpense__c
  		              FROM Service__c
  		              where ServiceCategory__c = :serviceCategoryId 
  		              and   ((BDTDeleted__c  = :showDeletedServices) or (BDTDeleted__c  = false))
  		              order by SequenceNumber__c,name];
  	} catch (Exception e) {}
  } 

  public List<ServiceCategory__c> getServiceCatReplaced(){
  	List<ServiceCategory__c> myServiceCategoryRep = new List<ServiceCategory__c>();
  	
  	for(ServiceCategory__c service: myServiceCategory){
    		// remove the leading zero's as they are only for sorting purposes
			String tmpStr = BDT_Utils.removeLeadingZerosFromCat(service.Code__c);
			if( tmpStr!=NULL && tmpStr.contains('.') ) {
				tmpStr = tmpStr.substringBeforeLast('.') + '.:' + tmpStr.substringAfterLast('.');
			} else {
				tmpStr = ':' + tmpStr;
			}
			service.Code__c = tmpStr;
    		myServiceCategoryRep.add(service);
	}
  	return myServiceCategoryRep;
  }
  
  public List<Service__c> getServices() {
  	return myServices;
  }
  
  public String getServiceCategoryName(){
	return serviceCategoryName;
  }

  public boolean getShowDetails() {
	return showDetails;
  }  
  
  public PageReference editServiceCategory(){
  	serviceCategoryId = System.currentPageReference().getParameters().get('serviceCategoryId');
	return new PageReference(System.Page.BDT_NewEditServiceCategory.getUrl()+'?id='+serviceCategoryId);
  } 

  public PageReference editService(){
  	serviceId = System.currentPageReference().getParameters().get('serviceId');
	PageReference editServicePage =  new PageReference(System.Page.BDT_NewEditService.getUrl());
	editServicePage.getParameters().put('serviceID',serviceId);
	
	return editServicePage;
  }
  public PageReference createService(){
  	PageReference createServicePage= new PageReference(System.Page.BDT_NewEditService.getUrl() );
  	createServicePage.getParameters().put('serviceCategoryId',serviceCategoryId);
  	return createServicePage;
  }
}