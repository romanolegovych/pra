/**
*   'PRA_ProjectWBSCVO' class is a composite value object class which is used to hold all the wrapper related classes.
*   @author   Devaram Bhargav
*   @version  18-Oct-2013
*   @since    18-Oct-2013
*/
public with sharing class PRA_ProjectWBSCVO {
	
    /**
    *   'ProjectWBS' is a class used to create a list that binds with Projects with 
    *   its work break down structure status  into one list. 
    */
    public class ProjectWBS{    	
	    public String ProjectName{get;set;}   //Project Name.
	    public Boolean InitiateWBS{get;set;}  //Initiate the WBS by PZ.
	    public Boolean ExportWBS{get;set;}    //Export the WBS to CSV to import for Lawson.
	    public Boolean ConfirmWBS{get;set;}   //Confirm the exported csv to Lawson.
    } 
    
	/**
	*	Constants for WBSCVO class
	*/
	public static Integer WBSCVO_NUM_FIELDS = 59;

	public static Integer WBS_FLD_RUN_GRP      = 0;
	public static Integer WBS_FLD_ACTIVITY     = 2;
	public static Integer WBS_FLD_SHORT_DESC   = 3;
	public static Integer WBS_FLD_ACTIVITY_GRP = 4;
	public static Integer WBS_FLD_COMPANY_NUM  = 6;
	public static Integer WBS_FLD_ACCOUNT_UNIT = 7;
	public static Integer WBS_FLD_ACCOUNT_NUM  = 8;
	public static Integer WBS_FLD_STATUS_CODE  = 11;
	public static Integer WBS_FLD_CTRCT_ADDR   = 12;
	public static Integer WBS_FLD_PROJECT_ADDR = 13;
	public static Integer WBS_FLD_ACTIV_ADDR   = 14;
	public static Integer WBS_FLD_TASK_ADDR    = 15;
	public static Integer WBS_FLD_POSTING_ADDR = 16;
	public static Integer WBS_FLD_START_DATE   = 17;
	public static Integer WBS_FLD_END_DATE     = 18;
	public static Integer WBS_FLD_LONG_DESC    = 19;
	public static Integer WBS_FLD_LEVEL_TYPE   = 22;
	public static Integer WBS_FLD_MTX_ATTRIB_1 = 37;
	public static Integer WBS_FLD_MTX_VALUE_1  = 38;
	public static Integer WBS_FLD_MTX_ATTRIB_2 = 39;
	public static Integer WBS_FLD_MTX_VALUE_2  = 40;
	public static Integer WBS_FLD_MTX_ATTRIB_3 = 41;
	public static Integer WBS_FLD_MTX_VALUE_3  = 42;
	public static Integer WBS_FLD_MTX_ATTRIB_4 = 43;
	public static Integer WBS_FLD_MTX_VALUE_4  = 44;
	public static Integer WBS_FLD_MTX_ATTRIB_5 = 45;
	public static Integer WBS_FLD_MTX_VALUE_5  = 46;
	public static Integer WBS_FLD_MTX_ATTRIB_6 = 47;
	public static Integer WBS_FLD_MTX_VALUE_6  = 48;
	public static Integer WBS_FLD_PRJ_CURRENCY = 58;

	/**
	*	Constants for BD1CVO and BD9CVO classes
	*/
	public static Integer WBSBD_NUM_FIELDS = 74;
	
	public static Integer BD_FLD_RUN_GRP       = 0;
	public static Integer BD_FLD_SEQ_NUM       = 1;
	public static Integer BD_FLD_BUF_ACT_LVL   = 3;
	public static Integer BD_FLD_BUDGET_NUM    = 4;
	public static Integer BD_FLD_ACCT_CAT      = 5;
	public static Integer BD_FLD_PROJ_CURRENCY = 7;
	public static Integer BD_FLD_BUDGET_DESC   = 11;
	public static Integer BD_FLD_AMOUNT_RTYPE  = 15;
	public static Integer BD_FLD_UNIT_RTYPE    = 16;
	public static Integer BD_FLD_LIFE_ONLY_BGT = 17;
	public static Integer BD_FLD_ACTIVE_BGT    = 18;
	public static Integer BD_FLD_BUDGET_AMOUNT = 19;
	public static Integer BD_FLD_UNIT_AMOUNT   = 32;
	public static Integer BD_FLD_ACTIVE        = 73;
	
	public class ExportWBSCVO extends GenericCVO {
		public ExportWBSCVO() {
			super(WBSCVO_NUM_FIELDS);
			// The Account Number always has the following value
			items[WBS_FLD_ACCOUNT_NUM] = '51190';
		}
	}

	public class ExportBD1CVO extends GenericCVO {
		public ExportBD1CVO() {
			super(WBSBD_NUM_FIELDS);
			// These fields always have static values
			items[BD_FLD_BUDGET_NUM]	= '1';
			items[BD_FLD_ACCT_CAT]   	= 'LABOR';
			items[BD_FLD_BUDGET_DESC]	= 'Projected Hours Budget';
			items[BD_FLD_AMOUNT_RTYPE]	= '2';
			items[BD_FLD_UNIT_RTYPE]	= '2';
			items[BD_FLD_LIFE_ONLY_BGT]	= 'L';
			items[BD_FLD_ACTIVE_BGT]	= 'Y';
			items[BD_FLD_BUDGET_AMOUNT]	= '0';
			items[BD_FLD_UNIT_AMOUNT]	= '0.01';
			items[BD_FLD_ACTIVE]		= 'Y';
		}
	}
	
	public class ExportBD9CVO extends GenericCVO {
		public ExportBD9CVO() {
			super(WBSBD_NUM_FIELDS);
			// These fields always have static values
			items[BD_FLD_BUDGET_NUM]	= '999';
			items[BD_FLD_ACCT_CAT]   	= 'LABOR';
			items[BD_FLD_BUDGET_DESC]	= 'Contracted Budget';
			items[BD_FLD_AMOUNT_RTYPE]	= '2';
			items[BD_FLD_UNIT_RTYPE]	= '2';
			items[BD_FLD_LIFE_ONLY_BGT]	= 'L';
			items[BD_FLD_ACTIVE_BGT]	= 'Y';
			items[BD_FLD_BUDGET_AMOUNT]	= '0';
			items[BD_FLD_UNIT_AMOUNT]	= '0.01';
			items[BD_FLD_ACTIVE]		= 'N';
		}
	}
	
	private virtual class GenericCVO {
		public Integer numFields {get {return m_numFields;}}
		public List<String> items {get; set;} 
		
		private Integer m_numFields = 0;
				
		GenericCVO() {}
		
		GenericCVO(Integer numberOfFields) {
			m_numFields = numberOfFields;
			items = createList(numFields);
		}

		private List<String> createList(Integer numberOfFields) {
			List<String> rtnList = new List<String>();
	
			for (Integer i = 0; i < numberOfFields; i++) {
				rtnList.add('');
			}
					
			return rtnList;
		}
	}
	
}