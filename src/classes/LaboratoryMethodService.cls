/** Implements the Service Layer of the object LaboratoryMethod__c
 * @author	Dimitrios Sgourdos
 * @version	10-Dec-2013
 */
public with sharing class LaboratoryMethodService {
	
	// Global Constants
	public static final Integer	MAX_COMPOUNDS  = 20; 
	public static final String	MANY_COMPOUNDS = '1';
	public static final String	MISSING_FIELDS = '2';
	public static final String	SAVING_ERROR   = '3';
	public static final String	EVERYTHING_OK  = '4';
	public static final String	INITIAL_SHOWN  = 'Y';
	
	
	/** Determine if the given laboratory method has all the search criteria inserted.
	 * @author   Dimitrios Sgourdos
	 * @version  18-Oct-2013
	 * @param    labMethod			The given laboratory method
	 * @return   If the given laboratory method has all the search criteria inserted
	 */
	public static Boolean allRequiredFieldsExistent(LaboratoryMethod__c labMethod) {
		if(labMethod.Species__c==null 
				|| labMethod.Matrix__c				== null
				|| labMethod.AnalyticalTechnique__c	== null
				|| labMethod.Location__c			== null
				|| labMethod.Department__c			== null
				|| labMethod.Proprietary__c			== null) {
			return false;
		}
		
		// All the required fiels have values
		return true;	
	}
	
	
	/** Retieve the values from a picklist field with name fieldName.
	 * @author	Dimitrios Sgourdos
	 * @version 10-Dec-2013
	 * @param	fieldName			The name of the picklist field
	 * @param	noneValueFlag		A flag if a none value is added in the returned values
	 * @param	sortedFlag			A flag if the returned values are sorted or not
	 * @return	The values for the picklist field.
	 */
	public static List<SelectOption> getSelectOptionsFromLabMethodPickListField(String fieldName, Boolean noneValueFlag, Boolean sortedFlag) {
		List<SelectOption> valuesList = new List<SelectOption>();
		LaboratoryMethod__c labMethod = new LaboratoryMethod__c();
		valuesList = BDT_Utils.getSelectOptionsFromPickList(labMethod,
															fieldName,
															(noneValueFlag)? '----' : NULL,
															sortedFlag
			);
		return valuesList;
	}
	
	
	/** Generate a name for a laboratory method depends on the given location and department.
	 *	The name is produced as follow: Location-Department-Method Count Number.
	 * @author   Dimitrios Sgourdos
	 * @version  18-Oct-2013
	 * @param    location			The location of the method
	 * @param    department			The department of the method
	 * @return   The name of the method
	 */
	public static String generateLabMethodName(String location, String department) {
		// Count how many methods already exists in the given location
		List<LaboratoryMethod__c> methodList = LaboratoryMethodDataAccessor.getByLikeName(location); 
		String cntStr = String.valueOf(methodList.size()+1);
		
		// Generate and return a name
		String methodName = location + '-' +  department + '-' + BDT_Utils.lPad(cntStr, 4, '0'); // e.g. PRA-NL-SML-0010
		return methodName;
	}
	
	
	/** Create a new laboratory method from search criteria.
	 * @author   Dimitrios Sgourdos
	 * @version  18-Oct-2013
	 * @param    searchCriterialabMethod		The laboratory method which holds the search criteria
	 * @param    searchCriteriaCompoundList		The compounds that are inserted in the search criteria
	 * @return   A code that the creation was successful, else the predefined error code.
	 */
	public static String createNewLaboratoryMethodFromSearchCriteria(LaboratoryMethod__c searchCriterialabMethod, List<LaboratoryMethodCompound__c> searchCriteriaCompoundList) {
		// Check if we have more than MAX_COMPOUNDS compounds. This limitations pops up from the LaboratoryMethodCoMedication
		if(searchCriteriaCompoundList.size() > MAX_COMPOUNDS) {
			return MANY_COMPOUNDS;
		}
		
		// Check if all fields are existent (including if there are compounds)
		if( LaboratoryMethodCompoundService.allBlankNamesInLabCompoundsList(searchCriteriaCompoundList) 
				|| !allRequiredFieldsExistent(searchCriterialabMethod)) {
			return MISSING_FIELDS;
		}
		
		// Save the method
		searchCriterialabMethod.Id = (searchCriterialabMethod.Id != NULL)? NULL : searchCriterialabMethod.Id;
		searchCriterialabMethod.Name = generateLabMethodName(searchCriterialabMethod.Location__c, searchCriterialabMethod.Department__c);
		searchCriterialabMethod.ShowOnMethodList__c	= INITIAL_SHOWN;
		
		try {
			insert searchCriterialabMethod;	
		} catch (Exception e) {
			return SAVING_ERROR;
		}
		
		return EVERYTHING_OK;
	}
	
	
	/** Find how many compounds a laboratory method has.
	 * @author   Dimitrios Sgourdos
	 * @version  22-Oct-2013
	 * @param    labMethodId		The id of the laboratory method
	 * @param    studiesList		The user selected studies
	 * @return   The number of compounds that belong to the laboratory method
	 */
	public static Integer getNumberOfCompounds(String labMethodId) {
		List<Study__c> studiesList = new List<Study__c>(); // empty studies created just for the second parameter
		List<LaboratoryMethodCompound__c> compoundList = LaboratoryMethodCompoundDataAccessor.getByLabMethod(labMethodId, studiesList);
		return compoundList.size();
	}
	
	
	/** Read the predefined not applicable value for the picklists.
	 * @author   Dimitrios Sgourdos
	 * @version  30-Oct-2013
	 * @return   The predefined not applicable value for the picklists
	 */
	public static String getNotApplicableValue() {
		return LaboratoryMethodDataAccessor.NOT_APPLICABLE_VALUE;
	}
	
	
	/** Retrieve the Laboratory Method with Id equal to the given one.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 30-Oct-2013
	 * @param	laboratoryMethodId		The Id of the Laboratory method that we want to retrieve
	 * @return	The retrieved Laboratory Method.
	 */
	public static LaboratoryMethod__c getLabMethodById (String laboratoryMethodId) {
		return LaboratoryMethodDataAccessor.getById(laboratoryMethodId);
	}
	
	
	/** Retrieve the Laboratory Methods that meets the given search criteria. 
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 30-Oct-2013
	 * @param	searchLabMethod		The laboratory method that holds the search criteria
	 * @param	searchCompoundList	The compound list that holds the search criteria about compounds
	 * @param	limitation			The max number of laboratory methods that the query will return
	 * @return	The retrieved Laboratory Method instances.
	 */
	public static List<LaboratoryMethod__c> getLabMethodsBySearchCriteria( LaboratoryMethod__c searchLabMethod, 
																			List<LaboratoryMethodCompound__c> searchCompoundList, 
																			String limitation) {
		return LaboratoryMethodDataAccessor.getBySearchCriteria(searchLabMethod, searchCompoundList, limitation);
	}
	
	
	/**	Assign a method name with a given department to prevent not association when the method department is edited.
	*	The name is produced as follow: Location-Department-Method Count Number.
	* @author   Dimitrios Sgourdos
	* @version  19-Nov-2013
	* @param    methodId				The Id the method
	* @param    methodDepartment		The department of the method
	* @return   The new name of the method
	*/
	public static String checkNameAndDepartmentAssignment (Id methodId, String methodDepartment) {
		// Declare variables
		LaboratoryMethod__c tmpLabMethod = new LaboratoryMethod__c();
		String				itemName, itemNum;
		
		// Assign the name with the correct department
		try {
			tmpLabMethod = LaboratoryMethodDataAccessor.getById(methodId);
			itemName   = tmpLabMethod.Name;
		} catch(Exception e) {
			return '';
		}
		
		if(! String.IsBlank(itemName) ) {
			itemNum = itemName.substringAfterLast('-');
			// The location cannot be changed after the creation of the method
			itemName = tmpLabMethod.Location__c + '-' + methodDepartment + '-' + itemNum;
		} else {
			itemName = '';
		}
		return itemName;
	}
}