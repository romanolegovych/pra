/**
*   'PRA_ProjectWorkflowController' class is to display the work breakdown structure(WBS) for a single projects in P&F.
*   @author   Devaram Bhargav
*   @version  21-Oct-2013
*   @since    21-Oct-2013
*/
public class PRA_ProjectWorkflowController {
    
    /** WBSProject is record to hold the WBS_Project__c of a single project.*/
    public WBS_Project__c WBSProject{get;set;}
    
    /** Variables to enable the icons and buttons upon the status of a client tasks*/
    public Boolean InitiateWBS {get;set;}  // to Initiate WBS Variatble.
    public Boolean ExportWBS   {get;set;}  // to Export   WBS Variatble.
    public Boolean ConfirmWBS  {get;set;}  // to Confirm  WBS Variatble.   
    
    //this list hold all the WBS csv file data in it, used to display the excel sheet in PRA_ProjectWorkflowWBSCSV vf page.
    public List<PRA_ProjectWBSCVO.ExportWBSCVO> wbsListFile {get;set;}
    
    //this list hold all the BD1 csv file data in it, used to display the excel sheet in PRA_ProjectWorkflowBD1CSV vf page.
    public List<PRA_ProjectWBSCVO.ExportBD1CVO> bd1ListFile {get;set;}
    
    //this list hold all the BD9 csv file data in it, used to display the excel sheet in PRA_ProjectWorkflowBD9CSV vf page.
    public List<PRA_ProjectWBSCVO.ExportBD9CVO> bd9ListFile {get;set;} 
    
    //this list hold all the error messages while any SOQL or DML are performed.
    Public String ErrorMessage{get;set;}
    
    //this string holds the excel button value.
    public String ExcelButton{get;set;}
    
    /** PAGE_MODE,PAGE_MODE_VIEW,PAGE_MODE_EDIT variable is to display the type of mode in Visual force page*/     
    public String PAGE_MODE{get;private set;} 
    public String PAGE_MODE_VIEW {get{return PRA_Constants.PAGE_MODE_VIEW;}}
    public String PAGE_MODE_EDIT {get{return PRA_Constants.PAGE_MODE_EDIT;}}   
    
    /**
    *  PRA_ProjectWorkflowController is a Constructor that return the WBS_Project__c record for a single project.            
    */
    public PRA_ProjectWorkflowController () {  
       
        //call to get the method
        PAGE_MODE =  PRA_Constants.PAGE_MODE_VIEW; 
        getProjectWBSStatus();
    }
    
    /**
    *  getWBSProjectStatus method is to get the wbs_project__c data and the workflow for particiular project.              
    */
    public Void  getProjectWBSStatus(){
        
         //Initiating the varaible WBSProject. 
        WBSProject = new WBS_Project__c();
        
        /**
        *   Method retrieve the  wbs_project__c record for the attribute value (id).
        *   wbsproject record for the id passed from PRA_WBSProject page.
        */
        List<WBS_Project__c> wbsprojectList=PRA_DataAccessor.getWBSprojectbyID(ApexPages.currentPage().getParameters().get('id'));
        if(!wbsprojectList.isEmpty()){      
            wbsproject = wbsprojectList[0];
        }
        
        //Initialise the variables with default false.
        InitiateWBS=false;
        ExportWBS  =false;
        ConfirmWBS =false;
        
        List<Aggregateresult> ProjectWBSList=PRA_WBSService.getClientTaskCountbyStatus(wbsproject.Client_Project__r.name);
        for(Aggregateresult ar:ProjectWBSList){
                        
            //Assign to the Map for all different status.
            if(ar.get('s')==PRA_Constants.CU_STATUS_NEW){
                InitiateWBS  =  true;                //Assign true if you have client task with status New
            }else
            if(ar.get('s')==PRA_Constants.CU_STATUS_READY){
                ExportWBS    =  true;                //Assign true if you have client task with status Ready 
            }else
            if(ar.get('s')==PRA_Constants.CU_STATUS_SENT){
                ConfirmWBS   =  true;                //Assign true if you have client task with status Sent
            }       
        }   
    }
    
    /**
    *  EditWBSProject method is to Edit the wbs_project__c data for particiular project.              
    */
    public Void  editWBSProject(){
    
      //Make the page open in EDIT mode
      PAGE_MODE =  PRA_Constants.PAGE_MODE_EDIT;
    }
    
    /**
    *  viewWBSProject method is to view the wbs_project__c data for particiular project.              
    */
    public Void  viewWBSProject(){
    
      //Make the page open in EDIT mode
      PAGE_MODE =  PRA_Constants.PAGE_MODE_VIEW;
      getProjectWBSStatus();
    }
    
    /**
    *  saveWBSProject method is to save the wbs_project__c data for particiular project.              
    */
    public Void  saveWBSProject(){
        
        //Make a list to save the WBS Project record.     
        List<WBS_Project__c> wbsprojectList = new List<WBS_Project__c>();
        
        //add the single record to to upsert.
        wbsprojectList.add(wbsproject);
        
        //perform the list upsert.
        String ErrorMsg = PRA_DataAccessor.upsertWBSProject(WBSProjectList);       
        system.debug('------wbsproject------'+wbsproject);
        PAGE_MODE =  PRA_Constants.PAGE_MODE_VIEW; 
        //if you found error upon upsert dispaly error message.     
        if(ErrorMsg!=null){ 
            PAGE_MODE =  PRA_Constants.PAGE_MODE_EDIT; 
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, ErrorMsg));                       
        }else{
            getProjectWBSStatus();
        }        
    } 
    
    /**
    *  initiateWBSProject method is to initiate the client tasks from 'New' to 'Ready' status for particiular project.              
    */
    public Void  initiateWBSProject(){
        
        //get the client task by New status and update with to Ready status.
        String ErrorMsg = PRA_WBSService.saveClientTaskbyStatus(wbsproject.Client_Project__r.name, PRA_Constants.CU_STATUS_NEW, PRA_Constants.CU_STATUS_READY);       
        system.debug('------ErrorMsg------'+ErrorMsg);
        
        //if you found error upon upsert dispaly error message.     
        if(ErrorMsg!=null){ 
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, ErrorMsg));                       
        } 
        getProjectWBSStatus(); 
    } 
    
    /**
    *  SaveandexportWBSProject method is to export the wbs csv files and change the client tasks  from  'Ready' to 'Sent' status for particiular project.              
    */
    public Void  SaveandExportWBSProject(){
        
        try{             
            ErrorMessage= null;                  
            //get the client task by Ready status and update with to Sent status.
            String ErrorMsg = PRA_WBSService.saveClientTaskbyStatus(wbsproject.Client_Project__r.name, PRA_Constants.CU_STATUS_READY,PRA_Constants.CU_STATUS_SENT);                  
            system.debug('------ErrorMsg------'+ErrorMsg);
            exportWBSProject();
            
        }catch(Exception ex) {
            ErrorMessage=ex.getMessage();               
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, ex.getMessage()));             
        }            
    }      
            
    
    /**
    *  exportWBSProject method is to export the wbs csv files for particiular project.              
    */
    public Void  exportWBSProject(){
        
        try{             
            ErrorMessage= null;                 
           
            //initiate the wbs file.
            wbsListFile = new List<PRA_ProjectWBSCVO.ExportWBSCVO>();
            
            //get the data into the list by passing the project name int othe method.
            wbsListFile = PRA_WBSExportService.createWBSFile(wbsproject.Client_Project__c);
            system.debug('------wbsListFile ------'+wbsListFile);
            
            //initiate the wbs file.
            bd1ListFile = new List<PRA_ProjectWBSCVO.ExportBD1CVO>();
            
            //get the data into the list by passing the project name int othe method.
            bd1ListFile = PRA_WBSExportService.createBD1File(wbsproject.Client_Project__c);
            system.debug('------bd1ListFile ------'+bd1ListFile);
            
            //initiate the wbs file.
            bd9ListFile = new List<PRA_ProjectWBSCVO.ExportBD9CVO>();
            
            //get the data into the list by passing the project name int othe method.
            bd9ListFile = PRA_WBSExportService.createBD9File(wbsproject.Client_Project__c);
            system.debug('------bd9ListFile ------'+bd9ListFile);
             
            getProjectWBSStatus();            
            system.debug('------ErrorMessage------'+ErrorMessage);
            
        }catch(Exception ex) {
            ErrorMessage=ex.getMessage();               
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, ex.getMessage()));             
        }            
    } 
    
    /**
    *  confirmWBSProject method is to confirm the client tasks  from  'Sent' to null status for particiular project.              
    */
    public Void  confirmWBSProject(){
        
        //get the client task by Sent status and update with to Confirm status.
        String ErrorMsg = PRA_WBSService.saveClientTaskbyStatus(wbsproject.Client_Project__r.name,PRA_Constants.CU_STATUS_SENT,'');       
        system.debug('------ErrorMsg------'+ErrorMsg);
        
        //if you found error upon upsert dispaly error message.     
        if(ErrorMsg!=null){ 
            Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, ErrorMsg));                       
        } 
        getProjectWBSStatus();            
    } 
    
    /**
    *  exportfile method is to export the WBS,BD1,BD9 csv file for particiular project with variable value ExcelButton.              
    */
    public PageReference exportfile(){
        
        system.debug('------ExcelButton------'+ExcelButton);
        PageReference pageRef=new PageReference('/apex/PRA_ProjectWorkflow'+ExcelButton+'CSV');
        pageRef.setRedirect(false);      
        return pageRef;           
    }
    
    /**
    *  getShowPZblock method is to show the PZ block in the VF page.              
    */
    public boolean getShowPZblock(){
        
        //get the PZblock
        Boolean  retVal=PRA_WBSService.getPZblock(UserInfo.getProfileId());
        return retVal;
    }
    
    /**
    *  getShowFAblock method is to show the FA block in the VF page and show the save and cancel button to FA only.              
    */
    public boolean getShowFAblock(){
        
        //get the PZblock        
        Boolean  retVal=PRA_WBSService.getFAblock(UserInfo.getUserId());
        return retVal;
    }
}