/*
* Tests are in PAWS_ProjectFlowTests.cls
*/
public with sharing class PAWS_GanttStepPropertyTrigger extends STSWR1.AbstractTrigger
{
	public override void beforeInsert(List<SObject> records)
    {
    	fillProject((List<STSWR1__Gantt_Step_Property__c>)records);
    }
    
    public override void beforeUpdate(List<SObject> records, List<SObject> oldRecords)
    {
    	fillProject((List<STSWR1__Gantt_Step_Property__c>)records);
    }
    
    public override void afterInsert(List<SObject> records)
    {
    	sendRevisedChangedEmail((List<STSWR1__Gantt_Step_Property__c>)records, null);
    }
    
    public override void afterUpdate(List<SObject> records, List<SObject> oldRecords)
    {
    	sendRevisedChangedEmail((List<STSWR1__Gantt_Step_Property__c>)records, (List<STSWR1__Gantt_Step_Property__c>)oldRecords);
    }
    
    private void fillProject(List<STSWR1__Gantt_Step_Property__c> records)
    {
    	Map<ID, ID> flowProjectMap = new Map<ID, ID>();
    	for(STSWR1__Gantt_Step_Property__c record : records)
    	{
    		if(!String.isEmpty(record.STSWR1__Parent_Flow_Id__c)) flowProjectMap.put((ID)record.STSWR1__Parent_Flow_Id__c, null);
    	}
    	
    	PAWS_Utilities.checkLimit('Queries', 1);
    	for(PAWS_Project_Flow_Junction__c junction : [select Flow__c, Project__c from PAWS_Project_Flow_Junction__c where Flow__c in :flowProjectMap.keySet()])
    	{
    		flowProjectMap.put(junction.Flow__c, junction.Project__c);
    	}
    	
    	for(STSWR1__Gantt_Step_Property__c record : records)
    	{
    		record.Project__c = flowProjectMap.get((ID)record.STSWR1__Parent_Flow_Id__c);
    	}
    }
    
    private void sendRevisedChangedEmail(List<STSWR1__Gantt_Step_Property__c> records, List<STSWR1__Gantt_Step_Property__c> oldRecords)
    {
    	Map<ID, ecrf__c> projectsMap = new Map<ID, ecrf__c>();
    	Map<ID, List<STSWR1__Gantt_Step_Property__c>> projectPropertiesMap = new Map<ID, List<STSWR1__Gantt_Step_Property__c>>();
		for(Integer i = 0; i < records.size(); i++)
		{
			if(records[i].Project__c == null || records[i].STSWR1__Revised_End_Date__c == null) continue;
			
			if(oldRecords != null && records[i].STSWR1__Revised_End_Date__c != oldRecords[i].STSWR1__Revised_End_Date__c) 
    		{
    			if(!projectPropertiesMap.containsKey(records[i].Project__c)) projectPropertiesMap.put(records[i].Project__c, new List<STSWR1__Gantt_Step_Property__c>());
                projectPropertiesMap.get(records[i].Project__c).add(records[i]);
                projectsMap.put(records[i].Project__c, null);
    		}else if(oldRecords == null) 
    		{
    			if(!projectPropertiesMap.containsKey(records[i].Project__c)) projectPropertiesMap.put(records[i].Project__c, new List<STSWR1__Gantt_Step_Property__c>());
                projectPropertiesMap.get(records[i].Project__c).add(records[i]);
                projectsMap.put(records[i].Project__c, null);
    		}
		}
		
		if(projectsMap.size() == 0) return;

		PAWS_Utilities.checkLimit('Queries', 1);
		projectsMap = new Map<ID, ecrf__c>([select Project_Manager__r.Email, Clinical_Informatics_Manager__r.Email from ecrf__c where Id in :projectsMap.keySet()]);
    	
    	List<STSWR1__Email_Message__c> messages = new List<STSWR1__Email_Message__c>();
		for(ID projectId : projectPropertiesMap.keySet())
		{
			ecrf__c project = projectsMap.get(projectId);
			if(project == null || (project.Project_Manager__r == null && project.Clinical_Informatics_Manager__r == null)) 
				continue;
			
			List<String> ids = new List<String>();
			for(STSWR1__Gantt_Step_Property__c property : projectPropertiesMap.get(projectId))
				ids.add(property.Id);

			STSWR1__Email_Message__c message = new STSWR1__Email_Message__c(STSWR1__Type__c='Revised Changed', STSWR1__Subject__c=projectId);
			message.STSWR1__To__c = (project.Project_Manager__r != null ? project.Project_Manager__r.Email : project.Clinical_Informatics_Manager__r.Email);
			message.STSWR1__Additional_Data__c = String.join(ids, ',');
			
			messages.add(message);
		}

		if(messages.size() > 0)
		{
			PAWS_Utilities.checkLimit('DMLStatements', 1);
			insert messages;
		}
    }
}