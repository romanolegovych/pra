/**
 * @description	This class holds generic functions used ny the 'Notification and Communication Module'
 * @author		Dimitrios Sgourdos
 * @date		Created: 02-Sep-2015, Edited: 14-Sep-2015
 */
public with sharing class NCM_Utils {
	
	/**
	 * @description	Group the given list to a readable format set for dynamic queries
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 02-Sep-2015
	 * @param		value    The given list of values
	 * @return		The created set in readable format for dynamic queries
	*/
	public static String createSetForDynamicQuery(List<String> value) {
		return '\'' + String.join(value,'\',\'') + '\'';
	}
	
	
	/**
	 * @description	Create dummy users for the test classes
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 02-Sep-2015
	 * @param		numberOfUsers    The number of users to be created
	 * @return		The created users
	*/
	public static list<User> buildTestUserList (Integer numberOfUsers) {
		Profile p = [select id from profile where name='System Administrator' limit 1];
		
		List<User> results = new list<User>();
		
		for (integer i = 0 ; i < numberOfUsers; i++ ) {
			String uniqueEmail = COM_Utils.generateRandomEmail();
			
			results.add(	new User(	firstname = 'NCM',
										lastName = 'TESTUSER',
										email = uniqueEmail,
										Username = uniqueEmail,
										EmailEncodingKey = 'ISO-8859-1',
										Alias = uniqueEmail.substring(18, 23),
										TimeZoneSidKey = 'America/Los_Angeles',
										LocaleSidKey = 'en_US',
										LanguageLocaleKey = 'en_US',
										ProfileId = p.Id,
										isActive = true )
			);
		}
		insert results;
		
		return results;
	}
}