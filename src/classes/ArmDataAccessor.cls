public with sharing class ArmDataAccessor {

	/** Object definition for fields used in application for Arm__c
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('Arm__c');		
	}
	
	
	/** Object definition for fields used in application for Arm__c with the parameter referenceName as a prefix
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'ClinicalDesign__c,';
		result += referenceName + 'Arm_Number__c,';
		result += referenceName + 'Description__c';
		return result;
	}
	
	/** Retrieve list of arms.
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @param	whereClause			The criteria that Laboratory Selection Lists must meet
	 * @return	List of arms
	 */
	public static List<Arm__c> getArmList(String whereClause) {
		
		// no soql limits are counted for getting child records
		String query = String.format(
								'SELECT {0} , (select {2} from flowchartassignments__r) ' +
								'FROM Arm__c ' +
								'WHERE  {1} ' +
								'ORDER BY Arm_Number__c',
								new List<String> {
									getSObjectFieldString(),
									whereClause,
									FlowchartAssignmentDataAccessor.getSObjectFieldString('')
								}
							);
		return (List<Arm__c>) Database.query(query);
	}

}