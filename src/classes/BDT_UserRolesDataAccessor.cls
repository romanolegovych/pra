public with sharing class BDT_UserRolesDataAccessor {

	/**
	 * Add standard reference name to field list
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @return String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('BDT_UserRoles__c');		
	}

	/**
	 * Add custom reference name to field list
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @return String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'RoleNumbers__c';
		return result;
	}

	/**
	 * Get all users associated to BDT
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @return list<User>
	 */
	public static list<User> getBDTUsers () {
		Set<Id> BDT_Users = new Set<Id>();
		For (bdt_userroles__c bur : [select name from bdt_userroles__c]) {
			BDT_Users.add(ID.valueOf(bur.name));
		}
		List<User> BDTUserList = [select Id, Name, Email from User where id in :BDT_Users order by Name];
		return BDTUserList;
	}

	/**
	 * Get SF user ID for specific name
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @param String InputName User name
	 * @return String
	 */
	public static String getSFUser(String InputName) {
		try {
			user sfUser =  [select id from user where Name = :InputName limit 1];
			if (sfUser != null) {
				return sfUser.id;
			}
		} catch (Exception e) {}
		return null;
	}

	/**
	 * Get users roles
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @param String UserId User name
	 * @return String
	 */
	public static BDT_UserRoles__c getUserRoles(String userId) {
		return BDT_UserRoles__c.getInstance(userId);
	}

	/**
	 * Get user's name
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @param String UserId User ID
	 * @return String
	 */
	public static String getUserName (String UserId) {
		try {
			user sfUser =  [select Name from user where id = :UserId limit 1];
			if (sfUser != null) {
				return sfUser.name;
			}
		} catch (Exception e) {}
		return null;
	}

	/**
	 * Get userroles for specific rolenumber
	 * @author Maurice Kremer
	 * @version 15-Oct-13
	 * @param Decimal roleNumber Rolenumber
	 * @return List<BDT_UserRoles__c>
	 */
	public static List<BDT_UserRoles__c> getUserRolesByRoleNumber (Decimal roleNumber) {
		String query = String.format('select {0} from BDT_UserRoles__c where RoleNumbers__c like {1}',
								new List<String> {
									getSObjectFieldString(),
									'\'%' + roleNumber + '%\''
								}
							);
		return Database.query(query);
	}

	/**
	 * Get users for specific role
	 * @author Maurice Kremer, Dimitrios Sgourdos
	 * @version 29-Nov-13
	 * @param Decimal roleNumber Rolenumber
	 * @return List<BDT_UserRoles__c>
	 */
	public static List<User> getUserRolesByRoleNumber (String RoleNumber) {
		// skip if there if no valid rolenumber
		if (String.isBlank(RoleNumber)) {
			return null;
		}

		String likeStatement = '%'+RoleNumber+'%';
		Set<String> userIds = BDT_Utils.getStringSetFromList([select Name from BDT_UserRoles__c where RoleNumbers__c like :likeStatement],'Name');

		return [select id, name from user where id in :userIds order by Name];
	}

}