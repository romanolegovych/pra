/** Implements the Selector Layer of the object LaboratorySelectionLists__c
 * @author	Dimitrios Sgourdos
 * @version	07-Nov-2013
 */
public with sharing class LaboratorySelectionListsDataAccessor {
	
	/** Object definition for fields used in application for LaboratorySelectionLists
	 * @author	Dimitrios Sgourdos
	 * @version 15-Oct-2013
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('LaboratorySelectionLists__c');		
	}
	
	
	/** Object definition for fields used in application for LaboratorySelectionLists with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 07-Nov-2013
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'listFirstValue__c,';
		result += referenceName + 'listSecondvalue__c';
		return result;
	}
	
	
	/** Retrieve the Laboratory Selection Lists that meet the given whereClause.
	 * @author	Dimitrios Sgourdos
	 * @version 19-Nov-2013
	 * @param	whereClause			The criteria that Laboratory Selection Lists must meet
	 * @return	The Laboratory Selection Lists list.
	 */
	public static List<LaboratorySelectionLists__c> getLaboratorySelectionLists(String whereClause) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM LaboratorySelectionLists__c ' +
								'WHERE  {1}',
								new List<String> {
									getSObjectFieldString(),
									whereClause
								}
							);
		return (List<LaboratorySelectionLists__c>) Database.query(query);
	}
	
	
	/** Retrieve the Laboratory Selection Lists for the given category.
	 * @author	Dimitrios Sgourdos
	 * @version 19-Nov-2013
	 * @param	categoryName		The selection category that we want to retrieve its values
	 * @return	The Laboratory Selection Lists list.
	 */
	public static List<LaboratorySelectionLists__c> getByListCategory (String categoryName) {
		String whereClause = String.format(
								'NAME = {0} AND	listFirstValue__c != NULL AND	listFirstValue__c != {1}',
								new List<String> {
									'\'' + categoryName + '\'',
									'\'' + '\''
								}
							);
		return getLaboratorySelectionLists(whereClause);
	}
	
	
	/** Retrieve all the Laboratory Selection Lists of a specific type (categories or values).
	 * @author	Dimitrios Sgourdos
	 * @version 19-Nov-2013
	 * @param	categoryTypeFlag	A flag to show if the type is categories or not (then it is values)
	 * @return	The Laboratory Selection Lists list.
	 */
	public static List<LaboratorySelectionLists__c> getAllByType (Boolean categoryTypeFlag) {
		// Create where clause depends on the type (categories or values)
		String whereClause;
		if(categoryTypeFlag) {
			whereClause = 'listFirstValue__c = NULL OR listFirstValue__c = ' + '\'' + '\'';
		} else {
			whereClause = 'listFirstValue__c != NULL AND listFirstValue__c != ' + '\'' + '\'';
		}
		
		// Query 
		return getLaboratorySelectionLists(whereClause);
	}
	
	
	/** Retrieve all the Laboratory Selection Lists Values for all categories.
	 * @author	Dimitrios Sgourdos
	 * @version 16-Oct-2013
	 * @return	The Laboratory Selection Lists values list.
	 */
	public static List<LaboratorySelectionLists__c> getAllListValues () {
		return getAllByType(false);
	}
	
	
	/** Retrieve all the Laboratory Selection Lists Categories.
	 * @author	Dimitrios Sgourdos
	 * @version 16-Oct-2013
	 * @return	The Laboratory Selection Lists categories list.
	 */
	public static List<LaboratorySelectionLists__c> getAllListCategories () {
		return getAllByType(true);
	}
	
	
	/** Get the Matrix values that an Anti-coagulant can be assigned.
	 * @author	Dimitrios Sgourdos
	 * @version 21-Oct-2013
	 * @return	The list of Matrix values
	 */
	public static List<String> getMatrixValuesEnablingAnticoagulant () {
		List<String> valuesList = new List<String>{'Plasma','Whole blood', 'Blood'};
		return valuesList;
	}
	
	
	/** Get the Analytical technique values that a Detection can be assigned.
	 * @author	Dimitrios Sgourdos
	 * @version 21-Oct-2013
	 * @return	The list of Analytical technique values
	 */
	public static List<String> getAnalyticalTechniqueValuesEnablingDetection () {
		List<String> valuesList = new List<String>{'LC'};
		return valuesList;
	}
	
}