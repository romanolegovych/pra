/** Implements the Selector Layer of the object Study__c
 * @author	Dimitrios Sgourdos
 * @version	18-Nov-2013
 */
public with sharing class StudyDataAccessor {
	
	/** Object definition for fields used in application for Study
	 * @author	Dimitrios Sgourdos
	 * @version 09-Oct-2013
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('Study__c');
	}
	
	
	/** Object definition for fields used in application for Study with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 07-Nov-2013
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'BDTDeleted__c,';
		result += referenceName + 'Business_Unit__c,';
		result += referenceName + 'Legacy_Code__c,';
		result += referenceName + 'Project__c,';
		result += referenceName + 'Protocol_Title__c,';
		result += referenceName + 'RegulatoryStatus__c,';
		result += referenceName + 'Title__c,';
		result += referenceName + 'Sponsor_Code__c,';
		result += referenceName + 'Status__c,';
		result += referenceName + 'Code__c';
		return result;
	}
	
	
	/** Retrieve the List of Studies that are described in the string listOfStudiesIds ordered by their code
	 * @author	Dimitrios Sgourdos
	 * @version 09-Oct-2013
	 * @param	listOfStudiesIds	A string that holds the desired studies separated with the delimiter ':'
	 * @return	The list of Studies
	 */
	public static List<Study__c> getByStudyIds (String listOfStudiesIds) {
		List<String> studiesIdsList = listOfStudiesIds.split(':');
		String query = String.format(
								'SELECT {0} ' +
								'FROM Study__c ' +
								'WHERE Id in :studiesIdsList ' +
								'ORDER BY Code__c',
								new List<String> {
									getSObjectFieldString()
								}
							);
		return (List<Study__c>) Database.query(query);
	}
	
	
	/** Retrieve the List of Studies that are selected by the current user
	 * @author	Dimitrios Sgourdos
	 * @version 09-Oct-2013
	 * @return	The list of Studies
	 */
	public static List<Study__c> getByUserPreference() {
		return  getByStudyIds(BDT_Utils.getPreference('SelectedStudies'));	
	}
	
	
	/** Retrieve the List of Studies that belong to the given project and they are not deleted.
	 * @author	Dimitrios Sgourdos
	 * @version 05-Nov-2013
	 * @return	The list of Studies
	 */
	public static List<Study__c> getByProjectId(String projectId) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM Study__c ' +
								'WHERE Project__c = {1} ' +
								'AND BDTDeleted__c = false ' +
								'ORDER BY Code__c',
								new List<String> {
									getSObjectFieldString(),
									'\'' + projectId + '\''
								}
							);
		return (List<Study__c>) Database.query(query);
	}
	
	
	/** Retrieve the List of Studies that are described in the string listOfStudiesIds and they have
	 *	LaboratoryAnalysis selection as it is described in the parameter description.
	 * @author	Dimitrios Sgourdos
	 * @version 18-Nov-2013
	 * @param	listOfStudiesIds	A string that holds the desired studies separated with the delimiter ':'
	 * @param	description			The LaboratoryAnalysis selection that is assigned with the studies
	 * @return	The list of Studies
	 */
	public static List<Study__c> getByUsedType (String listOfStudiesIds, String description) {
		List<String> studiesIdsList = listOfStudiesIds.split(':');
		String query = String.format(
								'SELECT {0} ' +
								'FROM Study__c ' +
								'WHERE Id in :studiesIdsList ' +
								'AND Id in 	(select Study__c ' +
											 'from LaboratoryAnalysis__c ' +
											 'where {1}' +
											 ' AND {2})' +
								'ORDER BY Code__c',
								new List<String> {
									getSObjectFieldString(),
									description,
									'Study__c IN :studiesIdsList'
								}
							);
		return (List<Study__c>) Database.query(query);
	}
	
	
	/** Retrieve the List of Studies that are described in the string listOfStudiesIds and they have
	 *	LaboratoryAnalysis with Analysis__c selected assigned to them.
	 * @author	Dimitrios Sgourdos
	 * @version 18-Nov-2013
	 * @param	listOfStudiesIds	A string that holds the desired studies separated with the delimiter ':'
	 * @return	The list of Studies
	 */
	public static List<Study__c> getByUsedInAN (String listOfStudiesIds) {
		return getByUsedType(listOfStudiesIds, 'Analysis__c = TRUE');
	}
	
	
	/** Retrieve the List of Studies that are described in the string listOfStudiesIds and they have
	 *	LaboratoryAnalysis with MethodDevelopment__c or MethodValidation__c selected assigned to them.
	 * @author	Dimitrios Sgourdos
	 * @version 18-Nov-2013
	 * @param	listOfStudiesIds	A string that holds the desired studies separated with the delimiter ':'
	 * @return	The list of Studies
	 */
	public static List<Study__c> getByUsedInMdOrVal (String listOfStudiesIds) {
		return getByUsedType(listOfStudiesIds, '(MethodDevelopment__c = TRUE or MethodValidation__c = TRUE)');
	}
	
	
	/** Retrieve the List of Studies that are described in the string listOfStudiesIds and they have
	 *	LaboratoryAnalysis with MethodDevelopment__c selected assigned to them.
	 * @author	Dimitrios Sgourdos
	 * @version 18-Nov-2013
	 * @param	listOfStudiesIds	A string that holds the desired studies separated with the delimiter ':'
	 * @return	The list of Studies
	 */
	public static List<Study__c> getByUsedInMd (String listOfStudiesIds) {
		return getByUsedType(listOfStudiesIds, 'MethodDevelopment__c = TRUE');
	}
	
	
	/** Retrieve the List of Studies that are described in the string listOfStudiesIds and they have
	 *	LaboratoryAnalysis with MethodValidation__c selected assigned to them.
	 * @author	Dimitrios Sgourdos
	 * @version 18-Nov-2013
	 * @param	listOfStudiesIds	A string that holds the desired studies separated with the delimiter ':'
	 * @return	The list of Studies
	 */
	public static List<Study__c> getByUsedInVal (String listOfStudiesIds) {
		return getByUsedType(listOfStudiesIds, 'MethodValidation__c = TRUE');
	}
}