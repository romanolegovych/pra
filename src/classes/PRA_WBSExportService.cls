/*
*
*   This service contains functions to create the 
*   WBS files that are imported into Lawson
*
*/
public with sharing class PRA_WBSExportService {
    public class WBSExportException extends Exception{}
    
    static String WBS_EXP_ONLY            = 'Expense Only';
    static String WBS_EXP_TASK_ADDR       = '536';
    static String WBS_EXP_ACTIV_ADDR      = '203';
    static String WBS_EXP_POST_ADDR_EXPEN = '770203';
    static String WBS_EXP_POST_ADDR_LBREE = '615601';
    static String WBS_EXP_BUFUNC_EXPEN    = 'EXPEN';
    static String WBS_EXP_BUFUNC_LBREE    = 'LBREE';
    static String WBS_TG_PROJMAN		  = 'PROJ MAN';

	static Integer AGROUP_MAX = 12;
	    
    /*
    *	Validates a WBS Project record for completeness
    *	@param	A list of WBS_Project__c records, but assumes only a single item in the list
    */
    public static Boolean validateWbsProjectList(List<WBS_Project__c> wbsProjectList) {
	    if (wbsProjectList.size() < 1) {
	    	return false;
	    }
    	return validateWbsProject(wbsProjectList[0]);
    }
    
    /*
    *	Validates a WBS Project record for completeness
    *	@param	A WBS_Project__c record
    */
    public static Boolean validateWbsProject(WBS_Project__c wbsProj) {
    	if (String.isBlank(wbsProj.Activity_Group__c) ||
    		String.isBlank(wbsProj.Invoice_Method__c) ||
    		String.isBlank(wbsProj.Lawson_Status__c) ||
    		String.isBlank(wbsProj.Company_Number__c) ||
    		wbsProj.Contract_Level_Address__c < 1 ||
    		wbsProj.Project_Level_Address__c < 1) {
    		return false;
    	}
		return true;    	
    }
    
    /*
    *   Create WBS file
    *   @param ID of a Client_Project__c record
    */
    public static List<PRA_ProjectWBSCVO.ExportWBSCVO> createWBSFile(id projectID) {
        // Init the return list
        List<PRA_ProjectWBSCVO.ExportWBSCVO> listFile = new List<PRA_ProjectWBSCVO.ExportWBSCVO>();

        // Gather all data
        try {
            // WBS Project
            List<WBS_Project__c> wbsProjectList = PRA_DataAccessor.getWbsProjectByProjID(projectID);
            if (! validateWbsProjectList(wbsProjectList)) {
                throw new WBSExportException('WBS Project data is missing.');
            }
            WBS_Project__c wbsProject = wbsProjectList[0];
        	
        	
            // Task Data
            List<Client_Task__c> wbsTaskList = PRA_DataAccessor.getWbsTaskData(projectID, PRA_Constants.CU_STATUS_SENT);
            if (wbsTaskList.size() < 1) {
                throw new WBSExportException('No Client Task Data Found');
            }
            
            // This function throws a function if invalid data is discovered
            // The exception will be caught and rethrown
            validateClientTaskData(wbsTaskList);
            
			// Create Set of "distinct" task groups from the task list
			Set<Id> wbsTaskGroupSet = new Set<Id>();
            for (Client_Task__c ct : wbsTaskList) {
                wbsTaskGroupSet.add(ct.Task_Group__c);
            }
			// The task group 'PROJ MAN' must always exist.  Add it if missing.
			Task_Group__c tgProjManID = [SELECT id FROM Task_Group__c WHERE Lawson_Short_Desc__c = :WBS_TG_PROJMAN LIMIT 1]; 
			wbsTaskGroupSet.add(tgProjManID.id);

			// Task Group Data
			List<Task_Group__c> wbsTaskGroupList = PRA_DataAccessor.getWbsTaskGroupData(wbsTaskGroupSet);
			
            listFile.addAll(createWbsContractLine(wbsProject));
            listFile.addAll(createWbsProjectLine(wbsProject));  
            listFile.addAll(createWbsTaskGroupLines(wbsProject, wbsTaskGroupList));
            listFile.addAll(createWbsTaskLines(wbsProject, wbsTaskList));
            listFile.addAll(createWbsPostLines(wbsProject, wbsTaskList));

        } catch (WBSExportException e) {
            throw new WBSExportException(e.getMessage());
        } catch (Exception e) {
            system.debug('----- Exception in createWBSFile()');
            system.debug('----- ' + e.getMessage());
            system.debug('----- ' + e.getTypeName());
			throw new WBSExportException(e.getMessage(), e);
        }
    
        return listFile;
    } 
    
    /*
    *   Creates the contract (Level 1) line for a WBS export file
    *   @param  WBS_Project__c record for the project
    */
    static List<PRA_ProjectWBSCVO.ExportWBSCVO> createWbsContractLine(WBS_Project__c wbsProj) {
        List<PRA_ProjectWBSCVO.ExportWBSCVO> rtnList = new List<PRA_ProjectWBSCVO.ExportWBSCVO>();
        
        PRA_ProjectWBSCVO.ExportWBSCVO wbsCVO;

        wbsCVO = new PRA_ProjectWBSCVO.ExportWBSCVO();

        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_RUN_GRP] = wbsProj.Reference_Number__c;
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY] = wbsProj.Eight_Character_Contract__c;
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_SHORT_DESC] = wbsProj.Activity_Group__c.Left(AGROUP_MAX) + ' (' + wbsProj.Eight_Character_Contract__c + ')';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY_GRP] = wbsProj.Activity_Group__c;
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_COMPANY_NUM] = wbsProj.Company_Number__r.Name;
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACCOUNT_UNIT] = wbsProj.Accounting_Unit__c;
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_STATUS_CODE] = wbsProj.Lawson_Status__c; 
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_CTRCT_ADDR] = String.valueOf(wbsProj.Contract_Level_Address__c);
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PROJECT_ADDR] = '0';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIV_ADDR] = '0';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_TASK_ADDR] = '0';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_POSTING_ADDR] = '0';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_START_DATE] = wbsFormatDate(wbsProj.Start_Date__c); 
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_END_DATE] = wbsFormatDate(wbsProj.End_Date__c);
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LONG_DESC] = wbsProj.Activity_Group__c + ' (' + wbsProj.Eight_Character_Contract__c + ')';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LEVEL_TYPE] = 'S';  
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PRJ_CURRENCY] = wbsProj.Currency__c;

        rtnList.add(wbsCVO);

        return rtnList;
    }

    /*
    *   Creates the project (Level 2) line for a WBS export file
    *   @param  WBS_Project__c record for the project
    */
    static List<PRA_ProjectWBSCVO.ExportWBSCVO> createWbsProjectLine(WBS_Project__c wbsProj) {
        List<PRA_ProjectWBSCVO.ExportWBSCVO> rtnList = new List<PRA_ProjectWBSCVO.ExportWBSCVO>();
        
        PRA_ProjectWBSCVO.ExportWBSCVO wbsCVO;

        wbsCVO = new PRA_ProjectWBSCVO.ExportWBSCVO();

        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_RUN_GRP] = wbsProj.Reference_Number__c;
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY] = wbsProj.Client_Project__r.name;
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_SHORT_DESC] = wbsProj.Activity_Group__c.Left(AGROUP_MAX) + ' (' + wbsProj.Client_Project__r.name + ')';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY_GRP] = wbsProj.Activity_Group__c;
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_COMPANY_NUM] = wbsProj.Company_Number__r.Name;
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACCOUNT_UNIT] = wbsProj.Accounting_Unit__c;
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_STATUS_CODE] = wbsProj.Lawson_Status__c; 
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_CTRCT_ADDR] = String.valueOf(wbsProj.Contract_Level_Address__c);
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PROJECT_ADDR] = String.valueOf(wbsProj.Project_Level_Address__c);
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIV_ADDR] = '0';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_TASK_ADDR] = '0';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_POSTING_ADDR] = '0';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_START_DATE] = wbsFormatDate(wbsProj.Start_Date__c); 
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_END_DATE] = wbsFormatDate(wbsProj.End_Date__c);
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LONG_DESC] = wbsProj.Activity_Group__c + ' (' + wbsProj.Client_Project__r.name + ')';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LEVEL_TYPE] = 'C';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_1] = 'SETUP YEAR';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_1] =  wbsProj.Setup_Year__c;
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_2] = 'INVOICE METH';
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_2] = wbsProj.Invoice_Method__c;
        wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PRJ_CURRENCY] = wbsProj.Currency__c;

        rtnList.add(wbsCVO);

        return rtnList;
    }

    /*
    *   Creates the lines for the task group (Level 3) section of a wbs export file
    *   @param  WBS_Project__c record for the project
    *           A List of Task_Group__c records 
    */
    static List<PRA_ProjectWBSCVO.ExportWBSCVO> createWbsTaskGroupLines(WBS_Project__c wbsProj, List<Task_Group__c> wbsTaskGrpList) {
        List<PRA_ProjectWBSCVO.ExportWBSCVO> rtnList = new List<PRA_ProjectWBSCVO.ExportWBSCVO>();

        PRA_ProjectWBSCVO.ExportWBSCVO wbsCVO;
        
        for (Task_Group__c wbsTaskGrp : wbsTaskGrpList) {
            wbsCVO = new PRA_ProjectWBSCVO.ExportWBSCVO();
            
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_RUN_GRP] = wbsProj.Reference_Number__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY] = wbsProj.Six_Character_Contract__c + '-' + wbsTaskGrp.Lawson_Short_Desc__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_SHORT_DESC] = wbsTaskGrp.Lawson_30_Char_Desc__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY_GRP] = wbsProj.Activity_Group__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_COMPANY_NUM] = wbsProj.Company_Number__r.Name;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACCOUNT_UNIT] = wbsProj.Accounting_Unit__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_STATUS_CODE] = wbsProj.Lawson_Status__c; 
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_CTRCT_ADDR] = String.valueOf(wbsProj.Contract_Level_Address__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PROJECT_ADDR] = String.valueOf(wbsProj.Project_Level_Address__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIV_ADDR] = wbsTaskGrp.Activity_Code__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_TASK_ADDR] = '0';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_POSTING_ADDR] = '0';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_START_DATE] = wbsFormatDate(wbsProj.Start_Date__c); 
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_END_DATE] = wbsFormatDate(wbsProj.End_Date__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LONG_DESC] = wbsTaskGrp.Lawson_30_Char_Desc__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LEVEL_TYPE] = 'S';  
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PRJ_CURRENCY] = wbsProj.Currency__c;

            rtnList.add(wbsCVO);            
        }
        
        return rtnList;
    }

    /*
    *   Creates the lines for the task (Level 4) section of a wbs export file
    *   @param  WBS_Project__c record for the project
    *           A List of Client_Task__c records with referenced Task_Group__r data
    */
    static List<PRA_ProjectWBSCVO.ExportWBSCVO> createWbsTaskLines(WBS_Project__c wbsProj, List<Client_Task__c> wbsTaskList) {
        List<PRA_ProjectWBSCVO.ExportWBSCVO> rtnList = new List<PRA_ProjectWBSCVO.ExportWBSCVO>();

        PRA_ProjectWBSCVO.ExportWBSCVO wbsCVO;

        // Create the section header
        if (wbsTaskList.size() > 0) {
            wbsCVO = new PRA_ProjectWBSCVO.ExportWBSCVO();
    
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_RUN_GRP] = wbsProj.Reference_Number__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY] = wbsProj.Six_Character_Contract__c + 'AUP';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_SHORT_DESC] = WBS_EXP_ONLY;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY_GRP] = wbsProj.Activity_Group__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_COMPANY_NUM] = wbsProj.Company_Number__r.Name;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACCOUNT_UNIT] = wbsProj.Accounting_Unit__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_STATUS_CODE] = wbsProj.Lawson_Status__c; 
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_CTRCT_ADDR] = String.valueOf(wbsProj.Contract_Level_Address__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PROJECT_ADDR] = String.valueOf(wbsProj.Project_Level_Address__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIV_ADDR] = WBS_EXP_ACTIV_ADDR;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_TASK_ADDR] = WBS_EXP_TASK_ADDR;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_POSTING_ADDR] = '0';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_START_DATE] = wbsFormatDate(wbsProj.Start_Date__c); 
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_END_DATE] = wbsFormatDate(wbsProj.End_Date__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LONG_DESC] = WBS_EXP_ONLY;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LEVEL_TYPE] = 'S';  
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PRJ_CURRENCY] = wbsProj.Currency__c;

            rtnList.add(wbsCVO);            
        }
        
        // Add Task lines
        for (Client_Task__c wbsTask : wbsTaskList) {
            wbsCVO = new PRA_ProjectWBSCVO.ExportWBSCVO();

            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_RUN_GRP] = wbsProj.Reference_Number__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY] = wbsProj.Six_Character_Contract__c + wbsTask.Combo_Code__c.leftPad(3,'0');
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_SHORT_DESC] = wbsFormatDescription(wbsTask.Client_Unit_Number__c, wbsTask.Description__c, 30);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY_GRP] = wbsProj.Activity_Group__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_COMPANY_NUM] = wbsProj.Company_Number__r.Name;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACCOUNT_UNIT] = wbsProj.Accounting_Unit__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_STATUS_CODE] = wbsProj.Lawson_Status__c; 
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_CTRCT_ADDR] = String.valueOf(wbsProj.Contract_Level_Address__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PROJECT_ADDR] = String.valueOf(wbsProj.Project_Level_Address__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIV_ADDR] = wbsTask.Task_Group__r.Activity_Code__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_TASK_ADDR] = wbsTask.Combo_Code__c.leftPad(3,'0');
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_POSTING_ADDR] = '0';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_START_DATE] = wbsFormatDate(wbsProj.Start_Date__c); 
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_END_DATE] = wbsFormatDate(wbsProj.End_Date__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LONG_DESC] = wbsFormatDescription(wbsTask.Client_Unit_Number__c, wbsTask.Description__c, 60);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LEVEL_TYPE] = 'S';  
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PRJ_CURRENCY] = wbsProj.Currency__c;

            rtnList.add(wbsCVO);            
        }
        return rtnList;
    }
    
    /*
    *   Creates the lines for the posting (Level 5) section of a wbs export file
    *   @param  WBS_Project__c record for the project
    *           A List of Client_Task__c records with referenced Task_Group__r data
    */
    static List<PRA_ProjectWBSCVO.ExportWBSCVO> createWbsPostLines(WBS_Project__c wbsProj, List<Client_Task__c> wbsTaskList) {
        List<PRA_ProjectWBSCVO.ExportWBSCVO> rtnList = new List<PRA_ProjectWBSCVO.ExportWBSCVO>();

        PRA_ProjectWBSCVO.ExportWBSCVO wbsCVO;

        // Create the section header
        if (wbsTaskList.size() > 0) {
            wbsCVO = new PRA_ProjectWBSCVO.ExportWBSCVO();

            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_RUN_GRP] = wbsProj.Reference_Number__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY] = wbsProj.Six_Character_Contract__c + 'AUP' + WBS_EXP_BUFUNC_EXPEN;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_SHORT_DESC] = WBS_EXP_ONLY;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY_GRP] = wbsProj.Activity_Group__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_COMPANY_NUM] = wbsProj.Company_Number__r.Name;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACCOUNT_UNIT] = wbsProj.Accounting_Unit__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_STATUS_CODE] = wbsProj.Lawson_Status__c; 
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_CTRCT_ADDR] = String.valueOf(wbsProj.Contract_Level_Address__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PROJECT_ADDR] = String.valueOf(wbsProj.Project_Level_Address__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIV_ADDR] = WBS_EXP_ACTIV_ADDR;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_TASK_ADDR] = WBS_EXP_TASK_ADDR;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_POSTING_ADDR] = WBS_EXP_POST_ADDR_EXPEN;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_START_DATE] = wbsFormatDate(wbsProj.Start_Date__c); 
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_END_DATE] = wbsFormatDate(wbsProj.End_Date__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LONG_DESC] = WBS_EXP_ONLY;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LEVEL_TYPE] = 'P';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_2] = 'BUS UNIT';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_2] = WBS_EXP_BUFUNC_EXPEN.mid(0,3);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_3] = 'FUNCTION';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_3] = WBS_EXP_BUFUNC_EXPEN.mid(3,2);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_4] = 'BU UNIT FUNC';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_4] = WBS_EXP_BUFUNC_EXPEN;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_5] = 'BUFDIVSION';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_5] = wbsProj.Company_Number__r.Division__c.toUpperCase();
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_6] = 'BUFREGION';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_6] = wbsProj.Company_Number__r.Global_Region__r.Region_Name__c.toUpperCase();
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PRJ_CURRENCY] = wbsProj.Currency__c;

            rtnList.add(wbsCVO);            
        }
        
        // Add Posting lines
        for (Client_Task__c wbsTask : wbsTaskList) {
            wbsCVO = new PRA_ProjectWBSCVO.ExportWBSCVO();

            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_RUN_GRP] = wbsProj.Reference_Number__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY] = wbsProj.Six_Character_Contract__c + wbsTask.Combo_Code__c.leftPad(3,'0') + WBS_EXP_BUFUNC_LBREE;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_SHORT_DESC] = wbsFormatDescription(wbsTask.Client_Unit_Number__c, wbsTask.Description__c, 30);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY_GRP] = wbsProj.Activity_Group__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_COMPANY_NUM] = wbsProj.Company_Number__r.Name;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACCOUNT_UNIT] = wbsProj.Accounting_Unit__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_STATUS_CODE] = wbsProj.Lawson_Status__c; 
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_CTRCT_ADDR] = String.valueOf(wbsProj.Contract_Level_Address__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PROJECT_ADDR] = String.valueOf(wbsProj.Project_Level_Address__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIV_ADDR] = wbsTask.Task_Group__r.Activity_Code__c;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_TASK_ADDR] = wbsTask.Combo_Code__c.leftPad(3,'0');
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_POSTING_ADDR] = WBS_EXP_POST_ADDR_EXPEN;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_START_DATE] = wbsFormatDate(wbsProj.Start_Date__c); 
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_END_DATE] = wbsFormatDate(wbsProj.End_Date__c);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LONG_DESC] = wbsFormatDescription(wbsTask.Client_Unit_Number__c, wbsTask.Description__c, 60);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_LEVEL_TYPE] = 'P';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_2] = 'BUS UNIT';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_2] = WBS_EXP_BUFUNC_LBREE.mid(0,3);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_3] = 'FUNCTION';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_3] = WBS_EXP_BUFUNC_LBREE.mid(3,2);
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_4] = 'BU UNIT FUNC';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_4] = WBS_EXP_BUFUNC_LBREE;
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_5] = 'BUFDIVSION';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_5] = wbsProj.Company_Number__r.Division__c.toUpperCase();
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_6] = 'BUFREGION';
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_6] = wbsProj.Company_Number__r.Global_Region__r.Region_Name__c.toUpperCase();
            wbsCVO.items[PRA_ProjectWBSCVO.WBS_FLD_PRJ_CURRENCY] = wbsProj.Currency__c;

            rtnList.add(wbsCVO);            
        }
        return rtnList;
    }

    /*
    *   Create BD1 file
    *   @param ID of a Client_Project__c record
    */
    public static List<PRA_ProjectWBSCVO.ExportBD1CVO> createBD1File(id projectID) {
        // Init the return list
        List<PRA_ProjectWBSCVO.ExportBD1CVO> listFile = new List<PRA_ProjectWBSCVO.ExportBD1CVO>();

        // Gather all data
        try {
            // WBS Project
            List<WBS_Project__c> wbsProjectList = PRA_DataAccessor.getWbsProjectByProjID(projectID);
            if (! validateWbsProjectList(wbsProjectList)) {
                throw new WBSExportException('WBS Project data is missing.');
            }
            WBS_Project__c wbsProject = wbsProjectList[0];
        
            // Task Data
            List<Client_Task__c> wbsTaskList = PRA_DataAccessor.getWbsTaskData(projectID, PRA_Constants.CU_STATUS_SENT);
            if (wbsTaskList.size() < 1) {
                throw new WBSExportException('No Client Task Data Found');
            }
            
            listFile.addAll(createBd1Lines(wbsProject, wbsTaskList));

        } catch (WBSExportException e) {
            throw new WBSExportException(e.getMessage());
        } catch (Exception e) {
            system.debug('----- Exception in createWBSFile()');
            system.debug('----- ' + e.getMessage());
            system.debug('----- ' + e.getTypeName());
			throw new WBSExportException(e.getMessage(), e);
        }
    
        return listFile;
    } 

    static List<PRA_ProjectWBSCVO.ExportBD1CVO> createBd1Lines(WBS_Project__c wbsProj, List<Client_Task__c> wbsTaskList) {
        List<PRA_ProjectWBSCVO.ExportBD1CVO> rtnList = new List<PRA_ProjectWBSCVO.ExportBD1CVO>();

        PRA_ProjectWBSCVO.ExportBD1CVO bd1CVO;

        Integer seqNum = 1;
        for (Client_Task__c wbsTask : wbsTaskList) {
            bd1CVO = new PRA_ProjectWBSCVO.ExportBD1CVO();

            bd1CVO.items[PRA_ProjectWBSCVO.BD_FLD_RUN_GRP] = wbsProj.Six_Character_Contract__c + 'BD1';
            bd1CVO.items[PRA_ProjectWBSCVO.BD_FLD_SEQ_NUM] = String.valueOf(seqNum);
            bd1CVO.items[PRA_ProjectWBSCVO.BD_FLD_BUF_ACT_LVL] = wbsProj.Six_Character_Contract__c + wbsTask.Combo_Code__c.leftPad(3,'0') + WBS_EXP_BUFUNC_LBREE;
            bd1CVO.items[PRA_ProjectWBSCVO.BD_FLD_PROJ_CURRENCY] = wbsProj.Currency__c;
            
            seqNum++;
            rtnList.add(bd1CVO);
        }
                
        return rtnList;
    }
    
    /*
    *   Create BD9 file
    *   @param ID of a Client_Project__c record
    */
    public static List<PRA_ProjectWBSCVO.ExportBD9CVO> createBD9File(id projectID) {
        // Init the return list
        List<PRA_ProjectWBSCVO.ExportBD9CVO> listFile = new List<PRA_ProjectWBSCVO.ExportBD9CVO>();

        // Gather all data
        try {
            // WBS Project
            List<WBS_Project__c> wbsProjectList = PRA_DataAccessor.getWbsProjectByProjID(projectID);
            if (! validateWbsProjectList(wbsProjectList)) {
                throw new WBSExportException('WBS Project data is missing.');
            }
            WBS_Project__c wbsProject = wbsProjectList[0];
        
            // Task Data
            List<Client_Task__c> wbsTaskList = PRA_DataAccessor.getWbsTaskData(projectID, PRA_Constants.CU_STATUS_SENT);
            if (wbsTaskList.size() < 1) {
                throw new WBSExportException('No Client Task Data Found');
            }
            
            listFile.addAll(createBd9Lines(wbsProject, wbsTaskList));

        } catch (WBSExportException e) {
            throw new WBSExportException(e.getMessage());
        } catch (Exception e) {
            system.debug('----- Exception in createWBSFile()');
            system.debug('----- ' + e.getMessage());
            system.debug('----- ' + e.getTypeName());
			throw new WBSExportException(e.getMessage(), e);
        }
    
        return listFile;
    } 

    static List<PRA_ProjectWBSCVO.ExportBD9CVO> createBd9Lines(WBS_Project__c wbsProj, List<Client_Task__c> wbsTaskList) {
        List<PRA_ProjectWBSCVO.ExportBD9CVO> rtnList = new List<PRA_ProjectWBSCVO.ExportBD9CVO>();

        PRA_ProjectWBSCVO.ExportBD9CVO bd9CVO;

        Integer seqNum = 1;
        for (Client_Task__c wbsTask : wbsTaskList) {
            bd9CVO = new PRA_ProjectWBSCVO.ExportBD9CVO();

            bd9CVO.items[PRA_ProjectWBSCVO.BD_FLD_RUN_GRP] = wbsProj.Six_Character_Contract__c + 'BD9';
            bd9CVO.items[PRA_ProjectWBSCVO.BD_FLD_SEQ_NUM] = String.valueOf(seqNum);
            bd9CVO.items[PRA_ProjectWBSCVO.BD_FLD_BUF_ACT_LVL] = wbsProj.Six_Character_Contract__c + wbsTask.Combo_Code__c.leftPad(3,'0') + WBS_EXP_BUFUNC_LBREE;
            bd9CVO.items[PRA_ProjectWBSCVO.BD_FLD_PROJ_CURRENCY] = wbsProj.Currency__c;
            
            seqNum++;
            rtnList.add(bd9CVO);
        }
                
        return rtnList;
    }

    /*
    *   Creates a string representation of a date in YYYYMMDD format
    *   @param  date
    */
    static String wbsFormatDate(Date sourceDate) {
        String formattedDate;
        formattedDate = String.valueOf(sourceDate);
        formattedDate = formattedDate.remove('-');
        return formattedDate;
    }
    
    /*
    *   Creates the formatted and scrubbed description test for task and posting records
    *   of a WBS export file.
    *   @param  Client Unit Number
    *           Description
    *           Max length of the intended description field
    */
    static String wbsFormatDescription(String clientUnitNumber, String description, Integer maxLength) {
        String rtnString;
        String regExInclusive = '[^A-Za-z0-9 -/()]';
        
        rtnString = clientUnitNumber.replaceAll(regExInclusive,'') + ' ' + description.replaceAll(regExInclusive,'');
        if (rtnString.length() > maxLength) {
            rtnString = rtnString.substring(0, maxLength);
        }
        
        return rtnString; 
    }
    
    /*
    *	Validates the completness of all Client_Task__c records in the list
    *	Thows a WBSExportException if data is invalid
    *	@param	List of Client_Task__c records
    */
    static void validateClientTaskData(List<Client_Task__c> ctList) {
    	List<String> errorList = new List<String>();
    	
    	for (Client_Task__c ct : ctList) {
    		if (String.isBlank(ct.Client_Unit_Number__c)) {
    			errorList.add('Missing Client Unit Number');
    		}
    		if (String.isBlank(ct.Description__c)) {
    			errorList.add(ct.Client_Unit_Number__c + ': Missing Description');
    		}
    		if (String.isBlank(ct.Combo_Code__c)) {
    			errorList.add(ct.Client_Unit_Number__c + ': Missing Combo Code');
    		}
    		if (String.isBlank(ct.Task_Group__r.Lawson_Short_Desc__c)) {
    			errorList.add(ct.Client_Unit_Number__c + ': Missing Task Group Short Description');
    		}
    		if (String.isBlank(ct.Task_Group__r.Lawson_30_Char_Desc__c)) {
    			errorList.add(ct.Client_Unit_Number__c + ': Missing Task Group 30-char Description');
    		}
    		if (String.isBlank(ct.Task_Group__r.Activity_Code__c)) {
    			errorList.add(ct.Client_Unit_Number__c + ': Missing Activity Code');
    		}
    	}
    	if (errorList.size() > 0) {
    		String errorMsg = '';
    		for (String s : errorList) {
    			errorMsg += s + '<br>';
    		}
    		throw new WBSExportException(errorMsg);
    	}
    }
}