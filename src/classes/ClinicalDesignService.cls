public with sharing class ClinicalDesignService {

	/** Retrieve list of clinical designs for project id.
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @param	ProjectId	Project to retrieve designs for.
	 * @return	List of clinical designs
	 */
	public static List<ClinicalDesign__c> getClinicalDesignListForProject(ID ProjectId) {
		return ClinicalDesignDataAccessor.getClinicalDesignList('Project__c = \'' + ProjectId + '\'');
		//return (List<ClinicalDesign__c>) BDT_DataAccessor.getData('ClinicalDesign__c', 'Project__c = \'' + ProjectId + '\'', '', 100, 0);
		
	}

	public static ClinicalDesign__c getClinicalDesignById(ID ClinicalDesignId) {
		return ClinicalDesignDataAccessor.getClinicalDesignList('id = \'' + ClinicalDesignId + '\'')[0];
		//return (ClinicalDesign__c) BDT_DataAccessor.getData('ClinicalDesign__c', 'id = \'' + ClinicalDesignId + '\'', '', 1, 0)[0];
		
	}
	
	public class designDisplay {
		public ClinicalDesign__c	design				{get;set;}
		public list<epoch__c> 		epochList			{get;set;}
		public list<armRowDisplay> 	armRowDisplayList	{get;set;}
		
		public designDisplay(ID ClinicalDesignId) {
			// Query initial data
			design 				 = getClinicalDesignById(ClinicalDesignId);
			epochList 			 = EpochDataAccessor.getEpochList('ClinicalDesign__c = \'' + ClinicalDesignId + '\'');
			
			List<Arm__c> armList = ArmDataAccessor.getArmList('ClinicalDesign__c = \'' + ClinicalDesignId + '\'');
			
			map<id,Flowcharts__c> flowchartMap = new Map<id,Flowcharts__c>();
			flowchartMap.putAll(FlowchartDataAccessor.getFlowchartList('ClinicalDesign__c = \'' + ClinicalDesignId + '\''));
			
			// Build secondary data
			designDisplayConstructorHelper(armList, flowchartMap);
		}
		
		
		public designDisplay(ClinicalDesign__c pDesign,
							List<Epoch__c> pEpochList,
							List<Arm__c> pArmList,
							List<Flowcharts__c> pFlowchartList) {
			//Initial data
			design 	  = pDesign;
			epochList = pEpochList;
			
			map<id,Flowcharts__c> flowchartMap = new Map<id,Flowcharts__c>();
			flowchartMap.putAll(pFlowchartList);
			
			// Build secondary data
			designDisplayConstructorHelper(pArmList, flowchartMap);
		}
		
		
		public void designDisplayConstructorHelper (List<Arm__c> armList, Map<id,Flowcharts__c> flowchartMap) {
			// Define colors
			list<string> colorCodes = new list<string>{'#6699CC','#E7A550','#999999','#7AA3CC','#E7AF5A','#B2B2B2','#8EADCC','#E7B964','#CCCCCC','#A2B7CC','#E7C36E','#E5E5E5','#B6C1CC','#E7CD78', '#F9F9F9'};
			
			// populate armRowDisplay
			armRowDisplayList = new list<armRowDisplay>();
			for (arm__c arm:armList) {
				armRowDisplay arItem 	= new armRowDisplay();
				arItem.arm				= arm;
				arItem.flowchartlist 	= new list<flowchartDisplayItem>();

				List<Flowchartassignment__c> FlowchartAssignmentList = arm.flowchartassignments__r;

				for (epoch__c epoch:epochList) {
					
					Flowcharts__c flowchart;
					
					// check if this epoch has a flowchartassignment
					for (Flowchartassignment__c flowchartassignment:flowchartassignmentList) {
						if (flowchartassignment.epoch__c == epoch.id) {
							if (flowchartMap.containsKey(flowchartassignment.flowchart__c)) {
								flowchart = flowchartMap.get(flowchartassignment.flowchart__c);
							} 
							
							// break out of loop on found
							break;
						}
					}
					
					flowchartDisplayItem fdItem = new flowchartDisplayItem();
					fdItem.flowchart = flowchart;
					
					try {
						fdItem.colorCode = (flowchart != null && flowchart.flowchart_number__c != null) ? 
											colorCodes[math.mod(Integer.valueOf(flowchart.flowchart_number__c)-1, colorCodes.size())] :
											'';
					} catch (Exception e) {
						// if flowchart number was not properly defined then don't set a color code
					}
					
					arItem.flowchartlist.add(fdItem);
				}
				
				armRowDisplayList.add(arItem);
			}
		}

	}
	class armRowDisplay {
		public arm__c 						arm 			{get;set;}
		public list<flowchartDisplayItem> 	flowchartlist	{get;set;}
	}
	class flowchartDisplayItem {
		public flowcharts__c		flowchart			{get;set;}
		public string				colorCode			{get;set;}
	}

}