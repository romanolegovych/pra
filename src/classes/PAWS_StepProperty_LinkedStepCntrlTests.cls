/**
 * @author Konstantin Ermolenko
 * @Date 1/28/2015
 * @Description tests for Linked Step functionality: 
 * - PAWS_StepProperty_LinkedStepController.cls
 * - PAWS_StepProperty_LinkedStepHelper.cls
 * - PAWS_StepProperty_LinkedStepActCntrl.cls
 * - PAWS_FlowStepPropertyTrigger.cls
 * - PAWS_AfterCloneImportHandler.cls
 * - PAWS_FlowStepProperty.trigger
 */
@isTest
private class PAWS_StepProperty_LinkedStepCntrlTests
{
	static testMethod void test_PAWS_StepProperty_LinkedStepController()
	{
		PAWS_ApexTestsEnvironment.init();
		
		STSWR1__Flow_Step_Connection__c flowStepConnection = PAWS_ApexTestsEnvironment.FlowStepConnection;
		PAWS_Project_Flow_Junction__c projectFlow = PAWS_ApexTestsEnvironment.ProjectFlow;
		
		List<STSWR1__Flow_Step_Junction__c> steps = [Select Name, STSWR1__Guid__c, STSWR1__Flow__r.Name From STSWR1__Flow_Step_Junction__c];
		
		PageReference pageRef = Page.PAWS_StepProperty_LinkedStep;
		pageRef.getParameters().put('stepId', steps.get(0).Id);
		Test.setCurrentPageReference(pageRef);
		
		Test.startTest();
		
		PAWS_StepProperty_LinkedStepController controller = new PAWS_StepProperty_LinkedStepController();
		controller.init();
		
		system.assert(controller.Items != null);
		system.assert(controller.Steps != null);
		
		controller.LinkedStepTypeOnChange();
		controller.SelectedItemOnChange();
		controller.SelectedItemId = 'folder-root';
		controller.SelectedItemOnChange();
		
		controller.LinkedStepId = steps.get(1).Id;
		controller.Save();
		
		STSWR1__Flow_Step_Property__c masterProperty = new STSWR1__Flow_Step_Property__c(
			STSWR1__Type__c = 'Linked Step',
			STSWR1__Flow_Step__c = steps.get(0).Id,
			STSWR1__Value__c = new PAWS_StepProperty_LinkedStepController.LinkedStepData(steps.get(1), 'Master').toJson(),
			STSWR1__Reqiured__c = false,
			STSWR1__Read_Only__c = false,
			STSWR1__Hidden__c = false
		);
		
		insert masterProperty;
		
		pageRef.getParameters().put('propertyId', masterProperty.Id);
		controller.init();
		
		STSWR1__Item__c item = [Select Id From STSWR1__Item__c Where STSWR1__Type__c = 'Folder' limit 1];
		controller.FolderId = item.Id;
		
		controller.Items = null;
		system.assert(controller.Items != null);
		
		Test.stopTest();
	}
	
	static testMethod void test_PAWS_StepProperty_LinkedStepHelper_Configuration()
	{
		PAWS_ApexTestsEnvironment.init();
		
		STSWR1__Flow_Step_Connection__c flowStepConnection = PAWS_ApexTestsEnvironment.FlowStepConnection;
		
		List<STSWR1__Flow_Step_Junction__c> steps = [Select Name, STSWR1__Guid__c, STSWR1__Flow__r.Name From STSWR1__Flow_Step_Junction__c];
		
		Test.startTest();
		
		STSWR1__Flow_Step_Property__c masterProperty = new STSWR1__Flow_Step_Property__c(
			STSWR1__Type__c = 'Linked Step',
			STSWR1__Flow_Step__c = steps.get(0).Id,
			STSWR1__Value__c = new PAWS_StepProperty_LinkedStepController.LinkedStepData(steps.get(1), 'Master').toJson(),
			STSWR1__Reqiured__c = false,
			STSWR1__Read_Only__c = false,
			STSWR1__Hidden__c = false
		);
		
		insert masterProperty;
		
		String flowId = steps.get(0).STSWR1__Flow__r.Id;
		new PAWS_AfterCloneImportHandler().Execute(new Map<String, String>{flowId => flowId});
		
		masterProperty.STSWR1__Value__c = new PAWS_StepProperty_LinkedStepController.LinkedStepData(steps.get(1), 'Slave').toJson();
		update masterProperty;
		
		delete masterProperty;
		
		Test.stopTest();
	}
	
	static testMethod void test_PAWS_StepProperty_LinkedStepHelper_Execution()
	{
		PAWS_ApexTestsEnvironment.init();
		
		STSWR1__Flow_Step_Connection__c flowStepConnection = PAWS_ApexTestsEnvironment.FlowStepConnection;
		STSWR1__Flow_Instance_Cursor__c flowInstanceCursor = PAWS_ApexTestsEnvironment.FlowInstanceCursor;
		
		List<STSWR1__Flow_Step_Junction__c> steps = [Select Name, STSWR1__Guid__c, STSWR1__Flow__r.Name From STSWR1__Flow_Step_Junction__c Where Id != :flowInstanceCursor.STSWR1__Step__c];
		
		STSWR1__Flow_Step_Property__c masterProperty = new STSWR1__Flow_Step_Property__c(
			STSWR1__Type__c = 'Linked Step',
			STSWR1__Flow_Step__c = flowInstanceCursor.STSWR1__Step__c,
			STSWR1__Value__c = new PAWS_StepProperty_LinkedStepController.LinkedStepData(steps.get(0), 'Master').toJson(),
			STSWR1__Reqiured__c = false,
			STSWR1__Read_Only__c = false,
			STSWR1__Hidden__c = false
		);
		
		insert masterProperty;
		
		Test.startTest();
		
		STSWR1__Flow_Instance_History__c h = [Select STSWR1__Actual_Start_Date__c, STSWR1__Actual_Complete_Date__c From STSWR1__Flow_Instance_History__c Where STSWR1__Cursor__c = :flowInstanceCursor.Id And STSWR1__Step__c = :flowInstanceCursor.STSWR1__Step__c];
		h.STSWR1__Actual_Start_Date__c = Date.today();
		h.STSWR1__Actual_Complete_Date__c = Date.today();
		update h;
		
		flowInstanceCursor.STSWR1__Step__c = steps.get(0).Id;
		update flowInstanceCursor;
		
		Test.stopTest();
		
		//TEST PAWS_StepProperty_LinkedStepActCntrl
		STSWR1__Flow_Step_Junction__c step = [Select Id From STSWR1__Flow_Step_Junction__c limit 1];
		
		STSWR1.AbstractTrigger.Disabled = true;
		PAWS_Queue__c q = new PAWS_Queue__c(
			Type__c = 'Linked Step Proceed',
			Data__c = JSON.serialize(new Map<String, String>{'sourceStepId' => step.Id})
		);
		insert q;
		
		PAWS_StepProperty_LinkedStepActCntrl controller = new PAWS_StepProperty_LinkedStepActCntrl();
		system.assert(controller.PAWSQueueWrappers != null);
		controller.Refresh();
	}
}