public with sharing class BDT_NewEditServiceCatergoryController {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Administration Services
	public String serviceCategoryID {get;set;}
	public ServiceCategory__c serviceCategory {get;set;}
	
	public BDT_NewEditServiceCatergoryController(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Services');
		serviceCategoryID =  System.CurrentPageReference().getParameters().get('id');
		retrieveSC();
	}
	
	public void retrieveSC () {

		serviceCategory = new ServiceCategory__c();
				
		if(serviceCategoryID!=null && serviceCategoryID!=''){  
			try{
				serviceCategory = [Select s.SystemModstamp, s.OwnerId, s.Name, s.LastModifiedDate, s.LastModifiedById, s.IsDeleted, s.Id, s.CreatedDate, s.CreatedById, s.Code__c 
									 From ServiceCategory__c s 
									where s.id = :serviceCategoryID];
									
				serviceCategory.Code__c = BDT_Utils.removeLeadingZerosFromCat(serviceCategory.Code__c);
									
									
			}catch(QueryException e){
				serviceCategory = new ServiceCategory__c();
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Service Category could not be found. ');
        		ApexPages.addMessage(msg); 
			}
		}else{
			serviceCategory = new ServiceCategory__c();
		}
	
	}
	
	public PageReference save(){
		serviceCategory.Code__c = serviceCategory.Code__c.removeStart('.').removeEnd('.');
		
		
		if (serviceCategory.Code__c.replaceAll('[0-9]*[\\.]*','').length()>0){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Invalid category code has been specified.');
        	ApexPages.addMessage(msg); 
			return null;
		}
		
		String Code = BDT_Utils.addLeadingZerosToCat(serviceCategory.Code__c);
		List<String> codeList = Code.replace('.','x').split('x');
		
		for (Integer i = 0 ; i < codeList.size() ; i++) {
			if (codeList[i].length()>3) {
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'No more than 3 digits are allowed to identify a category.');
        		ApexPages.addMessage(msg); 
				return null;
			}
		} 
		
		// split('.') does not split therefor . is replaced by x first
		if (codeList.size()>3){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'No more than 3 levels are allowed.');
        	ApexPages.addMessage(msg); 
			return null;
		}
		if (codeList.size()>1){
			String parentCode = '';
			
			for (Integer i = 0 ; i < codeList.size() -1 ; i++) {
				parentCode += '.' + codeList[i];
			}
			parentCode = parentCode.removeStart('.');
			try {
				ServiceCategory__c sc = [select id, (Select Id From Services__r Where BDTDeleted__c = false) from ServiceCategory__c where code__c = :parentCode limit 1];
				
				if (sc.Services__r.size()>0) {
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'The parent category has services; you cannot create a sub category on a category that has services.');
        			ApexPages.addMessage(msg); 
					return null;
				}
				
			} catch (Exception e) {
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'No parent category exists with code: ' + BDT_Utils.removeLeadingZerosFromCat(parentCode));
        		ApexPages.addMessage(msg); 
				return null;			
			}
		}
		
		serviceCategory.Code__c = BDT_Utils.addLeadingZerosToCat(serviceCategory.Code__c);
		
		try{
			upsert serviceCategory;
		}
		catch(Exception e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Service Category could not be saved. ');
        	ApexPages.addMessage(msg); 
			return null;
		}
			
		
		PageReference servicesPage = new PageReference(System.Page.BDT_Services.getUrl());
		servicesPage.getParameters().put('ScId', serviceCategory.id);
		servicesPage.setRedirect(true);
		return servicesPage;
	}
	
	public PageReference serviceCategoryDelete(){
		
		serviceCategory.Code__c = BDT_Utils.addLeadingZerosToCat(serviceCategory.Code__c);
		serviceCategory.BDTDeleted__c = true;
		
		update serviceCategory;
		
		return new PageReference(System.Page.BDT_Services.getUrl());
	}
	
	public PageReference cancel(){
		PageReference servicesPage = new PageReference(System.Page.BDT_Services.getUrl());
		if(serviceCategory.id!=null){
			servicesPage.getParameters().put('ScId', serviceCategory.id);
		}
		servicesPage.setRedirect(true);
		return servicesPage;
	} 

}