public with sharing class BDT_PassThroughServicesController {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} //Administration Pass-through
	public List<ServiceCategory__c> ServiceCategoryList{get;set;}
	public List<PassThroughService__c> PassThroughServicesList{get;set;}
	public String ServiceCategoryId{get;set;}
	public String ScId{get;set;}
	public string PassThroughServiceId;
	public string ServiceCategoryName{get;set;}
	
	
	public BDT_PassThroughServicesController(){

		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Pass-through');
		//ScId is only set the moment a user returns from the NewEditPassThrough Service page
		ScId = system.currentPageReference().getParameters().get('ScId');
		loadServiceCategories();
		
		if(ScId!=null){
			loadPassThroughServicesPerCategory( ScId );
			ServiceCategoryId = ScId;
			servicecategory__c sc = [select name from servicecategory__c where id = :ScId limit 1];
			ServiceCategoryName = sc.name;			
		}
	}

	public void loadServiceCategories(){
		ServiceCategoryList=[Select Id,
									Name,
									Code__c,
									(select code__c From ServiceCategories__r where BDTDeleted__c  = false)
		                     from 	ServiceCategory__c
		                     where 	BDTDeleted__c=false
		                     order by code__c];
	}
	
	public List<ServiceCategory__c> getServiceCatListClean(){
  		List<ServiceCategory__c> serviceCategoryRep = new List<ServiceCategory__c>();
  	
  	 	for(ServiceCategory__c sc: ServiceCategoryList){
    		// remove the leading zero's as they are only for sorting purposes
			String tmpStr = BDT_Utils.removeLeadingZerosFromCat(sc.Code__c);
			if( tmpStr!=NULL && tmpStr.contains('.') ) {
				tmpStr = tmpStr.substringBeforeLast('.') + '.:' + tmpStr.substringAfterLast('.');
			} else {
				tmpStr = ':' + tmpStr;
			}
			sc.Code__c = tmpStr;
			serviceCategoryRep.add(sc);
	    }
  		return serviceCategoryRep;
  	}
  
  	public void showPassThroughServicesPerCategory(){
  		ServiceCategoryId = System.currentPageReference().getParameters().get('ServiceCategoryId');
    	ServiceCategoryName = System.currentPageReference().getParameters().get('ServiceCategoryName');

     	loadPassThroughServicesPerCategory( ServiceCategoryId );
  	}
  
  	public void loadPassThroughServicesPerCategory(String serviceCategoryId ){
  	  	try{
  	  		PassThroughServicesList = [select Name, InformationalText__c, Displaysequence__c, ServiceCategory__c
  	    		                       from PassThroughService__c
  	                		           where  ServiceCategory__c = :serviceCategoryId
  	                		           and	BDTDeleted__c=false
  	                		           order by Displaysequence__c];
  	  	}
  	  	catch(QueryException e){
  	  		PassThroughServicesList = new List<PassThroughService__c>();
  	  	}
  	}
  	
  	public PageReference createPassThroughService(){
  		PageReference createPassThroughServicePage = new PageReference(System.Page.BDT_NewEditPassThroughService.getUrl());
   		createPassThroughServicePage.getParameters().put('ServiceCategoryId',ServiceCategoryId);
   	
  		return  createPassThroughServicePage;
	}
  
  	public PageReference editPassThroughService(){
	  	PassThroughServiceId = System.currentPageReference().getParameters().get('PassThroughServiceId');
	  	PageReference editPassThroughServicePage = new PageReference(System.Page.BDT_NewEditPassThroughService.getUrl());
	   	
	   	editPassThroughServicePage.getParameters().put('PassThroughServiceId',PassThroughServiceId);
	   	
  		return  editPassThroughServicePage;
 	 }
}