/**
    The purpose of this class is to encapsulate all interaction with the PBB Billing Rate Card.
    Functions provided are loading the relevant rate card entries into memory for effiecient processing, 
    quick lookup of individual rates, and providing various functions to calculate a weighted average
    rate for a specific entry.
    
    Since this class is designed to operate in procedures that may approach certain governor limits, it 
    is designed to minimize SOQL calls and to minimize the impact on heap space.  
    
    The intended useage is:
    -- create an instance of the service
    -- set various query parameters
    -- load the data
    -- execute query functions
    
*/
public with sharing class PBB_BudgetBillRateService extends PRA_BaseService implements IBudgetBillRateService {
    final String NAME_COUNTRY_OBJ        = 'Country__c';
    final String NAME_JOB_CLASS_DESC_OBJ = 'Job_Class_Desc__c';
    final String NAME_BILL_RATE_CARD_OBJ = 'Bill_Rate_Card_Model__c';
    final String NAME_CURRENCY_OBJ       = 'Currency__c';
    
    @TestVisible private Boolean loaded = false;
    @TestVisible private map<String, Bill_Rate__c> mapBillRates = new map<String, Bill_Rate__c>();
    
    @TestVisible private String  strRateCardModelID;
    @TestVisible private String  strCurrencyID;
    @TestVisible private Set<ID> setCountryIDs;
    @TestVisible private Set<ID> setJobPositionIDs;
    
    public PBB_BudgetBillRateService() {
        super();
    }

    /**
        @Description Internal method to create a map key. Validate the type of the two ID arguments.
    */
    @TestVisible private String createKey(ID aCountry, ID aJob, String aYear) {
        if (aCountry.getSObjectType().getDescribe().getName() != NAME_COUNTRY_OBJ) {
            throw new PRAServiceException('Error in method createKey: argument aCountry does not refer to Country__c object');
        }
        if (aJob.getSObjectType().getDescribe().getName() != NAME_JOB_CLASS_DESC_OBJ) {
            throw new PRAServiceException('Error in method createKey: argument aJob does not refer to Job_Class_Desc__c object');
        }
        
        List<Object> lstObj = new List<Object> {aCountry, aJob, aYear};
        return String.join(lstObj, '');        
    }
    
    /**
        @Description This method sets the basic query parameters required for loading bill rate records into the object.
        
        @Arguments  aRateCard - ID of the Bill_Rate_Card_Model__c parent record
                    aCurrency - ID of the Currency__c record used to filter query by a Currency
    */
    public void setBaseParameters(ID aRateCard, ID aCurrency) {
        if (aRateCard.getSObjectType().getDescribe().getName() != NAME_BILL_RATE_CARD_OBJ) {
            throw new PRAServiceException('Error in method setBaseParameters: argument aRateCard does not refer to Bill_Rate_Card_Model__c object');
        }
        if (aCurrency.getSObjectType().getDescribe().getName() != NAME_CURRENCY_OBJ) {
            throw new PRAServiceException('Error in method setBaseParameters: argument aCurrency does not refer to Currency__c object');
        }

        strRateCardModelID = aRateCard;
        strCurrencyID = aCurrency;
    }
    
    /**
        @Description This method sets the query parameter to restrict the result set to certain countries
        
        @Arguments  aCountries - a set of ids representing the countries to be included in the result set
    */
    public void setCountryParameters(Set<ID> aCountries) {
        for (ID theId : aCountries) {
            if (theId != null &&
                theId.getSObjectType().getDescribe().getName() != NAME_COUNTRY_OBJ) {
                throw new PRAServiceException('Error in method setCountryParameters: argument aCountries contains an ID that does not refer to Country__c object');
            }
        }
        setCountryIDs = aCountries;
    }
    
    /**
        @Description This method sets the query parameter to restrict the result set to certaion job classes
        
        @Arguments  aJobs - a set of ids representing the job class descriptions to be included in the result set
    */
    public void setJobParameters(Set<ID> aJobs) {
        for (ID theId : aJobs) {
            if (theId.getSObjectType().getDescribe().getName() != NAME_JOB_CLASS_DESC_OBJ) {
                throw new PRAServiceException('Error in method setJobParameters: argument aJobs contains an ID that does not refer to Job_Class_Desc__c object');
            }
        }
        setJobPositionIDs = aJobs;
    }
    
    /**
        @Description This method executes a query based on the given parameters and loads the result set
                     into a map.
    */
    public void loadBillRates() {
        List<String> validationErr = new List<String>();
        if (loaded == true) {
            throw new PRAServiceException('Bill Rates already loaded.');
        }
        if (String.isBlank(strRateCardModelID)) {
            validationErr.add('Rate Card Model');
        }
        if (String.isBlank(strCurrencyID)) {
            validationErr.add('Currency');
        }
        if (validationErr.isEmpty() == false) {
            PRAServiceException ex = new PRAServiceException('Load Bill Rates Validation Error');
            ex.setErrorList(validationErr);
            throw ex;
        }
        
        // The following query is not in a data accessor because it is used directly in a for loop.
        // Lists that are returned from for-loop queries do not get stored in the session heap, and
        // thus do not count against the heap space limit.
        // Since this query can return thousands of records and this service will be used in
        // processes that will test the boundries of governor limits, this is a case where 
        // efficiency is preferable over established coding patterns.
        String query =  'SELECT Bill_Rate__c, Country__c, Job_Position__c, Year_Rate_Applies_To__c ';
               query += 'FROM Bill_Rate__c ';
               query += 'WHERE Model_Id__c = :strRateCardModelID ';
               query += 'AND   Currency__c = :strCurrencyID ';
               
        if ((setCountryIDs != null) && (setCountryIDs.isEmpty() == false)) {
            query += 'AND Country__r.id IN :setCountryIDs ';
        }
        
        if ((setJobPositionIDs != null) && (setJobPositionIDs.isEmpty() == false)) {
            query += 'AND Job_Position__r.id IN :setJobPositionIDs ';
        }
        
        for (Bill_Rate__c br : Database.query(query)) {
            String theKey = createKey(br.Country__c, br.Job_Position__c, br.Year_Rate_Applies_To__c);
            mapBillRates.put(theKey, br);
        }
        
        loaded = true;
    }

    /**
        @Description Returns the bill rate for a given counrty, job class, and year.
                     Provides a check to see if data is loaded. 
    */
    public decimal getBillRate(String aCountryId, String aJobClassDescId, String aYear) {
        decimal rtnVal;
        
           if (loaded == false) {
               throw new PRAServiceException('getBillRate called before bill rates have been loaded');
           }
           
           String theKey = createKey(aCountryId, aJobClassDescId, aYear);

           Bill_Rate__c theRate = mapBillRates.get(theKey);
           if (theRate != null) {
               rtnVal = theRate.Bill_Rate__c;
           }
        
        return rtnVal;
    }
    
    /**
        @Description This method calculates the weighted average rate for a service task that expects an
                     even distribution of effort across the time period denoted by aStartDate and aEndDate.
                     The weighting is based on months.
    */
    public decimal getWeightedRateEven(String aCountryId, String aJobClassDescId, Date aStartDate, Date aEndDate) {
        decimal rtnVal;
        
        Integer startYear = aStartDate.year();
        Integer endYear = aEndDate.year();
        Integer totalMonths = 0;
        Integer weightMonths = 0;
        decimal sumRates = 0.00;
        for(Integer i = startYear; i <= endYear; i++) {
            if (i == startYear) {
                weightMonths = (13 - aStartDate.month());
            }
            else if (i == endYear) {
                weightMonths = (aEndDate.month());
            }
            else {
                weightMonths = 12;
            }
            
            decimal yearlyRate = getBillRate(aCountryId, aJobClassDescId, String.valueOf(i));
            if (yearlyRate == null) {
                PRAServiceException ex = new PRAServiceException('Missing Bill Rate Record');
                List<String> errLst = new List<String> {aCountryId, aJobClassDescId, String.valueOf(i)};
                ex.setErrorList(errLst);
                throw ex;
            } 
            
            sumRates += (yearlyRate * weightMonths);
            totalMonths += weightMonths;
        }
        
        if (totalMonths > 0) {
            rtnVal = sumRates / totalMonths;
            rtnVal.setScale(2);
        }
        
        return rtnVal;
    }
    
    public interface IBudgetBillRateService {
        void setBaseParameters(ID aRateCard, ID aCurrency);
        void setCountryParameters(Set<ID> aCountries);
        void setJobParameters(Set<ID> aJobs);
        void loadBillRates();
        decimal getBillRate(String aCountryId, String aJobClassDescId, String aYear);
        decimal getWeightedRateEven(String aCountryId, String aJobClassDescId, Date aStartDate, Date aEndDate);
    }    
 }