public with sharing abstract class PBB_ServiceBase {
    
    public String serviceId;
    public String previousServiceId;
    public Boolean isValid;
    public Boolean flag;
    public String parentServiceId;
    
    @TestVisible
    protected List<SObject> serviceRecords;
    @TestVisible
    protected SObject serviceRecord;
    @TestVisible
    protected SObject serviceForUpdate;
    
    public abstract void preparationCreateOrEditService();
    public abstract void refreshServices();
    protected abstract String getServiceWord();
    
    public PBB_ServiceBase() {
        refreshServices();
        flag = false;
        previousServiceId = null;
    }
    
    public PBB_ServiceBase(String parentServiceId) {
        this.parentServiceId = parentServiceId;
        this.previousServiceId  = parentServiceId;
        refreshServices();
    }

    public virtual void removeService() {
        try {
            Map<Id, SObject> serviceRecordsMap = new Map<Id, SObject>(serviceRecords);
            serviceRecord = serviceRecordsMap.get( getEditServiceId() );
            delete serviceRecord;
            serviceId = null;
            serviceRecord = null;
            refreshServices();
        } catch(Exception e) {
            showErrorMessageOnDelete(e);
        }
    }
     
    public virtual void getDetailService(){
        Map<Id, SObject> serviceRecordsMap = new Map<Id, SObject>(serviceRecords);
        serviceRecord = serviceRecordsMap.get(serviceId);
    }
    
    @TestVisible
    protected virtual Boolean isValidService(){
        isValid = false;
        Boolean validationResult = true;
        if( String.isBlank( (String)serviceForUpdate.get('Name') ) ){
            validationResult = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Service ' + getServiceWord() + ' Name: You must enter a value'));
        }
        return validationResult;
    }

    public virtual void createService(){
        isValid = false;
        if(isValidService()) {
            saveService();
        }
        refreshServices();                    
        getDetailService();
    }
    
    @TestVisible
    protected void saveService() {
        isValid = false;
        try {
        	SObject record = serviceForUpdate != null ? serviceForUpdate : serviceRecord;
            upsert record;
            if(serviceRecord != null && serviceForUpdate !=null && flag == true){
                PBB_Services.cloneServiceModelChildRecords(serviceRecord.id,serviceForUpdate.id);
            	upsert serviceForUpdate;
                flag = false;
            }
            isValid = true;
        } catch(Exception e) {
            isValid = false;
            showErrorMessage(e);
        }
    }

    public void cancelService(){
        refreshServices();
        getDetailService();
    }
    
    public static void showErrorMessageOnDelete(Exception e){
        if ( e instanceof DMLException && e.getMessage().contains('could not be completed because it is associated with the following') ){
            ApexPages.addMessage( new ApexPages.Message( ApexPages.severity.ERROR, 'This object can\'t be deleted until it has child records' ) );
        }else{
            showErrorMessage(e);
        }
    }
    
    public static void showErrorMessage(Exception e){
        System.debug('--- Error: ' + e);
        ApexPages.addMessages(e);
    }
    
    /*protected Id getServiceId() {
        return ApexPages.currentPage().getParameters().get('service' + getServiceWord() + 'Id');
    }*/
	
	@TestVisible
    protected String getEditServiceId() {
        return ApexPages.currentPage().getParameters().get('editService' + getServiceWord() + 'Id');
    }
    
    @TestVisible
    protected String getCloneServiceId() {
        return ApexPages.currentPage().getParameters().get('cloneService' + getServiceWord() + 'Id');
    }

    public class ServiceModelException extends Exception { }
}