public with sharing class PAWS_CallApexRuleWrapper
{
	public class PAWS_CallApexRuleWrapperException extends STSWR1.GenericException {}
	
	public Boolean checkStraightCrossFlowRule(sObject context, STSWR1__Flow_Instance_Cursor__c flowInstanceCursor, Map<String, Object> paramsMap)
	{
		Map<String, String> fieldsMap = new Map<String, String>{
			'ecrf__c:ecrf__c' => 'Id',
			'ecrf__c:paws_project_flow_country__c' => 'PAWS_Project_Id__c',
			'ecrf__c:paws_project_flow_site__c' => 'PAWS_Project_Id__c',
			'ecrf__c:paws_project_flow_document__c' => 'PAWS_Project_Id__c',
			'ecrf__c:paws_project_flow_agreement__c' => 'PAWS_Project_Id__c',
			'ecrf__c:paws_project_flow_submission__c' => 'PAWS_Project_Id__c',
			'paws_project_flow_country__c:paws_project_flow_site__c' => 'PAWS_Project_Flow_Site__r.PAWS_Country__c',
			'paws_project_flow_country__c:paws_project_flow_document__c' => 'PAWS_Project_Flow_Document__r.PAWS_Country__c',
			'paws_project_flow_country__c:paws_project_flow_agreement__c' => 'PAWS_Project_Flow_Agreement__r.PAWS_Country__c',
			'paws_project_flow_country__c:paws_project_flow_submission__c' => 'PAWS_Project_Flow_Submission__r.PAWS_Country__c',
			'paws_project_flow_site__c:paws_project_flow_document__c' => 'PAWS_Project_Flow_Document__r.PAWS_Site__c',
			'paws_project_flow_site__c:paws_project_flow_agreement__c' => 'PAWS_Project_Flow_Agreement__r.PAWS_Site__c',
			'paws_project_flow_site__c:paws_project_flow_submission__c' => 'PAWS_Project_Flow_Submission__r.PAWS_Site__c'
		};
		
		String objectName = ((String)paramsMap.get('object')).toLowerCase();
		String flowName = (String)paramsMap.get('flow');
		String stepName = (String)paramsMap.get('step');
		String statuses = (String)paramsMap.get('statuses');
		String condition = (String)paramsMap.get('condition');
		String contextId = ((String)context.Id).left(15);
		String parentFieldName = fieldsMap.get(String.valueOf(context.getSObjectType()).toLowerCase() + ':' + objectName);
		if(parentFieldName == null) throw new PAWS_CallApexRuleWrapperException('You can not create validation rule between objects: ' + String.valueOf(context.getSObjectType()) + ' and ' + objectName);
		
		List<STSWR1__Flow__c> targetFlows = (parentFieldName == 'PAWS_Project_Id__c' ?
			Database.query('select Name, (select Name from STSWR1__Flow_Steps__r where Name = :stepName) from STSWR1__Flow__c where ' + parentFieldName + ' like \'' + contextId + '%\' and Name like \'' + flowName + '%\' and STSWR1__Object_Type__c = :objectName') :
			Database.query('select Name, (select Name from STSWR1__Flow_Steps__r where Name = :stepName) from STSWR1__Flow__c where ' + parentFieldName + ' = \'' + contextId + '\' and Name like \'' + flowName + '%\' and STSWR1__Object_Type__c = :objectName'));
		
		//if(targetFlows.size() == 0) throw new PAWS_CallApexRuleWrapperException('Can not find flow based on: ProjectID=' + projectId + ', FlowName=' + flowName + ', ObjectType=' + objectName);
	
		Map<ID, String> steps = new Map<ID, String>();
		for(STSWR1__Flow__c targetFlow : targetFlows)
		{
			if(targetFlow.STSWR1__Flow_Steps__r.size() == 0) throw new PAWS_CallApexRuleWrapperException('Can not find step with name \'' + stepName + '\' in flow \'' + targetFlow.Name + '\'');
			steps.put(targetFlow.STSWR1__Flow_Steps__r[0].Id, null);
		}

		Set<String> statusSet = new Set<String>(statuses.replace('Completed', 'Completed;Approved;Rejected').split(';'));
		List<STSWR1__Flow_Instance_History__c> history = [select STSWR1__Step__c, STSWR1__Status__c from STSWR1__Flow_Instance_History__c where STSWR1__Step__c in :steps.keySet() order by STSWR1__Step__c, CreatedDate, Name];
    	for(STSWR1__Flow_Instance_History__c historyItem : history)
    	{
    		steps.put(historyItem.STSWR1__Step__c, historyItem.STSWR1__Status__c);
    	}
    	
    	Decimal numberOfPassed = 0.0;
    	for(String status : steps.values())
    	{
    		if(statuses.contains('Not Started') && status == null) numberOfPassed++;
			else if(statusSet.contains(status)) numberOfPassed++;	
    	}
    	
    	String[] items = condition.split(':');
    	if(items[0] == '=') return numberOfPassed == calculateValue(items[1], steps.size());
    	else if(items[0] == '<') return numberOfPassed < calculateValue(items[1], steps.size());
    	else if(items[0] == '<=') return numberOfPassed <= calculateValue(items[1], steps.size());
    	else if(items[0] == '>') return numberOfPassed > calculateValue(items[1], steps.size());
    	else if(items[0] == '>=') return numberOfPassed >= calculateValue(items[1], steps.size());
    	else if(items[0] == '!=') return numberOfPassed != calculateValue(items[1], steps.size());

		throw new PAWS_CallApexRuleWrapperException('Unsupported operation: ' + items[0]);
	}
	
	public Boolean checkBackCrossFlowRule(sObject context, STSWR1__Flow_Instance_Cursor__c flowInstanceCursor, Map<String, Object> paramsMap)
	{
		String objectName = (String)paramsMap.get('object');
		String flowName = (String)paramsMap.get('flow');
		String stepName = (String)paramsMap.get('step');
		String statuses = (String)paramsMap.get('statuses');
		String namePath = (String)paramsMap.get('namePath');
		String projectId = (context instanceof ecrf__c ? (String)context.Id : (String)context.get('paws_project__c'));
		if(projectId != null) projectId = projectId.left(15);
		
		if(!String.isEmpty(namePath))
		{
			List<sObject> records = PAWS_Utilities.makeQuery(String.valueOf(context.getSObjectType()), 'Id = \'' + context.Id + '\'', new Set<String>{namePath});
			flowName += ' (' + (String)PAWS_Utilities.extractValue(records[0], namePath) + ')';
		}
		
		List<STSWR1__Flow__c> targetFlows = [select Id, (select Id from STSWR1__Flow_Steps__r where Name = :stepName) from STSWR1__Flow__c where PAWS_Project_Id__c like :(projectId + '%') and Name = :flowName and STSWR1__Object_Type__c = :objectName.toLowerCase()];
		if(targetFlows.size() > 1) throw new PAWS_CallApexRuleWrapperException('Found more than one flow based on: ProjectID=' + projectId + ', FlowName=' + flowName + ', ObjectType=' + objectName);

		List<STSWR1__Flow_Instance_History__c> history = new List<STSWR1__Flow_Instance_History__c>();
		if(targetFlows.size() > 0)
		{
			STSWR1__Flow__c targetFlow = targetFlows[0];
			if(targetFlow.STSWR1__Flow_Steps__r.size() > 0)
			{
				STSWR1__Flow_Step_Junction__c targetStep = targetFlow.STSWR1__Flow_Steps__r[0];
				history = [select STSWR1__Status__c from STSWR1__Flow_Instance_History__c where STSWR1__Step__c = :targetStep.Id order by CreatedDate, Name];
			}
		}
		
    	if(statuses.contains('Not Started') && history.size() == 0) return true;
		else if(history.size() == 0) return false;

    	Set<String> statusSet = new Set<String>(statuses.replace('Completed', 'Completed;Approved;Rejected').split(';'));
    	return statusSet.contains(history[history.size() - 1].STSWR1__Status__c);
	}
	
	private Decimal calculateValue(String value, Integer numberOfAll)
	{
		if(value.endsWith('%'))
		{
			return (Decimal.valueOf(numberOfAll) * Decimal.valueOf(value.removeEnd('%'))) / 100.0;
		}else{
			return Decimal.valueOf(value);
		}
	}
}