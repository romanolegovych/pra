public with sharing class PBB_SRMModelController {

    /** 
       * to display the list of SRM models
    */
    public List <SRM_Model__c> SRMModelsList    { get; set; }
    
    /** 
       @description Constructor
       @author Niharika Reddy
       @date 2015   
   */
    public PBB_SRMModelController() {
        SRMModelsList = PBB_DataAccessor.GetAllSrmModels();
    }
    
    /** 
        @description Function to call New Page of required Model
        @author Tkachenko Oleksiy
        @date 2015  
   */
   public PageReference getNewPageReference() {
       Schema.DescribeSObjectResult R;
       R = SRM_Model__c.SObjectType.getDescribe();
       System.debug('/' + R.getKeyPrefix() + '/e');
       return new PageReference('/' + R.getKeyPrefix() + '/e');
   }
    
}