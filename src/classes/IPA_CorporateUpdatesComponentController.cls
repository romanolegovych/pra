public class IPA_CorporateUpdatesComponentController
{
    public List<IPA_Articles__c> lstPolicyUpdates {get; set;}
    
    public IPA_CorporateUpdatesComponentController()
    {
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        lstPolicyUpdates = ipa_bo.returnPolicyUpdates();
    }
}