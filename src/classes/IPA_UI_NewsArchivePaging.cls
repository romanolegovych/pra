public class IPA_UI_NewsArchivePaging
{
    //public List<IPA_Links__c> lstQuickLinks {get; set;}
    //public List<IPA_Articles__c> newsArchive;
    public String Title {get; set;}
    public string titleclass {get; set;}
    public string titleiconclass {get; set;}
    public List<String> lstUniqueMonths {get; set;}
    public Integer nxtMonthIndex {get; set;}
    public List<IPA_Articles__c> lstMonth1 {get; set;}
    public List<IPA_Articles__c> lstMonth2 {get; set;}
    public String Article_Type;
    public IPA_BO_NewsArchive bo_news;
    public Integer currMonth1 {get; set;}
    public Integer currYear1 {get; set;}
    public Integer currYear2 {get; set;}
    public Integer currMonth2 {get; set;}
    public Boolean showM1 {get; set;}
    public Boolean showM2 {get; set;}
    public Date tmpD1 {get; set;}
    public Date tmpD2 {get; set;}
    public string Type {get; set;}
    
    public Boolean showNext {get; set;}
    public Boolean showPrev {get; set;}
    
    //Constructor
    public IPA_UI_NewsArchivePaging()
    {
        bo_news  = new IPA_BO_NewsArchive();
        //lstQuickLinks = bo_news.returnQuickLinks();
        lstMonth1 = new List<IPA_Articles__c>();
        lstMonth2 = new List<IPA_Articles__c>();

        Type = '';
        String page = '';
        String action = '';
        
        try
        {
            //Local initialization
            Type = ApexPages.currentPage().getParameters().get('Type');
            page = ApexPages.currentPage().getParameters().get('P');
            action = ApexPages.currentPage().getParameters().get('A');
        }
        catch(Exception e) 
        {
            Type = '';
            page = '0';
            action = '';
        }
        
        if(page != '')
            nxtMonthIndex = Integer.valueOf(page);
        else
            nxtMonthIndex = 0;
            
        if(Type == 'Policy')
        {
            //newsArchive = bo_news.returnPolicyUpdateArchive(Type);
            Title = 'Corporate Updates Archive'; 
            titleclass = 'widget-title-gradient-policy';
            titleiconclass = 'widget-title-gradient-policy-icon';
            
            Article_Type = 'Corporate Update';
            lstUniqueMonths = bo_news.returnUniqueMonthsForArchive('Corporate Update');
        }
        else
        {
            Type = 'News';
            
            //newsArchive = bo_news.returnPRAArchive(Type);
            Title = 'PRA News Archive'; 
            titleclass = 'widget-title-gradient';
            titleiconclass = 'widget-title-gradient-icon';
            
            Article_Type = 'PRA News';
            lstUniqueMonths = bo_news.returnUniqueMonthsForArchive('PRA News');
        }
        
        //Set 1st & 2nd Month when page loads 1st time
        if(action == 'Next')
            moveNext();
        else if(action == 'Prev')
            movePrev();
        else
            loadNextMonths();
            
        //Next & Prev Buttons
        if(nxtMonthIndex == 2) {showNext = true; showPrev = false; }
        if(nxtMonthIndex >= lstUniqueMonths.size() ) {showNext = false; showPrev = true; }
        if(nxtMonthIndex > 2 && nxtMonthIndex < lstUniqueMonths.size()) {showNext = true; showPrev = true; }
        if(3 > lstUniqueMonths.size()) {showNext = false; showPrev = false; }
    }
    
    private void loadNextMonths()
    {   
        currMonth1 = 0;
        currMonth2 = 0;
        showM1 = false;
        showM2 = false;
        
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'' + nxtMonthIndex ));
                    
        if(lstUniqueMonths.size() > nxtMonthIndex)
        {   
            currMonth1 = Integer.valueOf(lstUniqueMonths[nxtMonthIndex].split('-')[0]);
            currYear1 = Integer.valueOf(lstUniqueMonths[nxtMonthIndex].split('-')[1]);
            lstMonth1 = bo_news.returnArchiveForMonth(currMonth1, currYear1, Article_Type);
            tmpD1 = Date.newInstance(currYear1, currMonth1, 20); 
            showM1 = true; 
        }
        else
            lstMonth1 = new List<IPA_Articles__c>();
            
        nxtMonthIndex = nxtMonthIndex + 1;
        
        if(lstUniqueMonths.size() > nxtMonthIndex)
        {
            currMonth2 = Integer.valueOf(lstUniqueMonths[nxtMonthIndex].split('-')[0]);
            currYear2 = Integer.valueOf(lstUniqueMonths[nxtMonthIndex].split('-')[1]);
            lstMonth2 = bo_news.returnArchiveForMonth(currMonth2, currYear2, Article_Type);
            tmpD2 = Date.newInstance(currYear2, currMonth2, 20);  
            showM2 = true;
        }
        else
            lstMonth2 = new List<IPA_Articles__c>();
            
        nxtMonthIndex = nxtMonthIndex + 1;
    }
    
    public PageReference moveNext()
    {       
        loadNextMonths();
        return null;
    }
    
    public PageReference movePrev()
    {  
        nxtMonthIndex = nxtMonthIndex - 4; //go back 2 months
        loadNextMonths();
        return null;
    }
}