@isTest
public with sharing class BDT_SecuritiesDataAccessorTest {

	static testMethod void getAllSecuritiesTest() {
	
		list<BDT_Securities__c> securityList;
	
		// create a couple of securities
		securityList = new list<BDT_Securities__c>();
		securityList.add(new BDT_Securities__c(Name='s2'));
		securityList.add(new BDT_Securities__c(Name='s1'));
		insert securityList;
		
		// check order of returned data
		securityList = BDT_SecuritiesDataAccessor.getAllSecurities();
		system.assertEquals('s1',securityList[0].Name);
		system.assertEquals('s2',securityList[1].Name);
		
	}

}