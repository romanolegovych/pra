@isTest
private class ClinicalDesignServiceTest {

    static testMethod void test_getClinicalDesignListForProject() {
    	
    	// create test data
        Client_Project__c cp = new Client_Project__c(name = 'test');
        insert cp;
        
        // create test data
        list<ClinicalDesign__c> cd = new list<ClinicalDesign__c>();
        cd.add(new ClinicalDesign__c(name='1',project__c = cp.id));
        cd.add(new ClinicalDesign__c(name='2',project__c = cp.id));
        cd.add(new ClinicalDesign__c(name='3',project__c = cp.id));
        insert cd;
        
        // test retrieve logic
        cd = ClinicalDesignService.getClinicalDesignListForProject(cp.id);
        system.assertEquals(3,cd.size());
        
    }
	
	
	/** Test the function getClinicalDesignById.
	 * @author	Dimitrios Sgourdos
	 * @version	14-Feb-2014
	 */
	static testMethod void getClinicalDesignByIdTest() {
		// Create data
		Client_Project__c cp = new Client_Project__c(name = 'test');
		insert cp;
		
		ClinicalDesign__c cd = new ClinicalDesign__c(name='1',project__c = cp.id);
		insert cd;
		
		// Check the function
		ClinicalDesign__c result = ClinicalDesignService.getClinicalDesignById(cd.Id);
		system.assertEquals(cd.Id, result.Id, 'Error in retrieving Clinical Design from the system');
	}
	
	
	/** Test the class designDisplay.
	 * @author	Dimitrios Sgourdos
	 * @version	14-Feb-2014
	 */
	static testMethod void designDisplayTest() {
		// Create data
		Client_Project__c cp = new Client_Project__c(name = 'test');
		insert cp;
		
		ClinicalDesign__c cd = new ClinicalDesign__c(name='1',project__c = cp.id);
		insert cd;
		
		Epoch__c ep = new Epoch__c(ClinicalDesign__c = cd.Id, Description__c = 'Epoch 1', Epoch_Number__c = 1);
		insert ep;
		
		Arm__c a = new Arm__c(ClinicalDesign__c = cd.Id, Arm_Number__c = 1, Description__c='Epoch 1');
		insert a;
		
		String errorMessage = 'Error in constructing the design display';
		
		// Check the first constructor
		ClinicalDesignService.designDisplay result = new ClinicalDesignService.designDisplay(cd.Id);
		
		system.assertEquals(result.design.Id, cd.Id, errorMessage);
		system.assertEquals(1, result.epochList.size(), errorMessage);
		system.assertEquals(ep.Id, result.epochList[0].Id, errorMessage);
		system.assertEquals(1, result.armRowDisplayList.size(), errorMessage);
		system.assertEquals(a.Id, result.armRowDisplayList[0].arm.Id, errorMessage);
		
		// Check the second constructor
		result = new ClinicalDesignService.designDisplay(cd,
														new List<Epoch__c> {ep},
														new List<Arm__c>{a},
														new List<Flowcharts__c>());
		
		system.assertEquals(result.design.Id, cd.Id, errorMessage);
		system.assertEquals(1, result.epochList.size(), errorMessage);
		system.assertEquals(ep.Id, result.epochList[0].Id, errorMessage);
		system.assertEquals(1, result.armRowDisplayList.size(), errorMessage);
		system.assertEquals(a.Id, result.armRowDisplayList[0].arm.Id, errorMessage);
	}
}