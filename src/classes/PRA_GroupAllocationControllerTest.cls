@isTest (seeAllData=false)
private class PRA_GroupAllocationControllerTest {

    static testMethod void shouldCreateNewGroup() {
        // given 
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        Region__c r1=new Region__c();
        r1.Region_Name__c='Asia/Pacific';
        r1.Region_Id__c=111;
        insert r1;
        Region__c r2=new Region__c();
        r2.Region_Name__c='Europe/Africa';
        r2.Region_Id__c=222;
        insert r2;
        
        // when
        test.startTest();
            List<client_task__c> ctlist=[select id,client_unit_number__c from client_task__c where project__r.name=:tu.clientProject.name];
            ctlist[0].client_unit_number__c='abc';
            ctlist[1].client_unit_number__c='bcd';
            update ctlist;
            
            PRA_GroupAllocationController GaController = new PRA_GroupAllocationController();
            GaController.projectText=(tu.clientProject.name);            
            GaController.MainSearch();
            GaController.getRegionnames();            
            GaController.NewGroup();
            GaController.GroupName='g1';
            List<String> liSelectedTasks = new List<String>(); 
            GaController.selectedClienttasks=Json.serialize(liSelectedTasks);           
            GaController.SaveandNewGroup();
            GaController.NeworEditGrouplist[0].selected=true;
            GaController.NeworEditGrouplist[1].selected=true;           
            GaController.SaveandNewGroup();            
            liSelectedTasks = new List<String>{ string.valueof(GaController.NeworEditGrouplist[0].clientUnitNumber+'___'+r1.id) , GaController.NeworEditGrouplist[1].clientUnitNumber+'___'+r2.id }; 
            GaController.selectedClienttasks=Json.serialize(liSelectedTasks);
            GaController.SaveandNewGroup();
            GaController.GroupName='g1';
            GaController.EditGroup();            
            liSelectedTasks = new List<String>{GaController.NeworEditGrouplist[1].clientUnitNumber+'___'+r2.id }; 
            GaController.selectedClienttasks=Json.serialize(liSelectedTasks); 
            GaController.SaveandNewGroup();            
            liSelectedTasks = new List<String>{ string.valueof(GaController.NeworEditGrouplist[0].clientUnitNumber+'___'+r1.id) , GaController.NeworEditGrouplist[1].clientUnitNumber+'___'+r1.id }; 
            GaController.selectedClienttasks=Json.serialize(liSelectedTasks); 
            GaController.SaveandNewGroup();           
            liSelectedTasks = new List<String>{ string.valueof(GaController.NeworEditGrouplist[0].clientUnitNumber+'___'+r1.id) , GaController.NeworEditGrouplist[1].clientUnitNumber+'___'+r2.id }; 
            GaController.selectedClienttasks=Json.serialize(liSelectedTasks);             
            GaController.SaveandNewGroup();
            GaController.RecalculateGroupTasks();
            GaController.SelectedDeletedGroup='g1';
            GaController.DeleteGroup();
            PRA_GroupAllocationController GaController1 = new PRA_GroupAllocationController();            
            GaController1.Reset(); 
            GaController1.exporttoExcel();
        test.stopTest();        
    }        
}