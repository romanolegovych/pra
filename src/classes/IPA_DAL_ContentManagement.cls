public class IPA_DAL_ContentManagement
{ 
    //This function returns the list of Corporate Updates links to shows on Dashboard
    public  List<IPA_Articles__c> returnPolicyUpdates()
    {
        List<IPA_Articles__c> ipa_events = null;
        ipa_events = [select Title__c, Display_Date__c, Content__c, Embedded_Video__c, Embedded_Video_Position__c from IPA_Articles__c
            where RecordType.Name = 'Corporate Update' AND Display_area__c = 'Corporate Updates' AND Published__c = true AND Display_Date__c <= Today
            order by Weight__c, Display_date__c DESC, Title__c  Limit 5];
        return ipa_events;
    }
    
    //This function returns the list of Events filtered by their Category
    public  List<IPA_Articles__c> returnTop3EventsByCategory(Id catId) 
    {
        List<IPA_Articles__c> ipa_events;
        ipa_events = [SELECT Id, Title__c, RecordType.Name, Display_Date__c
                          FROM IPA_Articles__c
                          WHERE RecordType.Name='Event' AND Display_Date__c >= today AND Published__c = true AND Category__c =: catId  
                          ORDER BY Display_date__c, Title__c
                          LIMIT 3];
        return ipa_events;
    }
    
    //This function returns the list of Events filtered by their related Topics
    public  List<IPA_Articles__c> returnTop3EventsByTopic(Set<Id> topicIds) 
    {
        List<IPA_Articles__c> ipa_events;
        ipa_events = [SELECT Id, Title__c, RecordType.Name, Display_Date__c
                          FROM IPA_Articles__c
                          WHERE RecordType.Name='Event' AND Display_Date__c >= today AND Published__c = true AND Id in (
                              SELECT Content__c 
                              FROM IPA_Content_Topic__c 
                              WHERE Topic__c = :topicIds) 
                          ORDER BY Display_date__c, Title__c
                          LIMIT 3];    
        return ipa_events;
    }
    
    //This function returns the list of Quick Links specifically identified by their ids
    public  List<IPA_Articles__c> returnTop3EventsByContent(Set<Id> contentIds) 
    {
        List<IPA_Articles__c> ipa_events;
        ipa_events = [SELECT Id, Title__c, RecordType.Name, Display_Date__c
                          FROM IPA_Articles__c
                          WHERE Id = : ContentIds 
                          ORDER BY Display_date__c, Title__c
                          LIMIT 3];
        return ipa_events;
    }
  
    //This function is used to return list of Holidays to be shown on Dashboard homepage.
    public  List<IPA_Articles__c> returnTop3Holidays() 
    {
        list<IPA_Articles__c> ipa_events = null; 
        ipa_events = [select Id, Title__c, RecordType.Name, Display_Date__c from IPA_Articles__c
                            where RecordType.Name = 'Holiday' AND Display_Date__c >= today AND Published__c = true order by Display_date__c, Title__c  Limit 3];
        return ipa_events;
    }
    
    //This function is used to return latest featured PRA News Article to be shown on Dashboard homepage
    public  IPA_Articles__c returnFeaturedPRANews() 
    {
        IPA_Articles__c ipa_event = null;
        try
        {
            ipa_event = [select Id, Title__c, Display_Date__c, Teaser__c, Featured_Image__c from IPA_Articles__c
                            where Published__c = true AND
                            RecordType.Name = 'PRA News' AND
                            Display_Area__c = 'PRA News – Featured Article' AND
                            Display_Date__c <= today
                            Order By Weight__c, Display_date__c DESC, Title__c Limit 1];
        } catch(Exception ex) {}
        if(ipa_event == null)
        {
            ipa_event = new IPA_Articles__c();
            ipa_event.Display_Date__c = Date.today();
        }                       
             
        //tmp
        if(ipa_event.Featured_Image__c == '' || ipa_event.Featured_Image__c == null)
            ipa_event.Featured_Image__c = '/resource/1386872813000/IPA_InsidePRA_Logo'; //'http://placekitten.com/470/306';
        return ipa_event;
    }
    
    //This function is used to return latest PRA News Article to be shown on Dashboard homepage
    public  List<IPA_Articles__c> returnPRANews() 
    {
        List<IPA_Articles__c> ipa_event = null;
        ipa_event = [select Id, Title__c, Display_Date__c from IPA_Articles__c
                            where Published__c = true AND
                            RecordType.Name = 'PRA News' AND
                            Display_Area__c = 'PRA News' AND
                            Display_Date__c <= today
                            Order By Weight__c, Display_date__c DESC, Title__c Limit 6];
        return ipa_event;
    }
    
     //This function is used to return latest Integration Update Article to be shown on Dashboard homepage
    public  IPA_Articles__c returnIntegrationUpdate() 
    {
        IPA_Articles__c ipa_event = null;
        try
        {
            ipa_event = [select Id, Title__c, Display_Date__c, Teaser__c, Featured_Image__c from IPA_Articles__c
                            where Published__c = true AND
                            RecordType.Name = 'Corporate Update' AND
                            Display_Area__c LIKE '%Integration%' AND
                            Display_Date__c <= today
                            Order By Weight__c, Display_date__c DESC, Title__c Limit 1];
        } catch(Exception ex) {}
        if(ipa_event == null)
        {
            ipa_event = new IPA_Articles__c();
            ipa_event.Display_Date__c = Date.today();
        }   
        
        //tmp
        if(ipa_event.Featured_Image__c == '' || ipa_event.Featured_Image__c == null)
            ipa_event.Featured_Image__c = '/resource/1386872813000/IPA_InsidePRA_Logo'; //'http://placekitten.com/470/306';
        return ipa_event;
    }
    
    //This function is used to return latest Industry Watch Article to be shown on Dashboard homepage
    public List<IPA_Articles__c> returnIndustryWatch() 
    {
        List<IPA_Articles__c> ipa_event = null;        
        try
        {
            ipa_event = [select Id, Title__c, Display_Date__c, Teaser__c, URL__c, Featured_Image__c from IPA_Articles__c
                            where Published__c = true AND
                            RecordType.Name = 'Industry Watch' AND
                            Display_Date__c <= today
                            Order By Weight__c, Display_date__c DESC, Title__c Limit 5];
        } catch(Exception ex) {}
        if(ipa_event.size() == 0)
        {
            ipa_event = new  List<IPA_Articles__c>();   
            IPA_Articles__c obj = new IPA_Articles__c();         
            obj.Display_Date__c = Date.today();          
            if(obj.Featured_Image__c == '' || obj.Featured_Image__c == null)
            obj.Featured_Image__c = '/resource/1386872813000/IPA_InsidePRA_Logo'; //'http://placekitten.com/470/306';
            ipa_event.add(obj);
        }           
        
        return ipa_event;
    }
       
    //This function is used to return latest Employee PRA News Articles to be shown on News Slider on dashboard
    public  List<IPA_Articles__c> returnEmpNewsForSlider() 
    {
        List<IPA_Articles__c> ipa_event = null;
        ipa_event = [select Id, Title__c, Display_Date__c, Featured_Image__c from IPA_Articles__c
                            where Published__c = true AND
                            RecordType.Name = 'PRA News' AND
                            Display_Area__c = 'Employee News' AND
                            Display_Date__c <= today 
                            Order By Weight__c, Display_date__c DESC, Title__c  Limit 3];
                            
        //tmp
        for(IPA_Articles__c c: ipa_event)
        {
            if(c.Featured_Image__c == '' || c.Featured_Image__c == null)
                c.Featured_Image__c = '/resource/1386872813000/IPA_InsidePRA_Logo'; //'http://placekitten.com/470/306';
        }
        return ipa_event;
    }
    
    //This function is used to return entire Article Details
    public IPA_Articles__c returnArticleDetails(String Id) 
    {
        IPA_Articles__c ipa_event = null;
        ipa_event = [select Id, Category__c, RecordType.Name, Display_Area__c, Display_Date__c, Teaser__c, Title__c, Content__c, Embedded_Video__c, Embedded_Video_Position__c,
                   image_1__c, image_2__c, image_3__c, image_4__c, image_5__c from IPA_Articles__c where id =: Id];                   
        
        return ipa_event;
    }   
    
    //This function is used to return list of Events & Holidays for given date
    public  List<IPA_Articles__c> returnEventsHolidays(date sdate, date ldate)
    {
        List<IPA_Articles__c> ipa_events = null;
        ipa_events = [select Id, Title__c, RecordType.Name, Display_date__c from IPA_Articles__c 
            where (RecordType.Name = 'Event' OR RecordType.Name = 'Holiday') AND Display_date__c >= :sdate 
            AND Display_date__c <= :ldate AND Published__c = true order by Display_Date__c, Weight__c, Title__c];
        return ipa_events;
    }
    
    //This function returns the list of Department Contacts filtered by their Category
    public List<IPA_Articles__c> returnDepContactsByCategory(Id catId) 
    {
        List<IPA_Articles__c> contacts;
        contacts = [SELECT Id, Contact_Name__c, Title__c, Phone__c, Email__c, Speciality__c, Office_Location__c,
                         Featured_Image__c, Content__c, Embedded_Video__c, Embedded_Video_Position__c FROM IPA_Articles__c
                          WHERE RecordType.Name = 'Department Contact' AND Category__c =: catId And Published__c = true
                          ORDER BY Weight__c, Contact_Name__c ];
        return contacts;
    }
    
    //This function returns the list of Department Contacts filtered by their related Topics
    public List<IPA_Articles__c> returnDepContactsByTopic(Set<Id> topicIds) 
    {
        List<IPA_Articles__c> contacts;
        contacts = [SELECT IPA_Articles__c.Id, Contact_Name__c, Title__c, Phone__c, Email__c, Speciality__c, Office_Location__c,
                         Featured_Image__c, Content__c, Embedded_Video__c, Embedded_Video_Position__c FROM IPA_Articles__c
                          WHERE RecordType.Name = 'Department Contact' And Published__c = true AND Id in (
                              SELECT Content__c
                              FROM IPA_Content_Topic__c 
                              WHERE Topic__c = :topicIds) 
                          ORDER BY Weight__c, Contact_Name__c];
        return contacts;
    }
    
     //This function returns the list of Department Contacts filtered by their Category
    public List<IPA_Articles__c> returnContactsByContent(Set<Id> contentId) 
    { 
        List<IPA_Articles__c> contacts;
        contacts = [SELECT Id, Contact_Name__c, Title__c, Phone__c, Email__c, Speciality__c, Office_Location__c,
                         Featured_Image__c, Content__c, Embedded_Video__c, Embedded_Video_Position__c FROM IPA_Articles__c
                          WHERE Id =: contentId AND Published__c = true
                          ORDER BY Weight__c, Contact_Name__c ];
        return contacts;
    }
    
    /* Nishant`s Code
    //This function returns a single Content Item related to a Page Widiget instance
    public IPA_Articles__c returnHeroGraphicContent(IPA_Page_Widget__c pageWidgetObj)
    {
        IPA_Articles__c i;
        
        try
        {   
            i = [select Id, Title__c, Teaser__c, Featured_Image__c from IPA_Articles__c a
                 where Published__c = true and a.Id in
                     (
                        select Content__c from IPA_Page_Widget_Content__c c 
                        where c.Page_Widget__c =: pageWidgetObj.Id
                     )
                 Order by a.Weight__c LIMIT 1];
        }catch(Exception e) {}
        
        return i;
    }
    */
    //This function returns a single Content Item related to a Page Widiget instance
    public IPA_Articles__c returnHeroGraphicContent(set<Id> ContentIds)
    {
        IPA_Articles__c hero = new IPA_Articles__c();        
        try
        {   
            hero = [SELECT Id, Title__c, Teaser__c, Featured_Image__c from IPA_Articles__c
                 WHERE Id =: ContentIds AND Published__c = true
                 ORDER by Weight__c LIMIT 1];
        }catch(Exception e) {}
        
        return hero;
    }
    
    //This function returns the list of Department Contacts filtered by their related Topics
    public IPA_Articles__c returnHeroGraphicContentbyTopic(Set<Id> topicIds) 
    {
        IPA_Articles__c hero = new IPA_Articles__c();
        try 
        {
            hero = [select Id, Title__c, Teaser__c, Featured_Image__c from IPA_Articles__c
                          WHERE RecordType.Name='Hero Graphic' AND Published__c = true AND Id in (
                              SELECT Content__c
                              FROM IPA_Content_Topic__c 
                              WHERE Topic__c = :topicIds) 
                          ORDER BY Weight__c limit 1];
        }catch(Exception e){}
        return hero;
    }
    
    //This function returns a single Content Item related to Category instance
    public IPA_Articles__c returnHeroGraphicContentbyCategory(Id catId)
    {
        IPA_Articles__c hero = new IPA_Articles__c();
        
        try
        {   
            hero = [select Id, Title__c, Teaser__c, Featured_Image__c from IPA_Articles__c a
                 where RecordType.Name = 'Hero Graphic' 
                 AND Category__c =: catId AND Published__c = true
                 Order by Weight__c LIMIT 1];
        }catch(Exception e) {}
        
        return hero;
    }
    
    //This function returns the list of Department Contents filtered by their Category
    public List<IPA_Articles__c> returnDepContentsByCategory(Id catId) 
    {
        List<IPA_Articles__c> contents;
        contents = [SELECT Id, Title__c, Teaser__c, Content__c, Embedded_Video__c, Embedded_Video_Position__c, Featured_Image__c, Link__c, Page__c, URL__c FROM IPA_Articles__c
                          WHERE RecordType.Name = 'Department Content' 
                          AND Category__c =: catId And Published__c = true
                          ORDER BY Weight__c, Display_Date__c desc, Title__c];
        return contents;
    }
    
    //This function returns the list of Department Contents filtered by their related Topics
    public List<IPA_Articles__c> returnDepContentsByTopic(Set<Id> topicIds) 
    {
        List<IPA_Articles__c> contents;
        contents = [SELECT Id, Title__c, Teaser__c, Content__c, Embedded_Video__c, Embedded_Video_Position__c, Featured_Image__c, Link__c, Page__c, URL__c FROM IPA_Articles__c
                          WHERE RecordType.Name = 'Department Content' And Published__c = true AND Id in (
                              SELECT Content__c
                              FROM IPA_Content_Topic__c 
                              WHERE Topic__c = :topicIds) 
                          ORDER BY Weight__c, Display_Date__c desc, Title__c];
        return contents;
    }
    
    //This function returns the list of Department Contents filtered by their Category
    public List<IPA_Articles__c> returnDepContentsByPageWidget(Set<Id> contentIds) 
    {
        List<IPA_Articles__c> contents;
        contents = [SELECT Id, Title__c, Teaser__c, Content__c, Embedded_Video__c, Embedded_Video_Position__c, Featured_Image__c, Link__c, Page__c, URL__c FROM IPA_Articles__c
                          WHERE RecordType.Name = 'Department Content' And Published__c = true
                          AND Id =: contentIds
                          ORDER BY Weight__c, Display_Date__c desc, Title__c];
        return contents;
    }
    
    //This function returns the list of Department News filtered by their Category
    public List<IPA_Articles__c> returnDepNewsByCategory(Id catId) 
    {
        List<IPA_Articles__c> contents;
        contents = [SELECT Id, Title__c, Teaser__c, Featured_Image__c, Display_Date__c  FROM IPA_Articles__c
                          WHERE RecordType.Name = 'Department News' 
                          AND Category__c =: catId And Published__c = true And Display_Date__c <= today
                          And Display_Date__c <> null
                          ORDER BY Weight__c, Display_Date__c desc, Name 
                          LIMIT 5];
        return contents;
    }
    
    //This function returns the list of Department News filtered by their related Topics
    public List<IPA_Articles__c> returnDepNewsByTopic(Set<Id> topicIds) 
    {
        List<IPA_Articles__c> contents;
        contents = [SELECT Id, Title__c, Teaser__c, Featured_Image__c, Display_Date__c  FROM IPA_Articles__c
                          WHERE RecordType.Name = 'Department News' And Published__c = true 
                          And Display_Date__c <= today And Display_Date__c <> null
                          AND Id in (
                              SELECT Content__c
                              FROM IPA_Content_Topic__c 
                              WHERE Topic__c = :topicIds) 
                          ORDER BY Weight__c, Display_Date__c desc, Name 
                          LIMIT 5];
        return contents;
    }
    
    //This function returns the list of Department News filtered by their Category
    public List<IPA_Articles__c> returnDepNewsByPageWidget(Set<Id> contentIds) 
    {                   
        List<IPA_Articles__c> contents;
        contents = [SELECT Id, Title__c, Teaser__c, Featured_Image__c, Display_Date__c  FROM IPA_Articles__c
                          WHERE RecordType.Name = 'Department News' And Published__c = true
                          And Display_Date__c <= today And Display_Date__c <> null AND Id =: contentIds 
                          ORDER BY Weight__c, Display_Date__c desc, Name 
                          LIMIT 5];
        return contents;
    }
}