public virtual with sharing class PRA_BaseService {
    public class PRAServiceException extends Exception{
       private List<String> ErrorList = new List<String>();
       
       public void setErrorList(List<String> val) {
           ErrorList = val;
       } 
       
       public List<String> getErrorList() {
           return ErrorList;
       }
    }

    public PRA_BaseService() {
    }
}