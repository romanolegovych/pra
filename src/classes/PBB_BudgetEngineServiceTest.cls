/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PBB_BudgetEngineServiceTest {
    static testMethod void testCreateBudget() {
        PBB_TestUtilsIntegrated tu = new PBB_TestUtilsIntegrated();
        
        tu.createCurrency();
        tu.createCountries();
        tu.createJobs(3);
        tu.createServiceModel();
        tu.createServiceAreas(tu.objServiceModel, 3);
        tu.createServiceFunctions(tu.lstServiceArea, 3);
        tu.createServiceTasks(tu.lstServiceFunction, 2);
        tu.pbbTestUtils.approveServicemodel(tu.objServiceModel);
        tu.createPbbScenario(null);
        tu.createBillRateCardModel();
        System.debug('---- ' + tu.objPbbScenario);
        System.debug('---- ' + tu.lstCountry);
        tu.createScenarioCountries(tu.objPbbScenario, 
                                   new list<Country__c> {tu.lstCountry[tu.COUNTRY_US], tu.lstCountry[tu.COUNTRY_UK] },
                                   true);
        
        list<list<sObject>> countriesAndTasks = new list<list<sObject>>();
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[0]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[1]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[2], tu.lstServiceTask[4]});
        tu.createCountriesServiceTasks(countriesAndTasks);
        
        // Add Bill Rate Card to PBB Scenario
        tu.objPbbScenario.Bill_Rate_Card_Model__c = tu.objBillRateCardModel.id;
        update tu.objPbbScenario;
        
        // Create Mocks
        MockPbbPawsData thePawsContext = new MockPbbPawsData();
        thePawsContext.createMockData(tu.lstJobClassDesc);
        MockBudgetBillRateService theBillRateContext = new MockBudgetBillRateService();

        Test.startTest();
        PBB_BudgetEngineService theService = new PBB_BudgetEngineService();
        theService.setPawsDataContext(thePawsContext);
        theService.setBudgetBillRateContext(theBillRateContext);
        theService.createBudget(tu.objPbbScenario.id);

        list<Scenario_Budget__c> testSB = [SELECT id FROM Scenario_Budget__c WHERE active__c = true];
        list<Scenario_Budget_Task__c> testSBT = [SELECT id FROM Scenario_Budget_Task__c];
        list<Scenario_Budget_RoleCountry__c> tstSBRC = [SELECT id FROM Scenario_Budget_RoleCountry__c];
        
        System.debug('---- testSB.size(): ' + testSB.size());
        System.debug('---- testSBT.size(): ' + testSBT.size());
        System.debug('---- tstSBRC.size(): ' + tstSBRC.size());
        
        System.assertequals(1, testSB.size());
        System.assertequals(3, testSBT.size());
        System.assertequals(9, tstSBRC.size());

        // Since we have a created budget structure, let's test
        // the deactivation method here as well.
        theService.deactivateBudgets(tu.objPbbScenario.id);                                              

        testSB = [SELECT id FROM Scenario_Budget__c WHERE active__c = true];
        System.assertequals(0, testSB.size());
        Test.stopTest();
    }

    static testMethod void testCreateBudgetNegatives() {
        PBB_TestUtilsIntegrated tu = new PBB_TestUtilsIntegrated();
        
        tu.createCurrency();
        tu.createCountries();
        tu.createJobs(3);
        tu.createServiceModel();
        tu.createServiceAreas(tu.objServiceModel, 3);
        tu.createServiceFunctions(tu.lstServiceArea, 3);
        tu.createServiceTasks(tu.lstServiceFunction, 2);
        tu.pbbTestUtils.approveServicemodel(tu.objServiceModel);
        tu.createPbbScenario(null);
        tu.createBillRateCardModel();
        System.debug('---- ' + tu.objPbbScenario);
        System.debug('---- ' + tu.lstCountry);
        tu.createScenarioCountries(tu.objPbbScenario, 
                                   new list<Country__c> {tu.lstCountry[tu.COUNTRY_US], tu.lstCountry[tu.COUNTRY_UK] },
                                   true);
        
        list<list<sObject>> countriesAndTasks = new list<list<sObject>>();
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[0]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[1]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[2], tu.lstServiceTask[4]});
        tu.createCountriesServiceTasks(countriesAndTasks);

        Boolean caughtMissingRateCard = false;
        Boolean caughtMissingRoles    = false;
        Boolean caughtMissingRates    = false;
                
        PBB_BudgetEngineService theService;

        // Test Bill Rate Card attachment validation
        try {
            theService = new PBB_BudgetEngineService();
            theService.createBudget(tu.objPbbScenario.id);
        }
        catch (PRA_BaseService.PRAServiceException ex) {
            List<String> errList = ex.getErrorList(); 
            if (errList[0].contains('does not have a Billing Rate Card')) {
                caughtMissingRateCard = true;
            }
        }
        
        // Add Bill Rate Card to PBB Scenario
        tu.objPbbScenario.Bill_Rate_Card_Model__c = tu.objBillRateCardModel.id;
        update tu.objPbbScenario;


        // Test Missing Roles validation
        try {
            theService = new PBB_BudgetEngineService();

            // Create Mock but don't load with data this will trigger the exception
            MockPbbPawsData thePawsContext = new MockPbbPawsData();
            theService.setPawsDataContext(thePawsContext);

            MockBudgetBillRateService theBillRateContext = new MockBudgetBillRateService();
            theService.setBudgetBillRateContext(theBillRateContext);

            theService.createBudget(tu.objPbbScenario.id);
        }
        catch (PRA_BaseService.PRAServiceException ex) {
            List<String> errList = ex.getErrorList(); 
            if (errList[0].contains('has no roles for country')) {
                caughtMissingRoles = true;
            }
        }

        // Test Missing Bill Rates validation
        try {
            theService = new PBB_BudgetEngineService();
            MockPbbPawsData thePawsContext = new MockPbbPawsData();
            thePawsContext.createMockData(tu.lstJobClassDesc);
            theService.setPawsDataContext(thePawsContext);
            
            // Create Mock that throws exceptions
            MockBudgetBillRateServiceErrors theBillRateContext = new MockBudgetBillRateServiceErrors();
            theService.setBudgetBillRateContext(theBillRateContext);

            theService.createBudget(tu.objPbbScenario.id);
        }
        catch (PRA_BaseService.PRAServiceException ex) {
            List<String> errList = ex.getErrorList(); 
            if (errList[0].contains('Missing Bill Rate')) {
                caughtMissingRates = true;
            }
        }
        
        System.assert(caughtMissingRateCard);
        System.assert(caughtMissingRoles);
        System.assert(caughtMissingRates);
    }

    static testmethod void testUpdateImpactBeforeBudgetCreated() {
        PBB_TestUtilsIntegrated tu = new PBB_TestUtilsIntegrated();
        
        tu.createCurrency();
        tu.createCountries();
        tu.createJobs(3);
        tu.createServiceModel();
        tu.createServiceAreas(tu.objServiceModel, 3);
        tu.createServiceFunctions(tu.lstServiceArea, 3);
        tu.createServiceTasks(tu.lstServiceFunction, 2);
        tu.pbbTestUtils.approveServicemodel(tu.objServiceModel);
        tu.createPbbScenario(null);
        tu.createBillRateCardModel();
        System.debug('---- ' + tu.objPbbScenario);
        System.debug('---- ' + tu.lstCountry);
        tu.createScenarioCountries(tu.objPbbScenario, 
                                   new list<Country__c> {tu.lstCountry[tu.COUNTRY_US], tu.lstCountry[tu.COUNTRY_UK] },
                                   true);
        
        list<list<sObject>> countriesAndTasks = new list<list<sObject>>();
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[0]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[1]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[2], tu.lstServiceTask[4]});
        tu.createCountriesServiceTasks(countriesAndTasks);
        
        // Add Bill Rate Card to PBB Scenario
        tu.objPbbScenario.Bill_Rate_Card_Model__c = tu.objBillRateCardModel.id;
        update tu.objPbbScenario;
        
        // Create responses, attach service impact responses, and add values to impact responses
        tu.createImpactQuestionAndResponses(2, 2);  // 2 questions with 2 responses each
        tu.lstImpactResponses[0].Effort_Impacts_by_Hours__c = 1;
        tu.lstImpactResponses[1].Effort_Impacts_by_percentage__c = 0.25;
        tu.lstImpactResponses[2].Effort_Impacts_by_Hours__c = 5;
        tu.lstImpactResponses[3].Effort_Impacts_by_percentage__c = 0.50;
        update tu.lstImpactResponses;
        tu.createServiceTaskResponses(tu.lstCountriesServiceTasks, tu.lstImpactResponses);

                
        // Create Mocks
        MockPbbPawsData thePawsContext = new MockPbbPawsData();
        thePawsContext.createMockData(tu.lstJobClassDesc);
        MockBudgetBillRateService theBillRateContext = new MockBudgetBillRateService();

        // Create the budget
        PBB_BudgetEngineService theService = new PBB_BudgetEngineService();
        theService.setPawsDataContext(thePawsContext);
        theService.setBudgetBillRateContext(theBillRateContext);
        theService.createBudget(tu.objPbbScenario.id);
        
        // Test to make sure impacts loaded when budget created
        for (Scenario_Budget_RoleCountry__c sbrc : [SELECT Effort_Impact_Hours__c, Effort_Impact_Percent__c FROM Scenario_Budget_RoleCountry__c]) {
            System.debug('---- sbrc.Effort_Impact_Hours__c: ' + sbrc.Effort_Impact_Hours__c);
            System.debug('---- sbrc.Effort_Impact_Percent__c: ' + sbrc.Effort_Impact_Percent__c);
            System.assert(sbrc.Effort_Impact_Hours__c == 6);
            System.assert(sbrc.Effort_Impact_Percent__c == 0.75);
        }
    }

    static testmethod void testUpdateImpactAfterBudgetCreated() {
        PBB_TestUtilsIntegrated tu = new PBB_TestUtilsIntegrated();
        
        tu.createCurrency();
        tu.createCountries();
        tu.createJobs(3);
        tu.createServiceModel();
        tu.createServiceAreas(tu.objServiceModel, 3);
        tu.createServiceFunctions(tu.lstServiceArea, 3);
        tu.createServiceTasks(tu.lstServiceFunction, 2);
        tu.pbbTestUtils.approveServicemodel(tu.objServiceModel);
        tu.createPbbScenario(null);
        tu.createBillRateCardModel();
        System.debug('---- ' + tu.objPbbScenario);
        System.debug('---- ' + tu.lstCountry);
        tu.createScenarioCountries(tu.objPbbScenario, 
                                   new list<Country__c> {tu.lstCountry[tu.COUNTRY_US], tu.lstCountry[tu.COUNTRY_UK] },
                                   true);
        
        list<list<sObject>> countriesAndTasks = new list<list<sObject>>();
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[0]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[1]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[2], tu.lstServiceTask[4]});
        tu.createCountriesServiceTasks(countriesAndTasks);
        
        // Add Bill Rate Card to PBB Scenario
        tu.objPbbScenario.Bill_Rate_Card_Model__c = tu.objBillRateCardModel.id;
        update tu.objPbbScenario;
        
        // Create service impact responses, and add values to impact responses
        tu.createImpactQuestionAndResponses(2, 2);  // 2 questions with 2 responses each
        tu.lstImpactResponses[0].Effort_Impacts_by_Hours__c = 1;
        tu.lstImpactResponses[1].Effort_Impacts_by_percentage__c = 0.25;
        tu.lstImpactResponses[2].Effort_Impacts_by_Hours__c = 5;
        tu.lstImpactResponses[3].Effort_Impacts_by_percentage__c = 0.50;
        update tu.lstImpactResponses;
        
        // Create Mocks
        MockPbbPawsData thePawsContext = new MockPbbPawsData();
        thePawsContext.createMockData(tu.lstJobClassDesc);
        MockBudgetBillRateService theBillRateContext = new MockBudgetBillRateService();

        // Create the budget
        PBB_BudgetEngineService theService = new PBB_BudgetEngineService();
        theService.setPawsDataContext(thePawsContext);
        theService.setBudgetBillRateContext(theBillRateContext);
        theService.createBudget(tu.objPbbScenario.id);
        
        
        // This simulates answering a bunch of questions
        tu.createServiceTaskResponses(tu.lstCountriesServiceTasks, tu.lstImpactResponses);

        // Update the budget with revised impacts
        PBB_BudgetEngineService.updateImpact(tu.lstServiceTaskResponses);
        
        // Test to make sure impacts properly updated
        for (Scenario_Budget_RoleCountry__c sbrc : [SELECT Effort_Impact_Hours__c, Effort_Impact_Percent__c FROM Scenario_Budget_RoleCountry__c]) {
            System.debug('---- sbrc.Effort_Impact_Hours__c: ' + sbrc.Effort_Impact_Hours__c);
            System.debug('---- sbrc.Effort_Impact_Percent__c: ' + sbrc.Effort_Impact_Percent__c);
            System.assert(sbrc.Effort_Impact_Hours__c == 6);
            System.assert(sbrc.Effort_Impact_Percent__c == 0.75);
        }
    }

    
    
    /*
         =========== TEST DATA =========== 
    */
    private class MockPbbPawsData implements PBB_BudgetEngineService.IPawsData {
        list<PBB_BudgetEngineService.PawsWorkItem> mockData = new list<PBB_BudgetEngineService.PawsWorkItem>();
        
        public MockPbbPawsData() {}

        public List<PBB_BudgetEngineService.PawsWorkItem> getWorkBreakdown(ID aPawsProjectId) {
            System.debug('---- mockData: ' + mockData);
            return mockData;                        
        }
        
        public void createMockData(list<Job_Class_Desc__c> aLstJCD) {
            list<Countries_Service_Tasks__c> lstCST = [SELECT Service_Task__r.id, PBB_Scenario_Country__r.name, PBB_Scenario_Country__r.Country__r.id
                                                       FROM Countries_Service_Tasks__c];
            for (Countries_Service_Tasks__c cst : lstCST) {
                for (Job_Class_Desc__c jcd : aLstJCD) {
                    PBB_BudgetEngineService.PawsWorkItem theItem =
                    new PBB_BudgetEngineService.PawsWorkItem(jcd.Job_Code__c,
                                                             100,
                                                             Date.today(),
                                                             Date.today().addMonths(3),
                                                             cst.Service_Task__c,
                                                             cst.PBB_Scenario_Country__r.name,
                                                             cst.PBB_Scenario_Country__r.Country__r.id);
                    mockData.add(theItem);
                }
            }
        }
    }
    
    private class MockBudgetBillRateService implements PBB_BudgetBillRateService.IBudgetBillRateService {
        public void setBaseParameters(ID aRateCard, ID aCurrency) {}
        public void setCountryParameters(Set<ID> aCountries) {}
        public void setJobParameters(Set<ID> aJobs) {}
        public void loadBillRates() {}
        public decimal getBillRate(String aCountryId, String aJobClassDescId, String aYear) {
            return 10;
        }
        public decimal getWeightedRateEven(String aCountryId, String aJobClassDescId, Date aStartDate, Date aEndDate) {
            return 10;
        }
    }    
 
    private class MockBudgetBillRateServiceErrors extends PRA_BaseService implements PBB_BudgetBillRateService.IBudgetBillRateService {
        public void setBaseParameters(ID aRateCard, ID aCurrency) {}
        public void setCountryParameters(Set<ID> aCountries) {}
        public void setJobParameters(Set<ID> aJobs) {}
        public void loadBillRates() {}
        public decimal getBillRate(String aCountryId, String aJobClassDescId, String aYear) {
            PRAServiceException ex = new PRAServiceException('Missing Bill Rate Record');
            List<String> errLst = new List<String> {aCountryId, aJobClassDescId, aYear};
            ex.setErrorList(errLst);
            throw ex;
            return 10;
        }
        public decimal getWeightedRateEven(String aCountryId, String aJobClassDescId, Date aStartDate, Date aEndDate) {
            getBillRate(aCountryId, aJobClassDescId, String.valueOf(aStartDate.year()));
            return 10;
        }
    }
    
     static testmethod void testgetClientGridByRolectry() {
        PBB_TestUtilsIntegrated tu = new PBB_TestUtilsIntegrated();
        
        tu.createCurrency();
        tu.createCountries();
        tu.createJobs(3);
        tu.createServiceModel();
        tu.createServiceAreas(tu.objServiceModel, 3);
        tu.createServiceFunctions(tu.lstServiceArea, 3);
        tu.createServiceTasks(tu.lstServiceFunction, 2);
        tu.pbbTestUtils.approveServicemodel(tu.objServiceModel);
        tu.createPbbScenario(null);
        tu.createBillRateCardModel();
        System.debug('---- ' + tu.objPbbScenario);
        System.debug('---- ' + tu.lstCountry);
        tu.createScenarioCountries(tu.objPbbScenario, 
                                   new list<Country__c> {tu.lstCountry[tu.COUNTRY_US], tu.lstCountry[tu.COUNTRY_UK] },
                                   true);
        
        list<list<sObject>> countriesAndTasks = new list<list<sObject>>();
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[0]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[1]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[2], tu.lstServiceTask[4]});
        tu.createCountriesServiceTasks(countriesAndTasks);
        
        // Add Bill Rate Card to PBB Scenario
        tu.objPbbScenario.Bill_Rate_Card_Model__c = tu.objBillRateCardModel.id;
        update tu.objPbbScenario;
        
        // Create Mocks
        MockPbbPawsData thePawsContext = new MockPbbPawsData();
        thePawsContext.createMockData(tu.lstJobClassDesc);
        MockBudgetBillRateService theBillRateContext = new MockBudgetBillRateService();

        Test.startTest();
        PBB_BudgetEngineService theService = new PBB_BudgetEngineService();
        theService.setPawsDataContext(thePawsContext);
        theService.setBudgetBillRateContext(theBillRateContext);
        theService.createBudget(tu.objPbbScenario.id);

        list<Scenario_Budget__c> testSB = [SELECT id FROM Scenario_Budget__c WHERE active__c = true];
        list<Scenario_Budget_Task__c> testSBT = [SELECT id FROM Scenario_Budget_Task__c];
        list<Scenario_Budget_RoleCountry__c> tstSBRC = [SELECT id FROM Scenario_Budget_RoleCountry__c];
        
        System.debug('---- testSB.size(): ' + testSB.size());
        System.debug('---- testSBT.size(): ' + testSBT.size());
        System.debug('---- tstSBRC.size(): ' + tstSBRC.size());
        
        System.assertequals(1, testSB.size());
        System.assertequals(3, testSBT.size());
        System.assertequals(9, tstSBRC.size());
        
        Map<String,PBB_BudgetEngineService.ClientUnitGridCVO> MapCUGCVO = new Map<String,PBB_BudgetEngineService.ClientUnitGridCVO>();
        System.debug('@@@@ tu.objPbbScenario.id:' +tu.objPbbScenario.id);
        MapCUGCVO = PBB_BudgetEngineService.getClientGridByRolectry(tu.objPbbScenario.id);
        System.Assert(MapCUGCVO != null);
        System.debug('@@@@ MapCUGCVO:' +MapCUGCVO);
        Test.stopTest();
    }
    
    static testmethod void testgetClientgridbyRole() {
        PBB_TestUtilsIntegrated tu = new PBB_TestUtilsIntegrated();
        
        tu.createCurrency();
        tu.createCountries();
        tu.createJobs(3);
        tu.createServiceModel();
        tu.createServiceAreas(tu.objServiceModel, 3);
        tu.createServiceFunctions(tu.lstServiceArea, 3);
        tu.createServiceTasks(tu.lstServiceFunction, 2);
        tu.pbbTestUtils.approveServicemodel(tu.objServiceModel);
        tu.createPbbScenario(null);
        tu.createBillRateCardModel();
        System.debug('---- ' + tu.objPbbScenario);
        System.debug('---- ' + tu.lstCountry);
        tu.createScenarioCountries(tu.objPbbScenario, 
                                   new list<Country__c> {tu.lstCountry[tu.COUNTRY_US], tu.lstCountry[tu.COUNTRY_UK] },
                                   true);
        
        list<list<sObject>> countriesAndTasks = new list<list<sObject>>();
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[0]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[0], tu.lstServiceTask[1]});
        countriesAndTasks.add(new List<SObject> {tu.lstPbbScenarioCountry[2], tu.lstServiceTask[4]});
        tu.createCountriesServiceTasks(countriesAndTasks);
        
        // Add Bill Rate Card to PBB Scenario
        tu.objPbbScenario.Bill_Rate_Card_Model__c = tu.objBillRateCardModel.id;
        update tu.objPbbScenario;
        
        // Create Mocks
        MockPbbPawsData thePawsContext = new MockPbbPawsData();
        thePawsContext.createMockData(tu.lstJobClassDesc);
        MockBudgetBillRateService theBillRateContext = new MockBudgetBillRateService();

        Test.startTest();
        PBB_BudgetEngineService theService = new PBB_BudgetEngineService();
        theService.setPawsDataContext(thePawsContext);
        theService.setBudgetBillRateContext(theBillRateContext);
        theService.createBudget(tu.objPbbScenario.id);

        list<Scenario_Budget__c> testSB = [SELECT id FROM Scenario_Budget__c WHERE active__c = true];
        list<Scenario_Budget_Task__c> testSBT = [SELECT id FROM Scenario_Budget_Task__c];
        list<Scenario_Budget_RoleCountry__c> tstSBRC = [SELECT id FROM Scenario_Budget_RoleCountry__c];
        
        System.debug('---- testSB.size(): ' + testSB.size());
        System.debug('---- testSBT.size(): ' + testSBT.size());
        System.debug('---- tstSBRC.size(): ' + tstSBRC.size());
        
        System.assertequals(1, testSB.size());
        System.assertequals(3, testSBT.size());
        System.assertequals(9, tstSBRC.size());
        
        Map<String,PBB_BudgetEngineService.ClientUnitGridCVO> MapCUGCVOrole = new Map<String,PBB_BudgetEngineService.ClientUnitGridCVO>();
        MapCUGCVOrole = PBB_BudgetEngineService.getClientgridbyRole(tu.objPbbScenario.id);
        System.Assert(MapCUGCVOrole != null);
        System.debug('@@@@ MapCUGCVOrole:' +MapCUGCVOrole);
        Test.stopTest();
    }

    
}