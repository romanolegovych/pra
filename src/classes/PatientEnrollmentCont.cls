public with sharing class PatientEnrollmentCont {
    public PAWS_ProjectDetailCVO projectDetailItem {get;set;}
    public PBB_WR_APIs.PatientEnrollmentwrapper patientEnrollmentItem {get;set;}
    public String gType {get;set;}
    public String activeComponent {get;set;}
    public Id scenarioId {get;set;}

    public List<PBBMobileSettingWrapper> settingList {get;set;} 
    
    private Id projectId = ApexPages.currentPage().getParameters().get( PBBUtils.PROJECTID_PARAM_STRING );
    private PBB_Scenario__c scenarioItem {get;set;}
    
    public PatientEnrollmentCont() {
        settingList = PBBMobileSettingServices.getSettingList( Id.valueOf(UserInfo.getUserId() ) );

        if( projectId != null ) projectDetailItem = PAWS_API.PBB_getProjectDetails( projectId );
        if( projectDetailItem != null ){ // to do remove
            scenarioItem = PBB_WR_APIs.getAprrovedScenarioID( projectDetailItem.ProjectID );
            
            if( scenarioItem == null ){
                scenarioId = Id.valueOf('a0YL0000008ar8ZMAQ');
            } else {
                scenarioId = scenarioItem.Id;
            }
            patientEnrollmentItem = PBB_WR_APIs.GetPatientEnrollmentAttributes(scenarioId); // TODO Dima Hardcore
        }
        gType = 'Enrollment Status by Country';
    }

    public void showComponent(){
        gType = activeComponent;

    }

    public String saveSettings(){
        return PBBMobileSettingServices.saveSettings( settingList );
    }
}