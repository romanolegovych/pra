/** 
@Author Ramya
@Date 2015
@Description this class is inserting the Bid Project Records
*/
public class PBB_BidProjectController {   
    
    public    ID                bpID                  {get;set;}     //to get the Service Area ID
    public    Bid_Project__c    bp                    {get;set;}     //to get the Service Area Sobject for edit or new     
    public    string            SelectBSs             {get;set;}     //to get the selected Business Segments
    public    String            studyStrtupBegin      {get;set;}
    public    String            fstSubFirVis          {get;set;}
    public    String            lstSubLstVis          {get;set;}
    public    String            allSitesClsd          {get;set;}
    public    String            finCliStyRep          {get;set;}
    public    String            finProDate            {get;set;}
    public    String            invMeeting            {get;set;}
    public    String            lstSubFirVis          {get;set;}
    public    String            dataLoc               {get;set;}
    public    String            stasReprtCmplte       {get;set;}
    public    String            StdyDocRetToSponsr    {get;set;} 
    public    String            propDueDate           {get;set;}
    public    String            bidDefDate            {get;set;}
    public    String            projSrtDate           {get;set;}
    public    String            projEndDate           {get;set;}
    public    String            fstPatientIn          {get;set;}
    public    String            lstPatientIn          {get;set;}
    
    //Standard Constructor
    public PBB_BidProjectController(ApexPages.StandardController controller) {        
        
        //check if the bp is for edit
        bp= (Bid_Project__c)controller.getRecord();
        
        //check if the bp is for edit
        bpID = ApexPages.currentPage().getParameters().get('id');
        if(bpID !=null){
            bp = PBB_DataAccessor.getBidProjectByID(bpID);    
            if(bp.Business_Segment__c!=NULL){            
                SelectBSs =bp.Business_Segment__c; 
            } 
         studyStrtupBegin = PBB_Utils.getDateInStringFormat(bp.Study_Startup_Activities_Begin__c);
         fstSubFirVis = PBB_Utils.getDateInStringFormat(bp.First_Subject_First_Visit__c);
         lstSubLstVis= PBB_Utils.getDateInStringFormat(bp.Last_Subject_Last_Visit__c);
         allSitesClsd= PBB_Utils.getDateInStringFormat(bp.All_Sites_Closed__c);
         finCliStyRep= PBB_Utils.getDateInStringFormat(bp.Final_Clinical_Study_Report__c);
         finProDate=PBB_Utils.getDateInStringFormat(bp.Final_Protocol_Date__c);
         invMeeting = PBB_Utils.getDateInStringFormat(bp.Investigator_Meeting__c);
         lstSubFirVis = PBB_Utils.getDateInStringFormat(bp.Last_Subject_First_Visit__c);
         dataLoc = PBB_Utils.getDateInStringFormat(bp.Database_Lock_Top_Line_results__c);
         stasReprtCmplte = PBB_Utils.getDateInStringFormat(bp.Statistical_Report_Complete__c);
         StdyDocRetToSponsr = PBB_Utils.getDateInStringFormat(bp.All_Study_Documents_Returned_to_Sponsor__c); 
         
         propDueDate = PBB_Utils.getDateInStringFormat(bp.Proposal_Due_Date__c);
         bidDefDate  = PBB_Utils.getDateInStringFormat(bp.Bid_Defense_Date__c);         
         projSrtDate = PBB_Utils.getDateInStringFormat(bp.Project_Start_Date__c);
         projEndDate = PBB_Utils.getDateInStringFormat(bp.Project_End_Date__c);
         fstPatientIn = PBB_Utils.getDateInStringFormat(bp.First_Patient_In__c);
         lstPatientIn =  PBB_Utils.getDateInStringFormat(bp.Last_Patient_In__c);               
                   
        }       
    } 
    
    
    
    /** 
@Author Ramya
@Date 2015
@Description get the types of Businesssegments
*/
    public List<selectOption> getBusinessSegmentTypes(){
        List<SelectOption> options = new List<SelectOption>(); 
        for(PRA_Business_Unit__c bs:PBB_DataAccessor.getPRABusinessUnit()){
            options.add(new SelectOption(bs.name,bs.Business_Unit_Code__c+'-'+bs.name));
        }  
        return options;
    }
    
    
    /** 
@Author Ramya
@Date 2015
@Description save the Bid Project 
*/
    public PageReference saveBP() { 
        
        PageReference pageRef =null; 
        
        try{ 
            
            List<PBB_Scenario_Country__c> CST  = new List<PBB_Scenario_Country__c>(); 
            CST =  PBB_DataAccessor.getservicetaskforcntry(bpID);
            
            //checking if bid project contains any service tasks attached to it
            if(CST.size()>0 && bp.Business_Segment__c !=SelectBSs){
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''+'Please remove the selected Service Areas for existing scenarios prior to changing the Business Segment!')); 
            }
            
            //check if the Business segment is selected 
            else if(SelectBSs !=null){     
                bp.Business_Segment__c =SelectBSs;
                System.debug('--bp--'+bp);
                bp.Study_Startup_Activities_Begin__c=PBB_Utils.getDate(studyStrtupBegin) ;
                bp.Investigator_Meeting__c= PBB_Utils.getDate(invMeeting) ;
                bp.First_Subject_First_Visit__c=PBB_Utils.getDate(fstSubFirVis) ;
                bp.Last_Subject_First_Visit__c= PBB_Utils.getDate(lstSubFirVis) ;
                bp.Last_Subject_Last_Visit__c=PBB_Utils.getDate(lstSubLstVis);
                bp.Database_Lock_Top_Line_results__c=PBB_Utils.getDate(dataLoc) ;
                bp.All_Sites_Closed__c=PBB_Utils.getDate(allSitesClsd);
                bp.Statistical_Report_Complete__c=PBB_Utils.getDate(stasReprtCmplte );
                bp.Final_Clinical_Study_Report__c=PBB_Utils.getDate(finCliStyRep);
                bp.All_Study_Documents_Returned_to_Sponsor__c=PBB_Utils.getDate(StdyDocRetToSponsr) ;
                bp.Final_Protocol_Date__c=PBB_Utils.getDate(finProDate);
                bp.Proposal_Due_Date__c = PBB_Utils.getDate(propDueDate);   
                bp.First_Patient_In__c =PBB_Utils.getDate(fstPatientIn);
                bp.Last_Patient_In__c =PBB_Utils.getDate(lstPatientIn);
                  
                upsert bp;
                pageRef=new PageReference('/'+bp.id);
                pageRef.setRedirect(true);  
                System.debug('--pageRef--'+pageRef);
                
            }
            else{
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''+'Business Segment is required'));                      
            }
        }
        catch (Exception e) {
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''+e.getMessage()));           
        }
        return pageRef;        
    }  
}