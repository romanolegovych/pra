/**
@author Niharika Reddy
@date 2014
@description this is PBB_ProjectCriteriaController 's test class
**/

@isTest
   /** 
    * This is for code coverage
    */
    private class PBB_ProjectCriteriaControllerTest {
    static testMethod void testConstructor()    {
        test.startTest();
        PBB_DataAccessor pda = new PBB_DataAccessor();
        test.stopTest();
    }
   
   /**
    *  Init Test Utils
    */
    static testMethod void testPBB_ProjectCriteriaController() {
        PBB_TestUtils tu = new PBB_TestUtils();
        test.startTest();
        PBB_ProjectCriteriaController pcc = new PBB_ProjectCriteriaController();
        test.stopTest();
    }
    
}