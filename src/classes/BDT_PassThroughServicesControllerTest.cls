@isTest
public with sharing class BDT_PassThroughServicesControllerTest {
	
	static testMethod void PassThroughServicesTest(){
		BDT_PassThroughServicesController ptsc = new BDT_PassThroughServicesController();
		
		system.assertNotEquals(null,ptsc);
		
		ptsc.loadServiceCategories();
		
		ptsc.showPassThroughServicesPerCategory();
		
		ptsc.getServiceCatListClean();
		
		system.assertNotEquals( null, ptsc.createPassThroughService() );
		system.assertNotEquals( null, ptsc.editPassThroughService() );
		
		ptsc.ServiceCategoryList = new List<ServiceCategory__c>();
		ptsc.ServiceCategoryList.add(new ServiceCategory__c(Code__c='001.001.001'));
		ptsc.ServiceCategoryList = ptsc.getServiceCatListClean();
		system.assertEquals('1.1.:1', ptsc.ServiceCategoryList[0].Code__c);
	}

}