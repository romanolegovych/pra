@isTest
private class BDT_SubcontractorControllerTest {
	
	static testmethod void SubcontractorControllertest(){
		BDT_SubcontractorController scController = new BDT_SubcontractorController();
		
		system.assertNotEquals(null,scController);
		
		scController.showSubcontractorActivities();
		
		//the Subcontroller always returns a list, when there are no activities, an empty list will be instantiated.		
		system.assertNotEquals(null,scController.SubcontractorActivityList);
		
		system.assertNotEquals(null, scController.createSubcontractor() );
		system.assertNotEquals(null, scController.createSubcontractorActivity() );
		system.assertNotEquals(null, scController.editSubcontractor() );
		system.assertNotEquals(null, scController.editSubcontractorActivity() );
	}


}