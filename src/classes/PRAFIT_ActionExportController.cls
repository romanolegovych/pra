/*
 * PRAFIT_ActionExportController supports the VF page PRAFIT_ActionExport for exporting all PRAFIT_Action records a user has access to.
 *  
 *  IMPORTANT: Uses the “with sharing” keyword to enforce Sharing Rules and ensure limited PRAFIT_Action sObect records 
 *  are returned to User for export based on their Public Group memberships.
*/

public with sharing class PRAFIT_ActionExportController{

    //get all PRAFIT_Action records using PRAFIT_Utils dynamic soql method getSObjects
    public static List<PRAFIT_Action__c> getActivitiesForExport(){
        List<PRAFIT_Action__c> actionList = new List<PRAFIT_Action__c>();
        try{
            actionList = PRAFIT_Utils.getSObjects('PRAFIT_Action__c', null, new List<String>(),1000);                             
        }catch(Exception e){
            System.debug('>>>>>>>>>>>>>>>>>>>>  Exception in PRAFIT_ActionExportController : ' + e.getMessage());
        }
        if( actionList.size() > 0 )
        {
            return actionList;
        }
        else{
            return null;
        }
    }
}