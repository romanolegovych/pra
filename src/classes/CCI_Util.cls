public class CCI_Util {
	
	public static String transformStringWithNumbers(String original) {
		String transformed = '';
		Boolean findNextString = false;
		for (Integer i = 0; i < original.length(); i++) {
			Integer numberCount = 0;
			String sub = original.substring(i, i + 1);
			system.debug('--------------------------------------' + sub);
			String numberString = '';
			if (findNextString) {
				if (!sub.isNumeric()) {
					findNextString = false;
					transformed += sub;
				}
			} else {
				if (sub.isNumeric()) {
					numberCount = 1;
					Integer index = i;
					numberString += sub;
					findNextString = true;
					Boolean continueReading = true;
					while (continueReading) {
						if (index == original.length() - 1) {
							continueReading = false;
						} else {
							index++;
							String substr = original.substring(index, index + 1);
							if (substr.isNumeric()) {
								numberString += substr;
								numberCount++;
							} else {
								continueReading = false;
							}
						}
					}
				} 
				if (numberCount > 0) {
					for (Integer j = 0; j < 5 - numberCount; j++) {
						transformed += '0';
					}
					transformed += numberString;
				} else {
					transformed += sub;
				}
			}
		}
		return transformed;
	}
}