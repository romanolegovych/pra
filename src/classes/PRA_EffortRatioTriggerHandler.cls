/*
* Trigger Handler for the EffortRatio trigger
*/
public class PRA_EffortRatioTriggerHandler {
	
	public static final String THIS_CLASS_IS_DEPRACTED = 'Remove after Migration';
    public String testValue {get;set;}
    
    public PRA_EffortRatioTriggerHandler() {
    	testValue = THIS_CLASS_IS_DEPRACTED;
    }
}