public with sharing class PBB_LayoutModelController {

    public String instanceModelName {get;set;}
    public PBB_ServiceModelComponentController serviceModelController {get;set;}
    public PBB_SRMModelController sRMModelController {get;set;}
    public PBB_RegionModelsController regionModelsController {get;set;}
    public PBB_WRModelsController wRModelsController {get;set;}
    public PBB_BillRateCardModelsController billRateCardModelsController {get;set;}
    public PBB_BillFormulaModelsController billFormulaModelsController {get;set;}
    public PBB_ServiceImpactQuestionController serviceImpactQuestionController {get;set;}

    /** 
    @Author Oleksiy Tkachenko
    @Date 2015
    @Description Constructer
    */
    public PBB_LayoutModelController() {
    }

    private PBB_ComponentControllerBase componentController;
    
    public virtual PBB_ComponentControllerBase getMyComponentController() {
        return componentController;
    }

    public virtual void setComponentController(PBB_ComponentControllerBase compController) {
        componentController = compController;
    }

    /** 
    @Author Oleksiy Tkachenko
    @Date 2015
    @Description Instance PBB_ServiceModelComponentController
    */
    public void instanceServiceModelController() {
        if(instanceModelName == 'Service_Models') {
            serviceModelController = new PBB_ServiceModelComponentController();
        } else if(instanceModelName == 'SRM_Models') {
            sRMModelController = new PBB_SRMModelController();
        } else if(instanceModelName == 'Region_Models') {
            regionModelsController = new PBB_RegionModelsController();
        } else if(instanceModelName == 'WR_Models') {
            wRModelsController = new PBB_WRModelsController();
        } else if(instanceModelName == 'Bill_Rate_Card_Models') {
            billRateCardModelsController = new PBB_BillRateCardModelsController();
        } else if(instanceModelName == 'Bill_Formula_Models') {
            billFormulaModelsController = new PBB_BillFormulaModelsController();
        } else if(instanceModelName == 'Service_Impact_Question') {
            serviceImpactQuestionController = new PBB_ServiceImpactQuestionController();
        }
    }
}