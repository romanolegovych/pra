global with sharing class PIT_NewEditIssueController {
    public Issue__c                           issue                 {get;set;}  
    public boolean                            newIssueFlag          {get;set;}
    public String                             projectText           {get;set;} 
    public String                             selectedSite          {get;set;}
    public list<SelectOption>                 ctmsSubjectIds        {set;}
    public String                             selectedSubject       {get;set;}
    public ID                                 ProjectID             {get;set;}
    
    public List<Attachment>                   attachResults         {get;set;} 
    public string                             AttachId              {get;set;}
    public String                             delRowIndex           {get;set;}
    public boolean                            attachExist           {get;set;} 
    public Attachment                         newAttach             {get;set;}
    public boolean                            showUploadPage        {get;set;} 
     
     ApexPages.standardController  globalStdCtr;   
        
    Public PIT_NewEditIssueController (ApexPages.standardController stdCtr) {
        
                globalStdCtr    = stdCtr;
                showUploadPage  = false;
                newAttach               = new Attachment(); 
                attachResults   = new List<Attachment>();
                attachExist             = false;
       
  // Decide if the page is called for a new issue or for editing an existed issue
                String tmpId = system.currentPageReference().getParameters().get('id');
                newIssueFlag = (String.isBlank(tmpId));
                if(newIssueFlag) { // new issue
                        issue = new Issue__c();
                } else { // edit issue
                        try { // find the issue
                                issue = [SELECT Id,
                                                                Name,
                                                                Category__c,
                                                                Assign_To__c,
                                                                Subcategory__c,
                                                                Escalate_To__c,
                                                                Project__c,
                                                                Project__r.name,
                                                                Notified_To__c,
                                                                Site_Id__c,
                                                                Status__c,
                                                                Subject_ID__c,
                                                                Date_Resolved__c,
                                                                Issue_Type__c,
                                                                Action_Taken__c,
                                                                Description__c
                                                FROM    Issue__c
                                                WHERE   Id=:tmpId];
                                ProjectText=issue.Project__r.name;
                                ProjectID=issue.Project__c;
                                selectedSite=issue.Site_Id__c;
                                selectedSubject=issue.Subject_ID__c;                
                                attachExist = findAttachments(); 
                        } catch (exception e){ // unable to find the issue
                                issue = new Issue__c();
                                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected issue. Please, try again.');
                                ApexPages.addMessage(msg);  
                        }
                }
    }
    
   
     // used for cancel functionality when we are in save mode 
        public PageReference CancelNew() {
                if(issue.id != null) {
                        delete issue; // delete the issue cause temporarily we had insert it
                }
                try {
                        return globalStdCtr.Cancel();
                } catch (Exception e) {
                        return ApexPages.currentPage();
                } 
        } 
    
    
      // used to save functionality 
       public PageReference Save() {  
           try {
                     
                        system.debug('================'+issue.Project__r.name);
                        system.debug('================'+issue.Project__c+issue);
                        
                        system.debug('=========selectedSite======'+selectedSite+selectedSubject);                     
                        issue.Site_Id__c= (selectedSite== 'None')?null:selectedSite;
                        issue.Subject_ID__c=(selectedSubject== 'None')?null:selectedSubject;
                        system.debug('=========issue======'+issue);
                        issue.Project__c=ProjectID;
                        upsert issue; // insert or edit
                        return new PageReference('/'+issue.id);
                        
                } catch (exception e) {
                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                        ApexPages.addMessage(msg); 
                        return null;    
                } 
        }
        
    
    
    
     public list<SelectOption> getSiteIds(){
     system.debug('----projectID->'+projectID+projecttext);
       //projectID=null;
       List<SelectOption> prepareSiteIds =  new List<SelectOption>();
       prepareSiteIds.add(new SelectOption('None', '--None--')); 
       List<WFM_Project__c> lstProject =[select id,name from WFM_Project__c where ID = :ProjectID];
            if(lstProject.size()>0){ 
                List<WFM_Protocol__c > lstProtocol =[select id,name,Project_ID__c from WFM_Protocol__c where Project_ID__c in :lstProject];
                if(lstProtocol.size()>0){      
                    for(WFM_Site_Detail__c  site : [select id,name,Project_Protocol__c,Project_Protocol__r.name from WFM_Site_Detail__c where Project_Protocol__c in :lstProtocol AND Status__c != 'On Hold' AND Status__c != 'Closed' order by Name]){
                         prepareSiteIds .add(new SelectOption(site.Id, site.Name+':'+site.Project_Protocol__r.name)); 
                    }
                }
            }
            system.debug('----->'+prepareSiteIds);
       return prepareSiteIds ;     
   }  
   
   public list<SelectOption> getctmsSubjectIds(){
       List<SelectOption> receivedctmsSubjetIds =  new List<SelectOption>();
       receivedctmsSubjetIds.add(new SelectOption('None', '--None--'));
       //SiteId = '1-10QBQS';
       System.debug('*** site Id selected'+selectedSite);
       if(!Test.isRunningTest()){
       if(selectedSite!= NULL && selectedSite!= ''){ 
           System.debug('*** site Id selected'+selectedSite);
          List<WFM_Site_Detail__c> siteDetails = [select id,Site_ID__c from WFM_Site_Detail__c where Id=:selectedSite Limit 1];
          if(!siteDetails .isEmpty()){
               PIT_CtmsService.ICtmsServicePort ctms = new PIT_CtmsService.ICtmsServicePort();
               PIT_CtmsService.subjectIdResponse wsResult = ctms.getSubjectBySite(siteDetails[0].Site_ID__c);
               //PIT_CtmsService.subjectIdResponse wsResult = ctms.getSubjectBySite('1-10QBQS');
               system.debug('=============================='+wsResult);
               if(wsResult.subjectIds != null){
                   for(PIT_CtmsService.ctmsSubjectIdVO receivedResult: wsResult.subjectIds){
                       system.debug(receivedResult+'\n');
                       receivedctmsSubjetIds.add(new SelectOption(receivedResult.ctmsSubjectId, receivedResult.ctmsSubjectId));
                   }
               }else{
               system.debug('There are no Subjects for the selected site : '+siteDetails[0].Site_ID__c);
               }
               system.debug('Site Ids: '+receivedctmsSubjetIds );
           }    
       } 
    }      
       return receivedctmsSubjetIds;
   } 
   
   
         
        // used to find the attachments 
        public boolean findAttachments() {
                try{
                        attachResults = [SELECT Id, 
                                                                        ParentId, 
                                                                        Name, 
                                                                        BodyLength, 
                                                                        LastModifiedDate 
                                                        FROM    Attachment 
                                                        WHERE   ParentId =: issue.id 
                                                        ORDER BY LastModifiedDate];
                        return (attachResults.size() > 0);
                } catch(Exception e){
                        return false;
                }
        }
        
        
        // used to open a new tab to show the attachment
        public PageReference viewAttach(){
                return new PageReference('/servlet/servlet.FileDownload?file='+ AttachId);
        }       
        
        
        // used to remove an attachment from the attachments list
        public void removeAttach() {
                Attachment tmpAttach = new Attachment();
                try{
                        tmpAttach = [SELECT id FROM Attachment WHERE id =: AttachId];
                        delete tmpAttach;
                        attachResults.remove(Integer.valueOf(delRowIndex));
                        attachExist = (attachResults.size() > 0);
                } catch (Exception e) {
                        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the delete the selected attachment. Please, try again.');
                        ApexPages.addMessage(msg);  
                } 
        }
        
        
        // used to insert an attachment to the task(edit mode) or temporary to whatId(save mode)
        public void insertAttachment() {
                try {
                        // Insert the issue cause an existing issue is needed, later it will be deleted if the procedure is cancelled
                       system.debug('-------------------------'+issue+'=='+newAttach);
                        if(issue.id == null) {
                                insert issue;
                        }
                        if(newAttach.Body != NULL) { // if there is an attachment
                                newAttach.parentId = issue.id;
                                insert newAttach;
                        }
                        newAttach.Body = null; // to avoid limitations
                        attachResults.add(newAttach);
                        newAttach = new Attachment();
                        attachExist = true;
                } catch(Exception e) {
                 ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
                        newAttach.Body = null; // to avoid limitations
                        newAttach = new Attachment();
                }
                hideAttachSelPanel();
                
        }
        
        
        // used to show the attachment selection panel
        public void showAttachSelPanel() {
                showUploadPage = true;
        }
        
        
        // used to hide the attachment selection panel
        public void hideAttachSelPanel() {
                showUploadPage = false;
        }

}