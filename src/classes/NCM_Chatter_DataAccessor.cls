/**
 * @description	The Data Accessor Layer for the Chatter API
 * @author		Dimitrios Sgourdos
 * @date		Created: 09-Sep-2015
 */
public with sharing class NCM_Chatter_DataAccessor {
	
	/**
	 * @description	Get the subscription for the given parent object and the given user
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sep-2014
	 * @param		objectFollowedId    The object to check if the user is subscribed to
	 * @param		userId              The user to check if he is subscribed to the given feed
	 * @return		The subscriptions related with the given object and user
	*/
	static public List<EntitySubscription> getSubscriptionByFeedAndUser(Id objectToFollowId, Id userId) {
		return [SELECT	ID
				FROM	EntitySubscription 
				WHERE	ParentId = :objectToFollowId
						AND	SubscriberId = :userId
				LIMIT 1]; // Anyway maximum one subscription can exist per combination feed and user
	}
	
	
	/**
	 * @description	Get the subscription for the given parent object and the given user
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sep-2014
	 * @param		feedIds    The feeds that subscriptions could be related with
	 * @param		userIds    The users that subscriptions could be related with
	 * @return		The subscriptions related with the given feeds and users
	*/
	static public List<EntitySubscription> getSubscriptionsByFeedSetAndUserSet(Set<Id> feedIds, Set<Id> userIds) {
		return [SELECT	Id,
						ParentId,
						SubscriberId
				FROM	EntitySubscription 
				WHERE	ParentId IN : feedIds
						AND SubscriberId IN :userIds
				LIMIT	50000];
	}
	
	
	/**
	 * @description	Get the feed posts that are related with the given parent object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sep-2014
	 * @param		feedParentId    The id of the parent object
	 * @return		The posts related with the given object
	*/
	static public List<FeedItem> getFeedItemsByParentFeed(Id feedParentId) {
		return	[SELECT CreatedDate,
						CreatedBy.Name,
						parentId,
						Body,
						(select Id, CommentBody, CreatedDate, CreatedBy.Name
						 from FeedComments
						 order by CreatedDate
						 limit 50000)
				FROM	FeedItem
				WHERE	parentId = :feedParentId
				ORDER BY CreatedDate DESC
				LIMIT	50000];
	}
	
	
	/**
	 * @description	Get the posts that are related with the given parent object and topic
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sep-2015
	 * @param		feedParentId    	The id of the parent object
	 * @param		relatedTopicName    The related topic of the posts
	 * @return		The posts related with the given object and topic
	*/
	static public List<FeedItem> getFeedItemsByParentFeedAndRelatedTopic(Id feedParentId, String relatedTopicName) {
		return  [SELECT	CreatedDate,
						CreatedBy.Name,
						parentId,
						Body,
						(select Id, CommentBody, CreatedDate, CreatedBy.Name
						 from	FeedComments
						 order by CreatedDate
						 limit	50000)
				FROM	FeedItem
				WHERE	parentId = :feedParentId
						AND	Id IN (select EntityId
									from  TopicAssignment
									where Topic.Name = :relatedTopicName)
				ORDER BY CreatedDate DESC
				LIMIT	50000];
	}
}