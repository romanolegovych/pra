public class IPA_Calendar
{
public integer DaysinMonth{get; set;}
public list<integer> lsDaysinMonth{get; set;}
public integer rows {get; set;}
public integer StartofWeek {get; set;}
public integer tmpMonth {get; set;}
public integer tmpYear {get; set;}
public String tmpAction {get; set;}
public date startOfMonthDT {get; set;}
public integer Total_pg_Total_pg_date;

public IPA_Calendar()
{
    try
    {
        tmpMonth = Integer.ValueOf(ApexPages.currentPage().getParameters().get('Month'));    //System.Today().Month();
        tmpYear = Integer.ValueOf(ApexPages.currentPage().getParameters().get('Year'));  //System.Today().Year();    
        tmpAction = ApexPages.currentPage().getParameters().get('Action');  //System.Today().Year(); 
    }
    catch(Exception e)
    {
        if (tmpMonth == null ) tmpMonth = system.Today().Month();
        if (tmpYear == null ) tmpYear = system.Today().Year();
    }   
    
    if(tmpAction == 'Next') Next(); 
    else if (tmpAction == 'Previous') Previous();
    else LogicalFun();
}

public class dayRecord
{
    public date clsdayDate {get; set;}
    public list<IPA_Articles__c> clsdayArticle {get; set;}
    public String ShadeClass {get; set;}
}
public list<dayRecord> lsclsdayRecords {get; set;}

public void LogicalFun()
{
    DaysinMonth = date.daysInMonth(tmpYear,tmpMonth); //count total days in month
    lsclsdayRecords = new list<dayRecord>();
    lsDaysinMonth = new list<integer>();
    
    startOfMonthDT = Date.newInstance(tmpyear,tmpmonth,1);
    Date endOfMonthDT = Date.newInstance(tmpyear,tmpmonth,DaysinMonth);
    
    // To calculate day of week for 1st day of Month
    Date toFindDayofWeek = Date.newInstance(1900,1,7);
    StartofWeek  = toFindDayofWeek.daysBetween(startOfMonthDT);
    StartofWeek = math.mod(StartofWeek,7);
    System.debug('*****StartofWeek :' +StartofWeek);  
        
    Date Start_pg_date = startOfMonthDT - StartofWeek;
    integer days = DaysinMonth + StartofWeek;    
    if(days <= 35) days = 35 - days;
    else days = 42 - days;
    Date Last_pg_date = endOfMonthDT + days;
    integer Total_pg_date = StartofWeek + DaysinMonth + days;
    System.debug('*****Total_pg_date :' +StartofWeek +' ' +DaysinMonth +' '+ days +' '+Total_pg_date);
    
    IPA_BO_LinksManagement bo = new IPA_BO_LinksManagement();
    list<IPA_Articles__c> lsEventsHolidays = bo.returnEventsHolidays(Start_pg_date, Last_pg_date);
    system.debug('*****Date sz days :' +lsEventsHolidays.size()); 
    
    // Actual data  entries
    Date loopdate = Start_pg_date;
    for(integer loopexe=1; loopexe <= Total_pg_date; loopexe++)
    {
        dayRecord objdayRecord = new dayRecord();
        list<IPA_Articles__c> cart = new list<IPA_Articles__c>();
           
            for(IPA_Articles__c loopobj: lsEventsHolidays)
            {
                if(loopobj.Display_date__c == loopdate )
                {
                    cart.add(loopobj);
                }
            }
                        
            DateTime dtConverted = Datetime.newInstance(loopdate.year(), loopdate.month(), loopdate.day(),0,0,0);
            if(dtConverted.format('E') == 'Sun' || dtConverted.format('E') == 'Sat')            
            objdayRecord.shadeClass = 'calendar-weekend';
            
            if(loopdate == System.Today())
            objdayRecord.shadeClass = 'calendar-holiday';

            lsDaysinMonth.add(loopexe);
            objdayRecord.clsdayDate= loopdate;
            objdayRecord.clsdayArticle = cart;
            
            lsclsdayRecords.add(objdayRecord);
            loopdate += 1;
        
    }
    system.debug('***Pg days :' +lsclsdayRecords.size()); 
    
    rows = math.mod(Total_pg_date ,7);
    if(rows > 0) rows = Total_pg_date /7 +1 ;
    else rows = Total_pg_date /7;    
}

public PageReference Next()
{ if(tmpMonth==12){tmpMonth = 1; tmpYear+=1;} else{tmpMonth+=1;} LogicalFun(); return null;}

public PageReference Previous()
{ if(tmpMonth==1){tmpMonth = 12; tmpYear -=1;} else{tmpMonth-=1;} LogicalFun(); return null;}

}