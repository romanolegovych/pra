@isTest
private class ProjectServiceDataAccessorTest {

    static testMethod void myUnitTest() {
        // create test data
        Client_Project__c cp = new Client_Project__c(name='test');
        insert cp;
        
        ServiceCategory__c sc = new ServiceCategory__c(name='test',code__c='001');
        insert sc;
        
        list<Service__c> sList = new list<Service__c>();
        sList.add(new Service__c(ServiceCategory__c = sc.id, Name = 'a', SequenceNumber__c = 1));
        sList.add(new Service__c(ServiceCategory__c = sc.id, Name = 'b', SequenceNumber__c = 2));
        sList.add(new Service__c(ServiceCategory__c = sc.id, Name = 'c', SequenceNumber__c = 3));
        insert sList;
        
        list<ProjectService__c> psList = new list<ProjectService__c>();
        psList.add(new ProjectService__c(Client_Project__c = cp.id, Service__c = sList[0].id));
        psList.add(new ProjectService__c(Client_Project__c = cp.id, Service__c = sList[1].id));
        psList.add(new ProjectService__c(Client_Project__c = cp.id, Service__c = sList[2].id));
        insert psList;
        
        //test retrieve logic
        psList = ProjectServiceDataAccessor.getProjectServiceList('Client_Project__c = \'' + cp.id + '\'');
        system.assertEquals(3,sList.size());
    }
}