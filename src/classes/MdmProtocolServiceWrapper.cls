public class MdmProtocolServiceWrapper {
	
	private static String ENDPOINT = '';
	private static String SERVICENAME = '';
	private static Integer TIMEOUT = 0;
	private static List<String> errors;
	
	static {
		initSettings();
	}   
	
	public static MdmProtocolService.protocolResponse getProtocolVOBySystemId(String systemId) {
		MdmProtocolService.protocolResponse response = null;
		if (systemId != null) {
			MdmProtocolService.MdmProtocolServicePort service = getService();
			try {
				response = service.getProtocolVOBySystemId(systemId);
				system.debug('---------------- ProtocolVO retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- ProtocolVO retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}

	public static MdmProtocolService.protocolResponse getProtocolVOByClientProtNum(String clientProtNum) {
		MdmProtocolService.protocolResponse response = null;
		if (clientProtNum != null) {
			MdmProtocolService.MdmProtocolServicePort service = getService();
			try {
				response = service.getProtocolVOByClientProtNum(clientProtNum);
				system.debug('---------------- ProtocolVO retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- ProtocolVO retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmProtocolService.protocolListResponse getProtocolsLikeClientProtocolNum(String clientProtNum) {
		MdmProtocolService.protocolListResponse response = null;
		if (clientProtNum != null) {
			MdmProtocolService.MdmProtocolServicePort service = getService();
			try {
				response = service.getProtocolVOsLikeClientProtNum(clientProtNum);
				system.debug('---------------- ProtocolVO retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- ProtocolVO retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	} 
	
	public static MdmProtocolService.protocolListResponse getProtocolsNotMappedToSite() {
		MdmProtocolService.protocolListResponse response = null;
		MdmProtocolService.MdmProtocolServicePort service = getService();
		try {
			response = service.getProtocolsNotMappedToSite();
			system.debug('---------------- ProtocolVOs retrieval successfull -----------------');
		} catch (Exception e) {
			errors = new List<String>();
			errors.add(e.getMessage());
			system.debug('---------------- ProtocolVOs retrieval failed -----------------');
			system.debug('----- Error: ' + e.getMessage());
			system.debug('----- Stack trace: ' + e.getStackTraceString());
			system.debug('----- Cause: ' + e.getCause());
		}
		system.debug('----- Response is: ' + response);
		
		return response;
	}
	
	public static MdmProtocolService.protocolListResponse getProtocolsNotMappedToSiteBySystemId(String systemId) {
		MdmProtocolService.protocolListResponse response = null;
		if (systemId != null) {
			MdmProtocolService.MdmProtocolServicePort service = getService();
			try {
				response = service.getProtocolsNotMappedToSiteBySystemId(systemId);
				system.debug('---------------- ProtocolVOs retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- ProtocolVOs retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmProtocolService.valueResponse getEDocsUrlForProtocol(MdmProtocolService.ProtocolVO protocolVO) {
		MdmProtocolService.valueResponse response = null;
		if (protocolVO != null) {
			MdmProtocolService.MdmProtocolServicePort service = getService();
			try {
				response = service.getEDocsUrlForProtocol(protocolVO);
				system.debug('---------------- EDocs URL retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- EDocs URL retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmProtocolService.valueResponse getEDocsUrlByClientProtNum(String clientProtNum) {
		MdmProtocolService.valueResponse response = null;
		if (clientProtNum != null) {
			MdmProtocolService.MdmProtocolServicePort service = getService();
			try {
				response = service.getEDocsUrlByClientProtNumResponse(clientProtNum);
				system.debug('---------------- EDocs URL retrieval successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- EDocs URL retrieval failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmProtocolService.mdmCrudResponse saveProtocolFromVO(MdmProtocolService.protocolVO protocolVO) {
		MdmProtocolService.mdmCrudResponse response = null;
		if (protocolVO != null) {
			MdmProtocolService.MdmProtocolServicePort service = getService();
			try {
				response = service.saveProtocolFromVO(protocolVO);
				system.debug('---------------- Protocol save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Protocol save failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmProtocolService.mdmCrudResponse deleteProtocolFromVO(MdmProtocolService.protocolVO protocolVO) {
		MdmProtocolService.mdmCrudResponse response = null;
		if (protocolVO != null) {
			MdmProtocolService.MdmProtocolServicePort service = getService();
			try {
				response = service.deleteProtocolFromVO(protocolVO);
				system.debug('---------------- Protocol delete successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Protocol delete failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}
	
	public static MdmProtocolService.mdmCrudResponse saveRegionMappingForProtocolResponse(MdmProtocolService.regionVO regionVO) {
		MdmProtocolService.mdmCrudResponse response = null;
		if (regionVO != null) {
			MdmProtocolService.MdmProtocolServicePort service = getService();
			try {
				response = service.saveRegionMappingForProtocolResponse(regionVO);
				system.debug('---------------- Region save successfull -----------------');
			} catch (Exception e) {
				errors = new List<String>();
				errors.add(e.getMessage());
				system.debug('---------------- Region save failed -----------------');
				system.debug('----- Error: ' + e.getMessage());
				system.debug('----- Stack trace: ' + e.getStackTraceString());
				system.debug('----- Cause: ' + e.getCause());
			}
			system.debug('----- Response is: ' + response);
		}
		return response;
	}

	public static List<String> getErrors() {
		return errors;
	}
	
	private static MdmProtocolService.MdmProtocolServicePort getService() {
		MdmProtocolService.MdmProtocolServicePort service = new MdmProtocolService.MdmProtocolServicePort();
		service.endpoint_x = ENDPOINT + SERVICENAME;
		service.timeout_x = TIMEOUT;
		return service;
	}
	
	private static void initSettings() {
		Map<String, MuleServicesCS__c> settings = MuleServicesCS__c.getAll();
		ENDPOINT = settings.get('ENDPOINT').Value__c;
		SERVICENAME = settings.get('MdmProtocolService').Value__c;
		Integer to = Integer.valueOf(settings.get('TIMEOUT').Value__c);
		if (to != null && to > 0) {
			TIMEOUT = to;
		} 
		system.debug('--------- Using following Custom Settings for MuleServiceCS ---------');
		system.debug('ENDPOINT:'+ENDPOINT);
		system.debug('TIMEOUT:'+TIMEOUT);
		system.debug('----------------------- MuleServiceCS END ----------------------------');
	}
}