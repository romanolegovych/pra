@isTest
private class BDT_LaboratoryListsControllerTest {

    static testMethod void myUnitTest() {
        // Create the default records to represent two different lists
		List<LaboratorySelectionLists__c> tmpList = new List<LaboratorySelectionLists__c>();
		tmpList.add(new LaboratorySelectionLists__c(Name='Amount/Volume Units'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Analytical Techniques'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Anti-coagulants'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Compound Classes'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Concentration Units'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Detection Techniques'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Detection Types'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Matrices'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Sample Preparation Techniques'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Species'));
		tmpList.add(new LaboratorySelectionLists__c(Name='Storage Temperatures'));
		insert tmpList;
		// Create the controller
		BDT_LaboratoryListsController p = new BDT_LaboratoryListsController();
		system.assertEquals(p.listsNames.size(), 11);
		p.selectedListName = 'Amount/Volume Units';
		p.getListValues();
		p.selectedListName = 'Analytical Techniques';
		p.getListValues();
		p.selectedListName = 'Anti-coagulants';
		p.getListValues();
		p.selectedListName = 'Compound Classes';
		p.getListValues();
		p.selectedListName = 'Concentration Units';
		p.getListValues();
		p.selectedListName = 'Detection Techniques';
		p.getListValues();
		p.selectedListName = 'Detection Types';
		p.getListValues();
		p.selectedListName = 'Matrices';
		p.getListValues();
		p.selectedListName = 'Sample Preparation Techniques';
		p.getListValues();
		p.selectedListName = 'Species';
		p.getListValues();
		p.selectedListName = 'Storage Temperatures';
		p.getListValues();
		system.assertEquals(p.selectedLabList.size(), 0);
		p.addSelectedValue();
		p.cancelValue();
		p.addSelectedValue();
		p.saveValue();
		system.assertEquals(p.selectedLabList.size(), 0); // 0 cause the record must not be saved as it has null listFirstValue__c
		p.labSelListItem.listFirstValue__c = 'room temperature';
		p.saveValue();
		system.assertEquals(p.selectedLabList.size(), 1); // now the record must be saved
		p.selectedValueIndex = '0';
		p.editSelectedValue();
		p.labSelListItem.listFirstValue__c = '10 *c';
		p.saveValue();
		system.assertEquals(p.selectedLabList[0].listFirstValue__c, '10 *c');
		p.deleteValue();
		system.assertEquals(p.selectedLabList.size(), 0);
	}
}