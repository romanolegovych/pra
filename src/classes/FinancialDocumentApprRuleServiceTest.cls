/** Implements the test for the Service Layer of the object FinancialDocumentApprRule__c
 * @author	Dimitrios Sgourdos
 * @version	19-Dec-2013
 */
@isTest
private class FinancialDocumentApprRuleServiceTest {
	
	/** Test the function getAllFinancialDocumentApprRules.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	10-Dec-2013
	 */
	static testMethod void getAllFinancialDocumentApprRulesTest() {
		List<FinancialDocumentApprRule__c> results = FinancialDocumentApprRuleService.getAllFinancialDocumentApprRules();
	}
	
	
	/** Test the function getApprovalRuleWrapperData
	 * @author	Dimitrios Sgourdos
	 * @version	16-Dec-2013
	 */
	static testMethod void getApprovalRuleWrapperDataTest() {
		// Create data
		List<FinancialDocumentApprRule__c> initialList = new List<FinancialDocumentApprRule__c>();
		initialList.add(new FinancialDocumentApprRule__c(SequenceNumber__c = 1,
														Description__c = 'Test 1',
														SelectedRule__c = FinancialDocumentApprRuleDataAccessor.PAYMENT_TERMS,
														RuleFirstParameter__c = '10'));
		initialList.add(new FinancialDocumentApprRule__c(SequenceNumber__c=2,
														Description__c='Test 2',
														SelectedRule__c = FinancialDocumentApprRuleDataAccessor.DELTA_MORE,
														RuleFirstParameter__c = '10',
														ListOfServiceCategories__c='Srv1:Srv2' ));
		insert initialList;
		
		// Check the function
		String errorMessage = 'Error in reading the approval rules from the system';
		List<FinancialDocumentApprRuleService.approvalRuleWrapper> results = new List<FinancialDocumentApprRuleService.approvalRuleWrapper>();
		results = FinancialDocumentApprRuleService.getApprovalRuleWrapperData();
		
		system.assertEquals(results.size(), 2, errorMessage);
		
		system.assertEquals(results[0].rule.Id, initialList[0].Id);
		system.assertEquals(results[0].serviceCategoriesIdsList.size(), 0);
		
		system.assertEquals(results[1].rule.Id, initialList[1].Id);
		system.assertEquals(results[1].serviceCategoriesIdsList.size(), 2);
		system.assertEquals(results[1].serviceCategoriesIdsList[0], 'Srv1');
		system.assertEquals(results[1].serviceCategoriesIdsList[1], 'Srv2');
	}
	
	
	/** Test the function addRuleToApprovalRuleWrapperList
	 * @author	Dimitrios Sgourdos
	 * @version	12-Dec-2013
	 */
	static testMethod void addRuleToApprovalRuleWrapperListTest() {
		String errorMessage = 'Error in adding a new Financial Document Approval Rule';
		List<FinancialDocumentApprRuleService.approvalRuleWrapper> results = new List<FinancialDocumentApprRuleService.approvalRuleWrapper>();
		
		results = FinancialDocumentApprRuleService.addRuleToApprovalRuleWrapperList(results);
		system.assertEquals(results.size(), 1, errorMessage);
		system.assertEquals(results[0].rule.SequenceNumber__c, 1, errorMessage);
		
		results = FinancialDocumentApprRuleService.addRuleToApprovalRuleWrapperList(results);
		system.assertEquals(results.size(), 2, errorMessage);
		system.assertEquals(results[0].rule.SequenceNumber__c, 1, errorMessage);
		system.assertEquals(results[1].rule.SequenceNumber__c, 2, errorMessage);
	}
	
	
	/** Test the function resetApprovalRuleParameters
	 * @author	Dimitrios Sgourdos
	 * @version	12-Dec-2013
	 */
	static testMethod void resetApprovalRuleParametersTest() {
		// Create data
		FinancialDocumentApprRuleService.approvalRuleWrapper sourceItem = new FinancialDocumentApprRuleService.approvalRuleWrapper();
		sourceItem.rule = new FinancialDocumentApprRule__c(RuleFirstParameter__c ='TestA:TestB',
														RuleSecondParameter__c='TestC');
		sourceItem.serviceCategoriesIdsList = new List<String>{'TestA','TestB'};
		
		// Check the function
		String errorMessage = 'Error in reseting approval rule parameters';
		
		FinancialDocumentApprRuleService.approvalRuleWrapper result = new FinancialDocumentApprRuleService.approvalRuleWrapper();
		result = FinancialDocumentApprRuleService.resetApprovalRuleParameters(sourceItem);
		system.assertEquals(result.rule.RuleFirstParameter__c, '', errorMessage);
		system.assertEquals(result.rule.RuleFirstParameter__c, '', errorMessage);
		system.assertEquals(result.serviceCategoriesIdsList.size(), 0, errorMessage);
		
		sourceItem.rule = NULL;
		result = FinancialDocumentApprRuleService.resetApprovalRuleParameters(sourceItem);
		system.assertEquals(result.rule, NULL, errorMessage);
		system.assertEquals(result.serviceCategoriesIdsList.size(), 0, errorMessage);
	}
	
	
	/** Test the function removeRuleFromApprovalRuleWrapperList
	 * @author	Dimitrios Sgourdos
	 * @version	12-Dec-2013
	 */
	static testMethod void removeRuleFromApprovalRuleWrapperListTest() {
		// Create Dummy data
		List<FinancialDocumentApprRuleService.approvalRuleWrapper> sourceList = new List<FinancialDocumentApprRuleService.approvalRuleWrapper>();
		
		FinancialDocumentApprRuleService.approvalRuleWrapper newItem = new FinancialDocumentApprRuleService.approvalRuleWrapper();
		newItem.rule = new FinancialDocumentApprRule__c(Name='Test 1');
		sourceList.add(newItem);
		
		newItem = new FinancialDocumentApprRuleService.approvalRuleWrapper();
		newItem.rule = new FinancialDocumentApprRule__c(Name='Test 2');
		sourceList.add(newItem);
		
		// Check the function with illegal index as parameter
		String errorMessage = 'Accepting invalid index in removing approval rule';
		List<FinancialDocumentApprRuleService.approvalRuleWrapper> results = new List<FinancialDocumentApprRuleService.approvalRuleWrapper>();
		
		results = FinancialDocumentApprRuleService.removeRuleFromApprovalRuleWrapperList(sourceList, -1);
		system.assertEquals(results.size(), 2, errorMessage);
		system.assertEquals(results[0].rule.Name, 'Test 1', errorMessage);
		system.assertEquals(results[1].rule.Name, 'Test 2', errorMessage);
		
		results = FinancialDocumentApprRuleService.removeRuleFromApprovalRuleWrapperList(sourceList, 2);
		system.assertEquals(results.size(), 2, errorMessage);
		system.assertEquals(results[0].rule.Name, 'Test 1', errorMessage);
		system.assertEquals(results[1].rule.Name, 'Test 2', errorMessage);
		
		// Check the function with legal index as a parameter
		errorMessage = 'Error in removing approval rule';
		results = FinancialDocumentApprRuleService.removeRuleFromApprovalRuleWrapperList(sourceList, 0);
		system.assertEquals(results.size(), 1, errorMessage);
		system.assertEquals(results[0].rule.Name, 'Test 2', errorMessage);
	}
	
	
	/** Test the function deleteApprovalRules
	 * @author	Dimitrios Sgourdos
	 * @version	12-Dec-2013
	 */
	static testMethod void deleteApprovalRulesTest() {
		// Create data
		List<FinancialDocumentApprRule__c> sourceList = new List<FinancialDocumentApprRule__c>();
		sourceList.add(new FinancialDocumentApprRule__c(Description__c='Test 1', SequenceNumber__c=1));
		sourceList.add(new FinancialDocumentApprRule__c(Description__c='Test 2', SequenceNumber__c=2));
		insert sourceList;
		
		// Before deletion
		List<FinancialDocumentApprRule__c> rulesList = [SELECT Id FROM FinancialDocumentApprRule__c];
		system.assert(rulesList.size() > 0, 'Error in reading approval rules from the system');
		
		// After deletion
		String errorMessage = 'Error in deleting approval rules from the system';
		Boolean resultFlag = FinancialDocumentApprRuleService.deleteApprovalRules(sourceList);
		system.assertEquals(resultFlag, true, errorMessage);
		
		rulesList = [SELECT Id FROM FinancialDocumentApprRule__c];
		system.assertEquals(rulesList.size(), 0, errorMessage);
	}
	
	
	/** Test the function validateApprovalRules
	 * @author	Dimitrios Sgourdos
	 * @version	16-Dec-2013
	 */
	static testMethod void validateApprovalRulesTest() {
		// Create dummy data
		List<FinancialDocumentApprRuleService.approvalRuleWrapper> sourceList = new List<FinancialDocumentApprRuleService.approvalRuleWrapper>();
		
		FinancialDocumentApprRuleService.approvalRuleWrapper newItem = new FinancialDocumentApprRuleService.approvalRuleWrapper();
		newItem.rule = new FinancialDocumentApprRule__c();
		sourceList.add(newItem);
		
		String errorMessage = 'Error in validating aproval rules';
		
		// Validate with general fields (Description, SelectedRule, RUleFirstParameter) null
		Boolean result = FinancialDocumentApprRuleService.validateApprovalRules(sourceList);
		system.assertEquals(result, false, errorMessage);
		
		// Validate with general fields filled but without Service Category selection
		sourceList[0].rule.Description__c		 = 'Test rule';
		sourceList[0].rule.SelectedRule__c		 = FinancialDocumentApprRuleDataAccessor.DELTA_MORE;
		sourceList[0].rule.RuleFirstParameter__c = '10';
		result = FinancialDocumentApprRuleService.validateApprovalRules(sourceList);
		system.assertEquals(result, false, errorMessage);
		
		// Validate with general fields filled and Service Categories selected
		sourceList[0].serviceCategoriesIdsList.add('Service Category 1');
		
		result = FinancialDocumentApprRuleService.validateApprovalRules(sourceList);
		system.assertEquals(result, true, errorMessage);
		
		// Validate with selected rule configuration equal to PROPOSAL_VALUE_MORE (similar TOTAl_VALUE_MORE)
		sourceList[0].rule.SelectedRule__c = FinancialDocumentApprRuleDataAccessor.PROPOSAL_VALUE_MORE;
		result = FinancialDocumentApprRuleService.validateApprovalRules(sourceList);
		system.assertEquals(result, false, errorMessage);
		
		sourceList[0].rule.RuleSecondParameter__c = 'EUR';
		result = FinancialDocumentApprRuleService.validateApprovalRules(sourceList);
		system.assertEquals(result, true, errorMessage);
	}
	
	
	/** Test the function renumberApprovalRulesList
	 * @author	Dimitrios Sgourdos
	 * @version	13-Dec-2013
	 */
	static testMethod void renumberApprovalRulesListTest() {
		List<FinancialDocumentApprRule__c> sourceList = new List<FinancialDocumentApprRule__c>();
		sourceList.add(new FinancialDocumentApprRule__c(Description__c='Test A', SequenceNumber__c = 10));
		sourceList.add(new FinancialDocumentApprRule__c(Description__c='Test B', SequenceNumber__c = 8));
		sourceList.add(new FinancialDocumentApprRule__c(Description__c='Test C', SequenceNumber__c = 9));
		insert sourceList;
		
		String errorMessage = 'Error in reordering approval rules';
		
		sourceList = FinancialDocumentApprRuleService.renumberApprovalRulesList(sourceList);
		system.assertEquals(sourceList[0].Description__c, 'Test B', errorMessage);
		system.assertEquals(sourceList[0].SequenceNumber__c, 1, errorMessage);
		system.assertEquals(sourceList[1].Description__c, 'Test C', errorMessage);
		system.assertEquals(sourceList[1].SequenceNumber__c, 2, errorMessage);
		system.assertEquals(sourceList[2].Description__c, 'Test A', errorMessage);
		system.assertEquals(sourceList[2].SequenceNumber__c, 3, errorMessage);
	}
	
	
	/** Test the function extractApprovalRules
	 * @author	Dimitrios Sgourdos
	 * @version	16-Dec-2013
	 */
	static testMethod void extractApprovalRulesTest() {
		// Create Dummy data
		List<FinancialDocumentApprRuleService.approvalRuleWrapper> sourceList = new List<FinancialDocumentApprRuleService.approvalRuleWrapper>();
		
		FinancialDocumentApprRuleService.approvalRuleWrapper newItem = new FinancialDocumentApprRuleService.approvalRuleWrapper();
		newItem.rule = new FinancialDocumentApprRule__c(Name = 'Test 1',
														SelectedRule__c = FinancialDocumentApprRuleDataAccessor.PAYMENT_TERMS,
														SequenceNumber__c = 1);
		sourceList.add(newItem);
		
		newItem = new FinancialDocumentApprRuleService.approvalRuleWrapper();
		newItem.rule = new FinancialDocumentApprRule__c(Name='Test 2',
														SelectedRule__c = FinancialDocumentApprRuleDataAccessor.DELTA_MORE,
														SequenceNumber__c = 2);
		newItem.serviceCategoriesIdsList = new List<String>{'', 'Srv1', 'Srv2'};
		sourceList.add(newItem);
		
		// Check the function
		List<FinancialDocumentApprRule__c> results = new List<FinancialDocumentApprRule__c>();
		String errorMessage = 'Error in extracting approval rules from wrapper';
		
		results = FinancialDocumentApprRuleService.extractApprovalRules(sourceList);
		
		system.assertEquals(results.size(), 2, errorMessage);
		
		system.assertequals(results[0].Name, 'Test 1', errorMessage);
		system.assertequals(results[0].SequenceNumber__c, 1, errorMessage);
		
		system.assertequals(results[1].Name, 'Test 2', errorMessage);
		system.assertequals(results[1].SequenceNumber__c, 2, errorMessage);
		system.assertequals(results[1].ListOfServiceCategories__c, 'Srv1:Srv2', errorMessage);
	}
	
	
	/** Test the function saveApprovalRules
	 * @author	Dimitrios Sgourdos
	 * @version	16-Dec-2013
	 */
	static testMethod void saveApprovalRulesTest() {
		List<FinancialDocumentApprRule__c> sourceList = new List<FinancialDocumentApprRule__c>();
		sourceList.add(new FinancialDocumentApprRule__c());
		
		String errorMessage = 'Error in saving the approval rules';
		
		// Create an exception while saving
		Boolean result = FinancialDocumentApprRuleService.saveApprovalRules(sourceList);
		system.assertEquals(result, false, errorMessage);
		
		List<FinancialDocumentApprRule__c> retrievedRules = [SELECT Id FROM FinancialDocumentApprRule__c];
		system.assertEquals(retrievedRules.size(), 0, errorMessage);
		
		// Save acceptable data
		sourceList[0].Description__c = 'Test 1';
		sourceList[0].SelectedRule__c = FinancialDocumentApprRuleDataAccessor.PAYMENT_TERMS;
		sourceList[0].RuleFirstParameter__c = '10';
		
		result = FinancialDocumentApprRuleService.saveApprovalRules(sourceList);
		system.assertEquals(result, true, errorMessage);
		
		retrievedRules = [SELECT Id FROM FinancialDocumentApprRule__c];
		system.assertEquals(retrievedRules.size(), 1, errorMessage);
	}
	
	
	/** Test the function isNumOfCurrenciesRuleApplicable
	 * @author	Dimitrios Sgourdos
	 * @version	19-Dec-2013
	 */
	static testMethod void isNumOfCurrenciesRuleApplicableTest() {
		// Create document data
		FinancialDocumentService.financialDocumentGroupWrapper documentData = new FinancialDocumentService.financialDocumentGroupWrapper();
		
		List<FinancialCatTotPrice__c> pricesList = new List<FinancialCatTotPrice__c>();
		pricesList.add(new FinancialCatTotPrice__c(OriginalCurrency__c='EUR'));
		documentData.pricesPerServiceCategoryMap.put('SrvCat1', pricesList);
		
		pricesList = new List<FinancialCatTotPrice__c>();
		pricesList.add(new FinancialCatTotPrice__c(OriginalCurrency__c='EUR'));
		pricesList.add(new FinancialCatTotPrice__c(OriginalCurrency__c='USD'));
		documentData.pricesPerServiceCategoryMap.put('SrvCat2', pricesList);
		
		// Create rule data
		FinancialDocumentApprRuleService.approvalRuleWrapper ruleData = new FinancialDocumentApprRuleService.approvalRuleWrapper();
		ruleData.rule = new FinancialDocumentApprRule__c(RuleFirstParameter__c = '1');
		ruleData.serviceCategoriesIdsList.add('SrvCat1');
		ruleData.serviceCategoriesIdsList.add('SrvCat3');
		
		// Check the function
		String errorMessage = 'Error in apply approval rule to financial document';
		
		// Check with only SrvCat1 present in the rule, rule number of currencies equal to 1. SrvCat3 is not assigned
		// to the document
		boolean result = FinancialDocumentApprRuleService.isNumOfCurrenciesRuleApplicable(ruleData, documentData);
		system.assertEquals(result, false, errorMessage);
		
		// Check with SrvCat1, SrvCat2 present in the rule, rule number of currencies equal to 1. SrvCat3 is
		// not assigned to the document
		ruleData.serviceCategoriesIdsList.add('SrvCat2');
		result = FinancialDocumentApprRuleService.isNumOfCurrenciesRuleApplicable(ruleData, documentData);
		system.assertEquals(result, true, errorMessage);
		
		// Check function by creating an exception in the number of currencies
		ruleData.rule.RuleFirstParameter__c = 'This value creates an exception';
		result = FinancialDocumentApprRuleService.isNumOfCurrenciesRuleApplicable(ruleData, documentData);
		system.assertEquals(result, false, errorMessage);
	}
	
	
	/** Test the function isValueOfProposalRuleApplicable
	 * @author	Dimitrios Sgourdos
	 * @version	19-Dec-2013
	 */
	static testMethod void isValueOfProposalRuleApplicableTest() {
		// Create document data
		FinancialDocumentService.financialDocumentGroupWrapper documentData = new FinancialDocumentService.financialDocumentGroupWrapper();
		
		List<FinancialCatTotPrice__c> pricesList = new List<FinancialCatTotPrice__c>();
		pricesList.add(new FinancialCatTotPrice__c(OriginalCurrency__c='EUR', TotalPrice__c=5));
		pricesList.add(new FinancialCatTotPrice__c(OriginalCurrency__c='USD', TotalPrice__c=30));
		documentData.pricesPerServiceCategoryMap.put('SrvCat1', pricesList);
		
		pricesList = new List<FinancialCatTotPrice__c>();
		pricesList.add(new FinancialCatTotPrice__c(OriginalCurrency__c='EUR', TotalPrice__c=10));
		pricesList.add(new FinancialCatTotPrice__c(OriginalCurrency__c='USD', TotalPrice__c=20));
		documentData.pricesPerServiceCategoryMap.put('SrvCat2', pricesList);
		
		// Create rule data
		FinancialDocumentApprRuleService.approvalRuleWrapper ruleData = new FinancialDocumentApprRuleService.approvalRuleWrapper();
		ruleData.rule = new FinancialDocumentApprRule__c(RuleFirstParameter__c = '7', RuleSecondParameter__c = 'EUR');
		ruleData.serviceCategoriesIdsList.add('SrvCat1');
		ruleData.serviceCategoriesIdsList.add('SrvCat3');
		
		// Check the function
		String errorMessage = 'Error in apply approval rule to financial document';
		
		// Check with only SrvCat1 present in the rule, value limit equal to 7. SrvCat3 is not assigned
		// to the document
		boolean result = FinancialDocumentApprRuleService.isValueOfProposalRuleApplicable(ruleData, documentData);
		system.assertEquals(result, false, errorMessage);
		
		// Check with SrvCat1, SrvCat2 present in the rule, value limit equal to 7. SrvCat3 is
		// not assigned to the document
		ruleData.serviceCategoriesIdsList.add('SrvCat2');
		result = FinancialDocumentApprRuleService.isValueOfProposalRuleApplicable(ruleData, documentData);
		system.assertEquals(result, true, errorMessage);
		
		// Check with SrvCat1, SrvCat2 present in the rule, value limit equal to 17. SrvCat3 is
		// not assigned to the document
		ruleData.rule.RuleFirstParameter__c = '17';
		result = FinancialDocumentApprRuleService.isValueOfProposalRuleApplicable(ruleData, documentData);
		system.assertEquals(result, false, errorMessage);
		
		// Check function by creating an exception in the limit value
		ruleData.rule.RuleFirstParameter__c = 'This value creates an exception';
		result = FinancialDocumentApprRuleService.isValueOfProposalRuleApplicable(ruleData, documentData);
		system.assertEquals(result, false, errorMessage);
	}
	
	
	/** Test the function isTotalValueOfProposalRuleApplicable
	 * @author	Dimitrios Sgourdos
	 * @version	19-Dec-2013
	 */
	static testMethod void isTotalValueOfProposalRuleApplicableTest() {
		// Create document data
		FinancialDocumentService.financialDocumentGroupWrapper documentData = new FinancialDocumentService.financialDocumentGroupWrapper();
		
		List<FinancialCatTotPrice__c> pricesList = new List<FinancialCatTotPrice__c>();
		pricesList.add(new FinancialCatTotPrice__c(OriginalCurrency__c='EUR', TotalPrice__c=5));
		pricesList.add(new FinancialCatTotPrice__c(OriginalCurrency__c='USD', TotalPrice__c=30));
		documentData.pricesPerServiceCategoryMap.put('SrvCat1', pricesList);
		
		pricesList = new List<FinancialCatTotPrice__c>();
		pricesList.add(new FinancialCatTotPrice__c(OriginalCurrency__c='EUR', TotalPrice__c=10));
		pricesList.add(new FinancialCatTotPrice__c(OriginalCurrency__c='USD', TotalPrice__c=20));
		documentData.pricesPerServiceCategoryMap.put('SrvCat2', pricesList);
		
		// Create rule data
		FinancialDocumentApprRuleService.approvalRuleWrapper ruleData = new FinancialDocumentApprRuleService.approvalRuleWrapper();
		ruleData.rule = new FinancialDocumentApprRule__c(RuleFirstParameter__c = '17', RuleSecondParameter__c = 'EUR');
		
		// Check the function
		String errorMessage = 'Error in apply approval rule to financial document';
		
		// Check with limit more than the total value of the document
		boolean result = FinancialDocumentApprRuleService.isTotalValueOfProposalRuleApplicable(ruleData, documentData);
		system.assertEquals(result, false, errorMessage);
		
		// Check with limit less than the total value of the document
		ruleData.rule.RuleFirstParameter__c = '12';
		result = FinancialDocumentApprRuleService.isTotalValueOfProposalRuleApplicable(ruleData, documentData);
		system.assertEquals(result, true, errorMessage);
		
		// Check function by creating an exception in the limit value
		ruleData.rule.RuleFirstParameter__c = 'This value creates an exception';
		result = FinancialDocumentApprRuleService.isTotalValueOfProposalRuleApplicable(ruleData, documentData);
		system.assertEquals(result, false, errorMessage);
	}
	
	
	/** Test the function isDeltaRuleApplicable
	 * @author	Dimitrios Sgourdos
	 * @version	19-Dec-2013
	 */
	static testMethod void isDeltaRuleApplicableTest() {
		// Create document data
		FinancialDocumentService.financialDocumentGroupWrapper documentData = new FinancialDocumentService.financialDocumentGroupWrapper();
		
		List<FinancialCatTotPrice__c> pricesList = new List<FinancialCatTotPrice__c>();
		pricesList.add(new FinancialCatTotPrice__c(PriceDelta__c = 5) );
		pricesList.add(new FinancialCatTotPrice__c(PriceDelta__c = 10) );
		documentData.pricesPerServiceCategoryMap.put('SrvCat1', pricesList);
		
		pricesList = new List<FinancialCatTotPrice__c>();
		pricesList.add(new FinancialCatTotPrice__c(PriceDelta__c = 12) );
		pricesList.add(new FinancialCatTotPrice__c(PriceDelta__c = -20) );
		documentData.pricesPerServiceCategoryMap.put('SrvCat2', pricesList);
		
		// Create rule data
		FinancialDocumentApprRuleService.approvalRuleWrapper ruleData = new FinancialDocumentApprRuleService.approvalRuleWrapper();
		ruleData.rule = new FinancialDocumentApprRule__c(RuleFirstParameter__c = '15');
		ruleData.serviceCategoriesIdsList.add('SrvCat1');
		ruleData.serviceCategoriesIdsList.add('SrvCat3');
		
		// Check the function
		String errorMessage = 'Error in apply approval rule to financial document';
		
		// Check with only SrvCat1 present in the rule, delta limit equal to 15. SrvCat3 is not assigned
		// to the document
		boolean result = FinancialDocumentApprRuleService.isDeltaRuleApplicable(ruleData, documentData);
		system.assertEquals(result, false, errorMessage);
		
		// Check with SrvCat1, SrvCat2 present in the rule, delta limit equal to 15. SrvCat3 is
		// not assigned to the document
		ruleData.serviceCategoriesIdsList.add('SrvCat2');
		result = FinancialDocumentApprRuleService.isDeltaRuleApplicable(ruleData, documentData);
		system.assertEquals(result, true, errorMessage);
		
		// Check function by creating an exception in the limit value
		ruleData.rule.RuleFirstParameter__c = 'This value creates an exception';
		result = FinancialDocumentApprRuleService.isDeltaRuleApplicable(ruleData, documentData);
		system.assertEquals(result, false, errorMessage);
	}
	
	
	/** Test the function getApplicableApprovalRulesForFinancialDocument
	 * @author	Dimitrios Sgourdos
	 * @version	19-Dec-2013
	 */
	static testMethod void getApplicableApprovalRulesForFinancialDocumentTest() {
		// Create project
		Client_Project__c currentProject = BDT_BuildTestData.buildClientProject('PRA1312-001', 'BDT Project First Experiments');
		insert currentProject;
		
		// Create study
		Study__c study = new Study__c();
		study.Project__c = currentProject.Id;
		insert study;
		
		// Create financial document
		FinancialDocument__c financialDocument = new FinancialDocument__c();
		financialDocument.DocumentType__c	= 'Proposal';
		financialDocument.Client_Project__c	= currentProject.Id; 
		insert financialDocument;
		
		// Create document data
		ServiceCategory__c srvCat = new ServiceCategory__c();
		srvCat.Code__c = '001';
		srvCat.Name = 'Test Category';
		insert srvCat;
		
		FinancialCategoryTotal__c fct = new FinancialCategoryTotal__c();
		fct.FinancialDocument__c = financialDocument.Id;
		fct.ServiceCategory__c	 = srvCat.Id;
		fct.Study__c			 = study.Id;
		insert fct;
		
		List<FinancialCatTotPrice__c> sourceList = new List<FinancialCatTotPrice__c>();
		sourceList.add(new FinancialCatTotPrice__c(FinancialCategoryTotal__c = fct.Id,
												OriginalCurrency__c = 'EUR',
												TotalPrice__c = 100,
												PriceDelta__c = 10));
		sourceList.add(new FinancialCatTotPrice__c(FinancialCategoryTotal__c = fct.Id,
												OriginalCurrency__c = 'EUR',
												TotalPrice__c = 50,
												PriceDelta__c = 20));
		sourceList.add(new FinancialCatTotPrice__c(FinancialCategoryTotal__c = fct.Id,
												OriginalCurrency__c = 'USD',
												TotalPrice__c = 50,
												PriceDelta__c = 30));
		insert sourceList;
		
		// Create approval rules data
		List<FinancialDocumentApprRule__c> rulesList = new List<FinancialDocumentApprRule__c>();
		
		rulesList.add(new FinancialDocumentApprRule__c(
											SequenceNumber__c = 1,
											Description__c = 'Applicable Total Value more rule',
											SelectedRule__c = FinancialDocumentApprRuleDataAccessor.TOTAL_VALUE_MORE,
											RuleFirstParameter__c = '120',
											RuleSecondParameter__c = 'EUR'));
		
		rulesList.add(new FinancialDocumentApprRule__c(
											SequenceNumber__c = 2,
											Description__c = 'Not Applicable Total Value more rule',
											SelectedRule__c = FinancialDocumentApprRuleDataAccessor.TOTAL_VALUE_MORE,
											RuleFirstParameter__c = '200',
											RuleSecondParameter__c = 'EUR'));
		
		rulesList.add(new FinancialDocumentApprRule__c(
											SequenceNumber__c = 3,
											Description__c = 'Applicable Proposal Value more rule',
											SelectedRule__c = FinancialDocumentApprRuleDataAccessor.PROPOSAL_VALUE_MORE,
											RuleFirstParameter__c = '120',
											RuleSecondParameter__c = 'EUR',
											ListOfServiceCategories__c = srvCat.Id));
		
		rulesList.add(new FinancialDocumentApprRule__c(
											SequenceNumber__c = 4,
											Description__c = 'Not Applicable Proposal Value more rule',
											SelectedRule__c = FinancialDocumentApprRuleDataAccessor.PROPOSAL_VALUE_MORE,
											RuleFirstParameter__c = '200',
											RuleSecondParameter__c = 'EUR',
											ListOfServiceCategories__c = srvCat.Id));
		
		rulesList.add(new FinancialDocumentApprRule__c(
											SequenceNumber__c = 5,
											Description__c = 'Applicable Delta more rule',
											SelectedRule__c = FinancialDocumentApprRuleDataAccessor.DELTA_MORE,
											RuleFirstParameter__c = '15',
											ListOfServiceCategories__c = srvCat.Id));
		
		rulesList.add(new FinancialDocumentApprRule__c(
											SequenceNumber__c = 6,
											Description__c = 'Not Applicable Delta more rule',
											SelectedRule__c = FinancialDocumentApprRuleDataAccessor.DELTA_MORE,
											RuleFirstParameter__c = '40',
											ListOfServiceCategories__c = srvCat.Id));
		
		rulesList.add(new FinancialDocumentApprRule__c(
											SequenceNumber__c = 7,
											Description__c = 'Applicable Number of Currencies more rule',
											SelectedRule__c = FinancialDocumentApprRuleDataAccessor.NUM_OF_CURRENCIES,
											RuleFirstParameter__c = '1',
											ListOfServiceCategories__c = srvCat.Id));
		
		rulesList.add(new FinancialDocumentApprRule__c(
											SequenceNumber__c = 8,
											Description__c = 'Not applicable Number of Currencies more rule',
											SelectedRule__c = FinancialDocumentApprRuleDataAccessor.NUM_OF_CURRENCIES,
											RuleFirstParameter__c = '2',
											ListOfServiceCategories__c = srvCat.Id));
		
		insert rulesList;
		
		// Check the function
		List<FinancialDocumentApprRule__c> results =
				FinancialDocumentApprRuleService.getApplicableApprovalRulesForFinancialDocument(financialDocument.Id);
		
		String errorMessage = 'Error in getting the applicable rules for the document';
		
		system.assertEquals(results.size(), 4, errorMessage);
		system.assertequals(results[0].Description__c, 'Applicable Total Value more rule', errorMessage);
		system.assertequals(results[1].Description__c, 'Applicable Proposal Value more rule', errorMessage);
		system.assertequals(results[2].Description__c, 'Applicable Delta more rule', errorMessage);
		system.assertequals(results[3].Description__c, 'Applicable Number of Currencies more rule', errorMessage);
	}
}