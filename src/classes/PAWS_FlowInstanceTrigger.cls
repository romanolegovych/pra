public with sharing class PAWS_FlowInstanceTrigger extends STSWR1.AbstractTrigger
{
	/************************ PUBLIC SECTION ****************************/
	
	public override void beforeInsert(List<SObject> records)
	{
		fillProject((List<STSWR1__Flow_Instance__c>)records);
	}
	
	public override void beforeUpdate(List<SObject> records, List<SObject> oldRecords)
	{
		fillProject((List<STSWR1__Flow_Instance__c>)records);
	}
	
	public override void afterInsert(List<SObject> records)
	{
		try
		{
			updateFlowInstances((List<STSWR1__Flow_Instance__c>)records);
			updateCompleteDates((List<STSWR1__Flow_Instance__c>)records);
			//updateCriticalChains((List<STSWR1__Flow_Instance__c>) records);
		}catch(Exception ex)
		{
			for(Integer i = 0; i < records.size(); i++)
				records[i].addError(ex.getMessage());
		}
	}
	
	public override void afterUpdate(List<SObject> records, List<SObject> oldRecords)
	{
		try
		{
			updateCompleteDates((List<STSWR1__Flow_Instance__c>)records);
		}catch(Exception ex)
		{
			for(Integer i = 0; i < records.size(); i++)
				records[i].addError(ex.getMessage());
		}
	}
	
	/************************ PRIVATE SECTION ****************************/
	
	private void fillProject(List<STSWR1__Flow_Instance__c> records)
	{
		for(Integer i = 0; i < records.size(); i++)
		{
			if(records[i].STSWR1__Object_Type__c != String.valueOf(ecrf__c.getSObjectType())) continue;
			records[i].PAWS_Project_Flow__c = records[i].STSWR1__Object_Id__c;
		}
	}

    private void updateFlowInstances(List<STSWR1__Flow_Instance__c> records)
    {
    	Map<String, String> pawsObjects = new Map<String, String>{
    		'ecrf__c' => 'paws_project_flow_junction__c', 
    		'paws_project_flow_country__c' => 'paws_project_flow_country__c', 
    		'paws_project_flow_site__c' => 'paws_project_flow_site__c', 
    		'paws_project_flow_document__c' => 'paws_project_flow_document__c', 
    		'paws_project_flow_agreement__c' => 'paws_project_flow_agreement__c', 
    		'paws_project_flow_submission__c' => 'paws_project_flow_submission__c'
    	};
    	
    	Map<String, String> fieldsMapping = new Map<String, String>{
    		'paws_project_flow_junction__c' => 'Project__c,Flow__c,Flow_Instance__c', 
    		'paws_project_flow_country__c' => 'Id,Flow__c,Flow_Instance__c', 
    		'paws_project_flow_site__c' => 'Id,Flow__c,Flow_Instance__c', 
    		'paws_project_flow_document__c' => 'Id,Flow__c,Flow_Instance__c', 
    		'paws_project_flow_agreement__c' => 'Id,Flow__c,Flow_Instance__c', 
    		'paws_project_flow_submission__c' => 'Id,Flow__c,Flow_Instance__c'
    	};

    	Map<String, STSWR1__Flow_Instance__c> recordsMap = new Map<String, STSWR1__Flow_Instance__c>();
    	Set<String> projectIds = new Set<String>();
    	Set<String> flowIds = new Set<String>();
    	
    	Map<String, String> queries = new Map<String, String>();
    	for(STSWR1__Flow_Instance__c record : records)
    	{
    		if(!pawsObjects.containsKey(record.STSWR1__Object_Type__c.toLowerCase())) continue;
    		
    		String fields = fieldsMapping.get(pawsObjects.get(record.STSWR1__Object_Type__c.toLowerCase()));
    		List<String> fieldsItems = fields.split(',');

    		String query = 'select ' + fields + ' from ' + pawsObjects.get(record.STSWR1__Object_Type__c.toLowerCase()) + ' where ' + fieldsItems[0] +' in :projectIds and ' + fieldsItems[1] +' in :flowIds';
    		queries.put(record.STSWR1__Object_Type__c.toLowerCase(), query);
    		
    		recordsMap.put(record.STSWR1__Object_Id__c + record.STSWR1__Flow__c, record);
    		projectIds.add(record.STSWR1__Object_Id__c);
    		flowIds.add(record.STSWR1__Flow__c);
    	}
    	
    	if(recordsMap.size() == 0) return;
    	
    	for(String objectName : queries.keySet())
    	{
    		PAWS_Utilities.checkLimit('Queries',1);
    		system.debug('----objectName = ' + objectName);
    		system.debug('----query = ' + queries.get(objectName));
    		List<sObject> sources = Database.query(queries.get(objectName));
    		
    		String fields = fieldsMapping.get(pawsObjects.get(objectName));
    		List<String> fieldsItems = fields.split(',');
    		
	    	for(Integer i = 0; i < sources.size(); i++)
	    	{
	    		String key = String.valueOf(sources[i].get(fieldsItems[0])) + String.valueOf(sources[i].get(fieldsItems[1]));
	    		if(!recordsMap.containsKey(key))
	    		{
	    			sources.remove(i);
	    			i--;
	    			continue;
	    		}
	    		
	    		sources[i].put(fieldsItems[2], recordsMap.get(key).Id);
	    	}
	    	
	    	PAWS_Utilities.checkLimit('DMLStatements',1);
	    	update sources;
    	}
    }

	public void updateStartDates(List<STSWR1__Flow_Instance_History__c> histories)
	{
		List<ID> cursorIDs = new List<ID>();
		for (STSWR1__Flow_Instance_History__c history : histories)
		{
			cursorIDs.add(history.STSWR1__Cursor__c);
		}
		
		PAWS_Utilities.checkLimit('Queries',1);
		updateStartDates([select Name from STSWR1__Flow_Instance__c where Id in (select STSWR1__Flow_Instance__c from STSWR1__Flow_Instance_Cursor__c where Id in :cursorIDs)]);
	}
	
	private void updateStartDates(List<STSWR1__Flow_Instance__c> instances)
	{
		PAWS_Utilities.checkLimit('Queries',1);
		Map<ID, STSWR1__Flow_Instance_History__c> startHistoryByLockedStepID = new Map<ID, STSWR1__Flow_Instance_History__c>();
		for (STSWR1__Flow_Instance_Cursor__c startCursor : [select 
					STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c, STSWR1__Flow_Instance__r.STSWR1__Parent_Step__c,
					//(select STSWR1__Actual_Start_Date__c from STSWR1__Flow_Instance_History__r order by STSWR1__Actual_Start_Date__c asc limit 1)
					(select STSWR1__Actual_Start_Date__c from STSWR1__Flow_Instance_History__r where STSWR1__Step__r.STSWR1__Is_First_Step__c = true and STSWR1__Actual_Start_Date__c != null order by STSWR1__Actual_Start_Date__c asc limit 1)
				from STSWR1__Flow_Instance_Cursor__c
				where STSWR1__Flow_Instance__c in :instances
					and STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c != null
					and STSWR1__Flow_Instance__r.STSWR1__Parent_Step__c != null for update])
		{
			if (startCursor.STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c
					== startCursor.STSWR1__Flow_Instance__r.STSWR1__Parent_Step__c)
			{
				for (STSWR1__Flow_Instance_History__c completeHistory : startCursor.STSWR1__Flow_Instance_History__r)
				{
					if (startHistoryByLockedStepID.get(startCursor.STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c) == null
							|| startHistoryByLockedStepID.get(startCursor.STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c).STSWR1__Actual_Start_Date__c > completeHistory.STSWR1__Actual_Start_Date__c)
					{
						startHistoryByLockedStepID.put(startCursor.STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c, completeHistory);
					}
				}
			}
		}
		
		//System.debug('----startHistoryByLockedStepID = ' + System.Json.serialize(startHistoryByLockedStepID));//@TODO: Delete this line!
		if(startHistoryByLockedStepID.size() == 0) return;
		
		PAWS_Utilities.checkLimit('Queries',1);
		List<STSWR1__Flow_Instance_History__c> modifiedHistories = new List<STSWR1__Flow_Instance_History__c>();
		for (STSWR1__Flow_Step_Junction__c lockedStep : [select 
				(select STSWR1__Actual_Start_Date__c from STSWR1__Flow_Instance_History__r order by CreatedDate desc limit 1)
			from STSWR1__Flow_Step_Junction__c
			where Id in :startHistoryByLockedStepID.keyset() for update])
		{
			STSWR1__Flow_Instance_History__c subflowHistory = startHistoryByLockedStepID.get(lockedStep.Id);
			//System.debug('----subflowHistory = ' + System.Json.serialize(subflowHistory));//@TODO: Delete this line!
			if (subflowHistory.STSWR1__Actual_Start_Date__c != null)
			{
				for (STSWR1__Flow_Instance_History__c existingHistory : lockedStep.STSWR1__Flow_Instance_History__r)
				{
					//System.debug('----subflowHistory.STSWR1__Actual_Start_Date__c = ' + subflowHistory.STSWR1__Actual_Start_Date__c);
					//System.debug('----existingHistory.STSWR1__Actual_Start_Date__c = ' + existingHistory.STSWR1__Actual_Start_Date__c);
					if (existingHistory.STSWR1__Actual_Start_Date__c == null
							|| subflowHistory.STSWR1__Actual_Start_Date__c < existingHistory.STSWR1__Actual_Start_Date__c)
					{
						//DEBUG to test problem with UNABLE_TO_LOCK_ROW
						//if(existingHistory.STSWR1__Actual_Start_Date__c == subflowHistory.STSWR1__Actual_Start_Date__c) continue;
						//DEBUG
						
						existingHistory.STSWR1__Actual_Start_Date__c = subflowHistory.STSWR1__Actual_Start_Date__c;
						//System.debug('----UPDATE: existingHistory.STSWR1__Actual_Start_Date__c = ' + existingHistory.STSWR1__Actual_Start_Date__c);
						modifiedHistories.add(existingHistory);
					}
				}
			}
		}
		
		PAWS_Utilities.checkLimit('DMLStatements',1);
		//System.debug('----doing update');
		update modifiedHistories;
	}
	
	/**
	* Updates Actual_Complete_Date__c for parent step when a subflow has been completed
	* and current flow had locked the same Parent Step it has been kicked-off from.
	*/
	private void updateCompleteDates(List<STSWR1__Flow_Instance__c> instances)
	{
		/**
		по-хорошему, чтобы правильно определить последний степ, нужно взять список всех коннекторов, где Connector.Source_Step_Id = Step.Id.
		Если коннекторов нет, или среди них есть хоть один, у которого Target_Step_Id == null, то тогда такой степ можно считать последним
		*/
		List<ID> flowIDs = new List<ID>();
		for (STSWR1__Flow_Instance__c each : instances)
		{
			flowIDs.add(each.STSWR1__Flow__c);
		}
		
		PAWS_Utilities.checkLimit('Queries',1);
		List<STSWR1__Flow_Step_Junction__c> lastSteps = new List<STSWR1__Flow_Step_Junction__c>();
		for (STSWR1__Flow_Step_Junction__c step : [select (select Name from STSWR1__Flow_Step_Output_Connections__r where STSWR1__Flow_Step_Target__c != null) from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__c in :flowIDs])
		{
			if (step.STSWR1__Flow_Step_Output_Connections__r.isEmpty())
			{
				lastSteps.add(step);
			}
		}
		
		PAWS_Utilities.checkLimit('Queries',1);
		Map<ID, STSWR1__Flow_Instance_History__c> completeHistoryByLockedStepID = new Map<ID, STSWR1__Flow_Instance_History__c>();
		for (STSWR1__Flow_Instance_Cursor__c completedCursor : [select 
					STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c, STSWR1__Flow_Instance__r.STSWR1__Parent_Step__c,
					(select STSWR1__Actual_Complete_Date__c from STSWR1__Flow_Instance_History__r where STSWR1__Actual_Complete_Date__c != null and STSWR1__Step__c in :lastSteps order by STSWR1__Actual_Complete_Date__c desc limit 1)
				from STSWR1__Flow_Instance_Cursor__c
				where STSWR1__Flow_Instance__c in :instances
					and STSWR1__Flow_Instance__r.STSWR1__Is_Completed__c = 'Yes'
					and STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c != null
					and STSWR1__Flow_Instance__r.STSWR1__Parent_Step__c != null
					for update])
		{
			if (completedCursor.STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c
					== completedCursor.STSWR1__Flow_Instance__r.STSWR1__Parent_Step__c)
			{
				for (STSWR1__Flow_Instance_History__c completeHistory : completedCursor.STSWR1__Flow_Instance_History__r)
				{
					if (completeHistoryByLockedStepID.get(completedCursor.STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c) == null
							|| completeHistoryByLockedStepID.get(completedCursor.STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c).STSWR1__Actual_Complete_Date__c < completeHistory.STSWR1__Actual_Complete_Date__c)
					{
						completeHistoryByLockedStepID.put(completedCursor.STSWR1__Flow_Instance__r.STSWR1__Lock_Parent_Step__c, completeHistory);
					}
				}
			}
		}
		
		if(completeHistoryByLockedStepID.size() == 0) return;
		
		PAWS_Utilities.checkLimit('Queries',1);
		List<STSWR1__Flow_Instance_History__c> modifiedHistories = new List<STSWR1__Flow_Instance_History__c>();
		for (STSWR1__Flow_Step_Junction__c lockedStep : [select 
				(select STSWR1__Actual_Complete_Date__c from STSWR1__Flow_Instance_History__r order by CreatedDate desc limit 1)
			from STSWR1__Flow_Step_Junction__c
			where Id in :completeHistoryByLockedStepID.keyset() for update])
		{
			STSWR1__Flow_Instance_History__c subflowHistory = completeHistoryByLockedStepID.get(lockedStep.Id);
			if (subflowHistory.STSWR1__Actual_Complete_Date__c != null)
			{
				for (STSWR1__Flow_Instance_History__c existingHistory : lockedStep.STSWR1__Flow_Instance_History__r)
				{
					
					if (existingHistory.STSWR1__Actual_Complete_Date__c == null
							|| subflowHistory.STSWR1__Actual_Complete_Date__c > existingHistory.STSWR1__Actual_Complete_Date__c)
					{
						//DEBUG to test problem with UNABLE_TO_LOCK_ROW
						//if(existingHistory.STSWR1__Actual_Complete_Date__c == subflowHistory.STSWR1__Actual_Complete_Date__c) continue;
						//DEBUG
						
						existingHistory.STSWR1__Actual_Complete_Date__c = subflowHistory.STSWR1__Actual_Complete_Date__c;
						modifiedHistories.add(existingHistory);
					}
				}
			}
		}
		
		PAWS_Utilities.checkLimit('DMLStatements',1);
		update modifiedHistories;
	}
	
	/*private void updateCriticalChains(List<STSWR1__Flow_Instance__c> instances)
	{
		PAWS_Utilities.checkLimits(new Map<String, Integer> 
		{
			'Queries' => 1,//2,
			'DMLStatements' => 1
		});
		
		Map<ID, ID> flowIdFlowInstanceIdMap = new Map<ID, ID>();
		for (STSWR1__Flow_Instance__c instance : instances)
		{
			flowIdFlowInstanceIdMap.put(instance.STSWR1__Flow__c, instance.Id);
		}
		
		List<Critical_Chain__c> associatedChains = new List<Critical_Chain__c>();
		
		for (Critical_Chain__c chain : [select Flow__c, Flow_Instance__c from Critical_Chain__c where Flow__c in :flowIdFlowInstanceIdMap.keyset()])
		{
			chain.Flow_Instance__c = flowIdFlowInstanceIdMap.get(chain.Flow__c);
			associatedChains.add(chain);
		}
		
		update associatedChains;
	}*/
}