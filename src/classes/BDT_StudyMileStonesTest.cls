@isTest
private class BDT_StudyMileStonesTest {
	public static Client_Project__c clientProject 		{get;set;}
	public static List<Study__c> studiesList 			{get;set;}
	public static List<Business_Unit__c> buList  		{get;set;}
	public static list<MileStoneDefinition__c> msdList	{get;set;}
	public static list<StudyMileStone__c> smsList		{get;set;}
	
	static void init(){
		clientProject = BDT_TestDataUtils.buildProject();
		insert clientProject;
		
		buList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert buList;
		
		
		studiesList = BDT_TestDataUtils.buildStudies(clientProject);
		studiesList[0].Business_Unit__c = buList[0].Id; 
		studiesList[1].Business_Unit__c = buList[1].Id; 
		studiesList[2].Business_Unit__c = buList[2].Id;
		studiesList[3].Business_Unit__c = buList[3].Id; 
		studiesList[4].Business_Unit__c = buList[4].Id;
		insert studiesList;
		string selectedStudies ='';
		for(Study__c st: studiesList){
			selectedStudies += st.Id+':';
		}
		BDT_CreateTestData.buildMileStoneDefinition();
		msdList = [select id from MileStoneDefinition__c];
		
		smsList = new list<StudyMileStone__c>();
		for(MileStoneDefinition__c msd: msdList ){
			StudyMileStone__c sms = new StudyMileStone__c();
			sms.DueDate__c = Date.today();
			sms.Study__c = studiesList[0].id;
			sms.MileStoneDefinition__c = msd.id;
		}
		insert smsList;
		
		
		
	}
	
	
	static testmethod void BDT_StudyMileStonesTest(){
		init();
		
		//check void methods
		BDT_StudyMileStones a = new BDT_StudyMileStones();
		a.clientProject = clientProject;

		a.studyId= studiesList[0].Id;
		a.buildMileStoneWrapper();
				
		a.mileStoneTable[1].applicable = true;
		a.mileStoneTable[1].sms.DueDate__c = Date.today();
		system.assertNotEquals(null, a.save() );
		
		a.showCopyMileStones();
		a.copyFromStudyId = studiesList[0].Id;
		a.buildCopyToList();
		
		//normal copy
		a.studiesToCopyTo = string.valueOf(studieslist[1].id);
		system.assertNotEquals(null, a.doCopy() );
		
		a.copyFromStudyId = studiesList[2].Id;
		a.buildCopyToList();
		
		//source is empty
		list<string> tsList2 = new list<string>();
		tslist2.add(studiesList[3].Id);
		a.studiesToCopyTo = tsList2[0];
		system.assertEquals(null, a.doCopy() );
		
		//target has milestones
		a.copyFromStudyId = studiesList[1].Id;
		list<string> tsList3 = new list<string>();
		tslist3.add(studiesList[0].Id);
		a.studiesToCopyTo = tsList3[0];
		system.assertEquals(null, a.doCopy() );
		
		a.addrow();
		
		a.buildStudyOptions();
				
		system.assertNotEquals(null, a.cancel() );
		//system.assertEquals(null, a.doCopy() );
		
		//SAVE returns null, since no duedate is set
		a.mileStoneTable[0].applicable = true;
		system.assertEquals(null, a.save() );
		
	}


}