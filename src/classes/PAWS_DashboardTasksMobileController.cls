public with sharing class PAWS_DashboardTasksMobileController
{
	public String ObjectTypeFilterParam = 'ecrf__c';

	public List<TaskItem> Tasks
	{
		get
		{
			if(Tasks == null)
			{
				Tasks = loadTasks();
			}
			return Tasks;
		}
		set;
	}

    public void refreshTasks()
    {
        Tasks = null;
    }

    private List<TaskItem> loadTasks()
    {
    	List<TaskItem> taskItems = new List<TaskItem>();
    	Map<String, Object> params = new Map<String, Object> { 'filter' => 'Object_Type__c = \'' + ObjectTypeFilterParam + '\''};
		List<STSWR1__Flow_Instance_Cursor__c> cursors = (List<STSWR1__Flow_Instance_Cursor__c>) new STSWR1.API().call('WorkflowTasksService', 'getCurrentUserTasks', null);
		//List<STSWR1__Flow_Instance_Cursor__c> cursors = new List<STSWR1__Flow_Instance_Cursor__c>();
		for(STSWR1__Flow_Instance_Cursor__c cursor : cursors)
		{
			taskItems.add(new TaskItem(cursor));
		}

		taskItems.sort();
		return taskItems;
    }
    
    public class TaskItem implements Comparable
    {
    	public TaskItem(STSWR1__Flow_Instance_Cursor__c flowInstanceCursor)
        {
        	this.FlowInstanceCursor = flowInstanceCursor;
        	this.ObjectName = this.FlowInstanceCursor.STSWR1__Flow_Instance__r.STSWR1__Object_Name__c;

        	this.DueDate = flowInstanceCursor.STSWR1__Step__r.STSWR1__Duration__c == null || flowInstanceCursor.STSWR1__Step_Changed_Date__c == null ? null : flowInstanceCursor.STSWR1__Step_Changed_Date__c.addDays(Integer.valueOf(flowInstanceCursor.STSWR1__Step__r.STSWR1__Duration__c));
        	this.IsLate = this.DueDate == null ? false : (this.DueDate < Datetime.now());
        }

        public transient STSWR1__Flow_Instance_Cursor__c FlowInstanceCursor {get;set;}
        public String ObjectName {get;set;}
        public Datetime DueDate {get;set;}
        public Boolean IsLate {get;set;}
        public String StatusClass
        {
        	get
        	{
        		if(IsLate == true)
        		{
					return 'vs_red';
        		}
        		else if(FlowInstanceCursor.STSWR1__Status__c == 'Pending')
        		{
        			return 'vs_blue';
        		}
        		else if(FlowInstanceCursor.STSWR1__Status__c == 'In Progress')
        		{
        			return 'vs_yellow';
        		}
        		else if(FlowInstanceCursor.STSWR1__Status__c == 'Complete')
        		{
        			return 'vs_green';
        		}
        		else
        		{
        			return '';
        		}
        	}
        }

        public String getViewUrl()
        {
        	//PageReference pageRef = FlowInstanceCursor.STSWR1__Step__r.STSWR1__Layout_Mode__c == 'Edit' ? Page.DataEditMobile : Page.DataViewMobile;
        	PageReference pageRef = Page.PAWS_DashboardTasksMobile;
        	pageRef.getParameters().put('stepId', FlowInstanceCursor.STSWR1__Step__c);
			pageRef.getParameters().put('cursorId', FlowInstanceCursor.Id);
        	return pageRef.getUrl();
        }

		public Integer compareTo(Object compareTo)
		{
			TaskItem compareToTask = (TaskItem)compareTo;
			if(this.DueDate == null && compareToTask.DueDate == null)
			{
				return 0;
			}
			else if(this.DueDate == null && compareToTask.DueDate != null)
			{
				return 1;
			}
			else if(this.DueDate != null && compareToTask.DueDate == null)
			{
				return -1;
			}

			return this.DueDate > compareToTask.DueDate ? 1 : (this.DueDate < compareToTask.DueDate ? -1 : 0); 
		}
    }
    
    /***************************** TESTS SECTION *******************************/

    /*
    static testmethod void dashboardTasksMobileControllerTests()
    {   
    	STSWR1__Flow_Step_Junction__c step = FlowApexTestsEnvironment.FlowStep;
		step.STSWR1__Duration__c = 1;

        STSWR1__Flow_Instance_Cursor__c cursor = FlowApexTestsEnvironment.FlowInstanceCursor;
        cursor.STSWR1__Step_Changed_Date__c = Datetime.now();
		cursor.STSWR1__Step__r = step;

        TaskItem testItem = new TaskItem(cursor);
        testItem.DueDate = Datetime.now();
		testItem.compareTo(testItem);

        PAWS_DashboardTasksMobileController controller = new PAWS_DashboardTasksMobileController();
        controller.refreshTasks();
        System.assert(controller.Tasks != null);
        System.assert(controller.Tasks[0].getViewUrl() != null);

        PAWS_DashboardTasksMobileController.TaskItem task = new PAWS_DashboardTasksMobileController.TaskItem(cursor, new User());
        System.assert(task.StatusClass != null);
    }
    */
}