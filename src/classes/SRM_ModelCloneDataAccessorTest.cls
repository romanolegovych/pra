/**
* @author Ramya Shree Edara
* @date 21-Oct-2014
* @description this is test class for scenario related classes
*/
@isTest
public with sharing class SRM_ModelCloneDataAccessorTest {
	public static testMethod void testSRM_ModelCloneDataAccessor(){
        //Create Country
        Country__c country = new Country__c();
        country.Name = 'India';
        country.Country_Code__c = 'IN';
        country.Daily_Business_Hrs__c = 8;
        country.Description__c = 'Test India';
        country.PRA_Country_Id__c = 'TI1';
        insert country;
                
         //Create SRM Model
        SRM_Model__c model = new SRM_MOdel__c();
        model.Status__c = 'In Progress';
        model.Comments__c = 'Test Comments';
        insert model;
        
        //Create Site Activation for Model
        SRM_Model_Site_Activation__c siteBZ = new SRM_Model_Site_Activation__c();
        siteBZ.SRM_MOdel__c = model.Id;
        siteBZ.Country__c = country.Id;
        siteBZ.IRB_Type__c = 'Local IRB';
        siteBZ.Site_Activation_Formula__c = 'Gaussian';
        siteBZ.Number_Of_Weeks__c = 20;
        siteBZ.Amplitude__c = 100;
        siteBZ.Shift_Center_By__c = 1;
        siteBZ.Standard_Deviation__c = 1;
        siteBZ.Gaussian_Constant__c = 0;
        siteBZ.Start_Week__c = 1;
        siteBZ.Comments__c = 'Test Comment';
        insert siteBZ;
        
        //Create Subject Enrollment Rate records
        SRM_Model_Subject_Enrollment__c subEnroll = new SRM_MOdel_Subject_Enrollment__c();
        subEnroll.Country__c = country.Id;
        subEnroll.SRM_Model__c = model.Id;
        subEnroll.Subject_Enrollment_Rate__c = 2;
        insert subEnroll;
        
        //Create Calendar Adjustment records
        SRM_Calender_Adjustments__c cal = new SRM_Calender_Adjustments__c();
        cal.Country__c = country.Id;
        cal.SRM_Model__c = model.Id;
        cal.From_Date__c = Date.newInstance(2015,1,21);
        cal.To_Date__c = Date.newInstance(2015, 1, 22);
        System.debug('>>> sdtd >>> '+cal.From_Date__c  + ' >>> todt >>> '+cal.To_Date__c);
        cal.Site_Act_Adjustment__c = 100;
        cal.Subject_Enroll_Adjustment__c = -100;
        insert cal;
        
        //Create Week Adjustment
        SRM_Week_Adjustments__c week = new SRM_Week_Adjustments__c();
        week.Country__c = country.Id;
        week.SRM_Model__c = model.Id;
        week.Week_Of_Project__c = 15;
        week.Site_Act_Adjustment__c = 100;
        week.Subject_Enroll_Adjustment__c = 100;
        insert week;
        
        //Create question
        SRM_Model_Questions__c que1 = new SRM_Model_Questions__c();
        que1.SRM_Model__c = model.Id;
        que1.Question__c = 'This is test question';
        que1.Primary_Role__c = 'Proposal Director';
        que1.Group__c = 'Country';
        que1.Sub_Group__c = 'Site Activation';
        que1.Selecting_Country__c = 'Specific Countries'; 
        que1.Country__c = '';
        insert que1;
        
        SRM_Model_Response__c res1 = new SRM_Model_Response__c();
        res1.SRM_Question_ID__c = que1.Id;
        res1.Response__c = 'No';
        insert res1;               
        
        SRM_ModelCloneDataAccessor.ListofSRMModels(model.Id);
        SRM_ModelCloneDataAccessor.ListofSiteActivations(model);
        SRM_ModelCloneDataAccessor.ListofSubjectEnrollments(model);
        SRM_ModelCloneDataAccessor.ListofsrmQuestions(model);
        SRM_ModelCloneDataAccessor.ListofWeekadjustments(model);
        SRM_ModelCloneDataAccessor.Listofcalenderadjustments(model);                    
        SRM_ModelCloneDataAccessor.countries();
        SRM_ModelCloneDataAccessor.modelQuestions(que1.Id);
        
        list<Country__c> countries = SRM_ModelCloneDataAccessor.countries();
        System.assertEquals(1,countries.size());            
        
	}

}