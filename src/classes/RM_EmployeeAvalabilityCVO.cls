public with sharing class RM_EmployeeAvalabilityCVO {

	public AvailabilityView  pto {get; set;}
	public AvailabilityView  leave {get; set;}
	public AvailabilityView  nonBilable {get; set;}
	public AvailabilityView  preStarter {get; set;}
	public AvailabilityView  training {get; set;}
	public AvailabilityView  grossAvailability {get; set;}
	public AvailabilityView  totalAvailable {get; set;}
	public AvailabilityView  totalNonAvailable {get; set;}
	
	public RM_EmployeeAvalabilityCVO(integer startMonth, integer endMonth){	     
     
    
      grossAvailability = new AvailabilityView('Gross Availability', startMonth, endMonth);
      pto = new AvailabilityView('PTO', startMonth, endMonth);
      leave = new AvailabilityView('Leave (Non-PTO)', startMonth, endMonth);
      nonBilable = new AvailabilityView('Other Non Billable', startMonth, endMonth);
      preStarter = new AvailabilityView('Pre-Starter', startMonth, endMonth);
      training = new AvailabilityView('Training', startMonth, endMonth);
      totalAvailable = new AvailabilityView('Total Available', startMonth, endMonth);
      totalNonAvailable =  new AvailabilityView('Non-Billable', startMonth, endMonth);	
	}

	public class AvailabilityView{
        public String availType{get;  set;}  
        public List<string> lstFTE{get; set;} 
        public List<string> lstHr{get;set;}         
       
        public AvailabilityView(string avaType, integer startMonth, integer endMonth){
                
             availType = avaType;
            
             init(startMonth, endMonth);
            
        }
        private void init(integer startMonth, integer endMonth){
             //initialize month
            Integer iMonth = endMonth-startMonth + 1; 
            lstHr = new string[iMonth];
            lstFTE = new string[iMonth];
            for (Integer i = 0; i < iMonth; i++) {
              lstHr[i]='0.0';
              lstFTE[i]='0.00';
            }
        }        
	}
}