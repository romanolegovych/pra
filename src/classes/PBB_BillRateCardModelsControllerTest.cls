@isTest
private class PBB_BillRateCardModelsControllerTest {
	
	private static PBB_BillRateCardModelsController cont;
	
	@isTest
	static void PBB_BillRateCardModelsControllerTest() {
		Test.startTest();
    		cont = new PBB_BillRateCardModelsController();
    	Test.stopTest();
    	System.assert(cont.BRModelsList.size() == 0);
	}

	@isTest
	static void getNewPageReferenceTest() {
		cont = new PBB_BillRateCardModelsController();
		Test.startTest();
    		PageReference pr = cont.getNewPageReference();
    	Test.stopTest();
    	Schema.DescribeSObjectResult  r = Bill_Rate_Card_Model__c.SObjectType.getDescribe();
    	System.assertEquals(pr.getUrl(), '/'+r.getKeyPrefix()+'/e');
	}
}