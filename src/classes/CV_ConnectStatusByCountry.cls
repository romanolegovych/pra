@RestResource(urlMapping='/CV_ConnectStatusByCountry/*')
global with sharing class CV_ConnectStatusByCountry{
	
    @HttpPost
    global static StatusByCountryResult getStatusByCountry(String protocolUniqKey){
    	try{
	    	return new StatusByCountryResult( PC_Utils.getStatusByCountry(protocolUniqKey) );
        }catch(Exception e){
            return new StatusByCountryResult( e.getMessage() );
        }
    }
        
	global class StatusByCountryResult{
	    
	        global String errorMessage { get; private set; }
	        
	        global List<PRA_WrappersForMobile.WrapperCountry> returnedValues { get; private set; }
	        
	        public StatusByCountryResult(List<PRA_WrappersForMobile.WrapperCountry> returnedValues ){
	            this.returnedValues = returnedValues ;
	        }
	        
	        public StatusByCountryResult(String errorMessage){
	            this.errorMessage = errorMessage;
	        }
	}
}