/**
 * Test for PRAToCourse controller
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestPRAToCourseController {
	
	static List<Job_Class_Desc__c> jobClasses;
	static List<Job_Title__c> jobTitles;
	static List<PRA_Business_Unit__c> businessUnits;
	static List<Department__c> departments;
	static List<Region__c> regions;
	static List<Country__c> countries;
	static List<Employee_Type__c> empTypes;
	static List<Employee_Status__c> empStatus;
	static List<Role_Type__c> roleTypes;
	static List<LMS_Role__c> roles;
	static List<Employee_Details__c> employees;
	static List<Course_Domain__c> courseDomains;
	static List<LMS_Course__c> courses;
	static List<LMS_Role_Course__c> roleCourses;
	static List<LMSConstantSettings__c> constants;
	static List<CourseDomainSettings__c> domains;
	static List<RoleAdminServiceSettings__c> settings;
	
	static void init(){
    	
    	constants = new LMSConstantSettings__c[] {
        	new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
    		new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
    		new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
    		new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D'),
    		new LMSConstantSettings__c(Name = 'typePRA', Value__c = 'PRA'),
    		new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
    		new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
    		new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
    		new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
    		new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
    		new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
    		new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
    		new LMSConstantSettings__c(Name = 'statusToDelete', Value__c = 'Delete'),
    		new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended'),
    		new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
    		new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
    		new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
    		new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete')
    	};
    	insert constants;
    	
    	domains = new CourseDomainSettings__c[] {
    		new CourseDomainSettings__c(Name = 'Internal', Domain_Id__c = 'Internal'),
    		new CourseDomainSettings__c(Name = 'PST', Domain_Id__c = 'Project Specific'),
    		new CourseDomainSettings__c(Name = 'Archive', Domain_Id__c = 'Archive')
    	};
    	insert domains;
    	
    	settings = new RoleAdminServiceSettings__c[] {
    		new RoleAdminServiceSettings__c(Name = 'DOMAIN', Value__c = 'DOMAIN'),
    		new RoleAdminServiceSettings__c(Name = 'ENDPOINT', Value__c = 'ENDPOINT'),
    		new RoleAdminServiceSettings__c(Name = 'TIMEOUT', Value__c = '50000')
    	};
    	insert settings;
		
		jobClasses = new Job_Class_Desc__c[]{
			new Job_Class_Desc__c(Name = 'class1', Job_Class_Code__c = 'code1', Job_Class_ExtID__c = 'class1'),
			new Job_Class_Desc__c(Name = 'class2', Job_Class_Code__c = 'code2', Job_Class_ExtID__c = 'class2')
		};
		insert jobClasses;
		
		jobTitles = new Job_Title__c[]{
			new Job_Title__c(Job_Code__c = 'Code1', Job_Title__c = 'title1', Job_Class_Desc__c = jobClasses[0].Id, Status__c = 'A'),
			new Job_Title__c(Job_Code__c = 'Code2', Job_Title__c = 'title2', Job_Class_Desc__c = jobClasses[1].Id, Status__c = 'A'),
			new Job_Title__c(Job_Code__c = 'Code3', Job_Title__c = 'title3', Job_Class_Desc__c = jobClasses[1].Id, Status__c = 'A')
		};
		insert jobTitles;
		
		businessUnits = new PRA_Business_Unit__c[]{
			new PRA_Business_Unit__c(Name = 'unit', Business_Unit_Code__c = 'BUC', Status__c = 'A')
		};
		insert businessUnits;
		
		departments = new Department__c[]{
			new Department__c(Name = 'department', Department_Code__c = 'DEPTC', Status__c = 'A')
		};
		insert departments;
		
		regions = new Region__c[]{
			new Region__c(Region_Name__c = 'region1', Region_Id__c = 1, Status__c = 'A'),
			new Region__c(Region_Name__c = 'region2', Region_Id__c = 2, Status__c = 'A')
		};
		insert regions;
		
		countries = new Country__c[]{
			new Country__c(Name = 'country1', Region_Name__c = 'region1', Country_Code__c = 'C1', PRA_Country_ID__c = '100'),
			new Country__c(Name = 'country2', Region_Name__c = 'region2', Country_Code__c = 'C2', PRA_Country_ID__c = '200')
		};
		insert countries;
		
		empTypes = new Employee_Type__c[]{
			new Employee_Type__c(Name = 'type1'),
			new Employee_Type__c(Name = 'type2')
		};
		insert empTypes;
		
		empStatus = new Employee_Status__c[]{
			new Employee_Status__c(Employee_Status__c = 'AA', Employee_Type__c = empTypes[0].Id)
		};
		insert empStatus;
		
		roleTypes = new Role_Type__c[]{
			new Role_Type__c(Name = 'PRA'),
			new Role_Type__c(Name = 'Project Specific'),
			new Role_Type__c(Name = 'Additional Role')
		};
		insert roleTypes;
		
		courseDomains = new Course_Domain__c[]{
			new Course_Domain__c(Domain__c = 'Internal'),
			new Course_Domain__c(Domain__c = 'Archive'),
			new Course_Domain__c(Domain__c = 'Project Specific')
		};
		insert courseDomains;
				
		courses = new LMS_Course__c[]{
    		new LMS_Course__c(Offering_Template_No__c = 'course123', Course_Code__c = 'C123', Title__c = 'Course 123', Domain_Id__c = 'Internal',
    						  Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
    						  Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse1',
    						  Discontinued_From__c = Date.today()+1, Duration__c = 60),
    		new LMS_Course__c(Offering_Template_No__c = 'course321', Course_Code__c = 'C124', Title__c = 'Course 321', Domain_Id__c = 'Internal',
    						  Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
    						  Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse2',
    						  Discontinued_From__c = Date.today()+1, Duration__c = 60),
    		new LMS_Course__c(Offering_Template_No__c = 'course124', Course_Code__c = 'C234', Title__c = 'Course 234', Domain_Id__c = 'Internal',
    						  Type__c = 'SOP', Status__c = 'Retired', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
    						  Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse3',
    						  Discontinued_From__c = Date.today()+1, Duration__c = 60),
    		new LMS_Course__c(Offering_Template_No__c = 'course341', Course_Code__c = 'C231', Title__c = 'Course 432', Domain_Id__c = 'Internal',
    						  Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
    						  Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse4',
    						  Discontinued_From__c = Date.today()+1, Duration__c = 60)
    	};
    	insert courses;
	}
	
	static void initRoles(){
		roles = new LMS_Role__c[]{
			new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = jobTitles[0].Id, Business_Unit__c = businessUnits[0].Id,
							Department__c = departments[0].Id, Region__c = regions[0].Id, Country__c = countries[0].Id,
							Employee_Type__c = empTypes[0].Id, Status__c = 'Active', Sync_Status__c = 'Y', Role_Type__c = roleTypes[0].Id),
			new LMS_Role__c(Job_Class_Desc__c = jobClasses[1].Id, Job_Title__c = jobTitles[2].Id, Business_Unit__c = businessUnits[0].Id,
							Department__c = departments[0].Id, Region__c = regions[1].Id, Country__c = countries[1].Id,
							Employee_Type__c = empTypes[1].Id, Status__c = 'Active', Sync_Status__c = 'Y', Role_Type__c = roleTypes[0].Id),
			new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = jobTitles[0].Id, Business_Unit__c = businessUnits[0].Id,
							Department__c = departments[0].Id, Region__c = regions[0].Id, Country__c = countries[0].Id,
							Employee_Type__c = empTypes[1].Id, Status__c = 'Active', Sync_Status__c = 'Y', Role_Type__c = roleTypes[0].Id),
			new LMS_Role__c(Job_Class_Desc__c = null, Job_Title__c = null, Business_Unit__c = null,
							Department__c = null, Region__c = null, Country__c = null,
							Employee_Type__c = empTypes[0].Id, Status__c = 'Active', Sync_Status__c = 'Y', Role_Type__c = roleTypes[0].Id),
			new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = null, Business_Unit__c = businessUnits[0].Id,
							Department__c = null, Region__c = regions[0].Id, Country__c = null,
							Employee_Type__c = empTypes[0].Id, Status__c = 'Active', Sync_Status__c = 'Y', Role_Type__c = roleTypes[0].Id),
			new LMS_Role__c(Job_Class_Desc__c = null, Job_Title__c = null, Business_Unit__c = null,
							Department__c = departments[0].Id, Region__c = null, Country__c = null,
							Employee_Type__c = null, Status__c = 'Active', Sync_Status__c = 'Y', Role_Type__c = roleTypes[0].Id),
			new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = jobTitles[0].Id, Business_Unit__c = businessUnits[0].Id,
							Department__c = departments[0].Id, Region__c = regions[1].Id, Country__c = countries[1].Id,
							Employee_Type__c = empTypes[0].Id, Status__c = 'Active', Sync_Status__c = 'Y', Role_Type__c = roleTypes[0].Id)
		};
		insert roles;
	}
	
	static void initMappings(){
		roleCourses = new LMS_Role_Course__c[]{
			new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
			new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft Delete', Assigned_By__c = 'ASA'),
			new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
			new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Committed', Assigned_By__c = 'ASA')
		};
		insert roleCourses;
	}
	
	static void initEmployees() {    	
        employees = new Employee_Details__c[] {
            new Employee_Details__c(Name = 'employee1', Employee_Unique_Key__c = 'employee1', First_Name__c = 'Andrew', Last_Name__c = 'Allen', 
            	Email_Address__c = 'a@a.com', Job_Class_Desc__c = jobClasses[0].Name, Job_Code__c = jobTitles[0].Job_Code__c, 
            	Business_Unit_Desc__c = businessUnits[0].Name, Department__c = departments[0].Name, Country_Name__c = countries[0].Id, 
            	Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'), Business_Unit__c = 'RDU')
        };
        insert employees;
	}
	
    static testMethod void testConstructor() {
    	init(); 
    	String internalDomain = domains[0].Domain_Id__c;
    	LMS_PRAToCourseController c = new LMS_PRAToCourseController();
    	System.assert(c.courseFilter == 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
        	'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
        	'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')');
    }
    
    static testMethod void testJobClassLOV(){
		init();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.jobTitle = null;
		List<SelectOption> options = c.getJobFamilyList();
		System.assert(options.size() == 3);
		System.assert(options[1].getValue() == 'class1');		
	}
	
	static testMethod void testJobTitleLOV(){
		init();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.jobFamily = null;
		List<SelectOption> options = c.getJobTitleList();
		System.assert(options.size() == 4);
		
		c.jobFamily = 'class1';
		c.getTitleOnFamily();
		options = c.getJobTitleList();
		System.assert(options.size() == 2 && options[1].getValue() == 'title1');
		
		c.jobFamily = 'class2';
		c.getTitleOnFamily();
		options = c.getJobTitleList();
		System.assert(options.size() == 3 && options[1].getValue() == 'title2' && options[2].getValue() == 'title3');
	}
	
	static testMethod void testBusinessUnitLOV(){
		init();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.department = null;
		List<SelectOption> options = c.getBusinessUnitList();
		System.debug('-----------------------------------'+options[1].getValue());
		System.assert(options.size() == 2);
		
		c.department = 'department';
		options = c.getBusinessUnitList();
		System.assert(options.size() == 2);
		System.assert(options[1].getValue() == 'unit');
	}
	
	static testMethod void testDepartmentLOV(){
		init();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.businessUnit = null;
		List<SelectOption> options = c.getDepartmentList();
		System.debug('-----------------------------------'+options[1].getValue());
		System.assert(options.size() == 2);
		
		c.businessUnit = 'unit';
		//c.getDepartmentOnBU();
		options = c.getDepartmentList();
		System.assert(options.size() == 2);
		System.assert(options[1].getValue() == 'department');
	}
	
	static testMethod void testRegionLOV(){
		init();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		List<SelectOption> options = c.getRegionList();
		System.assert(options.size() == 3 && options[1].getValue() == 'region1');
	}
	
	static testMethod void testCountryLOV(){
		init();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		List<SelectOption> options = c.getCountryList();
		System.assert(options.size() == 3 && options[1].getValue() == 'country1');
		
		c.region = 'region2';
		c.getCountryOnRegion();
		options = c.getCountryList();
		System.assert(options.size() == 2 && options[1].getValue() == 'country2');
	}
	
	static testMethod void testEmployeeTypeLOV(){
		init();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		List<SelectOption> options = c.getTypesList();
		System.assert(options.size() == 3 && options[1].getValue() == 'Contractor' && options[2].getValue() == 'Employees');
	}
	
	static testMethod void testSearch(){
		init();
		initRoles();
		initMappings();
		initEmployees();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.courseText = courses[0].Title__c;
		c.search();
		System.debug('------------courseList-------------'+c.roles.size());
		System.assert(c.roles[0].assignmentStatus == 'Draft' && c.roles[1].assignmentStatus == 'Draft Delete');
		
		c.roles = null;
		c.courseText = courses[1].Title__c;
		c.search();
		System.assert(c.roles[1].assignmentStatus == 'Draft' && c.roles[0].assignmentStatus == 'Committed');
		
		c.roles = null;
		c.courseText = courses[2].Title__c;
		c.search();
		System.assert(c.roles == null);
		
		c.roles = null;
		c.courseText = 'new course';
		c.search();
		System.assert(c.roles == null);
	}
	
	static testMethod void testRoleSearch(){
		init();
		initRoles();
		initMappings();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		c.roleSearch();
		System.assert(c.selRoles.size() == 6);
		
		c.jobFamily = jobClasses[0].Name;
		c.roleSearch();
		System.assert(c.selRoles.size() == 4);
	}
	
	static testMethod void testRoleSearchByRoleName(){
		init();
		initRoles();
		initMappings();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		
		c.roleName = 'Test Failure Role Name';
		c.roleSearch();
		System.assert(c.selRoles.size() == 0 && 
			c.roleErrorText == 'No results found, role does not exist, is inactive, or is currently mapped to course. Please enter a different role name.');
	}
	
	static testMethod void testRoleSearchNoMappings(){
		init();
		initRoles();
		initMappings();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.courseText = courses[3].Title__c;
		c.search();
		System.assert(c.roles.size() == 0);
	}
	
	static testMethod void testRoleSearchFailure(){
		init();
		initRoles();
		initMappings();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		
		c.jobFamily = 'Job';
		c.roleSearch();
		System.assert(c.selRoles.size() == 0);
	}
	
	static testMethod void testAddRole(){
		init();
		initRoles();
		initMappings();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		
		c.jobFamily = jobClasses[0].Name;
		c.jobTitle = jobTitles[0].Job_Title__c;
		c.empType = empTypes[0].Name;
		c.roleSearch();
		
		c.selRoles[0].selected = true;
		PageReference pr = c.addRole();
		System.assert(pr == null);
	}
	
	static testMethod void testCancel(){
		init();
		initRoles();
		initMappings();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		
		PageReference pr = c.cancel();
		System.assert(pr == null);
	}
	
	static testMethod void testCommitRoles(){
		init();
		initRoles();
		initMappings();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		PageReference pr = c.commitRoles();
		System.assert(pr == null);
	}
	
	static testMethod void testApplyCommitDate(){
		init();
		initRoles();
		initMappings();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		
		c.roles[0].selected = true;
		c.commitDate = '12/12/2102';
		PageReference pr = c.setCommitDate();
		System.assert(pr == null);
	}
	
	static testMethod void testRemoveRole(){
		init();
		initRoles();
		initMappings();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		
		PageReference pr = c.removeRoles();
		System.assert(pr == null);
	}
	
	static testMethod void testCourseReset() {
		init();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.courseReset();
		System.assert(c.bWebServiceError == false && c.roles == null && c.selRoles == null);
	}
	
	static testMethod void testRoleReset() {
		init();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.roleReset();
		System.assert(c.selRoles == null);
	}
	
	static testMethod void testCloseCourseSearch() {
		init();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		c.closeCourseSearch();
		System.assert(c.selRoles == null);
	}
	
	static testMethod void testShowErrorPage() {
		init();
		LMS_PRAToCourseController c = new LMS_PRAToCourseController();
		PageReference newPage = c.showErrors();
		System.assert(newPage.getUrl() == '/apex/LMS_PRACourseError');
	}

}