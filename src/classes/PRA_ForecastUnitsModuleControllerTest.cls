@isTest(SeeAllData=false)
private class PRA_ForecastUnitsModuleControllerTest {
    
    public static Decimal TOTAL_UNITS = 48.00;
    public static Integer TOTAL_HOURS = 120;
    // Effort should be total hours / total units, assign it here until hours are getting populated
    public static Integer TOTAL_EFFORT = 2; 
    public static Client_Project__c selectedProject {get; set;}
    public static Project_Client_Region__c selectedRegion {get; set;}
    
    // Create test data    
    private static List<Client_Task__c> insertCRCTsByOA(Integer CRCTSize ) {
        
        // Insert Operational Area - Project Management only
        List <Operational_Area__c> liOA = new List<Operational_Area__c>();
        liOA.add(new Operational_Area__c(Name = 'Project Management', Description__c = 'Project Management'));
        insert liOA;
        
        // Task Groups (how ever many operational areas there are)
        List<Task_Group__c> liTG = new List<Task_Group__c>();
        for( Operational_Area__c oa : liOA ) {
            Task_Group__c tg = new Task_Group__c( Operational_Area__c = oa.id, Description__c = 'Project Managmeent' );
            liTG.add( tg );
        }
        insert liTG;
        
        // Projects (however many is specified in CRCTSize)
        Date start_date = Date.today();
        Date end_date = Date.today().addMonths( 12 );
        long projIDOffset = 12345678;
        
        List<Client_Project__c> liCP = new List<Client_Project__c>();           
        Client_Project__c cp = new Client_Project__c( contracted_start_date__c = start_date, contracted_end_date__c = end_date, 
                                    estimated_end_date__c = end_date, name = 'JNX-HP123234465' );
        cp.PRA_Project_ID__C = String.ValueOf( projIDOffset);
        liCP.add(cp);  
        insert liCP;
        selectedProject = liCP[0];
               
        // Insert Region
        List<Project_Client_Region__c> clientRegions = new List<Project_Client_Region__c>();
        clientRegions.add(new Project_Client_Region__c(Name = 'US/Canada'));
        clientRegions.add(new Project_Client_Region__c(Name = 'Asia/Pacific'));
        clientRegions.add(new Project_Client_Region__c(Name = 'Europe/Africe'));
        
        insert clientRegions;
        
        clientRegions = [Select Id, Name from Project_Client_Region__c];
        selectedRegion = clientRegions[0];
        
        // Insert Country
        List<Country__c> countries = new List<Country__c>();
        countries.add(new Country__c(Name = 'UK'));
        countries.add(new Country__c(Name = 'U.S.A'));
        countries.add(new Country__c(Name = 'Germany'));
        countries.add(new Country__c(Name = 'India'));
        insert countries;
        
        countries = [Select Id, Name from country__c];
        
        // Client task (however many is specified in CRCTSize)
        List<Client_Task__c> liCT = new List<Client_Task__c>();
        long clientTaskIDOffset = 251115000;
        for( Integer i = 0; i < CRCTSize; i++ ) {
            Client_Task__c ct = new Client_Task__c( project__c = liCP[0].id, task_group__c = liTG[0].id, PRA_Client_Task_ID__C = String.ValueOf( clientTaskIDOffset + i), Description__c = 'Client Task' + i );
            ct.Start_Date__c = start_date;
            ct.Original_End_date__c = end_date;
            ct.Forecast_Curve__c = 'Project Management';
            ct.Country__c = countries[Math.mod(i,3)].Id;
            ct.Project_Region__c = clientRegions[Math.mod(i,3)].Id;
            liCT.add( ct );
        }
        insert liCT;
        
        
        // client region country task (however many is specified in CRCTSize)
        /*List<Client_Task__c> liCRCT = new List<Client_Task__c>();
        for( Integer i = 0; i < CRCTSize; i++ ) {
            //system.debug('the i is: '+i);
            Client_Task__c crct = new Client_Task__c();
            //crct.Id = liCT[i].id;
            crct.Country__c = countries[Math.mod(i,3)].Id;
            crct.Project_Region__c = clientRegions[Math.mod(i,3)].Id;
            liCRCT.add( crct );
        }
        
        insert liCRCT;*/
        
        // INsertintg Bid contract to get FOrecast
        /*List<Bid_Contract__c> liBC = new List<Bid_Contract__c>();
        for( Client_Task__c crct : liCRCT ) {
            Bid_Contract__c bc = new Bid_Contract__c( total_units__c = TOTAL_UNITS, total_effort__c = TOTAL_EFFORT, total_hours__c = TOTAL_HOURS, Client_Task__c = crct.id );
            liBC.add( bc);
        }
        insert liBC;*/
        //PRA_BidTriggerHandler.OnBeforeInsert(liBC);
        //PRA_BidTriggerHandler.OnAfterInsert(liBC);
        
        return liCT;
    }
    
    
    // So, what should we test here folks?
    // Unlike some developers who only call controller methods to increase test coverage
    // Let me try and do something radical like actually testing the code functionality
    // Alright, let's dive in
    static testmethod void testConstructorBehaviour() {
        
        // Lets test the controller when there is a user preference
        // Let create a user to run the test 
        // Let us call the user Charlie's Antigels
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='sysadmin@testorg.com',
            emailencodingkey='UTF-8', lastname='Antigels', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='charlies.antigels@testorg.com');
        insert u;
        System.runAs(u) {
            // Using the excellent test utils class to create some test data
            PRA_TestUtils testUtil = new PRA_TestUtils();
            testUtil.initAll();
            // Insert a dummy user preference
            List<String> regions = new List<String>();
            for(Project_Client_Region__c region: testUtil.projectRegions.values()) {
                regions.add(region.Id);
            }
            List<String> operationalAreas = new List<String>();
            operationalAreas.add(testUtil.operationalArea.Id);
            
            List<String> selectedTasks = new List<String>();
            selectedTasks.add(testUtil.clientTask.Id);
            List<String> selectedGroups = new List<String>();
            selectedGroups.add(testUtil.taskGroup.Id);
            
            PRA_Utils.savePrefs( testUtil.clientProject.Id, regions, operationalAreas, selectedTasks, selectedGroups, false, false);
            PRA_ForecastUnitsModuleController forecastUnitsModuleController = new PRA_ForecastUnitsModuleController(); 
            // Assert if it's the same project
            System.assertEquals(forecastUnitsModuleController.selectedProjectName, testUtil.clientProject.Name);
            
            // Reset all filters
            forecastUnitsModuleController.reset();
            System.assertEquals(forecastUnitsModuleController.selectedProject, 'None');
            System.assertEquals(forecastUnitsModuleController.selectedOperationalAreas[0], 'None' );
            // Set the selected region to null
            forecastUnitsModuleController.selectedRegions = null;
            forecastUnitsModuleController.ctSearch.initRegionOptions();
            // Set the selectedOperationalArea to null
            forecastUnitsModuleController.selectedOperationalAreas = null;
            forecastUnitsModuleController.ctSearch.initOperationalAreaOptions();
            // Set the selectedgroup to null
            forecastUnitsModuleController.selectedGroups = null;
            forecastUnitsModuleController.ctSearch.initGroupOptions();
            
            // make it fail by saving it by name
            PRA_Utils.savePrefs( testUtil.clientProject.Name, regions, operationalAreas, selectedTasks, selectedGroups, false, false);
            PRA_ForecastUnitsModuleController forecastUnitsModuleController2 = new PRA_ForecastUnitsModuleController(); 
            // Assert if it's the same project
            System.assertEquals(forecastUnitsModuleController2.selectedProjectName, 'Home');
            
        }
    }
    
    static testmethod void testSearchTasksVF() {
        // Let create a user to run the test 
        // Let us call the user Charlie's Antigels
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='sysadmin@testorg.com',
            emailencodingkey='UTF-8', lastname='Antigels', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='charlies.antigels@testorg.com');
        insert u;
        System.runAs(u) {
            // Using the excellent test utils class to create some test data
            PRA_TestUtils testUtil = new PRA_TestUtils();
            testUtil.initAll();
            PRA_ForecastUnitsModuleController forecastUnitsModuleController = new PRA_ForecastUnitsModuleController();
            forecastUnitsModuleController.selectedProject = testUtil.clientProject.Id;
            forecastUnitsModuleController.selectProject();
            forecastUnitsModuleController.searchTasksVF();
            
            // check liWrappers size
            System.assertEquals(forecastUnitsModuleController.liWrappers.size(), 2);
            
            // Test the search project method
            List<Client_Project__c> clientProjects = [select Id, Name from Client_Project__c];
            System.assertEquals(clientProjects, PRA_ForecastUnitsModuleController.searchProject(testUtil.clientProject.Name));
        }
                
    }
    
    /*** Added by Andrew Allen ***/
    static testMethod void testSaveUpdatedUnits() {
        //Create user to test saving units
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='sysadmin@testorg.com',
            emailencodingkey='UTF-8', lastname='Antigels', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='charlies.antigels@testorg.com');
        insert u;
        System.runAs(u) {
            // Using the excellent test utils class to create some test data
            PRA_TestUtils testUtil = new PRA_TestUtils();
            testUtil.initAll();
            PRA_ForecastUnitsModuleController forecastUnitsModuleController = new PRA_ForecastUnitsModuleController();
            forecastUnitsModuleController.selectedProject = testUtil.clientProject.Id;
            forecastUnitsModuleController.selectProject();
            forecastUnitsModuleController.searchTasksVF();
            
            List<Client_Task__c> tasks = [select Start_Date__c from Client_Task__c order by Project__c];
            List<Unit_Effort__c> units = 
            [select Id, Forecast_Unit__c from Unit_Effort__c where Month_Applies_To__c = :tasks[1].Start_Date__c.addMonths(4).toStartOfMonth()
             order by Client_Task__c];
            
            // Update the unit value and prepare json string
            units[0].Forecast_Unit__c = 4.0;
            String recordId = units[0].Id;
            Decimal value = units[0].Forecast_Unit__c;
            String json = '[{"recordId":"' + recordId + '","value":"' + value + '"}]';
            
            // Send json string to save method
            PRA_ForecastUnitsModuleController.saveForecasts(json);
            Unit_Effort__c unit = [select Forecast_Unit__c from Unit_Effort__c where Id = :recordId];
            System.assertEquals(unit.Forecast_Unit__c, value);
        }
    }
    
    /*** Added by Andrew Allen ***/
    static testMethod void testGetGraphs() {
        //Create user to test saving units
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='sysadmin@testorg.com',
            emailencodingkey='UTF-8', lastname='Antigels', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='charlies.antigels@testorg.com');
        insert u;
        System.runAs(u) {
            // Using the excellent test utils class to create some test data
            PRA_TestUtils testUtil = new PRA_TestUtils();
            testUtil.initAll();
            PRA_ForecastUnitsModuleController forecastUnitsModuleController = new PRA_ForecastUnitsModuleController();
            forecastUnitsModuleController.selectedProject = testUtil.clientProject.Id;
            forecastUnitsModuleController.selectProject();
            forecastUnitsModuleController.searchTasksVF();
            
            List<Id> clientTaskIds = new List<Id>();
            List<Client_Task__c> tasks = [select Id from Client_Task__c];
            for(Client_Task__c cT : tasks) {
                clientTaskIds.add(cT.Id);
            }
            
            PRA_ForecastUnitsModuleController.getGraphs(clientTaskIds);
            PRA_ForecastUnitsModuleController.getDescriptions(clientTaskIds);
        }
    }
    
    /*** Added by Andrew Allen ***/
    static testMethod void testGetPreviousMonths() {
        //Create user to test saving units
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='sysadmin@testorg.com',
            emailencodingkey='UTF-8', lastname='Antigels', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='charlies.antigels@testorg.com');
        insert u;
        System.runAs(u) {
            // Using the excellent test utils class to create some test data
            PRA_TestUtils testUtil = new PRA_TestUtils();
            testUtil.initAll();
            PRA_ForecastUnitsModuleController forecastUnitsModuleController = new PRA_ForecastUnitsModuleController();
            forecastUnitsModuleController.selectedProject = testUtil.clientProject.Id;
            forecastUnitsModuleController.selectProject();
            forecastUnitsModuleController.searchTasksVF();
            
            List<Client_Task__c> tasks = [select Id from Client_Task__c];
            String taskId = tasks[0].Id;
            
            PRA_ForecastUnitsModuleController.getPreviousMonth(0,taskId);
        }
    }
    
    /*** Added by Andrew Allen ***/
    static testMethod void testSortUnitList() {
        //Create user to test saving units
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='sysadmin@testorg.com',
            emailencodingkey='UTF-8', lastname='Antigels', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='charlies.antigels@testorg.com');
        insert u;
        System.runAs(u) {
            // Using the excellent test utils class to create some test data
            PRA_TestUtils testUtil = new PRA_TestUtils();
            testUtil.initAll();
            PRA_ForecastUnitsModuleController forecastUnitsModuleController = new PRA_ForecastUnitsModuleController();
            forecastUnitsModuleController.selectedProject = testUtil.clientProject.Id;
            forecastUnitsModuleController.selectProject();
            forecastUnitsModuleController.searchTasksVF();
            
            List<Unit_Effort__c> units = [select Id, Forecast_Unit__c, Month_Applies_To__c from Unit_Effort__c order by Client_Task__c];
            
            PRA_ForecastUnitsModuleController.sortUnitList(units);
        }
    }
    
    /*** Added by Andrew Allen ***/
    static testMethod void testGetCategories() {
        //Create user to test saving units
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='sysadmin@testorg.com',
            emailencodingkey='UTF-8', lastname='Antigels', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='charlies.antigels@testorg.com');
        insert u;
        System.runAs(u) {
            // Using the excellent test utils class to create some test data
            String[] categoryArray = new String[]{'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'};
            System.assertEquals(categoryArray, PRA_ForecastUnitsModuleController.getCategories());
        }
    }
    
    // test the export to excel functionality
    static testmethod void testExportToExcel() {
        // Let create a user to run the test 
        // Let us call the user Charlie's Antigels
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='sysadmin@testorg.com',
            emailencodingkey='UTF-8', lastname='Antigels', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='charlies.antigels@testorg.com');
        insert u;
        System.runAs(u) {
            // Using the excellent test utils class to create some test data
            PRA_TestUtils testUtil = new PRA_TestUtils();
            testUtil.initAll();
            PRA_ForecastUnitsModuleController forecastUnitsModuleController = new PRA_ForecastUnitsModuleController();
            forecastUnitsModuleController.selectedProject = testUtil.clientProject.Id;
            forecastUnitsModuleController.selectProject();
            forecastUnitsModuleController.isFirstTime = true;
            forecastUnitsModuleController.exportToExcel();
            
            // check liWrappers size
            System.assertEquals(forecastUnitsModuleController.liWrappers.size(), 2);
        }   
    }
    
    // test the remote action next months
    // this should be fun
    static testmethod void testNextMonthsProperly() {
        // Let create a user to run the test 
        // Let us call the user Charlie's Antigels
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='sysadmin@testorg.com',
            emailencodingkey='UTF-8', lastname='Antigels', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='charlies.antigels@testorg.com');
        insert u;
        System.runAs(u) {
            // Using the excellent test utils class to create some test data
            PRA_TestUtils testUtil = new PRA_TestUtils();
            testUtil.initAll();
            PRA_ForecastUnitsModuleController forecastUnitsModuleController = new PRA_ForecastUnitsModuleController();
            forecastUnitsModuleController.selectedProject = testUtil.clientProject.Id;
            forecastUnitsModuleController.selectProject();
            forecastUnitsModuleController.isFirstTime = true;
            forecastUnitsModuleController.searchTasks();
            List<String> taskIdStrings = new List<String>();
            
            for(PRA_ForecastUnitsModuleController.TaskWrapper wrap: forecastUnitsModuleController.liWrappers) {
                taskIdStrings.add(wrap.taskId);
            }
            PRA_ForecastUnitsModuleController.nextMonths(0, taskIdStrings);
            
            // check liWrappers size
            System.assertEquals(forecastUnitsModuleController.liWrappers.size(), 2);
        }    
    }
    
    static testmethod void testNumberOfCRCTSReturnedOnSearch() {
        // Let create a user to run the test 
        // Let us call the user Charlie's Antigels
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='sysadmin@testorg.com',
            emailencodingkey='UTF-8', lastname='Antigels', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='charlies.antigels@testorg.com');
        insert u;
        System.runAs(u) {
            // Using the excellent test utils class to create some test data
            PRA_TestUtils testUtil = new PRA_TestUtils();
            testUtil.initAll();
        
            // Check the forecast units
            //String forecastType = 'Official Forecast';
            List<Unit_Effort__c> forecastUnits = database.query('select Id, Name, Forecast_Unit__c, Client_Task__c from Unit_Effort__c');
            system.assertEquals(24, forecastUnits.size()); 
            
            // Test for a project only
            PRA_ForecastUnitsModuleController forecastUnitsModuleController = new PRA_ForecastUnitsModuleController();
            forecastUnitsModuleController.selectedProject = testUtil.clientProject.Id;
            forecastUnitsModuleController.selectProject();
            forecastUnitsModuleController.isFirstTime = true;
            forecastUnitsModuleController.searchTasks();
            // Check the crct wrapper size
            system.assertEquals(2, forecastUnitsModuleController.liWrappers.size());
            
            // Apply more filters
            String reg = testUtil.projectRegions.get('EMEA').Id;
            forecastUnitsModuleController.selectedRegions = new List<String> {reg};
            forecastUnitsModuleController.searchTasks();
            // Check the crct wrapper size
            system.assertEquals(1, forecastUnitsModuleController.liWrappers.size());
        }
    }
    
    // Test the wtd units and total units value
    static testmethod void testWTDAndTotalUnits()  {
        // Let create a user to run the test 
        // Let us call the user Charlie's Antigels
        Profile p = [select id from profile where name='System Administrator'];
        User u = new User(alias = 'standt', email='sysadmin@testorg.com',
            emailencodingkey='UTF-8', lastname='Antigels', languagelocalekey='en_US',
            localesidkey='en_US', profileid = p.Id,
            timezonesidkey='America/Los_Angeles', username='charlies.antigels@testorg.com');
        insert u;
        System.runAs(u) {
            // Using the excellent test utils class to create some test data
            PRA_TestUtils testUtil = new PRA_TestUtils();
            testUtil.initAll();
        
            // Check the forecast units
            List<Unit_Effort__c> forecastUnits = database.query('select Id, Name, Forecast_Unit__c, Client_Task__c from Unit_Effort__c');
            system.assertEquals(24, forecastUnits.size()); 
            
            List<Client_Task__c> liTask = [Select Id, Total_Worked_Units__c, Total_Units__c from Client_Task__c];
            
            Map <ID, Decimal> bidContractVal = PRA_ForecastUnitsModuleController.getBidContractValuePerTask(liTask);
            for(Client_Task__c task: liTask) {
                System.assertEquals(24.00000000, task.Total_Worked_Units__c);
                System.assertEquals(TOTAL_UNITS, task.Total_Units__c);
                Decimal variationFromContact = PRA_ForecastUnitsModuleController.getVariantionFromContract(bidContractVal.get(task.Id), task.Total_Units__c) ;
                System.assertEquals(task.Total_Units__c - bidContractVal.get(task.Id), variationFromContact);
            }
        }
    }
    
    // Test the reset functionality
    static testmethod void testReset() {
        
        List<Client_Task__c> liTask = new List<Client_Task__c>();
        List<Unit_Effort__c> forecastUnits = new List<Unit_Effort__c>();
        
        test.startTest();
        liTask = InsertCRCTsByOA(  10 );
        test.stopTest();
    }
    
    
    // Duplicating the forecast and setting a Month created to M-1
    /*static void preparePreviousMonths(Id crctId){
        List<Unit_Effort__c> liUnits = [select Id, Client_Task__c, Client_Task__r.Cumulative_Forecast_Units__c, Month_Applies_To__c, 
                                       Month_Forecast_Created__c, Last_Approved_Unit__c from Unit_Effort__c where Client_Task__c=:crctId 
                                       ORDER BY Month_Applies_To__c];
        // We create one Unit for Month-1 as it wasnt part of the one created during Month
        //Unit_Effort__c un = liUnits[0].clone(false, true);
        Unit_Effort__c un = new Unit_Effort__c();
        un.Client_Task__c = crctId;
        un.Month_Applies_To__c = un.Month_Applies_To__c.addMonths(-1);
        un.Last_Approved_Unit__c = un.Last_Approved_Unit__c + 4;
        System.debug('-----------------Unit------------------'+un);
        insert un;
    }
    
    // Testing the nextMonths JS remoting method
    static testmethod void testNextMonths() {
        Test.startTest();
        List<Client_Task__c> liCRCT = insertCRCTsByOA(1);
        System.debug('------------------Client Task------------------' + liCRCT);
        preparePreviousMonths(liCRCT[0].id);
        List<PRA_ForecastUnitsModuleController.MonthValueWrapper> results = PRA_ForecastUnitsModuleController.getPreviousMonth(0, liCRCT[0].id);
        System.assertEquals(9, results.size());
        Decimal expectedValue; 
        List<Unit_Effort__c> unitEfforts = [select Last_Approved_Unit__c from Unit_Effort__c where Client_Task__c=:liCRCT[0].id 
                    and Month_Applies_To__c=:Date.today().toStartOfMonth()];
        for(Unit_Effort__c uE : unitEfforts) {
            expectedValue = uE.Last_Approved_Unit__c;
            // 2 empty months, month-1, current month => results[3] = current month value
            System.assertEquals(expectedValue, results[3].valueMonth);
        }        
        Test.stopTest();
    }
    
    // Added by Bhargav
    /*static testmethod void testSaveForecasts(){
        Test.startTest();
        PRA_ForecastUnitsModuleController.saveForecasts();
        Test.stopTest();
    }*/
    
}