public with sharing class PIT_LogCallTaskController {
    Public Task     upTask   {get;set;}
    Public Task     downTask {get;set;}
    public String   parentIssueStr;
    
    
    public PIT_LogCallTaskController(ApexPages.standardController stdCtr) {
        upTask   = new Task();
        downTask = new Task();
        try {
            String tmpStr   = system.currentPageReference().getParameters().get('retURL');
            parentIssueStr = (String.isBlank(tmpStr))? NULL : tmpStr.substring(1); // to avoid the '/' character
            if(parentIssueStr.length() > 18) {
                parentIssueStr = parentIssueStr.substring(0,18);
            }
            upTask.WhatId   = parentIssueStr;
            downTask.WhatId = parentIssueStr;
        } catch (exception e) {
            upTask   = new Task();
            downTask = new Task();
        }
        upTask.ownerid      = Userinfo.getUserId();
        downTask.ownerid    = Userinfo.getUserId();
        upTask.Subject      = 'Call';
        upTask.ActivityDate = DateTime.now().Date();
        upTask.Status       = 'Completed';
        upTask.Priority     = 'Normal'; 
    }   
    
    
    public PageReference Save() {
        try {
            upsert upTask;
            if(downTask.Subject!=NULL && downTask.Subject!='') {
                upsert downTask;
            }
            String tmpStr = system.currentPageReference().getParameters().get('retURL');
            return new PageReference(tmpStr);
        } catch (exception e) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to save the task. Please, try again.');
            ApexPages.addMessage(msg); 
            return ApexPages.currentPage();  
        } 
    }
}