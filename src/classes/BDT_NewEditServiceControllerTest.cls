@isTest
private class BDT_NewEditServiceControllerTest {

    static testMethod void myUnitTest() {
    	
    	ServiceCategory__c sc = new ServiceCategory__c(name = 'Test',code__c = '001.001');
        upsert sc;
    	
        Service__c s = new Service__c(name = 'Test', ServiceCategory__c = sc.id, IsDesignObject__c=true, SequenceNumber__c=0);
        upsert s;
        
        //mimicing Create New
   		Pagereference c = new Pagereference( system.Page.BDT_NewEditService.getUrl() );
   		c.getParameters().put('ServiceCategoryId', sc.Id);
   		Test.setCurrentPage(c);
   		
   		BDT_NewEditServiceController d= new BDT_NewEditServiceController();
   		
   		d.retrieveService();
              
        
        //mimic Edit
        Pagereference b = new Pagereference( system.Page.BDT_NewEditService.getUrl() );
        b.getParameters().Put('serviceCategoryId',sc.id);
        b.getParameters().Put('serviceId',s.id);
        Test.setCurrentPage(b);
        
        BDT_NewEditServiceController a= new BDT_NewEditServiceController();
        
        
        System.assertEquals(s.id,a.serviceID);
        
        //a.service = s;
        System.assertEquals(s.Name,a.service.Name);
        
        a.retrieveService();
        System.assertNotEquals(null,a.service);
        
        a.save();
        a.serviceDelete();
        System.assertEquals(true,a.service.BDTDeleted__c);
        
        System.assertEquals(false,a.cancel()==null);
        
        //a.serviceCategoryId = sc.id;
        a.retrieveService();
        System.assertNotEquals(null,a.service);
        
        
        System.assertNotEquals(null,a.serviceCategoryName);
        System.assertNotEquals(null,a.serviceCategoryId);
     
   		        
        //force an error
        b.getParameters().Put('ServiceID','a000');
        Test.setCurrentPage( b );
        a.retrieveService();
        System.assertNotEquals(null,a.service);
        
        
        
    }
}