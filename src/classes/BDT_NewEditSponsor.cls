public with sharing class BDT_NewEditSponsor {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Administration Sponsors
	public String sponsorId {get;set;}
	public Sponsor__c sponsor {get;set;}
	
	public BDT_NewEditSponsor(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Administration Sponsors');
		sponsorId =  System.CurrentPageReference().getParameters().get('sponsorId');
		
		if(sponsorId!=null && sponsorId!=''){
			try{
				sponsor = [Select b.Name, b.Abbreviation__c 
								From Sponsor__c b 
								where b.id = :sponsorId];
			}catch(QueryException e){
				sponsor = new Sponsor__c();
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Sponsor Id could not be found. ');
        		ApexPages.addMessage(msg); 
			}
		}else{
			sponsor = new Sponsor__c();
		}
	
	}
	
	public PageReference save(){
		upsert sponsor;
		PageReference sponsorPage = new PageReference(System.Page.BDT_Sponsors.getUrl());
		if(sponsor.id!=null){		
			sponsorPage.getParameters().put('sponsorId',sponsor.id);
		}
		sponsorPage.setRedirect(true);
		system.debug('sponsorPage: '+ sponsorPage);
		return sponsorPage;
	}
	
	public PageReference cancel(){
		PageReference sponsorPage = new PageReference(System.Page.BDT_Sponsors.getUrl());
		if(sponsor.id!=null){		
			sponsorPage.getParameters().put('sponsorId',sponsor.id);
		}
		sponsorPage.setRedirect(true);
		return sponsorPage;
	}
	
	public PageReference deleteSoft(){
		//set Sponsor to deleted
		
		// no softdelete on account
		List<Account> sponsorEntityPerSponsor = [Select a.Id, a.Client_Abbreviation__c, a.BillingStreet, a.BillingState, 
									a.BillingPostalCode, a.BillingCountry, a.BillingCity, a.name, 
									a.Sponsor__r.Abbreviation__c 
									From Account a  
									Where Sponsor__c=: sponsorId];
			
		for (Account a : sponsorEntityPerSponsor) {
			try {
				delete a;
			} catch (Exception e) {
				// delete failed, likely because of relations to other records
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Deletion failed with: '+e);
				ApexPages.addMessage(msg); 
			}
		}
		
		sponsor.BDTDeleted__c = true;
		update sponsor;
		
		PageReference sponsorPage = new PageReference(System.Page.BDT_Sponsors.getUrl());
		sponsorPage.setRedirect(true);
		return sponsorPage;
	}

}