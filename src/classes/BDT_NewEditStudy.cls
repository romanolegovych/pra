public with sharing class BDT_NewEditStudy {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;}
	public String studyId 					{get;set;}
	public String projectId 				{get;set;}
	public String customId 					{get;set;}
	public Study__c study 					{get;set;}
	public Client_Project__c singleProject	{get;set;}
	public List<String> selectedStudiesUserPrefences		{get; set;}
	private BDT_UserPreferenceService UserPreferences;
	public Boolean showDeleted{
		get {if (showDeleted == null) {
				showDeleted = false;
			}
			return showDeleted;
		}
		set;
	}
	
	public BDT_NewEditStudy(){
		
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Study');
		
		// USER PREFERENCES
		UserPreferences = new BDT_UserPreferenceService();
		projectId = UserPreferences.getPreference(BDT_UserPreferenceDataAccessor.PROJECT);
		
		studyId =  System.CurrentPageReference().getParameters().get('studyId');
		projectId = BDT_Utils.getPreference('SelectedProject');
		
			
		try{
			selectedStudiesUserPrefences = BDT_Utils.getPreference('SelectedStudies').split(':');
		}catch(QueryException e){
			selectedStudiesUserPrefences = new List<String>();
		}
				
		if(projectId!=null && projectId!=''){
			singleProject = BDT_Utils.getProject(projectId, showDeleted);
		}
				
		if(studyId!=null && studyId!=''){
			try{
				study = [Select s.Title__c, s.Status__c, s.Sponsor_Code__c, s.Protocol_Title__c, s.Project__c, s.Name, s.Legacy_Code__c, s.BDTDeleted__c, s.Code__c, s.Business_Unit__c, s.RegulatoryStatus__c 
								From Study__c s
								where s.id = :studyId
								and ((BDTDeleted__c  = :showDeleted) or (BDTDeleted__c  = false))];
			}catch(QueryException e){
				study = new Study__c();
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Study Id could not be found. ');
        		ApexPages.addMessage(msg); 
			}
		}else{
			if(projectId!=null && projectId!=''){
				study = new Study__c(Project__c = singleProject.id, code__c = getCodeNumbering());
			}
		}
	
	}
	
	public String getCodeNumbering(){
		String code = '';
			Study__c lastStudy;
			try{
				lastStudy = [Select Name, Project__c, Code__c From Study__c 
									where (Code__c != '' or Code__c != null) and Project__c = : singleProject.Id 
									order by Code__c desc limit 1];
			}catch(QueryException e){
				lastStudy = new Study__c();
			}
			
			if(lastStudy.Code__c!=null && lastStudy.Code__c!=''){
				String[] str = lastStudy.Code__c.split('-');
	
		  	Integer latestStudyN;			  		
			  	latestStudyN = 1 + Integer.valueOf(str[str.size()-1]);

			  	code = singleProject.code__c+'-'+BDT_Utils.lpad(String.valueOf(latestStudyN),2,'0');
			}else{
				
				code = singleProject.code__c+'-01';
			}
		return code;
	}
	
	public PageReference save(){
		
		if(study.id!=null){
			update study;
		}else{
			insert study;
			updateSelectedStudyUserPreferences();//oninsert add this new Study ID to the user preferences by default
		}
					
		PageReference singleprojectPage = new PageReference(System.Page.BDT_singleproject.getUrl());		
		singleprojectPage.setRedirect(true);		
		return singleprojectPage;
	}
	
	public void updateSelectedStudyUserPreferences(){
		
		UserPreferences.addStudyCodes(new Set<String>{String.valueOf(study.Id)});
		UserPreferences.saveUserPreferences();
		
		/*String selectedStudies = '';

		if(selectedStudiesUserPrefences.size()>0){
			for (String sw:selectedStudiesUserPrefences){
				if(sw!=null && sw!=''){			
					selectedStudies = selectedStudies + ':' + sw;
				}
			}
		}
		
		//the latest study gets added by default as a preference
		selectedStudies = selectedStudies + ':' + study.Id;	
		
		if (selectedStudies.length()>0) {
			BDT_Utils.setPreference('SelectedStudies', selectedStudies.substring(1));
		} else {
			BDT_Utils.setPreference('SelectedStudies', '');
		}*/
	}
	
	public PageReference cancel(){
		PageReference singleprojectPage = new PageReference(System.Page.BDT_singleproject.getUrl());		
		singleprojectPage.setRedirect(true);		
		return singleprojectPage;
	}
	
	public PageReference deleteSoft(){
		//set study to deleted
		study.BDTDeleted__c = true;		
		update study;
		PageReference singleprojectPage = new PageReference(System.Page.BDT_singleproject.getUrl());		
		singleprojectPage.setRedirect(true);		
		return singleprojectPage;
	}

}