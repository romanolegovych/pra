//Generated by wsdl2apex

public class LMS_RoleAdminService {
    public class personMapResponse {
        public String details;
        public LMS_RoleAdminService.error[] errors;
        public String personNo;
        public Boolean success;
        private String[] details_type_info = new String[]{'details','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] errors_type_info = new String[]{'errors','http://roleadmin.service.esb.pra.com/','error','0','-1','true'};
        private String[] personNo_type_info = new String[]{'personNo','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] success_type_info = new String[]{'success','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'details','errors','personNo','success'};
    }
    public class IRoleAdminServicePort {
        public String endpoint_x = 'http://localhost:55561/services/RoleAdminService';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://roleadmin.service.esb.pra.com/', 'LMS_RoleAdminService'};
        public LMS_RoleAdminService.roleEmployeeMessageHolder processRoleEmployeeAsync(LMS_RoleAdminService.rolesPersonMap[] rolesPersonMapList,LMS_RoleAdminService.personsRoleMap[] personsRoleMapList) {
            LMS_RoleAdminService.processRoleEmployeeAsync request_x = new LMS_RoleAdminService.processRoleEmployeeAsync();
            LMS_RoleAdminService.processRoleEmployeeAsyncResponse response_x;
            request_x.rolesPersonMapList = rolesPersonMapList;
            request_x.personsRoleMapList = personsRoleMapList;
            Map<String, LMS_RoleAdminService.processRoleEmployeeAsyncResponse> response_map_x = new Map<String, LMS_RoleAdminService.processRoleEmployeeAsyncResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://roleadmin.service.esb.pra.com/',
              'processRoleEmployeeAsync',
              'http://roleadmin.service.esb.pra.com/',
              'processRoleEmployeeAsyncResponse',
              'LMS_RoleAdminService.processRoleEmployeeAsyncResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.roleEmployeeResponse;
        }
        public LMS_RoleAdminService.personRoleResponse processRoleEmployeeAssociation(LMS_RoleAdminService.rolesPersonMap[] rolesPersonMapList,LMS_RoleAdminService.personsRoleMap[] personsRoleMapList) {
            LMS_RoleAdminService.processRoleEmployeeAssociation request_x = new LMS_RoleAdminService.processRoleEmployeeAssociation();
            LMS_RoleAdminService.processRoleEmployeeAssociationResponse response_x;
            request_x.rolesPersonMapList = rolesPersonMapList;
            request_x.personsRoleMapList = personsRoleMapList;
            Map<String, LMS_RoleAdminService.processRoleEmployeeAssociationResponse> response_map_x = new Map<String, LMS_RoleAdminService.processRoleEmployeeAssociationResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://roleadmin.service.esb.pra.com/',
              'processRoleEmployeeAssociation',
              'http://roleadmin.service.esb.pra.com/',
              'processRoleEmployeeAssociationResponse',
              'LMS_RoleAdminService.processRoleEmployeeAssociationResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.roleEmployeeResponse;
        }
        public LMS_RoleAdminService.courseRoleResponse processRoleCourseAssociation(LMS_RoleAdminService.rolesCourseMap[] rolesCourseMapList,LMS_RoleAdminService.coursesRoleMap[] coursesRoleMapList) {
            LMS_RoleAdminService.processRoleCourseAssociation request_x = new LMS_RoleAdminService.processRoleCourseAssociation();
            LMS_RoleAdminService.processRoleCourseAssociationResponse response_x;
            request_x.rolesCourseMapList = rolesCourseMapList;
            request_x.coursesRoleMapList = coursesRoleMapList;
            Map<String, LMS_RoleAdminService.processRoleCourseAssociationResponse> response_map_x = new Map<String, LMS_RoleAdminService.processRoleCourseAssociationResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://roleadmin.service.esb.pra.com/',
              'processRoleCourseAssociation',
              'http://roleadmin.service.esb.pra.com/',
              'processRoleCourseAssociationResponse',
              'LMS_RoleAdminService.processRoleCourseAssociationResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.roleCourseResponse;
        }
        public LMS_RoleAdminService.roleResponse processRoles(LMS_RoleAdminService.role[] roles) {
            LMS_RoleAdminService.processRoles request_x = new LMS_RoleAdminService.processRoles();
            LMS_RoleAdminService.processRolesResponse response_x;
            request_x.roles = roles;
            Map<String, LMS_RoleAdminService.processRolesResponse> response_map_x = new Map<String, LMS_RoleAdminService.processRolesResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              '',
              'http://roleadmin.service.esb.pra.com/',
              'processRoles',
              'http://roleadmin.service.esb.pra.com/',
              'processRolesResponse',
              'LMS_RoleAdminService.processRolesResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.roleResponse;
        }
    }
    public class processRoleEmployeeAssociation {
        public LMS_RoleAdminService.rolesPersonMap[] rolesPersonMapList;
        public LMS_RoleAdminService.personsRoleMap[] personsRoleMapList;
        private String[] rolesPersonMapList_type_info = new String[]{'rolesPersonMapList','http://roleadmin.service.esb.pra.com/','rolesPersonMap','0','-1','false'};
        private String[] personsRoleMapList_type_info = new String[]{'personsRoleMapList','http://roleadmin.service.esb.pra.com/','personsRoleMap','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'rolesPersonMapList','personsRoleMapList'};
    }
    public class rolesCourseResponseItem {
        public String courseId;
        public String details;
        public LMS_RoleAdminService.error[] errors;
        public LMS_RoleAdminService.roleMapResponse[] roleMapResponses;
        public Boolean success;
        private String[] courseId_type_info = new String[]{'courseId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] details_type_info = new String[]{'details','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] errors_type_info = new String[]{'errors','http://roleadmin.service.esb.pra.com/','error','0','-1','true'};
        private String[] roleMapResponses_type_info = new String[]{'roleMapResponses','http://roleadmin.service.esb.pra.com/','roleMapResponse','0','-1','true'};
        private String[] success_type_info = new String[]{'success','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'courseId','details','errors','roleMapResponses','success'};
    }
    public class processRoleEmployeeAsync {
        public LMS_RoleAdminService.rolesPersonMap[] rolesPersonMapList;
        public LMS_RoleAdminService.personsRoleMap[] personsRoleMapList;
        private String[] rolesPersonMapList_type_info = new String[]{'rolesPersonMapList','http://roleadmin.service.esb.pra.com/','rolesPersonMap','0','-1','false'};
        private String[] personsRoleMapList_type_info = new String[]{'personsRoleMapList','http://roleadmin.service.esb.pra.com/','personsRoleMap','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'rolesPersonMapList','personsRoleMapList'};
    }
    public class personRoleResponse {
        public String details;
        public LMS_RoleAdminService.error[] errors;
        public LMS_RoleAdminService.personsRoleResponseItem[] personsRoleResponseItems;
        public LMS_RoleAdminService.rolesPersonResponseItem[] rolesPersonResponseItems;
        public Boolean success;
        private String[] details_type_info = new String[]{'details','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] errors_type_info = new String[]{'errors','http://roleadmin.service.esb.pra.com/','error','0','-1','true'};
        private String[] personsRoleResponseItems_type_info = new String[]{'personsRoleResponseItems','http://roleadmin.service.esb.pra.com/','personsRoleResponseItem','0','-1','true'};
        private String[] rolesPersonResponseItems_type_info = new String[]{'rolesPersonResponseItems','http://roleadmin.service.esb.pra.com/','rolesPersonResponseItem','0','-1','true'};
        private String[] success_type_info = new String[]{'success','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'details','errors','personsRoleResponseItems','rolesPersonResponseItems','success'};
    }
    public class role {
        public String name;
        public String domain;
        public String description;
        public String id;
        public String roleid;
        public String status;
        public String type_x;
        public String custom3;
        public String custom4;
        public String custom5;
        public String custom6;
        public String custom7;
        public String custom8;
        public String custom9;
        private String[] name_type_info = new String[]{'name','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] domain_type_info = new String[]{'domain','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] description_type_info = new String[]{'description','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] id_type_info = new String[]{'id','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] roleid_type_info = new String[]{'roleid','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] status_type_info = new String[]{'status','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] type_x_type_info = new String[]{'type','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] custom3_type_info = new String[]{'custom3','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] custom4_type_info = new String[]{'custom4','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] custom5_type_info = new String[]{'custom5','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] custom6_type_info = new String[]{'custom6','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] custom7_type_info = new String[]{'custom7','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] custom8_type_info = new String[]{'custom8','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] custom9_type_info = new String[]{'custom9','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'name','domain','description','id','roleid','status','type_x','custom3','custom4','custom5','custom6','custom7','custom8','custom9'};
    }
    public class personsRoleMap {
        public String action;
        public String roleId;
        public String[] personNos;
        private String[] action_type_info = new String[]{'action','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] roleId_type_info = new String[]{'roleId','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] personNos_type_info = new String[]{'personNos','http://www.w3.org/2001/XMLSchema','string','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'action','roleId','personNos'};
    }
    public class rolesPersonResponseItem {
        public String details;
        public LMS_RoleAdminService.error[] errors;
        public String personNo;
        public LMS_RoleAdminService.roleMapResponse[] roleMapResponses;
        public Boolean success;
        private String[] details_type_info = new String[]{'details','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] errors_type_info = new String[]{'errors','http://roleadmin.service.esb.pra.com/','error','0','-1','true'};
        private String[] personNo_type_info = new String[]{'personNo','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] roleMapResponses_type_info = new String[]{'roleMapResponses','http://roleadmin.service.esb.pra.com/','roleMapResponse','0','-1','true'};
        private String[] success_type_info = new String[]{'success','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'details','errors','personNo','roleMapResponses','success'};
    }
    public class roleResponse {
        public String details;
        public LMS_RoleAdminService.error[] errors;
        public LMS_RoleAdminService.roleResponseItem[] responseItems;
        public Boolean success;
        private String[] details_type_info = new String[]{'details','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] errors_type_info = new String[]{'errors','http://roleadmin.service.esb.pra.com/','error','0','-1','true'};
        private String[] responseItems_type_info = new String[]{'responseItems','http://roleadmin.service.esb.pra.com/','roleResponseItem','0','-1','true'};
        private String[] success_type_info = new String[]{'success','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'details','errors','responseItems','success'};
    }
    public class processRoleEmployeeAssociationResponse {
        public LMS_RoleAdminService.personRoleResponse roleEmployeeResponse;
        private String[] roleEmployeeResponse_type_info = new String[]{'roleEmployeeResponse','http://roleadmin.service.esb.pra.com/','personRoleResponse','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'roleEmployeeResponse'};
    }
    public class rolesPersonMap {
        public String action;
        public String personNo;
        public String[] rolesIds;
        private String[] action_type_info = new String[]{'action','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] personNo_type_info = new String[]{'personNo','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] rolesIds_type_info = new String[]{'rolesIds','http://www.w3.org/2001/XMLSchema','string','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'action','personNo','rolesIds'};
    }
    public class error {
        public String code;
        public String message;
        private String[] code_type_info = new String[]{'code','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] message_type_info = new String[]{'message','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'code','message'};
    }
    public class roleEmployeeMessageHolder {
        public LMS_RoleAdminService.rolesPersonMap[] rolesPersonMapList;
        public LMS_RoleAdminService.personsRoleMap[] personsRoleMapList;
        public LMS_RoleAdminService.personRoleResponse personRoleResponse;
        private String[] rolesPersonMapList_type_info = new String[]{'rolesPersonMapList','http://roleadmin.service.esb.pra.com/','rolesPersonMap','0','-1','true'};
        private String[] personsRoleMapList_type_info = new String[]{'personsRoleMapList','http://roleadmin.service.esb.pra.com/','personsRoleMap','0','-1','true'};
        private String[] personRoleResponse_type_info = new String[]{'personRoleResponse','http://roleadmin.service.esb.pra.com/','personRoleResponse','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'rolesPersonMapList','personsRoleMapList','personRoleResponse'};
    }
    public class courseRoleResponse {
        public LMS_RoleAdminService.coursesRoleResponseItem[] coursesRoleResponseItems;
        public String details;
        public LMS_RoleAdminService.error[] errors;
        public LMS_RoleAdminService.rolesCourseResponseItem[] rolesCourseResponseItems;
        public Boolean success;
        private String[] coursesRoleResponseItems_type_info = new String[]{'coursesRoleResponseItems','http://roleadmin.service.esb.pra.com/','coursesRoleResponseItem','0','-1','true'};
        private String[] details_type_info = new String[]{'details','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] errors_type_info = new String[]{'errors','http://roleadmin.service.esb.pra.com/','error','0','-1','true'};
        private String[] rolesCourseResponseItems_type_info = new String[]{'rolesCourseResponseItems','http://roleadmin.service.esb.pra.com/','rolesCourseResponseItem','0','-1','true'};
        private String[] success_type_info = new String[]{'success','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'coursesRoleResponseItems','details','errors','rolesCourseResponseItems','success'};
    }
    public class courseMapResponse {
        public String courseId;
        public String details;
        public LMS_RoleAdminService.error[] errors;
        public Boolean success;
        private String[] courseId_type_info = new String[]{'courseId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] details_type_info = new String[]{'details','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] errors_type_info = new String[]{'errors','http://roleadmin.service.esb.pra.com/','error','0','-1','true'};
        private String[] success_type_info = new String[]{'success','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'courseId','details','errors','success'};
    }
    public class processRoleCourseAssociation {
        public LMS_RoleAdminService.rolesCourseMap[] rolesCourseMapList;
        public LMS_RoleAdminService.coursesRoleMap[] coursesRoleMapList;
        private String[] rolesCourseMapList_type_info = new String[]{'rolesCourseMapList','http://roleadmin.service.esb.pra.com/','rolesCourseMap','0','-1','false'};
        private String[] coursesRoleMapList_type_info = new String[]{'coursesRoleMapList','http://roleadmin.service.esb.pra.com/','coursesRoleMap','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'rolesCourseMapList','coursesRoleMapList'};
    }
    public class roleResponseItem {
        public String details;
        public LMS_RoleAdminService.error[] errors;
        public String roleId;
        public Boolean success;
        private String[] details_type_info = new String[]{'details','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] errors_type_info = new String[]{'errors','http://roleadmin.service.esb.pra.com/','error','0','-1','true'};
        private String[] roleId_type_info = new String[]{'roleId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] success_type_info = new String[]{'success','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'details','errors','roleId','success'};
    }
    public class roleMapResponse {
        public String details;
        public LMS_RoleAdminService.error[] errors;
        public String roleId;
        public Boolean success;
        private String[] details_type_info = new String[]{'details','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] errors_type_info = new String[]{'errors','http://roleadmin.service.esb.pra.com/','error','0','-1','true'};
        private String[] roleId_type_info = new String[]{'roleId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] success_type_info = new String[]{'success','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'details','errors','roleId','success'};
    }
    public class coursesRoleResponseItem {
        public LMS_RoleAdminService.courseMapResponse[] coursesMapResponses;
        public String details;
        public LMS_RoleAdminService.error[] errors;
        public String roleId;
        public Boolean success;
        private String[] coursesMapResponses_type_info = new String[]{'coursesMapResponses','http://roleadmin.service.esb.pra.com/','courseMapResponse','0','-1','true'};
        private String[] details_type_info = new String[]{'details','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] errors_type_info = new String[]{'errors','http://roleadmin.service.esb.pra.com/','error','0','-1','true'};
        private String[] roleId_type_info = new String[]{'roleId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] success_type_info = new String[]{'success','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'coursesMapResponses','details','errors','roleId','success'};
    }
    public class rolesCourseMap {
        public String action;
        public String courseId;
        public String[] rolesIds;
        private String[] action_type_info = new String[]{'action','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] courseId_type_info = new String[]{'courseId','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] rolesIds_type_info = new String[]{'rolesIds','http://www.w3.org/2001/XMLSchema','string','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'action','courseId','rolesIds'};
    }
    public class processRoles {
        public LMS_RoleAdminService.role[] roles;
        private String[] roles_type_info = new String[]{'roles','http://roleadmin.service.esb.pra.com/','role','0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'roles'};
    }
    public class processRoleEmployeeAsyncResponse {
        public LMS_RoleAdminService.roleEmployeeMessageHolder roleEmployeeResponse;
        private String[] roleEmployeeResponse_type_info = new String[]{'roleEmployeeResponse','http://roleadmin.service.esb.pra.com/','roleEmployeeMessageHolder','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'roleEmployeeResponse'};
    }
    public class coursesRoleMap {
        public String action;
        public String roleId;
        public String[] courseIds;
        private String[] action_type_info = new String[]{'action','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] roleId_type_info = new String[]{'roleId','http://www.w3.org/2001/XMLSchema','string','1','1','false'};
        private String[] courseIds_type_info = new String[]{'courseIds','http://www.w3.org/2001/XMLSchema','string','1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'action','roleId','courseIds'};
    }
    public class personsRoleResponseItem {
        public String details;
        public LMS_RoleAdminService.error[] errors;
        public LMS_RoleAdminService.personMapResponse[] personMapResponses;
        public String roleId;
        public Boolean success;
        private String[] details_type_info = new String[]{'details','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] errors_type_info = new String[]{'errors','http://roleadmin.service.esb.pra.com/','error','0','-1','true'};
        private String[] personMapResponses_type_info = new String[]{'personMapResponses','http://roleadmin.service.esb.pra.com/','personMapResponse','0','-1','true'};
        private String[] roleId_type_info = new String[]{'roleId','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] success_type_info = new String[]{'success','http://www.w3.org/2001/XMLSchema','boolean','1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'details','errors','personMapResponses','roleId','success'};
    }
    public class processRoleCourseAssociationResponse {
        public LMS_RoleAdminService.courseRoleResponse roleCourseResponse;
        private String[] roleCourseResponse_type_info = new String[]{'roleCourseResponse','http://roleadmin.service.esb.pra.com/','courseRoleResponse','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'roleCourseResponse'};
    }
    public class processRolesResponse {
        public LMS_RoleAdminService.roleResponse roleResponse;
        private String[] roleResponse_type_info = new String[]{'roleResponse','http://roleadmin.service.esb.pra.com/','roleResponse','0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://roleadmin.service.esb.pra.com/','false','false'};
        private String[] field_order_type_info = new String[]{'roleResponse'};
    }
}