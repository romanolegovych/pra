@isTest
private class FlowchartAssignmentDataAccessorTest {

    static testMethod void myUnitTest() {
        // create test data
        ClinicalDesign__c cd = new ClinicalDesign__c(name='1');
        insert cd;

        list<sobject> aList = new list<sobject>();
        aList.add(new arm__c(ClinicalDesign__c = cd.id, Arm_Number__c = 1, Description__c = 'Arm 1')); //0
        aList.add(new arm__c(ClinicalDesign__c = cd.id, Arm_Number__c = 2, Description__c = 'Arm 2')); //1
        aList.add(new arm__c(ClinicalDesign__c = cd.id, Arm_Number__c = 3, Description__c = 'Arm 3')); //2
        aList.add(new Epoch__c(ClinicalDesign__c = cd.id, Epoch_Number__c = 1, Description__c = 'Epoch 1')); //3
        aList.add(new Epoch__c(ClinicalDesign__c = cd.id, Epoch_Number__c = 2, Description__c = 'Epoch 2')); //4
        aList.add(new Epoch__c(ClinicalDesign__c = cd.id, Epoch_Number__c = 3, Description__c = 'Epoch 3')); //5
        aList.add(new flowcharts__c(ClinicalDesign__c = cd.id, Flowchart_Number__c = 1, Description__c = 'Flowchart 1')); //6
        aList.add(new flowcharts__c(ClinicalDesign__c = cd.id, Flowchart_Number__c = 2, Description__c = 'Flowchart 2')); //7
        aList.add(new flowcharts__c(ClinicalDesign__c = cd.id, Flowchart_Number__c = 3, Description__c = 'Flowchart 3')); //8
        insert aList;

        list<FlowchartAssignment__c> fList = new list<FlowchartAssignment__c>();
        fList.add(new FlowchartAssignment__c(
        	Arm__c 			= (Id)(aList[0].get('ID')), 
        	Epoch__c 		= (Id)(aList[3].get('ID')), 
        	Flowchart__c 	= (Id)(aList[6].get('ID'))
        ));
        fList.add(new FlowchartAssignment__c(
        	Arm__c 			= (Id)(aList[0].get('ID')), 
        	Epoch__c 		= (Id)(aList[4].get('ID')), 
        	Flowchart__c 	= (Id)(aList[6].get('ID'))
        ));
        fList.add(new FlowchartAssignment__c(
        	Arm__c 			= (Id)(aList[0].get('ID')), 
        	Epoch__c 		= (Id)(aList[5].get('ID')), 
        	Flowchart__c 	= (Id)(aList[7].get('ID'))
        ));
        insert fList;
        
        //test retrieve logic
        fList = FlowchartAssignmentDataAccessor.getFlowchartAssignmentList('Arm__r.ClinicalDesign__c = \'' + cd.id + '\'');
        system.assertEquals(3,fList.size());
        
    }
}