@isTest
private class BDT_NewEditDesignTest {
	
	public static Client_Project__c firstProject {get;set;}
	
	static void init(){
		firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;
		
		BDT_Utils.setPreference('SelectedProject', firstProject.id);
		
	}
	
	static testMethod void createDesignPath() { 
		init();
		
		
		BDT_NewEditDesign newEditDesign = new BDT_NewEditDesign();
		newEditDesign.design.Name = 'New Design';
		newEditDesign.design.Design_Name__c = 'My Design';
		insert newEditDesign.design;
		
		System.assertNotEquals(null, newEditDesign.save());		
		
		BDT_NewEditDesign deleteDesign = new BDT_NewEditDesign();
		System.assertNotEquals(null, deleteDesign.save());
	
	}
	
	
	static testMethod void createDesignWithItemsPath() { 
		init();
		
		BDT_NewEditDesign newEditDesign = new BDT_NewEditDesign();
		newEditDesign.design.Name = 'New Design';
		newEditDesign.design.Design_Name__c = 'My Design';
		
		System.assertNotEquals(null, newEditDesign.save());
		System.assertNotEquals(null, newEditDesign.design.Id);
		
		for(integer i=0; i<3; i++){
			newEditDesign.addArm();
			newEditDesign.armList[i].Description__c = 'TestArm'+i;
			newEditDesign.armList[i].Arm_Number__c = i+1;
			system.debug('Arm: '+newEditDesign.armList[i]);
		}
		system.debug('armList:'+ newEditDesign.armList);
		
		for(integer i=0; i<6; i++){
			newEditDesign.addEpoch();
			newEditDesign.epochList[i].Description__c = 'TestEpoch'+i;
			newEditDesign.epochList[i].Epoch_Number__c = i+1;
			system.debug('Epoch: '+ newEditDesign.epochList[i]);
		}
		for(integer i=0; i<6; i++){
			newEditDesign.addFlowchart();
			newEditDesign.flowchartList[i].Description__c = 'TestFC'+i;
			newEditDesign.flowchartList[i].Flowchart_Number__c = i+1;
		}
		
		newEditDesign.save();
		
		
		PageReference b = new PageReference(system.Page.BDT_NewEditDesign.getUrl() );
		
		b.getParameters().put('deleteArmId', newEditDesign.armList[2].Id);
		b.getParameters().put('deleteEpochId', newEditDesign.epochList[5].Id);
		b.getParameters().put('deleteFlowchartId', newEditDesign.flowchartList[5].Id);
		Test.setCurrentPage( b );
		
		
		newEditDesign.deleteArm();
		newEditDesign.deleteEpoch();
		newEditDesign.deleteFlowchart();
		newEditDesign.save();
		newEditDesign.JSONsave();
		newEditDesign.JSONload();
		
		system.assertNotEquals(null, newEditDesign.deleteD());  //class will not return a deleteDesign link because there are still arms, epochs and flowcharts associated		
		
		system.assertNotEquals(null,newEditDesign.cancel());
		
		system.assertNotEquals(null, newEditDesign.copy());
	}
	

}