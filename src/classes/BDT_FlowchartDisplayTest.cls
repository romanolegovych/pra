@isTest
private class BDT_FlowchartDisplayTest {
	
	public static Client_Project__c firstProject 		{get;set;}
	public static List<Sponsor__c> mySponsorList 		{get;set;}
	public static List<Account> mySponsorEntityList  	{get;set;}
	public static List<Business_Unit__c> myBUList  		{get;set;}
	public static List<Study__c> stydiesList 			{get;set;}	
	public static List<Site__c> mySiteList  			{get;set;}
	public static List<ClinicalDesign__c> designList 	{get;set;}
	public static List<Epoch__c> 	epochList 			{get;set;}
	public static List<Arm__c> armList					{get;set;}
	public static List<Flowcharts__c> FlowchartList		{get;set;}
	public static List<flowchartassignment__c> flowchartAss {get;set;}
	
	static void init(){		
		
		mySponsorList = BDT_TestDataUtils.buildSponsor(1);		
		insert mySponsorList;
		
		mySponsorEntityList = BDT_TestDataUtils.buildSponsorEntity(mySponsorList);
		insert mySponsorEntityList;
				
		myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert myBUList;
		
		firstProject = BDT_TestDataUtils.buildProject();
		firstProject.Client__c = mySponsorEntityList[0].Id;
		insert firstProject;
		
		mySiteList = BDT_TestDataUtils.buildSite(myBUList);
		insert mySiteList;
		
		stydiesList = BDT_TestDataUtils.buildStudies(firstProject);
		stydiesList[0].Business_Unit__c = myBUList[0].Id; 
		stydiesList[1].Business_Unit__c = myBUList[1].Id; 
		stydiesList[2].Business_Unit__c = myBUList[2].Id;
		stydiesList[3].Business_Unit__c = myBUList[3].Id; 
		stydiesList[4].Business_Unit__c = myBUList[4].Id;
		insert stydiesList;
		
		designList = BDT_TestDataUtils.buildClinicDesign(firstProject); 
		insert designList;		
				 
		flowchartList = BDT_TestDataUtils.buildFlowchart(designList[0].id);
		insert flowchartList;
				
		BDT_Utils.setPreference('SelectedProject', firstProject.id);
		BDT_Utils.setPreference('SelectedStudies',stydiesList[0].id);
		//ApexPages.currentPage().getParameters().put('designId',designList[0].id);
		
		armList = BDT_TestDataUtils.buildArms(designList,3);			 				  
		insert armList;
		
		EpochList = BDT_TestDataUtils.buildEpochs(designList,1);
		insert EpochList;
		
		flowchartAss = BDT_TestDataUtils.buildFlowchartAssignment(designList, flowchartList, armList, EpochList);
		insert flowchartAss;
	}
	
	static testMethod void createFlowchartDesign() { 
		init();
		//this class is only used to build the grid
		List<BDT_FlowchartDisplay.armWrapper> armWrapperList = BDT_FlowchartDisplay.getFlowchart(designList[0].id, firstProject.id);   
		List<Epoch__c> myEpochList = BDT_FlowchartDisplay.getEpochHeaderList(designList[0].id);
		        
	}
	

}