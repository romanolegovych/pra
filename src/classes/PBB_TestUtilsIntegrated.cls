/**
    @Description This class is designed to create test data for unit tests that
                 require a comprehensive set of data.
                 
                 Because unit test execution speed is severly impacted by DML 
                 statements, these utils should be used only for classes that
                 perform complex logic on integrated data.
*/
@isTest 
public with sharing class PBB_TestUtilsIntegrated {
    public PBB_TestUtils pbbTestUtils = new PBB_TestUtils();
     
    // Reference Data
    public list<Currency__c>                       lstCurrency;
    public list<Country__c>                        lstCountry;
    public list<Job_Class_Desc__c>                 lstJobClassDesc;
    public Service_Model__c                        objServiceModel;
    public list<Service_Area__c>                   lstServiceArea;
    public list<Service_Function__c>               lstServiceFunction;
    public list<Service_Task__c>                   lstServiceTask;
    public Bill_Rate_Card_Model__c                 objBillRateCardModel;
    public PBB_Scenario__c                         objPbbScenario;
    public list<PBB_Scenario_Country__c>           lstPbbScenarioCountry;
    public list<Countries_Service_Tasks__c>        lstCountriesServiceTasks;
    public list<Service_Task_Responses__c>         lstServiceTaskResponses;
    public list<Service_Impact_Question_Name__c>   lstImpactQuestionNames;
    public list<Service_Impact_Questions__c>       lstImpactQuestions;
    public list<Service_Impact_Response__c>        lstImpactResponses;
    
    // Constants to reference specific list items
    public final Integer CURR_USD    = 0;
    public final Integer CURR_EUR    = 1;
    public final Integer COUNTRY_US  = 0;
    public final Integer COUNTRY_UK  = 1;
    public final Integer COUNTRY_MX  = 2;

    public list<Currency__c> createCurrency() {
        lstCurrency = new list<Currency__c>();
        lstCurrency.add(new Currency__c(name='USD'));
        lstCurrency.add(new Currency__c(name='EUR'));
        insert lstCurrency;
        return lstCurrency; 
    }
        
    public list<Country__c> createCountries() {
        lstCountry = new list<Country__c>();
        lstCountry.add(new Country__c(name='UNITED STATES', PRA_Country_Id__c='1', Country_Code__c='US'));
        lstCountry.add(new Country__c(name='UNITED KINGDOM', PRA_Country_Id__c='2', Country_Code__c='UK'));
        lstCountry.add(new Country__c(name='MEXICO', PRA_Country_Id__c='3', Country_Code__c='MX'));
        insert lstCountry;
        return lstCountry;
    }

    public list<Job_Class_Desc__c> createJobs(Integer aNumRecs) {
        lstJobClassDesc = new list<Job_Class_Desc__c>();
        for (Integer i = 1; i <= aNumRecs; i++) {
            lstJobClassDesc.add(new Job_Class_Desc__c(name = String.Format('Job {0}', new String[]{String.valueOf(i)}),
                                              Job_Class_Code__c = String.Format('J{0}', new String[]{String.valueOf(i)}),
                                              Job_Code__c = String.Format('J{0}', new String[]{String.valueOf(i)}),
                                              Job_Class_ExtID__c = String.Format('Job {0}', new String[]{String.valueOf(i)})));
        }
        insert lstJobClassDesc;
        return lstJobClassDesc;
    }

    // Service Model
    public Service_Model__c createServiceModel() {
        objServiceModel = new Service_Model__c(Name='Service Model 1',Description__c ='The One and Only SM',Status__c ='In-Process');
        insert objServiceModel;
        return objServiceModel;
    }

    public list<Service_Area__c> createServiceAreas(Service_Model__c aSM, Integer aNumRecs) {
        lstServiceArea = new list<Service_Area__c>();
        for (Integer i = 1; i <= aNumRecs; i++) {
            lstServiceArea.add(new Service_Area__c(Name = String.Format('Service Area {0}', new String[]{String.valueOf(i)}), Service_Model__c = aSM.id));
        }
        insert lstServiceArea;
        return lstServiceArea;
    }
    
    public list<Service_Function__c> createServiceFunctions(list<Service_Area__c> aLstSA, Integer aNumRecs) {
        lstServiceFunction = new list<Service_Function__c>();
        for (Service_Area__c sa : aLstSA) {
            for (Integer i = 1; i <= aNumRecs; i++) {
                lstServiceFunction.add(new Service_Function__c(Name = String.Format('{0} : Service Function {1}', new String[]{sa.Name, String.valueOf(i)}), Service_Area__c = sa.id));
            }
        }
        insert lstServiceFunction;
        return lstServiceFunction;
    }
    
    public list<Service_Task__c> createServiceTasks(list<Service_Function__c> aLstSF, Integer aNumRecs) {
        lstServiceTask = new list<Service_Task__c>();
        for (Service_Function__c sf : aLstSF) {
            for (Integer i = 1; i <= aNumRecs; i++) {
                lstServiceTask.add(new Service_Task__c(Name = String.Format('{0} : Service Task {1}', new String[]{sf.Name, String.valueOf(i)}), Service_Function__c = sf.id));
            }
        }
        insert lstServiceTask;
        return lstServiceTask;
    }
    
    public Bill_Rate_Card_Model__c createBillRateCardModel() {
        objBillRateCardModel = new Bill_Rate_Card_Model__c(name = 'Bill Rate Card 1', Year__c = '2015');
        insert objBillRateCardModel;
        objBillRateCardModel.Status__c = 'Approved';
        update objBillRateCardModel;
        return objBillRateCardModel;
    }
    
    public PBB_Scenario__c createPbbScenario(Bid_Project__c aBidProj) {
        Bid_Project__c bidProj;
        if (aBidProj == null) {
            pbbTestUtils.createwfmproject();
            bidProj = pbbTestUtils.createbidproject();
        }
        else {
            bidProj = aBidProj;
        }
        objPbbScenario = new PBB_Scenario__c(Description__c = 'PBB Scenario 1', Bid_Project__c = bidProj.Id, Bill_Rate_Card_Model__c = null);
        insert objPbbScenario;
        return objPbbScenario;
    }
    
    public list<PBB_Scenario_Country__c> createScenarioCountries(PBB_Scenario__c aPbbScen, list<Country__c> aCountries, Boolean createGlobal) {
        System.debug('---- ' + aPbbScen);
        System.debug('---- ' + aCountries);
        lstPbbScenarioCountry = new list<PBB_Scenario_Country__c>();
        for (Country__c ctry : aCountries) {
            lstPbbScenarioCountry.add(new PBB_Scenario_Country__c(Country__c = ctry.id, PBB_Scenario__c = aPbbScen.id));
        }
        if (createGlobal) {
            lstPbbScenarioCountry.add(new PBB_Scenario_Country__c(PBB_Scenario__c = aPbbScen.id));
        }
        insert lstPbbScenarioCountry;
        return lstPbbScenarioCountry;
    }
    
    public list<Countries_Service_Tasks__c> createCountriesServiceTasks(list<list<sObject>> aCountriesAndTasks) {
        System.debug('---- aCountriesAndTasks: ' + aCountriesAndTasks);
        lstCountriesServiceTasks = new list<Countries_Service_Tasks__c>();
        for (list<sObject> ctryToTask : aCountriesAndTasks) {
            lstCountriesServiceTasks.add( new Countries_Service_Tasks__c(PBB_Scenario_Country__c = ctryToTask[0].id, Service_Task__c = ctryToTask[1].id));
        }
        insert lstCountriesServiceTasks;
        System.debug('---- lstCountriesServiceTasks: ' + lstCountriesServiceTasks);
        return lstCountriesServiceTasks;
    }
    
    public void createImpactQuestionAndResponses(Integer aNumQuestions, Integer aNumResponses) {
    	lstImpactQuestionNames = new list<Service_Impact_Question_Name__c>();
        lstImpactQuestions = new list<Service_Impact_Questions__c>();
        lstImpactResponses = new list<Service_Impact_Response__c>();
    	
    	for (Integer i = 1; i <= aNumQuestions; i++) {
            lstImpactQuestionNames.add(new Service_Impact_Question_Name__c(
                                           name = String.Format('Service Impact Question {0}', new String[]{String.valueOf(i)}),
                                           description__c = String.Format('Service Impact Question {0}', new String[]{String.valueOf(i)}),
                                           isActive__c = true));
        }
        insert lstImpactQuestionNames;
        
        for (Service_Impact_Question_Name__c siqn : lstImpactQuestionNames) {
        	lstImpactQuestions.add(new Service_Impact_Questions__c( Service_Impact_Question__c = siqn.id ));
        }
        insert lstImpactQuestions;
        
    	for (Service_Impact_Questions__c siq : lstImpactQuestions) {
	    	for (Integer i = 1; i <= aNumResponses; i++) {
	    		lstImpactResponses.add(new Service_Impact_Response__c(
	    		                           name = String.Format('Service Impact Response {0}', new String[]{String.valueOf(i)}),
	    		                           Service_Impact_Questions__c = siq.id));
	    	}
    	}
    	insert lstImpactResponses;
    }
    
    public list<Service_Task_Responses__c> createServiceTaskResponses(list<Countries_Service_Tasks__c> aCountriesServiceTasks, 
                                                                      list<Service_Impact_Response__c> aServiceImpactResponses) {
        lstServiceTaskResponses = new list<Service_Task_Responses__c>();
        
        for (Countries_Service_Tasks__c cst : aCountriesServiceTasks) {
            for (Service_Impact_Response__c sir : aServiceImpactResponses) {
                lstServiceTaskResponses.add( new Service_Task_Responses__c( Countries_Service_Tasks__c = cst.id, Service_Impact_Response__c = sir.id ));
            }
        }
        insert lstServiceTaskResponses;
        return lstServiceTaskResponses;
    }
}