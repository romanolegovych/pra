@isTest
private class ServiceDataAccessorTest {

    static testMethod void myUnitTest() {
        // create test data
        ServiceCategory__c sc = new ServiceCategory__c(name='test',code__c='001');
        insert sc;
        
        list<Service__c> sList = new list<Service__c>();
        sList.add(new Service__c(ServiceCategory__c = sc.id, Name = 'a', SequenceNumber__c = 1));
        sList.add(new Service__c(ServiceCategory__c = sc.id, Name = 'b', SequenceNumber__c = 2));
        sList.add(new Service__c(ServiceCategory__c = sc.id, Name = 'c', SequenceNumber__c = 3));
        insert sList;
        
        //test retrieve logic
        sList = ServiceDataAccessor.getServiceList('ServiceCategory__c = \'' + sc.id + '\'');
        system.assertEquals(3,sList.size());
    }
}