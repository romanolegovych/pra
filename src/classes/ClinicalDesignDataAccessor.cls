public with sharing class ClinicalDesignDataAccessor {

	/** Object definition for fields used in application for ClinicalDesign__c
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('ClinicalDesign__c');		
	}
	
	
	/** Object definition for fields used in application for ClinicalDesign__c with the parameter referenceName as a prefix
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'Design_Name__c,';
		result += referenceName + 'Project__c';
		return result;
	}
	
	/** Retrieve list of clinical designs.
	 * @author	Maurice Kremer
	 * @version 07-Jan-2014
	 * @param	whereClause			The criteria that Laboratory Selection Lists must meet
	 * @return	List of clinical designs
	 */
	public static List<ClinicalDesign__c> getClinicalDesignList(String whereClause) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM ClinicalDesign__c ' +
								'WHERE  {1}',
								new List<String> {
									getSObjectFieldString(),
									whereClause
								}
							);
		return (List<ClinicalDesign__c>) Database.query(query);
	}

}