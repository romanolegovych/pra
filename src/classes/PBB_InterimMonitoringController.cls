/**
@author Ramya
@date 2015
@description this controller class is to display the parameters for Interim Monitoring
**/

public class PBB_InterimMonitoringController {

     
    //to hide/show the main tabs in the component.
    public  List<String> HiddenHomeTabsList       {get;set;}  
    
    //to hide/show the subtabs in the component.
    public  List<String> HiddenSubTabsList        {get;set;}
    
    //PBB scenarios   
    Public PBB_Scenario__c  PBBScenario           {get;set;}
    
    //PBB scenarios ID  
    Public String PBBScenarioID                   {get;set;}
      
    
     /**
    * @author Ramya
    * @Date 2015
    * @Description constructor 
    *
    */
    
    public PBB_InterimMonitoringController(ApexPages.StandardController controller) {
        
        HiddenHomeTabsList = new List<String>();
        HiddenSubTabsList = new List<String>();
        HiddenSubTabsList.add('SDpage');      
        
        if(ApexPages.currentPage().getParameters().get('id')!=null){
        PBBScenarioID =ApexPages.currentPage().getParameters().get('id');
        }else
        PBBScenarioID = ApexPages.currentPage().getParameters().get('PBBScenarioID');
             
        if(PBBScenarioID!=null && PBBScenarioID!=''){
            PBBScenario = PBB_DataAccessor.getScenarioByID(PBBScenarioID);
            system.debug('----PBBScenario ----'+PBBScenario );
        }
       
        //setting the homepage and subpage components
        if(pbbscenario==null && PBBScenarioID==null){           
          
            HiddenHomeTabsList.add('PRpage');
            HiddenSubTabsList.add('PIpage');
            HiddenSubTabsList.add('CSpage');    
            HiddenSubTabsList.add('SRMpage');   
            HiddenSubTabsList.add('SDpage');    
            HiddenHomeTabsList.add('PCpage');  
        }else{
        }                
    }
    
      /**
    * @author Ramya
    * @Date 1/8/2012
    * @Description Method to save Scenario
    *
    */
    
    public PageReference save(){
    
        PageReference pageRef =null; 
        upsert PBBScenario; 
        pageRef = new PageReference('/apex/PBB_InterimMonitoring?id='+PBBScenario.Id);
        pageRef.setRedirect(true);
        return pageRef; 
    }
    
      /** 
   @author Ramya
   @date 2015   
   @description method to cancel and navigate to project page
   */ 
    public pageReference Cancel(){
    
        String baseURL = System.URL.getSalesforceBaseUrl().toExternalForm() + '/' + PBBScenario.Bid_Project__c ;
        PageReference returnPage = new PageReference(baseURL);
        returnPage.setRedirect(true);    
        return returnPage;
    }   
}