public class AgreementControllerExtension {

Apttus__APTS_Agreement__c agreement;
public AgreementControllerExtension(ApexPages.standardController std)
{
    agreement = (Apttus__APTS_Agreement__c )std.getRecord();
    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Constructor called.'));
}

public PageReference save() {
System.debug('-----------------> AgreementControllerExtension:save():'+agreement.Name);
ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Save Called.'));
return new PageReference('/' + agreement.Id);
}


}