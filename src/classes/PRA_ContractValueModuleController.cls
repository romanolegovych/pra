public with sharing class PRA_ContractValueModuleController {
    
    // private Static variables
    public static Integer PAGE_SIZE { get{return 10000;}} //paggining moved to client site
    // Constant representing the select value option in the picklist
    public final String ST_NONE = 'None'; 
    // Page variables
    public String selectedProject;
    public String selectedProjectLabel  {get;set;}
    public String selectedProjectName  {get;set;}
    public Client_Project__c project {get;set;}
    // Total Detail Client Unit Contract Values
    public Decimal totalContractValues {get;set;}
    public Decimal totalNa {get;set;}
    public Decimal totalEmea {get;set;}
    public Decimal totalLatin {get;set;}
    public Decimal totalApac {get;set;}
    public Decimal totalConBdg {get;set;}
    private Map<String, Regional_Contract_Value__c> regionContractValue = new Map<String, Regional_Contract_Value__c>();
    private Map<Id, Client_Task__c> regionalContractValues = new Map<Id, Client_Task__c>();
    private Map<String, List<Decimal>> bidContractValues = new Map<String, List<Decimal>> ();
    private List<Client_Task__c> allTasks = new List<Client_Task__c> ();
    private Map<Id, Client_Task__c> selectedTasks = new  Map<Id, Client_Task__c> ();
    private List<SummaryValuesWraper> summaryValues = new List<SummaryValuesWraper>();
    private Integer pageIdx = 0;
    private Decimal projectContractsValue;
    // for user prefs
    private Integer searchCount{get;set;}
    private PRA_Utils.UserPreference userPreference{get;private set;}
    
    public PRA_ContractValueModuleController() {
        searchCount = -1;
        if(ApexPages.currentPage().getParameters().get('id') != null) {
            setSelectedProject(ApexPages.currentPage().getParameters().get('id'));
        }
        
        userPreference = PRA_Utils.retrieveUserPreference();
        if(userPreference != null) {
            selectedProject = userPreference.projectId;
            List<Client_Project__c> clientProjectsList = 
                [SELECT Name, Is_Project_Locked__c, Migrated_Date__c FROM Client_Project__c WHERE Id = :selectedProject and Load_Status__c != 'New'];
            if(clientProjectsList.size() == 1) {
                setSelectedProject(userPreference.projectId);
                selectedProjectLabel = clientProjectsList.get(0).Name;
                selectedProjectName  =  clientProjectsList.get(0).Name;
            } else {
                searchCount++;
            }
        } else {
            searchCount++;
            selectedProjectName  ='Home';
        }
    }
    
    //Reset Button
    public void reset() {
        selectedProject = ST_NONE;
        selectedProjectLabel = '';
        project = null;
        summaryValues = null;
        PRA_Utils.deleteUserPreference();  
        selectedProjectName  ='Home';   
    }
    
    public void init() {
        summaryValues = new List<SummaryValuesWraper>();
        initRegionContractValue();
        initBidContractValues();
        initTasks();
        selectTaskPage();
    }
    
    public void initRegionContractValue() {
       regionContractValue = new Map<String, Regional_Contract_Value__c> ();
        
        if(project != null) {
            List<Regional_Contract_Value__c> regionCtrValList = [SELECT Asia_Pacific__c, Client_Task__c, Effective_Date__c, Europe_Africa__c, 
                        Id, Latin_America__c, US_Canada__c 
                        FROM Regional_Contract_Value__c 
                        WHERE Client_Task__r.Project__c = :project.Id];
            for(Regional_Contract_Value__c rcv : regionCtrValList) {
                if(!regionContractValue.containsKey(rcv.Client_Task__c)) {
                    regionContractValue.put(rcv.Client_Task__c, rcv);
                } else {
                    if(regionContractValue.get(rcv.Client_Task__c).Effective_Date__c < rcv.Effective_Date__c) {
                        regionContractValue.put(rcv.Client_Task__c, rcv);
                    }
                }
            }
        }
    }
    
    public void initBidContractValues() {
        bidContractValues = new Map<String, List<Decimal>>();
        regionalContractValues = new Map<Id, Client_Task__c>();
        projectContractsValue = 0;
        
        if(project != null) {
            List<Client_Task__c> bidContractsSum = [SELECT Id, Contract_Value__c, Total_Contract_Units__c, Total_CON_BDG_Value__c, 
                                                    Total_CON_BDG_Value_APAC__c, Total_CON_BDG_Value_EA__c, Total_CON_BDG_Value_LA__c,
                                                    Total_CON_BDG_Value_US_Ca__c, Contract_Value_APAC__c, Contract_Value_EA__c,
                                                    Contract_Value_LA__c, Contract_Value_US_CA__c, Contract_Value_Per_Unit__c,
                                                    CVPU_US_Canada__c, CVPU_Europe_Africa__c, CVPU_Asia_Pacific__c, CVPU_Latin_America__c
                                                    FROM Client_Task__c 
                                                    WHERE Project__c = :project.Id];                                                   
            for(Client_Task__c ct : bidContractsSum) {
                Decimal contractValue = 0;
                Decimal totalUnits = 0;
                if(ct.Contract_Value__c != null) {
                    contractValue = (Decimal)ct.Contract_Value__c;
                    projectContractsValue += (Decimal)ct.Contract_Value__c;
                    //bidContractValues.put((String)ar.get('Client_Task__c'),(Decimal)ar.get('expr0'));
                }
                if(ct.Total_Contract_Units__c != null) {
                    totalUnits = (Decimal)ct.Total_Contract_Units__c;
                }
                regionalContractValues.put(ct.Id, ct);
                bidContractValues.put(ct.Id, new List<Decimal>{contractValue, totalUnits});
            }
        }
    }
    
    public void initProjects() {
    }
    
    public void initTasks() {
        allTasks = new List<Client_Task__c>();
        if(project != null) {
            allTasks = [SELECT  Id, Description__c, Client_Unit_Number__c, Project_Region__r.Name, 
                        Total_Units__c, Total_Baseline_Units__c, LTD_Contracted_BDG_Unit__c, REM_Contracted_BDG_Unit__c
                        FROM Client_Task__c 
                        WHERE Project__c = :project.Id
                        ORDER BY Client_Unit_Number__c, Description__c, Project_Region__r.Name];
        }
    }
    
    public void selectTaskPage() {
        selectedTasks = new Map<Id, Client_Task__c>();
        if(allTasks != null && allTasks.size() > 0) {
            for(Integer i = pageIdx * PAGE_SIZE; (i < (pageIdx + 1) * PAGE_SIZE) && (i < allTasks.size()); i++ ) {
                system.debug('--- i=' + i + ' size=' + allTasks.size());
                selectedTasks.put(allTasks.get(i).Id, allTasks.get(i));
            }
        }
    }
    
    public void setSelectedProject(String projectId) {
        if(projectId != null && projectId !='') {
            searchCount++;
            if(userPreference != null) {
                userPreference.projectId = projectId;
            } else { 
                userPreference = new PRA_Utils.UserPreference();
                userPreference.projectId = projectId;
                userPreference.regionId = new String[]{ST_NONE};
                userPreference.countryId = new String[]{ST_NONE};
                userPreference.clientUnitId = new String[]{ST_NONE};
                userPreference.clientUnitGroupId = new String[]{ST_NONE};
                userPreference.operationalAreaId = new String[]{ST_NONE};
                userPreference.unconfirmedOnly = false;
                userPreference.confirmedOnly = false;
                userPreference.crctId = '';
                userPreference.userId = UserInfo.getUserId();
            }  
            selectedProject = projectId;
            
            userPreference.projectId = projectId;
            
            project = [SELECT Name, Client__c, Global_Project_Analyst__c, Global_Project_Manager_Director__c, Project_Currency__c, Contract_type__c, 
                        Migrated_date__c, Discount__c, Discount_Type__c, Volume_Discount__c, Volume_Discount_Type__c 
                        FROM Client_Project__c WHERE Id = :projectId and Load_Status__c != 'New'];
                        
            // initialize dependent variables
            selectedProjectName  =project.name;
            init();
        }
    }
    public void initSummaryValue() {
        summaryValues = new List<SummaryValuesWraper>();
        List<SummaryValuesWraper> discounts = new List<SummaryValuesWraper>();
        
        //Contracted Value
        summaryValues.add(new SummaryValuesWraper('Contracted Value after Bottom Line Discount'));
        //Value of Downscope
        summaryValues.add(new SummaryValuesWraper('Value of Downscope'));
        //Regional Contract Value Adjustment
        summaryValues.add(new SummaryValuesWraper('Regional Contract Value Adjustment'));
        //Forecast Contracted Value
        summaryValues.add(new SummaryValuesWraper('Contract Value before Volume Discount'));
        // Discount percentage of Net discount
        summaryValues.add(new SummaryValuesWraper('Volume Discount'));
        // Net Discount - Volume Discount
        summaryValues.add(new SummaryValuesWraper('Current Proposed Contracted Value'));        
        

        if(project != null) {
            SummaryValuesWraper contracted      = summaryValues.get(0);
            SummaryValuesWraper downscope       = summaryValues.get(1);
            SummaryValuesWraper RCVAdj          = summaryValues.get(2);
            SummaryValuesWraper forecast        = summaryValues.get(3);
            SummaryValuesWraper volumeDiscount  = summaryValues.get(4);
            SummaryValuesWraper currentProposed = summaryValues.get(5);
            
            contracted.resetRegionValues();
            downscope.resetRegionValues();
            RCVAdj.resetRegionValues();
            forecast.resetRegionValues();
            volumeDiscount.resetRegionValues();
            currentProposed.resetRegionValues();
            
            Decimal rcvNa       = 0;
            Decimal rcvEmea     = 0;
            Decimal rcvLatin    = 0;
            Decimal rcvApac     = 0;
            Decimal total       = 0;
            
            Decimal percentNA   = 0;
            Decimal percentEMEA = 0;
            Decimal percentLATIN = 0;
            Decimal percentAPAC = 0;
            
            for(Client_Task__c task : allTasks) {
                rcvNa       = 0;
                rcvEmea     = 0;
                rcvLatin    = 0;
                rcvApac     = 0;
                total       = 0;
                if(regionalContractValues.containsKey(task.Id)) {
                    if(regionalContractValues.get(task.Id).Contract_Value_US_CA__c != null) {
                        rcvNa = regionalContractValues.get(task.Id).Contract_Value_US_CA__c;
                    }
                    if(regionalContractValues.get(task.Id).Contract_Value_EA__c != null) {
                        rcvEmea = regionalContractValues.get(task.Id).Contract_Value_EA__c;
                    }
                    if(regionalContractValues.get(task.Id).Contract_Value_LA__c != null) {
                        rcvLatin = regionalContractValues.get(task.Id).Contract_Value_LA__c;
                    }
                    if(regionalContractValues.get(task.Id).Contract_Value_APAC__c != null) {
                        rcvApac = regionalContractValues.get(task.Id).Contract_Value_APAC__c;
                    }
                    if(regionalContractValues.get(task.Id).Contract_Value__c != null) {
                        total = regionalContractValues.get(task.Id).Contract_Value__c;
                    }
                }
                
                //Contracted Value
                contracted.total    += total;
                contracted.na       += rcvNa;
                contracted.emea     += rcvEmea;
                contracted.latin    += rcvLatin;
                contracted.apac     += rcvApac;
            }
            system.debug('-----------total-------------'+contracted.total);
            system.debug('-----------na-------------'+contracted.na);            
            
            //to calculate the Regional Contract Value Adjustment values for different regions and total
            Map<String,Decimal> AdjRCVMap=New Map<String,Decimal>();            
            AdjRCVMap = PRA_DataAccessor.getAdjtoRCVbyGroupRollupbyProject(project.Name);
            system.debug('-----------AdjRCVMap-------------'+AdjRCVMap);
            
            //RCVAdj Value
            RCVAdj.total    = (Decimal)AdjRCVMap.get('Total'); 
            RCVAdj.na       = (AdjRCVMap.get('US/Canada')== null)?0.00:(Decimal)AdjRCVMap.get('US/Canada');             
            RCVAdj.emea     = (AdjRCVMap.get('Europe/Africa')== null)?0.00:(Decimal)AdjRCVMap.get('Europe/Africa'); 
            RCVAdj.latin    = (AdjRCVMap.get('Latin America')== null)?0.00:(Decimal)AdjRCVMap.get('Latin America'); 
            RCVAdj.apac     = (AdjRCVMap.get('Asia/Pacific')== null)?0.00:(Decimal)AdjRCVMap.get('Asia/Pacific');
            
            system.debug('-----------RCVAdj-------------'+RCVAdj);
            
            // PRA-964 - make sure that total values had been calculated
            getDetailClientUnitContractValues();
            
            // populate forecast values
            forecast.na     = totalNa;
            forecast.emea   = totalEmea;
            forecast.latin  = totalLatin;
            forecast.apac   = totalApac;
            forecast.total  = totalConBdg;
            
            if(contracted.total != 0) { 
                percentNA = contracted.na / contracted.total;
                percentEMEA = contracted.emea / contracted.total;
                percentLATIN = contracted.latin / contracted.total;
                percentAPAC = contracted.apac / contracted.total;
            }            
            
            // populate bottom line and net discount
            String discountType = '';
            if(project.Discount_Type__c != null) {
                discountType = project.Discount_Type__c;
            } else {
                discountType = 'Value';
            }
            Decimal discountValue = 0;
            if(project.Discount__c != null) {
                discountValue = project.Discount__c;
            }
            if(discountType.equals('%')) {
                contracted.na       = (contracted.na * (100 - discountValue) / 100);
                contracted.emea     = (contracted.emea * (100 - discountValue) / 100);
                contracted.latin    = (contracted.latin * (100 - discountValue) / 100);
                contracted.apac     = (contracted.apac * (100 - discountValue) / 100);
                contracted.total    = (contracted.total * (100 - discountValue) / 100);
                
                forecast.na     = (forecast.na * (100 - discountValue) / 100);
                forecast.emea   = (forecast.emea * (100 - discountValue) / 100);
                forecast.latin  = (forecast.latin * (100 - discountValue) / 100);
                forecast.apac   = (forecast.apac * (100 - discountValue) / 100);
                forecast.total  = (forecast.total * (100 - discountValue) / 100);
            } else {
                contracted.na       = contracted.na - (discountValue * percentNA);
                contracted.emea     = contracted.emea - (discountValue * percentEMEA);
                contracted.latin    = contracted.latin - (discountValue * percentLATIN);
                contracted.apac     = contracted.apac - (discountValue * percentAPAC);
                contracted.total    = contracted.total - (discountValue);
                
                forecast.na     = forecast.na - (discountValue * percentNA);
                forecast.emea   = forecast.emea - (discountValue * percentEMEA);
                forecast.latin  = forecast.latin - (discountValue * percentLATIN);
                forecast.apac   = forecast.apac - (discountValue * percentAPAC);
                forecast.total  = forecast.total - (discountValue);
            }
            
            system.debug('-----------total after discount-------------'+contracted.total);
            system.debug('-----------na after discount-------------'+contracted.na);
            
            // populate downscope values
            downscope.na    = forecast.na - contracted.na;
            downscope.emea  = forecast.emea - contracted.emea;
            downscope.latin = forecast.latin - contracted.latin;
            downscope.apac  = forecast.apac - contracted.apac; 
            downscope.total = forecast.total - contracted.total; 
            
            String volumeDiscountType = '';
            if(project.Volume_Discount_Type__c != null) {
                volumeDiscountType = project.Volume_Discount_Type__c;
            } else {
                volumeDiscountType = 'Value';
            }
            Decimal volumeDiscountValue = 0;
            if(project.Volume_Discount__c != null) {
                volumeDiscountValue = project.Volume_Discount__c;
            }
            if(volumeDiscountType.equals('%')) {
                volumeDiscount.na       = -(forecast.na * volumeDiscountValue / 100);
                volumeDiscount.emea     = -(forecast.emea * volumeDiscountValue / 100);
                volumeDiscount.latin    = -(forecast.latin * volumeDiscountValue / 100);
                volumeDiscount.apac     = -(forecast.apac * volumeDiscountValue / 100);
                volumeDiscount.total    = -(forecast.total * volumeDiscountValue / 100);
                
                currentProposed.na      = forecast.na + volumeDiscount.na;
                currentProposed.emea    = forecast.emea + volumeDiscount.emea;
                currentProposed.latin   = forecast.latin + volumeDiscount.latin;
                currentProposed.apac    = forecast.apac + volumeDiscount.apac;
                currentProposed.total   = forecast.total + volumeDiscount.total;
            } else {
                volumeDiscount.na       = -(volumeDiscountValue * percentNA);
                volumeDiscount.emea     = -(volumeDiscountValue * percentEMEA);
                volumeDiscount.latin    = -(volumeDiscountValue * percentLATIN);
                volumeDiscount.apac     = -(volumeDiscountValue * percentAPAC);
                volumeDiscount.total    = -(volumeDiscountValue);
                
                currentProposed.na      = forecast.na + volumeDiscount.na;
                currentProposed.emea    = forecast.emea + volumeDiscount.emea;
                currentProposed.latin   = forecast.latin + volumeDiscount.latin;
                currentProposed.apac    = forecast.apac + volumeDiscount.apac;
                currentProposed.total   = forecast.total + volumeDiscount.total;
            }
        }
    }
    
    public String getSelectedProject() {
        return selectedProject;
    }
    
    // Selecting a Project 
    public void selectProject() {
        if(selectedProject == ST_NONE) {
        }
        
    }
    
    public List<SummaryValuesWraper> getSummaryValues() {

        if (summaryValues == null || summaryValues.size() == 0) {
            initSummaryValue();
        }
        
        return summaryValues;
    }
 
    public List<ClientUnitContractValue> getDetailClientUnitContractValues() {
        List<ClientUnitContractValue> retVal = new List<ClientUnitContractValue>();
        totalContractValues = 0;
        totalNa = 0;
        totalEmea = 0;
        totalLatin = 0;
        totalApac = 0;
        totalConBdg = 0;
        
        for(Client_Task__c task : allTasks) {
            
            Decimal TOTAL = 0.0;
            
            if(task.Client_Unit_Number__c != 'Migrated Worked Hours') {
            
                //Id, Client_Task__r.Description__c, Project_Region__r.Name
                ClientUnitContractValue cucv = new ClientUnitContractValue();
                cucv.taskId = task.Id; // not displayed - mainly for testing purpose
                
                if(task.Client_Unit_Number__c != null) {
                    cucv.unitNumber = task.Client_Unit_Number__c;
                } else {
                    cucv.unitNumber = '-';
                }
                cucv.unitName = task.Description__c;
                cucv.region = task.Project_Region__r.Name;
                         
                // US Canada Contract Value
                Decimal regionalVal = 0;
                if(regionalContractValues.containsKey(task.Id) && regionalContractValues.get(task.Id).Total_CON_BDG_Value_US_Ca__c != null) {
                    regionalVal = (regionalContractValues.get(task.Id).Total_CON_BDG_Value_US_Ca__c == null) ? 0 : regionalContractValues.get(task.Id).Total_CON_BDG_Value_US_Ca__c;//.setScale(2);
                }
                totalNa += regionalVal;
                cucv.na =  regionalVal;
                //totalNa = totalNa.setScale(2);
               
                
                // Europe Africa Contract Value
                regionalVal = 0;
                if (regionalContractValues.containsKey(task.Id) && regionalContractValues.get(task.Id).Total_CON_BDG_Value_EA__c != null){
                    regionalVal = (regionalContractValues.get(task.Id).Total_CON_BDG_Value_EA__c == null) ? 0 : regionalContractValues.get(task.Id).Total_CON_BDG_Value_EA__c;//.setScale(2);
                }
                totalEmea += regionalVal;
                cucv.emea = regionalVal;
                //totalEmea = totalEmea.setScale(2);
                
                // Latin America Contract  Value
                regionalVal = 0;
                if(regionalContractValues.containsKey(task.Id) && regionalContractValues.get(task.Id).Total_CON_BDG_Value_LA__c != null) {
                    regionalVal = (regionalContractValues.get(task.Id).Total_CON_BDG_Value_LA__c == null) ? 0 : regionalContractValues.get(task.Id).Total_CON_BDG_Value_LA__c;//.setScale(2);
                }
                totalLatin += regionalVal;
                cucv.latin = regionalVal;
                //totalLatin = totalLatin.setScale(2);
                
                // Asia Pacific Contract Value
                regionalVal = 0;
                if(regionalContractValues.containsKey(task.Id) && regionalContractValues.get(task.Id).Total_CON_BDG_Value_APAC__c != null) {
                    regionalVal = (regionalContractValues.get(task.Id).Total_CON_BDG_Value_APAC__c == null) ? 0 : regionalContractValues.get(task.Id).Total_CON_BDG_Value_APAC__c;//.setScale(2);
                }
                totalApac += regionalVal;
                cucv.apac = regionalVal;
                //totalApac = totalApac.setScale(2);
                
                //Unit Price
                cucv.unitPrice = regionalContractValues.get(task.Id).Contract_Value_Per_Unit__c;
                //cucv.unitPrice = cucv.unitPrice.setScale(2);
                
                //Total
                if(regionalContractValues.containsKey(task.Id) && regionalContractValues.get(task.Id).Total_CON_BDG_Value__c != null) {
                    TOTAL = regionalContractValues.get(task.Id).Total_CON_BDG_Value__c;
                }
                totalConBdg += TOTAL;
                cucv.total = TOTAL;
                //cucv.total = cucv.total.setScale(2);
                totalContractValues += TOTAL;
                //totalContractValues = totalContractValues.setScale(2);   
                
                retVal.add(cucv);
            }
        }
        
        if(project == null) {
            retVal = new List<ClientUnitContractValue>();
            totalContractValues = 0;
            totalNa = 0;
            totalEmea = 0;
            totalLatin = 0;
            totalApac = 0;
            totalConBdg = 0;
        }
        
        return retVal;
    }
    
    public PageReference exportToExcel() {
        Pagereference retVal=Page.PRA_ContractValueModuleCSV;

        if(project != null) {
            retVal.getParameters().put('id', project.Id);
        }
        system.debug('---' + retVal.getUrl());
        return retVal;
    }
    
    /* Remote actions*/
    // JS Remoting action called when looking for Project
    @RemoteAction
    public static List<Client_Project__c> searchProject(String projectName) {
        System.debug('projectName: '+projectName );
        List<Client_Project__c> liProj = Database.query('SELECT id, name FROM Client_Project__c WHERE name LIKE \'%' 
            + String.escapeSingleQuotes(projectName) + '%\' and Load_Status__c != \'New\'');
        return liProj;
    }
    
    public void saveUserPreferences() { 
        if(searchCount > 0) {
            // Save user prefs
            PRA_Utils.saveUserPreference(userPreference);
        } 
    } 
    
    // Wraper classes
    public class SummaryValuesWraper {
        public String name {get;set;}
        public Decimal total {get;set;}
        public Decimal na {get;set;}
        public Decimal emea {get;set;}
        public Decimal latin {get;set;}
        public Decimal apac  {get;set;}
        
        public SummaryValuesWraper() {
        }
        
        public SummaryValuesWraper(String lineName) {
            name = lineName;
        }
        
        public void resetRegionValues() {
            total = 0;
            na = 0;
            emea = 0;
            latin = 0;
            apac = 0;
        }
    }
   
    public class ClientUnitContractValue {
        public String   unitNumber {get;set;}
        public String   unitName {get;set;}
        public String   region {get;set;}
        public Decimal  unitPrice {get;set;}
        public Decimal  total {get;set;} //{get{return na+emea+latin+apac;}} 
        public Decimal  na {get;set;}
        public Decimal  emea {get;set;}
        public Decimal  latin {get;set;}
        public Decimal  apac  {get;set;}
        public ID       taskId  {get;set;}
        
        public ClientUnitContractValue() {
        }
        
        public ClientUnitContractValue(String clientUnitNumber, String clientUnitName, String clientRegion, Decimal unitPriceVal, 
                            Decimal naCV, Decimal emeaCV, Decimal latinCV, Decimal apacCV, Decimal totalContactValue) {
            unitNumber = clientUnitNumber;
            unitName = clientUnitName;
            region = clientRegion;
            unitPrice = unitPriceVal;//.setScale(2);
            total = totalContactValue;//.setScale(2);
            na = naCV;//.setScale(2);
            emea = emeaCV;//.setScale(2); 
            latin = latinCV;//.setScale(2);
            apac = apacCV;//.setScale(2);
        }
    }
}