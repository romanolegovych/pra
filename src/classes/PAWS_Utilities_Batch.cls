//Database.executeBatch(new PAWS_Utilities_Batch());
global with sharing class PAWS_Utilities_Batch implements Database.Batchable<sObject>, Database.Stateful
{
	global List<String> errors = new List<String>();
	global String Query = 'Select Flow__c, Flow_Id__c From STSWR1__Flow_Step_Property__c Where Flow__c = null';
	
	global Database.QueryLocator start(Database.BatchableContext BC)
	{
		return Database.getQueryLocator(this.Query);
	}
	
	global void execute(Database.BatchableContext BC, List<sObject> scope)
	{
		try
		{
			for(STSWR1__Flow_Step_Property__c record : (List<STSWR1__Flow_Step_Property__c>) scope)
			{
				record.Flow__c = record.Flow_Id__c;
			}
			
			STSWR1.AbstractTrigger.Disabled = true;
			update scope;
		}
		catch(Exception ex)
		{
			handleException(scope, ex.getMessage());
		}
	}
	
	global void finish(Database.BatchableContext BC)
	{
		String docName = 'Batch Script 128141 Results';
		String docDevName = 'Batch_Script_128141_Results';
		
		List<Document> docs = [Select Body From Document Where DeveloperName = :docDevName];
		if (docs.isEmpty())
		{
			docs.add(new Document(
					Name = docName,
					DeveloperName = docDevName,
					ContentType = 'text/plain',
					FolderId = [select Id from Folder where Name = 'Work Relay' and Type = 'Document'].Id
				)
			);
		}
		
		Document doc = docs.get(0);
		doc.Body = Blob.valueOf(JSON.serialize(errors));
		if (!Test.isRunningTest()) upsert doc;
	}
	
	public void handleException(List<STSWR1__Flow_Step_Property__c> props, String exMsg)
	{
		Map<String, String> exData = new Map<String, String>{
			'propIds' => JSON.serialize(new Map<ID, STSWR1__Flow_Step_Property__c>(props).keySet()),
			'error' => exMsg
		};
		
		errors.add(JSON.serialize(exData));
	}
}