/** Implements the test for the controller BDT_ApprovalRulesController
 * @author	Dimitrios Sgourdos
 * @version	13-Dec-2013
 */
@isTest
private class BDT_ApprovalRulesControllerTest {

	/** The test is only for code coverage as in controller there are only calls to functions
	 *	from other classes that are already tested.
	 * @author	Dimitrios Sgourdos
	 * @version	13-Dec-2013
	 */
	static testMethod void myUnitTest() {
		BDT_ApprovalRulesController p = new BDT_ApprovalRulesController();
		
		// Call functions for obtaining the code coverage
		p.addApprovalRule();
		p.changedRuleRowIndex = '0';
		p.clearParameters();
		p.deletedRuleRowIndex = '0';
		p.removeApprovalRule();
		p.addApprovalRule();
		p.save();
		p.rulesWrapperList[0].rule.Description__c = 'Test 1';
		p.rulesWrapperList[0].rule.SelectedRule__c = FinancialDocumentApprRuleDataAccessor.PROPOSAL_VALUE_MORE;
		p.rulesWrapperList[0].rule.RuleFirstParameter__c  = '10';
		p.rulesWrapperList[0].rule.RuleSecondParameter__c = 'EUR';
		p.rulesWrapperList[0].serviceCategoriesIdsList.add('Service Category 1');
		p.save(); 
	}
}