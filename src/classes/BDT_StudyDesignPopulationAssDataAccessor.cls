public with sharing class BDT_StudyDesignPopulationAssDataAccessor {

	public static String getSObjectFieldString() {
		return getSObjectFieldString('StudyDesignPopulationAssignment__c');		
	}

	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = '';
		result += referenceName + 'AgregateNumberOfBackups__c,';
		result += referenceName + 'AgregateNumberOfSubjects__c,';
		result += referenceName + 'ClinicalDesignStudyAssignment__c,';
		result += referenceName + 'PopulationStudyAssignment__c,';
		result += referenceName + 'Id';
		return result;
	}

	public static List<StudyDesignPopulationAssignment__c> getList(String whereClause) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM StudyDesignPopulationAssignment__c ' +
								'WHERE  {1}',
								new List<String> {
									getSObjectFieldString(),
									whereClause
								}
							);
		return (List<StudyDesignPopulationAssignment__c>) Database.query(query);
	}

}