public with sharing class BDT_Design {

	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Clinical Design
	public BDT_SecuritiesService.pageSecurityWrapper securitiesB {get;set;} // Study Population
	public BDT_SecuritiesService.pageSecurityWrapper securitiesC {get;set;} // Sample Shipments lab Design
	public BDT_SecuritiesService.pageSecurityWrapper securitiesD {get;set;} // Slots for Analysis lab Design
	public BDT_SecuritiesService.pageSecurityWrapper securitiesE {get;set;} // Slots for MD and Val lab Design
	public String				projectId {get;set;} 
	public list<Study__c> 		studiesList {get;set;}
	public Set<ID> 				studyIDs;
	public list<designWrapper> 	designWrapperList {get;set;}
	// ---------- LaboratoryDesigns variables ----------
	private List<Study__c> 				projectStudiesList;
	// Lab Design Ia: Sample Shipments
	public	List<Study__c> 												  studiesANList				 {get;set;}
	public	List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> labSamplesWrapperList 	 {get;set;}
	public	String														  addSampleIndex	  		 {get;set;}
	public	String														  removeSampleIndex			 {get;set;}
	private List<LaboratorySamples__c>									  labSamplesDeletedList;
	// Lab Design Ib: Slots for Analysis
	public  Boolean 														  showShipmentsPopup		 {get;set;}
	public	List<SelectOption>												  studyAndMethodAnSelectList {get;set;}
	public  List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> labSlotsAnWrapperList		 {get;set;}
	public  List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> labSlotsAnSummaryList		 {get;set;}
	public	List<LaboratorySamplesService.ShipmentSelectionWrapper> 	 	  availableShipmentsForSlots {get;set;}
	public	String															  addAnSlotLineIndex		 {get;set;}
	public	String															  removeAnSlotLineIndex		 {get;set;}
	public  String															  selectedSlotIndex			 {get;set;}
	private List<LaboratoryAnalysisSlots__c> 								  slotsAnDeletedList;
	// Lab Design II: Slots for Method Development and Validation
	public	List<SelectOption>												  studyAndMethodMdValSelectList {get;set;}
	public	List<SelectOption>												  slotTypeSelectList			{get;set;}
	public  List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> labSlotsMdValWrapperList 		{get;set;}
	public	String															  addMdAndValSlotLineIndex		{get;set;}
	public	String															  removeMdAndValSlotLineIndex	{get;set;}
	private List<LaboratoryAnalysisSlots__c> 								  slotsMdAndValDeletedList;
	
	
	public BDT_Design() {
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Clinical Design');
		securitiesB = new BDT_SecuritiesService.pageSecurityWrapper('Study Population');
		securitiesC = new BDT_SecuritiesService.pageSecurityWrapper('Sample Shipments Design');
		securitiesD = new BDT_SecuritiesService.pageSecurityWrapper('Slots AN Design');
		securitiesE = new BDT_SecuritiesService.pageSecurityWrapper('Slots MD and Val Design');
		init();
		projectId = BDT_Utils.getPreference('SelectedProject'); 
		readStudies();
		buildDesignWrapper();
		readSubjectAssignment();
		initializeLaboratoryDesigns();
		renderDesignDisplay = false;
	}
	
	
	/** Initialize global variables
	 * @author	Dimitrios Sgourdos
	 * @version	13-Nov-2013
	 */
	private void init() {
		projectStudiesList = new List<Study__c>();
		// Lab Design Ia
		studiesANList		  = new List<Study__c>();
		labSamplesWrapperList = new List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
		labSamplesDeletedList = new List<LaboratorySamples__c>();
		// Lab Design Ib
		studyAndMethodAnSelectList = new List<SelectOption>();
		labSlotsAnWrapperList	   = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		labSlotsAnSummaryList	   = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		availableShipmentsForSlots = new List<LaboratorySamplesService.ShipmentSelectionWrapper>();
		slotsAnDeletedList		   = new List<LaboratoryAnalysisSlots__c>();
		showShipmentsPopup		   = false;
		// Lab Design II
		studyAndMethodMdValSelectList = new List<SelectOption>();
		slotTypeSelectList		 	  = new List<SelectOption>();
		labSlotsMdValWrapperList 	  = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		slotsMdAndValDeletedList 	  = new List<LaboratoryAnalysisSlots__c>();
	}
	
	
	public PageReference copyDesign (){
		return new pagereference(System.Page.BDT_DesignCopy.getURL() );
	}
	
	public PageReference AddDesign (){
		return new pagereference(System.Page.BDT_NewEditDesign.getURL() );
	}
	
/***************************Study designs**********************************/	
	
	public void readStudies () {
		studiesList = BDT_Utils.readSelectedStudies();
		studyIDs = new Set<ID>();
		for (Study__c s: studiesList){
			studyIDs.add(s.id);
		}
	}
	
	public void buildDesignWrapper() {
		designWrapperList = new List<designWrapper>();
		designWrapper DesignW;
		studyWrapper  StudyW;
		List<studyWrapper> StudyWList = new List<studyWrapper>();
		
		Map<String,ClinicalDesignStudyAssignment__c> CdsaM = New Map<String,ClinicalDesignStudyAssignment__c>();
		For (ClinicalDesignStudyAssignment__c CDSA:[Select Id, Study__c, ClinicalDesign__c 
													  From ClinicalDesignStudyAssignment__c
													 where study__c in :studyIDs]){
			CdsaM.put(''+CDSA.ClinicalDesign__c+':'+CDSA.Study__c,CDSA);
		}

		for (ClinicalDesign__c CD: [Select Id, Design_Name__c, Name 
									  From ClinicalDesign__c 
									 where Project__c = :projectId]) {
			StudyWList = new List<studyWrapper>();							       	
			for (Study__c Study : studiesList) {
				if (CdsaM.containsKey(''+CD.ID+':'+Study.ID)) {
					StudyW = new studyWrapper(CdsaM.get(''+CD.ID+':'+Study.ID).id,CD.ID,Study.ID,true,false);
				} else {
					StudyW = new studyWrapper(null,CD.ID,Study.ID,false,false);
				}
				StudyWList.add(StudyW); 
			}
			DesignW = new designWrapper(CD,StudyWList);
			designWrapperList.add(DesignW);
			system.debug(DesignW);
		}
	}
	
	public PageReference saveDesignWrapper () {
		
		Set<ID> deleteIDs = new Set<ID>();
		List<ClinicalDesignStudyAssignment__c> newItems = new List<ClinicalDesignStudyAssignment__c>();
		
		for (designWrapper dw:designWrapperList) {
			for (studyWrapper sw:dw.studyWrapperList) {
				if (!sw.selected && sw.ClinicalDesignStudyAssignmentId <> null) {
					deleteIDs.add(sw.ClinicalDesignStudyAssignmentId);
					sw.ClinicalDesignStudyAssignmentId = null;
				} else if (sw.selected && sw.ClinicalDesignStudyAssignmentId == null) {
					newItems.add(new ClinicalDesignStudyAssignment__c(Study__c = sw.studyId, ClinicalDesign__c = sw.designId)); 
				}
			}
		}
		
		if (deleteIDs.size() > 0) {
			
			delete  [select id
			           from ClinicalDesignStudyAssignment__c
			          where id in: deleteIDs];				   
		}
		
		if (newItems.size() > 0) {
			insert newItems;
		}
		
		buildDesignWrapper();	
		readSubjectAssignment();
		return null; 	
	}
	
	public PageReference editClinicDesign(){		
		String editDesignId =  System.CurrentPageReference().getParameters().get('editDesignId');  
		PageReference Ref   = new PageReference(System.Page.bdt_neweditdesign.getUrl()); 
		ref.getParameters().put('designId',editDesignId);
		//designId =  BDT_Utils.getPreference('designId');
		return Ref;
	}
	
	public class designWrapper{
		public ClinicalDesign__c designRecord	{get; set;}		
		public List<studyWrapper> studyWrapperList {get; set;}
		
		public designWrapper(ClinicalDesign__c sDesignRecord, List<studyWrapper> sStudyWrapperList){
			designRecord = sDesignRecord;			
			studyWrapperList = sStudyWrapperList;
		}
	}
	
	public class studyWrapper{
		public String  designId	{get; set;}
		public String  ClinicalDesignStudyAssignmentId	{get; set;}
		public String  studyId	{get; set;}		
		public boolean selected {get; set;}
		public boolean disabled {get; set;}
		
		public studyWrapper(String sClinicalDesignStudyAssignmentId,String sDesignId, String sStudyId, boolean sSelected, boolean sDisabled){
			ClinicalDesignStudyAssignmentId = sClinicalDesignStudyAssignmentId;
			designId = sDesignId;
			studyId = sStudyId;			
			selected = sSelected;
			disabled = sDisabled;
		}
	}
	
/****************************Clinical design layout display****************************/

	public boolean 									renderDesignDisplay {get;set;}
	public List<Epoch__c> 							epochList 			{get;set;}
	public List<BDT_FlowchartDisplay.armWrapper> 	armWrapperList 		{get;set;}
	public ClinicalDesign__c 						design				{get;set;}

	public void 	showClinicDesign() {
		renderDesignDisplay = true;
		String DesignId =  System.CurrentPageReference().getParameters().get('designId');
		armWrapperList = BDT_FlowchartDisplay.getFlowchart(DesignId, projectId);   
		epochList = BDT_FlowchartDisplay.getEpochHeaderList(DesignId);
		design = BDT_Utils.getDesign(DesignId, projectid);		
	}
		
	public void 	hideDesignContainer() {
		renderDesignDisplay = false;
	}

	
/******************************Subject Assignment**************************/

	public List<saHeaderWrapper> 	saHeader 	{get;set;}
	public List<saBodyWrapper> 		saBody		{get;set;}
	
	public void readSubjectAssignment() {
		saHeader = new List<saHeaderWrapper>();
		saHeaderWrapper saHeaderItem;
		List<ClinicalDesignStudyAssignment__c> cdsaList;
		for (Study__c study :[Select Id
								   , Code__c
								   , (Select Id, 
								             ClinicalDesign__r.name
								        From ClinicalDesignStudyAssignments__r
								       order by ClinicalDesign__r.name ) 
								From Study__c s
							   Where id in :StudyIDs
							   order by code__c]) {
							   	
			cdsaList = study.ClinicalDesignStudyAssignments__r;
			if (cdsaList.size() == 0) {
				saHeaderItem = new saHeaderWrapper();
				saHeaderItem.study = study;
				saHeaderItem.EditVisible = false;
				saHeader.add(saHeaderItem);
			}
			for (ClinicalDesignStudyAssignment__c cdsa : study.ClinicalDesignStudyAssignments__r) {
				saHeaderItem = new saHeaderWrapper();
				saHeaderItem.study = study;
				saHeaderItem.EditVisible = false;
				saHeaderItem.ClinicalDesignStudyAssignment = cdsa;
				saHeader.add(saHeaderItem);				
			}
		}

		Map <String,PopulationStudyAssignment__c> psaMap = new Map <String,PopulationStudyAssignment__c>();
		for (PopulationStudyAssignment__c psa : [Select Population__c
														  	, Id
															, AggregateNumberOfSubjects__c
														  	, AggregateNumberOfBackups__c 
															, study__c
														 From PopulationStudyAssignment__c
														where study__c in :StudyIDs
													   order by study__r.code__c, Population__r.Name]) {
			psaMap.put(''+psa.study__c+':'+psa.Population__c,psa);	
		}
		
		Map <String,StudyDesignPopulationAssignment__c> sdpaMap = new Map <String,StudyDesignPopulationAssignment__c>();
		for (StudyDesignPopulationAssignment__c sdpa : [Select Id, 
															   PopulationStudyAssignment__c,
															   ClinicalDesignStudyAssignment__c,
															   AgregateNumberOfSubjects__c,
															   AgregateNumberOfBackups__c 
														  From StudyDesignPopulationAssignment__c s
													 	 where PopulationStudyAssignment__r.Study__c in :StudyIDs]) {
			sdpaMap.put(''+sdpa.PopulationStudyAssignment__c+':'+sdpa.ClinicalDesignStudyAssignment__c,sdpa);
		}
		
		saBody = new List<saBodyWrapper>();
		saBodyWrapper saBodyItem;
		saBodyWrapperInt saBodyIntItem;
		
		// loop through populations
		for (Population__c population : [Select ScreeningEfficiency__c, Name, Id 
										   From Population__c
										  where BDTDeleted__c != true
										    and id in (select population__c
										                 from PopulationStudyAssignment__c
										                where study__c in :StudyIDs)]){
		
			saBodyItem = new saBodyWrapper();
			saBodyItem.Population = population;	
			saBodyItem.saBodyInt = new List<saBodyWrapperInt>();
			
			// loop through header
			for (saHeaderWrapper saHI : saHeader) {
				saBodyIntItem = new saBodyWrapperInt();
				
				// check if a checkbox is to be displayed for population
				if (saHI.ClinicalDesignStudyAssignment != null &&
					psaMap.containsKey(''+saHI.study.id+':'+population.id)) {
					saBodyIntItem.checkboxVisible = true;
					saBodyIntItem.ClinicalDesignStudyAssignment = saHI.ClinicalDesignStudyAssignment;	
					saBodyIntItem.PopulationStudyAssignment = psaMap.get(''+saHI.study.id+':'+population.id); 				
					// check if checkbox should be checked
					if (sdpaMap.containsKey(''+psaMap.get(''+saHI.study.id+':'+population.id).id+':'+saHI.ClinicalDesignStudyAssignment.id)) {
						saBodyIntItem.checkboxChecked = true;
						saBodyIntItem.StudyDesignPopulationAssignment = sdpaMap.get(''+psaMap.get(''+saHI.study.id+':'+population.id).id+':'+saHI.ClinicalDesignStudyAssignment.id);
						saHI.EditVisible = true;
					} else {
						saBodyIntItem.checkboxChecked = false;
					}
				} else {
					saBodyIntItem.checkboxVisible = false;
				}
				saBodyItem.saBodyInt.add(saBodyIntItem);
			}
			saBody.add(saBodyItem);			
		}					   	
	}	
	
	public void saveStudyPopulation () {

		Set<ID> deleteIDs = new Set<ID>();
		List<StudyDesignPopulationAssignment__c> insertList = new List<StudyDesignPopulationAssignment__c>();
		StudyDesignPopulationAssignment__c SDPA;
		
		for (saBodyWrapper saBodyItem : saBody){
			for (saBodyWrapperInt saBodyIntItem : saBodyItem.saBodyInt) {

				if (saBodyIntItem.checkboxVisible) {
					if (saBodyIntItem.checkboxChecked && saBodyIntItem.StudyDesignPopulationAssignment == null) {
						// new checked
						SDPA = new StudyDesignPopulationAssignment__c();
						SDPA.PopulationStudyAssignment__c = saBodyIntItem.PopulationStudyAssignment.id;
						SDPA.ClinicalDesignStudyAssignment__c = saBodyIntItem.ClinicalDesignStudyAssignment.id;
						insertList.add(SDPA);
					} else if (!saBodyIntItem.checkboxChecked && saBodyIntItem.StudyDesignPopulationAssignment != null) {
						// new unchecked
						deleteIDs.add(saBodyIntItem.StudyDesignPopulationAssignment.id);
					}
				}
			}
		}
		
		if (deleteIDs.size()>0){
			delete [select id 
					from StudyDesignPopulationAssignment__c 
					where id in :deleteIDs];
		}	
		
		if (insertList.size()>0){
			insert insertList;
		}
		
		// Sanitize the data these objects should not be semi- or fully orphaned
		delete [select id 
				from GroupArmAssignment__c 
				where arm__c = null 
				or 	StudyDesignPopulationAssignment__c  = null];
		delete [select id 
				from StudyDesignPopulationAssignment__c 
				where ClinicalDesignStudyAssignment__c = null 
				or PopulationStudyAssignment__c = null];
		
		readSubjectAssignment();
	}
	
	public PageReference EditGroupArmAssignment() {
		String cdaID = ApexPages.currentPage().getParameters().get('ClinicalDesignStudyAssignmentID');
		PageReference myRef   = new PageReference(System.Page.BDT_NewEditGroupArmAssignment.getUrl()); 
		myRef.getParameters().put('ClinicalDesignStudyAssignmentID',cdaID);
		return myRef;
	}
	
	public class saHeaderWrapper {
		public Study__c study {get;set;}
		public ClinicalDesignStudyAssignment__c ClinicalDesignStudyAssignment {get;set;}
		public boolean EditVisible {get;set;}
	}		
	
	public class saBodyWrapper {
		public Population__c Population {get;set;}
		public List<saBodyWrapperInt>  saBodyInt {get;set;}
	}
	
	public class saBodyWrapperInt {
		public Boolean checkboxVisible {get;set;}
		public Boolean checkboxChecked {get;set;}
		public PopulationStudyAssignment__c PopulationStudyAssignment {get;set;}
		public ClinicalDesignStudyAssignment__c ClinicalDesignStudyAssignment {get;set;}
		public StudyDesignPopulationAssignment__c StudyDesignPopulationAssignment {get;set;}
	} 
	
	
	/****************************Laboratory Designs****************************/
	
	/** Initialize the Lab Designs.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 */
	private void initializeLaboratoryDesigns() {
		projectStudiesList = StudyService.getStudiesByProjectId(projectId);
		initializeSampleShipmentsLabDesign();
		initializeSlotsForAnLabDesign();
		initializeSlotsForMdAndValLabDesign();
	}
	
	
	/** Initialize the Lab Design Ia:Sample Shipments.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 */
	private void initializeSampleShipmentsLabDesign() {
		String listOfStudiesIds = BDT_Utils.createConcatinatedStringFromSobject(projectStudiesList, 'Id');
		studiesANList = StudyService.getStudiesByUsedInAN(listOfStudiesIds);
		labSamplesWrapperList = LaboratorySamplesService.getLaboratorySamplesDesignContainer(projectId, studiesANList);
	}
	
	
	/** Initialize the Lab Design Ib:Slots for Analysis.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 */
	private void initializeSlotsForAnLabDesign() {
		List<LaboratoryAnalysis__c> tmpLabAnalysisList = new List<LaboratoryAnalysis__c>();
		tmpLabAnalysisList = LaboratoryAnalysisService.getLabAnalysisByAnalysisAndUserStudies(projectStudiesList);
		studyAndMethodAnSelectList = LaboratoryAnalysisService.getStudyAndMethodSelectOptions(tmpLabAnalysisList);
		labSlotsAnWrapperList = LaboratoryAnalysisSlotsService.getLaboratorySlotsForLabDesigns(projectStudiesList, false);
		labSlotsAnSummaryList = LaboratoryAnalysisSlotsService.summarizeLabSlotsDesignByStudyMethodCombination(labSlotsAnWrapperList, studyAndMethodAnSelectList);
		availableShipmentsForSlots = LaboratorySamplesService.getAvailableShipmentSelection(projectId, projectStudiesList);
	}
	
	
	/** Initialize the Lab Design II:Slots for Method Development and Validation.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 */
	private void initializeSlotsForMdAndValLabDesign() {
		LaboratoryAnalysisSlots__c tmpItem = new LaboratoryAnalysisSlots__c();
		slotTypeSelectList = BDT_Utils.getSelectOptionsFromPickList(tmpItem, 'SlotType__c', 'Please select...', false);
		List<LaboratoryAnalysis__c> tmpLabAnalysisList = new List<LaboratoryAnalysis__c>();
		tmpLabAnalysisList = LaboratoryAnalysisService.getLabAnalysisByMdOrValAndUserStudies(projectStudiesList);
		studyAndMethodMdValSelectList = LaboratoryAnalysisService.getStudyAndMethodSelectOptions(tmpLabAnalysisList);
		labSlotsMdValWrapperList = LaboratoryAnalysisSlotsService.getLaboratorySlotsForLabDesigns(projectStudiesList, true);
	}
	
	
	/** Add a shipment in the sample shipments lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 31-Oct-2013
	 */
	public void addShipmentToLabDesign() {
		labSamplesWrapperList = LaboratorySamplesService.addShipmentToLabDesign(labSamplesWrapperList, studiesANList, projectId);
	}
	
	
	/** Add a sample in the sample shipments lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 31-Oct-2013
	 */
	public void addSampleToLabDesign() {
		labSamplesWrapperList = LaboratorySamplesService.addSampleToLabDesign(labSamplesWrapperList, Integer.valueOf(addSampleIndex), studiesANList, projectId);
	}
	
	
	/** Remove a sample from the sample shipments lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 04-Nov-2013
	 */
	public void removeSampleFromLabDesign() {
		labSamplesDeletedList.add(labSamplesWrapperList[Integer.valueOf(removeSampleIndex)].labSample);
		labSamplesWrapperList = LaboratorySamplesService.removeSampleFromLabdesign(labSamplesWrapperList, Integer.valueOf(removeSampleIndex));
	}
	
	
	/** Reset the sample shipments lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 31-Oct-2013
	 */
	public void resetSampleDesign() {
		labSamplesDeletedList = new List<LaboratorySamples__c>();
		labSamplesWrapperList = LaboratorySamplesService.getLaboratorySamplesDesignContainer(projectId, studiesANList);
	}
	
	
	/** Save the sample shipments lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 14-Nov-2013
	 */
	public void saveSampleDesign() {
		// Clone the shipment numbers to a temporary wrapper list and recalcultate it
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> tmpClonedWrapperList = LaboratorySamplesService.cloneSamplesWrapperListShipmentNumbers(labSamplesWrapperList);
		List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> tmpShipNumsWrapperList = LaboratorySamplesService.getUpdatedFirstSamplesRecordsFromSamplesDesign(tmpClonedWrapperList);
		// Retrieve the data
		List<LaboratorySamples__c> updateList = LaboratorySamplesService.extractLabSamplesFromLabDesign(labSamplesWrapperList);
		labSamplesDeletedList.addAll(LaboratorySamplesService.getSamplesWithoutStudySelection(updateList));
		// Save and delete samples
		Boolean successFlag = LaboratorySamplesService.saveLaboratorySamples(updateList);
		if( ! successFlag ) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The save was unsuccessful! Please, try again!');
			ApexPages.addMessage(msg);
			return;
		} else {
			successFlag = LaboratorySamplesService.deleteLaboratorySamples(labSamplesDeletedList);
			if( ! successFlag ) {
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The deletion of laboratory samples was unsuccessful! Please, try again!');
				ApexPages.addMessage(msg);
				return;
			}
		}
		LaboratoryAnalysisSlotsService.updateListOfShipmentsInLabSlots(projectStudiesList, tmpShipNumsWrapperList);
		resetSampleDesign();
		availableShipmentsForSlots = LaboratorySamplesService.getAvailableShipmentSelection(projectId, projectStudiesList);
		resetSlotAnDesign();
	}
	
	
	/** Add a slot in the the slots for analysis lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Nov-2013
	 */
	public void addSlotAnDesign() {
		labSlotsAnWrapperList = LaboratoryAnalysisSlotsService.addSlotToLabDesign(labSlotsAnWrapperList, false);
		calculateSummary(); // Refresh the summary in rerender
	}
	
	
	/** Add a study/method combination for a slot in the slots for analysis lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Nov-2013
	 */
	public void addSlotCombinationAnDesign() {
		labSlotsAnWrapperList = LaboratoryAnalysisSlotsService.addSlotCombinationLabDesign(labSlotsAnWrapperList, Integer.valueOf(addAnSlotLineIndex), false);
		calculateSummary();
	}
	
	
	/** Remove a study/method combination from a slot in the slots for analysis lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Nov-2013
	 */
	public void removeSlotCombinationAnDesign() {
		slotsAnDeletedList.add(labSlotsAnWrapperList[Integer.valueOf(removeAnSlotLineIndex)].labSlot);
		labSlotsAnWrapperList = LaboratoryAnalysisSlotsService.removeSlotCombinationFromLabDesign(labSlotsAnWrapperList, Integer.valueOf(removeAnSlotLineIndex));
		calculateSummary();
	}
	
	
	/** Open the popup for selecting shipment for the current slot.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 */
	public void selectShipments() {
		availableShipmentsForSlots = LaboratorySamplesService.updateShipmentSelectionBySelectedInSlot(availableShipmentsForSlots, labSlotsAnWrapperList[Integer.valueOf(selectedSlotIndex)].labSlot);
		showShipmentsPopup = true;
	}
	
	
	/** Cancel the selection of shipments for the current slot.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 */
	public void cancelSelectShipments() {
		showShipmentsPopup = false;
	}
	
	
	/** Save the selection of shipments for the current slot.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 */
	public void saveSelectShipments() {
		labSlotsAnWrapperList[Integer.valueOf(selectedSlotIndex)].labSlot = LaboratorySamplesService.saveShipmentsAssociationToSlot(availableShipmentsForSlots, labSlotsAnWrapperList[Integer.valueOf(selectedSlotIndex)].labSlot);
		showShipmentsPopup = false;
	}
	
	
	/** Summarize the Slots for Analysis Lab Design by the study-method combinations. 
	 * @author	Dimitrios Sgourdos
	 * @version 11-Nov-2013
	 */
	public void calculateSummary() {
		labSlotsAnSummaryList = LaboratoryAnalysisSlotsService.summarizeLabSlotsDesignByStudyMethodCombination(labSlotsAnWrapperList, studyAndMethodAnSelectList);
	}
	
	
	/** Reset the slots for analysis lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Nov-2013
	 */
	public void resetSlotAnDesign() {
		slotsAnDeletedList = new List<LaboratoryAnalysisSlots__c>();
		labSlotsAnWrapperList = LaboratoryAnalysisSlotsService.getLaboratorySlotsForLabDesigns(projectStudiesList, false);
		calculateSummary();
	}
	
	
	/** Save the slots for analysis lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 13-Nov-2013
	 */
	public void saveSlotAnDesign() {
		// Validate and retrieve the data
		if(LaboratoryAnalysisSlotsService.validateLabSlotsAnDesign(labSlotsAnWrapperList)) {
			List<LaboratoryAnalysisSlots__c> updateList = LaboratoryAnalysisSlotsService.extractLabAnalysisSlotsFromLabDesign(labSlotsAnWrapperList);
			// Save and delete slots
			Boolean successFlag = LaboratoryAnalysisSlotsService.saveLaboratoryAnalysisSlots(updateList);
			if( ! successFlag ) {
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The save was unsuccessful! Please, try again!');
				ApexPages.addMessage(msg);
				return;
			} else {
				successFlag = LaboratoryAnalysisSlotsService.deleteLaboratoryAnalysisSlots(slotsAnDeletedList);
				if( ! successFlag ) {
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The deletion of laboratory analysis slots was unsuccessful! Please, try again!');
					ApexPages.addMessage(msg);
					return;
				}
			}
		} else {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please, select shipments and Study/Method for all the slots and try to save again!');
			ApexPages.addMessage(msg);
			return;
		}
		resetSlotAnDesign();
	}
	
	
	/** Add a slot in the the slots for method development and validation lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Nov-2013
	 */
	public void addSlotMdAndValDesign() {
		labSlotsMdValWrapperList = LaboratoryAnalysisSlotsService.addSlotToLabDesign(labSlotsMdValWrapperList, true);
	}
	
	
	/** Add a study/method combination for a slot in the slots for method development and validation lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 12-Nov-2013
	 */
	public void addSlotCombinationMdAndValDesign() {
		labSlotsMdValWrapperList = LaboratoryAnalysisSlotsService.addSlotCombinationLabDesign(labSlotsMdValWrapperList, Integer.valueOf(addMdAndValSlotLineIndex), true);
	}
	
	
	/** Remove a study/method combination from a slot in the slots for method development and validation lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 07-Nov-2013
	 */
	public void removeSlotCombinationMdAndValDesign() {
		slotsMdAndValDeletedList.add(labSlotsMdValWrapperList[Integer.valueOf(removeMdAndValSlotLineIndex)].labSlot);
		labSlotsMdValWrapperList = LaboratoryAnalysisSlotsService.removeSlotCombinationFromLabDesign(labSlotsMdValWrapperList, Integer.valueOf(removeMdAndValSlotLineIndex));
	}
	
	
	/** Reset the slots for method development and validation lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 07-Nov-2013
	 */
	public void resetSlotMdAndValDesign() {
		slotsMdAndValDeletedList = new List<LaboratoryAnalysisSlots__c>();
		labSlotsMdValWrapperList = LaboratoryAnalysisSlotsService.getLaboratorySlotsForLabDesigns(projectStudiesList, true);
	}
	
	
	/** Save the slots for method development and validation lab design.
	 * @author	Dimitrios Sgourdos
	 * @version 08-Nov-2013
	 */
	public void saveSlotMdAndValDesign() {
		// Validate and retrieve the data
		if(LaboratoryAnalysisSlotsService.validateLabSlotsMdAndValDesign(labSlotsMdValWrapperList)) {
			List<LaboratoryAnalysisSlots__c> updateList = LaboratoryAnalysisSlotsService.extractLabAnalysisSlotsFromLabDesign(labSlotsMdValWrapperList);
			// Save and delete slots
			Boolean successFlag = LaboratoryAnalysisSlotsService.saveLaboratoryAnalysisSlots(updateList);
			if( ! successFlag ) {
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The save was unsuccessful! Please, try again!');
				ApexPages.addMessage(msg);
				return;
			} else {
				successFlag = LaboratoryAnalysisSlotsService.deleteLaboratoryAnalysisSlots(slotsMdAndValDeletedList);
				if( ! successFlag ) {
					ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'The deletion of laboratory analysis slots was unsuccessful! Please, try again!');
					ApexPages.addMessage(msg);
					return;
				}
			}
		} else {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please, select Study/Method and Type for all the slots and try to save again!');
			ApexPages.addMessage(msg);
			return;
		}
		resetSlotMdAndValDesign();
	}
}