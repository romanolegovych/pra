global virtual without sharing class PBB_Single_PageCont {
    
    public PBB_Single_PageCont() {   
    }
    
    @RemoteAction
    global static List<PRA_WrappersForMobile.WrapperSite> getStatusBySite(String protocolUniqKey){
    	return PC_Utils.getStatusBySite(protocolUniqKey);
    }
    
    @RemoteAction
    global static List<PRA_WrappersForMobile.WrapperCountry> getStatusByCountry(String protocolUniqKey){
        List<PRA_WrappersForMobile.WrapperCountry> wrappedCountries = new List<PRA_WrappersForMobile.WrapperCountry>();
        for (PBB_WR_APIs.WrapperCountry country : PBB_WR_APIs.getPatientEnrollmntStatusByCountry(protocolUniqKey) ){
            wrappedCountries.add( new PRA_WrappersForMobile.WrapperCountry(
                    country.country,
                    country.countryData
            ) );
        }
        return wrappedCountries;
    }
    
    @RemoteAction
    global static List<PAWS_ProjectDetailCVO> getProjectsDetails(){
    	List<PAWS_ProjectDetailCVO> projectDetails = PAWS_API.PBB_getActiveProjectList( null );
    	//PC_Utils.convertAllDateTimeValuesToLocalTime(projectDetails);
        return projectDetails;
    }

    @RemoteAction
    global static PAWS_ProjectDetailCVO getProjectDetail( Id projectId ){
        PAWS_ProjectDetailCVO pr = PAWS_API.PBB_getProjectDetails( projectId );
        //PC_Utils.convertAllDateTimeValuesToLocalTime( new List<PAWS_ProjectDetailCVO>{pr} );
        return pr;
    }

    @RemoteAction
    global static PRA_WrappersForMobile.OperationResult getWeekEventsByProject(String protocolUniqKey){
        System.debug('--- protocolUniqKey' + protocolUniqKey);
        try{
            List<PRA_WrappersForMobile.CountryWeeklyEventsWrapper> wrappedEvents = new List<PRA_WrappersForMobile.CountryWeeklyEventsWrapper>();
            for (PBB_WR_APIs.CountryWeeklyEvents weeklyEvent : PBB_WR_APIs.weekEventsByProject(protocolUniqKey) ){
                wrappedEvents.add( new PRA_WrappersForMobile.CountryWeeklyEventsWrapper(weeklyEvent) );
            }
            return new PRA_WrappersForMobile.OperationResult(wrappedEvents);
        }catch(Exception e){
            return new PRA_WrappersForMobile.OperationResult( e.getMessage() );
        }
    }
    
    /**
    * @description  Returns patient enrollment attributes 
    */
    @RemoteAction
    global static PRA_WrappersForMobile.PatientEnrollmentWrapperGlobal getPatientEnrollmentAttributes(String protocolUniqueKey) {
        try{
            return new PRA_WrappersForMobile.PatientEnrollmentWrapperGlobal( PBB_WR_APIs.GetPatientEnrollmentAttributes(protocolUniqueKey) );
         }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return null;
        }
    }
 
    /**
    * @description Sends mail 
    */
    @RemoteAction
    global static PRA_WrappersForMobile.OperationResult sendEmail(
    		String projectManagerUserId,
    		String subject,
    		String body
    ){
        try{
        	//System.debug('--- subject: ' + subject);
        	//System.debug('--- body: ' + body);
        	Id managerId = PC_Utils.getIdFromString(projectManagerUserId, 'Project manager Id');
        	Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        	mail.setSubject(subject);
        	mail.setHtmlBody( PC_Utils.getTrimmedBody(body) );
        	mail.setTargetObjectId(managerId);
        	//String userEmailAddress = retrieveCurrentUser().Email;
        	//mail.setCcAddresses( new String[]{userEmailAddress, 'khladkyi@corevalue.net', 'rkuleshov@corevalue.net'} );
        	mail.setSaveAsActivity(false);
        	Messaging.sendEmail( new List<Messaging.SingleEmailMessage>{ mail } );
            return new PRA_WrappersForMobile.OperationResult();
        }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return new PRA_WrappersForMobile.OperationResult( e.getMessage() );
        }
    }
    
    @RemoteAction
    global static PRA_WrappersForMobile.UserData getUserData(){
        try{
            return new PRA_WrappersForMobile.UserData( PC_Utils.retrieveCurrentUser() );
        }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return new PRA_WrappersForMobile.UserData( e.getMessage() );
        }
    }
    
    @RemoteAction
    global static String getUserPreferrences(String projectId, String applicationName){
        try{
        	return UserServices.getUserPreferrences(projectId, applicationName);
            //return new PRA_WrappersForMobile.EstimatorData( retrieveCurrentUser() );
        }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return null; //new PRA_WrappersForMobile.EstimatorData( e.getMessage() );
        }
    }
    
    @RemoteAction
    global static PRA_WrappersForMobile.OperationResult saveUserPreferrences(String projectId, String applicationName, String value){
        try{
        	UserServices.saveUserPreferrences(projectId, applicationName, value);
            return new PRA_WrappersForMobile.OperationResult( (List<Object>)null );
        }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return new PRA_WrappersForMobile.OperationResult( e.getMessage() );
        }
    }
    
    /**
            ------------------------------ Begin of NCM functionality -------------------------------
    */
    
    @RemoteAction
    global static PRA_WrappersForMobile.OperationResult saveUserPhone( String phone ){
        try{
            performSavePhone(phone);
            return new PRA_WrappersForMobile.OperationResult();
        }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return new PRA_WrappersForMobile.OperationResult( e.getMessage() );
        }
    }
    
    private static void performSavePhone(String phoneNumber){
        PRA_WrappersForMobile.UserData ui = getUserData();
        if ( phoneNumber != ui.phone ){
            User u = new User(
                    Id = UserInfo.getUserId(),
                    Phone = phoneNumber
            );
            update u;
        }
    }
    
    @RemoteAction
    global static PRA_WrappersForMobile.OperationResult getRegistrationsForAllProtocols(){
        try{
            return new PRA_WrappersForMobile.OperationResult( NCM_API.buildAllRegistrationsForNotifications( UserInfo.getUserId(), null, NCM_API_DataTypes.PROTOCOL_NOTIFICATION) );
        }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return new PRA_WrappersForMobile.OperationResult( e.getMessage() );
        }
    }
    
    @RemoteAction
    global static PRA_WrappersForMobile.OperationResult getRegistrationsForProtocol(String protocolId){
        try{
            System.debug('getRegistrationsForProtocol. protocolId: ' + protocolId);
            if ( String.isEmpty(protocolId) ){
                throw new CheckException('Passed protocol id value is empty.');
            }
            return new PRA_WrappersForMobile.OperationResult( NCM_API.buildAllRegistrationsForNotifications( UserInfo.getUserId(), protocolId, NCM_API_DataTypes.PROTOCOL_NOTIFICATION) );
            //PRA_WrappersForMobile.OperationResult r = new PRA_WrappersForMobile.OperationResult( NCM_API.buildAllRegistrationsForNotifications( UserInfo.getUserId(), protocolId, NCM_API_DataTypes.PROTOCOL_NOTIFICATION) );
            //return r;
        }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return new PRA_WrappersForMobile.OperationResult( e.getMessage() );
        }
    }
    
    @RemoteAction
    global static PRA_WrappersForMobile.RegistrationsPerProtocolResult registerForNotifications( List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrations, List<String> protocolIds ){
    	return PC_Utils.registerForNotifications( registrations, protocolIds );
    }
    
    @RemoteAction
    global static PRA_WrappersForMobile.RegistrationsPerProtocolResult getNumberOfActiveRegistrationsPerRelatedObjectByUser(){
        try{
            return new PRA_WrappersForMobile.RegistrationsPerProtocolResult( PC_Utils.getNumberOfActiveRegistrationsPerRelatedObjectByCurrentUser(), null );
        }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return new PRA_WrappersForMobile.RegistrationsPerProtocolResult( e.getMessage() );
        }
    }
    
    @RemoteAction
    global static PRA_WrappersForMobile.OperationResult getPendingNotificationsByUser(){
        try{
            List<NCM_API_DataTypes.NotificationWrapper> notifications = NCM_API.getPendingNotificationsByUser( UserInfo.getUserId() );
            return new PRA_WrappersForMobile.OperationResult( PC_Utils.populateNotificationsWithProtocolNames(notifications) );
        }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return new PRA_WrappersForMobile.OperationResult( e.getMessage() );
        }
    }
    
    @RemoteAction
    global static PRA_WrappersForMobile.OperationResult getPendingNotificationsByUserAndRelatedObject(String protocolId){
        try{
            return new PRA_WrappersForMobile.OperationResult( NCM_API.getPendingNotificationsByUserAndRelatedObject( UserInfo.getUserId(), protocolId ) );
        }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return new PRA_WrappersForMobile.OperationResult( e.getMessage() );
        }
    }
    
    @RemoteAction
    global static PRA_WrappersForMobile.NotificationsPerProtocolResult getNumberOfPendingNotificationsPerRelatedObjectByUser(){
        try{
            return new PRA_WrappersForMobile.NotificationsPerProtocolResult( NCM_API.getNumberOfPendingNotificationsPerRelatedObjectByUser( UserInfo.getUserId() ) );
        }catch(Exception e){
            System.debug(LoggingLevel.Error, '--- Exception: ' + e);
            return new PRA_WrappersForMobile.NotificationsPerProtocolResult( e.getMessage() );
        }
    }
    
    @RemoteAction
    global static PRA_WrappersForMobile.NotificationsPerProtocolResult acknowledgePendingNotifications(List<Id> notificationIds){
        if ( !notificationIds.isEmpty() ){
            try{
                List<NCM_API_DataTypes.NotificationWrapper> nWrappers = new List<NCM_API_DataTypes.NotificationWrapper>();
                for (Id nId : notificationIds){
                    NCM_API_DataTypes.NotificationWrapper nw = new NCM_API_DataTypes.NotificationWrapper();
                    nw.recordId = nId;
                    nWrappers.add(nw);
                }
                NCM_API.acknowledgePendingNotifications( nWrappers );
            }catch(Exception e){
                System.debug(LoggingLevel.Error, '--- Exception: ' + e);
                return new PRA_WrappersForMobile.NotificationsPerProtocolResult( e.getMessage() );
            }
        }
        return getNumberOfPendingNotificationsPerRelatedObjectByUser();
    }
    
    /*private static void checkRegistrationsBeforeSave(List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrations, String emailAddress, String phoneNumber){
        for ( NCM_API_DataTypes.NotificationRegistrationWrapper reg : registrations ){
            if (reg.active != true){
                continue;
            }
            if ( reg.notifyByEmail == true && String.isEmpty( emailAddress) ){
                throw new CheckException('Email is empty. So notifiying by Email is impossible for registration: ' + reg.recordId);
            }
            if ( reg.notifyBySMS == true && String.isEmpty( phoneNumber) ){
                throw new CheckException('Phone number is empty. So notifiying by SMS is impossible for registration: ' + reg.recordId);
            }
        }
    }*/   
    
    private class CheckException extends Exception {}
    
    /**
            ------------------------------ End of NCM functionality -------------------------------
    */
    
   
}