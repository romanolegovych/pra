/** Implements the test for the Service Layer of the object LaboratoryMethod__c
 * @author	Dimitrios Sgourdos
 * @version	19-Nov-2013
 */
@isTest
private class LaboratoryMethodServiceTest {
	
	/** Test the function allRequiredFieldsExistent
	 * @author	Dimitrios Sgourdos
	 * @version	18-Oct-2013
	 */
	static testMethod void allRequiredFieldsExistentTest() {
		LaboratoryMethod__c labMethod = new LaboratoryMethod__c();
		
		// One field is missing (AnalyticalTechnique__c)
		labMethod.Species__c	 = 'test species'; 
		labMethod.Matrix__c		 = 'test matrix';
		labMethod.Location__c	 = 'test location'; 
		labMethod.Department__c	 = 'test department'; 
		labMethod.Proprietary__c = 'Y';
		
		Boolean allExistFlag = LaboratoryMethodService.allRequiredFieldsExistent(labMethod);
		system.assertEquals(allExistFlag, false);
		
		// All the required fields exist
		labMethod.AnalyticalTechnique__c = 'test analytical technique';
		allExistFlag = LaboratoryMethodService.allRequiredFieldsExistent(labMethod);
		system.assertEquals(allExistFlag, true);
	}
	
	
	/** Test the function createNewLaboratoryMethodFromSearchCriteria
	 * @author	Dimitrios Sgourdos
	 * @version	18-Oct-2013
	 */	
	static testMethod void createNewLaboratoryMethodFromSearchCriteriaTest() {
		LaboratoryMethod__c labMethod = new LaboratoryMethod__c();
		List<LaboratoryMethodCompound__c> compoundList = new List<LaboratoryMethodCompound__c>();
		
		// All the required fields exist
		labMethod.Species__c			 = 'test species'; 
		labMethod.Matrix__c		 		 = 'test matrix';
		labMethod.AnalyticalTechnique__c = 'test analytical technique';
		labMethod.Location__c			 = 'PRA_NL'; 
		labMethod.Department__c			 = 'SML'; 
		labMethod.Proprietary__c 		 = LaboratoryMethodService.INITIAL_SHOWN;
		
		// Check with one more compound than the max-compounds
		for(Integer i=0; i<=LaboratoryMethodService.MAX_COMPOUNDS; i++) {
			compoundList.add(new LaboratoryMethodCompound__c());
		}
		
		String result = LaboratoryMethodService.createNewLaboratoryMethodFromSearchCriteria(labMethod, compoundList);
		system.assertEquals(result, LaboratoryMethodService.MANY_COMPOUNDS);
		
		// Check with accepted number of compounds but with all compounds names empty
		compoundList.remove(0);
		result = LaboratoryMethodService.createNewLaboratoryMethodFromSearchCriteria(labMethod, compoundList);
		system.assertEquals(result, LaboratoryMethodService.MISSING_FIELDS);
		
		// Check with one accepted compounds but with one field missing from the method (Department__c)
		compoundList[0].Name = 'test compound';
		labMethod.Department__c	= NULL;
		
		result = LaboratoryMethodService.createNewLaboratoryMethodFromSearchCriteria(labMethod, compoundList);
		system.assertEquals(result, LaboratoryMethodService.MISSING_FIELDS);
		
		// Check with all ok except from Anticoagulant__c and detection missing (that cannot be in search criteria, but to catch the exception)
		labMethod.Department__c	= 'test department';
		result = LaboratoryMethodService.createNewLaboratoryMethodFromSearchCriteria(labMethod, compoundList);
		system.assertEquals(result, LaboratoryMethodService.SAVING_ERROR);
		
		// Check with everything ok
		labMethod.Anticoagulant__c = 'test anti-coagulant';
		labMethod.Detection__c	   = 'test detection';
		result = LaboratoryMethodService.createNewLaboratoryMethodFromSearchCriteria(labMethod, compoundList);
		system.assertEquals(result, LaboratoryMethodService.EVERYTHING_OK);
	}
	
	
	/** Test the function getSelectOptionsFromLabMethodPickListField
	 * @author	Dimitrios Sgourdos
	 * @version	21-Oct-2013
	 */
	 static testMethod void getSelectOptionsFromLabMethodPickListFieldTest() {
	 	List<SelectOption> valuesListWithoutNone = LaboratoryMethodService.getSelectOptionsFromLabMethodPickListField('Location__c',false,true);
	 	List<SelectOption> valuesListWithNone = LaboratoryMethodService.getSelectOptionsFromLabMethodPickListField('Location__c',true,true);
	 	system.assert(valuesListWithoutNone.size() > 0);
	 	system.assert(valuesListWithNone.size() > 0);
	 	system.assertEquals(valuesListWithNone.size(), valuesListWithoutNone.size() + 1);
	 }
	
	
	/** Test the function generateLabMethodName
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void generateLabMethodNameTest() {
		// Create some data
		List<LaboratoryMethod__c> testlmList = new List<LaboratoryMethod__c>();
		testlmList.add(new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
											Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood',
											Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle'));
		testlmList.add(new LaboratoryMethod__c (Name='PRA-NL-SML-0001', AnalyticalTechnique__c='ELISA', AntiCoagulant__c='N/A',
											Department__c='SML', Detection__c='N/A', Location__c='PRA-US', Matrix__c='Breast milk',
											Proprietary__c='N', ShowOnMethodList__c='Y', Species__c='Human'));
		insert testlmList;
		
		// Check the function
		String methodName = LaboratoryMethodService.generateLabMethodName('PRA-US', 'LML');
		system.assertEquals(methodName, 'PRA-US-LML-0001');
		
		methodName = LaboratoryMethodService.generateLabMethodName('PRA-NL', 'LML');
		system.assertEquals(methodName, 'PRA-NL-LML-0003'); // cause the department doesn't affect the numbering
	}
	
	
	/** Test the function getNumberOfCompounds
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void getNumberOfCompoundsTest() {
		// Create data
		LaboratoryMethod__c tmpMethod = new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
											Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood',
											Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle');
		insert tmpMethod;
		
		List<LaboratoryMethodCompound__c> tmpCompoundList = new List<LaboratoryMethodCompound__c>();
		tmpCompoundList.add(new LaboratoryMethodCompound__c(Name='Test 1',LaboratoryMethod__c=tmpMethod.Id));
		tmpCompoundList.add(new LaboratoryMethodCompound__c(Name='Test 2',LaboratoryMethod__c=tmpMethod.Id));
		insert tmpCompoundList;
		
		// Check the function
		Integer numberOfCompounds = LaboratoryMethodService.getNumberOfCompounds(tmpMethod.Id);
		system.assertEquals(numberOfCompounds, 2);
	}
	
	
	/** Test the function getNotApplicableValue
	 * @author	Dimitrios Sgourdos
	 * @version	30-Oct-2013
	 */
	static testMethod void getNotApplicableValueTest() {
		String result = LaboratoryMethodService.getNotApplicableValue();
		system.assertEquals(result, LaboratoryMethodDataAccessor.NOT_APPLICABLE_VALUE);
	}
	
	
	/** Test the function getLabMethodById.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Oct-2013
	 */
	static testMethod void getLabMethodByIdTest() {
		LaboratoryMethod__c result = LaboratoryMethodService.getLabMethodById(NULL);
	}
	
	
	/** Test the function getLabMethodsBySearchCriteria.
	 *	The test is for the code coverage as the functionality is tested in data accessor controller.
	 * @author	Dimitrios Sgourdos
	 * @version	30-Oct-2013
	 */
	static testMethod void getLabMethodsBySearchCriteriaTest() {
		LaboratoryMethod__c labMethod = new LaboratoryMethod__c();
		List<LaboratoryMethodCompound__c> compoundList = new List<LaboratoryMethodCompound__c>();
		List<LaboratoryMethod__c> results = LaboratoryMethodService.getLabMethodsBySearchCriteria(labMethod, compoundList, '0');
	}
	
	
	/** Test the function checkNameAndDepartmentAssignment
	 * @author	Dimitrios Sgourdos
	 * @version	19-Nov-2013
	 */
	static testMethod void checkNameAndDepartmentAssignmentTest() {
		LaboratoryMethod__c tmpMethod = new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
											Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood',
											Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle');
		insert tmpMethod;
		
		String result = LaboratoryMethodService.checkNameAndDepartmentAssignment(tmpMethod.Id, 'SML');
		system.assertEquals(result, 'PRA-NL-SML-0001');
	}
}