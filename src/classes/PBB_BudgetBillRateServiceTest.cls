/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PBB_BudgetBillRateServiceTest {
    static testMethod void loadAll() {
        TestData td = new TestData();
        td.initData();
                
        Test.startTest();
        PBB_BudgetBillRateService brService = new PBB_BudgetBillRateService();
        brService.setBaseParameters(td.billRateCardModel.id, td.lstCurrency[td.CURR_USD].id);
        brService.loadBillRates();

        Decimal theRate = brService.getBillRate(td.lstCountry[td.COUNTRY_US].id, td.lstJobClassDesc[0].id, '2021');

        Date startDate = Date.valueOf('2015-06-01');
        Date endDate = Date.valueOf('2020-12-31');
        Decimal theWgtRate = brService.getWeightedRateEven(td.lstCountry[td.COUNTRY_US].id, td.lstJobClassDesc[1].id, startDate, EndDate);
        Test.stopTest();

        System.assert((brService.mapBillRates.isEmpty() == false));
        System.debug('---- theRate: ' + theRate);
        System.debug('---- theWgtRate: ' + theWgtRate);
        System.assertEquals(107.00, theRate);
    }
    
    static testMethod void loadSelected() {
        TestData td = new TestData();
        td.initData();
                
        Test.startTest();
        PBB_BudgetBillRateService brService = new PBB_BudgetBillRateService();
        brService.setBaseParameters(td.billRateCardModel.id, td.lstCurrency[td.CURR_USD].id);
        brService.setCountryParameters(new Set<ID> {td.lstCountry[td.COUNTRY_US].id});
        brService.setJobParameters(new Set<ID> {td.lstJobClassDesc[1].id, td.lstJobClassDesc[2].id});
        brService.loadBillRates();

        Decimal theRate = brService.getBillRate(td.lstCountry[td.COUNTRY_US].id, td.lstJobClassDesc[2].id, '2021');
        
        Decimal theNotFoundRate = brService.getBillRate(td.lstCountry[td.COUNTRY_US].id, td.lstJobClassDesc[0].id, '2021');

        Test.stopTest();
        
        System.assert((brService.mapBillRates.isEmpty() == false));
        System.debug('---- theRate: ' + theRate);
        System.debug('---- theNotFoundRate: ' + theNotFoundRate);
        System.assertEquals(220.00, theRate);
        System.assertEquals(null, theNotFoundRate);
        
    }
    
    static testMethod void negativeTests() {
        TestData td = new TestData();
        td.initData();
        
        Boolean caughtMissingParams = false;
        Boolean caughtLoadTwice = false;
        Boolean caughtGetBeforeLoad = false;
        Boolean caughtMissingBillRate = false;
        
        Test.startTest();
        PBB_BudgetBillRateService brService = new PBB_BudgetBillRateService();
        try {
            brService.loadBillRates();
        }
        catch (PRA_BaseService.PRAServiceException ex) {
            caughtMissingParams = true;
        }

        try {
            brService.getBillRate(td.lstCountry[td.COUNTRY_US].id, td.lstJobClassDesc[2].id, '2021');
        }
        catch (PRA_BaseService.PRAServiceException ex) {
            caughtGetBeforeLoad = true;
        }
        
        brService.setBaseParameters(td.billRateCardModel.id, td.lstCurrency[td.CURR_USD].id);
        brService.loadBillRates();
        try {
            brService.loadBillRates();
        }
        catch (PRA_BaseService.PRAServiceException ex) {
            caughtLoadTwice = true;
        }
        
        try {
            Date theDate = Date.newInstance(2010,1,1);
            brService.getWeightedRateEven(td.lstCountry[td.COUNTRY_US].id, td.lstJobClassDesc[2].id, theDate, theDate.addYears(4));
        }
        catch (PRA_BaseService.PRAServiceException ex) {
            caughtMissingBillRate = true;
        }
        Test.stopTest();
        
        System.assert(caughtMissingParams);
        System.assert(caughtLoadTwice);
        System.assert(caughtGetBeforeLoad);
        System.assert(caughtMissingBillRate);
    }
    
    /**
    @Description This test method tests all methods that have code to validate parameters of type ID
    */
    static testMethod void testParamExceptions() {
        TestData td = new TestData();
        td.initData();
        
        Boolean paramException;
        
        Test.startTest();
        PBB_BudgetBillRateService brService = new PBB_BudgetBillRateService();
         
        
        // Using an Account object to get an ID type that is invalid for all test cases
        Account testObj = new Account(name='Generic Object');
        insert testObj;
        
        /**** setBaseParameters ****/
        paramException = false;
        try {
            brService.setBaseParameters(testObj.id, td.lstCurrency[td.CURR_USD].id);
        }
        catch (PRA_BaseService.PRAServiceException ex) {
            system.debug(ex);
            paramException = true;
        }
        System.assert(paramException);

        paramException = false;
        try {
            brService.setBaseParameters(td.billRateCardModel.id, testObj.id);
        }
        catch (PRA_BaseService.PRAServiceException ex) {
            system.debug(ex);
            paramException = true;
        }
        System.assert(paramException);

        /**** createKey ****/
        paramException = false;
        try {
            brService.createKey(testObj.id, td.lstJobClassDesc[0].id, '2015');
        }
        catch (PRA_BaseService.PRAServiceException ex) {
            system.debug(ex);
            paramException = true;
        }
        System.assert(paramException);

        paramException = false;
        try {
            brService.createKey(td.lstCountry[td.COUNTRY_US].id, testObj.id, '2015');
        }
        catch (PRA_BaseService.PRAServiceException ex) {
            system.debug(ex);
            paramException = true;
        }
        System.assert(paramException);

        /**** setCountryParameters ****/
        paramException = false;
        try {
            brService.setCountryParameters(new Set<ID> {testObj.id});
        }
        catch (PRA_BaseService.PRAServiceException ex) {
            system.debug(ex);
            paramException = true;
        }
        System.assert(paramException);

        /**** setJobParameters ****/
        paramException = false;
        try {
            brService.setJobParameters(new Set<ID> {testObj.id});
        }
        catch (PRA_BaseService.PRAServiceException ex) {
            system.debug(ex);
            paramException = true;
        }
        System.assert(paramException);

    }

    private class TestData{
        public Bill_Rate_Card_Model__c billRateCardModel;
        public list<Currency__c>       lstCurrency;
        public list<Country__c>        lstCountry;
        public list<Job_Class_Desc__c> lstJobClassDesc;
        
        public final Integer CURR_USD    = 0;
        public final Integer CURR_EUR    = 1;
        public final Integer COUNTRY_US  = 0;
        public final Integer COUNTRY_UK  = 1;
        public final Integer COUNTRY_MX  = 2;
        
        public void initData() {
            billRateCardModel = createRateCard();
            insert billRateCardModel;
            
            lstCurrency = createCurrency();
            insert lstCurrency;
            
            lstCountry = createCountries();
            insert lstCountry;
            
            lstJobClassDesc = createJobs();
            insert lstJobClassDesc;
            
            list<Integer> rates = new list<Integer> {100, 100, 150, 120};
            list<Integer> incs = new list<Integer> {1, 5, 10, 10};
            list<Bill_Rate__c> bulkBillRates = new list<Bill_Rate__c>();
            for (Country__c ctry : lstCountry) {
                
                Integer i = 0;
                for (Job_Class_Desc__c job : lstJobClassDesc) {
                    list<Bill_Rate__c> billRates = createBillRates(billRateCardModel.ID, lstCurrency[CURR_USD].ID, ctry.ID, job.ID, 2014, rates[i], incs[i]);
                    i++;
                    bulkBillRates.addAll(billRates);
                }
            }
            insert bulkBillRates;
        }

        private Bill_Rate_Card_Model__c createRateCard() {
            return new Bill_Rate_Card_Model__c(name='Rate Card 1', year__c='2014');
        }

        private list<Currency__c> createCurrency() {
            list<Currency__c> rtnList = new list<Currency__c>();
            Currency__c newObj;
            newObj = new Currency__c(name='USD');
            rtnList.add(newObj);
            newObj = new Currency__c(name='EUR');
            rtnList.add(newObj);
            return rtnList; 
        }
        
        private list<Country__c> createCountries() {
            list<Country__c> rtnList = new list<Country__c>();
            Country__c newObj;
            newObj = new Country__c(name='UNITED STATES', PRA_Country_Id__c='1', Country_Code__c='US');
            rtnList.add(newObj);
            newObj = new Country__c(name='UNITED KINGDOM', PRA_Country_Id__c='2', Country_Code__c='UK');
            rtnList.add(newObj);
            newObj = new Country__c(name='MEXICO', PRA_Country_Id__c='3', Country_Code__c='MX');
            rtnList.add(newObj);
            return rtnList;
        }

        private list<Job_Class_Desc__c> createJobs() {
            list<Job_Class_Desc__c> rtnList = new list<Job_Class_Desc__c>();
            Job_Class_Desc__c newObj;
            newObj = new Job_Class_Desc__c(name='Job 1', Job_Class_Code__c='J1', Job_Code__c='J1', Job_Class_ExtID__c='Job 1');
            rtnList.add(newObj);
            newObj = new Job_Class_Desc__c(name='Job 2', Job_Class_Code__c='J2', Job_Code__c='J2', Job_Class_ExtID__c='Job 2');
            rtnList.add(newObj);
            newObj = new Job_Class_Desc__c(name='Manager 1', Job_Class_Code__c='M1', Job_Code__c='M1', Job_Class_ExtID__c='Manager 1');
            rtnList.add(newObj);
            return rtnList;
        }
                
        private List<Bill_Rate__c> createBillRates(ID aBillRateModel, ID aCurrency, ID aCountry, ID aJob, 
                                                   Integer aStartYear, Integer aStartRate, Integer aIncrement) {
            
            List<Bill_Rate__c> lstRates = new List<Bill_Rate__c>();
    
            String mStrStartYear = String.valueOf(aStartYear);
            Integer mBillRate = aStartRate - aIncrement;
            for (Integer i = 0; i < 15; i++) {
                String mYear = String.valueOf(aStartYear + i);
                mBillRate += aIncrement;
                
                Bill_Rate__c newRate = new Bill_Rate__c(Model_Id__c = aBillRateModel,
                                                        Bill_Rate__c = mBillRate,
                                                        Currency__c = aCurrency,
                                                        Job_Position__c = aJob,
                                                        Country__c = aCountry,
                                                        Schedule_Year__c = mStrStartYear,
                                                        Year_Rate_Applies_To__c = mYear);
                
                lstRates.add(newRate);            
            }
            
            return lstRates;
        }
    }
    
}