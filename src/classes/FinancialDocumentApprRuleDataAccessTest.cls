/** Implements the test for the Selector Layer of the object FinancialDocumentApprRule__c
 * @author	Dimitrios Sgourdos
 * @version	10-Dec-2013
 */
@isTest
private class FinancialDocumentApprRuleDataAccessTest {
	
	/** Test the function getFinancialDocumentApprRules
	 * @author	Dimitrios Sgourdos
	 * @version	10-Dec-2013
	 */
	static testMethod void getFinancialDocumentApprRulesTest() {
		List<FinancialDocumentApprRule__c> initialList = new List<FinancialDocumentApprRule__c>();
		initialList.add(new FinancialDocumentApprRule__c(Description__c='Test 2', SequenceNumber__c=2));
		initialList.add(new FinancialDocumentApprRule__c(Description__c='Test 1', SequenceNumber__c=1));
		insert initialList;
		
		List<FinancialDocumentApprRule__c> results = FinancialDocumentApprRuleDataAccessor.getFinancialDocumentApprRules();
		system.assertEquals(results.size(), 2, 'Error in retrieving the approval rules from the system');
		system.assertEquals(results[0].Description__c, 'Test 1', 'Error in the order of retrieved approval rules');
		system.assertEquals(results[1].Description__c, 'Test 2', 'Error in the order of retrieved approval rules');
	}
}