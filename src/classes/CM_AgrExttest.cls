@isTest
public with sharing class CM_AgrExttest {
    
    private static final RecordType RT_Template = [SELECT Id from RecordType WHERE  SObjectType = 'Apttus__APTS_Agreement__c' and DeveloperName = 'Template'];
    
    static testMethod void testnewAgreement() {
    
        WFM_Client__c testClient = new WFM_Client__c();
        WFM_Contract__c testContract = new WFM_Contract__c();       
        WFM_Project__c testProject = new WFM_Project__c();
        WFM_Protocol__c testProtocol = new WFM_Protocol__c();
        Apttus__APTS_Agreement__c testtemplate = new Apttus__APTS_Agreement__c();
        List<Apttus__APTS_Agreement__c> testlist = new List<Apttus__APTS_Agreement__c>();
        
        testClient.Name = 'Ranbaxytest';                
        testClient.Client_Unique_Key__c = 'RBX001';
        insert testClient;
        
        testContract.Name = 'testcontract';
        testContract.Contract_Unique_Key__c = 'crnttest1';
        testContract.Client_ID__c = testClient.Id;
        insert testContract;
        
        testProject.Name = 'LiverCancer';
        testProject.Project_Unique_Key__c = 'LVC1';
        insert testProject;
        
        testProtocol.Name = 'testprotocol';
        testProtocol.Protocal_Unique_Key__c = 'test1';
        testProtocol.Project_ID__c = testProject.Id;
        insert testProtocol;
                        
        testtemplate.name='Test Template';
        testtemplate.PRA_Status__c = 'Budget Template Initiated';   
        testtemplate.Contract_Type__c='Master CTA';     
        testtemplate.First_Draft_Sent_Date__c = Date.newInstance(2015, 02, 01);
        testtemplate.Planned_Final_Exec_Date__c = Date.newInstance(2015, 12, 31);
        testlist.add(testtemplate);
        insert testlist;
        
        ApexPages.StandardsetController Cont = new ApexPages.StandardsetController(testlist);
        ApexPages.CurrentPage().GetParameters().put('Id',testProtocol.id);
        CM_AgrExt testController = new CM_AgrExt(Cont);
        
        test.startTest();
        testController.saveAgreement();
        test.stoptest();
                    
    }
}