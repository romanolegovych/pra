public with sharing class BDT_FlowchartServiceTotal {

	public static List<GroupArmAssignment__c> getGroupArmDataForAssesmentCalculations (Set<ID> ArmIDs) {

		return [select studydesignpopulationassignment__r.populationstudyassignment__r.population__r.name
					, name
					, numberofsubjects__c
					, studydesignpopulationassignment__r.clinicaldesignstudyassignment__r.study__c
					, arm__c
					, arm__r.description__c
					from grouparmassignment__c 
					where studydesignpopulationassignment__r.populationstudyassignment__r.population__r.name != null
					and studydesignpopulationassignment__r.clinicaldesignstudyassignment__r.study__c != null
					and numberofsubjects__c != 0 
					and arm__c in :ArmIDs
					order by studydesignpopulationassignment__r.populationstudyassignment__r.population__r.name
					, name
					, numberofsubjects__c];
		
	} 

	public static List<ProjectServiceTimePoint__c> getProjectServiceTimePointData (Set<ID> flowchartIds) {

		return [SELECT Flowchart__c,
				Flowchart__r.Description__c,
				Flowchart__r.ClinicalDesign__r.Name,
				ProjectService__c,
				TimePointCount__c
			FROM ProjectServiceTimePoint__c
			WHERE ProjectService__c != NULL
			AND Flowchart__c IN :flowchartIds
			AND TimePointCount__c != 0
			AND TimePointCount__c != NULL
			ORDER BY Flowchart__c,
				ProjectService__c];

	}
	
	public static list<Flowchartassignment__c> getFlowchartAssignmentData (Set<ID> ArmIDs, Set<ID> FlowchartIDs) {
		
		String query = 'select arm__c, flowchart__c, epoch__c, id from Flowchartassignment__c where flowchart__c != null ';
		
		if (ArmIDs != null && !ArmIDs.isEmpty()) {
			query += 'and arm__c in :ArmIDs ';
		}
		if (FlowchartIDs != null && !FlowchartIDs.isEmpty()) {
			query += 'and flowchart__c in :FlowchartIDs';
		}
		
		return database.query(query);
	}
	
	public static list<FlowchartServiceTotal__c> getFlowchartServiceTotals (Set<ID> flowchartAssignmentIDs) {
		return [SELECT ExplanationJSON__c, 
				ProjectService__c, 
				Id, 
				Study__c, 
				Study_ProjectService_Combination__c, 
				TotalUnits__c, 
				flowchartassignment__c 
				FROM FlowchartServiceTotal__c 
				where flowchartassignment__c in :flowchartAssignmentIDs];
	}

	public static list<ArmGroupDetailsWrapper> determineArmData (Set<ID> ArmIDs) {
		
		list<ArmGroupDetailsWrapper> result = new list<ArmGroupDetailsWrapper>();
		
		// read data
		List<GroupArmAssignment__c>  gaaList = getGroupArmDataForAssesmentCalculations (ArmIDs);
		
		For (GroupArmAssignment__c gaa : gaaList) {
			
			// populate the wrapper
			ArmGroupDetailsWrapper w = new ArmGroupDetailsWrapper();
			w.armId = gaa.arm__c;
			w.populationName = gaa.studydesignpopulationassignment__r.populationstudyassignment__r.population__r.name;
			w.groupName = gaa.name;
			w.groupSize = gaa.numberofsubjects__c;
			w.studyId = gaa.studydesignpopulationassignment__r.clinicaldesignstudyassignment__r.study__c;
			w.armName = gaa.arm__r.description__c;
			result.add(w);
			
		}
		
		return result;
		
	}

	public static List<FlowchartDataWrapper> determineFlowchartData(Set<ID> flowchartIds) {
		List<FlowchartDataWrapper> result = new List<FlowchartDataWrapper>();
		// Read Data
		List<ProjectServiceTimePoint__c> pstpList = getProjectServiceTimePointData(flowchartIds);
		// parse data
		for(ProjectServiceTimePoint__c pstp : pstpList) {
			//populate the wrapper
			FlowchartDataWrapper	dataWrapperItem	= new FlowchartDataWrapper();
			dataWrapperItem.flowchartId			= pstp.Flowchart__c;
			dataWrapperItem.flowchartName			= pstp.Flowchart__r.Description__c;
			dataWrapperItem.designName				= pstp.Flowchart__r.ClinicalDesign__r.Name;
			dataWrapperItem.projectServiceId		= pstp.ProjectService__c;
			dataWrapperItem.timePointCount		= pstp.TimePointCount__c;
			result.add(dataWrapperItem);
		}
		return	result;
	}

	public static list<BaseJsonWrapper> createBaseJSON (list<ArmGroupDetailsWrapper> armDetailList,
			list<FlowchartDataWrapper> flowchartDetailList,
			Map<String,String> EpochOnArmFlowchart
			) {

		list<BaseJsonWrapper> JSONresult = new list<BaseJsonWrapper>();
	
		for (ArmGroupDetailsWrapper armDetail : armDetailList) {
			for (FlowchartDataWrapper flowchartDetail : flowchartDetailList) {

				BaseJsonWrapper b 	= new BaseJsonWrapper();
				b.armId				= armDetail.armId;
				b.flowchartId		= flowchartDetail.flowchartId;
				b.studyId			= armDetail.studyId;
				b.projectServiceId	= flowchartDetail.projectServiceId;
				
				SVWrapper s 		= new SVWrapper();
				s.populationName	= armDetail.populationName;
				s.groupname			= armDetail.groupName;
				s.groupsize			= (armDetail.groupSize==null)?0:armDetail.groupSize;
				s.armname			= armDetail.armName;
				s.designname		= flowchartDetail.designName;
				s.flowchartname		= flowchartDetail.flowchartName;
				s.timepoints		= (flowchartDetail.timePointCount==null)?0:flowchartDetail.timePointCount;
				s.totalunits		= s.timepoints * s.groupsize;
				
				if (EpochOnArmFlowchart.containsKey(armDetail.armId + ':' + flowchartDetail.flowchartId)) {
					s.epochname		= EpochOnArmFlowchart.get(armDetail.armId + ':' + flowchartDetail.flowchartId);
				}
				
				b.details = s;
				JSONresult.add(b);
			}
		}
	
		return JSONresult;
	
	}

	public static map<string,map<id,map<id,list<SVWrapper>>>> createMapOfStudyVolunteerWrapper (list<BaseJsonWrapper> sourceDataList) {

		// creating a mapping by ARM & FLOWCHART on a mapping by STUDY on a mapping by PROJECTSERVICE on a list of SVWrapper
		map<string,map<id,map<id,list<SVWrapper>>>> result = new map<string,map<id,map<id,list<SVWrapper>>>>();

		for (BaseJsonWrapper sourceData : sourceDataList) {

			String key = sourceData.armId + ':' + sourceData.flowchartId;
			
			// outer map, mapped on arm & flowchart combination
			map<id,map<id,list<SVWrapper>>> svwPerStudyMap;
			if (result.containsKey(key)) {
				svwPerStudyMap = result.get(Key);
			} else {
				svwPerStudyMap = new map<id,map<id,list<SVWrapper>>>();
			}

			// middle map, mapped on study
			map<id,list<SVWrapper>> svwPerProjectService;
			if (svwPerStudyMap.containsKey(sourceData.studyId)) {
				svwPerProjectService = svwPerStudyMap.get(sourceData.studyId);
			} else {
				svwPerProjectService = new map<id,list<SVWrapper>>();
			}

			// inner map, mapped on project service
			list<SVWrapper> svwList;
			if (svwPerProjectService.containsKey(sourceData.projectServiceId)) {
				svwList = svwPerProjectService.get(sourceData.projectServiceId);
			} else {
				svwList = new list<SVWrapper>();
			}
			
			// populate list
			svwList.add(sourceData.details);
			// map list on projectserviceid
			svwPerProjectService.put(sourceData.projectServiceId,svwList);
			// map map on studyid
			svwPerStudyMap.put(sourceData.studyId,svwPerProjectService);
			// map map on arm & flowchart
			result.put(key,svwPerStudyMap);
		} 

		return result;
	}


	public static void updateFSTonDesign (Set<ID> DesignIDs) {

		Set<ID> ArmSet = new Set<ID>();
		For (Arm__c a : [select id from arm__c where ClinicalDesign__c in :DesignIDs]){
			ArmSet.add(a.id);
		}
		
		Set<ID> FlowchartSet = new Set<ID>();
		For (Flowcharts__c a : [select id from flowcharts__c where ClinicalDesign__c in :DesignIDs]){
			FlowchartSet.add(a.id);
		}
		
		delete [select id from flowchartservicetotal__c where flowchartassignment__r.arm__r.clinicaldesign__c in :DesignIDs];
		
		updateFlowchartServiceTotal (ArmSet,FlowchartSet);
		
	}

	public static void updateFlowchartServiceTotal (Set<ID> ArmIDs, Set<ID> FlowchartIDs) {
		
		list<Flowchartassignment__c> fcaList = getFlowchartAssignmentData (ArmIDs, FlowchartIDs);
		
		Set<ID> StudyIDs = new Set<ID>();
		Set<ID> ProjectServiceIDs = new Set<ID>();
		
		ArmIDs = new Set<ID>();
		FlowchartIDs = new Set<ID>();
		Set<ID> flowchartAssignmentIDs = new Set<ID>();
		for (Flowchartassignment__c fca : fcaList) {
			ArmIDs.add(fca.arm__c);
			FlowchartIDs.add(fca.flowchart__c);
			flowchartAssignmentIDs.add(fca.id);
		}
		
		// read and parse all required data
		list<FlowchartDataWrapper> flowchartDetailList = determineFlowchartData(FlowchartIDs);
		list<ArmGroupDetailsWrapper> armDetailList = determineArmData(ArmIDs);
		
		// create map that maps arm flowchart combinations to epoch descriptions.
		map<string,string> EpochOnArmFlowchart = new map<string,string>();
		For (FlowchartAssignment__c fa: [
				select arm__c, epoch__r.description__c, flowchart__c 
				from flowchartAssignment__c
				where flowchart__c in :FlowchartIDs
				or arm__c in :ArmIDs
				]) {
			String key = fa.arm__c + ':' + fa.flowchart__c;
			String description;
			if (EpochOnArmFlowchart.containsKey(key)){
				// This flowchart is used twice in the same arm. There is no distinguishing currently
				// just let user know all epoch names
				description = EpochOnArmFlowchart.remove(key);
				description += ' (+) ' + fa.epoch__r.description__c;
			} else {
				description = fa.epoch__r.description__c;
			}
			EpochOnArmFlowchart.put(key, description);
		}
		
		list<BaseJsonWrapper> sourceDataList = createBaseJSON (armDetailList,flowchartDetailList,EpochOnArmFlowchart);
		map<string,map<id,map<id,list<SVWrapper>>>> dataForFST = createMapOfStudyVolunteerWrapper (sourceDataList);
		
		// create searchable map of existing FST records
		Map<String,SObject> existingFSTmap = BDT_Utils.mapSObjectOnKey(
				getFlowchartServiceTotals (flowchartAssignmentIDs), 
				new List<String>{'flowchartassignment__c','Study__c','ProjectService__c'} 
		);
		
		// create store for new and updated records
		list<FlowchartServiceTotal__c> upsertList = new list<FlowchartServiceTotal__c>();
		
		// parse the flowchartAssignmentRecords
		for (Flowchartassignment__c fca : fcaList) {
			
			// within dataForFST search if records need to be created for specified flowchartassignment			
			String key = fca.Arm__c + ':' + fca.Flowchart__c;
			
			if (!dataForFST.containsKey(key)) {
				// no child records for this FCA exist, continue to next iteration
				continue;
				// Cleanup is handled: Existing items in FST are stored in map existingFSTmap 
				// any items remaining in this map will be removed at end of this method.
			}
			
			// get map of results for arm & flowchart combination
			map<id,map<id,list<SVWrapper>>> FSTperStudyResults = dataForFST.get(key);
			//per study
			for (ID studyId : FSTperStudyResults.keySet()) {

				StudyIDs.add(studyId);
				map<id,list<SVWrapper>> FSTperProjectService = FSTperStudyResults.get(studyId);
				//per project service
				for (ID projectServiceId : FSTperProjectService.keySet()) {

					ProjectServiceIDs.add(projectServiceId);
					List<SVWrapper> SVlist = FSTperProjectService.get(projectServiceId);
					String newExplanationJSON = JSON.serialize(SVlist);
					
					FlowchartServiceTotal__c fst = new FlowchartServiceTotal__c();
					
					// find existing record for this fst
					if (existingFSTmap.containsKey(fca.id + ':' + studyId + ':' + projectServiceId)) {
						
						// Remove from map so we know this record is still current. Record remaining 
						// in map will be obsolete and will be deleted upon completion of loops
						fst = (FlowchartServiceTotal__c) existingFSTmap.remove(fca.id + ':' + studyId + ':' + projectServiceId);
						
						// existing record found, check if new fst is different from exisiting
						if (fst.ExplanationJSON__c == newExplanationJSON ) {
							// ExplanationJSON's are equal, no update needed, continue to next iteration.
							continue;
						}
					}
					// existing record will be updated, new record will be created
					if (fst.id == null) {	/* set references on new instance only */
						fst.Study__c = studyId;
						fst.ProjectService__c = projectServiceId;
						fst.flowchartassignment__c = fca.id;
					}
					fst.TotalUnits__c = getTotalFromListSV (SVlist);
					fst.ExplanationJSON__c = newExplanationJSON;
					upsertList.add(fst);
				}
			}
		}

		// list of objects to upsert: upsertList
		upsert upsertList;
		// list of objects to delete: existingFSTmap.values();
		//delete existingFSTmap.values();
		cleanupFlowchartServiceTotals(existingFSTmap.values());

		// calculate study services
		calculateStudyService (StudyIDs, ProjectServiceIDs);
	}

	public static void cleanupFlowchartServiceTotals (list<FlowchartServiceTotal__c> ItemsToDelete) {
		
		delete ItemsToDelete;
		
		// collect removed combinations
		Set<String> removedStudyProjectserviceCombinations = new Set<String>();
		Set<id> studyIds = new Set<id>();
		For (FlowchartServiceTotal__c fst : ItemsToDelete) {
			removedStudyProjectserviceCombinations.add(fst.Study_ProjectService_Combination__c);
			studyIds.add(fst.study__c);
		}
		
		// remove from collection the combinations that are still used
		for (flowchartservicetotal__c fst : [
				select study_projectservice_combination__c
				from flowchartservicetotal__c
				where study_projectservice_combination__c in :removedStudyProjectserviceCombinations]) {
			removedStudyProjectserviceCombinations.remove(fst.study_projectservice_combination__c);
		}
	
		if (removedStudyProjectserviceCombinations.size() > 0) {
		
			// delete records without any approved financial document
			delete [
				select id
				from studyservice__c
				where projectservice__r.service__r.isdesignobject__c = true	
				and study_projectservice_combination__c in :removedStudyProjectserviceCombinations
				and id not in (select studyservice__c
									from studyservicepricing__c
									where approvedfinancialdocument__c != null)
			];
			
			/* if the study service has a child study service pricing with an approved document
				the record should not be deleted but set to 0 */
			list<StudyService__c> ssUpdateList = new list<StudyService__c>();
			for (StudyService__c ss : [
					select id
					from studyservice__c
					where projectservice__r.service__r.isdesignobject__c = true	
					and study_projectservice_combination__c not in :removedStudyProjectserviceCombinations
					and id in (select studyservice__c
										from studyservicepricing__c
										where approvedfinancialdocument__c != null)
			]) {
				
				ss.explanationJSON__c = '';
				ss.NumberOfUnits__c = 0;
				ssUpdateList.add(ss);
			}
			update ssUpdateList;
		}
	}

	public static Decimal getTotalFromListSV (List<SVWrapper> SVlist) {
		Decimal result = 0;
		For (SVWrapper SV : SVlist) {
			result += SV.totalunits;
		}
		return result;
	}

	public static void calculateStudyService (Set<ID> StudyIDs, Set<ID> ProjectServiceIDs) {

		// this method (re-)calculates the studyserviceratecard units and updates or creates
		// the studyserviceratecard based on data in flowchartassignment__c

		map<string, list<SVWrapper>>	fstExplanationsMap = new map<string, list<SVWrapper>>();
		map<string,Decimal>			 numberOfUnitsMap = new map<string,Decimal>();

		// get the details

		for(FlowchartServiceTotal__c fst: [select explanationJSON__c
												, TotalUnits__c
												, Study__c
												, ProjectService__c
											from  FlowchartServiceTotal__c
											where Study__c in :StudyIDs
											and   ProjectService__c in :ProjectServiceIDs
											and   Study__c != null
											and   ProjectService__c != null
											]){

			// count the numberOfUnits
			if(numberOfUnitsMap.containsKey(''+fst.Study__c+':'+fst.ProjectService__c)){
				Decimal currentTotal = numberOfUnitsMap.remove(''+fst.Study__c+':'+fst.ProjectService__c);
				numberOfUnitsMap.put(''+fst.Study__c+':'+fst.ProjectService__c,currentTotal + fst.TotalUnits__c);
			} else {
				numberOfUnitsMap.put(''+fst.Study__c+':'+fst.ProjectService__c,fst.TotalUnits__c);
			}

			// build the json details
			List<SVWrapper> tmpExObject;
			if(fstExplanationsMap.containsKey(fst.Study__c +':'+ fst.ProjectService__c ) ){
				tmpExObject  = fstExplanationsMap.remove(fst.Study__c +':'+ fst.ProjectService__c);
			}else{
				tmpExObject = new List<SVWrapper>();
			}
			try {
				if (fst.explanationJSON__c != null) {
					tmpExObject.addAll( (List<SVWrapper>)JSON.deserialize( fst.explanationJSON__c, List<SVWrapper>.class ) );
				}
			} catch (Exception e) {
				system.debug('Deserialize failed on:'+fst.explanationJSON__c);
			}

			fstExplanationsMap.put(fst.Study__c +':'+ fst.ProjectService__c,tmpExObject);

		}

		// save the data
		map<string,StudyService__c> ssMap = new map<string,StudyService__c>();
		for (StudyService__c ss : [select id
										, Study__c
										, ProjectService__c
										, NumberOfUnits__c
										, explanationJSON__c
									from  StudyService__c
									where study__c in :StudyIDs
									and   projectservice__c in :ProjectServiceIDs
									and   Study__c != null
									and   ProjectService__c != null]) {

			ssMap.put(''+ss.Study__c+':'+ss.ProjectService__c,ss);
		}

		list<StudyService__c> ssList = new list<StudyService__c>();

		for(string key : numberOfUnitsMap.keySet()){
			StudyService__c StudyService;
			if(ssMap.containsKey(key)){
				StudyService = ssMap.remove(key);
			} else {
				StudyService = new StudyService__c(ProjectService__c =key.split(':')[1], Study__c = key.split(':')[0] );
			}

			Decimal newUnitCount = numberOfUnitsMap.get(key);
			String newJSONExplanation = JSON.serialize( fstExplanationsMap.get(key));

			if (StudyService.NumberOfUnits__c != newUnitCount || 
					StudyService.explanationJSON__c != newJSONExplanation) {
				
				StudyService.NumberOfUnits__c = newUnitCount;
				StudyService.explanationJSON__c = newJSONExplanation;

				ssList.add(StudyService);
			}

		}

		if (ssList.size()>0) {
			upsert ssList;
		}

		// elements remaining in ssMap are obsolete elements
		delete ssMap.values();
	}

	public class BaseJsonWrapper {
		public ID armId;
		public ID flowchartId;
		public ID studyId;
		public ID projectServiceId;
		public SVWrapper details;
	}

	public class SVWrapper {
		public string populationName;
		public string groupname;
		public decimal groupsize;
		public string designname;
		public string armname;
		public string flowchartname;
		public string epochname;
		public decimal timepoints;
		public decimal totalunits;
	}
	
	public class FlowchartDataWrapper {
		public ID flowchartId;
		public String flowchartName;
		public String designName;
		public ID projectServiceId;
		public decimal timePointCount;
	}
	
	public class ArmGroupDetailsWrapper {
		public string populationName;
		public string groupName;
		public decimal groupSize;
		public ID studyId;
		public ID armId;
		public string armName;
	}

}