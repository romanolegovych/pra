/**
 * @description	The Batch class that implements the polling mechanism to check if notification events have occured
 * @author		Dimitrios Sgourdos
 * @date		Created: 24-Sep-2015, Edited: 01-Oct-2015
 */
global class NCM_EventPollingMechanismBatch implements Database.Batchable<SObject>, Database.Stateful {

	/**
	 * @description	The constructor of the class
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 24-Sep-2015
	**/
	global NCM_EventPollingMechanismBatch() {
		
	}
	
	/**
	* @description  This start method populates the QueryLocator object with all the notification topics to check
					if they have occured.
	* @author       Dimitrios Sgourdos
	* @date         Created: 24-Sep-2015, Edited: 25-Sep-2015
	**/
	global Database.queryLocator start(Database.BatchableContext ctx){
		return NCM_DataAccessor.getNotificationRegistrationsforSchedule();
	}
	
	/**
	 * @description	Implement the polling mechanism for triggering topics, create events, notifications and sending
	 *				the notifications to the subscribed users.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 24-Sep-2015, Edited: 01-Oct-2015
	**/
	global void execute(Database.BatchableContext ctx, List<NotificationRegistration__c> registrationList) {
		NCM_SrvLayer_NotificationEvent.createEventsFromPollingMechanism(registrationList);
	}
	
	 
	/**
	* @description  The finish method finds all the events that were created in the batch job and it marks them as
					completed by release their name from the NCM_API_DataTypes.EVENT_UNDER_BATCH_PROCEDURE value.
	* @author       Dimitrios Sgourdos
	* @date         Created: 24-Sep-2015, Edited: 25-Sep-2015
	**/
	global void finish(Database.BatchableContext ctx) {
		NCM_SrvLayer_NotificationEvent.releaseNotificationEventsFromBatchProcedure();
	}
}