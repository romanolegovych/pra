public with sharing class PBB_ServiceTaskToDriversServices {
	
	public PBB_ServiceTaskToDriversServices() {
		
	}
	
	public static void checkForApprovedServices(List<Service_Task_To_Drivers__c> newList){
		Set<Id> tasksWithDuplicates = new Set<Id>();
		for (AggregateResult res : [
				Select Service_Task__c, COUNT(Id) countOfDrivers 
				From Service_Task_To_Drivers__c 
				Where Service_Task__c In :getTaskIdsForApprovedServices(newList) 
				and Driver_Type__c = 'Service' and Status__c = 'Approved'
				Group By Service_Task__c] 
		){
			if ( (Integer)res.get('countOfDrivers') > 1 ){
				tasksWithDuplicates.add( (Id)res.get('Service_Task__c') );
			}
		}
		for (Service_Task_To_Drivers__c sttd : newList){
			if ( tasksWithDuplicates.contains(sttd.Service_Task__c) ){
				sttd.addError('Task can contain only one Service Task to Drivers with Type: Service and Status: Approved');
			}
		}
	}
	
	private static Set<Id> getTaskIdsForApprovedServices(List<Service_Task_To_Drivers__c> newList){
		Set<Id> taskIds = new Set<Id>();
		for (Service_Task_To_Drivers__c sttd : newList){
			//if (sttd.Driver_Type__c == 'Service' && sttd.Status__c == 'Approved'){
				taskIds.add(sttd.Service_Task__c);
			//}
		}
		return taskIds;
	}
}