public without sharing class PAWS_PBB_ProjectHelper
{
    public class PAWS_PBB_ProjectHelper_Exception extends Exception { }
        
    public List<PAWS_ProjectDetailCVO> getActiveProjectList(String sponsorName)
    {
        //TODO: Get rid of rudiment "sponsorName" parameter in this method
        List<PAWS_ProjectDetailCVO> result= getProjectDetailList(
            getAllPAWSProjectList(getActiveProjectIds(),'Regular')
        );

        return result;
    }

    public List<PAWS_ProjectDetailCVO> getActiveProjectList()
    {
        List<PAWS_ProjectDetailCVO> result= getProjectDetailList(
            getAllPAWSProjectList(getActiveProjectIds(),'Regular')
        );

        return result;
    }
    
    public List<PAWS_ProjectCVO> getActiveProjects(String sponsorName)
    {
        List<PAWS_ProjectCVO> result = new List<PAWS_ProjectCVO>();
        for(ecrf__c eachEcrf : getPAWSProjectAllByIDAndSponsorName(getActiveProjectIds(), sponsorName))
        {
            result.add(new PAWS_ProjectCVO(eachEcrf));
        }

        return result;
    }

    public PAWS_ProjectDetailCVO getProjectDetails(ID projectID)
    {
        List<PAWS_ProjectDetailCVO> resultList = getProjectDetailList(
            new List<ecrf__c> {
                getPAWSProjectByID(projectID)
            }
        );

        if(resultList.isEmpty() == true)
        {
            return null;
        }

        PAWS_ProjectDetailCVO projectDetails = resultList.get(0);

        Set<ID> projectCountryIDs = new Set<ID>();
        for(PAWS_ProjectCountryCVO eachCountry : projectDetails.Countries)
        {
            projectCountryIDs.add(eachCountry.RecordId);
        }

        projectDetails.addSites(
            getSiteAllByCountryID(projectCountryIDs)
        );

        return projectDetails;
    }

    private List<PAWS_ProjectDetailCVO> getProjectDetailList(List<ecrf__c> pawsProjectList)
    {
        List<PAWS_ProjectDetailCVO> result = new List<PAWS_ProjectDetailCVO>();
		
		Set <ID> protocolRecordIDs = new Set <ID> ();
		for(ecrf__c eachPawsProject : pawsProjectList)
        {
            PAWS_ProjectDetailCVO projectDetails = new PAWS_ProjectDetailCVO(eachPawsProject);
            result.add(projectDetails);
            
            protocolRecordIDs.add(projectDetails.ProtocolRecordID);
        }
        
        // Inject Patien Enrollment Statuses:
        // There is PBB_WR_APIs.getPatientEnrollmentStatusPerProtocol(set<ID> protocolrecordIDs) method
        // that we can use for that. It returns Map<Id,String>, where 
        // key =  WFM Protocol record ID
        // value = Status (e.g. 'Not Started', 'Completed', 'On Track', 'At Risk', 'Delayed')
        
        Map <ID, String> enrollmentStatuses = PBB_WR_APIs.getPatientEnrollmentStatusPerProtocol(protocolRecordIDs);
        for(PAWS_ProjectDetailCVO projectDetails : result)
        {
        	projectDetails.PatientEnrollmentStatus = enrollmentStatuses.get(projectDetails.ProtocolRecordID);
        }       

        return result;
    }

    private List<ecrf__c> getPAWSProjectAllByIDAndSponsorName(Set<ID> ids, String sponsorName)
    {
        return [select Project_ID__r.Name from ecrf__c where ID = :ids and Sponsor__c = :sponsorName and Type__c = 'Regular' limit 5];
    }
    
    

    private List<ecrf__c> getPAWSProjectList(Set<ID> ids, String sponsorName, String type)
    {
        String query =
            'select ' +
                'Project_ID__r.Name, ' +
                'Protocol_ID__c, ' +
                'Protocol_ID__r.Name, ' +
                'Protocol_ID__r.Protocal_Unique_Key__c, ' +
                'Compound__c, ' +
                'Therapeutic_Area__c, ' +
                'Project_Manager__c, ' +
                'Project_Manager__r.Name, ' +
                'Project_Manager__r.Email, ' +
                'Sponsor__c, ' +
                'Study_Start_Up_Status__c, ' +
                'Patient_Enrollment_Status__c, ' +
                'Patient_Retention_Status__c, ' +
                'Indication__c, ' +
                'Number_Of_Sites_At_Risk_Aggregated__c, ' +
                'Number_Of_Sites_Delayed_Aggregated__c, ' +
                'Number_Of_Sites_On_Track_Aggregated__c, ' +
                'Number_Of_Sites_Completed__c, ' +
                '(' +
                    'select ' +
                        'Name, ' +
                        'Number_Of_Sites_On_Track__c, ' +
                        'Number_Of_Sites_Delayed__c, ' +
                        'Number_Of_Sites_Completed__c, ' +
                        'Number_Of_Sites_At_Risk__c, ' +
                        'Number_Of_Sites__c ' +
                    'from ' +
                        'PAWS_Countries__r ' +
                ')' +
            'from ' +
                'ecrf__c ' +
            'where ' +
                'ID = :ids ';

        if(String.isEmpty(sponsorName) == false)
        {
            query += 'and Sponsor__c = :sponsorName ';
        }
        if(String.isEmpty(type) == false)
        {
            query += 'and Type__c = :type ';
        }

        return (List<ecrf__c>) Database.query(query);
    }
    
    private List<ecrf__c> getAllPAWSProjectList(Set<ID> ids, String type)
    {   
        List <String> protocolIDs = new List <String>();
        List <String> projectIDs = new List <String>();
        for (CTMax__Study_Team__c team : [ SELECT ID, CTMax__User__c, CTMax__Study__c, CTMax__Study__r.CTMax__External_Id__c, CTMax__Study__r.CTMax__Clinical_Project__r.Name, CTMax__Start_Date__c, CTMax__End_Date__c FROM CTMax__Study_Team__c WHERE CTMax__User__c = :UserInfo.getUserId() AND ((CTMax__Start_Date__c <= TODAY OR CTMax__Start_Date__c = NULL) AND (CTMax__End_Date__c >= TODAY OR CTMax__End_Date__c = NULL ))])
        {
            protocolIDs.add(team.CTMax__Study__r.CTMax__External_Id__c);
            projectIDs.add(team.CTMax__Study__r.CTMax__Clinical_Project__r.Name);
        }
        String query =
            'select ' +
                'ID, ' +
                'Project_ID__r.Name, ' +
                'Protocol_ID__c, ' +
                'Protocol_ID__r.Name, ' +
                'Protocol_ID__r.Protocal_Unique_Key__c, ' +
                'Compound__c, ' +
                'Therapeutic_Area__c, ' +
                'Project_Manager__c, ' +
                'Project_Manager__r.Name, ' +
                'Project_Manager__r.Email, ' +
                'Sponsor__c, ' +
                'Study_Start_Up_Status__c, ' +
                'Patient_Enrollment_Status__c, ' +
                'Patient_Retention_Status__c, ' +
                'Indication__c, ' +
                'Number_Of_Sites_At_Risk_Aggregated__c, ' +
                'Number_Of_Sites_Delayed_Aggregated__c, ' +
                'Number_Of_Sites_On_Track_Aggregated__c, ' +
                'Number_Of_Sites_Completed__c, ' +
                '(' +
                    'select ' +
                        'Name, ' +
                        'Number_Of_Sites_On_Track__c, ' +
                        'Number_Of_Sites_Delayed__c, ' +
                        'Number_Of_Sites_Completed__c, ' +
                        'Number_Of_Sites_At_Risk__c, ' +
                        'Number_Of_Sites__c ' +
                    'from ' +
                        'PAWS_Countries__r ' +
                ')' +
            'from ' +
                'ecrf__c ' +
            'where ' +
                'ID = :ids ' + 
            'and ' + 
                'Protocol_ID__r.Name IN :protocolIDs ' + 
            'and ' + 
                'Project_ID__r.Name IN :projectIDs ';

       
        if(String.isEmpty(type) == false)
        {
            query += 'and Type__c = :type ';
        }
        return (List<ecrf__c>) Database.query(query);
    }


    private List<ecrf__c> getPAWSProjectListByIDs(Set<ID> projectIDs)
    {
        return getPAWSProjectList(projectIDs, null, null);
    }

    private ecrf__c getPAWSProjectByID(ID projectID)
    {
        List<ecrf__c> result = getPAWSProjectListByIDs(new Set<ID> { projectID });
        return result.isEmpty() == true ? null : result.get(0);
    }

    private List<PAWS_Project_Flow_Site__c> getSiteAllByCountryID(Set<ID> ids)
    {
        // NOTE: PAWS_Project__c reference to ecrf__c
        return  [select 
                    Name,
                    PI_Name__c,
                    WFM_Site__r.Investigator__r.Last_Name__c,
                    WFM_Site__r.Investigator__r.First_Name__c,
                    PAWS_Project__c,
                    PAWS_Country__c,

                    Study_Start_Up_Expected_Date_BI_1__c,
                    Study_Start_Up_Expected_Date_BI_0__c,
                    Study_Start_Up_Expected_Date_BI_05__c,
                    Essential_Documents_Status_Description__c,
                    Site_Contracts_Status_Description__c,
                    Start_Up_Requirements_Status_Description__c,
                    Site_Approvals_Status_Description__c,
                    
                    Essential_Documents_Target_Date__c,
                    Essential_Documents_Complete_Date__c,
                    Start_Up_Requirements_Target_Date__c,
                    Start_Up_Requirements_Complete_Date__c,
                    Site_Approvals_Complete_Date__c,
                    Site_Approvals_Target_Date__c,
                    Site_Contracts_Complete_Date__c,
                    Site_Contracts_Target_Date__c,

                    Site_Approvals_Status__c,
                    Essential_Documents_Status__c,
                    Start_Up_Requirements_Status__c,
                    Site_Contracts_Status__c,
                    Study_Start_Up_Status__c,

                    Study_Start_Up_Complete_Date__c,
                    Study_Start_Up_Target_Date__c
                from
                    PAWS_Project_Flow_Site__c
                where
                    PAWS_Country__c = :ids];
    }

    private List<EW_Aggregated_Milestone__c> getAggregatedMilestonesByIdAndName(Set<ID> projectIDs, String milestoneName)
    {
        return  [select
                    Milestone_Target_Date__c,
                    Goal__c,
                    PAWS_Project_Flow__c
                from
                    EW_Aggregated_Milestone__c
                where
                    PAWS_Project_Flow__c = :projectIDs and Name = :milestoneName];
    }

    private EW_Aggregated_Milestone__c getAggregatedMilestoneByIdAndName(ID projectID, String milestoneName)
    {
        List<EW_Aggregated_Milestone__c> resultList = getAggregatedMilestonesByIdAndName(new Set<ID> { projectID }, milestoneName);
        return resultList.isEmpty() == true ? null : resultList.get(0);
    }

    private Set<ID> getActiveProjectIds()
    {
        Set<ID> ecrfIds = new Set<ID>();
        for(STSWR1__Flow_Instance__c eachInstance : [select STSWR1__Object_Id__c from STSWR1__Flow_Instance__c where STSWR1__Object_Type__c = 'ecrf__c' and STSWR1__Is_Active__c = true /* And STSWR1__Is_Completed__c = 'No'*/])
        {
            ecrfIds.add(eachInstance.STSWR1__Object_Id__c);
        }
        return ecrfIds;
    }

    /* ============= START trigger methods ============= */

    public static void beforeUpsert(List<PAWS_Project_Flow_Site__c> sites)
    {
        for(PAWS_Project_Flow_Site__c eachSite : sites)
        {
            //Status Calculations
            eachSite.Essential_Documents_Status__c = calculateStatus(
                eachSite.Is_Essential_Documents_Completed__c, eachSite.Essential_Documents_BI__c
            );
            eachSite.Site_Approvals_Status__c = calculateStatus(
                eachSite.Is_Site_Approvals_Completed__c, eachSite.Site_Approvals_BI__c
            );
            eachSite.Site_Contracts_Status__c = calculateStatus(
                eachSite.Is_Site_Contracts_Complete__c, eachSite.Site_Contracts_BI__c
            );
            eachSite.Start_Up_Requirements_Status__c = calculateStatus(
                eachSite.Is_Start_Up_Requirements_Completed__c, eachSite.Start_Up_Requirements_BI__c
            );
            
            //Aggregated Status Calculations
            eachSite.Essential_Documents_Status_Aggr__c = calculateStatus(
                eachSite.Is_Essential_Documents_Completed__c, eachSite.Essential_Documents_BI_Aggr__c
            );
            eachSite.Site_Approvals_Status_Aggr__c = calculateStatus(
                eachSite.Is_Site_Approvals_Completed__c, eachSite.Site_Approvals_BI_Aggr__c
            );
            eachSite.Site_Contracts_Status_Aggr__c = calculateStatus(
                eachSite.Is_Site_Contracts_Complete__c, eachSite.Site_Contracts_BI_Aggr__c
            );
            eachSite.Start_Up_Requirements_Status_Aggr__c = calculateStatus(
                eachSite.Is_Start_Up_Requirements_Completed__c, eachSite.Start_Up_Requirements_BI_Aggr__c
            );          
        }
    }

    private static String calculateStatus(Boolean isComplete, Decimal bi)
    {
        String status = 'Not Started';

        if(isComplete == true)
        {
            status = 'Completed';
        }
        else if(bi < 0)
        {
            status = 'Delayed';
        }
        else if(bi >= 1)
        {
            status = 'On Track';
        }
        else if(bi >= 0)
        {
            status = 'At Risk';
        }

        return status;
    }

    /* ============= END trigger methods ============= */
}