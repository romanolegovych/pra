/** Implements the Selector Layer of the object LaboratoryMethod__c
 * @author	Dimitrios Sgourdos
 * @version	07-Nov-2013
 */
public with sharing class LaboratoryMethodDataAccessor {
	
	// Global Constants
	public static final String	NOT_APPLICABLE_VALUE = 'N/A';
	
	/** Object definition for fields used in application for LaboratoryMethod
	 * @author	Dimitrios Sgourdos
	 * @version 08-Oct-2013
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('LaboratoryMethod__c');
	}
	
	
	/** Object definition for fields used in application for LaboratoryMethod with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 07-Nov-2013
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result =	referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'AnalyticalTechnique__c,';
		result += referenceName + 'AntiCoagulant__c,';
		result += referenceName + 'CompoundClass__c,';
		result += referenceName + 'Department__c,';
		result += referenceName + 'Detection__c,';
		result += referenceName + 'DetectionType__c,';
		result += referenceName + 'Location__c,';
		result += referenceName + 'Matrix__c,';
		result += referenceName + 'Proprietary__c,';
		result += referenceName + 'SampleAmount__c,';
		result += referenceName + 'SampleAmountValue__c,';
		result += referenceName + 'SamplePreparation__c,';
		result += referenceName + 'ShowOnMethodList__c,';
		result += referenceName + 'Species__c,';
		result += referenceName + 'StabilAgent_SpecialCond__c,';
		result += referenceName + 'StorageTemperature__c,';
		result += referenceName + 'TherapeuticArea__c,';
		result += referenceName + 'ValidationStudyIDs__c';
		return result;
	}
	
	
	/** Retrieve the Laboratory Method with Id equal to the given one. 
	 * @author	Dimitrios Sgourdos
	 * @version 15-Oct-2013
	 * @param	laboratoryMethodId		The Id of the Laboratory method that we want to retrieve
	 * @return	The retrieved Laboratory Method.
	 */
	public static LaboratoryMethod__c getById (String laboratoryMethodId) {
		String query = String.format(
								'SELECT {0},  ' +
								'(select {1} from LaboratoryMethodCompound__r ORDER BY Name), ' +
								'(select {2} from LaboratoryMethodComedication__r ORDER BY CreatedDate) ' +
								'FROM LaboratoryMethod__c ' +
								'WHERE Id = {3} ' + 
								'ORDER BY Name',
								new List<String> {
									getSObjectFieldString(),
									LaboratoryMethodCompoundDataAccessor.getSObjectFieldString(''),
									LaboratoryMethodComedicationDataAccessor.getSObjectFieldString(''),
									'\'' + laboratoryMethodId + '\''
								}
							);
		
		try {
			return (LaboratoryMethod__c) Database.query(query);
		} catch (Exception e) {
			system.debug('Query error: ' + query);
			return null;
		}
	}
	
	
	/** Retrieve the Laboratory Method with Name like the given one. 
	 * @author	Dimitrios Sgourdos
	 * @version 21-Oct-2013
	 * @param	likeName			The name that the query will be based on
	 * @return	The retrieved Laboratory Method instances.
	 */
	public static List<LaboratoryMethod__c> getByLikeName(String likeName) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM LaboratoryMethod__c ' +
								'WHERE  NAME LIKE {1}',
								new List<String> {
									getSObjectFieldString(),
									'\'%' + likeName + '%\''
								}
							);
		return (List<LaboratoryMethod__c>) Database.query(query);
	}
	
	
	/** Retrieve the Laboratory Methods that meets the given search criteria. 
	 * @author	Dimitrios Sgourdos
	 * @version 22-Oct-2013
	 * @param	searchLabMethod		The laboratory method that holds the search criteria
	 * @param	searchCompoundList	The compound list that holds the search criteria about compounds
	 * @param	limitation			The max number of laboratory methods that the query will return
	 * @return	The retrieved Laboratory Method instances.
	 */
	public static List<LaboratoryMethod__c> getBySearchCriteria(LaboratoryMethod__c searchLabMethod, List<LaboratoryMethodCompound__c> searchCompoundList, String limitation) {
		String query = String.format(
								'SELECT {0},  ' +
								'(select {1} from LaboratoryMethodCompound__r ORDER BY Name) ' +
								'FROM LaboratoryMethod__c {2}' + 
								'ORDER BY Name ' + 
								'LIMIT {3}',
								new List<String> {
									getSObjectFieldString(),
									LaboratoryMethodCompoundDataAccessor.getSObjectFieldString(''),
									createSearchCriteriaWhereClause(searchLabMethod, searchCompoundList),
									limitation
								}
							);
		return (List<LaboratoryMethod__c>) Database.query(query);
	}
	
	
	/** Create the where clause for searching laboratory methods by search criteria. 
	 * @author	Dimitrios Sgourdos
	 * @version 22-Oct-2013
	 * @param	searchLabMethod		The laboratory method that holds the search criteria
	 * @param	searchCompoundList	The compound list that holds the search criteria about compounds.
	 * @return	The where clause for searching laboratory methods by search criteria.
	 */
	public static String createSearchCriteriaWhereClause(LaboratoryMethod__c searchLabMethod, List<LaboratoryMethodCompound__c> searchCompoundList) { 
		// Initialize variables
		String whereClause = '';
		Boolean	compoundFlag = ! LaboratoryMethodCompoundService.allBlankNamesInLabCompoundsList(searchCompoundList);
		Boolean antiCoagulantFlag = LaboratorySelectionListsService.enableAssociatedSelectOption(
										LaboratorySelectionListsService.readMatrixValuesEnablingAnticoagulant(), 
										searchLabMethod.Matrix__c);
		Boolean detectionTechFlag = LaboratorySelectionListsService.enableAssociatedSelectOption(
										LaboratorySelectionListsService.readAnalyticalTechniqueValuesEnablingDetection(), 
										searchLabMethod.AnalyticalTechnique__c);
		
		// Create where clause
		if(compoundFlag
					|| searchLabMethod.Species__c!=null
					|| searchLabMethod.Matrix__c!=null
					||  searchLabMethod.AnalyticalTechnique__c!=null
					|| searchLabMethod.Location__c!=null
					|| searchLabMethod.Department__c!=NULL
					|| searchLabMethod.Proprietary__c!=null
					|| antiCoagulantFlag
					|| detectionTechFlag) {
			// Initialize whereClause
			whereClause  += ' WHERE Name != NULL AND ';
			
			// Add search criteria in case they are applicable
			if(searchLabMethod.Species__c!=null) {
				whereClause += ' Species__c = \'' + searchLabMethod.Species__c + '\' AND';
			}
			
			if(searchLabMethod.Matrix__c!=null) {
				whereClause += ' Matrix__c = \'' + searchLabMethod.Matrix__c + '\' AND';
			}
			
			if(searchLabMethod.AnalyticalTechnique__c!=null) {
				whereClause += ' AnalyticalTechnique__c = \'' + searchLabMethod.AnalyticalTechnique__c + '\' AND';
			}
			
			if(searchLabMethod.Location__c!=null) {
				whereClause += ' Location__c = \'' + searchLabMethod.Location__c + '\' AND';
			}
			
			if(searchLabMethod.Department__c!=null) {
				whereClause += ' Department__c = \'' + searchLabMethod.Department__c + '\' AND';
			}
			
			if(searchLabMethod.Proprietary__c!=null) {
				whereClause += ' Proprietary__c = \'' + searchLabMethod.Proprietary__c + '\' AND';
			}
			
			if(antiCoagulantFlag) {
				whereClause += ' AntiCoagulant__c = \'' + searchLabMethod.AntiCoagulant__c + '\' AND';
			}
			
			if(detectionTechFlag) {
				whereClause += ' Detection__c = \'' + searchLabMethod.Detection__c + '\' AND';
			}
			
			// Add the Laboratory Method Compounds
			if(compoundFlag) {
				whereClause += ' Id IN (select LaboratoryMethod__c from LaboratoryMethodCompound__c where ';
				for(LaboratoryMethodCompound__c tmpCompound : searchCompoundList) {
					if( String.isNotBlank(tmpCompound.Name) ) {
						whereClause += ' Name LIKE \'%' + tmpCompound.Name + '%\' OR';
					}
				}
				whereClause = whereClause.removeEnd('OR');
				whereClause += ')'; 
			} else {
				whereClause = whereClause.removeEnd('AND');
			}
			
		}
		
		return whereClause;
	}
	
	
	/** Retrieve the Laboratory Methods with Id inside the given set of ids. 
	 * @author	Dimitrios Sgourdos
	 * @version 19-Nov-2013
	 * @param	methodIds			The Id of the Laboratory methods that we want to retrieve
	 * @return	The retrieved Laboratory Methods.
	 */
	public static List<LaboratoryMethod__c> getBySetIds (Set<String> methodIds) {
		String query = String.format(
								'SELECT {0},  ' +
								'(select {1} from LaboratoryMethodCompound__r ORDER BY Name), ' +
								'(select {2} from LaboratoryMethodComedication__r ORDER BY CreatedDate) ' +
								'FROM LaboratoryMethod__c ' +
								'WHERE {3} ' + 
								'ORDER BY Name',
								new List<String> {
									getSObjectFieldString(),
									LaboratoryMethodCompoundDataAccessor.getSObjectFieldString(''),
									LaboratoryMethodComedicationDataAccessor.getSObjectFieldString(''),
									'Id IN :methodIds'
								}
							);
		return (List<LaboratoryMethod__c>) Database.query(query);
	}
}