public class DWF_BlackListHomeController {

	public Integer pageIndex {get;set;}
	public Integer maxIndex {get;set;}
	public String selectedView {get;set;}
	public String deleteBlacklistID {get;set;}
	public list<DWF_Blacklist__c> blackLists {get;set;} 
	public map<Integer, list<DWF_Blacklist__c>> blPageMap {get;set;}
	
	private static final String VIEW_RECENT = 'RecentBL';
	private static final String VIEW_MY_CREATED	= 'MyBL';
	private static final String VIEW_ALL = 'All';
	
	public DWF_BlackListHomeController() {
		pageIndex = 1;
		blPageMap = new map<Integer, list<DWF_Blacklist__c>>();
		selectedView = VIEW_ALL;
		searchBlacklists();
		getMapValue();
	}
	
	public list<selectoption> getFilterTypes() {
		transient list<selectoption> options = new list<selectoption>();
		options.add(new selectoption(VIEW_RECENT, 'Recently Created Blacklists'));
		options.add(new selectoption(VIEW_MY_CREATED, 'My Created Blacklists'));
		options.add(new selectoption(VIEW_ALL, 'All Blacklists'));
		return options;
	}
	
	public void searchBlacklists() {
		String soql = '';
		if (selectedView.equals(VIEW_RECENT)) {
			soql += 'select id, name, employee_id__c, DWF_System__r.Name from DWF_BlackList__c limit 100';
		} else if (selectedView.equals(VIEW_MY_CREATED)) {
			String userId = UserInfo.getUserId();
			soql += 'select id, name, employee_id__c, DWF_System__r.Name from DWF_BlackList__c where CreatedById = :userId';
		} else if (selectedView.equals(VIEW_All)) {
			soql += 'select id, name, employee_id__c, DWF_System__r.Name from DWF_BlackList__c';
		}
		list<DWF_Blacklist__c> bls = Database.query(soql);
		Integer mapIndex = pageIndex;
		blPageMap.put(mapIndex, new list<DWF_Blacklist__c>());
		for (Integer i = 0; i < bls.size(); i++) {
			if (i > 0 && Math.mod(i, 1000) == 0) {
				mapIndex++;
				blPageMap.put(mapIndex, new list<DWF_Blacklist__c>());
			}
			blPageMap.get(mapIndex).add(bls.get(i));
		}
		maxIndex = mapIndex;
		system.debug('============================================' + blPageMap);
		system.debug('============================================' + pageIndex);
		system.debug('============================================' + maxIndex);
	}
	
	public void getMapValue() {
		blackLists = blPageMap.get(pageIndex);
	}
	
	public void incrementPageIndex() {
		pageIndex++;
		getMapValue();
	}
	
	public void decrementPageIndex() {
		pageIndex--;
		getMapValue();
	}
	
	public void deleteBlacklist() {
		DWF_Blacklist__c blackList = [select Id from DWF_Blacklist__c where Id = :deleteBlacklistID];
		if (blackList != null) {
			delete blackList;
			searchBlacklists();
		}
	}
	
}