public with sharing class PBB_ServiceTaskToServiceImpact extends PBB_ServiceBase {

    public List<Service_Task_To_Service_Impact__c> serviceTaskToServiceImpacts {
        get{
            return (List<Service_Task_To_Service_Impact__c>) serviceRecords;
        }
        set{
            serviceRecords = value;
        }
    }
    public Service_Task_To_Service_Impact__c serviceTaskToServiceImpact {
        get{
            return (Service_Task_To_Service_Impact__c)serviceRecord;
        }
        set{
            serviceRecord = value;
        }
    }

    public Service_Task__c serviceTask {get;set;}
    public String keyPrefix {get;set;} 
    
    //public String serviceTaskId;
        
    public PBB_ServiceTaskToServiceImpact(String parentServiceId) {
        super(parentServiceId);

        Schema.DescribeSObjectResult r = Service_Impact_Question_Name__c.sObjectType.getDescribe();
        keyPrefix = r.getKeyPrefix();

        if(parentServiceId != null) {
            serviceTask = PBB_ServiceModelServices.getServiceTaskById((Id)parentServiceId);
        }else{
            serviceTask = new Service_Task__c();
        }
    }
    
    @TestVisible
    protected override String getServiceWord(){
        return 'TaskToServiceImpact';
    }
    
    public override void refreshServices(){
        serviceTaskToServiceImpacts = PBB_ServiceModelServices.getServiceTaskToServiceImpactsByServiceTaskId(parentServiceId);
    }
    
    public override void getDetailService() {
    }
    
    public override void preparationCreateOrEditService() {
        if(String.isBlank(getEditServiceId())) {
            serviceTaskToServiceImpact = new Service_Task_To_Service_Impact__c();
            serviceTaskToServiceImpact.Service_Task__c = parentServiceId;
        } else {
            refreshServices();
            Map<Id, Service_Task_To_Service_Impact__c> serviceTaskToServiceImpactMap = new Map<Id, Service_Task_To_Service_Impact__c>(serviceTaskToServiceImpacts);
            serviceTaskToServiceImpact = serviceTaskToServiceImpactMap.get(getEditServiceId());
        }
    }
    
    public override void removeService() {
        try {
            Map<Id, Service_Task_To_Service_Impact__c> serviceTaskToServiceImpactMap = new Map<Id, Service_Task_To_Service_Impact__c>(serviceTaskToServiceImpacts);
            Id deletedStToSiId = getEditServiceId();
            if ( String.isEmpty(deletedStToSiId) ){
                throw new ServiceModelException('Id of Service Task to Service Impact for deletion is undefined');
            }
            serviceTaskToServiceImpact = serviceTaskToServiceImpactMap .get( deletedStToSiId );
            if ( serviceTaskToServiceImpact == null ){
                throw new ServiceModelException('Service Task to Service Impact for deletion not found by id: ' + deletedStToSiId);
            }
            delete serviceTaskToServiceImpact;
            refreshServices();
        } catch(Exception e) {
            showErrorMessage(e);
        }
    }
    
    @TestVisible
    protected override Boolean isValidService() {
        isValid = false;
        Boolean isValidServiceTaskToServiceImpact = true;
        if(String.isBlank(serviceTaskToServiceImpact.Service_Impact_Question_Name__c)){
            isValidServiceTaskToServiceImpact = false;
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Service Impact Question Name: You must enter a value'));
        }
        return isValidServiceTaskToServiceImpact;
    }
    
}