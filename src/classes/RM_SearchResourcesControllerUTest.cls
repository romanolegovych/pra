/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_SearchResourcesControllerUTest {
    static Country__c country;
    static list<WFM_Location_Hours__c> hrs;
    static Employee_Details__c empl;
    static list<Employee_Status__c> lstEmplStatus;
    static list<WFM_Employee_Availability__c> avas;
    
    static list<WFM_EE_Therapeutic_Exp__c> theras;
    static list<WFM_EE_LANGUAGE__c> lang;
    static list<WFM_EE_Education__c> edu;
    static list<WFM_EE_CERTIFICATION__c> cer;
    static list<WFM_EE_SYSTEM_EXPERIENCE__c> sys;
    static list<WFM_EE_PHASE_EXPERIENCE__c> phase;
    static WFM_Client__c client;
    static WFM_Contract__c  contract;
    static WFM_Project__c project;
    static list<WFM_EE_Work_History__c> history;
    static List<WFM_Employee_Assignment__c> assigns;
    static WFM_employee_Allocations__c alloc;
    static Root_Folder__c root1;
    static Sub_Folder__c sub1;
    static string selectedMonth;
    static integer intMonth = 3;
    static void init(){
    	selectedMonth = '1';
        list<string> months = RM_Tools.GetMonthsList(-4, 5);
        country = new Country__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0, RM_Region__c='TestRegion');
        insert country;
        
        hrs = new List<WFM_Location_Hours__c>();
        for (string month:months){
            WFM_Location_Hours__c hr = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + month,YEAR_MONTH__c=month,NBR_Business_Day_In_Month__c=22);
            hrs.add(hr); 
        }
        insert hrs;  
        empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='1000', Employee_Unique_Key__c='1000', First_Name__c='John', Last_Name__c='TestSmith', fullname_noaccent__c = 'TestSmith, John',
        email_Address__c='JSmith@gmail.com', Job_Class_Desc__c='TEST',
        Function_code__c='TT',Buf_Code__c='TSTTT', State_Province__c = 'TestState', Business_Unit__c='TST', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), 
        Manager_Last_Name__c='TestManager',  Manager_First_Name__c='TestFirst', Manager_NoAccent__c='TestManager, TestFirst', 
        FTE_Equivalent__c = 0.8, NBR_PRA_Monitoring_Yrs__c = 5.0, NBR_Non_PRA_Monitoring_Yrs__c = 6.0, NBR_AT_PRA_Yrs__c=5.0);
        insert empl;
        
        lstEmplStatus = new list<Employee_Status__c>();
        Employee_Status__c emplStatus = new Employee_Status__c(employee_status__c = 'AA', employee_Type__c = 'Employees', Employee_Group__c = 'Active');
        lstEmplStatus.add(emplStatus);
        insert lstEmplStatus;
        
        avas = new list<WFM_Employee_Availability__c>();
      
        for (integer i = 0; i <months.size(); i++){
            WFM_Employee_Availability__c a = new WFM_Employee_Availability__c(EMPLOYEE_ID__c=empl.id, year_month__c=months[i], Availability_Unique_Key__c=empl.id+months[i], 
            pto_unit__c='Hours', PTO_value__c=8,  location_Hours__c=hrs[i].id);
            avas.add(a);
        }
        insert avas;
        //allocation       
      
       
        theras = new WFM_EE_Therapeutic_Exp__c[]{
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Colorectal', employee_id__c=empl.id, Therapeutic_area__c='TestOncology',Indication_Group__c='Solid Tumors',NBR_Total_Monitoring_Exp__c=4.29, NBR_PRA_Monitoring_Exp_Yrs__c=4.29),
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Colorectal', employee_id__c=empl.id, Therapeutic_area__c='TestOncology',Indication_Group__c='Solid Tumors',Role__c='UNDEFINED', NBR_Role_Experience_Yrs__c=0.43), 
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Colorectal', employee_id__c=empl.id, Therapeutic_area__c='TestOncology',Indication_Group__c='Solid Tumors',Study_Drug_Type__c='UNDEFINED', NBR_Study_Drug_Yrs__c=0.43),   
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Colorectal', employee_id__c=empl.id, Therapeutic_area__c='TestOncology',Indication_Group__c='Solid Tumors',Subject_Population__c='UNDEFINED'),                                  
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Sedation', employee_id__c=empl.id, Therapeutic_area__c='TestAnesthesiology',Indication_Group__c='Anesthesia',Role__c='Clinical Team Manager', NBR_Role_Experience_Yrs__c=1.4),
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Sedation', employee_id__c=empl.id, Therapeutic_area__c='TestAnesthesiology',Indication_Group__c='Anesthesia',NBR_Total_Monitoring_Exp__c=4.29, NBR_PRA_Monitoring_Exp_Yrs__c=4.29),
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Surgical Anesthesia', employee_id__c=empl.id, Therapeutic_area__c='TestAnesthesiology',Indication_Group__c='Anesthesia',Role__c='UNDEFINED', NBR_Role_Experience_Yrs__c=1.92),
            new WFM_EE_Therapeutic_Exp__c(primary_indication__c='Gout', employee_id__c=empl.id, Therapeutic_area__c='TestEndocrinology',Indication_Group__c='Metabolic',Role__c='Clinical Team Manager', NBR_Role_Experience_Yrs__c=0.15)
            
        
        };
        insert theras;
        Edu = new WFM_EE_Education__c[]{
            new WFM_EE_Education__c(Degree_Type__c='BA', employee_id__c=empl.id,  Institution__c='Instituation', Month_Obtained__c= Date.parse('4/1/2010'))       
        };
        insert Edu;
        lang = new WFM_EE_LANGUAGE__c[]{
            new WFM_EE_LANGUAGE__c(Language__c='TestEnglish',employee_id__c=empl.id, Level_of_Med_Term__c='Low',   Level_of_Speak__c='High',  Level_of_Write__c='High')
        };
        insert lang;
        
        cer = new WFM_EE_CERTIFICATION__c[]{
            new WFM_EE_CERTIFICATION__c(Certification_Affilication_Name__c='Certification', employee_id__c=empl.id, Effective_Date__c = Date.parse('4/1/2010'),Granting_Institution__c='Granting')
        };
        insert cer;
        
       sys = new WFM_EE_SYSTEM_EXPERIENCE__c[]{
            new WFM_EE_SYSTEM_EXPERIENCE__c(CRS_System__c='Sys', employee_id__c=empl.id,Proficiency_Level__c = 1)
        };
        insert sys;
        
        phase = new WFM_EE_PHASE_EXPERIENCE__c[]{
            new WFM_EE_PHASE_EXPERIENCE__c(phase__c='IV', employee_id__c=empl.id,Phase_experience_yrs__c=3)
        };
        insert phase;
        client = new WFM_Client__c(name='TestClient', Client_Name__c='TestClinet_Name', Client_Unique_Key__c='TestClient');
        insert client;
        contract = new WFM_Contract__c(name='TestContract', Contract_Status__c='AA', Client_ID__c=client.id, Contract_Unique_Key__c='TestContract');
        insert contract;
        project = new WFM_Project__c(name='TestProject', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProject');
        insert project;
        alloc= new WFM_employee_Allocations__c(EMPLOYEE_ID__c=empl.id, Project_Buf_Code__c='KCI',Project_Country__c='United State', Project_ID__c=project.id, Project_Function__c='Clinical Research Associate', 
            Status__c='Confirmed', Allocation_Unique_Key__c=empl.name+project.name+'KCICRUnited State',  allocation_start_date__c = Date.today(), allocation_end_date__c = Date.today().addMonths(5));
      
        insert alloc;
        //assignment
        assigns = new List<WFM_Employee_Assignment__c>();
        for (Integer i = 0; i < months.size(); i++){    
            WFM_Employee_Assignment__c ass =  new WFM_Employee_Assignment__c(Assignment_Unique_Key__c=avas[i].id+(string)alloc.id, EMPLOYEE_ID__c = empl.id, Availability_FK__c=avas[i].id, Allocation_Key__c=alloc.id, Year_Month__c=months[i], 
            Assigned_value__c=0.1, assigned_unit__c = 'FTE');
            
            assigns.add(ass);
        }
        insert assigns;
      
        // four month history
        list <string> monthHistry = RM_Tools.GetMonthsList(-4, -1);
        history = new list<WFM_EE_Work_History__c>();
    
        for (integer i = 0; i < monthHistry.size(); i++){
            WFM_EE_Work_History__c h = new WFM_EE_Work_History__c(Project_ID__c=project.id, employee_id__c=empl.id, Availability__c = avas[i].id, Work_History_Unique_Key__c=empl.id+(string)project.id+monthHistry[i]+'KCI',Buf_Code__c='KCI',year_month__c=monthHistry[i], Hours__c=20);
            history.add(h);
        }
        insert history;
    }
    static testMethod void TestDropDowns() {
    
        Employee_Status__c emplStatus = new Employee_Status__c(employee_status__c = 'AA', employee_Type__c = 'Employees', Employee_Group__c = 'Active');
        emplstatus.Status_Desc__c='Description';
        insert emplStatus;
        
        PRA_Business_Unit__c PBU = new PRA_Business_Unit__c(name = 'Clinical Informatics',Business_Unit_Code__c='CI');        
        insert PBU;
        
        Job_Class_Desc__c jcb = new Job_Class_Desc__c(name = 'Clinical data Admin',Job_Class_Code__c='j',Job_Class_ExtID__c='j');        
        insert jcb;
        Test.startTest();
        
        RM_SearchResourcesController ResController = new RM_SearchResourcesController();   
        List<SelectOption> option1 = ResController.getSearchOnItems();
        //system.debug('---options---' + options[0].getValue());
        system.assert(option1[0].getValue() == 'Employee'); 
        
        option1.clear();
        option1 = ResController.getTheropeticAreas();
        system.assert(option1[0].getValue() == '');
        system.assert(option1.size() > 0);
        option1.clear();
        option1 = ResController.getMonitoringExps();
        system.assert(option1[0].getValue() == '');
        system.assert(option1.size() > 0);
        
        option1.clear();
        option1 = ResController.getAvailabilityItems();
        system.assert(option1[0].getValue() == 'FTE');
        system.assert(option1.size() > 0);
        
        option1.clear();
        option1 = ResController.getItems();
        system.assert(option1[0].getValue() == '');
        system.assert(option1.size() > 0);
        
       
        
        option1.clear();
        option1 = ResController.getBusinessUnit();
        system.assert(option1[0].getValue() == '');
        system.assert(option1.size() > 0);
        
        option1.clear();
        option1 = ResController.getStatus();
        system.assert(option1[0].getValue() == '');
        system.assert(option1.size() > 0);
        
        option1.clear();
        option1 = ResController.getRMRegion();
        system.assert(option1[0].getValue() == '');
        system.assert(option1.size() > 0);
        
        option1.clear();
        option1 = ResController.getSubRegion();
        system.assert(option1[0].getValue() == '');
        system.assert(option1.size() > 0);
        
        option1.clear();
        option1 = ResController.getmonthlabList();
        
        system.assert(option1.size() == 12);
       
        option1.clear();
        option1 = ResController.getmonthmoreList();
        
        system.assert(option1.size() > 0);
        
        option1.clear();
        option1 = ResController.getCodes();
        system.assert(option1[0].getValue() == '');
        system.assert(option1.size() > 0);
        
        option1.clear();
        option1 = ResController.getFuncCodes();
        system.assert(option1[0].getValue() == '');
        system.assert(option1.size() > 0);
        
        option1.clear();
        option1 = ResController.getproficiencyItems();
        system.assert(option1[0].getValue() == '');
        system.assert(option1.size() > 0);
        
        option1.clear();
        option1 = ResController.getTerms();
        system.assert(option1[0].getValue() == 'Term');
        system.assert(option1.size() > 1);
       
        
        option1.clear();
        option1 = ResController.getResourceFolders();
       // system.assert(option1[0].getValue() == 'Resources');
        system.assert(option1.size() > 0);
        Test.stopTest(); 
        
    }
    static testMethod void TestSearchType() {
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController();   
        ResController.searchOn = 'Manager';
        PageReference page = ResController.SearchType();
        system.assert(ResController.searchObject  == 'Employee_Details__c');
        ResController.searchOn = 'Project ID';
        page = ResController.SearchType();
        system.assert(ResController.searchObject  == 'WFM_Project__c');
        ResController.searchOn = 'Client ID';
        page = ResController.SearchType();
        system.assert(ResController.searchObject  == 'WFM_Client__c');
        Test.stopTest(); 
    }
    static testMethod void TestIndicationbasedOnTherapetica(){
        WFM_Therapeutic__c ther = new WFM_Therapeutic__c(primary_indication__c='Colorectal', name='TestOncology',Indication_Group__c='Solid Tumors');
        insert ther;
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController();  
        ResController.strTheropeticArea='TestOncology';
        PageReference page = ResController.getIndicationsBasedOnTheropeticArea();
        
        system.assert(ResController.Indications.size() > 0);
        Test.stopTest(); 
    }
    static testMethod void TestJobClassBasedonBU(){
     	PRA_Business_Unit__c praBU = new PRA_Business_Unit__c(name='BU', Business_Unit_Code__c='BC');
     	insert praBU;
     	WFM_BU_Group_Mapping__c buGroup = new WFM_BU_Group_Mapping__c(Group_Name__c='TestGroup', PRA_Business_Unit__c=praBU.ID);
     	insert buGroup;
     	Job_Class_Desc__c jobClass = new Job_Class_Desc__c(name='TestJob', Job_Class_Code__c = 'TJ', Job_Class_ExtID__c='TestJob',IsClinical__c= true );
        insert jobClass;
        WFM_JC_Group_Mapping__c jcGroup = new WFM_JC_Group_Mapping__c(Group_Name__c='TestGroup', Job_Class_Desc__c=jobClass.ID);
        insert jcGroup;
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController();  
        ResController.selectedBusinessUnit.add('BU');
        PageReference page = ResController.getJobClassWithBU();
        
        system.assert(ResController.jobClass.size() > 0);
        Test.stopTest(); 
    }
    static testMethod void TestStateBasedonSubRegion(){
     	init();
     	WFM_Sub_Region__c subRegion = new WFM_Sub_Region__c(Country__c=country.ID, State_Provience__c='TestState', Sub_Region__c='TestSub');
     	insert subRegion;
     	list<string> lstSub = new list<string>();
     	lstSub.add('TestSub');
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController();  
        ResController.selectedSubRegion= lstSub;
        PageReference page = ResController.getStateBySubRegion();
        
        system.assert(ResController.lstState.size() > 0);
        Test.stopTest(); 
    }
    static testMethod void TestCountryBasedonRMRegion(){
        	
     	list<string> lstRMRegion = new list<string>();
     	lstRMRegion.add('TestRegion');
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController();  
        ResController.selectedRMRegion= lstRMRegion;
        PageReference page = ResController.getCountryByRegion();
        
        system.assert(ResController.lstCountry.size() > 0);
        Test.stopTest(); 
    }
    static testMethod void TestTextSearch(){
        init();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        ResController.strSearchText='Test';
        ResController.searchOn = 'Project ID';      
        ResController.search();    
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');
        ResController.strSearchText='Test';
        ResController.searchOn = 'Client/Sponsor';      
        ResController.search();    
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');    
        ResController.strSearchText='Test';
        ResController.searchOn = 'Employee';      
        ResController.search(); 
        system.debug('---searchResult---' + ResController.AllAvails[0].lastname);   
        system.assert(ResController.AllAvails[0].lastname.indexOf('Test') > -1);    
        ResController.strSearchText='Test';
        ResController.searchOn = 'Manager';      
        ResController.search();    
        system.assert(ResController.AllAvails[0].lastname=='TestSmith'); 
        //exact search
        ResController.strSearchText='TestProject';
        ResController.selSearch = 'TestProject';
        ResController.searchOn = 'Project ID';      
        ResController.search();    
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');
        ResController.strSearchText='TestClient';
        ResController.selSearch = 'TestClient';
        ResController.searchOn = 'Client/Sponsor';      
        ResController.search();    
        system.assert(ResController.AllAvails[0].lastname=='TestSmith'); 
        
        ResController.strSearchText='TestSmith, John';
        ResController.searchOn = 'Employee';      
        ResController.search(); 
        system.debug('---searchResult---' + ResController.AllAvails[0].lastname);   
        system.assert(ResController.AllAvails[0].lastname == 'TestSmith');    
        ResController.strSearchText='TestManager, TestFirst';
        ResController.searchOn = 'Manager';      
        ResController.search();    
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');    
        Test.stopTest(); 
    }
    static testMethod void TestSearchLanguage(){
        init();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        ResController.strLanguage = 'TestEnglish';
        ResController.blnLangProficiency = true;
        ResController.strMedical = 'Low';
        ResController.strSpeaking = 'High';
        ResController.strWriting = 'High';
        ResController.search();    
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');
        ResController.strSpeaking = 'Medium';
        ResController.strWriting = 'Medium'; 
        ResController.search();    
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');
        ResController.strSpeaking = 'Low';
        ResController.strWriting = 'Low'; 
        ResController.search();  
          
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');
        ResController.strMedical = 'Medium';
        ResController.strSpeaking = '';
        ResController.search();    
        system.assert(ResController.AllAvails.size() == 0);
        ResController.strMedical = 'High';
         ResController.strWriting = ''; 
        ResController.search();    
        system.assert(ResController.AllAvails.size() == 0);
        Test.stopTest(); 
    }
     static testMethod void TestSearchLanguagewithTextSearch(){
        init();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        ResController.strLanguage = 'TestEnglish';
        ResController.blnLangProficiency = true;
        ResController.strMedical = 'Low';
        ResController.strSpeaking = 'High';
        ResController.strWriting = 'High';
        ResController.strSearchText='TestProject';
        ResController.selSearch = 'TestProject';
        ResController.searchOn = 'Project ID'; 
        ResController.strTheropeticArea = 'TestOncology';  
        ResController.search();    
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');
      
        Test.stopTest(); 
    }
    static testMethod void TestTheropeticArea(){
        init();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        ResController.strTheropeticArea = 'TestOncology';
        ResController.search();    
        system.assert(ResController.AllAvails.size() == 0);
      /*  list <string> indication = new list<string>();
        indication.add('Solid Tumors~Colorectal');
        ResController.selectedIndications = indication;
        ResController.search(); 
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');
        ResController.selectedIndications = null;
        ResController.dblMonitoringExperience = 4.0;
        ResController.search(); 
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');
        ResController.blnPraOnly = true;
        ResController.search(); 
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');*/
        Test.stopTest(); 
    }
    static testMethod void TestMonitorExp(){
        init();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        ResController.dblMonitoringExperience = 4.0;
        ResController.blnPraOnly = false;
        ResController.search(); 
        system.assert(ResController.AllAvails.size() > 0);
        ResController.blnPraOnly = true;
        ResController.search(); 
        system.assert(ResController.AllAvails.size() > 0);
        Test.stopTest(); 
    }
    static testMethod void TestSearchAvailability(){
        init();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        ResController.dblMinAvailability = 0.5;
        ResController.strAvailType = 'FTE';
         ResController.strSelectedMonth = selectedMonth;
        ResController.strSelectmoreMonth = intMonth;
        ResController.search(); 
        //system.assert(ResController.AllAvails.size() > 0);
        Test.stopTest();        
    }
    
    static testMethod void TestAvailabilityinHr(){
        init();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        
        ResController.dblMinAvailability = 10;
        ResController.strAvailType = 'Hours';
        ResController.strSelectedMonth = selectedMonth;
        ResController.strSelectmoreMonth = intMonth;
        ResController.search(); 
      
        //system.assert(ResController.AllAvails.size() > 0);
        Test.stopTest(); 
    }
    static testMethod void TestSearchAvailabilitywithOther(){
        init();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        ResController.strLanguage = 'TestEnglish';
        ResController.blnLangProficiency = false;
        ResController.dblMinAvailability = 0.5;
        ResController.strAvailType = 'FTE';
         ResController.strSelectedMonth = selectedMonth;
        ResController.strSelectmoreMonth = intMonth;
        system.debug('---------availability TEST---------'+RM_AssignmentsService.GetAvailabilityByEmployee(empl.id, RM_Tools.GetMonthsList(0, 5)));
        ResController.search(); 
        //system.assert(ResController.AllAvails.size() > 0);
        Test.stopTest();        
    }
   static testMethod void TestSearchAvailabilitywithOtherTwo(){
        init();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        ResController.strLanguage = 'TestEnglish';
        ResController.blnLangProficiency = false;
        ResController.strTheropeticArea = 'TestOncology';
        ResController.dblMinAvailability = 0.5;
        ResController.strAvailType = 'FTE';
         ResController.strSelectedMonth = selectedMonth;
        ResController.strSelectmoreMonth = intMonth;
        ResController.search(); 
        //system.assert(ResController.AllAvails.size() > 0);
        Test.stopTest();        
    }
    static testMethod void TestTenure(){
        init();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        ResController.dblMinTenure = 3.0;
        ResController.search(); 
      
        system.assert(ResController.AllAvails.size() > 0);
        Test.stopTest(); 
    }
   /*  static testMethod void TestDate(){
        init();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        ResController.startDate = '1/1/2009';
        ResController.endDate = '1/1/2010';
        
        ResController.search(); 
      
        system.assert(ResController.AllAvails.size() > 0);
        
        empl.Term_Date__c = date.parse('12/27/2011');
        update empl;
        ResController.endDate = '1/1/2012';
        ResController.selectedTerm = 'Term';
        ResController.search(); 
      
        system.assert(ResController.AllAvails.size() > 0);
        Test.stopTest(); 
    }  */
    static testMethod void TestSearchRoleLocationBufCountry(){
        init();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController();
        list <string> lsRole = new list<string>();
        lsRole.add('TEST'); 
        ResController.selectedRoles = lsRole;
       
        ResController.search(); 
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');
        
        list<string> lsState = new list<string>();
        lsState.add('TestState');
        ResController.selectedState = lsState;
        
        list<string> lsBuf = new list<string>();
        lsBuf.add('TST');
        ResController.selectedCodes = lsBuf;
        
        list<string> lsFunc = new list<string>();
        lsFunc.add('TT');
        ResController.selectedFuncCodes = lsFunc;
        
        list<string> lsCountry = new list<string>();
        lsCountry.add('TestCountry');
        ResController.selectedCountry = lsCountry;
        ResController.search(); 
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');
        
        Test.stopTest(); 
    }
    static testMethod void TestExport(){
        init();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        PageReference page= ResController.exportExcel();
        system.assert(page.getUrl()== '/apex/RM_ExcelResourceList');
        ResController.strSearchText='Test';
        ResController.searchOn = 'Project ID';
        ResController.search();
        page= ResController.exportExcel();
        system.assert(ResController.AllAvails[0].lastname=='TestSmith');
        
        
        Test.stopTest(); 
    }
    static void initSavedSearch(){
        
        //create and query fake roots//
        root1 = new Root_Folder__c(Name = 'root1');      
        insert root1;
        //create and query fake subs//
        sub1 = new Sub_Folder__c(Name = 'sub1', Root__c = root1.id, Custom__c = false, User__c = UserInfo.getUserID(), Sub_Folder_Unique_Key__c='sub1'+root1.id + UserInfo.getUserID());
        
        insert sub1;        
    }
    static testMethod void TestSave(){
        initSavedSearch();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        
        
        ResController.saveSubFolder = sub1.id;
        ResController.searchName = 'Test';
        ResController.selectedStatus = new list<string>();
        ResController.selectedStatus.add('');
        ResController.selectedBusinessUnit = new list<string>();
        ResController.selectedBusinessUnit.add('');
        
        ResController.selectedRoles = new list<string>();
        ResController.selectedRoles.add('TEST');
        ResController.selectedCodes = new list<string>();
        ResController.selectedCodes.add('TST');
        ResController.selectedFuncCodes = new list<string>();
        ResController.selectedFuncCodes.add('TT');
        ResController.selectedCountry = new list<string>();
        ResController.selectedCountry.add('TestCountry');
        ResController.selectedRMRegion = new list<string>();
        ResController.selectedRMRegion.add('RMRegion');
        ResController.selectedSubRegion = new list<string>();
        ResController.selectedSubRegion.add('SubRegion');
        ResController.strTheropeticArea = 'TestOncology';
        ResController.selectedIndications = new list<string>();
        ResController.selectedIndications.add('Solid Tumors~Colorectal');
        ResController.selectedState = new list<string>();
        ResController.selectedState.add('TestState'); 
        ResController.strLanguage = 'TestEnglish';
        ResController.blnLangProficiency = true;
        ResController.strMedical = 'Low';
        ResController.strSpeaking = 'High';
        ResController.strWriting = 'High'; 
        ResController.dblMonitoringExperience = 4.0;
        ResController.blnPraOnly = false;  
        ResController.dblMinAvailability = 80;
        ResController.strAvailType = 'Hours';   
        ResController.strSearchText='Test';
        ResController.searchOn = 'Project ID';
        PageReference page= ResController.saveSearch();
        Saved_Search__c searchName = RM_OtherService.getSavedSearch(ResController.searchName,UserInfo.getUserId(), sub1.id );
        list<Search_Detail__c>  details = RM_OtherService.getSavedSearchDetail(searchName.id);
        system.assert(searchName.name == ResController.searchName);
        system.assert(details[0].name == ResController.searchOn);
        //test editSearch
        PageReference pageRef = new PageReference('/apex/RM_SearchResources?cid=' + searchName.id);
     
        Test.setCurrentPage(pageRef);
        RM_SearchResourcesController ResController1 = new RM_SearchResourcesController(); 
        system.assert(ResController1.searchOn=='Project ID');
        system.assert(ResController1.strLanguage=='TestEnglish');
        system.assert(ResController1.strAvailType=='Hours');
        Test.stopTest(); 
    }
      static testMethod void TestSave2(){
        initSavedSearch();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        
        
        ResController.saveSubFolder = sub1.id;
        ResController.searchName = 'Test';
        ResController.selectedStatus = new list<string>();
        ResController.selectedStatus.add('');
        ResController.selectedRoles = new list<string>();
        ResController.selectedRoles.add('');
        ResController.selectedBusinessUnit = new list<string>();
        ResController.selectedBusinessUnit.add('Test');
        ResController.selectedCodes = new list<string>();
        ResController.selectedCodes.add('');
        ResController.selectedFuncCodes = new list<string>();
        ResController.selectedFuncCodes.add('');
        ResController.selectedCountry = new list<string>();
        ResController.selectedCountry.add('');
         ResController.selectedRMRegion = new list<string>();
        ResController.selectedRMRegion.add('');
        ResController.selectedSubRegion = new list<string>();
        ResController.selectedSubRegion.add('');
        ResController.selectedIndications = new list<string>();
        ResController.selectedIndications.add('');
        ResController.selectedState = new list<string>();
        ResController.selectedState.add(''); 
        
        ResController.dblMonitoringExperience = 4.0;
        ResController.blnPraOnly = true; 
        ResController.dblMinAvailability = 0.5;
        ResController.strAvailType = 'FTE'; 
        ResController.dblMinTenure = 3.0;   
        ResController.startDate = '1/1/2009';
        ResController.endDate = '1/1/2010';
        ResController.selectedTerm = 'Hire';
        ResController.strSearchText='Test';
        ResController.searchOn = 'Client/Sponsor Name';
        PageReference page= ResController.saveSearch();
        Saved_Search__c searchName = RM_OtherService.getSavedSearch(ResController.searchName,UserInfo.getUserId(), sub1.id );
        list<Search_Detail__c>  details = RM_OtherService.getSavedSearchDetail(searchName.id);
        system.assert(searchName.name == ResController.searchName);
        system.assert(details[0].name == ResController.searchOn);
        //test editSearch
        PageReference pageRef = new PageReference('/apex/RM_SearchResources?cid=' + searchName.id);
     
        Test.setCurrentPage(pageRef);
        RM_SearchResourcesController ResController1 = new RM_SearchResourcesController(); 
        system.assert(ResController1.searchOn=='Client/Sponsor Name');
        system.assert(ResController1.dblMonitoringExperience == 4.0);
        system.assert(ResController1.startDate == '1/1/2009');
        Test.stopTest(); 
    }
     static testMethod void TestSave3(){
        initSavedSearch();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        
        
        ResController.saveSubFolder = sub1.id;
        ResController.searchName = 'Test';
        ResController.selectedStatus = new list<string>();
        ResController.selectedStatus.add('Term');
        ResController.selectedRoles = new list<string>();
        ResController.selectedRoles.add('');
        ResController.selectedCodes = new list<string>();
        ResController.selectedCodes.add('');
        ResController.selectedFuncCodes = new list<string>();
        ResController.selectedFuncCodes.add('');
        ResController.selectedCountry = new list<string>();
        ResController.selectedCountry.add('');
        ResController.selectedRMRegion = new list<string>();
        ResController.selectedRMRegion.add('');
        ResController.selectedSubRegion = new list<string>();
        ResController.selectedSubRegion.add('');
        
        ResController.selectedIndications = new list<string>();
        ResController.selectedIndications.add('');
        ResController.selectedState = new list<string>();
        ResController.selectedState.add(''); 
        
        
        ResController.strSearchText='Test';
        ResController.searchOn = 'Employee';
        PageReference page= ResController.saveSearch();
        Saved_Search__c searchName = RM_OtherService.getSavedSearch(ResController.searchName,UserInfo.getUserId(), sub1.id );
        list<Search_Detail__c>  details = RM_OtherService.getSavedSearchDetail(searchName.id);
        system.assert(searchName.name == ResController.searchName);
        system.assert(details[0].name == ResController.searchOn);
        //test editSearch
        PageReference pageRef = new PageReference('/apex/RM_SearchResources?cid=' + searchName.id);
     
        Test.setCurrentPage(pageRef);
        RM_SearchResourcesController ResController1 = new RM_SearchResourcesController(); 
        system.assert(ResController1.searchOn=='Employee');
        Test.stopTest(); 
    }
    static testMethod void TestSave4(){
        initSavedSearch();
        Test.startTest();
        RM_SearchResourcesController ResController = new RM_SearchResourcesController(); 
        
        ResController.saveSubFolder = sub1.id;
        ResController.searchName = 'Test';
        ResController.selectedStatus = new list<string>();
        ResController.selectedStatus.add('');        
        ResController.selectedRoles = new list<string>();
        ResController.selectedRoles.add('');
        ResController.selectedCodes = new list<string>();
        ResController.selectedCodes.add('');
        ResController.selectedFuncCodes = new list<string>();
        ResController.selectedFuncCodes.add('');
        ResController.selectedCountry = new list<string>();
        ResController.selectedCountry.add('');
         ResController.selectedRMRegion = new list<string>();
        ResController.selectedRMRegion.add('');
        ResController.selectedSubRegion = new list<string>();
        ResController.selectedSubRegion.add('');
        ResController.selectedIndications = new list<string>();
        ResController.selectedIndications.add('');
        ResController.selectedState = new list<string>();
        ResController.selectedState.add(''); 
        
        
        ResController.strSearchText='Test';
        ResController.searchOn = 'Manager';
        PageReference page= ResController.saveSearch();
        Saved_Search__c searchName = RM_OtherService.getSavedSearch(ResController.searchName,UserInfo.getUserId(), sub1.id );
        list<Search_Detail__c>  details = RM_OtherService.getSavedSearchDetail(searchName.id);
        system.assert(searchName.name == ResController.searchName);
        system.assert(details[0].name == ResController.searchOn);
        //test editSearch
        PageReference pageRef = new PageReference('/apex/RM_SearchResources?cid=' + searchName.id);
     
        Test.setCurrentPage(pageRef);
        RM_SearchResourcesController ResController1 = new RM_SearchResourcesController(); 
        system.assert(ResController1.searchOn=='Manager');
        Test.stopTest(); 
    }
}