public with sharing class PAWS_RevisedChangedTemplateController 
{
	/************************* PROPERTIES REGION ****************************/
    
    public String MessageId { get; set; }
    
    public STSWR1__Email_Message__c Message 
    {
        get
        {
            if(Message == null && MessageId != null)
            {
                Message = [select STSWR1__Additional_Data__c from STSWR1__Email_Message__c where Id = :MessageId];
            }
            return Message;
        }
        set;
    }
    
    public List<Project> Projects
    {
        get
        {
            if(Projects == null && Message != null)
            {
            	List<String> ids = Message.STSWR1__Additional_Data__c.split(',');
            	
            	Map<ID, Project> projectMap = new Map<ID, Project>();
                for(STSWR1__Gantt_Step_Property__c property : [select Project__r.Name, STSWR1__Step__r.Name, STSWR1__Revised_End_Date__c, STSWR1__Revised_Comment__c from STSWR1__Gantt_Step_Property__c where Id in :ids order by STSWR1__Step__r.STSWR1__Index__c])
                {
                	if(!projectMap.containsKey(property.Project__r.Id)) projectMap.put(property.Project__r.Id, new Project(property.Project__r));
                	projectMap.get(property.Project__r.Id).Properties.add(property);
                }
                
                Projects = projectMap.values();
            }
            return Projects;
        }
        set;
    }

    public class Project
    {
    	public Project(ecrf__c project)
    	{
    		this.Project = project;
    		this.Properties = new List<STSWR1__Gantt_Step_Property__c>();
    	}
    	
    	public ecrf__c Project {get;set;}
    	public List<STSWR1__Gantt_Step_Property__c> Properties {get;set;}
    }
}