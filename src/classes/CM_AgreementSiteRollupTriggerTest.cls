/** 
     * @description Test class CM_AgreementSiteRollupTrigger
     * @author      LavakusaReedy Poluru
     * @date        Created: 30-Jan-2015
     */

@isTest
public class CM_AgreementSiteRollupTriggerTest{
    private static final RecordType RT_Agreement = [SELECT Id from RecordType WHERE Name = 'Contract Agreement'];
    private static final RecordType RT_Template = [SELECT Id from RecordType WHERE Name = 'Contract Template'];
       
    /** 
     * @description Test class testCreateNewAgreement
     * @author      LavakusaReedy Poluru
     * @date        Created: 30-Jan-2015
     */
    static testMethod void testCreateNewAgreement() {
        WFM_Client__c testClient = new WFM_Client__c();
        WFM_Contract__c testContract = new WFM_Contract__c();       
        WFM_Project__c testProject = new WFM_Project__c();
        WFM_Protocol__c testProtocol = new WFM_Protocol__c();
        WFM_Site_Detail__c testSite = new WFM_Site_Detail__c();
        Protocol_Country__c testProtocolCountry = new Protocol_Country__c();
        Country__c testcountry = new Country__c();
        Apttus__APTS_Agreement__c newAgreement_Site = new Apttus__APTS_Agreement__c();
        Apttus__APTS_Agreement__c newAgreement_Protocol = new Apttus__APTS_Agreement__c();
        Apttus__APTS_Agreement__c newAgreement_Protocol_Country = new Apttus__APTS_Agreement__c();
        
        //Test Client, Contract, Project, Protocol, and Protocol Country and agreement records
        testClient.Name = 'Client1';                
        testClient.Client_Unique_Key__c = 'Client001';
        insert testClient;
        
        testContract.Name = 'Contract1';
        testContract.Contract_Unique_Key__c = 'Contract001';
        testContract.Client_ID__c = testClient.Id;
        insert testContract;
        
        testProject.Name = 'Project1';
        testProject.Project_Unique_Key__c = 'Project001';
        //testProject.Contract_ID__c = testContract.Id;
        insert testProject;
        
        testProtocol.Name = 'Protocol1';
        testProtocol.Protocal_Unique_Key__c = 'Protocol001';
        testProtocol.Project_ID__c = testProject.Id;
        insert testProtocol;
        
        testcountry.Name = 'ALBANIA';
        insert testcountry;
        
        testProtocolCountry.Name = 'Netherlands';
        testProtocolCountry.Client_Protocol__c = testProtocol.id;
        testProtocolCountry.Region__c = testcountry.id;
        insert testProtocolCountry;
        
        testSite.Project_Protocol__c = testProtocol.Id;
        testSite.Site_ID__c = '0123456789';
        testSite.Name = 'tstSite01';
        insert testSite;
                
        newAgreement_Site.RecordTypeId = RT_Agreement.Id;
        newAgreement_Site.Name = 'Agreement ' + Math.random();
        newAgreement_Site.PRA_Status__c = 'At Site Review';        
        newAgreement_Site.Site__c = testSite.Id;
        newAgreement_Site.First_Draft_Sent_Date__c = Date.newInstance(1980, 01, 01);
        newAgreement_Site.Planned_Final_Exec_Date__c = Date.newInstance(2080, 12, 31);
        
        newAgreement_Protocol.RecordTypeId = RT_Template.Id;
        newAgreement_Protocol.Name = 'Template ' + Math.random();
        newAgreement_Protocol.PRA_Status__c = 'Final Budget Received';        
        newAgreement_Protocol.Protocol__c = testProtocol.Id;
        newAgreement_Protocol.First_Draft_Sent_Date__c = Date.newInstance(1980, 01, 01);
        newAgreement_Protocol.Planned_Final_Exec_Date__c = Date.newInstance(2080, 12, 31);
        
        newAgreement_Protocol_Country.RecordTypeId = RT_Template.Id;
        newAgreement_Protocol_Country.Name = 'Template ' + Math.random();
        newAgreement_Protocol_Country.PRA_Status__c = 'Final Budget Received';        
        newAgreement_Protocol_Country.Protocol_Country__c = testProtocolCountry.Id;
        newAgreement_Protocol_Country.First_Draft_Sent_Date__c = Date.newInstance(1980, 01, 01);
        newAgreement_Protocol_Country.Planned_Final_Exec_Date__c = Date.newInstance(2080, 12, 31);
        
        test.startTest();
        insert newAgreement_Site;
        insert newAgreement_Protocol;
        insert newAgreement_Protocol_Country;
        
        System.assertEquals(3, Limits.getFutureCalls());
        WFM_Site_Detail__c testSiteForNewAgreement = [SELECT Id, Name, Total_Number_of_Agreements__c, Fully_Executed_Agreements__c, First_Draft_Sent_Date__c, Planned_Final_Execution_Date__c FROM WFM_Site_Detail__c WHERE Name = 'tstSite01'];        System.assertEquals(1, testSiteForNewAgreement.Total_Number_of_Agreements__c);
        System.assertEquals(0, testSiteForNewAgreement.Fully_Executed_Agreements__c);
        System.assertEquals(Date.newInstance(1980, 01, 01), testSiteForNewAgreement.First_Draft_Sent_Date__c);
        System.assertEquals(Date.newInstance(2080, 12, 31), testSiteForNewAgreement.Planned_Final_Execution_Date__c);
        
        /*
        integer iStatusHistories  = 0;
        iStatusHistories = [SELECT count() FROM Agreement_Status_History__c WHERE Agreement__c =: newAgreement.Id];        
                
        newAgreement.PRA_Status__c = 'At Translator';
        update newAgreement;
        
        iStatusHistories = [SELECT count() FROM Agreement_Status_History__c WHERE Agreement__c =: newAgreement.Id];        
                 
        newAgreement.PRA_Status__c = 'Partially Executed';
        update newAgreement;
        
        iStatusHistories = [SELECT count() FROM Agreement_Status_History__c WHERE Agreement__c =: newAgreement.Id];        
        */
              
        test.stopTest();
    }
    
    static testMethod void testDeleteAgreement() {
        WFM_Client__c testClient = new WFM_Client__c();
        WFM_Contract__c testContract = new WFM_Contract__c();       
        WFM_Project__c testProject = new WFM_Project__c();
        WFM_Protocol__c testProtocol = new WFM_Protocol__c();
        WFM_Site_Detail__c testSite = new WFM_Site_Detail__c();
        Protocol_Country__c testProtocolCountry = new Protocol_Country__c();
        Country__c testcountry = new Country__c();
        Apttus__APTS_Agreement__c newAgreement = new Apttus__APTS_Agreement__c();
        Apttus__APTS_Agreement__c newAgreement_Protocol = new Apttus__APTS_Agreement__c();
        Apttus__APTS_Agreement__c newAgreement_Protocol_Country = new Apttus__APTS_Agreement__c();
           
           //Test Client, Contract, Project, Protocol, and Protocol Country and agreement records 
        testClient.Name = 'Client1';                
        testClient.Client_Unique_Key__c = 'Client001';
        insert testClient;
        
        testContract.Name = 'Contract1a1';
        testContract.Contract_Unique_Key__c = 'Contract00121';
        testContract.Client_ID__c = testClient.Id;
        insert testContract;
        
        testProject.Name = 'Project1';
        testProject.Project_Unique_Key__c = 'Project001';
        //testProject.Contract_ID__c = testContract.Id;
        insert testProject;
        
        testProtocol.Name = 'Protocol1';
        testProtocol.Protocal_Unique_Key__c = 'Protocol001';
        testProtocol.Project_ID__c = testProject.Id;
        insert testProtocol;
        
        testcountry.Name = 'ALBANIA';
        insert testcountry;
        
        testProtocolCountry.Name = 'Netherlands';
        testProtocolCountry.Client_Protocol__c = testProtocol.id;
        testProtocolCountry.Region__c = testcountry.id;
        insert testProtocolCountry;
        
        testSite.Project_Protocol__c = testProtocol.Id;
        testSite.Site_ID__c = '0123456789';
        testSite.Name = 'tstSite01';
        insert testSite;
                
        newAgreement.RecordTypeId = RT_Agreement.Id;
        newAgreement.Name = 'Agreement ' + Math.random();
        newAgreement.PRA_Status__c = 'At Site Review';
        newAgreement.Site__c = testSite.Id;
        newAgreement.First_Draft_Sent_Date__c = Date.newInstance(2014, 01, 01);
        newAgreement.Planned_Final_Exec_Date__c = Date.newInstance(2014, 12, 31);       
        insert newAgreement;
        
        newAgreement_Protocol.RecordTypeId = RT_Template.Id;
        newAgreement_Protocol.Name = 'Template ' + Math.random();
        newAgreement_Protocol.PRA_Status__c = 'Final Budget Received';        
        newAgreement_Protocol.Protocol__c = testProtocol.Id;
        newAgreement_Protocol.First_Draft_Sent_Date__c = Date.newInstance(1980, 01, 01);
        newAgreement_Protocol.Planned_Final_Exec_Date__c = Date.newInstance(2080, 12, 31);
        insert newAgreement_Protocol;
        
        newAgreement_Protocol_Country.RecordTypeId = RT_Template.Id;
        newAgreement_Protocol_Country.Name = 'Template ' + Math.random();
        newAgreement_Protocol_Country.PRA_Status__c = 'Final Budget Received';        
        newAgreement_Protocol_Country.Protocol_Country__c = testProtocolCountry.Id;
        newAgreement_Protocol_Country.First_Draft_Sent_Date__c = Date.newInstance(1980, 01, 01);
        newAgreement_Protocol_Country.Planned_Final_Exec_Date__c = Date.newInstance(2080, 12, 31);
        insert newAgreement_Protocol_Country;
        
        test.startTest();
        delete newAgreement; 
        delete newAgreement_Protocol;
        delete newAgreement_Protocol_Country;      
        test.stopTest();        
    }    
}