public with sharing class CV_PreviewHelpRecordCtrl {
    
    public Help_Record__c record { get; private set; }
    
    public CV_PreviewHelpRecordCtrl(ApexPages.StandardController stdController){
        try{
            List<Help_Record__c> foundRecords = [Select Rich_Html_Content__c From Help_Record__c Where Id = : stdController.getRecord().Id];
            if ( foundRecords.isEmpty() ){
                throw new CV_Utils.HelpSideBarException( 'Help Record not found by id: ' + stdController.getRecord().Id );
            }
            record = foundRecords[0];
        }catch(Exception e){
            ApexPages.addMessages(e);
            record = new Help_Record__c();
        }
    }
    
}