@isTest
private class BDT_FinancialDocPaymentContrTest {

    static testMethod void myUnitTest() {
        List<Study__c> StudiesList = new List<Study__c>();
        // Create test data
		BDT_CreateTestData.createall();
		Client_Project__c project = [select id from Client_Project__c limit 1];
		BDT_Utils.setPreference('SelectedProject', String.valueOf(project.id));
		String StudyIDs = '';
		for (Study__c st : [select id from Study__c where Project__c = :project.id]) {
			StudyIDs = StudyIDs + ':' + st.id;
			StudiesList.add(st);
		}
    	BDT_Utils.setPreference('SelectedStudies', StudyIDs.removeStart(':'));
    	// Create the new financial document, with the predefined attributes values   	
		String tmpString;
		FinancialDocument__c financialDocument 	  = new FinancialDocument__c();
		financialDocument.DocumentStatus__c    	  = 'In Progress';
		financialDocument.DocumentType__c      	  = 'Proposal';
		financialDocument.Client_Project__c    	  = project.id;
		financialDocument.TotalValue__c		   	  = '-';
		financialDocument.ApprovalStatus__c    	  = 'In Progress';
		tmpString = StudyIds.substringAfter(':'); // to skip the first ':'
		financialDocument.listOfStudyIds__c	   	  = tmpString.substringBefore(':');
		financialDocument.PaymentTermsComplete__c = true;
		insert financialDocument;
		// Create a payment term
		FinancialDocumentPaymentTerm__c  tmpPayment = new FinancialDocumentPaymentTerm__c();
		tmpPayment.FinancialDocument__c =  financialDocument.id;
		tmpPayment.Description__c = 'My second payment term';
		tmpPayment.StudyIdPercentageJSON__c =  '[{ "studyId":"' + StudiesList[0].id + '" , "percentage":"100" }]';
		insert tmpPayment;
		// Create the controller
		BDT_FinancialDocPaymentController p = new BDT_FinancialDocPaymentController();
		PageReference pageRef = New PageReference(System.Page.BDT_FinancialDocPayment.getUrl());
		pageref.getParameters().put('financialDocumentId' , financialDocument.id);
		pageref.getParameters().put('deletePayTermId' , '1');
		Test.setCurrentPage(pageRef); 
		p = new BDT_FinancialDocPaymentController();
		p.addRow();
		p.linkedPaymentIndex = '0';
		p.linkPaymentToMileStone();
		p.columnPaymentTerm = '0';
		p.calculateColumnSums();
		p.delRowPaymentTerm = '1';
		p.DeletePaymentTerm();
		p.save();
		p.commentSave();
		p.commentValue = 'Test comment';
		p.commentSave();
		p.closeComment();
		p.closePage();
    }
}