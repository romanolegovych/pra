/**
 * @description	Implements the test for the NCM_API_DataTypes class
 * @author		Dimitrios Sgourdos
 * @version		Created: 14-Sep-2015, Edited: 09-Oct-2015
 */
@isTest
private class NCM_API_DataTypesTest {
	
	/**
	 * @description	Test the constructors of the NotificationRegistrationWrapper.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015
	 */
	static testMethod void NotificationRegistrationWrapperConstructorsTest() {
		String errorMessage = 'Error in creating a NotificationRegistrationWrapper instance';
		
		// Check the standard constructor
		NCM_API_DataTypes.NotificationRegistrationWrapper result = new NCM_API_DataTypes.NotificationRegistrationWrapper();
		
		system.assertEquals(NULL, result.recordId, errorMessage);
		system.assertEquals(NULL, result.notificationCatalogId, errorMessage);
		system.assertEquals(NULL, result.userId, errorMessage);
		system.assertEquals(NULL, result.reminderPeriod, errorMessage);
		system.assertEquals(NULL, result.maxNumberReminders, errorMessage);
		system.assertEquals(NULL, result.relatedToId, errorMessage);
		system.assertEquals(NULL, result.active, errorMessage);
		system.assertEquals(NULL, result.notifyByEmail, errorMessage);
		system.assertEquals(NULL, result.notifyBySMS, errorMessage);
		system.assertEquals(NULL, result.notifyByChatter, errorMessage);
		system.assertEquals(NULL, result.parentTopicData, errorMessage);
		
		// Check the constructor with the NotificationRegistration__c and the Boolean
		NotificationCatalog__c notCat = new NotificationCatalog__c(	NotificationName__c = 'Test Notification',
																	ImplementationClassName__c = 'TestClassName');
		insert notCat;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		NotificationRegistration__c registration = new NotificationRegistration__c(
																			NotificationCatalog__c	= notCat.Id,
																			UserId__c				= testUser.Id,
																			ReminderPeriod__c		= 2,
																			MaxNumberReminders__c	= 3,
																			RelatedToId__c			= acc.Id,
																			Active__c				= true,
																			NotifyByEmail__c		= true,
																			NotifyBySMS__c			= false,
																			NotifyByChatter__c		= true
																		);
		insert registration;
		
		registration = [SELECT	Id,
								NotificationCatalog__c,
								UserId__c,
								ReminderPeriod__c,
								MaxNumberReminders__c,
								RelatedToId__c,
								Active__c,
								NotifyByEmail__c,
								NotifyBySMS__c,
								NotifyByChatter__c,
								NotificationCatalog__r.NotificationName__c,
								NotificationCatalog__r.NotificationDescription__c,
								NotificationCatalog__r.Category__c,
								NotificationCatalog__r.Type__c,
								NotificationCatalog__r.Comments__c,
								NotificationCatalog__r.DefaultReminderPeriod__c,
								NotificationCatalog__r.Active__c,
								NotificationCatalog__r.Priority__c,
								NotificationCatalog__r.AcknowledgementRequired__c,
								NotificationCatalog__r.ChatterEnabled__c,
								NotificationCatalog__r.SMSallowed__c,
								NotificationCatalog__r.TriggeredByBatchJob__c,
								NotificationCatalog__r.ImplementationClassName__c
						FROM	NotificationRegistration__c
						LIMIT	1];
		
		result = new NCM_API_DataTypes.NotificationRegistrationWrapper(registration, true);
		
		system.assertEquals(registration.Id, result.recordId, errorMessage);
		system.assertEquals(notCat.Id, result.notificationCatalogId, errorMessage);
		system.assertEquals(testUser.Id, result.userId, errorMessage);
		system.assertEquals(2, result.reminderPeriod, errorMessage);
		system.assertEquals(3, result.maxNumberReminders, errorMessage);
		system.assertEquals(acc.Id, result.relatedToId, errorMessage);
		system.assertEquals(true, result.active, errorMessage);
		system.assertEquals(true, result.notifyByEmail, errorMessage);
		system.assertEquals(false, result.notifyBySMS, errorMessage);
		system.assertEquals(true, result.notifyByChatter, errorMessage);
		system.assertEquals('Test Notification', result.parentTopicData.notificationName, errorMessage);
		system.assertEquals('TestClassName', result.parentTopicData.implementationClassName, errorMessage);
	}
	
	/**
	 * @description	Test the constructors of the NotificationCatalogWrapper.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015, Edited: 08-Oct-2015
	 */
	static testMethod void NotificationCatalogWrapperConstructorsTest() {
		// Create data
		NotificationCatalog__c notCat = new NotificationCatalog__c(
																NotificationName__c = 'Test Notification',
																NotificationDescription__c = 'Test Description',
																Category__c	= NCM_API_DataTypes.PROTOCOL_NOTIFICATION,
																Type__c = NCM_API_DataTypes.ALERT_TYPE,
																Comments__c = 'Test comments',
																DefaultReminderPeriod__c = 24,
																Active__c = true,
																Priority__c = NCM_API_DataTypes.NORMAL_PRIORITY,
																AcknowledgementRequired__c = true,
																ChatterEnabled__c = true,
																SMSallowed__c = true,
																TriggeredByBatchJob__c = false,
																ImplementationClassName__c = 'TestClassName');
		insert notCat;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		NotificationRegistration__c registration = new NotificationRegistration__c(
																			NotificationCatalog__c	= notCat.Id,
																			UserId__c				= testUser.Id,
																			ReminderPeriod__c		= 2,
																			MaxNumberReminders__c	= 3,
																			RelatedToId__c			= acc.Id,
																			Active__c				= true,
																			NotifyByEmail__c		= true,
																			NotifyBySMS__c			= false,
																			NotifyByChatter__c		= true
																		);
		insert registration;
		
		registration = [SELECT	Id,
								NotificationCatalog__c,
								UserId__c,
								ReminderPeriod__c,
								MaxNumberReminders__c,
								RelatedToId__c,
								Active__c,
								NotifyByEmail__c,
								NotifyBySMS__c,
								NotifyByChatter__c,
								NotificationCatalog__r.NotificationName__c,
								NotificationCatalog__r.NotificationDescription__c,
								NotificationCatalog__r.Category__c,
								NotificationCatalog__r.Type__c,
								NotificationCatalog__r.Comments__c,
								NotificationCatalog__r.DefaultReminderPeriod__c,
								NotificationCatalog__r.Active__c,
								NotificationCatalog__r.Priority__c,
								NotificationCatalog__r.AcknowledgementRequired__c,
								NotificationCatalog__r.ChatterEnabled__c,
								NotificationCatalog__r.SMSallowed__c,
								NotificationCatalog__r.TriggeredByBatchJob__c,
								NotificationCatalog__r.ImplementationClassName__c
						FROM	NotificationRegistration__c
						LIMIT	1];
		
		NotificationEvent__c notificationEvent = new NotificationEvent__c(	NotificationCatalog__c	= notCat.Id,
																			RelatedToId__c			= acc.Id
																		);
		insert notificationEvent;
		
		Notification__c notification = new Notification__c(	NotificationEvent__c = notificationEvent.Id,
															NotificationRegistration__c = registration.Id,
															Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
															Reminder__c = DateTime.now());
		insert notification;
		
		notification = [SELECT	Id,
								NotificationEvent__c,
								NotificationRegistration__c,
								Status__c,
								Reminder__c,
								NotificationRegistration__r.RelatedToId__c,
								NotificationRegistration__r.NotificationCatalog__r.NotificationName__c,
								NotificationRegistration__r.NotificationCatalog__r.NotificationDescription__c,
								NotificationRegistration__r.NotificationCatalog__r.Category__c,
								NotificationRegistration__r.NotificationCatalog__r.Type__c,
								NotificationRegistration__r.NotificationCatalog__r.Comments__c,
								NotificationRegistration__r.NotificationCatalog__r.DefaultReminderPeriod__c,
								NotificationRegistration__r.NotificationCatalog__r.Active__c,
								NotificationRegistration__r.NotificationCatalog__r.Priority__c,
								NotificationRegistration__r.NotificationCatalog__r.AcknowledgementRequired__c,
								NotificationRegistration__r.NotificationCatalog__r.ChatterEnabled__c,
								NotificationRegistration__r.NotificationCatalog__r.SMSallowed__c,
								NotificationRegistration__r.NotificationCatalog__r.TriggeredByBatchJob__c,
								NotificationRegistration__r.NotificationCatalog__r.ImplementationClassName__c
						FROM	Notification__c
						LIMIT	1];
		
		String errorMessage = 'Error in creating a NotificationCatalogWrapper instance';
		
		// Check the standard constructor
		NCM_API_DataTypes.NotificationCatalogWrapper result = new NCM_API_DataTypes.NotificationCatalogWrapper();
		
		system.assertEquals(NULL, result.recordId, errorMessage);
		system.assertEquals(NULL, result.notificationName, errorMessage);
		system.assertEquals(NULL, result.notificationDescription, errorMessage);
		system.assertEquals(NULL, result.notificationCategory, errorMessage);
		system.assertEquals(NULL, result.notificationType, errorMessage);
		system.assertEquals(NULL, result.comments, errorMessage);
		system.assertEquals(NULL, result.defaultReminderPeriod, errorMessage);
		system.assertEquals(NULL, result.active, errorMessage);
		system.assertEquals(NULL, result.priority, errorMessage);
		system.assertEquals(NULL, result.acknowledgementRequired, errorMessage);
		system.assertEquals(NULL, result.chatterEnabled, errorMessage);
		system.assertEquals(NULL, result.SMSallowed, errorMessage);
		system.assertEquals(NULL, result.triggeredByBatchJob, errorMessage);
		system.assertEquals(NULL, result.implementationClassName, errorMessage);
		
		// Check the constructor with the NotificationCatalog__c
		result = new NCM_API_DataTypes.NotificationCatalogWrapper(notCat);
		
		system.assertEquals(notCat.Id, result.recordId, errorMessage);
		system.assertEquals('Test Notification', result.notificationName, errorMessage);
		system.assertEquals('Test Description', result.notificationDescription, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PROTOCOL_NOTIFICATION, result.notificationCategory, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ALERT_TYPE, result.notificationType, errorMessage);
		system.assertEquals('Test comments', result.comments, errorMessage);
		system.assertEquals(24, result.defaultReminderPeriod, errorMessage);
		system.assertEquals(true, result.active, errorMessage);
		system.assertEquals(NCM_API_DataTypes.NORMAL_PRIORITY, result.priority, errorMessage);
		system.assertEquals(true, result.acknowledgementRequired, errorMessage);
		system.assertEquals(true, result.chatterEnabled, errorMessage);
		system.assertEquals(true, result.SMSallowed, errorMessage);
		system.assertEquals(false, result.triggeredByBatchJob, errorMessage);
		system.assertEquals('TestClassName', result.implementationClassName, errorMessage);
		
		// Check the constructor with the NotificationRegistration__c
		result = new NCM_API_DataTypes.NotificationCatalogWrapper(registration);
		
		system.assertEquals(notCat.Id, result.recordId, errorMessage);
		system.assertEquals('Test Notification', result.notificationName, errorMessage);
		system.assertEquals('Test Description', result.notificationDescription, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PROTOCOL_NOTIFICATION, result.notificationCategory, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ALERT_TYPE, result.notificationType, errorMessage);
		system.assertEquals('Test comments', result.comments, errorMessage);
		system.assertEquals(24, result.defaultReminderPeriod, errorMessage);
		system.assertEquals(true, result.active, errorMessage);
		system.assertEquals(NCM_API_DataTypes.NORMAL_PRIORITY, result.priority, errorMessage);
		system.assertEquals(true, result.acknowledgementRequired, errorMessage);
		system.assertEquals(true, result.chatterEnabled, errorMessage);
		system.assertEquals(true, result.SMSallowed, errorMessage);
		system.assertEquals(false, result.triggeredByBatchJob, errorMessage);
		system.assertEquals('TestClassName', result.implementationClassName, errorMessage);
		
		// Check the constructor with the Notification__c
		result = new NCM_API_DataTypes.NotificationCatalogWrapper(notification);
		
		system.assertEquals(notCat.Id, result.recordId, errorMessage);
		system.assertEquals('Test Notification', result.notificationName, errorMessage);
		system.assertEquals('Test Description', result.notificationDescription, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PROTOCOL_NOTIFICATION, result.notificationCategory, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ALERT_TYPE, result.notificationType, errorMessage);
		system.assertEquals('Test comments', result.comments, errorMessage);
		system.assertEquals(24, result.defaultReminderPeriod, errorMessage);
		system.assertEquals(true, result.active, errorMessage);
		system.assertEquals(NCM_API_DataTypes.NORMAL_PRIORITY, result.priority, errorMessage);
		system.assertEquals(true, result.acknowledgementRequired, errorMessage);
		system.assertEquals(true, result.chatterEnabled, errorMessage);
		system.assertEquals(true, result.SMSallowed, errorMessage);
		system.assertEquals(false, result.triggeredByBatchJob, errorMessage);
		system.assertEquals('TestClassName', result.implementationClassName, errorMessage);
	}
	
	
	/**
	 * @description	Test the constructors of the NotificationEventWrapper.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Oct-2015
	 */
	static testMethod void NotificationEventWrapperConstructorsTest() {
		// Create data
		NotificationCatalog__c notCat = new NotificationCatalog__c(
																NotificationName__c = 'Test Notification',
																Category__c	= NCM_API_DataTypes.PROTOCOL_NOTIFICATION,
																Type__c = NCM_API_DataTypes.ALERT_TYPE,
																ImplementationClassName__c = 'TestClassName');
		insert notCat;
		
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		NotificationEvent__c notificationEvent = new NotificationEvent__c(
																NotificationCatalog__c	= notCat.Id,
																RelatedToId__c			= acc.Id,
																Type__c					= NCM_API_DataTypes.ALERT_TYPE,
																EmailBody__c			= 'Test body',
																Subject__c				= 'Test subject'
															);
		insert notificationEvent;
		
		notificationEvent = [SELECT Id,
									NotificationCatalog__c,
									RelatedToId__c,
									EmailBody__c,
									Subject__c,
									Type__c,
									CreatedDate
							FROM	NotificationEvent__c
							LIMIT	1];
		
		Notification__c notification = new Notification__c(	NotificationEvent__c = notificationEvent.Id );
		insert notification;
		
		notification = [SELECT	NotificationEvent__c,
								NotificationEvent__r.NotificationCatalog__c,
								NotificationEvent__r.RelatedToId__c,
								NotificationEvent__r.EmailBody__c,
								NotificationEvent__r.Subject__c,
								NotificationEvent__r.Type__c,
								NotificationEvent__r.CreatedDate
						FROM	Notification__c
						LIMIT	1];
		
		String errorMessage = 'Error in creating a NotificationEventWrapper instance';
		
		// Check the standard constructor
		NCM_API_DataTypes.NotificationEventWrapper result = new NCM_API_DataTypes.NotificationEventWrapper();
		
		system.assertEquals(NULL, result.recordId, errorMessage);
		system.assertEquals(NULL, result.notificationCatalogId, errorMessage);
		system.assertEquals(NULL, result.relatedToId, errorMessage);
		system.assertEquals(NULL, result.notificationType, errorMessage);
		system.assertEquals(NULL, result.notificationSubject, errorMessage);
		system.assertEquals(NULL, result.emailBody, errorMessage);
		system.assertEquals(NULL, result.recordCreatedDate, errorMessage);
		
		// Check the constructor with the NotificationCatalog__c
		result = new NCM_API_DataTypes.NotificationEventWrapper(notificationEvent);
		
		system.assertEquals(notificationEvent.Id, result.recordId, errorMessage);
		system.assertEquals(notCat.Id, result.notificationCatalogId, errorMessage);
		system.assertEquals(acc.Id, result.relatedToId, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ALERT_TYPE, result.notificationType, errorMessage);
		system.assertEquals('Test subject', result.notificationSubject, errorMessage);
		system.assertEquals('Test body', result.emailBody, errorMessage);
		system.assertEquals(notificationEvent.CreatedDate, result.recordCreatedDate, errorMessage);
		
		// Check the constructor with the NotificationRegistration__c
		result = new NCM_API_DataTypes.NotificationEventWrapper(notification);
		
		system.assertEquals(notificationEvent.Id, result.recordId, errorMessage);
		system.assertEquals(notCat.Id, result.notificationCatalogId, errorMessage);
		system.assertEquals(acc.Id, result.relatedToId, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ALERT_TYPE, result.notificationType, errorMessage);
		system.assertEquals('Test subject', result.notificationSubject, errorMessage);
		system.assertEquals('Test body', result.emailBody, errorMessage);
		system.assertEquals(notificationEvent.CreatedDate, result.recordCreatedDate, errorMessage);
	}
	
	
	/**
	 * @description	Test the constructors of the NotificationWrapper.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015, Edited: 09-Oct-2015
	 */
	static testMethod void NotificationWrapperConstructorsTest() {
		String errorMessage = 'Error in creating a NotificationWrapper instance';
		
		// Check the standard constructor
		NCM_API_DataTypes.NotificationWrapper result = new NCM_API_DataTypes.NotificationWrapper();
		
		system.assertEquals(NULL, result.recordId, errorMessage);
		system.assertEquals(NULL, result.notificationEvent, errorMessage);
		system.assertEquals(NULL, result.notificationRegistration, errorMessage);
		system.assertEquals(NULL, result.status, errorMessage);
		system.assertEquals(NULL, result.reminder, errorMessage);
		system.assertEquals(NULL, result.relatedToId, errorMessage);
		system.assertEquals(NULL, result.parentTopicData, errorMessage);
		
		// Check the constructor with the Notification__c and the Boolean
		NotificationCatalog__c notCat = new NotificationCatalog__c(	NotificationName__c = 'Test Notification',
																	ImplementationClassName__c = 'TestClassName');
		insert notCat;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		NotificationRegistration__c registration = new NotificationRegistration__c(
																			NotificationCatalog__c	= notCat.Id,
																			UserId__c				= testUser.Id,
																			ReminderPeriod__c		= 2,
																			MaxNumberReminders__c	= 3,
																			RelatedToId__c			= acc.Id,
																			Active__c				= true,
																			NotifyByEmail__c		= true,
																			NotifyBySMS__c			= false,
																			NotifyByChatter__c		= true
																		);
		insert registration;
		
		NotificationEvent__c notificationEvent = new NotificationEvent__c(
																NotificationCatalog__c	= notCat.Id,
																RelatedToId__c			= acc.Id,
																Type__c					= NCM_API_DataTypes.ALERT_TYPE,
																EmailBody__c			= 'Test body',
																Subject__c				= 'Test subject'
															);
		insert notificationEvent;
		
		Notification__c notification = new Notification__c(	NotificationEvent__c = notificationEvent.Id,
															NotificationRegistration__c = registration.Id,
															Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
															Reminder__c = DateTime.now());
		insert notification;
		
		notification = [SELECT	Id,
								NotificationEvent__c,
								NotificationRegistration__c,
								Status__c,
								Reminder__c,
								CreatedDate,
								NotificationRegistration__r.RelatedToId__c,
								NotificationRegistration__r.NotificationCatalog__r.NotificationName__c,
								NotificationRegistration__r.NotificationCatalog__r.NotificationDescription__c,
								NotificationRegistration__r.NotificationCatalog__r.Category__c,
								NotificationRegistration__r.NotificationCatalog__r.Type__c,
								NotificationRegistration__r.NotificationCatalog__r.Comments__c,
								NotificationRegistration__r.NotificationCatalog__r.DefaultReminderPeriod__c,
								NotificationRegistration__r.NotificationCatalog__r.Active__c,
								NotificationRegistration__r.NotificationCatalog__r.Priority__c,
								NotificationRegistration__r.NotificationCatalog__r.AcknowledgementRequired__c,
								NotificationRegistration__r.NotificationCatalog__r.ChatterEnabled__c,
								NotificationRegistration__r.NotificationCatalog__r.SMSallowed__c,
								NotificationRegistration__r.NotificationCatalog__r.TriggeredByBatchJob__c,
								NotificationRegistration__r.NotificationCatalog__r.ImplementationClassName__c,
								NotificationEvent__r.NotificationCatalog__c,
								NotificationEvent__r.relatedToId__c,
								NotificationEvent__r.Type__c,
								NotificationEvent__r.Subject__c,
								NotificationEvent__r.EmailBody__c,
								NotificationEvent__r.CreatedDate
						FROM	Notification__c
						LIMIT	1];
		
		result = new NCM_API_DataTypes.NotificationWrapper(notification, true, true);
		
		system.assertEquals(notification.Id, result.recordId, errorMessage);
		system.assertEquals(notificationEvent.Id, result.notificationEvent, errorMessage);
		system.assertEquals(registration.Id, result.notificationRegistration, errorMessage);
		system.assertNotEquals(NULL, result.reminder, errorMessage);
		system.assertNotEquals(NULL, result.recordCreatedDate, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, result.status, errorMessage);
		
		system.assertEquals('Test Notification', result.parentTopicData.notificationName, errorMessage);
		system.assertEquals('TestClassName', result.parentTopicData.implementationClassName, errorMessage);
		
		system.assertEquals(notificationEvent.Id, result.parentEventData.recordId, errorMessage);
		system.assertEquals(notcat.Id, result.parentEventData.notificationCatalogId, errorMessage);
		system.assertEquals(acc.Id, result.parentEventData.relatedToId, errorMessage);
		system.assertEquals(NCM_API_DataTypes.ALERT_TYPE, result.parentEventData.notificationType, errorMessage);
		system.assertEquals('Test subject', result.parentEventData.notificationSubject, errorMessage);
		system.assertEquals('Test body', result.parentEventData.emailBody, errorMessage);
		system.assertEquals(notification.NotificationEvent__r.CreatedDate,
							result.parentEventData.recordCreatedDate,
							errorMessage);
	}
}