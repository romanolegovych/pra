@isTest
private class BDT_NewEditStudyTest {
	
	public static Client_Project__c firstProject {get;set;}
	public static List<Study__c> stydiesList {get;set;}
	public static List<Business_Unit__c> myBUList  {get;set;}
	public static List<Site__c> mySiteList  {get;set;}
	
	static void init(){
		firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;
		
		myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert myBUList;
		
		mySiteList = BDT_TestDataUtils.buildSite(myBUList);
		insert mySiteList;
		
		stydiesList = BDT_TestDataUtils.buildStudies(firstProject);
		stydiesList[0].Business_Unit__c = myBUList[0].Id; 
		stydiesList[1].Business_Unit__c = myBUList[1].Id; 
		stydiesList[2].Business_Unit__c = myBUList[2].Id;
		stydiesList[3].Business_Unit__c = myBUList[3].Id; 
		stydiesList[4].Business_Unit__c = myBUList[4].Id;
		insert stydiesList;
		
		BDT_Utils.setPreference('SelectedProject', firstProject.id);
		BDT_Utils.setPreference('SelectedStudies',stydiesList[0].id);
		
	}
	
	static testMethod void createnewStudy() { 
		init();
				
		BDT_NewEditStudy newEditStudy = new BDT_NewEditStudy();
		newEditStudy.study.Protocol_Title__c = 'Lorem ipsilum Lorem ipsilum Lorem ipsilum Lorem ipsilum Lorem ipsilum Lorem ipsilum Lorem ipsilum';
		
		System.assertNotEquals(null, newEditStudy.save());
		System.assertEquals(6, [select count() from Study__c where Project__c = :firstProject.id]);		
	}
	
	static testMethod void cancelNewStudy() { 
		init();
				
		BDT_NewEditStudy newEditStudy = new BDT_NewEditStudy();
		newEditStudy.study.Protocol_Title__c = 'Lorem ipsilum Lorem ipsilum Lorem ipsilum Lorem ipsilum Lorem ipsilum Lorem ipsilum Lorem ipsilum';
		newEditStudy.cancel();
		System.assertEquals(5, [select count() from Study__c where Project__c = :firstProject.id]);		
	}
	
	static testMethod void editStudy() { 
		init();
		System.currentPageReference().getParameters().put('studyId', stydiesList[0].id);
		
		BDT_NewEditStudy newEditStudy = new BDT_NewEditStudy();
		newEditStudy.study.Protocol_Title__c = 'Lorem ipsilum';
		newEditStudy.save();
		System.assertEquals('Lorem ipsilum', [select Protocol_Title__c from Study__c where id = :stydiesList[0].id].Protocol_Title__c);		
	}
	
	static testMethod void softdeleteStudy() { 
		init();
		
		System.currentPageReference().getParameters().put('studyId', stydiesList[0].id);
		
		BDT_NewEditStudy newEditStudy = new BDT_NewEditStudy();		
		newEditStudy.deleteSoft();
		System.assertEquals(true, [select BDTDeleted__c from Study__c where id = :stydiesList[0].id].BDTDeleted__c);		
	}
	
}