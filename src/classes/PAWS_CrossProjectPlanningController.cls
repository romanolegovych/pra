global with sharing virtual class PAWS_CrossProjectPlanningController 
{
    public PAWS_CrossProjectPlanningController(){}
    
    /****************************** PROPERTIES SECTION *********************************/

    public DateTime ReinitFlag  
    {
        get
        {
            if(ReinitFlag == null) ReinitFlag = DateTime.now();
            return ReinitFlag;
        }
        set;
    }

    public ApexPages.StandardSetController RowsSetController 
    {
        get 
        {
            if(RowsSetController == null) 
            {
                RowsSetController = new ApexPages.StandardSetController(Database.getQueryLocator(buildQuery() + ' limit 2000'));
                RowsSetController.setPageSize(25);
            }
            return RowsSetController;
        }
        set;
    }
    
    public List<Row> Rows
    {
        get
        {
            if(RowsSetController == null) return null;
            return buildRows(RowsSetController.getRecords());
        }
    }

    public ID SeletedObjectId {get;set;}
    public ID SeletedFlowInstanceId {get;set;}
    public Boolean ObjectSpecificProgressMode {get;set;}
    
    public sObject SeletedObject 
    {
        get
        {
            if(SeletedObject == null && !String.isEmpty(SeletedObjectId))
            {
                List<String> fields = new List<String>();
                Schema.DescribeSObjectResult describe = ecrf__c.getSObjectType().getDescribe();
                fields.addAll(describe.fields.getMap().keySet());

                SeletedObject = Database.query('select ' + String.join(fields, ',') + ' from ecrf__c where Id = :SeletedObjectId');
            }
            return SeletedObject;
        }
        set;
    }
    
    public String DefaultDateFormat
    {
    	get
    	{
    		return PAWS_Utilities.getDefaultDateFormat();
    	}
    }
    
    /******************** ORDER BY SECTION ********************/
    
    public String OrderBy 
	{
		get
		{
			if (OrderBy == null)
			{
				//By default order by Start Date
				OrderBy = 'Start_Date__c';//OrderBy = 'Project__r.Name';
			}
			
			return OrderBy;
		}
		set;
	}
    
    public String OrderType
    {
    	get
    	{
    		if(OrderType == null) OrderType = 'asc';
    		return OrderType;
    	}
    	set;
    }

    /******************** FILTERS SECTION ********************/
    
    public void applyFilters()
    {
        refresh();
    }
    
    public void clearFilters()
    {
        SponsorFilter = null;
        ProjectFilter = null;
        FlowFilter = null;
        StepFilter = null;
        RoleFilter = null;
        AssigneeFilter = null;
        StartDateFilterForStartDate = null;
        EndDateFilterForStartDate = null;
        StartDateFilterForEndDate = null;
        EndDateFilterForEndDate = null;
        ExcludeCompletedStepsFilter = true;
        ExcludeParentStepsFilter = true;
        SelectedTags = null;
        SelectedProtocols = null;
        SelectedCountries = null;
        refresh();
    }
    
    //******************** SPONSORS FILTER SECTION ********************

	public String SponsorFilter {get;set;}
	
    @RemoteAction
    public @ReadOnly static List<String> loadAvailableSponsors(String filter)
    {
    	PAWS_CrossProjectPlanningController controller = new PAWS_CrossProjectPlanningController();
    	
    	String conditions = controller.buildFolderFilterByProject('Id');
		if(!String.isEmpty(filter)) conditions += ' and Sponsor__c like \'%' + String.escapeSingleQuotes(filter) + '%\'';
				
		Set<String> names = new Set<String>();
		for (AggregateResult each : (List<AggregateResult>)Database.query('select count(id), Project_ID__r.WFM_Client__r.Name name from ecrf__c where Project_ID__r.WFM_Client__c != null ' + conditions + ' group by Project_ID__r.WFM_Client__r.Name limit 1000'))
		{
			names.add((String)each.get('name'));
		}

		for (AggregateResult each : (List<AggregateResult>)Database.query('select count(id), Project_ID__r.Contract_ID__r.Client_ID__r.Name name from ecrf__c where Project_ID__r.WFM_Client__c = null and Project_ID__r.Contract_ID__c != null and Project_ID__r.Contract_ID__r.Client_ID__c != null ' + conditions + ' group by Project_ID__r.Contract_ID__r.Client_ID__r.Name limit 1000'))
		{
			names.add((String)each.get('name'));
		}
				
		List<String> namesList = new List<String>(names);
		namesList.sort();
		
		List<String> result = new List<String>();
		for(String name : namesList)
			result.add(name);

        return result;
    }
    
    //******************** PROJECTS FILTER SECTION ********************
    
    public String ProjectFilter {get;set;}

    @RemoteAction
    public @ReadOnly static List<String> loadAvailableProjects(String filter)
    {
    	PAWS_CrossProjectPlanningController controller = new PAWS_CrossProjectPlanningController();
    	
    	String conditions = 'Type__c = \'Regular\'';
		conditions += controller.buildFolderFilterByProject('Id');
		if(!String.isEmpty(filter)) conditions += ' and ' + controller.buildLikeFilter2('Name', filter);
		
		System.debug('select Name name from ecrf__c where ' + conditions + ' group by Name order by Name limit 100');
		List<String> result = new List<String>();
        for(sObject record : Database.query('select Name name from ecrf__c where ' + conditions + ' group by Name order by Name limit 100'))
        {
            result.add((String)record.get('name'));
        }

        return result;
    }
    
    //******************** FLOWS FILTER SECTION ********************
    
    public String FlowFilter {get;set;}
    
    @RemoteAction
    public @ReadOnly static List<String> loadAvailableFlows(String filter)
    {
    	PAWS_CrossProjectPlanningController controller = new PAWS_CrossProjectPlanningController();
    	
    	String conditions = controller.buildInFilter('STSWR1__Object_Type__c', new List<String>(PAWS_APIHelper.PAWS_OBJECTS));
        conditions += controller.buildFolderFilter('Id');
        if(!String.isEmpty(filter)) conditions += ' and ' + controller.buildLikeFilter2('Name', filter);
           
        Set<String> names = new Set<String>();     
        for(STSWR1__Flow__c record : (List<STSWR1__Flow__c>)Database.query('select Name from STSWR1__Flow__c where ' + conditions + ' order by Name limit 10000'))
        {
            names.add(record.Name);
            if(names.size() >= 50) break;
        }
        
        List<String> result = new List<String>(names);
        result.sort();
        return result;
    }
    
    //******************** STEPS FILTER SECTION ********************
    
    public String StepFilter {get;set;}

    @RemoteAction
    public @ReadOnly static List<String> loadAvailableSteps(String filter)
    {
    	PAWS_CrossProjectPlanningController controller = new PAWS_CrossProjectPlanningController();
    	
    	String conditions = controller.buildInFilter('STSWR1__Flow__r.STSWR1__Object_Type__c', new List<String>(PAWS_APIHelper.PAWS_OBJECTS));
        conditions += controller.buildFolderFilter('STSWR1__Flow__c');
        if(!String.isEmpty(filter)) conditions += ' and ' + controller.buildLikeFilter2('Name', filter);

		Set<String> names = new Set<String>();     
		for(STSWR1__Flow_Step_Junction__c record : (List<STSWR1__Flow_Step_Junction__c>)Database.query('select Name from STSWR1__Flow_Step_Junction__c where ' + conditions + ' order by Name limit 10000'))
        {
            names.add(record.Name);
            if(names.size() >= 50) break;
        }

        List<String> result = new List<String>(names);
        result.sort();
        return result;
    }
    
    //******************** ROLES FILTER SECTION ********************
    
    public String RoleFilter {get;set;}
    
    @RemoteAction
    public @ReadOnly static List<String> loadAvailableRoles(String filter)
    {
    	PAWS_CrossProjectPlanningController controller = new PAWS_CrossProjectPlanningController();
    	
    	String conditions = controller.buildInFilter('STSWR1__Flow__r.STSWR1__Object_Type__c', new List<String>(PAWS_APIHelper.PAWS_OBJECTS));
	    conditions += controller.buildFolderFilter('STSWR1__Flow__c');
	    if(!String.isEmpty(filter)) conditions += ' and ' + controller.buildLikeFilter2('Name', filter);
	    
	    Set<String> names = new Set<String>();     
		for(STSWR1__Flow_Swimlane__c record : (List<STSWR1__Flow_Swimlane__c>)Database.query('select Name from STSWR1__Flow_Swimlane__c where ' + conditions + ' order by Name limit 10000'))
        {
            names.add(record.Name);
            if(names.size() >= 50) break;
        }

        List<String> result = new List<String>(names);
        result.sort();
        return result;
    }

    //******************** USERS FILTER SECTION ********************
    
    public String AssigneeFilter {get;set;}

    @RemoteAction
    public @ReadOnly static List<String> loadAvailableUsers(String filter)
    {
    	String conditions = 'IsActive = true';
        //if(!String.isEmpty(filter)) conditions += ' and (' + buildLikeFilter('FirstName', filter) + ' or ' + buildLikeFilter('FirstName', filter) + ')';
        if (!String.isEmpty(filter)) conditions += ' and (Name like \'%' + String.escapeSingleQuotes(filter) + '%\')';

		List<String> result = new List<String>();
        for(User record : (List<User>)Database.query('select Name from User where ' + conditions + ' order by Name limit 100'))
        {
        	result.add(record.Name);
        }

        return result;
    }
    
    //******************** COUNTRIES FILTER SECTION ********************
	
	public String SelectedCountries {get; set;}

	public String buildCountriesFilter()
	{
		return ' and (' + String.join(
			new List<String>{
				buildInFilter('STSWR1__Flow__r.PAWS_Project_Flow_Country__r.Name', SelectedCountries.split(';')),
				buildInFilter('STSWR1__Flow__r.PAWS_Project_Flow_Site__r.PAWS_Country__r.Name', SelectedCountries.split(';')),
				buildInFilter('STSWR1__Flow__r.PAWS_Project_Flow_Agreement__r.PAWS_Country__r.Name', SelectedCountries.split(';')),
				buildInFilter('STSWR1__Flow__r.PAWS_Project_Flow_Document__r.PAWS_Country__r.Name ', SelectedCountries.split(';')),
				buildInFilter('STSWR1__Flow__r.PAWS_Project_Flow_Submission__r.PAWS_Country__r.Name', SelectedCountries.split(';'))
			}, 
			' or ')
		 + ')';// + buildInFilter('Project__r.Protocol_ID__r.Name', selectedProtocols.split(';'));
	}
	
	@RemoteAction
    public @ReadOnly static List<String> loadAvailableCountries(String filter)
    {
    	PAWS_CrossProjectPlanningController controller = new PAWS_CrossProjectPlanningController();
    	
    	String conditions = 'Name != null';
		conditions += controller.buildFolderFilterByProject('PAWS_Project__c');
		if(!String.isEmpty(filter)) conditions += ' and Name like \'%' + String.escapeSingleQuotes(filter) + '%\'';
				
		List<String> result = new List<String>();
		for (AggregateResult each : (List<AggregateResult>)Database.query('select count(id), Name name from PAWS_Project_Flow_Country__c where ' + conditions + ' group by Name limit 1000'))
		{
			result.add((String) each.get('name'));
		}

        return result;
    }
	
	//******************** PROTOCOLS FILTER SECTION ********************
	
	public String SelectedProtocols {get; set;}

	public String buildProtocolsFilter()
	{
		return ' and ' + buildInFilter('Project__r.Protocol_ID__r.Name', SelectedProtocols.split(';'));
	}
	
	@RemoteAction
    public @ReadOnly static List<String> loadAvailableProtocols(String filter)
    {
    	PAWS_CrossProjectPlanningController controller = new PAWS_CrossProjectPlanningController();
    	
    	String conditions = 'Protocol_ID__r.Name != null';
		conditions += controller.buildFolderFilterByProject('Id');
		if(!String.isEmpty(filter)) conditions += ' and Protocol_ID__r.Name like \'%' + String.escapeSingleQuotes(filter) + '%\'';
				
		List<String> result = new List<String>();
		for (AggregateResult each : (List<AggregateResult>)Database.query('select count(id), Protocol_ID__r.Name name from ecrf__c where ' + conditions + ' group by Protocol_ID__r.Name limit 1000'))
		{
			result.add((String) each.get('name'));
		}

        return result;
    }

    //******************** TAGS FILTER SECTION ********************
    
    public String SelectedTags {get; set;}

	@RemoteAction
    public @ReadOnly static List<String> loadAvailableTags(String filter)
    {
    	PAWS_CrossProjectPlanningController controller = new PAWS_CrossProjectPlanningController();
    	
    	String conditions = 'STSWR1__Type__c = \'Tag\' and Name != null';
		conditions += ' and ' + controller.buildInFilter('STSWR1__Flow_Step__r.STSWR1__Flow__r.STSWR1__Object_Type__c', new List<String>(PAWS_APIHelper.PAWS_OBJECTS));
		//conditions += controller.buildFolderFilter('STSWR1__Flow_Step__r.STSWR1__Flow__c');
		if(filter != null) conditions += ' and Name like \'%' + String.escapeSingleQuotes(filter) + '%\'';
		
		List<STSWR1__Flow_Step_Property__c> properties = (List<STSWR1__Flow_Step_Property__c>)Database.query('select Name from STSWR1__Flow_Step_Property__c where ' + conditions + ' order by STSWR1__Flow_Step__r.STSWR1__Flow__c limit 5000');
		Set<String> names = new Set<String>();     
		for (STSWR1__Flow_Step_Property__c property : properties)
		{
			names.add(property.Name);
            if(names.size() >= 50) break;
		}

        List<String> result = new List<String>(names);
        result.sort();
        return result;
    }
	
	//******************** DATES FILTER SECTION ********************
	
	public Date StartDateFilterForStartDate {get;set;}
    public Date EndDateFilterForStartDate {get;set;}
    public Date StartDateFilterForEndDate {get;set;}
    public Date EndDateFilterForEndDate {get;set;}
    
    public String GetStartDateFilterForStartDateURLParam()
	{
		return GetDateToURlParam(StartDateFilterForStartDate);
	}
	
	public String GetEndDateFilterForStartDateURLParam()
	{
		return GetDateToURlParam(EndDateFilterForStartDate);
	}
	
	public String GetStartDateFilterForEndDateURLParam()
	{
		return GetDateToURlParam(StartDateFilterForEndDate);
	}
	
	public String GetEndDateFilterForEndDateURLParam()
	{
		return GetDateToURlParam(EndDateFilterForEndDate);
	}
	
	private String GetDateToURlParam(Date val)
	{
		String result;
		
		if (val != null)
		{
			result = String.valueOf(val.year()) + '-' + String.valueOf(val.month()) + '-' + String.valueOf(val.day());
		}
		
		return result;
	}
	
	private Date GetURLParamToDate(String val)
	{
		Date result;
		
		try
		{
			List<String> dateParts = val.split('-');
			result = Date.newInstance(Integer.valueOf(dateParts.get(0)), Integer.valueOf(dateParts.get(1)), Integer.valueOf(dateParts.get(2)));		
		}
		catch (Exception ex){/*do nothing*/}
		
		return result;
	}
    
    //******************** OTHER FILTERS SECTION ********************
    
    public Boolean ExcludeCompletedStepsFilter {get;set;}
    public Boolean ExcludeParentStepsFilter {get;set;}
    
	public String GetExcludeCompletedStepsFilterURLParam()
	{
		return (ExcludeCompletedStepsFilter == true ? '1' : '0');
	}
	
	public String GetExcludeParentStepsFilterURLParam()
	{
		return (ExcludeParentStepsFilter == true ? '1' : '0');
	}

    /****************************** METHODS SECTION *********************************/
    
    public void refresh()
    {
        RowsSetController = null;
        SeletedFlowInstanceId = null;
        SeletedObjectId = null;
        SeletedObject = null;
        ReinitFlag = null;
    }
    
    public void enableObjectSpecificProgressMode()
    {
        ObjectSpecificProgressMode = true;
        ReinitFlag = null;
    }
    
    public void cancelObjectSpecificProgressMode()
    {
        ObjectSpecificProgressMode = false;
        SeletedFlowInstanceId = null;
        SeletedObjectId = null;
        SeletedObject = null;
        ReinitFlag = null;
    }

    public String buildQuery()
    {
        List<String> fields = new List<String>();
        Schema.DescribeSObjectResult describe = STSWR1__Gantt_Step_Property__c.getSObjectType().getDescribe();
        fields.addAll(describe.fields.getMap().keySet());
        
        describe = STSWR1__Flow_Step_Junction__c.getSObjectType().getDescribe();
        for(String fieldName : describe.fields.getMap().keySet())
            fields.add('STSWR1__Step__r.' + fieldName);
            
        describe = STSWR1__Flow__c.getSObjectType().getDescribe();
        for(String fieldName : describe.fields.getMap().keySet())
            fields.add('STSWR1__Step__r.STSWR1__Flow__r.' + fieldName);
            
        describe = STSWR1__Flow_Swimlane__c.getSObjectType().getDescribe();
        for(String fieldName : describe.fields.getMap().keySet())
            fields.add('STSWR1__Step__r.STSWR1__Flow_Swimlane__r.' + fieldName);
            
        describe = ecrf__c.getSObjectType().getDescribe();
        for(String fieldName : describe.fields.getMap().keySet())
            fields.add('Project__r.' + fieldName);

        String finalQuery = 'select ' + String.join(fields, ',') + ' from STSWR1__Gantt_Step_Property__c where ' + buildFilters() + ' order by ' + OrderBy + ' ' + OrderType + ' NULLS LAST, CreatedDate';
        system.debug('----finalQuery = ' + finalQuery);
        return finalQuery;
    }
    
    public String buildFilters()
    {
    	return buildStaticFilters() + buildDynamicFilters();
    }
    
    private String buildStaticFilters()
    {
    	String filters = buildInFilter('STSWR1__Step__r.STSWR1__Flow__r.STSWR1__Object_Type__c', new List<String>(PAWS_APIHelper.PAWS_OBJECTS));
        filters += ' and ' + buildEqualsTextFilter('STSWR1__Step__r.STSWR1__Flow__r.STSWR1__Type__c', 'One Time');
        filters += ' and ' + buildEqualsTextFilter('STSWR1__Step__r.STSWR1__Flow__r.STSWR1__Status__c', 'Active');
        filters += ' and ' + buildEqualsTextFilter('STSWR1__Level__c', 'First');
        filters += ' and Project__c != null and STSWR1__Parent_Flow_Id__c != null';
        filters += ' and Project__r.Type__c = \'Regular\'';
        filters += ' and STSWR1__Is_Skipped__c = false';

        return filters;
    }
    
    /**
	* Chnaged visibility from "private" to "public" and added "virtual" keyword to be able to "play"
	* and override this method in extended class.
	*
	* @author	ilya.leshchuk
	* @date		09/26/14
	*/
	public virtual String buildDynamicFilters()
	{
		String filters = '';
		
		if(!String.isEmpty(ProjectFilter)) 
		{
			List<String> projects = ProjectFilter.split(';');
			if (projects.size() == 1)
			{
				filters += ' and ' + buildLikeFilter('Project__r.Name', projects[0]);
			}
			else if (projects.size() > 1)
			{
				filters += ' and ' + buildInFilter('Project__r.Name', projects);
			}
		}
		
		if(!String.isEmpty(SponsorFilter)) 
		{
			filters += buildMultiselectFilter('Project__r.Sponsor__c', SponsorFilter, 'and');
		}
		
		if (!String.isEmpty(FlowFilter))
		{
			filters += buildMultiselectFilter('STSWR1__Step__r.STSWR1__Flow__r.Name', FlowFilter, 'and');
		}
		
		if(!String.isEmpty(StepFilter)) 
		{
			filters += buildMultiselectFilter('STSWR1__Step__r.Name', StepFilter, 'and');
		}
		
		if(!String.isEmpty(RoleFilter)) 
		{
			filters += buildMultiselectFilter('STSWR1__Step__r.STSWR1__Flow_Swimlane__r.Name', RoleFilter, 'and');
		}
		
		if(!String.isEmpty(AssigneeFilter)) 
		{
			filters += buildAssigneeFilter();
		}
		
		if(StartDateFilterForStartDate != null || EndDateFilterForStartDate != null || StartDateFilterForEndDate != null || EndDateFilterForEndDate != null)
		{
			if(StartDateFilterForStartDate != null || EndDateFilterForStartDate != null)
			{
				filters += ' and ' + buildDateFilter('Start_Date__c', StartDateFilterForStartDate, EndDateFilterForStartDate);
			}
			
			if(StartDateFilterForEndDate != null || EndDateFilterForEndDate != null)
			{
				filters += ' and ' + buildDateFilter('End_Date__c', StartDateFilterForEndDate, EndDateFilterForEndDate);
			}
		}
		
		if (ExcludeCompletedStepsFilter == true)
		{
			filters += ' and STSWR1__Step__c not in (select STSWR1__Step__c from STSWR1__Flow_Instance_History__c where STSWR1__Status__c in (\'Completed\', \'Approved\', \'Rejected\')) ';
		}
		
		if (ExcludeParentStepsFilter == true)
		{
			filters += ' and STSWR1__Step__r.Number_of_Sub_Flow_Actions__c = 0';
			//filters += ' and STSWR1__Step__c not in (select STSWR1__Flow_Step__c from STSWR1__Flow_Step_Action__c where STSWR1__Type__c = \'Start Sub Flow\') ';
		}
		
		if (selectedTags != null && selectedTags != '')
		{
			filters += ' and STSWR1__Step__c in (select STSWR1__Flow_Step__c from STSWR1__Flow_Step_Property__c where ' + buildInFilter('Name', selectedTags.split(';')) + ')';
			//filters += buildTagsFilter();
		}
		
		if (!String.isEmpty(selectedProtocols))
		{
			filters += buildProtocolsFilter();
		}
		
		if (!String.isEmpty(selectedCountries))
		{
			filters += buildCountriesFilter();
		}
		
		return filters;
    }

    public List<Row> buildRows(List<sObject> records)
    {
        List<STSWR1__Gantt_Step_Property__c> properties = (List<STSWR1__Gantt_Step_Property__c>)records;
        
        STSWR1.API wrApi = new STSWR1.API();
        
        Map<ID, String> propertyAssignee = new Map<ID, String>();
        Map<String, Set<ID>> typeMap = new Map<String, Set<ID>>();
    	for(STSWR1__Gantt_Step_Property__c property : properties)
    	{
    		if(String.isEmpty(property.STSWR1__Step__r.STSWR1__Assign_To_Value__c)) continue;
    		
    		String assignTo = null;
    		try
    		{
	    		assignTo = (String)wrApi.call('WorkflowService', 'buildAssignTo', new Map<String, Object>
	    		{
	    			'assignType' => property.STSWR1__Step__r.STSWR1__Assign_Type_Value__c,
	    			'assignTo' => property.STSWR1__Step__r.STSWR1__Assign_To_Value__c,
	    			'record' => property.Project__r
	    		});
	    		
	    		if(String.isEmpty(assignTo)) continue;
    		}catch(Exception ex)
    		{
    			continue;
    		}

			{			
				if(!typeMap.containsKey(property.STSWR1__Step__r.STSWR1__Assign_Type_Value__c)) typeMap.put(property.STSWR1__Step__r.STSWR1__Assign_Type_Value__c, new Set<ID>());
    			for(String assigneeItem : assignTo.split(','))
    			{
    				if(assigneeItem instanceof ID) 
    				{
    					typeMap.get(property.STSWR1__Step__r.STSWR1__Assign_Type_Value__c).add((ID)assigneeItem);
    				}
    			}
			}
    		
    		propertyAssignee.put(property.Id, assignTo);
    	}


        Map<ID, String> assigners = (Map<ID, String>)wrApi.call('WorkflowService', 'loadAssigners', typeMap);
        Map<String, STSWR1__Flow_Instance_History__c> historyMap = loadHistory(properties);
        Map<ID, PAWS_Project_Flow_Junction__c> projectJunctions = loadProjectFlowJunctions(properties);
        
        List<Row> result = new List<Row>();
        for(STSWR1__Gantt_Step_Property__c property : properties)
        {
            Row rowItem = new Row(property, historyMap.get((String)property.STSWR1__Step__c), propertyAssignee.get(property.Id), assigners);
            rowItem.ProjectJunction = projectJunctions.get(property.Id);
            result.add(rowItem);
        }
        
        return result;
    }
    
    private Map<String, STSWR1__Flow_Instance_History__c> loadHistory(List<STSWR1__Gantt_Step_Property__c> properties)
    {
        Set<ID> ids = new Set<ID>();
        for(STSWR1__Gantt_Step_Property__c property : properties)
            ids.add(property.STSWR1__Step__c);
            
        List<String> fields = new List<String>();
    	Schema.DescribeSObjectResult describe = STSWR1__Flow_Instance_History__c.getSObjectType().getDescribe();
    	fields.addAll(describe.fields.getMap().keySet());
    	fields.add('STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Object_Id__c');
    	fields.add('STSWR1__Cursor__r.STSWR1__Flow_Branch__c');
    	fields.add('STSWR1__Step__r.STSWR1__Flow_Branch__c');
    	fields.add('STSWR1__Step__r.STSWR1__Is_System_Value__c');
        	
       	Map<String, STSWR1__Flow_Instance_History__c> result = new Map<String, STSWR1__Flow_Instance_History__c>();
        List<STSWR1__Flow_Instance_History__c> history = (List<STSWR1__Flow_Instance_History__c>)Database.query('select ' + String.join(fields, ',') + ' from STSWR1__Flow_Instance_History__c where STSWR1__Step__c in :ids and STSWR1__Obsolete__c != true order by STSWR1__Step__c, CreatedDate asc, Name');
		for(Integer i = 0; i < history.size(); i++)
		{
			if(!result.containsKey((String)history[i].STSWR1__Step__c) || history[i].STSWR1__Step__r.STSWR1__Flow_Branch__c == (ID)history[i].STSWR1__Cursor__r.STSWR1__Flow_Branch__c)
				result.put((String)history[i].STSWR1__Step__c, history[i]);
		}

        return result;
    }
    
    private Map<ID, PAWS_Project_Flow_Junction__c> loadProjectFlowJunctions(List<STSWR1__Gantt_Step_Property__c> properties)
    {
    	Set<ID> projectIds = new Set<ID>();
    	Set<ID> flowIds = new Set<ID>();
        for(STSWR1__Gantt_Step_Property__c property : properties)
        {
            projectIds.add(property.Project__c);
            flowIds.add(property.STSWR1__Parent_Flow_Id__c);
        }
        
        Map<String, PAWS_Project_Flow_Junction__c> projectFlowMap = new Map<String, PAWS_Project_Flow_Junction__c>();
        for(PAWS_Project_Flow_Junction__c projectFlow : [select Project__c, Flow__c, Flow_Instance__c from PAWS_Project_Flow_Junction__c where Project__c in :projectIds and Flow__c in :flowIds])
        {
        	projectFlowMap.put((String)projectFlow.Project__c + (String)projectFlow.Flow__c, projectFlow);
        }

    	Map<ID, PAWS_Project_Flow_Junction__c> result = new Map<ID, PAWS_Project_Flow_Junction__c>();
        for(STSWR1__Gantt_Step_Property__c property : properties)
        {
            result.put(property.Id, projectFlowMap.get((String)property.Project__c + (String)property.STSWR1__Parent_Flow_Id__c));
        }
        
        return result;
    }
    
	public void populateDashboardDefaults()
	{
		Map<String, String> urlParams = ApexPages.currentPage().getParameters();
		
		SponsorFilter = urlParams.get('sp');
		ProjectFilter = urlParams.get('pr');
		FlowFilter = urlParams.get('fl');
		StepFilter = urlParams.get('st');
		RoleFilter = urlParams.get('ro');
		AssigneeFilter = urlParams.get('as');
		StartDateFilterForStartDate = GetURLParamToDate(urlParams.get('sd1'));
		EndDateFilterForStartDate = GetURLParamToDate(urlParams.get('sd2'));
		StartDateFilterForEndDate = GetURLParamToDate(urlParams.get('ed1'));
		EndDateFilterForEndDate = GetURLParamToDate(urlParams.get('ed2'));
		ExcludeCompletedStepsFilter = (urlParams.get('ecs') == null ? true : (urlParams.get('ecs') == '1' ? true : false));
		ExcludeParentStepsFilter = (urlParams.get('eps') == null ? true : (urlParams.get('eps') == '1' ? true : false));
		
		OrderBy = urlParams.get('ob');
		OrderType = urlParams.get('ot');
		
		SelectedTags = urlParams.get('vws');
		SelectedProtocols = urlParams.get('ptls');
		SelectedCountries = urlParams.get('ctrs');
		
		RowsSetController.setPageSize(25);
		try
		{
			RowsSetController.setPageSize(Integer.valueOf(urlParams.get('rscPS')));
		}
		catch(Exception ex){/*do nothing*/}
		
		RowsSetController.setPageNumber(1);
		try
		{
			RowsSetController.setPageNumber(Integer.valueOf(urlParams.get('rscPN')));
		}
		catch(Exception ex){/*do nothing*/}
	}

    private String buildEqualsTextFilter(String field, String value)
    {
        return field + ' = \'' + escape(value) + '\'';
    }
    
    private String buildLikeFilter(String field, String value)
    {
        return field + ' like \'' + escape(value) + '%\'';
    }
    
    public String buildLikeFilter2(String field, String value)
    {
    	return field + ' like \'%' + String.escapeSingleQuotes(value) + '%\'';
    }
	
	private String buildAssigneeFilter()
	{
		List<String> conditions = new List<String>();
		for (User each : [select Id from User where Name in :AssigneeFilter.split(';')])
		{
			conditions.add('Step_Assigned_To__c like \'%' + each.Id + '%\'');
		}
		
		return (conditions.isEmpty() ? ' and Id = null ' : ' and (' + String.join(conditions, ' or ') + ')');
	}
	
	/**
	* Builds multi-select filter - if filter value is a single value - returns LIKE clause,
	* otherwise returns IN clause.
	*/
	private String buildMultiselectFilter(String filterField, String filterValue, String operator)
	{
		String result = ' ';
		List<String> values = filterValue.split(';');
		if (values.size() == 1)
		{
			result += operator + ' ' + buildLikeFilter(filterField, values.get(0));
		}
		else if (values.size() > 1)
		{
			result += operator + ' ' + buildInFilter(filterField, values);
		}
		
		return result;
	}
	
	/**
	* Changed visibility from "private" to "public" to be able to use it in extended class.
	*
	* @author	ilya.leshchuk
	* @date		09/26/14
	*/
	public String buildInFilter(String field, List<String> values)
	{
		List<String> escapedValues = new List<String>();
		for (String value : values)
		{
			escapedValues.add(value != null ? escape(value) : value);
		}
		
		return field + ' in (\'' + String.join(escapedValues, '\',\'') + '\')';
	}
    
    private String buildDateFilter(String field, Date startDate, Date endDate)
    {
        String filterStart = (startDate != null ? field + ' >= ' + PAWS_Utilities.formatDate(startDate, 'yyyy-MM-dd\'T00:00:00Z\'') : '');
        String filterEnd = (endDate != null ? field + ' <= ' + PAWS_Utilities.formatDate(endDate, 'yyyy-MM-dd\'T23:59:59Z\'') : '');
        return '(' + filterStart + (startDate != null && endDate != null ? ' and ' : '') + filterEnd + ')';
    }
    
    private String buildDateFilter(String fieldStart, String fieldEnd, Date dt)
    {
        if(dt == null) return '';
        return '(' + fieldStart + ' <= ' + PAWS_Utilities.formatDate(dt, 'yyyy-MM-dd\'T00:00:00Z\'') + ' and ' + fieldEnd + ' >= ' + PAWS_Utilities.formatDate(dt, 'yyyy-MM-dd\'T23:59:59Z\'') + ')';
    }
    
    private transient String FolderId;
    public String buildFolderFilter(String field)
	{
		if(FolderId == null) 
		{
			String folderName = PAWS_Settings__c.getOrgDefaults().Project_Folder_Name__c;
			if(!String.isEmpty(folderName))
			{
				List<STSWR1__Item__c> items = [select Id from STSWR1__Item__c where STSWR1__Parent__c = null and STSWR1__Type__c = 'Folder' and Name = :folderName];
				if(items.size() > 0) FolderId = items[0].Id;
			}
		}
		
		if(FolderId == null) return '';
		return ' and ' + field + ' in (select STSWR1__Source_Flow__c from STSWR1__Item__c where STSWR1__Source_Flow__c != null and STSWR1__Path__c like \'%' + FolderId + '%\')';
	}
		
	public String buildFolderFilterByProject(String field)
	{
		if(FolderId == null) 
		{
			String folderName = PAWS_Settings__c.getOrgDefaults().Project_Folder_Name__c;
			if(!String.isEmpty(folderName))
			{
				List<STSWR1__Item__c> items = [select Id from STSWR1__Item__c where STSWR1__Parent__c = null and STSWR1__Type__c = 'Folder' and Name = :folderName];
				if(items.size() > 0) FolderId = items[0].Id;
			}
		}
		
		if(FolderId == null) return '';
		return ' and ' + field + ' in (select Project__c from PAWS_Project_Flow_Junction__c where Folder__c != null and Folder__r.STSWR1__Path__c like \'%' + FolderId + '%\')';
	}
	
	public String buildFolderFilterByClient(String field)
	{
		if(FolderId == null) 
		{
			String folderName = PAWS_Settings__c.getOrgDefaults().Project_Folder_Name__c;
			if(!String.isEmpty(folderName))
			{
				List<STSWR1__Item__c> items = [select Id from STSWR1__Item__c where STSWR1__Parent__c = null and STSWR1__Type__c = 'Folder' and Name = :folderName];
				if(items.size() > 0) FolderId = items[0].Id;
			}
		}
		
		if(FolderId == null) return '';
		return field + ' in (select Project__r.Project_ID__r.WFM_Client__c from PAWS_Project_Flow_Junction__c where Folder__c != null and Folder__r.STSWR1__Path__c like \'%' + FolderId + '%\')';
	}
    
    private String escape(String value)
    {
    	return (value == null ? value : String.escapeSingleQuotes(value.replace('%3B', ';')));
    }

    /****************************** GANTT SECTION *********************************/

    public String RowsJSON
    {
    	get
        {
        	List<Map<String, Object>> resultRows = new List<Map<String, Object>>();
        	for(Row rowItem : Rows)
        	{
        		resultRows.add(new Map<String, Object>{
        			'ProjectName' => rowItem.Project.Name,
        			'Sponsor' => rowItem.Project.Sponsor__c,
        			'ObjectId' => rowItem.Project.Id,
        			'ObjectName' => rowItem.Project.Name,
        			'FlowName' => rowItem.Property.STSWR1__Step__r.STSWR1__Flow__r.Name,
        			'StepName' => rowItem.Property.STSWR1__Step__r.Name,
        			'Duration' => rowItem.Property.STSWR1__Step__r.STSWR1__Duration__c,
        			'StartDate' => rowItem.Property.Start_Date__c ,
        			'PlannedStartDate' => rowItem.Property.STSWR1__Planned_Start_Date__c,
        			'ActualStartDate' => (rowItem.History.STSWR1__Step__r.STSWR1__Is_System_Value__c == true ? rowItem.History.STSWR1__Actual_Start_Date_Value__c : rowItem.History.STSWR1__Actual_Start_Date__c),
        			'ActualEndDate' => (rowItem.History.STSWR1__Step__r.STSWR1__Is_System_Value__c == true ? rowItem.History.STSWR1__Actual_Complete_Date_Value__c : rowItem.History.STSWR1__Actual_Complete_Date__c),
        			'PlannedEndDate' => rowItem.Property.STSWR1__Planned_End_Date__c,
        			'RevisedStartDate' => rowItem.Property.STSWR1__Revised_Start_Date__c,
        			'RevisedEndDate' => rowItem.Property.STSWR1__Revised_End_Date__c,
        			'Role' => rowItem.Property.STSWR1__Step__r.STSWR1__Flow_Swimlane__r.Name,
        			'AssignedTo' => rowItem.AssignedTo
        		});
        	}

    		return JSON.serialize(resultRows);
        }
    }
    
    public PAWS_CrossProjectPlanningController pageController
    {
    	get
    	{
    		return this;
    	}
    } 
    
    public Boolean GanttMode {get;set;}
    
    public void enableGanttMode()
    {
    	GanttMode = true;
    }
    
    public void cancelGanttMode()
    {
    	GanttMode = false;
    }


    /****************************** OTHER SECTION *********************************/
    
    public class Row
    {
        public Row(STSWR1__Gantt_Step_Property__c property, STSWR1__Flow_Instance_History__c history, String assignedTo, Map<ID, String> assignersMap)
        {
        	this.Property = property;
            this.Project = property.Project__r;
            this.History = history;

            if(!String.isEmpty(assignedTo))
            {
                List<String> assigneeNames = new List<String>();
                for(String assignee : assignedTo.split(','))
                {
                    if(assignee instanceof ID) assigneeNames.add(assignersMap.get(assignee));
                    else assigneeNames.add(assignee);
                }
                
                this.AssignedTo = String.join(assigneeNames, ', ');
            }
        }
        
        public ecrf__c Project {get;set;}
        public STSWR1__Flow_Instance_History__c History {get;set;}
        public STSWR1__Gantt_Step_Property__c Property {get;set;}
        public String AssignedTo {get;set;}
        public PAWS_Project_Flow_Junction__c ProjectJunction {get;set;}
    }
}