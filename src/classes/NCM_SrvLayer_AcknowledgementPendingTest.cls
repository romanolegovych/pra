/**
 * @description	Implements the test for the functions of the NCM_SrvLayer_AcknowledgementPending class
 * @author		Kalaiyarasan Karunanithi
 * @version		Created: 10-Sep-2015
 */
@isTest
private class NCM_SrvLayer_AcknowledgementPendingTest {
	
	/**
	 * @description	Test the function getPendingAcknowledgement.
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 	10-Sep-2015
	 */
    static testMethod void getPendingAcknowledgementTest() {
        // Create data
		NotificationCatalog__c catalog = new NotificationCatalog__c(NotificationName__c = 'Topic A',
													ImplementationClassName__c = 'Class A',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		insert catalog;
		
		List<User> userList = NCM_Utils.buildTestUserList(2);
		
		List<NotificationRegistration__c> source = new List<NotificationRegistration__c>();
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = 'Rl A',
													Active__c = false) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = 'Rl A',
													Active__c = true) );
		insert source;
		
		List<Notification__c> notificationList = new List<Notification__c>();
		notificationList.add(new Notification__c(NotificationRegistration__c = source[0].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today()));
		notificationList.add(new Notification__c(NotificationRegistration__c = source[1].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today()));
		notificationList.add(new Notification__c(NotificationRegistration__c = source[1].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today()));
		insert notificationList;
		
		List<NCM_API_DataTypes.PendingAcknowledgementWrapper> results =  NCM_SrvLayer_AcknowledgementPending.getPendingAcknowledgement(userList[0].Id);
		
		String errorMessage = 'Error in building Pending Acknowledgement Status';
		
		system.assertEquals(2, results.size(), errorMessage);
		
		system.assertEquals(notificationList[1].Id, results[0].notificationId, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, results[0].Status, errorMessage);
		system.assertEquals(notificationList[1].Reminder__c, results[0].Reminder, errorMessage);
		
		system.assertEquals(notificationList[2].Id, results[1].notificationId, errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, results[1].Status, errorMessage);
		system.assertEquals(notificationList[2].Reminder__c, results[1].Reminder, errorMessage);
    }
    
    /**
	 * @description	Test the function updatePendingAcknowledgement.
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 	10-Sep-2015
	 */
    static testMethod void updatePendingAcknowledgementTest() {
    	
    	  // Create data
		NotificationCatalog__c catalog = new NotificationCatalog__c(NotificationName__c = 'Topic A',
													ImplementationClassName__c = 'Class A',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		insert catalog;
		
		List<User> userList = NCM_Utils.buildTestUserList(2);
		
		List<NotificationRegistration__c> source = new List<NotificationRegistration__c>();
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = 'Rl A',
													Active__c = false) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = 'Rl A',
													Active__c = true) );
		insert source;
		
		List<Notification__c> notificationList = new List<Notification__c>();
		notificationList.add(new Notification__c(NotificationRegistration__c = source[0].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today()));
		notificationList.add(new Notification__c(NotificationRegistration__c = source[1].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today()));
		notificationList.add(new Notification__c(NotificationRegistration__c = source[1].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today()));
		insert notificationList;
		
		List<NCM_API_DataTypes.PendingAcknowledgementWrapper> results = new List<NCM_API_DataTypes.PendingAcknowledgementWrapper>();
		NCM_API_DataTypes.PendingAcknowledgementWrapper tmpwrapper = new NCM_API_DataTypes.PendingAcknowledgementWrapper();
		tmpwrapper.notificationId= notificationList[1].Id;
		tmpwrapper.status = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT;
		tmpwrapper.reminder = Date.today();
		results.add( tmpwrapper );
													
		tmpwrapper = new NCM_API_DataTypes.PendingAcknowledgementWrapper();
		tmpwrapper.notificationId= notificationList[2].Id;
		tmpwrapper.status = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT;
		tmpwrapper.reminder = Date.today();
		results.add( tmpwrapper );
		
		NCM_SrvLayer_AcknowledgementPending.updatePendingAcknowledgement(results);
	
    }
}