/** Implements the Service Layer of the object StudyService__c
 * @author	Dimitrios Sgourdos
 * @version	27-Feb-2014
 */
public with sharing class StudyServiceService {
	
	/** Retrieve the StudyService with the given id.
	 *	This function is used as intermediary for the communication between Selector layer and Controllers.
	 * @author	Dimitrios Sgourdos
	 * @version 28-Jan-2014
	 * @param	studyServiceId		The id of the StudyService
	 * @return	The retrieved StudyService.
	 */
	public static StudyService__c getStudyServiceById(String studyServiceId) {
		return StudyServiceDataAccessor.getStudyServiceById(studyServiceId);
	}
	
	
	/** Create a select option list for the Calculation Helper for the Study Service Number of Units.
	 * @author	Dimitrios Sgourdos
	 * @version 29-Jan-2014
	 * @return	The created select option list.
	 */
	public static List<SelectOption> createSelectOptionsForCalculations(String noSelectionLabel) {
		// Create the select option list
		List<SelectOption> results = new List<SelectOption>();
		
		// Add the none value in case it is needed
		if(String.isNotBlank(noSelectionLabel)) {
			results.add(new SelectOption('', noSelectionLabel));
		}
		
		// Add the options for calculation
		results.add( new SelectOption(StudyServiceDataAccessor.TIME_PERIOD_BETWEEN_MILESTONES,
										StudyServiceDataAccessor.TIME_PERIOD_BETWEEN_MILESTONES));
		results.add( new SelectOption(StudyServiceDataAccessor.ASSESSMENTS_FOR_ALL_FC_IN_STUDY_RULE,
										StudyServiceDataAccessor.ASSESSMENTS_FOR_ALL_FC_IN_STUDY_RULE) );
		results.add( new SelectOption(StudyServiceDataAccessor.PREDEFINED_CATEGORIES_ASSESSMENTS_IN_STUDY_RULE,
										StudyServiceDataAccessor.PREDEFINED_CATEGORIES_ASSESSMENTS_IN_STUDY_RULE) );
		
		
		return results;
	}
	
	
	/** Implement the calculation for the calculation helper when the rule type is 
	 *	'ASSESSMENTS_FOR_ALL_FC_IN_STUDY_RULE'.
	 * @author	Dimitrios Sgourdos
	 * @version 29-Jan-2014
	 * @param	studyId				The Id of the study that the calculation is assigned with
	 * @return	The calculated number.
	 */
	public static Decimal calculateAssessmentForAllFlowchartsInStudy(String studyId) {
		String whereClause = 'Study__c = \'' + studyId + '\' AND Totalunits__c != null and Totalunits__c != 0';
		
		List<FlowchartServiceTotal__c> fstList = FlowchartServiceTotalDataAccessor.getFlowchartServiceTotalList(
																									whereClause, NULL);
		
		Set<String> uniqueCombSet = new Set<String>();
		for (FlowchartServiceTotal__c fst : fstList) {
			uniqueCombSet.add(fst.flowchartassignment__r.flowchart__c + ':' + fst.projectservice__r.service__c);
		}
		
		Decimal result = uniqueCombSet.size();
		
		return result.setScale(0);
	}
	
	
	/** Implement the calculation for the calculation helper when the rule type is 
	 *	'PREDEFINED_CATEGORIES_ASSESSMENTS_IN_STUDY_RULE'.
	 * @author	Dimitrios Sgourdos
	 * @version 31-Jan-2014
	 * @param	studyId				The Id of the study that the calculation is assigned with
	 * @return	The calculated number.
	 */
	public static Decimal calculateAssessmentForPredefCategoriesInStudy(String studyId, List<String> srvCatIds) {
		// Initialize where clause
		String whereClause = 'Study__c = \'' + studyId + '\' AND Totalunits__c != null and Totalunits__c != 0';
		
		// Initialize query to find applicable Services
		Set<String> srvCatIdsSet = new Set<String>();
		srvCatIdsSet.addAll(srvCatIds);
		String listOfIds = '';
		
		for (String categoryId : srvCatIdsSet) {
			if(categoryId != null) {
				listOfIds += ',\''+ categoryId +'\'';
			}
		}
		
		listOfIds = listOfIds.removeStart(',');
		
		if(String.isNotBlank(listOfIds)) {
			// Find applicable Services
			String srvWhereClause = 'isdesignobject__c = true and bdtdeleted__c = false ';
			srvWhereClause += ' AND ( (servicecategory__c in (' +listOfIds+'))';
			srvWhereClause += ' OR (servicecategory__r.parentcategory__c in (' +listOfIds+'))';
			srvWhereClause += ' OR (servicecategory__r.parentcategory__r.parentcategory__c in (' +listOfIds+')) )';
			List<Service__c> serviceList = ServiceDataAccessor.getServiceList(srvWhereClause);
			
			// Update final where clause
			Set<String> srvIdsSet = BDT_Utils.getStringSetFromList(serviceList, 'Id');
			String srvlistOfIds = '';
		
			for (String serviceId : srvIdsSet) {
				if(serviceId != null) {
					srvlistOfIds += ',\''+ serviceId +'\'';
				}
			}
		
			srvlistOfIds = srvlistOfIds.removeStart(',');
			if( String.isNotBlank(srvlistOfIds) ) {
				whereClause += ' AND ProjectService__r.Service__c in (' + srvlistOfIds +')';
			}
		} else {
			return 0;
		}
		
		// Calculate assessments
		Decimal result = 0;
		
		List<FlowchartServiceTotal__c> fstList = FlowchartServiceTotalDataAccessor.getFlowchartServiceTotalList(
																									whereClause, NULL);
		
		for (FlowchartServiceTotal__c fst : fstList) {
			List<FlowchartServiceTotalService.ExplanationJSONWrapper> tmpExplList =
					FlowchartServiceTotalService.deserializeFromStringToExplanationJSONWrapperList(fst.ExplanationJSON__c);
			
			for(FlowchartServiceTotalService.ExplanationJSONWrapper tmpItem : tmpExplList) {
				if(tmpItem.timepoints != NULL) {
					result += tmpItem.timepoints;
				}
			}
		}
		
		return result.setScale(0);
	}
	
	
	/** Create a select option list for the express type of the Calculation Helper 'TIME_PERIOD_BETWEEN_MILESTONES'
	 * @author	Dimitrios Sgourdos
	 * @version 25-Feb-2014
	 * @return	The created select option list.
	 */
	public static List<SelectOption> createOptionsForExpressTypeOfMilestoneCalculation(String noSelectionLabel) {
		// Create the select option list
		List<SelectOption> results = new List<SelectOption>();
		
		// Add the none value in case it is needed
		if(String.isNotBlank(noSelectionLabel)) {
			results.add(new SelectOption('', noSelectionLabel));
		}
		
		// Add the options for calculation
		results.add( new SelectOption(StudyServiceDataAccessor.EXPRESS_IN_DAYS,
										StudyServiceDataAccessor.EXPRESS_IN_DAYS));
		results.add( new SelectOption(StudyServiceDataAccessor.EXPRESS_IN_WEEKS,
										StudyServiceDataAccessor.EXPRESS_IN_WEEKS) );
		results.add( new SelectOption(StudyServiceDataAccessor.EXPRESS_IN_MONTHS,
										StudyServiceDataAccessor.EXPRESS_IN_MONTHS) );
		results.add( new SelectOption(StudyServiceDataAccessor.EXPRESS_IN_YEARS,
										StudyServiceDataAccessor.EXPRESS_IN_YEARS) );
		
		return results;
	}
	
	
	/** Implement the calculation for the calculation helper when the rule type is 
	 *	'TIME_PERIOD_BETWEEN_MILESTONES'.
	 * @author	Dimitrios Sgourdos
	 * @version 27-Feb-2014
	 * @param	startMilestoneId	The Id of the StudyMileStone that was selected for start milestone
	 * @param	endMilestoneId		The Id of the StudyMileStone that was selected for end milestone
	 * @param	expressIn			The type that the time period will be expressed
	 * @return	The calculated number.
	 */
	public static Decimal calculateTimePeriodBetweenMileStones( String startMilestoneId,
																String endMileStoneId,
																String expressIn) {
		// Check for bad parameters
		if(String.isBlank(startMilestoneId) || String.isBlank(endMileStoneId) || String.isBlank(expressIn)) {
			return 0;
		}
		
		// Find the StudyMileStones and check if they exist
		StudyMileStone__c startMileStone = BDT_StudyMileStoneService.getStudyMileStoneById(startMilestoneId);
		StudyMileStone__c endMileStone	 = BDT_StudyMileStoneService.getStudyMileStoneById(endMilestoneId);
		
		if(	startMileStone == null
				|| endMileStone == NULL
				|| startMileStone.DueDate__c == NULL
				|| endMileStone.DueDate__c == NULL) {
			return 0;
		}
		
		// Calculate the result
		Decimal result = 0;
		
		if(expressIn == StudyServiceDataAccessor.EXPRESS_IN_DAYS) {
			result = startMileStone.DueDate__c.daysBetween(endMileStone.DueDate__c);
		} else if(expressIn == StudyServiceDataAccessor.EXPRESS_IN_WEEKS) {
			result = startMileStone.DueDate__c.daysBetween(endMileStone.DueDate__c);
			result /= 7;
		} else if(expressIn == StudyServiceDataAccessor.EXPRESS_IN_MONTHS) {
			result = BDT_Utils.getAbsoluteMonthsBetweenDates(startMileStone.DueDate__c, endMileStone.DueDate__c);
		} else if(expressIn == StudyServiceDataAccessor.EXPRESS_IN_YEARS) {
			result = BDT_Utils.getAbsoluteMonthsBetweenDates(startMileStone.DueDate__c, endMileStone.DueDate__c);
			result /= 12;
		}
		
		return result.setScale(1, System.RoundingMode.UP);
	}
	
	
	/** Deserialize a string to the CalculationHelper sub-class.
	 * @author	Dimitrios Sgourdos
	 * @version 28-Jan-2014
	 * @return	The created sub-class.
	 */
	public static CalculationHelper deserializeFromStringToCalculationHelper(String jsonValue) {
		CalculationHelper result = new CalculationHelper();
		
		if( String.isNotBlank(jsonValue) ) {
			result = (CalculationHelper) JSON.deserialize(jsonValue, CalculationHelper.class);
		}
		
		return result;
	}
	
	
	/**	This class keeps a calculation for the number of units field of StudyService object.
	 * @author	Dimitrios Sgourdos
	 * @version	26-Feb-2014
	 */
	public class CalculationHelper {
		public String	rule			{get;set;}
		public Decimal	outcome			{get;set;}
		public String	categoriesIds	{get;set;}
		public String	startMileStone	{get;set;}
		public String	endMileStone	{get;set;}
		public String	timeExpressIn	{get;set;}
		public String	commentMileStone{get;set;}
		
		// It serialize the Item to a string
		public String  serializeToString() {
			return JSON.serialize(this);
		}
		
		// Formatted outcome
		public String  getOutcomeValue() {
			String result;
			Integer decimals = 0;
			if(rule==StudyServiceDataAccessor.TIME_PERIOD_BETWEEN_MILESTONES
					&& timeExpressIn!=StudyServiceDataAccessor.EXPRESS_IN_DAYS) {
				 decimals = 1 ;
			}
			if(outcome == NULL) {
				return NULL;
			}
			result = BDT_Utils.formatNumberWithSeparators(outcome, decimals);
			
			return result;
		}
		
		// Split the selected categories Ids
		public List<String> getSelectedCategoriesIds() {
			List<String> results = new List<String>();
			if( String.isNotBlank(categoriesIds) ) {
				results.addAll(categoriesIds.split(':'));
			}
			return results;
		}
		
		// Clean the not necessary data
		public void cleanUnusedData() {
			if(rule != StudyServiceDataAccessor.PREDEFINED_CATEGORIES_ASSESSMENTS_IN_STUDY_RULE) {
				categoriesIds = '';
			}
			if(rule != StudyServiceDataAccessor.TIME_PERIOD_BETWEEN_MILESTONES) {
				startMileStone	 = '';
				endMileStone	 = '';
				timeExpressIn	 = '';
				commentMileStone = '';
			}
		}
	}
}