/**
 * Test class for lms_role_employee delete trigger
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestHandleDeleteRoleEmpEvent {
	
	static List<LMSConstantSettings__c> constants;
    static List<LMS_Role__c> roles;
    static List<Role_Type__c> roleTypes;
    static List<Job_Class_Desc__c> jobClasses;
    static List<Job_Title__c> jobTitles;
    static List<PRA_Business_Unit__c> businessUnits;
    static List<Department__c> departments;
    static List<Region__c> regions;
    static List<Country__c> countries;
    static List<Employee_Type__c> empTypes;
    static List<Employee_Status__c> empStatuses;
    static List<LMS_Role_Employee__c> employeeMappings;
    static List<Employee_Details__c> employees;

	static void init() {
        constants = new LMSConstantSettings__c[] {
            new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
            new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
            new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
            new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
    		new LMSConstantSettings__c(Name = 'typePRA', Value__c = 'PRA'),
    		new LMSConstantSettings__c(Name = 'typeProject', Value__c = 'Project Specific'),
    		new LMSConstantSettings__c(Name = 'typeAdhoc', Value__c = 'Additional Role'),
            new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
            new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
            new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
            new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
            new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
            new LMSConstantSettings__c(Name = 'statusPending', Value__c = 'Pending'),
            new LMSConstantSettings__c(Name = 'statusToDelete', Value__c = 'Delete'),
            new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
            new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
            new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
            new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete'),
            new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended'),
            new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
            new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D')
        };
        insert constants;
        
        jobClasses = new Job_Class_Desc__c[]{
            new Job_Class_Desc__c(Name = 'Accountant', Job_Class_ExtID__c = 'Accountant', Job_Class_Code__c = 'JC1'),
            new Job_Class_Desc__c(Name = 'Account Manager', Job_Class_ExtID__c = 'Account Manager', Job_Class_Code__c = 'JC2')
        };
        insert jobClasses;
        
        jobTitles = new Job_Title__c[]{
            new Job_Title__c(Job_Code__c = 'Code1', Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = 'Accountant 1', Status__c = 'A'),
            new Job_Title__c(Job_Code__c = 'Code2', Job_Class_Desc__c = jobClasses[1].Id, Job_Title__c = 'Account Manager 1', Status__c = 'A')
        };
        insert jobTitles;
        
        businessUnits = new PRA_Business_Unit__c[]{
            new PRA_Business_Unit__c(Name = 'Analysis and Reporting', Business_Unit_Code__c = 'BUA', Status__c = 'A'),
            new PRA_Business_Unit__c(Name = 'CEO', Business_Unit_Code__c = 'BUB', Status__c = 'A'),
            new PRA_Business_Unit__c(Name = 'Accounting', Business_Unit_Code__c = 'BUC', Status__c = 'A')
        };
        insert businessUnits;
        
        departments = new Department__c[]{
            new Department__c(Name = 'Analysis and Reporting', Department_Code__c = 'DEPTA', Status__c = 'A'),
            new Department__c(Name = 'Executive Office', Department_Code__c = 'DEPTB', Status__c = 'A'),
            new Department__c(Name = 'Analysis and Reporting', Department_Code__c = 'DEPTC', Status__c = 'A')
        };
        insert departments;
        
        regions = new Region__c[]{
            new Region__c(Region_Name__c = 'US/Canada', Region_Id__c = 1, Status__c = 'A'),
            new Region__c(Region_Name__c = 'Latin America', Region_Id__c = 2, Status__c = 'A'),
            new Region__c(Region_Name__c = 'Europe/Asia', Region_Id__c = 3, Status__c = 'A'),
            new Region__c(Region_Name__c = 'Asia/Pacific', Region_Id__c = 4, Status__c = 'A')    
        };
        insert regions;
        
        countries = new Country__c[]{
            new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='US/Canada', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry2',PRA_Country_ID__c='200', Country_Code__c='JU',Region_Name__c='Latin America', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry3',PRA_Country_ID__c='300', Country_Code__c='TB',Region_Name__c='Asia/Pacific', 
                               Daily_Business_Hrs__c=8.0),
            new Country__c(name='TestCountry4',PRA_Country_ID__c='400', Country_Code__c='AB',Region_Name__c='Europe/Africa', 
                               Daily_Business_Hrs__c=8.0)
        };
        insert countries;
        
        empTypes = new Employee_Type__c[]{
            new Employee_Type__c(Name = 'Employees'),
            new Employee_Type__c(Name = 'Contractor')
        };
        insert empTypes;
        
        empStatuses = new Employee_Status__c[]{
            new Employee_Status__c(Employee_Status__c = 'AA', Employee_Type__c = empTypes[0].Name),
            new Employee_Status__c(Employee_Status__c = 'ZZ', Employee_Type__c = empTypes[1].Name)
        };
        insert empStatuses;
        
        roleTypes = new Role_Type__c[]{
            new Role_Type__c(Name = 'PRA')
        };
        insert roleTypes;
        
        roles = new LMS_Role__c[]{
            new LMS_Role__c(Job_Class_Desc__c = jobClasses[0].Id, Job_Title__c = jobTitles[0].Id, Business_Unit__c = businessUnits[0].Id,
                            Department__c = departments[0].Id, Region__c = regions[0].Id, Country__c = countries[0].Id, Employee_Type__c = empTypes[0].Id,
                            Status__c = 'Active', Sync_Status__c = 'Y', Role_Type__c = roleTypes[0].Id, SABA_Role_PK__c = '1')
        };
        insert roles;
        
        employees = new Employee_Details__c[]{
            new Employee_Details__c(Name = 'employee1', Employee_Unique_Key__c = 'employee1', First_Name__c = 'Andrew', Last_Name__c = 'Allen', Email_Address__c = 'a@a.com', 
                                    Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', Department__c = 'Analysis and Reporting',
                                    Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', Date_Hired__c = date.parse('12/27/2009'),
                                    Business_Unit__c = 'RDU')
        };
        insert employees;	
	}

    static testMethod void testRoleEmpStatusChangeDelete() {
        init();
        LMS_Role_Employee__c re = [SELECT Status__c FROM LMS_Role_Employee__c where Role_Id__c = :roles[0].Id];
        re.Status__c = 'Delete';
        update re;
        List<LMS_Role_Employee__c> roleEmps = [SELECT Status__c FROM LMS_Role_Employee__c WHERE Role_Id__c = :roles[0].Id];
        System.assert(roleEmps.size() == 0);
    }
}