/** Implements the Selector Layer of the object ServiceCategory__c
 * @author	Dimitrios Sgourdos
 * @version	10-Dec-2013
 */
public with sharing class ServiceCategoryDataAccessor {
	/** Object definition for fields used in application for ServiceCategory
	 * @author	Dimitrios Sgourdos
	 * @version 10-Dec-2013
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('ServiceCategory__c');
	}
	
	
	/** Object definition for fields used in application for ServiceCategory with the parameter referenceName as a prefix
	 * @author	Dimitrios Sgourdos
	 * @version 10-Dec-2013
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'BDTDeleted__c,';
		result += referenceName + 'Code__c,';
		result += referenceName + 'ParentCategory__c';
		return result;
	}
	
	
	/** Retrieve the Service Categories that meet the given whereClause.
	 * @author	Dimitrios Sgourdos
	 * @version 10-Dec-2013
	 * @param	whereClause			The criteria that Service Categories must meet
	 * @param	orderByField		The field that the Service Categories will be ordered by
	 * @return	The Service Categories list.
	 */
	public static List<ServiceCategory__c> getServiceCategories(String whereClause, String orderByField) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM ServiceCategory__c ' +
								'WHERE  {1} ' +
								'ORDER BY {2}',
								new List<String> {
									getSObjectFieldString(),
									whereClause,
									orderByField
								}
							);
		return (List<ServiceCategory__c>) Database.query(query);
	}
	
	
	/** Retrieve the First Level Service Categories.
	 * @author	Dimitrios Sgourdos
	 * @version 10-Dec-2013
	 * @return	The First Level Service Categories list.
	 */
	public static List<ServiceCategory__c> getFirstLevelServiceCategories() {
		return getServiceCategories('ParentCategory__c = NULL AND BDTDeleted__c = false', 'Code__c');
	}
	
	public static List<ServiceCategory__c> getServiceAndCategoriesForProject(ID ProjectId) {
		
		Set<id> ServiceIds = new Set<id>();
		Set<id> ServiceCategoryIds = new Set<id>();
		
		// read deleted services or deleted categories for the specified project
		For (ProjectService__c ps:[
				select service__c 
					,service__r.servicecategory__c
					,service__r.servicecategory__r.parentcategory__c
					,service__r.servicecategory__r.parentcategory__r.parentcategory__c
				from ProjectService__c 
				where client_project__c = :ProjectId
				and (service__r.bdtdeleted__c = true
					or service__r.servicecategory__r.bdtdeleted__c = true
					or service__r.servicecategory__r.parentcategory__r.bdtdeleted__c = true
					or service__r.servicecategory__r.parentcategory__r.parentcategory__r.bdtdeleted__c = true)]){
			ServiceIds.add(ps.service__c);
			ServiceCategoryIds.add(ps.service__r.servicecategory__c);
			ServiceCategoryIds.add(ps.service__r.servicecategory__r.parentcategory__c);
			ServiceCategoryIds.add(ps.service__r.servicecategory__r.parentcategory__r.parentcategory__c);
		}
		
		ServiceIds.remove(null);
		ServiceCategoryIds.remove(null);
		
		String ListOfServices = '';
		String ListOfServiceCategories = '';
		
		// convert set to a string list
		if (ServiceIds.size()>0) {
			For (Id serviceId:ServiceIds) {
				ListOfServices += ',\''+serviceId+'\'';
			}
			ListOfServices = 'or id in (' + ListOfServices.removeStart(',') +')';
		}
			
		
		// convert set to a string list
		if (ServiceIds.size()>0) {
			For (Id serviceCategoryId:ServiceCategoryIds) {
				ListOfServiceCategories += ',\''+serviceCategoryId+'\'';
			}
			ListOfServiceCategories = 'or id in (' + ListOfServiceCategories.removeStart(',') +')';
		}
		
		String query = String.format(
								'SELECT {0} , (' +
									'Select {1} ' +
									'From Services__r ' +
									'where BDTDeleted__c = false ' +
									'{2} ' +
									'order by SequenceNumber__c ) ' +
								'FROM ServiceCategory__c ' +
								'WHERE BDTDeleted__c = false ' +
								'{3} ' +
								'ORDER BY Code__c',
								new List<String> {
									getSObjectFieldString(),
									ServiceDataAccessor.getSObjectFieldString(),
									ListOfServices,
									ListOfServiceCategories
								}
							);
							
		system.debug (query);
		
		return (List<ServiceCategory__c>) Database.query(query);
		
	}
	
}