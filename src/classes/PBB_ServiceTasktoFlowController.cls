/** 
@Author Fahad Arif
@Date 2015
@Description this class is gives the picklist of records of service task and flow steps in Service_Task_To_Flow_Step__c  object.
*/
public class PBB_ServiceTasktoFlowController {
    
    public Service_Task_To_Flow_Step__c STtoFS{get; set;}
    public WR_model__c wrModel{get; set;}
    
    //Standard Constructor
    public PBB_ServiceTasktoFlowController(ApexPages.StandardController controller) {
        
        STtoFS = new Service_Task_To_Flow_Step__c();
        STtoFS = (Service_Task_To_Flow_Step__c)controller.getRecord();
        if(STtoFS.id!=null){
          STtoFS = PBB_DataAccessor.getServiceTaskRecord(STtoFS.id);
        }
        wrModel = PBB_DataAccessor.getWrModelRecord(STtoFS.WR_Model__c);
        
        if(ApexPages.currentPage().getParameters().get('clone')=='1'){
            STtoFS = STtoFS.clone(false, true);
        }
    }
    
    /** 
	@Author Fahad Arif
	@Date 2015
	@Description this method gives the picklist of records of service task in Service_Task_To_Flow_Step__c  object.
	*/
    public List<SelectOption> getServiceTaskPickList(){
        List<SelectOption> serviceTaskPickList = new List<SelectOption>();
        serviceTaskPickList.add(new selectoption('','Please Select the Service Task'));
        for(Service_task__c x:PBB_DataAccessor.getServiceTaskListbyServiceModel(wrModel.Service_Model__c)){           
                serviceTaskPickList.add(new selectoption(x.Id,x.name));        	
       	}
        return serviceTaskPickList; 
    }     
    
    /** 
	@Author Fahad Arif
	@Date 2015
	@Description this method gives the picklist of records of flow steps in Service_Task_To_Flow_Step__c  object.
	*/
	public List<SelectOption> getFlowStepPickList(){
        List<SelectOption> flowStepPickList = new List<SelectOption>();
        if(wrModel.Flow__c !=null){
            List<PAWS_PBB_Helper.WrModelStepWrapper> listofFlows = PAWS_API.PBB_getWRModelSteps(wrModel.Flow__c);
            flowStepPickList.add(new selectoption('','Please Select the Flow Step'));    
            for(PAWS_PBB_Helper.WrModelStepWrapper pWRModel: listofFlows){
                flowStepPickList.add(new selectoption(pWRModel.StepId,pWRModel.StepName +'/'+pWRModel.FlowName +'/'+pWRModel.FlowPath ));
            }
        }
        return flowStepPickList;
    }    
    
     /** 
	@Author Fahad Arif
	@Date 2015
	@Description this method is used for saving the record.
	*/
    public PageReference saveSTtoFS() {
        PageReference pageRef =null;   
        try{   
            if(STtoFS.Flow_Step__c != null && STtoFS.Service_Tasks__c!=null)    {
                upsert STtoFS;
                pageRef=new PageReference('/'+STtoFS.id);
                pageRef.setRedirect(true); 
            } else {
                ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, ' Please select required field before Saving' );
                ApexPages.addMessage(msg);
            }
                
        } catch (Exception e) {
            ApexPages.Message msg = new ApexPages.Message(Apexpages.Severity.ERROR, e.getdmlMessage(0) );
            ApexPages.addMessage(msg);
        } 
        return pageRef;  
    } 
}