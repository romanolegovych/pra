public class AA_Service {
    public static list<Holiday_Schedule__c> getHolidayByCountryYear(string country, decimal Year){
        return AA_DataAccessor.getHolidayByCountryYear(country, year);
    }
    
    public static Holiday_Schedule__c getHolidayDetailsById(string Id){
        return AA_DataAccessor.getHolidayDetailsById(Id);                 
    } 
    
    public static Holiday_Schedule__c getHolidayToDelete(String Id){
        return AA_DataAccessor.getHolidayToDelete(Id);
    }
    
    public static List<Holiday_schedule__c> getHolidayToApprove(string country, decimal year, string status){
        return AA_DataAccessor.getHolidayToApprove(country,year,status);
    }
    
    public static List<Holiday_Country_State__c>  getStateList(String country){
        return AA_DataAccessor.getStateList(country);
    }
    
    public static List<Holiday_Schedule__c> getHolidayToClone(string country, decimal year){
       return AA_DataAccessor.getHolidayToClone(country,year);
    }
    
    public static List<Country__c> getCountryList(boolean office){
       return AA_DataAccessor.getCountryList(office);
    }   

}