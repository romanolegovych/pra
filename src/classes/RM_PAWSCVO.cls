/**
* @author       Min Chen
* @description  RM Assignment CVO for PAWS class to hold data for PAWS
                
*/

public with sharing class RM_PAWSCVO {

	public string ProjectID{get; set;}
    public string Emp_LastName{get;set;}
    public string Emp_FirstName{get; set;}
    public string Emp_BusinessUnit{get; set;}
    public string Project_Role{get; set;}
    public Boolean Is_Lead{get; set;}
    public string Assignment_Country{get; set;}
    public string EmployeeID{get; set;}
    public string UserID{get; set;}
    public Date Start_Date{get; set;}
    public Date End_Date{get; set;}
    
     /**
        * @author   Min Chen
        * @date     Feb 2015   
        * @description  Constructor 
        **/
    public RM_PAWSCVO(WFM_employee_Allocations__c all){
    	ProjectID = all.project_id__r.name;
    	emp_LastName = all.employee_id__r.last_name__c;
    	emp_FirstName = all.employee_id__r.first_name__c;
    	Emp_BusinessUnit = all.employee_id__r.business_unit_desc__c;
    	Project_Role = all.project_function__c;
    	Is_Lead = all.Is_lead__c;
    	Assignment_Country = all.Project_Country__c;
    	EmployeeID = all.employee_id__c;
    	UserID = all.employee_id__r.user__c;
    	Start_Date = all.Allocation_Start_Date__c;
    	End_Date = all.Allocation_End_Date__c;
    }
}