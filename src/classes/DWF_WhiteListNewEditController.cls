public class DWF_WhiteListNewEditController {
	
	public Boolean isPageEdit {get;set;}
	public DWF_Whitelist__c whiteList {get;set;}
	public DWF_FilterService.companyVO companyVO {get;set;}
	public DWF_FilterService.businessUnitVO businessUnitVO {get;set;}
	public list<selectoption> companyNames {get;set;}
	public list<selectoption> businessUnits {get;set;}
	private list<DWF_FilterService.companyVO> companyVOs;
	ApexPages.standardController standardController;
	
	public DWF_WhiteListNewEditController(ApexPages.standardController stdCtr) {
		standardController = stdCtr;
		String recordID = standardController.getId();
		isPageEdit = !(String.isBlank(recordID));
		companyNames = getCompanyNames();
		businessUnits = getBusinessUnits(null);
		if (!isPageEdit) {
			whiteList = new DWF_WhiteList__c();
		} else {
			try {
				whiteList = [select Name, Id, Company_No__c, Company_Name__c, Business_Unit_No__c, Business_Unit_Name__c, DWF_System__c
					from DWF_WhiteList__c where Id = :recordID];
				getCompanyOnLoad();
			} catch (Exception e) {
				Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			}
		}
	}
	
	public list<selectoption> getCompanyNames() {
		DWF_FilterService.companyListResponse response = DWF_FilterServiceWrapper.getAllCompanyVOs();
		companyVOs = response.companyVOs;
		return DWF_Util.getCompanyNames(companyVOs);
	}
	
	public list<selectoption> getBusinessUnits(list<DWF_FilterService.businessUnitVO> businessUnitVOs) {
		return DWF_Util.getBusinessUnits(businessUnitVOs);
	}
	
	public list<selectoption> getSystemNames() {
		return DWF_Util.getSystemNames();
	}

	public void getCompanyFromSelectedValue() {
		companyVO = new DWF_FilterService.companyVO();
		if (companyVOs != null) {
			for (DWF_FilterService.companyVO compVO : companyVOs) {
				if (whiteList.Company_No__c == compVO.companyNo) {
					companyVO = compVO;
					whiteList.Company_No__c = compVO.companyNo;
					whiteList.Company_Name__c = compVO.companyName;
					whiteList.Business_Unit_No__c = null;
					businessUnits = getBusinessUnits(compVO.businessUnits);
				}
			}
		} else {
			whiteList.Company_No__c = null;
		}
	}
	
	public void getCompanyOnLoad() {
		companyVO = new DWF_FilterService.companyVO();
		if (companyVOs != null) {
			for (DWF_FilterService.companyVO compVO : companyVOs) {
				if (whiteList.Company_No__c == compVO.companyNo) {
					companyVO = compVO;
					whiteList.Company_No__c = compVO.companyNo;
					whiteList.Company_Name__c = compVO.companyName;
					businessUnits = getBusinessUnits(compVO.businessUnits);
				}
			}
		} else {
			whiteList.Company_No__c = null;
		}
	}
	
	public PageReference saveWhiteList() {
		if (whiteList != null) {
			// Perform data validation
			set<String> whiteListKeys = DWF_Util.getCurrentWLsForSystem(whiteList.DWF_System__c);
			try {
				if (!whiteListKeys.contains(whiteList.Company_Name__c + '_' + whiteList.Business_Unit_Name__c)) {
					upsert whiteList;
				}
				return standardController.cancel();
			} catch (Exception e) {
				Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
			}
		}
		return null;
	}
}