@IsTest
public with sharing class BDT_AutocompleteTest {
	
	static testmethod void BDT_AutocompleteTest(){
		Subcontractor__c sc = new Subcontractor__c(Name='Test');
		upsert sc;
		SubcontractorActivity__c sca = new SubcontractorActivity__c(Name='tetstest',Subcontractor__c=sc.Id);
		upsert sca;
		
		List<Sobject> testList = BDT_Autocomplete.getAutocompleteData('Subcontractor__c', 'Name,City__c','T',false);
		
		system.assertEquals(1,testList.size() );
	}
	
	
	/** Test the function getAutocompleteDataPlenboxUsers
	 * @author	Dimitrios Sgourdos
	 * @version	29-Jan-2014
	 */
	static testMethod void getAutocompleteDataPlenboxUsersTest() {
		// Create data
		Profile p = [select id from profile where name='System Administrator'];
		User myUser = new User(alias = 'TestMan', email='justtesting@testmail.com',
			emailencodingkey='UTF-8', firstname='Clark', lastname='Kent', languagelocalekey='en_US',
			localesidkey='en_US', profileid = p.Id,
			timezonesidkey='America/Los_Angeles', username='justtesting@testmail.com');
		insert myUser;
		
		BDT_Role__c role = new BDT_Role__c(Name='Test User', Role_Number__c=1);
		insert role;
		
		BDT_UserRoles__c userRole = new BDT_UserRoles__c(name=myUser.id, RoleNumbers__c = '1');
		insert userRole;
		
		// Check the function
		List<Sobject> testList = BDT_Autocomplete.getAutocompleteDataPlenboxUsers('Name', 'Cla');
		
		system.assertEquals(1,testList.size() );
	}
}