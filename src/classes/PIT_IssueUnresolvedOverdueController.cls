global with sharing class PIT_IssueUnresolvedOverdueController implements Schedulable {
    
    global void execute(SchedulableContext SC) {
        List<IssueUnresolved__c> issueUnresList = new List<IssueUnresolved__c>();   
        List<Issue__c>           issueList      = new List<Issue__c>();
        // Delete all the records
        issueUnresList = [SELECT id FROM IssueUnresolved__c];
        delete issueUnresList;
        // Find all the issues that have not reached the Status of 'Resolved' on or before the Requested Resolution Date
        issueList = [SELECT Id, Assign_To__c,Assign_To__r.EmployeeNumber, Assign_To__r.ManagerId, Assign_To__r.Name, Date_Resolved__c 
                    FROM    Issue__c 
                    WHERE   Status__c != 'Resolved'     AND 
                            Date_Resolved__c < TODAY    AND 
                            Assign_To__c!=NULL           
                            //AND Assign_To__r.ManagerId != NULL
                            ];
        // For every overdue issue, created an IssueUnresolved__c record
        issueUnresList = new List<IssueUnresolved__c>();
        set<string> asstlset=new set<string>();
         for(Issue__c tmpIssue : issueList) {             
             asstlset.add(tmpIssue.Assign_To__r.EmployeeNumber);             
         }
        system.debug('-----------asstlset-------'+asstlset);
        
        
        set<string> mangset=new set<string>();
        set<string> assget=new set<string>();
        map<string,string> MapAsstoMang=new map<string,string>();
        for(Employee_details__c e:[select id,Name,Supervisor_ID__c from Employee_details__c  where name in:asstlset]){           
            MapAsstoMang.put(string.valueof(e.name),string.valueof(e.Supervisor_ID__c ));
        }
        system.debug('-----------MapAsstoMang-------'+MapAsstoMang);
        
        map<string,string> usermap=new map<string,string>();
        for(user u:[select id,Name,EmployeeNumber from user where EmployeeNumber in: MapAsstoMang.values()]){
           
            usermap.put(string.valueof(u.EmployeeNumber ),string.valueof(u.id ));
        }
        system.debug('-----------usermap-------'+usermap);
        
        for(Issue__c tmpIssue : issueList) {
            IssueUnresolved__c tmpIssueUnres = new IssueUnresolved__c();
            
            string s=MapAsstoMang.get(string.valueof(tmpIssue.Assign_To__r.EmployeeNumber));
            
            tmpIssueUnres.FunctionalManager__c =usermap.get(string.valueof(s));
            // tmpIssue.Assign_To__r.EmployeeNumber; 
            tmpIssueUnres.Date_resolved__c    = tmpIssue.Date_Resolved__c;
            tmpIssueUnres.Assign_To_Person__c = tmpIssue.Assign_To__r.Name;
            tmpIssueUnres.Issue__c            = tmpIssue.id;
            issueUnresList.add(tmpIssueUnres);
        }  
        if(issueUnresList.size() > 0) {
            insert issueUnresList;
        }
        
    }
}