public class Bhargav_testpageContlr {

    public void save() {
        system.debug('----SALIst1---'+SALIst1);
        SALIst1.clear();
        serviceTask stask1 = new serviceTask();
        stask1.ST='ST1';
        stask1.STID='STID1';
        List<serviceTask> STLIst1 = new List<serviceTask>();
        STLIst1.add(stask1);
        
        serviceTask stask2 = new serviceTask();
        stask2.ST='ST2';
        stask2.STID='STID2';
        
        serviceTask stask3 = new serviceTask();
        stask3.ST='ST3';
        stask3.STID='STID3';
        STLIst1.add(stask3);
        List<serviceTask> STLIst2 = new List<serviceTask>();
        STLIst2.add(stask2);
        
        
        system.debug('----STLIst1---'+STLIst1);
        system.debug('----STLIst2---'+STLIst2);


        serviceFunction sf1 = new serviceFunction();
        sf1.SF='SF1';
        sf1.SFID='SFID1';
        sf1.STLIst=(STLIst1);
        List<serviceFunction> SFLIst1 = new List<serviceFunction>();
        SFLIst1.add(sf1);

        serviceFunction sf2 = new serviceFunction();
        sf2.SF='SF2';
        sf2.SFID='SFID2';
        sf2.STLIst=(STLIst2);
        
        serviceFunction sf3 = new serviceFunction();
        sf3.SF='SF3';
        sf3.SFID='SFID3';
        sf3.STLIst=(STLIst1);
        
        
        List<serviceFunction> SFLIst2 = new List<serviceFunction>();
        SFLIst2.add(sf2);
        //SFLIst1.add(sf3);
        system.debug('----SFLIst1---'+SFLIst1);
        system.debug('----SFLIst2---'+SFLIst2);

        serviceArea sa1 = new serviceArea();
        sa1.SA='SA1';
        sa1.SAID='SAID1';
        sa1.SFLIst=(SFLIst1);
        SALIst1 = new List<serviceArea>();
        SALIst1.add(sa1);

        serviceArea sa2 = new serviceArea();
        sa2.SA='SA2';
        sa2.SAID='SAID2';
        sa2.SFLIst=(SFLIst2);
        SALIst2 = new List<serviceArea>();
        SALIst1.add(sA2);
        system.debug('----SALIst1---'+SALIst1);
        system.debug('----SALIst2---'+SALIst2);
        PageReference acctPage = ApexPages.currentPage();
        acctPage.setRedirect(true);
        //return acctPage;
        //return null;
    }


    public List<serviceArea> SALIst1{get;set;}    
    public List<serviceArea> SALIst2{get;set;}
    
    public Bhargav_testpageContlr (){
        serviceTask stask1 = new serviceTask();
        stask1.ST='ST1';
        stask1.STID='STID1';
        List<serviceTask> STLIst1 = new List<serviceTask>();
        STLIst1.add(stask1);
        
        serviceTask stask2 = new serviceTask();
        stask2.ST='ST2';
        stask2.STID='STID2';
        
        serviceTask stask3 = new serviceTask();
        stask3.ST='ST3';
        stask3.STID='STID3';
        STLIst1.add(stask3);
        List<serviceTask> STLIst2 = new List<serviceTask>();
        STLIst2.add(stask2);
        
        
        system.debug('----STLIst1---'+STLIst1);
        system.debug('----STLIst2---'+STLIst2);


        serviceFunction sf1 = new serviceFunction();
        sf1.SF='SF1';
        sf1.SFID='SFID1';
        sf1.STLIst=(STLIst1);
        List<serviceFunction> SFLIst1 = new List<serviceFunction>();
        SFLIst1.add(sf1);

        serviceFunction sf2 = new serviceFunction();
        sf2.SF='SF2';
        sf2.SFID='SFID2';
        sf2.STLIst=(STLIst2);
        
        serviceFunction sf3 = new serviceFunction();
        sf3.SF='SF3';
        sf3.SFID='SFID3';
        sf3.STLIst=(STLIst1);
        
        
        List<serviceFunction> SFLIst2 = new List<serviceFunction>();
        SFLIst2.add(sf2);
        //SFLIst1.add(sf3);
        system.debug('----SFLIst1---'+SFLIst1);
        system.debug('----SFLIst2---'+SFLIst2);

        serviceArea sa1 = new serviceArea();
        sa1.SA='SA1';
        sa1.SAID='SAID1';
        sa1.SFLIst=(SFLIst1);
        SALIst1 = new List<serviceArea>();
        SALIst1.add(sa1);

        serviceArea sa2 = new serviceArea();
        sa2.SA='SA2';
        sa2.SAID='SAID2';
        sa2.SFLIst=(SFLIst2);
        SALIst2 = new List<serviceArea>();
        SALIst1.add(sA2);
        system.debug('----SALIst1---'+SALIst1);
        system.debug('----SALIst2---'+SALIst2);
    }
    
    class serviceArea{
        public Boolean Selected{get;set;}
        Public String SA{get;set;}
        Public String SAID{get;set;}
        Public List<serviceFunction> SFLIst{get;set;}
        public serviceArea(){
            SFLIst= new List<serviceFunction>();
        }
    }

    class serviceFunction{
        public Boolean Selected{get;set;}
        Public String SF{get;set;}
        Public String SFID{get;set;}
        Public List<serviceTask> STLIst{get;set;}
        public serviceFunction(){
            STLIst= new List<serviceTask>();
        }
    }

    class serviceTask{
        public Boolean Selected{get;set;}
        Public String ST{get;set;}
        Public String STID{get;set;}
        Public String STB{get;set;}
    }
}