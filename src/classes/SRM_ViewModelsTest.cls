/**
* @author Ramya Shree Edara
* @date 21-Oct-2014
* @description this is test class for Model classes
*/
@isTest
public with sharing class  SRM_ViewModelsTest{
    
    static testMethod void myUnitTest() {
        List<SRM_Model__c> srmList = new List<SRM_Model__c>();        
        
        for(integer i=0; i<=10; i++){
            srmList.add(new SRM_Model__c());
        }
        
        insert srmList;
    
        SRM_ViewModels viewModel = new SRM_ViewModels(new ApexPages.StandardController(srmList[0]));
        
        ApexPages.StandardSetController modelSetController = viewModel.modelSetController;
        viewModel.getModelExistingViews();
        viewModel.redirect();
        
        SRM_ViewModels viewModel1 = new SRM_ViewModels();
    }
}