global with sharing class BDT_MainRightStudyDetails {
	
	public BDT_UserPreferenceService UserPreferences;
	public list<BDT_UserPreferenceService.quickListWrapper> quickList {get;set;}
		
	public String projectCode {get;set;}
	public String projectTitle {get;set;}
	public String drugAdmin {get;set;}
	public String sponsor {get;set;}
	public String sponsorCode {get;set;}
	public String projectId {get;set;}
	public String customId {get;set;} 
	public Client_Project__c project {get;set;}
	public List<Study__c>	studyList {get;set;}
	public Contact sponsorContactC{get;set;}
	//search variables
	public String selectedProject   {get;set;}
    public String selectedProjectLabel  {get;set;}    

    public List<studyWrapper> RelatedStudyWrapperList{get;set;}
    
    public List<String> selectedStudies{get;set;}
	public Boolean showDeleted{
		get {if (showDeleted == null) {
				showDeleted = false;
			}
			return showDeleted;
		}
		set;
	}
	public String description {get;set;}
	

	public BDT_MainRightStudyDetails(){
		
		// USER PREFERENCES
		UserPreferences = new BDT_UserPreferenceService();
		
		//projectId = BDT_Utils.getPreference('SelectedProject');
		
		projectId = UserPreferences.getPreference('SelectedProject');
						
		if(projectId!=null && projectId!=''){
			project = BDT_Utils.getProject(projectId, showDeleted);
			description = project.Project_Description__c;
		}else{
			project = new Client_Project__c();
		}
				
		selectedStudies = new List<String>();
		quickList = UserPreferences.getQuickList();
		buildStudyWrapperList();
	}
	
	public void deleteProjectFromQuickList(){		
		String deleteProjectId =  System.CurrentPageReference().getParameters().get('projectId');
		UserPreferences.removeProjectFromQuickList(deleteProjectId);
		UserPreferences.saveUserPreferences();
		quickList = UserPreferences.getQuickList();
	}
	
	public class studyWrapper{
		public Study__c studyRecord{get; set;}
		public Boolean selected{get; set;}
		
		public studyWrapper(Study__c sStudyRecord, Boolean sSelected){
			studyRecord = sStudyRecord;
			selected = sSelected;
		}
		
	}
	

	public void buildStudyWrapperList() {
		// get the studies for the currently selected project
		List<studyWrapper> studyWrapperList = new List<studyWrapper>();
		
		try{
			//selectedStudies = BDT_Utils.getPreference('SelectedStudies').split(':');
			selectedStudies = UserPreferences.getPreference('SelectedStudies').split(':');
		}
		catch(Exception e){
			selectedStudies = new List<String>();
		}
		
		Boolean isSelected;
		for(Study__c s: project.Studies__r){
			isSelected = false;
			for (String ss:selectedStudies){
				if(ss.equals(s.id)){isSelected=true;}
			}
			studyWrapperList.add(new studyWrapper(s, isSelected));	
		}
		RelatedStudyWrapperList = studyWrapperList;
	}
	
	@RemoteAction
	global static String toggleStudy(String StudyIds) {
		
		BDT_UserPreferenceService UserPreferences = new BDT_UserPreferenceService();
		Set<String> StudyIdSet = new Set<String>();
		try{
			for (String studyId : StudyIDs.split(':')){
				StudyIdSet.add(studyId);
			}
		} catch (Exception e) {}

		String ProjectID = UserPreferences.getPreference(BDT_UserPreferenceDataAccessor.PROJECT);
		Client_Project__c project;
		try{
			project = [select id, code__c from Client_Project__c where id = :ProjectID];
		} catch (Exception e) {}

		if (project!= null) {
			UserPreferences.setProjectAndStudies(project, StudyIdSet);
		}
		UserPreferences.saveUserPreferences();
		
		return 'Finished';
		
	}
	
	public PageReference quickListSwitchProject(){

		String project = System.CurrentPageReference().getParameters().get('projectId');
		// update the user preferences
		UserPreferences.setProject (project);
		UserPreferences.saveUserPreferences();

		// load single project page
		PageReference pageRef = new PageReference(System.Page.BDT_singleproject.getUrl());		
		pageRef.setRedirect(true);  // true will flush the view state
		return pageRef;

	}

}