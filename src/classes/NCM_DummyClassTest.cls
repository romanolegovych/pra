/**
 * @description	To test the class NCM_DummyClass
 * @author		Jegadheesan Muthusamy
 * @date		Created: 11-Sep-2015, Edited: 09-Oct-2015
 */
@isTest
private class NCM_DummyClassTest {
	
	/**
	 * @description	Test the functions getNotificationSubject,getNotificationEmailBody.
	 * @author		Jegadheesan Muthusamy
	 * @date		Created: 10-Sep-2015
	 */
	static testMethod void myUnitTest() {
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		List<NotificationCatalog__c> catalogList = new List<NotificationCatalog__c>();
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic A',
													ImplementationClassName__c = 'NCM_DummyClass',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION,
													AcknowledgementRequired__c = true ));
		insert catalogList;
		NCM_DummyClass dummyInstance = new NCM_DummyClass();
		String errorMessage = 'Error in NCM_DummyClass';
		system.assertEquals( true, dummyInstance.didNotificationTopicOccur(catalogList[0].Id, acc.Id), errorMessage );
		system.assertEquals( 'Test email subject', dummyInstance.getNotificationSubject(catalogList[0].Id, acc.Id),
																									errorMessage );
		system.assertEquals( 'Test email body', dummyInstance.getNotificationEmailBody(catalogList[0].Id, acc.Id),
																									errorMessage );
	}
}