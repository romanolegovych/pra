/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PRA_DataAccessorTest {

    static testMethod void testConstructor() {
        // This is just for code coverage
        test.startTest();
        PRA_DataAccessor da = new PRA_DataAccessor();
        test.stopTest();
    }
    
    static testMethod void testGetMissingERs() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        // Zero out all effort ratios
        List<Hour_EffortRatio__c> erList = [select Forecast_ER__c
                                          from Hour_EffortRatio__c
                                          where Unit_Effort__r.Client_Task__r.Project__r.id = :tu.clientProject.id
                                          and Unit_Effort__r.Client_Task__r.Description__c != 'Migrated Worked Hours'];
        
        for (Hour_EffortRatio__c er : erList) {
            er.Forecast_ER__c = 0;
        }
        update erList;
        
        test.startTest();
        
        List<Unit_Effort__c> ue1 = PRA_DataAccessor.getMissingEffortRatios(tu.clientProject.id, System.today(), System.today().addMonths(12));
        List<Unit_Effort__c> ue2 = PRA_DataAccessor.getMissingEffortRatios(tu.clientProject.id, System.today());

        test.stopTest();
        
        System.assertNotEquals(null, ue1);
        System.assertNotEquals(0, ue1.size());
        System.assertNotEquals(null, ue2);
        System.assertNotEquals(0, ue2.size());

        System.debug('--- TEST OUT: ue1.size = ' + ue1.size() );
        System.debug('--- TEST OUT: ue1 = ' + ue1);
        System.debug('--- TEST OUT: ue2.size = ' + ue2.size() );
        System.debug('--- TEST OUT: ue2 = ' + ue2);
    }
    
    static testMethod void testGetMissingERsByTask() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        // Zero out all effort ratios
        List<Hour_EffortRatio__c> erList = [select Forecast_ER__c
                                          from Hour_EffortRatio__c
                                          where Unit_Effort__r.Client_Task__r.Project__r.id = :tu.clientProject.id
                                          and Unit_Effort__r.Client_Task__r.Description__c != 'Migrated Worked Hours'];

        for (Hour_EffortRatio__c er : erList) {
            er.Forecast_ER__c = 0;
        }
        update erList;
        
        Client_Task__c testTask = tu.clientTasks.get(0);
        test.startTest();

        List<Unit_Effort__c> ue1 = PRA_DataAccessor.getMissingEffortRatiosByTask(new List<Id>{testTask.id}, System.today(), System.today().addMonths(12));
        
        test.stopTest();        

        System.assertNotEquals(null, ue1);
        System.assertNotEquals(0, ue1.size());

        System.debug('--- TEST OUT: ue1.size = ' + ue1.size() );
        System.debug('--- TEST OUT: ue1 = ' + ue1);
    }
    
    static testMethod void testGetMissingEffortByTask() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        // Zero out all effort ratios
        List<Unit_Effort__c> ueList = [select Forecast_Effort__c
                                          from Unit_Effort__c
                                          where Client_Task__r.Project__r.id = :tu.clientProject.id
                                          and Client_Task__r.Description__c != 'Migrated Worked Hours'];
        
        for (Unit_Effort__c ue : ueList) {
            ue.Forecast_Effort__c = 0;
        }
        update ueList;
        
        Client_Task__c testTask = tu.clientTasks.get(0);
        
        test.startTest();

        List<Unit_Effort__c> ue1 = PRA_DataAccessor.getMissingEffortByTask(new List<Id>{testTask.id}, System.today(), System.today().addMonths(12));
        
        test.stopTest();        

        System.assertNotEquals(null, ue1);
        System.assertNotEquals(0, ue1.size());

        System.debug('--- TEST OUT: ue1.size = ' + ue1.size() );
        System.debug('--- TEST OUT: ue1 = ' + ue1);
    }
    
    static testMethod void testgetClientTaskCountbyNullStatus() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();        
        // Zero out all effort ratios
        List<Client_Task__c> ctList = [select id,project__r.name,Project__c,status__c
                                          from Client_Task__c
                                          where Project__r.id = :tu.clientProject.id
                                          ];
        ctList[0].status__c=PRA_Constants.CU_STATUS_NEW;
        ctList[1].status__c=PRA_Constants.CU_STATUS_READY;
        update ctList;
        System.debug('---ctList--' + ctList);
        
        test.startTest();
        List<Aggregateresult> aggrList = PRA_DataAccessor.getClientTaskCountbyNullStatus(tu.clientProject.name);        
        test.stopTest();        

        //System.assertEquals(PRA_Constants.CU_STATUS_NEW,aggrList[0].status__c);
        System.assertEquals(2, aggrList.size());
    }
    
    static testMethod void testGetOpenedClientTasks() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();        

        test.startTest();
        List<Client_Task__c> ctList = PRA_DataAccessor.getOpenedClientTasks((String)tu.clientProject.id, Datetime.newInstance(Date.today().year(), Date.today().month(), 1));         
		System.assertEquals(2, ctList.size());
		test.stopTest();    	
    }
    
    static testMethod void testgetClientTaskbyProjectOpenClosedStatus() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();        
       
        
        test.startTest();
        List<Client_Task__c> ctList = PRA_DataAccessor.getClientTaskbyProjectOpenClosedStatus(tu.clientProject.id,PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS);        
        System.assertEquals(2, ctList.size());
        System.assertEquals(null, ctList[0].close_date__c);
        ctList[0].close_date__c=Date.today().addMonths(1).toStartOfMonth();
        update ctList;
        ctList = PRA_DataAccessor.getClientTaskbyProjectOpenClosedStatus(tu.clientProject.id,PRA_ClientTaskService.TO_RE_OPEN_CLIENT_TASKS);
        System.assertEquals(1, ctList.size());        
        System.assertNotEquals(null, ctList[0].close_date__c);
        test.stopTest();
    }
    
    static testMethod void testgetClientTaskbyProjectandNotInCOItems() {
         // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();        
       
        List<Client_Task__c> ctList = [select id,project__r.name,Project__c,status__c
                                          from Client_Task__c
                                          where Project__r.id = :tu.clientProject.id
                                          ];
        Set<ID> ctIDlist=new Set<ID>();
        ctIDlist.add(ctList[0].id);
        //ctIDlist.add(ctList[1].id);
        test.startTest();
         List<Client_Task__c> ctList1 = PRA_DataAccessor.getClientTaskbyProjectandNotInCOItems(tu.clientProject.id,PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS,ctIDlist);        
        System.assertEquals(1, ctList1.size());
        System.assertEquals(null, ctList1[0].close_date__c);
        ctList[1].close_date__c=Date.today().addMonths(1).toStartOfMonth();
        update ctList;
        ctList1 = PRA_DataAccessor.getClientTaskbyProjectandNotInCOItems(tu.clientProject.id,PRA_ClientTaskService.TO_RE_OPEN_CLIENT_TASKS,ctIDlist);
        System.assertEquals(1, ctList1.size());        
        System.assertNotEquals(null, ctList1[0].close_date__c);
        test.stopTest();
    }
    
    
    static testMethod void testgetCOItemswithClientTaskCloseDate() {
         // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();        
        Change_Order__c  co=tu.createChangeOrder(tu.clientProject.id);
        List<Change_Order_item__c> coList = [select id,name
                                          from Change_Order_item__c
                                          where Change_Order__r.id = :co.id
                                          ];
        
        system.debug('-------coList--------'+coList);
        test.startTest();
        List<Change_Order_Item__c> COIList = PRA_DataAccessor.getCOItemswithClientTaskCloseDate(co.id,PRA_ClientTaskService.TO_CLOSE_CLIENT_TASKS);        
        System.assertEquals(2, COIList.size());
        //System.assertEquals(null, ctList[0].close_date__c);        
        
        COIList = PRA_DataAccessor.getCOItemswithClientTaskCloseDate(co.id,PRA_ClientTaskService.TO_RE_OPEN_CLIENT_TASKS);
        System.assertEquals(0, COIList.size());        
        //System.assertNotEquals(null, COIList[0].close_date__c);
        test.stopTest();
    }
    
    static testMethod void testgetClientTaskbyProjectStatus() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();        
        // Zero out all effort ratios
        List<Client_Task__c> ctList = [select id,project__r.name,Project__c,status__c
                                          from Client_Task__c
                                          where Project__r.id = :tu.clientProject.id
                                          ];
        Client_Task__c testTask = ctList[0];
        System.debug('---testTask--' + testTask.status__c);
        
        test.startTest();
        List<Client_Task__c> ct1 = PRA_DataAccessor.getClientTaskbyProjectStatus(tu.clientProject.name,'');        
               
        
        System.assertEquals(null,ct1[0].status__c);
        System.assertEquals(2, ct1.size());
        
        
        List<Client_Task__c> ct2 = PRA_DataAccessor.getWbstaskdata(tu.clientProject.id,'');        
        test.stopTest(); 
        
        //System.assertEquals(null,ct2[0].status__c);
        System.assertEquals(2, ct2.size());
    }
    
    static testMethod void testgetCObyProjectIDandStatus() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();        
        Change_Order__c  co=tu.createChangeOrder(tu.clientProject.id);
        
        test.startTest();
        
        List<Change_Order__c> co1 = PRA_DataAccessor.getCObyProjectIDandStatus(tu.clientProject.id,'Active');        
        System.assertEquals('Active',co1[0].status__c);
        //System.assertEquals(2, ct1.size());
        test.stopTest(); 
    }
    
    static testMethod void testupsertClientTaskList() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();        
        // Zero out all effort ratios
        List<Client_Task__c> ctList = [select id,project__r.name,Project__c,status__c
                                          from Client_Task__c
                                          where Project__r.id = :tu.clientProject.id
                                          ];
        ctList[0].status__c=PRA_Constants.CU_STATUS_NEW;
        ctList[1].status__c=PRA_Constants.CU_STATUS_READY;
        
        test.startTest();
        String ErrorMsg = PRA_DataAccessor.upsertClientTaskList(ctList);        
        test.stopTest();        

        System.assertEquals(null,ErrorMsg);
        //System.assertEquals(2, ct1.size());
    }
    
    static testMethod void testgetClientTaskbyctIDs() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();        
        // Zero out all effort ratios
        List<Client_Task__c> ctList = [select id,project__r.name,Project__c,status__c
                                          from Client_Task__c
                                          where Project__r.id = :tu.clientProject.id
                                          ];
        System.assertEquals(2,ctList.size());
        List<string> ctidsList =new List<string>();
        ctidsList.add(ctList[0].id);
        ctidsList.add(ctList[1].id);
        test.startTest();
        ctList = PRA_DataAccessor.getClientTaskbyctIDs(ctidsList);        
        test.stopTest();        

        System.assertEquals(2,ctList.size());
        
    }
    
    //tetsing getUnitEffortByTaskIDsandMonth and upsertUnitEffortList methods at once
    static testMethod void testgetUnitEffortByTaskIDsandMonth() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();        
        // Zero out all effort ratios
        List<Client_Task__c> ctList = [select id,project__r.name,Project__c,status__c
                                          from Client_Task__c
                                          where Project__r.id = :tu.clientProject.id
                                          ];
        System.assertEquals(2,ctList.size());
        List<string> ctidsList =new List<string>();
        ctidsList.add(ctList[0].id);
        ctidsList.add(ctList[1].id);
        test.startTest();
        List<Unit_Effort__c> ue= PRA_DataAccessor.getUnitEffortByTaskIDsandMonth(ctidsList,Date.today().addMonths(1).toStartOfMonth());        
        String ErrorMsg=PRA_DataAccessor.upsertUnitEffortList(ue);        
        test.stopTest();        

        System.assertEquals(2,ctList.size());
        
    }
    
    //tetsing getWorkedUnitsforOpenUnits
    static testMethod void testgetWorkedUnitsforOpenUnits() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();        
        // Zero out all effort ratios
        List<Client_Task__c> ctList = [select id,project__r.name,Project__c,status__c
                                          from Client_Task__c
                                          where Project__r.id = :tu.clientProject.id
                                          ];
        System.assertEquals(2,ctList.size());
        List<Date> Datelist=new List<Date>();
        Datelist.add(Date.today().addMonths(-1).toStartOfMonth());
        
        test.startTest();
        List<Unit_Effort__c> ue= PRA_DataAccessor.getWorkedUnitsforOpenUnits(tu.clientProject.id,Datelist,true);        
        System.assertEquals(0,ue.size());     
        test.stopTest();
    }
    
    static testMethod void testgetWBSprojectbyProjectName() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll(); 
        test.startTest();
        List<wbs_project__c> wbslist = PRA_DataAccessor.getWBSprojectbyProjectName(tu.clientProject.Name);        
        test.stopTest();
        System.assertEquals(1, wbslist.size());
    }
    
    static testMethod void testgetClientProjectDetailsbyProjectName() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll(); 
        test.startTest();        
        client_project__c  cp = PRA_DataAccessor.getClientProjectDetailsbyProjectName(tu.clientProject.Name);        
        test.stopTest();
        System.assertEquals(tu.clientProject.Name, cp.Name);
    }
    
    static testMethod void testgetClientProjectDetailsbyProjectID() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll(); 
        test.startTest();        
        client_project__c  cp = PRA_DataAccessor.getClientProjectDetailsbyProjectID(tu.clientProject.ID);        
        test.stopTest();
        System.assertEquals(tu.clientProject.ID, cp.ID);
    }
    
    static testMethod void testupsertWBSProject() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll(); 
        test.startTest();
        List<wbs_project__c> wbslist = PRA_DataAccessor.getWBSprojectbyProjectName(tu.clientProject.Name);
        String ErrorMsg  = PRA_DataAccessor.upsertWBSProject(wbslist);        
        test.stopTest();
        System.assertEquals(null,ErrorMsg);
    }
    
    static testMethod void testgetWBSprojectbyID() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll(); 
        test.startTest();
        List<wbs_project__c> wbslist = PRA_DataAccessor.getWBSprojectbyID(tu.wbsProject.id);                
        test.stopTest();
        System.assertEquals(1, wbslist.size());
    }
    
    static testMethod void testWbsProjectByProjID() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll(); 
        test.startTest();
        List<wbs_project__c> wbslist = PRA_DataAccessor.getWbsProjectByProjID(tu.clientProject.id);                
        test.stopTest();
        System.assertEquals(1, wbslist.size());
    }
    
    static testMethod void testgetProfilebyId() {
        
        test.startTest();
        profile p = PRA_DataAccessor.getProfilebyId(UserInfo.getProfileId());                
        test.stopTest();
        System.assertEquals(UserInfo.getProfileId(), p.id);
    }    
    
    static testMethod void testgetPermissionSetAssignmentbyUserIdandProfiles() {
        
        test.startTest();
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        User testUser = new User(alias='TestFAUr',Email='testFAPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='FA', 
                            LocaleSidKey='en_US', ProfileId=p.id, TimeZoneSidKey='America/New_York', Username='Test.FA@praintl.com.unitTestPMConsole');
        insert testUser;
        
        PermissionSet ps=new PermissionSet();
        ps.name='FATestPRAUnitTest';
        ps.label='FATestPRAUnitTest';
        insert ps;
        
        PermissionSetAssignment psa =new PermissionSetAssignment();
        psa.AssigneeId= testUser.id; 
        psa.PermissionSetId = ps.id;
        insert psa;
        
        set<string> pset=new set<string>();
        pset.add('FATestPRAUnitTest');
        
        System.runAs(testUser){
           List<PermissionSetAssignment> paList = PRA_DataAccessor.getPermissionSetAssignmentbyUserIdandProfiles(testUser.id,pset);  
          System.assertEquals(ps.name, paList[0].PermissionSet.Name);  
        }              
        test.stopTest();        
    }
    
    static testMethod void testgetUserListbyIds() {
        
        test.startTest();
        Set<String> UserIDList = new Set<String>();
        UserIDList.add(String.valueof(UserInfo.getUserId()));    
        List<User> userlist = PRA_DataAccessor.getUserListbyIds(UserIDList);
        System.assertEquals(userlist.size(), 1);
        test.stopTest();       
    } 
    
    static testMethod void testgetAdjtoRCVbyGroupRollupbyProject() {
        
        test.startTest();
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();         
        Map<String,Decimal> AdjRCVMap = PRA_DataAccessor.getAdjtoRCVbyGroupRollupbyProject(tu.clientProject.Name);
        System.assertEquals(AdjRCVMap.get('Total'), 0.00);
        test.stopTest();       
    }    

}