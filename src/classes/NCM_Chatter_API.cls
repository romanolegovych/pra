/**
 * @description	The API that implements Chatter functionalities
 * @author		Dimitrios Sgourdos
 * @date		Created: 09-Sep-2015
 */
public class NCM_Chatter_API {
	
	/**
	 * @description	Post a feed under the given object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sept-2015
	 * @param		objectToPostToId    The object to post to
	 * @param		postText            The text of the post
	*/
	static public void postFeedItem(Id objectToPostToId, String postText) {
		NCM_SrvLayer_Chatter.postFeedItem(objectToPostToId, postText);
	}
	
	
	/**
	 * @description	Post the feeds under the given corresponding objects
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sept-2015
	 * @param		source    The wrapper data that holds the combination of feed and related object
	*/
	static public void postFeedItem(List<NCM_Chatter_API_DataTypes.ChatterPostWrapper> source) {
		NCM_SrvLayer_Chatter.postFeedItem(source);
	}
	
	
	/**
	 * @description	Create a comment under a feed post
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sept-2015
	 * @param		postComment       The text of the comment
	 * @param		feedToPostToId    The feed post that the comment will be related with
	*/
	static public void postComment(Id feedToPostToId, String postComment) {
		NCM_SrvLayer_Chatter.postComment(feedToPostToId, postComment);
	}
	
	
	/**
	 * @description	Create comments under the given corresponding feed posts
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sept-2015
	 * @param		source    The wrapper data that holds the combination of comment and related feed
	*/
	static public void postComment(List<NCM_Chatter_API_DataTypes.ChatterPostWrapper> source) {
		NCM_SrvLayer_Chatter.postComment(source);
	}
	
	
	/**
	 * @description	Check if a user is subscribed to a feed
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sept-2015
	 * @param		objectFollowedId    The object to check if the user is subscribed to
	 * @param		userId              The user to check if he is subscribed to the given feed
	 * @return		Flag if a user is subscribed to a feed or not
	*/
	static public Boolean isUserSubscribedToFeed(Id objectToFollowId, Id userId) {
		return NCM_SrvLayer_Chatter.isUserSubscribedToFeed(objectToFollowId, userId);
	}
	
	
	
	/**
	 * @description	Get the feed posts that are related with the given parent object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sept-2015
	 * @param		feedParentId    The id of the parent object
	 * @return		The posts related with the given object
	*/
	static public List<FeedItem> getFeedItems(Id feedParentId) {
		return NCM_SrvLayer_Chatter.getFeedItems(feedParentId);
	}
	
	
	/**
	 * @description	Get the posts that are related with the given parent object and topic
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sept-2015
	 * @param		feedParentId    	The id of the parent object
	 * @param		relatedTopicName    The related topic of the posts
	 * @return		The posts related with the given object and topic
	*/
	static public List<FeedItem> getFeedItems(Id feedParentId, String relatedTopicName) {
		return NCM_SrvLayer_Chatter.getFeedItems(feedParentId, relatedTopicName);
	}
	
	
	/**
	 * @description	Unsubscribe the given user from the given feed
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sept-2015
	 * @param		objectFollowedId    The object the user is unsubscribed from
	 * @param		userId              The user that will be unsubscribed from the feed
	*/
	static public void unsubscribeUserFromFeed(Id objectFollowedId, Id userId) {
		NCM_SrvLayer_Chatter.unsubscribeUserFromFeed(objectFollowedId, userId);
	}
	
	
	/**
	 * @description	Unsubscribe the given combinations of users and feeds
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sept-2015
	 * @param		source    The wrapper data that keeps which subscriptions must be deleted
	*/
	static public void unsubscribeUserFromFeed(List<NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper> source) {
		NCM_SrvLayer_Chatter.unsubscribeUserFromFeed(source);
	}
	
	
	/**
	 * @description	Subscribe given user to the given feed
	 * @author 		Dimitrios Sgourdos
	 * @date		Created: 09-Sept-2015
	 * @param		objectToFollowId    The object the user is subscribed to
	 * @param		userId              The user that will be subscribed to the feed
	*/
	static public void subscribeUserToFeed(Id objectToFollowId, Id userId) {
		NCM_SrvLayer_Chatter.subscribeUserToFeed(objectToFollowId, userId);
	}
	
	
	/**
	 * @description	Subscribe the given combinations of users and feeds
	 * @author 		Dimitrios Sgourdos
	 * @date		Created: 09-Sept-2015
	 * @param		source    The wrapper data that keeps which subscriptions must be inserted
	*/
	static public void subscribeUserToFeed(List<NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper> source) {
		NCM_SrvLayer_Chatter.subscribeUserToFeed(source);
	}
}