public  class DWF_Util {

	public static list<selectoption> getCompanyNames(list<DWF_FilterService.companyVO> companyVOs) {
		list<selectoption> options = new list<selectoption>();
		options.add(new selectoption('', '----- Select Company -----'));
		for (DWF_FilterService.companyVO compVO : companyVOs) {
			options.add(new selectoption(String.valueOf(compVO.companyNo), compVO.companyName.trim()));
		}
		return options;
	}
	
	public static list<selectoption> getBusinessUnits(list<DWF_FilterService.businessUnitVO> businessUnitVOs) {
		list<selectoption> options = new list<selectoption>();
		options.add(new selectoption('', '----- Select Business Unit -----'));
		if (businessUnitVOs != null) {
			for (DWF_FilterService.businessUnitVO businessUnitVO : businessUnitVOs) {
				options.add(new selectoption(businessUnitVO.businessUnitName, businessUnitVO.businessUnitName));
			}
		}
		return options; 
	}
	
	public static list<selectoption> getSystemNames() {
		list<DWF_System__c> systems = [select Id, Name from DWF_System__c];
		list<selectoption> options = new list<selectoption>();
		options.add(new selectoption('', '----- Select System -----'));
		if (systems != null && systems.size() > 0) {
			for (DWF_System__c dwfSystem : systems) {
				options.add(new selectoption(dwfSystem.Id, dwfSystem.Name));
			}
		}
		return options;
	}
	
	public static set<String> getCurrentWLsForSystem(Id targetSystemId) {
		set<String> whiteListKeys = new set<String>();
		list<DWF_Whitelist__c> whiteLists = [select Company_Name__c, Business_Unit_Name__c from DWF_Whitelist__c where DWF_System__c = :targetSystemId];
		if (whiteLists != null && whiteLists.size() > 0) {
			for (DWF_Whitelist__c wl : whiteLists) {
				if (!whiteListKeys.contains(wl.Company_Name__c + '_' + wl.Business_Unit_Name__c)) {
					whiteListKeys.add(wl.Company_Name__c + '_' + wl.Business_Unit_Name__c);
				}
			}
		}
		return whiteListKeys;
	}
	
	public static set<String> getCurrentEmployeeBLsForSystem(Id targetSystemId) {
		set<String> employeeIds = new set<String>();
		list<DWF_Blacklist__c> blackLists = [select Employee_Id__c from DWF_Blacklist__c where DWF_System__c = :targetSystemId];
		if (blackLists != null && blackLists.size() > 0) {
			for (DWF_Blacklist__c bl : blackLists) {
				if (!employeeIds.contains(bl.Employee_Id__c)) {
					employeeIds.add(bl.Employee_Id__c);
				}
			}
		}
		return employeeIds;
	}
}