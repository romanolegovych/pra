/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AA_BufCodeDataAccessorTest{

    static testMethod void getBuCodesTest() {
        // TO DO: implement unit test
        List<Business_Unit__c> buList = AA_BUFCodeDataAccessor.getBuCodes();
        
        System.assertNotEquals(buList, null);
    }
    static testMethod void getFCodesTest() {
        // TO DO: implement unit test
        List<Function_Code__c> fCodes = AA_BUFCodeDataAccessor.getFCodes();
        System.assertNotEquals(fCodes, null);
    }
    static testMethod void getBufCodeDetailsTest() {
        initTestData ();
        // TO DO: implement unit test
        BUF_Code__c bufCode = AA_BUFCodeDataAccessor.getBufCodeDetails('BU1F1');
        System.assertEquals(bufCode.Name, 'BU1F1');
    }
    
    static testMethod void getBufCodeNameTest() {
        initTestData ();
        // TO DO: implement unit test
        List<BUF_Code__c> bufCodeList = AA_BUFCodeDataAccessor.getBufCodeName('BU1F1');
        System.assertEquals(null,null);
    }
    
    static testMethod void insertBufCodeTest() {
        BUF_Code__c bufCode = new BUF_Code__c();
        bufCode.Name = 'BU3F3'; 
        AA_BUFCodeDataAccessor.insertBufCode(bufCode);
        System.assertEquals(AA_BUFCodeDataAccessor.getBufCodeDetails('BU3F3').Name, 'BU3F3');
    }
    
    /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }   
}