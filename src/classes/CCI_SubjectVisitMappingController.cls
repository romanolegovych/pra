/**
 * Controller for the CCI_SubjectVisitMapping UI
 * Provides visibility to CCI MDM data and allows for creation/update of mappings
 *
 * Author: AllenAndrew
 */
public class CCI_SubjectVisitMappingController {

	// String inputs/displays
	public String praStudyId {get;set;}
	public String protocolText {get;set;}
	public String keySvtId {get;set;} 
	public Boolean dummyBoolean {get;set;}   
	// Display objects/maps
	public list<MdmSvtService.svtVO> svtVOs {get;set;}
	public MdmProtocolService.protocolVO protocolVO {get;set;}
	public map<String, SvtEcrfMappingWrapper> svtEcrfWrapper {get;set;}
	private map<String, MdmSvtService.svtItemVO> svtItemVOMap {get;set;}
	private map<String, MdmSvtService.ecrfItemVO> ecrfItemVOMap {get;set;}
	private map<String, MdmSvtService.ecrfFormVO> ecrfFormVOMap {get;set;}
	// Rendering
	public map<String, Boolean> shouldRenderMapping {get;set;}

	public CCI_SubjectVisitMappingController() {   
		init();
	}

	// Initialization method, currently empty kept incase we need it later 
	private void init() {
		svtEcrfWrapper = new map<String, SvtEcrfMappingWrapper>();
		shouldRenderMapping = new map<String, Boolean>();
	}

	// Populate the pra study identifiers from the selected protocol
	public list<selectoption> getClientProjectsFromProtocol() {
		transient List<selectoption> options = new List<SelectOption>(); 
		if (protocolVO != null && protocolVO.studyVOs != null) {
			list<MdmProtocolService.studyVO> studyVOs = protocolVO.studyVOs;
			options.add(new selectoption('--- Select a Study Identifier ---', '--- Select a Study Identifier ---'));
			for (MdmProtocolService.studyVO cp : studyVOs) {
				options.add(new selectoption(cp.praId, cp.praId));
			}
		} else {
			options.add(new selectoption('',''));
		}
		return options;
	}

	// Get SvtItem selectOptions for a given SVT systemId
	public list<selectoption> populateSvtItems(String svtId, set<String> procedureSet) {
		transient List<selectoption> options = new List<SelectOption>(); 
		if (svtId != null) {
			list<MdmSvtService.svtItemVO> svtItems = svtEcrfWrapper.get(svtId).svtItemVOs;
			// Add logic for minimum index
			Long minIndex;
			Long maxIndex;
			for (MdmSvtService.svtItemVO svtItem : svtItems) {
				if (minIndex == null) {
					minIndex = svtItem.sequenceNumber;
					maxIndex = svtItem.sequenceNumber;
				} else {
					if (svtItem.sequenceNumber < minIndex) {
						minIndex = svtItem.sequenceNumber;
					}
					if (svtItem.sequenceNumber > maxIndex) {
						maxIndex = svtItem.sequenceNumber;
					}
				}
			}
			for (Long i = minIndex; i <= maxIndex; i++) {
				for (MdmSvtService.svtItemVO svtItem : svtItems) {					
					if (svtItem.sequenceNumber == i && !svtItem.visitType.equals('Re-screening')) {
						if (procedureSet.contains(svtItem.visitName)) {
							options.add(new selectoption(svtItem.systemId, '* ' + svtItem.visitName));
						} else {
							options.add(new selectoption(svtItem.systemId, svtItem.visitName));
						}
						
					}
				}
			}
		} else {
			options.add(new selectoption('',''));
		}
		system.debug('--------------------options----------------------' + options);
		return options;
	}

	// Get SvtItem selectOptions for a given SVT systemId
	public list<selectoption> populateSvtUsvItems(String svtId) {
		transient List<selectoption> options = new List<SelectOption>(); 
		if (svtId != null) {
			list<MdmSvtService.svtItemVO> svtItems = svtEcrfWrapper.get(svtId).svtItemVOs;
			// Add logic for minimum index
			Long minIndex;
			Long maxIndex;
			for (MdmSvtService.svtItemVO svtItem : svtItems) {
				if (minIndex == null) {
					minIndex = svtItem.sequenceNumber;
					maxIndex = svtItem.sequenceNumber;
				} else {
					if (svtItem.sequenceNumber < minIndex) {
						minIndex = svtItem.sequenceNumber;
					}
					if (svtItem.sequenceNumber > maxIndex) {
						maxIndex = svtItem.sequenceNumber;
					}
				}
			}
			for (Long i = minIndex; i <= maxIndex; i++) {
				for (MdmSvtService.svtItemVO svtItem : svtItems) {
					if (svtItem.sequenceNumber == i && svtItem.visitType.equals('Event') && !svtItem.visitName.equals('Unscheduled Visit')) {
						//if (procedureSet.contains(svtItem.visitName)) {
							//options.add(new selectoption(svtItem.systemId, '* ' + svtItem.visitName));
						//} else {
							options.add(new selectoption(svtItem.systemId, svtItem.visitName));
						//}
					}
				}
			}
		} else {
			options.add(new selectoption('',''));
		}
		system.debug('--------------------options----------------------' + options);
		return options;
	}

	// Get EcrfItem selectOptions for a given SVT systemId
	public list<selectoption> populateEcrfItems(String svtId, set<String> eventSet) {
		transient List<selectoption> options = new List<SelectOption>();
		if (svtId != null) {
			list<MdmSvtService.ecrfItemVO> ecrfItems = svtEcrfWrapper.get(svtId).ecrfItemVOs;
			for (MdmSvtService.ecrfItemVO item : ecrfItems) {
				if (eventSet.contains(item.eventLabel)) {
					options.add(new selectoption(item.eventLabel, '* ' + item.eventLabel));
				} else {
					options.add(new selectoption(item.eventLabel, item.eventLabel));
				}
			}
		} else {
			options.add(new selectoption('',''));
		}
		return options;
	}
	
	public list<selectoption> populateEcrfForms(String svtId) {
		transient list<selectoption> options = new list<selectoption>();
		if (svtId != null) {
			list<MdmSvtService.ecrfFormVO> ecrfForms = svtEcrfWrapper.get(svtId).ecrfFormVOs;
			for (MdmSvtService.ecrfFormVO form : ecrfForms) {
				options.add(new selectoption(form.formLabel, form.formLabel));
			}
		} else {
			options.add(new selectoption('',''));
		}
		return options;
	}

	// Clear all viewstate, as well as private variables
	public void resetAllData() {
		praStudyId = null;
		protocolText = null;
		keySvtId = null;
		dummyBoolean = false;
		svtVOs = null;
		protocolVO = null;
		svtEcrfWrapper.clear(); 
		shouldRenderMapping.clear();
	}

	// Method to find a protocol based on the protocolText provided
	public void searchProtocol() {
		MdmProtocolService.protocolResponse response = MdmProtocolServiceWrapper.getProtocolVOByClientProtNum(String.escapeSingleQuotes(protocolText));
		protocolVO = response.protocolVO;
	}

	/**  
	 * Method to get all data elements needed to create new and display existing mappings
	 * We will pull the following elements:
	 *  - All SVTs for the selected Protocol/Study Identifier
	 *  - SvtItems for each SVT
	 *  - EcrfItems for each SVT
	 *  - EcrfMappings for each SVT
	 */ 
    public void searchAllData() {
        system.debug('--------------------------praStudyId-----------------------------' + praStudyId);
        if (protocolVO != null && praStudyId != null) {
            list<MdmSvtService.ecrfItemVO> ecrfItemVOs = new list<MdmSvtService.ecrfItemVO>();
            list<MdmSvtService.ecrfFormVO> ecrfFormVOs = new list<MdmSvtService.ecrfFormVO>();
            list<MdmSvtService.ecrfMappingVO> visitMappingVOs = new list<MdmSvtService.ecrfMappingVO>();
            list<MdmSvtService.ecrfFormMappingVO> formMappingVOs = new list<MdmSvtService.ecrfFormMappingVO>();
            svtItemVOMap = new map<String, MdmSvtService.svtItemVO>();
            ecrfItemVOMap = new map<String, MdmSvtService.ecrfItemVO>();
            ecrfFormVOMap = new map<String, MdmSvtService.ecrfFormVO>();
            svtEcrfWrapper = new map<String, SvtEcrfMappingWrapper>();
            shouldRenderMapping = new map<String, Boolean>();
            set<String> svtItemIds = new set<String>();
            map<String, set<String>> svtProcedureMap = new Map<String, set<String>>();
            map<String, set<String>> edcEventMap = new Map<String, set<String>>();
            map<String, set<String>> svtFormProcedureMap = new Map<String, set<String>>();
            map<String, set<String>> edcFormMap = new Map<String, set<String>>();
            map<String, Boolean> proceduresMap = new map<String, Boolean>();
            
            // Invoke webservice to get all necessary data
            MdmSvtService.svtEcrfUsvFormMappingResponse response = MdmSvtServiceWrapper.getSvtsEcrfsUsvFormsMappingsFromProtocolPraId(protocolVO, praStudyId);
            
            // Set the elements based on the response
            if (response != null) {
                svtVOs = response.svts;
                ecrfItemVOs = response.ecrfItems;
                ecrfFormVOs = response.usvForms;
                visitMappingVOs = response.visitMappings;
                formMappingVOs = response.formMappings;
            }

            // Iterate through svts if not null
            if (svtVOs != null) {
                for (MdmSvtService.svtVO svt : svtVOs) {
                    system.debug('-----------svt-------------' + svt.systemId);
                    shouldRenderMapping.put(svt.systemId, false);
                    svtEcrfWrapper.put(svt.systemId, new SvtEcrfMappingWrapper());
                    svtProcedureMap.put(svt.systemId, new set<String>());
                    edcEventMap.put(svt.systemId, new set<String>());

                    // Iterate through svtItems if not null
                    // Update svtItem map and svtItemVOs for wrapper object
                    for (MdmSvtService.svtItemVO svtItem : svt.svtItems) {
                        svtItemVOMap.put(svtItem.systemId, svtItem);
                        svtEcrfWrapper.get(svt.systemId).svtItemVOs.add(svtItem);
                        if ((!svtItem.visitType.equals('Event') || 
                        	svtItem.visitType.equals('Event') && svtItem.visitName.equals('Unscheduled Visit'))) {
                        	proceduresMap.put(svt.systemId + '_' + svtItem.systemId, false);
                        }
                    }
                    
                    // Iterate through ecrfItems if not null
                    // Update ecrfItem map and ecrfItemVOs for wrapper object 
                    if (ecrfItemVOs != null) {
                        for (MdmSvtService.ecrfItemVO ecrfItem : ecrfItemVOs) {
                            ecrfItemVOMap.put(ecrfItem.eventLabel, ecrfItem);
                            svtEcrfWrapper.get(svt.systemId).ecrfItemVOs.add(ecrfItem);
                        }
                    }
                    
                    // Iterate through ecrfForms if not null
                    // Update ecrfForm map and ecrfFormVOs for wrapper object
                    if (ecrfFormVOs != null) {
                    	for (MdmSvtService.ecrfFormVO ecrfForm : ecrfFormVOs) {
                    		ecrfFormVOMap.put(ecrfForm.formLabel, ecrfForm);
                    		svtEcrfWrapper.get(svt.systemId).ecrfFormVOs.add(ecrfForm);
                    	}
                    }
                }
            }

            // Iterate through mappings if not null
            // Add the mapping to the MappingWrapper wrapper object
            if (visitMappingVOs != null) {
                for (MdmSvtService.ecrfMappingVO mapping : visitMappingVOs) {
                    svtEcrfWrapper.get(mapping.svtItemVO.svtSystemId).visitMappings.add(new EcrfMappingWrapper(false, false, mapping));
                    svtEcrfWrapper.get(mapping.svtItemVO.svtSystemId).visitMappings.sort();
                    if (!shouldRenderMapping.get(mapping.svtItemVO.svtSystemId)) {
                        shouldRenderMapping.put(mapping.svtItemVO.svtSystemId, true);
                    }
                    if (!mapping.mapped) {
                        svtEcrfWrapper.get(mapping.svtItemVO.svtSystemId).isSvtMapped = false;
                    }
                    proceduresMap.put(mapping.svtItemVO.svtSystemId + '_' + mapping.svtItemVO.systemId, true);
                    if (!svtProcedureMap.get(mapping.svtItemVO.svtSystemId).contains(mapping.svtItemVO.visitName)) {
                    	svtProcedureMap.get(mapping.svtItemVO.svtSystemId).add(mapping.svtItemVO.visitName);
                    }
                    if (!edcEventMap.get(mapping.svtItemVO.svtSystemId).contains(mapping.ecrfItemVO.eventLabel)) {
                    	edcEventMap.get(mapping.svtItemVO.svtSystemId).add(mapping.ecrfItemVO.eventLabel);
                    }
                }
            }
            
            if (formMappingVOs != null) {
            	for (MdmSvtService.ecrfFormMappingVO mapping : formMappingVOs) {
                    svtEcrfWrapper.get(mapping.svtItemVO.svtSystemId).formMappings.add(new FormMappingWrapper(false, false, mapping));
                    svtEcrfWrapper.get(mapping.svtItemVO.svtSystemId).formMappings.sort();
                    if (!shouldRenderMapping.get(mapping.svtItemVO.svtSystemId)) {
                        shouldRenderMapping.put(mapping.svtItemVO.svtSystemId, true);
                    }
                    if (!mapping.mapped) {
                        svtEcrfWrapper.get(mapping.svtItemVO.svtSystemId).isSvtFormsMapped = false;
                    }
                }
            }

            for (String key : proceduresMap.keySet()) {
                if (!proceduresMap.get(key)) {
                    list<String> idList = key.split('_');
                    svtEcrfWrapper.get(idList.get(0)).areAllProceduresMapped = false;
                }
            }

            for (String key : svtEcrfWrapper.keySet()) {
                // Set the svtItem and ecrfItem options for the dropdown lists on each svt tab
                svtEcrfWrapper.get(key).svtItemOptions = populateSvtItems(key, svtProcedureMap.get(key));
                svtEcrfWrapper.get(key).svtUsvItemOptions = populateSvtUsvItems(key);
                svtEcrfWrapper.get(key).ecrfItemOptions = populateEcrfItems(key, edcEventMap.get(key));
                svtEcrfWrapper.get(key).ecrfFormOptions = populateEcrfForms(key);
                if (svtEcrfWrapper.get(key).visitMappings.size() == 0) {
                    svtEcrfWrapper.get(key).isSvtMapped = false;
                }
                if (svtEcrfWrapper.get(key).formMappings.size() == 0) {
                	svtEcrfWrapper.get(key).isSvtFormsMapped = false;
                }
            }

            system.debug('----------------svtVOs-----------------' + svtVOs);
            system.debug('----------------ecrfItemVOs-----------------' + ecrfItemVOs);
            system.debug('----------------ecrfFormVOs-----------------' + ecrfFormVOs);
            system.debug('----------------visitMappingVOs-----------------' + visitMappingVOs);
            system.debug('----------------formMappingVOs-----------------' + formMappingVOs);
            system.debug('----------------map-----------------' + svtEcrfWrapper);                
        }
    }

    /**
     * Method to add a new mapping row
     * Takes the selected svtItem and ecrfItem and updates the mapping list
     */
    public void addScheduledMappingData() {
        String selectedSvtItem = svtEcrfWrapper.get(keySvtId).selectedSVT;
        String selectedEcrfItem = svtEcrfWrapper.get(keySvtId).selectedECRF;
        system.debug('-------------------------svt----------------------------' + keySvtId);
        system.debug('------------------------svtItem-------------------------' + selectedSvtItem);
        system.debug('------------------------ecrfItem-------------------------' + selectedEcrfItem);
        MdmSvtService.svtItemVO svtItem = svtItemVOMap.get(selectedSvtItem);
        MdmSvtService.ecrfItemVO ecrfItem = ecrfItemVOMap.get(selectedEcrfItem);
        MdmSvtService.ecrfMappingVO newMapping = new MdmSvtService.ecrfMappingVO();
        list<EcrfMappingWrapper> mappings = svtEcrfWrapper.get(keySvtId).visitMappings;

        Boolean isNew = true;
        Boolean visitExists = false;
        for (EcrfMappingWrapper mapping : mappings) {
            // Check if mapping already exists
            if (mapping.ecrfMappingVO.svtItemVO.systemId.equals(svtItem.systemId) && mapping.ecrfMappingVO.ecrfItemVO.eventName.equals(ecrfItem.eventName)) {
                isNew = !isNew;
            }
            // Check if mapping for visit type exists, if true set boolean value
            if (!visitExists && mapping.ecrfMappingVO.svtItemVO.systemId.equals(svtItem.systemId)) {
                visitExists = !visitExists;
            }
        }

        // Add the mapping if it doesn't exist, ignore otherwise
        if (isNew) {
            system.debug('------------------------svtItemId-------------------------' + svtItem.systemId);
            system.debug('------------------------ecrfItemNam-------------------------' + ecrfItem.eventName);
            newMapping.svtItemVO = svtItem;
            newMapping.ecrfItemVO = ecrfItem;
            newMapping.mapped = false;
            if (!visitExists) {
                newMapping.primary = isNew;
            }
            svtEcrfWrapper.get(keySvtId).visitMappings.add(new EcrfMappingWrapper(false, true, newMapping));
            svtEcrfWrapper.get(keySvtId).visitMappings.sort();
        }

        processScheduledMappingData(mappings);
    }
    
    public void addUnscheduledMappingData() {
    	String selectedSvtItem = svtEcrfWrapper.get(keySvtId).selectedUsvSvtItem;
        String selectedEcrfForm = svtEcrfWrapper.get(keySvtId).selectedEcrfForm;
        list<String> selectedEcrfForms = svtEcrfWrapper.get(keySvtId).selectedEcrfForms;
        set<String> ecrfFormGrouping = new set<String>();
        map<String, set<String>> existingFormMaps = new map<String, set<String>>(); 
        system.debug('-------------------------svt----------------------------' + keySvtId);
        system.debug('------------------------svtItem-------------------------' + selectedSvtItem);
        system.debug('------------------------ecrfForm-------------------------' + selectedEcrfForm);
        system.debug('------------------------ecrfForms-------------------------' + selectedEcrfForms);
        MdmSvtService.svtItemVO svtItem = svtItemVOMap.get(selectedSvtItem);
        list<FormMappingWrapper> mappings = svtEcrfWrapper.get(keySvtId).formMappings;
        
        existingFormMaps.put(svtItem.systemId, new set<String>());
        for (String form : selectedEcrfForms) {
        	ecrfFormGrouping.add(form);
        	existingFormMaps.get(svtItem.systemId).add(form);
        }

        Boolean isNew = true;
        // create map for all existing
        for (FormMappingWrapper mapping : mappings) {
        	if (mapping.ecrfFormMappingVO.svtItemVO.systemId.equals(svtItem.systemId)) {
        		isNew = false;
        	}
        	if (!existingFormMaps.containsKey(mapping.ecrfFormMappingVO.svtItemVO.systemId)) {
        		existingFormMaps.put(mapping.ecrfFormMappingVO.svtItemVO.systemId, new set<String>());
        		if (!existingFormMaps.get(mapping.ecrfFormMappingVO.svtItemVO.systemId).contains(mapping.ecrfFormMappingVO.ecrfFormVO.formLabel))
        			existingFormMaps.get(mapping.ecrfFormMappingVO.svtItemVO.systemId).add(mapping.ecrfFormMappingVO.ecrfFormVO.formLabel);
        	} else {
        		if (!existingFormMaps.get(mapping.ecrfFormMappingVO.svtItemVO.systemId).contains(mapping.ecrfFormMappingVO.ecrfFormVO.formLabel))
        			existingFormMaps.get(mapping.ecrfFormMappingVO.svtItemVO.systemId).add(mapping.ecrfFormMappingVO.ecrfFormVO.formLabel);
        	}
        }
        
        Boolean invalidMapping = false;
        set<String> currentFormGrouping = existingFormMaps.get(svtItem.systemId);
        for (String key : existingFormMaps.keySet()) {
        	if (!key.equals(svtItem.systemId)) {
	        	set<String> forms = existingFormMaps.get(key);        	
	        	system.debug('---------------------set 1-----------------------' + ecrfFormGrouping);
	        	system.debug('---------------------set 2-----------------------' + forms);
	        	system.debug('---------------------set equal?-----------------------' + ecrfFormGrouping.equals(forms));
	        	if (currentFormGrouping.equals(forms)) {
	        		invalidMapping = true;
	        	}
        	}
        }

        // Add the mapping if it doesn't exist, ignore otherwise
        if (!invalidMapping) {
            system.debug('------------------------svtItemId-------------------------' + svtItem.systemId);
            system.debug('------------------------ecrfForms-------------------------' + ecrfFormGrouping);
            
            if (isNew) {
	            for (String form : ecrfFormGrouping) {
	        		MdmSvtService.ecrfFormMappingVO newMapping = new MdmSvtService.ecrfFormMappingVO();
	        		MdmSvtService.ecrfFormVO ecrfForm = ecrfFormVOMap.get(form);
		            newMapping.svtItemVO = svtItem;
		            newMapping.ecrfFormVO = ecrfForm;
	        		newMapping.mapped = false;
	            	svtEcrfWrapper.get(keySvtId).formMappings.add(new FormMappingWrapper(false, true, newMapping));
	            }           
            } else {
            	list<FormMappingWrapper> existingWrappers = svtEcrfWrapper.get(keySvtId).formMappings;
            	set<String> existingForms = new set<String>();
            	for (Integer index = 0; index < existingWrappers.size(); index++) {
            		if (existingWrappers.get(index).ecrfFormMappingVO.svtItemVO.systemId.equals(svtItem.systemId)) {
            			existingForms.add(existingWrappers.get(index).ecrfFormMappingVO.ecrfFormVO.formLabel);
            		}
            	}
            	for (String form : ecrfFormGrouping) {
	        		MdmSvtService.ecrfFormMappingVO newMapping = new MdmSvtService.ecrfFormMappingVO();
            		if (!existingForms.contains(form)) {
            			MdmSvtService.ecrfFormVO ecrfForm = ecrfFormVOMap.get(form);
			            newMapping.svtItemVO = svtItem;
			            newMapping.ecrfFormVO = ecrfForm;
		        		newMapping.mapped = false;
	            		svtEcrfWrapper.get(keySvtId).formMappings.add(new FormMappingWrapper(false, true, newMapping));            			
            		}
            	}
            } 
            	
            svtEcrfWrapper.get(keySvtId).formMappings.sort();
        } else {
        	// exception handling, throw error to the page
        	Apexpages.getMessages().clear();
        	Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Duplicate form mapping groups cannot be created.  Please update the selected forms and try again.'));
        }
    }

    /**
     * Method to remove mapping row(s) from the mapping list
     */
    public void removeScheduledMappingData() {
        system.debug('-----------------------svtId------------------------' + keySvtId);
        list<EcrfMappingWrapper> mappings = svtEcrfWrapper.get(keySvtId).visitMappings;
        list<MdmSvtService.ecrfMappingVO> objectsToRemove = new list<MdmSvtService.ecrfMappingVO>();
        
        for (Integer i = 0; i < mappings.size(); i++) {
            if (mappings.get(i).isSelected) {
                if (!mappings.get(i).isFromSession) {
                    objectsToRemove.add(mappings.get(i).ecrfMappingVO);
                } else {
                    mappings.remove(i);
                    i--;
                }
            }
        }
    
        // If collection to remove > 0 call webservice to remove, repull data
        if (objectsToRemove.size() > 0) {
            MdmSvtService.mdmCrudResponse mdmResponse = MdmSvtServiceWrapper.deleteSvtEcrfMappingFromVOs(objectsToRemove);
            searchAllData();
        }
    
        processScheduledMappingData(mappings);
    }
    
    public void removeUnscheduledMappingData() {
    	system.debug('-----------------------svtId------------------------' + keySvtId);
        list<FormMappingWrapper> mappings = svtEcrfWrapper.get(keySvtId).formMappings;
        list<MdmSvtService.ecrfFormMappingVO> objectsToRemove = new list<MdmSvtService.ecrfFormMappingVO>();
        for (Integer i = 0; i < mappings.size(); i++) {
            if (mappings.get(i).isSelected) {
                if (!mappings.get(i).isFromSession) {
                    objectsToRemove.add(mappings.get(i).ecrfFormMappingVO);
                } else {
                    mappings.remove(i);
                    i--;
                }
            }
        }
    
        // If collection to remove > 0 call webservice to remove, repull data
        if (objectsToRemove.size() > 0) {
            MdmSvtService.mdmCrudResponse mdmResponse = MdmSvtServiceWrapper.deleteSvtUsvFormMappingFromVOs(objectsToRemove);
            searchAllData();
        }
    }

    /**
     * Method to persist the mapping data to the CCI MDM
     * Save is performed for one SVT at a time
     */
    public void saveScheduledMappingData() {
        system.debug('-----------------------svtId------------------------' + keySvtId);
        list<EcrfMappingWrapper> mappings = svtEcrfWrapper.get(keySvtId).visitMappings;
        list<MdmSvtService.ecrfMappingVO> mapsToSend = new list<MdmSvtService.ecrfMappingVO>();
        map<String, Boolean> eventDate = new map<String,Boolean>();
        Boolean doSave = true;

        ApexPages.getMessages().clear();
        for(EcrfMappingWrapper maps : mappings) {

            if(eventDate.containsKey(maps.ecrfMappingVO.svtItemVO.visitType + '_' + maps.ecrfMappingVO.svtItemVO.visitName) && 
            	(maps.ecrfMappingVO.primary)) {
                Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'The eventDate checkbox is already checked for this visitType. Multiple selections are not allowed'));
                doSave = false;
            }

            if(maps.ecrfMappingVO.primary)
                eventDate.put(maps.ecrfMappingVO.svtItemVO.visitType + '_' + maps.ecrfMappingVO.svtItemVO.visitName, maps.ecrfMappingVO.primary);
        }

        if(doSave) {
            for (EcrfMappingWrapper maps : mappings) {
                maps.ecrfMappingVO.mapped = true;
                maps.ecrfMappingVO.mappedBy = UserInfo.getName();
                mapsToSend.add(maps.ecrfMappingVO);
                system.debug('-------------------isSelected?-------------------' + maps.isSelected);
                system.debug('-------------------isMapped?-------------------' + maps.ecrfMappingVO.mapped);
                system.debug('-------------------isPrimary?-------------------' + maps.ecrfMappingVO.primary);
                system.debug('-------------------mappedBy-------------------' + maps.ecrfMappingVO.mappedBy);
                system.debug('-------------------svtItemVO-------------------' + maps.ecrfMappingVO.svtItemVO);
                system.debug('-------------------ecrfItemVO-------------------' + maps.ecrfMappingVO.ecrfItemVO);
            }
            MdmSvtService.mdmCrudResponse mdmResponse = MdmSvtServiceWrapper.saveSvtEcrfMappingFromVOs(mapsToSend);
            searchAllData();
        }
    }
    
    public void saveUnscheduledMappingData() {
    	system.debug('-----------------------svtId------------------------' + keySvtId);
        list<FormMappingWrapper> mappings = svtEcrfWrapper.get(keySvtId).formMappings;
        list<MdmSvtService.ecrfFormMappingVO> mapsToSend = new list<MdmSvtService.ecrfFormMappingVO>();
        Boolean doSave = true;

        ApexPages.getMessages().clear();

        if(doSave) {
            for (FormMappingWrapper maps : mappings) {
                maps.ecrfFormMappingVO.mapped = true;
                maps.ecrfFormMappingVO.mappedBy = UserInfo.getName();
                mapsToSend.add(maps.ecrfFormMappingVO);
                system.debug('-------------------isSelected?-------------------' + maps.isSelected);
                system.debug('-------------------isMapped?-------------------' + maps.ecrfFormMappingVO.mapped);
                system.debug('-------------------mappedBy-------------------' + maps.ecrfFormMappingVO.mappedBy);
                system.debug('-------------------svtItemVO-------------------' + maps.ecrfFormMappingVO.svtItemVO);
                system.debug('-------------------ecrfFormVO-------------------' + maps.ecrfFormMappingVO.ecrfFormVO);
            }
            MdmSvtService.mdmCrudResponse mdmResponse = MdmSvtServiceWrapper.saveSvtUsvFormMappingFromVOs(mapsToSend);
            searchAllData();
        }
    }

    /**
     * Utility method to update the primary flags after addition/deletion of new mappings
     * Will look for the latest event for a certain visit type and set the primary flag to true
     */
    private static void processScheduledMappingData(List<EcrfMappingWrapper> ecrfMappingWrappers) {
        ecrfMappingWrappers.sort();
        EcrfMappingWrapper previousWrapper = null;
        for (EcrfMappingWrapper wrapper : ecrfMappingWrappers) {
            if (previousWrapper != null) {
                if (wrapper.ecrfMappingVO.svtItemVO.systemId.equals(previousWrapper.ecrfMappingVO.svtItemVO.systemId)
                    && wrapper.ecrfMappingVO.ecrfItemVO.sequenceNumber > previousWrapper.ecrfMappingVO.ecrfItemVO.sequenceNumber) {
                    wrapper.ecrfMappingVO.primary = true;
                    previousWrapper.ecrfMappingVO.primary = false;
                }
            } else {
                previousWrapper = new EcrfMappingWrapper();
            }
            previousWrapper = wrapper;
        }
    }

    /**
     * Wrapper class to hold all data related to a certain subject visit template (SVT)
     */
    public class SvtEcrfMappingWrapper {
        public String selectedSVT {get;set;}
        public String selectedUsvSvtItem {get;set;}
        public String selectedECRF {get;set;}
        public String selectedEcrfForm {get;set;}
        public list<String> selectedEcrfForms {get;set;}
        public MdmSvtService.svtVO svtVO {get;set;}
        public Boolean isSvtMapped {get;set;}
        public Boolean areAllProceduresMapped {get;set;}
        public Boolean isSvtFormsMapped {get;set;}
        public Boolean areAllFormProceduresMapped {get;set;}
        public list<MdmSvtService.svtItemVO> svtItemVOs {get;set;}
        public list<MdmSvtService.ecrfItemVO> ecrfItemVOs {get;set;}
        public list<MdmSvtService.ecrfFormVO> ecrfFormVOs {get;set;}
        public list<EcrfMappingWrapper> visitMappings {get;set;}
        public list<FormMappingWrapper> formMappings {get;set;}
        public list<selectoption> svtItemOptions {get;set;}
        public list<selectoption> svtUsvItemOptions {get;set;}
        public list<selectoption> ecrfItemOptions {get;set;}
        public list<selectoption> ecrfFormOptions {get;set;}
        
        public SvtEcrfMappingWrapper() {
            isSvtMapped = true;
            isSvtFormsMapped = true;
            areAllProceduresMapped = true;
            svtItemVOs = new list<MdmSvtService.svtItemVO>();
            ecrfItemVOs = new list<MdmSvtService.ecrfItemVO>();
            ecrfFormVOs = new list<MdmSvtService.ecrfFormVO>();
            visitMappings = new list<EcrfMappingWrapper>();
            formMappings = new list<FormMappingWrapper>();             
        }
    
    }
    
    /**
     * Wrapper class to hold the data for individual mappings for svtItem, ecrfItem, ecrfForm
     * Implements Comparable to allow sorting of data in SF Data Table
     */
    public abstract class MappingWrapper {
        public Boolean isSelected{get;set;}
        public Boolean isFromSession{get;set;}
        
        public MappingWrapper() {
            this.isSelected = false;
            this.isFromSession = false;
        }
        
        public MappingWrapper(Boolean isSelected, Boolean isFromSession) {
            this.isSelected = isSelected;
            this.isFromSession = isFromSession;
        }
    }
    
    public class EcrfMappingWrapper extends MappingWrapper implements Comparable {
    	public MdmSvtService.ecrfMappingVO ecrfMappingVO {get;set;}
    	
    	public EcrfMappingWrapper() {
    		this.ecrfMappingVO = new MdmSvtService.ecrfMappingVO();
    	}
    	
    	public EcrfMappingWrapper(Boolean isSelected, Boolean isFromSession, MdmSvtService.ecrfMappingVO ecrfMapVO) {
    		super(isSelected, isFromSession);
    		this.ecrfMappingVO = ecrfMapVO;
    	}
    	
    	// Method used to sort data on the SF Data Table
        public Integer compareTo(Object compareTo) {
            EcrfMappingWrapper wrapper = (EcrfMappingWrapper) compareTo;
            if (ecrfMappingVO.svtItemVO.sequenceNumber == wrapper.ecrfMappingVO.svtItemVO.sequenceNumber) {
                if (ecrfMappingVO.ecrfItemVO.sequenceNumber > wrapper.ecrfMappingVO.ecrfItemVO.sequenceNumber) {
                    return 1;
                }  else if (ecrfMappingVO.ecrfItemVO.sequenceNumber < wrapper.ecrfMappingVO.ecrfItemVO.sequenceNumber) {
                    return -1;
                }
                return 0;
            } else if (ecrfMappingVO.svtItemVO.sequenceNumber > wrapper.ecrfMappingVO.svtItemVO.sequenceNumber) { 
                return 1;
            }
            return -1;
        }
    }
    
    public class FormMappingWrapper extends MappingWrapper implements Comparable {
    	public MdmSvtService.ecrfFormMappingVO ecrfFormMappingVO {get;set;}
    	
    	public FormMappingWrapper() {
    		this.ecrfFormMappingVO = new MdmSvtService.ecrfFormMappingVO();
    	}
    	
    	public FormMappingWrapper(Boolean isSelected, Boolean isFromSession, MdmSvtService.ecrfFormMappingVO ecrfFormMapVO) {
    		super(isSelected, isFromSession);
    		this.ecrfFormMappingVO = ecrfFormMapVO;
    	}
    	
    	// Method used to sort data on the SF Data Table
        public Integer compareTo(Object compareTo) {
            FormMappingWrapper wrapper = (FormMappingWrapper) compareTo;
            if (ecrfFormMappingVO.svtItemVO.sequenceNumber == wrapper.ecrfFormMappingVO.svtItemVO.sequenceNumber) {
                if(CCI_Util.transformStringWithNumbers(ecrfFormMappingVO.ecrfFormVO.formLabel) > 
                	CCI_Util.transformStringWithNumbers(wrapper.ecrfFormMappingVO.ecrfFormVO.formLabel)) {
                    return 1;
                }  else if(CCI_Util.transformStringWithNumbers(ecrfFormMappingVO.ecrfFormVO.formLabel) < 
                	CCI_Util.transformStringWithNumbers(wrapper.ecrfFormMappingVO.ecrfFormVO.formLabel)) {
                    return -1;
                }
                return 0;
            } else if (ecrfFormMappingVO.svtItemVO.sequenceNumber > wrapper.ecrfFormMappingVO.svtItemVO.sequenceNumber) { 
                return 1;
            }
            return -1;
        }
    }
    	
}