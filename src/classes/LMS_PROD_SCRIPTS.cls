global class LMS_PROD_SCRIPTS {
	
	private static String query{get;set;}
	private static String statusDraft{get;set;}
	private static String inSync{get;set;}
	private static String outSync{get;set;}
	private static String actionAdd{get;set;}
	
	static {
		Map<String, LMSConstantSettings__c> constants = LMSConstantSettings__c.getAll();
		inSync = constants.get('inSync').Value__c;
		outSync = constants.get('outSync').Value__c;
		actionAdd = constants.get('addMappings').Value__c;
		statusDraft = constants.get('statusDraft').Value__c;
	}
    
    public static void sendRoleData(List<LMS_Role__c> roles) {
    	/*List<LMS_Role__c> roles = 
    	[SELECT Role_Name__c, Status__c, Id, SABA_Role_PK__c, Role_Type__r.Name FROM LMS_Role__c WHERE Sync_Status__c = :outSync LIMIT 100];*/
    	List<LMS_Role__c> rolesToUpdate = new List<LMS_Role__c>();
    	Map<String, LMS_Role__c> roleMap = new Map<String, LMS_Role__c>();
    	for(LMS_Role__c role : roles) {
    		roleMap.put(role.Id, role);
    	}
    	rolesToUpdate = LMS_ToolsService.sendRolesData(roles, roleMap);
    	
    	update rolesToUpdate;
    }  
    
    public static void populateRoleEmployees(List<LMS_Role__c> roles) {
    	List<String> values = new List<String>();
    	List<String> statusList = new List<String>();
    	List<String> excludeStatus = new List<String>();
    	List<LMS_Role_Employee__c> roleEmps = new List<LMS_Role_Employee__c>();
    	Map<String, List<String>> empStatusMap = new Map<String, List<String>>();
    	
    	List<Employee_Status__c> employeeStatus = [SELECT Employee_Status__c, Employee_Type__c FROM Employee_Status__c ORDER BY Employee_Type__c ASC];
    	String empType;
    	List<String> statuses;
    	for(Employee_Status__c empStatus : employeeStatus) {
    		if(empType != empStatus.Employee_Type__c) {
    			statuses = new List<String>();
    			empType = empStatus.Employee_Type__c;
    		}
    		if(empStatus.Employee_Type__c == 'Terminated') {
    			excludeStatus.add(empStatus.Employee_Status__c);
    		}
    		statuses.add(empStatus.Employee_Status__c);
    		empStatusMap.put(empStatus.Employee_Type__c, statuses);
    	}
    	
    	/*List<LMS_Role__c> roles = 
    	[SELECT Job_Class_Desc__r.Name, Job_Title__r.Job_Code__c, Business_Unit__r.Name, Department__r.Name, Region__r.Region_Name__c, 
    		Country__r.Name, Employee_Type__r.Name FROM LMS_Role__c WHERE Role_Type__r.Name = 'PRA' LIMIT 98];*/
    	for(LMS_Role__c role : roles) {
    		values.add(role.Job_Class_Desc__r.Name);
    		values.add(role.Job_Title__r.Job_Code__c);
    		values.add(role.Business_Unit__r.Name);
    		values.add(role.Department__r.Name);
    		values.add(role.Region__r.Region_Name__c);
    		values.add(role.Country__r.Name);
    		if(role.Employee_Type__r.Name != null) {
    			statusList = empStatusMap.get(role.Employee_Type__r.Name);
    			System.debug('----------------status list----------------' + statusList);
    		} else
    			statusList = null;
    			System.debug('---------------exclude status---------------' + excludeStatus);
    		
    		String qry = 'SELECT Name, Date_Hired__c, Term_Date__c FROM Employee_Details__c WHERE Status__c not in:excludeStatus';
			if(values[0]!=null)qry += ' and Job_Class_Desc__c=\'' + values[0] + '\'';
			if(values[1]!=null)qry += ' and Job_Code__c=\'' + values[1] + '\'';
			if(values[2]!=null)qry += ' and Business_Unit_Desc__c=\'' + values[2] + '\'';
			if(values[3]!=null)qry += ' and Department__c=\'' + values[3] + '\'';
			if(values[4]!=null)qry += ' and Country_Name__r.Region_Name__c=\'' + values[4] + '\'';
			if(values[5]!=null)qry += ' and Country_Name__r.Name=\'' + values[5] + '\'';
			if(statusList!=null)qry += ' and Status__c in:statusList';
			qry += ' ' + LMS_ToolsFilter.getWhitelistEmployeeFilter() + ' ' + LMS_ToolsFilter.getBlackListEmployeeFilter();
			List<Employee_Details__c> fullEmpList = Database.query(qry);
			
			for(integer i = 0; i < fullEmpList.size(); i++) {
		    	LMS_Role_Employee__c employee = new LMS_Role_Employee__c();
	            if(fullEmpList[i].Date_Hired__c <= Date.today()) {
	            	employee.Status__c = statusDraft;
	            } else {
	            	employee.Status__c = 'Hold';
	            }
	            employee.Role_Id__c = role.Id;
	            employee.Employee_Id__c = fullEmpList[i].Id;
	            employee.Created_On__c = Date.today();
	            employee.Updated_On__c = Date.today();
	            employee.Sync__c = outSync;
	            roleEmps.add(employee);
	        }
			
			values = new List<String>();
			statusList = new List<String>();
    	}
    	
		try {
        	insert roleEmps;
		} catch(Exception e) {
			System.debug(e.getMessage());
		}   	
    }
    
    public static void sendRoleCourseMappings(List<LMS_Role_Course__c> roleCourses) {
    	/*List<LMS_Role_Course__c> roleCourses = 
    	[SELECT Role_Id__c, Role_Id__r.SABA_Role_PK__c, Course_Id__r.SABA_ID_PK__c, Sync_Status__c, SABA_Role_Course_PK__c, Status__c, 
    		Assigned_On__c, Assigned_By__c FROM LMS_Role_Course__c WHERE Sync_Status__c = :outSync ORDER BY Role_Id__c ASC LIMIT 1000];*/
    	List<LMS_Role_Course__c> addRoleCourses = new List<LMS_Role_Course__c>();
    	Map<String, List<LMS_Role_Course__c>> sendMap = new Map<String, List<LMS_Role_Course__c>>();
    	Map<String,Map<String, LMS_Role_Course__c>> sendObjects = new Map<String,Map<String, LMS_Role_Course__c>>();
    	
    	if(roleCourses.size() > 0) {
        	String roleId;
	        Map<String, LMS_Role_Course__c> roleCourseMatch = new Map<String, LMS_Role_Course__c>();
	        for(LMS_Role_Course__c rc : roleCourses) {
	        	if(roleId != rc.Role_Id__c) {
	        		roleCourseMatch = new Map<String, LMS_Role_Course__c>();
	            	roleId = rc.Role_Id__c;
	        	}
	        	roleCourseMatch.put(rc.Course_Id__r.SABA_ID_PK__c, rc);
	            sendObjects.put(rc.Role_Id__r.SABA_Role_PK__c, roleCourseMatch);
	            addRoleCourses.add(rc);
	        }
	        sendMap.put(actionAdd, addRoleCourses);
	        addRoleCourses = LMS_ToolsService.executeCoursesToRolesWS(sendMap, sendObjects);
        }        
        update addRoleCourses;
    }
    
    public static void sendRoleEmployeeMappings(List<LMS_Role_Employee__c> roleEmployees) {
    	/*List<LMS_Role_Employee__c> roleEmployees = 
    	[SELECT  Role_Id__c, Status__c, Employee_Id__r.Name, Role_Id__r.SABA_Role_PK__c, Employee_Id__r.Date_Hired__c 
    		FROM LMS_Role_Employee__c WHERE Sync__c = :outSync ORDER BY Role_Id__c LIMIT 1000];*/
    	List<LMS_Role_Employee__c> toUpdate = new List<LMS_Role_Employee__c>();
        List<LMS_Role_Employee__c> updateList = new List<LMS_Role_Employee__c>();
        Map<String, LMS_Role_Employee__c> personsAdd = null;
        Map<String, List<LMS_Role_Employee__c>> mapUpdate = new Map<String, List<LMS_Role_Employee__c>>();
        Map<String, Map<String, LMS_Role_Employee__c>> objectsUpdate = new Map<String, Map<String, LMS_Role_Employee__c>>();
        
        if(roleEmployees.size() > 0) {
        	String roleId;
        	personsAdd = new Map<String, LMS_Role_Employee__c>();
	        for(LMS_Role_Employee__c re : roleEmployees) {
	        	if(roleId != re.Role_Id__c) {
	        		personsAdd = new Map<String, LMS_Role_Employee__c>();
        			roleId = re.Role_Id__c;
	        	}
	        	if(re.Employee_Id__r.Date_Hired__c <= Date.today()) {
		        	personsAdd.put(re.Employee_Id__r.Name, re);
	        		objectsUpdate.put(re.Role_Id__r.SABA_Role_PK__c, personsAdd);
	        		toUpdate.add(re);
	        	}
	        }
        }
		
        if((null != toUpdate && toUpdate.size() > 0)) {
	        mapUpdate.put(actionAdd, toUpdate);
        	updateList = LMS_ToolsService.executeEmployeesToRolesWS(mapUpdate, objectsUpdate);
        }
        if(updateList.size() > 0) {
        	update updateList;
        }
    }

}