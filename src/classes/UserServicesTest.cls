@isTest
private class UserServicesTest {
	
	private static final String RELATED_ID = '001000000000000AAA';
	private static final String APP_NAME = 'APP_NAME';
	private static final String TEST_VALUE = 'TEST_VALUE';
	private static final String NEW_VALUE = 'NEW_VALUE';
	
	@isTest
	private static void getUserPreferrencesNoRecord(){
		System.assertEquals( null, UserServices.getUserPreferrences(RELATED_ID, APP_NAME) );
	}
	
	@isTest
	private static void getUserPreferrencesPositive(){
		createPreferrences();
		System.assertEquals( TEST_VALUE, UserServices.getUserPreferrences(RELATED_ID, APP_NAME) );
	}
	
	@isTest
	private static void saveUserPreferrencesNoRecord(){
		Test.startTest();
			UserServices.saveUserPreferrences(RELATED_ID, APP_NAME, TEST_VALUE);
		Test.stopTest();
		System.assertEquals( TEST_VALUE, UserServices.getUserPreferrences(RELATED_ID, APP_NAME) );
	}
	
	@isTest
	private static void saveUserPreferrencesRecordExists(){
		createPreferrences();
		Test.startTest();
			UserServices.saveUserPreferrences(RELATED_ID, APP_NAME, NEW_VALUE);
		Test.stopTest();
		System.assertEquals( NEW_VALUE, UserServices.getUserPreferrences(RELATED_ID, APP_NAME) );
	}
	
	private static void createPreferrences(){
		insert new User_Preferences__c(
				Related_Id__c = RELATED_ID,
				Application_Name__c = APP_NAME,
				User__c = UserInfo.getUserId(),
				Value__c= TEST_VALUE
		);
	}
}