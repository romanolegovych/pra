@istest private class PAWS_FlowStepActionTriggerTest
{
	@istest private static void coverAll()
	{
		PAWS_ApexTestsEnvironment.init();
		
		STSWR1__Flow_Step_Action__c stepAction = PAWS_ApexTestsEnvironment.FlowStepAction;
		stepAction.STSWR1__Type__c = 'Start Sub Flow';
		
		stepAction.STSWR1__Config__c = System.Json.Serialize(new Map<String, String>{'className' => 'FlowApexTestsEnvironment', 'flowId' => PAWS_ApexTestsEnvironment.Flow.Id});
		update stepAction;
		
		new PAWS_FlowStepActionTrigger().afterDelete(new List<STSWR1__Flow_Step_Action__c>{stepAction});
		
		System.assertEquals(0, [select count() from STSWR1__Gantt_Step_Property__c where STSWR1__Flow__c = :PAWS_ApexTestsEnvironment.Flow.Id]);
	}
}