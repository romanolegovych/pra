/**
@author Bhargav Devaram
@date 2015
@description this is PBB_ServiceTasktoQuestionsCntrl test class
**/
@isTest
private class PBB_ServiceTasktoQuestionsCntrlTest{
     static testMethod void testPBB_ServiceTasktoQuestionsCntrl(){
         test.startTest();       
             PBB_TestUtils tu = new PBB_TestUtils ();
             
             //create model,service area, service function,service tasks
             tu.smm =tu.CreateServiceModelAttributes();
             tu.sa = tu.createServiceAreaAttributes();
             tu.sf = tu.createServiceFunctionAttributes();
             tu.st = tu.createServiceTaskAttributes();  
             
             tu.siqn = tu.createSIQNAttributes();  
             tu.siqs = tu.createSIQSAttributes('Project');  
             tu.sir = tu.createSIRAttributes(); 
              
             Service_Task_To_Service_Impact__c STtoSI = new Service_Task_To_Service_Impact__c ();
             STtoSI.Service_Task__c = tu.st.id;
             STtoSI.Service_Impact_Question_Name__c = tu.siqn.id ;
             insert STtoSI;
             tu.smm = tu.approveServicemodel(tu.smm);
             Apexpages.currentPage().getParameters().put('id',STtoSI.id);
             PBB_ServiceTasktoQuestionsCntrl stq= new PBB_ServiceTasktoQuestionsCntrl(new Apexpages.Standardcontroller(STtoSI));
             stq.saveSTtoSIQ();
        
         test.stopTest();
     }
}