/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest(SeeAllData=false)
private class PBB_ModelPageControllerTest    {
   
    static testMethod void servicesModelPageReferenceTest()    {
       
        Test.startTest();
        PBB_ModelPageController controller =  new PBB_ModelPageController();
        
        controller.modelName = 'Service_Model__c';
        Schema.DescribeSObjectResult R = Service_Model__c.SObjectType.getDescribe();
        System.assertEquals(controller.getNewPageReference().getUrl(), new PageReference('/' + R.getKeyPrefix() + '/e').getUrl());
        
        controller.modelName = 'WR_Model__c';
        Schema.DescribeSObjectResult R1 = WR_Model__c.SObjectType.getDescribe();
        System.assertEquals(controller.getNewPageReference().getUrl(), new PageReference('/' + R1.getKeyPrefix() + '/e').getUrl());
        
        controller.modelName = 'SRM_Model__c';
        Schema.DescribeSObjectResult R2 = SRM_Model__c.SObjectType.getDescribe();
        System.assertEquals(controller.getNewPageReference().getUrl(), new PageReference('/' + R2.getKeyPrefix() + '/e').getUrl());
        
        controller.modelName = 'Service_Impact_Question_Name__c';
        Schema.DescribeSObjectResult R3 = Service_Impact_Question_Name__c.SObjectType.getDescribe();
        System.assertEquals(controller.getNewPageReference().getUrl(), new PageReference('/' + R3.getKeyPrefix() + '/e').getUrl()); 
        
        controller.modelName = 'Mapping_Region_Model__c';
        Schema.DescribeSObjectResult R4 = Mapping_Region_Model__c.SObjectType.getDescribe();
        System.assertEquals(controller.getNewPageReference().getUrl(), new PageReference('/' + R4.getKeyPrefix() + '/e').getUrl());
        
        controller.modelName = 'Bill_Rate_Card_Model__c';
        Schema.DescribeSObjectResult R5 = Bill_Rate_Card_Model__c.SObjectType.getDescribe();
        System.assertEquals(controller.getNewPageReference().getUrl(), new PageReference('/' + R5.getKeyPrefix() + '/e').getUrl());
        
        controller.modelName = 'Formula_Name__c';
        Schema.DescribeSObjectResult R6 = Formula_Name__c.SObjectType.getDescribe();
        System.assertEquals(controller.getNewPageReference().getUrl(), new PageReference('/' + R6.getKeyPrefix() + '/e').getUrl());
        Test.stopTest();
    }
}