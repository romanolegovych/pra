public with sharing class AA_BUFCodeDataAccessor{
     
    public static List<Business_Unit__c> getBuCodes()
    {
        return [SELECT ID, Name FROM Business_Unit__c] ;
    }
    
    public static List<Function_Code__c> getFCodes()
    {
        return [SELECT ID, Name FROM Function_Code__c];
    }
    
    public static BUF_Code__c getBufCodeDetails(String val){
        return [SELECT ID, Name FROM BUF_Code__c WHERE Name=:val ];
    }
    
    public static List<BUF_Code__c> getBufCodeName(String val){
        return [SELECT ID, Name FROM BUF_Code__c WHERE Id=:val ];
    }
    
    public static void insertBufCode(BUF_Code__c bufCode){
        insert bufCode;
    }
}