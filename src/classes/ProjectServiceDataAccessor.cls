public with sharing class ProjectServiceDataAccessor {

	/** Object definition for fields used in application for ProjectService__c
	 * @author	Maurice Kremer
	 * @version 09-Jan-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('ProjectService__c');		
	}
	
	
	/** Object definition for fields used in application for ProjectService__c with the parameter referenceName as a prefix
	 * @author	Maurice Kremer
	 * @version 09-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Name,';
		result += referenceName + 'Service__c,';
		result += referenceName + 'Unique__c';	
		return result;
	}
	
	/** Retrieve list of Services.
	 * @author	Maurice Kremer
	 * @version 09-Jan-2014
	 * @param	whereClause			The criteria that ProjectService__c Lists must meet
	 * @return	List of Services
	 */
	public static List<ProjectService__c> getProjectServiceList(String whereClause) {
		String query = String.format(
								'SELECT {0} ' +
								'FROM ProjectService__c ' +
								'WHERE {1} ',
								new List<String> {
									getSObjectFieldString(),
									whereClause
								}
							);
		return (List<ProjectService__c>) Database.query(query);
	}

}