@isTest
private class BillRateCardModelsControllerTest {
	
	private static BillRateCardModelsController cont;
	
	@isTest
	static void BillRateCardModelsControllerTest() {
		Test.startTest();
    		cont = new BillRateCardModelsController();
    	Test.stopTest();
    	System.assert(cont.BRModelsList.size() == 0);
	}

	@isTest
	static void getNewPageReferenceTest() {
		cont = new BillRateCardModelsController();
		Test.startTest();
    		PageReference pr = cont.getNewPageReference();
    	Test.stopTest();
    	Schema.DescribeSObjectResult  r = Bill_Rate_Card_Model__c.SObjectType.getDescribe();
    	System.assertEquals(pr.getUrl(), '/'+r.getKeyPrefix()+'/e');
	}
}