/**
* @author 	Sukrut Wagh
* @date 	06/24/2014
* @description Constants used by Integration framework
*			   Note: These constants should not be externalized to Custom Settings, nor it's value should be changed. It might lead to data corruption.	
*/

public with sharing class COM_IntegrationConstants {
	
	/* sObject API Name */
	public static final String API_INT_STATUS = 'INT_STATUS__c';
	public static final String API_INT_SETUP = 'INT_SETUP__c';
	public static final String API_INT_SETUP_STATUS = 'INT_SETUP_STATUS__c';
	
	/* Integration Statuses	*/
	
	/**
	* @author 	Sukrut Wagh
	* @date 	06/24/2014
	* @description Record is in-sync with the integrated system.
	*			   When the transaction (synchronous or asynchronous) is successful, record will be in-sync status 
	*/
	public static final String STATUS_INSYNC = 'INSYNC';
	
	/**
	* @author 	Sukrut Wagh
	* @date 	06/24/2014
	* @description Record is not in sync with the integrated system.
	*			   TODO: List the scenarios for this status 
	*/
	public static final String STATUS_OUTSYNC = 'OUTSYNC';
	
	/**
	* @author 	Sukrut Wagh
	* @date 	06/24/2014
	* @description  Record errored out. The exception/error field should indicate the failure reasons
	*  				There could be various possibilities for this status:
	*  				1. Validation failures before making the integration transaction
	*  				2. Integration transaction retuned specific errors, possibly with error codes & reasons
	*  				3. TODO: Add additional scenarios
	*  				Some errors might be unrecoverable & might need to bring in to attention by an email notification
	*/
	public static final String STATUS_ERROR = 'ERROR';
	
	/**
	* @author 	Sukrut Wagh
	* @date 	06/24/2014
	* @description Record is in-transit with the integrated system. Means the transaction is in process / request was sent.
	* 				 In case of an asynchronous integration (most of the WS will be designed for bulk processing),
	* 				 it might take hours for the external system to respond back & update the record with appropriate status.
	*/
	public static final String STATUS_TRANSIT_ADD = 'TRANSIT_ADD';
	
	public static final String STATUS_TRANSIT_UPDATE = 'TRANSIT_UPDATE';
	
	public static final String STATUS_TRANSIT_DELETE = 'TRANSIT_DELETE';

}