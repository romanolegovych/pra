public with sharing class BDT_ProjectPassThroughController {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;}
	public list<passthroughServiceWrapper> ProjectPTServices 				{get;set;}
	//public list<ProjectPassThroughService__c> SelectedPassThroughServices	{get;set;}
	public String projectId													{get;set;}
	public Client_Project__c singleProject									{get;set;}
	public list<Study__c> studiesList										{get;set;}
	public list<Decimal> studyValues										{get;set;}
	
	public class passthroughServiceWrapper{
		public PassThroughService__c PTService			{get;set;}
		public List<studyPTWrapper> studyPTS			{get;set;}
		
		public passthroughServiceWrapper( PassThroughService__c p_PTService, list<studyPTWrapper> p_studyPTS){
			PTService = p_PTService;
			studyPTS = p_studyPTS;
		}
	}
	public class studyPTWrapper{
		public string studyId 								{get;set;}
		public boolean selected 							{get;set;}
		public ProjectPassThroughStudyAssignment__c PPTSA  	{get;set;}
		public studyPTWrapper(string p_studyId, boolean p_selected, ProjectPassThroughStudyAssignment__c p_pptsa){
			studyId = p_studyId;
			selected = p_selected;
			PPTSA = p_pptsa;
		}
	}
	
	public BDT_ProjectPassThroughController(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Pass Through Services');
		projectId = BDT_Utils.getPreference('SelectedProject');
		if(projectId!=null && projectId!=''){
			singleProject = BDT_Utils.getProject(projectId, false);
			studiesList = BDT_Utils.readSelectedStudies(singleProject.id);
		}
		
		buildProjectPassthroughWrapper();	
	}
	public void buildProjectPassthroughWrapper(){
		list<PassThroughService__c> availablePTS;
		list<ProjectPassThroughStudyAssignment__c> assignedPTs;
		//list<ProjectPassThroughService__c> PTSinProject;
		studyValues= new list<decimal>();
		
		ProjectPTServices = new list<passthroughServiceWrapper>();
		
		availablePTS = [select Id, name, DisplaySequence__c, ServiceCategory__r.Name, ServiceCategory__c, ServiceCategory__r.Code__c
						from PassThroughService__c
						where BDTDeleted__c = false
						order by ServiceCategory__r.Code__c, DisplaySequence__c];
						
		assignedPTs = [select id, name, Study__r.Project__c, Study__c, approximatevalue__c,PassThroughService__r.Name 
		     			from ProjectPassThroughStudyAssignment__c
						where study__r.Project__c = :projectId];
						
		map<string, ProjectPassThroughStudyAssignment__c> ptMap = new map<string, ProjectPassThroughStudyAssignment__c>();				
		for(ProjectPassThroughStudyAssignment__c pptsa: assignedPTs ){
			ptMap.put(pptsa.study__c+'-'+pptsa.PassThroughService__c, pptsa );
		}
		
		studyValues = new decimal[studiesList.Size()];
				
		for(PassThroughService__c pts: availablePTs){
			PassThroughServiceWrapper ptswItem = new passThroughServiceWrapper(pts, new list<studyPTwrapper>() );
			integer studyIndex = 0;
			for(Study__c st: studiesList){
				if(ptMap.get(st.id+'-'+pts.id) != null ){
					ProjectPassThroughStudyAssignment__c pptsa = ptMap.get(st.id+'-'+pts.id);
					studyPTWrapper tmp = new studyPTWrapper(st.id,true, ptMap.get(st.id+'-'+pts.id) );
					ptswItem.studyPTS.add(tmp);
					decimal approxValue  = ((pptsa.ApproximateValue__c==null)?0:pptsa.ApproximateValue__c);
					decimal value = ((studyValues[studyIndex]==null)?approxValue:studyValues[studyIndex]+approxValue);
					studyValues[studyIndex] = value;
				}else{
					studyPTWrapper tmp = new studyPTWrapper(st.id, false, new ProjectPassThroughStudyAssignment__c(study__c = st.id, PassThroughService__c= pts.id ));
					ptswItem.studyPTS.add(tmp);
				}					
				studyIndex ++;
			}
			ProjectPTServices.add(ptswItem);
		}	
	}
	public void recalculateTotals(){
		string studyId = system.currentPagereference().getParameters().get('studyId');
		integer column =Integer.valueOf(system.currentPagereference().getParameters().get('column'));
		decimal totalValue =0;	
		
		for(passthroughServiceWrapper ptsw: ProjectPTServices){
			for(studyPTWrapper sptw: ptsw.studyPTS){
				if(sptw.studyId == studyId){
					if(sptw.selected){
						decimal value=((sptw.pptsa.ApproximateValue__c==null)?0:sptw.PPTSA.ApproximateValue__c);
						totalValue += value;
					}
				}
			}
		}
		studyValues[column] = totalValue;	
	}
	public void save(){
		
		list<ProjectPassThroughStudyAssignment__c> pptsaToSave = new list<ProjectPassThroughStudyAssignment__c>();
		list<ProjectPassThroughStudyAssignment__c> pptsaToDelete = new list<ProjectPassThroughStudyAssignment__c>();
				
		for(passthroughServiceWrapper ptsw: ProjectPTServices){
			for(studyPTWrapper sptw: ptsw.studyPTS){
				if( sptw.selected ){
					pptsaToSave.add(sptw.PPTSA);	
				}else{
					if(sptw.PPTSA.Id!=null){
						pptsaToDelete.add(sptw.PPTSA);	
					}
				}
			}	
		}
		if(!pptsaToSave.isEmpty()){
			try {
				upsert pptsaToSave;
			} catch (Exception e){}
		}
		if(!pptsaToDelete.isEmpty()){
			try {
				delete pptsaToDelete;
			} catch (Exception e){}
		}
		
		buildProjectPassthroughWrapper();
	}
}