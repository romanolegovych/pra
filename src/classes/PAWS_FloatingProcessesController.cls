public with sharing class PAWS_FloatingProcessesController {
	
	public ID sourceID {
        get {
            if (sourceID == null) sourceID = Apexpages.currentPage().getParameters().get('sourceID');
            return sourceID;
            }
        set;
    }
    public String retURL {
        get {
            if (retURL == null) retURL = Apexpages.currentPage().getParameters().get('retURL');
            return retURL;
            }
        set;
    }	    
    public List <SelectOption> availableFlows {get;set;}
    public List <STSWR1__Flow_Instance__c> flowsInProgress {get;set;}
    public List <STSWR1__Flow_Instance__c> flowsCompleted {get;set;}
    public String selectedFlowId {get;set;}
    public Boolean leaveThePage {get;set;}
    String objType = sourceID.getSObjectType().getDescribe().getName();

	public void init() {		
		List <STSWR1__Flow_Instance__c> flowsToDisable = [SELECT STSWR1__Flow__c FROM STSWR1__Flow_Instance__c WHERE STSWR1__Object_Id__c = :sourceID];
		Set <ID> flowsToDisableIDs = New Set <ID>();
		for (STSWR1__Flow_Instance__c f : flowsToDisable) {
			flowsToDisableIDs.add(f.STSWR1__Flow__c); 
		}
		List <STSWR1__Flow__c> flowsToShow = [SELECT Name FROM STSWR1__Flow__c WHERE STSWR1__Object_Type__c = :objType 
										AND STSWR1__Type__c = 'Recurrent' 
										AND STSWR1__Start_Type__c = 'Manual'
										AND STSWR1__Status__c = 'Active' 
										ORDER BY Name];
		availableFlows = New List <SelectOption>();
		availableFlows.add(new SelectOption('', '--None--'));
		for (Integer i=0; i < flowsToShow.size(); i++) {
			if (flowsToDisableIDs.contains(flowsToShow[i].id)) {			
				availableFlows.add(new SelectOption(flowsToShow[i].id, flowsToShow[i].Name, true));
			} else {
				availableFlows.add(new SelectOption(flowsToShow[i].id, flowsToShow[i].Name));
			}
		}
		flowsInProgress = [SELECT STSWR1__Flow__r.Name, STSWR1__Is_Active__c, CreatedDate, CreatedBy.Name FROM STSWR1__Flow_Instance__c 
									WHERE STSWR1__Object_Id__c = :sourceID 
									AND STSWR1__Is_Completed__c = 'No' 
									AND STSWR1__Flow__r.STSWR1__Type__c = 'Recurrent' 
									AND STSWR1__Flow__r.STSWR1__Start_Type__c = 'Manual'
									ORDER BY CreatedDate];
		flowsCompleted = [SELECT STSWR1__Flow__r.Name, LastModifiedDate, LastModifiedBy.Name FROM STSWR1__Flow_Instance__c 
									WHERE STSWR1__Object_Id__c = :sourceID 
									AND STSWR1__Is_Completed__c = 'Yes' 
									AND STSWR1__Flow__r.STSWR1__Type__c = 'Recurrent' 
									AND STSWR1__Flow__r.STSWR1__Start_Type__c = 'Manual'
									ORDER BY LastModifiedDate];		
	}
	
	public void apply() {
      if (String.isEmpty(selectedFlowId)) {
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'You should select a Flow!'));
        return;
      }
      
      List<STSWR1__Flow__c> flows = [SELECT Name, STSWR1__Object_Type__c, STSWR1__Type__c, STSWR1__Conditions__c, STSWR1__Conditions_Logic__c, 
												STSWR1__Number_Of_Instances__c, STSWR1__Enable_User_Chatter_Notifications__c, STSWR1__Enable_Object_Chatter_Notifications__c, 
												(SELECT STSWR1__Flow__r.Name, STSWR1__Object_Id__c FROM STSWR1__Flow_Instances__r WHERE STSWR1__Object_Id__c = :sourceID)
												FROM STSWR1__Flow__c 
												WHERE Id = :SelectedFlowId 
												AND STSWR1__Status__c = 'Active'];
	  
	  sObject sourceObject = PAWS_Utilities.makeQuery(objType, 'Id =\'' + sourceID + '\'' , null)[0];
	  Map<sObject, List<STSWR1__Flow__c>> allowedObjectFlowMap = new Map<sObject, List<STSWR1__Flow__c>>{sourceObject => flows};
	  
	  try {
	  STSWR1.WorkflowActivator.getInstance().processObjects(allowedObjectFlowMap, true);
	  }catch(Exception ex){
	  	ApexPages.addMessages(ex);
	  	leaveThePage = false;
	  	return;
	  }
      leaveThePage = true;     
    }
    
}