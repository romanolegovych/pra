public class LMS_ToolsModifyRoles {
	
	private static String rolePRA {get;set;}
	private static String rolePST {get;set;}
	private static String roleAdhoc {get;set;}
	private static String roleActive {get;set;}
	private static String outSync {get;set;}
	private static String statusDraft {get;set;}
	private static String statusPendingAdd {get;set;}
	private static String wlblFilter {get;set;}
	private static List<String> sfdcErrors {get;set;}
	private static List<String> errorList {get;set;}
	private static Map<String, List<String>> errorMap {get;set;}
	
	/**
	 * Initialize instance variables
	 */
	static {
		// Custom Setting maps
		Map<String, LMSConstantSettings__c> constants = LMSConstantSettings__c.getAll();
		
		outSync = constants.get('outSync').Value__c;
		rolePRA = constants.get('typePRA').Value__c;
		rolePST = constants.get('typeProject').Value__c;
		roleAdhoc = constants.get('typeAdhoc').Value__c;
		roleActive = constants.get('roleActive').Value__c;
		statusDraft = constants.get('statusDraft').Value__c;
		statusPendingAdd = constants.get('statusPendingAdd').Value__c;
				
		wlblFilter = LMS_ToolsFilter.getWhitelistEmployeeFilter() + ' ' + LMS_ToolsFilter.getBlackListEmployeeFilter();
	}
	
	public static Map<String, List<String>> getErrorMap() {
		return errorMap;
	}
	
	/**
	 * Creates a new role and determines which role method to perform
	 */
	public static Map<String, List<String>> createRole(List<String> values, Integer roleType) {
		System.debug('------------------creating role data---------------------');
		errorMap = new Map<String, List<String>>();
		errorList = new List<String>();
		sfdcErrors = new List<String>();
		LMS_Role__c role = new LMS_Role__c();
		
		if(roleType == 1) {
			role = praRole(values, roleActive, outSync);
		} else if(roleType == 2) {
			role = projectRole(values, roleActive, outSync);
		} else if(roleType == 3) {
			role = adhocRole(values, roleActive, outSync);
		}
		try {
        	insert role;
		} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
		}
		return errorMap;
	}
	
	/**
	 * Creates role to employee mappings for a given role
	 */
	public static void createEmployeeMapping(List<String> values, String roleId) {
		errorMap = new Map<String, List<String>>();
		sfdcErrors = new List<String>();
		List<String> statuses = new List<String>();
		List<Employee_Status__c> statusList = new List<Employee_Status__c>();
		String jobCode = '';
		String jobTitle = values[1];
		String statusType = values[6];
		
		if(jobTitle != '' && jobTitle != null) {
			Job_Title__c jc = [SELECT Job_Code__c FROM Job_Title__c WHERE Job_Title__c = :jobTitle];
			jobCode = jc.Job_Code__c;
		} else {
			jobCode = null;
		}
		if(statusType != '' && statusType != null) {
			statusList = [SELECT Employee_Status__c FROM Employee_Status__c WHERE Employee_Type__c = :statusType];
			for(Employee_Status__c s : statusList) {
				statuses.add(s.Employee_Status__c);
			}
		} else {
			statusList = null;
		}
		
		List<Employee_Status__c> termStatus = [SELECT Employee_Status__c FROM Employee_Status__c WHERE Employee_Type__c = 'Terminated'];
		Set<String> excludeStatus = new Set<String>();
		for(Employee_Status__c s : termStatus) {
			excludeStatus.add(s.Employee_Status__c);
		}
		
		System.debug('------------jc---------------'+values[0]);
		System.debug('------------jt---------------'+jobCode);
		System.debug('------------bu---------------'+values[2]);
		System.debug('------------dp---------------'+values[3]);
		System.debug('------------rg---------------'+values[4]);
		System.debug('------------co---------------'+values[5]);
		System.debug('------------et---------------'+statusList);
		
		String qry = 'SELECT Name, Date_Hired__c, Term_Date__c FROM Employee_Details__c WHERE Status__c not in:excludeStatus';
		if(values[0]!=null)qry += ' and Job_Class_Desc__c=\'' + values[0] + '\'';
		if(jobCode!=null)qry += ' and Job_Code__c=:jobCode';
		if(values[2]!=null)qry += ' and Business_Unit_Desc__c=\'' + values[2] + '\'';
		if(values[3]!=null)qry += ' and Department__c=\'' + values[3] + '\'';
		if(values[4]!=null)qry += ' and Country_Name__r.Region_Name__c=\'' + values[4] + '\'';
		if(values[5]!=null)qry += ' and Country_Name__r.Name=\'' + values[5] + '\'';
		if(statusList!=null)qry += ' and Status__c in:statuses';
		qry += ' ' + wlblFilter;
		
		System.debug('------------query---------------'+qry);
        List<Employee_Details__c> fullEmpList = Database.query(qry);
        List<LMS_Role_Employee__c> empsToAdd = new list<LMS_Role_Employee__c>();
        
        for(integer i = 0; i < fullEmpList.size(); i++) {
            LMS_Role_Employee__c employee = new LMS_Role_Employee__c();
            if(fullEmpList[i].Date_Hired__c < Date.today()) {
            	employee.Status__c = statusDraft;
            } else {
            	employee.Status__c = statusPendingAdd;
            }
            employee.Role_Id__c = roleId;
            employee.Employee_Id__c = fullEmpList[i].Id;
            employee.Created_On__c = Date.today();
            employee.Updated_On__c = Date.today();
            employee.Sync__c = outSync;
            empsToAdd.add(employee);
        }
        
		try {
        	insert empsToAdd;
		} catch(Exception e) {
			System.debug(e.getMessage());
			sfdcErrors.add(e.getMessage());
			errorMap.put('SFDCEX', sfdcErrors);
		}
		
	}
	
	public static Map<String, List<String>> createRoleEmployeeAssociation(List<LMS_Role__c> roles) {
		for(LMS_Role__c r : roles) {
			String roleId;
			List<String> values = new List<String>();
			values.add(r.Job_Class_Desc__r.Name);
			values.add(r.Job_Title__r.Job_Title__c);
			values.add(r.Business_Unit__r.Name);
			values.add(r.Department__r.Name);
			values.add(r.Region__r.Region_Name__c);
			values.add(r.Country__r.Name);
			values.add(r.Employee_Type__r.Name);
			System.debug('------------jc---------------'+values[0]);
			System.debug('------------jt---------------'+values[1]);
			System.debug('------------bu---------------'+values[2]);
			System.debug('------------dp---------------'+values[3]);
			System.debug('------------rg---------------'+values[4]);
			System.debug('------------co---------------'+values[5]);
			System.debug('------------et---------------'+values[6]);
			roleId = r.Id;
			createEmployeeMapping(values, roleId);
		}
		return null;
	}
	
	public static Map<String, List<String>> breakRoleEmployeeAssociation(List<LMS_Role__c> roles) {
		List<String> roleIds = new List<String>();
		for(LMS_Role__c r : roles) {
			roleIds.add(r.Id);
		}
		
		List<LMS_Role_Employee__c> re = 
		[select Id from LMS_Role_Employee__c where Role_Id__c IN :roleIds];
		if(re.size() > 0) {
			delete re;
		}
		return null;
	}
	
	/**
	 * Creates a non-project PRA Role
	 */
	private static LMS_Role__c praRole(List<String> values,String status,String sync) {
		String jobFamily,jobTitle,businessUnit,department,region,country,empType = '';
		for(Integer i = 0; i < values.size(); i++) {
			if(i == 0)jobFamily = values[i];
			if(i == 1)jobTitle = values[i];
			if(i == 2)businessUnit = values[i];
			if(i == 3)department = values[i];
			if(i == 4)region = values[i];
			if(i == 5)country = values[i];
			if(i == 6)empType = values[i];
		}
		LMS_Role__c roles = new LMS_Role__c();
        Job_Class_Desc__c jcId;
        Job_Title__c jtId;
        PRA_Business_Unit__c buId;
        Department__c depId;
        Country__c cId;
        Region__c rId;
        Employee_Type__c etId;
        Role_Type__c roleType = [SELECT Id FROM Role_Type__c WHERE Name = :rolePRA];
        
        // Set attributes and insert new role 
		/*************************************************************/
        if(jobFamily == null && jobTitle == null) {
            roles.Job_Class_Desc__c = null;
            roles.Job_Title__c = null;
        } else if(jobFamily != null && jobTitle == null) {
        	jcId = getJobClassByDesc(jobFamily);
            roles.Job_Class_Desc__c = jcId.Id;
			roles.Job_Title__c = null;
        } else if((jobFamily == null && jobTitle != null) || (jobFamily != null && jobTitle != null)) {
        	jtId = getJobTitleByCode(jobTitle);
        	roles.Job_Class_Desc__c = getJobFamily(jobTitle);
        	roles.Job_Title__c = jtId.Id;
        }
        /*************************************************************/
        if(businessUnit == null && department == null) {
            roles.Business_Unit__c = null;
			roles.Department__c = null;
		} else if(businessUnit != null && department == null) {
			buId = getBusinessUnitByDesc(businessUnit);
            roles.Business_Unit__c = buId.Id;
			roles.Department__c = null;
		} else if(businessUnit == null && department != null) {
			depId = getDepartmentByDesc(department);
            roles.Business_Unit__c = null;
			roles.Department__c = depId.Id;
		} else if(businessUnit != null && department != null) {
			buId = getBusinessUnitByDesc(businessUnit);
			depId = getDepartmentByDesc(department);
            roles.Business_Unit__c = buId.Id;
			roles.Department__c = depId.Id;
		}
        /*************************************************************/
        if(region == null && country == null) {
            roles.Region__c = null;
            roles.Country__c = null;
        } else if(region != null && country == null) {
        	rId = getRegionByDesc(region);
            roles.Region__c = rId.id;
			roles.Country__c = null;
        } else if((region == null && country != null) || (region != null && country != null)) {
        	cId = getCountryByDesc(country);
        	roles.Region__c = getRegion(country);
        	roles.Country__c = cId.Id;
        }
        /*************************************************************/
        if(empType == null) {
            roles.Employee_Type__c = null;
        } else {
        	etId = getEmployeeTypeByDesc(empType);
            roles.Employee_Type__c = etId.Id;
        }
        /*************************************************************/
        roles.Status__c = status;
        roles.Sync_Status__c = sync;
        roles.Role_Type__c = roleType.Id;
        return roles;
	}
	
	/**
	 * Creates a project specific role
	 */
	private static LMS_Role__c projectRole(List<String> values, String status, String sync) {
		LMS_Role__c role = new LMS_Role__c();
		String clientText,projectText,projectRole = '';
		
		for(Integer i=0; i < values.size(); i++) {
			if(i == 0)clientText = values[i];
			if(i == 1)projectText = values[i];
			if(i == 2)projectRole = values[i];
		}
		
        List<Job_Class_Desc__c> jcId = new list<Job_Class_Desc__c>([SELECT name,id FROM Job_Class_Desc__c WHERE Name = :projectRole]);
        List<PST_Adhoc_Role__c> pstAdId = new list<PST_Adhoc_Role__c>([SELECT PST_Adhoc_Role__c,id from PST_Adhoc_Role__c WHERE PST_Adhoc_Role__c = :projectRole]);	
        List<WFM_Client__c> clientId = new list<WFM_Client__c>([SELECT name,id FROM WFM_Client__c WHERE Name = :clientText]);
        List<WFM_Project__c> projectId = new list<WFM_Project__c>([SELECT name,id,Contract_ID__r.Client_ID__r.Id FROM WFM_Project__c WHERE Name = :projectText]);
        Role_Type__c roleType = [SELECT Id FROM Role_Type__c WHERE Name = :rolePST];
        
        System.debug('--------projId---------'+projectText);
        
        // Set attributes and insert new role
        if(projectText != null && projectText != '') {
            role.Project_Id__c = projectId[0].id;
            role.Client_Id__c = projectId[0].Contract_Id__r.Client_Id__r.Id;
        } else if(projectText == '' || projectText == null) {
            role.Client_Id__c = clientId[0].Id;
            role.Project_Id__c = null;
        }
        if(projectRole == null || projectRole == '') {
            role.Project_Role__c = null;
        } else {
        	if(jcId != null && jcId.size() > 0) {
            	role.Job_Class_Desc__c = jcId[0].Id;
        	} else if(pstAdId != null && pstAdId.size() > 0) {
            	role.PST_Adhoc_Role__c = pstAdId[0].Id;
        	}
        }
        role.Status__c = status;
        role.Sync_Status__c = sync;
        role.Role_Type__c = roleType.Id;
		return role;
	}
	
	/**
	 * Creates an Adhoc Role
	 */
	private static LMS_Role__c adhocRole(List<String> values, String status, String sync) {
		String roleText;
		LMS_Role__c role = new LMS_Role__c();
        Role_Type__c roleType = [SELECT Id FROM Role_Type__c WHERE Name = :roleAdhoc];
		
		for(Integer i = 0; i < values.size(); i++) {
			if(i == 0)roleText = values[i];
		}
		role.Adhoc_Role_Name__c = roleText;
        role.Status__c = status;
        role.Sync_Status__c = sync;
        role.Role_Type__c = roleType.Id;
		return role;
	}
	
	/**
	 * Get job family from job title
	 */
	private static String getJobFamily(String title) {
		String family;
		Job_Title__c jf = [SELECT Job_Class_Desc__c FROM Job_Title__c WHERE Job_Title__c = :title];
		family = jf.Job_Class_Desc__c;
		return family;
	}
	
	/**
	 * Get business unit from department
	 */
	/*private static String getBusinessUnit(String department) {
		String businessUnit;
		BU_Department_Map__c depMap = [SELECT Business_Unit_Desc__c FROM BU_Department_Map__c WHERE Department__c = :department];
		PRA_Business_Unit__c unit = [SELECT Id FROM PRA_Business_Unit__c WHERE Name = :depMap.Business_Unit_Desc__c];
		businessUnit = unit.Id;
		return businessUnit;
	}*/
	
	/**
	 * Get region from country
	 */
	private static String getRegion(String country) {
		String region;
		Country__c regionName = [SELECT Region_Name__c FROM Country__c WHERE Name = :country];
		Region__c r = [SELECT Id FROM Region__c WHERE Region_Name__c = :regionName.Region_Name__c];
		region = r.Id;
		return region;
	}
	
	private static Job_Class_Desc__c getJobClassByDesc(String jobClass) {
		return [SELECT Id FROM Job_Class_Desc__c WHERE Name = :jobClass];
	}
	
	private static Job_Title__c getJobTitleByCode(String jobCode) {
		return [SELECT Id From Job_Title__c WHERE Job_Title__c = :jobCode AND Status__c = 'A'];
	}
	
	private static PRA_Business_Unit__c getBusinessUnitByDesc(String businessUnit) {
		return [SELECT Id From PRA_Business_Unit__c WHERE Name = :businessUnit AND Status__c = 'A'];
	}
	
	private static Department__c getDepartmentByDesc(String department) {
		return [SELECT Id From Department__c WHERE Name = :department AND Status__c = 'A'];
	}
	
	private static Region__c getRegionByDesc(String region) {
		return [SELECT Id From Region__c WHERE Region_Name__c = :region AND Status__c = 'A'];
	}
	
	private static Country__c getCountryByDesc(String country) {
		return [SELECT Id From Country__c WHERE Name = :country];
	}
	
	private static Employee_Type__c getEmployeeTypeByDesc(String empType) {
		return [SELECT Id From Employee_Type__c WHERE Name = :empType];
	}
}