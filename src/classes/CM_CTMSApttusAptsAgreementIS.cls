/*
    @author:        Sukrut Wagh
    @description:   Service responsible for integrating sObject=Apttus__APTS_Agreement__c with ETMF
                    
                    INT_SETUP_STATUS__c.Unique_Key__c=Apttus__APTS_Agreement__c.Id
                    Ex: a7qL0000000CacPIAS
    
*/

public class CM_CTMSApttusAptsAgreementIS extends COM_BaseIntegrationService {
    
    private static final String CLASSNAME = 'CM_CTMSApttusAptsAgreementIS';
    private static final COM_Logger LOGGER = new COM_Logger(CLASSNAME); 

    public CM_CTMSApttusAptsAgreementIS() {
        super();
    }
    
    public CM_CTMSApttusAptsAgreementIS(final String intSystem, final String sObjType) {
        super(intSystem,sObjType);
    }
    
    public String getProcessingQuery(){
        String methodName = 'getProcessingQuery';
        LOGGER.entry(methodName);
        String query = '';
        LOGGER.info(query);
        LOGGER.exit(methodName);
        return query;
    }
    
    public void prepare(){
        
    }
    
    public COM_ServiceResult invoke(List<sObject> scope){
        COM_ServiceResult result = new COM_ServiceResult();
        if(COM_Utils.isNotEmpty(scope) && scope.get(0) instanceof Apttus__APTS_Agreement__c) {
            result.setRequest(scope);
            List<Exception> errors = new List<Exception>();
            try {
                //TODO: perform processing
            } catch(COM_Exception ex) {
                LOGGER.error(ex);
                errors.add(ex);
            } finally {
                result.setErrors(errors);
                //result.setResponse(response);
                //Upsert INT_SETUP_STATUS__c 
            }
        }
        return result;
    }
    
    public Boolean processResult(List<COM_ServiceResult> results){
        if(COM_Utils.isNotEmpty(results)) {
            for(COM_ServiceResult result:   results) {
                
            }
        }
        return true;
    }
    
    public String getEmailRecipients() {
        return CM_Constants.CTMS_AGREEMENT_EMAIL_RECIPIENT;
    }

}