global class RM_RemoveAvailabilityBatch extends PRA_BatchaQueueable{
	global RM_RemoveAvailabilityBatch() {
        
    }

    //Set the parameters over the variables
    global override void  setBatchParameters(String parametersJSON){
       // List<String> params = (List<String>) Json.deserialize(parametersJSON, List<String>.class);
       // query = params.get(0);    
     }
    
    global override Database.QueryLocator start(Database.BatchableContext BC) {
    	 Date todayDate = system.today();
	    string todayMonth = RM_Tools.GetYearMonth(0);
	    String query = 'SELECT id, Employee_ID__c, year_month__c, (select year_month__c from assignments__r where assigned_value__c > 0 ) ' 
	    + ' FROM WFM_Employee_Availability__c where employee_id__r.status_desc__c = \'Terminated\' and employee_id__r.term_date__c < :todayDate and year_month__c > :todayMonth';
	    system.debug('---------query---------'+query  );
        return Database.getQueryLocator(query);
    }
    
    global override void execute(Database.BatchableContext BC, List<sObject> scope) {
	    list<WFM_Employee_Availability__c> lstAva = new list<WFM_Employee_Availability__c>();
	    for(Sobject s : scope) {	    	
		      if(s instanceof WFM_Employee_Availability__c) {
		      	system.debug('---------s---------'+s  );
		        WFM_Employee_Availability__c a = (WFM_Employee_Availability__c)s;
		    
		        if (a.assignments__r!= null)
		        	lstAva.add(a);
		      }
	    }
	    system.debug('---------lstAva to Delete---------'+lstAva.size()  );
	    if(lstAva != null && lstAva.size() > 0) {
	       delete lstAva;
	    }
    }
    
    global override void finish(Database.BatchableContext BC) {
    
    }
}