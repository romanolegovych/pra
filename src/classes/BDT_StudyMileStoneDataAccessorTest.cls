/** Implements the test for the Selector Layer of the object StudyMileStone__c
 * @author	Dimitrios Sgourdos
 * @version	25-Feb-2014
 */
@isTest
private class BDT_StudyMileStoneDataAccessorTest {
	
	/** Test the function getStudyMileStones
	 *	The function will be used for calls from Service Layer. By just call the function in the test, we can check
	 *	that its syntax in the query is correct.
	 * @author	Dimitrios Sgourdos
	 * @version	25-Feb-2014
	 */
	static testMethod void getStudyMileStonesTest() {
		List<StudyMileStone__c> results = BDT_StudyMileStoneDataAccessor.getStudyMileStones(NULL, NULL);
	}
}