/**
*   'HR_QuestionnaireCVO' class is a composite value object which is used to bind HR_Exit_Interview_Questionnaire__c and 
*   Questionnaire__c objects into one list.
*   @author   Devaram Bhargav
*   @version  26-Sep-2013
*   @since    17-Sep-2013
*/
public class HR_QuestionnaireCVO {
    /**
    *   'HR_QuestionnaireCVO' is to create a list that binds with HR_Exit_Interview_Questionnaire__c and ,
    *   HR_Questionnaire__c object into one list with even the picklist options. 
    */
    
    /** EXIquestionnaire variable is to hold the HR_Exit_Interview_Questionnaire__c record*/    
    public HR_Exit_Interview_Questionnaire__c EXIquestionnaire{get;set;}
    
    /** Questionnaire variable is to hold the Questionnaire__c record*/
    public Questionnaire__c Questionnaire{get;set;}
    
    /** QuestionChoices variable is to hold all the picklist values from Question_Choices__c of Questionnaire__c*/
    public String QuestionChoices{set;get;} 
    
    /** PicklistResponse variable is the value
    *   selected by the user from Question_Choices__c of Questionnaire__c*/
    public String PicklistResponse{get;set;} 
    
    /** MultiPickListResponse variable is the values 
    *   selected by the user from Question_Choices__c of Questionnaire__c*/
    public String[] MultiPickListResponse{get;set;}
    
    /** This method is to get the reponses back from HR_Questionnaire__c into picklist/Multipicklist 
    *   in the edit mode to select by the user.*/
    public void getResponsesintoPicklists(){
         system.debug('%%%%%%%%%%%%%%5'+EXIquestionnaire.Response__c +'%%%%%%%'+Questionnaire.Question_Type__c);
        if(Questionnaire.Question_Type__c=='Picklist' && EXIquestionnaire.Response__c!=null){
            PicklistResponse= (EXIquestionnaire.Response__c).replace(';', '');                   
        }else if(Questionnaire.Question_Type__c=='Multi-picklist' && EXIquestionnaire.Response__c!=null){                                                             
            MultiPicklistResponse= (EXIquestionnaire.Response__c).split(',');
            system.debug('%%%%%%%%%%%%%%5'+EXIquestionnaire.Response__c);
        }                 
        
    }
    
    /** This method is to make the picklistck from Question_Choices__c of Questionnaire__c
    *   in the edit mode to select by the user.*/      
    public List<SelectOption> getPicklistoptions(){
        List<SelectOption> options=new List<SelectOption>(); 
        options.add(new SelectOption('',''));            
        for(String obj:questionnaire.Question_Choices__c.split(';')){        
            options.add(new selectOption(obj,obj));
            //options.sort();
            system.debug('--------options------------'+options);
        }
        return options;
    }
    
    /** This is the constructor of the class  
    *   HR_QuestionnaireCVO.*/  
    public  HR_QuestionnaireCVO() {
                      
    }     
}