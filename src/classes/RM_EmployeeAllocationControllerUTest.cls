/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_EmployeeAllocationControllerUTest {
 	static COUNTRY__c country;
    static list<WFM_Location_Hours__c> hrs;
    static Employee_Details__c empl;
    static list<WFM_Employee_Availability__c> avas;
    static List<WFM_Employee_Assignment__c> assigns;
    static WFM_employee_Allocations__c alloc;
   
    static WFM_Client__c client;
    static WFM_Contract__c  contract;
    static WFM_Project__c project;
    static list<WFM_EE_Work_History__c> history;
   
    
    
    static void init(){
        list<string> months = RM_Tools.GetMonthsList(-4, 5);
        country = new COUNTRY__c(name='TestCountry',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
        daily_Business_Hrs__c=8.0);
        insert country;
        
       hrs = new List<WFM_Location_Hours__c>();
        for (string month:months){
            WFM_Location_Hours__c hr = new WFM_Location_Hours__c(location_code__c = 'TTT', Loc_Hour_ExtID__c='TTT:' + month,YEAR_MONTH__c=month,NBR_Business_Day_In_Month__c=22);
            hrs.add(hr); 
        }
        insert hrs;  
        empl = new Employee_Details__c(COUNTRY_name__c = country.id, name='1000', Employee_Unique_Key__c='1000', First_Name__c='John', Last_Name__c='Smith',email_Address__c='JSmith@gmail.com', RM_Comment__c='Test',
        Function_code__c='PR',Buf_Code__c='KCIPR', Business_Unit__c='BUC', Status__c='AA', Date_Hired__c=date.parse('12/27/2009'), FTE_Equivalent__c=1 );
        insert empl;
        avas = new list<WFM_Employee_Availability__c>();
      
        for (integer i = 0; i <months.size(); i++){
            WFM_Employee_Availability__c a = new WFM_Employee_Availability__c(EMPLOYEE_ID__c=empl.id, year_month__c=months[i], Availability_Unique_Key__c=empl.id+months[i],  
            Location_Hours__c=hrs[i].id);
            avas.add(a);
        }
        insert avas;
        
    	
       
        client = new WFM_Client__c(name='TestClient', Client_Name__c='TestClinet_Name', Client_Unique_Key__c='TestClient');
        insert client;
        contract = new WFM_Contract__c(name='TestContract', Contract_Status__c='AA', Client_ID__c=client.id, Contract_Unique_Key__c='TestContract');
        insert contract;
        project = new WFM_Project__c(name='TestProject', Contract_ID__c=contract.id, Project_Unique_Key__c='TestProject');
        insert project;
       	
       	alloc= new WFM_employee_Allocations__c(EMPLOYEE_ID__c=empl.id, Project_Buf_Code__c='KCIPR',Project_Country__c='United State', Project_Region__c='North America', Project_ID__c=project.id, Project_Function__c='Clinical Research Associate', 
            Status__c='Confirmed', Allocation_Unique_Key__c=empl.name+project.name+'KCICRUnited State');
      
        insert alloc;
        //assignment
        assigns = new List<WFM_Employee_Assignment__c>();
        for (Integer i = 0; i < months.size(); i++){    
            WFM_Employee_Assignment__c ass =  new WFM_Employee_Assignment__c(Assignment_Unique_Key__c=avas[i].id+(string)alloc.id, EMPLOYEE_ID__c = empl.id, Availability_FK__c=avas[i].id, Allocation_Key__c=alloc.id, Year_Month__c=months[i], 
            Assigned_Value__c=20, Assigned_Unit__c = 'Hours');
            
            assigns.add(ass);
        }
        insert assigns;
      
        // four month history
        list <string> monthHistry = RM_Tools.GetMonthsList(-4, 0);
        history = new list<WFM_EE_Work_History__c>();
    
        for (integer i = 0; i < monthHistry.size(); i++){
            WFM_EE_Work_History__c h = new WFM_EE_Work_History__c(Project_ID__c=project.id, employee_id__c=empl.id, Availability__c = avas[i].id, Work_History_Unique_Key__c=empl.id+(string)project.id+monthHistry[i]+'KCIPR',Buf_Code__c='KCIPR',year_month__c=monthHistry[i], Hours__c=20);
            history.add(h);
        }
        insert history;
      
      
    }
     static testMethod void TestAllocationHistry(){
         
        init();
        system.debug('---history---' + history);
        list<WFM_EE_Work_History__c> lstHist = [SELECT cal_FTE__c, Hours__c, year_month__c, project_id__r.name 
        FROM WFM_EE_Work_History__c where Employee_ID__c=:empl.id order by year_month__c];
        
        system.debug('---lstHist---' + lstHist);
        Test.startTest();
        PageReference pageRef = new PageReference('/apex/RM_EmployeeAllocation?ID='+ empl.id );
        Test.setCurrentPage(pageRef);
        RM_EmployeeAllocationController allocController = new RM_EmployeeAllocationController();
        
        List<RM_EmployeeAllocationController.allocationReview> alloctHist = allocController.getAllocation();
        system.assert(alloctHist.size() == 1);
        system.debug('---alloctHist---' + alloctHist);
        system.assert(alloctHist[0].actHr[0] == '20.0');
        system.assert(alloctHist[0].assHr[0] == '20.00');
      }
}