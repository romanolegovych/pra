public class IPA_DepNewsComponentController
{
    public IPA_Page_Widget__c pageWidgetObj {get; set;}
    public string showDiv {get; set;}
    public List<IPA_Articles__c> lstNews {get; set;}

    public void getfetchContents() 
    {
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        if(pageWidgetObj != null)
        {
            lstNews = ipa_bo.returnDepartmentNews(pageWidgetObj);
            if(lstNews.size() == 0)
                hide();
            else
                showDiv = '';
        }
        else
            hide();
    }
    
    private void hide()
    {
        lstNews = new List<IPA_Articles__c>();
        showDiv = 'hidden';
    }
}