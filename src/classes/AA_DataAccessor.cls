public class AA_DataAccessor {

    public static list<Holiday_Schedule__c> getHolidayByCountryYear(string country, decimal Year){
        return [Select Holiday_Name__c, Holiday_Date__c, Holiday_Country_State__r.Country__r.name, Holiday_Country_State__r.State_Province__r.name , Duration_of_Day__c, Status__c 
            from Holiday_Schedule__c 
            WHERE Holiday_Country_State__r.Country__r.Name =: country AND Year__c =: Year 
            ORDER BY LastModifiedDate DESC NULLS LAST];
    }
    
    public static Holiday_Schedule__c getHolidayDetailsById(string Id){
        return [SELECT Holiday_Name__c, Holiday_Date__c, Duration_of_Day__c
            FROM Holiday_Schedule__c
            WHERE ID =: Id limit 1];                 
    } 
   
    public static Holiday_Schedule__c getHolidayToDelete(String Id){
        return [SELECT id, name 
            FROM Holiday_Schedule__c 
            WHERE id=:Id limit 1];
    }
    public static List<Holiday_schedule__c> getHolidayToApprove(string country, decimal year, string status){
        return [SELECT Holiday_Name__c,  Holiday_Country_State__c,  Holiday_Date__c, Duration_of_Day__c, Status__c 
            FROM Holiday_Schedule__c 
            WHERE Holiday_Country_State__r.Country__r.Name =: country AND Year__c =: year AND Status__c =: status];
    }
    
    public static List<Holiday_Country_State__c>  getStateList(String country){
        return [SELECT State_Province__r.Name, Id 
            FROM Holiday_Country_State__c 
            WHERE Country__r.Name =: country]; 
    }
    
    public static List<Holiday_Schedule__c> getHolidayToClone(string country, decimal year){
       return [SELECT Holiday_Name__c, Holiday_Country_State__c,  Holiday_Date__c, Duration_of_Day__c 
            FROM Holiday_Schedule__c 
            WHERE Holiday_Country_State__r.Country__r.Name =: country AND Year__c =: year]; 
    }
    
    public static List<Country__c> getCountryList(boolean office){
       return [SELECT Name 
            FROM Country__c 
            WHERE Is_PRA_Office__c =: office];
    }                    
 }