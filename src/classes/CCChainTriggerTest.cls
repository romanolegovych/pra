@istest public with sharing class CCChainTriggerTest {
	@istest private static void testLoadDashboard() {
		Map<String, SObject> sobjectMap = CCApiTest.setup();
		STSWR1__Flow__c flow = (STSWR1__Flow__c) sobjectMap.get('Flow');
		
		flow.STSWR1__Source_Id__c = sobjectMap.get('PAWS Project').Id;
		update flow;
		update sobjectMap.get('Critical Chain');
		
		Critical_Chain_Milestone__c milestone = new Critical_Chain_Milestone__c(Name = 'A');
		insert milestone;
		
		EW_Aggregated_Milestone__c aggregatedMilesone = new EW_Aggregated_Milestone__c(
			Name = 'TEST'
		);
		
		insert aggregatedMilesone;
		for (Critical_Chain__c each : [select Name from Critical_Chain__c])
		{
			insert new EW_Aggregated_Milestone_EW_Junction__c(Aggregated_Milestone__c = aggregatedMilesone.Id, Early_Warning__c = each.Id);
		}
		
		Critical_Chain__c chainRecord = (Critical_Chain__c) sobjectMap.get('Critical Chain').clone();
		chainRecord.Cloned_From_Early_Warning__c = sobjectMap.get('Critical Chain').Id;
		chainRecord.Milestone__c = milestone.Id;
		
		insert chainRecord;
		
		System.assert([select count() from Critical_Chain__c where PAWS_Project_Flow__c != null] > 0);
	}
}