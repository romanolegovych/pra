/**
@author 
@date 2014
@description this controller class is for clone the model
**/
public with sharing class SRM_ModelCloneDataAccessor {
    
      public static String ListofSRMModels(String srmModelId){
      	
         String modelQuery = 'select '+retrieveAllfields('SRM_Model__c')+' from SRM_Model__c where id=\''+srmModelId+'\'';
         return modelQuery;
      }
      
      public static String ListofSiteActivations(SRM_Model__c srmModelObj){
      	
         String SiteActivationQuery = 'select '+retrieveAllfields('SRM_Model_Site_Activation__c')+' from SRM_Model_Site_Activation__c where SRM_Model__c=\''+srmModelObj.id+'\'';
         return SiteActivationQuery;
      }
      
      public static String ListofSubjectEnrollments(SRM_Model__c srmModelObj){
      	
         String SubjectEnrollmentquery='select '+retrieveAllfields('SRM_Model_Subject_Enrollment__c')+' from SRM_Model_Subject_Enrollment__c where SRM_Model__c=\''+srmModelObj.id+'\'';
         return SubjectEnrollmentquery;
      }
      
      public static String ListofsrmQuestions(SRM_Model__c srmModelObj){
      	
         String Questionsquery='select '+retrieveAllfields('SRM_Model_Questions__c')+',(Select '+retrieveAllfields('SRM_Model_Response__c')+' from SRM_Model_Response__r) from SRM_Model_Questions__c where SRM_Model__c=\''+srmModelObj.id+'\'';
         return Questionsquery;
      }
      
      public static String ListofWeekadjustments(SRM_Model__c srmModelObj){
      	
         String Weekadjustmentquery='select '+retrieveAllfields('SRM_Week_Adjustments__c')+' from SRM_Week_Adjustments__c where SRM_Model__c=\''+srmModelObj.id+'\''; 
         return Weekadjustmentquery;
      }
      
      public static String Listofcalenderadjustments(SRM_Model__c srmModelObj){
      	
         String calenderadjustmentquery='select '+retrieveAllfields('SRM_calender_Adjustments__c')+' from SRM_calender_Adjustments__c where SRM_Model__c=\''+srmModelObj.id+'\''; 
         return calenderadjustmentquery;
      }  
       
      public static String retrieveAllfields(String objectName){
      	
            Map<String, Schema.SObjectType> gd = Schema.getGlobalDescribe();        
            Map<String,Schema.SObjectField> M = gd.get(objectName).getDescribe().fields.getMap();       
            string fieldnames=' ';
            for(Schema.SObjectField s:m.values()){ 
                    if(s.getDescribe().isAccessible() && !s.getDescribe().getName().equalsIgnoreCase('TotalPrice'))                               
                        fieldnames+=s+',';  //Here we concatenating all field names with comma seperation for preparing SOQL query 
            }
                
            fieldnames=fieldnames.substring(0,fieldnames.length()-1);//Fieldnames string contains all the fields with comma separation
            return fieldnames;
      }
      
      public static List<Country__c> countries(){   
      	    
        List<Country__c> countryList= [SELECT Id,Name FROM Country__c ORDER BY Name limit 50000];
        return countryList;       
      }
      
      public static SRM_Model_Questions__c modelQuestions(String Id){
      	
        SRM_Model_Questions__c modelQuestionsList = [select Id,Name,SRM_Model__c,Therapeutic_Area__c,Sub_Group__c,Required_for_Approval__c,Question__c,Primary_Role__c,Information__c,
                      OldquestionId__c,Dependent_Question__c,Group__c,Country__c,Selecting_Country__c from SRM_Model_Questions__c where Id =:id limit 1];
        return modelQuestionsList;                       
      }
}