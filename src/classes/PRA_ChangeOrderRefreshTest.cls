@isTest (SeeAllData=false)
private class PRA_ChangeOrderRefreshTest {

    static testMethod void shouldRefreshChangeOrderSOurceValues() {
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        //tu.createBidContractDetails();
        
        Change_Order__c changeOrder = tu.createChangeOrder(tu.clientProject.id);
        Change_Order_Item__c coItem = [select (select BUF_Code__c, Current_Contract_Hours__c, Current_Contract_Value__c, Current_Hours_Unit__c, 
                                                Current_Total_Cost__c, Current_Unit_Cost__c, Id, Type__c from Change_Order_Line_Item__r), 
                                             Change_Order__c, Client_Task__c, Client_Unit_Number__c, Combo_Code__c, Description__c, End_Date__c, Forecast_Curve__c, Number_Of_Contracted_Units__c, 
                                             Project_Region__c, Start_Date__c, Task_Group__c, Total_Cost__c, Type__c, Unit_Completed_Event_Lookup__c, 
                                             Unit_Cost__c, Unit_of_Measurement__c, Worked_Hours__c,  Worked_Units__c 
                                         from Change_Order_Item__c
                                         where Change_Order__c = :changeOrder.id limit 1];
        
        system.debug('------------------- coItem list --------------------' + coItem);
        system.debug('-------------------------- COITEM DESCRIPTION -----------------------------' + coItem.Description__c);
        Change_Order_Line_Item__c coLineItem = coItem.Change_Order_Line_Item__r.get(0);
        coLineItem.Type__c = PRA_ChangeOrderController.CHANGE_ORDER_ITEM_STATUS_NEW; // chack if type is changed
        update coLineItem;
        
        Client_Task__c task = [select Client_Unit_Number__c, Combo_Code__c, Description__c,
                                                    Forecast_Curve__c, Id, Original_End_Date__c, 
                                                    Project__c, Revised_End_Date__c, Start_Date__c, 
                                                    Status__c, Task_Group__c, Unit_completed_event_id__c, 
                                                    Unit_of_Measurement__c, 
                                                    Total_Worked_Hours__c, Total_Worked_Units__c,
                                                    Average_Contract_Effort__c, Total_Contract_Hours__c, Total_Contract_Units__c
                                                //(select BUF_Code__c, Type__c from Effort_Ratios__r where Type__c = 'Baseline')
                                                from Client_Task__c
                                                where id = :coItem.Client_Task__c];

        //Effort_Ratio__c er = task.Effort_Ratios__r.get(0);
        List<Bid_Contract_Detail__c> bidDetails = [select Client_Task__c, BUF_Code__c, Contracted_Hours__c, Contracted_Value__c, Id from Bid_Contract_Detail__c];
        
        // when
        
        // change project end date
        tu.clientProject.Estimated_End_Date__c = date.today().addMonths(36);
        update tu.clientProject;
        
        // update client task
        task.Client_Unit_Number__c   = 'UN1';
        //task.Combo_Code__c           = 'Combo1';
        task.Combo_Code__c           = 'Com';
        task.Description__c          = 'Changef Unit Name';
        task.Forecast_Curve__c       = 'Managment curve';
        task.Start_Date__c           = date.today();
        update task;
        
        // update task - worked units
        
        Client_Task__c expectedTask = [select id, Total_Worked_Hours__c, Total_Worked_Units__c, Total_Contract_Units__c, Contract_Value__c 
                                                        from Client_Task__c where id = :task.id];
        
        //bid contract
        expectedTask.Total_Contract_Units__c += 5;
        expectedTask.Contract_Value__c += 10;
        update expectedTask;
        
        // bid contract details
        for(Bid_Contract_Detail__c bd : bidDetails) {
            bd.Contracted_Hours__c += 5;
            bd.Contracted_Value__c += 10;
            update bd;
        }
        
        // To check:
        
        // Change order
        // client project - date
        
        // Change Order item
        // client task - description, code, cdn.
        // task - cumulative hours, unit 
        // bid contract
        
        // change Order line item
        // bid contract details
        
        // then
        test.startTest();
        PRA_ChangeOrderRefresh.refreshChangeOrder(changeOrder.id);
        
        changeOrder = [select Id, Proposed_Estimated_End_Date__c, Is_Project_Updated__c, Last_Refreshed__c from Change_Order__c];
        coItem = [select Change_Order__c, Client_Task__c, Client_Unit_Number__c, Combo_Code__c, Description__c, End_Date__c, Forecast_Curve__c, 
                    Number_Of_Contracted_Units__c, Project_Region__c, Start_Date__c, Task_Group__c, Total_Cost__c, Type__c, Unit_Completed_Event_Lookup__c, 
                    Unit_Cost__c, Unit_of_Measurement__c, Worked_Hours__c,  Worked_Units__c 
                 from Change_Order_Item__c
                 where Change_Order__c = :changeOrder.Id and Description__c = :task.Description__c];
        coLineItem = [select BUF_Code__c, Current_Contract_Hours__c, Current_Contract_Value__c, Current_Hours_Unit__c,Current_Total_Cost__c, Current_Unit_Cost__c, 
                            Id, Type__c 
                        from Change_Order_Line_Item__c
                        where Change_Order_Item__c = :coItem.Id limit 1];
        
        // chech change order
        system.assertEquals(null, changeOrder.Proposed_Estimated_End_Date__c);
        system.assertEquals(false, changeOrder.Is_Project_Updated__c);
        system.assertNotEquals(null, changeOrder.Last_Refreshed__c);
        
        // check change order item
        system.assertEquals(task.Client_Unit_Number__c, coItem.Client_Unit_Number__c);
        system.assertEquals(task.Combo_Code__c, coItem.Combo_Code__c);
        system.assertEquals(task.Description__c, coItem.Description__c);
        system.assertEquals(task.Forecast_Curve__c, coItem.Forecast_Curve__c);
        system.assertEquals(task.Start_Date__c, coItem.Start_Date__c);
        
        system.assertEquals(expectedTask.Total_Worked_Hours__c, coItem.Worked_Hours__c);
        system.assertEquals(expectedTask.Total_Worked_Units__c, coItem.Worked_Units__c);
        
        // check change order line item
        //system.assertEquals(bidDetails.get(0).Contracted_Hours__c, coLineItem.Current_Contract_Hours__c);
        //system.assertEquals(bidDetails.get(0).Contracted_Value__c, coLineItem.Current_Contract_Value__c);
        //system.assertEquals(PRA_ChangeOrderController.CHANGE_ORDER_ITEM_STATUS_MODIFIED, coLineItem.Type__c);
         
        test.stopTest();
        
    }
    
    static testMethod void shouldRefreshChangeOrderAndAddNewCOLineItem() {
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        Change_Order__c changeOrder = tu.createChangeOrder(tu.clientProject.id);
        Change_Order_Item__c coItem = [select (select BUF_Code__c, Current_Contract_Hours__c, Current_Contract_Value__c, Current_Hours_Unit__c, 
                                                Current_Total_Cost__c, Current_Unit_Cost__c, Id, Type__c from Change_Order_Line_Item__r), 
                                             Change_Order__c, Client_Task__c, Client_Unit_Number__c, Combo_Code__c, Description__c, End_Date__c, Forecast_Curve__c, Number_Of_Contracted_Units__c, 
                                             Project_Region__c, Start_Date__c, Task_Group__c, Total_Cost__c, Type__c, Unit_Completed_Event_Lookup__c, 
                                             Unit_Cost__c, Unit_of_Measurement__c, Worked_Hours__c,  Worked_Units__c 
                                         from Change_Order_Item__c
                                         where Change_Order__c = :changeOrder.id limit 1];
        
        Client_Task__c task = [select Client_Unit_Number__c, Combo_Code__c, Description__c,
                                                    Forecast_Curve__c, Id, Original_End_Date__c, 
                                                    Project__c, Revised_End_Date__c, Start_Date__c, 
                                                    Status__c, Task_Group__c, Unit_completed_event_id__c, 
                                                    Unit_of_Measurement__c, 
                                                    Total_Worked_Hours__c, Total_Worked_Units__c,
                                                    Average_Contract_Effort__c, Total_Contract_Hours__c, Total_Contract_Units__c
                                                //(select BUF_Code__c, Type__c from Effort_Ratios__r where Type__c = 'Baseline')
                                                from Client_Task__c
                                                where id = :coItem.Client_Task__c];
        List<Unit_Effort__c> unitEfforts = [SELECT Id FROM Unit_Effort__c];
    
        // when
        // create new records - simulate new buf code added to task by other change order
        BUF_Code__c newBufCode = tu.getBufCode('New buf code', null, null, 'Europe Africa');
        insert newBufCode;
        System.debug('--------------unitEfforts------------------'+unitEfforts);
        System.debug('--------------size------------------'+unitEfforts.size());
        Hour_EffortRatio__c newEffRat = tu.getEffortRatio(unitEfforts.get(0).Id, newBufCode.id, 1, false);
        insert newEffRat;
        Bid_Contract_Detail__c newBidDetails = new Bid_Contract_Detail__c (Client_Task__c = coItem.Client_Task__c, BUF_Code__c = newBufCode.id, 
                                                                            Contracted_Hours__c = 123, Contracted_Value__c = 313);
        insert newBidDetails;
        
        // then
        test.startTest();
        PRA_ChangeOrderRefresh.refreshChangeOrder(changeOrder.id);
        
        changeOrder = [select Id, Proposed_Estimated_End_Date__c, Is_Project_Updated__c, Last_Refreshed__c from Change_Order__c where Id = :changeOrder.Id];
        coItem = [select Change_Order__c, Client_Task__c, Client_Unit_Number__c, Combo_Code__c, Description__c, End_Date__c, Forecast_Curve__c, 
                    Number_Of_Contracted_Units__c, Project_Region__c, Start_Date__c, Task_Group__c, Total_Cost__c, Type__c, Unit_Completed_Event_Lookup__c, 
                    Unit_Cost__c, Unit_of_Measurement__c, Worked_Hours__c,  Worked_Units__c 
                 from Change_Order_Item__c
                 where Id = :coItem.Id];
        Change_Order_Line_Item__c coLineItem = [select BUF_Code__c, Current_Contract_Hours__c, Current_Contract_Value__c, Current_Hours_Unit__c,Current_Total_Cost__c, Current_Unit_Cost__c, 
                            Id, Type__c 
                        from Change_Order_Line_Item__c
                        where BUF_Code__c = :newBufCode.Id and Change_Order_Item__c = :coItem.Id];
        
        // chech change order
        system.assertEquals(null, changeOrder.Proposed_Estimated_End_Date__c);
        system.assertEquals(false, changeOrder.Is_Project_Updated__c);
        system.assertNotEquals(null, changeOrder.Last_Refreshed__c);
        
        // check change order item
        system.assertEquals(task.Client_Unit_Number__c, coItem.Client_Unit_Number__c);
        system.assertEquals(task.Combo_Code__c, coItem.Combo_Code__c);
        system.assertEquals(task.Description__c, coItem.Description__c);
        system.assertEquals(task.Forecast_Curve__c, coItem.Forecast_Curve__c);
        system.assertEquals(task.Start_Date__c, coItem.Start_Date__c);
        
        // check change order line item
        system.assertEquals(newBidDetails.Contracted_Hours__c, coLineItem.Current_Contract_Hours__c);
        system.assertEquals(newBidDetails.Contracted_Value__c, coLineItem.Current_Contract_Value__c);
        
        test.stopTest();
        
    }
    
    static testMethod void shouldRefreshChangeOrderAndSetCrctForNewChangeOrderLineItem() {
       // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        //tu.createBidContractDetails();
        
        Change_Order__c changeOrder = tu.createChangeOrder(tu.clientProject.id);
        List<Change_Order_Item__c> coItem = [select (select BUF_Code__c, Current_Contract_Hours__c, Current_Contract_Value__c, Current_Hours_Unit__c, 
                                                Current_Total_Cost__c, Current_Unit_Cost__c, Id, Type__c from Change_Order_Line_Item__r), 
                                             Change_Order__c, Client_Task__c, Client_Unit_Number__c, Combo_Code__c, Description__c, End_Date__c, Forecast_Curve__c, Number_Of_Contracted_Units__c, 
                                             Project_Region__c, Start_Date__c, Task_Group__c, Total_Cost__c, Type__c, Unit_Completed_Event_Lookup__c, 
                                             Unit_Cost__c, Unit_of_Measurement__c, Worked_Hours__c,  Worked_Units__c 
                                         from Change_Order_Item__c
                                         where Change_Order__c = :changeOrder.id order by Client_Task__c];
        //system.debug('------------------- coItem size --------------------' + coItem.size());
        //system.debug('------------------- coItem list --------------------' + coItem);
        //Added End date in below line as it is failing 
        Change_Order_Item__c coNewItem = new Change_Order_Item__c(Change_Order__c = changeOrder.id, Type__c = PRA_ChangeOrderController.CHANGE_ORDER_ITEM_STATUS_NEW,
                                                                    Client_Unit_Number__c = coItem.get(0).Client_Unit_Number__c, End_Date__c = coItem.get(0).End_Date__c);
        
        
        Client_Task__c task = [select Client_Unit_Number__c, Combo_Code__c, Description__c,
                                                    Forecast_Curve__c, Id, Original_End_Date__c, 
                                                    Project__c, Revised_End_Date__c, Start_Date__c, 
                                                    Status__c, Task_Group__c, Unit_completed_event_id__c, 
                                                    Unit_of_Measurement__c, 
                                                    Total_Worked_Hours__c, Total_Worked_Units__c,
                                                    Average_Contract_Effort__c, Total_Contract_Hours__c, Total_Contract_Units__c
                                                //(select BUF_Code__c, Type__c from Effort_Ratios__r where Type__c = 'Baseline')
                                                from Client_Task__c
                                                where id = :coItem.get(0).Client_Task__c order by Id];
        //system.debug('------------------------- DESCRIPTION ---------------------------------' + task.Description__c);
        //Effort_Ratio__c er = task.Effort_Ratios__r.get(0);
        
        delete coItem;
        
        // when
        insert coNewItem;
                
        Client_Task__c expectedTask = [select Id, Total_Worked_Hours__c, Total_Worked_Units__c from Client_Task__c where Id = :task.Id];
        
        // To check:
        
        // Change order
        // client project - date
        
        // Change Order item
        // client task - description, code, cdn.
        // task - cumulative hours, unit 
        // bid contract
        
        // change Order line item
        // bid contract details
        
        // then
        test.startTest();
        PRA_ChangeOrderRefresh.refreshChangeOrder(changeOrder.id);
        
        changeOrder = [select Id, Proposed_Estimated_End_Date__c, Is_Project_Updated__c, Last_Refreshed__c from Change_Order__c where id = :changeOrder.id];
        coItem = [select Change_Order__c, Client_Task__c, Client_Unit_Number__c, Combo_Code__c, Description__c, End_Date__c, Forecast_Curve__c, 
                    Number_Of_Contracted_Units__c, Project_Region__c, Start_Date__c, Task_Group__c, Total_Cost__c, Type__c, Unit_Completed_Event_Lookup__c, 
                    Unit_Cost__c, Unit_of_Measurement__c, Worked_Hours__c,  Worked_Units__c 
                 from Change_Order_Item__c
                 where id = :coNewItem.id order by Client_Task__c];
        
        //system.debug('-------------------------- COITEM DESCRIPTION -----------------------------' + coItem.get(0).Description__c);

        // chech change order
        system.assertEquals(null, changeOrder.Proposed_Estimated_End_Date__c);
        system.assertEquals(false, changeOrder.Is_Project_Updated__c);
        system.assertNotEquals(null, changeOrder.Last_Refreshed__c);
        
        // check change order item
        system.assertEquals(task.Client_Unit_Number__c, coItem.get(0).Client_Unit_Number__c);
        system.assertEquals(task.Combo_Code__c, coItem.get(0).Combo_Code__c);
        //system.assertEquals(task.Description__c, coItem.get(0).Description__c);
        system.assertEquals(task.Forecast_Curve__c, coItem.get(0).Forecast_Curve__c);
        system.assertEquals(task.Start_Date__c, coItem.get(0).Start_Date__c);
        
        //system.assertEquals(expectedTask.Total_Worked_Hours__c, coItem.get(0).Worked_Hours__c);
        //system.assertEquals(expectedTask.Total_Worked_Units__c, coItem.get(0).Worked_Units__c);
 
        test.stopTest();
        
    }
}