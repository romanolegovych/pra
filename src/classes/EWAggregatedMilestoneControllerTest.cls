@istest public with sharing class EWAggregatedMilestoneControllerTest
{
	@testsetup public static void setup()
	{
		EWBatchTest.setup();
	}
	
	@istest private static void cover()
	{
		EW_Aggregated_Milestone__c aggregatedMilesone = new EW_Aggregated_Milestone__c(
			Name = 'TEST'
		);
		
		insert aggregatedMilesone;
		Test.setCurrentPage(Page.EWAggregatedMilestoneEdit);
		ApexPages.currentPage().getParameters().put('Id', aggregatedMilesone.Id);
		
		for (Critical_Chain__c each : [select Name from Critical_Chain__c])
		{
			insert new EW_Aggregated_Milestone_EW_Junction__c(Aggregated_Milestone__c = aggregatedMilesone.Id, Early_Warning__c = each.Id);
		}
		
		EWAggregatedMilestoneController controller = new EWAggregatedMilestoneController(new ApexPages.StandardController(aggregatedMilesone));
		controller.save();
		System.assert(controller.junctions == null);
		controller.onPAWSProjectFlowChange();
		try
		{
			controller.saveWrappers();
		}
		catch (Exception e)
		{
			//do nothing
		}
	}
}