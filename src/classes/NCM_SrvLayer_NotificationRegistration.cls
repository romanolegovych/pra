/**
 * @description	The Service Layer for the Notification Registrations
 * @author		Dimitrios Sgourdos
 * @date		Created: 01-Sep-2015, Edited: 25-Sep-2015
 */
public with sharing class NCM_SrvLayer_NotificationRegistration {
	
// *********************************************************************************************************************
//				DataTypes with objects transformations
// *********************************************************************************************************************
	
	/**
	 * @description	Retrieve a notification registration instance with attributes filled from the corresponding wrapper
	 *				data type. The record that will be refreshed with new values is passed as parameter in order to
	 *				capture situations that we want to refresh an existing (already stored in the DB) record.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015, Edited: 09-Sep-2015
	 * @param		source    The wrapper that holds the values for the attributes of the Notification Registration Record
	 * @param		result    The Notification Registration record to be updated with the attributes from the wrapper item
	 * @return		The refreshed Notification Reigistration record.
	*/
	public static NotificationRegistration__c getNotificationRegistrationFromWrapperData(
																NCM_API_DataTypes.NotificationRegistrationWrapper source,
																NotificationRegistration__c result) {
		// Check for invalid parameters
		if(source == NULL) {
			return result;
		}
		
		if(result == NULL) {
			result = new NotificationRegistration__c();
		}
		
		// Update the registration record
		result.NotificationCatalog__c	= source.notificationCatalogId;
		result.UserId__c				= source.userId;
		result.ReminderPeriod__c		= source.reminderPeriod;
		result.MaxNumberReminders__c	= (source.maxNumberReminders == NULL)?
																	NCM_API_DataTypes.DEFAULT_MAX_NUMBER_OF_REMINDERS :
																	source.maxNumberReminders;
		result.RelatedToId__c			= source.relatedToId;
		result.Active__c				= (source.Active == NULL)? true : source.Active;
		result.notifyByEmail__c			= (source.notifyByEmail == NULL)? false : source.notifyByEmail;
		result.notifyBySMS__c			= (source.notifyBySMS == NULL)? false : source.notifyBySMS;
		result.notifyByChatter__c		= (source.notifyByChatter == NULL)? false : source.notifyByChatter;
		
		return result;
	}
	
// *********************************************************************************************************************
//				Build / Search for Notification Registrations
// *********************************************************************************************************************
	
	/**
	 * @description	Build registrations in the 'NotificationRegistrationWrapper' format, to let users register for a
	 *				notification topic. The data includes all the current active registrations for the given user and
	 *				for the given related object id (thus also for the given category), but also registrations 
	 *				instances for the notification topics that the user isn't subscribed (for the given related object
	 *				id). The data are ordered by the notification topic name.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	 * @param		userId                  The id of the user that is assigned with the registrations
	 * @param		relatedToId             The related object id that is connected with the registrations
	 * @param		notificationCategory    The category of the notification topics that are connected with the
	 *										registrations
	 * @return		The registration data in the 'NotificationRegistrationWrapper' format including existing
	 *				registrations and possible new registrations.
	*/
	public static List<NCM_API_DataTypes.NotificationRegistrationWrapper> buildAllRegistrationsForNotifications(
																							ID userId,
																							String relatedToId,
																							String notificationCategory) {
		List<NCM_API_DataTypes.NotificationRegistrationWrapper> results = new List<NCM_API_DataTypes.NotificationRegistrationWrapper>();
		
		// Find the Notification Catalogs and build the notification registration results
		List<NotificationCatalog__c> catalogTopics = NCM_DataAccessor.getNotificationTopicsWithActiveSubscriptions(
																								userId,
																								relatedToId,
																								notificationCategory);
		
		for(NotificationCatalog__c tmpTopic : catalogTopics) {
			NCM_API_DataTypes.NotificationCatalogWrapper catalogWrapperItem = new NCM_API_DataTypes.NotificationCatalogWrapper(tmpTopic);
			
			NCM_API_DataTypes.NotificationRegistrationWrapper regWrapperItem = new NCM_API_DataTypes.NotificationRegistrationWrapper();
			
			if(tmpTopic.NotificationRegistrations__r == NULL ||  tmpTopic.NotificationRegistrations__r.isEmpty()) {
				regWrapperItem.notificationCatalogId = tmpTopic.Id;
				regWrapperItem.userId				= userId;
				regWrapperItem.relatedToId			= relatedToId;
				regWrapperItem.reminderPeriod		= tmpTopic.DefaultReminderPeriod__c;
				regWrapperItem.maxNumberReminders	= NCM_API_DataTypes.DEFAULT_MAX_NUMBER_OF_REMINDERS;
				regWrapperItem.active				= false;
				regWrapperItem.notifyByEmail		= false;
				regWrapperItem.notifyBySMS			= false;
				regWrapperItem.notifyBychatter		= false;
			} else {
				// For a specific notification topic, specific user and specific related to object, it can be only
				// one record
				regWrapperItem = new NCM_API_DataTypes.NotificationRegistrationWrapper(	tmpTopic.NotificationRegistrations__r[0],
																						false);
			}
			
			regWrapperItem.parentTopicData = catalogWrapperItem;
			
			results.add(regWrapperItem);
		}
		
		return results;
	}
	
	
	/**
	 * @description	Build registrations in the 'NotificationRegistrationWrapper' format, to let users register for a
	 *				notification topic. The data includes all the current active registrations for the given user and
	 *				for the given category, but also registration instances for the notification topics that the user
	 *				isn't subscribed. The data are ordered by the notification topic name.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	 * @param		userId                  The id of the user that is assigned with the registrations
	 * @param		notificationCategory    The category of the notification topics that are connected with the
	 *										registrations
	 * @return		The registration data in the 'NotificationRegistrationWrapper' format including existing
	 *				registrations and possible new registrations.
	*/
	public static List<NCM_API_DataTypes.NotificationRegistrationWrapper> buildAllRegistrationsForNotifications(
																							ID userId,
																							String notificationCategory) {
		List<NCM_API_DataTypes.NotificationRegistrationWrapper> results = new List<NCM_API_DataTypes.NotificationRegistrationWrapper>();
		
		// Find the Notification Catalogs and build the notification registration results
		List<NotificationCatalog__c> catalogTopics = NCM_DataAccessor.getNotificationTopicsWithActiveSubscriptions(
																								userId,
																								notificationCategory);
		
		for(NotificationCatalog__c tmpTopic : catalogTopics) {
			NCM_API_DataTypes.NotificationCatalogWrapper catalogWrapperItem = new NCM_API_DataTypes.NotificationCatalogWrapper(tmpTopic);
			
			NCM_API_DataTypes.NotificationRegistrationWrapper regWrapperItem = new NCM_API_DataTypes.NotificationRegistrationWrapper();
			
			if(tmpTopic.NotificationRegistrations__r == NULL ||  tmpTopic.NotificationRegistrations__r.isEmpty()) {
				regWrapperItem.notificationCatalogId = tmpTopic.Id;
				regWrapperItem.userId				= userId;
				regWrapperItem.relatedToId			= NULL;
				regWrapperItem.reminderPeriod		= tmpTopic.DefaultReminderPeriod__c;
				regWrapperItem.maxNumberReminders	= NCM_API_DataTypes.DEFAULT_MAX_NUMBER_OF_REMINDERS;
				regWrapperItem.active				= false;
				regWrapperItem.notifyByEmail		= false;
				regWrapperItem.notifyBySMS			= false;
				regWrapperItem.notifyBychatter		= false;
				regWrapperItem.parentTopicData = catalogWrapperItem;
				results.add(regWrapperItem);
			} else {
				List<NotificationRegistration__c> tmpRegList = tmpTopic.NotificationRegistrations__r;
				for(NotificationRegistration__c tmpReg : tmpRegList) {
					regWrapperItem = new NCM_API_DataTypes.NotificationRegistrationWrapper(	tmpReg, false);
					regWrapperItem.parentTopicData = catalogWrapperItem;
					results.add(regWrapperItem);
				}
			}
		}
		
		return results;
	}
	
	
	/**
	 * @description	Build active registrations in the 'NotificationRegistrationWrapper' format. The data includes all
	 *				the current active registrations for the given user and for the given related object id (thus also
	 *				for the given category). The data are ordered by the notification topic name.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	 * @param		userId                  The id of the user that is assigned with the registrations
	 * @param		relatedToId             The related object id that is connected with the registrations
	 * @param		notificationCategory    The category of the notification topic that are connected with the
	 *										registrations
	 * @return		The active registrations in the 'NotificationRegistrationWrapper' format
	*/
	public static List<NCM_API_DataTypes.NotificationRegistrationWrapper> buildActiveRegistrationsForNotifications(
																							ID userId,
																							String relatedToId,
																							String notificationCategory) {
		List<NCM_API_DataTypes.NotificationRegistrationWrapper> results = new List<NCM_API_DataTypes.NotificationRegistrationWrapper>();
		
		// Find the Notification Catalogs and build the notification registration results
		List<NotificationCatalog__c> catalogTopics = NCM_DataAccessor.getNotificationTopicsWithActiveSubscriptions(
																								userId,
																								relatedToId,
																								notificationCategory);
		
		for(NotificationCatalog__c tmpTopic : catalogTopics) {
			NCM_API_DataTypes.NotificationCatalogWrapper catalogWrapperItem = new NCM_API_DataTypes.NotificationCatalogWrapper(tmpTopic);
			
			NCM_API_DataTypes.NotificationRegistrationWrapper regWrapperItem = new NCM_API_DataTypes.NotificationRegistrationWrapper();
			
			if(tmpTopic.NotificationRegistrations__r != NULL && (!tmpTopic.NotificationRegistrations__r.isEmpty()) ) {
				// For a specific notification topic, specific user and specific related to object, it can be only
				// one record
				regWrapperItem = new NCM_API_DataTypes.NotificationRegistrationWrapper(	tmpTopic.NotificationRegistrations__r[0],
																						false);
				regWrapperItem.parentTopicData = catalogWrapperItem;
				results.add(regWrapperItem);
			}
		}
		
		return results;
	}
	
	
	/**
	 * @description	Build active registrations in the 'NotificationRegistrationWrapper' format. The data includes all
	 *				the current active registrations for the given user and for the given category.
	 *				The data are ordered by the notification topic name.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 17-Sep-2015
	 * @param		userId                  The id of the user that is assigned with the registrations
	 * @param		notificationCategory    The category of the notification topic
	 * @return		The active registrations in the 'NotificationRegistrationWrapper' format
	*/
	public static List<NCM_API_DataTypes.NotificationRegistrationWrapper> buildActiveRegistrationsForNotifications(
																							ID userId,
																							String notificationCategory) {
		List<NCM_API_DataTypes.NotificationRegistrationWrapper> results = new List<NCM_API_DataTypes.NotificationRegistrationWrapper>();
		
		// Find the Notification Catalogs and build the notification registration results
		List<NotificationCatalog__c> catalogTopics = NCM_DataAccessor.getNotificationTopicsWithActiveSubscriptions(
																								userId,
																								notificationCategory);
		
		for(NotificationCatalog__c tmpTopic : catalogTopics) {
			NCM_API_DataTypes.NotificationCatalogWrapper catalogWrapperItem = new NCM_API_DataTypes.NotificationCatalogWrapper(tmpTopic);
			
			NCM_API_DataTypes.NotificationRegistrationWrapper regWrapperItem = new NCM_API_DataTypes.NotificationRegistrationWrapper();
			
			if(tmpTopic.NotificationRegistrations__r != NULL && (!tmpTopic.NotificationRegistrations__r.isEmpty()) ) {
				List<NotificationRegistration__c> tmpRegList = tmpTopic.NotificationRegistrations__r;
				for(NotificationRegistration__c tmpReg : tmpRegList) {
					regWrapperItem = new NCM_API_DataTypes.NotificationRegistrationWrapper(	tmpReg, false);
					regWrapperItem.parentTopicData = catalogWrapperItem;
					results.add(regWrapperItem);
				}
			}
		}
		
		return results;
	}
	
	
	/**
	 * @description	Build registrations in the 'NotificationRegistrationWrapper' format.The built data are registrations
	 *				instances for the notification topics that the user isn't already subscribed (for the given related
	 *				object id). The data are ordered by the notification topic name.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	 * @param		userId                  The id of the user that will be assigned with the registrations
	 * @param		relatedToId             The related object id that will be connected with the registrations
	 * @param		notificationCategory    The category of the notification topics
	 * @return		The registration instances in the 'NotificationRegistrationWrapper' format
	*/
	public static List<NCM_API_DataTypes.NotificationRegistrationWrapper> buildNonSubscribedRegistrationsForNotifications(
																							ID userId,
																							String relatedToId,
																							String notificationCategory) {
		List<NCM_API_DataTypes.NotificationRegistrationWrapper> results = new List<NCM_API_DataTypes.NotificationRegistrationWrapper>();
		
		// Find the Notification Catalogs and build the notification registration results
		List<NotificationCatalog__c> catalogTopics = NCM_DataAccessor.getNotificationTopicsWithActiveSubscriptions(
																								userId,
																								relatedToId,
																								notificationCategory);
		
		for(NotificationCatalog__c tmpTopic : catalogTopics) {
			NCM_API_DataTypes.NotificationCatalogWrapper catalogWrapperItem = new NCM_API_DataTypes.NotificationCatalogWrapper(tmpTopic);
			
			NCM_API_DataTypes.NotificationRegistrationWrapper regWrapperItem = new NCM_API_DataTypes.NotificationRegistrationWrapper();
			
			if(tmpTopic.NotificationRegistrations__r == NULL ||  tmpTopic.NotificationRegistrations__r.isEmpty()) {
				regWrapperItem.notificationCatalogId = tmpTopic.Id;
				regWrapperItem.userId				= userId;
				regWrapperItem.relatedToId			= relatedToId;
				regWrapperItem.reminderPeriod		= tmpTopic.DefaultReminderPeriod__c;
				regWrapperItem.maxNumberReminders	= NCM_API_DataTypes.DEFAULT_MAX_NUMBER_OF_REMINDERS;
				regWrapperItem.active				= false;
				regWrapperItem.notifyByEmail		= false;
				regWrapperItem.notifyBySMS			= false;
				regWrapperItem.notifyBychatter		= false;
				regWrapperItem.parentTopicData = catalogWrapperItem;
				
				results.add(regWrapperItem);
			}
		}
		
		return results;
	}
	
	
	/**
	 * @description	Build registrations in the 'NotificationRegistrationWrapper' format.The built data are registrations
	 *				instances for the notification topics that the user isn't already subscribed. The data are ordered
	 *				by the notification topic name.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 17-Sep-2015
	 * @param		userId                  The user that is assigned with the registrations
	 * @param		notificationCategory    The category of the notification topic
	 * @return		The registration instances in the 'NotificationRegistrationWrapper' format
	*/
	public static List<NCM_API_DataTypes.NotificationRegistrationWrapper> buildNonSubscribedRegistrationsForNotifications(
																							ID userId,
																							String notificationCategory) {
		List<NCM_API_DataTypes.NotificationRegistrationWrapper> results = new List<NCM_API_DataTypes.NotificationRegistrationWrapper>();
		
		// Find the Notification Catalogs and build the notification registration results
		List<NotificationCatalog__c> catalogTopics = NCM_DataAccessor.getNotificationTopicsWithActiveSubscriptions(
																								userId,
																								notificationCategory);
		
		for(NotificationCatalog__c tmpTopic : catalogTopics) {
			NCM_API_DataTypes.NotificationCatalogWrapper catalogWrapperItem = new NCM_API_DataTypes.NotificationCatalogWrapper(tmpTopic);
			
			NCM_API_DataTypes.NotificationRegistrationWrapper regWrapperItem = new NCM_API_DataTypes.NotificationRegistrationWrapper();
			
			if(tmpTopic.NotificationRegistrations__r == NULL ||  tmpTopic.NotificationRegistrations__r.isEmpty()) {
				regWrapperItem.notificationCatalogId = tmpTopic.Id;
				regWrapperItem.userId				= userId;
				regWrapperItem.relatedToId			= NULL;
				regWrapperItem.reminderPeriod		= tmpTopic.DefaultReminderPeriod__c;
				regWrapperItem.maxNumberReminders	= NCM_API_DataTypes.DEFAULT_MAX_NUMBER_OF_REMINDERS;
				regWrapperItem.active				= false;
				regWrapperItem.notifyByEmail		= false;
				regWrapperItem.notifyBySMS			= false;
				regWrapperItem.notifyBychatter		= false;
				regWrapperItem.parentTopicData = catalogWrapperItem;
				results.add(regWrapperItem);
			}
		}
		
		return results;
	}
	
	
	/**
	 * @description	Get the number Of Active Registrations per Related Object for the given user
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 24-Sep-2015
	 * @param		userId    The id of the user that the results are connected with
	 * @return		The Map with key the related object id and value the Number of active registrations assigned to
	 *				this related object id for the given user
	*/
	public static Map<String, Integer> getNumberOfActiveRegistrationsPerRelatedObjectByUser(Id userId) {
		// Initialize variables and check for invalid parameters
		Map<String, Integer> resultMap = new Map<String, Integer>();
		
		if(userId == NULL) {
			return resultMap;
		}
		
		// Find the Notification and build the notification Pending Acknowledgment results
		String whereClause = 'UserId__c = \'' + userId + '\' AND Active__c = true';
		List<NotificationRegistration__c> registrationList = NCM_DataAccessor.getNotificationRegistrationWithWhereClause(
																										whereClause);
		
		Integer activeRegsNum;
		
		for(NotificationRegistration__c reg : registrationList ) {
			activeRegsNum = 1;
			
			// check the map already contains relatedObjectId
			if(resultMap.containsKey( reg.RelatedToId__c )) {
				activeRegsNum += resultMap.remove( reg.RelatedToId__c );
			}
			
			resultMap.put(reg.RelatedToId__c, activeRegsNum);
		}
		
		return resultMap;
	}
	
// *********************************************************************************************************************
//				Create Notification Registrations
// *********************************************************************************************************************
	/**
	 * @description	Register the given subscriptions to the Notification Registration object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 03-Sep-2015
	 * @param		source    The subscriptions to be registered
	*/
	public static void registerForNotifications(List<NCM_API_DataTypes.NotificationRegistrationWrapper> source) {
		// Check for invalid parameters
		if(source == NULL || source.isEmpty()) {
			return;
		}
		
		// Isolate the userIds, notification catalog ids and related to ids to avoid duplicated registrations
		Set<ID> userIdsSet			= new Set<ID>();
		Set<ID> notifCatalogIdsSet	= new Set<ID>();
		Set<String> relatedToIdsSet = new Set<String>();
		Set<String> combinationSet	= new Set<String>();
		
		for(NCM_API_DataTypes.NotificationRegistrationWrapper tmpItem : source) {
			userIdsSet.add(tmpItem.userId);
			notifCatalogIdsSet.add(tmpItem.notificationCatalogId);
			relatedToIdsSet.add(tmpItem.relatedToId);
			// Add the combination: user id, notification catalog id and related to id that must be unique
			combinationSet.add('' + tmpItem.userId + ':' + tmpItem.notificationCatalogId + ':' + tmpItem.relatedToId);
		}
		
		// Find the possible duplicated records and map them in their unique identifier 
		// (combination of user id, notification catalog id and related to id
		List<NotificationRegistration__c> existingRegistrations = NCM_DataAccessor.getNotificationRegistrationForCheckingDuplicates(
																									userIdsSet,
																									notifCatalogIdsSet,
																									relatedToIdsSet);
		
		Map<String, NotificationRegistration__c> registrationsMap = new Map<String, NotificationRegistration__c>();
		
		for(NotificationRegistration__c tmpReg : existingRegistrations) {
			registrationsMap.put('' + tmpReg.UserId__c + ':' + tmpReg.NotificationCatalog__c + ':' + tmpReg.relatedToId__c,
								tmpReg
							);
		}
		
		// Create new registrations or enable old ones
		List<NotificationRegistration__c> results = new List<NotificationRegistration__c>();
			
		for(NCM_API_DataTypes.NotificationRegistrationWrapper tmpItem : source) {
			tmpItem.active = true; // just in case
			String key = '' + tmpItem.userId + ':' + tmpItem.notificationCatalogId + ':' + tmpItem.relatedToId;
			
			NotificationRegistration__c tmpReg = new NotificationRegistration__c();
			if( registrationsMap.containsKey(key) ) {
				tmpReg = registrationsMap.remove(key);
			}
			tmpReg = getNotificationRegistrationFromWrapperData(tmpItem, tmpReg);
			
			results.add(tmpReg);
		}
		
		upsert results;
	}
	
	
// *********************************************************************************************************************
//				Disable Notification Registrations
// *********************************************************************************************************************
	
	/**
	 * @description	Unregister the given subscriptions by making the Active field equal to false in the corresponding
	 *				records in the Notification Registration object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 25-Sep-2015
	 * @param		source    The subscriptions to be unregistered
	*/
	public static void disableRegistrationsByWrapperData(List<NCM_API_DataTypes.NotificationRegistrationWrapper> source) {
		// Check for invalid parameters
		if(source == NULL || source.isEmpty()) {
			return;
		}
		
		// Isolate the userIds, notification catalog ids and related to ids to avoid duplicated registrations
		Set<ID> userIdsSet			= new Set<ID>();
		Set<ID> notifCatalogIdsSet	= new Set<ID>();
		Set<String> relatedToIdsSet = new Set<String>();
		Set<String> combinationSet	= new Set<String>();
		
		for(NCM_API_DataTypes.NotificationRegistrationWrapper tmpItem : source) {
			userIdsSet.add(tmpItem.userId);
			notifCatalogIdsSet.add(tmpItem.notificationCatalogId);
			relatedToIdsSet.add(tmpItem.relatedToId);
			// Add the combination: user id, notification catalog id and related to id that must be unique
			combinationSet.add('' + tmpItem.userId + ':' + tmpItem.notificationCatalogId + ':' + tmpItem.relatedToId);
		}
		
		// Find the possible duplicated records and map them in their unique identifier 
		// (combination of user id, notification catalog id and related to id
		List<NotificationRegistration__c> existingRegistrations =
													NCM_DataAccessor.getNotificationRegistrationForCheckingDuplicates(
																									userIdsSet,
																									notifCatalogIdsSet,
																									relatedToIdsSet);
		
		Map<String, NotificationRegistration__c> registrationsMap = new Map<String, NotificationRegistration__c>();
		
		for(NotificationRegistration__c tmpReg : existingRegistrations) {
			registrationsMap.put(
						'' + tmpReg.UserId__c + ':' + tmpReg.NotificationCatalog__c + ':' + tmpReg.relatedToId__c,
						tmpReg
					);
		}
		
		// Disable existing registrations
		List<NotificationRegistration__c> registrationList = new List<NotificationRegistration__c>();
			
		for(NCM_API_DataTypes.NotificationRegistrationWrapper tmpItem : source) {
			String key = '' + tmpItem.userId + ':' + tmpItem.notificationCatalogId + ':' + tmpItem.relatedToId;
			
			if( registrationsMap.containsKey(key) ) {
				NotificationRegistration__c tmpReg = registrationsMap.remove(key);
				tmpReg.Active__c = false;
				registrationList.add(tmpReg);
			}
		}
		
		disableRegistrations(registrationList);
	}
	
	
	/**
	 * @description	Disable the subscriptions to notifications that corresponds to the given notification registration
	 *				ids.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015
	 * @param		registrationsIds    The ids of the subscriptions that must be disabled
	*/
	public static void disableRegistrationsByIds (Set<ID> registrationIds) {
		// Check for invalid parameters
		if(registrationIds == NULL || registrationIds.isEmpty() ) {
			return;
		}
		
		// Find and disable registrations
		List<String> tmpRegIds = new LIst<String>();
		for(ID tmpID : registrationIds) {
			tmpRegIDs.add(tmpID);
		}
		
		String whereClause = 'ID in (' + NCM_Utils.createSetForDynamicQuery(tmpRegIds) + ') ';
		List<NotificationRegistration__c> registrationList = NCM_DataAccessor.getNotificationRegistrationWithWhereClause(
																											whereClause);
		
		disableRegistrations(registrationList);
	}
	
	
	/**
	 * @description	Disable all the subscriptions that are related to the given object id and to the given user
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015
	 * @param		userId         The id of the user that his subscriptions will be disabled
	 * @param		relatedToId    The id of the object that is related to the subscriptions that will be disabled
	*/
	public static void disableRegistrationsByUserAndRelatedToId(ID userId, String relatedToId) {
		String whereClause = 'UserId__c = \'' + userId + '\'';
		whereClause += (relatedToId == NULL)?
										' AND RelatedToId__c = NULL' :
										' AND RelatedToId__c = \'' + relatedToId + '\'';
		
		List<NotificationRegistration__c> registrationList = NCM_DataAccessor.getNotificationRegistrationWithWhereClause(
																											whereClause);
		
		disableRegistrations(registrationList);
	}
	
	
	/**
	 * @description	Disable all the subscriptions that are connected to the given catalog and to the given user
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 02-Sep-2015
	 * @param		userId                   The id of the user that his subscriptions will be disabled
	 * @param		notificationCatalogId    The id of the notification topic that is related to the subscription that
	 *										 will be disabled.
	*/
	public static void disableRegistrationsByUserAndNotificationTopic(ID userId, ID notificationCatalogId) {
		String whereClause = 'NotificationCatalog__c = \'' + notificationCatalogId + '\''
							+ ' AND UserId__c = \'' + userId + '\'';
		List<NotificationRegistration__c> registrationList = NCM_DataAccessor.getNotificationRegistrationWithWhereClause(
																											whereClause);
		
		disableRegistrations(registrationList);
	}
	
	
	/**
	 * @description	Disable all the subscriptions of the given user
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015
	 * @param		userId    The id of the user that all his subscriptions will be disabled
	*/
	public static void disableRegistrationsByUser (ID userId) {
		String whereClause = 'UserId__c = \'' + userId + '\'';
		List<NotificationRegistration__c> registrationList = NCM_DataAccessor.getNotificationRegistrationWithWhereClause(
																											whereClause);
		
		disableRegistrations(registrationList);
	}
	
	
	/**
	 * @description	Disable all the subscriptions that are related to the given object id
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015
	 * @param		relatedToId    The id of the object that is related to the subscription
	*/
	public static void disableRegistrationsByRelatedToId (String relatedToId) {
		//String whereClause = 'RelatedToId__c = \'' + relatedToId + '\'';
		String whereClause = (relatedToId == NULL)?
										'RelatedToId__c = NULL' :
										'RelatedToId__c = \'' + relatedToId + '\'';
		List<NotificationRegistration__c> registrationList = NCM_DataAccessor.getNotificationRegistrationWithWhereClause(
																											whereClause);
		
		disableRegistrations(registrationList);
	}
	
	
	/**
	 * @description	Disable the given notification registrations
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 01-Sep-2015
	 * @param		registrationsIds    The ids of the subscriptions that must be disabled
	*/
	@TestVisible private static void disableRegistrations (List<NotificationRegistration__c> source) {
		// Check for invalid parameters
		if(source == NULL || source.isEmpty() ) {
			return;
		}
		
		// Disable the given registrations
		for(NotificationRegistration__c tmpItem : source) {
			tmpItem.Active__c = false;
		}
		
		update source;
	}
	
	
// *********************************************************************************************************************
//				Helper functions
// *********************************************************************************************************************
	
	/**
	 * @description	Retrieve the user ids from the given subscriptions that notification by Chatter is selected
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 11-Sep-2015
	 * @param		source    The subscriptions from which we need to retrieve the user ids
	 * @return		The user ids to be notified by chatter
	*/
	public static Set<Id> getUserIdsToBeNotififiedByChatter (List<NotificationRegistration__c> source) {
		// Check for invalid parameters
		Set<Id> userIds = new Set<Id>();
		
		if(source == NULL || source.isEmpty() ){
			return userIds;
		}
		
		// Retrieve user ids
		for(NotificationRegistration__c tmpReg : source) {
			if( tmpReg.NotifyByChatter__c ) {
				userIds.add(tmpReg.UserId__c);
			}
		}
		
		return userIds;
	}
}