/**
*   A wrapper class for the CTMS Contract Management Web Service (WS).
*/
public with sharing class CM_CtmsServiceWrapper {
    
    private static final COM_Logger LOGGER = new COM_Logger('CM_CtmsServiceWrapper');
    private static String ENDPOINT = '';
    private static String SERVICENAME = '';
    private static Integer TIMEOUT = 0;
    private static List<String> errors;
    
    static {
        initSettings();
    }  
    
    public static List<String> getErrors() {
        return errors;
    }

    /**
    *   Configure the WS endpoint
    */
    private static void initSettings() {
        Map<String, MuleServicesCS__c> settings = MuleServicesCS__c.getAll();
        ENDPOINT = settings.get('ENDPOINT').Value__c;
        SERVICENAME = settings.get('CtmsCTAService').Value__c;
        Integer to = Integer.valueOf(settings.get('TIMEOUT').Value__c);
        if (to != null && to > 0) {
            TIMEOUT = to;
        } 
        LOGGER.info('Using following Custom Settings for CM_CtmsServiceWrapper');
        LOGGER.info('ENDPOINT:'+ENDPOINT);
        LOGGER.info('SERVICENAME:'+SERVICENAME);
        LOGGER.info('TIMEOUT:'+TIMEOUT);
    }
    
    /**
    *   Returns the service
    */
    private static CM_CtmsService.PRACTAInboundWebService getService() {
        CM_CtmsService.PRACTAInboundWebService service = new CM_CtmsService.PRACTAInboundWebService();
        service.endpoint_x = ENDPOINT + SERVICENAME;
        service.timeout_x = TIMEOUT;
        return service;
    }
    
    /**
    *   Returns the list of Sites for the input criteria
    */
    /*
    public static CM_CtmsService.ReadSite_Output_element getSites(CM_CtmsService_Site.site site) {
        CM_CtmsService.ReadSite_Output_element response = null;
        if (site != null) {
            CM_CtmsService.PRACTAInboundWebService service = getService();
            try {
                CM_CtmsService_Site.sites sites = new CM_CtmsService_Site.sites();
                sites.site = new CM_CtmsService_Site.site[] {site};
                response = service.ReadSite(sites);
                LOGGER.info('getSites successfull');
            } catch (Exception e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                LOGGER.error('getSites failed',e);
            }
            LOGGER.info('Response is: ' + response);
        }
        return response;
    }
    */
    
    /**
    *   Returns the list of Site Contracts for the input criteria
    */
    /*
    public static CM_CtmsService.ReadSiteContract_Output_element getSiteContracts(CM_CtmsService_SiteContract.site site) {
        CM_CtmsService.ReadSiteContract_Output_element response = null;
        if (site != null && null != site.id) {
            CM_CtmsService.PRACTAInboundWebService service = getService();
            try {
                CM_CtmsService_SiteContract.sites sites = new CM_CtmsService_SiteContract.sites();
                sites.site = new CM_CtmsService_SiteContract.site[] {site};
                response = service.ReadSiteContract(sites);
                LOGGER.info('getSiteContracts successfull');
            } catch (Exception e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                LOGGER.error('getSiteContracts failed',e);
            }
            LOGGER.info('Response is: ' + response);
        }
        return response;
    }
    */
    
    /**
     * Returns the LOV - List of values
     */
     /*
    public static CM_CtmsService.ReadLOV_Output_element getLOVs(CM_CtmsService_LOV.lov lov) {
        CM_CtmsService.ReadLOV_Output_element response = null;
        if (lov != null && null != lov.type_x) {
            CM_CtmsService.PRACTAInboundWebService service = getService();
            try {
                CM_CtmsService_LOV.lovs lovs = new CM_CtmsService_LOV.lovs();
                lovs.lov = new CM_CtmsService_LOV.lov[] {lov};
                response = service.ReadLOV(lovs);
                LOGGER.info('getLOVs successfull');
            } catch (Exception e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                LOGGER.error('getLOVs failed',e);
            }
            LOGGER.info('Response is: ' + response);
        }
        return response;
    }
    */
    
    /**
     * Returns the LOV's for Contract Statuses
     */
     /*
    public static CM_CtmsService.ReadLOV_Output_element getContractStatuses() {
        CM_CtmsService.ReadLOV_Output_element response = null;
        CM_CtmsService_LOV.lov lov = new CM_CtmsService_LOV.lov();
        lov.active = 'Y';
        lov.type_x = 'PRA_CONTRACT_INV_STATUS';
        if (lov != null) {
            try {
                response = getLOVs(lov);
                LOGGER.info('getContractStatuses successfull');
            } catch (Exception e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                LOGGER.error('getContractStatuses failed',e);
            }
            LOGGER.info('Response is: ' + response);
        }
        return response;
    }
    */
    
    /**
     * Returns the LOV's for Contract Types
     */
     /*
    public static CM_CtmsService.ReadLOV_Output_element getContractTypes() {
        CM_CtmsService.ReadLOV_Output_element response = null;
        CM_CtmsService_LOV.lov lov = new CM_CtmsService_LOV.lov();
        lov.active = 'Y';
        lov.parent = 'Inv Contracts';
        lov.type_x = 'TODO_TYPE';
        if (lov != null) {
            try {
                response = getLOVs(lov);
                LOGGER.info('getContractTypes successfull');
            } catch (Exception e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                LOGGER.error('getContractTypes failed',e);
            }
            LOGGER.info('Response is: ' + response);
        }
        return response;
    }
    */
    
    /**
    *   Returns the list of Accounts for the input criteria
    */
    /*
    public static CM_CtmsService.ReadAccount_Output_element getAccounts(CM_CtmsService_Account.account account) {
        CM_CtmsService.ReadAccount_Output_element response = null;
        if (account != null) {
            CM_CtmsService.PRACTAInboundWebService service = getService();
            try {
                CM_CtmsService_Account.accounts accounts = new CM_CtmsService_Account.accounts();
                accounts.account = new CM_CtmsService_Account.account[] {account};
                response = service.ReadAccount(accounts);
                LOGGER.info('getAccounts successfull');
            } catch (Exception e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                LOGGER.error('getAccounts failed',e);
            }
            LOGGER.info('Response is: ' + response);
        }
        return response;
    }
    */
    
    /**
    *   Returns the list of Contacts for the input criteria
    */
    /*
    public static CM_CtmsService.ReadContact_Output_element getContacts(CM_CtmsService_Contact.contact contact) {
        CM_CtmsService.ReadContact_Output_element response = null;
        if (contact != null) {
            CM_CtmsService.PRACTAInboundWebService service = getService();
            try {
                CM_CtmsService_Contact.contacts contacts = new CM_CtmsService_Contact.contacts();
                contacts.contact = new CM_CtmsService_Contact.contact[] {contact};
                response = service.ReadContact(contacts);
                LOGGER.info('getContacts successfull');
            } catch (Exception e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                LOGGER.error('getContacts failed',e);
            }
            LOGGER.info('Response is: ' + response);
        }
        return response;
    }
    */
    
    /**
    *   Returns the list of Site Contacts for the input criteria
    */
    /*
    public static CM_CtmsService.ReadSiteContact_Output_element getSiteContacts(CM_CtmsService_SiteContact.site site) {
        CM_CtmsService.ReadSiteContact_Output_element response = null;
        if (site != null && null != site.id) {
            CM_CtmsService.PRACTAInboundWebService service = getService();
            try {
                CM_CtmsService_SiteContact.sites sites = new CM_CtmsService_SiteContact.sites();
                sites.site = new CM_CtmsService_SiteContact.site[] {site};
                response = service.ReadSiteContact(sites);
                LOGGER.info('getSiteContacts successfull');
            } catch (Exception e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                LOGGER.error('getSiteContacts failed',e);
            }
            LOGGER.info('Response is: ' + response);
        }
        return response;
    }
    */
    
    /**
    *   Returns the list of Site Teams for the input criteria
    */
    /*
    public static CM_CtmsService.ReadSiteTeam_Output_element getSiteTeams(CM_CtmsService_SiteTeam.site site) {
        CM_CtmsService.ReadSiteTeam_Output_element response = null;
        if (site != null && null != site.id) {
            CM_CtmsService.PRACTAInboundWebService service = getService();
            try {
                CM_CtmsService_SiteTeam.sites sites = new CM_CtmsService_SiteTeam.sites();
                sites.site = new CM_CtmsService_SiteTeam.site[] {site};
                response = service.ReadSiteTeam(sites);
                LOGGER.info('getSiteTeams successfull');
            } catch (Exception e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                LOGGER.error('getSiteTeams failed',e);
            }
            LOGGER.info('Response is: ' + response);
        }
        return response;
    }
    */
    
    /**
    *   Upserts the list of Site Activities
    */
    public static CM_CtmsService.WriteSiteActivity_Output_element upsertSiteActivities(CM_CtmsService_SiteActivity.site site, String user) {
        CM_CtmsService.WriteSiteActivity_Output_element response = null;
        if (null != site && null != site.id && null != site.activities && null != site.activities.activity && null != user) {
            CM_CtmsService.PRACTAInboundWebService service = getService();
            try {
                CM_CtmsService_SiteActivity.sites sites = new CM_CtmsService_SiteActivity.sites();
                sites.site = new CM_CtmsService_SiteActivity.site[] {site};
                response = service.WriteSiteActivity(sites, user);
                LOGGER.info('upsertSiteActivities successfull');
                for(CM_CtmsService_SiteActivity.site siteResponse : response.sites.site) {
                    if (null != siteResponse  && null != siteResponse.activities) {
                        for(CM_CtmsService_SiteActivity.activity activity : siteResponse.activities.activity) {
                            LOGGER.info('upsertSiteActivities:siteID:'+siteResponse.id+', contractID:'+activity.id);
                        }
                    }
                }
            } catch (Exception e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                LOGGER.error('upsertSiteActivities failed',e);
            }
            LOGGER.info('Response is: ' + response);
        }
        return response;
    }
    
    /**
    *   Upserts the list of Site Contracts
    */
    public static CM_CtmsService.WriteSiteContract_Output_element upsertSiteContracts(final List<Apttus__APTS_Agreement__c> contracts) {
        CM_CtmsService_SiteContract.site site;
        String user;
        CM_CtmsService.WriteSiteContract_Output_element response = null;
        if (null != site && null != site.id && null != site.contracts && null != site.contracts.contract) {
            CM_CtmsService.PRACTAInboundWebService service = getService();
            try {
                CM_CtmsService_SiteContract.sites sites = new CM_CtmsService_SiteContract.sites();
                sites.site = new CM_CtmsService_SiteContract.site[] {site};
                response = service.WriteSiteContract(sites, user);
                LOGGER.info('upsertSiteContracts successfull');
                if(null != response && null != response.sites && null != response.sites.site) {
                    for(CM_CtmsService_SiteContract.site siteResponse : response.sites.site) {
                    if (null != siteResponse  && null != siteResponse.contracts && null != siteResponse.contracts.contract) {
                        for(CM_CtmsService_SiteContract.contract contract : siteResponse.contracts.contract) {
                            LOGGER.info('upsertSiteContracts:siteID:'+siteResponse.id+', contractID:'+contract.id);
                        }
                    }
                    }
                }
            } catch (Exception e) {
                errors = new List<String>();
                errors.add(e.getMessage());
                LOGGER.error('upsertSiteContracts failed',e);
            }
            LOGGER.info('Response is: ' + response);
        }
        return response;
    }
    
}