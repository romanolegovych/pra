public class LMS_RoleViewerController {

	public Boolean renderPRATab{get;private set;}
	public Boolean renderPSTTab{get;private set;}
	public Boolean renderAdhocTab{get;private set;}

	public LMS_RoleViewerController() {
		checkPermissions();
	}
	
	private void checkPermissions() {
		renderPRATab = true;
		renderPSTTab = true;
		renderAdhocTab = true;
		
		String profileName = LMS_ToolsDataAccessor.getProfileNameFromId(UserInfo.getProfileId());
		Boolean isPRAAdmin = LMS_ToolsDataAccessor.isPRAAdmin(profileName);
		Boolean isPSTAdmin = LMS_ToolsDataAccessor.isPSTAdmin(profileName);
		Boolean isAdhocAdmin = LMS_ToolsDataAccessor.isAdhocAdmin(profileName);
		Boolean isPRAAdhocAdmin = LMS_ToolsDataAccessor.isPRAAdhocAdmin(profileName);
		if(isPRAAdhocAdmin) {
			renderPSTTab = false;
		} else if(isPRAAdmin) {
			renderPSTTab = false;
			renderAdhocTab = false;
		} else if(isPSTAdmin) {
			renderPRATab = false;
			renderAdhocTab = false;
		} else if(isAdhocAdmin) {
			renderPRATab = false;
			renderPSTTab = false;
		}
	}
}