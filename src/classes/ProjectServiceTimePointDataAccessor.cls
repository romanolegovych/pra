public with sharing class ProjectServiceTimePointDataAccessor {

	/** Object definition for fields used in application for ProjectServiceTimePoint__c
	 * @author	Maurice Kremer
	 * @version 09-Jan-2014
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString() {
		return getSObjectFieldString('ProjectServiceTimePoint__c');		
	}
	
	
	/** Object definition for fields used in application for ProjectServiceTimePoint__c with the parameter referenceName as a prefix
	 * @author	Maurice Kremer
	 * @version 09-Jan-2014
	 * @param	referenceName		Used to define if the fields are retrieved from another object or from the object itself
	 * @return	The list of necessary fields concatted in a String
	 */
	public static String getSObjectFieldString(String referenceName) {
		referenceName = (String.isBlank(referenceName))? '' : referenceName + '.';
		String result = referenceName + 'Id,';
		result += referenceName + 'Flowchart__c,';
		result += referenceName + 'ProjectService__c,';
		result += referenceName + 'TimePoint__c,';	
		result += referenceName + 'listOfTimeMinutes__c,';	
		result += referenceName + 'TimePointCount__c';	
		return result;
	}
	
	/** Retrieve list of ProjectServiceTimePoint__c.
	 * @author	Maurice Kremer
	 * @version 09-Jan-2014
	 * @param	whereClause			The criteria that ProjectServiceTimePoint__c Lists must meet
	 * @return	List of ProjectServiceTimePoint__c
	 */
	public static List<ProjectServiceTimePoint__c> getProjectServiceTimePointList(String whereClause) {
		
	
		String query = String.format(
								'SELECT {0}, {2}, {3}, ProjectService__r.Service__r.ServiceCategory__r.Code__c ' +
								'FROM ProjectServiceTimePoint__c ' +
								'WHERE {1} order by ProjectService__r.Service__r.ServiceCategory__r.Code__c, ProjectService__r.Service__r.SequenceNumber__c',
								new List<String> {
									getSObjectFieldString(),
									whereClause,
									ProjectServiceDataAccessor.getSObjectFieldString('ProjectService__r'),
									ServiceDataAccessor.getSObjectFieldString('ProjectService__r.Service__r')
								}
							);
		return (List<ProjectServiceTimePoint__c>) Database.query(query);
	}

}