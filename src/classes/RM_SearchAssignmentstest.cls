/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 @isTest (seealldata=true)
private class RM_SearchAssignmentstest{

  public static Saved_Search__c ss;
  public static Saved_Search__c ss1;
  public static Saved_Search__c ss2;
  public static Saved_Search__c ss3;
  public static Saved_Search__c ss4;
  static void init(){   
   List<Folder> lstFolder = [Select Id From Folder Where Name = 'Projects'  OR Name='My Project Searches'];                
   if(lstFolder.size() > 0)
   {
    Document document = new Document(FolderId = lstFolder.get(0).Id, Name='Test Name',Keywords = 'Test');
    insert document;
   }
   Root_Folder__c rf=new Root_Folder__c(Name='Projects');
   insert rf;
   Sub_Folder__c sf=new Sub_Folder__c(Name='My Project Searches',Root__c=rf.id ,User__c=UserInfo.getUserId(),Sub_Folder_Unique_Key__c='sf001');
   insert sf;
   Group g=new Group(Name='RM_Test');
   insert g;
   GroupMember gm=new GroupMember(GroupID =g.id,UserOrGroupid=UserInfo.getUserId());
   insert gm;
   
   WFM_Client__c wclient=new WFM_Client__c(Client_Name__c='Test001',Client_Unique_Key__c='T0001');
   insert wclient;
      
   WFM_Contract__c wc=new WFM_Contract__c(Contract_Unique_Key__c='Testcontr',Contract_Status__c='T1',Client_ID__c=wclient.id);
   insert wc;
   
   WFM_Project__c wp=new WFM_Project__c(Name='Test Project',Contract_ID__c=wc.id, Project_Unique_Key__c='Te01');
   insert wp; 
       
   Country__c c=new Country__c(Name='TestCon');
   insert c;
   
   
   WFM_Request__c wr=new WFM_Request__c(Request_ID__c='req01',Request_Status__c='Approved',Request_Type__c='RM');
   insert wr;
   
   Employee_Details__c emd=new Employee_Details__c(Name='Employee Test',First_Name__c='Tet1',Last_Name__c='Test2',Country_Name__c=c.id,Status__c='St',Employee_Unique_Key__c='empTest001',Date_Hired__c=System.today(),Business_Unit_Desc__c='Product Registration',Business_Unit__c='RP');
   insert emd;
   
   WFM_employee_Allocations__c wea=new WFM_employee_Allocations__c(Allocation_Unique_Key__c='Al001',Project_Buf_Code__c='Buf001',Project_ID__c=wp.id,Request__c=wr.id,Status__c='Confirmed',EMPLOYEE_ID__c=emd.id,Project_Function__c='In House CRA');
   insert wea;
   
   ss=new Saved_Search__c(Name='Project ID',Saved_Search_Unique_Key__c='Test1',User__c=userinfo.getUserid());
   insert ss;    
   Search_Detail__c sd=new Search_Detail__c(Name='Project ID',Search_Value__c='Test save value',Search_Detail_Unique_Key__c='sduniquekey1'+'100' ,File__c=ss.id);
   insert sd;
   
   ss1=new Saved_Search__c(Name='Sponsor',Saved_Search_Unique_Key__c='Test2',User__c=userinfo.getUserid());
   insert ss1;    
   Search_Detail__c sd1=new Search_Detail__c(Name='Sponsor',Search_Value__c='Test save value',Search_Detail_Unique_Key__c='sduniquekey1'+'100' ,File__c=ss1.id);
   insert sd1;
   
   ss2=new Saved_Search__c(Name='Created By',Saved_Search_Unique_Key__c='Test3',User__c=userinfo.getUserid());
   insert ss2;    
   Search_Detail__c sd2=new Search_Detail__c(Name='Created By',Search_Value__c='Test save value',Search_Detail_Unique_Key__c='sduniquekey3'+'100' ,File__c=ss2.id);
   insert sd2;
   
   ss3=new Saved_Search__c(Name='Last Modified By',Saved_Search_Unique_Key__c='Test4',User__c=userinfo.getUserid());
   insert ss3;    
   Search_Detail__c sd3=new Search_Detail__c(Name='Last Modified By',Search_Value__c='Test save value',Search_Detail_Unique_Key__c='sduniquekey3'+'100' ,File__c=ss3.id);
   insert sd3;
   
   ss4=new Saved_Search__c(Name='Request ID',Saved_Search_Unique_Key__c='Test5',User__c=userinfo.getUserid());
   insert ss4;    
   Search_Detail__c sd4=new Search_Detail__c(Name='Request ID',Search_Value__c='Test save value',Search_Detail_Unique_Key__c='sduniquekey4'+'100' ,File__c=ss4.id);
   insert sd4;
   
  }
  static testMethod void TestSearchMethod(){  
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {         
          init();
          RM_SearchAssignments obj=new RM_SearchAssignments();
          obj.selstatus='Proposed';
          obj.selDatType='Created Date';
          obj.myval='Request ID';
          obj.strsearchText='Test';
          obj.selSearch='Test';
          //obj.myval='Created By';
          //obj.myval='Last Modified By';         
          obj.datefilter='12/01/2013';
          obj.datefilter1='12/15/2013';
          try{
          obj.getAllocationDetail();    
          obj.go();
          obj.changedval();
          obj.gettxtsearch(); 
          //obj.initVals();
          obj.getstatus();
          obj.getassigntype();
          obj.getdattype();
          obj.getgroup();
          obj.getJobClassWithBU();          
          
          //obj.getbufcode();
          //obj.getFuncCodes();
          //obj.getgreater();
          obj.getmonths();
          obj.getResourceFolders();
          RM_SearchAssignments.CheckSaveSearch('Unit Test', [Select Id From Folder Where Name = 'Projects'].ID);
          obj.saveSearch();
          }catch(Exception e){}       
        }
    }   
    
    static testMethod void TestSearchMethod11(){  
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {         
          init();
          RM_SearchAssignments obj=new RM_SearchAssignments();
          obj.selstatus='Proposed';
          obj.selDatType='Created Date';
          obj.myval='Request ID';
          obj.strsearchText='Test';
          obj.selSearch='Test111111';
          //obj.myval='Created By';
          //obj.myval='Last Modified By';         
          obj.datefilter='12/01/2013';
          obj.datefilter1='12/15/2013';
          try{
          obj.getAllocationDetail();    
          obj.go();
          obj.changedval();
          obj.gettxtsearch(); 
          //obj.initVals();
          obj.getstatus();
          obj.getassigntype();
          obj.getdattype();
          obj.getgroup();
          obj.getJobClassWithBU();          
          
          //obj.getbufcode();
          //obj.getFuncCodes();
          //obj.getgreater();
          obj.getmonths();
          obj.getResourceFolders();
          RM_SearchAssignments.CheckSaveSearch('Unit Test', [Select Id From Folder Where Name = 'Projects'].ID);
          obj.saveSearch();
          }catch(Exception e){}       
        }
    }   
  
        static testMethod void TestSearchResult(){  
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {         
          init();
          List<String> litbu=new List<String>();
          litbu.add('Product Registration');
          
          List<String> litrole=new List<String>();
          litbu.add('In House CRA');
          
          List<String> litcodes=new List<String>();
          litbu.add('Product Registration');
          
          RM_SearchAssignments obj=new RM_SearchAssignments();
          obj.selstatus='Proposed';
          obj.selDatType='End Date';
          obj.myval='Created By';
          obj.selectedBusinessUnit=litbu;
          obj.selectedRoles=litrole;
          //obj.selectedCountry='INDIA';
          obj.selectedCodes=litcodes;
          obj.strSearchText='Tandon';
          obj.selSearch='';       
          obj.datefilter='12/01/2013';
          obj.datefilter1='12/15/2013';       
          try{
             obj.getAllocationDetail();                     
          obj.go();
          obj.changedval();
          obj.gettxtsearch(); 
         // obj.initVals();
          obj.getstatus();
          obj.getassigntype();
          obj.getdattype();
          obj.getgroup();
          obj.getJobClassWithBU();
          //obj.getcountry();
          //obj.getbufcode();
          //obj.getFuncCodes();
          //obj.getgreater();
          obj.getmonths();
          obj.getResourceFolders();
          RM_SearchAssignments.CheckSaveSearch('Unit Test', [Select Id From Folder Where Name = 'Projects'].ID);
          obj.saveSearch();  
          }catch(Exception e){}           
        }
      } 
      
      static testMethod void TestSearch(){  
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {         
          init();
          List<String> litbu=new List<String>();
          litbu.add('Product Registration');
          
          List<String> litrole=new List<String>();
          litbu.add('In House CRA');
          
          List<String> litcodes=new List<String>();
          litbu.add('Product Registration');
          
          RM_SearchAssignments obj=new RM_SearchAssignments();
          obj.selstatus='Proposed';
          obj.selDatType='Start Date';
          obj.myval='Project ID';
          obj.selectedBusinessUnit=litbu;
          obj.selectedRoles=litrole;
          List<String> lc=new List<String>();
          lc.add('INDIA');
          obj.selectedCountry=lc;
          obj.selectedCodes=litcodes;         
          obj.strSearchText='Test Project';
          obj.selSearch='Test Project';
          obj.datefilter='12/01/2013';
          obj.datefilter1='12/15/2013';
          obj.isLead=true;
          obj.ispssv=true;
          obj.isunblinded=true;
          //obj.cmpasgn='>Greater than';
          
          
          obj.getAllocationDetail(); 
            obj.go();          
          obj.changedval();
          obj.gettxtsearch(); 
         // obj.initVals();
          obj.getstatus();
          obj.getassigntype();
          obj.getdattype();
          obj.getgroup();
          obj.getJobClassWithBU();
         // obj.getcountry();
         // obj.getbufcode();
         // obj.getFuncCodes();
         // obj.getgreater();
          obj.getmonths();
          obj.getResourceFolders();

          //create and query fake roots//
         Root_Folder__c root1 = new Root_Folder__c(Name = 'root1');      
         insert root1;
         //create and query fake subs//
         Sub_Folder__c sub1 = new Sub_Folder__c(Name = 'sub1', Root__c = root1.id, Custom__c = false, User__c = UserInfo.getUserID(), Sub_Folder_Unique_Key__c='sub1'+root1.id + UserInfo.getUserID());
        
        insert sub1; 
        
          obj.saveSubFolder = sub1.id;
          obj.searchName = 'Test';
          obj.selectedStatus = new list<string>();
          obj.selectedStatus.add('');
          obj.selectedBusinessUnit = new list<string>();
          //obj.selectedBusinessUnit.add('');
          obj.selectedBusinessUnit.add('Clinical Informatics');
          obj.selectedBusinessUnit.add('Product Registration');
          
          PageReference page= obj.saveSearch();
          Saved_Search__c searchName = RM_DataAccessor.getSavedSearch(obj.searchName,UserInfo.getUserId(), sub1.id );
          list<Search_Detail__c>  details = RM_DataAccessor.getSavedSearchDetail(searchName.id);
          system.assert(searchName.name == obj.searchName);
          //system.assert(details[0].name == obj.searchOn);
          
          //obj.saveSearch();  
                 
        }
      } 
      
       static testMethod void TestSearch1(){  
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {         
          init();
          PageReference p = Page.RM_SearchAssignments;  
          String s=ss4.id;
                      
          Test.setCurrentPage(p);
          ApexPages.currentPage().getParameters().put('cid',s);
          
          List<String> litbu=new List<String>();
          litbu.add('Product Registration');
          
          List<String> litrole=new List<String>();
          litbu.add('In House CRA');
          
          List<String> litcodes=new List<String>();
          litbu.add('Product Registration');
          
          RM_SearchAssignments obj=new RM_SearchAssignments();
          obj.selstatus='Proposed';
          obj.selDatType='Last Modified Date';
          obj.myval='Sponsor';
          obj.selectedBusinessUnit=litbu;
          obj.selectedRoles=litrole;
         // obj.selectedCountry='INDIA';
          obj.selectedCodes=litcodes;
          obj.strSearchText='Test Project';
          obj.selSearch='Test Project';
          obj.datefilter='12/01/2013';
          obj.datefilter1='12/15/2013';
          try{
            obj.getAllocationDetail(); 
            obj.go();          
          obj.changedval();
          obj.gettxtsearch(); 
         // obj.initVals();
          obj.getstatus();
          obj.getassigntype();
          obj.getdattype();
          obj.getgroup();
          obj.getJobClassWithBU();
        //  obj.getcountry();
        //  obj.getbufcode();
          //obj.getFuncCodes();
          //obj.getgreater();
          obj.getmonths();
          obj.getResourceFolders();

          //create and query fake roots//
         Root_Folder__c root1 = new Root_Folder__c(Name = 'root1');      
         insert root1;
         //create and query fake subs//
         Sub_Folder__c sub1 = new Sub_Folder__c(Name = 'sub1', Root__c = root1.id, Custom__c = false, User__c = UserInfo.getUserID(), Sub_Folder_Unique_Key__c='sub1'+root1.id + UserInfo.getUserID());
        
        insert sub1; 
        
          obj.saveSubFolder = sub1.id;
          obj.searchName = 'Test';
          obj.selectedStatus = new list<string>();
          obj.selectedStatus.add('');
          obj.selectedBusinessUnit = new list<string>();
          obj.selectedBusinessUnit.add('');
          
          PageReference page= obj.saveSearch();
          Saved_Search__c searchName = RM_DataAccessor.getSavedSearch(obj.searchName,UserInfo.getUserId(), sub1.id );
          list<Search_Detail__c>  details = RM_DataAccessor.getSavedSearchDetail(searchName.id);
          system.assert(searchName.name == obj.searchName);
          //system.assert(details[0].name == obj.searchOn);
          
          obj.saveSearch();  
         }catch(Exception e){}        
        }
      }
      
    /*  static testMethod void TestSearchMethod2(){  
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {         
          init();
          
          PageReference p = Page.RM_SearchAssignments;  
          String s=ss3.id;
                      
          Test.setCurrentPage(p);
          ApexPages.currentPage().getParameters().put('cid',s);
          
          RM_SearchAssignments obj=new RM_SearchAssignments();
          List<String> litbu=new List<String>();
          litbu.add('Product Registration');          
          List<String> litrole=new List<String>();
          litrole.add('In House CRA');
          list<String> lc=new list<String>();
          lc.add('INDIA');
          obj.selectedCountry=lc;          
          obj.selassigntype='Assignment';          
          obj.selectedBusinessUnit=litbu;
          obj.selectedRoles=litrole;
          obj.isLead=true;
          obj.ispssv=true;
          obj.isunblinded=true;
        //  obj.cmpasgn='< Less than';
          obj.strSearchText='McKinney, Craig';
          obj.selstatus='Proposed';
          obj.selDatType='Created Date';          
          obj.myval='Sponsor';         
          obj.datefilter='12/01/2013';
          obj.datefilter1='12/15/2013';
          
          obj.getAllocationDetail();    
          obj.go();
          obj.changedval();
          obj.gettxtsearch(); 
          //obj.initVals();
          obj.getstatus();
          obj.getassigntype();
          obj.getdattype();
          obj.getgroup();
          obj.getJobClassWithBU();
        //  obj.getcountry();
        //  obj.getbufcode();
        //  obj.getFuncCodes();
        //  obj.getgreater();
          obj.getmonths();
          obj.getResourceFolders();
          RM_SearchAssignments.CheckSaveSearch('Unit Test', [Select Id From Folder Where Name = 'Projects'].ID);
          
          List<string> lCode=new list<String>();
          //lCode.add('');
          lCode.add('BDV');
          lCode.add('KCI');
          obj.selectedCodes=lCode;
          obj.saveSearch();
                
        }
    }*/
    
    static testMethod void TestSearchMethod3(){  
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {         
          init();
          try{
          
          PageReference p = Page.RM_SearchAssignments;  
          String s=ss2.id;
                      
          Test.setCurrentPage(p);
          ApexPages.currentPage().getParameters().put('cid',s);
          
          RM_SearchAssignments obj=new RM_SearchAssignments();
          List<String> litbu=new List<String>();
          litbu.add('Product Registration');          
          List<String> litrole=new List<String>();
          litrole.add('In House CRA');
          list<String> lc=new list<String>();
          lc.add('INDIA');
          obj.selectedCountry=lc;
          obj.selectedDir='Before';
          obj.selectedDate='12/01/2013';
          obj.selassigntype='Assignment';
          String uName='Craig'+','+'Min';
          obj.getUserName(uName);
          obj.getBULocWithCountry();
          obj.getSign();
          obj.getComparionDate();
          obj.getDirection();
          
          obj.selectedBusinessUnit=litbu;
          obj.selectedRoles=litrole;
          obj.isLead=true;
          obj.ispssv=true;
          obj.isunblinded=true;
        //  obj.cmpasgn='>Greater than';
          obj.strSearchText='McKinney, Craig';
          obj.selstatus='Proposed';
          obj.selDatType='Created Date';          
          //obj.myval='Last Modified By'; 
          obj.myval='Project ID';         
          obj.datefilter='12/01/2013';
          obj.datefilter1='12/15/2013';
          obj.selSearch='kk';
          obj.getAllocationDetail();    
          obj.go();
          obj.changedval();
          obj.gettxtsearch(); 
          //obj.initVals();
          obj.getstatus();
          obj.getassigntype();
          obj.getdattype();
          obj.getgroup();
          obj.getJobClassWithBU();
          //obj.getcountry();
         // obj.getbufcode();
        //  obj.getFuncCodes();
        //  obj.getgreater();
          obj.getmonths();
          obj.getResourceFolders();
          RM_SearchAssignments.CheckSaveSearch('Unit Test', [Select Id From Folder Where Name = 'Projects'].ID);
          List<string> lCode=new list<String>();
          //lCode.add('');
          lCode.add('BDV');
          lCode.add('KCI');
          obj.selectedCodes=lCode;
          obj.saveSearch();
          }catch(Exception e){}      
        }
    }
    
   /* static testMethod void TestSearchMethod4(){  
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {         
          init();
          
          PageReference p = Page.RM_SearchAssignments;  
          String s=ss1.id;
                      
          Test.setCurrentPage(p);
          ApexPages.currentPage().getParameters().put('cid',s);
          
          RM_SearchAssignments obj=new RM_SearchAssignments();
          List<String> litbu=new List<String>();
          litbu.add('Product Registration');          
          List<String> litrole=new List<String>();
          litrole.add('In House CRA');
          list<String> lc=new list<String>();
          lc.add('INDIA');
          obj.selectedCountry=lc;
          obj.selectedDir='Before';
          obj.selassigntype='Assignment';
          String uName='McKinney'+','+'Min';
          obj.getUserName(uName);
          obj.getBULocWithCountry();
          obj.getSign();
          obj.getComparionDate();
          obj.getDirection();
          
          obj.selectedBusinessUnit=litbu;
          obj.selectedRoles=litrole;
          obj.isLead=true;
          obj.ispssv=true;
          obj.isunblinded=true;
        //  obj.cmpasgn='>Greater than';
          obj.strSearchText='McKinney, Craig';
          obj.selstatus='Proposed';
          obj.selDatType='Created Date';          
          obj.myval='Last Modified By'; 
          //obj.myval='Project ID';       
          obj.datefilter='12/01/2013';
          obj.datefilter1='12/15/2013';
          obj.selSearch='Craig';
          obj.getAllocationDetail();    
          obj.go();
          obj.changedval();
          obj.gettxtsearch(); 
          //obj.initVals();
          obj.getstatus();
          obj.getassigntype();
          obj.getdattype();
          obj.getgroup();
          obj.getJobClassWithBU();
          //obj.getcountry();
         // obj.getbufcode();
        //  obj.getFuncCodes();
        //  obj.getgreater();
          obj.getmonths();
          obj.getResourceFolders();
          RM_SearchAssignments.CheckSaveSearch('Unit Test', [Select Id From Folder Where Name = 'Projects'].ID);
          List<string> lCode=new list<String>();
          //lCode.add('');
          lCode.add('BDV');
          lCode.add('KCI');
          obj.selectedCodes=lCode;
          obj.saveSearch();
                
        }
    }*/
    
    static testMethod void TestSearchResult5(){  
        User usr = [Select id from User where Id = :UserInfo.getUserId()];
        System.RunAs(Usr)
        {         
          init();
          
          PageReference p = Page.RM_SearchAssignments;  
          String s=ss.id;
                      
          Test.setCurrentPage(p);
          ApexPages.currentPage().getParameters().put('cid',s);
          
          List<String> litbu=new List<String>();
          litbu.add('Product Registration');
          
          List<String> litrole=new List<String>();
          litbu.add('In House CRA');
          
          List<String> litcodes=new List<String>();
          litbu.add('Product Registration');
          
          RM_SearchAssignments obj=new RM_SearchAssignments();
          obj.selstatus='Proposed';
          obj.selDatType='End Date';
          obj.myval='Created By';
          obj.selectedBusinessUnit=litbu;
          obj.selectedRoles=litrole;
          //obj.selectedCountry='INDIA';
          obj.selectedCodes=litcodes;
          obj.strSearchText='McKinney, Craig';
          obj.selSearch='';       
          obj.datefilter='12/01/2013';
          obj.datefilter1='12/15/2013';       
          try{
             obj.getAllocationDetail();                     
          obj.go();
          obj.changedval();
          obj.gettxtsearch(); 
         // obj.initVals();
          obj.getstatus();
          obj.getassigntype();
          obj.getdattype();
          obj.getgroup();
          obj.getJobClassWithBU();
          //obj.getcountry();
          //obj.getbufcode();
          //obj.getFuncCodes();
          //obj.getgreater();
          obj.getmonths();
          obj.getResourceFolders();
          RM_SearchAssignments.CheckSaveSearch('Unit Test', [Select Id From Folder Where Name = 'Projects'].ID);
          obj.saveSearch();  
          }catch(Exception e){}           
        }
      } 
}