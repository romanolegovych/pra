/**
 *  @author     Sukrut Wagh
 *  @date       07/08/2014
 *  @description    Defines all the constants used in this app
 *                  Constant values are read from a CustomSetting for this app, applies defaults when values not found in CustomSetting 
*/
public with sharing class CM_Constants {
    
    /** @description Register the integration system types & systems used by this app here */
    public static final String INT_SYSTEM_TYPE_ETMF = 'INT_SYSTEM_TYPE_ETMF';
    public static final String INT_SYSTEM_TYPE_CTMS = 'INT_SYSTEM_TYPE_CTMS';
    
    /* Despite the system type & systems are same here (an co-incidence), they mean different things.
       Think of a scenario where you have to integrate with multiple systems of the same family/type.
       Example, you have multiple CTMS instances. 
    */
    public static final String INT_SYSTEM_ETMF = 'INT_SYSTEM_ETMF';
    public static final String INT_SYSTEM_CTMS = 'INT_SYSTEM_CTMS';

    public static final String API_APTTUS_APTS_AGREEMENT = 'API_APTTUS_APTS_AGREEMENT';
    public static final String API_APTTUS_AGREEMENT_DOCUMENT = 'API_APTTUS_AGREEMENT_DOCUMENT';

    public static final String AGREEMENT_RECORD_TYPE_AGREEMENT = 'AGREEMENT_RECORD_TYPE_AGREEMENT';
    public static final String AGREEMENT_RECORD_TYPE_TEMPLATE = 'AGREEMENT_RECORD_TYPE_TEMPLATE';
    
    public static final String AGREEMENT_STATUS_FULLY_EXECUTED = 'AGREEMENT_STATUS_FULLY_EXECUTED';
    
    /** @description Specifies the time in hours to exclude the in-transit records for processing
     *               Ex: If value is 6, records with in-transit statuses in the last 6 hours will be excluded  
    */ 
    public static final String ETMF_AGREEMENT_IN_TRANSIT_HOURS = 'ETMF_AGREEMENT_IN_TRANSIT_HOURS';
    public static final String CTMS_AGREEMENT_IN_TRANSIT_HOURS = 'CTMS_AGREEMENT_IN_TRANSIT_HOURS';
    
    /** @description Comma or semi-colon separated email addressess */
    public static final String ETMF_AGREEMENT_EMAIL_RECIPIENT = 'ETMF_AGREEMENT_EMAIL_RECIPIENT';
    public static final String CTMS_AGREEMENT_EMAIL_RECIPIENT = 'CTMS_AGREEMENT_EMAIL_RECIPIENT';
    
    private static final Map<String, String> defaults = new Map<String,String> {INT_SYSTEM_TYPE_ETMF => 'ETMF', INT_SYSTEM_TYPE_CTMS => 'CTMS',
                INT_SYSTEM_ETMF => 'ETMF', INT_SYSTEM_CTMS => 'CTMS',
                API_APTTUS_APTS_AGREEMENT => 'Apttus__APTS_Agreement__c', API_APTTUS_AGREEMENT_DOCUMENT => 'Apttus__Agreement_Document__c',
                AGREEMENT_RECORD_TYPE_AGREEMENT => 'Agreement', AGREEMENT_RECORD_TYPE_TEMPLATE => 'Template',
                AGREEMENT_STATUS_FULLY_EXECUTED => 'Fully Executed', 
                ETMF_AGREEMENT_IN_TRANSIT_HOURS => '-6', CTMS_AGREEMENT_IN_TRANSIT_HOURS => '-6',
                ETMF_AGREEMENT_EMAIL_RECIPIENT => '', CTMS_AGREEMENT_EMAIL_RECIPIENT => ''};
    
    /**
     * @description Returns the constant value. Constant is looked up in custom settings. If not found, default value is returned.
     * @param       key
    */
    public static String getConstant(String key) {
        String val = '';
        if(COM_Utils.isNotEmpty(key)) {
            val = (COM_Utils.isNotEmpty(CM_CS__c.getValues(key)) && COM_Utils.isNotEmpty(CM_CS__c.getValues(key).Value__c)) ?
                    CM_CS__c.getValues(key).Value__c : defaults.get(key);
        }
        return val;
    }
    
}