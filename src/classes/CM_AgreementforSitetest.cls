/** 
     * @description Test class CM_AgreementforSite
     * @author      Lavakusareddy Poluru
     * @date        Created: 30-Jan-2015
     */

@isTest
public with sharing class CM_AgreementforSitetest {
    private static final String Agreementid =  [SELECT Id from RecordType WHERE SObjectType = 'Apttus__APTS_Agreement__c' and DeveloperName = 'Agreement'].Id;
    
    /** 
     * @description Test method testnewAgreement
     * @author      Lavakusareddy Poluru
     * @date        Created: 30-Jan-2015
     */
         
    static testMethod void testnewAgreement() {
    
        WFM_Client__c testClient = new WFM_Client__c();
        WFM_Contract__c testContract = new WFM_Contract__c();       
        WFM_Project__c testProject = new WFM_Project__c();
        WFM_Protocol__c testProtocol = new WFM_Protocol__c();
        WFM_Site_Detail__c testSite = new WFM_Site_Detail__c();
        Protocol_Country__c testProtocolCountry = new Protocol_Country__c();
        Country__c testcountry = new Country__c();
        Apttus__APTS_Agreement__c testagreement = new Apttus__APTS_Agreement__c();
        list<Apttus__APTS_Agreement__c> testlist = new list<Apttus__APTS_Agreement__c>();
        
        testClient.Name = 'Ranbaxytest';                
        testClient.Client_Unique_Key__c = 'RBX001';
        insert testClient;
        
        testContract.Name = 'testcontract';
        testContract.Contract_Unique_Key__c = 'crnttest1';
        testContract.Client_ID__c = testClient.Id;
        insert testContract;
        
        testProject.Name = 'LiverCancer';
        testProject.Project_Unique_Key__c = 'LVC1';
        insert testProject;
        
        testProtocol.Name = 'testprotocol';
        testProtocol.Protocal_Unique_Key__c = 'test1';
        testProtocol.Project_ID__c = testProject.Id;
        insert testProtocol;
        
        testSite.Project_Protocol__c = testProtocol.Id;
        testSite.Site_ID__c = '010';
        testSite.Name = 'testsite1';
        insert testSite;
        
        testcountry.Name = 'ALBANIA';
        insert testcountry;
        
        testProtocolCountry.Name = 'Netherlands';
        testProtocolCountry.Client_Protocol__c = testProtocol.id;
        testProtocolCountry.Region__c = testcountry.id;
        insert testProtocolCountry;
        
        testagreement.RecordTypeId = Agreementid;
        testagreement.name='Test Template';
        testagreement.PRA_Status__c = 'At Site-Signature';   
        testagreement.Contract_Type__c='Master CTA';     
        testagreement.First_Draft_Sent_Date__c = Date.newInstance(2015, 01, 31);
        testagreement.Planned_Final_Exec_Date__c = Date.newInstance(2015, 12, 31);
        testlist.add(testagreement);
        insert testlist;
        
        ApexPages.StandardsetController Cont1 = new ApexPages.StandardsetController(testlist);
        ApexPages.CurrentPage().GetParameters().put('Id',testSite.id);
        ApexPages.CurrentPage().GetParameters().put('RecordType',Agreementid);
        CM_AgreementforSite testController1 = new CM_AgreementforSite(Cont1);
        
        ApexPages.StandardsetController Cont2 = new ApexPages.StandardsetController(testlist);
        ApexPages.CurrentPage().GetParameters().put('Id',testProtocol.id);
        ApexPages.CurrentPage().GetParameters().put('RecordType',Agreementid);
        CM_AgreementforSite testController2 = new CM_AgreementforSite(Cont2);
        
        ApexPages.StandardsetController Cont3 = new ApexPages.StandardsetController(testlist);
        ApexPages.CurrentPage().GetParameters().put('Id',testProtocolCountry.id);
        ApexPages.CurrentPage().GetParameters().put('RecordType',Agreementid);
        CM_AgreementforSite testController3 = new CM_AgreementforSite(Cont3);
        
        test.startTest();
        testController1.saveAgreement();
        testController2.saveAgreement();
        testController3.saveAgreement();
        test.stoptest();
                    
    }
}