Global class PRA_EmailHelper {

public static void sendEmail(String[] RecipientTOEmailList,String[] RecipientCCEmailList, String Subject,String HTMLBody) {

//New instance of a single email message     
Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
 
// Who you are sending the email to   
if(RecipientTOEmailList!=null)   
mail.setToAddresses(RecipientTOEmailList);    
if(RecipientCCEmailList!=null)   
mail.setCcAddresses(RecipientCCEmailList); 


//Setting Subject
if(Subject==null || Subject=='')
Subject='';
mail.setSubject(Subject);
 
//Setting up the HTML Body includes Link
if(HTMLBody==null || HTMLBody=='')
HTMLBody='';
mail.setHtmlBody(HTMLBody);
 

/*
//mail.setTargetObjectIds(targetObjectIds);
  // The email template ID used for the email
//mail.setTemplateId('00X30000001GLJj');
mail.setPlainTextBody('Project Name'+ Projectname +' is waiting for approval');
//mail.setWhatId(idsList);    
mail.setBccSender(false);
mail.setUseSignature(false);
mail.setReplyTo('recruiting@acme.com');
mail.setSenderDisplayName('HR Recruiting');
mail.setSaveAsActivity(false);  

*/
 
Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });

  }  
}