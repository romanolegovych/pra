@isTest
private class BDT_FinancialDocStudySelContrTest {

	static testMethod void myUnitTest() {
		List<Study__c> StudiesList = new List<Study__c>();
        // Create test data
		BDT_CreateTestData.createall();
		Client_Project__c project = [select id from Client_Project__c limit 1];
		BDT_Utils.setPreference('SelectedProject', String.valueOf(project.id));
		String StudyIDs = '';
		for (Study__c st : [select id from Study__c where Project__c = :project.id]) {
			StudyIDs = StudyIDs + ':' + st.id;
			StudiesList.add(st);
		}
		StudyIDs = StudyIds.removeStart(':');
		// Create the new financial document, with the predefined attributes values   	
		FinancialDocument__c proposal = new FinancialDocument__c();
		proposal.DocumentStatus__c = 'Approved';
		proposal.DocumentType__c   = 'Proposal';
		proposal.Client_Project__c = project.id;
		proposal.TotalValue__c 	   = '-';
		proposal.ApprovalStatus__c = 'Accepted';
		proposal.listOfStudyIds__c = StudyIds;
		insert proposal;
		FinancialDocument__c changeOrder = new FinancialDocument__c();
		changeOrder.DocumentStatus__c    	   = 'New';
		changeOrder.DocumentType__c      	   = 'Change Order';
		changeOrder.Client_Project__c    	   = project.id;
		changeOrder.TotalValue__c		 	   = '-';
		changeOrder.FinancialDocumentParent__c = proposal.id;
		changeOrder.listOfStudyIds__c	 	   = '';
		insert changeOrder;
		// Create the controller
		BDT_FinancialDocStudySelController p = new BDT_FinancialDocStudySelController();
		// Study selection for proposal
		PageReference pageRef = New PageReference(System.Page.BDT_FinancialDocStudySel.getUrl());
		pageref.getParameters().put('financialDocumentId' , proposal.id);
		Test.setCurrentPage(pageRef); 
		p = new BDT_FinancialDocStudySelController();
		system.assertEquals(p.proposalStudies.size(), StudiesList.size());
		for(BDT_DC_FinancialDocument.studySelectionInFinDocProcessWrapper tmpWrapperItem : p.studiesWrapperList) {
			system.assertEquals(tmpWrapperItem.disabled, true);
			system.assertEquals(tmpWrapperItem.selected, true);
		}
		p.closePage();
		// Study selection for change order
		pageref.getParameters().put('financialDocumentId' , changeOrder.id);
		Test.setCurrentPage(pageRef); 
		p = new BDT_FinancialDocStudySelController();
		for(BDT_DC_FinancialDocument.studySelectionInFinDocProcessWrapper tmpWrapperItem : p.studiesWrapperList) {
			tmpWrapperItem.selected = true;
			system.assertEquals(tmpWrapperItem.disabled, false);
		}
		p.save();
		// check if all the selected studies were stored in the change order document
		FinancialDocument__c tmpDocument = [SELECT listOfStudyIds__c FROM FinancialDocument__c WHERE Id=:changeOrder.Id];
		system.assertEquals(proposal.listOfStudyIds__c, tmpDocument.listOfStudyIds__c); 
    }
}