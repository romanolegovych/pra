/**
*   'PAWS_ECRFWorkflowExtensionTests' is the test class for PAWS_ECRFWorkflowExtension
*   Test classes are used to write the unit test and cover the code coverage of the code.
*   @author   Work Relay
*/
@isTest 
public with sharing class PAWS_ECRFWorkflowExtensionTests 
{
	static testmethod void run()
    {
    	PAWS_ApexTestsEnvironment.init();
    	
    	PageReference ref = Page.ECRFWorkflow;
    	ref.getParameters().put('Id', PAWS_ApexTestsEnvironment.Project.Id);
    	Test.setCurrentPage(ref);

    	STSWR1__Flow_Instance_Cursor__c cursor = PAWS_ApexTestsEnvironment.FlowInstanceCursor;
    	System.assert(PAWS_ECRFWorkflowExtension.loadCursors(PAWS_ApexTestsEnvironment.Project.Id) != null);
    	
    	PAWS_ECRFWorkflowExtension controller = new PAWS_ECRFWorkflowExtension(new ApexPages.StandardController(PAWS_ApexTestsEnvironment.FlowInstanceCursor));
    	System.assert(controller.CursorsJSON != null);
    	System.assert(controller.IsOldVersion != null);
    }
}