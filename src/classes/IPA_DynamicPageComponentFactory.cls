public with sharing class IPA_DynamicPageComponentFactory
{
    private IPA_DAL_DynamicPage dal;
    
    //Constructor
    public IPA_DynamicPageComponentFactory()
    {
        //Initialize DAL
        dal = new IPA_DAL_DynamicPage();
    }
    
    private Component.Apex.OutputPanel createPanel(List<IPA_Page_Widget__c> lstWidgets)
    {
        Component.Apex.OutputPanel outPanel = new Component.Apex.OutputPanel();
        //Create & Add Components for each Widget
        for(IPA_Page_Widget__c w: lstWidgets)
        {       
            if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_EventsHolidaysComponent')
            {
                Component.c.IPA_EventsHolidaysComponent ipaEventsHolidaysComponent = new Component.c.IPA_EventsHolidaysComponent(pageWidgetVar = w);
                outPanel.childComponents.add(ipaEventsHolidaysComponent);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_EventsComponent')
            {
                Component.c.IPA_EventsComponent ipaEventsComponent = new Component.c.IPA_EventsComponent(pageWidgetVar = w);
                outPanel.childComponents.add(ipaEventsComponent);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_QuickLinksComponent')
            {
                Component.c.IPA_QuickLinksComponent ipaQuickLinksComponent = new Component.c.IPA_QuickLinksComponent(pageWidgetVar = w);
                outPanel.childComponents.add(ipaQuickLinksComponent);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_MyAppsComponent')
            {
                Component.c.IPA_MyAppsComponent ipaApplicationsComponent = new Component.c.IPA_MyAppsComponent();
                outPanel.childComponents.add(ipaApplicationsComponent);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_HeroGraphic1Component')
            {
                Component.c.IPA_HeroGraphic1Component ipaHeroGra1Component = new Component.c.IPA_HeroGraphic1Component(pageWidgetVar = w);
                outPanel.childComponents.add(ipaHeroGra1Component);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_HeroGraphic2Component')
            {
                Component.c.IPA_HeroGraphic2Component ipaHeroGra2Component = new Component.c.IPA_HeroGraphic2Component(pageWidgetVar = w);
                outPanel.childComponents.add(ipaHeroGra2Component);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_DepartmentContentsComponent')
            {
                Component.c.IPA_DepartmentContentsComponent com = new Component.c.IPA_DepartmentContentsComponent(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_DepartmentContactsComponent')
            {
                Component.c.IPA_DepartmentContactsComponent com = new Component.c.IPA_DepartmentContactsComponent(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_DepartmentNewsComponent')
            {
                Component.c.IPA_DepartmentNewsComponent com = new Component.c.IPA_DepartmentNewsComponent(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            /*else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_ChatterGroupFeedComponent')
            {
                Component.c.IPA_ChatterGroupFeedComponent com = new Component.c.IPA_ChatterGroupFeedComponent(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }*/
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_ResourceCabinetComponent')
            {
                Component.c.IPA_ResourceCabinetComponent com = new Component.c.IPA_ResourceCabinetComponent(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_TimeZoneComponent')
            {
                Component.c.IPA_TimeZoneComponent com = new Component.c.IPA_TimeZoneComponent(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_TabGroupComponent')
            {
                Component.c.IPA_TabGroupComponent com = new Component.c.IPA_TabGroupComponent(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_HeroGraphic3Component')
            {
                Component.c.IPA_HeroGraphic3Component com = new Component.c.IPA_HeroGraphic3Component(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            /*TODO: REMOVE THE CASES BELOW AFTER CSS/Media rework is completed and these components/widget are merged back into the source ones above*/
            /*else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_EventsHolidaysComponent_Narender')
            {
                Component.c.IPA_EventsHolidaysComponent_Narender com = new Component.c.IPA_EventsHolidaysComponent_Narender(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_EventsComponent_Narender')
            {
                Component.c.IPA_EventsComponent_Narender com = new Component.c.IPA_EventsComponent_Narender(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_QuickLinksComponent_Narender')
            {
                Component.c.IPA_QuickLinksComponent_Narender com = new Component.c.IPA_QuickLinksComponent_Narender(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_HeroGraphic1Component_Narender')
            {
                Component.c.IPA_HeroGraphic1Component_Narender com = new Component.c.IPA_HeroGraphic1Component_Narender(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_HeroGraphic2Component_Narender')
            {
                Component.c.IPA_HeroGraphic2Component_Narender com = new Component.c.IPA_HeroGraphic2Component_Narender(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_DepartmentContentsComponent_Narender')
            {
                Component.c.IPA_DepartmentContentsComponent_Narender com = new Component.c.IPA_DepartmentContentsComponent_Narender(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_DepartmentContactsComponent_Narender')
            {
                Component.c.IPA_DepartmentContactsComponent_Narender com = new Component.c.IPA_DepartmentContactsComponent_Narender(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }
            else if(w.Widget__r.VisualForce_Component_Name__c == 'IPA_DepartmentNewsComponent_Narender')
            {
                Component.c.IPA_DepartmentNewsComponent_Narender com = new Component.c.IPA_DepartmentNewsComponent_Narender(pageWidgetVar = w);
                outPanel.childComponents.add(com);
            }*/
            /*END REMOVE*/
        }     
        return outPanel;
    }
    
    public Component.Apex.OutputPanel createOutputPanel(String pageId, String displayColumn)
    {   
        List<IPA_Page_Widget__c> lstWidgets = new List<IPA_Page_Widget__c>();
        
        //Get Widgets for given IPA_Page Id
        lstWidgets = dal.returnWidgets(pageId, displayColumn);
            
        return createPanel(lstWidgets);
    }
    
    public Component.Apex.OutputPanel createOutputPanelForTabGroup(IPA_Page_Widget__c pageWidgetObj)
    {   
        List<IPA_Page_Widget__c> lstWidgets = new List<IPA_Page_Widget__c>();
        lstWidgets = dal.returnChildWidgets(pageWidgetObj);
        return createPanel(lstWidgets);
    }
    
    public Component.Apex.OutputPanel createArticleDetailOutputPanel(String articleId)
    {
        Component.Apex.OutputPanel outPanel = new Component.Apex.OutputPanel();
        if(articleId != null){
            Component.c.IPA_ArticleDetailComponent ipaArticleDetailComponent = new Component.c.IPA_ArticleDetailComponent(articleIdVar = articleId);
            outPanel.childComponents.add(ipaArticleDetailComponent);    
        }
        return outPanel;
    }

    public IPA_Category__c returnPageCategory(String pageId)
    {
        try
        {
            IPA_Page__c p = [select Category__c from IPA_Page__c where Id =: pageId limit 1];
            IPA_Category__c c = [select Id, Name from IPA_Category__c where Id=:p.Category__c];
            return c;
        }catch(Exception ex) {}
        return null;
    }
}