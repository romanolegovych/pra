/** Implements the test for the Service Layer of the object LaboratoryMethodComedication__c
 * @author	Dimitrios Sgourdos
 * @version	24-Oct-2013
 */
@isTest
private class LaboratoryMethodComedicationServiceTest {
	
	// Global variables
	private static LaboratoryMethod__c					labMethod;
	private static List<LaboratoryMethodCompound__c>	compoundList;
	
	
	/** Initialize data
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static void init(){
		// Create Laboratory Method
		labMethod = new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
											Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood',
											Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle');
		insert labMethod; 
		
		// Create Laboratory Method Compounds
		compoundList = new List<LaboratoryMethodCompound__c>();
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(labMethod, 'Compound A') );
		compoundList.add( LaboratoryMethodCompoundService.createLaboratoryMethodCompoundInstance(labMethod, 'Compound B') );
		insert compoundList;
	}
	
	
	/** Test the function createLaboratoryMethodComedicationInstance
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void createLaboratoryMethodComedicationInstanceTest() {
		LaboratoryMethod__c tmpLabMethod = new LaboratoryMethod__c (Name='PRA-NL-LML-0001', AnalyticalTechnique__c='LC', AntiCoagulant__c='K2-EDTA',
																 Department__c='LML', Detection__c='MS/MS', Location__c='PRA-NL', Matrix__c='Blood', 
																 Proprietary__c='Y', ShowOnMethodList__c='Y', Species__c='Beagle');
		insert tmpLabMethod;
		
		LaboratoryMethodComedication__c comedication = new LaboratoryMethodComedication__c();
		comedication = LaboratoryMethodComedicationService.createLaboratoryMethodComedicationInstance(tmpLabMethod, 'Test assessment');
		
		system.assertNotEquals(comedication, NULL);
		system.assertEquals(comedication.LaboratoryMethod__c, tmpLabMethod.Id);
		system.assertEquals(comedication.Assessment__c, 'Test assessment');
	}
	
	
	/** Test the function createComedicationCompoundAssignmentWrapperInstance
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void createComedicationCompoundAssignmentWrapperInstanceTest() {
		// Create data
		init();
		
		LaboratoryMethodComedication__c comedication = new LaboratoryMethodComedication__c();
		LaboratoryMethodComedicationService.MethodComedicationWrapper comedWrapperItem = new LaboratoryMethodCOmedicationService.MethodComedicationWrapper();
		
		// Check the function with no assignment with compounds
		comedication = LaboratoryMethodComedicationService.createLaboratoryMethodComedicationInstance(labMethod, 'Assessment A');
		comedWrapperItem = LaboratoryMethodComedicationService.createComedicationCompoundAssignmentWrapperInstance(comedication, compoundList);
		
		system.assertEquals(comedWrapperItem.comedication.Id, comedication.Id);
		system.assertEquals(comedWrapperItem.comedCompoundValuesList.size(), 2);
		system.assertEquals(comedWrapperItem.comedCompoundValuesList[0].compoundId, compoundList[0].Id);
		system.assertEquals(comedWrapperItem.comedCompoundValuesList[0].comedicationValue, '');
		system.assertEquals(comedWrapperItem.comedCompoundValuesList[1].compoundId, compoundList[1].Id);
		system.assertEquals(comedWrapperItem.comedCompoundValuesList[1].comedicationValue, '');
		
		// Check the function with a comedication with reversed compounds than in compoundList (various fields from 01-20)
		comedication = LaboratoryMethodComedicationService.createLaboratoryMethodComedicationInstance(labMethod, 'Assessment A');
		comedication.Compound03ID__c   = compoundList[1].Id;
		comedication.Compound03Text__c = 'Assign with Compound B';
		comedication.Compound04ID__c   = compoundList[0].Id;
		comedication.Compound04Text__c = 'Assign with Compound A';
		comedWrapperItem = LaboratoryMethodComedicationService.createComedicationCompoundAssignmentWrapperInstance(comedication, compoundList);
		
		system.assertEquals(comedWrapperItem.comedication.Id, comedication.Id);
		system.assertEquals(comedWrapperItem.comedCompoundValuesList.size(), 2);
		system.assertEquals(comedWrapperItem.comedCompoundValuesList[0].compoundId, compoundList[0].Id);
		system.assertEquals(comedWrapperItem.comedCompoundValuesList[0].comedicationValue, 'Assign with Compound A');
		system.assertEquals(comedWrapperItem.comedCompoundValuesList[1].compoundId, compoundList[1].Id);
		system.assertEquals(comedWrapperItem.comedCompoundValuesList[1].comedicationValue, 'Assign with Compound B');
		
		// Check the function with a comedication with only one compound from the compoundList (various fields from 01-20)
		comedication = LaboratoryMethodComedicationService.createLaboratoryMethodComedicationInstance(labMethod, 'Assessment A');
		comedication.Compound11ID__c   = compoundList[1].Id;
		comedication.Compound11Text__c = 'Assign with Compound B';
		comedWrapperItem = LaboratoryMethodComedicationService.createComedicationCompoundAssignmentWrapperInstance(comedication, compoundList);
		
		system.assertEquals(comedWrapperItem.comedication.Id, comedication.Id);
		system.assertEquals(comedWrapperItem.comedCompoundValuesList.size(), 2);
		system.assertEquals(comedWrapperItem.comedCompoundValuesList[0].compoundId, compoundList[0].Id);
		system.assertEquals(comedWrapperItem.comedCompoundValuesList[0].comedicationValue, '');
		system.assertEquals(comedWrapperItem.comedCompoundValuesList[1].compoundId, compoundList[1].Id);
		system.assertEquals(comedWrapperItem.comedCompoundValuesList[1].comedicationValue, 'Assign with Compound B');
	}
	
	
	/** Test the function createMethodComedicationWrapperList
	 * @author	Dimitrios Sgourdos
	 * @version	24-Oct-2013
	 */
	static testMethod void createMethodComedicationWrapperListTest() {
		init();
		LaboratoryMethodComedication__c comedication = new LaboratoryMethodComedication__c();
		List<LaboratoryMethodComedication__c> comedicationList = new List<LaboratoryMethodComedication__c>();
		List<LaboratoryMethodComedicationService.MethodComedicationWrapper>  comedicationWrapperList = new List<LaboratoryMethodComedicationService.MethodComedicationWrapper>();
		
		// Add two comedications in the comedicationList
		comedication = LaboratoryMethodComedicationService.createLaboratoryMethodComedicationInstance(labMethod, 'Assessment A');
		comedicationList.add(comedication);
		
		comedication = LaboratoryMethodComedicationService.createLaboratoryMethodComedicationInstance(labMethod, 'Assessment B');
		comedication.Compound03ID__c   = compoundList[1].Id;
		comedication.Compound03Text__c = 'Assign with Compound B';
		comedication.Compound04ID__c   = compoundList[0].Id;
		comedication.Compound04Text__c = 'Assign with Compound A';
		comedicationList.add(comedication);
		
		comedicationWrapperList = LaboratoryMethodComedicationService.createMethodComedicationWrapperList(comedicationList, compoundList);
		
		system.assertEquals(comedicationWrapperList.size(), comedicationList.size());
		system.assertEquals(comedicationWrapperList[0].comedication.Id, comedicationList[0].Id);
		system.assertEquals(comedicationWrapperList[0].comedCompoundValuesList.size(), 2);
		system.assertEquals(comedicationWrapperList[0].comedCompoundValuesList[0].compoundId, compoundList[0].Id);
		system.assertEquals(comedicationWrapperList[0].comedCompoundValuesList[0].comedicationValue, '');
		system.assertEquals(comedicationWrapperList[0].comedCompoundValuesList[1].compoundId, compoundList[1].Id);
		system.assertEquals(comedicationWrapperList[0].comedCompoundValuesList[1].comedicationValue, '');
		system.assertEquals(comedicationWrapperList[1].comedication.Id, comedicationList[1].Id);
		system.assertEquals(comedicationWrapperList[1].comedCompoundValuesList.size(), 2);
		system.assertEquals(comedicationWrapperList[1].comedCompoundValuesList[0].compoundId, compoundList[0].Id);
		system.assertEquals(comedicationWrapperList[1].comedCompoundValuesList[0].comedicationValue, 'Assign with Compound A');
		system.assertEquals(comedicationWrapperList[1].comedCompoundValuesList[1].compoundId, compoundList[1].Id);
		system.assertEquals(comedicationWrapperList[1].comedCompoundValuesList[1].comedicationValue, 'Assign with Compound B');
	}
}