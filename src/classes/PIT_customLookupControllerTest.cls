@isTest
private class PIT_customLookupControllerTest {

    static testMethod void myUnitTest() {
        // create some records
        // User
        Profile pfl = [select id from profile where name='Standard User'];
        User PIT_customLookupControllerUser = new User(alias = 'u1', email='PIT_customLookupUser@testorg.com',
                                emailencodingkey='UTF-8', lastname='PIT_customLookupUser', languagelocalekey='en_US',
                                localesidkey='en_US', profileid = pfl.Id,  country='United States', CommunityNickname = 'PIT_customLookupUser',
                                timezonesidkey='America/Los_Angeles', username='PIT_customLookupUser@testorg.com',
                                 Department='testDepartment');
                                
       insert PIT_customLookupControllerUser ;
       
        // Country
        Country__c tmpCountry = new Country__c(Country_Code__c='US', Description__c='United States');
        insert tmpCountry;
        // Employee
        Employee_Details__c tmpEmpl = new Employee_Details__c(First_Name__c='testFirstName', Last_Name__c='Testing', Status__c='St', 
                                                            Employee_Unique_Key__c='testUniq', Date_Hired__c= date.newinstance(2010, 1, 30),
                                                            Country_Name__c=tmpCountry.id, Business_Unit__c='BU', Business_Unit_Desc__c='BU', 
                                                            Department__c='testDepartment', Job_Class_Desc__c='testJob');
        insert tmpEmpl;        
        // create the controller
        PIT_customLookupController p = new PIT_customLookupController(); // without search string
        PageReference pageRef = New PageReference(System.Page.PIT_customLookup.getUrl());
        pageref.getParameters().put('lksrch' , 'PIT_customLookupUser');
        Test.setCurrentPage(pageRef); 
        p = new PIT_customLookupController(); // with search string 'Testing'
        // system.debug('------------'+p.dummyUser.name);
        //System.assertEquals(p.dummyUser.name, 'PIT_customLookupUser');
        p.clearCriteria();
       
        //System.assertEquals(p.dummyUser.name, null);
    }
}