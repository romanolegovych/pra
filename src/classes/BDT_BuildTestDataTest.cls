/** Implements the test for the controller BDT_BuildTestData
 * @author	Dimitrios Sgourdos
 * @since	05-Dec-2013
 * @version	05-Dec-2013
 */
@isTest
private class BDT_BuildTestDataTest {
	
	/** The test is only for code coverage as in controller there are only functions 
	 *	for creating initial test data.
	 * @author	Dimitrios Sgourdos
	 * @version	05-Dec-2013
	 */
	static testMethod void myUnitTest() {
		BDT_BuildTestData.deleteAllData();
		BDT_BuildTestData.createAllData();
	}
	
}