/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PRA_ProjectWBSCVOTest {

    static testMethod void myUnitTest() {
    	test.startTest();

    	PRA_ProjectWBSCVO.ExportWBSCVO wbsCVO = new PRA_ProjectWBSCVO.ExportWBSCVO();
		PRA_ProjectWBSCVO.ExportBD1CVO bd1CVO = new PRA_ProjectWBSCVO.ExportBD1CVO();
		PRA_ProjectWBSCVO.ExportBD9CVO bd9CVO = new PRA_ProjectWBSCVO.ExportBD9CVO();

		system.assertEquals(PRA_ProjectWBSCVO.WBSCVO_NUM_FIELDS, wbsCVO.items.size());
		system.assertEquals(PRA_ProjectWBSCVO.WBSBD_NUM_FIELDS, bd1CVO.items.size());
		system.assertEquals(PRA_ProjectWBSCVO.WBSBD_NUM_FIELDS, bd9CVO.items.size());
		
		test.stopTest();    	
    }
}