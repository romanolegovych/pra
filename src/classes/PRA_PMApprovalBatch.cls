// MLH: this class is pretty much a stripped-down copy of Piotr Czechumski's PRA_ForecastUpdateBatch class.

global class PRA_PMApprovalBatch extends PRA_BatchaQueueable{

    //get the object, fields 
    private String query;
    private String query1;
    private String oName;
    private String fMonthAppliesTo;
    private String pName;
    
    global PRA_PMApprovalBatch (){
        
    }

    //Set  the parameters over the variables
    global override  void  setBatchParameters(String parametersJSON){
        List<String> params = (List<String>) Json.deserialize(parametersJSON, List<String>.class);
        oName = params.get(0);
        fMonthAppliesTo = params.get(1);
        pName = params.get(2);
        
     }
     
     
     global override Database.QueryLocator start(Database.BatchableContext BC){
        // get string with first day of current month
        Date fdm = System.today().toStartOfMonth();
        Datetime dt = Datetime.newInstance(fdm.year(), fdm.month(), fdm.day(), 0,0, 0);
        String firstDayOfCurrentMonth = dt.format('yyyy-MM-dd');
        
        //Check the dates
        Client_Project__c cp = [select id,Contracted_End_Date__c,Estimated_End_Date__c from Client_Project__c where name=:pName];
        
        //See if Project Estimated End date is not null 
        if(cp.Estimated_End_Date__c!=null ){
             query = 'select Id '  +
                     'from ' + oName + ' ' +
                     'where ' ; 
                     //Check if object is unit_Effort 
                     if(oName == 'Unit_Effort__c'){                
                       query +=  ' Client_Task__r.Project__r.Estimated_End_Date__c >= ' + firstDayOfCurrentMonth + ' ' + 
                                 'and ' + fMonthAppliesTo + '>= ' + firstDayOfCurrentMonth + ' ' +
                                 'and Client_Task__r.Project__r.Name=\'' + pName + '\' ' ;
                                 //'order by Type__c, Client_Task__c, ' + fMonthAppliesTo + ' DESC';
                     }else{
                     //Check if object is Hour_EffortRatio 
                       query +=  ' Unit_Effort__r.Client_Task__r.Project__r.Estimated_End_Date__c >= ' + firstDayOfCurrentMonth + ' ' + 
                                 'and ' + fMonthAppliesTo + '>= ' + firstDayOfCurrentMonth + ' ' +
                                 'and Unit_Effort__r.Client_Task__r.Project__r.Name=\'' + pName + '\' ' ;
                                 //'order by Type__c, Client_Task__c, ' + fMonthAppliesTo + ' DESC';                
                     
                     }                   
                    
         } else{
         //Check if Project Estimated End date is null then use Contracted End date
              query = 'select Id '  +
                      'from ' + oName + ' ' +
                      'where ' ;
                      //Check if object is unit_Effort
                      if(oName == 'Unit_Effort__c'){                
                           query += ' Client_Task__r.Project__r.Contracted_End_Date__c >= ' + firstDayOfCurrentMonth + ' ' + 
                                    'and ' + fMonthAppliesTo + '>= ' + firstDayOfCurrentMonth + ' ' +
                                    'and Client_Task__r.Project__r.Name=\'' + pName + '\' ' ;
                                    //'order by Type__c, Client_Task__c, ' + fMonthAppliesTo + ' DESC';
                       }else{
                            //Check if object is Hour_EffortRatio 
                           query +=  ' Unit_Effort__r.Client_Task__r.Project__r.Contracted_End_Date__c >= ' + firstDayOfCurrentMonth + ' ' + 
                                     'and ' + fMonthAppliesTo + '>= ' + firstDayOfCurrentMonth + ' ' +
                                     'and Unit_Effort__r.Client_Task__r.Project__r.Name=\'' + pName + '\' ' ;
                                      //'order by Type__c, Client_Task__c, ' + fMonthAppliesTo + ' DESC';                           
                        
                       }                     
                        
         }          
        // MLH: added a date filter here on month applies to
        if (Test.isRunningTest()){
            query += ' Limit 200';
        }
        system.debug('---' + query + '---');
        return Database.getQueryLocator(query);
    }
    
     global override void execute(Database.BatchableContext BC, List<Sobject> scope){
        
        Date dateFirstDayOfMonth = Date.today().toStartOfMonth();
        Datetime dt = Datetime.newInstance(dateFirstDayOfMonth.year(), dateFirstDayOfMonth.month(), dateFirstDayOfMonth.day(), 0,0, 0);
        String firstDayOfCurrentMonth = dt.format('yyyy-MM-dd');
        
        Set <Id> setobjIDs               = new Set <Id> ();
        List<Unit_Effort__c>  uniteffortlist = new List<Unit_Effort__c>(); 
        List<Hour_EffortRatio__c>  HourEffortRatiolist = new List<Hour_EffortRatio__c>(); 
         
         
         //Collect all the object record id's
         for (Sobject sObj : scope){  
           system.debug('-----sobj---'+sObj);        
           setobjIDs.add((ID)sObj.get('Id'));  
         }
         system.debug('-----setobjIDs---'+setobjIDs.size()); 
         
         //if the object is unit_Effort query to get all field values
         if(oName == 'Unit_Effort__c'){           
           uniteffortlist = [select id,Month_Applies_To__c,Forecast_Unit__c,Forecast_Effort__c,
                                   Last_Approved_Unit__c,Last_Approved_Effort__c,Last_Approved_Date__c,Month_Forecast_Created__c,
                                    LA_Total_Hour__c,LA_Total_Cost__c,LA_Total_Contracted_BDG_Cost__c,  LA_Total_Contracted_BDG_Hours__c,
                                    LA_Total_Capped_Cost__c,LA_Total_Capped_Hours__c,Sum_Capped_Cost__c,Total_Capped_Hour__c,
                                     Sum_Contracted_BDG_Cost__c,Total_Contracted_BDG_Hours__c, Total_Worked_Hours__c,Total_Forecast_Hours__c,
                                     Total_Worked_Cost__c, Sum_Forecast_Cost__c,Contracted_BDG_Unit__c,LA_Contracted_BDG_Unit__c,LA_Total_Forecast_Hours__c,LA_Total_Forecast_Cost__c     
                                    from Unit_Effort__c where id in :setobjIDs order by Month_Applies_To__c];
           for(Unit_Effort__c uf:uniteffortlist){ 
               system.debug('-----uf---'+uf);            
               uf.Last_Approved_Unit__c=uf.Forecast_Unit__c;
               uf.Last_Approved_Effort__c=uf.Forecast_Effort__c;
               uf.Last_Approved_Date__c=dateFirstDayOfMonth;              
               
               uf.LA_Total_Hour__c=((uf.Total_Worked_Hours__c== null)?0:uf.Total_Worked_Hours__c)+((uf.Total_Forecast_Hours__c== null)?0:uf.Total_Forecast_Hours__c);               
               uf.LA_Total_Cost__c=((uf.Total_Worked_Cost__c== null)?0:uf.Total_Worked_Cost__c)+((uf.Sum_Forecast_Cost__c== null)?0:uf.Sum_Forecast_Cost__c);
               
               uf.LA_Total_Contracted_BDG_Hours__c=uf.Total_Contracted_BDG_Hours__c;
               uf.LA_Total_Contracted_BDG_Cost__c=uf.Sum_Contracted_BDG_Cost__c;
               
               uf.LA_Total_Capped_Hours__c=uf.Total_Capped_Hour__c;
               uf.LA_Total_Capped_Cost__c=uf.Sum_Capped_Cost__c; 
               
               uf.LA_Contracted_BDG_Unit__c=uf.Contracted_BDG_Unit__c;   
               
               uf.LA_Total_Forecast_Hours__c=((uf.Total_Forecast_Hours__c== null)?0:uf.Total_Forecast_Hours__c);
               uf.LA_Total_Forecast_Cost__c=((uf.Sum_Forecast_Cost__c== null)?0:uf.Sum_Forecast_Cost__c);     
           
           }
           //Update UnitEffortlist 
           if(uniteffortlist.size()>0)
             update uniteffortlist;
             system.debug('-----uniteffortlist---'+uniteffortlist);          
         
         }else{
         //if the object is Hour_EffortRatio query to get all field values
           HourEffortRatiolist = [select id,Unit_Effort__r.Month_Applies_To__c,Forecast_Hours__c,Forecast_ER__c,
                                  Last_Approved_Hours__c,Last_Approved_ER__c,Unit_Effort__r.Last_Approved_Date__c ,
                                  LA_Contracted_BDG_Hours__c,LA_Capped_Hours__c,LA_Capped_Cost__c,Capped_Cost__c,Capped_Hours__c,
                                  Contracted_BDG_Cost__c,Contracted_BDG_Hours__c,LA_Contracted_BDG_Cost__c                                            
                                    from Hour_EffortRatio__c where id in :setobjIDs order by Unit_Effort__r.Month_Applies_To__c];
           for(Hour_EffortRatio__c her:HourEffortRatiolist){ 
               system.debug('-----her---'+her);          
               her.Last_Approved_Hours__c=her.Forecast_Hours__c;
               her.Last_Approved_ER__c=her.Forecast_ER__c;
               //her.Unit_Effort__r.Last_Approved_Date__c=dateFirstDayOfMonth;
               
               her.LA_Contracted_BDG_Hours__c=  her.Contracted_BDG_Hours__c;
               her.LA_Contracted_BDG_Cost__c=  her.Contracted_BDG_Cost__c; 
                                               
               her.LA_Capped_Hours__c=  her.Capped_Hours__c; 
               her.LA_Capped_Cost__c =  her.Capped_Cost__c;                                
           
           }
            //Update HourEffortRatiolist 
           if(HourEffortRatiolist.size()>0)
             update HourEffortRatiolist;
             system.debug('-----uniteffortlist---'+uniteffortlist);        
         
         }
        
        }
        global override void finish(Database.BatchableContext BC){
           
        }
}