public with sharing class PRA_ForecastUpdateSchedulerEffort {
	
	public static final String THIS_CLASS_IS_DEPRACTED = 'Remove after Migration';
    public String testValue {get;set;}
    
    public PRA_ForecastUpdateSchedulerEffort() {
    	testValue = THIS_CLASS_IS_DEPRACTED;
    }
}