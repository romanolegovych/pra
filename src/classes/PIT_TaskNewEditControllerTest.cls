@isTest
private class PIT_TaskNewEditControllerTest {

    static testMethod void myUnitTest() {
        // create the controller with New version
 //       Client_Project__c objproject = new Client_Project__c();
 //       objproject.Name= 'Test';
 //       objproject.Discount_Type__c ='%';
 //       objproject.Volume_Discount_Type__c ='%';
 //       insert objproject;
  // create the controller with New version
        WFM_Client__c client = new WFM_Client__c();
        client.Name ='test clt';
        client.Client_Name__c ='test clt';
        client.Client_Unique_Key__c='test clt';
        insert client;
         
        WFM_Contract__c objContract =new WFM_Contract__c();
        objContract.Name ='test';
        objContract.Client_ID__c =client.Id;
        objContract.Contract_Status__c='12';
        objContract.Contract_Unique_Key__c='test clt';
        insert objContract;
        WFM_Project__c objProj = new WFM_Project__c();
        objProj.Name = 'test';
        objProj.Contract_ID__c =objContract.Id;
        objProj.Project_Unique_Key__c ='tesfsdf';
        insert objProj;
        Issue__c tmpIssue = new Issue__c();
        tmpIssue.Project__c = objProj.Id;
        insert tmpIssue;
        //Issue__c tmpIssue = new Issue__c();
//        tmpIssue.Project__c =objproject.Id;
        //insert tmpIssue;
        ApexPages.standardController stdCtr;
        PageReference pageRef = New PageReference(System.Page.PIT_TaskNewEdit.getUrl());
        pageref.getParameters().put('retURL' , 'thoughException');
        Test.setCurrentPage(pageRef); 
        PIT_TaskNewEditController p = new PIT_TaskNewEditController(stdCtr);
        pageRef = New PageReference(System.Page.PIT_TaskNewEdit.getUrl());
        pageref.getParameters().put('retURL' , '/' + tmpIssue.id);
        Test.setCurrentPage(pageRef); 
        p = new PIT_TaskNewEditController(stdCtr);
        p.ta.description = 'testing 1';
        p.SaveNewTask();
        p.ta.description = 'testing 2';
        p.save();
        p.cancel();
        // For throwing the exceptions to achieve the percentage of the coverage
        p.SaveNewTask();
        // Test the save
        List<Task> taskList = [SELECT id FROM Task];
        System.assertEquals(taskList.size(), 2);
        // Edit version
        // create the controller
        Task tmpTask = [SELECT id FROM Task LIMIT 1];
        pageRef = New PageReference(System.Page.PIT_TaskNewEdit.getUrl());
        pageref.getParameters().put('id' , tmpTask.id);
        pageref.getParameters().put('retURL' , '/' + tmpIssue.id);
        Test.setCurrentPage(pageRef); 
        p = new PIT_TaskNewEditController(stdCtr); // can find ID
        System.assertEquals(p.ta.id, tmpTask.id);
        p.Ta.Description = 'testing';
        p.save();
        Task tmpTask2 = [SELECT Description FROM Task WHERE id = :tmpTask.id];
        System.assertEquals(tmpTask2.Description, 'testing');
        p.showAttachSelPanel();
        p.hideAttachSelPanel();
        p.showAttachSelPanel();
        p.insertAttachment();
        p.removeAttach();
        p.viewAttach();
        p.cancel();
    }
}