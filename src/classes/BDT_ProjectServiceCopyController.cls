public with sharing class BDT_ProjectServiceCopyController {
	
	public list<SelectOption> 	SourceStudySelectOption {get;set;}
	public string 			  	SelectedSourceStudy 	{get;set;}
	public list<SelectOption> 	TargetStudySelectOption {get;set;}
	public string			  	SelectedTargetStudy 	{get;set;}
	public string				SelectedCopyMethod		{get;set;}
	
	public boolean				 	ShowCopy {get;set;}
	public list<Study__c> 		 	StudyList;
	public string				 	ProjectID;
	public List<serviceMessage>  	serviceMessageList {get;set;}
	public List<StudyService__c> 	ssFinalTargetList; 
	public Map<Id, ProjectService__c>	prSrvFinalTargetMap;
	
	public String SourceStudyCode {get;set;}
	public String TargetStudyCode {get;set;}
	
	// search variables
	public string 				ProjectSearchTerm 	{get;set;}
	public string 				ProjectSearchError  {get;set;}
	// project selection variables
	public string 				SelectedProjectID 	{get;set;}
	public List<SelectOption> 	ProjectSelectList 	{get;set;}
	public boolean 				EnableProjectSelect {get;set;}	
		
		
		
	public BDT_ProjectServiceCopyController() { 
		ShowCopy = true;
		serviceMessageList = new List<serviceMessage>();
		ssFinalTargetList 	 = new List<StudyService__c>(); 
		prSrvFinalTargetMap  = new Map<Id, ProjectService__c>();
		try{
			ProjectID = BDT_Utils.getPreference('SelectedProject');			
			StudyList = BDT_Utils.readSelectedStudies();
			if (String.isNotEmpty(ProjectID)) {
				ReadProjectSelectList();
				// read studies for project
				SelectedCopyMethod = 'z';	// 'z' is associated with no resolution selection	
			}
		} catch (Exception e) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to load study information. Please select a project and reload the page.');
         	ApexPages.addMessage(msg);
		}
	}
	
	
	public void ButtonSearchProject(){
		ReadProjectSelectList();
	}
	
	public void ReadProjectSelectList () {
		/* if there is a search string then parse the search string else
		 * else get the project code for the current project
		 */
		ProjectSelectList 	= new List<SelectOption>();
		SelectedProjectID 	= '';
		EnableProjectSelect = true;
		if (String.isEmpty(ProjectSearchTerm)) {
			// get the current project and creat a single item in the dropdownlist
			Client_Project__c cp = BDT_Utils.getProject(ProjectID, false);
			ProjectSelectList.add(new SelectOption(cp.id, (cp.Code__c!=null)?cp.Code__c:'No Code'));
			SelectedProjectID = cp.id;
			EnableProjectSelect = false;
		} else {
			// search for matching projects
			List<Client_Project__c> projects = searchProjects();
			if (projects!=null && projects.size()>0) {
				// create dropdown values for query results
				For (Client_Project__c project:projects){
					ProjectSelectList.add(new SelectOption(project.id,project.Code__c));
				}
				SelectedProjectID = projects[0].id;
			} else {
				// no search results found
				ProjectSelectList.add(new SelectOption('0','Query returned no results.'));
				EnableProjectSelect = false;
			}
		}
		fillSourceStudySelectOption();
		fillTargetStudySelectOption();
	}
	
	public List<Client_Project__c> searchProjects () {
		ProjectSearchError = '';
		try {
				String searchquery = 'FIND \''+ProjectSearchTerm+'*\' IN ALL FIELDS RETURNING';
				searchquery += ' Client_Project__c(Code__c, Title__c, Project_Description__c, id WHERE code__c != null and bdtdeleted__c = false ORDER BY Code__c LIMIT 100)';
				List<List<SObject>> searchList = search.query(searchquery);
				List<Client_Project__c> projectSearchResultList = (List<Client_Project__c>)searchList[0];
				return projectSearchResultList;
				
		}catch (Exception e) {				
			ProjectSearchError='Your search could not be completed. Please rephrase your search term.';
		}
		return null;
	}
	
	
	public list<SelectOption> getCopyOptions() {
		list<SelectOption> CopyOptions = new list<SelectOption>();
		CopyOptions.add(new SelectOption('a','Copy all units and overwrite existing units.'));
		CopyOptions.add(new SelectOption('b','Copy all units, but keep existing units.'));
		CopyOptions.add(new SelectOption('c','Copy all units, and sum copied and existing units.'));
		return CopyOptions;
	}
	
	
	public PageReference CancelBack() {
		return new PageReference(System.Page.BDT_ProjectServices.getUrl());
	}
	
	
	public void fillSourceStudySelectOption() {
		List<Study__c> tmpStudyList = new List<Study__c>();
		SourceStudySelectOption = new list<SelectOption>();
		SourceStudySelectOption.add(new SelectOption('0','Please select Source'));
		SelectedSourceStudy = '0';
		tmpStudyList = readStudiesOfSelectedProject (SelectedProjectID);
		for (Study__c st : tmpStudyList) {
			SourceStudySelectOption.add(new SelectOption(st.id,st.code__c));
		} 
	}
	
	public List<Study__c> readStudiesOfSelectedProject (String selProjectId) {
		List<Study__c> prStudies = new List<Study__c>();
		try {
			prStudies = [SELECT Title__c, 
								Status__c, 
								Sponsor_Code__c, 
								Protocol_Title__c, 
								Project__c, 
								Name, 
								Legacy_Code__c, 
								BDTDeleted__c, 
								Id, 
								Code__c, 
								Business_Unit__c, 
								Business_Unit__r.Name 
						 FROM 	Study__c 
						 WHERE 	Project__c=:selProjectId AND 
						 		BDTDeleted__c = false
						 ORDER BY code__c];
			
		} catch (Exception e) {
			
		}
		return prStudies; 
	}
	
	
	public void fillTargetStudySelectOption() {
		TargetStudySelectOption = new list<SelectOption>();
		SelectedTargetStudy = '0';
		if(SelectedSourceStudy != '0') {
			TargetStudySelectOption.add(new SelectOption('0','Please select Target'));
			for (Study__c st : StudyList) {
				if(st.id != SelectedSourceStudy) {
					TargetStudySelectOption.add(new SelectOption(st.id, st.code__c));
				}
			}
		} 	
	}
	
	
	public boolean validateDataEntry() {
		boolean valid = true; 
		ApexPages.Message msg;
		if(SelectedProjectID == '') {
			msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No project selected.');
         	ApexPages.addMessage(msg);
         	valid = false;
		}
		if (SelectedSourceStudy == '0' ) {
			msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No source study selected.');
         	ApexPages.addMessage(msg);
         	valid = false;
		}
		if (SelectedTargetStudy == '0' ) {
			msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No target study selected.');
         	ApexPages.addMessage(msg);
         	valid = false;
		}
		if (SelectedCopyMethod == null || SelectedCopyMethod == 'z' ) {
			msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'No conflict resolution selected.');
         	ApexPages.addMessage(msg);
         	valid = false;
		}
		return valid; 
	}
	
	
	public List<StudyService__c> readSourceStudyServices() {
		List<StudyService__c> sspList =  [SELECT 	NumberOfUnits__c, 
													ProjectService__r.id,
												 	ProjectService__r.Service__r.id,
												 	ProjectService__r.Service__r.Name, 
												 	ProjectService__r.Service__r.ServiceCategory__r.Code__c,
												 	ProjectService__r.Service__r.ServiceCategory__r.Name
										  FROM	 	StudyService__c 
										  WHERE	 	ProjectService__r.Client_Project__c = :SelectedProjectID 	AND
										  			Study__r.Project__c = :SelectedProjectID					AND
												 	Study__r.code__c = :SourceStudyCode 			  			AND
												 	ProjectService__r.Service__r.IsDesignObject__c = false  	AND
												 	ProjectService__r.Service__r.BDTDeleted__c	   = false
										  ORDER BY 	ProjectService__r.Service__r.ServiceCategory__r.Code__c,  
												 	ProjectService__r.Service__r.SequenceNumber__c ];
		return sspList; 
	}
	
	
	public Map<String,StudyService__c> readTargetStudyServices() {
		Map<String,StudyService__c> targetMap = new Map<String,StudyService__c>();
		List<StudyService__c> sspList =  [SELECT 	NumberOfUnits__c, 
												 	ProjectService__r.id,
												 	ProjectService__r.Service__r.id,
												 	ProjectService__r.Service__r.Name, 
												 	ProjectService__r.Service__r.ServiceCategory__r.Code__c,
												 	ProjectService__r.Service__r.ServiceCategory__r.Name
										  FROM	 	StudyService__c
										  WHERE	 	ProjectService__r.Client_Project__c = :ProjectID 		AND
										  			Study__r.Project__c = :ProjectID						AND
												 	Study__r.code__c = :TargetStudyCode 			  		AND
												 	ProjectService__r.Service__r.IsDesignObject__c = false	AND
												 	ProjectService__r.Service__r.BDTDeleted__c	   = false
										  ORDER BY 	ProjectService__r.Service__r.ServiceCategory__r.Code__c,  
												 	ProjectService__r.Service__r.SequenceNumber__c ];
		for(StudyService__c ssp : sspList) {
			targetMap.put(ssp.ProjectService__r.Service__r.id, ssp);
		}
		return targetMap; 
	}
	
	
	public Map<Id, ProjectService__c> readProjectServicesOfCurrentProject(String currProjectId) {
		Map<Id, ProjectService__c> tmpMap  = new Map<Id, ProjectService__c>();
		List<ProjectService__c>		   tmpList = new List<ProjectService__c>();
		tmpList =  [SELECT 	Id,
							Service__r.Id
					FROM	ProjectService__c
					WHERE	Client_Project__c = :currProjectId	AND
							BDTDeleted__c = false]; 
		for(ProjectService__c tmpItem : tmpList) {
			tmpMap.put(tmpItem.Service__r.Id, tmpItem);
		}
		return tmpMap; 
	}
	
	
	public String calculateResultAmount(Decimal sourceAmount, Decimal targetAmount) {
		if(SelectedCopyMethod == 'a') { // Copy all and overwrite existing units
			return String.valueOf(sourceAmount);
		} else if(SelectedCopyMethod == 'b') { // Copy all but keep existing units
			return String.valueOf(targetAmount);
		} else if(SelectedCopyMethod == 'c') { // Copy all and sum copied and existing units
			return String.valueOf(sourceAmount+targetAmount);
		}
		// As default return source nimber of units
		return String.valueOf(sourceAmount);
	}
	
	
	public void testCopyStudyServices() {
		// Define and Initialize variables
		ApexPages.Message 			msg;
		List<StudyService__c>       sspSourceList = new List<StudyService__c>();
		Map<String,StudyService__c> sspTargetMap  = new Map<String,StudyService__c>();
		StudyService__c 			newTarget	  = new StudyService__c();	
		Map<Id, ProjectService__c> prSrvTargetMap = new Map<Id, ProjectService__c>(); 
		serviceMessage 				serviceMessageItem;	
		serviceMessageList = new List<serviceMessage>();	
		// Procedure to the dummy/test run
		if (validateDataEntry()) {
			ssFinalTargetList 	 = new List<StudyService__c>(); 
			prSrvFinalTargetMap  = new Map<Id, ProjectService__c>();
			ShowCopy = false;
			// Read the user selection
			for (Study__c study :[select code__c from study__c where id = :SelectedSourceStudy or id = :SelectedTargetStudy]){
				if (study.id == SelectedSourceStudy) {SourceStudyCode = study.code__c;}
				if (study.id == SelectedTargetStudy) {TargetStudyCode = study.code__c;}
			} 
			// Retrieve the data for the Source and the target
			sspSourceList 	 = readSourceStudyServices();
			sspTargetMap  	 = readTargetStudyServices();
			prSrvTargetMap	 = readProjectServicesOfCurrentProject(ProjectID); 
			// Create ProjectService records if necessary
			for(StudyService__c ssp : sspSourceList){
				// if there is not a ProjectService for the current project and the current service, 
				// create one and add it to the map that will be deleted if the whole procedure will be cancelled
				ProjectService__c tmpPrSrvItem = new ProjectService__c();
				if( ! prSrvTargetMap.ContainsKey(ssp.ProjectService__r.Service__r.id) ) {
					tmpPrSrvItem.Client_Project__c = ProjectID;
					tmpPrSrvItem.Service__c 	   = ssp.ProjectService__r.Service__r.id;
					prSrvFinalTargetMap.put(ssp.ProjectService__r.Service__r.id, tmpPrSrvItem);
				} 
			}
			// Insert the new ProjectService records to take a record id
			upsert prSrvFinalTargetMap.values();
			// Copy the added ProjectService records to the prSrvTargetMap, so the prSrvTargetMap has all the ProjectServices of the current project
			for(Id tmpId : prSrvFinalTargetMap.keySet() ) {
				prSrvTargetMap.put(tmpId, prSrvFinalTargetMap.get(tmpId));				
			}
			// Create the table rows			
			for(StudyService__c ssp : sspSourceList) {
				ProjectService__c tmpPrSrvItem = new ProjectService__c();
				if( prSrvTargetMap.containsKey(ssp.ProjectService__r.Service__r.id) ) { // Just in case, cause normally it will always contain it 
					tmpPrSrvItem = prSrvTargetMap.get(ssp.ProjectService__r.Service__r.id); 
				}
				serviceMessageItem = new serviceMessage();
				serviceMessageItem.ServiceCategoryCode = bdt_utils.removeLeadingZerosFromCat(ssp.ProjectService__r.Service__r.ServiceCategory__r.Code__c);
				serviceMessageItem.ServiceCategoryName = ssp.ProjectService__r.Service__r.ServiceCategory__r.Name;
				serviceMessageItem.ServiceId 		   = ssp.ProjectService__r.Service__r.id;
				serviceMessageItem.ServiceName 		   = ssp.ProjectService__r.Service__r.Name;
				serviceMessageItem.setSourceAmount(String.valueOf(ssp.NumberOfUnits__c));
				// Check if this service is already assigned in target
				if (sspTargetMap.containsKey('' + ssp.ProjectService__r.Service__r.id) ) {
					newTarget = sspTargetMap.get('' + ssp.ProjectService__r.Service__r.id);
					serviceMessageItem.setTargetAmount(string.valueOf(newTarget.NumberOfUnits__c));
					serviceMessageItem.setResultingAmount(calculateResultAmount(ssp.NumberOfUnits__c, newTarget.NumberOfUnits__c));
					newTarget.NumberOfUnits__c = Decimal.valueOf(serviceMessageItem.getResultingAmount());
					ssFinalTargetList.add(newTarget);
				}
				else {
					serviceMessageItem.setTargetAmount('-');
					serviceMessageItem.setResultingAmount(serviceMessageItem.SourceAmount);
					newTarget = new StudyService__c();
					newTarget.NumberOfUnits__c  = Decimal.valueOf(serviceMessageItem.getResultingAmount());
					newTarget.ProjectService__c = tmpPrSrvItem.id;
					newTarget.Study__c		 	= SelectedTargetStudy;
					ssFinalTargetList.add(newTarget);
				}
				// Add the record
				serviceMessageList.add(serviceMessageItem);
			}
		} // end of if (validateDataEntry())
	} 
	
	
	public void Cancel() {
		delete prSrvFinalTargetMap.values(); 
		ShowCopy = true;
	}
	
	
	public PageReference ApplyTestRun () {	
		try{
			if(	! ssFinalTargetList.isEmpty() ) {
				upsert ssFinalTargetList;
			}
		} catch (Exception e) {
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Action failed. ' + e));
		}
		return new PageReference(System.Page.BDT_ProjectServices.getUrl()); 		
	}
	
	
	public class serviceMessage {
		public String ServiceCategoryCode {get;set;}
		public String ServiceCategoryName {get;set;}
		public String ServiceId {get;set;}
		public String ServiceName {get;set;}
		
		private String SourceAmount;
		public String getSourceAmount(){return SourceAmount;}
		public void setSourceAmount(String value){
			SourceAmount = BDT_Utils.removeTailingZerosFromDecimals(value);
		}
		
		private String TargetAmount;
		public String getTargetAmount(){return TargetAmount;}
		public void setTargetAmount(String value){
			TargetAmount = BDT_Utils.removeTailingZerosFromDecimals(value);
		}
		
		private String ResultingAmount;
		public String getResultingAmount(){return ResultingAmount;}
		public void setResultingAmount(String value){
			ResultingAmount = BDT_Utils.removeTailingZerosFromDecimals(value);
		}
	}	
}