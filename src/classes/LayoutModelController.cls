public with sharing class LayoutModelController {

    public String instanceModelName {get;set;}
    public ServiceModelController serviceModelController {get;set;}
    public SRMModelController sRMModelController {get;set;}
    public RegionModelsController regionModelsController {get;set;}
    public WRModelsController wRModelsController {get;set;}
    public BillRateCardModelsController billRateCardModelsController {get;set;}
    public BillFormulaModelsController billFormulaModelsController {get;set;}

    /** 
    @Author Oleksiy Tkachenko
    @Date 2015
    @Description Constructer
    */
    public LayoutModelController() {
    }

    private ComponentControllerBase componentController;
    
    public virtual ComponentControllerBase getMyComponentController() {
        return componentController;
    }

    public virtual void setComponentController(ComponentControllerBase compController) {
        componentController = compController;
    }

    /** 
    @Author Oleksiy Tkachenko
    @Date 2015
    @Description Instance ServiceModelController
    */
    public void instanceServiceModelController() {
        if(instanceModelName == 'Service_Models') {
            serviceModelController = new ServiceModelController();
        } else if(instanceModelName == 'SRM_Models') {
            sRMModelController = new SRMModelController();
        } else if(instanceModelName == 'Region_Models') {
            regionModelsController = new RegionModelsController();
        } else if(instanceModelName == 'WR_Models') {
            wRModelsController = new WRModelsController();
        } else if(instanceModelName == 'Bill_Rate_Card_Models') {
            billRateCardModelsController = new BillRateCardModelsController();
        } else if(instanceModelName == 'Bill_Formula_Models') {
            billFormulaModelsController = new BillFormulaModelsController();
        }
    }
}