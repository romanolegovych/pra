/**
*   'HR_QuestionnaireController' is to create HR_Questionnaire object date for HR Exit Interview form
*   @author   Devaram Bhargav
*   @version  25-Sep-2013
*   @since    17-Sep-2013
*/
public class HR_QuestionnaireController {
    /**
    *   'HR_QuestionnaireController' is to create fill the HR Questionnarie form,
    *   and update/insert the data into the HR_Questionnaire__c object for its associated HR_Exit_Interview__c object. 
    */
        
    /** EXIid variable is to hold the ID value of HR_Exit_interview__c object*/
    Private ID EXIid{get;set;}
    public Boolean displayPopup {get;set;} 
    public List<pdfgeneratecls> innerclasslist{get;set;}      
    
    /** PAGE_MODE,PAGE_MODE_VIEW,PAGE_MODE_EDIT variable is to display the type of mode in Visual force page*/     
    Public String PAGE_MODE{get;private set;}
    public String PAGE_MODE_VIEW {get{return 'View';}}
    public String PAGE_MODE_EDIT {get{return 'Edit';}}
    
    /** ListofHRQCVOList variable is the List of a HR_QuestionnaireList*/
    public List<List<HR_QuestionnaireCVO>> ListofHRQCVOList{get;set;}
    
    /**
    *   Constructor that directs from the standard page.
    *   @param    controller  The controller is the ID of the HR_Exit_Interview__c object record.      
    */
    public HR_QuestionnaireController(ApexPages.StandardController controller) {
        EXIid  = ((HR_Exit_Interview__c)controller.getRecord()).id;
        innerclasslist = new List<pdfgeneratecls>();
        /** Verifies whether the EXIis has HR Questionnaire records,
        *  if so dispalys other wise create the records
        */         
        if((HR_ExitinterviewService.getHRQuestionnaireDatabyEXIid(EXIid)).size()>0){
            ViewHRQuestionnaire(); 
        }else{
            EditHRQuestionnaire();
        }   
    }    
    
    /**
    *   This method retrieve the records in the view mode 
    *   of HR_Questionnaire__c for EXIid.          
    */
    public void ViewHRQuestionnaire() {
       PAGE_MODE=PAGE_MODE_VIEW ;
       HRQuestionnaire();
    }
        
    /**
    *   This method retrieve the records in the Edit mode 
    *   of HR_Questionnaire__c for EXIid.          
    */
    public void EditHRQuestionnaire() {
       PAGE_MODE=PAGE_MODE_EDIT ;
       HRQuestionnaire();
    }
        
    /**
    *   This method binds the HR_Questionnaire__c data to the 
    *   List of List (ListofHRQCVOList).          
    */
    public void HRQuestionnaire() {
        ListofHRQCVOList=new List<List<HR_QuestionnaireCVO>>();
        ListofHRQCVOList=HR_ExitinterviewService.getHRQuestionnaire(EXIid);
        system.debug('--------ListofHRQCVOList------------'+ListofHRQCVOList);
        integer i=0;
       /* for(integer g=0;g<ListofHRQCVOList.size()-1;g++)
        {       
            i=i+1;
            pdfgeneratecls pdfcls = new pdfgeneratecls();
            pdfcls.ListofHRQCVOList1.add(ListofHRQCVOList[g]);
            if(math.mod(i,5)==0)
            {
                pdfcls.PageBreak = 'page-break-after:always';
            }        
            innerclasslist.add(pdfcls);
                 
        } */

       }
    
    /**
    *   This save method unbinds the ListofHRQCVOList data to the 
    *   HR_Questionnaire__c List  and upserts the lista and show the data in view mode.          
    */ 
    public void SaveHRQuestionnaire() {    
        String ErrorMsg=HR_ExitinterviewService.saveHRQuestionnairefromlistofHRQCVOlist(ListofHRQCVOList);
        system.debug('------ErrorMsg-----------'+ErrorMsg);
        if(ErrorMsg==null || ErrorMsg==''){        
            ViewHRQuestionnaire();
        }else{
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ErrorMsg));  
        }        
    }  
    
    /**
    *   This Delete method delete all the  
    *   HR_Questionnaire__c date for the EXIid and direct back to Exit Interview record.          
    */ 
    public PageReference  DeleteHRQuestionnaire() {        
        PageReference pageRef;
        String ErrorMsg=HR_ExitinterviewService.DeleteHRQuestionnairebyEXIid(EXIid);
        system.debug('------ErrorMsg-----------'+ErrorMsg);
        if(ErrorMsg==null || ErrorMsg==''){        
             pageRef=new PageReference('/' +EXIid);
             pageRef.setRedirect(true);                
        }else{
             Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ErrorMsg));  
        }
        return pageRef;
    }
    public void showPopup()
    {        
    displayPopup = true;
    
    }
    
    public void closePopup() {
        displayPopup = false;
        
    }
    public PageReference redirectPopup()
    {
       displayPopup = false;
       system.debug('--EXIid----'+EXIid);
       PageReference p=new Pagereference('/'+EXIid);
       p.setRedirect(true);
       return p;
        
    } 
    public class pdfgeneratecls{
       public List<List<HR_QuestionnaireCVO>> ListofHRQCVOList1;
       public string pagebreak;
       public pdfgeneratecls(){
       }
    
    }  
}