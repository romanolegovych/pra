global with sharing class BDT_Search {
		/******* SEARCH ******/
	
	Public String searchTerm {get;set;}
	
	public list<displayWrapper>		DisplayList {get;set;}
	public Boolean 					DisplaySearchResults	{get;set;}
	Public Integer					SOSLlimit;
	Public String					SearchError{get;set;}
		
	Public BDT_Search() {
		DisplaySearchResults = false;
		SearchError = '';
	}
	
	Public void searchObjects() {
		SOSLlimit = 20;
		
		SearchError = '';
		// the minimal number of characters to start a search is 2
		if (searchTerm.length()<2) {
			DisplaySearchResults = false;
			SearchError = 'Unable to complete your search, please specify at least 2 characters to initiate a search.';
		} else {
	
			try {
	
				DisplaySearchResults = true;
				String searchquery = 'FIND \''+searchTerm+'*\' IN ALL FIELDS RETURNING';
				searchquery += ' Client_Project__c(Code__c, Title__c, Project_Description__c, id, Client__r.Name WHERE bdtdeleted__c = false ORDER BY Code__c LIMIT SOSLlimit)';
				searchquery += ', Study__c(Protocol_Title__c, Title__c, Sponsor_Code__c, Legacy_Code__c, Id, Code__c, Project__c, Project__r.Code__c  WHERE bdtdeleted__c = false ORDER BY Code__c LIMIT SOSLlimit)';
				
				searchquery = searchquery.replace('SOSLlimit',''+SOSLlimit);
				
				List<List<SObject>> searchList = search.query(searchquery);
				
				// write results into their specific lists
				Set<String> ProjectIds = new Set<String>();
				For (Client_Project__c cp : (List<Client_Project__c>)searchList[0]) {
					ProjectIds.add(cp.id);
				}
				For (Study__c s : (List<Study__c>)searchList[1]) {
					ProjectIds.add(s.project__c);
				}
				
				// Add queries for Bussiness Units Name
				String soqlSearchterm = '%' + searchTerm + '%';
				List<Study__c> tmpStudies = [SELECT Id, Project__c
											FROM	Study__c
											WHERE	(Project__c not IN :ProjectIds)
											AND		(Business_Unit__r.Name like :soqlSearchterm)
											limit	:SOSLlimit];
				for (Study__c s : tmpStudies) {
					ProjectIds.add(s.project__c);
				}
				
				// Add queries for Sponsor Name and Sponsor Entities Name
				List<Client_Project__c> tmpProjects =  [SELECT	Id
														FROM	Client_Project__c
														WHERE	(Id not IN :ProjectIds)
														AND		(Client__r.Sponsor__r.Name like :soqlSearchterm
																OR Client__r.Name like :soqlSearchterm)
														limit	:SOSLlimit];
				Set<String> tmpProjectIdsSet = BDT_Utils.getStringSetFromList(tmpProjects, 'Id');
				ProjectIds.addAll(tmpProjectIdsSet);
				
				// Build data
				buildDisplayWrapper(ProjectIds);
				
			}catch (Exception e) {
				DisplaySearchResults = false;
				SearchError='Your search could not be completed. Please rephrase you search term.';
				
			}
		
		}
		
	}
	
	public void buildDisplayWrapper(Set<String> ProjectIds){
		//concSponsorSponsorEntity = '['+singleProject.Client__r.Sponsor__r.Name+'] '+singleProject.Client__r.Name;
		List<Client_Project__c> cpList = [
			select 	Code__c, 
					Title__c, 
					Project_Description__c, 
					id,
					Client__r.Sponsor__r.Name,
					Client__r.Name,
					(select Protocol_Title__c, 
							Title__c, 
							Sponsor_Code__c, 
							Legacy_Code__c, 
							Id, 
							Code__c, 
							Project__c
					from	Studies__r
					where	bdtdeleted__c = false
					order   by code__c)
			from	Client_Project__c
			where	id in :ProjectIds
			and		bdtdeleted__c = false
			order   by code__c
		];
		DisplayList = new list<displayWrapper>();
		for (Client_Project__c cp : cpList) {
			displayWrapper displayItem = new displayWrapper();
			displayItem.project = cp;
			displayItem.sponsorname = '['+cp.Client__r.Sponsor__r.Name+'] '+cp.Client__r.Name;
			displayItem.studieList = cp.Studies__r;
			
			// clean up empty fields
			if (cp.Client__r.Name == null) {displayItem.sponsorname = 'Sponsor not specified.';}
			
			DisplayList.add(displayItem);
		}
		
	}
	
	
	public void closeSearch() {
		DisplaySearchResults = false;
	}

	@RemoteAction
	global static String SetProjectAndStudies(String ProjectID, String StudyIDs) {

		BDT_UserPreferenceService UserPreferences = new BDT_UserPreferenceService();
		Set<String> StudyIdSet = new Set<String>();
		try{
			for (String studyId : StudyIDs.split(':')){
				StudyIdSet.add(studyId);
			}
		} catch (Exception e) {}

		Client_Project__c project;
		try{
			project = [select id, code__c from Client_Project__c where id = :ProjectID];
		} catch (Exception e) {}

		if (project!= null) {
			UserPreferences.setProjectAndStudies(project, StudyIdSet);
		}
		UserPreferences.saveUserPreferences();
		
		return 'Finished';
		
	}
	
	class displayWrapper {
		public Client_Project__c project {get;set;}
		public string        sponsorname {get;set;}
		public list<Study__c> studieList {get;set;}
	}

}