/**
 * @description	The Service Layer for the Acknowledgement Pending
 * @author		Kalaiyarasan Karunanithi, Dimitrios Sgourdos
 * @date		Created: 10-Sep-2015, Edited: 09-Oct-2015
 */
public with sharing class NCM_SrvLayer_Notification {
	
// *********************************************************************************************************************
//				DataTypes with objects transformations
// *********************************************************************************************************************
	
	/**
	 * @description	Retrieve a notification record with attributes filled from the corresponding wrapper data type.
	 *				The record that will be refreshed with new values is passed as parameter in order to capture
	 *				situations that we want to refresh an existing (already stored in the DB) record.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015
	 * @param		source    The wrapper that holds the values for the attributes of the Notification Record
	 * @param		result    The Notification record to be updated with the attributes from the wrapper item
	 * @return		The updated Notification record.
	*/
	public static Notification__c getNotificationFromWrapperData(	NCM_API_DataTypes.NotificationWrapper source,
																	Notification__c result) {
		// Check for invalid parameters
		if(source == NULL) {
			return result;
		}
		
		if(result == NULL) {
			result = new Notification__c();
		}
		
		// Update the notification record
		result.NotificationEvent__c			= source.notificationEvent;
		result.NotificationRegistration__c	= source.notificationRegistration;
		result.Reminder__c					= (source.reminder == NULL)?
																	DateTime.now() :
																	source.reminder;
		result.Status__c					= (source.status ==NULL)?
																	NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT :
																	source.status;
		
		return result;
	}
	
	
// *********************************************************************************************************************
//				Retrieve and acknowledge notifications
// *********************************************************************************************************************
	
	/**
	 * @description	Build notifications in the 'NotificationWrapper' format, to let the given user acknowledge pending
	 				notifications The data includes all the 'Pending Acknowledgment' notifications for the given user.
	 *				The data are ordered by the notification Reminder.
	 * @author		Kalaiyarasan Karunanithi, Dimitrios Sgourdos
	 * @date		Created: 10-Sep-2015, Edited: 09-Oct-2015
	 * @param		userId    The id of the user that is assigned with the 'Pending Acknowledgement' notifications
	 * @return		The 'Pending Acknowledgement' notifications of the given user
	*/
	public static List<NCM_API_DataTypes.NotificationWrapper> getPendingNotificationsByUser(ID userId) {
		// Initialize data
		List<NCM_API_DataTypes.NotificationWrapper> results = new List<NCM_API_DataTypes.NotificationWrapper>();
		
		// Find the Notification and build the notification Pending Acknowledgment results
		List<Notification__c> notificationList = NCM_DataAccessor.getPendingNotificationsByUser(userId);
		
		for(Notification__c notification : notificationList) {
			NCM_API_DataTypes.NotificationWrapper notificationWrapperItem
															= new NCM_API_DataTypes.NotificationWrapper(notification,
																										true,
																										true);
			results.add(notificationWrapperItem);
		}
		
		return results;
	}
	
	
	/**
	 * @description	Build notifications in the 'NotificationWrapper' format, to let the given user acknowledge pending
	 *				notifications under a specific related to object. The data are ordered by the notification Reminder.
	 * @author		Kalaiyarasan Karunanithi, Dimitrios Sgourdos
	 * @date		Created: 10-Sep-2015, Edited: 09-Oct-2015
	 * @param		userId         The id of the user that is assigned with the 'Pending Acknowledgement' notifications
	 * @param		relatedToId    The related Id that corresponds to the the notifications
	 * @return		The 'Pending Acknowledgement' notifications of the given user and related to id
	*/
	public static List<NCM_API_DataTypes.NotificationWrapper> getPendingNotificationsByUserAndRelatedObject(
																								Id userId,
																								String relatedToId) {

		List<NCM_API_DataTypes.NotificationWrapper> results = new List<NCM_API_DataTypes.NotificationWrapper>();
		
		// Find the Notification and build the notification Pending Acknowledgment results
		List<Notification__c> notificationList = NCM_DataAccessor.getPendingNotificationsByUserAndRelatedObject(
																										userId,
																										relatedToId);
		
		for(Notification__c notification : notificationList) {
			NCM_API_DataTypes.NotificationWrapper notificationWrapperItem
															= new NCM_API_DataTypes.NotificationWrapper(notification,
																										true,
																										true);
			results.add(notificationWrapperItem);
		}
		
		return results;
	}
	
	
	/**
	 * @description	Get the number Of Pending Notifications per Related Object for the given user
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 11-Sep-2015
	 * @param		userId    The id of the user to find the number of pending notification per related object
	 * @return		The Map with key the related object id and value the Number of pending acknowledgement notifications
	*/
	public static Map<String, Integer> getNumberOfPendingNotificationsPerRelatedObjectByUser(Id userId) {
		Map<String, Integer> resultMap = new Map<String, Integer>();
		
		// Find the Notification and build the notification Pending Acknowledgment results
		List<Notification__c> notificationList = NCM_DataAccessor.getPendingNotificationsByUser(userId);
		
		Integer numberOfPending;
		String relatedId = '';
		
		for( Notification__c notification : notificationList ) {
			numberOfPending = 1;
			relatedId = notification.NotificationRegistration__r.RelatedToId__c;
			
			// check the map already contains relatedObjectId
			if(resultMap.containsKey( relatedId )) {
				numberOfPending += resultMap.remove( relatedId );
			}
			
			resultMap.put( relatedId, numberOfPending );
		}
		
		return resultMap;
	}
	
	
	/**
	 * @description	Acknowledge the given notifications
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 10-Sep-2015
	 * @param		source    The wrapper data that holds the pending acknowledgement notifications
	*/
	public static void acknowledgePendingNotifications(List<NCM_API_DataTypes.NotificationWrapper> source) {
		// Check for invalid parameters
		if(source == NULL || source.isEmpty() ) {
			return;
		}
		
		// Build the records that will be updated
		List<Notification__c> results = new List<Notification__c>();
		
		for(NCM_API_DataTypes.NotificationWrapper tmpWrapperItem : source) {
			if( tmpWrapperItem.recordId == NULL ) {
				continue;
			}
			Notification__c notification = new Notification__c();
			notification.Id 		= tmpWrapperItem.recordId;
			notification.Status__c  = NCM_API_DataTypes.ACKNOWLEDGED;
			results.add( notification );
		}
		
		update results;
	}
	
	
	
// *********************************************************************************************************************
//				Helping functions for activate events
// *********************************************************************************************************************	
	
	/**
	 * @description	Create an instance from the given registration record and the given event id
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 28-Sep-2015
	 * @param		registration    The registration with which the notification will be assigned
	 * @param		eventId         The id of the even with which the notification will be assigned
	 * @return		The created notification instance
	*/
	public static Notification__c createNotificationInstanceFromRegistrationAndEvent(
																			NotificationRegistration__c registration,
																			Id eventId) {
		// Find reminder: Reminder is now + reminder period in the registration
		Decimal tmpReminderPeriodDec = (registration.ReminderPeriod__c == NULL)? 24 : registration.ReminderPeriod__c;
		Integer reminderPeriodInt = Integer.valueOf(tmpReminderPeriodDec);
		DateTime recordReminder = DateTime.now().addHours( reminderPeriodInt );
		
		// Find status
		String recordStatus = (registration.NotificationCatalog__r.AcknowledgementRequired__c) ?
																	NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT :
																	NCM_API_DataTypes.ACKNOWLEDGED;
		
		// Create instance
		Notification__c result = new Notification__c(	NotificationEvent__c = eventId,
														NotificationRegistration__c = registration.Id,
														Status__c = recordStatus,
														Reminder__c = recordReminder );
		return result;
	}
}