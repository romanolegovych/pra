/** 
@Author Bhargav Devaram
@Date 2015
@Description this class is insert the records of service task and service impact questions in Service_Task_To_Service_Impact__c  object.
*/
public class  PBB_ServiceTasktoQuestionsCntrl {

    public Service_Task_To_Service_Impact__c STtoSI{get;set;}    //to get the service task and service impact Sobject
    public String keyPrefix {get;set;}                           //to get the SObject 3 digit code or id
    public String STtoSIid{get;set;}                             //to get the service task and service impact ID 
    public Service_Task__c ST{get;set;}                          //to get the service task  Sobject
    public Boolean isGlobal{get;set;}                          //to get the service task  Sobject
    public Boolean modelStatus{get;set;}
    
    //Standard Constructor
    public PBB_ServiceTasktoQuestionsCntrl(ApexPages.StandardController controller) {
        
        STtoSI = new Service_Task_To_Service_Impact__c ();
        ST     = new Service_Task__c ();
        
        //check if the STtoSI is for edit
        STtoSI = (Service_Task_To_Service_Impact__c)controller.getRecord();
        STtoSIid = ApexPages.currentPage().getParameters().get('id');
        if(STtoSIid !=null){
            Set<String> STtoSIids = new Set<String>();
            STtoSIids.add(STtoSIid);
            List<Service_Task_To_Service_Impact__c>  STtoSIList = PBB_DataAccessor.getServiceTaskToServiceImpact(STtoSIids);
            if(STtoSIList.size()>0)
                STtoSI = STtoSIList[0];          
            isGlobal =  STtoSI.Service_Task__r.global__c;
            if( STtoSI.Service_Mod_Status__c == 'Retired' || STtoSI.Service_Mod_Status__c =='Approved'){
                modelStatus = True;           
            }
            else{
                modelStatus = False;
            }
        }
         if(STtoSI !=null){
            Set<String> STids = new Set<String>();
            STids.add(STtoSI.Service_Task__c);
            List<Service_Task__c>  STList   = PBB_DataAccessor.getServiceTaskList(STids);
            if(STList.size()>0)
                ST = STList[0];
            isGlobal =  ST.global__c;
            if( ST.Service_Function__r.Service_Area__r.Service_Model__r.Status__c  == 'Retired' || ST.Service_Function__r.Service_Area__r.Service_Model__r.Status__c  =='Approved'){
                modelStatus = True;           
            }
            else{
                modelStatus = False;
            }           
        }
        System.debug('ST--'+ST.global__c );
        //check if the STtoSI is for Clone
        String clone =ApexPages.currentPage().getParameters().get('clone');
        if(clone !=null)
            STtoSI =STtoSI.clone(false, true); 
        
        //get the Sobject 3 digit code for  Service_Impact_Question_Name__c as we need to pass it for lookup page   
        Schema.DescribeSObjectResult r = Service_Impact_Question_Name__c.sObjectType.getDescribe();
        keyPrefix = r.getKeyPrefix();
        System.debug('Printing --'+keyPrefix );
    }
    
    /** 
    @Author Bhargav Devaram
    @Date 2015
    @Description save the service Tasks with Service impact questions 
    */
    public PageReference saveSTtoSIQ() {
        System.debug('STtoSI--'+STtoSI);
        if( STtoSI.Service_Mod_Status__c == 'Retired' || STtoSI.Service_Mod_Status__c =='Approved')
            Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''+'Service Impact Questions cannot be created or edited once service model is approved or retired.')); 
        PageReference pageRef =null;   
        try{   
            upsert STtoSI;
            pageRef=new PageReference('/'+STtoSI.id);
            pageRef.setRedirect(true); 
        }catch (Exception exc) {
            if(exc.getMessage().contains('DUPLICATE_VALUE, duplicate value found:'))
            {
                Apexpages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,''+'A record already exists for this Service Task and Service Impact Question combination. Please modify this record before saving so it is not an exact duplicate.')); 
            }       
        }         
        System.debug('STtoSI--'+STtoSI);
        return pageRef;  
    }    
}