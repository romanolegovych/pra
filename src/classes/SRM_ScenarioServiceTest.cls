/**
* @author Ramya Shree Edara
* @date 21-Oct-2014
* @description this is test class for scenario related classes
*/
@isTest
public with sharing class SRM_ScenarioServiceTest{
    
    public static testMethod void testGetSRMScenarios(){
        //Create Account
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Status__c = 'Active';
        acc.Rating = 'A';
        acc.Type = 'Competitor';
        acc.Industry = 'Small/Speciality Pharma';
        acc.Therapeutic_Areas__c = 'Anesthesiology';
        insert acc;
        
        Test.startTest();
        
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Oppty';
        opp.AccountId = acc.Id;
        opp.StageName = 'Opportunity Identified';
        opp.Type = 'New Business';
        opp.Business_Unit__c = 'PR-Safety & Risk Management';
        opp.PRA_Project_ID__c='44441230-333123';
        opp.Phase__c = 'Phase I';
        opp.Therapeutic_Area__c = 'Cardio-Metabolic Diseases';
        opp.Indication_Group__c = 'Arterial disease';
        opp.Study_Start_Date__c = Date.Today().addDays(1);
        opp.Study_End_Date__c = Date.Today().addDays(185);
        opp.RFP_1st_Patient_In_Date__c = Date.Today().addDays(14);
        opp.RFP_Last_Patient_In_Date__c = Date.Today().addDays(180);
        opp.Expected_Patients__c = 5000;
        opp.Expected_Sites__c = 500;
        opp.NBA_Date__c = Date.Today().addDays(1);
        opp.Probability = 10;
        opp.ForecastCategoryName = 'Pipeline';
        opp.CloseDate = Date.Today().addDays(190);
        insert opp;
        
        Test.stopTest();
        System.debug('>>> Oppty Created >>> '+opp.Name);
        
        list<WFM_Project__c> projList = RM_ProjectsDataAccessor.getProjectByProjectName(opp.PRA_Project_ID__c);
        System.debug('>>> WFM Project Created >>> '+projList[0].Name);
        system.assertEquals(projList[0].Name, opp.PRA_Project_ID__c);
        
        WFM_Project__c prj = projList[0];
        prj.Project_Start_Date__c = Date.Today().addDays(1);
        prj.Project_End_Date__c = Date.Today().addDays(450);
        update prj;
        
         Bid_Project__c bs = new Bid_Project__c(Name='BS1',PRA_Project_ID__c=prj.Id);        
        insert bs;
        List<Bid_Project__c> srmProjList = SRM_ScenarioDataAccessor.getSRMProjectByWFMProjectName(prj.Name);
        
        //Create SRM Model
        SRM_Model__c model = new SRM_MOdel__c();
        model.Status__c = 'In Progress';
        model.Comments__c = 'Test Comments';
        insert model;
        
        //Create Country
        Country__c country = new Country__c();
        country.Name = 'Test India';
        country.Country_Code__c = 'TI';
        country.Daily_Business_Hrs__c = 8;
        country.Description__c = 'Test India';
        country.PRA_Country_Id__c = 'TI1';
        insert country;
        
        //Create SRM Site Activation
        SRM_Model_Site_Activation__c site = new SRM_Model_Site_Activation__c();
        site.SRM_Model__c = model.Id;
        site.Country__c = country.Id;
        site.Number_of_Weeks__c = 10;
        site.Gaussian_Constant__c = 10;
        site.Amplitude__c = 100;
        site.Shift_Center_By__c = 2;
        site.Standard_Deviation__c = 4;
        site.Start_Week__c = 2;
        site.Site_Activation_Formula__c = 'Gaussian';
        insert site;
        
        //Create Subject Enrollment
        SRM_Model_Subject_Enrollment__c subEnroll = new SRM_Model_Subject_Enrollment__c();
        subEnroll.SRM_Model__c = model.Id;
        subEnroll.Country__c = country.Id;
        subEnroll.Subject_Enrollment_Rate__c = 3;
        subEnroll.Standard_Deviation__c = 3;
        subEnroll.Shift_Center_By__c = -7;
        insert subEnroll;
        
        //Create Calendar Adjustment records
        SRM_Calender_Adjustments__c cal = new SRM_Calender_Adjustments__c();
        cal.Country__c = country.Id;
        cal.SRM_Model__c = model.Id;
        cal.From_Date__c = Date.newInstance(2015,1,21);
        cal.To_Date__c = Date.newInstance(2015, 1, 22);
        System.debug('>>> sdtd >>> '+cal.From_Date__c  + ' >>> todt >>> '+cal.To_Date__c);
        cal.Site_Act_Adjustment__c = 100;
        cal.Subject_Enroll_Adjustment__c = -100;
        insert cal;
        
        //Create Week Adjustment
        SRM_Week_Adjustments__c week = new SRM_Week_Adjustments__c();
        week.Country__c = country.Id;
        week.SRM_Model__c = model.Id;
        week.Week_Of_Project__c = 15;
        week.Site_Act_Adjustment__c = 100;
        week.Subject_Enroll_Adjustment__c = 100;
        insert week;
        
        //Create question
        SRM_Model_Questions__c que = new SRM_Model_Questions__c();
        que.SRM_Model__c = model.Id;
        que.Question__c = 'This is test question';
        que.Primary_Role__c = 'Proposal Director';
        que.Group__c = 'Project';
        que.Sub_Group__c = 'Site Activation';
        insert que;
        
        SRM_Model_Response__c res = new SRM_Model_Response__c();
        res.SRM_Question_ID__c = que.Id;
        res.Response__c = 'Yes';
        res.Adjust_LPI__c = 5;
        insert res;
        
        model.Status__c = 'Approved';
        update model;
        
        String activeModel = SRM_ScenarioService.getRecentActiveModel();
        System.assertNotEquals(null, activeModel);
        Map<String, List<SRM_Model_Site_Activation__c>> countryMap = SRM_ScenarioService.getAllCountries('SRM_Model_Site_Activation__c', model.Id);
        Map<String, List<SRM_Model_Subject_Enrollment__c>> countrySubMap = SRM_ScenarioService.getAllCountries('SRM_Model_Subject_Enrollment__c', model.Id);
        
        System.assertNotEquals(null, countryMap);
        System.assertNotEquals(null, countrySubMap);
        
        SRM_Scenario__c scen = new SRM_Scenario__c();
        scen.Project_Start_Date__c = Date.Today().addDays(1);
        scen.Final_Protocol_Date__c = Date.Today().addDays(3);
        scen.First_Patient_Randomized__c = Date.Today().addDays(10);
        scen.Last_Patient_Randomized__c = Date.Today().addDays(365);
        scen.Expected_Patients__c = 5000;
        scen.Expected_Sites__c = 500;
        scen.SRM_Model__c = model.Id;
        scen.Bid_Project__c = bs.Id;
        scen.Status__c = 'In Progress';
        insert scen;
        
        SRM_Scenario_Country__c scencont = new SRM_Scenario_Country__c();
        scencont.Country__c = country.Id;
        scencont.Expected_Sub_Enrollment_Rate__c = 1;
        scencont.SRM_Model_Site_Activation__c = site.Id;
        scencont.SRM_Scenario__c = scen.Id;
        scencont.Subject_Enrollment_Rate__c = 2;
        scencont.Target_Number_Of_Sites__c = 300;
        scencont.Target_Number_Subjects__c = 2000;
        insert scencont;
        
        SRM_Questions_Response__c scenQue = new SRM_Questions_Response__c();
        scenQue.Country__c = country.Id;
        scenQue.ScenarioId__c = scen.Id;
        scenQue.SRM_Question_ID__c = que.Id;
        scenQue.QuestionResponseUnique__c = scen.Id+':'+que.Id;
        scenQue.SRM_Response_Id__c = res.Id;
        insert scenQue;
        
        SRM_ScenarioService service = new SRM_ScenarioService();
        
        Map<String, SRM_Scenario_country__c> scenContUpdateMap = SRM_ScenarioService.getScenarioCountriesToUpdate(scen.Id);
        List<SRM_Scenario_Country__c> scenContList = SRM_ScenarioService.getScenarioCountries(scen.Id);
        
        System.assertEquals(1, scenContUpdateMap.size());
        System.assertEquals(1, scenContList.size());
        List<SRM_ScenarioController.InnerClass> inrList = SRM_ScenarioService.getScenarioSelectedCountries(scen.Id, countryMap, false, scen.SRM_Model__c);
        System.assertEquals(1, inrList.size());
        
        List<SRM_ScenarioController.RelateSponsorAndTherArea> dispSAList = service.findSponsorAndTherCountryList('SRM_Model_Site_Activation__c', inrList[0].countryName, scen, countryMap);
        inrList[0].siteActSelectOptionList = SRM_ScenarioService.getSelectOptionListForCountry(dispSAList);
        
        List<SRM_ScenarioController.InnerClass> inrListClone = SRM_ScenarioService.getScenarioSelectedCountries(scen.Id, countryMap, true, scen.SRM_Model__c);
        System.assertEquals(1, inrListClone.size());
        
        List<SelectOption> selOptionList = SRM_ScenarioService.convertToSelectOption(countryMap);
        System.assertNotEquals(null, selOptionList);
        
        String isSame = SRM_ScenarioService.CheckForSameSiteActivationRecord(scenCont, new List<SRM_Model_Site_Activation__c>{site});
        
        List<SRM_Calender_Adjustments__c> calAdjLIst = new List<SRM_Calender_Adjustments__c>();
        calAdjList.add(cal);
        
        List<SRM_Week_Adjustments__c> weekList = new List<SRM_Week_Adjustments__c>();
        weekList.add(week);
        
        Map<String, List<SRM_Calender_Adjustments__c>> calMap = SRM_ScenarioService.getAllCountries('SRM_Calender_Adjustments__c', model.Id);
        Map<String, List<SRM_Week_Adjustments__c>> weekMap = SRM_ScenarioService.getAllCountries('SRM_Week_Adjustments__c', model.Id);
        
        List<Decimal> siteActValues = service.getSiteActivationData(site);
        List<Decimal> calAdjustedValues = service.getAdjustedValuesPerDate(scen, Integer.valueOf(site.Start_Week__c), siteActValues, calMap.get(inrList[0].countryName), 'Site');
        List<Decimal> weekAdjustedValues = service.getAdjustedValuesPerWeek(scen, Integer.valueOf(site.Start_Week__c), calAdjustedValues, weekMap.get(inrList[0].countryName), 'Site');
        
        List<Decimal> percentageValues = service.convertToPercentage(weekAdjustedValues);
        
        List<SRM_Model_Site_Activation__c> modelCountrySiteList = (List<SRM_Model_Site_Activation__c>)service.findCountryList('SRM_Model_Site_Activation__c', inrList[0].countryName, scen, countryMap);
        List<SRM_Model_Subject_Enrollment__c> modelCountrySubEnrollList = (List<SRM_Model_Subject_Enrollment__c>)service.findCountryList('SRM_Model_Subject_Enrollment__c', inrList[0].countryName, scen, countrySubMap);
        
        Map<String, SRM_Model_Subject_Enrollment__c> subEnrollMap = SRM_ScenarioService.getAllSubEnrollments(activeModel);
        system.debug('>> SubEnrollMap <<<'+subEnrollMap.get(subEnroll.Id)+'---'+subEnrollMap);
        system.debug('inr List'+inrList[0]);
        Map<Integer, Decimal> subjectEnrollRateMap = service.getSubEnrollValues(subEnrollMap.get(subEnroll.Id), scen, 3);
        
        
        String chartDataSiteC = service.generateStringChartData(scen, country.Name, 100, 'cumulative', 'Site', countryMap.get(country.Name), subjectEnrollRateMap, percentageValues, calMap.get(country.Name), true, weekMap.get(country.Name), true, '100'); 
        List<String> chartDataSiteCGrid = service.generateGridData(scen, country.Name, 100, 'cumulative', 'Site', countryMap.get(country.Name), subjectEnrollRateMap, percentageValues, calMap.get(country.Name), true, weekMap.get(country.Name), true, '100');
        
        String chartDataSiteNC = service.generateStringChartData(scen, country.Name, 100, 'noncumulative', 'Site', countryMap.get(country.Name), subjectEnrollRateMap, percentageValues, calMap.get(country.Name), true, weekMap.get(country.Name), true, '100');
        List<String> chartDataSiteNCGrid = service.generateGridData(scen, country.Name, 100, 'noncumulative', 'Site', countryMap.get(country.Name), subjectEnrollRateMap, percentageValues, calMap.get(country.Name), true, weekMap.get(country.Name), true, '100');
        
        String chartDataSubC = service.generateStringChartData(scen, country.Name, 100, 'cumulative', 'Subject', countryMap.get(country.Name), subjectEnrollRateMap, percentageValues, calMap.get(country.Name), true, weekMap.get(country.Name), true, '100');
        List<String> chartDataSubCGrid = service.generateGridData(scen, country.Name, 100, 'cumulative', 'Subject', countryMap.get(country.Name), subjectEnrollRateMap, percentageValues, calMap.get(country.Name), true, weekMap.get(country.Name), true, '100');
        
        String chartDataSubNC = service.generateStringChartData(scen, country.Name, 100, 'noncumulative', 'Subject', countryMap.get(country.Name), subjectEnrollRateMap, percentageValues, calMap.get(country.Name), true, weekMap.get(country.Name), true, '100');
        List<String> chartDataSubNCGrid = service.generateGridData(scen, country.Name, 100, 'noncumulative', 'Subject', countryMap.get(country.Name), subjectEnrollRateMap, percentageValues, calMap.get(country.Name), true, weekMap.get(country.Name), true, '100');
        
        Map<String, List<String>> weekAndDateLabel = service.getWeeksAndDateLabels(scen);
        Map<String, SRM_Model_Response__c> queResMap = SRM_ScenarioService.getQuestionsResponses(model.Id);
        
        SRM_Model_Site_Activation__c siteAct = SRM_ScenarioService.getObjValues(site.Id);
        Map<String,String> prefixList = service.PrefixList();
        String role = SRM_ScenarioService.getrolenamebyproject(scen.Id);
        
        String roleBySponsor = SRM_ScenarioService.getrolenamebysponser(scen.Id);
        String roleByCountry = SRM_ScenarioService.getrolenamebycountry(scen.Id);
        String roleByContract = SRM_ScenarioService.getrolenamebycontract(scen.Id);
        
        Map<String, SRM_Model_Site_Activation__c>  siteActMap = SRM_ScenarioService.getAllSiteActivations(model.Id);
        Map<String, SRM_Model_Questions__c> modelQues = SRM_ScenarioService.getAllModelQuestions(model.Id);
        SRM_ScenarioService.getScenarioCountriesToUpdate(scen.id);
     
    }   
}