global class LMS_EmployeeMappingBatch extends PRA_BatchaQueueable {

	String query;
    
    global LMS_EmployeeMappingBatch() {
        
    }

    //Set the parameters over the variables
    global override void  setBatchParameters(String parametersJSON){
        List<String> params = (List<String>) Json.deserialize(parametersJSON, List<String>.class);
        query = params.get(0);    
     }
    
    global override Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    global override void execute(Database.BatchableContext BC, List<sObject> scope) {
		list<LMS_Role__c> rolesToProcess = new list<LMS_Role__c>();
		for(Sobject s : scope) {
			if(s instanceof LMS_Role__c) {
				LMS_Role__c role = (LMS_Role__c)s;
				rolesToProcess.add(role);
			}
		}
		if(rolesToProcess != null && rolesToProcess.size() > 0) {
			LMS_MappingUtil.processRoles(rolesToProcess);
		}
    }
    
    global override void finish(Database.BatchableContext BC) {
		
    }
}