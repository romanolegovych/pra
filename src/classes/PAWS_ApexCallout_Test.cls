public with sharing class PAWS_ApexCallout_Test implements STSWR1.WorkflowCallApexActionInterface
{
	public class LimitsException extends Exception {}
	
	public void run(SObject context, STSWR1__Flow_Instance_Cursor__c cursor, String operationType, String parameter)
	{
		STSWR1__Stub__c stub = (STSWR1__Stub__c) context;
		
		Http h = new Http();
		
		HttpRequest req = new HttpRequest();
		req.setTimeout(59000);
		req.setEndpoint('https://www.google.com/');
		req.setMethod('get');
		
		HttpResponse res = h.send(req);
		
		stub.STSWR1__Area__c = 'HTTP Status Code - ' + res.getStatusCode() + '. HTTP Status - ' + res.getStatus();
		stub.STSWR1__Long__c = res.getBody();
		update stub;
		
		/*if (parameter == '1')
		{
			//stub.STSWR1__String__c = '1st action is complete';
			stub.STSWR1__String__c = 'DMLs = ' + String.valueOf(Limits.getDMLStatements());
			update stub;
			
			//throw new PAWS_API.PAWS_APIException('oops');
		}
		else
		{
			//stub.STSWR1__String__c = 'DMLs = ' + String.valueOf(Limits.getDMLStatements());
			//update stub;
			//if (Limits.getDMLStatements() >= 0) throw new LimitsException('Not enough DMLs to execute this class.');
			
			Http h = new Http();
			
			HttpRequest req = new HttpRequest();
			req.setTimeout(59000);
			req.setEndpoint('https://www.google.com/');
			req.setMethod('get');
			
			HttpResponse res = h.send(req);
			
			stub.STSWR1__Area__c = 'HTTP Status Code - ' + res.getStatusCode() + '. HTTP Status - ' + res.getStatus();
			stub.STSWR1__Long__c = res.getBody();
			update stub;
		}*/
	}
}