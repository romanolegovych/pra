public with sharing class BDT_NewEditStudySubcontractorActivity {
	public String ProjectId {get;set;}
	public List<SelectOption> subcontractorOptionList{get;set;}
	public String SubcontractorId{get;set;}
	public List<SubcontractorActivity__c> ActivitiesPerSCSelected{get;set;}
	public List<ProjectSubcontractorActivity__c> SelectedActivities{get;set;}
	public List<ActivityWrapper> ActivitySelector{get;set;}
	
	List<ProjectSubcontractorActivity__c> PSCAtoDelete; 
	List<ProjectSubcontractorActivity__c> PSCAtoSave;
	
	public BDT_NewEditStudySubcontractorActivity(){
		ProjectId = BDT_Utils.getPreference('SelectedProject');
		buildSubcontractorList();
	}
	
	public class ActivityWrapper{
		public SubcontractorActivity__c Activity{get;set;}
		public Boolean isSelected				{get;set;}
		public String projectId					{get;set;}
	
	
		public ActivityWrapper( SubcontractorActivity__c pActivity, boolean pIsSelected, string pProjectId){
			Activity = pActivity;
			isSelected = pIsSelected;
			projectId = pProjectId;
		}
	}

	public void buildSubcontractorList() {
		subcontractorOptionList = new List<SelectOption>();
		subcontractorOptionList.add(new SelectOption('1','Please select a Subcontractor'));
		for(Subcontractor__c sc: [Select id, Name from Subcontractor__c where BDTDeleted__c=false]){
			subcontractorOptionList.add(new SelectOption(sc.id,sc.Name));
		}
	}
		
	public void buildActivitySelector(){
		
		ActivitiesPerSCSelected = [select name, id 
									from SubcontractorActivity__c 
									where Subcontractor__c = :SubcontractorId
									and BDTDeleted__c = false];
		try{
			SelectedActivities = [Select p.SubcontractorActivity__r.Name, p.SubcontractorActivity__r.Id, p.SubcontractorActivity__c, p.Name, p.Id, p.ClientProject__c 
								  From ProjectSubcontractorActivity__c p
									where ClientProject__c = :ProjectId];
		}
		catch(QueryException e){
			SelectedActivities = new List<ProjectSubcontractorActivity__c>();
		}
									
		ActivitySelector = new List<ActivityWrapper>();
		for(SubcontractorActivity__c act: ActivitiesPerSCSelected ){
			boolean isSelected = false;
			//check if the Sc. Activity already has been selected for this project
			for(ProjectSubcontractorActivity__c psa: SelectedActivities){
				if(psa.ClientProject__c == ProjectId && psa.SubcontractorActivity__c == act.Id ){
					isSelected = true;
				}
			}
			
			ActivitySelector.add(new ActivityWrapper( act, isSelected,   ProjectId ));
		}
		
	}

	public PageReference save(){
		buildSubcontractorAssignments();
		
		PageReference characteristicsPage = New PageReference(system.page.BDT_Characteristics.getUrl() );
		
		if(!PSCAtoSave.isEmpty() ){
			insert PSCAtoSave;
		}
			
		if(!PSCAtoDelete.isEmpty() ){
			//first delete all study subcontractor Activity assignments which are associated with these ProjectSubcontractorActivities
			List<SubcontractorActivityStudyAssignment__c> scsalist = [select id, name, ProjectSubcontractorActivity__c, ProjectSubcontractorActivity__r.ClientProject__c 
																	  from SubcontractorActivityStudyAssignment__c 
																	  where  ProjectSubcontractorActivity__r.ClientProject__c = :ProjectId];
			list<SubcontractorActivityStudyAssignment__c> sasaToDelete = new list<SubcontractorActivityStudyAssignment__c>();
			
			for(SubcontractorActivityStudyAssignment__c sasa: scsalist){
				for(ProjectSubcontractorActivity__c psca: PSCAtoDelete ){
					if(sasa.ProjectSubcontractorActivity__c == psca.Id){
						sasaToDelete.add(sasa);
					}
				}
			}
			if(!sasaToDelete.isEmpty()){
				delete sasaToDelete;
			}
			
			delete PSCAtoDelete;
				
		}
				
		return  characteristicsPage; 
		
	}
		public PageReference cancel(){
		PageReference characteristicsPage = New PageReference(system.page.BDT_Characteristics.getUrl() );
		
		return  characteristicsPage; 
		
	}
	
	public void buildSubcontractorAssignments(){
		PSCAtoSave = new List<ProjectSubcontractorActivity__c>();
		PSCAtoDelete = new List<ProjectSubcontractorActivity__c>();
		
		for(ActivityWrapper aw: ActivitySelector){
			Boolean exists = false;
			if(aw.isSelected){
				for(ProjectSubcontractorActivity__c psca: SelectedActivities){
					if(psca.ClientProject__c == ProjectId && psca.SubcontractorActivity__c == aw.Activity.Id){
						exists=true;
					}
				}
				if(!exists){
					PSCAtoSave.add(new ProjectSubcontractorActivity__c(ClientProject__c=ProjectID, SubcontractorActivity__c = aw.Activity.Id ));
				}
			}
			else{
				for(ProjectSubcontractorActivity__c psca: SelectedActivities){
					if(psca.ClientProject__c == ProjectID && psca.SubcontractorActivity__c == aw.Activity.Id){
						PSCAtoDelete.add( psca );
					}
				}	
			}
		}
	}
}