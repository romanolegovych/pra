/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class RM_SaveSearchControllerUTest {
    static string jsonData;
    static string rId;
    static string sId;
    static string subFolder;
    static string folderId;
    static string folderName;
    static string childId;
    static string newParentId;
    static list<Root_Folder__c> rootList;
    static list<Sub_Folder__c> userFolder;
    static RM_SaveSearchController  ss;
    static Root_Folder__c root1;
    static Root_Folder__c root2;
    static Sub_Folder__c userSub1;
    static Sub_Folder__c userSub2;
    static Saved_Search__c file4;
    
    static void init(){
        //create and query fake roots//
        root1 = new Root_Folder__c(Name = 'root1');
        root2 = new Root_Folder__c(Name = 'root2', Custom__c = true);
        Root_Folder__c root3 = new Root_Folder__c(Name = 'root3');
            insert root1; insert root2; insert root3;
        list<Root_Folder__c> rootIds = [select Name, Id from Root_Folder__c];
        string id1 = string.valueof(rootIds[0].get('Id'));
        string id2 = string.valueof(rootIds[1].get('Id'));
        string id3 = string.valueof(rootIds[2].get('Id'));
       
        
        //create and query fake subs//
        Sub_Folder__c sub1 = new Sub_Folder__c(Name = 'sub1', Root__c = id1, Custom__c = false, User__c = UserInfo.getUserID(), Sub_Folder_Unique_Key__c='sub1'+id1 + UserInfo.getUserID());
        Sub_Folder__c sub2 = new Sub_Folder__c(Name = 'sub2', Root__c = id1, Custom__c = false, User__c = UserInfo.getUserID(), Sub_Folder_Unique_Key__c='sub2'+id1 + UserInfo.getUserID());
        Sub_Folder__c sub3 = new Sub_Folder__c(Name = 'sub3', Root__c = id1, Custom__c = false, User__c = UserInfo.getUserID(), Sub_Folder_Unique_Key__c='sub3'+id1 + UserInfo.getUserID());
            insert sub1; insert sub2; insert sub3;
        list<Sub_Folder__c> subList = [select Id from Sub_Folder__c where Custom__c = false];
        string sId1 = string.valueof(subList[0].get('Id'));
        string sId2 = string.valueof(subList[1].get('Id'));
        string sId3 = string.valueof(subList[2].get('Id'));
        
        userSub1 = new Sub_Folder__c(Name = 'userSub1', Root__c = id1, User__c = UserInfo.getUserID(), Custom__c = true, Sub_Folder_Unique_Key__c='userSub1'+id1 + UserInfo.getUserID());
        userSub2 = new Sub_Folder__c(Name = 'userSub2', Root__c = id1, User__c = UserInfo.getUserID(), Custom__c = true, Sub_Folder_Unique_Key__c='userSub2'+id1 + UserInfo.getUserID());
        Sub_Folder__c userSub3 = new Sub_Folder__c(Name = 'userSub3', Root__c = id1, User__c = UserInfo.getUserID(), Custom__c = true, Sub_Folder_Unique_Key__c='userSub3'+id1 + UserInfo.getUserID());
            insert userSub1; insert userSub2; insert userSub3;
            
        Sub_Folder__c userSub4 = new Sub_Folder__c(Name = 'userSub4', Root__c = id2, User__c = UserInfo.getUserID(), Custom__c = true, Sub_Folder_Unique_Key__c='userSub4'+id2 + UserInfo.getUserID());
        Sub_Folder__c userSub5 = new Sub_Folder__c(Name = 'userSub5', Root__c = id2, User__c = UserInfo.getUserID(), Custom__c = true, Sub_Folder_Unique_Key__c='userSub5'+id2 + UserInfo.getUserID());
        Sub_Folder__c userSub6 = new Sub_Folder__c(Name = 'userSub6', Root__c = id2, User__c = UserInfo.getUserID(), Custom__c = true, Sub_Folder_Unique_Key__c='userSub6'+id2 + UserInfo.getUserID());
            insert userSub4; insert userSub5; insert userSub6;
        
        Sub_Folder__c userSub7 = new Sub_Folder__c(Name = 'userSub7', Root__c = id3, User__c = UserInfo.getUserID(), Custom__c = true, Sub_Folder_Unique_Key__c='userSub7'+id3 + UserInfo.getUserID());
        Sub_Folder__c userSub8 = new Sub_Folder__c(Name = 'userSub8', Root__c = id3, User__c = UserInfo.getUserID(), Custom__c = true, Sub_Folder_Unique_Key__c='userSub8'+id3 + UserInfo.getUserID());
        Sub_Folder__c userSub9 = new Sub_Folder__c(Name = 'userSub9', Root__c = id3, User__c = UserInfo.getUserID(), Custom__c = true, Sub_Folder_Unique_Key__c='userSub9'+id3 + UserInfo.getUserID());
            insert userSub7; insert userSub8; insert userSub9;
        
        
        
        file4 = new Saved_Search__c(Name = 'file4', Sub__c = userSub1.Id, User__c = UserInfo.getUserID(), Saved_Search_Unique_Key__c='file4'+userSub1.Id);
        Saved_Search__c file5 = new Saved_Search__c(Name = 'file5', Sub__c = sId1, User__c = UserInfo.getUserID(), Saved_Search_Unique_Key__c='file5'+sId1);
        Saved_Search__c file6 = new Saved_Search__c(Name = 'file6', Sub__c = sId1, User__c = UserInfo.getUserID(), Saved_Search_Unique_Key__c='file6'+sId1);
        Saved_Search__c file7 = new Saved_Search__c(Name = 'file7', Sub__c = userSub1.id, User__c = UserInfo.getUserID(), Saved_Search_Unique_Key__c='file7'+userSub1.id);
            insert file4; insert file5; insert file6; insert file7;
            
    }
    
    static testMethod void testJsonBuild() {
        init();             
        ss = new RM_SaveSearchController ();
        
        ss.getRoot();
        system.assert(ss.userID == UserInfo.getUserID());
    }
    static testMethod void testJsonNoSub(){
        root1 = new Root_Folder__c(Name = 'root1');
        root2 = new Root_Folder__c(Name = 'root2');
        Root_Folder__c root3 = new Root_Folder__c(Name = 'root3');
            insert root1; insert root2; insert root3;
        
        ss = new RM_SaveSearchController ();
        ss.getRoot();
        
    }
    static testMethod void testAddText(){
        init();
        folderId = '123456789';
        folderName = 'The Folder Name';
        
        PageReference pf = new PageReference('apex/savedsearches');
        Test.setCurrentPage(pf);
        ss = new RM_SaveSearchController ();
        ApexPages.currentPage().getParameters().put('folderId', folderId);
        ApexPages.currentPage().getParameters().put('folderText', folderName);
        ss.folderId = ApexPages.currentPage().getParameters().get('folderId');
        ss.folderText = ApexPages.currentPage().getParameters().get('folderText');
        system.assert(ss.folderId == '123456789');
        system.assert(ss.folderText == 'The Folder Name');
        ss.addText();
    }
   
   
    static testMethod void testNewFolder(){
        init();            
        RM_SaveSearchController.InsertFolder(' New Test Folder',root1.id);
        
        Sub_Folder__c newSub = RM_OtherService.GetSubFolderID('New Test Folder', 'root1', UserInfo.getUserID());
        
    
        system.assert(newSub.Name == 'New Test Folder');
    }
   
 
    static testMethod void testRemove(){
        init(); 
        RM_SaveSearchController.removeFolderFile('File', file4.Id);  
        list<Saved_Search__c> removefile = [select Id, Name, sub__c from Saved_Search__c where id=:file4.Id];
        system.assert(removefile.size() == 0);
               
        RM_SaveSearchController.removeFolderFile('Folder', userSub1.Id);      
        list<Sub_Folder__c> removeSub = [ select Id, name, Custom__c, root__c from Sub_Folder__c where id= :userSub1.Id];
        system.assert(removeSub.size() == 0);
       
 
    }
    static testMethod void testRename(){
        init();        
        RM_SaveSearchController.RenameLibrary('Folder', userSub1.Id, ' Rename New');
        Sub_Folder__c renameSub = RM_OtherService.GetSubFolderbyID(userSub1.Id);
        system.assert(renameSub.name =='Rename New');
        
        RM_SaveSearchController.RenameLibrary('File', file4.Id, ' Rename New File');
        Saved_Search__c renameFile = RM_OtherService.getSavedSearchByID(file4.id);
        system.assert(renameFile.name == 'Rename New File');
    }
    static testMethod void testMove(){
        init();
        RM_SaveSearchController.moveFile(file4.Id, userSub2.Id, '', '');
        Saved_Search__c renameFile = RM_OtherService.getSavedSearchByID(file4.id);
        system.assert(renameFile.sub__c == userSub2.Id);
    }
    static testMethod void testRun(){
        init();
        PageReference pf = new PageReference('apex/savedsearches');
        Test.setCurrentPage(pf);
        ss = new RM_SaveSearchController ();
        
       
        ApexPages.currentPage().getParameters().put('searchId', file4.Id);
        PageReference page = ss.run();
        system.debug('------page-------'+page.getUrl());
        system.assert(page.getUrl() == '/apex/RM_searchresources?cid=' + file4.Id);
    }
}