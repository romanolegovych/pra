/**
 * Test for AdhocToCourse controller
 * Author: Andrew Allen
 */
@isTest
private class LMS_TestAdhocToCourseController {

	static List<Role_Type__c> roleTypes;
	static List<LMS_Role__c> roles;
	static List<Course_Domain__c> courseDomains;
	static List<LMS_Course__c> courses;
	static List<Country__c> countries;
	static List<Employee_Details__c> employees;
	static List<LMS_Role_Course__c> roleCourses;
	static List<LMS_Role_Employee__c> roleEmployees;
	static List<LMSConstantSettings__c> constants;
	static List<CourseDomainSettings__c> domains;
	static List<RoleAdminServiceSettings__c> settings;
	
	static void init(){
    	constants = new LMSConstantSettings__c[] {
    		new LMSConstantSettings__c(Name = 'inSync', Value__c = 'Y'),
    		new LMSConstantSettings__c(Name = 'outSync', Value__c = 'N'),
    		new LMSConstantSettings__c(Name = 'addMappings', Value__c = 'A'),
    		new LMSConstantSettings__c(Name = 'removeMappings', Value__c = 'D'),
    		new LMSConstantSettings__c(Name = 'roleActive', Value__c = 'Active'),
    		new LMSConstantSettings__c(Name = 'roleInactive', Value__c = 'Inactive'),
    		new LMSConstantSettings__c(Name = 'typePRA', Value__c = 'PRA'),
    		new LMSConstantSettings__c(Name = 'statusTransitAdd', Value__c = 'Transit - Add'),
    		new LMSConstantSettings__c(Name = 'statusTransitDelete', Value__c = 'Transit - Delete'),
    		new LMSConstantSettings__c(Name = 'statusDraft', Value__c = 'Draft'),
    		new LMSConstantSettings__c(Name = 'statusRemoved', Value__c = 'Removed'),
    		new LMSConstantSettings__c(Name = 'statusRetired', Value__c = 'Retired'),
    		new LMSConstantSettings__c(Name = 'statusToDelete', Value__c = 'Delete'),
    		new LMSConstantSettings__c(Name = 'statusSuspended', Value__c = 'Suspended'),
    		new LMSConstantSettings__c(Name = 'statusCommitted', Value__c = 'Committed'),
    		new LMSConstantSettings__c(Name = 'statusDraftDelete', Value__c = 'Draft Delete'),
    		new LMSConstantSettings__c(Name = 'statusPendingAdd', Value__c = 'Pending - Add'),
    		new LMSConstantSettings__c(Name = 'statusPendingDelete', Value__c = 'Pending - Delete')
    	};
    	insert constants;
    	
    	domains = new CourseDomainSettings__c[] {
    		new CourseDomainSettings__c(Name = 'Internal', Domain_Id__c = 'Internal'),
    		new CourseDomainSettings__c(Name = 'PST', Domain_Id__c = 'Project Specific'),
    		new CourseDomainSettings__c(Name = 'Archive', Domain_Id__c = 'Archive')
    	};
    	insert domains;
    	
    	settings = new RoleAdminServiceSettings__c[] {
    		new RoleAdminServiceSettings__c(Name = 'DOMAIN', Value__c = 'DOMAIN'),
    		new RoleAdminServiceSettings__c(Name = 'ENDPOINT', Value__c = 'ENDPOINT'),
    		new RoleAdminServiceSettings__c(Name = 'TIMEOUT', Value__c = '50000')
    	};
    	insert settings;
    	
		roleTypes = new Role_Type__c[]{
			new Role_Type__c(Name = 'PRA'),
			new Role_Type__c(Name = 'Project'),
			new Role_Type__c(Name = 'Additional Role')
		};
		insert roleTypes;
		
		courseDomains = new Course_Domain__c[]{
			new Course_Domain__c(Domain__c = 'Internal'),
			new Course_Domain__c(Domain__c = 'Archive'),
			new Course_Domain__c(Domain__c = 'Project Specific')
		};
		insert courseDomains;
				
		courses = new LMS_Course__c[]{
    		new LMS_Course__c(Offering_Template_No__c = 'course123', Course_Code__c = 'C123', Title__c = 'Course 123', Domain_Id__c = 'Internal',
    						  Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
    						  Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse1',
    						  Discontinued_From__c = Date.today()+1, Duration__c = 60),
    		new LMS_Course__c(Offering_Template_No__c = 'course321', Course_Code__c = 'C124', Title__c = 'Course 321', Domain_Id__c = 'Internal',
    						  Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
    						  Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse2',
    						  Discontinued_From__c = Date.today()+1, Duration__c = 60),
    		new LMS_Course__c(Offering_Template_No__c = 'course456', Course_Code__c = 'C125', Title__c = 'Course 456', Domain_Id__c = 'Internal',
    						  Type__c = 'SOP', Status__c = 'Active', Created_By__c = 'ASA', Created_On__c = Datetime.now()-30, Updated_By__c = 'ASA', 
    						  Updated_On__c = Datetime.now()-10, Available_From__c = Date.today(), SABA_ID_PK__c = 'sabaCourse3',
    						  Discontinued_From__c = Date.today()+1, Duration__c = 60)
    	};
    	insert courses;
	}
	
	static void initRoles(){
		roles = new LMS_Role__c[]{
			new LMS_Role__c(Adhoc_Role_Name__c = 'Adhoc Role 1', Status__c = 'Active', Sync_Status__c = 'Y', SABA_Role_PK__c = 'SABA Role 1', Role_Type__c = roleTypes[2].Id),
			new LMS_Role__c(Adhoc_Role_Name__c = 'Adhoc Role 2', Status__c = 'Active', Sync_Status__c = 'Y', SABA_Role_PK__c = 'SABA Role 2', Role_Type__c = roleTypes[2].Id)
		};
		insert roles;
	}
	
	static void initMappings(){
		roleCourses = new LMS_Role_Course__c[]{
			new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
			new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[0].Id, Sync_Status__c = 'N', Status__c = 'Draft Delete', Assigned_By__c = 'ASA'),
			new LMS_Role_Course__c(Role_Id__c = roles[0].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Draft', Assigned_By__c = 'ASA'),
			new LMS_Role_Course__c(Role_Id__c = roles[1].Id, Course_Id__c = courses[1].Id, Sync_Status__c = 'N', Status__c = 'Committed', Assigned_By__c = 'ASA')
		};
		insert roleCourses;
	}
	
	static void initEmployees() {    	
    	countries = new Country__c[] {
    		new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='US',Region_Name__c='US/Canada', 
                               Daily_Business_Hrs__c=8.0)
    	};
    	insert countries;
    	
        employees = new Employee_Details__c[] {
            new Employee_Details__c(Name = 'employee1', Employee_Unique_Key__c = 'employee1', First_Name__c = 'Andrew', Last_Name__c = 'Allen', 
            	Email_Address__c = 'a@a.com', Job_Class_Desc__c = 'Accountant', Job_Code__c = 'Code1', Business_Unit_Desc__c = 'Analysis and Reporting', 
            	Department__c = 'Analysis and Reporting', Country_Name__c = countries[0].Id, Status__c = 'AA', Function_code__c='PR',Buf_Code__c='KCICR', 
            	Date_Hired__c = date.parse('12/27/2009'), Business_Unit__c = 'RDU')
        };
        insert employees;
        
        roleEmployees = new LMS_Role_Employee__c[] {
        	new LMS_Role_Employee__c(Role_Id__c = roles[0].Id, Employee_Id__c = employees[0].Id, Status__c = 'Draft', Sync__c = 'N',
                                     Created_On__c = Date.today(), Updated_On__c = Date.today())
        };
        insert roleEmployees;
	}

	static testMethod void testConstructor(){
    	init();
    	String internalDomain = domains[0].Domain_Id__c;
    	LMS_AdhocToCourseController c = new LMS_AdhocToCourseController();
    	System.assert(c.courseFilter == 'and Name != null and Type__c != \'PST\' and Domain_Id__c = \'' + internalDomain + '\' ' + 
        	'and (Discontinued_From__c >= TODAY OR Discontinued_From__c = null) and Status__c IN (\'Active\',\'Testing\') ' + 
        	'and (NOT Course_Code__c LIKE \'%_ASSESS_PILOT\') AND (NOT Course_Code__c LIKE \'%_ASSESS_RETAKE\') AND (NOT Course_Code__c LIKE \'%_RETRAINING\')');
    }
    
    static testMethod void testSearch(){
		init();
		initRoles();
		initMappings();
		initEmployees();
		LMS_AdhocToCourseController c = new LMS_AdhocToCourseController();
		c.courseText = courses[0].Title__c;
		c.search();
		System.debug('------------courseList-------------'+c.roles.size());
		System.assert(c.roles[0].assignmentStatus == 'Draft' && c.roles[1].assignmentStatus == 'Draft Delete');
		
		c.courseText = courses[1].Title__c;
		c.search();
		System.debug('------------courseList-------------'+c.roles.size());
		System.assert(c.roles[1].assignmentStatus == 'Draft' && c.roles[0].assignmentStatus == 'Committed');
		
		c.courseText = courses[2].Title__c;
		c.search();
		System.debug('------------courseList-------------'+c.roles.size());
		//System.assert(c.disableCourse == true && c.renderExport == false);
		
		c.courseText = 'new course';
		c.search();
		//System.assert(c.bRoleSearch == true && c.courseError == true && c.renderBlock == false);
	}
	
	static testMethod void testRoleSearch(){
		init();
		initRoles();
		initMappings();
		LMS_AdhocToCourseController c = new LMS_AdhocToCourseController();
		c.courseText = courses[0].Title__c;
		c.search();
		c.roleSearch();
		System.assert(c.selRoles.size() == 1);
		
		c.courseText = courses[2].Title__c;
		c.search();
		c.roleName = 'A';
		c.roleSearch();
		System.assert(c.selRoles.size() == 2);
	}
	
	static testMethod void testRoleSearchFailure(){
		init();
		initRoles();
		initMappings();
		LMS_AdhocToCourseController c = new LMS_AdhocToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		
		c.roleName = 'Job';
		c.roleSearch();
		System.assert(c.selRoles.size() == 0);
	}
	
	static testMethod void testAddRole(){
		init();
		initRoles();
		initMappings();
		LMS_AdhocToCourseController c = new LMS_AdhocToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		
		c.roleName = 'A';
		c.roleSearch();
		
		c.selRoles[0].selected = true;
		PageReference pr = c.addRole();
		System.assert(pr == null);
	}
	
	static testMethod void testCancel(){
		init();
		initRoles();
		initMappings();
		LMS_AdhocToCourseController c = new LMS_AdhocToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		
		PageReference pr = c.cancel();
		System.assert(pr == null);
	}
	
	static testMethod void testCommitRoles(){
		init();
		initRoles();
		initMappings();
		LMS_AdhocToCourseController c = new LMS_AdhocToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		PageReference pr = c.commitRoles();
		System.assert(pr == null);
	}
	
	static testMethod void testApplyCommitDate(){
		init();
		initRoles();
		initMappings();
		LMS_AdhocToCourseController c = new LMS_AdhocToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		
		c.roles[0].selected = true;
		c.commitDate = '12/12/2102';
		PageReference pr = c.setCommitDate();
		System.assert(pr == null);
	}
	
	static testMethod void testRemoveRole(){
		init();
		initRoles();
		initMappings();
		LMS_AdhocToCourseController c = new LMS_AdhocToCourseController();
		c.courseText = courses[1].Title__c;
		c.search();
		
		c.roles[0].selected = true;
		PageReference pr = c.removeRoles();
		System.assert(pr == null);
	}
	
	static testMethod void testCourseReset() {
		init();
		LMS_AdhocToCourseController c = new LMS_AdhocToCourseController();
		c.courseReset();
		System.assert(!c.bWebServiceError && c.roles == null && c.selRoles == null);
	}
	
	static testMethod void testRoleReset() {
		init();
		LMS_AdhocToCourseController c = new LMS_AdhocToCourseController();
		c.roleReset();
		System.assert(c.selRoles == null);
	}
	
	static testMethod void testCloseCourseSearch() {
		init();
		LMS_AdhocToCourseController c = new LMS_AdhocToCourseController();
		c.closeCourseSearch();
		System.assert(c.selRoles == null);
	}
	
	static testMethod void testShowErrorPage() {
		init();
		LMS_AdhocToCourseController c = new LMS_AdhocToCourseController();
		PageReference newPage = c.showErrors();
		System.assert(newPage.getUrl() == '/apex/LMS_AdhocCourseError');
	}
	
}