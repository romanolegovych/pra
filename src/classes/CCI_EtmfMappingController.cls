public class CCI_EtmfMappingController {
	
	public String protId { get; set; }
	public String etmfInstance { get; set; }
	public String ctmsInstance { get; set; }
	public String clientProtNum { get; set; }
	public String saveProtSystemId {get;set;}
	public String saveCtmsInstance {get;set;}
	public String saveEtmfInstance {get;set;}
	public String selectedProtSiteId{get;set;}
	public MdmProtocolService.protocolVO protocolVO {get;set;}
	public MdmMappingService.etmfSiteVO selectedEtmfSiteVO {get;set;}
	public Map<String, MdmMappingService.etmfSiteVO> etmfSiteVOMap {get;set;}
	public List <MdmMappingService.etmfSiteVO> etmfVO {get;set;}
	private MdmMappingService.etmfSiteVO ETMFVOObj;
	
	public List<MdmMappingService.etmfSiteVO> getEtmfVO() { 
		return etmfVO;
	}
	
	public list<selectoption> getEtmf() {
		
		MdmMappingService.valueListResposne resp=  MdmMappingServiceWrapper.getETMFCrsInstances();
		List<SelectOption> options = new List<SelectOption>();
		options.add(new selectoption('', '--- Select an ETMF Instance ---'));
		if (resp.values != null) 
		{
			for(String c : resp.values)
			{
				options.add(new selectoption(c,c));
			}	
		}
		return options;
		
	}
	
	public void searchProtocol() {
		
		Long protocolId = 0L;
		
		if(protId != NULL && protId != '')
		{
			protocolId=Long.valueOf(protId);
		}
		if (ctmsInstance == null) {
			ctmsInstance = '';
		}
		if (etmfInstance == null) {
			etmfInstance = '';
		}
		system.debug('-----------------protocolId-----------------' + protocolId);
		system.debug('-----------------clientProtNum-----------------' + clientProtNum);
		system.debug('-----------------ctmsInstance-----------------' + ctmsInstance);
		system.debug('-----------------etmfInstance-----------------' + etmfInstance);
		MdmMappingService.etmfSiteListResponse resp = MdmMappingServiceWrapper.getEtmfSitesSearch(protocolId,clientProtNum,ctmsInstance,etmfInstance);
		etmfVO =  new List <MdmMappingService.etmfSiteVO>();
		if (resp != null && resp.etmfSiteVOs != null) {
			etmfVO = resp.etmfSiteVOs;
			if (etmfVO != null && etmfVO.size() > 0) {
				etmfSiteVOMap = new Map<String, MdmMappingService.etmfSiteVO>();
				for (MdmMappingService.etmfSiteVO etmfSiteVO : etmfVO) {
					etmfSiteVOMap.put(String.valueOf(etmfSiteVO.siteId), etmfSiteVO);
					//instanceVOMap.put(String.valueOf(protSiteVO.siteId), protSiteVO.instanceVO);
				}
			}
		}
		
	}
	
	public void Add()
	{
		ApexPages.getMessages().clear();
		MdmMappingService.etmfSiteVO etmfSiteVO;
		
		if(saveProtSystemId != NULL  && saveCtmsInstance!=NULL && saveEtmfInstance!=NULL && protocolVO != null)
		{
			ETMFVOObj =new MdmMappingService.etmfSiteVO();
			ETMFVOObj.siteId = 0L;
			ETMFVOObj.etmfSiteName = saveEtmfInstance;
			ETMFVOObj.ctmsSiteName = saveCtmsInstance;
			ETMFVOObj.protocolId = protocolVO.protocolId;
			ETMFVOObj.protocolSystemId = protocolVO.systemId;
			ETMFVOObj.source= UserInfo.getName();
		}
		MdmMappingService.mdmCrudResponse response = MdmMappingServiceWrapper.saveEtmfProtocolMappingFromVO(ETMFVOObj);    
		if (response.operationStatus.equals('0')) {
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, 'Protocol Mapping saved successful'));
		} else {
			Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Protocol Mapping save failed with errors: ' + response.errors));
		}
	}
	
	public void searchProtocolSysIds()
	{
		MdmProtocolService.protocolResponse response = MdmProtocolServiceWrapper.getProtocolVOByClientProtNum(String.escapeSingleQuotes(saveProtSystemId));
		protocolVO = response.protocolVO;
	}
	
	public void findEtmfSiteVO() {
		if (selectedProtSiteId != null) {
			selectedEtmfSiteVO = new MdmMappingService.etmfSiteVO();
			selectedEtmfSiteVO = etmfSiteVOMap.get(selectedProtSiteId);
		}
	}
	
	public list<selectoption> getCtms() {
		
		MdmMappingService.valueListResposne resp=  MdmMappingServiceWrapper.getCTMSCrsInstances();
		List<SelectOption> options = new List<SelectOption>();
		options.add(new selectoption('', '--- Select a CTMS Instance ---'));
		for(String c : resp.values)
		{
			options.add(new selectoption(c,c));
		}
		return options;
		
	}
	
	public void save() {
		if (selectedEtmfSiteVO != null) {
			selectedEtmfSiteVO.source = UserInfo.getName();
			MdmMappingService.mdmCrudResponse response = MdmMappingServiceWrapper.saveEtmfProtocolMappingFromVO(selectedEtmfSiteVO);
			if (response.operationStatus.equals('0')) {
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, 'Protocol Mapping save successful'));
			} else {
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Protocol Mapping save failed with errors: ' + response.errors));
			}
		}
	}
	
	public void deleteMapping() {
		if (selectedEtmfSiteVO != null) {
			MdmMappingService.mdmCrudResponse response = MdmMappingServiceWrapper.deleteEtmfProtocolMappingFromVO(selectedEtmfSiteVO);
			if (response.operationStatus.equals('0')) {
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.INFO, 'Protocol Mapping delete successful'));
			} else {
				Apexpages.addMessage(new Apexpages.Message(Apexpages.Severity.ERROR, 'Protocol Mapping delete failed with errors: ' + response.errors));
			}
		}
	}

}