/**
@author 
@date 2014
@description this controller for model site activation 
**/
public with sharing class SRM_ModelSiteActivationController{

    //Member variables
    public SRM_Model_Site_Activation__c siteActivation{ get; set; }
    public String chartData{ get; set; }
    public List<String> xValuesList{ get; set; }
    public List<Decimal> yValuesList{ get; set; }
    public String showChart{ get; set; }
    public String returnUrl{ get; set; }
    public String recordId{ get; set; }
    public Boolean applyWeekAdj{ get; set; }
    public Country__c country{get; set; }
    
    public SRM_ModelSiteActivationController(ApexPages.StandardController controller) {
        String id = ApexPages.CurrentPage().getParameters().get('id');
        returnUrl = ApexPages.CurrentPage().getParameters().get('retUrl');
        String clone = ApexPages.CurrentPage().getParameters().get('clone');
        recordId = id;
        siteActivation = (SRM_Model_Site_Activation__c)controller.getRecord();
        String parentId = siteActivation.SRM_Model__c;
        xValuesList = new List<String>();
        yValuesList = new List<Decimal>();
        showChart = 'none';
        applyWeekAdj = false;
        if(String.isEmpty(id)){
            siteActivation = new SRM_Model_Site_Activation__c();
            if(!String.isEmpty(parentId)){
                siteActivation.SRM_Model__c = parentId;
            }
        }
        else if(!String.isEmpty(clone)){
                          
             SRM_Model_Site_Activation__c temp = SRM_ScenarioDataAccessor.getSiteActivationById(id);
             SRM_Model_Site_Activation__c clonerecord = temp.clone(false,true);
             siteActivation = clonerecord;
             generateChartData();
                                                              
             }
        else{
            SRM_Model_Site_Activation__c tmpObj = SRM_ScenarioDataAccessor.getSiteActivationById(id);
            if(tmpObj != null){
                siteActivation = tmpObj;
                system.debug('---siteActivation---'+siteActivation);
                generateChartData();
            }
            
        }
        
    }


    public SRM_ModelSiteActivationController(){}
    
    
        /**
@author 
@date 2014
@description this method to generate chart data
**/
    public PageReference generateChartData(){
        SRM_ModelSiteActivationSevice service = new SRM_ModelSiteActivationSevice();
        if(validate()){
            showChart = '';
            xValuesList.clear();
            yValuesList.clear();
            yValuesList = service.generateChartData(siteActivation, 'SRM_Model_Site_Activation__c', applyWeekAdj);
            for(Integer i=1; i<=(siteActivation.Number_of_Weeks__c); i++){
                xValuesList.add(i+'');
                System.debug('xxValuesList: '+i);
            }
            System.debug('txValuesList: '+xValuesList);
                
        }
        return null;
    }
    /*@author Fahad Arif
	@date 2015
	@description this method to generate the picklist from WFM_Therapeutics
	**/
    public List<selectOption> gettherptic(){    
       List<selectOption> therapticlist = new List<selectOption>();    
       Set<string> uniqueNames = new set<string>(); 
       //to add blank value as default
       therapticlist.add(new SelectOption('','  '));
       for(WFM_Therapeutic__c ta: [select id,name,Therapeutic_Area__c,Therapeutic_Area__r.Name  from WFM_Therapeutic__c 
                                          where Status__c = 'Approved' and Therapeutic_Area__c != null order by Therapeutic_Area__r.Name asc]) {
           if(!uniqueNames.contains(ta.Therapeutic_Area__r.name))                                   
           		therapticlist.add(new selectOption(ta.Therapeutic_Area__c,ta.Therapeutic_Area__r.name));
           uniqueNames.add(ta.Therapeutic_Area__r.name);                                   
           
       } 
       return therapticlist;
    }
    
   /**
@author 
@date 2014
@description this method to save chart data
**/
    //Method to save
    public PageReference saveSiteActivation(){
        if(siteActivation.country__c != null){
            country = new country__c();
            country = [select Id, Name from country__c where id =: siteActivation.country__c];
            if(!Country.Name.equals('UNITED STATES') && !siteActivation.IRB_Type__c.equals('N/A')){
                SRM_Utils.showErrorMessage('Invalid IRB Type selction for '+Country.Name+'.');
                return null;
            }
        }
        if(validate()){
            if(siteActivation != null){
                try{
                    upsert siteActivation;
                    PageReference p = new PageReference(URL.getSalesforceBaseUrl().toExternalForm()+'/'+siteActivation.Id);
                    p.setRedirect(true);
                    System.debug('Created or Updated Id: '+siteActivation.Id);
                    return p;
                }
                catch(DMLException ex){
                    if(ex.getDmlType(0) == StatusCode.DUPLICATE_VALUE){
                        SRM_Utils.showErrorMessage('A record already exists for this Country and Therapeutic Area combination. Please modify this record before saving so it is not an exact duplicate.');
                    }
                    /* if(ex.getDmlType(0) == StatusCode.FIELD_CUSTOM_VALIDATION_EXCEPTION){
                        System.debug('You do not have permission to edit a record once the model is Approved.');
                        SRM_Utils.showErrorMessage('You do not have permission to edit a record once the model is Approved or Retired.');
                    } */
                    ApexPages.addMessages(ex);
                }
            }    
        }
        return null;
    }
    
            /**
@author 
@date 2014
@description this method to validate chart data
**/ 
    public Boolean validate(){
        if(siteActivation.Start_Week__c < 0){
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Please enter a positive value for Delay');
           ApexPages.addMessage(myMsg);
           return false; 
        }
        else if(siteActivation.Amplitude__c <= 0){
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.FATAL,'Please enter a positive value for Curve Height');
           ApexPages.addMessage(myMsg);
           return false; 
        }
        else if(siteActivation.Standard_Deviation__c <= 0){
           ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR,'Please enter a positive value for Curve Width');
           ApexPages.addMessage(myMsg);
           return false; 
        }
        else if(siteActivation.Number_of_Weeks__c <=0){
            SRM_Utils.showErrorMessage('Invalid value is entered for Number of Weeks.');
            return false;
        }
        else{
            return true;
        }
    }
    
    
            /**
@author 
@date 2014
@description this method to cancel
**/
    //Method to return
    public PageReference cancel(){
        String returnRecordId = '';
        String redUrl = '';
        if(siteActivation.Id == null){
            redUrl = returnUrl;
        }
        else{
            returnRecordId = siteActivation.Id;
            redUrl = URL.getSalesforceBaseUrl().toExternalForm()+'/'+returnRecordId;
        }
        PageReference p = new PageReference(redUrl);
        p.setRedirect(true);
        return p;
    }
}