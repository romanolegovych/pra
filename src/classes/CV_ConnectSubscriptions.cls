@RestResource(urlMapping='/CV_ConnectSubscriptions/*')
global with sharing class CV_ConnectSubscriptions{
	
    @HttpPost
    global static PRA_WrappersForMobile.RegistrationsPerProtocolResult registerForNotifications( List<NCM_API_DataTypes.NotificationRegistrationWrapper> registrations, List<String> protocolIds ){
    	return PC_Utils.registerForNotifications( registrations, protocolIds );
    }
        
}