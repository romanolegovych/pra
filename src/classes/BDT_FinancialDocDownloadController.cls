public with sharing class BDT_FinancialDocDownloadController {
	public FinancialDocument__c 			 financialDocument 	  	{get;set;}
	public String 							 financialDocumentID	{get;set;}
	public Boolean							 showWarningTextFlag	{get;set;}
	public String							 paperSize			  	{get;set;}
	public Client_Project__c 				 singleProject		  	{get;set;}
	public List<ProposalStudiesWrapper>		 studiesListWr		  	{get;set;}
	private List<Study__c> 					 studiesList;
	public FinancialDocumentContent__c		 finDocContent			{get;set;}
	public Map<String,BDT_DC_FinancialDocument.financialDocumentContentEntriesWrapper> contentEntriesMap{get;set;}
	public Boolean							 existingContentFlag   	{get;set;}
	public List<mileStoneAndStudiesWrapper>  mileStoneToStudiesList	{get;set;}
	public List<PopulationToStudiesWrapper>  popToStudiesWr			{get;set;}
	public List<BDT_DC_FinancialDocument.passThroughServicesWrapper> passThroughServiceWrapperList{get;set;}
	public List<BDT_DC_FinancialDocument.priceWrapper>				 passThroughServiceTotalsList {get;set;}
	public List<BDT_DC_FinancialDocument.paymentTermsWrapper> 		 paymentTermsWrapperList	  {get;set;}
	public List<String>						 paymentTermTotalsList	{get;set;}
	public List<BDT_DC_FinancialDocument.serviceCategoryWrapper> 	 serviceCategoryWr{get;set;}
	public List<BDT_DC_FinancialDocument.serviceCategoryPriceTotalsWrapper> serviceCategoryPriceTotalsWrapperList{get;set;} 
	public List<Boolean>					 collapseDiscountColumnFlagList{get;set;}
	public List<clinicalDesignWrapper>		 clinicalDesignWr		{get;set;}
	public Integer							 maxTimePointsPerPage	{get;set;}
	public Integer							 maxAssessmentsPerPage	{get;set;}
	public Map<string, string> 				 categorySums			{get;set;}
	public map<string, string> 				 studyTotalsMap 		{get;set;}
	public String 							 currentDateString 		{get;set;}
	public String							 currentDateCoverPageString{get;set;}
	public List<BDT_DC_FinancialDocument.priceWrapper> projectPassThroughServiceTotalsList  {get;set;}
	public List<BDT_DC_FinancialDocument.priceWrapper> projectServiceCategoryPriceTotalsList{get;set;}
	public List<BDT_DC_FinancialDocument.priceWrapper> projectTotalsList{get;set;}
	public List<BDT_DC_FinancialDocument.serviceCategoryPriceTotalsWrapper> TotalCostsWrapperList {get;set;}
	public List<BDT_DC_FinancialDocument.servicesAndCostsReportStudyWrapper> servicesAndCostsStudiesReportList{get;set;}
	// Laboratory Items
	private Set<String>						studiesANSet;
	private Set<String>						studiesMdSet;
	private Set<String>						studiesValSet;
	private List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer> 	  methodCompStudyList;
	public  List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentMappedToMethod> methodDetailsList			{get;set;}
	public  List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> 				  labSlotsAnWrapperList		{get;set;}
	public  List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper> 				  labSlotsMdValWrapperList	{get;set;}
	public	List<LaboratorySamplesService.LaboratorySamplesDesignWrapper> 					  labSamplesWrapperList  	{get;set;}
	
	
	public BDT_FinancialDocDownloadController() {
		// Initialize variables
		initializeGlobalVariables();
		//Apexpages.currentPage().getHeaders().put('content-disposition', 'attachment; filename=Proposal.pdf');
		Apexpages.currentPage().getHeaders().put('content-disposition', 'inline; filename=Proposal.pdf');
		// Pdf paper size and current date
		paperSize=system.currentPageReference().getParameters().get('selectedPaperSize');
		paperSize = ( String.IsBlank(paperSize) )? 'A4' : paperSize;
		
		currentDateCoverPageString = BDT_DC_FinancialDocument.formatCurrentDateAnalytical();
		currentDateString		   = BDT_DC_FinancialDocument.formatCurrentDate();
		// Display sizes for design
		maxTimePointsPerPage   = (paperSize == 'A4')? 39 : 38;
		maxAssessmentsPerPage  = (paperSize == 'A4')? 31 : 32;
		// Find the selected financial Document
		financialDocumentID = system.currentPageReference().getParameters().get('financialDocumentID');
		if( String.isBlank(financialDocumentID) ) {
			errorOnReadingFinancialDocument();
			return;
		} else {
			financialDocument = BDT_DC_FinancialDocument.findFinancialDocument(financialDocumentId);
			if( String.isBlank(financialDocument.Name) ) {
				errorOnReadingFinancialDocument();
				return;
			}
			showWarningTextFlag = BDT_DC_FinancialDocument.showWarningMessagesInPDF(financialDocument);
			if( findProjectInfoAndProposalStudies() ) {
				findStudiesCurrency();
				contentEntriesMap = BDT_DC_FinancialDocument.readContentSelectionEntriesProposal(financialDocumentId);
				existingContentFlag = ! contentEntriesMap.isEmpty();
				if(existingContentFlag) {
					finDocContent = BDT_FinancialDocumentContentService.getContentByDocumentId(financialDocumentId);
				}
				getStudiesIdsByType();
				updateStudiesWithTheirType();
				findAssociationsWithStudies();
				readMilestonesPerStudies();
				readPassThroughServicesData();
				readPaymentTermsPerStudies();
				readServiceCategoriesPrices();
				readDesigns();
				readMethodAndLabDesignDetails();
				calculateCosts();
				readServicesAndCostsSectionData();
			} else {
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected project.Please, reload the page.');
				ApexPages.addMessage(msg);
			}
		}
	}
	
	
	/**
	*	Initialize the global variables of the controller.
	*	@author   Dimitrios Sgourdos
	*	@version  18-Nov-2013
	*/
	private void initializeGlobalVariables() {
		contentEntriesMap 	   = new Map<String,BDT_DC_FinancialDocument.financialDocumentContentEntriesWrapper>();
		finDocContent		   = new FinancialDocumentContent__c();
		studiesList 		   = new List<Study__c>();
		studiesListWr 	  	   = new List<ProposalStudiesWrapper>();
		existingContentFlag    = false;
		mileStoneToStudiesList = new List<mileStoneAndStudiesWrapper>();
		popToStudiesWr		   = new List<PopulationToStudiesWrapper>();
		passThroughServiceWrapperList = new List<BDT_DC_FinancialDocument.passThroughServicesWrapper>();
		passThroughServiceTotalsList  = new List<BDT_DC_FinancialDocument.priceWrapper>();
		paymentTermsWrapperList= new List<BDT_DC_FinancialDocument.paymentTermsWrapper>();
		paymentTermTotalsList  = new List<String>();
		serviceCategoryWr 	   = new List<BDT_DC_FinancialDocument.serviceCategoryWrapper>();
		serviceCategoryPriceTotalsWrapperList = new List<BDT_DC_FinancialDocument.serviceCategoryPriceTotalsWrapper>();
		collapseDiscountColumnFlagList 		  = new List<Boolean>();
		projectPassThroughServiceTotalsList	  = new List<BDT_DC_FinancialDocument.priceWrapper>();
		projectServiceCategoryPriceTotalsList = new List<BDT_DC_FinancialDocument.priceWrapper>();
		projectTotalsList	   = new List<BDT_DC_FinancialDocument.priceWrapper>();
		TotalCostsWrapperList  = new List<BDT_DC_FinancialDocument.serviceCategoryPriceTotalsWrapper>();
		clinicalDesignWr	   = new List<clinicalDesignWrapper>();
		servicesAndCostsStudiesReportList = new List<BDT_DC_FinancialDocument.servicesAndCostsReportStudyWrapper>();
		// Laboratory Items
		studiesANSet  = new Set<String>();
		studiesMdSet  = new Set<String>();
		studiesValSet = new Set<String>();
		methodCompStudyList		 = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentContainer>();
		methodDetailsList		 = new List<LaboratoryMethodCompoundService.MethodCompoundStudyAssignmentMappedToMethod>();
		labSlotsAnWrapperList	 = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		labSlotsMdValWrapperList = new List<LaboratoryAnalysisSlotsService.LaboratorySlotsDesignWrapper>();
		labSamplesWrapperList	 = new List<LaboratorySamplesService.LaboratorySamplesDesignWrapper>();
	}
	
	
	/**
	*	Give an error message in case the financial document is not found.
	*	@author   Dimitrios Sgourdos
	*	@version  16-Sep-2013
	*/
	private void errorOnReadingFinancialDocument() {
		ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find the selected financial document.Please, select a document and reload the page.');
		ApexPages.addMessage(msg);
	}
	
	
	// ******************** Studies ********************
	
	/** Initialize the project and the proposal studies info.
	* @author   Dimitrios Sgourdos
	* @version  18-Nov-2013
	* @return	If the initialization was successful or not
	*/
	public Boolean findProjectInfoAndProposalStudies() {
		if( ! String.isBlank(financialDocument.Client_Project__c) ){
			singleProject = BDT_Utils.getProject(financialDocument.Client_Project__c, true);
			studiesList = BDT_DC_FinancialDocument.readFinancialDocumentStudies(financialDocument);
			for(Study__c tmpStudy : studiesList) {
				ProposalStudiesWrapper tmpItem = new ProposalStudiesWrapper();
				tmpItem.proposalStudy = tmpStudy;
				studiesListWr.add(tmpItem);
			}
			return true;
		} else {
			return false;
		}
	}
	
	
	/**	Read the studies ids by type(AN or/and MD/VAL).
	* @author   Dimitrios Sgourdos
	* @version  18-Nov-2013
	*/
	private void getStudiesIdsByType() {
		String listOfStudiesIds = BDT_Utils.createConcatinatedStringFromSobject(studiesList, 'Id');
		List<Study__c> tmpStudiesList = new List<Study__c>();
		// AN studies
		tmpStudiesList = StudyService.getStudiesByUsedInAN(listOfStudiesIds);
		for(Study__c tmpStudy : tmpStudiesList) {
			studiesANSet.add(tmpStudy.Id);
		}
		// MD studies
		tmpStudiesList = new List<Study__c>();
		tmpStudiesList = StudyService.getStudiesByUsedInMd(listOfStudiesIds);
		for(Study__c tmpStudy : tmpStudiesList) {
			studiesMdSet.add(tmpStudy.Id);
		}
		// VAL studies
		tmpStudiesList = new List<Study__c>();
		tmpStudiesList = StudyService.getStudiesByUsedInVal(listOfStudiesIds);
		for(Study__c tmpStudy : tmpStudiesList) {
			studiesValSet.add(tmpStudy.Id);
		}
	}
	
	
	/**	Update the studies wrapper list with the study type(AN or/and MD/VAL).
	* @author   Dimitrios Sgourdos
	* @version  18-Nov-2013
	*/
	private void updateStudiesWithTheirType() {
		for(ProposalStudiesWrapper tmpItem : studiesListWr) {
			tmpItem.AnStudyFlag  = studiesANSet.contains(tmpItem.proposalStudy.Id);
			tmpItem.MdStudyFlag  = studiesMdSet.contains(tmpItem.proposalStudy.Id);
			tmpItem.ValStudyFlag = studiesValSet.contains(tmpItem.proposalStudy.Id);
		}
	}
	
	
	// ******************** Read method and design details laboratory ********************
	
	/** Read method and design details laboratory.
	 * @author   Dimitrios Sgourdos
	 * @version  27-Nov-2013
	*/
	private void readMethodAndLabDesignDetails() {
		// Method details
		methodCompStudyList = LaboratoryMethodCompoundService.getMethodCompoundStudyAssignmentContainerData(studiesList);
		methodDetailsList	= LaboratoryMethodCompoundService.getMethodDetailsData(methodCompStudyList);
		
		// Information about anticipated slots for Analysis
		labSlotsAnWrapperList = LaboratoryAnalysisSlotsService.getLaboratorySlotsForLabDesigns(studiesList, false);
		List<LaboratoryAnalysis__c> tmpLabAnalysisList = new List<LaboratoryAnalysis__c>();
		tmpLabAnalysisList = LaboratoryAnalysisService.getLabAnalysisByAnalysisAndUserStudies(studiesList);
		labSlotsAnWrapperList = LaboratoryAnalysisSlotsService.groupSlotsDesignWrapperByStudyMethod(labSlotsAnWrapperList,
																							studiesList,
																							tmpLabAnalysisList);
		
		// Information about anticipated shipments
		List<Study__c> projectStudiesList = StudyService.getStudiesByProjectId(singleProject.Id);
		String listOfStudiesIds = BDT_Utils.createConcatinatedStringFromSobject(projectStudiesList, 'Id');
		List<Study__c> studiesANList = StudyService.getStudiesByUsedInAN(listOfStudiesIds);
		labSamplesWrapperList = LaboratorySamplesService.getLaboratorySamplesDesignContainer(singleProject.Id, studiesANList);
		Set<Decimal> usedShipNumbers = LaboratoryAnalysisSlotsService.getUsedShipmentsNumbersInSlotsForAnalysisDesign(labSlotsAnWrapperList);
		labSamplesWrapperList = LaboratorySamplesService.filterSamplesDesignByShipmentNumberOrStudySelection(labSamplesWrapperList,
																								usedShipNumbers,
																								studiesList);
		
		// Information about anticipated slots for Method Development and Validation
		labSlotsMdValWrapperList = LaboratoryAnalysisSlotsService.getLaboratorySlotsForLabDesigns(studiesList, true);
		tmpLabAnalysisList = new List<LaboratoryAnalysis__c>();
		tmpLabAnalysisList = LaboratoryAnalysisService.getLabAnalysisByMdOrValAndUserStudies(studiesList);
		labSlotsMdValWrapperList = LaboratoryAnalysisSlotsService.groupSlotsDesignWrapperByStudyMethod(labSlotsMdValWrapperList,
																							studiesList,
																							tmpLabAnalysisList);
	}
		
	// ******************** Costs ********************	
		
	/**
	*	Calculate the total costs for the project.
	*	@author   Dimitrios Sgourdos
	*	@version  20-Sep-2013
	*/
	public void calculateCosts() {
		Integer counter = 0;
		// Project Totals and Total Costs
		projectPassThroughServiceTotalsList = BDT_DC_FinancialDocument.calculateTotalValuesPerCurrencyFromPriceWrapperList(passThroughServiceTotalsList);
		for(BDT_DC_FinancialDocument.serviceCategoryPriceTotalsWrapper tmpWrapperItem : serviceCategoryPriceTotalsWrapperList ){
			// project totals creation
			projectServiceCategoryPriceTotalsList.addAll(tmpWrapperItem.priceWrapperList);
			// total costs creation
			BDT_DC_FinancialDocument.serviceCategoryPriceTotalsWrapper tmpItem = new BDT_DC_FinancialDocument.serviceCategoryPriceTotalsWrapper();
			tmpItem.priceWrapperList.addAll(tmpWrapperItem.priceWrapperList);
			tmpItem.priceWrapperList.add(passThroughServiceTotalsList[counter]);
			tmpItem.priceWrapperList = BDT_DC_FinancialDocument.calculateTotalValuesPerCurrencyFromPriceWrapperList(tmpItem.priceWrapperList);
			TotalCostsWrapperList.add(tmpItem);
			counter++;
		}
		projectServiceCategoryPriceTotalsList = BDT_DC_FinancialDocument.calculateTotalValuesPerCurrencyFromPriceWrapperList(projectServiceCategoryPriceTotalsList);
		projectTotalsList.addAll(projectPassThroughServiceTotalsList);
		projectTotalsList.addAll(projectServiceCategoryPriceTotalsList);
		projectTotalsList = BDT_DC_FinancialDocument.calculateTotalValuesPerCurrencyFromPriceWrapperList(projectTotalsList);
	}	
		
		
	// ******************** Services and Costs ******************** 
	
	/**
	*	Calculate the wrapper list for the services and costs reports.
	*	@author   Dimitrios Sgourdos
	*	@version  24-Sep-2013
	*/
	public void readServicesAndCostsSectionData(){
		servicesAndCostsStudiesReportList = BDT_DC_FinancialDocument.readServicesAndCostsSectionData(financialDocumentId, studiesList, true);
	} 
		
		
	// ******************** Pass-through Services ********************
	
	/**
	*	Read the Pass Through Services data for the current financial document.
	*	@author   Dimitrios Sgourdos
	*	@version  18-Sep-2013
	*/
	private void readPassThroughServicesData() {
		passThroughServiceWrapperList = BDT_DC_FinancialDocument.readPassThroughServicesForPDF(StudiesList);
		passThroughServiceTotalsList  = BDT_DC_FinancialDocument.calculatePassThroughServicesSumsForPDF(passThroughServiceWrapperList, StudiesList.size());
	}
		
		
	// ******************** PRICES ******************** 
	
	/**
	*	Read the service categories prices for the current financial document (Financial Summary - Prices section).
	*	@author   Dimitrios Sgourdos
	*	@version  20-Sep-2013
	*/
	public void readServiceCategoriesPrices() {
		serviceCategoryWr = BDT_DC_FinancialDocument.readServiceCategoriesPricesForPDF(financialDocumentId, StudiesList);
		serviceCategoryPriceTotalsWrapperList = BDT_DC_FinancialDocument.calculateServiceCategoryPricesTotals(serviceCategoryWr, studiesList.size());
		collapseDiscountColumnFlagList = BDT_DC_FinancialDocument.collapseDeltaColumn(serviceCategoryPriceTotalsWrapperList);  
	}
	
		
	// ******************** Payment Terms and Conditions ********************	
		
	/**
	*	Read the Payment Terms data for the studies that are selected in the proposal document.
	*	@author   Dimitrios Sgourdos
	*	@version  17-Sep-2013
	*/
	public void readPaymentTermsPerStudies() {
		paymentTermsWrapperList = BDT_DC_FinancialDocument.readPaymentTermsForPDF(financialDocumentID, StudiesList); 
		paymentTermTotalsList	= BDT_DC_FinancialDocument.calculatePaymentTermSumsForPDF(paymentTermsWrapperList, StudiesList.size());
	} 
	
	
	// ******************** Milestones ********************
	
	// It reads the milestones per study
	public void readMileStonesPerStudies() {
		// Initialize variables
		Set<Id> 				studyIds 	  			 = new Set<Id>();
		Set<String> 			milestoneNameSet 		 = new Set<String>();
		List<StudyMileStone__c> milestoneList 			 = new List<StudyMileStone__c>();
		Map<String, String>		milestoneStudyDueDateMap = new Map<String, String>();
		// Take the proposal studies, to associate them with the milestones
		for(ProposalStudiesWrapper stWr : studiesListWr) {
			studyIds.add(stWr.proposalStudy.Id);
		}		
		// Find the milestones
		milestoneList = [SELECT id,
								Name,
								DueDate__c,
								MileStoneDefinition__c,
								Study__c,
								Study__r.Code__c
						FROM 	StudyMileStone__c
						WHERE 	Study__c in :studyIds
						ORDER BY Study__r.Code__c, DueDate__c];
		// Create a map with key MilestoneName:StudyID and value the dueDate__c value
		for(StudyMileStone__c tmpItem : mileStoneList) {
			DateTime tmpDate = tmpItem.dueDate__c;
			if(tmpDate != NULL) {
				tmpDate = tmpDate.addDays(1); // cause the transformation form date to daytime
				mileStoneStudyDueDateMap.put(tmpItem.Name+':'+tmpItem.Study__c, tmpDate.format('dd MMM yyyy') );
			}
		}
		// Create the table rows
		for(StudyMileStone__c tmpItem : mileStoneList) {
			if( (!milestoneNameSet.contains(tmpItem.Name)) && (!String.isBlank(tmpItem.Name)) ) {
				milestoneNameSet.add(tmpItem.Name);
				mileStoneAndStudiesWrapper tmpMileToStudyItem = new mileStoneAndStudiesWrapper();
				tmpMileToStudyItem.mileStoneName = tmpItem.Name;
				for(ProposalStudiesWrapper stWr : studiesListWr) { 
					String tmpStr = tmpItem.Name + ':' + stWr.proposalStudy.Id;
					if( mileStoneStudyDueDateMap.containsKey(tmpStr) ) {
						tmpMileToStudyItem.mileStoneDueDateList.add(mileStoneStudyDueDateMap.get(tmpStr));
					} else {
						tmpMileToStudyItem.mileStoneDueDateList.add('');
					}
				}
				mileStoneToStudiesList.add(tmpMileToStudyItem);
			}
		}
	}
	
	
	// The following class keeps track of the milestones and and assigned studies
	public class mileStoneAndStudiesWrapper {
		public String 	  	mileStoneName 	   	 {get;set;}
		public List<String> mileStoneDueDateList {get;set;}
		
		public mileStoneAndStudiesWrapper() {
			mileStoneDueDateList = new List<String>();
		}
	}
	
	
	// ******************** Designs and Flowcharts ********************
	
	/**	Get the epochs, that belongs to the given set of designs, mapped per design.
	* @author   Dimitrios Sgourdos
	* @version  17-Feb-2014
	* @param	designIdsQuery		The set of design ids for the dynamic query
	* @return	The retrieved epochs mapped per design
	*/
	private Map<String, List<Epoch__c>> readEpochsPerDesign(String designIdsQuery) {
		List<Epoch__c> epochList = EpochDataAccessor.getEpochList('ClinicalDesign__c IN ' + designIdsQuery);
		Map<String, List<Epoch__c>> results = (Map<String,List<Epoch__c>>) BDT_Utils.mapSObjectListOnKey(
																				epochList,
																				new List<String>{'ClinicalDesign__c'}
																			);
		return results;
	}
	
	
	/**	Get the arms, that belongs to the given set of designs, mapped per design.
	* @author   Dimitrios Sgourdos
	* @version  17-Feb-2014
	* @param	designIdsQuery		The set of design ids for the dynamic query
	* @return	The retrieved arms mapped per design
	*/
	private Map<String, List<Arm__c>> readArmsPerDesign(String designIdsQuery) {
		List<Arm__c> armList = ArmDataAccessor.getArmList('ClinicalDesign__c IN ' + designIdsQuery);
		Map<String, List<Arm__c>> results = (Map<String,List<Arm__c>>) BDT_Utils.mapSObjectListOnKey(
																				armList,
																				new List<String>{'ClinicalDesign__c'}
																			);
		return results;
	}
	
	
	/**	Get the ProjectServiceTimePoints, that belongs to the given set of flowcharts, mapped per flowchart.
	* @author   Dimitrios Sgourdos
	* @version  17-Feb-2014
	* @param	designIdsQuery		The set of design ids for the dynamic query
	* @return	The retrieved ProjectServiceTimePoints mapped per flowchart
	*/
	private Map<String, List<ProjectServiceTimePoint__c>> readProjectSrvTimePointsPerFlowchart(
																							String flowchartsIdsQuery) {
		// Get ProjectServiceTimePoints
		List<ProjectServiceTimePoint__c> pstpList = ProjectServiceTimePointDataAccessor.getProjectServiceTimePointList(
																	'flowchart__c IN '+ flowchartsIdsQuery);
		Map<String, List<ProjectServiceTimePoint__c>> results =
										(Map<String,List<ProjectServiceTimePoint__c>>) BDT_Utils.mapSObjectListOnKey(
																				pstpList,
																				new List<String>{'Flowchart__c'}
																			);
		return results;
	}
	
	
	/**	Get the StudyDesignPopulationAssignment, that belongs to the given set of designs and studies, mapped 
	*	per design and study.
	* @author   Dimitrios Sgourdos
	* @version  17-Feb-2014
	* @param	designIds			The design ids for the query
	* @param	studyIds			The study ids for the query
	* @return	The retrieved StudyDesignPopulationAssignment mapped per design
	*/
	private Map<String, List<StudyDesignPopulationAssignment__c>> readStDesPopAssignmentsPerDesign(
																							List<String> designIds,
																							List<String> studyIds) {
		// get data
		List<StudyDesignPopulationAssignment__c> sourceList = new List<StudyDesignPopulationAssignment__c>();
		sourceList = [SELECT	AgregateNumberOfSubjects__c,
								AgregateNumberOfBackups__c,
								PopulationStudyAssignment__r.Study__c,
								PopulationStudyAssignment__r.Population__r.Name,
								ClinicalDesignStudyAssignment__r.ClinicalDesign__c,
								ClinicalDesignStudyAssignment__r.Study__c
						FROM	StudyDesignPopulationAssignment__c
						WHERE	PopulationStudyAssignment__r.Study__c in :studyIDs
						AND		ClinicalDesignStudyAssignment__r.Study__c in :studyIds
						AND		ClinicalDesignStudyAssignment__r.ClinicalDesign__c in :designIds];
		
		// Map data to design
		Map<String, List<StudyDesignPopulationAssignment__c>> results = new Map<String, List<StudyDesignPopulationAssignment__c>>();
		
		for(StudyDesignPopulationAssignment__c tmpItem : sourceList) {
			String key = tmpItem.ClinicalDesignStudyAssignment__r.ClinicalDesign__c
						+ ':'
						+ tmpItem.ClinicalDesignStudyAssignment__r.Study__c;
			
			List<StudyDesignPopulationAssignment__c> tmpList = new List<StudyDesignPopulationAssignment__c>();
			If (results.containsKey(key)) {
				tmpList = results.remove(key);
			}
			tmpList.add(tmpItem);
			results.put(key, tmpList);
		}
		
		return results;
	}
	
	
	/**	Read the designs and flowchart details
	* @author   Dimitrios Sgourdos
	* @version  17-Feb-2014
	*/
	private void readDesigns() {
		// Read Designs
		List<ClinicalDesign__c> designList = ClinicalDesignService.getClinicalDesignListForProject(
																				financialDocument.Client_Project__c);
		
		List<String> designIds = new List<String>();
		for(ClinicalDesign__c tmpDes : designList) {
			designIds.add(tmpDes.Id);
		}
		String designIdsQuery = BDT_Utils.buildSetForDynamicQueries(designIds);
		
		// Read Study Design Population Assignments mapped to Design
		List<String> studyIds = new List<String>();
		for(Study__c tmpStudy : studiesList) {
			studyIds.add(tmpStudy.Id);
		}
		
		Map<String, List<StudyDesignPopulationAssignment__c>> assignmentsMap = readStDesPopAssignmentsPerDesign(
																											designIds,
																											studyIds);
		
		// Read Epochs, Arms and Flowcharts mapped to Design
		Map<String, List<Epoch__c>>	epochMap = readEpochsPerDesign(designIdsQuery);
		Map<String, List<Arm__c>>	armMap	 = readArmsPerDesign(designIdsQuery);
		
		List<Flowcharts__c> flList = FlowchartDataAccessor.getFlowchartList('ClinicalDesign__c IN ' + designIdsQuery);
		Map<String, List<Flowcharts__c>> flMap = (Map<String,List<Flowcharts__c>>) BDT_Utils.mapSObjectListOnKey(
																				flList,
																				new List<String>{'ClinicalDesign__c'}
																			);
		
		// Read and map ProjectServiceTimePoints to Flowcharts
		List<String> flowchartsIds = new List<String>();
		for(Flowcharts__c tmpFl : flList) {
			flowchartsIds.add(tmpFl.Id);
		}
		String flowchartsIdsQuery = BDT_Utils.buildSetForDynamicQueries(flowchartsIds);
		
		Map<String, List<ProjectServiceTimePoint__c>> pstpMap = readProjectSrvTimePointsPerFlowchart(flowchartsIdsQuery);
		
		// Create Clinical Design Wrapper list
		for(ClinicalDesign__c tmpDes : designList) {
			// Create design display
			List<Epoch__c> tmpEpochList = epochMap.containsKey(tmpDes.Id)? epochMap.remove(tmpDes.Id) : new List<Epoch__c>();
			
			List<Arm__c> tmpArmList = armMap.containsKey(tmpDes.Id)? armMap.remove(tmpDes.Id) : new List<Arm__c>();
			
			List<Flowcharts__c> tmpFlList = flMap.containsKey(tmpDes.Id) ? flMap.remove(tmpDes.Id) : new List<Flowcharts__c>();
			
			ClinicalDesignService.designDisplay tmpDesDisplay = new ClinicalDesignService.designDisplay(
																						tmpDes,
																						tmpEpochList,
																						tmpArmList,
																						tmpFlList);
			
			// Create flowchart display list
			List<FlowchartService.flowchartDisplay> tmpFlowchartDisplayList = new List<FlowchartService.flowchartDisplay>();
			for(Flowcharts__c tmpFl : tmpFlList) {
				List<ProjectServiceTimePoint__c> tmpPstpList = pstpMap.containsKey(tmpFl.Id) ?
																			pstpMap.remove(tmpFl.Id) :
																			new List<ProjectServiceTimePoint__c>();
				
				FlowchartService.flowchartDisplay tmpFlowcharDisplayItem = new FlowchartService.flowchartDisplay(
																					tmpFl,
																					tmpPstpList,
																					financialDocument.Client_Project__c);
				
				tmpFlowchartDisplayList.add(tmpFlowcharDisplayItem);
			}
			
			// Create assignment wrapper list
			List<designStudyAndPopWrapper> desStPopList = new List<designStudyAndPopWrapper>();
			
			for(Study__c tmpStudy : studiesList){
				String key = tmpdes.Id + ':' + tmpStudy.Id;
				
				List<StudyDesignPopulationAssignment__c> tmpAssignList = assignmentsMap.containsKey(key)?
																		assignmentsMap.remove(key) :
																		new List<StudyDesignPopulationAssignment__c>();
				
				if( ! tmpAssignList.isEmpty() ) {
					designStudyAndPopWrapper tmpAssignWrapperItem = new designStudyAndPopWrapper();
					tmpAssignWrapperItem.studyOfDesign  = tmpStudy;
					tmpAssignWrapperItem.assignmentList = tmpAssignList;
				
					desStPopList.add(tmpAssignWrapperItem);
				}
			}
			
			// Clone to total wrapper
			clinicalDesignWrapper wrapperItem = new clinicalDesignWrapper();
			wrapperItem.designDisplayItem	  = tmpDesDisplay;
			wrapperItem.flowchartDisplayList  = tmpFlowchartDisplayList;
			wrapperItem.assignmentWrapperList = desStPopList;
			wrapperItem.createRepeatedTablesInFlDisplay(maxTimePointsPerPage);
			clinicalDesignWr.add(wrapperItem);
		}
	}
	
	
	// The following class keeps track of the Designs
	public class clinicalDesignWrapper {
		public ClinicalDesignService.designDisplay		designDisplayItem			{get;set;}
		public List<designStudyAndPopWrapper>			assignmentWrapperList		{get;set;}
		public List<FlowchartService.flowchartDisplay>	flowchartDisplayList		{get;set;}
		public List<List<String>>						repeatedTablesInFlDisplay	{get;set;}
		
		public clinicalDesignWrapper() {
			assignmentWrapperList	  = new List<designStudyAndPopWrapper>();
			flowchartDisplayList	  = new List<FlowchartService.flowchartDisplay>();
			repeatedTablesInFlDisplay = new List<List<String>>();
		}
		
		public void createRepeatedTablesInFlDisplay(Integer maxPerPage) {
			for(FlowchartService.flowchartDisplay tmpDisplay : flowchartDisplayList) {
				// Calculate iterations of flowchart table
				Decimal param1 = tmpDisplay.TimePointsInFlowchart.size();
				Decimal param2 = maxPerPage;
				Integer cnt = (Integer) Math.ceil( param1 / param2 );
				// Create list that keeps the page iterations info (useful to break the tables in timepoints)
				List<String> tmpList = new List<String>();
				for(Integer i=1; i<=cnt; i++) {
					tmpList.add('(Part ' + i + ' / ' + cnt + ')');
				}
				// Add to the repeatedTablesInFlDisplay
				repeatedTablesInFlDisplay.add(tmpList);
			}
		}
	}
	
	
	// The following class keeps track of the Designs, Studies and Populations assignments
	public class designStudyAndPopWrapper {
		public Study__c									studyOfDesign	{get;set;}
		public List<StudyDesignPopulationAssignment__c> assignmentList	{get;set;}
		
		public designStudyAndPopWrapper() {
			assignmentList = new List<StudyDesignPopulationAssignment__c>();
		}
	}
	
	
	// The following class keeps track of the populations and assigned studies
	public class PopulationToStudiesWrapper {
		public Population__c  	projectPopulation {get;set;}
		public List<String>	  	studyLblList	  {get;set;}
		public List<String>		studyTitleList	  {get;set;}
		public List<String> 	inclCriteria	  {get;set;}
		public List<String> 	exclCriteria	  {get;set;}
		
		public PopulationToStudiesWrapper() {
			studyLblList   = new List<String>();
			studyTitleList = new List<String>();
			inclCriteria   = new List<String>();
			exclCriteria   = new List<String>();
		}
	}
	
	// ********************  ********************
	
	
	public void findStudiesCurrency() {
		Set<Id> studyIds 	 = new Set<Id>();
		List<Study__c> tmpStudyList = new List<Study__c>();
		Map<ID, ProposalStudiesWrapper> tmpWrapperMap = new Map<ID, ProposalStudiesWrapper>();
		ProposalStudiesWrapper tmpWrItem = new ProposalStudiesWrapper();
		// Take the proposal studies, to associate them with the milestones
		for(ProposalStudiesWrapper stWr : studiesListWr) { 
			studyIds.add(stWr.proposalStudy.Id);
			tmpWrapperMap.put(stWr.proposalStudy.Id, stWr);
		}
		// Retrieve the Study Currency
		tmpStudyList = [SELECT 	id,
								Business_Unit__r.Currency__r.Name
						FROM	Study__c
						WHERE	id in :studyIds AND
								Business_Unit__c != null];
		for(Study__c tmpStudy : tmpStudyList) {
			tmpWrItem = new ProposalStudiesWrapper();
			if(tmpWrapperMap.containsKey(tmpStudy.Id) && tmpStudy.Business_Unit__r.Currency__r.Name!=null) {
				tmpWrItem = tmpWrapperMap.get(tmpStudy.Id);
				tmpWrItem.studyCurrency = tmpStudy.Business_Unit__r.Currency__r.Name;
			}
		}
	}
	
	
	public void findAssociationsWithStudies() { 
		Set<Id> studyIds	  = new Set<Id>();
		Set<Id> populationIds  = new Set<Id>(); 
		Set<Id> siteIds		   = new Set<Id>();
		Set<Id> subContrActIds = new Set<Id>();	
		List<PopulationStudyAssignment__c> 			  tmpPopToStudyAssignList  = new List<PopulationStudyAssignment__c>();
		List<StudySiteAssignment__c>				  tmpSiteToStudyAssignList = new List<StudySiteAssignment__c>(); 
		List<SubcontractorActivityStudyAssignment__c> tmpSubContrActAssignList = new List<SubcontractorActivityStudyAssignment__c>();
		List<Population__c>							  populationList	  	   = new List<Population__c>();
		Map<Id, ProposalStudiesWrapper>   tmpMap 	 = new Map<Id, ProposalStudiesWrapper>();
		Map<Id, Population__c>			  tmpPopMap  = new Map<Id, Population__c>();
		Map<Id, ProjectSite__c>			  tmpSiteMap = new Map<Id, ProjectSite__c>();
		Map<Id, ProjectSubcontractorActivity__c> tmpSubContrActMap = new Map<Id, ProjectSubcontractorActivity__c>();
		ProposalStudiesWrapper	 	tmpPrStWrapperItem;
		PopulationToStudiesWrapper	tmpPopToStudiesItem;
		Population__c			 tmpPopItem;
		ProjectSite__c			 tmpSiteItem;
		ProjectSubcontractorActivity__c tmpSubContrItem;
		Map<Id, PopulationToStudiesWrapper> tmpPopToStudiesWrMap = new Map<Id, PopulationToStudiesWrapper>();
		List<ProjectSite__c>	 siteList = new List<ProjectSite__c>();
		List<ProjectSubcontractorActivity__c> subContrActivityList = new List<ProjectSubcontractorActivity__c>();
		// get the populations, the sites and the subcontractors for a particular project
		populationList = BDT_Characteristics.getPopulationList(financialDocument.Client_Project__c, false);
		siteList = [SELECT  id, 
							Name, 
							Site__r.Name, 
							site__r.Business_Unit__c 
					FROM 	ProjectSite__c
					WHERE	client_project__c = :financialDocument.Client_Project__c];
		subContrActivityList = [SELECT 	id, 
										name, 
										SubcontractorActivity__r.Name, 
										SubcontractorActivity__r.Subcontractor__r.Name
								FROM 	ProjectSubcontractorActivity__c 
								WHERE 	ClientProject__c = :financialDocument.Client_Project__c];
		//get list of Population Ids, Site Ids, Subcontractor activities Ids and selected Study Ids for further processing 
		for(Population__c pop:populationList){
			populationIds.add(pop.id);
			tmpPopMap.put(pop.id, pop);
			tmpPopToStudiesItem = new PopulationToStudiesWrapper(); 
			tmpPopToStudiesItem.projectPopulation = pop;
			tmpPopToStudiesWrMap.put(pop.id, tmpPopToStudiesItem);
		}
		for(ProjectSite__c tmpSite : siteList) {
			siteIds.add(tmpSite.id);
			tmpSiteMap.put(tmpSite.id, tmpSite);
		}
		for(ProjectSubcontractorActivity__c tmpSubContrAct : subContrActivityList ) {
			subContrActIds.add(tmpSubContrAct.id);
			tmpSubContrActMap.put(tmpSubContrAct.id, tmpSubContrAct);
		}
		for(ProposalStudiesWrapper stWr : studiesListWr) { 
			studyIds.add(stWr.proposalStudy.Id);
			tmpMap.put(stWr.proposalStudy.Id, stWr);
		}
		// Get the populations to studies associations
		tmpPopToStudyAssignList = [SELECT 	id,
											Study__c, 
											Study__r.Code__c,
											Study__r.Title__c,
											Population__c,
											AggregateNumberOfSubjects__c 
								   FROM 	PopulationStudyAssignment__c 
								   WHERE 	Population__c in : populationIds AND
								   			Study__c in : studyIds];
		// Add the populations-studies associations to the ProposalStudiesWrapper and to the PopulationToStudiesWrapper
		for(PopulationStudyAssignment__c tmpItem : tmpPopToStudyAssignList)	{
			if( tmpMap.containsKey(tmpItem.Study__c) && tmpPopMap.containsKey(tmpItem.Population__c)) {
				// Studies Wrapper
				tmpPrStWrapperItem = new ProposalStudiesWrapper();
				tmpPrStWrapperItem = tmpMap.get(tmpItem.Study__c);
				tmpPopItem		   = new Population__c();
				tmpPopItem		   = tmpPopMap.get(tmpItem.Population__c);
				tmpPrStWrapperItem.popToStudyList.add(tmpPopItem);
				tmpPrStWrapperItem.popToStudyNumList.add(String.valueOf(tmpItem.AggregateNumberOfSubjects__c) + ' ' + tmpPopItem.Name); 
				// Population Wrapper 
				tmpPopToStudiesItem = new PopulationToStudiesWrapper();
				tmpPopToStudiesItem = tmpPopToStudiesWrMap.get(tmpItem.Population__c); // This map contains the same keys with the tmpPopMap 
				tmpPopToStudiesItem.StudyLblList.add( tmpItem.Study__r.Code__c.substringAfterLast('-') );
				tmpPopToStudiesItem.StudyTitleList.add(tmpItem.Study__r.Title__c);
			}
		}
		for(PopulationToStudiesWrapper tmpItem : tmpPopToStudiesWrMap.values()) {
			if( ! tmpItem.studyLblList.isEmpty() ) {
				popToStudiesWr.add(tmpItem);
			}
		}
		addCriteriaToPopulationToStudiesWrapper(popToStudiesWr); 
		// Get the sites to studies associations
		tmpSiteToStudyAssignList = [SELECT 	id, 
											ProjectSite__c,
											Study__c
									FROM 	StudySiteAssignment__c 
									WHERE 	ProjectSite__c in : siteIds AND
								   			Study__c in : studyIds];
		// Add the sites-studies associations to the ProposalStudiesWrapper
		for(StudySiteAssignment__c tmpItem : tmpSiteToStudyAssignList)	{
			if( tmpMap.containsKey(tmpItem.Study__c) && tmpSiteMap.containsKey(tmpItem.ProjectSite__c)) {
				tmpPrStWrapperItem = new ProposalStudiesWrapper();
				tmpPrStWrapperItem = tmpMap.get(tmpItem.Study__c);
				tmpSiteItem		   = new ProjectSite__c();
				tmpSiteItem		   = tmpSiteMap.get(tmpItem.ProjectSite__c);
				tmpPrStWrapperItem.siteToStudyList.add(tmpSiteItem);
			}
		}
		// Get the subcontractors to studies associations
		tmpSubContrActAssignList = [SELECT 	id, 
											ProjectSubcontractorActivity__c,
											Study__c
									FROM 	SubcontractorActivityStudyAssignment__c 
									WHERE 	ProjectSubcontractorActivity__c in : subContrActIds AND
								   			Study__c in : studyIds];
		// Add the sites-studies associations to the ProposalStudiesWrapper
		for(SubcontractorActivityStudyAssignment__c tmpItem : tmpSubContrActAssignList)	{
			if( tmpMap.containsKey(tmpItem.Study__c) && tmpSubContrActMap.containsKey(tmpItem.ProjectSubcontractorActivity__c)) {
				tmpPrStWrapperItem = new ProposalStudiesWrapper();
				tmpPrStWrapperItem = tmpMap.get(tmpItem.Study__c);
				tmpSubContrItem	   = new ProjectSubcontractorActivity__c();
				tmpSubContrItem	   = tmpSubContrActMap.get(tmpItem.ProjectSubcontractorActivity__c);
				tmpPrStWrapperItem.subContrActToStudyList.add(tmpSubContrItem);
			}
		}
	}
	
	
	public void addCriteriaToPopulationToStudiesWrapper(List<PopulationToStudiesWrapper> wrList) { 
		List<Criteria__c> allCriteriaList = new List<Criteria__c>();
		Map<Id, PopulationToStudiesWrapper> popToStudiesMap = new Map<Id, PopulationToStudiesWrapper>(); 
		PopulationToStudiesWrapper tmpWrItem = new PopulationToStudiesWrapper();
		// Read all the criteria
		allCriteriaList = [SELECT Id,
								  Populations__c,  
								  Inclusive__c, 
								  Exclusive__c, 
								  CriteriaDescription__c 
						   FROM	  Criteria__c 
						   WHERE  BDTDeleted__c = false];
		// Create a map for PopulationToStudiesWrapper and associate the criteria with populations
		for(PopulationToStudiesWrapper tmpItem : wrList) {
			popToStudiesMap.put(tmpItem.projectPopulation.id, tmpItem);
		}
		for(Criteria__c tmpItem : allCriteriaList) {
			tmpWrItem = new PopulationToStudiesWrapper();
			if( popToStudiesMap.containsKey(tmpItem.Populations__c) ) {
				tmpWrItem = popToStudiesMap.get(tmpItem.Populations__c);
				if(tmpItem.Inclusive__c) {
					tmpWrItem.inclCriteria.add(tmpItem.CriteriaDescription__c);
				} else if(tmpItem.Exclusive__c) {
					tmpWrItem.exclCriteria.add(tmpItem.CriteriaDescription__c);
				} 
			}
		}
	}
	
	
	
	// The following class keeps track of the studies and associated info
	public class ProposalStudiesWrapper { // probably I will change the lists of objects with just List of strings  
		public Study__c				proposalStudy 		{get;set;}
		public String				studyCurrency		{get;set;} 
		public List<Population__c> 	popToStudyList		{get;set;}
		public List<String>			popToStudyNumList 	{get;set;} 
		public List<ProjectSite__c> siteToStudyList 	{get;set;}
		public List<ProjectSubcontractorActivity__c> subContrActToStudyList {get;set;} 
		public Boolean				AnStudyFlag			{get;set;}
		public Boolean				MdStudyFlag			{get;set;}
		public Boolean				ValStudyFlag		{get;set;}
		// The constructor of the wrapper class
		public ProposalStudiesWrapper() {
			popToStudyList  	   = new List<Population__c>();
			popToStudyNumList	   = new List<String>(); 
			siteToStudyList 	   = new List<ProjectSite__c>();
			subContrActToStudyList = new List<ProjectSubcontractorActivity__c>();
		}
		// Retrieve the label for study type
		public String getStudyType() {
			String result = '';
			result += (AnStudyFlag)?  'AN'	  : '';
			result += (MdStudyFlag)?  ', MD'  : '';
			result += (ValStudyFlag)? ', VAL' : '';
			result = result.removeStart(', ');
			return result;
		}
	}
}