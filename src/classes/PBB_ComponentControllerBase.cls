public with sharing virtual class PBB_ComponentControllerBase {

  public PBB_LayoutModelController layoutModelController { get; 
    set {
      if (value != null) {
	layoutModelController = value;
	layoutModelController.setComponentController(this);
      }
    }
  }
}