@isTest
private class PBB_SRMModelControllerTest {
	private static PBB_SRMModelController cont;

	@isTest
	static void SRMModelControllerTest() {
		Test.startTest();
    		cont = new PBB_SRMModelController();
    	Test.stopTest();
    	System.assert(cont.SRMModelsList.size() == 0);
	}

	@isTest
	static void getNewPageReferenceTest() {
		cont = new PBB_SRMModelController();
		Test.startTest();
    		PageReference pr = cont.getNewPageReference();
    	Test.stopTest();
    	Schema.DescribeSObjectResult  r = SRM_Model__c.SObjectType.getDescribe();
    	System.assertEquals(pr.getUrl(), '/'+r.getKeyPrefix()+'/e');
	}
}