/**
* A wrapper around Critical_Chain__c SObject and a container for different dates.
*/
public class EWChain
{
	public Datetime actualStart;
	public Datetime actualEnd
	{
		get
		{
			return (steps != null && !steps.isEmpty() ? steps.get(steps.size() - 1).actualEnd : null);
		}
	}
	public Datetime challengingStart;//
	public Datetime challengingEnd;//
	public Datetime expectedStart;//
	public Datetime expectedEnd;//
	public Datetime plannedStart;//
	public Datetime plannedEnd;//
	public Decimal gap = 0;
	public Decimal internalGap = 0;
	public Datetime bufferEnd;// {get { return new EWDatetime(bufferStart).addWorkdays(originalBuffer); } }
	//public Datetime bufferStart {get { return new EWDatetime(challengingEnd).addWorkdays(2/*next working day*/ + gap); } }
	public Datetime bufferStart;// {get { return new EWDatetime(challengingEnd).addWorkdays(2/*next working day*/ + gap); } }
	
	//public Decimal originalChallenging { get { return chainRecord.Aggregated_Challenging_Time__c; } }
	//public Decimal remainingChallenging;
	
	public Decimal originalBuffer { get { return chainRecord.Aggregated_Buffer_Time__c; } }
	//public Decimal consumedBuffer { get { return (bufferStart > expectedEnd ? 0 : new EWDatetime(bufferStart).workdaysBetween(expectedEnd)); } }
	//public Decimal remainingBuffer { get { return originalBuffer - consumedBuffer; } }
	
	//public Decimal remainingTime = 0;
	//public Decimal originalTime { get {return new EWDatetime(challengingStart).workdaysBetween(bufferStart);} }
	//public Datetime remainingStart;
	
	public Critical_Chain__c chainRecord = new Critical_Chain__c();
	
	public String id { get { return chainRecord.Id; } }
	
	public List<EWStep> steps = new List<EWStep>();
	
	public EWChain(Critical_Chain__c chainRecord)
	{
		this.chainRecord = chainRecord;
	}
	
	public Decimal calculateBufferIndex()
	{
		try
		{
			Decimal originalBuffer = this.originalBuffer;
			Decimal remainingBuffer = Math.min(this.originalBuffer, new EWDatetime(expectedEnd.addDays(1)).workdaysBetween(bufferEnd.addDays(1)));
			Decimal originalTime = new EWDatetime(actualStart != null && actualStart < challengingStart ? actualStart : challengingStart).workdaysBetween(bufferStart);
			Decimal remainingTime = new EWDatetime(actualStart != null || System.today() > challengingStart ? System.today() : challengingStart).workdaysBetween(expectedEnd.addDays(1));
			
			Decimal challengingTime = new EWDatetime(challengingStart).workdaysBetween(challengingEnd.addDays(1));
			originalTime = Math.max(originalTime, challengingTime);
			
			//System.debug(System.LoggingLevel.ERROR, 'Original buffer: ' + originalBuffer + ' remaining buffer: ' + remainingBuffer + ' original time: ' + originalTime + ' remaining time' + remainingTime);//@TODO: Delete this line!
			
			return ((remainingBuffer/originalBuffer)/(remainingTime/originalTime)).setScale(2);
		}
		catch (Exception e)
		{
			System.debug(System.LoggingLevel.ERROR, 'Exception caught: ' + e.getMessage() + '\n' + e.getStackTraceString());
		}
		
		return 0;
	}
	
	public Decimal calculateBufferIndex(Datetime milestoneDate)
	{
		Datetime bufferEnd = this.bufferEnd;
		Datetime bufferStart = this.bufferStart;
		
		applyMilestoneDate(milestoneDate);
		/*
		this.bufferEnd = milestoneDate;
		this.bufferStart = new EWDatetime(this.bufferStart).addWorkdays(new EWDatetime(this.bufferEnd).workdaysBetween(milestoneDate));
		/**/
		
		Decimal bi = calculateBufferIndex();
		
		this.bufferEnd = bufferEnd;
		this.bufferStart = bufferStart;
		
		return bi;
	}
	
	public void applyMilestoneDate(Datetime milestoneDate)
	{
		bufferEnd = milestoneDate;
		bufferStart = new EWDatetime(bufferEnd).addWorkdays(-originalBuffer);
	}
	
	public Datetime calculateMilestoneDate(Decimal bufferIndex)
	{
		Decimal bi = calculateBufferIndex();
		Datetime milestoneDate = bufferEnd;
		
		Integer direction = (bi < bufferIndex ? 1 : -1);
		for (Integer i = 0; bi != bufferIndex && originalBuffer != 0 && expectedEnd != null; i++)
		{
			if (direction == 1 && bi >= bufferIndex
					|| direction == -1 && bi <= bufferIndex)
			{
				break;
			}
			
			milestoneDate = bufferEnd.addDays(i * direction);
			bi = calculateBufferIndex(milestoneDate);
		}
		
		return milestoneDate.addSeconds(-1 * Userinfo.getTimezone().getOffset(milestoneDate) / 1000);
	}
	
	public Map<String, Object> toDTO()
	{
		Map<String, Object> result = (Map<String, Object>) System.Json.deserializeUntyped(System.Json.serialize(chainRecord));
		result.putAll(new Map<String, Object>{
			'Buffer_Index__c' => calculateBufferIndex()
		});
		
		return result;
	}
	
	public void recalculateEWData()
	{
		internalGap = 0;
		actualStart = null;
		Datetime expectedEnd;
		Decimal bufferShift = 0;//used for challenging dates calculation
		for (Integer i = 0, n = steps.size(); i < n; i++)
		{
			EWStep step = steps.get(i);
			step.index = i;
			//set step challenging dates
			step.setChallengingDates(bufferShift);
			bufferShift += step.bufferTime;
			
			//set chain challenging dates
			if (challengingStart == null)
			{
				challengingStart = step.challengingStart;
			}
			
			if (challengingEnd == null || step.challengingEnd > challengingEnd)
			{
				challengingEnd = step.challengingEnd;
			}
			
			//set expected dates
			if (expectedEnd == null)
			{
				expectedEnd = (step.actualStart != null || System.today() > step.challengingStart ? System.today() : step.challengingStart);
			}
			
			if (expectedEnd != null)
			{
				step.setExpectedDates(expectedEnd);
			}
			
			if (step.actualStart != null && actualStart == null)
			{
				actualStart = step.actualStart;
			}
			
			if (step.expectedEnd != null)
			{
				expectedEnd = new EWDatetime(step.expectedEnd.addDays(1)).addWorkdays(1);//(step.expectedEnd != null ? step.expectedEnd : expectedEnd);
				this.expectedEnd = step.expectedEnd;
			}
			
			if (step.expectedStart != null && expectedStart == null)
			{
				expectedStart = step.expectedStart;
			}
			
			
			//set planned dates
			if (plannedStart == null)
			{
				plannedStart = step.plannedStart;
			}
			
			plannedEnd = step.plannedEnd;
		}
		
		//set challenging dates in case of empty chains
		if (challengingStart == null)
		{
			challengingStart = System.now();
		}
		
		if (challengingEnd == null)
		{
			challengingEnd = System.now();
		}
		
		//set buffer dates
		bufferStart = new EWDatetime(challengingEnd).addWorkdays(2);
		bufferEnd = new EWDatetime(bufferStart).addWorkdays(originalBuffer);
	}
	
	public static List<EWChain> buildEWChains(List<ID> chainIDs)
	{
		Map<ID, Critical_Chain__c> chainMap = new Map<ID, Critical_Chain__c>();
		Map<String, EWChain> ewMap = new Map<String, EWChain>();
		Map<ID, List<Critical_Chain_Step_Junction__c>> chainJunctionsByStepIdMap = new Map<ID, List<Critical_Chain_Step_Junction__c>>();
		
		for (Critical_Chain__c eachChain : [select PAWS_Project_Flow__c, PAWS_Project_Flow_Site__c, Aggregated_Buffer_Time__c, Aggregated_Challenging_Time__c, Name, Flow__c, Milestone__c, Milestone__r.Name, Cloned_From_Early_Warning__r.Name,
				(select Aggregated_Milestone__r.Milestone_Target_Date__c, Aggregated_Milestone__r.Name from Aggregated_Milestone_EW_Junctons__r) from Critical_Chain__c where Id in :chainIDs])
		{
			chainMap.put(eachChain.Id, eachChain);
			ewMap.put(eachChain.Id, new EWChain(eachChain));
		}
		
		List<Critical_Chain_Step_Junction__c> stepJunctions = new List<Critical_Chain_Step_Junction__c>();
		for (Critical_Chain_Step_Junction__c stepJunction : [select Flow_Step__c, Duration__c, Buffer_Time__c, 
				Buffer_Percentage__c, Challenging_Time__c, Challenging_Percentage__c,
				Critical_Chain__c, Step_Name__c, Used_Time__c, Remaining_Time__c, Comment__c
				//,Flow_Step__r.STSWR1__Planned_Start_Date__c, Flow_Step__r.STSWR1__Revised_Start_Date__c
				from Critical_Chain_Step_Junction__c
				where Critical_Chain__c in :chainIDs
				order by Flow_Step__r.STSWR1__Revised_Start_Date__c, Flow_Step__r.STSWR1__Planned_Start_Date__c, Flow_Step__r.STSWR1__Index__c])
		{
			stepJunctions.add(stepJunction);
			if (chainJunctionsByStepIdMap.get(stepJunction.Flow_Step__c) == null)
			{
				chainJunctionsByStepIdMap.put(stepJunction.Flow_Step__c, new List<Critical_Chain_Step_Junction__c>());
			}
			
			chainJunctionsByStepIdMap.get(stepJunction.Flow_Step__c).add(stepJunction);
			//chainMap.put(stepJunction.Critical_Chain__c, stepJunction.Critical_Chain__r);
			
			EWChain ew = ewMap.get(stepJunction.Critical_Chain__c) == null ? new EWChain(chainMap.get(stepJunction.Critical_Chain__c)) : ewMap.get(stepJunction.Critical_Chain__c);
			ewMap.put(stepJunction.Critical_Chain__c, ew);
			ew.steps.add(new EWStep(new STSWR1__Flow_Step_Junction__c(STSWR1__Duration_Planned__c = 0), new STSWR1__Gantt_Step_Property__c(STSWR1__Planned_Start_Date__c = System.today(), STSWR1__Planned_End_Date__c = System.today()), stepJunction));
		}
		
		Map<ID, STSWR1__Flow_Step_Junction__c> stepMap = new Map<ID, STSWR1__Flow_Step_Junction__c>([select Id, Name,
			(select STSWR1__Actual_Start_Date__c, STSWR1__Actual_Start_Date_Value__c,
				STSWR1__Actual_Complete_Date__c, STSWR1__Actual_Complete_Date_Value__c,
				STSWR1__Step__r.STSWR1__Flow_Branch__c, STSWR1__Cursor__r.STSWR1__Flow_Branch__c,
				STSWR1__Step__r.STSWR1__Is_System_Value__c
				from STSWR1__Flow_Instance_History__r
				where STSWR1__Obsolete__c != true order by CreatedDate, Name)
			//order by CreatedDate desc limit 1)
			from STSWR1__Flow_Step_Junction__c where Id in :chainJunctionsByStepIdMap.keyset()]);
		
		Map<ID, STSWR1__Gantt_Step_Property__c> ganttMap = new Map<ID, STSWR1__Gantt_Step_Property__c>();
		for (STSWR1__Gantt_Step_Property__c each : [select STSWR1__Step__c from STSWR1__Gantt_Step_Property__c where STSWR1__Step__c in :stepMap.keyset() order by STSWR1__Cursor__c nulls first, CreatedDate asc])
		{
			ganttMap.put(each.STSWR1__Step__c, each);
		}
		
		List<ID> propertiesIDs = new List<ID>();
		for (STSWR1__Gantt_Step_Property__c each : ganttMap.values())
		{
			propertiesIDs.add(each.Id);
			for (Critical_Chain_Step_Junction__c ccJunction : chainJunctionsByStepIdMap.get(each.STSWR1__Step__c))
			{
				if (ewMap.containsKey(ccJunction.Critical_Chain__c))
				{
					ewMap.remove(ccJunction.Critical_Chain__c);
				}
			}
		}
		
		for (STSWR1__Gantt_Step_Property__c ganttProperty : [select Id, STSWR1__Step__c, Name, STSWR1__Step__r.Name, STSWR1__Flow__c, STSWR1__Revised_Duration__c, STSWR1__Planned_Start_Date__c, STSWR1__Planned_End_Date__c, STSWR1__Revised_Start_Date__c,
				STSWR1__Revised_End_Date__c, STSWR1__Step__r.STSWR1__Duration_Planned__c from STSWR1__Gantt_Step_Property__c where Id in :propertiesIDs order by STSWR1__Planned_Start_Date__c, STSWR1__Step__r.STSWR1__Index__c])
		{
			for (Critical_Chain_Step_Junction__c stepJunction : chainJunctionsByStepIdMap.get(ganttProperty.STSWR1__Step__c))
			{
				STSWR1__Flow_Step_Junction__c step = stepMap.get(ganttProperty.STSWR1__Step__c);
				Critical_Chain__c chain = chainMap.get(stepJunction.Critical_Chain__c);
				
				EWChain ew = (ewMap.get(stepJunction.Critical_Chain__c) == null ? new EWChain(chain) : ewMap.get(stepJunction.Critical_Chain__c));
				ewMap.put(stepJunction.Critical_Chain__c, ew);
				
				ew.steps.add(new EWStep(step, ganttProperty, stepJunction));
			}
		}
		
		//new EW calculations
		for (EWChain eachEWChain : ewMap.values())
		{
			eachEWChain.recalculateEWData();
		}
		
		return ewMap.values();
	}
	
	public Critical_Chain__c toRecord()
	{
		return new Critical_Chain__c(
			Id = chainRecord.Id,
			Buffer_Index__c = calculateBufferIndex(),
			Challenging_End_Date__c = (challengingEnd != null ? challengingEnd.date() : null),
			Expected_End_Date__c = (expectedEnd != null ? expectedEnd.date() : null),
			Recalculation_Time__c = System.now(),

			BI_0_Date__c = calculateMilestoneDate(0).date(),
			BI_0_5_Date__c = calculateMilestoneDate(0.5).date(),
			BI_1_Date__c = calculateMilestoneDate(1).date(),

			Actual_Complete_Date__c = this.actualEnd,
			Expected_Date_BI_0__c = calculateMilestoneDate(0),
			Expected_Date_BI_05__c = calculateMilestoneDate(0.5),
			Expected_Date_BI_1__c = calculateMilestoneDate(1)
		);
	}
}