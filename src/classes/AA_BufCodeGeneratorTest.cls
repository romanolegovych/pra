/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class AA_BufCodeGeneratorTest{

    static testMethod void getbufcodeDetailsTest() {
        initTestData ();
        // TO DO: implement unit test
        AA_BufCodeGenerator controller = new AA_BufCodeGenerator();
        List<BUF_Code__c> buList = controller.getbufcodeDetails();
        
        System.assertEquals(buList.size(), 0);
    }
    static testMethod void getBuselectoptionTest() {
        initTestData ();
        // TO DO: implement unit test
        AA_BufCodeGenerator controller = new AA_BufCodeGenerator();
        List<SelectOption> buList = controller.getBuselectoption();
        
        System.assertNotEquals(buList.size(), 0);
    }
    
     static testMethod void getfcselectoptionTest() {
        initTestData ();
        // TO DO: implement unit test
        AA_BufCodeGenerator controller = new AA_BufCodeGenerator();
        List<SelectOption> buList = controller.getfcselectoption();
        
        System.assertNotEquals(buList.size(), 0);
    }
    
    static testMethod void saveTest() {
        initTestData ();
        // TO DO: implement unit test
        AA_BufCodeGenerator controller = new AA_BufCodeGenerator();
        controller.selectedF = new List<String> { getFCode().Id };
        controller.selectedBU = getBuCode().Id;
        PageReference pageRef = controller.save();
        
        System.assertEquals(pageRef, null);
    }
    
    static testMethod void generateTest() {
        initTestData ();
        // TO DO: implement unit test
        AA_BufCodeGenerator controller = new AA_BufCodeGenerator();
        controller.selectedF = new List<String> {  getFCode().Id  };
        controller.selectedBU = getBuCode().Id;
        PageReference pageRef = controller.generate();
        
        System.assertEquals(pageRef, null);
    }
    
    
    /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }   
        /* Create test data using test utils */
    static Business_Unit__c getBuCode(){
        AA_TestUtils testUtil = new AA_TestUtils();
        return testUtil.getBUCode();
    }   
        /* Create test data using test utils */
    static Function_Code__c getFCode(){
        AA_TestUtils testUtil = new AA_TestUtils();
        return testUtil.getFCode();
    }   
}