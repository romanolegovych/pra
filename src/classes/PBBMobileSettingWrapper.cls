public with sharing class PBBMobileSettingWrapper {
	public String 				text 						{get;set;}
	public String 				pickListText				{get;set;}
	public String 				type 						{get;set;}
	public List<String> 		picklistValues 				{get;set;}
	public List<SelectOption> 	picklistValueSelectOption 	{get;set;}
	public Boolean 				checkboxValue 				{get;set;}
	public String 				pickListValue 				{get;set;}

	public Id 					settingId 					{get;set;}
	public Id 					childSettingId 				{get;set;}

	public Id 					settingValueId 				{get;set;}
	public Id 					childSettingValueId 		{get;set;}

	public PBBMobileSettingWrapper() {
		
	}
}