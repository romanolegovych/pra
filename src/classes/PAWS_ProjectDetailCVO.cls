/*
* Tests are in PAWS_APITests.cls
*/
global without sharing class PAWS_ProjectDetailCVO
{
    public ID RecordID { get; private set; }
    
    public String ProjectID { get; private set; }
    public String ProtocolID { get; private set; }
    public String ProtocolRecordID { get; private set; }
    public String ProtocolUniqueKey { get; private set; }
    public String ScenarioID { get; private set; }
    
    public String Indication { get; private set; }
    public String TherapeuticArea { get; private set; }
    public ID ProjectManagerUserID { get; private set; }
    public String ProjectManager { get; private set; }
    public String ProjectManagerEmail { get; private set; }
    public String Compound { get; private set; }
    public String Sponsor { get; private set; }
    public String StudyStartUpStatus { get; private set; }

    public String StudyStartUpGoalStatus { get; private set; }
    public String StudyStartUpStatusEstimated { get; private set; }

    public String PatientEnrollmentStatus { get; set; }
    public String PatientEnrollmentGoalStatus { get; set; }

    public String PatientRetentionStatus { get; private set; }
    public String DataCleanupStatus { get; private set; }
    public String AnalysisAndReportingStatus { get; private set; }
    
    public Integer ActivationGoal { get; private set; }
    public Integer PatientEnrollmentGoal { get; private set; }

    public Decimal NumberOfSitesCompleted { get; private set; }
    /* ======== based on Study_Start_Up_Aggregated_BI__c ======== */
    public Decimal NumberOfSitesOnTrackEstimated { get; private set; }
    public Decimal NumberOfSitesDelayedEstimated { get; private set; }
    public Decimal NumberOfSitesAtRiskEstimated { get; private set; }
    /* ======== END based on Study_Start_Up_Aggregated_BI__c ======== */

    public Datetime ActivationTargetDate
    {
        get;
        /*{
            return ActivationTargetDate != null
                ? DateTime.newInstanceGMT(ActivationTargetDate.date(), ActivationTargetDate.time())
                : ActivationTargetDate;
        }*/

        private set
        {
            ActivationTargetDate = value != null
                ? DateTime.newInstanceGMT(value.date(), value.time())
                : null;
        }
    }

    public List<PAWS_ProjectSiteCVO> Sites { get; private set; }
    public List<PAWS_ProjectCountryCVO> Countries { get; private set; }
    private transient Map<ID, PAWS_ProjectCountryCVO> CountriesMap;

    //public PBB_WR_APIs.PatientEnrollmentwrapper PatientEnrollmentItem { get; private set; }

    public PAWS_ProjectDetailCVO()
    {
        Countries = new List<PAWS_ProjectCountryCVO>();
        CountriesMap = new Map<ID, PAWS_ProjectCountryCVO>();
        Sites = new List<PAWS_ProjectSiteCVO>();
        StudyStartUpGoalStatus = 'Not Started';
    }

    public PAWS_ProjectDetailCVO(ecrf__c inEcrfProject)
    { 
        this();

        RecordID = inEcrfProject.ID;
        ProjectID = inEcrfProject.Project_ID__r.Name;
        ProtocolID = inEcrfProject.Protocol_ID__r.Name;
        ProtocolRecordID = inEcrfProject.Protocol_ID__c;
        ProtocolUniqueKey = inEcrfProject.Protocol_ID__r.Protocal_Unique_Key__c;
        TherapeuticArea = inEcrfProject.Therapeutic_Area__c;
        ProjectManagerUserID = inEcrfProject.Project_Manager__c;
        ProjectManager = inEcrfProject.Project_Manager__r.Name;
        ProjectManagerEmail = inEcrfProject.Project_Manager__r.Email;
        Compound = inEcrfProject.Compound__c;
        Sponsor = inEcrfProject.Sponsor__c;
        Indication = inEcrfProject.Indication__c;

        NumberOfSitesAtRiskEstimated = inEcrfProject.Number_Of_Sites_At_Risk_Aggregated__c;
        NumberOfSitesDelayedEstimated = inEcrfProject.Number_Of_Sites_Delayed_Aggregated__c;
        NumberOfSitesOnTrackEstimated = inEcrfProject.Number_Of_Sites_On_Track_Aggregated__c;
        NumberOfSitesCompleted = inEcrfProject.Number_Of_Sites_Completed__c;

        StudyStartUpStatus = inEcrfProject.Study_Start_Up_Status__c;

        for(PAWS_Project_Flow_Country__c eachCountry : inEcrfProject.PAWS_Countries__r)
        {
            PAWS_ProjectCountryCVO eachountryCVO = new PAWS_ProjectCountryCVO(eachCountry);
            Countries.add(eachountryCVO);
            CountriesMap.put(eachCountry.ID, eachountryCVO);
        }
    }

    public void addSites(List<PAWS_Project_Flow_Site__c> inSiteList)
    {
        for(PAWS_Project_Flow_Site__c eachProjectSite : inSiteList)
        {
            PAWS_ProjectCountryCVO projectCountry = CountriesMap.get(eachProjectSite.PAWS_Country__c);
            if(projectCountry != null)
            {   
                Sites.add(
                    projectCountry.addSite(eachProjectSite)
                );
            }
        }
    }

    public void updateActivationGoal(EW_Aggregated_Milestone__c milestone)
    {
        if(milestone != null)
        {
            ActivationGoal = Integer.valueOf(milestone.Goal__c);
            StudyStartUpGoalStatus = StudyStartUpStatusEstimated = getStudyStartUpGoalStatus();
        }
    }

    public void updateActivationTargetDate(EW_Aggregated_Milestone__c milestone)
    {
        if(milestone != null)
        {
            ActivationTargetDate = milestone.Milestone_Target_Date__c;
        }
    }

    public void updateScenarioID(PBB_Scenario__c scenario)
    {
        if (scenario != null)
        {
            ScenarioID = scenario.Name;
        }
    }

    /*
    public void setPatientEnrollmentItem(PBB_WR_APIs.PatientEnrollmentwrapper inPatientEnrollmentItem)
    {
        PatientEnrollmentItem = inPatientEnrollmentItem;
    }
    */

    public String getStudyStartUpGoalStatus()
    {
        String status;

        if(Sites.size() == 0 || ActivationGoal == null || ActivationGoal == 0)
        {
            status = 'Not Started';
        }
        else if(NumberOfSitesCompleted >= ActivationGoal)
        {
            status = 'Complated';
        }
        else if(NumberOfSitesOnTrackEstimated / ActivationGoal >= 0.8)
        {
            status = 'On Track';
        }
        else if((NumberOfSitesAtRiskEstimated + NumberOfSitesOnTrackEstimated) / ActivationGoal >= 0.8)
        {
            status = 'At Risk';
        }
        else if(NumberOfSitesDelayedEstimated / ActivationGoal >= 0.2)
        {
            status = 'Delayed';
        }

        return status;
    }
}