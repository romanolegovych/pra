@isTest
private class PBB_RegionModelsControllerTest {
	private static PBB_RegionModelsController cont;
	
	@isTest
	static void RegionModelsControllerTest() {
		Test.startTest();
    		cont = new PBB_RegionModelsController();
    	Test.stopTest();
    	System.assert(cont.Region_Models.size() == 0);
	}

	@isTest
	static void getNewPageReferenceTest() {
		cont = new PBB_RegionModelsController();
		Test.startTest();
    		PageReference pr = cont.getNewPageReference();
    	Test.stopTest();
    	Schema.DescribeSObjectResult  r = Mapping_Region_Model__c.SObjectType.getDescribe();
    	System.assertEquals(pr.getUrl(), '/'+r.getKeyPrefix()+'/e');
	}
}