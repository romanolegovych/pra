public with sharing class BDT_FinancialDocOverviewController {
	// Global Variables
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Financial Documents
	private String						ProjectID;
	private Boolean						availableStudiesFlag;
	public 	Boolean 					ShowAddEditProp			{get;set;}
	public	Boolean						ProjectOkFlag			{get;set;}
	public  Boolean						showInvalidFlag			{get;set;}
	public	Boolean						AddProp					{get;set;}
	public  List<FinancialDocument__c> 	financialDocList		{get;set;}
	public  List<SelectOption> 			docTypesList			{get;set;}
	public  List<SelectOption>			acceptedPropOptionList	{get;set;}
	public  FinancialDocument__c 	  	financialDocument		{get;set;}
	public  String 						financialDocumentId		{get;set;}
	public  Boolean						showProjectVersionPopup {get;set;}
	
	
	/**
	*	The constructor of the class.
	*	@author   Dimitrios Sgourdos
	*	@version  09-Sep-2013
	*/
	public BDT_FinancialDocOverviewController() {
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Financial Documents');
		if(initializeGlobalVariables()) {
			readFinancialDocuments(); 
		}
	}
	
	
	/**
	*	Initialize the global variables of the class.
	*	@author   Dimitrios Sgourdos
	*	@version  10-Sep-2013
	*	@return   If everything went well
	*/
	private Boolean initializeGlobalVariables() {
		//showProjectVersionPopup by default should be false, only after save should be true
		showProjectVersionPopup = false;
		financialDocument = new FinancialDocument__c();
		// Document types list
		docTypesList = BDT_DC_FinancialDocument.createDocumentTypesSelectionList();
		// Boolean for handling the different sections of the page
		ShowAddEditProp = false;
		showInvalidFlag = false;
		// Project Id and availability of studies
		List<Study__c> StudyList = new List<Study__c>();
		ProjectID = BDT_Utils.getPreference('SelectedProject');
		if(String.isBlank(ProjectID)) {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Unable to find selected project. Please select a project and reload the page.');
			ApexPages.addMessage(msg);
			ProjectOkFlag = false;
			return false;
		}
		ProjectOkFlag = true;
		StudyList = BDT_DC_FinancialDocument.readProjectStudies(projectID);
		availableStudiesFlag = ! StudyList.isEmpty();
		// Accepted proposals picklist
		acceptedPropOptionList = BDT_DC_FinancialDocument.createAcceptedProposalsSelectionList(projectID);
		return true;
	}
	
	
	/**
	*	Read the proposals (with the change order documents) of the selected project
	*	@author   Dimitrios Sgourdos
	*	@version  09-Sep-2013
	*	@return   If everything went well
	*/
	public void readFinancialDocuments() {
		if(showInvalidFlag) {
			financialDocList = BDT_DC_FinancialDocument.readProjectFinancialDocuments(projectID);
		} else {
			financialDocList = BDT_DC_FinancialDocument.readProjectValidProposals(projectID);
		}
		financialDocList = sortFD(financialDocList);
	}
	
	
	/**
	*	Call the section for adding new financial document
	*	@author   Dimitrios Sgourdos
	*	@version  10-Sep-2013
	*/
	public void addDocument() {
		showProjectVersionPopup = false;
		ShowAddEditProp = true;
		AddProp			= true;
		// Create the new financial document, with the predefined attributes values
		String tmpStr = ( availableStudiesFlag )?
							FinancialDocumentDataAccessor.DOCUMENT_STATUS_NEW :
							FinancialDocumentDataAccessor.DOCUMENT_STATUS_UNDEFINED;
		
		financialDocument = BDT_DC_FinancialDocument.createNewFinancialDocument(ProjectID, tmpStr);
	}
	
	
	/**
	*	Call the section for editing a financial document
	*	@author   Dimitrios Sgourdos
	*	@version  10-Sep-2013
	*/
	public void editDocument() {
		showProjectVersionPopup = false;
		financialDocument = BDT_DC_FinancialDocument.findFinancialDocument(financialDocumentId);
		if( String.isBlank(financialDocument.Name) ) {
			addDocument();
		} else {
			ShowAddEditProp = true;
			AddProp			= false;
		}
	}
	
	
	/**	Save the new or edited financial document
	 * @author   Dimitrios Sgourdos
	 * @version  13-Jan-2014
	*/
	public void save() {
		showProjectVersionPopup = false;
		
		if(BDT_DC_FinancialDocument.validateFinancialDocument(financialDocument)) {
			upsert financialDocument;
		
			if(addProp) {
				try {
					createDocumentFirstApproval();
				} catch (Exception e) { }
			}
		
			readFinancialDocuments();
			ShowAddEditProp  = false;
			showProjectVersionPopup = true;
		
		} else {
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'In order to save a document, the fields \'Document Type\' and \'Document Name\' are mandatory. In case the \'Document Type\' is \'Change Order\', the field \'Link to Proposal\' is also mandatory. Please, enter all the data and try to save again.');
			ApexPages.addMessage(msg);
		}
	}
	
	
	/**	Create the first approval that is assigned with the document owner.
	 * @author   Dimitrios Sgourdos
	 * @version  13-Jan-2014
	*/
	private void createDocumentFirstApproval() {
		Employee_Details__c emp = FinancialDocumentApprovalService.getEmployeeByNameAndEmail( UserInfo.getFirstName(),
																							UserInfo.getLastName(),
																							UserInfo.getUserEmail() );
		
		FinancialDocumentApproval__c approval = FinancialDocumentApprovalService.createApprovalInstance(
														financialDocument.Id,
														emp,
														NULL,
														true,
														FinancialDocumentApprovalDataAccessor.APPROVAL_CREATOR_REASON,
														1 );
		
		FinancialDocumentService.takeDocumentOwnership(financialDocument, approval, emp);
	}
	
	
	/**
	*	Cancel the add or edit of an financial document
	*	@author   Dimitrios Sgourdos
	*	@version  09-Sep-2013
	*/
	public void cancel() {
		ShowAddEditProp = false;
		showProjectVersionPopup = false;
	}
	
	
	/**
	*	Delete a financial document softly (makes the BDTDeleted__c equal to true).
	*	@author   Dimitrios Sgourdos
	*	@version  10-Sep-2013
	*/
	public void deleteSoft(){
		showProjectVersionPopup = false;
		if(financialDocument != null) { // just in case
			financialDocument.BDTDeleted__c = true;
			update financialDocument;
			if(financialDocument.DocumentType__c=='Proposal' && financialDocument.ApprovalStatus__c!='Accepted') { // can be that it was accepted and after rejected
				BDT_DC_FinancialDocument.softlyDeleteChangeOrderOfProposal(financialDocument.Id);
			}
			readFinancialDocuments();
			ShowAddEditProp = false;	
		}
	}
	
	/**
	*	Initialize the global variables of the class.
	*	@author   Dimitrios Sgourdos
	*	@version  10-Sep-2013
	*	@return   The study selection page for the selected financial document
	*/
	public PageReference goToWorkFlow() {
		PageReference tmpPage = new PageReference(system.page.BDT_FinancialDocStudySel.getUrl() );
		tmpPage.getParameters().put('financialDocumentID',financialDocumentId);
		return tmpPage;
	}

	/**
	*	Sorts the financialdocuments so that the earliest parent is on top and all its childs are 
	*   directly underneath it. Then it continues to the next parent and all it's childs etc. etc.
	*	@author   Maurice Kremer
	*	@version  18-Sep-2013
	*	@param	  list<FinancialDocument__c> List that needs to be sorted
	*	@return   list<FinancialDocument__c>
	*/
	private list<FinancialDocument__c> sortFD (List<FinancialDocument__c> fdList) {
		
		// read all element and extract the fields to be sorted
		List<String> sortList = new List<String>();
		Map<String,FinancialDocument__c> sortMap = new Map<String,FinancialDocument__c>();
		For (FinancialDocument__c fd:fdList) {
			String sortValue;
			sortValue += (fd.FinancialDocumentParent__c!=null)?fd.FinancialDocumentParent__r.CreatedDate.format('yyMMddHHmmss'):'';
			sortValue += fd.CreatedDate.format('yyMMddHHmmss');
			sortList.add(sortValue);
			sortMap.put(sortValue,fd);
		}
		
		sortList.sort();
		list<FinancialDocument__c> result = new list<FinancialDocument__c>();
		// populate the return in the order that the string was sorted
		for (String key : sortList) {
			result.add(sortMap.get(key));
		}
		return result;
	}
	
	
	
	
	
}