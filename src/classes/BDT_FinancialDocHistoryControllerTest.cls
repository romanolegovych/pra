@isTest
private class BDT_FinancialDocHistoryControllerTest {

    static testMethod void myUnitTest() {
    	// Create test data
		BDT_CreateTestData.createall();
		Client_Project__c project = [select id from Client_Project__c limit 1];
		BDT_Utils.setPreference('SelectedProject', String.valueOf(project.id));
    	// Create the new financial document, with the predefined attributes values   	
		FinancialDocument__c financialDocument = new FinancialDocument__c();
		financialDocument.DocumentStatus__c    = 'Undefined';
		financialDocument.DocumentType__c      = 'Proposal';
		financialDocument.Client_Project__c    = project.id;
		financialDocument.TotalValue__c		   = '-';
		insert financialDocument;
		// Create the FinancialDocumentHistory
		FinancialDocumentHistory__c finDocHistory = new FinancialDocumentHistory__c();
		finDocHistory.FinancialDocument__c = financialDocument.id;
		finDocHistory.UserComments__c      = 'Test comments';
		finDocHistory.ChangeDescription__c = 'Test change description';
		finDocHistory.TotalValue__c		   = '0';
		finDocHistory.ProcessStep__c	   = 'Price';
		upsert finDocHistory; 
		// Create the controller
		BDT_FinancialDocHistoryController p = new BDT_FinancialDocHistoryController();
		PageReference pageRef = New PageReference(System.Page.BDT_FinancialDocHistory.getUrl());
		pageref.getParameters().put('financialDocumentId' , financialDocument.id);
		Test.setCurrentPage(pageRef); 
    	p = new BDT_FinancialDocHistoryController();
    	p.getFilterOptions();
    	p.selectedFilterMethod = 'b';
    	p.changeFilterSelection();
    	p.selectedFilterMethod = 'c';
    	p.changeFilterSelection();
    	p.selectedFilterMethod = 'd';
    	p.changeFilterSelection();
    	p.selectedFilterMethod = 'e';
    	p.changeFilterSelection();
    	p.selectedFilterMethod = 'f';
    	p.changeFilterSelection();
    	p.selectedFilterMethod = 'g';
    	p.changeFilterSelection();
    	p.closePage();
    } 
}