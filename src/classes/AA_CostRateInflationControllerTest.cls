@isTest(SeeAllData=false)
private class AA_CostRateInflationControllerTest{   
    
    /* Test getresult() method */
    static testMethod void getresultTest() {
    
         initTestData ();
         
         Test.startTest();
         
         Cost_Rate__c costObj = getDummyCostRate();
       
         PageReference oPage = new PageReference('/apex/AA_Edit_Revenue_Allocation_Rate?functionId='+costObj.BUF_Code__c+'&scheduledYear=2011');
         //Test.setCurrentPage(oPage);

         Country__c country = new Country__c(name='TestCountry1',PRA_Country_ID__c='100', Country_Code__c='TS',Region_Name__c='North America', 
            daily_Business_Hrs__c=8.0);
         Business_Unit__c businessUnit1 = new Business_Unit__c(name='BU1',Default_Country__c=country.Id);
         Function_Code__c functionCode1 = new Function_Code__c(name='F1');
         Currency__c currency1 = new Currency__c(name='CUR1');
         BUF_Code__c BufCode1 = new BUF_Code__c(name='BU1F1',Business_Unit__c=businessUnit1.Id,Function_Code__c=functionCode1.Id);
         Cost_Rate__c costrate1= new Cost_Rate__c(BUF_Code__c=bufcode1.Id,Cost_Rate__c=123,Schedule_Year__c='2011',Year_Rate_Applies_To__c='2012',Currency__c=currency1.Id,IsActive__c = true);
         insert costrate1;
         
         AA_CostRateInflationController controller = new AA_CostRateInflationController();
         controller.id =  BufCode1.id;
         controller.scheduledYear =  '2011';
         List<AA_CostRateInflationController.CostRateInflationWrapper> result = controller.getresult();
         system.debug('------result ------'+result+ controller.result+controller.id+controller.scheduledYear);
         system.debug('------functionCode1.Id------'+functionCode1.Id+BufCode1.id);
         System.assertEquals(controller.result.size(),1);
         
         Test.stopTest();
    }
    
    /* Test Apply button */
    static testMethod void applyTest() {
    
         initTestData ();
         
         Test.startTest();
        
         AA_CostRateInflationController controller = new AA_CostRateInflationController();
         Pagereference pageref = controller.apply();
         
         System.assertEquals(pageref,null);
         
         Test.stopTest();
    }
    
     /* Test Save button */
    static testMethod void saveTest() {
    
         initTestData ();
         
         Test.startTest();
        
         AA_CostRateInflationController controller = new AA_CostRateInflationController();
         Pagereference pageref = controller.save();
         
         System.assertEquals(pageref,null);
         
         Test.stopTest();
    }
    
      /* Test delete button */
    static testMethod void deleterecordsTest() {
    
         initTestData ();
         
         Test.startTest();
        
         AA_CostRateInflationController controller = new AA_CostRateInflationController();
         Pagereference pageref = controller.deleterecords();
         
         System.assertEquals(pageref,null);
         
         Test.stopTest();
    }
    
    
      /* Test Back button */
    static testMethod void backTest() {
    
         initTestData ();
         
         Test.startTest();
        
         AA_CostRateInflationController controller = new AA_CostRateInflationController();
         Pagereference pageref = controller.back();
         
         System.assertEquals(pageref.geturl(), Page.AA_Cost_Rate.geturl());
         
         Test.stopTest();
    }
     /* Create test data using test utils */
    static void initTestData (){
        AA_TestUtils testUtil = new AA_TestUtils();
        testUtil.initAll();
    }  
    
    /* Get Dummy Cost Rate */
    static Cost_Rate__c getDummyCostRate(){
        AA_TestUtils testUtil = new AA_TestUtils();
        return testUtil.getCostRate();
    }   
}