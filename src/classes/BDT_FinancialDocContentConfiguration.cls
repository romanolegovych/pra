public with sharing class BDT_FinancialDocContentConfiguration {
	
	// Class for the required input
	public class inputContentWrapper {
		public String tooltipString		{get;set;}
		public String defaultText		{get;set;}
		
		public inputContentWrapper(string p_tooltip, string p_defaultText ) {
			tooltipString = p_tooltip;
			defaultText	  = p_defaultText;
		}
	}
	
	// Class for the checkBox items
	public class contentItemWrapper{
		public String  				itemName		{get;set;}
		public String  				itemCode		{get;set;}
		public Boolean 				defaultChecked	{get;set;}
		public Boolean 				selected		{get;set;}
		public Boolean				inputReqChk		{get;set;}
		public inputContentWrapper 	itemInput		{get;set;}
		public Boolean				photoAllowed	{get;set;}
		
		public contentItemWrapper(string p_Name, string p_Code, boolean p_defChecked, boolean p_inputReqChk, boolean p_photoAllowed){
			itemName 	   = p_Name;
			itemCode	   = p_Code;
			defaultChecked = p_defChecked;
			selected	   = p_defChecked;
			inputReqChk	   = p_inputReqChk;
			photoAllowed   = p_photoAllowed;
		}
	}
	
	// That class keeps track of the sub items inside the apex:tags
	public class contentSubChapterWrapper {
		public Boolean					 standardFlag {get; set;}
		public String					 titleString  {get; set;}
		public list<String> 			 items 		  {get; set;} 
		public list<contentItemWrapper>	 checkItems   {get; set;}
		
		public contentSubChapterWrapper (Boolean p_standard, String p_title) {
			standardFlag = p_standard;
			titleString  = p_title; 
			items 		 = new list<String>();
			checkItems	 = new list<contentItemWrapper>();
		}
	}
	
	// That class keeps track of the items inside the apex:tags
	public class contentChapterWrapper{
		public String 					 		ChapterName     {get;set;}
		public String  							ChapterCode		{get;set;}
		public boolean 							defaultChecked  {get;set;}
		public boolean 							selected 		{get;set;}
		public list<contentSubChapterWrapper>	subContent		{get; set;}
		
		public contentChapterWrapper(String p_Name, String p_Code, Boolean p_defChecked){
			ChapterName    = p_Name;
			ChapterCode	   = p_Code;
			defaultChecked = p_defChecked;
			selected	   = p_defChecked;
			subContent     = new list<contentSubChapterWrapper>();
		}
	}
	
	
	public map<string, list<contentChapterWrapper> > content {get;set;}
	
	
	/** The constructor of the class. It creates the content options for the current financial document.
	 * @author	Dimitrios Sgourdos
	 * @version 18-Nov-2013
	 */
	public BDT_FinancialDocContentConfiguration(){
		content =  new map<string, list<contentChapterWrapper>>();
		contentChapterWrapper  	 		contentChapterItem;
		contentSubChapterWrapper 		contSubChapterItem;
		contentItemWrapper     	 		contentItem;
		list<contentChapterWrapper> 	tmpCCW;
		// -------------------- Front Page & Summary --------------------
		tmpCCW = new list<contentChapterWrapper>();
		contentChapterItem = new contentChapterWrapper('Front Page', 'FrontPageChk', true);
			contSubChapterItem = new contentSubChapterWrapper(true, 'Standard items');
			contSubChapterItem.items.add('Proposal Name');
			contSubChapterItem.items.add('Project Description');
			contSubChapterItem.items.add('Project ID');
			contSubChapterItem.items.add('Study ID(s)');
			contSubChapterItem.items.add('Sponsor Entity');	
			contentChapterItem.subContent.add(contSubChapterItem);
		tmpCCW.add(contentChapterItem);
		contentChapterItem = new contentChapterWrapper('Summary', 'SummaryChk', true);
			contSubChapterItem = new contentSubChapterWrapper(true, 'Standard items');
			contSubChapterItem.items.add('Project Description');
			contSubChapterItem.items.add('Site(s)');
			contSubChapterItem.items.add('Population(s)');
			contSubChapterItem.items.add('Timelines');
			contSubChapterItem.items.add('Costs');
			contentChapterItem.subContent.add(contSubChapterItem);
		tmpCCW.add(contentChapterItem);
		content.put('Front Page and Summary',tmpCCW);
		// -------------------- Introduction --------------------
		tmpCCW = new list<contentChapterWrapper>();
		contentChapterItem = new contentChapterWrapper('Introduction', 'GeneralChk', true);
			contSubChapterItem = new contentSubChapterWrapper(false, 'Optional items (input required)');
			contentItem = new contentItemWrapper('Strategic Information', 'StrategicInfoChk', false, true, true);
			contentItem.itemInput = new inputContentWrapper('Strategical text should be included here. This could contain information on why this proposal is interesting, what special features are included or what special conditions apply.\nIn addition, if multiple options for execution are included in one proposal, it can be specified here.', '');
			contSubChapterItem.checkItems.add(contentItem);
			contentItem = new contentItemWrapper('Contact Details', 'ContactDetailsChk', false, true, true);
			contentItem.itemInput = new inputContentWrapper('Include at least the following information: Name // Function // Phone // Email.', '');
			contSubChapterItem.checkItems.add(contentItem);
			contentChapterItem.subContent.add(contSubChapterItem);
		tmpCCW.add(contentChapterItem);
		content.put('General',tmpCCW);
		// -------------------- Project Description --------------------
		tmpCCW = new list<contentChapterWrapper>();
		contentChapterItem = new contentChapterWrapper('Project Information', 'ProjectInfoChk', true);
			contSubChapterItem = new contentSubChapterWrapper(true, 'Standard items');
			contSubChapterItem.items.add('Project ID');
			contSubChapterItem.items.add('Project Description');
			contSubChapterItem.items.add('[Sponsor] Sponsor Entity');
			contentChapterItem.subContent.add(contSubChapterItem);
			contSubChapterItem = new contentSubChapterWrapper(false, 'Optional items');
			contSubChapterItem.checkItems.add(new contentItemWrapper('Therapeutic Area [Indication]', 'TherAreaChk', false, false, false) );
			contSubChapterItem.checkItems.add(new contentItemWrapper('Investigational Product', 'InvestProdChk', false, false, false) );
			contentChapterItem.subContent.add(contSubChapterItem);
		tmpCCW.add(contentChapterItem);
		contentChapterItem = new contentChapterWrapper('Studies', 'StudiesChk', true);
			contSubChapterItem = new contentSubChapterWrapper(true, 'Standard items');
			contSubChapterItem.items.add('Legacy Code(s)');
			contSubChapterItem.items.add('Sponsor Code(s)');
			contSubChapterItem.items.add('Short Title(s)');
			contSubChapterItem.items.add('Site(s)');
			contentChapterItem.subContent.add(contSubChapterItem);
			contSubChapterItem = new contentSubChapterWrapper(false, 'Optional items');
			contSubChapterItem.checkItems.add(new contentItemWrapper('Population(s)', 'PopulChk', false, false, false) );
			contSubChapterItem.checkItems.add(new contentItemWrapper('Method(s) and Compound(s)', 'MethCompChk', false, false, false) );
			contSubChapterItem.checkItems.add(new contentItemWrapper('Subcontractor(s)', 'SubContrChk', false, false, false) );
			contentChapterItem.subContent.add(contSubChapterItem);
		tmpCCW.add(contentChapterItem);
		content.put('Project Info (general)',tmpCCW);
		// -------------------- Project details --------------------
		tmpCCW = new list<contentChapterWrapper>();
		contentChapterItem = new contentChapterWrapper('Population Details', 'PopulationChk', true);
			contSubChapterItem = new contentSubChapterWrapper(true, 'Standard items');
			contSubChapterItem.items.add('Key Criteria');
			contSubChapterItem.items.add('Inclusion Criteria');
			contSubChapterItem.items.add('Exclusion Criteria');
			contentChapterItem.subContent.add(contSubChapterItem);
		tmpCCW.add(contentChapterItem);
		contentChapterItem = new contentChapterWrapper('Design and Flowchart Details', 'DesignFlowChk', true);
			contSubChapterItem = new contentSubChapterWrapper(false, 'Optional items');
			contSubChapterItem.checkItems.add(new contentItemWrapper('Design(s) with header', 'DesHeaderChk', false, false, false) );
			contSubChapterItem.checkItems.add(new contentItemWrapper('Flowchart(s)', 'FlowchartChk', false, false, false) );
			contSubChapterItem.checkItems.add(new contentItemWrapper('Design(s) for laboratory', 'DesLabChk', false, false, false) );
			contentChapterItem.subContent.add(contSubChapterItem);
		tmpCCW.add(contentChapterItem);
		contentChapterItem = new contentChapterWrapper('Services and Milestones', 'SrvMilesChk', true);
			contSubChapterItem = new contentSubChapterWrapper(true, 'Standard items');
			contSubChapterItem.items.add('Services (units)');
			contSubChapterItem.items.add('Pass-through Services');
			contSubChapterItem.items.add('Milestones');
			contentChapterItem.subContent.add(contSubChapterItem);
		tmpCCW.add(contentChapterItem);
		contentChapterItem = new contentChapterWrapper('Project Considerations', 'HeadConsiderChk', true);
			contSubChapterItem = new contentSubChapterWrapper(false, 'Optional items (input required)');
			contentItem = new contentItemWrapper('Considerations / Assumptions / Special Items', 'ConsiderChk', false, true, true);
			contentItem.itemInput = new inputContentWrapper('Some text can be included here that covers for example special procedures, special instrumentation, etc. It could also be a picture that is a graphical explanation of e.g. timelines or milestones.', '');
			contSubChapterItem.checkItems.add(contentItem);
			contentChapterItem.subContent.add(contSubChapterItem);
		tmpCCW.add(contentChapterItem);
		content.put( 'Project Details',tmpCCW );
		// -------------------- Financial Summary --------------------
		tmpCCW = new list<contentChapterWrapper>();
		contentChapterItem = new contentChapterWrapper('Financial', 'FinancialChk', true);
			contSubChapterItem = new contentSubChapterWrapper(true, 'Standard items');
			//contSubChapterItem.items.add('Prices (final price only)');
			contSubChapterItem.items.add('Payment terms');
			contentChapterItem.subContent.add(contSubChapterItem);
			contSubChapterItem = new contentSubChapterWrapper(false, 'Optional items (input required)');
			contSubChapterItem.checkItems.add(new contentItemWrapper('Prices', 'DiscountChk', false, false, false) );
			contentItem = new contentItemWrapper('Payment conditions', 'PaymentCondChk', false, true, false);
			contentItem.itemInput = new inputContentWrapper('', 'For each installment an invoice will be sent. This invoice should be paid within 30 days after receipt of the invoice.\nPerformed activities which are less than € 200 in one month will be invoiced together with the costs in the next month.\nThe cost estimation and planning of this quotation are based on the PRA flexible planning principle, which entitles PRA to schedule the study one week earlier or later than given in the Milestones.');
			contSubChapterItem.checkItems.add(contentItem);
			contentChapterItem.subContent.add(contSubChapterItem);
		tmpCCW.add(contentChapterItem);
		content.put('Financial Info', tmpCCW);
		// -------------------- Appendix --------------------
		tmpCCW = new list<contentChapterWrapper>();
		contentChapterItem = new contentChapterWrapper('Miscellaneous information', 'MiscellaneousChk', true);
			contSubChapterItem = new contentSubChapterWrapper(false, 'Optional items (input required)');
			contentItem = new contentItemWrapper('General conditions and disclaimer', 'CondAndDisclChk', false, true, false);
			contentItem.itemInput = new inputContentWrapper('', 'Costs for Pass-through Services are an estimate. The actual invoices will be sent to the Sponsor upon receipt.\nAll procedures will be applied as per PRA SOPs unless specifically mentioned otherwise.\nPRA templates will be used for study-related documents (including protocols and reports) unless specifically mentioned otherwise.\nIn case changes to the services (during execution of the project) as mentioned in this proposal occur, PRA holds the right to revise the proposal and/or issue (a) change order(s).' );
			contSubChapterItem.checkItems.add(contentItem);
			contentChapterItem.subContent.add(contSubChapterItem);
		tmpCCW.add(contentChapterItem);
		content.put('Appendix', tmpCCW);
	}
}