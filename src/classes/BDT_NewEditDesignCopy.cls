public with sharing class BDT_NewEditDesignCopy {
	
	public String designCopyTo {get;set;}
	public String copyFromId = null;
	public String projectId {get;set;}
	public List<Epoch__c>	epochList	{get;set;}
	public List<Arm__c>		armList		{get;set;}
	public List<Flowcharts__c>	flowchartList	{get;set;}
	public List<flowchartassignment__c> flowchartAssList {get;set;}
		
	public BDT_NewEditDesignCopy(){
		
		designCopyTo = system.currentPageReference().getParameters().get('designId');
		projectId = BDT_Utils.getPreference('SelectedProject');
		
	}
	
	public String getCopyFromId() {
		return copyFromId;
	}
	
	public void setCopyFromId(String copyFromId) { 
		this.copyFromId = copyFromId; 
	}
	
	public List<SelectOption> getCopyFromOptions(){
    	List<SelectOption> options = new List<SelectOption>();
   		List<ClinicalDesign__c> clinicalDesignList = new List<ClinicalDesign__c>();
    	try{
    		clinicalDesignList = [Select c.Project__c, c.Name, c.Design_Name__c From ClinicalDesign__c c where  c.Project__c =: projectId];
    	}catch(Exception e){
		}
    	options.add(new SelectOption('0','Select Option'));
    	for(ClinicalDesign__c clin: clinicalDesignList){
    		if(clin.id != designCopyTo){
    		options.add(new SelectOption(clin.id,clin.Name));
    		}
    	}
    	
    	return options;
    }
    
    public PageReference copy(){
    	
    	Map<String, Epoch__c> newEpochMap = new Map<String, Epoch__c>();    	
    	Map<String, Arm__c> newArmMap = new Map<String, Arm__c>();    	
    	Map<String, Flowcharts__c> newFlowchartMap = new Map<String, Flowcharts__c>();
    	List<flowchartassignment__c> newFlowchartAss = new List<flowchartassignment__c>();
    	epochList = BDT_FlowchartDisplay.getEpochHeaderList(copyFromId);
    	armList = BDT_FlowchartDisplay.getArmHeaderList(copyFromId);
    	flowchartAssList = BDT_FlowchartDisplay.getFlowchartsAssignmentList(copyFromId);
    	flowchartList = getFlowcharts(copyFromId);
    	
    	for(Epoch__c e: epochList){
    		newEpochMap.put(e.Description__c,new Epoch__c(Epoch_Number__c = e.Epoch_Number__c, Description__c = e.Description__c, Duration_days__c = e.Duration_days__c, ClinicalDesign__c = designCopyTo));
    	}
    	    	
    	for(Arm__c a: armList){
    		newArmMap.put(a.Description__c, new Arm__c( Description__c = a.Description__c, Arm_Number__c= a.Arm_Number__c, ClinicalDesign__c = designCopyTo));
    	}
    	
    	for(Flowcharts__c f: flowchartList){
    		newFlowchartMap.put(f.Description__c, new Flowcharts__c(Flowchart_Number__c = f.Flowchart_Number__c, Description__c = f.Description__c, ClinicalDesign__c = designCopyTo));
    	}
    	
    	try{
			insert newEpochMap.values();
			insert newArmMap.values();
			insert newFlowchartMap.values();
		}catch(DMLException e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to copy flowchart elements. ');
        	ApexPages.addMessage(msg); 
		}
    	
    	
		for(flowchartassignment__c flowass:flowchartAssList){
			
			String newEpochId=null;
			String newArmId=null;
			String newFlowchartId=null;
			
			
			if(newEpochMap.containsKey(flowass.epoch__r.Description__c)){
				newEpochId = newEpochMap.get(flowass.epoch__r.Description__c).id;
			}
			
			if(newArmMap.containsKey(flowass.arm__r.Description__c)){
				newArmId = newArmMap.get(flowass.arm__r.Description__c).id;
			}
			
			
			if(newFlowchartMap.containsKey(flowass.flowchart__r.Description__c)){
				newFlowchartId = newFlowchartMap.get(flowass.flowchart__r.Description__c).id;
			}
			 
			newFlowchartAss.add(new flowchartassignment__c(arm__c = newArmId, epoch__c = newEpochId, flowchart__c = newFlowchartId));
		}
		
		try{
			insert newFlowchartAss;
		}catch(DMLException e){
			ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.FATAL, 'Unable to copy flowchart coordinates. ');
        	ApexPages.addMessage(msg); 
		}
    	    			
		
		PageReference newEditDesignPage = new PageReference(System.Page.bdt_neweditdesign.getUrl());
		newEditDesignPage.getParameters().put('designId',designCopyTo); 		
		newEditDesignPage.setRedirect(true);
		return newEditDesignPage;
    
    }
    
    public static List<Flowcharts__c> getFlowcharts(String aDesign){
    	List<Flowcharts__c> flowChartList = new List<Flowcharts__c>();
    	
    	try{
     		flowChartList = [Select f.Name, f.Flowchart_Number__c, f.Description__c, f.ClinicalDesign__c From Flowcharts__c f 
     								where ClinicalDesign__c = : aDesign];
    	}catch(Exception e){
    		
    	}
    	
    	return flowChartList;
    }
    
    public PageReference cancel(){
    	PageReference newEditDesignPage = new PageReference(System.Page.bdt_neweditdesign.getUrl());
		newEditDesignPage.getParameters().put('designId',designCopyTo); 		
		newEditDesignPage.setRedirect(true);
		return newEditDesignPage;
    
    }
	

}