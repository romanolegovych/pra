/**
 * @description	Implements the test for the functions of the NCM_SrvLayer_Chatter class
 * @author		Dimitrios Sgourdos
 * @version		Created: 08-Sep-2015, Edited: 09-Sep-2015
 */
@isTest
private class NCM_SrvLayer_ChatterTest {
	
	/**
	 * @description	Test the function postFeedItem version A (with parameters Id and String)
	 * @author		Dimitrios Sgourdos
	 * @date		08-Sep-2015
	 */
	static testMethod void postFeedItemTest_version_A() {
		// Created Data
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		// Check the function
		List<FeedItem> results = [SELECT ID FROM FeedItem LIMIT 10];
		system.assertEquals(0, results.size(), 'Error in reading feed items');
		
		String errorMessage = 'Error in posting feed items';
		NCM_SrvLayer_Chatter.postFeedItem(acc.Id, 'Test Feed');
		
		results = [SELECT Id, parentId, Body FROM FeedItem LIMIT 10];
		system.assertEquals(1, results.size(), errorMessage);
		system.assertEquals(acc.Id, results[0].parentId, errorMessage);
		system.assertEquals('Test Feed', results[0].Body, errorMessage);
	}
	
	
	/**
	 * @description	Test the function postFeedItem version B (with parameter ChatterPostWrapper)
	 * @author		Dimitrios Sgourdos
	 * @date		08-Sep-2015
	 */
	static testMethod void postFeedItemTest_version_B() {
		// Created Data
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		List<NCM_Chatter_API_DataTypes.ChatterPostWrapper> source = new List<NCM_Chatter_API_DataTypes.ChatterPostWrapper>();
		source.add( new NCM_Chatter_API_DataTypes.ChatterPostWrapper(accList[0].Id, 'Feed A') );
		source.add( new NCM_Chatter_API_DataTypes.ChatterPostWrapper(accList[1].Id, 'Feed B') );
		
		// Check the function
		List<FeedItem> results = [SELECT ID FROM FeedItem LIMIT 10];
		system.assertEquals(0, results.size(), 'Error in reading feed items');
		
		String errorMessage = 'Error in posting feed items';
		NCM_SrvLayer_Chatter.postFeedItem(source);
		
		results = [SELECT Id, parentId, Body FROM FeedItem ORDER BY Body LIMIT 10];
		system.assertEquals(2, results.size(), errorMessage);
		system.assertEquals(accList[0].Id, results[0].parentId, errorMessage);
		system.assertEquals('Feed A', results[0].Body, errorMessage);
		system.assertEquals(accList[1].Id, results[1].parentId, errorMessage);
		system.assertEquals('Feed B', results[1].Body, errorMessage);
	}
	
	
	/**
	 * @description	Test the function postComment version A (with parameters Id and String)
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 08-Sep-2015
	 */
	static testMethod void postCommentTest_version_A() {
		// Create data
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		NCM_SrvLayer_Chatter.postFeedItem(acc.Id, 'Text Feed');
		List<FeedItem> source = [SELECT ID FROM FeedItem LIMIT 1];
		
		// Check the function
		List<FeedComment> results = [SELECT ID FROM FeedComment LIMIT 10];
		system.assertEquals(0, results.size(), 'Error in reading feed comments');
		
		String errorMessage = 'Error in posting feed comments';
		NCM_SrvLayer_Chatter.postComment(source[0].Id, 'Test comment');
		
		results = [SELECT Id, FeedItemId, CommentBody FROM FeedComment LIMIT 10];
		system.assertEquals(1, results.size(), errorMessage);
		system.assertEquals(source[0].Id, results[0].FeedItemId, errorMessage);
		system.assertEquals('Test comment', results[0].CommentBody, errorMessage);
	}
	
	
	/**
	 * @description	Test the function postComment version B (with parameter ChatterPostWrapper)
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 08-Sep-2015
	 */
	static testMethod void postCommentTest_version_B() {
		// Create data
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		NCM_SrvLayer_Chatter.postFeedItem(acc.Id, 'Text Feed');
		List<FeedItem> feedList = [SELECT ID FROM FeedItem LIMIT 1];
		
		List<NCM_Chatter_API_DataTypes.ChatterPostWrapper> source = new List<NCM_Chatter_API_DataTypes.ChatterPostWrapper>();
		source.add( new NCM_Chatter_API_DataTypes.ChatterPostWrapper(feedList[0].Id, 'Comment A') );
		source.add( new NCM_Chatter_API_DataTypes.ChatterPostWrapper(feedList[0].Id, 'Comment B') );
		
		// Check the function
		List<FeedComment> results = [SELECT ID FROM FeedComment LIMIT 10];
		system.assertEquals(0, results.size(), 'Error in reading feed comments');
		
		String errorMessage = 'Error in posting feed comments';
		NCM_SrvLayer_Chatter.postComment(source);
		
		results = [SELECT Id, FeedItemId, CommentBody FROM FeedComment ORDER BY CommentBody LIMIT 10];
		system.assertEquals(2, results.size(), errorMessage);
		system.assertEquals(feedList[0].Id, results[0].FeedItemId, errorMessage);
		system.assertEquals('Comment A', results[0].CommentBody, errorMessage);
		system.assertEquals(feedList[0].Id, results[1].FeedItemId, errorMessage);
		system.assertEquals('Comment B', results[1].CommentBody, errorMessage);
	}
	
	
	/**
	 * @description	Test the function isUserSubscribedToFeed
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 10-Jul-2014
	 */
	static testMethod void isUserSubscribedToFeedTest() {
		// Create data
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		String errorMessage = 'Error in determining if a user is subscribed to feeds';
		
		// Check the function with the user not subscribed to the created account
		Boolean result = NCM_SrvLayer_Chatter.isUserSubscribedToFeed(acc.Id, testUser.Id);
		system.assertEquals(false, result, errorMessage);
		
		// Check the function with the user subscribed to the created account
		EntitySubscription subscr = new EntitySubscription(parentId = acc.Id, subscriberId = testUser.Id);
		insert subscr;
		
		result = NCM_SrvLayer_Chatter.isUserSubscribedToFeed(acc.Id, testUser.Id);
		system.assertEquals(true, result, errorMessage);
	}
	
	
	/**
	 * @description	Test the function getFeedItems version A (with parameter Id)
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 10-Jul-2014, Edited: 08-Sep-2015
	 */
	static testMethod void getFeedItemsTest_version_A_Test() {
		// Create data
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		FeedItem source = new FeedItem(parentId = acc.Id, Body = 'Test Feed');
		insert source;
		
		String errorMessage = 'Error in getting feed items';
		
		// Check the function
		List<feedItem> results = NCM_SrvLayer_Chatter.getFeedItems(acc.Id);
		
		system.assertEquals(1, results.size(), errorMessage);
		system.assertEquals(source.Id, results[0].Id, errorMessage);
		system.assertEquals('Test Feed', results[0].Body, errorMessage);
		system.assertEquals(acc.Id, results[0].parentId, errorMessage);
	}
	
	
	/**
	 * @description	Test the function getFeedItems version B (with parameter Id and String)
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 10-Jul-2014, Edited: 08-Sep-2015
	 */
	static testMethod void getFeedItems_version_B_Test() {
		// Create data
		Account acc = new Account(Name = 'A',BillingCountry='NL', BillingCity='Groningen');
		insert acc;
		
		List<FeedItem> source = new List<FeedItem>();
		source.add( new FeedItem(parentId = acc.Id, Body = '#TestTopic Test Feed A') );
		source.add( new FeedItem(parentId = acc.Id, Body = '#TestTopic Test Feed B') );
		source.add( new FeedItem(parentId = acc.Id, Body = 'Test Feed B') );
		insert source;
		
		Set<Id> feedIds = new Set<Id>{source[0].Id, source[1].Id};
		
		String errorMessage = 'Error in getting feed items';
		
		// Check the function
		List<FeedItem> results = NCM_SrvLayer_Chatter.getFeedItems(acc.Id, 'Not a present topic');
		system.assertEquals(0, results.size(), errorMessage);
		
		results = NCM_SrvLayer_Chatter.getFeedItems(acc.Id, 'TestTopic');
		system.assertEquals(2, results.size(), errorMessage);
		system.assertEquals(true, feedIds.contains(results[0].Id), errorMessage);
		system.assertEquals(true, feedIds.contains(results[1].Id), errorMessage);
	}
	
	
	/**
	 * @description	Test the function unsubscribeUserFromFeed version A (with parameters Id and Id)
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 10-Jul-2014, Edited: 08-Sep-2015
	 */
	static testMethod void unsubscribeUserFromFeedTest_version_A() {
		// Created Data
		Account acc = new Account(Name = 'A',BillingCountry='NL', BillingCity='Groningen');
		insert acc;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		EntitySubscription subscr = new EntitySubscription(parentId = acc.Id, subscriberId = testUser.Id);
		insert subscr;
		
		// Check the function
		List<EntitySubscription> results = [SELECT ID FROM EntitySubscription LIMIT 1];
		system.assertEquals(1, results.size(), 'Error in reading subscriptions');
		
		NCM_SrvLayer_Chatter.unsubscribeUserFromFeed(acc.Id, testUser.Id);
		
		results = [SELECT ID FROM EntitySubscription LIMIT 1];
		system.assertEquals(0, results.size(), 'Error in unsubscribing user from object');
	}
	
	
	/**
	 * @description	Test the function unsubscribeUserFromFeed version B (with parameter SubscriptionToByUserWrapper)
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 08-Sep-2015
	 */
	static testMethod void unsubscribeUserFromFeedTest_version_B() {
		// Created Data
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'C', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		List<EntitySubscription> subscriptions = new List<EntitySubscription>();
		subscriptions.add( new EntitySubscription(parentId = accList[0].Id, subscriberId = testUser.Id) );
		subscriptions.add( new EntitySubscription(parentId = accList[1].Id, subscriberId = testUser.Id) );
		subscriptions.add( new EntitySubscription(parentId = accList[2].Id, subscriberId = testUser.Id) );
		insert subscriptions;
		
		List<NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper> source = new List<NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper>();
		source.add( new NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper(accList[0].Id, testUser.Id) );
		source.add( new NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper(accList[1].Id, testUser.Id) );
		
		String errorMessage = 'Error in unsubscribing user from object';
		
		// Check the function
		List<EntitySubscription> results = [SELECT ID FROM EntitySubscription LIMIT 10];
		system.assertEquals(3, results.size(), 'Error in reading subscriptions');
		
		NCM_SrvLayer_Chatter.unsubscribeUserFromFeed(NULL);
		results = [SELECT ID FROM EntitySubscription LIMIT 10];
		system.assertEquals(3, results.size(), errorMessage);
		
		NCM_SrvLayer_Chatter.unsubscribeUserFromFeed(source);
		
		results = [SELECT ID FROM EntitySubscription LIMIT 1];
		system.assertEquals(1, results.size(), errorMessage);
		system.assertEquals(subscriptions[2].Id, results[0].Id, errorMessage);
	}
	
	
	/**
	 * @description	Test the function subscribeUserToFeed version A (with parameters Id and Id)
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 10-Jul-2014, Edited: 08-Sep-2015
	 */
	static testMethod void subscribeUserToFeedTest_version_A() {
		// Created Data
		Account acc = new Account(Name = 'A',BillingCountry='NL', BillingCity='Groningen');
		insert acc;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		String errorMessage = 'Error in subscribing user to object';
		
		// Check the function
		List<EntitySubscription> results = [SELECT ID FROM EntitySubscription LIMIT 1];
		system.assertEquals(0, results.size(), 'Error in reading subscriptions');
		
		NCM_SrvLayer_Chatter.subscribeUserToFeed(acc.Id, testUser.Id);
		
		results = [SELECT Id, parentId, subscriberId FROM EntitySubscription LIMIT 10];
		system.assertEquals(1, results.size(), errorMessage);
		system.assertEquals(acc.Id, results[0].parentId, errorMessage);
		system.assertEquals(testUser.Id, results[0].subscriberId, errorMessage);
		
		NCM_SrvLayer_Chatter.subscribeUserToFeed(acc.Id, testUser.Id);
		
		results = [SELECT Id, parentId, subscriberId FROM EntitySubscription LIMIT 10];
		system.assertEquals(1, results.size(), errorMessage);
		system.assertEquals(acc.Id, results[0].parentId, errorMessage);
		system.assertEquals(testUser.Id, results[0].subscriberId, errorMessage);
	}
	
	
	/**
	 * @description	Test the function subscribeUserToFeed version B (with parameters SubscriptionToByUserWrapper)
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 08-Sep-2015
	 */
	static testMethod void subscribeUserToFeedTest_version_B() {
		// Created Data
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'C', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'D', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		List<EntitySubscription> subscriptions = new List<EntitySubscription>();
		subscriptions.add( new EntitySubscription(parentId = accList[2].Id, subscriberId = testUser.Id) );
		insert subscriptions;
		
		List<NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper> source = new List<NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper>();
		source.add( new NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper(accList[0].Id, testUser.Id) );
		source.add( new NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper(accList[1].Id, testUser.Id) );
		source.add( new NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper(accList[2].Id, testUser.Id) );
		
		String errorMessage = 'Error in subscribing user to object';
		
		// Check the function
		List<EntitySubscription> results = [SELECT ID FROM EntitySubscription LIMIT 10];
		system.assertEquals(1, results.size(), errorMessage);
		system.assertEquals(subscriptions[0].Id, results[0].Id, errorMessage);
		
		NCM_SrvLayer_Chatter.subscribeUserToFeed(NULL);
		results = [SELECT ID FROM EntitySubscription LIMIT 10];
		system.assertEquals(1, results.size(), errorMessage);
		system.assertEquals(subscriptions[0].Id, results[0].Id, errorMessage);
		
		NCM_SrvLayer_Chatter.subscribeUserToFeed(source);
		
		results = [SELECT Id, parentId, subscriberId FROM EntitySubscription LIMIT 10];
		system.assertEquals(3, results.size(), errorMessage);
		
		Set<Id> feedIds = new Set<Id>{accList[0].Id, accList[1].Id, accList[2].Id};
		system.assertEquals(true, feedIds.contains(results[0].parentId), errorMessage);
		system.assertEquals(true, feedIds.contains(results[1].parentId), errorMessage);
		system.assertEquals(true, feedIds.contains(results[2].parentId), errorMessage);
	}
	
	
	/**
	 * @description	Test the function mapSubscriptionsOnSubscriberAndFeed
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 09-Sep-2015
	 */
	static testMethod void mapSubscriptionsOnSubscriberAndFeedTest() {
		// Created Data
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'C', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'D', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		User testUser = NCM_Utils.buildTestUserList(1)[0];
		
		List<EntitySubscription> subscriptions = new List<EntitySubscription>();
		subscriptions.add( new EntitySubscription(parentId = accList[0].Id, subscriberId = testUser.Id) );
		subscriptions.add( new EntitySubscription(parentId = accList[1].Id, subscriberId = testUser.Id) );
		subscriptions.add( new EntitySubscription(parentId = accList[2].Id, subscriberId = testUser.Id) );
		insert subscriptions;
		
		List<NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper> source = new List<NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper>();
		source.add( new NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper(accList[0].Id, testUser.Id) );
		source.add( new NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper(accList[1].Id, testUser.Id) );
		source.add( new NCM_Chatter_API_DataTypes.SubscriptionToByUserWrapper(accList[3].Id, testUser.Id) );
		
		String errorMessage = 'Error in mapping existing subscription on feed id and subscriber id';
		
		// Check the function with invalid parameters
		Map<String, EntitySubscription> results = NCM_SrvLayer_Chatter.mapSubscriptionsOnSubscriberAndFeed(NULL);
		
		system.assertEquals(0, results.size(), errorMessage);
		
		// Check the function with valid parameters
		results = NCM_SrvLayer_Chatter.mapSubscriptionsOnSubscriberAndFeed(source);
		
		system.assertEquals(2, results.size(), errorMessage);
		
		String key = '' + accList[0].Id + ':' + testUser.Id;
		system.assertEquals(true, results.containsKey(key), errorMessage);
		
		key = '' + accList[1].Id + ':' + testUser.Id;
		system.assertEquals(true, results.containsKey(key), errorMessage);
	}
}