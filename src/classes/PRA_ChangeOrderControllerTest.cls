@isTest (seeAllData=false)
private class PRA_ChangeOrderControllerTest {

    static testMethod void shouldCreateNewChangeOrder() {
        // given 
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        // when
        test.startTest();
        PRA_ChangeOrderController coController = new PRA_ChangeOrderController();
        coController.setSelectedProject(tu.clientProject.Id);
        
        // check that there are 5 actions available
        System.assertEquals(5, coController.getActionList().size());
        
        // check that no change order exists in system
         System.assertEquals(0,coController.getProjectChangeOrders().size());
         
         // check that all tasks are displayes
         coController.selectedAction = PRA_ChangeOrderController.TEXT_NEW_CHANGE_ORDER;
         System.assertEquals(tu.clientTasks.size() , coController.getTaskListWrapper().size());
         
         // create new change order
         List<String> liSelectedTasks = new List<String>();
         for(Client_Task__c task : tu.clientTasks) {
            liSelectedTasks.add(task.Id);           
         }
         coController.selectedTasks = Json.serialize(liSelectedTasks);
         coController.changeOrderName = 'Unit Test Change Order';
         Pagereference pr =  coController.createNewChangeOrder();
         system.assertNotEquals(null, pr);

        // then
        List<Change_Order__c> liChangeOrders = [select Id, Name from Change_Order__c where Name = 'Unit Test Change Order'];
        System.assertEquals(1, liChangeOrders.size());
        
        // check if there was create change order item for each task
        List<Change_Order_Item__c> liChangeOrderOItems = [select Change_Order__c, Client_Task__c, Id, Type__c from Change_Order_Item__c 
                                    where Change_Order__c = :liChangeOrders.get(0).id];
        Map<Id, Change_Order_Item__c> mapChangeOrderItems = new Map<Id, Change_Order_Item__c> ();
        for (Change_Order_Item__c coItem : liChangeOrderOItems){
            if (mapChangeOrderItems.containsKey(coItem.Client_Task__c)){
                System.assertEquals('Ok', 'There were more than 1 change order item for crct');
            }
            mapChangeOrderItems.put(coItem.Client_Task__c, coItem);
        }
        
        for (Client_Task__c task : tu.clientTasks){
            system.assert(mapChangeOrderItems.containsKey(task.Id));
        }
        test.stopTest();
    }
    
    static testMethod void shouldDisplayChangeDatePage() {
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        PRA_ChangeOrderController coController = new PRA_ChangeOrderController();
        coController.setSelectedProject(tu.clientProject.Id);
        coController.selectedAction = PRA_ChangeOrderController.TEXT_NEW_CHANGE_ORDER;
         // create new change order
         List<String> liSelectedTasks = new List<String>();
         for (Client_Task__c task : tu.clientTasks){
            liSelectedTasks.add(task.Id);           
         }
         coController.selectedTasks = Json.serialize(liSelectedTasks);
         coController.changeOrderName = 'Unit Test Change Order';
         Pagereference pr =  coController.createNewChangeOrder();
        
        // when
        test.startTest();
        Change_Order__c changeOrder = [select id, Name from Change_Order__c where Name = 'Unit Test Change Order'];
        coController.setSelectedProject(tu.clientProject.Id);
        coController.selectedAction = PRA_ChangeOrderController.TEXT_MODIFY_END_DATE;
        coController.changeOrderId = changeOrder.Id;
        
        // then
        Pagereference expectedPGRef = Page.PRA_Change_Order_EndDate;
        expectedPGRef.getParameters().put('id', changeOrder.Id);
        Pagereference pgRef = coController.selectChangeOrder();
        
        system.assertNotEquals(null , pgRef);
        system.assertEquals(expectedPGRef.getUrl() , pgRef.getUrl());
        
        test.stopTest();
    }
    
    static testMethod void shouldDisplayExistingChangeOrder() {
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        PRA_ChangeOrderController coController = new PRA_ChangeOrderController();
        coController.setSelectedProject(tu.clientProject.Id);
        coController.selectedAction = PRA_ChangeOrderController.TEXT_NEW_CHANGE_ORDER;
        // create new change order
        List<String> liSelectedTasks = new List<String>();
        for (Client_Task__c task : tu.clientTasks){
            liSelectedTasks.add(task.Id);           
         }
        coController.selectedTasks = Json.serialize(liSelectedTasks);
        coController.changeOrderName = 'Unit Test Change Order';
        Pagereference pr =  coController.createNewChangeOrder();
        
        // when
        test.startTest();
        Change_Order__c changeOrder = [select Id, Name from Change_Order__c where Name = 'Unit Test Change Order'];
        coController.setSelectedProject(tu.clientProject.Id);
        coController.selectedAction = PRA_ChangeOrderController.TEXT_MODIFY_CHANGE_ORDER;
        coController.changeOrderId = changeOrder.Id;
        
        // then
        Pagereference expectedPGRef = Page.PRA_Change_Order_Display_Modify;
        expectedPGRef.getParameters().put('id', changeOrder.id);
        Pagereference pgRef = coController.selectChangeOrder();
        
        system.assertNotEquals(null , pgRef);
        system.assertEquals(expectedPGRef.getUrl() , pgRef.getUrl());
        
        test.stopTest();
        
    }
    
    static testMethod void shouldAddCrctsToChangeOrder() {
          // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        //createBidContractDetails();
        
        PRA_ChangeOrderController coController = new PRA_ChangeOrderController();
        coController.setSelectedProject(tu.clientProject.Id);
        coController.selectedAction = PRA_ChangeOrderController.TEXT_NEW_CHANGE_ORDER;
        coController.changeOrderName = 'Unit Test Change Order';
        List<String> liSelectedTasks = new List<String>();
        
         // create new change order without crcts
         coController.selectedTasks = Json.serialize(liSelectedTasks);
         Pagereference pr =  coController.createNewChangeOrder();
        
        // when
        test.startTest();
        Change_Order__c changeOrder = [select Id, Name from Change_Order__c where Name = 'Unit Test Change Order'];
        coController.setSelectedProject(tu.clientProject.Id);
        List<Change_Order_Item__c> liChangeOrderOItems = [select Change_Order__c, Client_Task__c, Id, Type__c from Change_Order_Item__c 
                                    where Change_Order__c = :changeOrder.id];
        system.assertEquals(0, liChangeOrderOItems.size());
        system.assertEquals(1, coController.getProjectChangeOrders().size());
                                    
        //then
        coController.selectedAction = PRA_ChangeOrderController.TEXT_ADD_UNITS;
        coController.changeOrderId = changeOrder.Id;
        
        system.assertEquals(tu.clientTasks.size(),coController.getTaskListWrapper().size());
        Pagereference expectedPGRef = Page.PRA_Change_Order_Display_Modify;
        expectedPGRef.getParameters().put('id', changeOrder.Id);
        
         for(Client_Task__c task : tu.clientTasks){
            liSelectedTasks.add(task.Id);           
         }
         coController.selectedTasks = Json.serialize(liSelectedTasks);
        
        Pagereference pgRef = coController.addChangeOrderItems();
        
        system.assertNotEquals(null , pgRef);
        system.assertEquals(expectedPGRef.getUrl() , pgRef.getUrl());
        
        liChangeOrderOItems = [select Change_Order__c, Client_Task__c, Id, Type__c from Change_Order_Item__c 
                                    where Change_Order__c = :changeOrder.id];
        Map<Id, Change_Order_Item__c> mapChangeOrderItems = new Map<Id, Change_Order_Item__c> ();
        for(Change_Order_Item__c coItem : liChangeOrderOItems) {
            if(mapChangeOrderItems.containsKey(coItem.Client_Task__c)) {
                System.assertEquals('Ok', 'There were more than 1 change order item for crct');
            }
            mapChangeOrderItems.put(coItem.Client_Task__c, coItem);
        }
        
        for(Client_Task__c task : tu.clientTasks) {
            system.assert(mapChangeOrderItems.containsKey(task.Id));
        }
        
        test.stopTest();
        
    }
    
    static testMethod void shouldIntializeProjectWhenIdGiven() {
        // given
        Client_Project__c project = new Client_Project__c(Name='Test change order project');
        insert project;
        
        // when
        Apexpages.currentPage().getParameters().put('id', project.Id);
        PRA_ChangeOrderController coController = new PRA_ChangeOrderController();
        
        // then
        system.assertEquals(project.Id, coController.project.Id);
        
    }
    
    static testMethod void shouldDeleteExistingChangeOrder() {
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();

        Change_Order__c changeOrder = tu.createChangeOrder(tu.clientProject.id);
        
        // when
        PRA_ChangeOrderController coController = new PRA_ChangeOrderController();
        coController.setSelectedProject(tu.clientProject.Id);
        
        // then
        // check if change order is on list
        system.assertEquals(1, coController.selectedChangeOrdersWrappers.size());
        system.assertEquals(changeOrder.Id, coController.selectedChangeOrdersWrappers.get(0).changeOrder.Id);
        
        // check that there is 1 change order for project
        List<Change_Order__c> coList = [select Id, Name from Change_Order__c where Client_Project__c = :tu.clientProject.Id];
        system.assertEquals(1, coList.size());
        
        PRA_ChangeOrderController.ChangeOrderWrapper delCOWrap = new PRA_ChangeOrderController.ChangeOrderWrapper(changeOrder);
        delCOWrap.isSelected = true;
        
        coController.selectedChangeOrdersWrappers = new List<PRA_ChangeOrderController.ChangeOrderWrapper> {delCOWrap};
        coController.deleteSelectedChangeOrders();
        
        // check that change order has been deleted
        coList = [select Id, Name from Change_Order__c where Client_Project__c = :tu.clientProject.Id];
        system.assertEquals(0, coList.size());
    }
    
    static testMethod void shouldDoTestCoveradge() {
        // given
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        PRA_ChangeOrderController coController = new PRA_ChangeOrderController();
        coController.setSelectedProject(tu.clientProject.Id);
        
        // when
        
        // then
        system.assertEquals('Executed', PRA_ChangeOrderController.CHANGE_ORDER_STATUS_EXECUTED);
        system.assertEquals('New', PRA_ChangeOrderController.CHANGE_ORDER_ITEM_STATUS_NEW);
        system.assertEquals(tu.clientProject.Id, coController.getSelectedProject());
        system.assertEquals( Page.PRA_Change_Order.getUrl(), coController.reset().getUrl());
        
        List<Client_Project__c> projects = PRA_ChangeOrderController.searchProject(tu.clientProject.Name);
        
        system.assertEquals(1, projects.size());
        
    }
    
    public static void createBidContractDetails(){
        List<Bid_Contract_Detail__c> inDetails =  new List<Bid_Contract_Detail__c>();
        List<Unit_Effort__c> unitEfforts = [select Id, Client_Task__c, Client_Task__r.Total_Units__c, 
                                            Client_Task__r.Contract_Date__c, Client_Task__r.Average_Contract_Effort__c, Client_Task__r.Total_Contract_Hours__c, 
                                            Client_Task__r.Total_Contract_Units__c, (select BUF_Code__c from Hour_EffortRatios__r)
                                            from Unit_Effort__c];
        
        for(Unit_Effort__c ue : unitEfforts) {
            for(Hour_EffortRatio__c hrER : ue.Hour_EffortRatios__r) {
                Bid_Contract_Detail__c bd = new Bid_Contract_Detail__c();
                bd.Client_Task__c = ue.Client_Task__c;
                bd.BUF_Code__c = hrER.BUF_Code__c;
                bd.Contracted_Hours__c = ue.Client_Task__r.Total_Contract_Hours__c;
                bd.Contracted_Value__c = ue.Client_Task__r.Total_Contract_Units__c;
                inDetails.add(bd);
            }
        }
        insert inDetails;
    }
}