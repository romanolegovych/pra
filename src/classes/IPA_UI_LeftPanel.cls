public class IPA_UI_LeftPanel
{
    public String leftMenuHTML {get; set;}
    public String UserName {get; set;}
    
    public String lbl1 {get; set;}
    public String lbl2 {get; set;}
    public String lbl3 {get; set;}
    public String lbl4 {get; set;}
    public String lbl5 {get; set;}
    
    public String lnk1 {get; set;}
    public String lnk2 {get; set;}
    public String lnk3 {get; set;}
    public String lnk4 {get; set;}
    public String lnk5 {get; set;}
    
    public String bkuplbl1 {get; set;}
    public String bkuplbl2 {get; set;}
    public String bkuplbl3 {get; set;}
    public String bkuplbl4 {get; set;}
    public String bkuplbl5 {get; set;}
    
    public String bkuplnk1 {get; set;}
    public String bkuplnk2 {get; set;}
    public String bkuplnk3 {get; set;}
    public String bkuplnk4 {get; set;}
    public String bkuplnk5 {get; set;}
    
    public String order {get; set;}
    
    //Constructor
    public IPA_UI_LeftPanel()
    {
        
    }
    
    public void loadLeftPanel()
    {
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        
        UserName = null;
        
        If(UserInfo.getUserId() != null & UserInfo.getUserType() != 'Guest')
        {   
            UserName = UserInfo.getFirstName() + ' ' + UserInfo.getLastName();
            
            //Load User's Custom Links
            List<IPA_Custom_Links__c> lstLinks = ipa_bo.returnCustomLinks(UserInfo.getUserId());
             
            //Load all Menu Items
            leftMenuHTML = ipa_bo.returnLeftMenuLinks(UserInfo.getUserId());
            
            integer i = 1;
            for(IPA_Custom_Links__c l: lstLinks)
            {    
                if(i == 1) { lnk1 = l.Link__c; lbl1 = l.Label__c; bkuplnk1 = l.Link__c; bkuplbl1 = l.Label__c;}
                if(i == 2) { lnk2 = l.Link__c; lbl2 = l.Label__c; bkuplnk2 = l.Link__c; bkuplbl2 = l.Label__c;}
                if(i == 3) { lnk3 = l.Link__c; lbl3 = l.Label__c; bkuplnk3 = l.Link__c; bkuplbl3 = l.Label__c;}
                if(i == 4) { lnk4 = l.Link__c; lbl4 = l.Label__c; bkuplnk4 = l.Link__c; bkuplbl4 = l.Label__c;}
                if(i == 5) { lnk5 = l.Link__c; lbl5 = l.Label__c; bkuplnk5 = l.Link__c; bkuplbl5 = l.Label__c;}
                i = i + 1;
            }
        }
        else
            leftMenuHTML = ipa_bo.returnLeftMenuLinks('');
    }
    
    //Save Custom Links
    public PageReference saveCustomLinks()
    {   
        String[] chars = new String[]{};
        if(Order == null || Order == '')
            Order = '12345';        
        System.debug('****Order = [' + Order + ']');
         
        List<IPA_Custom_Links__c> lstLinks = new List<IPA_Custom_Links__c>();
        IPA_Custom_Links__c l;
                     
        if(lbl1 != '' && lnk1 != '') {
            l = new IPA_Custom_Links__c();
            l.Label__c = lbl1; l.Link__c = lnk1; l.Order__c = Integer.valueOf(Order.indexOf('1')) + 1; l.User__c = UserInfo.getUserId();
            lstLinks.add(l);
        }        
        if(lbl2 != '' && lnk2 != '') {
            l = new IPA_Custom_Links__c();
            l.Label__c = lbl2; l.Link__c = lnk2; l.Order__c = Integer.valueOf(Order.indexOf('2')) + 1; l.User__c = UserInfo.getUserId();
            lstLinks.add(l);
        }        
        if(lbl3 != '' && lnk3 != '') {
            l = new IPA_Custom_Links__c();
            l.Label__c = lbl3; l.Link__c = lnk3; l.Order__c = Integer.valueOf(Order.indexOf('3')) + 1; l.User__c = UserInfo.getUserId();
            lstLinks.add(l);
        }        
        if(lbl4 != '' && lnk4 != '') {
            l = new IPA_Custom_Links__c();
            l.Label__c = lbl4; l.Link__c = lnk4; l.Order__c = Integer.valueOf(Order.indexOf('4')) + 1; l.User__c = UserInfo.getUserId();
            lstLinks.add(l);
        }        
        if(lbl5 != '' && lnk5 != '') {
            l = new IPA_Custom_Links__c();
            l.Label__c = lbl5; l.Link__c = lnk5; l.Order__c = Integer.valueOf(Order.indexOf('5')) + 1; l.User__c = UserInfo.getUserId();
            lstLinks.add(l);
        }
                
        IPA_BO_LinksManagement ipa_bo = new IPA_BO_LinksManagement();
        ipa_bo.saveCustomLinks(lstLinks, UserInfo.getUserId());
        
        //return null;
        PageReference self = Page.IPA_Dashboard;
        self.setRedirect(true);
        return self ;
    }
}