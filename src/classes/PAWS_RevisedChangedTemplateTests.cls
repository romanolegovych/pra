/**
*   'PAWS_RevisedChangedTemplateTests' is the test class for PAWS_RevisedChangedTemplateController
*   Test classes are used to write the unit test and cover the code coverage of the code.
*   @author   Work Relay
*/
@isTest 
public with sharing class PAWS_RevisedChangedTemplateTests 
{
	static testmethod void run()
    {
    	PAWS_ApexTestsEnvironment.init();
    	
    	PAWS_RevisedChangedTemplateController controller = new PAWS_RevisedChangedTemplateController();
    	controller.MessageId = PAWS_ApexTestsEnvironment.EmailMessage.Id;
    	
    	System.Assert(controller.Message != null);
    	System.Assert(controller.Projects != null);
    }
}