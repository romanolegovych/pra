/** 
     * @description Test class CM_ReceiveSignedAgreement - Test Class to Configure Email Service to Handle Inbound Emails for Contract Management Application
                          Global class to Handle Inbound Emails - Implements Inbound Email Handler
                          Attaches the Files form the Email to the Notes and Attachment Statement and changes the status 
                          of the Agreement Record as specified in the Subject
     * @author      Karthikeyan Sounderrajan
     * @date        Created: 27-Feb-2015
*/

global class CM_ReceiveSignedAgreement implements Messaging.InboundEmailHandler {
    
/* Global class to Handle Inbound Emails - Implements Inbound Email Handler
 Has Two parameters - Email and Envelope*/

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email,
                                                         Messaging.Inboundenvelope envelope) {
           Apttus__APTS_Agreement__c Agreement;
           Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
           try {
               List<String> subjectString=email.subject.split('\\|');
               String agreementnumber='';
               String PRAStatus='';
               String Comments='';
               String messageresult='';
               
               if(subjectString.size()>1){
                 agreementnumber=subjectString[0];
                 PRAStatus=subjectString[1];
                 Comments=subjectString[2];
               } 
               
               //Get the Agreement Id to Update
               if(agreementnumber==null){
                    messageresult='Please Specify the agreement number. \r\n';
               }
               else{
               
               Agreement=[Select id, name, PRA_Status__c, comments__c from Apttus__APTS_Agreement__c where Apttus__FF_Agreement_Number__c=:agreementnumber.trim() limit 1];
               
               if(Agreement==null){
                    messageresult='Please provide the valid agreement number. \r\n';
               }
               else {
                
               List<Attachment> AttachmentList = new List<Attachment>();
               List<Attachment> TextAttachmentList = new List<Attachment>();
               
               list<String> binaryfileNameSplit=new List<string>();
               List<String> TextfileNameSplit =new List<String>();
               Apttus__APTS_Agreement__c oldAttachments = [SELECT Id,Name,(SELECT Id,Name FROM Attachments) FROM Apttus__APTS_Agreement__c WHERE id=:Agreement.Id limit 1];
               Set<String> filenames=new Set<String>();
               for(Attachment attmn :oldAttachments.Attachments){
                   filenames.add(attmn.name);
               }
               
               //Handles the Attachment - Binary and Text Attachments
               if(email.binaryAttachments!=null){
                for (Messaging.Inboundemail.BinaryAttachment bAttachment : email.binaryAttachments) {
                        Attachment attachment = new Attachment();
                        attachment.Name = bAttachment.fileName;
                        binaryfileNameSplit=bAttachment.fileName.split('\\.');
                        
                        if(binaryfileNameSplit.size()>0){
                            Integer Last =binaryfileNameSplit.Size() -1;
                            if(filenames.contains(bAttachment.fileName)){
                                List<String> fileNameWOExtension= bAttachment.fileName.split('\\.' +binaryfileNameSplit[Last]);                        
                                attachment.Name=fileNameWOExtension[0] + ' [' + String.valueof(DateTime.Now().format('dd-MM-yy HH:mm:ss')) +'].' +binaryfileNameSplit[Last];
                            }
                            attachment.Body = bAttachment.body;
                            attachment.ParentId = Agreement.Id;
                            if(binaryfileNameSplit[Last]=='png' || binaryfileNameSplit[Last]=='jpg' || binaryfileNameSplit[Last]=='jpeg'
                            || binaryfileNameSplit[Last]=='jpe' || binaryfileNameSplit[Last]=='jfif' || binaryfileNameSplit[Last]=='bmp'
                            || binaryfileNameSplit[Last]=='dib' || binaryfileNameSplit[Last]=='gif' || binaryfileNameSplit[Last]=='tif'
                            || binaryfileNameSplit[Last]=='tiff'
                            ){}
                            else{
                              AttachmentList.add(attachment);
                            }
                        }
                        else{
                            messageresult=messageresult+'Email attachment contains Invalid File Name: ' + bAttachment.fileName +' \r\n';
                        }
                }
                if(AttachmentList.size()>0 ){           
                        insert AttachmentList;
                }
               }
               
               // Text Attchment
               if(email.textAttachments!=null){
                for (Messaging.Inboundemail.TextAttachment tAttachment : email.textAttachments) {
                        Attachment Textattachment = new Attachment();
                        Textattachment.Name = tAttachment.fileName;
                        TextfileNameSplit=tAttachment.fileName.split('\\.');
                        
                        if(TextfileNameSplit.size()>0){
                            Integer LastP =TextfileNameSplit.Size() -1;
                            if(filenames.contains(tAttachment.fileName)){
                                List<String> fileNameWOExtension= tAttachment.fileName.split('\\.' +TextfileNameSplit[LastP]); 
                                Textattachment.Name=fileNameWOExtension[0] + ' [' + String.valueof(DateTime.Now().format('dd-MM-yy HH:mm:ss')) + '].' +TextfileNameSplit[LastP];
                            }
                            Textattachment.Body = Blob.valueOf(tAttachment.body);
                            Textattachment.ParentId = Agreement.Id;
                            if(TextfileNameSplit[LastP]=='png' || TextfileNameSplit[LastP]=='jpg' || TextfileNameSplit[LastP]=='jpeg'
                            || TextfileNameSplit[LastP]=='jpe' || TextfileNameSplit[LastP]=='jfif' || TextfileNameSplit[LastP]=='bmp'
                            || TextfileNameSplit[LastP]=='dib' || TextfileNameSplit[LastP]=='gif' || TextfileNameSplit[LastP]=='tif'
                            || TextfileNameSplit[LastP]=='tiff'
                            ){}
                            else{
                                TextAttachmentList.add(Textattachment);
                            }
                        }
                        else{
                            messageresult=messageresult+'Email attachment contains Invalid File Name: ' + tAttachment.fileName +' \r\n';
                        }
                }
                if(TextAttachmentList.size()>0){
                        insert TextAttachmentList;
                }
               }
               if(TextAttachmentList.size()>0 || AttachmentList.size()>0){
                messageresult=messageresult+'The Agreement Document has been attached in the Notes and Attachment Section. \r\n';
               }
               
               //Update PRA Status
               if(Comments!=''){
                    Agreement.Comments__c=Comments;
                    messageresult=messageresult+'Comments value set to :' + Comments +'.\r\n';
               }
               
               //Update the Agreement Record
               if(PRAStatus!='' &&
               (PRAStatus=='Template Available' || PRAStatus=='Template Sent to Site' || PRAStatus=='At Site-Review' ||
                PRAStatus=='At PRA-CA'||PRAStatus=='At PRA-Signature'||PRAStatus=='At Sponsor-Review'||
PRAStatus=='At Agency or Review Board' || PRAStatus=='Negotiation Complete' || PRAStatus=='At Site-Signature'|| 
PRAStatus=='At Sponsor-Signature' ||PRAStatus=='Partially Executed' ||PRAStatus=='Fully Executed' ||
PRAStatus=='At PRA-Clinical' || PRAStatus=='At PRA-PM/PZ' || PRAStatus=='At Translator' ||
PRAStatus=='On Hold' || PRAStatus=='Dropped' || PRAStatus=='At PRA-Legal Counsel' || PRAStatus=='Escalated' || 
PRAStatus=='Sent for Payments'))
                    {
                        Agreement.PRA_Status__c=PRAStatus;
                        messageresult=messageresult+'Status value set to :' +PRAStatus +'.\r\n';               
                    }
                    else {
                        messageresult=messageresult+ ' Please Provide a Valid Status to Update.\r\n';
                    }
               
                if(PRAStatus!='' || Comments!=''){                    
                        //Set the Agreement Status
                        update Agreement;
                }
                messageresult =messageresult+'Note: Images/Picture files (like jpg, png, gif) will not be uploaded in the notes and attachment section. \r\n  ';
                result.success = true;
                result.message= messageresult;
               }
               }
            }catch(Exception e) {
                result.success = false;
                result.message = 'Failed to process the message successfully. Please verify the Email Subject. \r\n' +
                                 'Please Specify the subject in the following format for the message to be processed: \r\n' + 
                                 '<Agreement Number>|<Status>|<Comment>   \r\n'  +
                                 'example:00001234.5|Partially Executed|Site 001 executed the Agreement \r\n'+
                                 '-------------------------------------------------\r\n' +
                                 'Note: Subject should contain valid Agreement Number at first (Totally 8 digit with one decimal 12345678.9).\r\n' + 
                                 'Remove the additional characters RE: /FW: in the subject line while replying/forwarding the Message. \r\n'+
                                 'Subject line should not contain any additional spaces/characters.\r\n ' +
                                 'If issue still unresolved report the following exception message :' +e ;
            }
            return result;
    }
}