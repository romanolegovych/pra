/** Implements the Service Layer of the object LaboratoryMethodCompound__c
 * @author	Dimitrios Sgourdos
 * @version	19-Nov-2013
 */
public with sharing class LaboratoryMethodCompoundService {
	
	/** Create a LaboratoryMethodCompound__c instance with the given name and relate it to the given LaboratoryMethod__c record
	 * @author	Dimitrios Sgourdos
	 * @version 10-Oct-2013
	 * @param	labMethod			The LaboratoryMethod__c that the compound will be connected
	 * @param	compoundName		The name of the compound
	 * @return	The LaboratoryMethod__c instance
	 */
	public static LaboratoryMethodCompound__c createLaboratoryMethodCompoundInstance(LaboratoryMethod__c labMethod, String compoundName) {
		return ( new LaboratoryMethodCompound__c(Name=compoundName, LaboratoryMethod__c=labMethod.Id) );
	}
	
	
	/**	Get the data about laboratory method - laboratory method compound assignment with the given studies.
	 * @author	Dimitrios Sgourdos
	 * @version	11-Oct-2013
	 * @param	studiesList			The list of studies to retrieve the assignments
	 * @return	The list of the wrapper that holds the data.
	 */
	public static List<MethodCompoundStudyAssignmentContainer> getMethodCompoundStudyAssignmentContainerData(List<Study__c> studiesList) {
		List<MethodCompoundStudyAssignmentContainer> containerWrapperList = new List<MethodCompoundStudyAssignmentContainer>(); 
		
		// Read the initial needed data
		List<LaboratoryMethodCompound__c> compoundsList = LaboratoryMethodCompoundDataAccessor.getUsedByUserStudies(studiesList);
		
		// Create the data for the MethodCompoundStudyAssignmentContainer list
		for(LaboratoryMethodCompound__c tmpCompound : compoundsList ) {
			MethodCompoundStudyAssignmentContainer newWrapperItem = new MethodCompoundStudyAssignmentContainer();
			newWrapperItem.methodCompound = tmpCompound;
			newWrapperItem.analysisList = LaboratoryAnalysisService.reorderLaboratoryAnalysisListByStudiesList(
																				tmpCompound.LaboratoryAnalysis__r,
																				studiesList,
																				tmpCompound
											);
			containerWrapperList.add(newWrapperItem);
		}
		
		return containerWrapperList;
	}
	
	
	/**	Save the data about laboratory method - laboratory method compound assignment with studies.
	 * @author	Dimitrios Sgourdos
	 * @version	16-Oct-2013
	 * @param	containerWrapperList	The wrapper list that holds the data
	 * @return	If the save was succesfull or not 
	 */
	public static Boolean saveMethodCompoundStudyAssignment(List<MethodCompoundStudyAssignmentContainer> containerWrapperList) {
		List<LaboratoryAnalysis__c> updateList = new List<LaboratoryAnalysis__c>();
		
		// Iterate through all lines of the container
		for(MethodCompoundStudyAssignmentContainer tmpWrapperItem : containerWrapperList) {
			// Iterate through all analysis
			for(LaboratoryAnalysis__c tmpAnalysis : tmpWrapperItem.analysisList) {
				updateList.add(tmpAnalysis);
			}
		}
		
		// Save the analysis
		try {
			upsert updateList;
		} catch (Exception e) {
			return false;
		}
		
		return true;
	}
	
	
	/**	Retrieve the data about laboratory method - laboratory method compound assignment with studies that must be deleted.
	 * @author	Dimitrios Sgourdos
	 * @version	22-Oct-2013
	 * @param	containerWrapperList	The wrapper list that holds the data
	 * @param	recordIndex				The index of the compound that the whole analysis of its method will be deleted
	 * @return	The list of analysis that must be deleted 
	 */
	public static List<LaboratoryAnalysis__c> deleteMethodCompoundStudyAssignment(List<MethodCompoundStudyAssignmentContainer> containerWrapperList, Integer recordIndex) {
		List<LaboratoryAnalysis__c> deletedList = new List<LaboratoryAnalysis__c>();
		
		// Check for bad parameters
		if(recordIndex >= containerWrapperList.size()) {
			return deletedList;
		}
		
		// Add to the deletion list all the analysis associated with the method
		String deletedMethodName = containerWrapperList[recordIndex].methodCompound.LaboratoryMethod__r.Name;
		
		for(Integer i=recordIndex; i<containerWrapperList.size(); i++) {
			if(  containerWrapperList[i].methodCompound.LaboratoryMethod__r.Name == deletedMethodName ) {
				deletedList.addAll(containerWrapperList[i].analysisList);
			} else { // then there are not anymore compounds of this method 
				break;
			}
		}
		
		return deletedList;
	}
	
	
	/**	Remove all the records that have indexes from startRecordIndex to endRecordIndex from a MethodCompoundStudyAssignmentContainer list.
	 * @author	Dimitrios Sgourdos
	 * @version	22-Oct-2013
	 * @param	containerWrapperList	The wrapper list that holds the data
	 * @param	startRecordIndex		The index of the first record that will be removed
	 * @param	endRecordIndex			The index of the last record that will be removed
	 * @return	The MethodCompoundStudyAssignmentContainer list without the removed records 
	 */
	public static List<MethodCompoundStudyAssignmentContainer> removeRecordsFromMethodCompoundStudyAssignmentContainer(
																		List<MethodCompoundStudyAssignmentContainer> containerWrapperList,
																		Integer startRecordIndex,
																		Integer endRecordIndex) {
		// Declare variables
		Integer startRecord;
		
		// Check the given indexes
		if( startRecordIndex >= 0 && startRecordIndex < containerWrapperList.size() 
				&& endRecordIndex >= 0 && endRecordIndex < containerWrapperList.size() 
				&& startRecordIndex <= endRecordIndex) {
			// Remove the records from last to start index to avoid omitting records 
			for(Integer i=endRecordIndex; i>=startRecordIndex; i--) {
				containerWrapperList.remove(i);
			}
		}
		
		return containerWrapperList;
	}
	
	
	/**	Determine if there is at least one compound unassigned in the data about laboratory method - laboratory method compound assignment with studies.
	 * @author	Dimitrios Sgourdos
	 * @version	17-Oct-2013
	 * @param	containerWrapperList	The wrapper list that holds the data
	 * @return	If there is at least one compound unassigned or not 
	 */
	public static Boolean checkForUnassignedCompoundToStudyAssignment(List<MethodCompoundStudyAssignmentContainer> containerWrapperList) {
		// Iterate through the wrapper list
		for(MethodCompoundStudyAssignmentContainer tmpWrapperItem : containerWrapperList) {
			Boolean unassignedCompoundFlag = true;
			
			// Iterate through the analysis
			for(LaboratoryAnalysis__c tmpAnalysis : tmpWrapperItem.analysisList) {
				// If the compound is assigned then break the interior loop
				if(tmpAnalysis.MethodDevelopment__c || tmpAnalysis.MethodValidation__c || tmpAnalysis.Analysis__c) {
					unassignedCompoundFlag = false;
					break;
				}
			}
			
			// If the compound is unassigned then there is at least one unassigned compound, then return true
			if(unassignedCompoundFlag) {
				return true;
			}
		}
		
		// Here all the compounds are assigned
		return false;
	}
	
	
	/**	Determine if all the compound names are blank inside a laboratory method compounds list.
	 * @author	Dimitrios Sgourdos
	 * @version	17-Oct-2013
	 * @param	compoundList		The laboratory method compounds list that holds the data
	 * @return	If all the compound names are blank or not 
	 */
	public static Boolean allBlankNamesInLabCompoundsList(List<LaboratoryMethodCompound__c> compoundList) {
		for(LaboratoryMethodCompound__c tmpItem : compoundList) {
			if(tmpItem.Name!='' && tmpItem.Name!=null) {
				return false;
			}
		}
		
		return true;
	}
	
	
	/** Create new laboratory method compounds from search criteria.
	 * @author   Dimitrios Sgourdos
	 * @version  21-Oct-2013
	 * @param    compoundList			The laboratory method compounds which holds the search criteria about compounds
	 * @param    labMethodId			The id of the laboratory method which the compounds will be assigned with
	 * @return   A code that the creation was successful, else the predefined error code.
	 */
	public static Boolean createNewLaboratoryMethodCompoundsFromSearchCriteria( List<LaboratoryMethodCompound__c> compoundList, 
																				String labMethodId) {
		List<LaboratoryMethodCompound__c> saveList = new List<LaboratoryMethodCompound__c>();
		
		for(LaboratoryMethodCompound__c tmpItem : compoundList) {
			if(tmpItem.Name!='' && tmpItem.Name!=null) {
				tmpItem.Id = (tmpItem.Id!=NULL)? NULL : tmpItem.Id;
				tmpItem.LaboratoryMethod__c = labMethodId;
				String tmpStr = tmpItem.Name;
				tmpStr = tmpStr.replaceAll(':',';');
				tmpItem.Name = tmpStr;
				saveList.add(tmpItem);
			}
		}
		
		try {
			insert saveList;
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
	
	/** Determine if a laboratory method exists in the given laboratory method - laboratory method compound assignment with studies wrapper list.
	 * @author	Dimitrios Sgourdos
	 * @version 22-Oct-2013
	 * @param	containerWrapperList	The wrapper list that holds the data
	 * @param	labMethod				The laboratory method that will be tested for existence
	 * @return	if the laboratory method exists in the assignment data or not.
	 */
	public static Boolean existMethodInMethodCompoundStudyAssignmentContainerData(
																List<MethodCompoundStudyAssignmentContainer> containerWrapperList, 
																LaboratoryMethod__c labMethod) {
		for(MethodCompoundStudyAssignmentContainer tmpWrapperItem : containerWrapperList) {
			if(tmpWrapperItem.methodCompound.LaboratoryMethod__r.Name == labMethod.Name) {
				return true;
			}
		}
		
		return false;
	}
	
	
	/** Add a laboratory method in the given laboratory method - laboratory method compound assignment with studies wrapper list.
	 * @author	Dimitrios Sgourdos
	 * @version	22-Oct-2013
	 * @param	containerWrapperList	The wrapper list that holds the data
	 * @param	studiesList				The user selected studies
	 * @param	labMethod				The laboratory method that will be tested for existence
	 * @return  The new assignment wrapper list.
	 */
	public static List<MethodCompoundStudyAssignmentContainer> addMethodToMethodCompoundStudyAssignmentContainerData(
																			List<MethodCompoundStudyAssignmentContainer> containerWrapperList,
																			List<Study__c> studiesList, 
																			LaboratoryMethod__c labMethod) {
		// If there isn't meaning return
		if( existMethodInMethodCompoundStudyAssignmentContainerData(containerWrapperList, labMethod) ) {
			return containerWrapperList;
		}
		
		// Retrieve the compounds cause access to LaboratoryMethod__r and LaboratoryAnalysis__r is needed
		List<LaboratoryMethodCompound__c> compoundList = LaboratoryMethodCompoundDataAccessor.getByLabMethod(labMethod.Id, studiesList);
			
		// Create a record for each compound of the method
		for(LaboratoryMethodCompound__c tmpCompound : compoundList) {
			// Initialize
			MethodCompoundStudyAssignmentContainer newWrapperItem = new MethodCompoundStudyAssignmentContainer();
			newWrapperItem.methodCompound = tmpCompound;
			
			// Create analysis for each study
			for(Study__c tmpStudy : studiesList) {
				LaboratoryAnalysis__c tmpAnalysis = LaboratoryAnalysisService.createLaboratoryAnalysisInstance(tmpCompound,
																												tmpStudy,
																												false,
																												false,
																												false
													);
				newWrapperItem.analysisList.add(tmpAnalysis);
			}
			
			// Add the record
			containerWrapperList.add(newWrapperItem);
		}
		
		return containerWrapperList;
	}
	
	
	/** Map a method-compound assignment with studies wrapper list to the laboratory methods.
	 * @author	Dimitrios Sgourdos
	 * @version	19-Nov-2013
	 * @param	initialList			The wrapper list that holds the data
	 * @return  The mapped methods and compounds to study container.
	 */
	public static Map<String, List<MethodCompoundStudyAssignmentContainer>> mapCompoundStudyAssignmentsToMethods(List<MethodCompoundStudyAssignmentContainer> initialList) {
		Map<String, List<MethodCompoundStudyAssignmentContainer>> results = new Map<String, List<MethodCompoundStudyAssignmentContainer>>();
		
		for(MethodCompoundStudyAssignmentContainer tmpItem : initialList) {
			List<MethodCompoundStudyAssignmentContainer> newList = new List<MethodCompoundStudyAssignmentContainer>();
			String key = tmpItem.methodCompound.LaboratoryMethod__c;
			if( results.containsKey(key) ) {
				newList = results.remove(key);
			}
			newList.add(tmpItem);
			results.put(key, newList);
		}
		
		return results;
	}
	
	
	/** Read the MethodCompoundStudyAssignmentMappedToMethodWrapper data through the given
	 *	MethodCompoundStudyAssignmentContainer data.
	 * @author	Dimitrios Sgourdos
	 * @version	19-Nov-2013
	 * @param	initialList			The laboratory method compounds with the analysis that is assigned to them for the selected studies
	 * @return  The MethodCompoundStudyAssignmentMappedToMethodWrapper data.
	 */
	public static List<MethodCompoundStudyAssignmentMappedToMethod> getMethodDetailsData(List<MethodCompoundStudyAssignmentContainer> initialList) {
		List<MethodCompoundStudyAssignmentMappedToMethod> results = new List<MethodCompoundStudyAssignmentMappedToMethod>();
		
		// Map method-compound assignment with studies wrapper list to methods
		Map<String, List<MethodCompoundStudyAssignmentContainer>> assignmentsMap = mapCompoundStudyAssignmentsToMethods(initialList);
		
		// Read laboratory methods
		List<LaboratoryMethod__c> methodsList = LaboratoryMethodDataAccessor.getBySetIds (assignmentsMap.keySet());
		
		// Create wrapper
		for(LaboratoryMethod__c tmpMethod : methodsList) {
			MethodCompoundStudyAssignmentMappedToMethod newItem = new MethodCompoundStudyAssignmentMappedToMethod();
			newItem.labMethod = tmpMethod;
			newItem.assignemtsToStudy = assignmentsMap.remove(tmpMethod.Id);
			results.add(newItem);
		}
		
		return results;
	}
	
	
	/**	This class keeps a laboratory method compound with the analysis that is assigned to it for the selected studies.
	 * @author	Dimitrios Sgourdos
	 * @version	19-Nov-2013
	 */
	public class MethodCompoundStudyAssignmentContainer {
		public LaboratoryMethodCompound__c	methodCompound {get;set;}
		public List<LaboratoryAnalysis__c>	analysisList   {get;set;}
		
		// The constructor of the sub-class
		public MethodCompoundStudyAssignmentContainer() {
			analysisList = new List<LaboratoryAnalysis__c>();
		}
		
		// Retrieve the label for study selection
		public List<String> getStudyAnalysisInput() {
			List<String> results = new List<String>();
			for(LaboratoryAnalysis__c tmpItem : analysisList) {
				String tmpStr = '';
				tmpStr += (tmpItem.Analysis__c)?		  'AN'	  : '';
				tmpStr += (tmpItem.MethodDevelopment__c)? ', MD'  : '';
				tmpStr += (tmpItem.MEthodValidation__c)?  ', VAL' : '';
				tmpStr = tmpStr.removeStart(', ');
				results.add(tmpStr);
			}
			return results;
		}
	}
	
	
	/**	This class keeps a laboratory method with the methods and compounds to study assignment list.
	 * @author	Dimitrios Sgourdos
	 * @version	19-Nov-2013
	 */
	public class MethodCompoundStudyAssignmentMappedToMethod {
		public LaboratoryMethod__c 							labMethod 		  {get;set;}
		public List<MethodCompoundStudyAssignmentContainer> assignemtsToStudy {get;set;}
		
		// The constructor of the sub-class
		public MethodCompoundStudyAssignmentMappedToMethod() {
			assignemtsToStudy = new List<MethodCompoundStudyAssignmentContainer>();
		}
	}
}