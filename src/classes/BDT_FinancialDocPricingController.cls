public with sharing class BDT_FinancialDocPricingController {
	
	public BDT_SecuritiesService.pageSecurityWrapper securitiesA {get;set;} // Financial Documents Price
	public FinancialDocument__c 		financialDocument 		{get;set;}
	public Integer 						getNumberOfStudies() 	{return Studies.size();}
	public list<Study__c> 				Studies 				{get;set;}
	public list<serviceCategoryWrapper> serviceCategory 		{get;set;}
	public string 						comment 				{get;set;}
	public boolean						showComment				{get;set;}
	public boolean						showFakeSave			{get;set;}
	public boolean						showExchangeRate		{get;set;}
	public boolean						exchangeRateChanged		{get;set;}
	public boolean						priceCompleted			{get;set;}
	public boolean						showPage				{get;set;}
	public	Boolean						invalidDocFlag			{get;set;}
	public	Boolean						approvedDocFlag			{get;set;}
	
	public List<FinancialDocumentExchangeRates__c> upsertList;
	public List<FinancialDocumentExchangeRates__c> deleteList;
	
	private void warnForMissingRatecards(Set<ID> studyids){

		// if the currency is missing or the price is not specified then warn the user
		try{
			StudyServicePricing__c s = [select id 
										from StudyServicePricing__c
										where studyservice__r.study__c in :studyids
										and  (
											RateCardCurrency__c = null
											or CalculatedTotalPrice__c = null
											 )
										limit 1];
			// exception will be thrown when no row was found
			// an exception will indicate the data is okay

			apexPages.Message msg = new ApexPages.Message(
				ApexPages.Severity.FATAL,'Important price details are missing in the Unit Prices! Each service should have a ratecard and a price (0 will be accepted).');
			ApexPages.addMessage(msg);
		} catch (Exception e) {}
	}
	
	public BDT_FinancialDocPricingController(){
		securitiesA = new BDT_SecuritiesService.pageSecurityWrapper('Financial Documents Price');
		// read page parameter
		try {
			String FinancialDocumentID = system.currentPageReference().getParameters().get('financialDocumentID');
			financialDocument = FinancialDocumentService.getFinancialDocumentById(financialDocumentId);
								 
			Set<ID> studyIDs = new Set<ID>();					 

			for (String study:financialDocument.listOfStudyIds__c.split(':')){
				studyIDs.add(study);
			}

			warnForMissingRatecards(studyIDs);

			Studies = [select Title__c, Status__c, Sponsor_Code__c, Protocol_Title__c, Project__c, Name, Legacy_Code__c, 
								BDTDeleted__c, Id, Code__c, Business_Unit__c, Business_Unit__r.Name 
						from Study__c 
						where id in :studyIDs
						and BDTDeleted__c = false 
						order by code__c]; 

		} catch (Exception e) {
			apexPages.Message msg = new ApexPages.Message(
				ApexPages.Severity.FATAL, 
				'The document could not be found, reload your proposals screen. If the issue persists please contact your administrator.');
			ApexPages.addMessage(msg);
			financialDocument = new  FinancialDocument__c();
		}

		showExchangeRate = false;
		exchangeRateChanged = false;
		showComment = false;
		showFakeSave = (financialDocument.PricingComplete__c);
		invalidDocFlag = (financialDocument.DocumentStatus__c == FinancialDocumentDataAccessor.DOCUMENT_STATUS_INVALID);
		approvedDocFlag = (financialDocument.DocumentStatus__c == FinancialDocumentDataAccessor.DOCUMENT_STATUS_APPROVED);
		showPage = true; 
		
		if (financialDocument.id != null) {
			loadData();
		}
		
	}	
	
	public void loadData(){

		priceCompleted = financialDocument.PricingComplete__c;

		upsertList = new List<FinancialDocumentExchangeRates__c>();
		deleteList = new List<FinancialDocumentExchangeRates__c>();

		exchangeRateChanged = false;
		// refresh the list
		serviceCategory = new list<serviceCategoryWrapper>();
		initStudyTotal();
		
		Study__c s;
		
		// build fctMap
		Map<string,FinancialCategoryTotal__c> fctMap = new Map<string,FinancialCategoryTotal__c>();
		For (FinancialCategoryTotal__c fct : [SELECT Id
											, Study__c
											, ServiceCategory__c
											, FinancialDocument__c
											, (Select TotalPrice__c
												, PriceDelta__c
												, TargetCurrency__c
												, OriginalCurrency__c
												, FinancialCategoryTotal__c
												, Id
												, FinalPrice__c
												, ConversionRate__c
												from FinancialCatTotPrices__r
												order by TargetCurrency__c)
											FROM FinancialCategoryTotal__c
											WHERE FinancialDocument__c = :financialDocument.id]){
			
			String key = '' + fct.Study__c + ':' + fct.ServiceCategory__c;
			fctMap.put(key,fct);
		}
		
		// loop over the categories
		For (ServiceCategory__c sc: [select id
									, code__c
									, name
									, parentcategory__c
									 from ServiceCategory__c
									 where id in (select servicecategory__c
									 				from FinancialCategoryTotal__c
									 				where FinancialDocument__c = :financialDocument.id)
									 order by code__c]){
			
			serviceCategoryWrapper newServiceCategory = new serviceCategoryWrapper();
			newServiceCategory.serviceCategory = sc;
			newServiceCategory.financialCategoryTotalList = new list<financialCategoryTotalWrapper>();
			
			// loop over the studies
			For (integer i = 0; i<Studies.size();i++){
				s = Studies[i];
				
				String key = '' + s.id + ':' + sc.id;
				
				financialCategoryTotalWrapper newFinancialCategoryTotal = new financialCategoryTotalWrapper();
				
				if (fctMap.containsKey(key)) {
					newFinancialCategoryTotal.setfinancialCategoryTotal(fctMap.get(key));
				}
				
				newServiceCategory.financialCategoryTotalList.add(newFinancialCategoryTotal);
				
			}// end studies loop
			
			serviceCategory.add(newServiceCategory);
			
		} // end categories loop
		
	}	
	
	
	public list<studyTotalsWrp>	totalStudyPrices	{get;set;}
	public class studyTotalsWrp {
		public String totalPrice 	{get;set;}
		public String finalPrice 	{get;set;}
		
		public String delta 		{get;set;}
		
		public void calculateDelta() {
			
			if (totalPrice != null && totalPrice != '' && finalPrice != null && finalPrice != '') {
				List<Price> pricesTotal = (List<Price>)JSON.deserialize(totalPrice,List<price>.class);
				List<Price> pricesFinal = (List<Price>)JSON.deserialize(finalPrice,List<price>.class);
				List<Price> deltaList = new List<Price>();
				Price d;
				Decimal dd;
				
				For (integer i = 0 ; i < pricesTotal.size() ; i ++) {
					d = new Price();
					try { 
						dd = ((pricesFinal[i].amount / pricesTotal[i].amount -1 ) * 100);
						d.amount = dd.setScale(2);
					} catch (Exception e) {
						// catch devision by zero
						d.amount = 0;
					}
					d.amountCurrency ='';
					deltaList.add(d);
				}
				
				delta = JSON.serialize(deltaList);
			}
			
		}
	}
	
	public void initStudyTotal() {
		
		List<Price> pricesTotal, pricesFinal;
		Price pTotal,pFinal;
		String key;
		
		List<AggregateResult> arList = [Select TargetCurrency__c currency
										, financialcategorytotal__r.study__c study
										, sum(FinalPrice__c) finalamount
										, sum(TotalPrice__c) totalamount
									From FinancialCatTotPrice__c 
									where financialcategorytotal__r.servicecategory__r.parentcategory__c = null
									and financialcategorytotal__r.financialdocument__c =  :financialdocument.id
									group by TargetCurrency__c, financialcategorytotal__r.study__c
									order by TargetCurrency__c];
		
		Map<string,string> totalPricesPerStudy = new Map<string,string> ();
		Map<string,string> finalPricesPerStudy = new Map<string,string> ();
		
		For (AggregateResult ar:arList) {
			key = BDT_FinancialCategoryTotalUtils.stringArrCast(ar.get('study'));
			
			pTotal = new Price();
			pTotal.amount = BDT_FinancialCategoryTotalUtils.decimalArrCast(ar.get('totalamount'));
			pTotal.amountCurrency = BDT_FinancialCategoryTotalUtils.stringArrCast(ar.get('currency'));
			
			if (totalPricesPerStudy.containsKey(key)) {
				pricesTotal = (List<Price>)JSON.deserialize(totalPricesPerStudy.remove(key),List<price>.class);
			} else {
				pricesTotal = new List<Price>();
			}
			pricesTotal.add(pTotal);
			totalPricesPerStudy.put(key,JSON.serialize(pricesTotal));

			/////////

			pFinal = new Price();
			pFinal.amount = BDT_FinancialCategoryTotalUtils.decimalArrCast(ar.get('finalamount'));
			pFinal.amountCurrency = BDT_FinancialCategoryTotalUtils.stringArrCast(ar.get('currency'));
			
			if (finalPricesPerStudy.containsKey(key)) {
				pricesFinal = (List<Price>)JSON.deserialize(finalPricesPerStudy.remove(key),List<price>.class);
			} else {
				pricesFinal = new List<Price>();
			}
			pricesFinal.add(pFinal);
			finalPricesPerStudy.put(key,JSON.serialize(pricesFinal));

		}
		
		totalStudyPrices = new list<studyTotalsWrp>();
		
		studyTotalsWrp st;
		
		for (Study__c s : Studies){
			st = new studyTotalsWrp();
			if (totalPricesPerStudy.containsKey(''+s.id)){
				st.totalPrice = totalPricesPerStudy.get(s.id);
			} else {
				st.totalPrice = ''; 
			}
			if (finalPricesPerStudy.containsKey(''+s.id)){
				st.finalPrice = finalPricesPerStudy.get(s.id);
			} else {
				st.finalPrice = ''; 
			}
			st.calculateDelta();
			totalStudyPrices.add(st);
		}
	}
	
	public void fakeSave() {
		
		showComment = true;
		showFakeSave = false;
		
	}
	
	public void save(){
		
		Boolean commentOkay;
		
		// check if user needed to supply a comment
		if (financialDocument.PricingComplete__c) {
			//comment required
			if (comment != null && comment.length()>0) {
				// comment was specified
				showComment = false;
				showFakeSave = true;
				commentOkay = true;
			} else {
				// comment was not specified
				showComment = true;
				showFakeSave = false;
				commentOkay = false;
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, 'A comment is mandatory. Please, enter a comment.');
				ApexPages.addMessage(msg);
			}
		} else {
			// comment was not required
			showComment = false;
			showFakeSave = false;
			commentOkay = true;
		}
		
		if (commentOkay) {
			upsert financialdocument;
			
			List<FinancialCatTotPrice__c> listOfUpdates = new List<FinancialCatTotPrice__c> ();
			Map<String,String> parentDeltaSet = new Map<String,String>();
			
			for (serviceCategoryWrapper scw: serviceCategory){
				for (financialCategoryTotalWrapper fctw:scw.financialCategoryTotalList){
					if (fctw.financialCatTotPriceList!=null) {
						for (financialCatTotPriceWrapper fctpw:fctw.financialCatTotPriceList){
							
							// check if parentDelta needs to be taken
							String key = '' + fctpw.FinancialCatTotPrice.OriginalCurrency__c + ':' + scw.servicecategory.parentcategory__c + ':' + fctw.financialcategorytotal.study__c;
							if (parentDeltaSet.containsKey(key)) {
								fctpw.newDelta = parentDeltaSet.get(key);
							}
							
							// check if delta was changed
							Decimal newValue;
							// invalid entries will be cleared to null
							try {
								newValue = Decimal.valueOf(fctpw.newDelta);
							} catch (Exception e) {
								newValue = null;
							}
							Decimal oldValue = fctpw.FinancialCatTotPrice.PriceDelta__c;
							
							
							Decimal difference = (((newValue==null) ? 0 : newValue) - ((oldValue==null) ? 0 : oldValue)).abs();
							
							if ((newValue != oldValue && difference >= 0.1)||(fctpw.exchangeRateAdjusted)) {
								fctpw.FinancialCatTotPrice.PriceDelta__c = newValue;
								fctpw.FinancialCatTotPrice.FinalPrice__c = fctpw.FinancialCatTotPrice.TotalPrice__c * (newValue/100+1);
								listOfUpdates.add(fctpw.FinancialCatTotPrice);
								
								// ensure childs get same delta
								key = '' + fctpw.FinancialCatTotPrice.OriginalCurrency__c + ':' + scw.servicecategory.id + ':' + fctw.financialcategorytotal.study__c;
								parentDeltaSet.put(key,String.valueOf(newValue));
								
							}
						}
					}
				}
			}

			upsert listOfUpdates;

			// exchange rate changes
			delete deleteList;
			upsert upsertList;

			// redo the calculations to ensure data is correct and no rounding errors are present.
			BDT_FinancialCategoryTotalUtils.refreshFinancialCategoryTotal(financialDocument.id);
			//reload data
			loadData();

			financialDocument = FinancialDocumentService.getFinancialDocumentById(financialDocument.Id);

			if (financialDocument.PricingComplete__c) {
				FinancialDocumentHistoryService.saveHistoryObject(financialDocument.id,
																comment,
																'Pricing was adjusted',
																financialDocument.TotalValue__c,
																'Price');
			}
			
			// Check if the approvals are up to date
			Boolean oldValueFlag = financialDocument.ApprovalsUpToDate__c;
			Boolean upToDateFlag = FinancialDocumentApprovalService.areDocumentApprovalsUpToDate(financialDocument.id);
			if(oldValueFlag != upToDateFlag) {
				financialDocument.ApprovalsUpToDate__c = upToDateFlag;
				update financialDocument;
			}
			
			// Send e-mail notifications for adjustments
			FinancialDocumentApprovalService.sendAdjustmentsEmailNotifications(financialDocument,
																	FinancialDocumentHistoryDataAccessor.PRICE_PROCESS,
																	userInfo.getName());
			
			showPage = false; 
		}
	}
	
	
	public class Price {
		public decimal amount;
		public string  amountCurrency;
	}
		
	public class serviceCategoryWrapper{
		public ServiceCategory__c serviceCategory {get;set;}
		public list<FinancialCategoryTotalWrapper> financialCategoryTotalList {get;set;}
		public Integer getPadding() {return (serviceCategory.Code__c.length()-1)*4;}
		public string getCode(){return BDT_Utils.removeLeadingZerosFromCat(serviceCategory.Code__c);}
		
		
		public string getTotalValue () {
			
			Map<string,Price> total = new Map<string,Price>();
			Price p;
			String key;
			
			for (financialCategoryTotalWrapper fctw:financialCategoryTotalList) {
				if (fctw.financialCatTotPriceList!=null) {
					for (financialCatTotPriceWrapper fctpw:fctw.financialCatTotPriceList) {
						key = fctpw.FinancialCatTotPrice.TargetCurrency__c;
						if (total.containsKey(key)) {
							p = total.remove(key);
						} else {
							p = new Price();
							p.amountCurrency = key;
							p.amount = 0;
						}
						p.amount += fctpw.FinancialCatTotPrice.finalprice__c;
						
						total.put(key,p);
					}
				}
			}
			
			return JSON.serialize(total.values());
			
		}
	}
	
	public class financialCategoryTotalWrapper{
		public FinancialCategoryTotal__c financialCategoryTotal;
		public list<financialCatTotPriceWrapper> financialCatTotPriceList {get;set;}
		
		public void setfinancialCategoryTotal (FinancialCategoryTotal__c value){
			financialCategoryTotal = value;
			financialCatTotPriceList = new list<financialCatTotPriceWrapper>();
			for (FinancialCatTotPrice__c fctp:financialCategoryTotal.FinancialCatTotPrices__r){
				financialCatTotPriceWrapper newItem = new financialCatTotPriceWrapper();
				newItem.FinancialCatTotPrice = fctp;
				financialCatTotPriceList.add(newItem);
			}
		}
		public FinancialCategoryTotal__c getfinancialCategoryTotal () {
				return financialCategoryTotal;
		}
	}
	
	public class financialCatTotPriceWrapper {
		public FinancialCatTotPrice__c FinancialCatTotPrice {get;set;}
		
		public string newDelta;
		public string getDelta () {
			if (FinancialCatTotPrice.PriceDelta__c!= null) {
				string valueToUse = (newDelta == null) ? string.valueOf(FinancialCatTotPrice.PriceDelta__c.setScale(1)) : newDelta;
				return BDT_Utils.removeTailingZerosFromDecimals(valueToUse);
			}
			return newDelta;
		}
		public void setDelta (String value) {
			newDelta = value;
		}
		
		public string getTotalPrice(){
			return formatNumber(FinancialCatTotPrice.TotalPrice__c);
		}
		
		public string getFinalPrice(){
			return formatNumber(FinancialCatTotPrice.FinalPrice__c);
		}
		
		public string formatNumber (Decimal value){
			String regex = '(\\d)(?=(\\d{3})+$)';
			String result = String.valueOf(value.setScale(0));
			return result.replaceAll(regex,'$1,');
		}
		
		public boolean exchangeRateAdjusted {get;set;}
		public financialCatTotPriceWrapper() {
			exchangeRateAdjusted = false;
		}
	} 
	
	/*
	* Exchange rate stuff
	*/
	
	public void showExchangeRateForStudy () {
		
		exchangeRates = [Select TargetCurrency__c
						, Study__c
						, OriginalCurrency__c
						, Id
						, FinancialDocument__c
						, ExchangeRate__c 
						From FinancialDocumentExchangeRates__c 
						where FinancialDocument__c = :financialdocument.Id
						and Study__c = :editExchangeRateStudy
						];
						
		// add lines for currencies that do not yet have an exchange rate set
		Set<String> specifiedCurrencies = new Set<String>();
		For (FinancialDocumentExchangeRates__c f:exchangeRates) {
			specifiedCurrencies.add(f.OriginalCurrency__c);
		}
		for (AggregateResult ar : [Select OriginalCurrency__c
									From    FinancialCatTotPrice__c 
									where   financialcategorytotal__r.financialdocument__c = :financialdocument.Id
									and     financialcategorytotal__r.study__c = :editExchangeRateStudy
									and     OriginalCurrency__c not in :specifiedCurrencies
									group by OriginalCurrency__c
									order by OriginalCurrency__c]){
			string originalCurrency = BDT_FinancialCategoryTotalUtils.stringArrCast(ar.get('OriginalCurrency__c'));
			exchangeRates.add(new FinancialDocumentExchangeRates__c(
				Study__c = editExchangeRateStudy,
				FinancialDocument__c = financialdocument.Id,
				OriginalCurrency__c = originalCurrency
			));
		}
		
		showExchangeRate = true;
		
	}
	
	public void closeExchangeRate(){
		
		showExchangeRate = false;
		exchangeRates = null; 
		
	}
	
	public void applyExchangeRate() {
		
		Set<String> currencies = new Set<String>();
		for(Currency__c c:[select name,id from Currency__c]){
			currencies.add(c.name);
		}
		
		// validate
		For (FinancialDocumentExchangeRates__c e:exchangeRates){
			
			try {
				e.TargetCurrency__c = e.TargetCurrency__c.toUpperCase();
			} catch (Exception d) {}
			
			boolean valid = true;
			valid = (e.TargetCurrency__c == null || e.TargetCurrency__c == '') ? false :valid;
			valid = (e.OriginalCurrency__c == e.TargetCurrency__c) ? false : valid;
			valid = (e.ExchangeRate__c == null || e.ExchangeRate__c == 0) ? false : valid;
			valid = (valid && !currencies.contains(e.TargetCurrency__c)) ? false : valid;
			
			if (valid){
				upsertList.add(e);
				
				for (serviceCategoryWrapper scw: serviceCategory){
					for (financialCategoryTotalWrapper fctw:scw.financialCategoryTotalList){
						if (fctw.financialCatTotPriceList!=null) {
							for (financialCatTotPriceWrapper fctpw:fctw.financialCatTotPriceList){
								// adjust price
								if (fctpw.FinancialCatTotPrice.OriginalCurrency__c == e.OriginalCurrency__c) {
									fctpw.FinancialCatTotPrice.TargetCurrency__c = e.TargetCurrency__c;
									
									fctpw.FinancialCatTotPrice.ConversionRate__c = e.ExchangeRate__c;
									fctpw.FinancialCatTotPrice.TotalPrice__c = fctpw.FinancialCatTotPrice.TotalPrice__c * e.ExchangeRate__c;
									fctpw.FinancialCatTotPrice.FinalPrice__c = fctpw.FinancialCatTotPrice.FinalPrice__c * e.ExchangeRate__c;
									fctpw.exchangeRateAdjusted = true;
									exchangeRateChanged = true;
								}
							}
						}
					}
				}
			} 
			
			if (e.id != null && (e.TargetCurrency__c == '' || e.ExchangeRate__c == 0 || e.ExchangeRate__c == null)) {
				deleteList.add(e);
				for (serviceCategoryWrapper scw: serviceCategory){
					for (financialCategoryTotalWrapper fctw:scw.financialCategoryTotalList){
						for (financialCatTotPriceWrapper fctpw:fctw.financialCatTotPriceList){

							// adjust price
							if (fctpw.FinancialCatTotPrice.OriginalCurrency__c == e.OriginalCurrency__c) {
								fctpw.FinancialCatTotPrice.TargetCurrency__c = fctpw.FinancialCatTotPrice.OriginalCurrency__c;
								
								fctpw.FinancialCatTotPrice.TotalPrice__c = fctpw.FinancialCatTotPrice.TotalPrice__c * (1/e.ExchangeRate__c);
								fctpw.FinancialCatTotPrice.FinalPrice__c = fctpw.FinancialCatTotPrice.FinalPrice__c * (1/e.ExchangeRate__c);
								fctpw.FinancialCatTotPrice.ConversionRate__c = null;
								fctpw.exchangeRateAdjusted = true;
								exchangeRateChanged = true;
								
							}

						}
					}
				}
			}
			
		}
		closeExchangeRate();
		
		
		//cleanup
		Set<ID> deletedIDs = new Set<ID>();
		for (FinancialDocumentExchangeRates__c f : deleteList){
			deletedIDs.add(f.id);
		}

		List<FinancialDocumentExchangeRates__c> cleanInsertList =  new List<FinancialDocumentExchangeRates__c>();
		for (FinancialDocumentExchangeRates__c f : upsertList){
			if (!deletedIDs.contains(f.id)) {
				cleanInsertList.add(f);
			} 
		}
		upsertList = cleanInsertList.deepClone(true);
		
		
	}
	
	public string editExchangeRateStudy {get;set;}
	public list<FinancialDocumentExchangeRates__c> exchangeRates {get;set;}
	
	public void reapplyProposalrateCard () {
		Set<Id> studyIds = new Set<Id>();
		For (String s : financialDocument.listOfStudyIds__c.split(':')) {
			studyIds.add(s);
		}

		BDT_DC_FinancialDocument.replicatePriceFromParent(financialDocument.id,studyIds);
		loadData();
	}

}