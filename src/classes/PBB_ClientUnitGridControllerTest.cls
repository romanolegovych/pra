/**
* @author Devaram Bhargav
* @date 06/30/2015
* @description this is test class for PBB_ClientUnitGridController class
*/

 @istest(seealldata=false)
public with sharing class PBB_ClientUnitGridControllerTest {

    //test method for save
    public static testMethod void testPBB_ClientUnitGridController ()  {
        test.startTest();      
        
        //Prepare the test data
        PBB_TestUtils tu = new PBB_TestUtils ();
        tu.wfmp = tu.createwfmproject();
        tu.bid =tu.createbidproject();        
        tu.scen =tu.createscenarioAttributes();
        
        Apexpages.currentPage().getParameters().put('PBBScenarioID', tu.scen.id); 
        PBB_ClientUnitGridController cug= new PBB_ClientUnitGridController();        
        cug.getBudgettypeList();
        cug.getReportByList();
        cug.CreateBudget();
        cug.Reportby = 'Unit Grid';
        cug.GenerateReport();
        System.assertEquals(cug.ClientgridList.size(),0);
        
        cug.Reportby = 'Functional-Blended';
        cug.GenerateReport();
        System.assertEquals(cug.ClientgridList.size(),0);
        
        cug.Reportby = 'Functional-Country Specific';
        cug.GenerateReport();
        System.assertEquals(cug.ClientgridList.size(),0);
        test.stopTest();
    }   
}