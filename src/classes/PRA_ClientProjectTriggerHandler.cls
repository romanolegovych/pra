public without sharing class PRA_ClientProjectTriggerHandler {
    public void onAfterInsert (List<Client_Project__c> projects){
        List<Monthly_Approval__c> monthlyApprovals = new List<Monthly_Approval__c>();
        
        for (Client_Project__c project : projects){
            Date monthAppliesTo = Date.today().toStartOfMonth();
            Date endDate = project.Estimated_End_Date__c;            
            while (monthAppliesTo < endDate){
                Monthly_Approval__c ma= new Monthly_Approval__c(Client_Project__c =  project.id, Month_Approval_Applies_To__c = monthAppliesTo,
                                                                 Is_approval_complete__c=false);
                               
                String  month=(string.valueof(monthAppliesTo.month()).length()==1)?string.valueof('0'+monthAppliesTo.month()):string.valueof(monthAppliesTo.month());
                
                String madate=string.valueof(monthAppliesTo.year())+string.valueof(month)+'01';
                system.debug('------month--madate----'+month+madate);
                ma.MA_Unique_ID__c = project.Name+':'+madate;                
                system.debug('------ma------'+ma);
                monthlyApprovals.add(ma); 
                monthAppliesTo = monthAppliesTo.addMonths(1);
            }
        }
        system.debug('------map------'+monthlyApprovals.size()+monthlyApprovals);
        insert monthlyApprovals;
    }
}