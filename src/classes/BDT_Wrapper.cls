global class BDT_Wrapper {
	
	public class parentWrapper{
		public String parentName	{get; set;}	//e.g. Population name	
		public String parentId	{get; set;}		//e.g. Population Id
		public Integer countSelected	{get; set;} //number of occurrences of childWrapper relationship	
		public List<childWrapper> childWrapperList {get; set;}
		
		public parentWrapper(String sParentName, String sParentId, Integer sCountSelected, List<childWrapper> sChildWrapperList){
			parentName = sParentName;	
			parentId = sParentId;
			countSelected = sCountSelected;		
			childWrapperList = sChildWrapperList;
		}
	}
	
	public class childWrapper{
		public String childId		{get; set;}//e.g. Study Id
		public String parentId	{get; set;}    //parentId e.g. Population Id, this is required in order if needed loop only throw the list of childWrappers in order to create/edit a parent child relationship record
		public boolean selected 			{get; set;}
		public boolean disabled 			{get; set;}
		
		public childWrapper(String sChildId, String sParentId, boolean sSelected, boolean sDisabled){
			childId = sChildId;
			parentId = sParentId;			
			selected = sSelected;
			disabled = sDisabled;
		}
	}
	/*  Builds a list of boolean parent/child relationships.
		e.gThis method is used to build Population/Studies relationship for Population Study Assignment section on BDT_Characteristics page
		@aParentWrapperList = The instantiated list of ParentWrapper which needs to be fullfil 
		@parentRawList = List of Population, vertical list on the grid
		@childRawList = List of Studies, horizontal list on the grid
		@existingParentChildRel = Map of the existing records which have the Parent/child assignment relationship already stored
	*/
	public static void buildParentWrapperList(List<parentWrapper> aParentWrapperList, List<sObject> parentRawList, List<sObject> childRawList, Map<String, sObject> existingParentChildRel){ 
		aParentWrapperList.clear();
		
		for(sObject psObj: parentRawList){
			List<BDT_Wrapper.childWrapper> childWrapperList = new List<BDT_Wrapper.childWrapper>();
			Integer countSelected = 0;
				for(sObject csObj: childRawList){
					Boolean exists = false; 
					//if this record exists on the existing parent child relationship map then set EXISTS to true and add one to the counter
					if(existingParentChildRel.containsKey(psObj.id+':'+csObj.id)){
						exists=true;
						++countSelected;//add one to the counter
					}										
					childWrapperList.add(new BDT_Wrapper.childWrapper(csObj.id,psObj.id, exists, false));				
									
				}
				aParentWrapperList.add(new BDT_Wrapper.parentWrapper(String.valueOf(psObj.get('Name')), psObj.id, countSelected, childWrapperList));
		}
				
	}
	
	/* Saves the Child Record that have not been created before and deletes existingParentChildRel records which have been unselected by the user and were previously created
		@aParentWrapperList = List of ParentWrapper which contains the childWrapper that need to be processed for saving/deletion
		@existingParentChildRel = Map of the existing records which have the Parent/child assignment relationship already stored
	*/	
	public static void saveChildWrapper(List<parentWrapper> aParentWrapperList, Map<String, sObject> existingParentChildRel){
		List<childWrapper> childWrapperTOBEPROCESS = new List<childWrapper>();
		List<sObject> assignmentRecordTOBEDELETEDList = new List<sObject>();
		List<sObject> assignmentRecordTOBEINSERTEDList  = new List<sObject>();
				
		//collect all childWrappers that need to be processed
		for(parentWrapper si: aParentWrapperList){
			childWrapperTOBEPROCESS.addAll(si.childWrapperList);
			system.debug('Size: childWrapperTOBEPROCESS.size(): '+ childWrapperTOBEPROCESS.size());
		}
		
		//check all childWrappers, if any exists within existingParentChildRel and selected true, don't create, otherwise create it. 
		//In the case the record exists and selected is not true, then process for deletion. 		
		for(childWrapper sw: childWrapperTOBEPROCESS){
			if(sw.selected){
				//if the assignment record has not been created before then add it to the list for creation
				if(!existingParentChildRel.containsKey(sw.parentId+':'+sw.childId)){
					SObject newParentChildRel = createObject(existingParentChildRel.getsObjectType().getDescribe().getName(), sw.parentId, sw.childId);					
					assignmentRecordTOBEINSERTEDList.add(newParentChildRel);
				}
			}else{//if it is not selected check if exists already, if it does then add it to the delete list
				if(existingParentChildRel.containsKey(sw.parentId+':'+sw.childId)){
					assignmentRecordTOBEDELETEDList.add(existingParentChildRel.get(sw.parentId+':'+sw.childId));
				}
			}
			
		}
		system.debug('assignmentRecordTOBEINSERTEDList: '+ assignmentRecordTOBEINSERTEDList);
		system.debug('assignmentRecordTOBEDELETEDList: '+ assignmentRecordTOBEDELETEDList);
		if(!assignmentRecordTOBEDELETEDList.isEmpty()){
			delete assignmentRecordTOBEDELETEDList;
		}
		if(!assignmentRecordTOBEINSERTEDList.isEmpty()){
			insert assignmentRecordTOBEINSERTEDList;
		}
	} 
	
	public static sObject createObject(String typeName, String parentId, String childId) {
        Schema.SObjectType targetType = Schema.getGlobalDescribe().get(typeName);
        if (targetType == null) {
            // throw an exception 
        }
        
        // Instantiate an sObject with the type passed in as an argument at run time. 
        SObject newObject = targetType.newSObject();
        //Evaluate Object type and put field values accordingly
        if(typeName == 'PopulationStudyAssignment__c'){
        	newObject.put('Population__c', parentId);
			newObject.put('Study__c', childId); 
        }
        else if(typeName == 'ClinicalDesignStudyAssignment__c'){
        	newObject.put('ClinicalDesign__c', parentId);
			newObject.put('Study__c', childId); 
        } 
        /* Has wrong attribute, code no longer needed?
        else if(typeName == 'StudySiteAssignment__c'){
        	newObject.put('Site__c', parentId);
			newObject.put('Study__c', childId); 
        }                
        */      
        return newObject;
    }

}