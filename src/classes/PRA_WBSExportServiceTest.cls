/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
public class PRA_WBSExportServiceTest {
    
    static testMethod void testCreateWBSFile() {
        final Integer wbsFileExpectedSize = 14;
        
        PRA_TestUtils tu = new PRA_TestUtils();
        initTestData(tu);
        
        system.debug('----- clientProject.id: ' + tu.clientProject.id);
        system.debug('----- wbsProject.id: ' + tu.wbsProject.id);
        
        Test.startTest();
        List<PRA_ProjectWBSCVO.ExportWBSCVO> wbsFile = PRA_WBSExportService.createWBSFile(tu.clientProject.id);
        Test.stopTest();
        
        system.assertNotEquals(null, wbsFile);
        system.assertNotEquals(0, wbsFile.size());
        system.assertEquals(PRA_ProjectWBSCVO.WBSCVO_NUM_FIELDS, wbsFile[0].items.size());
        system.assertEquals(wbsFileExpectedSize, wbsFile.size());
        
        PRA_ProjectWBSCVO.ExportWBSCVO contractRec = wbsFile[0];
        system.assertEquals('FEASXLWBS', contractRec.items[PRA_ProjectWBSCVO.WBS_FLD_RUN_GRP]);
        system.assertEquals('ELEELEXL', contractRec.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY]);
        system.assertEquals('EXELIXIS (ELEELEXL)', contractRec.items[PRA_ProjectWBSCVO.WBS_FLD_SHORT_DESC]);
        system.assertEquals(8, contractRec.items[PRA_ProjectWBSCVO.WBS_FLD_START_DATE].length());
        system.assertEquals('17', contractRec.items[PRA_ProjectWBSCVO.WBS_FLD_CTRCT_ADDR]);
        system.assertEquals('0', contractRec.items[PRA_ProjectWBSCVO.WBS_FLD_PROJECT_ADDR]);
        system.assertEquals('S', contractRec.items[PRA_ProjectWBSCVO.WBS_FLD_LEVEL_TYPE]);
        
        PRA_ProjectWBSCVO.ExportWBSCVO projRec = wbsFile[1];
        system.assertEquals('FEASXLWBS', projRec.items[PRA_ProjectWBSCVO.WBS_FLD_RUN_GRP]);
        system.assertEquals('ELEELEXL-FEASXL', projRec.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY]);
        system.assertEquals('EXELIXIS (ELEELEXL-FEASXL)', projRec.items[PRA_ProjectWBSCVO.WBS_FLD_SHORT_DESC]);
        system.assertEquals('17', projRec.items[PRA_ProjectWBSCVO.WBS_FLD_CTRCT_ADDR]);
        system.assertEquals('1', projRec.items[PRA_ProjectWBSCVO.WBS_FLD_PROJECT_ADDR]);
        system.assertEquals('C', projRec.items[PRA_ProjectWBSCVO.WBS_FLD_LEVEL_TYPE]);
        system.assertEquals('SETUP YEAR', projRec.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_1]);
        system.assertEquals('INVOICE METH', projRec.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_ATTRIB_2]);
        system.assertEquals('UNIT', projRec.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_2]);

        
        PRA_ProjectWBSCVO.ExportWBSCVO taskGrpRec;
        PRA_ProjectWBSCVO.ExportWBSCVO taskRec;
        PRA_ProjectWBSCVO.ExportWBSCVO postRec;
        for (PRA_ProjectWBSCVO.ExportWBSCVO wbsCvo : wbsFile) {
            if (wbsCvo.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY] == 'FEASXL-PROJ MAN') {
                taskGrpRec = wbsCvo;
            }
            if (wbsCvo.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY] == 'FEASXL020') {
                taskRec = wbsCvo;
            }
            if (wbsCvo.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIVITY] == 'FEASXL080LBREE') {
                postRec = wbsCvo;
            }
        }
        
        system.assertEquals('Project Management', taskGrpRec.items[PRA_ProjectWBSCVO.WBS_FLD_SHORT_DESC]);
        
        system.assertEquals('1b Project Management Financia', taskRec.items[PRA_ProjectWBSCVO.WBS_FLD_SHORT_DESC]);
        system.assertEquals('1b Project Management Financial Suport', taskRec.items[PRA_ProjectWBSCVO.WBS_FLD_LONG_DESC]);
        
        system.assertEquals('3b Site recruitment and qualif', postRec.items[PRA_ProjectWBSCVO.WBS_FLD_SHORT_DESC]);
        system.assertEquals('3b Site recruitment and qualification', postRec.items[PRA_ProjectWBSCVO.WBS_FLD_LONG_DESC]);
        system.assertEquals('LBR', postRec.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_2]);
        system.assertEquals('EE', postRec.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_3]);
        system.assertEquals('LBREE', postRec.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_4]);
        system.assertEquals('PRODUCT REGISTRATION', postRec.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_5]);
        system.assertEquals('US/CANADA', postRec.items[PRA_ProjectWBSCVO.WBS_FLD_MTX_VALUE_6]);
        
    }
    
    static testMethod void testCreateBD1File() {
        final Integer bdFileExpectedSize = 4;
        
        PRA_TestUtils tu = new PRA_TestUtils();
        initTestData(tu);
        
        system.debug('----- clientProject.id: ' + tu.clientProject.id);
        system.debug('----- wbsProject.id: ' + tu.wbsProject.id);
        
        Test.startTest();
        List<PRA_ProjectWBSCVO.ExportBD1CVO> bdFile = PRA_WBSExportService.createBD1File(tu.clientProject.id);
        Test.stopTest();
        
        system.assertNotEquals(null, bdFile);
        system.assertNotEquals(0, bdFile.size());
        system.assertEquals(PRA_ProjectWBSCVO.WBSBD_NUM_FIELDS, bdFile[0].items.size());
        system.assertEquals(bdFileExpectedSize, bdFile.size());
    }
    
    static testMethod void testCreateBD9File() {
        final Integer bdFileExpectedSize = 4;
        
        PRA_TestUtils tu = new PRA_TestUtils();
        initTestData(tu);
        
        system.debug('----- clientProject.id: ' + tu.clientProject.id);
        system.debug('----- wbsProject.id: ' + tu.wbsProject.id);
        
        Test.startTest();
        List<PRA_ProjectWBSCVO.ExportBD9CVO> bdFile = PRA_WBSExportService.createBD9File(tu.clientProject.id);
        Test.stopTest();
        
        system.assertNotEquals(null, bdFile);
        system.assertNotEquals(0, bdFile.size());
        system.assertEquals(PRA_ProjectWBSCVO.WBSBD_NUM_FIELDS, bdFile[0].items.size());
        system.assertEquals(bdFileExpectedSize, bdFile.size());
    }

	static testMethod void testNoProjManGroup() {
        PRA_TestUtils tu = new PRA_TestUtils();
        initTestData(tu);
        
        // Clear Flag on tasks with PROJ MAN task group
        List<Client_Task__c> ctl = [SELECT id, status__c FROM Client_Task__c WHERE Task_Group__r.Lawson_Short_Desc__c = 'PROJ MAN'];
        for (Client_Task__c ct : ctl){
        	ct.status__c = '';
        }
        update ctl;  
         
        Test.startTest();
        List<PRA_ProjectWBSCVO.ExportWBSCVO> wbsFile = PRA_WBSExportService.createWBSFile(tu.clientProject.id);
        Test.stopTest();

		Boolean bFound203 = false;

		for (PRA_ProjectWBSCVO.ExportWBSCVO exCVO : wbsFile) {
			if(exCVO.items[PRA_ProjectWBSCVO.WBS_FLD_ACTIV_ADDR] == '203' &&
			   exCVO.items[PRA_ProjectWBSCVO.WBS_FLD_TASK_ADDR] == '0') {
				bFound203 = true;
			}
		}
		
		system.assertEquals(true, bFound203);
	}   
	 
    static void initTestData(PRA_TestUtils testUtils) {
        Date curentDate = System.today();
        insert testUtils.getAccount('Acme Inc.');
        insert testUtils.getClientProject(testUtils.client.id, 'ELEELEXL-FEASXL', 'ELEELEXL-FEASXL', curentDate.addMonths(-6), curentDate.addMonths(6));
		insert testUtils.getRegion();
        insert testUtils.getWbsCompany();
        insert testUtils.getWbsProject();
        insert testUtils.getOperationalArea('Clinical Operations', 'CO');
        insert testUtils.getBusinessUnit('Road runner hunters', 'RRH');
        insert testUtils.getCountries(testUtils.businessUnit.id).values();
        insert testUtils.getProjectRegions().values();
        
        List<Task_Group__c> tgList = new List<Task_Group__c>();
        tgList.add(testUtils.getTaskGroup('Clin ops - startup', testUtils.operationalArea.id, 'TG0', '201', 'CO-START', 'Clin ops - startup'));
        tgList.add(testUtils.getTaskGroup('Clin Ops - Conduct', testUtils.operationalArea.id, 'TG1', '202', 'CO-CONDT', 'Clin Ops - Conduct'));
        tgList.add(testUtils.getTaskGroup('Project Management', testUtils.operationalArea.id, 'TG2', '203', 'PROJ MAN', 'Project Management'));
        tgList.add(testUtils.getTaskGroup('Interim Monitoring Visits', testUtils.operationalArea.id, 'TG3', '208', 'INTR-VIS', 'Interim Monitoring Visits'));
        insert tgList;
        
        List<Client_Task__c> ctList = new List<Client_Task__c>();       
        Client_Task__c ct;
        ct = testUtils.getClientTask(tgList[2].id, testUtils.clientProject.id, 'CT0', 'Project Management Start Up Phase',  curentDate.addMonths(-4), curentDate.addMonths(4),
                            testUtils.countries.get('USA').id, testUtils.projectRegions.get('NA').id);
        ct.status__c = PRA_Constants.CU_STATUS_SENT;
        ct.Combo_Code__c = '010';
        ct.Client_Unit_Number__c = '1a';
        ctList.add(ct);
                                    
        ct = testUtils.getClientTask(tgList[2].id, testUtils.clientProject.id, 'CT1', 'Project Management Financial Suport',  curentDate.addMonths(-4), curentDate.addMonths(4),
                            testUtils.countries.get('USA').id, testUtils.projectRegions.get('NA').id);
        ct.status__c = PRA_Constants.CU_STATUS_SENT;
        ct.Combo_Code__c = '20';
        ct.Client_Unit_Number__c = '1b';
        ctList.add(ct);
        
        ct = testUtils.getClientTask(tgList[0].id, testUtils.clientProject.id, 'CT2', 'Clinical Team Management',  curentDate.addMonths(-4), curentDate.addMonths(4),
                            testUtils.countries.get('USA').id, testUtils.projectRegions.get('NA').id);
        ct.status__c = PRA_Constants.CU_STATUS_SENT;
        ct.Combo_Code__c = '070';
        ct.Client_Unit_Number__c = '3a';
        ctList.add(ct);

        ct = testUtils.getClientTask(tgList[0].id, testUtils.clientProject.id, 'CT3', 'Site recruitment and qualification',  curentDate.addMonths(-4), curentDate.addMonths(4),
                            testUtils.countries.get('USA').id, testUtils.projectRegions.get('NA').id);
        ct.status__c = PRA_Constants.CU_STATUS_SENT;
        ct.Combo_Code__c = '080';
        ct.Client_Unit_Number__c = '3b';
        ctList.add(ct);
        
        ct = testUtils.getClientTask(tgList[0].id, testUtils.clientProject.id, 'CT4', 'Migrated Worked Hours',  curentDate.addMonths(-4), curentDate.addMonths(4),
                            testUtils.countries.get('USA').id, testUtils.projectRegions.get('NA').id);
        ct.status__c = PRA_Constants.CU_STATUS_SENT;
        ct.Combo_Code__c = 'Migrated Worked';
        ct.Client_Unit_Number__c = 'Migrated Worked Hours';
        ctList.add(ct);
        
        insert ctList;
    }
     
}