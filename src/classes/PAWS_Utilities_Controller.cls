public with sharing class PAWS_Utilities_Controller
{
	public List<PAWS_Project_Flow_Junction__c> Bug_99873_Projects
	{
		get
		{
			if (Bug_99873_Projects == null)
			{
				Set<String> projectIds = new Set<String>();
				
				List<STSWR1__Flow_Instance__c> flowInstances = [Select STSWR1__Object_Id__c From STSWR1__Flow_Instance__c Where STSWR1__Is_Completed__c = 'No' And STSWR1__Object_Type__c = 'ecrf__c'];
				if (Test.isRunningTest()) flowInstances.add(new STSWR1__Flow_Instance__c(STSWR1__Object_Id__c = 'test'));
				
				for (STSWR1__Flow_Instance__c fi : flowInstances)
				{
					projectIds.add(fi.STSWR1__Object_Id__c);
				}
				
				Bug_99873_Projects = [Select Project__c, Flow__c From PAWS_Project_Flow_Junction__c Where Project__r.Type__c = 'Regular' And Flow__c != null And Project__c In :projectIds Order By CreatedDate];
			}
			return Bug_99873_Projects;
		}
		set;
	}
	
	@RemoteAction
	public static String updateGanttRecords(List<Map<String, String>> records) 
	{
		try
		{
			Map<String, STSWR1__Gantt_Step_Property__c> props = new Map<String, STSWR1__Gantt_Step_Property__c>(); 
			
			for (Map<String, String> rec : records)
			{
				STSWR1__Gantt_Step_Property__c prop = new STSWR1__Gantt_Step_Property__c();
				for (String fieldName : rec.keySet())
				{
					prop.put(fieldName, rec.get(fieldName));
				}
				props.put(prop.Id, prop);
			}
			
			STSWR1.AbstractTrigger.Disabled = true;
			update props.values();
			STSWR1.AbstractTrigger.Disabled = false;
			
			return 'true';
		}
		catch(Exception ex)
		{
			return ex.getMessage();
		}
	}
	
	@RemoteAction
	public static String updateSwimlaneLabels() 
	{
		try
		{
			Map<String, String> namesMap = new Map<String, String>{
				'Lead Safety Manager' => 'Safety Scientist',
				'Safety Lead' => 'Safety Scientist',
				'Clinical Informatics Specialist' => 'Clinical Informatics Spec',
				'Clinical Systems Designer' => 'Clinical System Designer',
				'Clinical Systems Developer' => 'Clinical System Developer',
				'Coder' => 'Safety Data Coordinator',
				'Lead Coder' => 'Safety Data Coordinator',
				'Director Safety and Risk Management' => 'Dir Of Safety Risk Management',
				'Operational Team Leader' => 'Operational Team Lead',
				'Medical Writing' => 'Medical Writer'
			};
			
			List<STSWR1__Flow_Swimlane__c> swimlanes = [Select Name From STSWR1__Flow_Swimlane__c Where Name in :namesMap.keySet()];
			for (STSWR1__Flow_Swimlane__c s : swimlanes)
			{
				s.Name = namesMap.get(s.Name);
			}
			
			update swimlanes;
			
			return 'true';
		}
		catch(Exception ex)
		{
			return ex.getMessage();
		}
	}
	
}