public class PAWS_StepProperty_ConnectionPointCntrl
{
    public PAWS_StepProperty_ConnectionPointCntrl()
    {
    
        if(String.isEmpty(ApexPages.currentPage().getParameters().get('ropertyId')) == false)
        {
            FlowSelected = '';
        }
    }
    
    public String FlowSelected {get;set;}
    public List<SelectOption> FlowSelectOptions
    {
        get
        {
            if(FlowSelectOptions == null)
            {
                FlowSelectOptions = new List<SelectOption>{ new SelectOption('','-please select-') };
                for(STSWR1__Flow__c flow : [select Name from STSWR1__Flow__c where STSWR1__Type__c = 'Template' and STSWR1__Start_Type__c = 'Manual'])
                {
                    FlowSelectOptions.add(new SelectOption(flow.Id, flow.Name));
                }
            }
            return FlowSelectOptions;
        }
        set;
    }
    
    public String StepSelected {get;set;}
    public List<SelectOption> StepSelectOptions
    {
        get
        {
            StepSelectOptions = new List<SelectOption>{ new SelectOption('','-please select-') };
            for(STSWR1__Flow_Step_Junction__c step : [select Name from STSWR1__Flow_Step_Junction__c where STSWR1__Flow__c = :FlowSelected])
            {
                StepSelectOptions.add(new SelectOption(step.Id, step.Name));
            }
            return StepSelectOptions;
        }
        set;
    }
}