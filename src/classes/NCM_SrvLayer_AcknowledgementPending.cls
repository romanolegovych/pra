/**
 * @description	The Service Layer for the Acknowledgement Pending
 * @author		Kalaiyarasan Karunanithi
 * @date		Created: 10-Sep-2015
 */
public with sharing class NCM_SrvLayer_AcknowledgementPending {
// *********************************************************************************************************************
//				Build for Notification Pending Acknowledgement
// *********************************************************************************************************************
	
	/**
	 * @description	Build data in the 'PendingAcknowledgementWrapper' format, to let users pending acknowledgment records list.
	 				The data includes all the Pending Acknowledgment for the given user. The datas are
	 *				ordered by the notification Reminder.
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 10-Sep-2015
	 * @param		userId                  The user that is assigned with the registrations
	 * @return		The Notification data in the 'PendingAcknowledgementWrapper' format
	*/
	public static List<NCM_API_DataTypes.PendingAcknowledgementWrapper> getPendingAcknowledgement(ID userId) {
																								
		List<NCM_API_DataTypes.PendingAcknowledgementWrapper> results = new List<NCM_API_DataTypes.PendingAcknowledgementWrapper>();
		
		// Find the Notification and build the notification Pending Acknowledgment results
		List<Notification__c> notificationList = NCM_DataAccessor.getPendingAcknowledgement(userId);
		
		for(Notification__c notification : notificationList) {
			NCM_API_DataTypes.PendingAcknowledgementWrapper notificationWrapperItem = new NCM_API_DataTypes.PendingAcknowledgementWrapper(notification);
			
			notificationWrapperItem.notificationId 	= notification.Id;
			notificationWrapperItem.status			= notification.Status__c;
			notificationWrapperItem.reminder		= notification.Reminder__c;
			
			results.add(notificationWrapperItem);
		}
		
		return results;
	}
	
// *********************************************************************************************************************
//				Update for Notification Pending Acknowledgement
// *********************************************************************************************************************
	/**
	 * @description	Update the given wrapper data in the Notification object
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 10-Sep-2015
	 * @param		source    The wrapper that holds the new Notification records
	*/
	public static void updatePendingAcknowledgement(List<NCM_API_DataTypes.PendingAcknowledgementWrapper> source) {
		// Check for invalid parameters
		if(source == NULL || source.isEmpty() ) {
			return;
		}
		
		// Build the records that will be updated
		List<Notification__c> results = new List<Notification__c>();
		
		for(NCM_API_DataTypes.PendingAcknowledgementWrapper tmpWrapperItem : source) {
			Notification__c notification = new Notification__c();
			notification.Id=tmpWrapperItem.notificationId;
			notification.Status__c = NCM_API_DataTypes.ACKNOWLEDGEMENT;
			results.add( notification );
		}
		
		update results;
	}

}