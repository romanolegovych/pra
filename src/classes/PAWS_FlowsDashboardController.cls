public with sharing class PAWS_FlowsDashboardController
{
    public class PAWS_FlowsDashboardControllerException extends Exception {}
    
    public PAWS_FlowsDashboardController() {}
    
    public STSWR1__Stub__c FromDateStub { get{ return FromDateStub = (FromDateStub == null ? new STSWR1__Stub__c(STSWR1__Date__c = Date.today()) : FromDateStub);} set; }
    public STSWR1__Stub__c ToDateStub { get{ return ToDateStub = (ToDateStub == null ? new STSWR1__Stub__c(STSWR1__Date__c = Date.today()) : ToDateStub);} set; }
    public Date FromDateFilter { get { return FromDateStub.STSWR1__Date__c; } }
    public Date ToDateFilter { get { return ToDateStub.STSWR1__Date__c; } }
    
    public String SelectedFlow { get; set; }
    public List<SelectOption> FlowOptionList
    {
        get
        {
            if(FlowOptionList == null)
            {
                FlowOptionList = new List<SelectOption> { new SelectOption('', '- Please Select -') };
                List<ID> parentFlowIds = new List<ID>();
                for(AggregateResult each : [select STSWR1__Flow__r.STSWR1__Parent__c ParentId from STSWR1__Flow_Instance__c where STSWR1__Flow__r.STSWR1__Parent__c != null and STSWR1__Flow__r.STSWR1__Object_Type__c = 'ecrf__c' group by STSWR1__Flow__r.STSWR1__Parent__c])
                {
                    parentFlowIds.add(String.valueOf(each.get('ParentId')));
                }
                for(STSWR1__Flow__c each : [select Name from STSWR1__Flow__c where Id = :parentFlowIds order by Name])
                {
                    FlowOptionList.add(new SelectOption(each.Id, each.Name));
                }
            }
            return FlowOptionList;
        }
        set;
    }
    
    private List<STSWR1__Flow_Instance__c> FlowInstanceList
    {
        get
        {
            if(FlowInstanceList == null)
            {
                FlowInstanceList = loadFlowInstanceByParentId(SelectedFlow);
            }
            return FlowInstanceList;
        }
        set;
    }
    public List<BoardHeader> BoardHeaderList
    {
        get
        {
            if(BoardHeaderList == null && SelectedFlow != null)
            {
                BoardHeaderList = new List<BoardHeader>();
                List<FlowStepJunctionWrapper> steps = loadHeaderSteps();
                Set<String> keys = new Set<String>();
                for(FlowStepJunctionWrapper eachStep : steps)
                {   
                    if(keys.add(eachStep.MilestoneKey))
                    {
                        BoardHeader item = new BoardHeader();
                        item.MilestoneName = eachStep.MilestoneName;
                        item.MilestoneKey = eachStep.MilestoneKey;
                        item.Steps.add(eachStep);
                        BoardHeaderList.add(item);
                        continue;
                    }
                    
                    for(BoardHeader eachHeader : BoardHeaderList)
                    {
                        if(eachHeader.MilestoneKey == eachStep.MilestoneKey)
                        {
                            eachHeader.Steps.add(eachStep);
                            break;
                        }
                    }
                }
            }
            return BoardHeaderList;
        }
        set;
    }
    
    public Integer BoardRowsCount { get { return BoardRowList == null ? 0 : BoardRowList.size();} }
    public List<BoardRow> BoardRowList
    {
        get
        {
            if(BoardRowList == null && SelectedFlow != null)
            {
                BoardRowList = new List<BoardRow>();
                                
                List<STSWR1__Flow_Instance__c> flowInstanseList = FlowInstanceList;
                List<FlowInstanceWrapper> flowWrappedList = wrapflowInstance(flowInstanseList);
                
                List<String> cursorIdsList = new List<String>();
                List<String> objecIdsList = new List<String>();
                for(FlowInstanceWrapper flowEach : flowWrappedList)
                {
                    cursorIdsList.addAll(flowEach.getCursorIds());
                    objecIdsList.add(flowEach.Flow.STSWR1__Object_Id__c);
                }
                
                List<STSWR1__Flow_Instance_Cursor__c> cursorWithHistory = loadCursorInstanceWithHistoryByIds(cursorIdsList);
                Set<ID> stepIds = new Set<ID>();
                for(FlowInstanceWrapper flowEach : flowWrappedList)
                {
                    for(STSWR1__Flow_Instance_Cursor__c cursorEach : cursorWithHistory)
                    {
                        if(cursorEach.STSWR1__Flow_Instance__c == flowEach.FlowId && cursorEach.STSWR1__Flow_Instance_History__r.isEmpty() == false)
                        {
                            Set<String> historyKeys = new Set<String>();
                            for(STSWR1__Flow_Instance_History__c eachHistory : cursorEach.STSWR1__Flow_Instance_History__r)
                            {
                                stepIds.add(eachHistory.STSWR1__Step__c);
                                String historyKey = eachHistory.STSWR1__Cursor__c + '-' + eachHistory.STSWR1__Step__c;
                                if(historyKeys.add(historyKey) == true)
                                {
                                    flowEach.FlowHistories.add(eachHistory);
                                }
                            }
                        }
                    }
                    
                }
                
                Map<ID, ecrf__c> projectsList = new Map<ID, ecrf__c>([select Name from ecrf__c where Id = :objecIdsList]);
                
                Map<ID, STSWR1__Gantt_Step_Property__c> propertiesMap = getFlowPropertyByStepAndObject(stepIds, projectsList.keySet());
                System.debug(LoggingLevel.ERROR, System.JSON.serialize(propertiesMap));
                for(FlowInstanceWrapper flowEach : flowWrappedList)
                {
                    BoardRow rowItem = new BoardRow(projectsList.get(flowEach.Flow.STSWR1__Object_Id__c));
                    BoardRowList.add(rowItem);
                    List<FlowStepJunctionWrapper> stepList = new List<FlowStepJunctionWrapper>();
                    for(BoardHeader headerEach : BoardHeaderList)
                    {
                        stepList.addAll(headerEach.steps);
                        for(FlowStepJunctionWrapper stepEach : headerEach.steps)
                        {
                            StepItem stpItem = new StepItem(null);
                            stpItem.FlowHasStep = stepEach.FlowsMap.get(flowEach.Flow.STSWR1__Flow__c);
                            
                            for(STSWR1__Flow_Instance_History__c eachHistory : flowEach.FlowHistories)
                            {
                                String key = eachHistory.STSWR1__Step__r.STSWR1__Index__c + '-' + eachHistory.STSWR1__Step__r.Name + '-' +
                                    eachHistory.STSWR1__Step__r.STSWR1__Flow_Milestone__r.STSWR1__Index__c + '-' + eachHistory.STSWR1__Step__r.STSWR1__Flow_Milestone__r.Name;
                                if(key == stepEach.Key)
                                {
                                    stpItem = new StepItem(eachHistory);
                                    STSWR1__Gantt_Step_Property__c prop = propertiesMap.get(eachHistory.STSWR1__Step__c);
                                    stpItem.PlannedStartDate = prop == null ? null : prop.STSWR1__Planned_Start_Date__c;
                                    //break;
                                }
                            }
                            rowItem.Steps.add(stpItem);
                        }
                    }
                }
                
            }
            return BoardRowList;
        }
        set;
    }
    
    public void applyAction()
    {
        BoardHeaderList  = null;
        BoardRowList  = null;
        FlowInstanceList = null;
    }
    
    private List<FlowStepJunctionWrapper> loadHeaderSteps()
    {
        Set<String> flowIds = new Set<String>();
        for(STSWR1__Flow_Instance__c each : FlowInstanceList)
        {
            flowIds.add(each.STSWR1__Flow__c);
        }
        
        List<STSWR1__Flow_Step_Junction__c> result =
            [select
                Name, STSWR1__Flow_Milestone__r.Name, STSWR1__Flow_Milestone__r.STSWR1__Index__c, STSWR1__Index__c, STSWR1__Flow__c
            from
                STSWR1__Flow_Step_Junction__c
            where
                STSWR1__Flow__c = :flowIds and STSWR1__Flow__r.STSWR1__Status__c = 'Active'
            order by
                STSWR1__Flow__c, STSWR1__Flow_Milestone__r.STSWR1__Index__c, STSWR1__Index__c];
                
        List<FlowStepJunctionWrapper> stepJunctionWrapperList = new List<FlowStepJunctionWrapper>();
        
        //group by  =>  Name, Flow_Milestone__r.Name, Flow_Milestone__r.Index__c, Index__c
        Set<String> keys = new Set<String>();
        Map<String, FlowStepJunctionWrapper> flowsMap = new Map<String, FlowStepJunctionWrapper>();
        for(STSWR1__Flow_Step_Junction__c each : result)
        {
            FlowStepJunctionWrapper wrappedStep = new FlowStepJunctionWrapper(each);
            if(keys.add(wrappedStep.Key))
            {
                stepJunctionWrapperList.add(wrappedStep);
                flowsMap.put(wrappedStep.Key, wrappedStep);
            }
            else
            {
                FlowStepJunctionWrapper stepFromMap = flowsMap.get(wrappedStep.Key);
                stepFromMap.FlowsMap.put(each.STSWR1__Flow__c, true);
            }
        }
        return stepJunctionWrapperList;
    }
    
    private List<FlowStepJunctionWrapper> loadHeaderSubflowSteps(List<FlowStepJunctionWrapper> flowSteps)
    {
        //SubflowSteps
        for(FlowStepJunctionWrapper each : flowSteps)
        {
            
        }
        return null;
    }
    private List<STSWR1__Flow_Swimlane__c> loadSwimlanesByFlowId(String flowId)
    {
        return [select
                    Name, STSWR1__Flow__c, STSWR1__Index__c
                from
                    STSWR1__Flow_Swimlane__c
                where
                    STSWR1__Flow__c = :flowId
                order by
                    STSWR1__Index__c];
    }
    
    private List<STSWR1__Flow_Instance__c> loadFlowInstanceByFlowName(String flowName)
    {
        return [select 
                    STSWR1__Flow__c, STSWR1__Is_Active__c, STSWR1__Is_Completed__c, STSWR1__Number_of_Active_Cursors__c,
                    STSWR1__Number_of_Cursors__c, STSWR1__Object_Id__c, STSWR1__Object_Name__c, STSWR1__Object_Type__c,
                    (select Id from STSWR1__Flow_Instance_Cursors__r)
                from
                    STSWR1__Flow_Instance__c
                where
                    STSWR1__Flow__r.Name = :flowName];
    }
    
    private List<STSWR1__Flow_Instance__c> loadFlowInstanceByParentId(String parentId)
    {
        return [select 
                    STSWR1__Flow__c, STSWR1__Is_Active__c, STSWR1__Is_Completed__c, STSWR1__Number_of_Active_Cursors__c,
                    STSWR1__Number_of_Cursors__c, STSWR1__Object_Id__c, STSWR1__Object_Name__c, STSWR1__Object_Type__c,
                    (select Id from STSWR1__Flow_Instance_Cursors__r)
                from
                    STSWR1__Flow_Instance__c
                where
                    STSWR1__Flow__r.STSWR1__Parent__c = :parentId
                order by
                    STSWR1__Object_Name__c];
    }
    
    private List<STSWR1__Flow_Instance_Cursor__c> loadCursorInstanceWithHistoryByIds(List<String> cursorIds)
    {
        return [select
                    Id, STSWR1__Flow_Instance__c, STSWR1__Step__r.STSWR1__Flow_Swimlane__c,
                    (select 
                        CreatedDate, STSWR1__Cursor__c, STSWR1__Status__c, STSWR1__Step__c, STSWR1__Step_Name__c,
                        STSWR1__Step__r.STSWR1__Flow_Swimlane__c, STSWR1__Actual_Start_Date__c, STSWR1__Step__r.STSWR1__Duration__c,
                        STSWR1__Cursor__r.STSWR1__Step_Changed_Date__c, STSWR1__Step__r.STSWR1__Index__c, STSWR1__Step__r.STSWR1__Flow_Milestone__r.Name,
                        STSWR1__Step__r.STSWR1__Flow_Milestone__r.STSWR1__Index__c, STSWR1__Step__r.Name
                    from
                        STSWR1__Flow_Instance_History__r
                    order by
                        CreatedDate desc)
                from
                    STSWR1__Flow_Instance_Cursor__c
                where
                    Id = :cursorIds];
    }
    
    // returns map StepId & Flow_Instance_History__c
    private Map<String, STSWR1__Flow_Instance_History__c> getAllHistoryMap(List<STSWR1__Flow_Instance_Cursor__c> cursorList)
    {
        Map<String, STSWR1__Flow_Instance_History__c> result = new Map<String, STSWR1__Flow_Instance_History__c>();
        for(STSWR1__Flow_Instance_Cursor__c cursorEach : cursorList)
        {
            if(cursorEach.STSWR1__Flow_Instance_History__r.isEmpty() == false)
            {
                STSWR1__Flow_Instance_History__c history = cursorEach.STSWR1__Flow_Instance_History__r.get(0);
                result.put(history.STSWR1__Step__c, history);
            }
        }
        return result;
    }
    
    private List<Object> selectFromSobject(String fieldName, List<sObject> sobjectList)
    {
        List<Object> result = new List<Object>();
        for(sObject eachSobject : sobjectList)
        {
            result.add(eachSobject.get(fieldName));
        }
        return result;
    }
    
    private List<String> selectFromSobjectStringList(String fieldName, List<sObject> sobjectList)
    {
        List<String> result = new List<String>();
        
        try
        {
            for(sObject eachSobject : sobjectList)
            {
                result.add((String)eachSobject.get(fieldName));
            }
        }
        catch(Exception ex)
        {
        }
        
        return result;
    }
    
    private List<FlowInstanceWrapper> wrapflowInstance(List<STSWR1__Flow_Instance__c> inputFlowsList)
    {
        List<FlowInstanceWrapper> result = new List<FlowInstanceWrapper>();
        for(STSWR1__Flow_Instance__c flowEach: inputFlowsList)
        {
            result.add(new FlowInstanceWrapper(flowEach));
        }
        return result;
    }
    
    private static Set<ID> loadFlowsIdsWithSubFlows(String flowId)
    {
        Set<ID> flowIds = new Set<ID>{flowId};

        Set<ID> subFlowsIds = new Set<ID>{flowId};
        Set<ID> temp = new Set<ID>();
        while(true)
        {
            temp.clear();
            for(STSWR1__Flow_Step_Action__c action : [select STSWR1__Flow_Step__c, STSWR1__Flow_Step__r.STSWR1__Flow__c, STSWR1__Config__c from STSWR1__Flow_Step_Action__c where STSWR1__Flow_Step__r.STSWR1__Flow__c in :subFlowsIds and STSWR1__Type__c = 'Start Sub Flow' and STSWR1__Status__c = 'Active'])
            {
                Map<String, Object> config = (Map<String, Object>)JSON.deserializeUntyped(action.STSWR1__Config__c);
                if(config != null && !String.isEmpty((String)config.get('flowId')) && !flowIds.contains((ID)config.get('flowId'))) temp.add((ID)config.get('flowId'));
            }
                
            if(temp.size() == 0) break;
                
            flowIds.addAll(temp);
            subFlowsIds = new Set<ID>(temp);
        }
        
        return flowIds;
    }
    
    public Map<ID, STSWR1__Gantt_Step_Property__c> getFlowPropertyByStepAndObject(Set<ID> stepIds, Set<ID> recordIds)
    {
        List<String> fields = new List<String>();
        Schema.DescribeSObjectResult describe = STSWR1__Gantt_Step_Property__c.getSObjectType().getDescribe();
        fields.addAll(describe.fields.getMap().keySet());
        
        describe = STSWR1__Flow_Step_Junction__c.getSObjectType().getDescribe();
        for(String fieldName : describe.fields.getMap().keySet())
        fields.add('STSWR1__Step__r.' + fieldName);
        
        describe = STSWR1__Flow__c.getSObjectType().getDescribe();
        for(String fieldName : describe.fields.getMap().keySet())
        fields.add('STSWR1__Step__r.STSWR1__Flow__r.' + fieldName);
        
        fields.add('STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Object_Id__c');
        
        Map<ID, STSWR1__Gantt_Step_Property__c> result = new Map<ID, STSWR1__Gantt_Step_Property__c>();
        for(STSWR1__Gantt_Step_Property__c record : (List<STSWR1__Gantt_Step_Property__c>)Database.query('select ' + String.join(fields, ',') + ' from STSWR1__Gantt_Step_Property__c where STSWR1__Step__c in :stepIds and (STSWR1__Cursor__c = null or STSWR1__Cursor__r.STSWR1__Flow_Instance__r.STSWR1__Object_Id__c in :recordIds) order by STSWR1__Step__c, CreatedDate asc'))
        {
        	STSWR1__Gantt_Step_Property__c prevProperty = result.get(record.STSWR1__Step__c);
            if(prevProperty != null && prevProperty.STSWR1__Cursor__c != null && record.STSWR1__Cursor__c == null) continue;
            
            result.put(record.STSWR1__Step__c, record);
        }
        return result;
    }
    
    public class FlowInstanceWrapper
    {
        public FlowInstanceWrapper(STSWR1__Flow_Instance__c inputFlow)
        {
            Flow = inputFlow;
            Cursors = new List<STSWR1__Flow_Instance_Cursor__c>();
            FlowHistories = new List<STSWR1__Flow_Instance_History__c>();
        }
        
        public ID FlowId { get { return Flow.Id; } }
        public STSWR1__Flow_Instance__c Flow { get; set; }
        public List<STSWR1__Flow_Instance_Cursor__c> Cursors { get; set; }
        public List<STSWR1__Flow_Instance_History__c> FlowHistories { get; set; }
        
        public List<String> getCursorIds()
        {
            List<String> cursorIdsList = new List<String>();
            if(Flow != null && Flow.STSWR1__Flow_Instance_Cursors__r != null)
            {
                for(STSWR1__Flow_Instance_Cursor__c cursorEach : Flow.STSWR1__Flow_Instance_Cursors__r)
                {
                    cursorIdsList.add(cursorEach.Id);
                }
            }
            return cursorIdsList;
        }
    }
    
    public class BoardHeader
    {
        public BoardHeader()
        {
            Steps = new List<FlowStepJunctionWrapper>();
        }
        
        public String MilestoneName { get; set; }
        public String MilestoneKey { get; set; }
        public Integer StepsCount { get { return Steps == null ? 1 : Steps.size();} }
        public List<FlowStepJunctionWrapper> Steps { get; set; }
    }
    
    public class BoardRow
    {
        public BoardRow(sObject obj)
        {
            Steps = new List<StepItem>();
            RowObject = obj;
        }
        
        public List<StepItem> Steps { get; set; }
        public sObject RowObject { get; set; }
        public String ObjectName { get { return RowObject != null ? String.valueOf(RowObject.get('Name')) : null; } }
    }
    
    public class StepItem
    {
        public StepItem(STSWR1__Flow_Instance_History__c inputStep)
        {
            Step = inputStep;
        }
        // Status could be: completed, late, inProgress, pending
        public String StatusCssStyle
        {
            get
            {
                if(Step != null)
                {
                    
                    DateTime expirationDate;
                    /*
                    if(Step.STSWR1__Step__r.STSWR1__Duration__c != null)
                    {
                        Integer duration = Step.STSWR1__Step__r.STSWR1__Duration__c == 0 ? 1 : Integer.valueOf(Step.STSWR1__Step__r.STSWR1__Duration__c);
                        expirationDate = Step.STSWR1__Cursor__r.STSWR1__Step_Changed_Date__c.addDays(duration);
                    }
                    */
                    if(Step.STSWR1__Step__r.STSWR1__Duration__c != null && PlannedStartDate != null)
                    {
                        Integer duration = Step.STSWR1__Step__r.STSWR1__Duration__c == 0 ? 1 : Integer.valueOf(Step.STSWR1__Step__r.STSWR1__Duration__c);
                        expirationDate = PlannedStartDate.addDays(duration);
                    }
                    
                    if(Step.STSWR1__Status__c == 'In Progress' && expirationDate != null && expirationDate < DateTime.now())
                    {
                        StatusCssStyle = 'late';
                    }
                    else if(Step.STSWR1__Status__c == 'In Progress')
                    {
                        StatusCssStyle = 'inProgress';
                    }
                    else if(Step.STSWR1__Status__c == 'Completed' || Step.STSWR1__Status__c == 'Approved' || Step.STSWR1__Status__c == 'Rejected')
                    {
                        StatusCssStyle = 'completed';
                    }
                    else if(Step.STSWR1__Status__c == 'Pending')
                    {
                        StatusCssStyle = 'pending';
                    }
                }
                else if(Step == null && FlowHasStep == true)
                {
                    StatusCssStyle = 'notStarted';
                }
                
                return StatusCssStyle;
            }
            set;
        }
        public String Id { get { return Step == null ? null : Step.Id; } }
        public STSWR1__Flow_Instance_History__c Step { get; set; }
        public Boolean FlowHasStep { get; set; }
        public Datetime PlannedStartDate { get; set; }
    }
    
    //select Flow__r.Name Name, Count(id) Total, Parent__c from Flow_Instance__c group by Flow__r.Name, Parent__c
    
    public class FlowStepJunctionWrapper
    {
        public String Name { get; set; }
        public Integer Index { get; set; }
        public String Key { get; set; }
        
        public String MilestoneName { get; set; }
        public Integer MilestoneIndex { get; set; }
        public String MilestoneKey { get; set; }
        public STSWR1__Flow_Step_Junction__c FlowStepJunction { get; set; }
                
        public List<FlowStepJunctionWrapper> SubflowSteps { get; set; }
        
        public Map<String, Boolean> FlowsMap { get; set; }
        
        // Flow_Step_Junction__c
        public FlowStepJunctionWrapper(STSWR1__Flow_Step_Junction__c step)
        {
            FlowStepJunction = step;
            Name = step.Name;
            Index = Integer.valueOf(step.STSWR1__Index__c);
            FlowsMap = new Map<String, Boolean> {step.STSWR1__Flow__c => true} ;
            
            MilestoneName = step.STSWR1__Flow_Milestone__r.Name;
            MilestoneIndex = Integer.valueOf(step.STSWR1__Flow_Milestone__r.STSWR1__Index__c);
            MilestoneKey = MilestoneIndex + '-' + MilestoneName;
            
            Key = Index + '-' + Name + '-' + MilestoneKey;
        }
    }
}