/**
  'PBB_TestUtils' is the TestUtil class used to create the data for PBB.
  @author  Ramya Shree Edara
  @version  1.0.0.0
  @since    26-Nov-2014
**/

@isTest(Seealldata=false)
Public with sharing class PBB_TestUtils {
    
    public static final String PROTOCOL_UNIQUE_KEY_VALUE = '23';
    
    Public Account acc;
    Public Opportunity opp;
    Public Country__c country;
    public PRA_Business_Unit__c prabu;
    Public Job_Class_Desc__c jcd;
    Public WFM_Project__c wfmp;
    Public Bid_Project__c bid;
    
    public Vendor_Category__c  vc;
    public Vendor__c  v;  
    public Project_Vendors__c pv;
        
    Public Service_Model__c smm;
    Public Service_Area__c Sa;
    Public Service_Function__c Sf ;
    Public Service_Task__c St;
    
    public Formula_Name__c billFormulaName ;
    public Formulas__c billFormula; 

    //Questions
    Public Service_Impact_Question_Name__c siqn;
    Public Service_Impact_Questions__c siqs;
    Public Service_Impact_Response__c sir;
    Public Service_Task_To_Service_Impact__c STtoSI;
    Public Service_Task_To_Drivers__c  STtoBF; 
    
    Public WR_Model__c Wrm;
    Public STSWR1__Flow__c Fl;
    Public STSWR1__Flow_Step_Junction__c Fsj;
    Public Service_Task_To_Flow_Step__c stfs;
    
    
    public Therapeutic_Area__c therp;
    public Indication_Group__c Indgrp;
    public Primary_Indication__c prmyind;
    public WFM_Phase__c Phase;
    
    Public Mapping_Region_Model__c mrm; 
    Public SRM_Model__c Sm;    
    Public SRM_Model_Site_Activation__c MSA;  
    Public SRM_Model_Subject_Enrollment__c MSE;   
    Public SRM_Calender_Adjustments__c SCA;
    Public SRM_Week_Adjustments__c SWA;         
    Public SRM_Model_Questions__c SMQ;          
    Public SRM_Model_Response__c SMR;                     
    //public SRM_Scenario__c srmScen; 
    
    Public PBB_Scenario__c scen;
    Public PBB_Scenario_Country__c psc;
    Public SRM_Scenario_Country__c scecont;
    Public List<PBB_Weekly_Events__c> pweList ;
    public PBB_Weekly_Events__c  prawe;
    Public Countries_Service_Tasks__c CSt;
    
    
    /** billModelList holds all Bill Rate Card Model*/
    public Bill_Rate_Card_Model__c billModel;
    /** billRateList is the hold of Bill rate records */
    public Bill_Rate__c billRate;
    public Currency__c currencyy;
    
    public WFM_Protocol__c protocol;
    public WFM_Site_Detail__c site;
    
    public Protocol_Country__c protocolCountry ;
    public Country_Weekly_Event__c countryWeekly;
    public NotificationCatalog__c notificationCatalogProto;
    public NotificationRegistration__c notifiRegistratn ;
    public NotificationRegistration__c  notifiRegistratnSite ;
    public NotificationCatalog__c  notificationCatalogSite;
    
    /**
    *   This Method is to create the test data.          
    */
    public void PBB_TestUtils() {
        
    }
    
    /**
    *   This Method is to create the Country data.          
    */
    Public Account createAccountAttributes() {
        acc = new Account ();
        acc.Name = 'Test Account';
        acc.Status__c = 'Active';
        acc.Rating = 'A';
        acc.Type = 'Competitor';
        acc.Industry = 'Small/Speciality Pharma';
        acc.Therapeutic_Areas__c = 'Cardio-Metabolic Diseases';
        insert acc;
        return acc;
    } 
    
     /**
    *   This Method is to create the Country data.          
    */
    Public Opportunity createOpportunityAttributes() {
        opp = new Opportunity ();
        opp.Name = 'Test Oppty';
        opp.AccountId = acc.Id;
        opp.StageName = 'Opportunity Identified';
        opp.Type = 'New Business';
        opp.Business_Unit__c = 'PR-Safety & Risk Management';
        opp.PRA_Project_ID__c='44441230-333123';
        opp.Phase__c = 'Phase I';
        opp.Therapeutic_Area__c = 'Cardio-Metabolic Diseases';
        opp.Indication_Group__c = 'Arterial disease';
        opp.Study_Start_Date__c = Date.Today().addDays(1);
        opp.Study_End_Date__c = Date.Today().addDays(185);
        opp.RFP_1st_Patient_In_Date__c = Date.Today().addDays(14);
        opp.RFP_Last_Patient_In_Date__c = Date.Today().addDays(180);
        opp.Expected_Patients__c = 5000;
        opp.Expected_Sites__c = 500;
        opp.NBA_Date__c = Date.Today().addDays(1);
        opp.Probability = 10;
        opp.ForecastCategoryName = 'Pipeline';
       
        opp.CloseDate = Date.Today().addDays(190);
        insert opp;
        return opp;
    } 
    
    
    
    /**
    *   This Method is to create the Country data.          
    */
    Public Country__c createCountryAttributes() {
        country = new Country__c(Name = 'Test India',Country_Code__c = 'TI',Daily_Business_Hrs__c = 8,Description__c = 'Test India',PRA_Country_Id__c = 'TI1');
        insert country;
        return country;
    } 
    
    /**
    *   This Method is to create the PRA Business Unit data.          
    */
    Public PRA_Business_Unit__c createPRABusinessUnits() {
        prabu = new PRA_Business_Unit__c(name = 'Product Registration',Business_Unit_Code__c = 'PR',Status__c ='A');
        insert prabu;
        return prabu;
    }
    
    /**
    *   This Method is to create Job Class Dec data.          
    */
    Public Job_Class_Desc__c createJobClass() {
        jcd = new Job_Class_Desc__c(Is_Assign__c =true,IsClinical__c = false,Job_Code__c = 'aa345678',
                                    Job_Class_Code__c ='gg5678', Job_Position_Long__c = 'desc9889');
        insert jcd;
        return  jcd;
    } 
    
    /**
    *   This Method is to create the WFM Project data.
    */
    Public WFM_Project__c createWfmProject() {
        wfmp = new WFM_Project__c(Name='P1', Project_Unique_Key__c='PUK000001');
        System.debug('>>> wfmp >>> '+wfmp);
        insert wfmp;  
        return wfmp;
    }
    
    /**
    *   This Method is to create the Bid Project data.          
    */
    Public Bid_Project__c createBidProject(){
        bid = new Bid_Project__c(Name='BS1',Study_Startup_Activities_Begin__c=Date.Today(),Business_Segment__c='Product Registration',  PRA_Project_ID__c=wfmp.Id);   
        System.debug('>>> bid >>> '+bid);  
        insert bid;  
        return bid;
    }
        
    /**
    *   This Method is to create Vendor Category data    
    */
    public Vendor_Category__c createVendorCategory(){
        vc = new Vendor_Category__c(Name='VC1',Unique_Vendor_Category__c='KNR000001');
        insert vc;
        return vc;
    }
    
    /**
    *   This Method is to create Vendor data          
    */
    public Vendor__c createVendor(){
        v = new Vendor__c(Name='V1',Vendor_Category__c=vc.id,Unique_Vendor__c='INR000001');
        insert v;
        return v;
    }
    
    /**
    *   This Method is to create Project Vendor data          
    */
    public Project_Vendors__c createOtherProjectVendors(){
        pv = new Project_Vendors__c(Bid_Project__c=bid.id,Vendor_Category__c=vc.id,Vendor_Name_Other__c='OV1');
        insert pv;
        return pv;
    }    

    /**
    *   This Method is to create Project Vendor data          
    */
    public Project_Vendors__c createProjectVendors(){
        pv = new Project_Vendors__c(Bid_Project__c=bid.id,Vendorr__c='V2',Vendor_Category__c=vc.id);
        insert pv;
        return pv;
    }   
    
    /**
    *   This Method is to create the Service Model data.          
    */
    Public Service_Model__c CreateServiceModelAttributes() {
        smm = new Service_Model__c(Name='name16',Description__c ='desc0016',Status__c ='In-Process');
        insert smm;           
        return smm;
    } 
    
    /**
    *   This Method is to create the Service Area data.          
    */
    Public Service_Area__c createServiceAreaAttributes(){
        Sa = new Service_Area__c(Description__c = 'Description01',Service_Model__c = smm.Id);
        insert Sa;
        return Sa;
    } 
    
    /**
    *   This Method is to create the Service Function data.          
    */
    Public Service_Function__c createServiceFunctionAttributes() {
        Sf = new Service_Function__c(Description__c = 'Description02',Service_Area__c = Sa.Id);
        insert Sf;
        return Sf;
    } 
  
    /**
    *   This Method is to create the Service Task data.          
    */
    Public Service_Task__c createServiceTaskAttributes() {
        St = new Service_Task__c(Description__c = 'Description03',Business_Segment__c='Product Registration',Service_Function__c = Sf.Id);
        insert St;
        return St;
    } 
    
    /**
    *   This Method is to create the Formula_Name__c
    */
    public Formula_Name__c createBillFormulaNames(){
        billFormulaName = new Formula_Name__c(Name='Bill Formula Name1');
        insert billFormulaName ;
        return billFormulaName ;    
    }
    
    /**
    *   This Method is to create the Formulas__c.          
    */
    public Formulas__c createBillFormula(){
       billFormula = new Formulas__c(Formula_Name__c=billFormulaName.id);
       insert billFormula ;
       return billFormula ;   
    }
    
    /**
    *   This Method is to create the Service_Impact_Question_Name__c data.          
    */
    Public Service_Impact_Question_Name__c  createSIQNAttributes() {
        siqn = new Service_Impact_Question_Name__c(Description__c = 'Question 1',Name='Q1',isActive__c=true);
        insert siqn ;
        return siqn ;
    } 
    
    /**
    *   This Method is to create the Service_Impact_Questions__c data.          
    */
    Public Service_Impact_Questions__c  createSIQSAttributes(String QAto) {
        siqs = new Service_Impact_Questions__c(Question__c= 'Question 1',Status__c='In-Process',Question_Applies_to__c=QAto,Service_Impact_Question__c=siqn.id);
        insert siqs ;
        return siqs ;
    } 
    
    /**
    *   This Method is to create the Service_Impact_Response__c data.          
    */
    Public Service_Impact_Response__c createSIRAttributes() {
        sir= new Service_Impact_Response__c(name='Q1R1',Service_Impact_Questions__c=siqs.id);
        insert sir;
        return sir;
    } 
    
    /**
    *   This Method is to create the Service_Task_To_Service_Impact__c data.          
    */
    Public Service_Task_To_Service_Impact__c createSTtoSI(Service_Task__c st,Service_Impact_Question_Name__c siq) {
        Service_Task_To_Service_Impact__c  STtoSI= new Service_Task_To_Service_Impact__c (Service_Task__c=st.id,Service_Impact_Question_Name__c =siq.id);
        insert STtoSI;
        return STtoSI;
    } 
    
    
    /**
    *   This Method is to create the Service_Task_To_Drivers__c data.          
    */
    Public Service_Task_To_Drivers__c createSTtoBF(Service_Task__c st,Formula_Name__c bfn) {
        Service_Task_To_Drivers__c STtoBF= new Service_Task_To_Drivers__c(Service_Task__c=st.id,Formula_Name__c =bfn.id);
        STtoBF.Status__c = 'In-Progress';
        STtoBF.Frequency__c= 'Study';
        STtoBF.Times_Frequency__c=1; 
        STtoBF.Per_Frequency__c =1; 
        insert STtoBF;
        return STtoBF;
    }
    
    /**
    *   This Method is to create the WR Model data.          
    */
    Public WR_Model__c createWRModelAttributes() {
        Wrm = new WR_Model__c(Name='name61',Description__c ='desc00817',Service_Model__c = smm.Id);
        insert Wrm;
        return Wrm;
    } 
    
    /**
    *   This Method is to create the Flow data.          
    */
    Public STSWR1__Flow__c createFlowAttributes() {
        Fl = new STSWR1__Flow__c();
        insert Fl;
        return Fl;
    } 
    
    /**
    *   This Method is to create the Flow Step Junction data.          
    */
    Public STSWR1__Flow_Step_Junction__c createFlowStepAttributes() {
        Fsj = new STSWR1__Flow_Step_Junction__c(STSWR1__Flow__c = Fl.id);
        insert Fsj;
        return Fsj;
    } 
    
    /**
    *   This Method is to create Service Task To Flow Step data.          
    */
    Public Service_Task_To_Flow_Step__c createTaskToFlow() {
        stfs = new Service_Task_To_Flow_Step__c(WR_Model__c = Wrm.Id,Service_Tasks__c = St.Id,Flow_Step__c = fsj.Id);
        insert stfs;
        return  stfs;
    } 
    
    /**
    *   This Method is to create the Therapeutic data.
    */
    Public Therapeutic_Area__c createTherapeutic() {
        therp = new Therapeutic_Area__c(Name='Cardio-Metabolic Diseases',TA_External_ID__c = 'Cardio-Metabolic Diseases');
        System.debug('>>> therp >>> '+therp);
        insert therp;  
        return therp;
    }
    
    /**
    *   This Method is to create the Primary Indication
    */
    Public Primary_Indication__c createPrimaryIndication() {
        prmyind = new Primary_Indication__c(Name='prmyind1',PI_External_ID__c = 'prmyind1' );
        System.debug('>>> prmyind>>> '+prmyind);
        insert prmyind;  
        return prmyind;
    }    
    
    /**
    *   This Method is to create the Indication Group
    */
    Public Indication_Group__c createIndicationGroup() {
        Indgrp = new Indication_Group__c(Name='Indgrp1',IG_External_ID__c = 'Indgrp1' );
        System.debug('>>> Indgrp>>> '+Indgrp);
        insert Indgrp;  
        return Indgrp;
    }
    
    /**
    *   This Method is to create the WFM_Phase
    */
    Public WFM_Phase__c createWFMPhase() {
        Phase = new WFM_Phase__c(Name='Phase1');
        System.debug('>>> Phase>>> '+Phase);
        insert Phase;  
        return Phase;
    }
    
    /**
    *   This Method is to create Mapping Region Model data.          
    */
    Public Mapping_Region_Model__c createMappingRegion() {
        mrm = new Mapping_Region_Model__c(Description__c = 'desc9898',Is_Active__c = false,/*mrm.Model_Number__c ='',*/ Model_Type__c = 'Genesis');
        insert mrm;
        return  mrm;
    }
    
    /**
    *   This Method is to approve the service model
    */
    Public Service_Model__c approveServicemodel(Service_Model__c  smodel) {
        smodel.status__c = 'Approved';
        upsert smodel;
        return smodel;
    } 
    
    /**
    *   This Method is to create the SRM Model data.          
    */
    Public SRM_Model__c createSRMModelAttributes(){
        sm = new SRM_Model__c(Comments__c='testComment6',Status__c='In Progress');
        insert sm;
        system.debug('----sm-'+sm);  
        return sm;
    } 
    
    
    /**
    *   This Method is to approve the SRM_Model__c 
    */
    Public SRM_Model__c approveSRMmodel(SRM_Model__c srmmodel) {
        srmmodel.status__c = 'Approved';
        upsert srmmodel;
        return srmmodel;
    } 
    
    /**
    *   This Method is to create the SRM_Model_Site_Activation__c data.          
    */
    Public  SRM_Model_Site_Activation__c  createSRMModelSiteActAttributes(){
        MSA = new SRM_Model_Site_Activation__c();
        MSA.SRM_Model__c = sm.Id;
        MSA.Country__c = country.Id;
        MSA.Number_of_Weeks__c = 10;
        MSA.Gaussian_Constant__c = 10;
        MSA.Amplitude__c = 100;
        MSA.Shift_Center_By__c = 2;
        MSA.Standard_Deviation__c = 4;
        MSA.Start_Week__c = 2;
        MSA.Site_Activation_Formula__c = 'Gaussian';
        
        MSA.Sponsor__c = acc.id;
        //MSA.Therapeutic__c = therp.id;
        MSA.IRB_Type__c = 'N/A';
        
        insert MSA;
        return MSA;
    } 
    
    /**
    *   This Method is to create the SRM_Model_Subject_Enrollment__c data.          
    */
    Public SRM_Model_Subject_Enrollment__c createSRMModelSubjEnrol(){
        MSE= new SRM_Model_Subject_Enrollment__c ();
        MSE.SRM_Model__c = sm.Id;
        MSE.Country__c = country.Id;
        MSE.Subject_Enrollment_Rate__c = 3;
        MSE.Standard_Deviation__c =1;
        MSE.Shift_Center_By__c=1;
        insert MSE;
        return MSE;
    } 
    
     /**
    *   This Method is to create the SRM_Calender_Adjustments__c data.          
    */
    Public SRM_Calender_Adjustments__c createSRMCalendarAdj(){
        SCA= new SRM_Calender_Adjustments__c ();
        SCA.SRM_Model__c = sm.Id;
        SCA.Country__c = country.Id;
        SCA.From_Date__c = Date.newInstance(2015, 7, 22);
        SCA.To_Date__c = Date.newInstance(2016, 1, 22);
        System.debug('>>> sdtd >>> '+SCA.From_Date__c  + ' >>> todt >>> '+SCA.To_Date__c);
        SCA.Site_Act_Adjustment__c = 100;
        SCA.Subject_Enroll_Adjustment__c = -100;
        insert SCA;
        return SCA;
    } 
    
     /**
    *   This Method is to create the SRM_Week_Adjustments__c data.          
    */
    Public SRM_Week_Adjustments__c createSRMWeekAdj(){
        SWA= new SRM_Week_Adjustments__c ();
        SWA.Country__c = country.Id;
        SWA.SRM_Model__c = sm.Id;
        SWA.Week_Of_Project__c = 15;
        SWA.Site_Act_Adjustment__c = 100;
        SWA.Subject_Enroll_Adjustment__c = 100;
        insert SWA;
        return SWA;
    } 
    
     /**
    *   This Method is to create the SRM_Model_Questions__c data.          
    */
    Public SRM_Model_Questions__c createSRMModelQues(){
        SMQ= new SRM_Model_Questions__c ();
        SMQ.SRM_Model__c = sm.Id;
        SMQ.Question__c = 'This is test question';
        SMQ.Primary_Role__c = 'Proposal Director';
        SMQ.Group__c = 'Project';
        SMQ.Sub_Group__c = 'Site Activation';
        SMQ.Country__c = country.Id;
        insert SMQ;
        return SMQ;
    }
    
     /**
    *   This Method is to create the SRM_Model_Response__c data.          
    */
    Public SRM_Model_Response__c createSRMModelResp(){
        SMR= new SRM_Model_Response__c ();
        SMR.SRM_Question_ID__c = SMQ.Id;
        SMR.Response__c = 'Yes';
        SMR.Adjust_LPI__c = 5;
        insert SMR;
        return SMR;
    } 
          
       
    /**
    *   This Method is to create the SRM Scenario Country data.          
    */
    Public SRM_Scenario_Country__c createSrmCountryAttributes() {
        scecont = new SRM_Scenario_Country__c(PBB_Scenario_Country__c = psc.Id,Expected_Sub_Enrollment_Rate__c = 1,Subject_Enrollment_Rate__c = 2,Target_Number_Of_Sites__c = 300,Target_Number_Subjects__c = 2000);
        insert scecont;
        return scecont;
    } 
    
    /**
    * author Niharika Reddy
    * Desc Method to create SRM Scenario data
    */
   /* public SRM_Scenario__c createSRMScenario(){
        srmScen = new SRM_Scenario__c();
        srmScen.Bid_Project__c = createbidproject().Id;
        srmScen.Expected_Patients__c = 100;
        srmScen.Expected_Sites__c = 1000;
        srmScen.Final_Protocol_Date__c = Date.Today().addDays(2);
        srmScen.First_Patient_Randomized__c = Date.Today().addDays(5);
        srmScen.Last_Patient_Out__c = Date.Today().addDays(150);
        srmScen.Last_Patient_Randomized__c = Date.Today().addDays(180);
        srmScen.Project_Start_Date__c = Date.Today().addDays(1);
        srmScen.SRM_Model__c = CreateSRMModelattributes().Id;
        srmScen.Status__c = 'In Progress';
        insert srmScen;
        return srmScen;
    }*/
    
    /**
    *   This Method is to create the PBB Scenario data.          
    */
    Public PBB_Scenario__c createScenarioAttributes(){
        scen = new PBB_Scenario__c(Description__c = 'Description01T' ,Bid_Project__c = bid.Id );
        scen.Expected_Patients__c = 100;
        scen.Expected_Sites__c = 1000;
        scen.Final_Protocol_Date__c = Date.Today().addDays(2);
        scen.First_Patient_in__c = Date.Today().addDays(5);
        scen.Last_Patient_Out__c = Date.Today().addDays(150);
        scen.Last_Patient_in__c = Date.Today().addDays(180);
        scen.Project_Start_Date__c = Date.Today().addDays(1);
        scen.SRM_Model__c = ((sm==null)?createSRMModelAttributes().id:sm.Id);
        scen.Status__c = 'In Progress';
        scen.Sponsor__c = ((acc==null)?'':acc.name);
        scen.protocol_name__c = 'protocol1';
        
        insert scen;  
        return scen;
    } 
    
    /**
    *   This Method is to create PBB Scenario Country Model data.          
    */
    Public PBB_Scenario_Country__c createPBBScenarioCountry() {
        psc = new PBB_Scenario_Country__c(Country__c = country.Id,PBB_Scenario__c = scen.Id);
        insert psc;
        return  psc;
    }
    
    /**
    *   This Method is to create the PRA weekly Events data.          
    */
    Public PBB_Weekly_Events__c createPRAWeeklyEvents() {
        prawe = new PBB_Weekly_Events__c(name = 'week',SRM_Scenario_Country__c =scecont.id,Actual_Subjects__c = 10.00,Expected_Subjects__c = 12.00);
        insert prawe;
        return prawe;
    }
    
    Public List<PBB_Weekly_Events__c> createPBBWeeklyEvents(SRM_Scenario_Country__c scecont,integer NoWeeks) {
        pweList = new List<PBB_Weekly_Events__c>();
        Integer days=0;
        for(integer i=1;i<=NoWeeks;i++){
            PBB_Weekly_Events__c pwe = new PBB_Weekly_Events__c();
            pwe.SRM_Scenario_Country__c =scecont.id;
            pwe.name=String.valueof(i);
            pwe.Week_Date__c=date.today()+days;
            pwe.Week_Number__c=i;
            pwe.Planned_Sites__c = 1;
            pwe.Planned_Subjects__c = 1;
            if(i==1 || i==2){
                pwe.Actual_sites__c = 1;
                pwe.Actual_Subjects__c = 1;
            }else{
                pwe.Low_Risk_Sites__c = 2;
                pwe.Medium_Risk_Sites__c = 3;
                pwe.High_Risk_Sites__c = 4;
                
                pwe.Low_Risk_Subjects__c = 2;
                pwe.Medium_Risk_Subjects__c = 3;
                pwe.Expected_Subjects__c = 4;
            }
        
            pweList.add(pwe);
            days=days+7;
        }
        insert pweList ;
        return  pweList ;
    }
        
    /**
    *   This Method is to create the Country Service Task data.          
    */
    Public Countries_Service_Tasks__c createCountryServiceTaskAttributes() {
        CSt = new Countries_Service_Tasks__c(Service_Task__c = St.Id,PBB_Scenario_Country__c=psc.id);
        insert CSt;
        return CSt;
    } 
     
     /**
    *   This Method is to create bill rate card model data       
    */
     public Bill_Rate_Card_Model__c createBillModel(){
        billModel = new Bill_Rate_Card_Model__c (Name = 'BillRateModel1',Year__c='2004');
        System.debug('----billRateModel-----'+billModel);
        insert billModel;
        return billModel;
    }
    
    /**
    *   This Method is to create bill rate records       
    */
    public Bill_Rate__c createBillRate(){
        Bill_Rate_Card_Model__c billModel1 = createBillModel();
        billRate = new Bill_Rate__c (Model_Id__c = billModel1.Id, currency__c=createCurrency().Id, country__c=createCountryAttributes().Id, Job_Position__c=createJobClass().Id, Schedule_Year__c=billModel1.Year__c, Bill_Rate__c=100.00, isActive__c = true);
        System.debug('----BillRate----'+billRate);
        insert billRate;
        return billRate;
    }
    
    /**
    *   This Method is to create currency__c records       
    */
    public currency__c createCurrency(){
        currencyy = new currency__c(Name='currency1');
        insert currencyy;
        return currencyy;
    }
    
    /**
    *   This Method is to create WFM_Protocol__c records       
    */
    public WFM_Protocol__c  createProtocol(){
        protocol = new WFM_Protocol__c(Name='Protocol1',
                                       Project_ID__c=wfmp.id, 
                                       Protocal_Unique_Key__c='Protocol1:'+wfmp.Name, 
                                       Status__c = 'In Progress',
                                       Notification_Site_Count__c=0,
                                       Notification_Patient_Count__c=0,
                                       Expected_Patients__c = 100,
                                       Contracted_First_Subj_Enroll__c = SRM_Utils.getStartOfWeek(Date.today().addDays(14)),
            						   Contracted_Last_Subj_Enroll__c = SRM_Utils.getStartOfWeek(Date.today().addDays(70)).addDays(5),
        							   SRM_Final_Protocol_Date__c = SRM_Utils.getStartOfWeek(Date.today()),
                                       SRM_LPI_Date__c = SRM_Utils.getStartOfWeek(Date.today().addDays(70)));
        insert protocol;
        return protocol;
    
    }
    
    /**
    *   This Method is to create WFM_Site_Detail__c records       
    */
    public WFM_Site_Detail__c createSite(){
        site = new WFM_Site_Detail__c(Name='Site1',Project_Protocol__c=protocol.id,Country__c='USA',Site_ID__c='2',status__c='Activated',Actual_Subj_Enroll__c=11);
        insert site;
        return site;
    }
    
    /**
    *   This Method is to create protocol country records   
    */
    public Protocol_Country__c createProtocolCountry(){
        protocolCountry = new Protocol_Country__c(Name='ProtoCountry1',
                                                  Client_Protocol__c=protocol.id,
                                                  Region__c=country.id,
                                                  External_Id__c=protocol.Protocal_Unique_Key__c+':'+country.Name);
        insert protocolCountry ;
        return protocolCountry ;
    }
    
    /**
    *   This Method is to create country weekly events
    */
    public Country_Weekly_Event__c createCountryWeeklyEvents(){
        countryWeekly = new Country_Weekly_Event__c (Name='Week 1',External_Id__c='24',Protocol_Country__c=protocolCountry.id,week_date__c = Date.today());
        insert countryWeekly;
        return countryWeekly;
    }
    
    public List<Country_Weekly_Event__c> createPlannedWeeklyEvents(){
        Date startWeek = protocol.SRM_Final_Protocol_Date__c;
        Date lastweek = protocol.SRM_LPI_Date__c;
        Integer wknum =1;
        List<Country_Weekly_Event__c> weekeventlist = new List<Country_Weekly_Event__c>();
        for(Integer i =1; i <= 11; i++){
            Country_Weekly_Event__c weekEvent = new Country_Weekly_Event__c();
            weekEvent.Protocol_Country__c = protocolCountry.id;
            weekEvent.Name = 'Week '+(wknum);
            weekEvent.Planned_Active_Sites__c = wknum;
            weekEvent.Planned_Subjects__c = wknum*3;
            weekEvent.Week_Number__c = wknum;
            weekEvent.Week_Date__c = startWeek;
            weekEvent.External_Id__c = protocolCountry.External_Id__c+':'+SRM_Utils.getDateInStringFormat(startWeek);
            weekeventlist.add(weekevent);
            startWeek = startWeek.addDays(7);
            wknum++;
            system.debug(startWeek.addDays(7));
            System.debug(weekEvent);
        }
        upsert weekeventlist External_Id__c;
        return weekeventlist;
    }
    
    public List<Country_Weekly_Event__c> updateWithPrevWeeks(List<Country_Weekly_event__c> weekeventlist){
        Map<Integer,Country_Weekly_Event__c> cwmap = new Map<Integer,Country_Weekly_Event__c>();
        for(Country_Weekly_Event__c cw : weekeventlist){
            if(cwmap.containsKey(Integer.valueOf(cw.Week_Number__c)-1)){
                cw.Previous_Weekly_Event__c = cwmap.get(Integer.valueOf(cw.Week_Number__c)-1).Id;
            }
            cwmap.put(Integer.valueOf(cw.Week_Number__c),cw);
        }
        update cwmap.values();
        return cwmap.values();
    }
    
    public List<Country_Weekly_Event__c> updateWithActuals(List<Country_Weekly_event__c> weekeventlist){
        Integer i =1;
        for(Country_Weekly_Event__c cw : weekeventlist){
            if(i <=3){
                cw.Actual_Active_Sites__c = i;
                cw.Actual_Subjects__c =i*3;
            }
            else{
                cw.Expected_Active_Sites__c = i+1;
                cw.Medium_Active_Sites__c = i;
                cw.Conservative_Active_Sites__c = i-1;
            }
            i++;
            system.debug(cw);
        }
        upsert weekeventlist;
        return weekeventlist;
    }
    /**
    *   This Method is to create notification catalog data for protocol
    */
    public NotificationCatalog__c createnotifiCatalogProtocol(){
        notificationCatalogProto = new NotificationCatalog__c(NotificationName__c = 'NotifiName',Active__c =true, Type__c='Info',Category__c = 'Protocol',
                                                              Priority__c = 'Normal',ImplementationClassName__c = 'PBB_PatientEnrollmentNotification');
        insert notificationCatalogProto;
        return notificationCatalogProto;
    }
    
    /**
    *   This Method is to create notification catalog data for site
    */
    public NotificationCatalog__c createnotifiCatalogSite(){
        notificationCatalogSite = new NotificationCatalog__c(NotificationName__c = 'NotifiName1',Active__c =true, Type__c='Info',Category__c = 'Protocol',
                                                              Priority__c = 'Normal',ImplementationClassName__c = 'PBB_SiteActivationNotification');
        insert notificationCatalogSite;
        return notificationCatalogSite;
    }
    
    /**
    *   This Method is to create notification registration for data for createnotifiCatalogProtocol()
    */
    public NotificationRegistration__c createNotificationRegistration(){
        notifiRegistratn = new  NotificationRegistration__c(Name='Registrationn',userId__c=UserInfo.getUserId(),NotificationCatalog__c=notificationCatalogProto.id,RelatedToId__c=protocol.id,Active__c=true,NotifyByEmail__c=true);   
        insert notifiRegistratn ;
        return notifiRegistratn ;
    }
    
    /**
    *   This Method is to create notification registration for data for createnotifiCatalogSite()
    */
    public NotificationRegistration__c createNotificationRegistrationSite(){
        notifiRegistratnSite = new  NotificationRegistration__c(Name='Registrationn1',userId__c=UserInfo.getUserId(),NotificationCatalog__c=notificationCatalogSite.id,RelatedToId__c=protocol.id,Active__c=true,NotifyByEmail__c=true);   
        insert notifiRegistratnSite ;
        return notifiRegistratnSite ;
    }
    
    
  
}