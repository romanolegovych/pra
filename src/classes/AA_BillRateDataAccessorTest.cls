/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 
@isTest
private class AA_BillRateDataAccessorTest{   
    
    
    static testMethod void JobPositonsbyModelAndgetJobpositionsTest() {
        AA_TestUtils ut = new AA_TestUtils();
        currency__c c = ut.createCurrency();
        country__c co = ut.createCountry();
        Bill_Rate__c br = ut.createBillRate();
        List<AggregateResult> a = AA_BillRateDataAccessor.getJobPositonsbyModel(br.Model_id__c);
        AA_BillRateDataAccessor.getJobPositions();
        System.assertNotEquals(a ,null);
        
    }
    
    static testMethod void getModelByIdAndgetModelsTest() {
         AA_TestUtils ut = new AA_TestUtils();
         Bill_Rate_Card_Model__c  brcm = ut.createBillModel();
         list<Bill_Rate_Card_Model__c>  brcmm = AA_BillRateDataAccessor.getModelById(brcm.id);
         AA_BillRateDataAccessor.getModels();
         System.assertNotEquals(brcm ,null);
    }
    
    static testMethod void getModelsTest() {
        AA_TestUtils ut = new AA_TestUtils();
        currency__c c = ut.createCurrency();
        list<currency__c> cc = AA_BillRateDataAccessor.getCurrencies();
        System.assertNotEquals(cc,null);
    }
    
    static testMethod void getRateDetailsAndgetRateListByFilterTest() {
        AA_TestUtils ut = new AA_TestUtils();
        currency__c c = ut.createCurrency();
        country__c co = ut.createCountry();
        Bill_Rate__c br = ut.createBillRate();
        AA_BillRateDataAccessor.getRateDetails(String.valueOf(br.model_id__c),String.valueof(br.Job_Position__c),String.valueof(br.country__c),br.Schedule_Year__c,'true');
        AA_BillRateDataAccessor.getRateListByFilter(String.valueOf(br.model_id__c),String.valueof(br.Job_Position__c),String.valueof(br.country__c),br.Schedule_Year__c,'true');
    }
    
    static testMethod void getBillRateAndgetBillRateBasedOnModelIdTest() {
        AA_TestUtils ut = new AA_TestUtils();
        currency__c c = ut.createCurrency();
        country__c co = ut.createCountry();
        Bill_Rate__c br = ut.createBillRate();
        br = AA_BillRateDataAccessor.getBillRate(br.id);
        AA_BillRateDataAccessor.getBillRateBasedOnModelId(br.model_id__c);
        System.assertNotEquals(br,null);
    }
    
}