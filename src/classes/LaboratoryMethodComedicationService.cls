/** Implements the Service Layer of the object LaboratoryMethodComedication__c
 * @author	Dimitrios Sgourdos
 * @version	15-Oct-2013
 */	
public with sharing class LaboratoryMethodComedicationService {
	
	/** Create a LaboratoryMethodComedication__c instance with the given assessment and relate it to the given LaboratoryMethod__c record
	 * @author	Dimitrios Sgourdos
	 * @version 14-Oct-2013
	 * @param	labMethod			The LaboratoryMethod__c that the comedication will be connected
	 * @param	assessment			The assessment of the comedication
	 * @return	The LaboratoryMethodComedication__c instance
	 */
	public static LaboratoryMethodComedication__c createLaboratoryMethodComedicationInstance(LaboratoryMethod__c labMethod, String assessment) {
		return ( new LaboratoryMethodComedication__c(Assessment__c=assessment, LaboratoryMethod__c=labMethod.Id) );
	}
	
	
	/**	This class keeps a laboratory method comedication with the values for the associated method compounds.
	 * @author	Dimitrios Sgourdos
	 * @version	15-Oct-2013
	 */
	public class MethodComedicationWrapper {
		public LaboratoryMethodComedication__c				comedication 			{get;set;}
		public List<ComedicationCompoundAssignmentWrapper>	comedCompoundValuesList {get;set;}
		
		// The constructor of the sub-class
		public MethodComedicationWrapper() {
			comedCompoundValuesList = new List<ComedicationCompoundAssignmentWrapper>();
		}
	}
	
	
	/**	This class keeps the id of a laboratory method compound and the assigned value with a mathod comedication.
	 * @author	Dimitrios Sgourdos
	 * @version	15-Oct-2013
	 */
	public class ComedicationCompoundAssignmentWrapper {
		public String	compoundId 			{get;set;}
		public String	comedicationValue 	{get;set;}
	}
	
	
	/** Create a MethodComedicationWrapper instance by reordering the text values of th given comedication 
	 *	by the sequence of the compounds in the given list.
	 * @author	Dimitrios Sgourdos
	 * @version 15-Oct-2013
	 * @param	comedication		The laboratory method comedication that its text values will be reordered
	 * @param	compoundList		The list of the compounds of the laboratory method that the comedication is associated with
	 * @return	The MethodComedicationWrapper instance
	 */
	public static MethodComedicationWrapper createComedicationCompoundAssignmentWrapperInstance(LaboratoryMethodComedication__c comedication,
																								List<LaboratoryMethodCompound__c> compoundList) {
		// Initialize variables
		MethodComedicationWrapper newWrapperRecord = new MethodComedicationWrapper();
		newWrapperRecord.comedication = comedication;
		
		// Create a Map with the compound Id and assignment Value of the existing comedication compound associations 
		Map<String, String>	compoundIdValueMap = new Map<String, String>(); 
		for(Integer i=1; i<21; i++) { // Maximum 20 compounds are allowed
			String tmpStr 	  = BDT_Utils.lPad(String.valueOf(i), 2, '0');
			String tmpStrId   = 'Compound' + tmpStr + 'ID__c';
			String tmpStrText = 'Compound' + tmpStr +'Text__c'; 
			if(comedication.get(tmpStrId) != NULL) {
				compoundIdValueMap.put( (String) comedication.get(tmpStrId), (String) comedication.get(tmpStrText) );
			}
		}
		
		// Create comedCompoundValuesList
		for(LaboratoryMethodCompound__c tmpCompound : compoundList) {
			ComedicationCompoundAssignmentWrapper newItem = new ComedicationCompoundAssignmentWrapper();
			newItem.compoundId = tmpCompound.Id;
			newItem.comedicationValue = ( compoundIdValueMap.containsKey(tmpCompound.Id) )? compoundIdValueMap.get(tmpCompound.Id) : '';
			newWrapperRecord.comedCompoundValuesList.add(newItem);
		}
		
		return newWrapperRecord;
	}
	
	
	/** Create a MethodComedicationWrapper list from a comedication list by reordering the text values of the given comedications 
	 *	by the sequence of the compounds in the given list.
	 * @author	Dimitrios Sgourdos
	 * @version 15-Oct-2013
	 * @param	comedicationList	The laboratory method comedications that their text values will be reordered
	 * @param	compoundList		The list of the compounds of the laboratory method that the comedications are associated with
	 * @return	The MethodComedicationWrapper list
	 */
	public static List<MethodComedicationWrapper> createMethodComedicationWrapperList(List<LaboratoryMethodComedication__c> comedicationList, 
																					List<LaboratoryMethodCompound__c> compoundList) {
		List<MethodComedicationWrapper> wrapperList = new List<MethodComedicationWrapper>();
		
		for(LaboratoryMethodComedication__c tmpComed : comedicationList) {
			MethodComedicationWrapper wrapperItem = createComedicationCompoundAssignmentWrapperInstance(tmpComed, compoundList);
			wrapperList.add(wrapperItem);
		}
		
		return wrapperList;
	}
}