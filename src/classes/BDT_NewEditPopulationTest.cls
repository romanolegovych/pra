@isTest
private class BDT_NewEditPopulationTest {
	
	public static Client_Project__c firstProject {get;set;}
	public static List<Study__c> stydiesList {get;set;}
	public static List<Population__c> populationList {get;set;}
	public static List<Business_Unit__c> myBUList  {get;set;}
	public static List<Site__c> mySiteList  {get;set;}
	
	static void init(){
		firstProject = BDT_TestDataUtils.buildProject();
		insert firstProject;
		
		myBUList = BDT_TestDataUtils.buildBusinessUnit(5);
		insert myBUList;
		
		mySiteList = BDT_TestDataUtils.buildSite(myBUList);
		insert mySiteList;
		
		stydiesList = BDT_TestDataUtils.buildStudies(firstProject);
		stydiesList[0].Business_Unit__c = myBUList[0].Id; 
		stydiesList[1].Business_Unit__c = myBUList[1].Id; 
		stydiesList[2].Business_Unit__c = myBUList[2].Id;
		stydiesList[3].Business_Unit__c = myBUList[3].Id; 
		stydiesList[4].Business_Unit__c = myBUList[4].Id;
		insert stydiesList;
		
		populationList = BDT_TestDataUtils.buildPopulation(firstProject.id, 10);
		insert populationList;
		
		BDT_Utils.setPreference('SelectedProject', firstProject.id);
		BDT_Utils.setPreference('SelectedStudies',stydiesList[0].id);
		BDT_Utils.setPreference('populationId',populationList[0].id);
	}
	
	static testMethod void newPopulationPath() { 
		init();
				
		BDT_NewEditPopulation newEditPop = new BDT_NewEditPopulation();
		newEditPop.save();//save population without adding exclusion or inclusion criteria
		System.assertEquals(10, [select count() from Population__c where Client_Project__c = :firstProject.id]);
		System.assertEquals(0, [select count() from Criteria__c where Populations__c = :newEditPop.population.id]);		
		
	}
	
	static testMethod void newPopulationWithCriteriaPath() { 
		init();
		
		
		BDT_NewEditPopulation newEditPop = new BDT_NewEditPopulation();
		newEditPop.exclusiveCriteriaList[0].CriteriaDescription__c = 'I want to exclude odd number of subjects';
		newEditPop.save();//save population adding exclusion or inclusion criteria
		System.assertEquals(10, [select count() from Population__c where Client_Project__c = :firstProject.id]);
		System.assertEquals(1, [select count() from Criteria__c where Populations__c = :newEditPop.population.id]);		
		
	}
	
	static testMethod void newPopulationSoftDeletePopulationPath() { 
		init();
		ApexPages.currentPage().getParameters().put('PopulationId', populationList[0].id);		
		BDT_NewEditPopulation newEditPop = new BDT_NewEditPopulation();		
		newEditPop.deleteSoft();//save population adding exclusion or inclusion criteria
		System.assertEquals(true, [select BDTDeleted__c from Population__c where id = : populationList[0].id].BDTDeleted__c);
				
	}
	
	static testMethod void newPopulationSoftDeleteCriteriaPath() { 
		init();
		
		ApexPages.currentPage().getParameters().put('PopulationId', populationList[0].id);
		BDT_NewEditPopulation newEditPop = new BDT_NewEditPopulation();		
		newEditPop.exclusiveCriteriaList[0].CriteriaDescription__c = 'I want to exclude odd number of subjects';
		newEditPop.save();//save population adding exclusion or inclusion criteria
		System.assertEquals(1, [select count() from Criteria__c where Populations__c = :populationList[0].id]);	
		
		ApexPages.currentPage().getParameters().put('PopulationId', newEditPop.population.id);
		BDT_NewEditPopulation newEditPop2 = new BDT_NewEditPopulation();	
		system.debug('newEditPop2.exclusiveCriteriaList: '+ newEditPop2.exclusiveCriteriaList);	
		System.currentPageReference().getParameters().put('deleteCriteriaId', newEditPop2.exclusiveCriteriaList[0].id);
		newEditPop2.deleteCriteria();
		newEditPop2.save();//save population adding exclusion or inclusion criteria
		System.assertEquals(1, [select count() from Criteria__c where Populations__c = :populationList[0].id]);	
				
	}
	

}