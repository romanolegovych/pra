public with sharing class PBB_ServiceImpactQuestionController {
    /**
    * to display the list of WR models   
    */
    public List<Service_Impact_Question_Name__c> serviceImpactQList { get; set; }

    /** 
       @description Constructor
       @author Tkachenko Oleksiy
       @date 2015   
   */
    public PBB_ServiceImpactQuestionController() {
        serviceImpactQList = PBB_DataAccessor.getAllServiceImpactQuestionNames();
    }

    /** 
        @description Function to call New Page of required Model
        @author Tkachenko Oleksiy
        @date 2015  
   */
   public PageReference getNewPageReference() {
       Schema.DescribeSObjectResult R;
       R = Service_Impact_Question_Name__c.SObjectType.getDescribe();
       return new PageReference('/' + R.getKeyPrefix() + '/e');
   }
}