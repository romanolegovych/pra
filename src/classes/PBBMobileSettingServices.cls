public with sharing class PBBMobileSettingServices {
	public static List<PBBMobileSettingWrapper> getSettingList( Id userId ) {
		List<PBBMobileSettingWrapper> settingList = new List<PBBMobileSettingWrapper>();

		Map<Id,PBB_Mobile_Setting__c> settingMap = new Map<Id,PBB_Mobile_Setting__c>([SELECT Id, Name, Type__c, Parent_Setting__c,
																							(SELECT Id, Name 
																							 FROM PBB_Mobile_Setting_Picklist_Values__r
																							 ORDER BY Name), 
																							(SELECT Id, Value__c 
																							 FROM User_Values__r
																							 WHERE User__c = : userId
																							 LIMIT 1 ),
																							(SELECT Id//, Name, Parent_Setting__c, Type__c 
																							FROM Child_Settings__r )
																						FROM PBB_Mobile_Setting__c
																						ORDER BY Order__c]);

		for( PBB_Mobile_Setting__c settingItem : settingMap.values() ){
System.debug( LoggingLevel.ERROR,'@@@settingItem : ' + settingItem );
			if( settingItem.Parent_Setting__c == null ){
				PBBMobileSettingWrapper newPBBMobileSetting = new PBBMobileSettingWrapper();

				if( settingItem.User_Values__r != null && !settingItem.User_Values__r.isEmpty() ){
					newPBBMobileSetting.checkboxValue = Boolean.valueOf( settingItem.User_Values__r[0].Value__c );
					newPBBMobileSetting.settingValueId = settingItem.User_Values__r[0].Id;
System.debug( LoggingLevel.ERROR,'@@@newPBBMobileSetting.settingValueId : ' + newPBBMobileSetting.settingValueId );
				} else {
					newPBBMobileSetting.checkboxValue = false;
				}

				newPBBMobileSetting.text = settingItem.Name;
				newPBBMobileSetting.settingId = settingItem.Id;
				newPBBMobileSetting.type = settingItem.Type__c;

				if( settingItem.Child_Settings__r != null && !settingItem.Child_Settings__r.isEmpty() ){
					PBB_Mobile_Setting__c childSettingItem = settingMap.get( settingItem.Child_Settings__r[0].Id );
					
					newPBBMobileSetting.childSettingId = childSettingItem.Id;
					newPBBMobileSetting.pickListText = childSettingItem.Name;

					newPBBMobileSetting.picklistValues = new List<String>();
					newPBBMobileSetting.picklistValueSelectOption = new List<SelectOption>();

					for( PBB_Mobile_Setting_Picklist_Value__c picklistValueItem : childSettingItem.PBB_Mobile_Setting_Picklist_Values__r ){
						newPBBMobileSetting.picklistValues.add( picklistValueItem.Name );
						newPBBMobileSetting.picklistValueSelectOption.add( new selectOption( picklistValueItem.Name, picklistValueItem.Name ) );
					}

					if( settingItem.User_Values__r != null && !settingItem.User_Values__r.isEmpty() ){
						newPBBMobileSetting.pickListValue = settingItem.User_Values__r[0].Value__c;
						newPBBMobileSetting.childSettingValueId = settingItem.User_Values__r[0].Id;
					} else {
						newPBBMobileSetting.pickListValue = newPBBMobileSetting.picklistValues[0];
					}
				}
System.debug( LoggingLevel.ERROR,'@@@newPBBMobileSetting : ' + newPBBMobileSetting );
				settingList.add( newPBBMobileSetting );
			}
		}

		return settingList;
	}

	public static String saveSettings( List<PBBMobileSettingWrapper> settingList ){
		String errors;
		List<PBB_Mobile_Setting_Value__c> upsertList = new List<PBB_Mobile_Setting_Value__c>();

		for( PBBMobileSettingWrapper settingWrapperItem : settingList ){
System.debug( LoggingLevel.ERROR,'@@@settingWrapperItem : ' + settingWrapperItem );
			PBB_Mobile_Setting_Value__c settingValue;
			PBB_Mobile_Setting_Value__c settingChildValue;

			if( settingWrapperItem.settingValueId != null ){
				settingValue = new PBB_Mobile_Setting_Value__c( Id = settingWrapperItem.settingValueId );
			} else {
				settingValue = new PBB_Mobile_Setting_Value__c( User__c = UserInfo.getUserId(),
																			PBB_Mobile_Setting__c = settingWrapperItem.settingId );
			}

			
			settingValue.Value__c = String.valueOf( settingWrapperItem.checkboxValue );

System.debug( LoggingLevel.ERROR,'@@@settingValue : ' + settingValue );
			
			upsertList.add( settingValue );

			if( settingWrapperItem.childSettingId != null ){
				if( settingWrapperItem.childSettingValueId != null ){
					settingChildValue = new PBB_Mobile_Setting_Value__c( Id = settingWrapperItem.childSettingValueId );
				} else {
					settingChildValue = new PBB_Mobile_Setting_Value__c( User__c = UserInfo.getUserId(),
																		 PBB_Mobile_Setting__c = settingWrapperItem.childSettingId );
				}

				settingChildValue.Value__c = String.valueOf( settingWrapperItem.pickListValue );

				upsertList.add( settingChildValue );
System.debug( LoggingLevel.ERROR,'@@@settingChildValue : ' + settingChildValue );
			}
			
		}

		try{
			upsert upsertList;
System.debug( LoggingLevel.ERROR,'@@@upsertList : ' + upsertList );
		} catch ( DmlException dmlEx ){
			errors = String.valueOf( dmlEx );
		}

		return errors;
	}
}