/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

/**
*   'PRA_WBSServiceTest' class is to cover the test coverage for 'PRA_WBSService' class.
*   @author   Devaram Bhargav
*   @version  06-Nov-2013
*   @since    06-Nov-2013
*/
@isTest
private class PRA_WBSServiceTest {
    
    static testMethod void testConstructor() {
        // This is just for code coverage
        test.startTest();
        PRA_WBSService da = new PRA_WBSService();
        test.stopTest();
    }
    
     static testMethod void testgetMapfromAllProjectsbyClientTaskStatus() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        // Zero out all effort ratios
        List<Client_Task__c> ctList = [select id,project__r.name,Project__c,status__c
                                          from Client_Task__c
                                          where Project__r.id = :tu.clientProject.id
                                          ];
        ctList[0].status__c=PRA_Constants.CU_STATUS_NEW;
        ctList[1].status__c=PRA_Constants.CU_STATUS_READY;
        
        test.startTest();         
        Map<String,PRA_ProjectWBSCVO.ProjectWBS> projectWBSMap  = PRA_WBSService.getMapfromAllProjectsbyClientTaskStatus();
        ctList[1].status__c=PRA_Constants.CU_STATUS_SENT;
        update ctList;
        projectWBSMap  = PRA_WBSService.getMapfromAllProjectsbyClientTaskStatus();
        system.debug('============='+projectWBSMap);
        test.stopTest();  
        
        System.assertEquals(1, projectWBSMap.size());
    }
    
    static testMethod void testgetWBSProjectbyProjectName() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        
        delete tu.wbsProject;        
        test.startTest();         
        wbs_project__c  wbsp  = PRA_WBSService.getWBSProjectbyProjectName(tu.clientProject.name);
        
        system.debug('============='+wbsp);
        System.assertEquals(wbsp.client_project__c,tu.clientProject.id);
        System.assertNotEquals(null,wbsp);
        test.stopTest();
    }
    
    static testMethod void testgetClientTaskCountbyStatus() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        test.startTest();         
        List<Aggregateresult>  aggres  = PRA_WBSService.getClientTaskCountbyStatus(tu.clientProject.name);        
        system.debug('============='+aggres);        
        System.assertEquals(0,aggres.size());
        test.stopTest();
    }    
    
    static testMethod void testsaveClientTaskbyStatus() {
        // Init Test Utils
        PRA_TestUtils tu = new PRA_TestUtils();
        tu.initAll();
        test.startTest();         
        String ErrorMsg  = PRA_WBSService.saveClientTaskbyStatus(tu.clientProject.name,'',PRA_Constants.CU_STATUS_NEW);        
        system.debug('============='+ErrorMsg);        
        System.assertEquals(null,ErrorMsg);
        test.stopTest();
    }
    
    static testMethod void testSendEmailbyWBSProjectsList() {        
       
        test.startTest();   
        Set<String> UserIDList = new Set<String>();
        UserIDList.add(String.valueof(UserInfo.getUserId())); 
        Set<String> UserEmailList = new Set<String>();
        UserEmailList = PRA_WBSService.getUserEmailbyUser(UserIDList);
        Set<String> ExportProjectSet = new Set<String>();
        ExportProjectSet.add('ProjecttoExport');
        Set<String> ConfirmProjectSet = new Set<String>();
        ConfirmProjectSet.add('ProjecttoConfirm');
        PRA_WBSService.SendEmailbyWBSProjectsList(UserEmailList,ExportProjectSet,ConfirmProjectSet);
        test.stopTest();
    }
    
    static testMethod void testgetPZblock() {
        // Init Test Utils
       
        test.startTest();    
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        PMC_ProfileGroup__c Plist = new PMC_ProfileGroup__c();
        plist.Profile__c='Standard User';
        plist.Name='Standard User';
        insert plist;
        User testUser = new User(alias='TestFAUr',Email='testFAPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='FA', 
                            LocaleSidKey='en_US', ProfileId=p.id, TimeZoneSidKey='America/New_York', Username='Test.FA@praintl.com.unitTestPMConsole');
        insert testUser;
        boolean pz = PRA_WBSService.getPZblock(p.id);  
        System.assertEquals(true, PZ);
        test.stopTest();
    }
    
    static testMethod void testgetFAblock() {        
       
        test.startTest();    
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User'];
        PMC_ProfileGroup__c Plist = new PMC_ProfileGroup__c();
        plist.Profile__c='Standard User';
        plist.Name='Standard User';
        insert plist;
        User testUser = new User(alias='TestFAUr',Email='testFAPMConsole@praintl.com', EmailEncodingKey='ISO-8859-1', FirstName='Test', LanguageLocaleKey='en_US', LastName='FA', 
                            LocaleSidKey='en_US', ProfileId=p.id, TimeZoneSidKey='America/New_York', Username='Test.FA@praintl.com.unitTestPMConsole');
        insert testUser;
        boolean FA = PRA_WBSService.getFAblock(testUser.id);  
        System.assertEquals(false, FA);
        test.stopTest();
    }
    
     static testMethod void testgetUserEmailbyUser() {        
       
        test.startTest();   
        Set<String> UserIDList = new Set<String>();
        UserIDList.add(String.valueof(UserInfo.getUserId()));        
        Set<String> useremail= PRA_WBSService.getUserEmailbyUser(UserIDList);  
        System.assertNotEquals(useremail,null);
        test.stopTest();
    }
}