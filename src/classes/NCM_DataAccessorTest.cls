/**
 * @description	Implements the test for the functions of the NCM_DataAccessor class
 * @author		Dimitrios Sgourdos
 * @version		Created: 07-Sep-2015, Edited: 08-Oct-2015
 */
@isTest
private class NCM_DataAccessorTest {
	
// *********************************************************************************************************************
//				Notification Registration queries
// *********************************************************************************************************************

	/**
	 * @description	Test the function getNotificationRegistrationWithWhereClause.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	 */
	static testMethod void getNotificationRegistrationWithWhereClauseTest() {
		// Create data
		NotificationCatalog__c notCat = new NotificationCatalog__c(	NotificationName__c = 'Test Notification',
																	ImplementationClassName__c = 'Test Class Name');
		insert notCat;
		
		List<NotificationRegistration__c> source = new List<NotificationRegistration__c>();
		source.add( new NotificationRegistration__c(NotificationCatalog__c = notCat.Id, Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = notCat.Id, Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = notCat.Id, Active__c = false) );
		insert source;
		
		Set<Id> regIdsSet = new Set<Id>{source[0].Id, source[1].Id};
		
		String errorMessage = 'Error in retrieving registrations for disabling';
		
		// Initialize data and build the query
		List<Notificationregistration__c> results = NCM_DataAccessor.getNotificationRegistrationWithWhereClause(
																									'Active__c = true');
		
		system.assertEquals(2, results.size(), errorMessage);
		system.assertEquals(true, regIdsSet.contains(results[0].Id), errorMessage);
		system.assertEquals(true, regIdsSet.contains(results[1].Id), errorMessage);
		
		results = NCM_DataAccessor.getNotificationRegistrationWithWhereClause(
																		'Active__c = true AND RelatedToId__c = NULL');
		
		system.assertEquals(2, results.size(), errorMessage);
		system.assertEquals(true, regIdsSet.contains(results[0].Id), errorMessage);
		system.assertEquals(true, regIdsSet.contains(results[1].Id), errorMessage);
	}
	
	
	/**
	 * @description	Test the function getNotificationRegistrationForCheckingDuplicates.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	 */
	static testMethod void getNotificationRegistrationForCheckingDuplicatesTest() {
		// Create data
		List<NotificationCatalog__c> catalogList = new List<NotificationCatalog__c>();
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic A',
													ImplementationClassName__c = 'Class A'));
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic B',
													ImplementationClassName__c = 'Class B'));
		insert catalogList;
		
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		List<User> userList = NCM_Utils.buildTestUserList(2);
		
		List<NotificationRegistration__c> source = new List<NotificationRegistration__c>();
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = accList[1].Id ) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[1].Id ) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = accList[0].Id ) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = accList[1].Id ) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = NULL ) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = NULL ) );
		insert source;
		
		Set<Id> regIdsSet = new Set<Id>{source[0].Id, source[1].Id, source[2].Id, source[4].Id};
		
		String errorMessage = 'Error in retrieving registrations for checking duplicates';
		
		// Check the function
		Set<Id> userIdSet = new Set<Id>{userList[0].Id};
		Set<Id> notifCatalogIdsSet = new Set<Id>{catalogList[0].Id};
		Set<String> relatedToIdsSet = new Set<String>{accList[0].Id};
		
		List<NotificationRegistration__c> results = NCM_DataAccessor.getNotificationRegistrationForCheckingDuplicates(
																									userIdset,
																									notifCatalogIdsSet,
																									relatedToIdsSet);
		system.assertEquals(4, results.size(), errorMessage);
		system.assertEquals(true, regIdsSet.contains(results[0].Id), errorMessage);
		system.assertEquals(true, regIdsSet.contains(results[1].Id), errorMessage);
		system.assertEquals(true, regIdsSet.contains(results[2].Id), errorMessage);
		system.assertEquals(true, regIdsSet.contains(results[3].Id), errorMessage);
		
		// Check the function with NULL related Id included in the query
		relatedToIdsSet.add(NULL);
		regIdsSet.add(source[4].Id);
		regIdsSet.add(source[5].Id);
		
		results = NCM_DataAccessor.getNotificationRegistrationForCheckingDuplicates(userIdset,
																					notifCatalogIdsSet,
																					relatedToIdsSet);
		system.assertEquals(5, results.size(), errorMessage);
		system.assertEquals(true, regIdsSet.contains(results[0].Id), errorMessage);
		system.assertEquals(true, regIdsSet.contains(results[1].Id), errorMessage);
		system.assertEquals(true, regIdsSet.contains(results[2].Id), errorMessage);
		system.assertEquals(true, regIdsSet.contains(results[3].Id), errorMessage);
		system.assertEquals(true, regIdsSet.contains(results[4].Id), errorMessage);
	}
	
	
	/**
	 * @description	Test the function getListOfNotificationRegistration.
	 * @author		Jegadheesan Muthusamy
	 * @date		Created: 10-Sep-2015
	 */
	static testMethod void getListOfNotificationRegistrationTest() {
		// Create data
		NotificationCatalog__c catalog = new NotificationCatalog__c(NotificationName__c = 'Topic A',
													ImplementationClassName__c = 'Class A',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		insert catalog;
		
		Account acc = new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen');
		insert acc;
		
		List<User> userList = NCM_Utils.buildTestUserList(2);
		
		List<NotificationRegistration__c> source = new List<NotificationRegistration__c>();
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = acc.Id,
													Active__c = false) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = acc.Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = NULL,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = NULL,
													Active__c = false) );
		insert source;
		
		String errorMessage = 'Error in getting List Of Notification Registration';
		
		// Check the function with normal related to id
		List<NotificationRegistration__c> results = NCM_DataAccessor.getListOfNotificationRegistration(	catalog.Id,
																										acc.Id);
		
		system.assertEquals(1, results.size(), errorMessage);
		
		system.assertEquals(source[1].Id, results[0].Id, errorMessage);
		system.assertEquals(userList[1].Id, results[0].UserId__c, errorMessage);
		
		// Check the function with NULL related to id
		results = NCM_DataAccessor.getListOfNotificationRegistration(catalog.Id, NULL);
		
		system.assertEquals(1, results.size(), errorMessage);
		
		system.assertEquals(source[2].Id, results[0].Id, errorMessage);
		system.assertEquals(userList[0].Id, results[0].UserId__c, errorMessage);
	}
	
	
	/**
	 * @description	Test the function getNotificationRegistrationsforSchedule.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 25-Sep-2015
	 */
	static testMethod void getNotificationRegistrationsforScheduleTest() {
		NCM_DataAccessor.getNotificationRegistrationsforSchedule();
	}
	
	
// *********************************************************************************************************************
//				Notification Catalog queries
// *********************************************************************************************************************

	/**
	 * @description	Test the function getNotificationTopicsWithActiveSubscriptions version A
	 *				(with parameters Id, String, String).
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	 */
	static testMethod void getNotificationTopicsWithActiveSubscriptionsTest_version_A() {
		// Create data
		List<NotificationCatalog__c> catalogList = new List<NotificationCatalog__c>();
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic A',
													ImplementationClassName__c = 'Class A',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic B',
													ImplementationClassName__c = 'Class B',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic C',
													ImplementationClassName__c = 'Class C',
													Category__c = NCM_API_dataTypes.COUNTRY_NOTIFICATION));
		insert catalogList;
		
		List<User> userList = NCM_Utils.buildTestUserList(2);
		
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		List<NotificationRegistration__c> source = new List<NotificationRegistration__c>();
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = false) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[2].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[0].Id,
													Active__c = false) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[2].Id,
													UserId__c = userList[0].Id,
													Active__c = true) );
		insert source;
		
		String errorMessage = 'Error in retrieving notification topics with their active subscriptions';
		
		// Check the function with normal related to Id
		List<NotificationCatalog__c> results = NCM_DataAccessor.getNotificationTopicsWithActiveSubscriptions (
																				userList[0].Id,
																				accList[0].Id,
																				NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		system.assertEquals(2, results.size(), errorMessage);
		
		system.assertEquals(catalogList[0].Id, results[0].Id, errorMessage);
		system.assertEquals(1, results[0].NotificationRegistrations__r.size(), errorMessage);
		system.assertEquals(source[0].Id, results[0].NotificationRegistrations__r[0].Id, errorMessage);
		
		system.assertEquals(catalogList[1].Id, results[1].Id, errorMessage);
		system.assertEquals(0, results[1].NotificationRegistrations__r.size(), errorMessage);
		
		// Check the function with NULL related to Id
		results = NCM_DataAccessor.getNotificationTopicsWithActiveSubscriptions(userList[0].Id,
																				NULL,
																				NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		system.assertEquals(2, results.size(), errorMessage);
		
		system.assertEquals(catalogList[0].Id, results[0].Id, errorMessage);
		system.assertEquals(0, results[0].NotificationRegistrations__r.size(), errorMessage);
		
		system.assertEquals(catalogList[1].Id, results[1].Id, errorMessage);
		system.assertEquals(1, results[1].NotificationRegistrations__r.size(), errorMessage);
		system.assertEquals(source[6].Id, results[1].NotificationRegistrations__r[0].Id, errorMessage);
	}
	
	
	/**
	 * @description	Test the function getNotificationTopicsWithActiveSubscriptions version A
	 *				(with parameters Id, String).
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 04-Sep-2015
	 */
	static testMethod void getNotificationTopicsWithActiveSubscriptionsTest_version_B() {
		// Create data
		List<NotificationCatalog__c> catalogList = new List<NotificationCatalog__c>();
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic A',
													ImplementationClassName__c = 'Class A',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic B',
													ImplementationClassName__c = 'Class B',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic C',
													ImplementationClassName__c = 'Class C',
													Category__c = NCM_API_dataTypes.COUNTRY_NOTIFICATION));
		insert catalogList;
		
		List<User> userList = NCM_Utils.buildTestUserList(2);
		
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		List<NotificationRegistration__c> source = new List<NotificationRegistration__c>();
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = false) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[2].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[0].Id,
													Active__c = false) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[0].Id,
													Active__c = true) );
		source.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[2].Id,
													UserId__c = userList[0].Id,
													Active__c = true) );
		insert source;
		
		String errorMessage = 'Error in retrieving notification topics with their active subscriptions';
		
		// Check the function
		List<NotificationCatalog__c> results = NCM_DataAccessor.getNotificationTopicsWithActiveSubscriptions (
																				userList[0].Id,
																				NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		system.assertEquals(2, results.size(), errorMessage);
		
		system.assertEquals(catalogList[0].Id, results[0].Id, errorMessage);
		system.assertEquals(1, results[0].NotificationRegistrations__r.size(), errorMessage);
		system.assertEquals(source[0].Id, results[0].NotificationRegistrations__r[0].Id, errorMessage);
		
		system.assertEquals(catalogList[1].Id, results[1].Id, errorMessage);
		system.assertEquals(2, results[1].NotificationRegistrations__r.size(), errorMessage);
		Set<Id> tmpIdsSet = new Set<Id>{source[3].Id, source[6].Id};
		system.assertEquals(true, tmpIdsSet.contains(results[1].NotificationRegistrations__r[0].Id), errorMessage);
		system.assertEquals(true, tmpIdsSet.contains(results[1].NotificationRegistrations__r[1].Id), errorMessage);
	}
	
	
	/**
	 * @description	Test the function getNotificationTopicsForUpdating.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 */
	static testMethod void getNotificationTopicsForUpdatingTest() {
		// Create data
		List<NotificationCatalog__c> catalogList = new List<NotificationCatalog__c>();
		catalogList.add( new NotificationCatalog__c(NotificationName__c='Topic A', ImplementationClassName__c='Class A'));
		catalogList.add( new NotificationCatalog__c(NotificationName__c='Topic B', ImplementationClassName__c='Class B'));
		catalogList.add( new NotificationCatalog__c(NotificationName__c='Topic C', ImplementationClassName__c='Class C'));
		insert catalogList;
		
		String errorMessage = 'Error in retrieving notification topics for updating';
		
		// Check the function
		Set<String> source = new Set<String>{'Topic A', 'Topic B'};
		
		List<NotificationCatalog__c> results = NCM_DataAccessor.getNotificationTopicsForUpdating(source);
		
		system.assertEquals(2, results.size(), errorMessage);
		
		system.assertEquals(catalogList[0].Id, results[0].Id, errorMessage);
		system.assertEquals('Topic A', results[0].NotificationName__c, errorMessage);
		system.assertEquals('Class A', results[0].ImplementationClassName__c, errorMessage);
		
		system.assertEquals(catalogList[1].Id, results[1].Id, errorMessage);
		system.assertEquals('Topic B', results[1].NotificationName__c, errorMessage);
		system.assertEquals('Class B', results[1].ImplementationClassName__c, errorMessage);
	}
	
	
	/**
	 * @description	Test the function getNotificationTopicById.
	 * @author		Jegadheesan Muthusamy
	 * @date		Created: 10-Sep-2015
	 */
	static testMethod void getNotificationTopicByIdTest() {
		// Create data
		List<NotificationCatalog__c> catalogList = new List<NotificationCatalog__c>();
		catalogList.add( new NotificationCatalog__c(NotificationName__c='Topic A', ImplementationClassName__c='Class A'));
		catalogList.add( new NotificationCatalog__c(NotificationName__c='Topic B', ImplementationClassName__c='Class B'));
		insert catalogList;
		
		String errorMessage = 'Error in retrieving notification catalog record by id';
		
		NotificationCatalog__c result = NCM_DataAccessor.getNotificationTopicById(catalogList[0].Id);
		system.assertEquals('Topic A', result.NotificationName__c, errorMessage);
		system.assertEquals('Class A', result.ImplementationClassName__c, errorMessage);
		
		result = NCM_DataAccessor.getNotificationTopicById(catalogList[1].Id);
		system.assertEquals('Topic B', result.NotificationName__c, errorMessage);
		system.assertEquals('Class B', result.ImplementationClassName__c, errorMessage);
	}
	
	
	/**
	 * @description	Test the function getNotificationTopicByName.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 14-Sep-2015
	 */
	static testMethod void getNotificationTopicByNameTest() {
		// Create data
		List<NotificationCatalog__c> source = new List<NotificationCatalog__c>();
		source.add( new NotificationCatalog__c(NotificationName__c='Topic A', ImplementationClassName__c='Class A'));
		source.add( new NotificationCatalog__c(NotificationName__c='Topic B', ImplementationClassName__c='Class B'));
		insert source;
		
		String errorMessage = 'Error in retrieving notification catalog record by name';
		
		NotificationCatalog__c result = NCM_DataAccessor.getNotificationTopicByName('Topic A');
		system.assertEquals(source[0].Id, result.Id, errorMessage);
		system.assertEquals('Topic A', result.NotificationName__c, errorMessage);
		system.assertEquals('Class A', result.ImplementationClassName__c, errorMessage);
		
		result = NCM_DataAccessor.getNotificationTopicByName('Topic B');
		system.assertEquals(source[1].Id, result.Id, errorMessage);
		system.assertEquals('Topic B', result.NotificationName__c, errorMessage);
		system.assertEquals('Class B', result.ImplementationClassName__c, errorMessage);
	}
	
	
// *********************************************************************************************************************
//				Notification queries
// *********************************************************************************************************************
	
	/**
	 * @description	Test the function getNotificationAcknowledgingforSchedule.
	 * @author		Kalaiyarasan Karunanithi
	 * @date		Created: 11-Sep-2015
	 */
	static testMethod void getNotificationAcknowledgingforScheduleTest() {
		NCM_DataAccessor.getNotificationAcknowledgingforSchedule();
	}
	
	/**
	 * @description	Test the function getPendingNotificationsByUser.
	 * @author		Kalaiyarasan Karunanithi, Dimitrios Sgourdos
	 * @date		Created: 10-Sep-2015, Edited: 08-Oct-2015
	 */
	static testMethod void getPendingNotificationsByUserTest() {
		// Create data
		NotificationCatalog__c catalog = new NotificationCatalog__c(NotificationName__c = 'Topic A',
													ImplementationClassName__c = 'Class A',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION);
		insert catalog;
		
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'C', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		List<User> userList = NCM_Utils.buildTestUserList(2);
		
		List<NotificationRegistration__c> regList = new List<NotificationRegistration__c>();
		regList.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = false) );
		regList.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true) );
		regList.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[2].Id,
													Active__c = true) );
		regList.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = NULL,
													Active__c = true) );
		regList.add( new NotificationRegistration__c(NotificationCatalog__c = catalog.Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = accList[2].Id,
													Active__c = true) );
		insert regList;
		
		List<Notification__c> notificationList = new List<Notification__c>();
		notificationList.add(new Notification__c(NotificationRegistration__c = regList[0].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 1));
		notificationList.add(new Notification__c(NotificationRegistration__c = regList[1].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 2));
		notificationList.add(new Notification__c(NotificationRegistration__c = regList[2].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 3));
		notificationList.add(new Notification__c(NotificationRegistration__c = regList[3].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 4));
		notificationList.add(new Notification__c(NotificationRegistration__c = regList[4].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 5));
		insert notificationList;
		
		String errorMessage = 'Error in building Pending Acknowledgement Status';
		
		// Check the function
		List<Notification__c> results = NCM_DataAccessor.getPendingNotificationsByUser(userList[0].Id);
		
		system.assertEquals(3, results.size(), errorMessage);
		
		Set<Id> notIds = new Set<Id>{notificationList[1].Id, notificationList[2].Id, notificationList[3].Id};
		
		system.assertEquals(true, notIds.contains(results[0].Id), errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, results[0].Status__c, errorMessage);
		
		system.assertEquals(true, notIds.contains(results[1].Id), errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, results[1].Status__c, errorMessage);
		
		system.assertEquals(true, notIds.contains(results[2].Id), errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, results[2].Status__c, errorMessage);
	}
	
	
	/**
	 * @description	Test the function getPendingNotificationsByUserAndRelatedObject.
	 * @author		Kalaiyarasan Karunanithi, Dimitrios Sgourdos
	 * @date		Created: 11-Sep-2015, Edited: 08-Oct-2015
	 */
	static testMethod void getPendingNotificationsByUserAndRelatedObjectTest() {
		// Create data
		List<NotificationCatalog__c> catalogList = new List<NotificationCatalog__c>();
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic A',
													ImplementationClassName__c = 'Class A',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic B',
													ImplementationClassName__c = 'Class B',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		catalogList.add( new NotificationCatalog__c(NotificationName__c = 'Topic C',
													ImplementationClassName__c = 'Class C',
													Category__c = NCM_API_dataTypes.PROTOCOL_NOTIFICATION));
		insert catalogList;
		
		List<Account> accList = new List<Account>();
		accList.add( new Account(Name = 'A', BillingCountry = 'NL', BillingCity = 'Groningen') );
		accList.add( new Account(Name = 'B', BillingCountry = 'NL', BillingCity = 'Groningen') );
		insert accList;
		
		List<User> userList = NCM_Utils.buildTestUserList(2);
		
		List<NotificationRegistration__c> regList = new List<NotificationRegistration__c>();
		regList.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = false) );
		regList.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[1].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		regList.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[2].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[0].Id,
													Active__c = true) );
		regList.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[0].Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true) );
		regList.add( new NotificationRegistration__c(NotificationCatalog__c = catalogList[0].Id,
													UserId__c = userList[1].Id,
													RelatedToId__c = accList[1].Id,
													Active__c = true) );
		insert regList;
		
		List<Notification__c> notificationList = new List<Notification__c>();
		notificationList.add(new Notification__c(NotificationRegistration__c = regList[0].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 1));
		notificationList.add(new Notification__c(NotificationRegistration__c = regList[1].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 2));
		notificationList.add(new Notification__c(NotificationRegistration__c = regList[2].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 3));
		notificationList.add(new Notification__c(NotificationRegistration__c = regList[3].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 4));
		notificationList.add(new Notification__c(NotificationRegistration__c = regList[4].Id, 
													Status__c = NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT,
													Reminder__c = Date.today() + 5));
		insert notificationList;
		
		String errorMessage = 'Error in getting Pending Acknowledgement by User And RelatedObject';
		
		// Check the function with normal related to id
		List<Notification__c> results = NCM_DataAccessor.getPendingNotificationsByUserAndRelatedObject(	userList[0].Id,
																										accList[0].Id);
		
		system.assertEquals(2, results.size(), errorMessage);
		
		Set<Id> notIds = new Set<Id>{notificationList[1].Id, notificationList[2].Id};
		
		system.assertEquals(true, notIds.contains(results[0].Id), errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, results[0].Status__c, errorMessage);
		
		system.assertEquals(true, notIds.contains(results[1].Id), errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, results[1].Status__c, errorMessage);
		
		// Check the function with NULL related to id
		regList[0].RelatedToId__c = NULL;
		regList[1].RelatedToId__c = NULL;
		regList[2].RelatedToId__c = NULL;
		update regList;
		
		results = NCM_DataAccessor.getPendingNotificationsByUserAndRelatedObject(userList[0].Id, NULL);
		
		system.assertEquals(2, results.size(), errorMessage);
		
		system.assertEquals(true, notIds.contains(results[0].Id), errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, results[0].Status__c, errorMessage);
		
		system.assertEquals(true, notIds.contains(results[1].Id), errorMessage);
		system.assertEquals(NCM_API_DataTypes.PENDING_ACKNOWLEDGEMENT, results[1].Status__c, errorMessage);
	}
	
	
	/**
	 * @description	Test the function getNotificationsById.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 30-Sep-2015
	 */
	static testMethod void getNotificationsByIdTest() {
		// Create data
		List<Notification__c> source = new List<Notification__c>();
		source.add(new Notification__c(Reminder__c = Date.today() + 1));
		source.add(new Notification__c(Reminder__c = Date.today() + 2));
		source.add(new Notification__c(Reminder__c = Date.today() + 3));
		source.add(new Notification__c(Reminder__c = Date.today() + 4));
		insert source;
		
		String errorMessage = 'Error in getting notifications by ids';
		
		// Check the function
		Set<Id> notificationIds = new Set<Id>{source[0].Id, source[2].Id};
		
		List<Notification__c> results = NCM_DataAccessor.getNotificationsById(new Set<Id>());
		system.assertEquals(0, results.size(), errorMessage);
		
		results = NCM_DataAccessor.getNotificationsById( notificationIds );
		system.assertEquals(2, results.size(), errorMessage);
		
		system.assertEquals(source[0].Id, results[0].Id, errorMessage);
		system.assertEquals(source[2].Id, results[1].Id, errorMessage);
	}
	
	
// *********************************************************************************************************************
//				Notification Event queries
// *********************************************************************************************************************
	
	/**
	 * @description	Test the function getNotificationEventUnderBatchProcedure.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 25-Sep-2015
	 */
	static testMethod void getNotificationEventUnderBatchProcedureTest() {
		// Create data
		List<NotificationEvent__c> source = new List<NotificationEvent__c>();
		source.add( new NotificationEvent__c(Name = 'Event A') );
		source.add( new NotificationEvent__c(Name = NCM_API_DataTypes.EVENT_UNDER_BATCH_PROCEDURE) );
		source.add( new NotificationEvent__c(Name = 'Event B') );
		source.add( new NotificationEvent__c(Name = NCM_API_DataTypes.EVENT_UNDER_BATCH_PROCEDURE) );
		insert source;
		
		Set<Id> eventIds = new Set<Id>{source[1].Id, source[3].Id};
		
		String errorMessage = 'Error in retrieving the notification events that are under a batch procedure';
		
		// Check the function
		List<NotificationEvent__c> results = NCM_DataAccessor.getNotificationEventUnderBatchProcedure();
		
		system.assertEquals(2, results.size(), errorMessage);
		system.assertEquals(true, eventIds.contains(results[0].Id), errorMessage);
		system.assertEquals(true, eventIds.contains(results[1].Id), errorMessage);
	}
}