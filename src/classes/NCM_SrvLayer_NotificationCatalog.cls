/**
 * @description	The Service Layer for the Notification Catalog
 * @author		Dimitrios Sgourdos
 * @date		Created: 07-Sep-2015, Edited: 08-Oct-2015
 */
public with sharing class NCM_SrvLayer_NotificationCatalog {
	
	/**
	 * @description	Retrieve a notification catalog instance with attributes filled from the corresponding wrapper
	 *				data type. The record that will be refreshed with new values is passed as parameter in order to
	 *				capture situations that we want to refresh an existing (already stored in the DB) record.
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015, Edited: 08-Oct-2015
	 * @param		source    The wrapper that holds the values for the attributes of the Notification Catalog Record
	 * @param		result    The Notification Catalog record to be updated with the attributes from the wrapper item
	 * @return		The updated Notification Catalog record.
	*/
	public static NotificationCatalog__c getNotificationCatalogFromWrapperData(
																	NCM_API_DataTypes.NotificationCatalogWrapper source,
																	NotificationCatalog__c result) {
		// Check for invalid parameters
		if(source == NULL) {
			return result;
		}
		
		if(result == NULL) {
			result = new NotificationCatalog__c();
		}
		
		
		// Create the registration catalog instance
		result.NotificationName__c			= source.notificationName;
		result.Notificationdescription__c	= source.notificationDescription;
		
		result.Category__c					= (source.notificationCategory == NULL)?
																	NCM_API_DataTypes.PROTOCOL_NOTIFICATION :
																	source.notificationCategory;
		
		result.Type__c						= (source.notificationType == NULL)?
																	NCM_API_DataTypes.ALERT_TYPE :
																	source.notificationType;
		
		result.Comments__c					= source.comments;
		result.DefaultReminderPeriod__c		= source.defaultReminderPeriod;
		result.Active__c					= (source.active == NULL)? true : source.active;
		
		result.Priority__c					= (source.priority == NULL)?
																	NCM_API_DataTypes.NORMAL_PRIORITY :
																	source.priority;
		
		result.AcknowledgementRequired__c	= (source.acknowledgementRequired == NULL)?
																	true :
																	source.acknowledgementRequired;
		
		result.ChatterEnabled__c			= (source.chatterEnabled == NULL)?		true : source.chatterEnabled;
		result.SMSallowed__c				= (source.SMSallowed == NULL)?			true : source.SMSallowed;
		result.TriggeredByBatchJob__c		= (source.TriggeredByBatchJob == NULL)? true : source.TriggeredByBatchJob;
		result.ImplementationClassName__c	= source.implementationClassName;
		
		return result;
	}
	
	
	/**
	 * @description	Insert the given Notification Catalog wrapper instances in the Notification Catalog object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 * @param		source    The wrapper data that holds the new Notification Catalog records
	*/
	public static void insertNotificationCatalogEntries(List<NCM_API_DataTypes.NotificationCatalogWrapper> source) {
		// Check for invalid parameters
		if(source == NULL || source.isEmpty() ) {
			return;
		}
		
		// Update the registration record
		List<NotificationCatalog__c> results = new List<NotificationCatalog__c>();
		for(NCM_API_DataTypes.NotificationCatalogWrapper tmpWrapperItem : source) {
			results.add( getNotificationCatalogFromWrapperData(tmpWrapperItem, new NotificationCatalog__c()) );
		}
		
		insert results;
	}
	
	
	/**
	 * @description	Update the given Notification Catalog wrapper instances in the Notification Catalog object
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 * @param		source    The wrapper that corresponds to the Notification Catalog records to be updated
	*/
	public static void updateNotificationCatalogEntries(List<NCM_API_DataTypes.NotificationCatalogWrapper> source) {
		// Check for invalid parameters
		if(source == NULL || source.isEmpty() ) {
			return;
		}
		
		// Retrieve the existing notification catalogs that corresponds to the given data and map them on their
		// notification name
		Set<String> topicNameSet = new Set<String>();
		
		for(NCM_API_DataTypes.NotificationCatalogWrapper tmpWrapperItem : source) {
			topicNameSet.add(tmpWrapperItem.notificationName);
		}
		
		List<NotificationCatalog__c> existingRecords = NCM_DataAccessor.getNotificationTopicsForUpdating(topicNameSet);
		
		Map<String, NotificationCatalog__c> catalogOnNameMap = new Map<String, NotificationCatalog__c>();
		for(NotificationCatalog__c tmpInstance : existingRecords) {
			catalogOnNameMap.put(tmpInstance.NotificationName__c, tmpInstance);
		}
		
		// Build the records that will be updated
		List<NotificationCatalog__c> results = new List<NotificationCatalog__c>();
		
		for(NCM_API_DataTypes.NotificationCatalogWrapper tmpWrapperItem : source) {
			if( ! catalogOnNameMap.containsKey(tmpWrapperItem.notificationName) ) {
				continue;
			}
			
			NotificationCatalog__c tmpInstance = catalogOnNameMap.remove(tmpWrapperItem.notificationName);
			tmpInstance = getNotificationCatalogFromWrapperData(tmpWrapperItem, tmpInstance);
			results.add(tmpInstance);
		}
		
		update results;
	}
	
	
	/**
	 * @description	Disable the notification catalog entries that corresponds to the given topic names
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 * @param		topicNameSet    The names of the notification catalog entries that we want to disable
	*/
	public static void disableNotificationCatalogEntriesByName(Set<String> topicNameSet) {
		// Check for invalid parameters
		if(topicNameSet == NULL || topicNameSet.isEmpty() ) {
			return;
		}
		
		// Retrieve and disable the notification catalog entries
		List<NotificationCatalog__c> results = NCM_DataAccessor.getNotificationTopicsForUpdating(topicNameSet);
		
		for(NotificationCatalog__c tmpItem : results) {
			tmpItem.Active__c = false;
		}
		
		update results;
	}
	
	
	/**
	 * @description	Disable the notification catalog entries that corresponds to the given notification catalog wrapper
	 *				data
	 * @author		Dimitrios Sgourdos
	 * @date		Created: 07-Sep-2015
	 * @param		source    The wrapper data that corrresponds to the Notification Catalog records to be disabled
	*/
	public static void disableNotificationCatalogEntries(List<NCM_API_DataTypes.NotificationCatalogWrapper> source) {
		// Check for invalid parameters
		if(source == NULL || source.isEmpty() ) {
			return;
		}
		
		// Disable the given entries
		Set<String> topicNameSet = new Set<String>();
		
		for(NCM_API_DataTypes.NotificationCatalogWrapper tmpWrapperItem : source) {
			topicNameSet.add(tmpWrapperItem.notificationName);
		}
		
		disableNotificationCatalogEntriesByName(topicNameSet);
	}
}