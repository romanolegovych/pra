/*
* A controller for VF page which will allow to edit Aggregated Milestone
*/
public with sharing class EWAggregatedMilestoneController
{
	public EW_Aggregated_Milestone__c currentMilestone {get; set;}
	public List<SObjectWrapper> junctionWrappers {get; set;}
	
	public ApexPages.StandardController controller;
	
	public EWAggregatedMilestoneController(ApexPages.StandardController controller)
	{
		this.controller = controller;
		initializeMilestone();
		initializeWrappers();
	}
	
	public List<EW_Aggregated_Milestone_EW_Junction__c> junctions {get; set;}
	
	private List<EW_Aggregated_Milestone_EW_Junction__c> selectJunctions()
	{
		return [select Aggregated_Milestone__c, Early_Warning__c, Early_Warning__r.Flow__c, Early_Warning__r.Name from EW_Aggregated_Milestone_EW_Junction__c where Aggregated_Milestone__c = :currentMilestone.Id];
	}

	public void onPAWSProjectFlowChange()
	{
		initializeWrappers();
	}
	
	private List<Critical_Chain__c> selectEWs()
	{
		return [select Name, Flow__r.Name, Flow__c from Critical_Chain__c where PAWS_Project_Flow__c = :currentMilestone.PAWS_Project_Flow__c order by Name];
	}
	
	private void initializeMilestone()
	{
		currentMilestone = new EW_Aggregated_Milestone__c();
		for (EW_Aggregated_Milestone__c eachMilestone : [select Name, Goal__c, PAWS_Project_Flow__c, Milestone_Target_Date__c from EW_Aggregated_Milestone__c where Id = :ApexPages.currentPage().getParameters().get('Id')])
		{
			currentMilestone = eachMilestone;
		}
	}
	
	public PageReference save()
	{
		PageReference landingPage;
		try
		{
			upsert currentMilestone;
			saveWrappers();
			
			landingPage = new PageReference('/' + currentMilestone.Id);
		}
		catch (Exception e)
		{
			ApexPages.addMessages(e);
		}
		
		return landingPage;
	}
	
	@testvisible private void saveWrappers()
	{
		List<EW_Aggregated_Milestone_EW_Junction__c> selectedJunctions = new List<EW_Aggregated_Milestone_EW_Junction__c>();
		for (SObjectWrapper junctionWrapper : junctionWrappers)
		{
			if (junctionWrapper.selected)
			{
				selectedJunctions.add((EW_Aggregated_Milestone_EW_Junction__c) junctionWrapper.wrappee);
			}
		}
		
		delete [select Name from EW_Aggregated_Milestone_EW_Junction__c where Aggregated_Milestone__c = :currentMilestone.Id and Id not in :selectedJunctions];
		
		for (EW_Aggregated_Milestone_EW_Junction__c eachJunction : selectedJunctions)
		{
			if (eachJunction.Aggregated_Milestone__c == null)
			{
				eachJunction.Aggregated_Milestone__c = currentMilestone.Id;
			}
		}
		
		upsert selectedJunctions;
	}
	
	private void initializeWrappers()
	{
		junctionWrappers = new List<SObjectWrapper>();
		Map<ID, List<EW_Aggregated_Milestone_EW_Junction__c>> existingJunctionsByEWID = new Map<ID, List<EW_Aggregated_Milestone_EW_Junction__c>>();
		for (EW_Aggregated_Milestone_EW_Junction__c eachJunction : selectJunctions())
		{
			if (existingJunctionsByEWID.get(eachJunction.Early_Warning__c) == null)
			{
				existingJunctionsByEWID.put(eachJunction.Early_Warning__c, new List<EW_Aggregated_Milestone_EW_Junction__c>());
			}
			
			existingJunctionsByEWID.get(eachJunction.Early_Warning__c).add(eachJunction);
		}
		
		for (Critical_Chain__c eachEW : selectEWs())
		{
			Boolean skip = false;
			for (Integer i = 0; existingJunctionsByEWID.get(eachEW.Id) != null && i < existingJunctionsByEWID.get(eachEW.Id).size(); i++)
			{
				SObjectWrapper junctionWrapper = new SObjectWrapper(existingJunctionsByEWID.get(eachEW.Id).get(i));
				junctionWrapper.selected = true;
				
				junctionWrappers.add(junctionWrapper);
				skip = true;
			}
			
			if (skip)
			{
				continue;
			}
			
			junctionWrappers.add(new SObjectWrapper(new EW_Aggregated_Milestone_EW_Junction__c(Early_Warning__c = eachEW.Id, Early_Warning__r = eachEW)));
		}
	}
	
	public class SObjectWrapper
	{
		public SObject wrappee {get; set;}
		public Boolean selected {get; set;}
		
		public SObjectWrapper(SObject wrappee)
		{
			this.wrappee = wrappee;
		}
	}
}