/** 
   @description Test Class For PBB Data Accessor
   @author Ramya
   @date 12/1/2014   
   */
   
/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/


@isTest
private class PBB_DataAccessorTest{

   /**
   * @author Niharika Reddy
   * @Desc Test method for getScenarioByID method from data accessor class
   */
   static testMethod void testGetScenarioByID(){
        PBB_TestUtils tu = new PBB_TestUtils ();
        //data creation
        WFM_Project__c wf = tu.createWfmProject();
        Bid_Project__c bd = tu.createBidProject();
        SRM_Model__c sv = tu.createSRMModelAttributes();
        //SRM_Scenario__c sr = tu.createSRMScenario();
        PBB_Scenario__c pb = tu.createScenarioAttributes();
        Country__c c = tu.createCountryAttributes();
        PBB_Scenario_Country__c pc = tu.createPBBScenarioCountry();
        SRM_Scenario_Country__c sc = tu.createSrmCountryAttributes();
        pbb_weekly_events__c pw = tu.createPRAWeeklyEvents();
        Service_Model__c sm = tu.CreateServiceModelAttributes();
        Service_Area__c sa = tu.createServiceAreaAttributes();
        sm.status__c = 'In-Progress';
        update sm;
        Service_Function__c sf = tu.createServiceFunctionAttributes();
        Service_Task__c st = tu.createServiceTaskAttributes();
        sm.status__c = 'Approved';
        update sm;
        Countries_Service_Tasks__c cst = tu.createCountryServiceTaskAttributes();
        update sm;
        WR_Model__c wr = tu.createWRModelAttributes();
        STSWR1__Flow__c sd = tu.createFlowAttributes();
        STSWR1__Flow_Step_Junction__c ss = tu.createFlowStepAttributes();
        Service_Task_To_Flow_Step__c stfs = tu.createTaskToFlow();
        Job_Class_Desc__c bc = tu.createJobClass();
        Mapping_Region_Model__c  mpr = tu.createMappingRegion();
        Formula_Name__c  bfn = tu.createBillFormulaNames();
        Bill_Rate_Card_Model__c brcm = tu.createBillModel();
        Service_Impact_Question_Name__c  siqn = tu.createSIQNAttributes();
        Service_Impact_Questions__c siq = tu.createSIQSAttributes(siqn.id);
        Service_Impact_Response__c sir = tu.createSIRAttributes();
        Service_Task_Responses__c str = new Service_Task_Responses__c ( Countries_Service_Tasks__c=cst.id,Service_Impact_Questions__c=siq.id,Service_Impact_Response__c=sir.id);
        Therapeutic_Area__c ther = tu.createTherapeutic();
        Indication_Group__c indi = tu.createIndicationGroup();
        Primary_Indication__c pr = tu.createPrimaryIndication();
        PRA_Business_Unit__c prab = tu.createPRABusinessUnits();
        Vendor_Category__c  vc = tu.createVendorCategory();
        Project_Vendors__c pv = tu.createProjectVendors();
        //Service_Task_To_Service_Impact__c stsi = tu.createSTtoSI(st.id,siqn.id);
        SRM_Model_Subject_Enrollment__c smdl = new SRM_Model_Subject_Enrollment__c(Name='subj1',SRM_Model__c=sv.id);
        //list<PBB_Weekly_events__c> pbbw = tu.createPBBWeeklyEvents(sc.id,4);
        //SRM_Calender_Adjustments__c cal = new SRM_Calender_Adjustments__c(SRM_Model__c=sv.id);
        SRM_Model_Questions__c smq = new SRM_Model_Questions__c(SRM_Model__c=sm.id);
        SRM_Questions_Response__c squeres = new SRM_Questions_Response__c(name='qres1',PBB_Scenario__c=pb.id,SRM_Question_ID__c=smq.id);
        Service_Task_Responses__c servTskFormula = new Service_Task_Responses__c(Countries_Service_Tasks__c=cst.id);
        
        
        //Method calls
        set<String> countr = new set<String>{'USA','Australia'};
        Set<ID> wrId = new Set<ID>();
        wrId.add(wr.id);  
        Set<String> tskID = new Set<String>();
        tskID.add(st.id);
        Set<Id> sfId = new Set<Id>();
        sfId.add(sf.id);
        Set<Id> saID = new Set<Id>();
        saID.add(sa.id);
        Set<String> siqnId = new Set<String>();
        siqnId.add(siqn.id);
        Set<String> saID1 = new Set<String>();
        saID1.add(sa.id);
            
        
        PBB_Scenario__c pbbojb = PBB_DataAccessor.getScenarioByID(pb.id);
        System.assertEquals(pbbojb.id,pb.id);
        
        /*SRM_Scenario__c srmobj = PBB_DataAccessor.getSRMScenarioById(sr.id);
        System.assertEquals(srmobj.id,sr.id);*/
        
        list<PBB_Scenario__c> pbbobj1 = PBB_DataAccessor.getPbbScenarioOrderByDesc();
        System.assertNotEquals(pbbobj1.size(),0);
        
        list<PBB_Scenario__c> pbbobj2 = PBB_DataAccessor.getPbbScenarioOrderByAsc();
        System.assertNotEquals(pbbobj2.size(),0);
        
        /*list<SRM_Scenario__c> srmobj1 = PBB_DataAccessor.getSRMScenarioListByBidProjectId(bd.id);
        System.assertEquals(srmobj1.size(),0);*/
        
        Bid_Project__c bd1 = PBB_DataAccessor.getBidProjectByID(bd.id);
        System.assertEquals(bd1.id,bd.id);
        
        list<PBB_Scenario_Country__c> pc1 = PBB_DataAccessor.GetSelectedCountries(pb.id);
        System.assertNotEquals(pc1.size(),null);
        
        list<PBB_Scenario_Country__c> pc2 = PBB_DataAccessor.GetSelectedCountriesbyasc(pb.id);
        System.assertNotEquals(pc1,null);
        
        List<SRM_Scenario_Country__c> sc1 = PBB_DataAccessor.GetPBBWeeklyCountriesbyasc(sc.PBB_Scenario_Country__r.PBB_Scenario__c);
        System.assertEquals(sc1.size(),0);
        
        PBB_Scenario__c pbbojb2 = PBB_DataAccessor.getSRMScenarioById1(pb.id);
        System.assertEquals(pbbojb2.id,pb.id);
        
        list<PBB_Weekly_Events__c> pw1 = PBB_DataAccessor.Getweeklyevents(sc.id);
        System.assertNotEquals(pw1.size(),0);
        
        List<PBB_Scenario_Country__c> pc3 = PBB_DataAccessor.GetSelectedProjectCountries(pb.id);
        System.assertEquals(pc3.size(),0);
        
        List<PBB_Scenario_Country__c> pc4 = PBB_DataAccessor.GetPBBCountriesandCST(pb.id);
        System.assertNotEquals(pc4.size(),0);
        
        List<Countries_Service_Tasks__c> cst1 = PBB_DataAccessor.GetSelectedServiceTask(pc.id,c.id);
        System.assertEquals(cst1.size(),0);
        
        List<Countries_Service_Tasks__c> cst2 = PBB_DataAccessor.GetSelectedServiceTaskbyCountries(pc.PBB_Scenario__c,countr);
        System.assertEquals(cst2.size(),0);
        
        List<Countries_Service_Tasks__c> cst3 = PBB_DataAccessor.GetCountryServiceTasksByScenario(pc.PBB_Scenario__c);
        System.assertNotEquals(cst3.size(),0);
        
        List<Service_Task_To_Flow_Step__c> sft = PBB_DataAccessor.GetAssociatedServiceTasksToStep(wr.id,wrId);
        System.assertEquals(sft.size(),0);
        
        List<Job_Class_Desc__c> jc = PBB_DataAccessor.GetJobPositionList();
        System.assertEquals(jc.size(),0);
        
        List<AggregateResult> cst4 = PBB_DataAccessor.groupbyServiceTask(cst.PBB_Scenario_Country__r.PBB_Scenario__c);
        System.assertEquals(cst4.size(),0);
        
        List<PBB_Scenario_Country__c> psc5 = PBB_DataAccessor.GetSelectedCountriesforPBB(pb.id,countr);
        System.assertEquals(psc5.size(),0);
        
        list<Service_task__c> st1 = PBB_DataAccessor.getServiceTaskList(tskID);
        System.assertNotEquals(st1.size(),0);
        
        list<Service_task__c> st2 = PBB_DataAccessor.getServiceTaskListbySFList(sfId);
        System.assertNotEquals(st2.size(),0);
        
        list<Service_Function__c> sf1 = PBB_DataAccessor.getServicefunctionListbySAList(saID);
        System.assertNotEquals(sf1.size(),0);
        
        list<Country__c> c2 = PBB_DataAccessor.getCountriesListbyNot(countr);
        System.assertNotEquals(c2.size(),0);
        
        list<Country__c> c3 = PBB_DataAccessor.getAllCountriesList();
        System.assertNotEquals(c3.size(),0);
        
        List<Mapping_Region_Model__c> mpr1 = PBB_DataAccessor.GetAllRegionModels();
        System.assertNotEquals(mpr1.size(),0);
        
        List<SRM_Model__c > srm1 = PBB_DataAccessor.GetAllSrmModels();
        System.assertNotEquals(srm1.size(),0);
        
        List<Service_Model__c> sm4 = PBB_DataAccessor.GetAllServicesModels();
        System.assertNotEquals(sm4.size(),0);
        
        List<WR_Model__c> wr4 = PBB_DataAccessor.GetAllWrModels();
        System.assertNotEquals(wr4.size(),0);
        
        List<Service_Impact_Question_Name__c> siqn1 = PBB_DataAccessor.getAllServiceImpactQuestions();
        System.assertNotEquals(siqn1.size(),0);
        
        List<Formula_Name__c> bfn1 = PBB_DataAccessor.getAllBillFormulas();
        System.assertNotEquals(bfn1.size(),0);
        
        List<Bill_Rate_Card_Model__c> brcm1 = PBB_DataAccessor.getAllBillRateCardModels();
        System.assertNotEquals(brcm1.size(),0);
        
        PBB_Scenario__c pbbobj4 = PBB_DataAccessor.getPbbScenariobyid(pb.id);
        //System.assetEquals(pbbobj4.id,pb.id);
        
        /*List<PBB_Scenario__c> pbbobj5 = PBB_DataAccessor.getPbbScenariobySRMScenarioID(sr.id);
        System.assertEquals(pbbobj5.size(),0);*/
        
        //List<Service_Impact_Questions__c> siq1 = PBB_DataAccessor.getqtnlistSA (siq.id);
        
        List<AggregateResult> aggr12 = PBB_DataAccessor.getImpactqtnsversion(sa.id,pb.id);
        System.assertEquals(aggr12.size(),0);
        
        List<AggregateResult> agge21 = PBB_DataAccessor.getImpactqtnbyScenario(siqnId,pb.id);
        System.assertEquals(agge21.size(),0);
        
        List<AggregateResult> agg8 = PBB_DataAccessor.getResponsebyScenarioforSingleresponse(pb.id);
        System.assertEquals(agg8.size(),0);
        
        list<Service_Function__c> sf4 = PBB_DataAccessor.getServiceFnctnByArea(saID1,true,'product');
        System.assertNotEquals(sf4.size(),0);
        
        list<Service_Task__c> st4 = PBB_DataAccessor.getServiceTaskbyID(tskID);
        System.assertNotEquals(st4.size(),0);
        
        Service_Area__c  sa7 = PBB_DataAccessor.getServiceAreaByID(sa.id);
        System.assertEquals(sa7.id,sa.id);
        
        Service_Task__c st5 = PBB_DataAccessor.getServiceTaskbyIDForBS(st.id);
        System.assertEquals(st5.id,st.id);
        
        Service_Model__c sm7 = PBB_DataAccessor.getServiceModel(sm.id);
        System.assertEquals(sm7.id,sm.id);
        
        Service_Impact_Response__c sir8 = PBB_DataAccessor.getSIResponse(sir.id);
        System.assertEquals(sir8.id,sir.id);
        
        List<Service_Impact_Response__c> sir9 = PBB_DataAccessor.getSIAllResponse(siq.id);
        System.assertNotEquals(sir9.size(),0);
        
        list<Service_Area__c> sa0 = PBB_DataAccessor.getServiceAreaByModel(sm.id);
        System.assertNotEquals(sa0.size(),0);
        
        list<Service_Area__c> sa6 = PBB_DataAccessor.getServiceAreaByids(saID1);
        System.assertNotEquals(sa6.size(),0);
        
        list<Therapeutic_Area__c> ther1 = PBB_DataAccessor.gettherapeuticvalues();
        System.assertNotEquals(ther1.size(),0);
        
        list<Indication_Group__c> indi1 = PBB_DataAccessor.getPrimaryIndforpicklist(ther.id);
        System.assertNotEquals(indi1.size(),0);
        
        list<Primary_Indication__c> pi1 = PBB_DataAccessor.getIndicationGRPforpicklist(indi.id);
        System.assertNotEquals(pi1.size(),0);
        
        list<WFM_Phase__c> wf1 = PBB_DataAccessor.getPhaseValues();
        System.assertEquals(wf1.size(),0);
        
        list<PRA_Business_Unit__c> prab1 = PBB_DataAccessor.getPRABusinessUnit();
        System.assertNotEquals(prab1.size(),0);
        
        list<PBB_Scenario_Country__c> psc7 = PBB_DataAccessor.getservicetaskforcntry(pc.PBB_Scenario__r.Bid_Project__c);
        System.assertEquals(psc7.size(),0);
        
        List<Project_Vendors__c> pv1 = PBB_DataAccessor.getProjectVendors(vc.id,bd.id);
        System.assertEquals(pv1.size(),1);
        
        List<Project_Vendors__c> pv2 = PBB_DataAccessor.getProjectVendorsForVendors(vc.id,bd.id);
        System.assertEquals(pv2.size(),1);
        
        list<Service_Impact_Questions__c> siq5 = PBB_DataAccessor.getServiceImpactQuestionsbyID(siq.id);
        System.assertNotEquals(siq5.size(),0);
        
        List<AggregateResult> ag4 = PBB_DataAccessor.getAggregateSA(cst.PBB_Scenario_Country__r.PBB_Scenario__c);
        System.assertEquals(ag4.size(),0);
        
        List<Service_Task__c> st7 = PBB_DataAccessor.getServiceTaskSA(sa.id,pb.id);
        System.assertNotEquals(st7.size(),0);
        
        //Set<String> stsiID = new Set<String>();
        //stsiID.add(stsi.id);
        //List<Service_Task_To_Service_Impact__c> stsi5 = PBB_DataAccessor.getServiceTaskToServiceImpact(stsiID);
        List<AggregateResult> ag5 = PBB_DataAccessor.getServiceImpactqtns(tskID);
        System.assertEquals(ag5.size(),0);
        
        Set<String> siqIDs = new Set<String>();
        siqIDs.add(siq.id);
        List<Service_Impact_Questions__c> siq4 = PBB_DataAccessor.getnewqtnsapproved(siqIDs);
        System.assertEquals(siq4.size(),0);
        
        List<Service_Impact_Questions__c> siq3 = PBB_DataAccessor.getqtnsbyqnsName(siq.id);
        System.assertEquals(siq3.size(),0);
        
        Set<String> cstIDs = new Set<String>();
        cstIDs.add(cst.id);
        List<Countries_Service_Tasks__c> cst7 = PBB_DataAccessor.getresponseCST(cstIDs,pb.id);
        System.assertNOtEquals(cst7.size(),0);
        
        list<Bill_Rate_Card_Model__c> brcm4 = PBB_DataAccessor.getAllBillModels();
        System.assertNotEquals(brcm4.size(),0);
        
        list<Formula_Name__c> bf4 = PBB_DataAccessor.getAllBillFormulaNames();
        System.assertNotEquals(bf4.size(),0);
        
        list<Service_Impact_Question_Name__c> siqn9 = PBB_DataAccessor.getAllServiceImpactQuestionNames();
        System.assertNotEquals(siqn9.size(),0);
        
        list<Service_Impact_Response__c> sir7 = PBB_DataAccessor.getAllServiceImpactresp(siq.id);
        System.assertNotEquals(sir7 .size(),0);
        
        list<Service_Impact_Questions__c> siq6 = PBB_DataAccessor.getServiceImpactQuestionsbyIDs(siqIDs,siqIDs);
        System.assertNotEquals(siq6 .size(),0);
        
        list<SRM_Model_Subject_Enrollment__c> mdlsub1 = PBB_DataAccessor.getSrmModelSubjectEnrollmetbymodelId(sv.id);
        System.assertEquals(mdlsub1 .size(),0);
        
        //List<PBB_Weekly_events__c> pbbw1 = PBB_DataAccessor.getPBBWeeklyEventsByScenario(pb.id);
        List<PBB_Scenario__c> pff = PBB_DataAccessor.getSRMScenarios(pb.id);
        System.assertNotEquals(pff .size(),0);
        
        pbbobj4 = PBB_DataAccessor.getscenario(pb.id);
        System.assertEquals(pbbobj4.id,pb.id);
        
        
        //pbbobj4  = PBB_DataAccessor.getapprovescenario(pb.id);
        //list<String> objtyp = PBB_DataAccessor.getAllFieldsFromObject(PBB_Scenario__c);
        list<Id> smqIds = new list<Id>();
        smqIds.add(smq.id);
        List<SRM_Questions_Response__c> sqr4 = PBB_DataAccessor.getModelQuestionsbyres(smqIds,String.valueof(pb.id));
        System.assertEquals(sqr4 .size(),0);
        
        sqr4  = PBB_DataAccessor.getexporttbyGroup(pb.id,'Contract');
        System.assertEquals(sqr4  .size(),0);
        
        sqr4 = PBB_DataAccessor.getexporttbycountry(pb.id);
        System.assertEquals(sqr4 .size(),0);
               
        sqr4 = PBB_DataAccessor.getQuestions(pb.id);
        System.assertEquals(sqr4 .size(),0);
        
        sqr4 = PBB_DataAccessor.getQuesResponse('USA',pb.id);
        System.assertEquals(sqr4 .size(),0);
        
        sqr4 = PBB_DataAccessor.getModelQuestionsbyres(smqIds,pb.id);
        System.assertEquals(sqr4 .size(),0);
        
        /*List<Bid_Project__c> bb = PBB_DataAccessor.getroleofproject(sr.id);
        System.assertEquals(bb.size(),0);*/
        
        list<PBB_Scenario_Country__c> ppbc = PBB_DataAccessor.getpbbScenarioCountryByScenarioID(pb.id);
        System.assertEquals(ppbc.size(),1);
        
        list<SRM_Scenario_Country__c> srmcountryy = PBB_DataAccessor.getSrmCountriesgvnPBBScenarioid(pb.id);
        System.assertEquals(srmcountryy.size(),1);
        
        list<Service_Task_Drivers__c> servTskFor = PBB_DataAccessor.getServicetskFormulaById(pb.id);
        System.assertEquals(servTskFor.size(),0);
        
        list<Service_Task_Responses__c> serTskRes = PBB_DataAccessor.getservTskResponseByPbbId(pb.id);
        System.assertEquals(serTskRes.size(),0);
        
        list<AggregateResult> w = PBB_DataAccessor.getGrpdPBBweeklybyScenario(pw.id);
        System.assertEquals(w.size(),0);
        
        list<PBB_Weekly_Events__c> w1 = PBB_DataAccessor.getPBBWeeklyEventsByScenario(pb);
        System.assertNotEquals(w1.size(),0);
        
        list<PBB_Weekly_Events__c> w2 = PBB_DataAccessor.getPBBWeeklyEventsByCountry(sc);
        System.assertNotEquals(w2.size(),0);
     }    

}