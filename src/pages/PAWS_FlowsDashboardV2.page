<apex:page controller="PAWS_FlowsDashboardV2Controller" readOnly="true">
    
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.12/angular.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/angularjs/1.3.12/angular-resource.min.js"></script>
    
    <script>
        Object.size = function (obj) {
        var size = 0, key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    };
    function fixColspans() {
        $('table.gamesBoard tr:first th').each(function (idx, element) {
            var el = $(element);
            var className = el.attr('class');
            
            if (typeof className != 'undefined') {
                className = className.replace('ng-hide', '').replace('class', 'vertical').replace(' ', '');
                var totalElements = $('[class~="headerSteps"] ' + generateSelector([className])).size();
                var hiddenElements = $('[class~="headerSteps"] ' + generateSelector([className, 'ng-hide'])).size();
                var totalVisible = totalElements - hiddenElements;
                if (totalVisible == 0) {
                    el.addClass('ng-hide');
                } else {
                    el.removeClass('ng-hide');
                    el.attr('colspan', totalVisible);
                }
            }
        })
    }
    
    function generateSelector(classes, prefix) {
        var selector = '';
        var pref = typeof prefix == 'undefined' ? '' : prefix;
        for (var idx = 0; idx < classes.length; idx++) {
            selector += pref + '[class~="' + classes[idx] + '"]';
        }
        return selector;
    }
    
    $(document).ready(function(){
        $('.headerSteps').on('click',function(){
            setTimeout(fixColspans, 1);
        })
    })
    
    var BoardApp = angular.module('BoardApp', [], function () {});
    
    BoardApp.directive('rotateText', ['$compile', '$sce', function ($compile, $sce) {
        return {
            restrict: 'A',
            template: '<div class="rotateContainer">' +
            '<div class="rotated" title="{{title}}"><p ng-hide="hStep.first" ng-bind-html="stepName"></p>'+
            '<a href="" ng-show="hStep.first" ng-bind-html="stepName" ng-click="hStep.group.toggleChildren()"></a></div></div>',
            link: function (scope, iElement, iAttrs) {
                
                scope.stepName = $sce.trustAsHtml(scope.hStep.name);
                scope.title = scope.hStep.name.replace('&#x2022','>');
                var match = /(&#x2022)+/.exec(scope.hStep.name);
                var stepNameLength = scope.hStep.name.length;
                if(match && match.length > 0){
                    stepNameLength -= match.length * 7 - match.length;
                }
                
                var maxHeight = stepNameLength * 7;
                if(typeof scope.HeaderSteps.maxHeight == 'undefined'){
                    scope.HeaderSteps.maxHeight = maxHeight;
                } else if(scope.HeaderSteps.maxHeight <= maxHeight){
                    scope.HeaderSteps.maxHeight = maxHeight;
                    document.getElementById('dynamicStyle').innerHTML = '.rotateContainer {height: ' + maxHeight + 'px;} .rotateContainer .rotated {width: ' + maxHeight +'px;} ';
                }
                
                if (scope.$last) {
                    setTimeout(fixColspans, 1);
                }
            }
        }
    }]);
    
    BoardApp.directive('step', [ '$compile', function ($compile) {
        return {
            restrict: 'C',
            compile: function ($templateElement, $templateAttributes, transcludeFn) {
                return function LinkingFunction($scope, $element, $attrs) {
                    var html = '<td ng-show="step.group.visible">{{step.Name}} ^</td><td class="flowStep" x-ng-repeat="flow in step.SubFlows"></td>';
                    var e = $compile(html)($scope);
                    $element.replaceWith(e);
                };
            }
        };
    }]);
    
    BoardApp.directive('flowStep', [ '$compile', function ($compile) {
        return {
            restrict: 'C',
            compile: function ($templateElement, $templateAttributes, transcludeFn) {
                return function LinkingFunction($scope, $element, $attrs) {
                    var html = '<td class="step" x-ng-repeat="step in flow.Steps">{{step.Name}}</td>';
                    var e = $compile(html)($scope);
                    $element.replaceWith(e);
                };
            }
        };        
    }]);
    
    BoardApp.controller('BoardController', ['$scope', function ($scope) {
        $scope.flows = [];
        $scope.groups = [];
        $scope.steps = {};
        $scope.flowObjects = {};
        $scope.flowObjectArray = [];
        $scope.flowArray = [];
        $scope.HeaderSteps = [];
        $scope.HeaderStepsMap = {};
        $scope.MilestonesMap = {};
        $scope.MilestonesArray = [];
        
        $scope.init = function () {
            var data = JSON.parse('{!BoardJson}');
            if(data && data.flows == null){
                return;
            }
            
            flowObjectsToMap(data.objects);
            angular.forEach(data.flows, function (currentFlow) {
                buildFlowArray(currentFlow);
            });
            
            headerStepsMapToArray($scope.HeaderStepsMap);
            $scope.HeaderSteps = headerStepsSortByMilestones($scope.MilestonesArray, $scope.HeaderSteps);
            
            angular.forEach($scope.flowObjectArray, function (flowObject) {
                angular.forEach($scope.HeaderSteps, function (headStep) {
                    if (typeof flowObject.StepsWraperMap == 'undefined') {
                        flowObject.StepsWraperMap = {};
                    }
                    
                    var step = flowObject.StepsWraperMap[headStep.key];
                    if (typeof step == 'undefined') {
                        flowObject.StepsWraperMap[headStep.key] = {
                            group: headStep.group,
                            Status: 'notExists',
                            StepKey: headStep.key
                        }
                    } else {
                        flowObject.StepsWraperMap[headStep.key].group = headStep.group;
                    }
                });
            });
        };
        
        function buildFlowArray(currentFlow, rootFlowObject, parentStep) {
            
            if (typeof currentFlow.FlowObject == 'undefined' || currentFlow.FlowObject == null) {
                console.log('currentFlow not assigned to any object', currentFlow);
                return;
            }
            
            var flowObject = $scope.flowObjects[currentFlow.FlowObject.Id];
            currentFlow.flowObject = flowObject;
            $scope.flowArray.push(currentFlow);
            
            if (typeof flowObject.group == 'undefined') {
                flowObject.group = new Group($scope.groups.length + 1, flowObject.parent == null);
                $scope.groups.push(flowObject.group);
            }
            
            if (typeof parentStep != 'undefined' && parentStep.flowObject.group.idx != flowObject.group.idx) {
                parentStep.flowObject.group.children.push(flowObject.group);
            }
            
            angular.forEach(currentFlow.Steps, function (step) {
                
                if (typeof flowObject != 'undefined') {
                    flowObject.steps.push(step);
                    if (typeof flowObject.StepsWraperMap == 'undefined') {
                        flowObject.StepsWraperMap = {};
                        flowObject.StepsWraperMap[step.StepKey] = step;
                    } else {
                        flowObject.StepsWraperMap[step.StepKey] = step;
                    }
                    flowObject.StepsWraperMap[step.StepKey].group = flowObject.group;
                }
                
                // NOTE: add all steps form subFlows to master flow
                if (typeof rootFlowObject != 'undefined') {
                    rootFlowObject.steps.push(step);
                    if (typeof rootFlowObject.StepsWraperMap == 'undefined') {
                        rootFlowObject.StepsWraperMap = {};
                        rootFlowObject.StepsWraperMap[step.StepKey] = step;
                    } else {
                        var rootStep = rootFlowObject.StepsWraperMap[step.StepKey];
                        if (typeof rootStep == 'undefined') {
                            rootFlowObject.StepsWraperMap[step.StepKey] = step;
                        } else {
                            // NOTE: rollup is here
                            if (rootStep.StatusId > step.StatusId) {
                                rootStep.Status = step.Status;
                            }
                        }
                    }
                }
                
                if (typeof parentStep != 'undefined') {
                    step.Name = parentStep.Name + ' &#x2022 ' + step.Name;
                }
                
                var headStep = $scope.HeaderStepsMap[step.StepKey];
                if ((typeof headStep == 'undefined' || headStep == null) && flowObject.parent == null && typeof parentStep == 'undefined') {
                    $scope.HeaderStepsMap[step.StepKey] = headStep = { name: step.Name, key: step.StepKey, milestoneKey: typeof parentStep == 'undefined' ? step.MilestoneKey : parentStep.MilestoneKey, milestoneName: typeof parentStep == 'undefined' ? step.MilestoneName : parentStep.MilestoneName, childrenMap: {}, children: []};
                    
                } else if (parentStep && parentStep.header) {
                    headStep = parentStep.header.childrenMap[step.StepKey];
                    if (typeof headStep == 'undefined') {
                        parentStep.header.childrenMap[step.StepKey] = headStep = { name: step.Name, key: step.StepKey, milestoneKey: typeof parentStep == 'undefined' ? step.MilestoneKey : parentStep.MilestoneKey, milestoneName: typeof parentStep == 'undefined' ? step.MilestoneName : parentStep.MilestoneName, childrenMap: {}, children: []};
                        parentStep.header.children.push(headStep);
                    }
                }
                
                
                var milestone = $scope.MilestonesMap[step.MilestoneKey];
                if (typeof milestone == 'undefined' && typeof parentStep == 'undefined') {
                    $scope.MilestonesMap[step.MilestoneKey] = milestone = { key: step.MilestoneKey, name: step.MilestoneName };
                    $scope.MilestonesArray.push(milestone);
                }
                
                if (typeof(step.SubFlows) != 'undefined' && step.SubFlows && step.SubFlows.length > 0) {
                    step.header = headStep;
                    step.flowObject = flowObject;
                    angular.forEach(step.SubFlows, function (subFlow) {
                        buildFlowArray(subFlow, typeof rootFlowObject == 'undefined' ? flowObject : rootFlowObject, step);
                    });
                }
            })
        }
        
        function flowObjectsToMap(objects) {
            for (var index in objects) {
                if (objects.hasOwnProperty(index)) {
                    var project = objects[index];
                    $scope.flowObjects[index] = project;
                    $scope.flowObjects[index].parent = null;
                    $scope.flowObjects[index].indent = 0;
                    $scope.flowObjects[index].steps = [];
                    $scope.flowObjectArray.push(project);
                    
                    for (var cIdx = 0; cIdx < project.Countries.length; cIdx++) {
                        var country = project.Countries[cIdx];
                        $scope.flowObjects[country.FlowObjectID] = country;
                        $scope.flowObjects[country.FlowObjectID].parent = project;
                        $scope.flowObjects[country.FlowObjectID].indent = 1;
                        $scope.flowObjects[country.FlowObjectID].steps = [];
                        $scope.flowObjectArray.push(country);
                        
                        for (var siteIdx = 0; siteIdx < country.Sites.length; siteIdx++) {
                            var site = country.Sites[siteIdx];
                            $scope.flowObjects[site.FlowObjectID] = site;
                            $scope.flowObjects[site.FlowObjectID].parent = country;
                            $scope.flowObjects[site.FlowObjectID].indent = 2;
                            $scope.flowObjects[site.FlowObjectID].steps = [];
                            $scope.flowObjectArray.push(site);
                            
                            for (var agreemIdx = 0; agreemIdx < site.Agreements.length; agreemIdx++) {
                                var agreement = site.Agreements[agreemIdx];
                                $scope.flowObjects[agreement.FlowObjectID] = agreement;
                                $scope.flowObjects[agreement.FlowObjectID].parent = site;
                                $scope.flowObjects[agreement.FlowObjectID].indent = 3;
                                $scope.flowObjects[agreement.FlowObjectID].steps = [];
                                $scope.flowObjectArray.push(agreement);
                                
                            }
                            for (var docIdx = 0; docIdx < site.Documents.length; docIdx++) {
                                var document = site.Documents[docIdx];
                                $scope.flowObjects[document.FlowObjectID] = document;
                                $scope.flowObjects[document.FlowObjectID].parent = site;
                                $scope.flowObjects[document.FlowObjectID].indent = 3;
                                $scope.flowObjects[document.FlowObjectID].steps = [];
                                $scope.flowObjectArray.push(document);
                                
                            }
                            for (var subIdx = 0; subIdx < site.Submissions.length; subIdx++) {
                                var submission = site.Submissions[subIdx];
                                $scope.flowObjects[submission.FlowObjectID] = submission;
                                $scope.flowObjects[submission.FlowObjectID].parent = site;
                                $scope.flowObjects[submission.FlowObjectID].indent = 3;
                                $scope.flowObjects[submission.FlowObjectID].steps = [];
                                $scope.flowObjectArray.push(submission);
                                
                            }
                        }
                    }
                }
            }
        }
        
        function headerStepsMapToArray(headerStepMap, parentGroup) {
            var headGroup = new Group($scope.groups.length + 1, typeof parentGroup == 'undefined');
            $scope.groups.push(headGroup);
            
            if (typeof parentGroup != 'undefined') {
                parentGroup.children.push(headGroup);
                
            }
            
            angular.forEach(headerStepMap, function (headStep) {
                headStep.group = headGroup;
                $scope.HeaderSteps.push(headStep);
                
                if (headStep.children.length > 0) {
                    headStep.first = true;
                    headStep.group = new Group($scope.groups.length + 1, typeof parentGroup == 'undefined');
                    $scope.groups.push(headStep.group);
                    headGroup.children.push(headStep.group);
                    
                    if (typeof parentGroup != 'undefined') {
                        parentGroup.children.push(headStep.group);
                    }
                    headerStepsMapToArray(headStep.childrenMap, headStep.group);
                }
            })
        }
        
        function headerStepsSortByMilestones(milstones, headerSteps) {
            var result = [];
            angular.forEach(milstones, function (milstone) {
                angular.forEach(headerSteps, function (hStep) {
                    if(milstone.key == hStep.milestoneKey){
                        result.push(hStep);
                    }
                })
            });
            return result;
        }
        
    }]);
    
    var Group = function (idx, visibility) {
        this.idx = idx;
        this.visible = visibility;
        this.parent = null;
        this.colapsed = true;
        this.indent = 0;
        this.children = [];
    };
    
    Group.prototype.toggleChildren = function (colapsed) {
        var self = this;
        this.colapsed = typeof colapsed == 'undefined' ? !this.colapsed : colapsed;
        angular.forEach(this.children, function (child) {
            child.visible = !self.colapsed;
            if (self.colapsed == true) {
                child.toggleChildren(true);
            }
        })
    };
    
    </script>
    
    <style>  
        table div.rotated {
        -webkit-transform: rotate(270deg);
        -moz-transform: rotate(270deg);
        writing-mode: tb-rl;
        white-space: nowrap;
        }
        
        thead th {
        vertical-align: top;
        }
        
        table .vertical {
        white-space: nowrap;
        font-weight: normal !important;
        }
        
        .gamesBoard {
        border: 1px solid #ccc;
        }
        
        table.gamesBoard th, table.gamesBoard td {
        border: 1px solid #eee !important;
        padding: 4px;
        }
        
        tr.center td, tr.center th {
        text-align: center !important;
        }
        
        table.gamesBoard tr {
        border: 1px solid #CCC !important;
        }
        
        table.gamesBoard tr td {
        text-align: left;
        }
        
        table.gamesBoard tr:nth-child(1n+2):hover {
        background-color: #e3f3ff;
        }
        
        tr.bottom th {
        vertical-align: bottom !important;
        }
        
        .completed {
        background-color: #92D050;
        }
        
        .late {
        background-color: #E93F33;
        }
        
        .inProgress {
        background-color: #FDF731;
        }
        
        .pending {
        background-color: #93CDDD;
        }
        
        .notStarted {
        background-color: #7A7A79;
        }
        
        .notExists {
        }
        
        .headTh1, .cellWrapper {
        min-width: 200px;
        width: 200px;
        }
        
        .headTh4 {
        min-width: 120px;
        width: 120px;
        }
        
        .cellWrapper {
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        }
        
        body table.gamesBoard tr td,
        body table.gamesBoard tr th {
        border-right: 0px none transparent !important;
        border-bottom: 0px none transparent !important;
        }
        
        body table.gamesBoard tr td.outlinetop {
        border-top-color: #555 !important;
        }
        
        body table.gamesBoard tr td.outlinebottom {
        border-bottom-color: #555 !important;
        }
        
        body table.gamesBoard tr td.outlineleft {
        border-left-color: #555 !important;
        }
        
        body table.gamesBoard tr td.outlineright {
        border-right-color: #555 !important;
        }
        
        body table.gamesBoard tr td:first-child {
        white-space: nowrap;
        }
        
        .rotateContainer {
        width: 1.1em;
        overflow: hidden;
        max-height: 200px;
        }
        
        .rotateContainer .rotated {
        height: 100%;
        cursor: default;
        max-width: 200px;
        }
        
        .rotateContainer .rotated p, .rotateContainer .rotated a {
        width: 100%;
        text-overflow: ellipsis;
        white-space: nowrap;
        overflow: hidden;
        display: block;
        margin: 0;
        }
        
        .gamesBoard {
        font-family: sans-serif;
        font-size: 14px;
        }
        
        .gamesBoard a {
        color: #015ba7;
        text-decoration: none;
        }
        
        .gamesBoard a:hover {
        text-decoration: underline;
        }
        
        .firstCol {
        cursor: default;
        }
        
        .headerSteps th, .milestones th {
        text-align: center;
        }
    </style>
    
    <style id="dynamicStyle"></style>
    
    <div ng-app="BoardApp">
        <!--
        <apex:form id="dashboardForm">
            <apex:actionStatus id="applyFilter" onstop="$('.gamesBoard').rotateTableCellContent();">
                <apex:facet name="start">
                    <STSWR1:ScreenLocker id="screenLocker"/>
                </apex:facet>
            </apex:actionStatus>
            
            <apex:pageBlock title="Settings" mode="edit">
                <apex:pageBlockButtons location="top">
                    <apex:commandButton id="applyButton" value="Apply" action="{!applyAction}"/>
                </apex:pageBlockButtons>
                
                <apex:pageBlockSection columns="1">
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Flow:"/>
                        <apex:selectList value="{!SelectedFlow}" size="1">
                            <apex:selectOptions value="{!FlowOptionList}" />
                        </apex:selectList>
                    </apex:pageBlockSectionItem>
                    
                </apex:pageBlockSection>
            </apex:pageBlock>
            
        </apex:form>
        -->
        <div x-ng-controller='BoardController' x-ng-init='init()' ng-cloak="true">
            
            <table class="gamesBoard" cellspacing="0" cellpadding="0" ng-show="flowArray.length > 0">
                <tr class="milestones">
                    <th></th>
                    <th x-ng-repeat="milestone in MilestonesArray" colspan="{{milestone.headers.length}}"
                        class="{{milestone.key}}">{{milestone.name}}
                    </th>
                </tr>
                <tr class="headerSteps">
                    <th>
                        Object Name
                    </th>
                    <td x-ng-repeat="hStep in HeaderSteps" rotate-text="{{hStep.name}}" class="vertical {{hStep.milestoneKey}}"
                        ng-show="hStep.group.visible">
                    </td>
                </tr>
                <tr x-ng-repeat="flowObject in flowObjectArray" ng-show="flowObject.group.visible">
                    <td class="firstCol">
                        <span style="margin-left: {{flowObject.indent * 30}}px" ng-hide="flowObject.group.children.length > 0">{{flowObject.FlowObjectName}}</span>
                        <a href="" ng-show="flowObject.group.children.length > 0" ng-click="flowObject.group.toggleChildren()"
                           style="margin-left: {{flowObject.indent * 30}}px">{{flowObject.FlowObjectName}}</a>
                    </td>
                    <td x-ng-repeat="hStep in HeaderSteps" class="{{flowObject.StepsWraperMap[hStep.key].Status}}"
                        ng-show="flowObject.StepsWraperMap[hStep.key].group.visible">
                    </td>
                </tr>
            </table>
            
        </div> 
    </div>
    
</apex:page>