<apex:page showHeader="false" sidebar="false" tabStyle="ecrf__c" controller="PAWS_ProjectFlowGeneratorController" action="{!Init}">
	<apex:includeScript value="{!URLFOR($Resource.STSWR1__workrelay_resources, '/plugin/progressbar/progressbar.min.js')}"/>
	<apex:stylesheet value="{!URLFOR($Resource.STSWR1__workrelay_resources, 'plugin/progressbar/progressbar.css')}" />
	
	<style type="text/css">
		.progressbar-container {
			position: relative;
			display: inline-block;
			width: 75%;
			padding: 20px 5px 20px 5px;
			-webkit-border-radius: 10px;
			-moz-border-radius: 10px;
			border-radius: 10px;
			background-color: #D1D0CE;
		}
		
		/*.progressbar-text {
			overflow: hidden;
			text-overflow: ellipsis;
		}*/
	</style>
	
	<script type="text/javascript">
		var currentProgressBarLabel = '',
			processRemovalActions_CallbackFn,
			progressBar, barItem,
			progressBarPercentageEl,
			progressBarLabelEl,
			progressBarTimeElapsedRemaining,
			oName,
			oValue,
			stopProcess = false,
			projectRemovalActions = [],
			actions = [],
			totalActionsCount,
			totalActionsProcessed = 0, totalActionsProcessedCurrentSection = 0,
			pawsProjectId = '',
			flowId = '',
			currentCloningFlowName = '',
			countSubflowsTotal = 0,
			countSubflowsLeft = 0,
			processStartTime,
			processStartTimeCurrentSession,
			processTimerInterval,
			startProcessFn,
			isResumeProcess = false,
			totalActionsProcessedServer = 0,
			isProcessingCountries = false,
			processStartTimeOnClientSide = 0;
		
		function getURLParameterByName(name){
			name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
			
			var regex = new RegExp('[\\?&]' + name + '=([^&#]*)'),
				results = regex.exec(location.search);
			
			return results === null ? null : decodeURIComponent(results[1].replace(/\+/g, ' '));
		};
		
		function progressBarSetPercent(percent){
			percent = (typeof percent === 'number' ? Math.min(percent, 100) : 0);
			progressBar.setPercent(percent, 'item1');
			progressBarPercentageEl.innerHTML = percent + '%';
		};
		
		function progressBarSetLabel(label){
			progressBarLabelEl.innerHTML = label;
		};
		
		function progressBarGetLabel(){
			return progressBarLabelEl.innerHTML;
		};
		
		function updateTimerInfo(){
			var timeElapsedInSecs, timeElapsedInSecsCurrentSession, timeRemainingInSecs = 0,
				avgActionTimeInSecs, actionsLeftCount,
				timeNow = new Date().getTime();
			
			timeElapsedInSecs = (timeNow - processStartTime) / 1000;
			timeElapsedInSecsCurrentSession = (timeNow - processStartTimeCurrentSession) / 1000;
			
			if (stopProcess !== true && totalActionsProcessed > 0){
				avgActionTimeInSecs = parseInt(timeElapsedInSecsCurrentSession / totalActionsProcessedCurrentSection, 10);
				actionsLeftCount = totalActionsCount - totalActionsProcessed;
				timeRemainingInSecs = avgActionTimeInSecs * actionsLeftCount;
			}
			
			progressBarTimeElapsedRemaining.innerHTML = 'Time Elapsed: <b>' + formatSecondsAsTime(timeElapsedInSecs) + '</b>' + (timeRemainingInSecs > 0 ? '. Estimated Time Remaining: <b>' + formatSecondsAsTime(timeRemainingInSecs) + '</b>' : '');
		}
		
		function formatSecondsAsTime(secs){
			var hr  = Math.floor(secs / 3600),
				min = Math.floor((secs - (hr * 3600)) / 60),
				sec = Math.floor(secs - (hr * 3600) - (min * 60));
				
			if (hr < 10) {hr = '0' + hr;}
			if (min < 10) {min = '0' + min;}
			if (sec < 10) {sec = '0' + sec;}
			
			return hr + ':' + min + ':' + sec;
		}
		
		function ajaxRequestCompleted(){
			if (checkIfPageRefreshIsNeeded()){
				totalActionsProcessed++;
				totalActionsProcessedCurrentSection++;
				processNextAction();
			}
		};
		
		function checkIfPageRefreshIsNeeded(){
			//there were reports that during long-running generation processes browser may crash.
			//in order to work around it the page is reloaded every 15 mins - hopefully that erases some browser caches and/or clears RAM
			
			var timeElapsedInSecs,
				href,
				processStartTimeParamOld, processStartTimeParamNew,
				intervalLengthInSecs = 900; //15 mins
			
			if (processStartTimeCurrentSession && isProcessingCountries === true){
				timeElapsedInSecs = (new Date().getTime() - processStartTimeCurrentSession) / 1000;
				if (timeElapsedInSecs > intervalLengthInSecs){
					forcePageReload('processStartTime');
					return false;
				}
			}
			
			return true;
		};
		
		function forcePageReload(paramName){
			processStartTimeParamOld = getURLParameterByName(paramName);
			processStartTimeParamNew = paramName + '=' + (new Date().getTime());
			
			if (!location.search){
				href = location.href + '?' + processStartTimeParamNew;
			}
			else if (processStartTimeParamOld == null){
				href = location.href + '&' + processStartTimeParamNew;
			}
			else{
				processStartTimeParamOld = paramName + '=' + processStartTimeParamOld;
				href = location.href.replace(processStartTimeParamOld, processStartTimeParamNew);
			}
			
			location.href = href;
		};
		
		function processNextAction(){
			if (stopProcess === true){
				rollbackChanges();
				return;
			}
			
			var action;
			
			progressBarSetPercent(totalActionsCount == 0 ? 100 : parseInt((totalActionsProcessed / totalActionsCount) * 100, 10));
			
			if (actions.length > 0){
				action = actions.shift();
				progressBarSetLabel(action.label);
				eval('(' + action.fn + ')();');
			}
			else{
				setTimeout(function(){
					if (pawsProjectId && flowId){
						progressBarSetLabel('Redirecting to Gantt...');
						progressBarPercentageEl.innerHTML = '';
						progressBar.initialMode(true);
						
						location.href = '/apex/STSWR1__GanttView?flow=' + flowId + '&sourceId=' + pawsProjectId + '&viewmode=true&isreadonly=true&autosave={!AutoSaveGantt}&autocollapse=false&hideheader=true&floatingDates=true';
					}
				}, 1000);
			}
		};
		
		function processRemovalActions(callbackFn){
			stopProcess = false;
			progressBar.initialMode(true);
			processRemovalActions_CallbackFn = callbackFn;
			processNextRemovalAction();
		};
		
		function processNextRemovalAction(){
			var action;
			
			if (stopProcess === true){
				afterRollback();
				return;
			}
			
			if (projectRemovalActions.length > 0){
				action = projectRemovalActions.shift();
				progressBarSetLabel(action.label);
				eval('(' + action.fn + ')();');
			}
			else{
				progressBarSetLabel('');
				if (processRemovalActions_CallbackFn){
					progressBar.initialMode(false);
					processRemovalActions_CallbackFn();
					processRemovalActions_CallbackFn = null;
				}
			}
		}
		
		function rollbackChanges(){
			clearInterval(processTimerInterval);
			
			if (progressBar){
				progressBarSetLabel('Rolling back changes...');
				progressBarPercentageEl.innerHTML = '';
				progressBarTimeElapsedRemaining.innerHTML = '';
				
				progressBarSetPercent(0);
			}
			
			if (!isProcessingCountries){
				processRemovalActions(rollback);
			}
			else{
				document.getElementById('progressbar-container').style.display = 'none';
			}
		}
		
		function afterRollback(){
			document.getElementById('progressbar-container').style.display = 'none';
		}
		
		function onProjectFlowCloneComplete(){
			if (stopProcess === true){
				rollbackChanges();
				return;
			}
			
			projectFlowAfterClone();
		}
		
		function onProjectFlowAfterClone(){
			if (stopProcess === true){
				rollbackChanges();
				return;
			}
			
			if (currentCloningFlowName){
				progressBarSetLabel('Cloning sub-flow (<b>' + countSubflowsLeft + '/' + countSubflowsTotal + '</b>): ' + currentCloningFlowName + '...');
				projectFlowClone();
			}
			else{
				ajaxRequestCompleted();
			}
		}
		
		function activateCountryTemplate(){
			if (stopProcess === true){
				rollbackChanges();
				return;
			}
			
			var lbl = currentProgressBarLabel.replace('...', '') + ' (templates: <b>' + currentTemplatesProcessedCount + '/' + currentTemplatesToProcessCount + '</b>)...';
			progressBarSetLabel(lbl);
			
			if (currentTemplatesProcessedCount === currentTemplatesToProcessCount){
				//setTimeout(function(){ajaxRequestCompleted();}, 50);
				ajaxRequestCompleted();
			}
			else{
				generateCountryWRFlow_ActivateTemplate();
			}
		}
		
		function activateSiteTemplate(){
			if (stopProcess === true){
				rollbackChanges();
				return;
			}
			
			var lbl = currentProgressBarLabel.replace('...', '') + ' (templates: <b>' + currentTemplatesProcessedCount + '/' + currentTemplatesToProcessCount + '</b>)...';
			progressBarSetLabel(lbl);
			
			if (currentTemplatesProcessedCount === currentTemplatesToProcessCount){
				//setTimeout(function(){ajaxRequestCompleted();}, 50);
				ajaxRequestCompleted();
			}
			else{
				generateSiteWRFlow_ActivateTemplate();
			}
		}
		
		function restartProcessOnClick(btn){
			var allInputs, i;
			
			if (confirm('Are you sure to restart the process?')){
				allInputs = document.getElementsByTagName('input');
				for (i = 0; i < allInputs.length; i++){
					if (allInputs[i].type == 'submit'){
						allInputs[i].className = 'btnDisabled';
					}
				}
				
				btn.value = 'Working...'
				
				restartProcess();
			}
		}
		
		function resumeProcessOnClick(btn){
			var allInputs, i;
			
			if (confirm('Are you sure to resume the process?')){
				allInputs = document.getElementsByTagName('input');
				for (i = 0; i < allInputs.length; i++){
					if (allInputs[i].type == 'submit'){
						allInputs[i].className = 'btnDisabled';
					}
				}
				
				btn.value = 'Working...'
				
				resumeProcess();
			}
		}
	</script>
	
	<apex:form >
		<apex:pageBlock >
			<apex:pageMessages id="page-messages" />
			
			<apex:outputPanel id="project-removal-page-messages">
				<apex:pageMessages rendered="{!IsRemovingFlow}"/>
			</apex:outputPanel>
			
			<apex:actionFunction name="generatePAWSProject" action="{!GeneratePAWSProject}" rerender="page-messages,action-functions-execution-results" oncomplete="ajaxRequestCompleted();"></apex:actionFunction>
			<apex:actionFunction name="updatePAWSProject" action="{!UpdatePAWSProject}" rerender="page-messages,action-functions-execution-results" oncomplete="ajaxRequestCompleted();"></apex:actionFunction>
			<apex:actionFunction name="removeWRFlow" action="{!RemoveWRFlow}" rerender="project-removal-page-messages,action-functions-execution-results" oncomplete="processNextRemovalAction();">
				<apex:param name="recordIdsToDelete" assignTo="{!RecordIdsToDelete}" value="" />
			</apex:actionFunction>
			
			<apex:actionFunction name="beforeProjectFlowClone" action="{!GenerateProjectWRFlow_BeforeClone}" rerender="page-messages,action-functions-execution-results" oncomplete="ajaxRequestCompleted();"></apex:actionFunction>
			<apex:actionFunction name="projectFlowClone" action="{!GenerateProjectWRFlow_Clone}" rerender="page-messages,action-functions-execution-results" oncomplete="onProjectFlowCloneComplete();"></apex:actionFunction>
			<apex:actionFunction name="projectFlowAfterClone" action="{!GenerateProjectWRFlow_AfterFlowClone}" rerender="page-messages,action-functions-execution-results" oncomplete="onProjectFlowAfterClone();"></apex:actionFunction>
			<apex:actionFunction name="afterProjectFlowClone" action="{!GenerateProjectWRFlow_AfterClone}" rerender="page-messages,action-functions-execution-results" oncomplete="ajaxRequestCompleted();"></apex:actionFunction>
			
			<apex:actionFunction name="saveProgressState" action="{!SaveProgressState}" rerender="page-messages,action-functions-execution-results" oncomplete="ajaxRequestCompleted();">
				<apex:param name="totalActionsProcessed" assignTo="{!TotalActionsProcessed}" value="" />
				<apex:param name="isProcessingCountries" assignTo="{!IsProcessingCountries}" value="" />
				<apex:param name="processStartTimeOnClientSide" assignTo="{!ProcessStartTimeOnClientSide}" value="" />
			</apex:actionFunction>
			
			<apex:actionFunction name="generateCountryWRFlow" action="{!GenerateCountryWRFlow}" rerender="page-messages,action-functions-execution-results" oncomplete="activateCountryTemplate();">
				<apex:param name="countryName" assignTo="{!CurrentCountryInProcess}" value="" />
				<apex:param name="countryID" assignTo="{!CurrentCountryIDInProcess}" value="" />
			</apex:actionFunction>
			<apex:actionFunction name="generateCountryWRFlow_ActivateTemplate" action="{!GenerateCountryWRFlow_ActivateTemplate}" rerender="page-messages,action-functions-execution-results" oncomplete="activateCountryTemplate();"></apex:actionFunction>
			<apex:actionFunction name="generateCountryWRFlow_ProcessSelectedServices" action="{!GenerateCountryWRFlow_ProcessSelectedServices}" rerender="page-messages,action-functions-execution-results" oncomplete="ajaxRequestCompleted();"></apex:actionFunction>
			
			<apex:actionFunction name="generateSiteWRFlow" action="{!GenerateSiteWRFlow}" rerender="page-messages,action-functions-execution-results" oncomplete="activateSiteTemplate();">
				<apex:param name="siteWeekDate" assignTo="{!CurrentSiteWeekDateInProcess}" value="" />
				<apex:param name="siteName" assignTo="{!CurrentSiteNameInProcess}" value="" />
			</apex:actionFunction>
			<apex:actionFunction name="generateSiteWRFlow_ActivateTemplate" action="{!GenerateSiteWRFlow_ActivateTemplate}" rerender="page-messages,action-functions-execution-results" oncomplete="activateSiteTemplate();"></apex:actionFunction>
			<apex:actionFunction name="generateSiteWRFlow_ProcessSelectedServices" action="{!GenerateSiteWRFlow_ProcessSelectedServices}" rerender="page-messages,action-functions-execution-results" oncomplete="ajaxRequestCompleted();"></apex:actionFunction>
			
			<apex:actionFunction name="finalize" action="{!Finalize}" rerender="page-messages,action-functions-execution-results" oncomplete="ajaxRequestCompleted();"></apex:actionFunction>
			<apex:actionFunction name="rollback" action="{!Rollback}" oncomplete="afterRollback();"></apex:actionFunction>
			
			<apex:actionFunction name="restartProcess" action="{!RestartProcess}" oncomplete="forcePageReload('dt');"></apex:actionFunction>
			<apex:actionFunction name="resumeProcess" action="{!ResumeProcess}" oncomplete="forcePageReload('dt');"></apex:actionFunction>
			
			<div id="progressbar-container" style="width:100%; padding:50px 0; text-align:center; display:{!IF(IsError, 'none', 'block')}">
				<div class="progressbar-container">
					<div id="progressbar-label" class="progressbar-text top-left"></div>
					<div id="progressbar"></div>
					<div id="progressbar-time-elapsed-remaining" class="progressbar-text bottom-left"></div>
					<div id="progressbar-percentage" class="progressbar-text bottom-right"></div>
				</div>
			</div>
			
			<apex:outputPanel id="action-functions-execution-results" layout="block">
				
				<script type="text/javascript">
					currentTemplatesProcessedCount = {!CurrentTemplatesProcessedCount};
					currentTemplatesToProcessCount = {!CurrentTemplatesToProcessCount};
					isProcessingCountries = {!IsProcessingCountries};
				</script>
				
				<apex:outputPanel layout="none" rendered="{!IsInit}">
					<script type="text/javascript">
						isResumeProcess = {!IsResumeProcess};
						processStartTimeOnClientSide = {!ProcessStartTimeOnClientSide};
						totalActionsProcessedServer = {!TotalActionsProcessed};
					</script>
				</apex:outputPanel>
			
				<apex:outputPanel layout="none" rendered="{!IsProjectFlowClone}">
					<script type="text/javascript">
						currentCloningFlowName = '{!JSENCODE(CurrentCloningFlowName)}';
						countSubflowsTotal = {!CountSubflowsTotal};
						countSubflowsLeft = {!CountSubflowsLeft};
					</script>
				</apex:outputPanel>
				
				<apex:outputPanel layout="none" rendered="{!IsError}">
					<script type="text/javascript">
						stopProcess = true;
						projectRemovalActions = JSON.parse('{!JSENCODE(RemoveProjectActionsListJSON)}');
					</script>
				</apex:outputPanel>
				
				<apex:outputPanel layout="none" rendered="{!IsComplete}">
					<script type="text/javascript">
						pawsProjectId = '{!GeneratedPAWSProjectId}';
						flowId = '{!GeneratedFlowId}';
					</script>
				</apex:outputPanel>
				
				<apex:commandButton value="Restart Process" title="Deletes everything that was generated previously and starts the process from the very beginning." onclick="restartProcessOnClick(this); return false;" rendered="{!AND(IsError, IsProcessingCountries)}"/>
				<apex:commandButton value="Resume Process" title="Tries to resume the process from the point where it stopped." onclick="resumeProcessOnClick(this); return false;" rendered="{!AND(IsError, IsProcessingCountries)}"/>
			</apex:outputPanel>
		</apex:pageBlock>
	</apex:form>
	
	<script type="text/javascript">
		function startGenerationProcess(skipRemovalActions){
			progressBarPercentageEl = document.getElementById('progressbar-percentage');
			progressBarLabelEl = document.getElementById('progressbar-label');
			progressBarTimeElapsedRemaining = document.getElementById('progressbar-time-elapsed-remaining');
			oName = ProgressBar.OPTION_NAME;
			oValue = ProgressBar.OPTION_VALUE;
			
			progressBar = new ProgressBar('progressbar', {'width':'100%', 'height':'4px'});
			
			barItem = {};
			barItem[oName.ITEM_ID] = 'item1';
			barItem[oName.TYPE] = oValue.TYPE.BAR;
			barItem[oName.OPACITY] = 1;
			barItem[oName.SPACE] = 0;
			barItem[oName.ALIGN] = oValue.ALIGN.LEFT;
			barItem[oName.POSITION] = oValue.POSITION.RELATIVE;
			barItem[oName.COLOR_ID] = oValue.COLOR_ID.BLUE;
			barItem[oName.PERCENT] = 0;
			progressBar.createItem(barItem);
			
			projectRemovalActions = JSON.parse('{!JSENCODE(RemoveProjectActionsListJSON)}');
			actions = JSON.parse('{!JSENCODE(ActionsListJSON)}');
			
			startProcessFn = function(){
				totalActionsCount = actions.length;
				processStartTime = new Date().getTime();
				processStartTimeCurrentSession = processStartTime; //needed for auto page reload
				processTimerInterval = setInterval(updateTimerInfo, 1000);
				
				if (isResumeProcess === true){
					totalActionsProcessed = totalActionsProcessedServer;
					
					if (isNaN(processStartTimeOnClientSide) || processStartTimeOnClientSide == 0 || processStartTimeOnClientSide > processStartTime) processStartTimeOnClientSide = processStartTime;
					processStartTime = processStartTimeOnClientSide;
					
					if (actions.length >= totalActionsProcessedServer){
						actions.splice(0, totalActionsProcessedServer); //remove processed items from actions list
					}
				}
				
				processNextAction(); //start process!
			};
			
			if (projectRemovalActions.length > 0 && !skipRemovalActions){
				processRemovalActions(startProcessFn);
			}
			else{
				startProcessFn();
			}
		}
		
		if (stopProcess !== true){
			startGenerationProcess(isResumeProcess);
		}
	</script>
</apex:page>