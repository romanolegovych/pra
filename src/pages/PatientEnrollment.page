<apex:page id="pg" sidebar="false" showHeader="false" controller="PBB_Mobile_Cont">>
<apex:stylesheet value="{!URLFOR($Resource.PRA_PBB_DataTables_1_10_7, 'DataTables-1.10.7/media/css/jquery.dataTables.css')}"/>
<!-- <apex:stylesheet value="{!URLFOR($Resource.PRA_PBB_styles, 'styles/fonts/styles.css')}"/> -->
<style>
    @font-face {
        font-family: "Gotham Pro Regular";
        src: url('{!URLFOR($Resource.PRA_PBB_styles, 'styles/fonts/GothaProReg.otf')}');
    }
    @font-face {
        font-family: "Gotham Pro Medium";
        src: url('{!URLFOR($Resource.PRA_PBB_styles, 'styles/fonts/GothaProMed.otf')}');
    }
    .main-page {
        width: 80%;
        margin-bottom: 63px;
        padding: 73px 10px 10px 10px;
        position: relative;
    }
</style>
<apex:stylesheet value="{!$Resource.PRA_PBB_CSS_Temp}"/>

<apex:includeScript value="{!URLFOR($Resource.PRA_PBB_DataTables_1_10_7, 'DataTables-1.10.7/media/js/jquery.js')}"/>
<apex:includeScript value="{!URLFOR($Resource.PRA_PBB_DataTables_1_10_7,'DataTables-1.10.7/media/js/jquery.dataTables.min.js')}"/>

<apex:form id="frm">
<apex:actionFunction action="{!showComponent}" name="showComponent" reRender="tableCom" >
    <apex:param assignTo="{!activeComponent}" name="activeComponent" value=""/>
</apex:actionFunction>
    <header class="basic-info">
        <div class="header-section">
            <p class="header-label">Therapeutic Area / Indication</p>
            <p class="header-value"><apex:outputText value="{!projectDetailItem.TherapeuticArea}"/> / Indication</p>
        </div>
        <div class="header-section">
            <p class="header-label">Compound</p>
            <p class="header-value"><apex:outputText value="{!projectDetailItem.Compound}"/></p>
        </div>
        <div class="header-section">
            <p class="header-label">Protocol ID</p>
            <p class="header-value"><apex:outputText value="{!projectDetailItem.ProtocolID}"/></p>
        </div>
        <div class="header-section">
            <p class="header-label">Project Manager</p>
            <p class="header-value"><apex:outputText value="{!projectDetailItem.ProjectManager}"/></p>
        </div>
    </header>
    <c:PBB_Left_Menu_Enrollment_Component />
    <main class="main-page">
        <div class="page hidden">
            <div class="enrollment-summary-info">
                <div class="enrollment-item-label">FPI Date</div>
                <div class="enrollment-item-value">
                    <apex:outputText value="{0,date,dd'-'MMM'-'yy}">
                        <apex:param value="{!patientEnrollmentItem.FPI_Planned}" /> 
                    </apex:outputText>
                </div>
            </div>
            <div class="enrollment-summary-info">
                <div class="enrollment-item-label">Target # of patients</div>
                <div class="enrollment-item-value">{!patientEnrollmentItem.TargetNoPatients}</div>
            </div>
            <div class="enrollment-summary-info">
                <div class="enrollment-item-label">Patients enrolled to date</div>
                <div class="enrollment-item-value">{!patientEnrollmentItem.EnrolledToDate}</div>
            </div>
            <div class="enrollment-summary-info">
                <div class="enrollment-item-label">LPI Planned</div>
                <div class="enrollment-item-value">
                    <apex:outputText value="{0,date,dd'-'MMM'-'yy}">
                        <apex:param value="{!patientEnrollmentItem.LPI_Planned}" /> 
                    </apex:outputText>
                </div>
            </div>
            <div class="enrollment-summary-info">
                <div class="enrollment-item-label">LPI expected</div>
            </div>
            <div class="enrollment-summary-info">
                <div class="enrollment-item-sub-label">Hight risk scenario</div>
                <div class="enrollment-item-value">
                    <apex:outputText value="{0,date,dd'-'MMM'-'yy}">
                        <apex:param value="{!patientEnrollmentItem.LPI_HighRisk}" /> 
                    </apex:outputText>
                </div>
            </div>
            <div class="enrollment-summary-info">
                <div class="enrollment-item-sub-label">Medium risk scenario</div>
                <div class="enrollment-item-value">
                    <apex:outputText value="{0,date,dd'-'MMM'-'yy}">
                        <apex:param value="{!patientEnrollmentItem.LPI_MedRisk}" /> 
                    </apex:outputText>
                </div>
            </div>
            <div class="enrollment-summary-info">
                <div class="enrollment-item-sub-label">Low risk scenario</div>
                <div class="enrollment-item-value">
                    <apex:outputText value="{0,date,dd'-'MMM'-'yy}">
                        <apex:param value="{!patientEnrollmentItem.LPI_LowRisk}" /> 
                    </apex:outputText>
                </div>
            </div>
        </div>

        <div class="page graph hidden">
            <c:PBB_ViewSitesandSubjectgraphs GType="SubjectEnrollment" PBBSID="{!scenarioId}" id="c"/>
        </div>

        <div class="page hidden">
            <c:PBB_OBIEE_Reports PID="SDI503XX-503318" GType="Enrollment Status by Country">
            </c:PBB_OBIEE_Reports>
        </div>

        <div class="page hidden">
            <c:PBB_OBIEE_Reports PID="SDI503XX-503318" GType="Enrollment Status by Site">
            </c:PBB_OBIEE_Reports>
        </div>

    </main>
    <c:PBB_Footer_Menu_Component />
</apex:form>
<script>
var j$ = jQuery.noConflict();

function getQueryParams(qs) {
    qs = qs.split("+").join(" ");

    var params = {}, tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }
    return params;
}

// var j$ = $; 
$(document).ready(function() {
    // page switching
    function showPage(number) {
        $('.page').addClass('hidden').eq(number).removeClass('hidden');
        $('.navigation a').removeClass('active').eq(number).addClass('active');
    }
    
    // show page passed in URL
    var params = getQueryParams(document.location.search);
    var numPages = 4;
        
    var page = params['page'];
    if (page && (page = parseInt(page)) && page > 0 && page <= numPages) {
        page = page - 1;
    } else {
        page = 0;
    }
    showPage(page);
    //showComponent('Enrollment Status by Country');

    // navigation    
    $('.navigation').on('click', 'a', function(){
        showPage($(this).index());
        return false;
    });
});

</script>
</apex:page>