<apex:page controller="BDT_Characteristics" showHeader="false">
	<apex:composition template="BDT_GeneralTemplate">
 		<apex:define name="MainContent">
			<apex:form >
				<apex:actionStatus startText="Communicating with server, please wait..." layout="block" startStyleClass="BDTbusy" id="busy"/>

				<!-- Population  -->
				<apex:outputPanel rendered="{!securitiesA.ReadPriv}">
					<div class="BDT_Container">
					 	<div class="BDT_PageBlockHeader">
							<h2>Populations</h2>
							<apex:commandLink styleClass="BDT_BlueButton" value="Add" action="{!createPopulation}" rendered="{!securitiesA.CreatePriv}">
								<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/new_icon.png')}"/>
							</apex:commandLink>
						</div>
						  	<apex:dataTable value="{!populationList}" var="sl" width="100%" styleClass="BDT_Table">
	   						<apex:column headerValue="{!$ObjectType.Population__c.fields.Name.label}">
	   							<apex:outputfield value="{!sl.Name}" />
							</apex:column>
							<apex:column headerValue="Gender">
								<apex:outputText value="M" rendered="{!sl.Male__c}"/>
								<apex:outputText value="+" rendered="{!if(sl.Male__c && sl.Female__c, true, false)}"/>
								<apex:outputText value="F" rendered="{!sl.Female__c}"/>
							</apex:column>
							<apex:column headerValue="Age">
								<apex:outputfield value="{!sl.Age_Min__c}" />-
								<apex:outputfield value="{!sl.Age_Max__c}" />
							</apex:column>
							<apex:column headerValue="Smoking">
								<apex:outputText value="{!IF(sl.Smoking__c,'Y','N')}"/>
								<apex:outputPanel rendered="{!sl.Smoking__c}">
									(<apex:outputField value="{!sl.Smoking_Max__c}" />)
								</apex:outputPanel>
							</apex:column>
							<apex:column headerValue="BMI">
								<apex:outputfield value="{!sl.BMI_Min__c}" />-
								<apex:outputfield value="{!sl.BMI_Max__c}" />
							</apex:column>
							<apex:column headerValue="Blood Pressure">
								<apex:outputfield value="{!sl.BloodPS_Min__c}" />/
								<apex:outputfield value="{!sl.BloodPD_Min__c}" />-
								<apex:outputfield value="{!sl.BloodPS_Max__c}" />/
								<apex:outputfield value="{!sl.BloodPD_Max__c}" />
							</apex:column>
							<apex:column style="width:15px">
								<apex:commandLink action="{!edit}" rerender="hiddenBlock" rendered="{!securitiesA.ReadPriv}">
										<img src="{!URLFOR($Resource.BDT_nav_files, '/img/edit.png')}"/>
										<apex:param name="PopulationId" value="{!sl.id}"/>
									</apex:commandLink>
							</apex:column>
							<apex:pageBlock id="hiddenBlock" rendered="false"/>
						</apex:dataTable>
					</div>
				</apex:outputPanel>
				<!-- Population Study Assignment -->
				<apex:outputPanel id="PopulationsToStudies" rendered="{!securitiesB.ReadPriv}">

					<div class="BDT_Container">
						<c:BDT_PageBlockHeader sectionHeading="Populations to Study"/>
						<table class="BDT_Table">
							<thead>
								<tr>
									<th>Population ID</th>
									<apex:repeat value="{!studiesList}" var="h">
										<th>{!h.Code__c}</th>
									</apex:repeat>
								</tr>
							</thead>
							<apex:repeat value="{!populationWrapperList}" var="a">
								<tr>
									<td><apex:outputText value="{!a.parentName}" /></td>
									<apex:repeat value="{!a.childWrapperList}" var="swl">
										<td><apex:inputcheckbox value="{!swl.selected}" disabled="{!swl.disabled}"/></td>
									</apex:repeat>
								</tr>
							</apex:repeat>
						</table>
						<div class="BDT_PageBlockFooter">
							<table>
								<tr >
									<td class="cPbButton" >
										<apex:commandLink styleClass="BDT_BlueButton" value="Save" action="{!saveStudyPopulationAss}" rendered="{!securitiesB.CreatePriv||securitiesB.UpdatePriv||securitiesB.DeletePriv}" status="busy" rerender="PopulationsToStudies">
											<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/save.png')}"/>
										</apex:commandLink>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</apex:outputPanel>

<!-- Start: Methods and Compounds to Study Assignment -->
				<style>
					td.smallerPaddingTd {
						padding-left:4px;
						padding-right:4px;
					}
					th.smallerPaddingTh {
						padding-left:4px;
						padding-right:4px;
					}
					th.centerTh{
						text-align:center;
					}
				</style>
				<apex:outputPanel id="MethodsAndCompoundsToStudies" rendered="{!securitiesC.ReadPriv}">
					<div class="BDT_Container">
						<div class="BDT_PageBlockHeader">
							<h2>Methods and Compounds to Study</h2>
							<apex:commandlink action="{!addLabMethodAssignment}" value="Add method" styleclass="BDT_BlueButton" status="busy" rendered="{!securitiesC.CreatePriv}">
								<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/new_icon.png')}"/>
							</apex:commandlink>
						</div>
						<div class="cPbBody">
							<table class="BDT_Table">
								<thead>
									<tr>
										<th rowspan="2">Method</th>
										<th rowspan="2">Compound(s)</th>
										<th rowspan="2">Species</th>
										<th rowspan="2">Matrix</th>
										<th rowspan="2">Anti-coagulant</th>
										<th rowspan="2">Analytical<br/>Technique</th>
										<th rowspan="2">Detection</th>
										<apex:repeat value="{!studiesList}" var="st">
											<th colspan="3" class="centerTh">
												{!RIGHT(st.Code__c,2)}
											</th>
										</apex:repeat>
										<apex:outputPanel layout="none" rendered="{!securitiesC.ReadPriv}">
											<th rowspan="2"></th>
										</apex:outputPanel>
										<tr>	
											<apex:repeat value="{!studiesList}" var="st">
												<th class="smallerPaddingTh centerTh" style="border-right:none;">MD</th>
												<th class="smallerPaddingTh centerTh" style="border:none;">VAL</th>
												<th class="smallerPaddingTh centerTh" style="border-left:none;">AN</th>
											</apex:repeat>
										</tr>
									</tr>
								</thead>
								<tbody> 
									<apex:variable var="previousMethod" value=""/>
									<apex:repeat value="{!methodCompStudyList}" var="mcs">
										<tr>
											<td>{!IF(mcs.methodCompound.LaboratoryMethod__c==previousMethod, '', mcs.methodCompound.LaboratoryMethod__r.Name)}</td>
											<td>{!mcs.methodCompound.Name}</td>
											<td>{!IF(mcs.methodCompound.LaboratoryMethod__c==previousMethod, '', mcs.methodCompound.LaboratoryMethod__r.Species__c)}</td>
											<td>{!IF(mcs.methodCompound.LaboratoryMethod__c==previousMethod, '', mcs.methodCompound.LaboratoryMethod__r.Matrix__c)}</td>
											<td>{!IF(mcs.methodCompound.LaboratoryMethod__c==previousMethod, '', mcs.methodCompound.LaboratoryMethod__r.AntiCoagulant__c)}</td>
											<td>{!IF(mcs.methodCompound.LaboratoryMethod__c==previousMethod, '', mcs.methodCompound.LaboratoryMethod__r.AnalyticalTechnique__c)}</td>
											<td>{!IF(mcs.methodCompound.LaboratoryMethod__c==previousMethod, '', mcs.methodCompound.LaboratoryMethod__r.Detection__c)}</td>
											<apex:repeat value="{!mcs.analysisList}" var="mcsAnalysis">
												<td class="smallerPaddingTd" style="border-right:none;">
													<apex:inputCheckbox value="{!mcsAnalysis.MethodDevelopment__c}" disabled="TRUE" rendered="{!mcsAnalysis.MethodDevelopment__c}" style="float:none;height:14px;"/>
												</td>
												<td class="smallerPaddingTd" style="border:none;">
													<apex:inputCheckbox value="{!mcsAnalysis.MethodValidation__c}" disabled="TRUE" rendered="{!mcsAnalysis.MethodValidation__c}" style="float:none;height:14px;"/>
												</td>
												<td class="smallerPaddingTd" style="border-left:none;">
													<apex:inputCheckbox value="{!mcsAnalysis.Analysis__c}" disabled="TRUE" rendered="{!mcsAnalysis.Analysis__c}" style="float:none;height:14px;"/>
												</td>
											</apex:repeat>
											<apex:outputPanel layout="none" rendered="{!securitiesC.ReadPriv}">
												<td>
													<apex:commandLink action="{!editLabMethodAssignment}" status="busy" rendered="{!NOT(mcs.methodCompound.LaboratoryMethod__c==previousMethod)}" styleclass="tiptip" title="edit">
														<img src="{!URLFOR($Resource.BDT_nav_files, '/img/edit.png')}"/>
														<apex:param name="selLaboratotyMethodId" value="{!mcs.methodCompound.LaboratoryMethod__r.id}" assignTo="{!laboratoryMethodId}"/>
													</apex:commandLink>
												</td>
											</apex:outputPanel>
										</tr>
										<apex:variable var="previousMethod" value="{!mcs.methodCompound.LaboratoryMethod__c}"/>
									</apex:repeat>
								</tbody>
							</table>
						</div>
					</div>
				</apex:outputPanel>
<!-- End: Methods and Compounds to Study Assignment -->

				<!-- Sites -->
				<apex:outputPanel id="SitesToStudies">
					<div class="BDT_Container">
						<c:BDT_PageBlockHeader sectionHeading="Sites to Study" buttonValue="Add" buttonName="Add"  goToPageName="BDT_NewEditStudySite" customId="{!projectId}"/>
						<table class="BDT_Table">
							<thead>
								<tr>
									<th>Site Name</th>
									<apex:repeat value="{!studiesList}" var="s">
										<th>{!s.Code__c}</th>
									</apex:repeat>
								</tr>
							</thead>
							<apex:repeat value="{!studySiteAssignments}" var="ssa">
								<tr>
									<td><apex:outputField value="{!ssa.projectSite.Site__r.Name}" /></td>
									<apex:repeat value="{!ssa.StudySiteAssignments}" var="swl">
										<td><apex:inputcheckbox value="{!swl.selected}" disabled="{!swl.disabled}" rendered="{!NOT(swl.disabled)}"/></td>
									</apex:repeat>
								</tr>
							</apex:repeat>
						</table>
						<div class="BDT_PageBlockFooter">
							<table>
								<tr>
									<td class="cPbButton" >
										<apex:commandLink styleClass="BDT_BlueButton" value="Save" action="{!saveStudySiteAssignments}" status="busy" rerender="SitesToStudies">
											<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/save.png')}"/>
										</apex:commandLink>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</apex:outputPanel>

				<!-- Subcontractors -->
				<apex:outputpanel id="subcontractors">
					<div class="BDT_Container">
						<div class="BDT_PageBlockHeader">
							<h2>Subcontractors to Study</h2>
							<apex:commandLink styleClass="BDT_BlueButton" value="Add" action="{!assignSubcontractors}">
								<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/new_icon.png')}"/>
								<!--<apex:param name="ProjectId" value="{!projectId}"/>-->
							</apex:commandLink>
						</div>
						<table class="BDT_Table">
							<thead>
								<tr>
									<th>Subcontractor</th>
									<th>Activity</th>
									<apex:repeat value="{!studiesList}" var="st">
										<th>{!st.Code__c}</th>
									</apex:repeat>
								</tr>
							</thead>
							<tbody>
								<apex:repeat value="{!subcontractorAssignmentList}" var="sca">
									<tr>
										<td>{!sca.PSA.SubcontractorActivity__r.Subcontractor__r.Name}</td>
										<td>{!sca.PSA.SubcontractorActivity__r.Name}</td>
										<apex:repeat value="{!sca.StudySubcAss}" var="st">
											<td><apex:inputcheckbox value="{!st.IsSelected}"/></td>
										</apex:repeat>
									</tr>
								</apex:repeat>
							</tbody>
						</table>
						<div class="BDT_PageBlockFooter">
							<table>
								<tr >
									<td class="cPbButton" >
										<apex:commandLink styleClass="BDT_BlueButton" value="Save" action="{!saveSubcontractorAssignments}" rerender="subcontractors" status="busy">
											<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/save.png')}"/>
										</apex:commandLink>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</apex:outputpanel>
			</apex:form>
		</apex:define>
	</apex:composition>
</apex:page>