<apex:page controller="CCI_CrsInstanceController" sidebar="false">
	<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/redmond/jquery-ui.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" type="text/css" href="{!URLFOR($Resource.jqueryDatatables,'DataTables/css/jquery.dataTables.css')}" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>    
	<script type="text/javascript" src=" {!URLFOR($Resource.jqueryDatatables,'DataTables/js/jquery.dataTables.min.js')}"></script>
	<script type="text/javascript" src=" {!URLFOR($Resource.cci_resources,'resources/js/datatable_plugins.js')}"></script>
	<apex:stylesheet value="{!$Resource.PRAStyle}"/> 
	
	<script>
		var j$ = jQuery.noConflict();
		function instanceTable() {
			j$(document).ready(function() {
				j$("Table[id$=instanceTable]").show();
				j$("Table[id$=instanceTable]").dataTable({
					"sPaginationType": "full_numbers",
					"bFilter": true,
					"iDisplayLength": 10,
					"aaSorting": [ [0,'asc'] ],
					"oLanguage": {    "sSearch": "Find  " },
					"aoColumns": [
						{ "sType": 'num-html' },
						null,
						null,
						null,
						null
					]
				});
			});
		}
		function endpointTable() {
			j$(document).ready(function() {
				j$("Table[id$=endpointTable]").show();
				j$("Table[id$=endpointTable]").dataTable({
					"bPaginate": false,
					"bFilter": true,
					"bInfo": false,
					"bSearchable": false,
					"aaSorting": [ [0,'asc'] ],
					"oLanguage": {    "sSearch": "Find  " }
				});
			});
		}
		function closeInstanceDialog() {
			j$("#newInstanceDialog").dialog("close");
		}
		j$(function() {
			j$("#newInstanceDialog").dialog({
				autoOpen: false,
				width: 500,
				height: 250,
				modal: true
			});	
			j$("[id$=instanceLink]").click(function() {
				j$("#newInstanceDialog").dialog("open");
			});
		});
	</script>
	
	<apex:form >
		<apex:pageBlock id="InstName" title="Instance Name Search">
			<span class="boldSpanLabelLeft">Search By Instance Name:</span>
			<apex:inputText value="{!SearchInstanceName}" disabled="false" />
			<br/><br/>
			<apex:commandButton action="{!Search}" value="Search" id="SearchButton" oncomplete="instanceTable();" status="searchingInstance" rerender="tabPanel,warningPanel"/>
			<apex:actionStatus id="searchingInstance" startText="Searching for Instances...">
				<apex:facet name="start">
					<apex:image value="/img/loading.gif" title="Searching for Instances..."/>
				</apex:facet>
				<apex:facet name="stop"></apex:facet>
			</apex:actionStatus>
			<span class="spanLabel boldText paddingHorizontal">Or</span>
			<apex:commandLink value="Create New Instance" rerender="AddInstName" id="instanceLink"/>

			<apex:outputPanel id="tabPanel">
				<apex:outputPanel rendered="{!not(isnull(instaVOs)) && instaVOs.size > 0}"><br/><br/></apex:outputPanel>
				<apex:pageBlockTable value="{!instaVOs}" var="map" rendered="{!not(isnull(instaVOs)) && instaVOs.size > 0}" id="instanceTable">
					<apex:column >
						<apex:facet name="header">
							<apex:outputLabel value="Instance Id"/>
						</apex:facet>                                    
						<apex:commandLink action="{!searchInstId}" status="searchingEndpoints" oncomplete="endpointTable()" 
							rerender="tabPanel1,warningPanel">
							{!map.instanceId}
							<apex:param name="instanceId" value="{!map.instanceId}" assignTo="{!selectedInstanceID}"/>
						</apex:commandLink>
					</apex:column>
					<apex:column value="{!map.instanceName}">
						<apex:facet name="header">
							<apex:outputLabel value="Instance Name"/>
						</apex:facet>
					</apex:column>
					<apex:column value="{!map.praSystem}">
						<apex:facet name="header">
							<apex:outputLabel value="PRA system"/>
						</apex:facet>
					</apex:column>
					<apex:column value="{!map.siteType}">
						<apex:facet name="header">
							<apex:outputLabel value="System Name"/>
						</apex:facet>
					</apex:column>
					<apex:column value="{!map.source}">
						<apex:facet name="header">
							<apex:outputLabel value="Created By"/>
						</apex:facet>
					</apex:column>
				</apex:pageBlockTable>
				<apex:outputPanel rendered="{!not(isnull(instaVOs)) && instaVOs.size > 0}"><br/><br/></apex:outputPanel>
			</apex:outputPanel>
		</apex:pageBlock>
		
		<apex:outputPanel id="warningPanel">
			<apex:pageMessages />
		</apex:outputPanel>
		
		<apex:outputPanel id="tabPanel1">     
	
			<apex:actionStatus id="searchingEndpoints" startText="Searching for Endpoints...">
				<apex:facet name="start">
					<apex:image value="/img/loading.gif" title="Searching for Endpoints..."/>
				</apex:facet>
				<apex:facet name="stop"></apex:facet>
			</apex:actionStatus>
			
			<apex:pageBlock id="Endpoints" title="Instance Endpoints" rendered="{!not(isnull(renderInstanceVO))}">
					<span class="boldSpanLabelLeft">Instance ID: </span>{!renderInstanceVO.instanceId}<br/>
					<span class="boldSpanLabelLeft">Instance Name: </span>{!renderInstanceVO.instanceName}<br/>
					<span class="boldSpanLabelLeft">Instance Type: </span>{!renderInstanceVO.siteType}<br/>
					<span class="boldSpanLabelLeft">PRA System: </span>{!renderInstanceVO.praSystem}<br/>

					<apex:outputPanel id="endpointTablePanel">
						<apex:pageBlockTable value="{!renderInstanceVO.siteAttributes.entry}" var="map" id="endpointTable">
							<apex:column value="{!map.key}">
								<apex:facet name="header">
									<apex:outputLabel value="Type"/>
								</apex:facet>
							</apex:column>
							<apex:column >
								<apex:facet name="header">
									<apex:outputLabel value="Value"/>
								</apex:facet>
								<apex:inputText value="{!map.value}" style="width:100%;"/>
							</apex:column>   
						</apex:pageBlockTable>
					</apex:outputPanel>
				
				<br/>
				<apex:commandButton id="saveButton" action="{!saveInstance}" value="Save Instance" oncomplete="endpointTable()" 
					status="savingInstance" rerender="tabPanel1,warningPanel" />
				<apex:actionStatus id="savingInstance" startText="Saving Instance...">
					<apex:facet name="start">
						<apex:image value="/img/loading.gif" title="Saving Instance..."/>
					</apex:facet>
					<apex:facet name="stop"></apex:facet>
				</apex:actionStatus>
				
				<br/><br/>
				<span class="boldSpanLabelLeft">Add an Attribute key:</span>
				<br/><br/>
				<span class="boldSpanLabelLeft">Attribute Key:</span>
				<apex:actionRegion >
					<apex:selectList value="{!attributeKey}" size="1" >
						<apex:selectOptions value="{!AttributeKeys}" />
					</apex:selectList>
				</apex:actionRegion>				
				<br/>
				<span class="boldSpanLabelLeft">Value:</span>
				<apex:actionRegion >
					<apex:inputText value="{!URLValue}" disabled="false" />
				</apex:actionRegion>
				
				<br/>
				<apex:commandButton id="AddButton" action="{!Add}" value="Create Attribute Key" oncomplete="endpointTable()" 
					status="addingAttribute" rerender="endpointTablePanel,warningPanel" />
				<apex:actionStatus id="addingAttribute" startText="Adding Attribute...">
					<apex:facet name="start">
						<apex:image value="/img/loading.gif" title="Adding Attribute..."/>
					</apex:facet>
					<apex:facet name="stop"></apex:facet>
				</apex:actionStatus>
			</apex:pageBlock> 
		</apex:outputPanel>
	</apex:form>            
	
	<div id="newInstanceDialog">
		<apex:form >
			<apex:pageBlock id="AddInstName" title="Add an Instance">
				Instance name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<apex:inputText value="{!instName}" disabled="false"/>
				<br/>
				PRA system: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<apex:actionRegion >
					<apex:selectList value="{!praSys}" size="1" >
						<apex:selectOptions value="{!PRASystem}" />
					</apex:selectList>
				</apex:actionRegion>
				
				<br/>
				System Name: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<apex:actionRegion >
					<apex:selectList value="{!SysName}" size="1" >
						<apex:selectOptions value="{!SystemNames}" />
					</apex:selectList>
				</apex:actionRegion>
				
				<br/><br/>
				<apex:commandButton id="SubmitButton" action="{!Submit}" value="Create New" oncomplete="closeInstanceDialog()" 
					status="createInstance" rerender="AddInstName,warningPanel" />
				<apex:commandButton id="closeButton" value="Cancel" oncomplete="closeInstanceDialog()" rerender="AddInstName"/>
				<apex:actionStatus id="createInstance" startText="Creating Instance...">
					<apex:facet name="start">
						<apex:image value="/img/loading.gif" title="Creating Instance..."/>
					</apex:facet>
					<apex:facet name="stop"></apex:facet>
				</apex:actionStatus>
				
			</apex:pageBlock>
		</apex:form>
	</div>
</apex:page>