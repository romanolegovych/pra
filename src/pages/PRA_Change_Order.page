<apex:page controller="PRA_ChangeOrderController" tabStyle="Change_Order__tab">
    <!--  Importing jQuery libs -->
    <apex:includeScript value="{!URLFOR($Resource.jQueryGraph, '/jqui/js/jquery-1.7.1.min.js')}"  />
    <apex:includeScript value="{!URLFOR($Resource.jqueryBlockUI, '/')}"  />
    <script type="text/javascript" src=" {!URLFOR($Resource.jqueryDatatables,'DataTables/js/jquery.dataTables.min.js')}"></script>
    <apex:includeScript value="{!URLFOR($Resource.jqueryUI, 'jquery-ui-1.8.21.smoothness/js/jquery-ui-1.8.21.custom.min.js')}"  />
    <apex:styleSheet value="{!URLFOR($Resource.jqueryUI, 'jquery-ui-1.8.21.smoothness/css/smoothness/jquery-ui-1.8.21.custom.css')}" />
    <link rel="stylesheet" type="text/css" href="{!URLFOR($Resource.jqueryDatatables,'DataTables/css/jquery.dataTables.css')}" />
    <apex:styleSheet value="{!$Resource.PMCStyle}" />
    
    <apex:form >
        <!--  Header -->
        <apex:actionfunction name="updateSelectedProject"  rerender="" status="statusFields" >
            <apex:param name="selectedProject" assignTo="{!selectedProject}" value=""/>
            <apex:param name="selectedProjectLabel" assignTo="{!selectedProjectLabel}" value=""/>
        </apex:actionfunction>
        <apex:outputpanel id="pageTitleHeader">
            <apex:sectionHeader title="Change Order"  subtitle="{!selectedProjectLabel}" >
            </apex:sectionHeader>
        </apex:outputpanel>
        <script>
            var idImgSearchBox = '{!$Component.imgSearchBox}';
            function esc(myid) {
               return '#' + myid.replace(/(:|\.)/g,'\\\\$1');
            }
        </script>  
        <apex:pageMessages /> 
            
        <!--  Search project section -->
        <apex:pageBlock id="fullSearchBox">
                <apex:pageBlockSection columns="2">
                    <apex:pageBlockSectionItem >
                        <apex:outputLabel value="Search by Project ID" for="searchProject" />
                        <apex:outputpanel >
                            <apex:inputText id="searchProject" value="{!selectedProjectId}" styleClass="placeHolder"/>
                            <apex:inputHidden id="searchProjectId" value="{!selectedProject}" />
                            <script>
                                var idInput = '{!$Component.searchProject}';
                                var idInputId = '{!$Component.searchProjectId}';
                            </script>
                            <apex:commandButton value="Search" id="btnSearch"  action="{!doNothingJustSubmitForm}" status="SearchProjectStatus">
                                <apex:actionstatus id="SearchProjectStatus" onstart="$.blockUI();" onstop="$.unblockUI();"/>
                             </apex:commandButton>
                            <apex:commandButton value="Reset" title="Clears all search options to perform new search" alt="this tooltip" action="{!reset}" id="resetButton"/>
                            <apex:actionFunction name="savePreferences" action="{!saveUserPreferences}"/>
                        </apex:outputpanel>
                    </apex:pageBlockSectionItem>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
        <!--  Select action section -->
        <apex:pageBlock id="selectAction">
            <apex:pageBlockSection columns="1">
                <apex:selectList id="slSelectAction" value="{!selectedAction}" label="Create/Modify a Change Order" disabled="{!project=''}">
                    <apex:selectOptions value="{!actionList}" />
                    <apex:actionsupport event="onchange" rerender="opCrctTable,selectAction" onsubmit="$.blockUI();" oncomplete="$.unblockUI();"/>
                </apex:selectList>
            </apex:pageBlockSection>
        </apex:pageBlock>
        
        <apex:outputpanel id="opCrctTable">
        
         <!--  Change order section -->
         <apex:pageBlock id="pbChangeOrderTable" rendered="{!OR(selectedAction==TEXT_ADD_UNITS, selectedAction==TEXT_MODIFY_END_DATE, selectedAction==TEXT_MODIFY_CHANGE_ORDER)}" title="Select change order" >
            <apex:pageblockTable id="coTable" value="{!ProjectChangeOrders }" var="co" >
                <apex:column headerValue="Name" >
                    <apex:commandLink action="{!selectChangeOrder}" value="{!co.name}" rerender="opCrctTable,pbCrctTable">
                        <apex:param name="coId" value="{!co.Id}" assignTo="{!changeOrderId}"/>
                        <apex:param name="coName" value="{!co.Name}" assignTo="{!changeOrderName}"/>                        
                    </apex:commandLink>
                </apex:column>
                <apex:column value="{!co.Status__c}" headerValue="Status"/>                
                <apex:column headerValue="Date Created">
                    <apex:outputText value="{0,date,dd-MMM-yyyy}">                      
                       <apex:param value="{!co.createddate}"/>
                    </apex:outputText>
                </apex:column>
                <apex:column value="{!co.owner.FirstName}   {!co.owner.LastName}" headerValue="Created by"/>
                <apex:column headerValue="Date Executed">
                    <apex:outputText value="{0,date,dd-MMM-yyyy}">                      
                       <apex:param value="{!co.ExecutedDate__c}" />
                    </apex:outputText>
                </apex:column>                
                <apex:column value="{!co.ExecutedBy__r.name}" headerValue="Executed by"/>
            </apex:pageblockTable>
            <script type="text/javascript">
                var idChangeOrderTable = '{!$Component.coTable}';
                $(esc(idChangeOrderTable)).dataTable({
                    "bFilter": false,
                    "bLengthChange": false,
                    "iDisplayLength": 20,
                    "aoColumns": [ 
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                       
                        
                    ]
                });
            </script>
         </apex:pageBlock>
         <!--  Select deleted change order section -->
         <apex:pageBlock id="pbSelectChangeOrderTable" rendered="{!selectedAction==TEXT_DELETE_CHANGE_ORDER}" title="Select change orders" >
            <apex:pageblockbuttons location="top">
                <apex:commandbutton id="cmdBtnDeleteSelCO" value="Delete" action="{!deleteSelectedChangeOrders}" onclick="return confirmDeleteChangeOrders();"/>
            </apex:pageblockbuttons>
            <apex:pageblockTable id="coSelctTable" value="{!selectedChangeOrdersWrappers}" var="co" >
                <apex:column headerValue="Delete" >
                    <div data-coId="{!co.changeOrder.id}">
                        <apex:inputcheckbox value="{!co.isSelected}"/>
                    </div>
                </apex:column>
                <apex:column value="{!co.changeOrder.Name}" headerValue="Name"/>
                <apex:column value="{!co.changeOrder.Status__c}" headerValue="Status"/>
                 <apex:column headerValue="Date Created">
                    <apex:outputText value="{0,date,dd-MMM-yyyy}">                      
                       <apex:param value="{!co.changeOrder.createddate}"/>
                    </apex:outputText>
                </apex:column>
                <apex:column value="{!co.changeOrder.owner.FirstName}   {!co.changeOrder.owner.LastName}" headerValue="Created by"/>
                <apex:column headerValue="Date Executed">
                    <apex:outputText value="{0,date,dd-MMM-yyyy}">                      
                       <apex:param value="{!co.changeOrder.ExecutedDate__c}" />
                    </apex:outputText>
                </apex:column>         
                <apex:column value="{!co.changeOrder.ExecutedBy__r.name}" headerValue="Executed by"/>
            </apex:pageblockTable>
            
            <script type="text/javascript">
                       
                var idSelectedChangeOrderTable = '{!$Component.coSelctTable}';
                var selectedChangeOrder4Delete = new Array();
                        
                $("table[id='{!$Component.coSelctTable}']").find("input[type=checkbox]").each(function(){
                    if ($(this).is(':checked')){
                        var changeOrderId = $(this).parent().attr('data-coId');
                        selectedChangeOrder4Delete.push(changeOrderId);
                    }
                });
                if ( selectedChangeOrder4Delete.length == 0){
                    $("input[id$='cmdBtnDeleteSelCO']").attr('disabled','disabled');
                    $("input[id$='cmdBtnDeleteSelCO']").addClass('btnDisabled');
                } else {
                    $("input[id$='cmdBtnDeleteSelCO']").removeAttr('disabled');
                    $("input[id$='cmdBtnDeleteSelCO']").removeClass('btnDisabled');
                }
                $("table[id='{!$Component.coSelctTable}']").find("input[type=checkbox]").on( "click",  function(event){
                    //  $(this).attr("checked", true);
                    var changeOrderId = $(this).parent().attr('data-coId');
                    var idx = selectedChangeOrder4Delete.indexOf(changeOrderId);
                    //console.debug('click:' + changeOrderId + ' idx:' + idx + ' is Checked:' + $(this).is(':checked'));
                        
                    if (idx == -1){
                        selectedChangeOrder4Delete.push(changeOrderId);
                    } else {
                        selectedChangeOrder4Delete.splice(idx, 1);
                    }
                    if ( selectedChangeOrder4Delete.length == 0){
                        $("input[id$='cmdBtnDeleteSelCO']").attr('disabled','disabled');
                        $("input[id$='cmdBtnDeleteSelCO']").addClass('btnDisabled');
                    } else {
                        $("input[id$='cmdBtnDeleteSelCO']").removeAttr('disabled');
                        $("input[id$='cmdBtnDeleteSelCO']").removeClass('btnDisabled');
                    }
                });
                        
                $(esc(idSelectedChangeOrderTable)).dataTable({
                    "bFilter": false,
                    "bLengthChange": false,
                    "iDisplayLength": 20,
                    "aoColumns": [ 
                        {"bSortable": false},
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                    ]
                });
            </script>
            
         </apex:pageBlock>
         <!--  Crcts table section -->
        <apex:pageBlock id="pbCrctTable" rendered="{!OR(selectedAction=TEXT_NEW_CHANGE_ORDER, AND(selectedAction=TEXT_ADD_UNITS,changeOrderId!=''))}" title="Select client units {!IF(changeOrderName!='', ' - ' + changeOrderName, '')}">
            <apex:pageBlockButtons >
                <apex:commandButton value="Save draft" onClick="getCONameAndSelectedCrcts()" rerender="opCrctTable" action="{!createNewChangeOrder}" rendered="{!selectedAction=TEXT_NEW_CHANGE_ORDER}" oncomplete="$.unblockUI();"/>
                <apex:commandButton value="Add" onClick="$.blockUI();getSelectedCrcts();" rerender="opCrctTable" action="{!addChangeOrderItems}" rendered="{!AND(selectedAction=TEXT_ADD_UNITS,changeOrderId!='')}" oncomplete="$.unblockUI();"/>
            </apex:pageBlockButtons>
            <apex:pageblockTable id="crctTable" value="{!taskListWrapper}" var="crct">
                <apex:column styleClass="tableColText">
                    <apex:facet name="header">
                        Add all <input id="confAllId" type="checkbox" onClick="actFunselectAllCrcts();" />
                    </apex:facet>
                    <input id="inSelectCrct" type="checkbox" data-val="{!crct.selected}" data-crctid="{!crct.taskId}"/>
                </apex:column>
                <apex:column value="{!crct.clientUnitNumber}" headerValue="Client Unit Number" styleClass="tableColText"/>
                <apex:column value="{!crct.description}" headerValue="Description" styleClass="tableColText"/>
                <apex:column value="{!crct.unitDriver}" headerValue="Unit Driver" styleClass="tableColText"/>
                <apex:column headerValue="# of Contracted Units" styleClass="tableColNumber" headerClass="tableHeaderRight">
                    <apex:outputtext value="{0, number, ###,##0.00}">
                         <apex:param value="{!(FLOOR(crct.contractedUnits*100))/100}"/>
                    </apex:outputtext> 
                </apex:column>
                <apex:column headerValue="Unit Cost" styleClass="tableColNumber" headerClass="tableHeaderRight">
                    <apex:outputtext value="{0, number, ###,##0.00}">
                         <apex:param value="{!(FLOOR(crct.unitCost*100))/100}"/>
                    </apex:outputtext> 
                </apex:column>
                <apex:column headerValue="Total Cost" styleClass="tableColNumber" headerClass="tableHeaderRight">
                    <apex:outputtext value="{0, number, ###,##0.00}">
                         <apex:param value="{!(FLOOR(crct.totalCost*100))/100}"/>
                    </apex:outputtext> 
                </apex:column>
            </apex:pageBlockTable>
            <apex:actionFunction action="{!selectAllTasks}" name="actFunselectAllCrcts" rerender="opCrctTable"/>
            <script type="text/javascript">
                var idCrctTable = '{!$Component.crctTable}';
                var selectedCrctIds = new Array();
                $('span[id$="opCrctTable"]').find("input[type=checkbox][data-val=true]").each(function(){
                    $(this).attr("checked", true);
                    var crctId = $(this).attr('data-crctid');
                    var idx = selectedCrctIds.indexOf(crctId);
                            
                    // add only if not already on list 
                    if (idx == -1){
                        selectedCrctIds.push(crctId);
                    }
                })
                if ({!bSelectAllTasks}){
                    $('span[id$="opCrctTable"]').find('input[id$="confAllId"]').attr("checked", true);
                }                        
            </script>
            <apex:inputHidden id="inSelectedCrctsList" value="{!selectedTasks}" />
            <apex:inputHidden id="inChangeOrderName" value="{!changeOrderName}" />
            <script type="text/javascript">
                $(function() {            
                     $('input[id$="inSelectCrct"]').on( "click",  function(event){
                        if ($(this).is(':checked')){
                            $(this).attr('data-val', 'true');
                            var crctId = $(this).attr('data-crctid');
                            var idx = selectedCrctIds.indexOf(crctId);
                                
                            // add only if not already on list 
                            if (idx == -1){
                                selectedCrctIds.push(crctId);
                            }
                        } else {
                            $(this).attr('data-val', 'false');
                            var crctId = $(this).attr('data-crctid');
                            var idx = selectedCrctIds.indexOf(crctId);
                            
                            // remove only if exists  on list 
                            if (idx != -1){
                                selectedCrctIds.splice(idx, 1);
                            }
                        }
                    })
                    $(esc(idCrctTable)).dataTable({
                        "bFilter": true,
                        "bLengthChange": false,
                        "iDisplayLength": 20,
                        "aoColumns": [ 
                            { "bSortable": false },
                            null,
                            null,
                            null,
                            null,
                            null,
                            null
                        ]
                    });
                })  
            </script>
        </apex:pageBlock>
        </apex:outputpanel>
    </apex:form>
    <script type="text/javascript">
        var PLACEHOLDER = 'Enter Project ID'; 
        //var selectedCrctIds = new Array();
        var sObjects;
        var queryTerm;
        $(esc(idInput)).autocomplete({
            minLength: 2,
            source: function(request, response) {
                        queryTerm = request.term;
                        PRA_ChangeOrderController.searchProject(request.term, function(result, event){
                            if(event.type == 'exception') {
                                  alert(event.message);
                            } else {
                                 sObjects = result;
                                 response(sObjects);
                            }
                        });
                   },
            focus: function( event, ui ) {
                    $(esc(idInput)).val( ui.item.Name );
                    return false;
                    },
            select: function( event, ui ) {
                        $(esc(idInput)).val( ui.item.Name );
                        $(esc(idInputId)).val( ui.item.Id );
                        
                        // Call VF JS method to select a project => rerender
                        updateSelectedProject(ui.item.Id, ui.item.Name);
                        return false;
                    },
         })
         .data( "autocomplete" )._renderItem = function( ul, item ) {
            var entry = "<a>" + item.Name;
           
            entry = entry + "</a>";
            entry = entry.replace(queryTerm, "<b>" + queryTerm + "</b>");
            return $( "<li></li>" )
                .data( "item.autocomplete", item )
                .append( entry )
                .appendTo( ul );
        };
            
        
        function getCONameAndSelectedCrcts(){
            var coName = window.prompt("Name this Change Order. Note only 80 characters including spaces are allowed.");
            if (coName != null) {
                if(coName.length > 80) {
                    alert('Character limit reached.  Only 80 characters are allowed for Change Order Name.');
                    getCONameAndSelectedCrcts();
                } else {
                    $('input[id$="inChangeOrderName"]').val(coName);
                    $.blockUI();
                    getSelectedCrcts();                 
                }
            } else {
                return null;
            }
        }
        function confirmDeleteChangeOrders(){
            var bConfirm = confirm('Are you sure you want to delete selected change orders?');
            if(bConfirm){
                $.blockUI();
            }
            return bConfirm;
        }
  
        function getSelectedCrcts(){
            $('input[id$="inSelectedCrctsList"]').val(JSON.stringify(selectedCrctIds));
        }
    
        
    </script>   
</apex:page>