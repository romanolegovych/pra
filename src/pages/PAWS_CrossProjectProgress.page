<apex:page controller="PAWS_CrossProjectProgressController" showHeader="true" sidebar="false" tabStyle="Cross_Project_Progress__tab">
    <apex:variable var="rootFolder" value="{!IF($CurrentPage.parameters.dev == 'true', 'http://localhost/flowbase/ganttdashboard', $Resource.PAWS_GanttDashboard)}" />
    <apex:variable var="noCache" value="{!IF($CurrentPage.parameters.dev == 'true', '?d=' + Substitute(Text(Now()), ' ', ''), '')}" />
    
    <script type="text/javascript">
        window.WorkRelay = {};
        WorkRelay.constants = WorkRelay.constants || {};
        WorkRelay.constants.resourcesURL = '{!URLFOR($Resource.STSWR1__workrelay_resources)}';
        WorkRelay.constants.appFolder = '{!rootFolder}';
    </script>
    
    <!-- APP CSS-->
    <apex:stylesheet value="{!rootFolder}/css/main.css{!noCache}"/>         
    <apex:stylesheet value="{!rootFolder}/js/lib/jquery-ui/jquery-ui-1.8.16.custom.css"/> 
    
    <apex:includeScript value="{!rootFolder}/js/lib/jquery-1.7.2.min.js"/>
    <apex:includeScript value="{!rootFolder}/js/lib/jquery-ui/jquery-ui-1.8.16.custom.min.js"/>
    
     <!-- APP LIB Scripts-->
    <apex:includeScript value="{!rootFolder}/js/lib/moment.min.js"/>
    <apex:includeScript value="{!rootFolder}/js/lib/moment.twix.js"/>
    <apex:includeScript value="{!rootFolder}/js/lib/xml2json.js"/> 
    
    <apex:includeScript value="{!rootFolder}/js/exportToExel.js"/>  
    
    <apex:actionStatus id="status">
        <apex:facet name="start">
            <STSWR1:ScreenLocker />
        </apex:facet>
    </apex:actionStatus>
    
    <style>
        .footerPageNavigator {background-image:url('/img/paginationArrows.gif'); background-repeat:no-repeat; height:10px; width:9px;}
        .headerColumn {}
        .centerHeaderColumn {font-weight:bold;text-decoration:none;text-align:center;}
        .wrapColumn {white-space: nowrap;overflow: hidden;text-overflow: ellipsis;}
        .filterHeaderColumn {font-weight:bold;padding-right:3px;}
        .filterValueColumn {margin-right:8px;}
    </style>
    
    <apex:form id="dashboardForm">
        <apex:outputPanel rendered="{!NOT(ObjectSpecificProgressMode)}">
            <apex:sectionHeader title="Cross Project Progress"/>

            <apex:pageBlock id="gridPanel">
                <apex:outputPanel layout="block" style="padding:3px 5px 5px 0px;">          
                    <apex:panelGrid columns="10">

                        <apex:outputPanel styleClass="filterHeaderColumn">Views:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:inputText id="tagFilterField" value="{!selectedTags}"/>
                            <input type="button" class="btn" value=">>" onclick="displayFilterDialog(true, 'Tag', '{!$Component.tagFilterField}')"/> 
                        </apex:outputPanel>

                        <apex:outputPanel styleClass="filterHeaderColumn">{!$ObjectType.ecrf__c.fields.Sponsor__c.label}:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:inputText id="sponsorFilterField" value="{!SponsorFilter}"/>
                            <input type="button" class="btn" value=">>" onclick="displayFilterDialog(true, 'Sponsor', '{!$Component.sponsorFilterField}')"/> 
                        </apex:outputPanel>
    
                        <apex:outputPanel styleClass="filterHeaderColumn">{!$ObjectType.ecrf__c.fields.Name.label}:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:inputText id="projectFilterField" value="{!ProjectFilter}"/>
                            <input type="button" class="btn" value=">>" onclick="displayFilterDialog(true, 'Project', '{!$Component.projectFilterField}')"/> 
                        </apex:outputPanel>
                        
                        <apex:outputPanel styleClass="filterHeaderColumn">Protocol ID:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:inputText id="protocolFilterField" value="{!selectedProtocols}"/>
                            <input type="button" class="btn" value=">>" onclick="displayFilterDialog(true, 'Protocol', '{!$Component.protocolFilterField}')"/> 
                        </apex:outputPanel>                        
                        
                        <apex:outputPanel styleClass="filterHeaderColumn">Country:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:inputText id="countryFilterField" value="{!selectedCountries}"/>
                            <input type="button" class="btn" value=">>" onclick="displayFilterDialog(true, 'Country', '{!$Component.countryFilterField}')"/> 
                        </apex:outputPanel>
                        
                        <apex:outputPanel styleClass="filterHeaderColumn">Status:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:selectList value="{!StatusFilter}" size="1">
                                <apex:selectOption itemValue="" itemLabel="All"/>
                                <apex:selectOption itemValue="Started" itemLabel="Started"/>
                                <apex:selectOption itemValue="Not Started" itemLabel="Not Started"/>
                            </apex:selectList>
                        </apex:outputPanel>
                        
                    </apex:panelGrid>
                    <apex:outputPanel layout="block" >
                        <apex:commandButton value="Apply Filters" action="{!applyFilters}" rerender="gridPanel, progressPanel" status="status"/>
                        <apex:commandButton value="Clear Filters" action="{!clearFilters}" rerender="gridPanel, progressPanel, dialogsPanel" status="status"/>
                        <input type="button" class="btn" value="Export" onclick="exportXml()"/> 
                    </apex:outputPanel>
                    
                    <script>    
                        function doExport(pageNumber, callback)
                        {
                            try{
                                PAWS_CrossProjectProgressController.exportData(pageNumber, "{!SponsorFilter}", "{!ProjectFilter}", "{!StatusFilter}", "{!JSENCODE(selectedTags)}", "{!JSENCODE(selectedProtocols)}", "{!JSENCODE(selectedCountries)}", "{!OrderBy}", "{!OrderType}", callback, {escape:false}); 
                            }catch(err)
                            {
                                callback(err);
                            }
                        }
                    </script>
                </apex:outputPanel>
            
                <div style="overflow-y: auto; overflow-x: hidden; height:300px; background-color:white;" onscroll="event.target.getElementsByTagName('THEAD')[0].style.transform = 'translateY(' + (event.target.scrollTop - 1) + 'px)';">
                    <apex:pageBlockTable value="{!Rows}" var="row">                     
                        <apex:column width="100" headerClass="headerColumn">
                            <apex:facet name="header">
                                <apex:outputPanel layout="block" style="cursor:pointer;">
                                    <apex:outputPanel >{!$ObjectType.ecrf__c.fields.Sponsor__c.label}</apex:outputPanel>
                                    <apex:image value="{!IF(OrderType == 'asc', '/img/colTitle_uparrow.gif', '/img/colTitle_downarrow.gif')}" style="margin-left: 5px;margin-top: 5px;position: absolute;" rendered="{!OrderBy == 'Sponsor__c'}"/>
                                
                                    <apex:actionSupport event="onclick" action="{!refresh}" rerender="dashboardForm" status="status">
                                        <apex:param name="ordBy" assignTo="{!OrderBy}" value="Sponsor__c"/>
                                        <apex:param name="ordType" assignTo="{!OrderType}" value="{!IF(AND(OrderType = 'asc', OrderBy = 'Sponsor__c'), 'desc', 'asc')}"/>
                                    </apex:actionSupport>
                                </apex:outputPanel>
                            </apex:facet>
                            
                            <apex:outputPanel layout="block"><apex:outputField value="{!row.Record['Sponsor__c']}"/></apex:outputPanel>
                        </apex:column> 
        
                        <apex:column headerClass="headerColumn" width="100">
                            <apex:facet name="header">
                                <apex:outputPanel layout="block" style="cursor:pointer;">
                                    <apex:outputPanel >{!$ObjectType.ecrf__c.fields.Name.label}</apex:outputPanel>
                                    <apex:image value="{!IF(OrderType == 'asc', '/img/colTitle_uparrow.gif', '/img/colTitle_downarrow.gif')}" style="margin-left: 5px;margin-top: 5px;position: absolute;" rendered="{!OrderBy == 'Name'}"/>
                                
                                    <apex:actionSupport event="onclick" action="{!refresh}" rerender="dashboardForm" status="status">
                                        <apex:param name="ordBy" assignTo="{!OrderBy}" value="Name"/>
                                        <apex:param name="ordType" assignTo="{!OrderType}" value="{!IF(AND(OrderType = 'asc', OrderBy = 'Name'), 'desc', 'asc')}"/>
                                    </apex:actionSupport>
                                </apex:outputPanel>
                            </apex:facet>
                            
                            <apex:outputPanel layout="block" styleClass="wrapColumn" style="width:132px;">
                                <apex:outputLink value="{!URLFOR('/' + row.Record.Id)}" title="{!row.Record['Name']}">{!row.Record['Name']}</apex:outputLink>
                            </apex:outputPanel>
                        </apex:column> 
                        
                        <apex:column headerValue="Progress" headerClass="headerColumn">
                            <apex:outputPanel layout="block" style="height:30px;display: table-cell;vertical-align: middle;" rendered="{!ISNULL(row.Instance)}">The flow is not started.</apex:outputPanel>
                            
                            <apex:outputPanel style="cursor:pointer;" rendered="{!NOT(ISNULL(row.Instance))}">
                                <c:PAWS_WorkflowProgress flowInstanceId="{!row.Instance.Id}" compactMode="true" tagFilter="{!selectedTags}" reinitFlag="{!ReinitFlag}"/>
    
                                <apex:actionSupport event="onclick" action="{!enableObjectSpecificProgressMode}" rerender="dashboardForm" status="status">
                                    <apex:param name="sfic" assignTo="{!SeletedFlowInstanceId}" value="{!row.Instance.Id}"/>
                                    <apex:param name="sp" assignTo="{!SeletedObjectId}" value="{!row.Record.Id}"/>
                                </apex:actionSupport>
                            </apex:outputPanel>
                        </apex:column> 
                    </apex:pageBlockTable>
                </div>
                
                <apex:outputPanel layout="block" style="text-align:center;padding-top: 5px;">
                    <apex:outputPanel rendered="{!RowsSetController.HasPrevious}">
                        <span><apex:commandLink action="{!RowsSetController.first}" rerender="dashboardForm" status="status"><img src="/s.gif" class="footerPageNavigator" style="background-position:0px 1px;"/></apex:commandLink></span>
                        <span><apex:commandLink action="{!RowsSetController.previous}" rerender="dashboardForm" status="status"><img src="/s.gif" class="footerPageNavigator" style="background-position:-10px 1px; margin: 0pt; padding: 0pt;"/>Previous</apex:commandLink></span>
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!NOT(RowsSetController.HasPrevious)}">
                        <span><img src="/s.gif" class="footerPageNavigator" style="background-position:0px -10px;"/></span>
                        <span style="color:#A8A8A8;"><img src="/s.gif" class="footerPageNavigator" style="background-position:-10px -10px; margin: 0pt; padding: 0pt;"/>Previous</span>
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!RowsSetController.HasNext}">
                        <span><apex:commandLink action="{!RowsSetController.next}" rerender="dashboardForm" status="status">Next<img src="/s.gif" class="footerPageNavigator" style="background-position:-17px 1px;"/></apex:commandLink></span>
                        <span><apex:commandLink action="{!RowsSetController.last}" rerender="dashboardForm" status="status"><img src="/s.gif" class="footerPageNavigator" style="background-position:-27px 1px;"/></apex:commandLink></span>
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!NOT(RowsSetController.HasNext)}">
                        <span style="color:#A8A8A8;">Next<img src="/s.gif" class="footerPageNavigator" style="background-position:-17px -10px;"/></span>
                        <span><img src="/s.gif" class="footerPageNavigator" style="background-position:-27px -10px;"/></span>
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:pageBlock>
            
            <apex:outputPanel id="dialogsPanel">
            	<script type="text/javascript">
            		var filters = new Object();
            		
			    	function getDashboardController()
			    	{
			    		return PAWS_CrossProjectProgressController;
			    	}
			    	
			    	function displayFilterDialog(show, filterName, filterFieldId)
                    {
                    	filters[filterName] = filterFieldId;
                    	
                    	var values = document.getElementById(filterFieldId).value;
                    	eval("displayDialog" + filterName + "(show, values)");
                    }
			    	
			    	function applyFilterValue(filterName, value)
	                {
	                	var elm = document.getElementById(filters[filterName]);
	                	if(elm == null) return;
	                	
	                    var oldValue = elm.value;
	                    if(oldValue.length > 0 && oldValue.charAt(value.length - 1) != ";") oldValue += ";";
	                    
	                    var values = (value ? value.split(";") : new Array());
	                    for(var i = 0; i < values.length; i++)
	                    	oldValue = oldValue.replace(values[i] + ";", "");
	                    
	                    elm.value = oldValue + value;
	                }
			    </script>

	            <apex:outputPanel >	            	
	            	<c:PAWS_CrossProjectFilterDialog name="Sponsor" applyFunction="applyFilterValue" filterFunction="getDashboardController().loadAvailableSponsors"/>
	            </apex:outputPanel>
	            
	            <apex:outputPanel >	            	
	            	<c:PAWS_CrossProjectFilterDialog name="Project" applyFunction="applyFilterValue" filterFunction="getDashboardController().loadAvailableProjects"/>
	            </apex:outputPanel>
	            
	            <apex:outputPanel >	            	
	            	<c:PAWS_CrossProjectFilterDialog name="Tag" applyFunction="applyFilterValue" filterFunction="getDashboardController().loadAvailableTags"/>
	            </apex:outputPanel>
	            
	            <apex:outputPanel >	            	
	            	<c:PAWS_CrossProjectFilterDialog name="Protocol" applyFunction="applyFilterValue" filterFunction="getDashboardController().loadAvailableProtocols"/>
	            </apex:outputPanel>
	            
	            <apex:outputPanel >	            	
	            	<c:PAWS_CrossProjectFilterDialog name="Country" applyFunction="applyFilterValue" filterFunction="getDashboardController().loadAvailableCountries"/>
	            </apex:outputPanel>
            </apex:outputPanel>
		</apex:outputPanel>

		<apex:outputPanel id="progressPanel">
	        <apex:outputPanel rendered="{!ObjectSpecificProgressMode}">
	            <apex:sectionHeader title="{!SeletedObject['Name']}" subTitle="Project Specific Progress"/>
	            
	            <apex:outputPanel layout="block" style="padding-bottom:5px;">
	                <apex:commandLink action="{!cancelObjectSpecificProgressMode}" rerender="dashboardForm" status="status">&lt;&lt; Back to Cross Project Progress</apex:commandLink>
	            </apex:outputPanel>
	            
	            <apex:pageBlock >
					<c:PAWS_WorkflowProgress flowInstanceId="{!SeletedFlowInstanceId}" compactMode="false" tagFilter="{!selectedTags}" reinitFlag="{!ReinitFlag}"/>     
				</apex:pageBlock>
	        </apex:outputPanel>
	    </apex:outputPanel>
    </apex:form>
</apex:page>