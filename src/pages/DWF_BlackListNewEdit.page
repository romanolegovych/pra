<apex:page standardController="DWF_Blacklist__c" extensions="DWF_BlackListNewEditController">
	<apex:stylesheet value="{!$Resource.PRAStyle}" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js"></script>  
	<script type="text/javascript" src=" {!URLFOR($Resource.jqueryDatatables,'DataTables/js/jquery.dataTables.min.js')}"></script>
	<link rel="stylesheet" type="text/css" href="{!URLFOR($Resource.jqueryDatatables,'DataTables/css/jquery.dataTables.css')}" />
	<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/redmond/jquery-ui.css" rel="stylesheet" type="text/css"/>
	
	<script>
		function getCompany() {
			getCompanyVO();
		}
		function getBusinessUnit() {
			getBusinessUnitVO();
		}
		function updateSystemId() {
			updateSysId();
		}
		function checkAll(cb,cbid) {
			var inputElem = document.getElementsByTagName("input");                    
			for(var i=0; i<inputElem.length; i++) {            
				if(inputElem[i].id.indexOf(cbid)!=-1){                                       
					inputElem[i].checked = cb.checked;
				}
			}
		}
		var j$ = jQuery.noConflict();
		function employeeTable() {
			j$(document).ready(function() {
				j$("Table[id$=employeeTable]").show();
				j$("Table[id$=employeeTable]").dataTable({
					"sPaginationType": "full_numbers",
					"bFilter": true,
					"iDisplayLength": 10,
					"aaSorting": [ [1,'asc'] ],
					"oLanguage": {    "sSearch": "Find  " },
					"aoColumns": [
						null,
						null,
						null,
						null,
						null,
						null
					]
				});
			});
		}
	</script>
	
	<apex:form id="newFrm">
		<apex:sectionheader title="Blacklists" subtitle="New Blacklist"/>
		<apex:pagemessages id="msgs"/>
		<apex:pageBlock title="Blacklist Edit" id="pb">
			<apex:pageBlockButtons id="pbb">
				<apex:commandButton id="saveBtn" value="Save" action="{!saveBlackList}" rerender="newFrm" />
				<apex:commandButton id="cancelBtn" value="Cancel" action="{!Cancel}" rerender="newFrm" />
			</apex:pageBlockButtons>
			<apex:pageBlockSection title="Information" columns="1" id="pbs">
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="Target System" for="system"/>
					<apex:outputPanel layout="block">
						<div class="requiredInput">
							<div class="requiredBlock"></div>
							<apex:actionFunction name="updateSysId" rerender="pbs"/>
							<apex:selectList value="{!selectedSystemId}" onchange="updateSystemId();" size="1">
								<apex:selectOptions value="{!systemNames}"/>
							</apex:selectList>							
						</div>
					</apex:outputPanel>
				</apex:pageBlockSectionItem>
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="Company Name" for="companyName"/>
					<apex:outputPanel layout="block">
						<div class="requiredInput">
							<div class="requiredBlock"></div>
							<apex:actionFunction name="getCompanyVO" action="{!getCompanyFromSelectedValue}" status="searchingCompany" 
								rerender="pbs"/>
							<apex:selectList value="{!selectedCompanyNo}" onchange="getCompany();" size="1" disabled="{!if(not(isnull(selectedSystemId)), false, true)}">
								<apex:selectOptions value="{!companyNames}" />
							</apex:selectList>
							<apex:actionStatus id="searchingCompany" startText="Searching for Company...">
								<apex:facet name="start">
									<apex:image value="/img/loading.gif" title="Searching for Company..."/>
								</apex:facet>
								<apex:facet name="stop"></apex:facet>
							</apex:actionStatus>
						</div>
					</apex:outputPanel>
				</apex:pageBlockSectionItem>
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="Business Unit Name" for="businessUnitName"/>
					<apex:outputPanel layout="block">
						<div class="requiredInput">
							<div class="requiredBlock"></div>
							<apex:actionFunction name="getBusinessUnitVO" action="{!getBusinessUnitFromSelectedValue}" status="searchingBU" 
								rerender="pbs"/>
							<apex:selectList value="{!selectedBusinessUnitName}" onchange="getBusinessUnit();" size="1" disabled="{!if(not(isnull(selectedSystemId)), false, true)}">
								<apex:selectOptions value="{!businessUnits}" />
							</apex:selectList>
							<apex:actionStatus id="searchingBU" startText="Searching for Business Unit...">
								<apex:facet name="start">
									<apex:image value="/img/loading.gif" title="Searching for Business Unit..."/>
								</apex:facet>
								<apex:facet name="stop"></apex:facet>
							</apex:actionStatus>
						</div>
					</apex:outputPanel>
				</apex:pageBlockSectionItem>
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="Employee First Name" for="employeeFirstName"/>
					<apex:inputText value="{!selectedEmpFirstName}" disabled="{!if(not(isnull(selectedSystemId)), false, true)}" />
				</apex:pageBlockSectionItem>
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="Employee Last Name" for="employeeLastName"/>
					<apex:inputText value="{!selectedEmpLastName}" disabled="{!if(not(isnull(selectedSystemId)), false, true)}" />
				</apex:pageBlockSectionItem>
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="Employee Status" for="employeeStatus"/>
					<apex:inputText value="{!selectedEmpStatus}" disabled="{!if(not(isnull(selectedSystemId)), false, true)}" />
				</apex:pageBlockSectionItem>
				<apex:pageBlockSectionItem >
					<apex:outputLabel value=""/>
					<apex:outputPanel layout="block">
						<apex:commandButton id="searchBtn" value="Search" action="{!searchEmployees}" status="searchingEmployees" 
							disabled="{!if(not(isnull(selectedSystemId)), false, true)}" rerender="newFrm" oncomplete="employeeTable()" />
						<apex:actionStatus id="searchingEmployees" startText="Searching for Employees...">
							<apex:facet name="start">
								<apex:image value="/img/loading.gif" title="Searching for Employees..."/>
							</apex:facet>
							<apex:facet name="stop"></apex:facet>
						</apex:actionStatus>
					</apex:outputPanel>
				</apex:pageBlockSectionItem>
			</apex:pageBlockSection>
			<apex:pageBlockTable id="employeeTable" value="{!employeeWrappers}" var="wrap" 
				rendered="{! not(isnull(employeeWrappers)) && (employeeWrappers.size > 0) }">
				<apex:column >
					<apex:facet name="header">
						<apex:inputCheckbox id="checkAllEmp" onclick="javascript:checkAll(this,'checkEmp');"/>
					</apex:facet>
					<apex:inputCheckbox id="checkEmp" value="{!wrap.isSelected}" />
				</apex:column>
				<apex:column value="{!wrap.employeeVO.employeeId}">
					<apex:facet name="header">
						<apex:outputLabel value="Employee ID"/>
					</apex:facet>
				</apex:column>
				<apex:column value="{!wrap.employeeVO.employeeStatus}">
					<apex:facet name="header">
						<apex:outputLabel value="Employee Status"/>
					</apex:facet>
				</apex:column>
				<apex:column value="{!wrap.employeeVO.employeeStatusDesc}">
					<apex:facet name="header">
						<apex:outputLabel value="Employee Status Desc"/>
					</apex:facet>
				</apex:column>
				<apex:column value="{!wrap.employeeVO.firstName}">
					<apex:facet name="header">
						<apex:outputLabel value="First Name"/>
					</apex:facet>
				</apex:column>
				<apex:column value="{!wrap.employeeVO.lastName}">
					<apex:facet name="header">
						<apex:outputLabel value="Last Name"/>
					</apex:facet>
				</apex:column>
			</apex:pageBlockTable>
		</apex:pageBlock>
	</apex:form>
</apex:page>