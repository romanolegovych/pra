<apex:page controller="LMS_CourseToPRAController" sidebar="false" showheader="false" tabStyle="LMS_Role_Course__c">
    <apex:stylesheet value="{!$Resource.PRAStyle}"/>
    <c:LMS_Scripts />
    <body style="background-color:transparent">
    <apex:form id="frm">   
        <apex:pageBlock title="Search Role Criteria" id="pb">
            <apex:outputPanel id="rop">
            <apex:actionStatus startText="Searching for Role..." id="searchRole">
                <apex:facet name="start">
                    <apex:image value="/img/loading.gif" title="Searching for Role..." />
                </apex:facet>
                <apex:facet name="stop"></apex:facet>
            </apex:actionStatus>
            <apex:pageBlockSection columns="1" id="pbs" title="Search Criteria">
                <apex:outputPanel >
                    <div id="topCriteria">
                        <span class="boldSpanLabel">Job Family</span>
                        <apex:selectList value="{!jobFamily}" id="slFamily" size="1" multiselect="false" tabIndex="1">
                            <apex:selectOptions value="{!jobFamilyList}"/>
                            <apex:actionSupport action="{!getTitleOnFamily}" event="onchange" rerender="slTitle" status="fetchTitle"/>
                        </apex:selectList>
                        <apex:actionStatus id="fetchTitle" startStyle="color:#ff0000;font-size:12px;font-weight:bold;padding-left:2em" startText="Fetching Job Titles...."/>
                        
                        <br/>
                        <span class="boldSpanLabel">Job Title</span>
                        <apex:selectList value="{!jobTitle}" id="slTitle" size="1" multiselect="false" tabIndex="2">
                            <apex:selectOptions value="{!jobTitleList}"/>
                            <apex:actionSupport action="{!updateJTList}" event="onchange" rerender="slTitle" status="fetchTitle"/>
                        </apex:selectList>
    
                        <br/>
                        <span class="boldSpanLabel">Business Unit</span>
                        <apex:selectList value="{!businessUnit}" id="slUnit" size="1" multiselect="false" tabIndex="3">
                            <apex:selectOptions value="{!businessUnitList}"/>
                        </apex:selectList>  
                        <apex:actionStatus id="fetchDepartment" startStyle="color:#ff0000;font-size:12px;font-weight:bold;padding-left:2em" startText="Fetching Departments...."/>
                        
                        <br/>
                        <span class="boldSpanLabel">Department</span>
                        <apex:selectList value="{!department}" id="slDepartment" size="1" multiselect="false" tabIndex="4">
                            <apex:selectOptions value="{!departmentList}"/>
                        </apex:selectList>
                        <apex:actionStatus id="fetchBU" startStyle="color:#ff0000;font-size:12px;font-weight:bold;padding-left:2em" startText="Fetching Business Units...."/>
    
                        <br/>
                        <span class="boldSpanLabel">Region</span>
                        <apex:selectList value="{!region}" id="slRegion" size="1" multiselect="false" tabIndex="5">
                            <apex:selectOptions value="{!regionList}"/>
                            <apex:actionSupport action="{!getCountryOnRegion}" event="onchange" rerender="slCountry" status="fetchCountry"/>
                        </apex:selectList>
                        <apex:actionStatus id="fetchCountry" startStyle="color:#ff0000;font-size:12px;font-weight:bold;padding-left:2em" startText="Fetching Countries...."/>
    
                        <br/>
                        <span class="boldSpanLabel">Country</span>
                        <apex:selectList value="{!country}" id="slCountry" size="1" multiselect="false" tabIndex="6">
                            <apex:selectOptions value="{!countryList}"/>
                        </apex:selectList>
    
                        <br/>
                        <span class="boldSpanLabel">Employee Type</span>
                        <apex:selectList value="{!empType}" id="slType" size="1" multiselect="false" tabIndex="7">
                            <apex:selectOptions value="{!typesList}"/>
                        </apex:selectList>
                        
                        <br/>
                        <span class="boldSpanLabel">Role Name</span>
                        <apex:inputText value="{!roleName}" styleClass="{!roleNameStyle}" style="width:300px" id="roleTextId" onclick="javascript:fnRText();return false;" onkeydown="javascript:if(event.keyCode==13)fnPRASearch();" tabIndex="8">
                            <c:AutoCompleteComp id="roleAC" objectname="LMS_Role__c" searchField="Role_Name__c" additionalFilter="{!roleFilter}" autocomplete_textbox="{!$Component.roleTextId}"/>
                        </apex:inputText>
                    </div>
                    <div class="buttonContainer">
                        <apex:commandButton value="Submit Search" id="commRSearch" onclick="javascript:fnPRASearch();return false;" rerender="pb" tabIndex="9"/>
                        <apex:commandButton value="Reset" id="commRReset" onclick="javascript:fnPRAReset();return false;" reRender="view" tabIndex="10"/>
                        <apex:actionFunction action="{!search}" name="search" status="searchRole" rerender="roleOp,cop" />
                        <apex:actionFunction action="{!roleReset}" name="rReset" status="searchRole" rerender="rop,cop"/>
                    </div>
                </apex:outputPanel>
            </apex:pageBlockSection> 
                       
            <apex:outputPanel id="roleOp">
            <apex:pageBlock rendered="{!not(isnull(roles)) && roles.size == 1 && roles[0].Status__c == 'Active'}" id="sr">
                <apex:outputLabel styleClass="lmsPbTitle" value="{!roleNameDisplay}"></apex:outputLabel>
                &nbsp;&nbsp;
                <apex:actionStatus startText="Syncing Role..." id="syncRole">
                    <apex:facet name="stop">
                        <apex:commandButton action="{!syncRoleData}" value="Sync Role" status="syncRole" rendered="{!not(isnull(roles)) && isnull(roles[0].SABA_Role_PK__c) && roles.size == 1}" rerender="roleOp"/>
                    </apex:facet>
                    <apex:facet name="start">
                        <apex:image value="/img/loading.gif" title="Syncing Role..." />
                    </apex:facet>
                </apex:actionStatus>
                <apex:pageBlockTable id="crTable" value="{!courses}" var="c" style="width:100%" border="2">
                    <apex:column styleClass="{!c.bgStyle}">
                        <apex:facet name="header">
                            <apex:inputCheckbox id="checkAllRole" value="{!allChecked}" onclick="javascript:checkAll(this,'checkRole');">
                            </apex:inputCheckbox>
                        </apex:facet>
                        <apex:inputCheckbox id="checkRole" value="{!c.selected}" styleClass="{!c.style}"/>
                    </apex:column>
                    <apex:column styleClass="{!c.bgStyle}">
                        <apex:facet name="header">
                            <apex:outputLabel value="Course Code"/>
                        </apex:facet>
                        <apex:outputLabel value="{!c.courseCode}" styleClass="{!c.style}"/>
                    </apex:column>
                    <apex:column styleClass="{!c.bgStyle}">
                        <apex:facet name="header">
                            <apex:outputLabel value="Course Title"/>
                        </apex:facet>
                        <apex:outputLabel value="{!c.courseTitle}" styleClass="{!c.style}"/>
                    </apex:column>
                    <apex:column styleClass="{!c.bgStyle}">
                        <apex:facet name="header">
                            <apex:outputLabel value="Assigned On"/>
                        </apex:facet>
                        <apex:outputLabel value="{!c.assignedOn}" styleClass="{!c.style}"/>
                    </apex:column>
                    <apex:column styleClass="{!c.bgStyle}">
                        <apex:facet name="header">
                            <apex:outputLabel value="Assigned By"/>
                        </apex:facet>
                        <apex:outputLabel value="{!c.assignedBy}" styleClass="{!c.style}"/>
                    </apex:column>
                    <apex:column styleClass="{!c.bgStyle}">
                        <apex:facet name="header">
                            <apex:outputLabel value="Duration"/>
                        </apex:facet>
                        <apex:outputLabel value="{!c.duration}" styleClass="{!c.style}"/>
                    </apex:column>
                    <apex:column styleClass="{!c.bgStyle}">
                        <apex:facet name="header">
                            <apex:outputLabel value="Target Days"/>
                        </apex:facet>
                        <apex:outputLabel value="{!c.targetDays}" styleClass="{!c.style}"/>
                    </apex:column>
                    <apex:column styleClass="{!c.bgStyle}">
                        <apex:facet name="header">
                            <apex:outputLabel value="Available From"/>
                        </apex:facet>
                        <apex:outputText value="{0,date,MMMM dd, yyyy}" styleClass="{!c.style}">
                            <apex:param value="{!c.availFrom}"/>
                        </apex:outputText>
                    </apex:column>
                    <apex:column styleClass="{!c.bgStyle}">
                        <apex:facet name="header">
                            <apex:outputLabel value="Discontinued From"/>
                        </apex:facet>
                        <apex:outputText value="{0,date,MMMM dd, yyyy}" styleClass="{!c.style}">
                            <apex:param value="{!c.disconFrom}"/>
                        </apex:outputText>
                    </apex:column>
                    <apex:column styleClass="{!c.bgStyle}">
                        <apex:facet name="header">
                            <apex:outputLabel value="Process Date"/>
                        </apex:facet>
                        <apex:outputText value="{!c.processed}" styleClass="{!c.commitStyle}" rendered="{!if(c.processed == 'Processed',true,false)}"/>
                        <apex:outputText value="{0,date,MMMM dd, yyyy}" styleClass="{!c.style}" rendered="{!if(c.processed != 'Processed',true,false)}">
                            <apex:param value="{!c.commitDate}"/>
                        </apex:outputText>
                    </apex:column>
                    <apex:column styleClass="{!c.bgStyle}">
                        <apex:facet name="header">
                            <apex:outputLabel value="Assignment Status"/>
                        </apex:facet>
                        <apex:outputLabel value="{!c.assignmentStatus}" styleClass="{!c.style}"/>
                    </apex:column>
                    <apex:column styleClass="{!c.bgStyle}">
                        <apex:facet name="header">
                            <apex:outputLabel value="Sync"/>
                        </apex:facet>
                        <apex:outputLabel value="{!c.sync}" styleClass="{!c.style}"/>
                    </apex:column>
                </apex:pageBlockTable>
                <div id="actionContainer">
                    <div id="dateButtons">
                        <apex:inputText id="date_1" value="{!commitDate}"/>
                        <apex:commandButton value="Apply Commit Date" rerender="view" disabled="{!not(not(isnull(courses)) && courses.size > 0)}" tabIndex="15" onclick="javascript:fnCheckCommitDate('checkRole');return false;"/>
                        <apex:actionFunction action="{!setCommitDate}" name="setCommitDate" status="setCommit" rerender="roleOp,courseOP,impactPanel"/>
                            <apex:actionStatus id="setCommit">
                            <apex:facet name="start">
                                <apex:image value="/img/loading.gif" title="Searching for Courses..." />
                            </apex:facet>
                            <apex:facet name="stop"></apex:facet>
                        </apex:actionStatus>
                    </div>
                    <div id="actionButtons">
                        <apex:commandButton value="Commit" action="{!commitCourses}" status="actionProcessing" rerender="roleOp,courseOP,impactPanel" disabled="{!not(not(isnull(courses)) && courses.size > 0 && not(isnull(roles[0].SABA_Role_PK__c)))}" tabIndex="11"/>
                        <apex:commandButton value="Remove" id="commRemove" action="{!removeCourses}" status="actionProcessing" disabled="{!not(not(isnull(courses)) && courses.size > 0)}" rerender="roleOp,courseOP,impactPanel" tabIndex="13"/>
                        <apex:outputLink value="/apex/LMS_ExcelCourseAssignments?rCid={!roleResultId}" rendered="{!not(isnull(courses)) && courses.size > 0}" styleClass="btn" tabIndex="14">Export</apex:outputLink>
                            <apex:actionStatus id="actionProcessing">
                            <apex:facet name="start">
                                <apex:image value="/img/loading.gif" title="Searching for Courses..." />
                            </apex:facet>
                            <apex:facet name="stop"></apex:facet>
                        </apex:actionStatus>
                    </div>
                </div>
            </apex:pageBlock>
            
            <script>
                var jq = jQuery.noConflict();
                jq(document).ready(function(){
                    jq("[id$=date_1]").datepicker({ 
                                  
                        changeMonth: true,
                        changeYear: true});
                });
            </script>
            
            <apex:pageMessage summary="{!roleErrorText}" strength="3" severity="info" rendered="{!OR(not(isnull(roles)) && roles.size == 0, not(isnull(roles)) && roles.size == 1 && isnull(roles[0].SABA_Role_PK__c), roles[0].Status__c == 'Inactive')}">
                <apex:commandButton id="roleAdd" value="Create Role" action="{!createRole}" status="addRoleStatus" rerender="roleOp,cop"
                    oncomplete="javascript:fnSendRoleData();return false;" rendered="{!bCreateRole}" disabled="false" tabIndex="16"/>
                <apex:commandButton id="roleCancel" value="Cancel" action="{!cancelCreateRole}" status="addRoleStatus" rerender="pb"
                    rendered="{!bCreateRole}" disabled="false" tabIndex="17"/>
                <apex:actionStatus id="addRoleStatus">
                    <apex:facet name="start">
                        <apex:image value="/img/loading.gif" title="Searching for Courses..." />
                    </apex:facet>
                    <apex:facet name="stop"/>
                </apex:actionStatus>
            </apex:pageMessage>
            <apex:actionFunction action="{!sendRoleData}" name="sendRoleData" status="addRoleStatus" rerender="roleOp,cop"/>
            
            <apex:pageMessage summary="{!webServiceError}" strength="3" severity="info" rendered="{!bWebServiceError}">
                <apex:commandLink action="{!showErrors}" title="view" target="_blank"
              rendered="{!or(addErrors.size > 0, createErrors.size > 0, deleteErrors.size > 0, calloutErrors.size > 0, 
              sfdcErrors.size > 0 )}">Error Log</apex:commandLink>
            </apex:pageMessage>
            </apex:outputPanel>
            </apex:outputPanel>
        </apex:pageBlock>
        
        <apex:pageBlock id="pbc" title="Course Management">
            <apex:outputPanel id="cop">
            <apex:actionStatus startText="Searching for Courses..." id="searchCourse">
                <apex:facet name="start">
                    <apex:image value="/img/loading.gif" title="Searching for Courses..." />
                </apex:facet>
                <apex:facet name="stop"></apex:facet>
            </apex:actionStatus>
            <apex:pageBlockSection id="cpbs" columns="1" title="Search Criteria">
                <apex:outputPanel >
                <div id="bottomCriteria">
                    <div id="viewImpact">
                        <apex:outputPanel id="impactPanel" rendered="{!not(isnull(roles)) && roles.size == 1 && roles[0].Status__c == 'Active'}">
                        <table border="0" cellspacing="0" cellpadding="0" class="list">
                            <tr class="headerRow">
                                <th class="headerRow">Impact</th>
                                <th class="headerRow">Employee Count</th>
                                <th class="headerRow">Total Hours</th>
                            </tr>
                            <tr>
                                <td>Current</td>
                                <td align="center">{!employeeCount}</td>
                                <td align="center">{!impactStart}</td>
                            </tr>
                            <tr>
                                <td>Change</td>
                                <td align="center">-</td>
                                <td align="center" style="color:red">{!impactChange}</td>
                            </tr>
                            <tr>
                                <td><apex:outputLink value="/apex/LMS_ViewCurrentImpact?id={!roleResultId}" title="view" target="_blank" style="color:blue">Total</apex:outputLink></td>
                                <td align="center">{!employeeCount}</td>
                                <td align="center">{!impactEnd}</td>
                            </tr>
                        </table>
                        </apex:outputPanel>
                    </div>
                    <span class="boldSpanLabel">Course Type</span>
                    <apex:selectList value="{!courseType}" id="slCourseId" size="1" multiselect="false" tabIndex="11">
                        <apex:selectoptions value="{!courseTypes}"/>
                        <apex:actionSupport action="{!searchType}" event="onchange" reRender="aC,cAcPackage"/>
                    </apex:selectList>
    
                    <br/>
                    
                    <span class="boldSpanLabel">Course Title</span>
                    <apex:inputText value="{!courseText}" styleClass="{!courseTextStyle}" style="width:300px" id="courseTextId" disabled="{!not(not(isnull(roles)) && roles.size == 1)}" onkeydown="javascript:if(event.keyCode==13)fnCSearch();" onclick="javascript:fnCText();return false;" tabIndex="12">
                        <c:AutoCompleteComp id="aC" objectname="LMS_Course__c" searchField="Title__c" additionalFilter="{!searchFilter}" autocomplete_textbox="{!$Component.courseTextId}"/>
                    </apex:inputText>
                    
                    <br/>
                    
                    <span class="boldSpanLabel">Course Package</span> 
                    <apex:inputText value="{!courseTextPackage}" styleClass="{!courseTextStylePackage}" style="width:300px" id="courseTextPackageId" disabled="{!not(not(isnull(roles)) && roles.size == 1)}" onkeydown="javascript:if(event.keyCode==13)fnCSearch();" onclick="javascript:fnCPackageText();return false;" tabIndex="13">
                    <c:AutoCompleteFieldComp id="cAcPackage" objectname="LMS_Course__c" searchField="Course_Package__c" additionalFilter="{!searchFilterPackage}" autocomplete_textbox="{!$Component.courseTextPackageId}" isFieldSearch="true"/>
                    </apex:inputText>
                    <br/>
                    
                    <span class="boldSpanLabel">Updated Recently</span>
                    <apex:inputCheckbox value="{!blnUpdated}" id="updatedId" tabIndex="14"/>Updated in Last 30 days
                    
                    <br/>
                    <span class="boldSpanLabel">Updated From</span>
                    <apex:inputText id="date_2" value="{!courseUpdateStart}" tabIndex="15"/>
    
                    <br/>
                    <span class="boldSpanLabel">To</span>
                    <apex:inputText id="date_3" value="{!courseUpdateEnd}" tabIndex="16"/>
                </div>
                <div class="buttonContainer">
                    <apex:commandButton value="Submit Search" id="commCSearch" onclick="javascript:fnCSearch();return false;" disabled="{!not(not(isnull(roles)) && roles.size == 1)}" rerender="cop" tabIndex="17"/>
                    <apex:commandButton value="Reset" id="commCReset" onclick="javascript:fnCReset();return false;" disabled="{!not(not(isnull(roles)) && roles.size == 1)}" rerender="cop" tabIndex="18"/>
                    <apex:actionFunction action="{!courseSearch}" name="courseSearch" status="searchCourse" rerender="courseOP"/>
                    <apex:actionFunction action="{!courseReset}" name="cReset" status="searchCourse" rerender="cop"/>
                </div>
                </apex:outputPanel>
            </apex:pageBlockSection>
            
            <script>
                var jq = jQuery.noConflict();
                jq(document).ready(function(){
                    jq("[id$=date_2]").datepicker({
                        changeMonth: true,
                        changeYear: true});
                    jq("[id$=date_3]").datepicker({
                        changeMonth: true,
                        changeYear: true});
                });
            </script>
            
            <apex:outputPanel id="courseOP">
            <apex:pageBlock id="pbCourse" title="Search Results" rendered="{!not(isnull(selCourses)) && selCourses.size > 0}">
                <apex:pageBlockButtons >
                    <apex:actionStatus startText="Saving as Draft..." id="addCourse">
                        <apex:facet name="stop">
                            <apex:commandButton value="Save as Draft" action="{!addCourse}" status="addCourse" rerender="roleOp,courseOP,impactPanel"/>
                        </apex:facet>
                        <apex:facet name="start">
                            <apex:image value="/img/loading.gif" title="Saving Courses as Draft..." />
                        </apex:facet>
                    </apex:actionStatus>
                    <apex:commandButton value="Close" action="{!closeCourseSearch}" oncomplete="javascript:fnCReset();" rerender="cop,courseTextId"/>
                </apex:pageBlockButtons>
                <apex:pageBlockTable id="courseTable" value="{!selCourses}" var="c" rowClasses="oddRow,evenRow" style="width:100%">
                    <apex:column >
                        <apex:facet name="header">
                            <apex:inputCheckbox id="checkAll" value="{!allCheckedCourse}" onclick="javascript:checkAll(this,'checkCourse');">
                            </apex:inputCheckbox>
                        </apex:facet>
                        <apex:inputCheckbox id="checkCourse" value="{!c.selected}">
                            <apex:actionSupport event="onclick" action="{!viewImpact}" rerender="impactPanel"/>
                        </apex:inputCheckbox>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">
                            <apex:outputLabel value="Course Code"/>
                        </apex:facet>
                        <apex:outputLabel value="{!c.courseCode}"/>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">
                            <apex:outputLabel value="Course Title"/>
                        </apex:facet>
                        <apex:outputLabel value="{!c.courseTitle}"/>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">
                            <apex:outputLabel value="Duration"/>
                        </apex:facet>
                        <apex:outputLabel value="{!c.duration}"/>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">
                            <apex:outputLabel value="Target Days"/>
                        </apex:facet>
                        <apex:outputLabel value="{!c.targetDays}"/>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">
                            <apex:outputLabel value="Available From"/>
                        </apex:facet>
                        <apex:outputText value="{0,date,MMMM dd, yyyy}">
                            <apex:param value="{!c.available}"/>
                        </apex:outputText>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">
                            <apex:outputLabel value="Discontinued From"/>
                        </apex:facet>
                        <apex:outputText value="{0,date,MMMM dd, yyyy}">
                            <apex:param value="{!c.discontinued}"/>
                        </apex:outputText>
                    </apex:column>
                    <apex:column >
                        <apex:facet name="header">
                            <apex:outputLabel value="Course Status"/>
                        </apex:facet>
                        <apex:outputLabel value="{!c.courseStatus}"/>
                    </apex:column>
                </apex:pageBlockTable>
            </apex:pageBlock>
            <apex:pageMessage summary="{!courseErrorText}" strength="3" severity="info" rendered="{!not(isnull(selCourses)) && OR(selCourses.size > 1000, selCourses.size == 0)}"/>
            </apex:outputPanel>
            </apex:outputPanel>
        </apex:pageBlock>
    </apex:form>
    </body>
</apex:page>