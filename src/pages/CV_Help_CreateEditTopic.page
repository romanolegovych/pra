<apex:page StandardController="Help_Record__c" extensions="CV_Help_CreateEditTopicCtrl" standardStylesheets="true">
    <apex:includeScript value="{!URLFOR($Resource.helpAndTraining, 'jquery-1.11.3.min.js')}" />
    <apex:includeScript value="{!URLFOR($Resource.helpAndTraining, 'CV_Help_CreateEditTopic.js')}" />
    <style>
        .pageSelect{
            width: 100%;
        }
        .multiPickButtonCell{
            vertical-align: middle;
        }
        .tdWithScroll div{
            height: 120px;
            max-height: 120px;
            overflow: auto;
        }        
        .tdWithScroll{
            border: solid 1px;
            border-width: 1px;
            border-color: grey;
        }
    </style>
    
    <apex:pageMessages id="idPageMessages"/>
    
    <apex:form >
    
        <apex:pageBlock title="Help Record" mode="edit">
        
            <apex:pageBlockSection title="Help Record details" columns="1">
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Label" for="idInputLabel"/>
                    <apex:inputText id="idInputLabel" value="{!record.Label__c}" style="width:100%;" />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Active" for="idInputActive"/>
                    <apex:inputField id="idInputActive" value="{!record.Active__c}" />
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Order" for="idOrder"/>
                    <apex:inputText id="idOrder" value="{!record.Order__c}" style="width:100%;" />
                </apex:pageBlockSectionItem>
                        
                <apex:pageBlockSectionItem labelTitle="Related Pages">
                    <apex:outputLabel value="Related Pages" for="idRelatedPagesPanel"/>
                    <apex:outputPanel id="idRelatedPagesPanel">
            
                        <table><tr> 
                            <td>
                                <apex:outputLabel value="Available Pages:" for="idAvailablePages"/>
                                <apex:selectList id="idAvailablePages" multiselect="true" size="12" styleClass="pageSelect" >
                                    <apex:selectOptions value="{!allPages[0]}"/>
                                    <apex:selectOptions value="{!allPages[1]}"/>
                                    <apex:selectOptions value="{!allPages[2]}"/>
                                    <apex:selectOptions value="{!allPages[3]}"/>
                                    <apex:selectOptions value="{!allPages[4]}"/>
                                </apex:selectList>
                            </td>
                            <td class="multiPickButtonCell" style="vertical-align: middle; text-align:center;">
                
                                <apex:outputPanel layout="block" styleClass="text">Add</apex:outputPanel>
                                 
                                <apex:outputPanel layout="block" styleClass="text">
                                  <apex:outputLink value="javascript:addToSelected();"
                                    id="btnRight">
                                    <apex:image value="/s.gif" alt="Add" styleClass="rightArrowIcon"
                                      title="Add" />
                                  </apex:outputLink>
                                </apex:outputPanel>
                                
                                <apex:outputPanel layout="block" styleClass="text">
                                  <apex:outputLink value="javascript:removeFromSelected();"
                                    id="btnLeft">
                                    <apex:image value="/s.gif" alt="Remove"
                                      styleClass="leftArrowIcon" title="Remove" />
                                  </apex:outputLink>
                                </apex:outputPanel>
                                
                                <apex:outputPanel layout="block" styleClass="duelingText">Remove</apex:outputPanel>
                                
                            </td>
                            <td>
                                <apex:outputLabel value="Selected Pages:" for="idSelectedPages"/>
                                <apex:selectList id="idSelectedPages" multiselect="true" size="12" styleClass="pageSelect" >
                                    <apex:selectOptions value="{!selectedPagesAsOptions}"/>
                                </apex:selectList>
                            </td>
                        </tr></table>
                        
                    </apex:outputPanel>
                    
                </apex:pageBlockSectionItem>
                
                <apex:pageBlockSectionItem >
                    <apex:outputLabel value="Content" for="idInputContent"/>
                    <apex:inputField id="idInputContent" value="{!record.Rich_Html_Content__c}" />
                </apex:pageBlockSectionItem>
            
                <!--br/>Profiles:<br/> 
                <apex:selectList value="{!selectedProfiles}" multiselect="true" size="8">
                    <apex:selectOptions value="{!allProfiles}"/>
                </apex:selectList><br/-->
        
                <!--br/>Objects:<br/>
                <apex:selectList value="{!selectedSObjects}" multiselect="true" size="5" onChange="refreshRecordTypes();">
                    <apex:selectOptions value="{!allSObjects}"/>
                </apex:selectList><br/>
                
                <br/>Record Types:<br/>
                <apex:outputPanel id="idRecordTypesPanel">
                    <apex:selectList value="{!selectedRecordTypes}" multiselect="true" size="4">
                        <apex:selectOptions value="{!recordTypesForSObject}"/>
                    </apex:selectList><br/>
                </apex:outputPanel-->
                <br/>
                
                <apex:inputHidden id="idSelectedPagesAsString" value="{!selectedPagesAsString}" />
    
            </apex:pageBlockSection>
            
            <apex:pageBlockButtons >
                <apex:outputPanel id="idButtons">
                    <apex:commandButton value="Save" id="idSaveButton" onClick="doSave();return false;" />
                    <apex:commandButton value="Quick Save" id="idQuickSaveButton" onClick="doQuickSave();return false;" />
                    <apex:commandButton value="Clone" id="idCloneButton" onClick="doCloneRecord();return false;" disabled="{! !isCloneDeleteAvailable}"/>
                    <apex:commandButton value="Delete" id="idDeleteButton" onClick="doDeleteRecord();return false;" disabled="{! !isCloneDeleteAvailable}" />
                    <apex:commandButton value="Cancel" id="idCancelButton" onClick="doCancel();return false;" />
                </apex:outputPanel>
            </apex:pageBlockButtons>
    
    
            <apex:actionFunction name="refreshRecordTypes" action="{!refreshRecordTypes}" rerender="idPageMessages,idRecordTypesPanel,idLabelPanel,idCloneButton" />
            <apex:actionFunction name="saveAction" action="{!save}" />
            <apex:actionFunction name="quickSaveAction" action="{!quickSave}" rerender="idPageMessages,idContentPreview,idButtons" />
            <apex:actionFunction name="cloneAction" action="{!cloneRecord}" />
            <apex:actionFunction name="deleteAction" action="{!deleteRecord}" />
            <apex:actionFunction name="cancelAction" action="{!cancel}" />
            
        </apex:pageBlock>

    </apex:form>
    
</apex:page>