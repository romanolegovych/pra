<apex:page showHeader="true" sidebar="false" cache="false" standardStylesheets="true" standardController="Critical_Chain__c" extensions="CCController,CCApi">
	<apex:sectionHeader title="Critical Chain" subtitle="{!Critical_Chain__c.Name}"/>
	<apex:form >
		<apex:pageBlock mode="edit">
			<apex:pageBlockButtons >
				<apex:commandButton value="Save" action="{!save}"/>
				<apex:commandButton value="Cancel" action="{!cancel}"/>
			</apex:pageBlockButtons>
			<apex:pageBlockSection title="Details">
				<apex:inputField value="{!Critical_Chain__c.Name}"/>
				<apex:inputField value="{!Critical_Chain__c.Flow__c}"/>
				<apex:inputField value="{!Critical_Chain__c.Milestone__c}"/>
			</apex:pageBlockSection>
			
			<apex:pageBlockSection rendered="false" title="Critical Chain Steps" columns="1" id="stepsSection" collapsible="false">
				<apex:pageMessage severity="info" title="Critical Path" detail="{!criticalPath}"/>
				<apex:outputPanel >
					<apex:actionStatus id="loading">
						<apex:facet name="start">
							<span style="color:red;">&nbsp;loading, please wait...</span>
						</apex:facet>
						<apex:facet name="stop">
							<span>&nbsp;</span>
						</apex:facet>
					</apex:actionStatus>
					<apex:pageBlockTable value="{!chainSteps}" var="chainStep">
						<apex:column headerValue="" width="50">
							<apex:facet name="footer">
								<apex:commandLink value="Add" action="{!addStep}" rerender="stepsSection" status="loading"/>
							</apex:facet>
							<apex:commandLink value="Remove"  action="{!deleteStep}" rerender="stepsSection" status="loading">
								<apex:param name="selectedStep" value="{!chainStep.Id}" assignTo="{!selectedStep}"/>
							</apex:commandLink>
						</apex:column>
						<apex:column value="{!chainStep.Flow_Step__r.STSWR1__Flow__r.Name}" headerValue="{!$ObjectType.STSWR1__Flow__c.Fields.Name.Label}">
							<apex:facet name="footer">
								<apex:selectList value="{!selectedFlow}" multiselect="false" size="1" onchange="refresh();">
									<apex:selectOptions value="{!flowOptions}"/>
								</apex:selectList>
							</apex:facet>
						</apex:column>
						<apex:column value="{!chainStep.Flow_Step__r.Name}" headerValue="{!$ObjectType.STSWR1__Flow_Step_Junction__c.Fields.Name.Label}">
							<apex:facet name="footer">
								<apex:selectList value="{!newJunction.Flow_Step__c}" multiselect="false" size="1" disabled="{!selectedFlow == NULL}">
									<apex:selectOptions value="{!stepOptions}"/>
								</apex:selectList>
							</apex:facet>
						</apex:column>
					</apex:pageBlockTable>
				</apex:outputPanel>
			</apex:pageBlockSection>
		</apex:pageBlock>
		
		<apex:actionFunction name="refresh" action="{!refresh}" rerender="stepsSection" status="loading"/>
	</apex:form>
	
    <apex:variable var="rootFolder" value="{!IF($CurrentPage.parameters.dev == 'true', 'http://localhost/flowbase/PAWS_cctree', $Resource.PAWS_cctree)}" />
    
            <script type="text/javascript">
                window.WorkRelay = window.WorkRelay || {};
                WorkRelay.constants = WorkRelay.constants || {};
                WorkRelay.constants.resourcesURL = '{!URLFOR($Resource.STSWR1__workrelay_resources)}';
                
                window.chainID = "{!JSENCODE(chain.Id)}";
            </script>    
            
            <meta name="apple-mobile-web-app-capable" content="yes"/>
            <meta name="viewport" content="width=device-width, minimum-scale=1.0"/>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        
<!-- APP CSS-->
            
            <apex:stylesheet value="{!rootFolder}/common_resources/css/common.css"/>
            <apex:stylesheet value="{!rootFolder}/common_resources/css/grid.css"/>
            <apex:stylesheet value="{!rootFolder}/common_resources/css/media.css"/>                        
            <apex:stylesheet value="{!rootFolder}/css/main.css"/>
            <apex:stylesheet value="{!rootFolder}/common_resources/plugin/jquery-layout/jquery.layout.css"/>
            <apex:stylesheet value="{!rootFolder}/common_resources/plugin/jquery-ui/jquery-ui-1.8.16.custom.css"/>
            <apex:stylesheet value="{!rootFolder}/common_resources/plugin/gritter/jquery.gritter.css"/>
            
            <apex:stylesheet value="{!rootFolder}/common_resources/components/toolbar/css/style.css"/>                                                

            <!-- Main lib Scripts-->
            <apex:includeScript value="{!rootFolder}/common_resources/js/jquery-1.7.2.min.js"/>       
            <apex:includeScript value="{!rootFolder}/common_resources/js/Class.js"/>       
            <apex:includeScript value="{!rootFolder}/common_resources/plugin/jquery-ui/jquery-ui-1.8.16.custom.min.js"/>       
            <apex:includeScript value="{!rootFolder}/common_resources/js/knockout-latest.debug.js"/>             
            <apex:includeScript value="{!rootFolder}/common_resources/plugin/gritter/jquery.gritter.min.js"/>                                                       
            <apex:includeScript value="{!rootFolder}/common_resources/js/user-notification-manager.js"/>    
            <apex:includeScript value="{!rootFolder}/common_resources/components/toolbar/js/Toolbar.js"/>                                                                                                                                                                                  
                        
            <apex:includeScript value="{!rootFolder}/common_resources/plugin/jquery-layout/jquery.layout.js"/>            
            
            
        
            <!-- APP Scripts-->
            <apex:includeScript value="{!rootFolder}/gui/ServerModel.js"/>
            <apex:includeScript value="{!rootFolder}/gui/Application.js"/>
            <apex:includeScript value="{!rootFolder}/gui/ViewModel/EditorViewModel.js"/>

           

        <script type="text/javascript">

                
                String.format = function () {
                    var s = arguments[0];
                    for (var i = 0; i < arguments.length - 1; i++) {
                        var reg = new RegExp("\\{" + i + "\\}", "gm");
                        s = s.replace(reg, arguments[i + 1]);
                    }
                    return s;
                }
        
                var app;
                $(document).ready(function () {
                    $.get('{!rootFolder}/' + "app.html",function(r){
                        $("#content").append(r);
                        $("#canvas").css("visibility", "hidden");
                        setTimeout(function(){
                            app = new CriticalTreeApp.Application();
                        },0);
                     })
                });

            </script>    

        <apex:insert name="mini_header_container">
            <div id="miniheader">
                <apex:insert name="header_container">
                    <div id="blackbox" class="clearfix logo">
                        <div class="headerBody">
                              <apex:insert name="header"/>
                        </div>
                    </div>
          
                </apex:insert>
                <apex:insert name="toolbar_container">
                <div id="wr_toolbar">
                    
                </div>
                </apex:insert>
            </div>
        </apex:insert>
                
        <div id="content" >
    </div>
	
</apex:page>