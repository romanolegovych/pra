<apex:page controller="BDT_LaboratoryListsController" showHeader="false">
	<apex:composition template="BDT_GeneralTemplate">
		<apex:define name="MainContent">
			<apex:form >
				<apex:actionStatus startText="Communicating with server, please wait..." layout="block" startStyleClass="BDTbusy" id="busy"/>
				<apex:outputPanel id="mainPanel" layout="block" rendered="{!NOT(addEditFlag)}"> 
					<!-- List of Lab lists -->
					<div class="BDT_Container_SideBySide">
						<c:BDT_PageBlockHeader sectionHeading="Lab Parameters"/>
						<apex:dataTable value="{!listsNames}" var="ln" width="100%" styleClass="BDT_Table"> 
							<apex:column headerValue="Parameter Name">
								<apex:commandLink action="{!getListValues}" value="{!ln}" rerender="resultPanel" status="busy">
									<apex:param name="selListName" value="{!ln}"  assignTo="{!selectedListName}"/> 
								</apex:commandLink>
							</apex:column>
						</apex:dataTable>
					</div>
					<!-- Values of selected list -->
					<apex:outputPanel id="resultPanel" layout="block"  rendered="{!securitiesA.ReadPriv}">
						<div class="BDT_Container_SideBySide">
							<c:BDT_PageBlockHeader sectionHeading="Select a list to view the values." rendered="{!noSelection}"/>
							<apex:outputPanel rendered="{!NOT(noSelection)}">
								<div class="BDT_PageBlockHeader">
									<h2>{!selectedListName}</h2>
									<apex:commandLink styleclass="BDT_BlueButton" action="{!addSelectedValue}" value="Add" rendered="{!securitiesA.CreatePriv}">
										<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/new_icon.png')}"/>
									</apex:commandLink>
								</div>
							</apex:outputPanel>
							<apex:variable var="IndexOfRow" value="{!0}"/>
							<apex:dataTable value="{!selectedLabList}" var="listVar" width="100%" styleClass="BDT_Table">
								<apex:column headerValue="{!firstColName}" >
									<apex:outputfield value="{!listVar.listFirstValue__c}"/>
								</apex:column>
								<apex:column headerValue="{!secondColName}" rendered="{!secondColFlag}">
									<apex:outputfield value="{!listVar.listSecondvalue__c}"/>
								</apex:column>
								<apex:column rendered="{!securitiesA.UpdatePriv}">
									<apex:commandLink action="{!EditSelectedValue}" styleClass="BDT_EditLink">
										<apex:param name="valIndex" value="{!IndexOfRow}" assignTo="{!selectedValueIndex}"/>
									</apex:commandLink>
									<apex:variable var="IndexOfRow" value="{!IndexOfRow + 1}"/>
								</apex:column>
							</apex:dataTable>
						</div>
					</apex:outputPanel>
				</apex:outputPanel>
				<!-- Add/Edit a list value -->
				<apex:outputPanel id="addEditPanel" layout="block" rendered="{!addEditFlag}"> 
					<div class="BDT_Container">
						<c:BDT_PageBlockHeader sectionHeading="New {!selectedListName} value" rendered="{!addFlag && addEditFlag}"/>
						<c:BDT_PageBlockHeader sectionHeading="Edit {!selectedListName} value" rendered="{!NOT(addFlag) && addEditFlag}"/>
						<div class="cPbBody">
							<apex:panelGrid columns="2" styleClass="BDT_EditTable">
								<apex:outputText value="{!firstColName}" />
								<apex:inputText value="{!labSelListItem.listFirstValue__c}" style="Width:300px"/> 
								<apex:outputText value="{!secondColName}" rendered="{!secondColFlag}"/>
								<apex:inputText value="{!labSelListItem.listSecondvalue__c}" style="Width:300px" rendered="{!secondColFlag}"/>
							</apex:panelGrid>
						</div>
						<div class="BDT_PageBlockFooter">
							<table cellspacing="0" cellpadding="0" border="0" >
								<tr >
									<td class="cPbButton" >
										<apex:commandLink styleClass="BDT_BlueButton" value="Cancel" action="{!cancelvalue}">
											<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/cancel-icon.png')}"/>
										</apex:commandLink>
									</td>
									<td class="cPbButton" >
										<apex:commandLink styleClass="BDT_BlueButton" value="Save" action="{!saveValue}">
											<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/save.png')}"/>
										</apex:commandLink>
									</td>
									<td class="cPbButton" >
										<apex:commandLink styleClass="BDT_RedButton" value="Delete" action="{!deleteValue}" rendered="{!NOT(addFlag) && securitiesA.DeletePriv}">
											<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/delete-icon.png')}"/>
										</apex:commandLink>
									</td>
								</tr>
							</table>
						</div>
			 		</div>
				</apex:outputPanel>
			</apex:form>
		</apex:define>
	</apex:composition> 
</apex:page>