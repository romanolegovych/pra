<apex:page sidebar="false" showHeader="false" controller="PBB_Mobile_Cont" docType="html-5.0">
<apex:stylesheet value="{!URLFOR($Resource.PRA_PBB_DataTables_1_10_7, 'DataTables-1.10.7/media/css/jquery.dataTables.css')}"/>
<!-- <apex:stylesheet value="{!URLFOR($Resource.PRA_PBB_styles, 'styles/fonts/styles.css')}"/> -->
<style>
    @font-face {
        font-family: "Gotham Pro Regular";
        src: url('{!URLFOR($Resource.PRA_PBB_styles, 'styles/fonts/GothaProReg.otf')}');
    }
    @font-face {
        font-family: "Gotham Pro Medium";
        src: url('{!URLFOR($Resource.PRA_PBB_styles, 'styles/fonts/GothaProMed.otf')}');
    }
    @font-face {
        font-family: "Heroic Regular";
        src: url('{!URLFOR($Resource.PRA_PBB_styles, 'styles/fonts/HeroicCondensed-Regular.otf')}');
    }

    .main-page {
        width: 80%;
        margin-bottom: 63px;
        position: relative;
        overflow: hidden;
    }
    .page table.dataTable.dataTable {
        border: none;
    }
    .page table.dataTable thead td {
        border-bottom: none;
    }
    .page table.dataTable > thead th {
        border-left: none !important;
        border-top: none !important;
        text-align: left;
    }
    .page table.dataTable > thead th:last-child {
        border-right: none !important;
    }
    .dataTables_wrapper {
        width: auto !important;
    }
    table.dataTable tbody tr {
        background: transparent !important;
    }
    .dataTable tbody tr td {
        padding: 7px 10px;
        font-family: "Gotham Pro Regular" !important;
    }
    table.dataTable thead tr td {
        padding: 7px 10px;
    }
    .dataTable tr td:first-child {
        border-left: 5px solid transparent;
        font-family: "Gotham Pro Medium" !important;
    }
    .dataTable tr.on-track    td:first-child, .summary tr.on-track    td:first-child { border-left-color: #09b239; }
    .dataTable tr.at-risk     td:first-child, .summary tr.at-risk     td:first-child { border-left-color: #ffb600; }
    .dataTable tr.delayed     td:first-child, .summary tr.delayed     td:first-child { border-left-color: #e20303; }
    .dataTable tr.not-started td:first-child, .summary tr.not-started td:first-child { border-left-color: #d9d9d9; }
    .dataTable tr.completed   td:first-child, .summary tr.completed   td:first-child { border-left-color: #1875a8; }

    .dataTable tbody tr.odd td {
        background-color: #404040 !important;
    }
    .dataTable tbody tr.even td {
        background-color: #303030 !important;
    }
    .dataTable tbody tr.shown + tr > td {
        border-top: none;
        border-bottom: none;
        border-left: none;
    }
    
    /* Cumulative Activation */
    .left, 
    .right {
        width: 50%;
        display: inline-block;
        vertical-align: top;
    }
    .left {
        max-height: 478px;
        overflow: auto;
    }
    .right {
        height: 478px;
        border: none;
        box-sizing: border-box;
    }
    #tableActivation tbody tr td {
        padding: 11px 10px;
    }
    .summary {
        background: #2a2a2a;
        width: 100%;
        padding: 15px;
    }
    .summary tr td:first-child {
        border-left: 5px solid transparent;
    }  
    .summary td {
        padding: 8px;
        color: white;
    }
    .summary td.marked input {
        color: #e71687;
        background-color: #2a2a2a;
        height: 20px;
        width: 120px;
    }
    .summary td:first-child {
        font-family: "Gotham Pro Regular" !important;
    }
    /* Overview by country */
    #tableOverviewByCountry.dataTable tbody tr td {
        padding: 7px 10px;
    }
    #tableOverviewByCountry .total td {
        padding: 7px 10px;
    }
        /***** Progress bar *****/
        .progress-bar-wrapper { 
            height: 20px;
            width: 50px;
            display: inline-block;
            vertical-align: sub;
            margin-right: 5px;
        }
        .progress-bar {
            height: 100%;
        }
        /***** / Progress bar *****/
    .color-on-track { background-color: #09b239; }
    .color-at-risk  { background-color: #ffb600; }
    .color-delayed  { background-color: #e20303; }
    /***** / Overview by country *****/
    
    /* fix 1st row of scrollable table */ 
    .dataTables_scrollBody table.dataTable thead th {
        padding: 0 10px !important;
    }
    
    .page .dataTables_wrapper.no-footer .dataTables_scrollBody {
        border-bottom: none;
    }
    
    main.main-page {
        margin-bottom: 0;
    }

</style>
<apex:stylesheet value="{!$Resource.PRA_PBB_CSS_Temp}"/>

<apex:includeScript value="{!URLFOR($Resource.PRA_PBB_DataTables_1_10_7, 'DataTables-1.10.7/media/js/jquery.js')}"  />
<apex:includeScript value=" {!URLFOR($Resource.PRA_PBB_DataTables_1_10_7,'DataTables-1.10.7/media/js/jquery.dataTables.min.js')}"/>
<apex:includeScript value=" {!URLFOR($Resource.PRA_PBB_DT_Scroller,'Scroller-1.2.2/js/dataTables.scroller.js')}"/>
<script>
// fix for blank page when you tap on an input field or scroll the page.
(function(){try{var a=navigator.userAgent;
if((a.indexOf('Salesforce')!=-1)&&(a.indexOf('iPhone')!=-1||a.indexOf('iPad')!=-1)&&(a.indexOf('OS/8')!=-1||a.indexOf('OS 8')!=-1)&&(a.indexOf('Safari')==-1)){
var s=document.createElement('style');
s.innerHTML="html,html body{overflow: auto;-webkit-overflow-scrolling:touch;}body{position:absolute;left:0;right:0;top:0;bottom:0;}";
document.getElementsByTagName('head')[0].appendChild(s);}}catch(e){}})();
</script>

<apex:form >
    <c:PBB_Header_Component title="{!$Label.PBB_Mobile_3_Study_Start_up}"/>

    <c:PBB_Left_Menu_Component />
    <main class="main-page">
        <div class="page hidden">
            <table id="tableOverviewByCountry" class="dataTable" data-sizeY="402">
                <thead>
                    <tr>
                        <th>{!$Label.PBB_Mobile_4_5_7_Country}</th>
                        <th>{!$Label.PBB_Mobile_4_Num_of_Sites}</th>
                        <th>{!$Label.PBB_Mobile_4_Active}</th>
                        <th>{!$Label.PBB_Mobile_4_On_Track}</th>
                        <th>{!$Label.PBB_Mobile_4_At_Risk}</th>
                        <th>{!$Label.PBB_Mobile_4_Delayed}</th>
                    </tr>
                </thead>
                <thead class="total">
                    <tr>
                        <td>{!$Label.PBB_Mobile_4_Total}</td>
                        <td><apex:outputText value="{!countrySummary['NumberOfSites']}"/></td>
                        <td><apex:outputText value="{!countrySummary['NumberOfSitesCompleted']}"/></td>
                        <td>
                            <div class="progress-bar-wrapper">
                                <div class="progress-bar color-on-track" style="width: 100%"></div>
                            </div>
                            <apex:outputText value="{!countrySummary['NumberOfSitesOnTrack']}"/>
                        </td>
                        <td>
                            <div class="progress-bar-wrapper">
                                <div class="progress-bar color-at-risk" style="width: 100%"></div>
                            </div>
                            <apex:outputText value="{!countrySummary['NumberOfSitesAtRisk']}"/>
                        </td>
                        <td>
                            <div class="progress-bar-wrapper">
                                <div class="progress-bar color-delayed" style="width: 100%"></div>
                            </div>
                            <apex:outputText value="{!countrySummary['NumberOfSitesDelayed']}"/>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <apex:repeat value="{!projectDetailItem.Countries}" var="country">
                        <tr>
                            <td><apex:outputText value="{!country.Name}"/></td>
                            <td><apex:outputText value="{!country.NumberOfSites}"/></td>
                            <td><apex:outputText value="{!country.NumberOfSitesCompleted}"/></td>
                            <td>
                                <div class="progress-bar-wrapper">
                                    <div class="progress-bar color-on-track" style="width: {!IF(countrySummary['NumberOfSitesOnTrack']==0, 0, country.NumberOfSitesOnTrack/(countrySummary['NumberOfSitesOnTrack'])*100)}%"></div>
                                </div>
                                <apex:outputText value="{!country.NumberOfSitesOnTrack}"/>
                            </td>
                            <td>
                                <div class="progress-bar-wrapper">
                                    <div class="progress-bar color-at-risk" style="width: {!IF(countrySummary['NumberOfSitesAtRisk']==0, 0, country.NumberOfSitesAtRisk/(countrySummary['NumberOfSitesAtRisk'])*100)}%"></div>
                                </div>
                                <apex:outputText value="{!country.NumberOfSitesAtRisk}"/></td>
                            <td>
                                <div class="progress-bar-wrapper">
                                    <div class="progress-bar color-delayed" style="width: {!IF(countrySummary['NumberOfSitesDelayed']==0, 0, country.NumberOfSitesDelayed/(countrySummary['NumberOfSitesDelayed'])*100)}%"></div>
                                </div>
                                <apex:outputText value="{!country.NumberOfSitesDelayed}"/>
                            </td>
                        </tr>
                    </apex:repeat>                
                </tbody>
            </table>
        </div>

        <div class="page graph hidden">
            <c:PBB_ViewSitesandSubjectgraphs GType="SitesActivated" PBBSID="{!scenarioId}" id="c"/>
        </div>

        <div class="page hidden">
            <c:PBB_Activation_by_Site projectDetailItem="{!projectDetailItem}" />
        </div>
        <div class="page hidden">
            <div class="left">
                <table id="tableActivation" class="dataTable" data-sizeY="432">
                    <thead>
                        <tr>
                            <th>{!$Label.PBB_Mobile_6_7_Site_ID_Name}</th>
                            <th>{!$Label.PBB_Mobile_4_5_7_Country}</th>
                        </tr>
                    </thead>
                    <tbody>
                        <apex:repeat value="{!projectDetailItem.Countries}" var="country">
                            <apex:repeat value="{!country.Sites}" var="site">
                                <apex:variable var="row_color" value="{!IF(site.StudyStartUpStatus == 'Completed', 'completed', IF(site.StudyStartUpStatus == 'At Risk', 'at-risk', IF(site.StudyStartUpStatus == 'On Track', 'on-track', IF(site.StudyStartUpStatus == 'Delayed', 'delayed', 'not-started'))))}"/>
                                <tr class="{!row_color}">
                                    <td>
                                        <apex:outputText value="{!site.Name}"/>
                                        <apex:outputText rendered="{!site.PI != null}"> /&nbsp;</apex:outputText>
                                        <apex:outputText value="{!site.PI}"/>
                                    </td>
                                    <td><apex:outputText value="{!country.Name}"/></td>
                                </tr>
                            </apex:repeat>
                        </apex:repeat>
                    </tbody>
                </table>
            </div>
            <div class="right">
                <table id="summary" class="summary">
                    <tbody>
                        <tr>
                            <td>{!$Label.PBB_Mobile_7_Goal}</td>
                            <td class="marked goal">
                                <apex:input type="number" value="{!goalInteger}"/>
                                <!--<apex:actionStatus startText="requesting..." id="myStatus"></apex:actionStatus>-->

                            </td>
                        </tr>
                        <tr>
                            <td>{!$Label.PBB_Mobile_7_Sites_Active_By}</td>
                            <td class="marked date">
                                <apex:input value="{!siteActivatedByDate}" type="date"/>
                            </td>
                        </tr>
                        <tr>
                            <td>{!$Label.PBB_Mobile_7_Active_to_date}</td>
                            <td>{!projectDetailItem.NumberOfSitesCompleted}</td>
                        </tr>
                        <tr>
                            <td>{!$Label.PBB_Mobile_7_Remaining}</td>
                            <td>{!goalInteger - projectDetailItem.NumberOfSitesCompleted}</td>
                        </tr>
                        <tr class="on-track">
                            <td>{!$Label.PBB_Mobile_4_On_Track}</td>
                            <td>{!projectDetailItem.NumberOfSitesOnTrackEstimated}</td>
                        </tr>
                        <tr class="at-risk">
                            <td>{!$Label.PBB_Mobile_4_At_Risk}</td>
                            <td>{!projectDetailItem.NumberOfSitesAtRiskEstimated}</td>
                        </tr>
                        <tr class="delayed">
                            <td>{!$Label.PBB_Mobile_4_Delayed}</td>
                            <td>{!projectDetailItem.NumberOfSitesDelayedEstimated}</td>
                        </tr>
                    </tbody>
                </table>
            </div>        
        </div>
    </main>

    <c:PBB_Footer_Menu_Component />
    
    <apex:actionFunction action="{!saveData}" name="saveData" rerender="summary" status="myStatus">
    </apex:actionFunction>

</apex:form>
<script>

function getQueryParams(qs) {
    qs = qs.split("+").join(" ");

    var params = {}, tokens,
        re = /[?&]?([^=]+)=([^&]*)/g;

    while (tokens = re.exec(qs)) {
        params[decodeURIComponent(tokens[1])]
            = decodeURIComponent(tokens[2]);
    }

    return params;
}

var j$ = $;
$(document).ready(function() {
    // dataTables
    var dt_options = {
        'bPaginate': false,
        'bLengthChange': false,
        'bFilter': false,
        'bInfo': false,
        'bAutoWidth': false,
        'sDom':            "t",
        'bUseRendered': false,
        'sScrollY': "400px",
    }
    var table;

    // page switching
    function showPage(number) {
        var $active = $('.page').addClass('hidden').eq(number).removeClass('hidden');
        
        // render dataTables on page show
        if (!$active.data('datatables')) {
            var $table = $active.find('.dataTable');
            var sizeY = $table.data('sizey') + 'px';
            dt_options.sScrollY = sizeY;
                        
            if ($table.attr('id') == 'tableOverviewBySite') {
                table = $table.DataTable($.extend({}, dt_options, {
                    "columns": [
                        null,
                        null,
                        null,
                        null,
                        null,
                        { "orderable": false },
                      ]
                }));
            } else {
                $table.DataTable(dt_options);
            }
                      
            $active.data('datatables', true);
        }
        $('.navigation a').removeClass('active').eq(number).addClass('active');
    }
    
    // show page passed in URL
    var params = getQueryParams(document.location.search);
    var numPages = 4;
        
    var page = params['page'];
    if (page && (page = parseInt(page)) && page > 0 && page <= numPages) {
        page = page - 1;
    } else {
        page = 0;
    }
    showPage(page);

    // navigation    
    $('.navigation').on('click', 'a', function(){
        showPage($(this).index());
        return false;
    });

    $('#tableOverviewBySite').on('click', '.arrow', function(){
        $(this).toggleClass('up');
        
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( $(this).next().html() ).show();
            tr.addClass('shown');
        }
    });
    
    var $goal = $('.goal input');
    if (!$goal.val()) {
        $goal.val('0');
    }
    
    var $date = $('.date input');
    if (!$date.val()) {
        $date[0].valueAsDate = new Date();
    }
    
    $('form').on('submit', function(){
        saveData();
        return false;
    });
    
    $('.summary').on('change', 'input', function(){
        console.log('!!!');
        saveData();
        return false;
    });
});
</script>
</apex:page>