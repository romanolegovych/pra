<apex:page controller="PAWS_CrossProjectPlanningController" showHeader="true" sidebar="false" tabStyle="Cross_Project_Planning_Review__tab" action="{!populateDashboardDefaults}">
    <apex:actionStatus id="status">
        <apex:facet name="start">
            <STSWR1:ScreenLocker />
        </apex:facet>
    </apex:actionStatus>
    
    <c:PAWS_CrossProjectPlanningGantt pageController="{!pageController}"/>
    
    <c:PAWS_CrossProjectPlanningExport status="{!$Component.status}"/>
    
    <style>
        .footerPageNavigator {background-image:url('/img/paginationArrows.gif'); background-repeat:no-repeat; height:10px; width:9px;}
        .headerColumn {}
        .headerColumn div>div {cursor:pointer;}
        .headerColumn img {margin-left: 5px;vertical-align: middle;}
        .centerHeaderColumn {font-weight:bold;text-decoration:none;text-align:center;}
        .wrapColumn div {overflow: hidden;white-space: nowrap;text-overflow: ellipsis;}
        .column100 div {width:100px;}
        .column150 div {width:150px;}
        .filterHeaderColumn {font-weight:bold;padding-right:3px;}
        .filterValueColumn {margin-right:8px;}
    </style>

    <apex:form id="dashboardForm">
        <apex:outputPanel rendered="{!NOT(ObjectSpecificProgressMode)}">
            <apex:sectionHeader title="Cross Project Planning Review"/>
            
            <apex:pageBlock >
                <apex:outputPanel id="filtersPanel" layout="block" style="padding:3px 5px 5px 0px;">
                    <apex:panelGrid columns="8">
                        <apex:outputPanel styleClass="filterHeaderColumn">{!$ObjectType.ecrf__c.fields.Sponsor__c.label}:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:inputText id="sponsorFilterField" value="{!SponsorFilter}"/>
                            <input type="button" class="btn" value=">>" onclick="displayFilterDialog(true, 'Sponsor', '{!$Component.sponsorFilterField}')"/> 
                        </apex:outputPanel>

                        <apex:outputPanel styleClass="filterHeaderColumn">Flow:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:inputText id="flowFilterField" value="{!FlowFilter}"/>
                            <input type="button" class="btn" value=">>" onclick="displayFilterDialog(true, 'Flow', '{!$Component.flowFilterField}')"/> 
                        </apex:outputPanel>
                        
                        <apex:outputPanel styleClass="filterHeaderColumn">Assigned To:</apex:outputPanel>
                        <apex:outputPanel layout="block" styleClass="filterValueColumn">
                            <apex:inputText id="assigneeFilterField" value="{!AssigneeFilter}"/>
                            <input type="button" class="btn" value=">>" onclick="displayAssigneeFilterDialog(true, '{!$Component.assigneeFilterField}')"/> 
                        </apex:outputPanel>
                
                        <apex:outputPanel styleClass="filterHeaderColumn" style="padding-left:8px;">Start Date Range:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:outputPanel style="padding:0px 3px;">from</apex:outputPanel>
                            <STSWR1:InputDate value="{!StartDateFilterForStartDate}"/>
                                
                            <apex:outputPanel style="padding:0px 3px;">to</apex:outputPanel>
                            <STSWR1:InputDate value="{!EndDateFilterForStartDate}"/>
                        </apex:outputPanel>

                        <apex:outputPanel styleClass="filterHeaderColumn">{!$ObjectType.ecrf__c.fields.Name.label}:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:inputText id="projectFilterField" value="{!ProjectFilter}"/>
                            <input type="button" class="btn" value=">>" onclick="displayProjectFilterDialog(true, '{!$Component.projectFilterField}')"/> 
                        </apex:outputPanel>

                        <apex:outputPanel styleClass="filterHeaderColumn">Step:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:inputText id="stepFilterField" value="{!StepFilter}"/>
                            <input type="button" class="btn" value=">>" onclick="displayStepFilterDialog(true, '{!$Component.stepFilterField}')"/> 
                        </apex:outputPanel>

                        <apex:outputPanel styleClass="filterHeaderColumn">Role:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:inputText id="roleFilterField" value="{!RoleFilter}"/>
                            <input type="button" class="btn" value=">>" onclick="displayRoleFilterDialog(true, '{!$Component.roleFilterField}')"/> 
                        </apex:outputPanel>
                        
                        <apex:outputPanel styleClass="filterHeaderColumn" style="padding-left:8px;">Complete Date Range:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:outputPanel style="padding:0px 3px;">from</apex:outputPanel>
                            <STSWR1:InputDate value="{!StartDateFilterForEndDate}"/>
                            <apex:outputPanel style="padding:0px 3px;">to</apex:outputPanel>
                            <STSWR1:InputDate value="{!EndDateFilterForEndDate}"/>                      
                        </apex:outputPanel>
                        
                        <apex:outputPanel styleClass="filterHeaderColumn">Views:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:inputText id="tagFilterField" value="{!selectedTags}"/>
                            <input type="button" class="btn" value=">>" onclick="displayTagFilterDialog(true, '{!$Component.tagFilterField}');"/> 
                        </apex:outputPanel>
                        
                        <apex:outputPanel styleClass="filterHeaderColumn">Protocol ID:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:inputText id="protocolFilterField" value="{!selectedProtocols}"/>
                            <input type="button" class="btn" value=">>" onclick="displayProtocolFilterDialog(true, '{!$Component.protocolFilterField}');"/> 
                        </apex:outputPanel>
                        
                        <apex:outputPanel styleClass="filterHeaderColumn">Country:</apex:outputPanel>
                        <apex:outputPanel styleClass="filterValueColumn">
                            <apex:inputText id="countryFilterField" value="{!selectedCountries}"/>
                            <input type="button" class="btn" value=">>" onclick="displayCountryFilterDialog(true, '{!$Component.countryFilterField}');"/> 
                        </apex:outputPanel>
                    </apex:panelGrid>
                    
                    <div style="width:100%; padding:0 0 7px 0;">
                        <table>
                            <tr>
                                <td>
                                    <apex:inputCheckbox value="{!ExcludeCompletedStepsFilter}" style="margin-left:0;"/>
                                </td>
                                <td>
                                    <apex:outputPanel styleClass="filterHeaderColumn">Exclude Completed Steps</apex:outputPanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <apex:inputCheckbox value="{!ExcludeParentStepsFilter}" style="margin-left:0;"/>
                                </td>
                                <td>
                                    <apex:outputPanel styleClass="filterHeaderColumn">Exclude Parent Steps</apex:outputPanel>
                                </td>
                            </tr>
                        </table>
                    </div>
                    
                    <apex:outputPanel layout="block">
                        <apex:commandButton value="Apply Filters" action="{!applyFilters}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status"/>
                        <apex:commandButton value="Clear Filters" action="{!clearFilters}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel, dialogsPanel" status="status"/>
                        <input type="button" class="btn" value="Export" onclick="{!IF(GanttMode, 'exportGanttXml()', 'exportXml()')}"/> 

                        <apex:commandButton value="Enable Gantt Mode" action="{!enableGanttMode}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status" rendered="{!NOT(GanttMode)}" style="float:right;"/>
                        <apex:commandButton value="Cancel Gantt Mode" action="{!cancelGanttMode}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status" rendered="{!GanttMode}" style="float:right;"/>
                    </apex:outputPanel>
                    
                    <script type="text/javascript">  
                        function doExport(callback)
                        {
                           try{
                                var sdForSD = '<apex:outputText value="{0,date,{!DefaultDateFormat}}"><apex:param value="{!StartDateFilterForStartDate}" /></apex:outputText>',
                                    edForSD = '<apex:outputText value="{0,date,{!DefaultDateFormat}}"><apex:param value="{!EndDateFilterForStartDate}" /></apex:outputText>',
                                    sdForED = '<apex:outputText value="{0,date,{!DefaultDateFormat}}"><apex:param value="{!StartDateFilterForEndDate}" /></apex:outputText>',
                                    edForED = '<apex:outputText value="{0,date,{!DefaultDateFormat}}"><apex:param value="{!EndDateFilterForEndDate}" /></apex:outputText>';
                                 
                                if(sdForSD == "") sdForSD = null;
                                if(edForSD == "") edForSD = null;
                                if(sdForED == "") sdForED = null;
                                if(edForED == "") edForED = null;
                                
                                cpprExportService.export("{!JSENCODE(SponsorFilter)}", "{!JSENCODE(ProjectFilter)}", "{!JSENCODE(FlowFilter)}", "{!JSENCODE(StepFilter)}", "{!JSENCODE(RoleFilter)}", "{!JSENCODE(AssigneeFilter)}", "{!JSENCODE(selectedTags)}", "{!JSENCODE(selectedProtocols)}", "{!JSENCODE(selectedCountries)}", "{!ExcludeCompletedStepsFilter}", "{!ExcludeParentStepsFilter}", sdForSD, edForSD, sdForED, edForED, "{!OrderBy}", "{!OrderType}", callback); 
                            }catch(err)
                            {
                                callback(err);
                            }
                        }
                    </script>
                </apex:outputPanel>
                
                <apex:outputPanel id="gridPanel">
                    <apex:outputPanel layout="block" style="overflow-y: auto; overflow-x: hidden; height:300px; background-color:white;" rendered="{!NOT(GanttMode)}">
                        <apex:pageBlockTable value="{!Rows}" var="row">
                            <apex:column headerClass="headerColumn" styleClass="wrapColumn column100">
                                <apex:facet name="header">
                                    <apex:outputPanel layout="block">
                                        <apex:outputPanel >{!$ObjectType.ecrf__c.fields.Sponsor__c.label}</apex:outputPanel>
                                        <apex:image value="{!IF(OrderType == 'asc', '/img/colTitle_uparrow.gif', '/img/colTitle_downarrow.gif')}" rendered="{!OrderBy == 'Project__r.Sponsor__c'}"/>
    
                                        <apex:actionSupport event="onclick" action="{!refresh}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status">
                                            <apex:param name="ordBy" assignTo="{!OrderBy}" value="Project__r.Sponsor__c"/>
                                            <apex:param name="ordType" assignTo="{!OrderType}" value="{!IF(AND(OrderType = 'asc', OrderBy = 'Project__r.Sponsor__c'), 'desc', 'asc')}"/>
                                        </apex:actionSupport>
                                    </apex:outputPanel>
                                </apex:facet>
                                
                                <apex:outputPanel layout="block"><apex:outputText value="{!row.Project.Sponsor__c}"/></apex:outputPanel>
                            </apex:column>
    
                            <apex:column headerClass="headerColumn" styleClass="wrapColumn column150">
                                <apex:facet name="header">
                                    <apex:outputPanel layout="block">
                                        <apex:outputPanel >{!$ObjectType.ecrf__c.fields.Name.label}</apex:outputPanel>
                                        <apex:image value="{!IF(OrderType == 'asc', '/img/colTitle_uparrow.gif', '/img/colTitle_downarrow.gif')}" rendered="{!OrderBy == 'Project__r.Name'}"/>
    
                                        <apex:actionSupport event="onclick" action="{!refresh}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status">
                                            <apex:param name="ordBy" assignTo="{!OrderBy}" value="Project__r.Name"/>
                                            <apex:param name="ordType" assignTo="{!OrderType}" value="{!IF(AND(OrderType = 'asc', OrderBy = 'Project__r.Name'), 'desc', 'asc')}"/>
                                        </apex:actionSupport>
                                    </apex:outputPanel>
                                </apex:facet>
                                
                                <apex:outputPanel layout="block"><apex:outputLink value="{!URLFOR('/' + row.Project.Id)}">{!row.Project.Name}</apex:outputLink></apex:outputPanel>
                            </apex:column> 
                            
                            <apex:column headerClass="headerColumn" styleClass="wrapColumn column150">
                                <apex:facet name="header">
                                    <apex:outputPanel layout="block">
                                        <apex:outputPanel >Flow Name</apex:outputPanel>
                                        <apex:image value="{!IF(OrderType == 'asc', '/img/colTitle_uparrow.gif', '/img/colTitle_downarrow.gif')}" rendered="{!OrderBy == 'STSWR1__Step__r.STSWR1__Flow__r.Name'}"/>
                                    
                                        <apex:actionSupport event="onclick" action="{!refresh}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status">
                                            <apex:param name="ordBy" assignTo="{!OrderBy}" value="STSWR1__Step__r.STSWR1__Flow__r.Name"/>
                                            <apex:param name="ordType" assignTo="{!OrderType}" value="{!IF(AND(OrderType = 'asc', OrderBy = 'STSWR1__Step__r.STSWR1__Flow__r.Name'), 'desc', 'asc')}"/>
                                        </apex:actionSupport>
                                    </apex:outputPanel>
                                </apex:facet>
                                
                                <apex:outputPanel layout="block"><apex:outputText value="{!row.Property.STSWR1__Step__r.STSWR1__Flow__r.Name}" title="{!row.Property.STSWR1__Step__r.STSWR1__Flow__r.Name}"/></apex:outputPanel>
                            </apex:column> 
                            
                            <apex:column headerClass="headerColumn" styleClass="wrapColumn column150">
                                <apex:facet name="header">
                                    <apex:outputPanel layout="block">
                                        <apex:outputPanel >Step Name</apex:outputPanel>
                                        <apex:image value="{!IF(OrderType == 'asc', '/img/colTitle_uparrow.gif', '/img/colTitle_downarrow.gif')}" rendered="{!OrderBy == 'STSWR1__Step__r.Name'}"/>
                                    
                                        <apex:actionSupport event="onclick" action="{!refresh}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status">
                                            <apex:param name="ordBy" assignTo="{!OrderBy}" value="STSWR1__Step__r.Name"/>
                                            <apex:param name="ordType" assignTo="{!OrderType}" value="{!IF(AND(OrderType = 'asc', OrderBy = 'STSWR1__Step__r.Name'), 'desc', 'asc')}"/>
                                        </apex:actionSupport>
                                    </apex:outputPanel>
                                </apex:facet>
                                
                                <apex:outputPanel layout="block"><apex:outputText value="{!row.Property.STSWR1__Step__r.Name}" title="{!row.Property.STSWR1__Step__r.Name}"/></apex:outputPanel>
                            </apex:column> 
                            
                            <apex:column headerClass="headerColumn">
                                <apex:facet name="header">
                                    <apex:outputPanel layout="block">
                                        <apex:outputPanel >Planned Start</apex:outputPanel>
                                        <apex:image value="{!IF(OrderType == 'asc', '/img/colTitle_uparrow.gif', '/img/colTitle_downarrow.gif')}" rendered="{!OrderBy == 'Start_Date__c'}"/>
    
                                        <apex:actionSupport event="onclick" action="{!refresh}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status">
                                            <apex:param name="ordBy" assignTo="{!OrderBy}" value="Start_Date__c"/>
                                            <apex:param name="ordType" assignTo="{!OrderType}" value="{!IF(AND(OrderType = 'asc', OrderBy = 'Start_Date__c'), 'desc', 'asc')}"/>
                                        </apex:actionSupport>
                                    </apex:outputPanel>
                                </apex:facet>
                                
                                <apex:outputPanel layout="block">
                                    <apex:outputText value="{0,date,{!$Setup.STSWR1__Work_Relay_Config__c.STSWR1__Date_Format__c}}">
                                       <apex:param value="{!row.Property.Start_Date__c}" />
                                    </apex:outputText>
                                </apex:outputPanel>
                            </apex:column> 
                            <!--  
                            <apex:column headerClass="headerColumn">
                                <apex:facet name="header">
                                    <apex:outputPanel layout="block">
                                        <apex:outputPanel >Actual Start</apex:outputPanel>
                                        <apex:image value="{!IF(OrderType == 'asc', '/img/colTitle_uparrow.gif', '/img/colTitle_downarrow.gif')}" rendered="{!OrderBy == 'STSWR1__Actual_Start_Date_Value__c'}"/>
    
                                        <apex:actionSupport event="onclick" action="{!refresh}" rerender="dashboardForm" status="status">
                                            <apex:param name="ordBy" assignTo="{!OrderBy}" value="STSWR1__Actual_Start_Date_Value__c"/>
                                            <apex:param name="ordType" assignTo="{!OrderType}" value="{!IF(AND(OrderType = 'asc', OrderBy = 'STSWR1__Actual_Start_Date_Value__c'), 'desc', 'asc')}"/>
                                        </apex:actionSupport>
                                    </apex:outputPanel>
                                </apex:facet>
                                
                                <apex:outputPanel layout="block">
                                    <apex:outputText value="{0,date,dd/MMM/yy}">
                                       <apex:param value="{!row.History.STSWR1__Actual_Start_Date_Value__c}" />
                                    </apex:outputText>
                                </apex:outputPanel>
                            </apex:column> 
                            -->
                            <apex:column headerClass="headerColumn">
                                <apex:facet name="header">
                                    <apex:outputPanel >Actual Start</apex:outputPanel>
                                </apex:facet>
                                
                                <apex:outputPanel layout="block">
                                    <apex:outputText value="{0,date,{!$Setup.STSWR1__Work_Relay_Config__c.STSWR1__Date_Format__c}}">
                                       <apex:param value="{!row.History.STSWR1__Actual_Start_Date__c}" />
                                    </apex:outputText>
                                </apex:outputPanel>
                            </apex:column> 
        
                            <apex:column headerClass="headerColumn">
                                <apex:facet name="header">
                                    <apex:outputPanel layout="block">
                                        <apex:outputPanel >Planned Complete</apex:outputPanel>
                                        <apex:image value="{!IF(OrderType == 'asc', '/img/colTitle_uparrow.gif', '/img/colTitle_downarrow.gif')}" rendered="{!OrderBy == 'STSWR1__Planned_End_Date__c'}"/>
    
                                        <apex:actionSupport event="onclick" action="{!refresh}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status">
                                            <apex:param name="ordBy" assignTo="{!OrderBy}" value="STSWR1__Planned_End_Date__c"/>
                                            <apex:param name="ordType" assignTo="{!OrderType}" value="{!IF(AND(OrderType = 'asc', OrderBy = 'STSWR1__Planned_End_Date__c'), 'desc', 'asc')}"/>
                                        </apex:actionSupport>
                                    </apex:outputPanel>
                                </apex:facet>
                                
                                <apex:outputPanel layout="block">
                                    <apex:outputText value="{0,date,{!$Setup.STSWR1__Work_Relay_Config__c.STSWR1__Date_Format__c}}">
                                       <apex:param value="{!row.Property.STSWR1__Planned_End_Date__c}" />
                                    </apex:outputText>
                                </apex:outputPanel>
                            </apex:column> 
                            
                            <apex:column headerValue="Revised Complete" headerClass="headerColumn">
                                <apex:facet name="header">
                                    <apex:outputPanel layout="block">
                                        <apex:outputPanel >Revised Complete</apex:outputPanel>
                                        <apex:image value="{!IF(OrderType == 'asc', '/img/colTitle_uparrow.gif', '/img/colTitle_downarrow.gif')}" rendered="{!OrderBy == 'STSWR1__Revised_End_Date__c'}"/>
    
                                        <apex:actionSupport event="onclick" action="{!refresh}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status">
                                            <apex:param name="ordBy" assignTo="{!OrderBy}" value="STSWR1__Revised_End_Date__c"/>
                                            <apex:param name="ordType" assignTo="{!OrderType}" value="{!IF(AND(OrderType = 'asc', OrderBy = 'STSWR1__Revised_End_Date__c'), 'desc', 'asc')}"/>
                                        </apex:actionSupport>
                                    </apex:outputPanel>
                                </apex:facet>
                                
                                <apex:outputPanel layout="block">
                                    <apex:outputText value="{0,date,{!$Setup.STSWR1__Work_Relay_Config__c.STSWR1__Date_Format__c}}">
                                       <apex:param value="{!row.Property.STSWR1__Revised_End_Date__c}" />
                                    </apex:outputText>
                                </apex:outputPanel>
                            </apex:column> 
                            
                            <apex:column headerClass="headerColumn">
                                <apex:facet name="header">
                                    <apex:outputPanel >Actual Complete</apex:outputPanel>
                                </apex:facet>
                                
                                <apex:outputPanel layout="block">
                                    <apex:outputText value="{0,date,{!$Setup.STSWR1__Work_Relay_Config__c.STSWR1__Date_Format__c}}">
                                       <apex:param value="{!IF(OR(row.History.STSWR1__Status__c == 'Completed', row.History.STSWR1__Status__c == 'Approved', row.History.STSWR1__Status__c == 'Rejected'), row.History.STSWR1__Actual_Complete_Date__c, NULL)}" />
                                    </apex:outputText>
                                </apex:outputPanel>
                            </apex:column> 
    
                            <apex:column headerClass="headerColumn" styleClass="wrapColumn column100">
                                <apex:facet name="header">
                                    <apex:outputPanel layout="block">
                                        <apex:outputPanel >Role</apex:outputPanel>
                                        <apex:image value="{!IF(OrderType == 'asc', '/img/colTitle_uparrow.gif', '/img/colTitle_downarrow.gif')}" rendered="{!OrderBy == 'STSWR1__Step__r.STSWR1__Flow_Swimlane__r.Name'}"/>
                                    
                                        <apex:actionSupport event="onclick" action="{!refresh}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status">
                                            <apex:param name="ordBy" assignTo="{!OrderBy}" value="STSWR1__Step__r.STSWR1__Flow_Swimlane__r.Name"/>
                                            <apex:param name="ordType" assignTo="{!OrderType}" value="{!IF(AND(OrderType = 'asc', OrderBy = 'STSWR1__Step__r.STSWR1__Flow_Swimlane__r.Name'), 'desc', 'asc')}"/>
                                        </apex:actionSupport>
                                    </apex:outputPanel>
                                </apex:facet>
                                
                                <apex:outputPanel layout="block"><apex:outputText value="{!row.Property.STSWR1__Step__r.STSWR1__Flow_Swimlane__r.Name}" title="{!row.Property.STSWR1__Step__r.STSWR1__Flow_Swimlane__r.Name}"/></apex:outputPanel>
                            </apex:column>
                            
                            <!--  
                            <apex:column headerClass="headerColumn" styleClass="wrapColumn">
                                <apex:facet name="header">
                                    <apex:outputPanel layout="block">
                                        <apex:outputPanel >Assigned To</apex:outputPanel>
                                        <apex:image value="{!IF(OrderType == 'asc', '/img/colTitle_uparrow.gif', '/img/colTitle_downarrow.gif')}" rendered="{!OrderBy == 'Step_Assigned_To_Name__c'}"/>
                                    
                                        <apex:actionSupport event="onclick" action="{!refresh}" rerender="dashboardForm" status="status">
                                            <apex:param name="ordBy" assignTo="{!OrderBy}" value="Step_Assigned_To_Name__c"/>
                                            <apex:param name="ordType" assignTo="{!OrderType}" value="{!IF(AND(OrderType = 'asc', OrderBy = 'Step_Assigned_To_Name__c'), 'desc', 'asc')}"/>
                                        </apex:actionSupport>
                                    </apex:outputPanel>
                                </apex:facet>
                                    
                                <apex:outputPanel layout="block"><apex:outputText value="{!row.AssignedTo}"/></apex:outputPanel>
                            </apex:column>
                            -->
                            <apex:column headerClass="headerColumn" styleClass="wrapColumn column100">
                                <apex:facet name="header">
                                    <apex:outputPanel >Assigned To</apex:outputPanel>
                                </apex:facet>
                                    
                                <apex:outputPanel layout="block"><apex:outputText value="{!row.AssignedTo}" title="{!row.AssignedTo}"/></apex:outputPanel>
                            </apex:column>
                            
                            <apex:column headerValue="Progress" headerClass="centerHeaderColumn" styleClass="centerHeaderColumn">
                                <apex:outputPanel layout="block">
                                    <apex:commandLink value="Go" action="{!enableObjectSpecificProgressMode}" rerender="dashboardForm" status="status">
                                        <apex:param name="sfic" assignTo="{!SeletedFlowInstanceId}" value="{!row.ProjectJunction.Flow_Instance__c}"/>
                                        <apex:param name="sp" assignTo="{!SeletedObjectId}" value="{!row.Project.Id}"/>
                                    </apex:commandLink>
                                </apex:outputPanel>
                            </apex:column>
                            
                            <apex:column headerValue="Diagram" headerClass="centerHeaderColumn" styleClass="centerHeaderColumn">
                                <apex:outputPanel layout="block"><apex:outputLink value="{!URLFOR($Page.STSWR1__FlowBuilderViewCompact, null, [id=row.Property.STSWR1__Step__r.STSWR1__Flow__c,objId=row.Project.Id,retURL=URLFOR($Page.PAWS_CrossProjectPlanning, null, [ctrs=selectedCountries, ptls=selectedProtocols, vws=selectedTags,sp=SponsorFilter,pr=ProjectFilter,fl=FlowFilter,st=StepFilter,ro=RoleFilter,as=AssigneeFilter,sd1=StartDateFilterForStartDateURLParam,sd2=EndDateFilterForStartDateURLParam,ed1=StartDateFilterForEndDateURLParam,ed2=EndDateFilterForEndDateURLParam,ecs=ExcludeCompletedStepsFilterURLParam,eps=ExcludeParentStepsFilterURLParam,ob=OrderBy,ot=OrderType,rscPS=RowsSetController.PageSize,rscPN=RowsSetController.PageNumber])])}" target="_self">Go</apex:outputLink></apex:outputPanel>
                            </apex:column>
                            
                            <apex:column headerValue="Gantt" headerClass="centerHeaderColumn" styleClass="centerHeaderColumn">
                                <apex:outputPanel layout="block"><apex:outputLink value="{!URLFOR($Page.STSWR1__GanttView, null, [flow=row.ProjectJunction.Flow__c,viewmode=true,retURL=URLFOR($Page.PAWS_CrossProjectPlanning, null, [ctrs=selectedCountries, ptls=selectedProtocols, vws=selectedTags,sp=SponsorFilter,pr=ProjectFilter,fl=FlowFilter,st=StepFilter,ro=RoleFilter,as=AssigneeFilter,sd1=StartDateFilterForStartDateURLParam,sd2=EndDateFilterForStartDateURLParam,ed1=StartDateFilterForEndDateURLParam,ed2=EndDateFilterForEndDateURLParam,ecs=ExcludeCompletedStepsFilterURLParam,eps=ExcludeParentStepsFilterURLParam,ob=OrderBy,ot=OrderType,rscPS=RowsSetController.PageSize,rscPN=RowsSetController.PageNumber])])}" target="_self">Go</apex:outputLink></apex:outputPanel>
                            </apex:column>
                        </apex:pageBlockTable>
                    </apex:outputPanel>
                </apex:outputPanel>
      
                <apex:outputPanel layout="block" styleClass="ganttBodyPanel">
                    <div id="app_main_holder"></div>
                </apex:outputPanel>
                    
                <apex:outputPanel id="ganttPanel">
                    <style>
                        .ganttBodyPanel {overflow-y: auto; overflow-x: hidden; height:300px !important; background-color:white; display:{!IF(GanttMode, 'inline', 'none')}}
                    </style>
                    
                    <apex:outputPanel rendered="{!GanttMode}">
                        <script type="text/javascript">
                            var rowsJson = '{!JSENCODE(pageController.RowsJSON)}';
                            drawGantt(rowsJson, "{!StartDateFilterForStartDate}", "{!EndDateFilterForStartDate}", "{!StartDateFilterForEndDate}", "{!EndDateFilterForEndDate}");
                        </script>                   
                    </apex:outputPanel>
                </apex:outputPanel>
    
                <apex:outputPanel id="pagerPanel" layout="block" style="text-align:center;padding-top: 5px;">
                    <apex:outputPanel rendered="{!RowsSetController.HasPrevious}">
                        <span><apex:commandLink action="{!RowsSetController.first}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status"><img src="/s.gif" class="footerPageNavigator" style="background-position:0px 1px;"/></apex:commandLink></span>
                        <span><apex:commandLink action="{!RowsSetController.previous}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status"><img src="/s.gif" class="footerPageNavigator" style="background-position:-10px 1px; margin: 0pt; padding: 0pt;"/>Previous</apex:commandLink></span>
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!NOT(RowsSetController.HasPrevious)}">
                        <span><img src="/s.gif" class="footerPageNavigator" style="background-position:0px -10px;"/></span>
                        <span style="color:#A8A8A8;"><img src="/s.gif" class="footerPageNavigator" style="background-position:-10px -10px; margin: 0pt; padding: 0pt;"/>Previous</span>
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!RowsSetController.HasNext}">
                        <span><apex:commandLink action="{!RowsSetController.next}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status">Next<img src="/s.gif" class="footerPageNavigator" style="background-position:-17px 1px;"/></apex:commandLink></span>
                        <span><apex:commandLink action="{!RowsSetController.last}" rerender="filtersPanel, gridPanel, ganttPanel, pagerPanel" status="status"><img src="/s.gif" class="footerPageNavigator" style="background-position:-27px 1px;"/></apex:commandLink></span>
                    </apex:outputPanel>
                    <apex:outputPanel rendered="{!NOT(RowsSetController.HasNext)}">
                        <span style="color:#A8A8A8;">Next<img src="/s.gif" class="footerPageNavigator" style="background-position:-17px -10px;"/></span>
                        <span><img src="/s.gif" class="footerPageNavigator" style="background-position:-27px -10px;"/></span>
                    </apex:outputPanel>
                    
                    <apex:outputPanel layout="block" style="float:right;">
                        <apex:outputPanel >Loaded: {!(RowsSetController.PageNumber - 1) * RowsSetController.PageSize + RowsSetController.Records.size} of {!RowsSetController.ResultSize}</apex:outputPanel>
                    </apex:outputPanel>
                </apex:outputPanel>
            </apex:pageBlock>
            
            
            <apex:outputPanel id="dialogsPanel">
            	<script type="text/javascript">
            		var filters = new Object();
            		
			    	function getDashboardController()
			    	{
			    		return PAWS_CrossProjectPlanningController;
			    	}
			    	
			    	function displayFilterDialog(show, filterName, filterFieldId)
                    {
                    	filters[filterName] = filterFieldId;
                    	
                    	var values = document.getElementById(filterFieldId).value;
                    	eval("displayDialog" + filterName + "(show, values)");
                    }
			    	
			    	function applyFilterValue(filterName, value)
	                {
	                	var elm = document.getElementById(filters[filterName]);
	                	if(elm == null) return;
	                	
	                    var oldValue = elm.value;
	                    if(oldValue.length > 0 && oldValue.charAt(value.length - 1) != ";") oldValue += ";";
	                    
	                    var values = (value ? value.split(";") : new Array());
	                    for(var i = 0; i < values.length; i++)
	                    	oldValue = oldValue.replace(values[i] + ";", "");
	                    
	                    elm.value = oldValue + value;
	                }
			    </script>

	            <apex:outputPanel >	            	
	            	<c:PAWS_CrossProjectFilterDialog name="Flow" applyFunction="applyFilterValue" filterFunction="getDashboardController().loadAvailableFlows"/>
	            </apex:outputPanel>
	            
	            <apex:outputPanel >	            	
	            	<c:PAWS_CrossProjectFilterDialog name="Sponsor" applyFunction="applyFilterValue" filterFunction="getDashboardController().loadAvailableSponsors"/>
	            </apex:outputPanel>
            </apex:outputPanel>
        </apex:outputPanel>

        <apex:outputPanel rendered="{!ObjectSpecificProgressMode}">
            <apex:sectionHeader title="{!SeletedObject['Name']}" subTitle="Project Specific Progress"/>
            
            <apex:outputPanel layout="block" style="padding-bottom:5px;">
                <apex:commandLink action="{!cancelObjectSpecificProgressMode}" rerender="dashboardForm" status="status">&lt;&lt; Back to Cross Project Planning Review</apex:commandLink>
            </apex:outputPanel>
            
            <apex:pageBlock >
                <c:PAWS_WorkflowProgress flowInstanceId="{!SeletedFlowInstanceId}"  compactMode="false" reinitFlag="{!ReinitFlag}"/>
            </apex:pageBlock>
        </apex:outputPanel>
    </apex:form>
</apex:page>