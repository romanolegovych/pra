<apex:page sidebar="false" showHeader="false" controller="PAWS_StepProperty_LinkedStepController" docType="html-5.0" action="{!Init}">
	<apex:composition template="STSWR1__FlowStepPropertyComposition" />
	
	<apex:form >
		<apex:actionFunction name="saveLinkedStepConfig" status="page-status" rerender="saveSection,messages" action="{!Save}"/>
		
		<apex:actionStatus id="page-status" onstart="showScreenLocker(true);" onstop="showScreenLocker(false);" />
		
		<apex:outputPanel id="invalidConfig" layout="block">
			<apex:outputPanel layout="block" style="font-weight:bold; color:red; padding-bottom:5px;" rendered="{!IsInvalidConfig}">
				Configuration is invalid! Most likely it happened after a flow has been cloned or imported. Please provide a new configuration.
			</apex:outputPanel>
		</apex:outputPanel>
		
		<div style="width:100;text-align:right;height:15px;padding-right:5px;font-size:10px;">
			<a href="#" onclick="activityOnClick();">overall activity</a>&nbsp;|&nbsp;<a href="#" onclick="helpOnClick();">help</a>
		</div>
		<apex:pageBlock >
			<apex:pageMessages id="messages" />
			
			<apex:pageBlockSection columns="1">
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="Connection Type" />
					<apex:outputPanel >
						<apex:selectList value="{!LinkedStepType}" size="1" style="width:100%;">
							<apex:selectOption itemValue="Master" itemLabel="Master"/>
							<apex:selectOption itemValue="Slave" itemLabel="Slave"/>
							<apex:actionSupport event="onchange" action="{!LinkedStepTypeOnChange}" status="page-status" rerender="propertyTypeLegend,stepSelectList,messages"/>
						</apex:selectList>
						<br/>
						<apex:outputPanel id="propertyTypeLegend" layout="block" style="padding:5px 0; font-size:10px;">
							<apex:outputPanel rendered="{!LinkedStepType == 'Master'}">
								Current step is <b>master</b> and controls a <b>slave</b> step selected below:
							</apex:outputPanel>
							<apex:outputPanel rendered="{!LinkedStepType == 'Slave'}">
								Current step is <b>slave</b> and is controled by a <b>master</b> step selected below:
							</apex:outputPanel>
						</apex:outputPanel>
					</apex:outputPanel>
				</apex:pageBlockSectionItem>
				
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="Flow" />
					<apex:outputPanel id="itemSelectListDiv" layout="block">
						<apex:selectList id="itemSelectList" value="{!SelectedItemId}" size="1" style="width:100%;">
							<apex:selectOptions value="{!Items}"/>
							<apex:actionSupport event="onchange" status="page-status" action="{!SelectedItemOnChange}" rerender="itemSelectListDiv,stepSelectList,invalidConfig,messages"/>
						</apex:selectList>
						<script type="text/javascript">
							var itemSelectList = document.getElementById('{!$Component.itemSelectList}');
							for (var i = 0; i < itemSelectList.options.length; i++){
								if (itemSelectList.options[i].value.indexOf('folder-') == 0){
									itemSelectList.options[i].style.color = 'grey';
								}
							}
						</script>
					</apex:outputPanel>
				</apex:pageBlockSectionItem>
				
				<apex:pageBlockSectionItem >
					<apex:outputLabel value="Step" />
					<apex:selectList id="stepSelectList" value="{!LinkedStepId}" size="1" style="width:100%;">
						<apex:selectOptions value="{!Steps}"/>
					</apex:selectList>
				</apex:pageBlockSectionItem>
			</apex:pageBlockSection>
		</apex:pageBlock>
		
		<apex:outputPanel id="saveSection">
			<apex:outputPanel rendered="{!IsSaveSuccessfull}">
				<script type="text/javascript">
					saveProperty('Linked Step', '{!JSENCODE(LinkedStepDataJSON)}');
				</script>
			</apex:outputPanel>
		</apex:outputPanel>
	</apex:form>
	
	<script type="text/javascript">
		asyncSave = function() {
			saveLinkedStepConfig();
		};
		
		helpOnClick = function(){
			openItemsPicker('/apex/PAWS_StepProperty_LinkedStepHelp', 0.75, 0.8);
		};
		
		activityOnClick = function(){
			openItemsPicker('/apex/PAWS_StepProperty_LinkedStepActivity', 0.6, 0.6);
		};
		
		openItemsPicker = function(url, widthPercent, heightPercent) {
			var width = parseInt(screen.width * widthPercent, 10),
				height = parseInt(screen.height * heightPercent, 10),
				left = parseInt((screen.width / 2) - (width / 2), 10),
				top = parseInt((screen.height / 2) - (height / 2), 10);
				
				window.open(url, 'Select Item', 'toolbar=no, location=no, directories=no, status=no, menubar8no, scrollbars=yes, resizable=yes, copyhistory=no, width=' + width + ', height=' + height + ', top=' + top + ', left=' + left);
		};
	</script>
</apex:page>