<apex:page controller="BDT_NewEditDesign" showHeader="false">
	<apex:composition template="BDT_GeneralTemplate">
		<apex:define name="MainContent">
			<apex:form rendered="{!securitiesA.ReadPriv}">
				<apex:actionStatus startText="Communicating with server, please wait..." layout="block" startStyleClass="BDTbusy" id="busy"/>

				<div style="width:100%">
				 	<div class="BDT_Container" style="float:none" >
						<div class="BDT_PageBlockHeader">
							<h2>Design details</h2>
							<apex:outputPanel id="ButtonArea">
							<apex:commandLink styleClass="BDT_RedButton" value="Delete" status="busy" action="{!deleteD}" rendered="{!securitiesA.DeletePriv}">
								<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/delete-icon.png')}"/>
						 	</apex:commandLink>
						 	<apex:commandLink styleClass="BDT_BlueButton" value="Save" status="busy" action="{!save}" rendered="{!showDragAndDrop && securitiesA.UpdatePriv}">
								<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/save.png')}"/>
						 	</apex:commandLink>
						 	<apex:commandLink styleClass="BDT_BlueButton" value="Update" status="busy" action="{!updatedata}" rendered="{!(!showDragAndDrop) && securitiesA.CreatePriv}">
							    <apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/save.png')}"/>
						 	</apex:commandLink>
						 	<apex:commandLink styleClass="BDT_BlueButton" value="Cancel" status="busy" action="{!cancel}">
								<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/cancel-icon.png')}"/>
						 	</apex:commandLink>
							<apex:commandLink styleClass="BDT_BlueButton" value="Copy from ..." status="busy" action="{!copy}" rendered="{!design.id!=null && securitiesA.CreatePriv}"/>
							</apex:outputPanel>
						</div>
						<apex:panelGrid columns="2" styleclass="BDT_EditTable">
							<apex:outputText value="{!$ObjectType.ClinicalDesign__c.fields.Name.label}" />
							<apex:inputField value="{!design.name}" />
						</apex:panelGrid>
					</div>
				</div>

			<c:BDT_HorizontalScroll divId="epocharmflowchart" >
				<div class="BDT_Container_SideBySide">
					<apex:outputPanel id="epochSection">
						<div class="BDT_PageBlockHeader">
							<h2>Epoch details</h2>
							<apex:commandLink styleClass="BDT_BlueButton" value="Add" action="{!addEpoch}" rerender="epochSection,flowchartdroparea,ButtonArea" status="busy" 
									rendered="{!securitiesA.CreatePriv&&(epochList.size<10)}">
								<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/new_icon.png')}"/>
							</apex:commandLink>
							<apex:outputText rendered="{!securitiesA.CreatePriv&&(epochList.size>9)}"
									style="font-size: 9px;float: right;line-height: 30px;"
									value="Max. of 10 epochs reached."/>
						</div>
						<div style="max-height:190px;overflow-x:hidden;overflow-y:auto">
       					  <apex:dataTable value="{!epochList}" var="sl" styleClass="BDT_Table">
       						<apex:column headerValue="Order">
								<apex:inputfield value="{!sl.Epoch_Number__c}" style="width:15px; height:20px;"/>
							</apex:column>
							<apex:column id="description" headerValue="{!$ObjectType.Epoch__c.fields.Description__c.label}">
								<apex:inputfield id="nameEpoch" value="{!sl.Description__c}" style="width:150px; height:20px;"/>
								<input type="hidden" value="{!sl.id}" />
							</apex:column>
							<apex:column style="padding-left:4px;padding-right:16px">
								<apex:commandLink action="{!deleteEpoch}" rerender="epochSection,flowchartdroparea,ButtonArea" rendered="{!sl.id!=null && securitiesA.UpdatePriv}" styleclass="BDT_DeleteLink" status="busy">
									<apex:param name="deleteEpochId" value="{!sl.id}"/>
								</apex:commandLink>
							</apex:column>
       					  </apex:dataTable>
       					  <script>
    						$j('[ID$="nameEpoch"]').attr('placeholder','Add Epoch here...');
						  </script>
       					</div>
   					</apex:outputPanel>
   				</div>

				<div class="BDT_Container_SideBySide">
					<apex:outputPanel id="armSection" >
						<div class="BDT_PageBlockHeader">
							<h2>Arm details</h2>
							<apex:commandLink styleClass="BDT_BlueButton" value="Add" action="{!addArm}" rerender="armSection,flowchartdroparea,ButtonArea" status="busy" 
									rendered="{!securitiesA.CreatePriv&&(armList.size<10)}">
								<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/new_icon.png')}"/>
							</apex:commandLink>
							<apex:outputText rendered="{!securitiesA.CreatePriv&&(armList.size>9)}"
									style="font-size: 9px;float: right;line-height: 30px;"
									value="Max. of 10 arms reached."/>
						</div>
  						
  						 <div style="max-height:190px;overflow-x:hidden;overflow-y:auto">
					      <apex:dataTable value="{!armList}" var="sl" style="width:auto;" styleClass="BDT_Table">
					    	<apex:column headerValue="Order">
								<apex:inputfield value="{!sl.Arm_Number__c}" style="width:15px; height:20px;"/>
							</apex:column>
							<apex:column id="description"  headerValue="{!$ObjectType.Arm__c.fields.Description__c.label}">
								<apex:inputfield id="nameArm" value="{!sl.Description__c}" style="width:150px; height:20px;"/>
								<input type="hidden" value="{!sl.id}" />
							</apex:column>
							<apex:column style="padding-left:4px;padding-right:16px">
								<apex:commandLink action="{!deleteArm}" rerender="armSection,flowchartdroparea,ButtonArea" rendered="{!sl.id!=null && securitiesA.UpdatePriv}" styleclass="BDT_DeleteLink">
						   			<apex:param name="deleteArmId" value="{!sl.id}"/>
						   		</apex:commandLink>
					  		</apex:column>
						  </apex:dataTable>
						<script>
							$j('[ID$="nameArm"]').attr('placeholder','Add ARM here...');
						</script>
						</div>
				  	</apex:outputPanel>
			  	</div>

				<div class="BDT_Container_SideBySide">
					<div class="BDT_PageBlockHeader">
						<h2>Flowchart details</h2>
						<apex:commandLink styleClass="BDT_BlueButton" value="Add" action="{!addFlowchart}" rerender="flowchartSection,flowchartdroparea,ButtonArea" status="busy" rendered="{!securitiesA.CreatePriv}">
							<apex:image value="{!URLFOR($Resource.BDT_nav_files, '/img/button_icons_15x15/new_icon.png')}"/>
						</apex:commandLink>
					</div>
					<apex:outputPanel id="flowchartSection"  >
						  <apex:dataTable value="{!flowchartList}" var="sl" style="width:auto;" styleClass="BDT_Table">
							<apex:column id="draggable" headerValue="Drag">
								<input id="flowchartid" type="hidden" value="{!sl.id}" />
							</apex:column>
							<apex:column headerValue="Order">
								<apex:inputfield value="{!sl.Flowchart_Number__c}" style="width:15px; height:20px;"/>
							</apex:column>
							<apex:column id="description"  headerValue="{!$ObjectType.Flowcharts__c.fields.Description__c.label}">
								<apex:inputfield id="nameFlow" value="{!sl.Description__c}" style="width:150px; height:20px;"/>
								<input type="hidden" value="{!sl.id}" />
							</apex:column>
							<apex:column style="padding-left:4px;padding-right:16px">
								<apex:commandLink action="{!deleteFlowchart}" rerender="flowchartSection,flowchartdroparea,ButtonArea" rendered="{!sl.id!=null && securitiesA.UpdatePriv}" styleclass="BDT_DeleteLink" status="busy">
						   			<apex:param name="deleteFlowchartId" value="{!sl.id}"/>
						   		</apex:commandLink>
					  		</apex:column>
						  </apex:dataTable>
						  <script>
    						$j('[ID$="nameFlow"]').attr('placeholder','Add Flowchart here...');
						  </script>
					</apex:outputPanel>
				</div>
			</c:BDT_HorizontalScroll>

<apex:outputPanel id="flowchartdroparea"> <apex:outputPanel rendered="{!showDragAndDrop}">
			<div style="width:100%">
 				<div class="BDT_Container">
					<div class="BDT_PageBlockHeader">
						<h2>Design</h2>
					</div>
<style>
 .BDT_DesignTable th p {width: 95px;
word-wrap: normal;
white-space: normal;
color: #6699CC;
font-size: 12px;
line-height: 12px;}

.BDT_DesignTable td.armlabel {
font-size: 12px !important;
line-height: 12px !important;
}

.BDT_DesignTable td div {padding:3px;}

</style>
					<div id="designContainer" class="designContainer">
						<apex:inputTextarea id="jsondata" value="{!Jsondata}" style="display:none;"/>
					</div>
				</div>
			</div></apex:outputPanel></apex:outputPanel>
		</apex:form>
	</apex:define>
</apex:composition>
<style>
    div.draggableObject 					{width:20px;height:20px;border: 1px solid #C0CDE2;z-index:1000;}
</style>
<!--[if IE 7]>
<script type="text/javascript" src="{!URLFOR($Resource.BDT_nav_files, '/src/JSON2.JS')}"></script>
<![endif]-->
<script type='text/javascript' src="{!URLFOR($Resource.BDT_designDragAndDrop)}"></script>
<script type='text/javascript' >
	var dDnD;
	var $j = jQuery.noConflict();
	$j(document).ready(function () {
			dDnD = new designDragAndDrop('');
		});
</script>
</apex:page>