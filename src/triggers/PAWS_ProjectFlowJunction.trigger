trigger PAWS_ProjectFlowJunction on PAWS_Project_Flow_Junction__c (after delete, after insert, after update, before delete, before insert, before update)
{
	new PAWS_ProjectFlowJunctionTrigger().execute();
	new STSWR1.AbstractTrigger().execute();
}