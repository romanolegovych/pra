/**
@author Grace
@date 2015
@description Restricting the deletion of service impact questions if status is approved or retired.
**/
trigger PBB_SIQuestion_To_SIResponseTrigger on Service_Impact_Questions__c (before delete) {
 for(Service_Impact_Questions__c siq : Trigger.old){
       if(siq.Status__c == 'Approved' || siq.Status__c == 'Retired' )
           siq.addError('UNABLE TO DELETE QUESTION BECAUSE SERVICE IMPACT QUESTION IS APPROVED/RETIRED!');
     }
}