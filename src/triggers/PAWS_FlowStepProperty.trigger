trigger PAWS_FlowStepProperty on STSWR1__Flow_Step_Property__c (after delete, after insert, after update, before delete, before insert, before update)
{
	new PAWS_FlowStepPropertyTrigger().executeIgnoreWorkflow();
}