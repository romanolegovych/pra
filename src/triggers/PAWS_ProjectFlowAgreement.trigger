trigger PAWS_ProjectFlowAgreement on PAWS_Project_Flow_Agreement__c (after delete, after insert, after update, before delete, before insert, before update)
{
	new PAWS_ProjectFlowAgreementTrigger().execute();
}