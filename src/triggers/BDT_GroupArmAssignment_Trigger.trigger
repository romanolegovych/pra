trigger BDT_GroupArmAssignment_Trigger on GroupArmAssignment__c (after delete, after insert, after update) {

	// recalculate affected designs after delete occurs
	if (Trigger.isAfter && Trigger.isDelete) {
		Set<ID> ArmIDs = new Set<ID>();
		for (grouparmassignment__c g : trigger.old) {
			ArmIDs.add(g.arm__c);
		}
		Set<ID> DesignIDs = new Set<ID>();
		for (arm__c a : [select ClinicalDesign__c from arm__c where id in :ArmIDs]) {
			DesignIDs.add(a.ClinicalDesign__c);
		}
		if (DesignIDs.size()>0) BDT_FlowchartServiceTotal.updateFSTonDesign(DesignIDs);
	}

	// recalculate affected arms after update or inserts occurs
	if (Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert)) {
		Set<ID> ArmIDs = new Set<ID>();
		for (GroupArmAssignment__c p : Trigger.new) {
			ArmIDs.add(p.arm__c);
		}
		if (ArmIDs.size()>0) BDT_FlowchartServiceTotal.updateFlowchartServiceTotal(ArmIDs,new Set<id>());
	}

}