trigger LMS_HandleCourseStatusChange on LMS_Course__c (after insert, after update) {

	String courseId;
    String replacementId;
    Set<Id> courseIds = new Set<Id>();
    
    Map<String, LMSConstantSettings__c> constants = LMSConstantSettings__c.getAll();
    Map<String, CourseDomainSettings__c> domains = CourseDomainSettings__c.getAll();
    static String outSync = constants.get('outSync').Value__c;
    static String statusDraft = constants.get('statusDraft').Value__c;
    static String statusPendingAdd = constants.get('statusPendingAdd').Value__c;
    static String statusRemoved = constants.get('statusRemoved').Value__c;
    static String statusRetired = constants.get('statusRetired').Value__c;
    static String statusSuspended = constants.get('statusSuspended').Value__c;
    static String archiveDomain = domains.get('Archive').Domain_Id__c;  
    
    for(LMS_Course__c course : trigger.new) {
        if(course.Status__c == statusRetired || course.Status__c == statusSuspended || course.Domain_Id__c == archiveDomain || 
            course.Discontinued_From__c <= Date.today()) {
            courseIds.add(course.Id);   
        }
        if(course.Replacement_Course__c != null) {
        	courseId = course.Id;
        	replacementId = course.Replacement_Course__c;
        }
    }
    
    if(courseIds != null && courseIds.size() > 0) {
        List<LMS_Role_Course__c> roleCourse = 
        [SELECT Status__c FROM LMS_Role_Course__c WHERE Course_Id__c IN :courseIds];
        List<LMS_Role_Course__c> invalidDrafts = new List<LMS_Role_Course__c>();
        
        if(roleCourse != null && roleCourse.size() > 0) {
            for(LMS_Role_Course__c rc : roleCourse) {
                if(rc.Status__c != statusDraft && rc.Status__c != statusPendingAdd) {
                    rc.Status__c = statusRemoved;
                } else {
                    invalidDrafts.add(rc);
                }
            }
            
            try {
                update roleCourse;
                delete invalidDrafts;
            } catch(DMLException e) {
                System.debug(e.getMessage());
            }
        }
    }
    
    if(courseId != null)	{
    	List<LMS_Role_Course__c> newMappings = new List<LMS_Role_Course__c>();
    	Map<String, String> currentMappings = new Map<String, String>();
    	Map<String, String> previousMappings = new Map<String, String>();
    	
    	// Get role_course mappings for the previous course
    	List<LMS_Role_Course__c> roleCourse = 
    	[select Role_Id__c, Course_Id__c, Assigned_By__c, Status__c, Sync_Status__c from LMS_Role_Course__c 
    	where Course_Id__r.SABA_ID_PK__c = :replacementId];
    	System.debug('------------------- mappings to replace ---------------------------');
    	System.debug(roleCourse);
    	if(roleCourse.size() > 0) {
    		for(LMS_Role_Course__c rc : roleCourse) {
    			previousMappings.put(rc.Role_Id__c, rc.Course_Id__c);
    		}
    	}
    	// Get the current mappings for the new course
    	List<LMS_Role_Course__c> roleCourseCurrent = 
    	[select Role_Id__c, Course_Id__c from LMS_Role_Course__c where Course_Id__c = :courseId];
    	System.debug('------------------- current mappings ---------------------------');
    	System.debug(roleCourseCurrent);
    	if(roleCourseCurrent.size() > 0) {
    		for(LMS_Role_Course__c rc : roleCourseCurrent) {
    			currentMappings.put(rc.Role_Id__c, rc.Course_Id__c);
    		}
    	}
    	
    	// Check to see if mappings already exits, if not create
    	for(LMS_Role_Course__c rc : roleCourse) {
    		if(!currentMappings.containsKey(rc.Role_Id__c)) {
	    		newMappings.add(new LMS_Role_Course__c(Course_Id__c = courseId, Role_Id__c = rc.Role_Id__c, Assigned_By__c = UserInfo.getName() , 
	        		Status__c = statusPendingAdd, Previous_Status__c = statusDraft, Sync_Status__c = outSync));
    		}
    	}
    	
    	System.debug(newMappings);
    	System.debug(newMappings.size());
    	insert newMappings;
    }
}