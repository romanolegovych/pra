trigger PAWS_Queue on PAWS_Queue__c (after delete, after insert, after update, before delete, before insert, before update)
{
	new PAWS_QueueTrigger().execute();
}