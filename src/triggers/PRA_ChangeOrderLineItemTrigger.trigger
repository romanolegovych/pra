trigger PRA_ChangeOrderLineItemTrigger on Change_Order_Line_Item__c (after delete, after insert, after update) {
    PRA_ChangeOrderItemTriggerHandler handler = new PRA_ChangeOrderItemTriggerHandler();
    
    if (Trigger.isInsert && Trigger.isAfter){
       handler.changeOrderLineItemOnAfterInsert(Trigger.new);
    }
    if (Trigger.isUpdate && Trigger.isAfter){
        handler.changeOrderLineItemOnAfterUpdate(Trigger.oldMap, Trigger.newMap);
    }
    if (Trigger.isDelete && Trigger.isAfter){
        handler.changeOrderLineItemOnAfterDelete(Trigger.old);
    }

}