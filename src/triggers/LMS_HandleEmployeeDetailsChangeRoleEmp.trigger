trigger LMS_HandleEmployeeDetailsChangeRoleEmp on Employee_Details__c (after update, after insert) {
    
    Set<Id> roleIds = new Set<Id>();
    Set<Id> updatedEmployeeIds = new Set<Id>();
    Set<String> jobFamily = new Set<String>();
    Set<String> jobTitle = new Set<String>();
    Set<String> unit = new Set<String>();
    Set<String> department = new Set<String>();
    List<String> regionName = new List<String>();
    List<String> countryName = new List<String>();
    Set<String> countryIds = new Set<String>();
    Set<String> status = new Set<String>();
    Set<String> types = new Set<String>();
    set<Integer> excludedCompanyCodes = new set<Integer>();
    Map<Id, String> regionMap = new Map<Id, String>();
    Map<Id, String> countryMap = new Map<Id, String>();
    Map<String, Set<Id>> empRoles = new Map<String, Set<Id>>();
    set<String> excludedEmployeeIds = LMS_ToolsFilter.getBlacklistEmployeeSet();
    map<Decimal, set<String>> companyBUMap = LMS_ToolsFilter.getWhitelistCompanyBUMap();
    
    static String outSync = 'N';
    static String statusDraft = 'Draft';
    static String statusRemoved = 'Removed';
    static String statusPendingAdd = 'Pending - Add';
    static String rolePRA = 'PRA';
    static String roleActive = 'Active';
    
    List<Employee_Status__c> statusType = [select Employee_Type__c,Employee_Status__c from Employee_Status__c];
    Map<String, String> empStatus = new Map<String, String>();
    for(Employee_Status__c empType : statusType) {
        empStatus.put(empType.Employee_Status__c, empType.Employee_Type__c);
    }
    List<Employee_Status__c> excludeStatuses = [SELECT Employee_Status__c FROM Employee_Status__c WHERE Employee_Type__c = 'Terminated'];
    Map<String,String> excludeStatus = new Map<String,String>();
    for(Employee_Status__c employeeStatus : excludeStatuses) {
        excludeStatus.put(employeeStatus.Employee_Status__c,employeeStatus.Employee_Status__c);
    }
    
    if(trigger.isAfter && trigger.isInsert) {
        for(Employee_Details__c emp : trigger.new) {
        	if ((excludedEmployeeIds == null || (excludedEmployeeIds != null && !excludedEmployeeIds.contains(emp.Name))) && 
        		(companyBUMap.isEmpty() || (!companyBUMap.isEmpty() && companyBUMap.containsKey(emp.Home_Company__c) && 
        		companyBUMap.get(emp.Home_Company__c).contains(emp.Business_Unit_Desc__c)))) {
	            
	            updatedEmployeeIds.add(emp.Id);
	            if(!jobFamily.contains(emp.Job_Class_Desc__c))
	                jobFamily.add(emp.Job_Class_Desc__c);
	            if(!jobTitle.contains(emp.Job_Code__c))
	                jobTitle.add(emp.Job_Code__c);
	            if(!unit.contains(emp.Business_Unit_Desc__c))
	                unit.add(emp.Business_Unit_Desc__c);
	            if(!department.contains(emp.Department__c))
	                department.add(emp.Department__c);
	            if(!countryIds.contains(emp.Country_Name__c))
	                countryIds.add(emp.Country_Name__c);
	            if(!status.contains(emp.Status__c))
	                status.add(emp.Status__c);
	                
        	}
        }
    }
    if(trigger.isAfter && trigger.isUpdate) {
        for(Employee_Details__c emp : trigger.new) {
        	if ((excludedEmployeeIds == null || (excludedEmployeeIds != null && !excludedEmployeeIds.contains(emp.Name))) && 
        		(companyBUMap.isEmpty() || (!companyBUMap.isEmpty() && companyBUMap.containsKey(emp.Home_Company__c) && 
        		companyBUMap.get(emp.Home_Company__c).contains(emp.Business_Unit_Desc__c)))) {
	            
	            if(emp.Job_Class_Desc__c != trigger.oldMap.get(emp.Id).Job_Class_Desc__c || emp.Job_Code__c != trigger.oldMap.get(emp.Id).Job_Code__c ||
	                emp.Business_Unit_Desc__c != trigger.oldMap.get(emp.Id).Business_Unit_Desc__c || emp.Department__c != trigger.oldMap.get(emp.Id).Department__c ||
	                emp.Country_Name__r.Region_Name__c != trigger.oldMap.get(emp.Id).Country_Name__r.Region_Name__c ||
	                emp.Country_Name__r.Name != trigger.oldMap.get(emp.Id).Country_Name__r.Name ||
	                emp.Status__c != trigger.oldMap.get(emp.Id).Status__c) {
	                system.debug('---------------------Role Values Altered---------------------');
	                updatedEmployeeIds.add(emp.Id);
	                if(!jobFamily.contains(emp.Job_Class_Desc__c))
	                    jobFamily.add(emp.Job_Class_Desc__c);
	                if(!jobTitle.contains(emp.Job_Code__c))
	                    jobTitle.add(emp.Job_Code__c);
	                if(!unit.contains(emp.Business_Unit_Desc__c))
	                    unit.add(emp.Business_Unit_Desc__c);
	                if(!department.contains(emp.Department__c))
	                    department.add(emp.Department__c);
	                if(!countryIds.contains(emp.Country_Name__c))
	                    countryIds.add(emp.Country_Name__c);
	                if(!status.contains(emp.Status__c))
	                    status.add(emp.Status__c);
	            }
	            
        	}
        }
    }
    
    if(countryIds != null && countryIds.size() > 0) {
    	List<Country__c> countries = [SELECT Name, Region_Name__c FROM Country__c WHERE Id IN :countryIds];
    	for(Country__c c : countries) {
    		regionMap.put(c.Id, c.Region_Name__c);
    		countryMap.put(c.Id, c.Name);
    	}
    	regionName = regionMap.values();
    	countryName = countryMap.values();
    	system.debug('-------------------------------'+jobFamily);
    	system.debug('-------------------------------'+jobTitle);
    	system.debug('-------------------------------'+unit);
    	system.debug('-------------------------------'+department);
    	system.debug('-------------------------------'+regionName);
    	system.debug('-------------------------------'+countryName);
    	system.debug('-------------------------------'+status);
    }
    
    if(updatedEmployeeIds != null && updatedEmployeeIds.size() > 0) {
        system.debug('------------------Starting Scan for Invalid Roles-------------------');
        List<LMS_Role_Employee__c> roleEmp = 
        [SELECT Role_Id__r.Job_Class_Desc__r.Name, Role_Id__r.Job_Title__r.Job_Code__c, Role_Id__r.Business_Unit__r.Name, Role_Id__r.Department__r.Name, 
         Role_Id__r.Region__r.Region_Name__c, Role_Id__r.Country__r.Name, Role_Id__r.Employee_Type__r.Name, Status__c, 
         Employee_Id__r.Job_Class_Desc__c, Employee_Id__r.Job_Code__c, Employee_Id__r.Business_Unit_Desc__c, Employee_Id__r.Department__c, 
         Employee_Id__r.Country_Name__r.Name, Employee_Id__r.Country_Name__r.Region_Name__c, Employee_Id__r.Status__c, Employee_Id__r.Date_Hired__c,
         Employee_Id__r.Term_Date__c FROM LMS_Role_Employee__c WHERE Employee_Id__c IN :updatedEmployeeIds ORDER BY Employee_Id__c];
        List<LMS_Role_Employee__c> deleteRoleEmps = new List<LMS_Role_Employee__c>();
        List<LMS_Role_Employee__c> removeRoleEmps = new List<LMS_Role_Employee__c>();
        
        if(roleEmp != null && roleEmp.size() > 0) {
	        String employeeId;
	        for(LMS_Role_Employee__c re : roleEmp) {
	            if(employeeId != re.Employee_Id__c) {
	                roleIds = new Set<Id>();
	                employeeId = re.Employee_Id__c;
	            }
	            if((re.Role_Id__r.Job_Class_Desc__r.Name != null && (re.Employee_Id__r.Job_Class_Desc__c != re.Role_Id__r.Job_Class_Desc__r.Name)) ||
	               (re.Role_Id__r.Job_Title__r.Job_Code__c != null && (re.Employee_Id__r.Job_Code__c != re.Role_Id__r.Job_Title__r.Job_Code__c)) ||
	               (re.Role_Id__r.Business_Unit__r.Name != null && (re.Employee_Id__r.Business_Unit_Desc__c != re.Role_Id__r.Business_Unit__r.Name)) ||
	               (re.Role_Id__r.Department__r.Name != null && (re.Employee_Id__r.Department__c != re.Role_Id__r.Department__r.Name)) ||
	               (re.Role_Id__r.Region__r.Region_Name__c != null && (re.Employee_Id__r.Country_Name__r.Region_Name__c != re.Role_Id__r.Region__r.Region_Name__c)) ||
	               (re.Role_Id__r.Country__r.Name != null && (re.Employee_Id__r.Country_Name__r.Name != re.Role_Id__r.Country__r.Name)) ||
	               (re.Role_Id__r.Employee_Type__r.Name != null && (empStatus.get(re.Employee_Id__r.Status__c) != re.Role_Id__r.Employee_Type__r.Name)) ||
	               (excludeStatus.containsKey(re.Employee_Id__r.Status__c))) {
	                system.debug('----------------------Role Does Not Match-----------------------');
	                if(re.Status__c != statusDraft) {
	                    re.Status__c = statusRemoved;
	                    removeRoleEmps.add(re);
	                } else {
	                    deleteRoleEmps.add(re);
	                }
	            } else {
	                system.debug('----------------------Role Already Exists-----------------------');
	                roleIds.add(re.Role_Id__c);
	            }
	            empRoles.put(re.Employee_Id__c, roleIds);
	        }
        }
    	
        List<LMS_Role_Employee__c> newMappings = new List<LMS_Role_Employee__c>();
        List<LMS_Role__c> rolesToCheck = 
        [SELECT Job_Class_Desc__r.Name, Job_Title__r.Job_Code__c, Business_Unit__r.Name, Department__r.Name, Region__r.Region_Name__c, Country__r.Name, 
         Employee_Type__r.Name, Role_Name__c FROM LMS_Role__c WHERE Role_Type__r.Name = :rolePRA AND Status__c = :roleActive
         AND (Job_Class_Desc__r.Name IN :jobFamily OR Job_Title__r.Job_Code__c IN :jobTitle OR Business_Unit__r.Name IN :unit OR 
              Department__r.Name IN :department OR Region__r.Region_Name__c IN :regionName OR Country__r.Name IN :countryName OR 
              Employee_Type__r.Name IN :types)];
        system.debug('------------------New Role Check----------------------'+rolesToCheck);
        system.debug('------------------Emp Roles----------------------'+empRoles);
        for(Employee_Details__c e : trigger.new) {
        	if (updatedEmployeeIds.contains(e.Id)) {
	            for(LMS_Role__c r : rolesToCheck) {
	            	system.debug('--------------------- checking role : ' + r.Role_Name__c + ' --------------------------');
	                if((((r.Job_Class_Desc__c != null && (e.Job_Class_Desc__c == r.Job_Class_Desc__r.Name)) || (r.Job_Class_Desc__r.Name == null)) &&
	                    ((r.Job_Title__c != null && (e.Job_Code__c == r.Job_Title__r.Job_Code__c)) || (r.Job_Title__c == null)) &&
	                    ((r.Business_Unit__c != null && (e.Business_Unit_Desc__c == r.Business_Unit__r.Name)) || (r.Business_Unit__c == null)) &&
	                    ((r.Department__c != null && (e.Department__c == r.Department__r.Name)) || (r.Department__c == null)) &&
	                    ((r.Region__c != null && (regionMap.get(e.Country_Name__c) == r.Region__r.Region_Name__c)) || (r.Region__c == null)) &&
	                    ((r.Country__c != null && (countryMap.get(e.Country_Name__c) == r.Country__r.Name)) || (r.Country__c == null)) &&
	                    ((r.Employee_Type__c != null && (empStatus.get(e.Status__c) == r.Employee_Type__r.Name)) || (r.Employee_Type__c == null)) &&
	                    ((!excludeStatus.containsKey(e.Status__c)) && ((empRoles.isEmpty()) || (empRoles.get(e.Id) != null && !empRoles.get(e.Id).contains(r.Id)))))) {
	                    system.debug('----------------added role with name : ' + r.Role_Name__c + ' ----------------------');
	                    if(e.Date_Hired__c > Date.today()) {
		                    newMappings.add(new LMS_Role_Employee__c(Role_Id__c = r.Id, Employee_Id__c = e.Id, Status__c = statusPendingAdd,
		                    	Created_On__c = System.today(), Updated_On__c = System.today(), Sync__c = outSync));
	                    } else {
		                    newMappings.add(new LMS_Role_Employee__c(Role_Id__c = r.Id, Employee_Id__c = e.Id, Status__c = statusDraft,
		                    	Created_On__c = System.today(), Updated_On__c = System.today(), Sync__c = outSync));
	                    }
	                } else {
	                    system.debug('-------------------Role with name : ' + r.Role_Name__c + ' does not match--------------------');
	                }
	            }
        	}
        }
	        
        try {
            insert newMappings;
            update removeRoleEmps;
            delete deleteRoleEmps;
        } catch(DMLException e) {
            system.debug(e.getMessage());
        }
    }
}