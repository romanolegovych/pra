trigger PAWS_ProjectFlowDocument on PAWS_Project_Flow_Document__c (after delete, after insert, after update, before delete, before insert, before update)
{
	new PAWS_ProjectFlowDocumentTrigger().execute();
}