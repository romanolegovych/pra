trigger PAWS_Flow on STSWR1__Flow__c (after delete, after insert, after update,
				before delete, before insert, before update)
{
	new PAWS_FlowTrigger().executeIgnoreWorkflow();
	if (Trigger.isBefore && Trigger.isDelete)
	{
		PAWS_FlowTrigger.cleanupEWs(Trigger.old);
	}
}