trigger RM_HandleProjectStatusChange on WFM_Project__c (after update) {
    
   Set<ID> projectIDs =new Set<ID>();
    Set<ID> closePIDs =new Set<ID>();
    RM_projectStatusChangeHandler handler = new RM_projectStatusChangeHandler(Trigger.isExecuting, Trigger.size);
    for (WFM_Project__c proj : trigger.new){
        if(Trigger.isAfter && Trigger.isUpdate){
            if (proj.Status_Desc__c != trigger.oldMap.get(proj.id).Status_Desc__c ){                
                projectIDs.add(proj.id);
                handler.notifychangeproject(proj.id,trigger.oldMap.get(proj.id).Status_Desc__c,proj.Status_Desc__c,'Status');
                System.debug('***** in Status If Condition ****' +proj);
                if (proj.Status_Desc__c=='Closed' || proj.Status_Desc__c == 'Lost') { 
                    closePIDs.add(proj.id);
                }                   
          }
          if (proj.Project_End_Date__c != trigger.oldMap.get(proj.id).Project_End_Date__c){
              RM_projectStatusChangeHandler notifyHandler = new RM_projectStatusChangeHandler(Trigger.isExecuting, Trigger.size);
              handler.notifychangeproject(proj.id,String.valueOf(trigger.oldMap.get(proj.id).Project_End_Date__c),String.valueOf(proj.Project_End_Date__c),'EndDate');
              System.debug('***** in End Date If Condition ****');
            }
        }
    }if(closePIDs != null){
        handler.statusClosedOrLost(closePIDs);
        }
    if(projectIDs != null){
        handler.projStatusChanges(projectIDs);           
    }    
}