trigger BDT_StudyService_Trigger on StudyService__c ( before insert, after insert, after update){
	
	if(trigger.isBefore) {
		if(trigger.isInsert) {
			for (StudyService__c ss : trigger.new) {
				// set unique to it's value to ensure no duplicate records can be created
				ss.Unique__c = '' + ss.ProjectService__c + ':' + ss.Study__c;
			}
		}
	} else {
		if(trigger.isInsert){
			BDT_StudyServicePricingCreation sspc = new BDT_StudyServicePricingCreation();
			sspc.updateStudyServicePricing( trigger.new, new list<StudyService__c>() );
		}
		if(trigger.isUpdate){
			BDT_StudyServicePricingCreation sspc = new BDT_StudyServicePricingCreation();
			sspc.updateStudyServicePricing( trigger.new, trigger.old );
		}
	}
	
}