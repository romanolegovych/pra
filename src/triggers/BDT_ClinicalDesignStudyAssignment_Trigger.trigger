trigger BDT_ClinicalDesignStudyAssignment_Trigger on ClinicalDesignStudyAssignment__c (before delete, after delete, after update) {

	if (trigger.isDelete && trigger.isBefore) {
		
		
		Set<ID> idSet = trigger.oldMap.keySet();
		// remove child-child grouparmassignment so to trigger calculations
		delete [select id
				from GroupArmAssignment__c
				where StudyDesignPopulationAssignment__r.ClinicalDesignStudyAssignment__c in :idSet];
		// remove child study design population assignments
		delete [select id
				from StudyDesignPopulationAssignment__c
				where ClinicalDesignStudyAssignment__c in :idSet];

	}

}