trigger AccountTrigger on Account (after insert, after update, before insert, before update) {
	
	List<String> handlerPrefix = new List<String>();
	handlerPrefix.add('RM');
	handlerPrefix.add('CM');
	
	// This is the only line of code that is required.
	TriggerFactory.createTDWithMultipleHandlers(null,Account.sObjectType,handlerPrefix);
}