trigger PAWS_ProjectFlow on ecrf__c (after delete, after insert, after update, before delete, before insert, before update)
{
	new PAWS_ProjectFlowTrigger().execute();
}