/**
@author 
@date 2014
@description this trigger to validate calendars
**/
trigger SRM_ValidateModelCalendarsTrigger on SRM_Calender_Adjustments__c (before insert, before update) {
    Boolean isValid = true; //SRM_ValidateModelCalendars.validateCalendars(Trigger.New, TRigger.oldMap);
    String errorReason = '';
    //Check for to and from date
    for(SRM_Calender_Adjustments__c calAd: Trigger.New){
        if(calAd.To_Date__c < calAd.From_Date__c){
            calAd.addError('End Date cannot be less than Start Date');
        }
    }
    if(Trigger.isInsert){
        errorReason = SRM_ValidateModelCalendarsTriggerHandler.validateCalendars(Trigger.New, new Map<Id, SRM_Calender_Adjustments__c >());
    }
    else if(Trigger.isUpdate){
        errorReason = SRM_ValidateModelCalendarsTriggerHandler.validateCalendars(Trigger.New, Trigger.oldMap);
    }
    if(errorReason == 'OVERLAP'){
        for(SRM_Calender_Adjustments__c cal: Trigger.New){
            cal.addError('The Start or End date overlap with adjustments already defined.');
        }
    }
    else if(errorReason == 'WEEKEND'){
        for(SRM_Calender_Adjustments__c cal: Trigger.New){
            cal.addError('Please select the weekdays for calendar adjustments.');
        }
    }
    else{
    }
}