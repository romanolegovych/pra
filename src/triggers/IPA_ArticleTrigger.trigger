trigger IPA_ArticleTrigger on IPA_Articles__c (before insert, before update, after insert, after update) {
    //constants
    static final string DEFAULTIMAGEURL = '/resource/1386872813000/IPA_InsidePRA_Logo'; //'http://placekitten.com/470/306';
    static final integer MAXIMAGES = 5;
    static final string MAXIMAGESERROR = 'Only ' + MAXIMAGES + ' images are allowed per article.';
    static final string MAXFEATUREDIMAGEERROR = 'Only one featured image is allowed per article.';
    static final string LOCALSALESFORCEIMAGEERROR = 'Please upload an image to salesforce vs linking to a non salesforce image.';
    static final string IMAGEURLLENGHTERROR = 'Image URLs must be less than 255 characters.';
    static final string PAGEHASERRORS = 'Please correct the errors noted below.';
    //member vars
    set<Id> insertedArticlesSet = new set<Id>();
    list<IPA_Articles__c> insertedArticlesList = new list<IPA_Articles__c>();        
    //trigger loop
    for(IPA_Articles__c articleObj:Trigger.new){
        if(Trigger.isBefore && (Trigger.isInsert||Trigger.isUpdate)){
            //check the featured image field for content
            String featuredSource = articleObj.Featured_Picture__c;
            Integer featuredSourceImageCount = featuredSource != null ? featuredSource.countMatches('<img') : 0 ;
            //check the pictures field for content
            String pictureSource = articleObj.Pictures__c;
            Integer pictureSourceImageCount = pictureSource != null ? pictureSource.countMatches('<img') : 0 ;        
            //vars for url string calc
            Integer imageStartIndex;
            Integer imageEndIndex; 
            Integer imagesProcessedCount = 0;
            String imageUrl;        
            //Validate only 5 images have been added to the pictures__c field      
            if(pictureSourceImageCount > MAXIMAGES){
                articleObj.adderror('');
                articleObj.Pictures__c.adderror(MAXIMAGESERROR);
            }
            //Validate only 1 image has been added to the featured_image__c field
            if(featuredSourceImageCount > 1){
                articleObj.adderror('');
                articleObj.Featured_Picture__c.adderror(MAXFEATUREDIMAGEERROR);
            }
            //Clear out previous image links
            articleObj.Featured_Image__c = '';
            articleObj.Image_1__c = '';
            articleObj.Image_2__c = '';
            articleObj.Image_3__c = '';
            articleObj.Image_4__c = '';
            articleObj.Image_5__c = '';
            //Set image links by parsing out img tags from the featured_image__c and picture__c field contents       
            if(featuredSourceImageCount == 0 && pictureSourceImageCount == 0 ){
                articleObj.Featured_Image__c = DEFAULTIMAGEURL;
            }else{ 
                if(featuredSourceImageCount == 1){
                    imageStartIndex = featuredSource.indexOf('<img');
                    if(imageStartIndex != null){
                        try{
                            //build the featured image url 
                            imageEndIndex = featuredSource.indexOf('>', imageStartIndex) + 1;
                            imageUrl = featuredSource.substring(imageStartIndex, imageEndIndex);                        
                            imageStartIndex = imageUrl.indexOf('src=') + 5;
                            imageEndIndex = imageUrl.indexOf('"', imageStartIndex + 1);
                            String url = imageUrl.substring(imageStartIndex, imageEndIndex).replace('&amp;','&');
                            if(url.length() > 255){ 
                                articleObj.adderror(PAGEHASERRORS);
                                articleObj.Featured_Picture__c.adderror('The URL of the Featured Image has an error. (' + IMAGEURLLENGHTERROR + ')');
                            }
                            articleObj.Featured_Image__c =  url;
                        }catch(Exception ex){
                            articleObj.Featured_Image__c = DEFAULTIMAGEURL;
                        }
                    }
                }
                while(imagesProcessedCount < pictureSourceImageCount){
                    imageStartIndex = pictureSource.indexOf('<img');
                    if(imageStartIndex != null){
                        try{
                            //build the image url starting and ending points 
                            imageEndIndex = pictureSource.indexOf('>', imageStartIndex) + 1;
                            imageUrl = pictureSource.substring(imageStartIndex, imageEndIndex);                        
                            imageStartIndex = imageUrl.indexOf('src=') + 5;
                            imageEndIndex = imageUrl.indexOf('"', imageStartIndex + 1);
                            String url = imageUrl.substring(imageStartIndex, imageEndIndex).replace('&amp;','&');
                            //validate the image url length as less than 255 characters
                            integer location = imagesProcessedCount + 1;
                            if(url.length() > 255){ 
                                articleObj.adderror(PAGEHASERRORS);
                                articleObj.Pictures__c.adderror('The URL of the Image at position ' +location + ' has an error. (' + IMAGEURLLENGHTERROR + ')');
                            }    
                            //assign the image url found to the appropriate image field  
                            if(imagesProcessedCount == 0){
                                if(featuredSourceImageCount == 0){
                                    articleObj.Featured_Image__c =  url;
                                }
                                articleObj.Image_1__c = url;
                            }else if(imagesProcessedCount == 1){
                                articleObj.Image_2__c = url;
                            }else if(imagesProcessedCount == 2){
                                articleObj.Image_3__c = url;                       
                            }else if(imagesProcessedCount == 3){
                                articleObj.Image_4__c = url;
                            }else if(imagesProcessedCount == 4){
                                articleObj.Image_5__c = url;
                            }
                            //trim off the image string already parsed from the picture source field before starting the next loop iteration    
                            imageEndIndex = pictureSource.indexOf('<', imageEndIndex) + 6;
                            pictureSource = pictureSource.substring(imageEndIndex, pictureSource.length());                        
                            //increment the images processed count
                            imagesProcessedCount++;        
                        }catch(Exception ex){
                            articleObj.Featured_Image__c = DEFAULTIMAGEURL;
                        }
                    }
                }
            }
        }else if(Trigger.isAfter){            
            
            if(Trigger.isInsert || Trigger.isUpdate){
                //validate image urls contain the file's eid
                if(articleObj.Featured_Image__c != null && articleObj.Featured_Image__c != DEFAULTIMAGEURL){
                    if(!articleObj.Featured_Image__c.contains('refid')) articleObj.Featured_Picture__c.adderror('The URL of the Featured Image has an error. (' + LOCALSALESFORCEIMAGEERROR + ')'); 
                }
                if(articleObj.Image_1__c != null){
                    if(!articleObj.Image_1__c.contains('refid')) articleObj.Pictures__c.adderror('The URL of the Image at position 1 has an error. (' + LOCALSALESFORCEIMAGEERROR + ')');
                }
                if(articleObj.Image_2__c != null){ 
                    if(!articleObj.Image_2__c.contains('refid')) articleObj.Pictures__c.adderror('The URL of the Image at position 2 has an error. (' + LOCALSALESFORCEIMAGEERROR + ')'); 
                }
                if(articleObj.Image_3__c != null){ 
                    if( !articleObj.Image_3__c.contains('refid')) articleObj.Pictures__c.adderror('The URL of the Image at position 3 has an error. (' + LOCALSALESFORCEIMAGEERROR + ')');
                }
                if(articleObj.Image_4__c != null){ 
                    if( !articleObj.Image_4__c.contains('refid')) articleObj.Pictures__c.adderror('The URL of the Image at position 4 has an error. (' + LOCALSALESFORCEIMAGEERROR + ')'); 
                }
                if(articleObj.Image_5__c != null){ 
                    if( !articleObj.Image_5__c.contains('refid')) articleObj.Pictures__c.adderror('The URL of the Image at position 5 has an error. (' + LOCALSALESFORCEIMAGEERROR + ')'); 
                }
            } 
            if(Trigger.isInsert){
                //double save inserts to properly capture image urls from rich text fields
                insertedArticlesSet.add(articleObj.Id);  
            }       
        }
    }
    system.debug('insertedArticlesSet >>> ' +insertedArticlesSet);
    if(!insertedArticlesSet.isEmpty()){
        try{    
            insertedArticlesList = [Select Id FROM IPA_Articles__c WHERE Id in : insertedArticlesSet];
            system.debug('insertedArticlesList >>> ' +insertedArticlesList);
            update insertedArticlesList;
        } 
        catch(exception e){
            System.Debug('>>>>>>>>>>>>>>>>>>>>  Exception "re-saving" insterted Articles in IPA_ArticleTrigger: ' + e.getMessage());
        }
    }

}