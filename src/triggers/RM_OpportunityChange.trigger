trigger RM_OpportunityChange on Opportunity (before delete, after insert, after update) {
    RM_OpportunityTriggerHandler handler = new RM_OpportunityTriggerHandler();  
    
    if (Trigger.isInsert && Trigger.isAfter){
       handler.OpportunityOnAfterInsert(Trigger.new);
    }
    if (Trigger.isUpdate && Trigger.isAfter){
        handler.OpportunityOnAfterUpdate(Trigger.oldMap, Trigger.newMap);
    }
    if (Trigger.isDelete && Trigger.isBefore){
        handler.OpportunityOnBeforeDelete(Trigger.old, Trigger.oldMap);
    }
}