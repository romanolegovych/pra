trigger LMS_Update_RC_UK on LMS_Role__c (after update) {
	
	Set<Id> roleIds = new Set<Id>();
	
	if(trigger.isAfter && trigger.isUpdate) {
		
		for(LMS_Role__c role : trigger.new) {
			if(role.SABA_Role_PK__c != null) {
				roleIds.add(role.Id);
			}
		}
		
		List<LMS_Role_Employee__c> roleEmps = 
		[SELECT Id, Role_Employee_Unique_Key__c FROM LMS_Role_Employee__c WHERE Role_Id__c IN :roleIds AND (NOT Role_Employee_Unique_Key__c LIKE 'roles%')];
		
		List<LMS_Role_Course__c> roleCourse = 
		[SELECT Id, Role_Course_Unique_Key__c FROM LMS_Role_Course__c WHERE Role_Id__c IN :roleIds AND (NOT Role_Course_Unique_Key__c LIKE 'roles%')];
		
		update roleCourse;
		update roleEmps;
	}
}