trigger PAWS_WFMSite on WFM_Site_Detail__c (after delete, after insert, after update, 
			before delete, before insert, before update)
{
	new PAWS_WFMSiteTrigger().execute();
}