trigger BDT_FlowChartAssignment_Trigger on flowchartassignment__c (before delete, after insert, after update) {

	if (Trigger.isBefore && Trigger.isDelete) {
		// upon delete remove childs
		delete [select id from flowchartservicetotal__c where flowchartassignment__c = :trigger.old];
	}

	if (Trigger.isAfter && (Trigger.isUpdate || Trigger.isInsert)) {
		// recalculate affected flowchartservicetotals
		Set<ID> FlowChartIDs = new Set<ID>();
		Set<ID> ArmIDs = new Set<ID>();

		for (flowchartassignment__c p : Trigger.new) {
			FlowChartIDs.add(p.flowchart__c);
			ArmIDs.add(p.arm__c);
		}

		if (FlowChartIDs.size()>0 || ArmIDs.size()>0){
			BDT_FlowchartServiceTotal.updateFlowchartServiceTotal(ArmIDs,FlowChartIDs);
		}
	}
	
	
	
}