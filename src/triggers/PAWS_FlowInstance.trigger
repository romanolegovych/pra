trigger PAWS_FlowInstance on STSWR1__Flow_Instance__c (before insert, after insert, before update, after update) {
	new PAWS_FlowInstanceTrigger().executeIgnoreWorkflow();
}