/**
*	Trigger on FinancialDocument__c
*	@author   Dimitris Sgourdos, Maurice Kremer
*	@version  13-Sep-2013
*/
trigger BDT_StudyServicePricing_Trigger on StudyServicePricing__c (
	after delete, 
	after insert,
	after update
) {

	/*
	 * Start for maintaining FinancialCategoryTotal__c and FinancialCatTotPrices__c
	 */
	Set<String> FinancialDocumentIDs = new Set<String>();
	
	if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate || Trigger.isDelete)) {
		
		List<StudyServicePricing__c> listOfObjects;
		if (Trigger.isDelete) {
			listOfObjects = Trigger.old;
		} else {
			listOfObjects = Trigger.new;
		}
		
		for (StudyServicePricing__c s:listOfObjects) {
			if (s.ApprovedFinancialDocument__c!=null) {
				FinancialDocumentIDs.add(s.ApprovedFinancialDocument__c);
			}
			if (s.FinancialDocumentIDs__c!=null) {
				FinancialDocumentIDs.addAll(s.FinancialDocumentIDs__c.split(':'));
			}
		}
	}

	
	if (Trigger.isUpdate){
		For (Integer i = 0; i < Trigger.old.size(); i++) {
			List<String> oldFDids = new List<String>();
			if ( Trigger.old[i].FinancialDocumentIDs__c != null) {
				oldFDids = Trigger.old[i].FinancialDocumentIDs__c.split(':');
			}
			 
			StudyServicePricing__c newValue = Trigger.newMap.get(Trigger.old[i].Id);
			List<String> newFDids = new List<String>();
			if (newValue.FinancialDocumentIDs__c != null) {
				newFDids = newValue.FinancialDocumentIDs__c.split(':');
			}
			
			Set<String> itemsInOldNotInNew = new Set<String>();
			itemsInOldNotInNew.addAll(oldFDids);
			itemsInOldNotInNew.removeAll(newFDids);
			
			Set<String> itemsInNewNotInOld = new Set<String>();
			itemsInNewNotInOld.addAll(newFDids);
			itemsInNewNotInOld.removeAll(oldFDids);
			
			FinancialDocumentIDs.addAll(itemsInNewNotInOld);
			FinancialDocumentIDs.addAll(itemsInOldNotInNew);
			
		}
	}

	for (String fdID : FinancialDocumentIDs) {
	//	Try { 
			Id a = Id.valueOf(fdID);
			BDT_FinancialCategoryTotalUtils.refreshFinancialCategoryTotal(ID.valueOf(fdID));			
	//	} catch (Exception e) {
			// skip empty items
	//	}
		
	}

}