trigger IPA_LinkTopic_Trigger on IPA_Link_Topic__c (before insert, before Update) {

    for(IPA_Link_Topic__c loopvar : Trigger.New)
    {   
        String RecordType = 'Quick Link';
        Integer MaxLimit = 10;        
        IPA_Links__c LinkRecord = [SELECT Id, RecordType.Name FROM IPA_Links__c WHERE Id =: loopvar.Link__c];
        system.debug('LinkRecord : ' +LinkRecord );        
        
        if(LinkRecord.RecordType.Name == RecordType )
        {        
            List<IPA_Link_Topic__c> lists = [SELECT Id, Link__c, Topic__c
                                             FROM IPA_Link_Topic__c 
                                             WHERE Link__r.RecordType.Name =: RecordType AND Topic__c =: loopvar.Topic__c];
                                          
            if(Trigger.isInsert)
            {   
                if(lists.size() >= MaxLimit)
                    loopvar.adderror('Only 10 Quick Links are allowed per Topic');  
            }
                      
            if(Trigger.isUpdate)
            {   
                 IPA_Link_Topic__c oldLinkTopicObj = System.Trigger.oldMap.get(loopvar.Id);
                 if(oldLinkTopicObj.Topic__c != loopvar.Topic__c)
                 {
                     if(lists.size() >= MaxLimit)
                     loopvar.adderror(' Already 10 Quick Links are exists for the Topic');   
                 }                 
                 /*if(oldRecordType != LinkRecord.RecordType.Name)
                 {
                     if(lists.size() >= MaxLimit)
                     loopvar.adderror(' Already 10 Quick Links are exists for the Topic');   
                 }*/
                 
            }        
        }
    }        
}