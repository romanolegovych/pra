trigger IPA_PageTrigger on IPA_Page__c (before insert, before update) {
	//vars
	IPA_Page__c masterLandingPageObj;
	string masterLandingPageError;
	try{
		masterLandingPageObj = [SELECT Id, Name
			   				    FROM IPA_Page__c					   						
	           	   			    WHERE Master_Landing_Page__c = true
	           				    LIMIT 1
               				   ];
		masterLandingPageError = 'There may be only one Page record flagged as the Master Landing Page. The current Master Landing Page is record <a href="/' + masterLandingPageObj.Id + '">' + masterLandingPageObj.Id + '</a>.';
	}catch(Exception e){
		System.Debug('>>>>>>>>>>>>>>>>>>>>  Exception fectching Master Landing Page from IPA_Page: ' + e.getMessage());
	}
	
	//validate the Master Landing Page has not already been set
    void validateMasterLandingPage(IPA_Page__c pageObj){
        if(masterLandingPageObj != null){
    		pageObj.adderror('');
   			pageObj.Master_Landing_Page__c.adderror(masterLandingPageError,false);
    	}else{
			masterLandingPageObj = pageObj; 
    	}
    }	
	
	//trigger loop       
    for(IPA_Page__c pageObj :Trigger.new){
   		if(Trigger.IsInsert){ 
        	if(pageObj.Master_Landing_Page__c == true){
				validateMasterLandingPage(pageObj);
        	}
        } 
        if(Trigger.IsUpdate){
            IPA_Page__c oldPageObj = System.Trigger.oldMap.get(pageObj.Id);    
            if(oldPageObj.Master_Landing_Page__c == false && pageObj.Master_Landing_Page__c == true){
	            validateMasterLandingPage(pageObj);   
            }
    	}
    }
}