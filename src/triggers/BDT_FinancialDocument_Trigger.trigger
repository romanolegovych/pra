/**
*	Trigger on FinancialDocument__c
*	@author   Dimitris Sgourdos, Maurice Kremer
*	@version  13-Sep-2013
*/
trigger BDT_FinancialDocument_Trigger on FinancialDocument__c (after update, after insert) {
	
	/* Clone previously assigned discounts and conversion rates for new or updated records
	 * that have a parent document assigned.
	 * @author	Maurice Kremer
	 * @version	13-Sep-2013
	 */
	if (Trigger.isAfter && (Trigger.isInsert || trigger.isUpdate)) {
		
		Map<String,Set<ID>> FinancialDocMap = new Map<String,Set<ID>>();
		Set<String> FinancialDocIds = new Set<String>();
		
		for (FinancialDocument__c fd : Trigger.new) {
			// if the record has a parent financial document id
			if (fd.FinancialDocumentParent__c == null) {
				// skip this itteration
				continue;
			}
			FinancialDocIds.add(fd.id);
			if (Trigger.isInsert && fd.listOfStudyIds__c != null) {
				Set<ID> studies = new Set<Id>();
				For (String studyId : fd.listOfStudyIds__c.split(':')) {
					try{
						studies.add(ID.valueOf(studyId));
					} catch (Exception e) {
						// skip empty or invalid values
					}
				}
				FinancialDocMap.put(fd.id,studies);
			} else if (trigger.isUpdate) {
				// get items that have been added to the new value
				Set<ID> studies = new Set<Id>();
				if (fd.listOfStudyIds__c != null) {
					For (String studyId : fd.listOfStudyIds__c.split(':')) {
						try{
							studies.add(ID.valueOf(studyId));
						} catch (Exception e) {
							// skip empty or invalid values
						}
					}
				}
				if (Trigger.oldMap.get(fd.Id).listOfStudyIds__c != null) {
					For (String studyId : Trigger.oldMap.get(fd.Id).listOfStudyIds__c.split(':')) {
						try{
							studies.remove(ID.valueOf(studyId));
						} catch (Exception e) {
							// skip empty or invalid values
						}
					}
				}
				FinancialDocMap.put(fd.id,studies);
			}
		}
		// process each document that was found newly added 
		for (String financialDocumentId:FinancialDocIds) {
			if (FinancialDocMap.containsKey(financialDocumentId) && FinancialDocMap.get(financialDocumentId).size() > 0) {
				BDT_DC_FinancialDocument.replicatePriceFromParent(financialDocumentId,FinancialDocMap.get(financialDocumentId));
			}
		}
	}
	
	
}