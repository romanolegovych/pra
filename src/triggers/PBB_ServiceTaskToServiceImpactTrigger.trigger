/**
@author Niharika Reddy
@date 2015
@description this Restricting Service task to service impact records if if service model staus is approved or retired.
**/
trigger PBB_ServiceTaskToServiceImpactTrigger on Service_Task_To_Service_Impact__c (before delete) {
    for(Service_Task_To_Service_Impact__c mo : Trigger.old){
       if(mo.Service_Mod_Status__c == 'Approved' || mo.Service_Mod_Status__c == 'Retired' )
           mo.addError('UNABLE TO DELETE RECORD BECAUSE SERVICE MODEL IS APPROVED/RETIRED!');
     }
}