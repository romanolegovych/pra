trigger PAWS_FlowInstanceCursor on STSWR1__Flow_Instance_Cursor__c (before insert, after insert, before update, after update) 
{
	if(Trigger.isBefore) PAWS_Utilities.updatePAWSFlag(Trigger.isdelete ? Trigger.old : Trigger.new);
	
	new PAWS_FlowInstanceCursorTrigger().executeIgnoreWorkflow();
}