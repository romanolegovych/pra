/**
* @description:  Trigger Class for the Hour object.
*                Delegates work to the HourTriggerHandler class.
*/
trigger PRA_Hour_EffortRatio on Hour_EffortRatio__c (after insert, after update, before insert, before update) {

   //ASA
   PRA_Hour_ER_TriggerHandler handler = new PRA_Hour_ER_TriggerHandler(Trigger.isExecuting, Trigger.size);
   
   ////Commenting it out as we dont need it for now
   /*
   if(Trigger.isInsert && Trigger.isBefore){
       //handler.OnBeforeInsert(Trigger.new);
       
   }
   else if(Trigger.isInsert && Trigger.isAfter){
        //handler.OnAfterInsert(Trigger.newMap);       
   }  
   else if(Trigger.isUpdate && Trigger.isBefore){
       //handler.OnBeforeUpdate(Trigger.oldMap,Trigger.newMap);
   }
   else
   */
   if(Trigger.isUpdate && Trigger.isAfter){       
       handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
   }
   /*
   //Commenting it out as we dont need it for now
   else if(Trigger.isDelete && Trigger.isBefore){
       //handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
   }
   else if(Trigger.isDelete && Trigger.isAfter){
       //handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
   }
   
   else if(Trigger.isUnDelete){
       //handler.OnUndelete(Trigger.new);  
   }
   */

}