trigger HR_EmployeeLawsonReasonUpdate on HR_Employee_Details__c (before insert,before update) {
  Map<string,string> custommap=new Map<string,string>(); 
  Map<ID,string> emplistmap=new Map<ID,string>(); 
  Map<ID,string> childmap=new Map<ID,string>();  
  for(HRLawsonReasonMapping__c cus:HRLawsonReasonMapping__c.getAll().values()){
     custommap.put(cus.Lawson_Code__c,cus.HR_Reason__c);      
  } 
  for(HR_Employee_Details__c hrlist:trigger.new){
     emplistmap.put(hrlist.id,hrlist.Interviewed__c);
  }
  List<HR_Exit_Interview__c> exitlist=new List<HR_Exit_Interview__c>([select id,Interviewed__c,HR_Employee_Details__c from HR_Exit_Interview__c where HR_Employee_Details__c IN:emplistmap.keyset() order by lastmodifieddate desc]);
  if(exitlist.size()>0){
        for(HR_Exit_Interview__c exlst:exitlist){
            childmap.put(exlst.HR_Employee_Details__c,exlst.Interviewed__c);
        }
  } 
 if (Trigger.isBefore) { 
      for(HR_Employee_Details__c hre:trigger.new){
         if(hre.Reason_Code_1__c!=null && custommap.containskey(hre.Reason_Code_1__c)){
            hre.HR_Lawason_Code_1__c=custommap.get(hre.Reason_Code_1__c);
         }
         if(hre.Reason_Code_2__c!=null && custommap.containskey(hre.Reason_Code_2__c)){
            hre.HR_Lawason_Code_2__c=custommap.get(hre.Reason_Code_2__c);
         }
         datetime limitdate=datetime.newInstance(2013,10,01);
         if((hre.Term_Date__c<limitdate || hre.Pending_Term_Date__c<limitdate) && (hre.Interviewed__c==null || hre.Interviewed__c=='')){
           hre.Interviewed__c='';
         }
         else if(((hre.Term_Date__c>limitdate || hre.Pending_Term_Date__c>limitdate) && (hre.Interviewed__c==null || hre.Interviewed__c=='')) || (!childmap.containskey(hre.id))){
           hre.Interviewed__c='Pending';
         }
         else if(childmap.containskey(hre.id)){
            hre.Interviewed__c=childmap.get(hre.id);
         }
      }
  
  }

}