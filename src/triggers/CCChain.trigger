trigger CCChain on Critical_Chain__c (after delete, after insert, after update, 
			before delete, before insert, before update) {
	new CCChainTrigger().execute();
}