trigger PIT_CreateNewIncident_Trigger on Issue__c (after insert) {
    
    for (Issue__c issue : Trigger.new)
    {
        Incident__c incident = new Incident__c();
        
        incident.Incident_Source__c = issue.Category__c;
        
        
      //  incident.closeDateTime__c = DateTime.newInstance(issue.Date_Resolved__c, DateTime.Now().Time());
        
        incident.incidentDescription__c = issue.Issue_Type__c;
        incident.Ticket_Type__c = issue.Action_Taken__c;
                
        //incident.BMCServiceDesk__FKClient__c = issue.Person_with_auto_complete_del_del__c; //id User, id Contact
      //  incident.FKClient__c = UserInfo.getUserId();
        
      //  incident.BMCServiceDesk__FKCategory__c = 'a1DG0000001MCl9'; //required field, set to 'Training'        
    
        insert incident;   
    }
}