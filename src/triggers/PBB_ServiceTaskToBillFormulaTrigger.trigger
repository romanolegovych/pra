/**
@author Niharika Reddy
@date 2015
@description Restricting the deletion of Service Task To Drivers records if the model is approved or retired.
**/
trigger PBB_ServiceTaskToBillFormulaTrigger on Service_Task_To_Bill_Formula__c (before delete) {
    for(Service_Task_To_Bill_Formula__c mo : Trigger.old){
       if(mo.Service_Model_Status__c == 'Approved' || mo.Service_Model_Status__c == 'Retired' )
           mo.addError('UNABLE TO DELETE RECORD BECAUSE SERVICE MODEL IS APPROVED/RETIRED!');
     }
}