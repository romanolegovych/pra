/**
 * Created by illia on 12/4/15.
 */

trigger EWCategory on EW_Category__c (before insert, before update, before delete, after insert,
		after update, after delete, after undelete)
{
	new EWCategoryTrigger().execute();
}