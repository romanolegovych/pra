/**
@author Niharika Reddy
@date 2015
@description Restricting the deletion of Service task to formula records if the model is approved or retired.
**/
trigger PBB_ServiceTaskToDriverTrigger on Service_Task_To_Drivers__c (after insert, after update, before delete) {
    if (Trigger.isAfter){
        if (Trigger.isInsert || Trigger.isUpdate){
            PBB_ServiceTaskToDriversServices.checkForApprovedServices(Trigger.new);
        }
    }
    if (Trigger.isbefore){
        if (Trigger.isdelete){
            for(Service_Task_To_Drivers__c mo : Trigger.old){
               if(mo.Status__c == 'Approved' || mo.Status__c == 'Retired' )
                   mo.addError('UNABLE TO DELETE RECORD BECAUSE SERVICE TASK TO DRIVER IS APPROVED/RETIRED!');
            }
        }       
    }
}