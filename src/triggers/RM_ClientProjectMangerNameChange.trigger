/*    Trigger on Client Project to update the WFM_Project
*/

trigger RM_ClientProjectMangerNameChange on Client_Project__c (after update) {
    RM_CPNameChangeHandler rmHandler= new RM_CPNameChangeHandler(Trigger.isExecuting, Trigger.size);
    
    if(Trigger.isUpdate && Trigger.isAfter){
        for(Client_Project__c cp: Trigger.New){
            if(Trigger.NewMap.get(cp.Id).Director_of_Project_Delivery__c != null){
                if(Trigger.NewMap.get(cp.Id).Director_of_Project_Delivery__c != Trigger.OldMap.get(cp.Id).Director_of_Project_Delivery__c ||
                   Trigger.NewMap.get(cp.Id).Global_Project_Manager_Director__c != Trigger.OldMap.get(cp.Id).Global_Project_Manager_Director__c  ){
                    rmHandler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);        
                }
            }
        }    
    }

}