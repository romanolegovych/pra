trigger PAWS_FlowStep on STSWR1__Flow_Step_Junction__c (after delete, before delete) 
{
	new PAWS_FlowStepTrigger().executeIgnoreWorkflow();
}