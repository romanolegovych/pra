trigger PAWS_ProjectFlowSite on PAWS_Project_Flow_Site__c (after delete, after insert, after update, before delete, before insert, before update)
{
	new PAWS_ProjectFlowSiteTrigger().execute();
}