trigger LMS_HandleDeleteRoleEmpEvent on LMS_Role_Employee__c (after update) {

	Set<Id> roleEmpIds = new Set<Id>();
	
	Map<String, LMSConstantSettings__c> constants = LMSConstantSettings__c.getAll();
	static String statusDelete = constants.get('statusToDelete').Value__c;
	
	for(LMS_Role_Employee__c re : Trigger.new) {
		if(re.Status__c == statusDelete) {
			roleEmpIds.add(re.Id);
		}
	}
	
	if(roleEmpIds != null && roleEmpIds.size() > 0) {
		List<LMS_Role_Employee__c> roleEmps = [SELECT Status__c FROM LMS_Role_Employee__c WHERE Id IN :roleEmpIds];		
		try {
			delete roleEmps;
		} catch(DMLException e) {
			System.debug(e.getMessage());
		}
	}
}