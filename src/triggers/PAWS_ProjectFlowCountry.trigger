trigger PAWS_ProjectFlowCountry on PAWS_Project_Flow_Country__c (after delete, after insert, after update, before delete, before insert, before update)
{
	new PAWS_ProjectFlowCountryTrigger().execute();
}