trigger BDT_StudyDesignPopulationAssignment_Trigger on StudyDesignPopulationAssignment__c (after insert, after update, before delete) {

	// Start sanitization
	// Sanitize the data, this object should not be semi- or fully orphaned
	/*
	if(trigger.isUpdate || trigger.isInsert) {
		List<StudyDesignPopulationAssignment__c> orphanedList = new List<StudyDesignPopulationAssignment__c>();
		for(StudyDesignPopulationAssignment__c e : trigger.new) {
			if (e.ClinicalDesignStudyAssignment__c == null) {
				StudyDesignPopulationAssignment__c orphanedItem = new StudyDesignPopulationAssignment__c(ID=e.id);
				orphanedList.add(orphanedItem);
			}
		}
		delete orphanedList;
	}*/
	// End sanitization

	// when deleting also remove grouparmassignments, a manual delete is performed to esure the
	// trigger a run on the child object (https://success.salesforce.com/ideaView?id=08730000000BqSJAA0)
	if (trigger.isBefore && trigger.isDelete) {
		Set<ID> deletedIds = new Set<Id>();
		for (StudyDesignPopulationAssignment__c s : trigger.old) {
			deletedIds.add(s.id);
		}	 
		delete [select id from grouparmassignment__c where StudyDesignPopulationAssignment__c in :deletedIds];
	}


}