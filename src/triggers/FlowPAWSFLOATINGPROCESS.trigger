trigger FlowPAWSFLOATINGPROCESS on PAWS_Floating_Process__c (after delete, after insert, after update, before delete, before insert, before update)
{
	new STSWR1.AbstractTrigger().execute();
}