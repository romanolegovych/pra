/**
 * Created by illia on 12/9/15.
 */

trigger EWCategoryEWJunction on EW_Category_EW_Junction__c (before insert, before update, before delete,
		after insert, after update, after delete)
{
	new EWCategoryEWJunctionTrigger().execute();
}