trigger PRA_ClientProject on Client_Project__c (after insert) {
	PRA_ClientProjectTriggerHandler handler = new PRA_ClientProjectTriggerHandler();
	
	if (Trigger.isInsert && Trigger.isAfter){
		handler.onAfterInsert(Trigger.New);
	}
	
}