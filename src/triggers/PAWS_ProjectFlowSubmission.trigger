trigger PAWS_ProjectFlowSubmission on PAWS_Project_Flow_Submission__c (after delete, after insert, after update, before delete, before insert, before update)
{
	new PAWS_ProjectFlowSubmissionTrigger().execute();
}