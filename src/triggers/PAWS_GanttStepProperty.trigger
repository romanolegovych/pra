trigger PAWS_GanttStepProperty on STSWR1__Gantt_Step_Property__c (before insert, before update, after insert, after update) 
{
	new PAWS_GanttStepPropertyTrigger().executeIgnoreWorkflow();
}