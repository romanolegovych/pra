trigger BDT_ProjectServiceTimePoint_Trigger on ProjectServiceTimePoint__c (
	after delete, 
	after insert, 
	after undelete,
	after update,
	before update,
	before insert) {
	
	// Ensure TimePointCount__c matches the actual number of timepoints in listOfTimeMinutes__c
	if (Trigger.isBefore && (Trigger.isInsert || Trigger.isUpdate)) {
		for (ProjectServiceTimePoint__c p : Trigger.new) {
			p.TimePointCount__c = (String.IsBlank(p.listOfTimeMinutes__c))? NULL : p.listOfTimeMinutes__c.split(':').size();
		}
	}
	
	// Start sanitization
	// Sanitize the data, this object should not be semi- or fully orphaned (except for Timepoint__c which can be null)
	if(Trigger.isAfter && (trigger.isUpdate || trigger.isInsert)) {
		List<ProjectServiceTimePoint__c> orphanedList = new List<ProjectServiceTimePoint__c>();
		for(ProjectServiceTimePoint__c e : trigger.new) {
			if (e.ProjectService__c == null || e.Flowchart__c == null) {
				ProjectServiceTimePoint__c orphanedItem = new ProjectServiceTimePoint__c(ID=e.id);
				orphanedList.add(orphanedItem);
			}
    	}
		delete orphanedList;
    }
    // End sanitization

	// recalculate affected flowchartservicetotals
	Set<ID> FlowChartIDs = new Set<ID>();

	if (Trigger.isAfter && Trigger.isDelete) {
		for (ProjectServiceTimePoint__c p : Trigger.old) {
			FlowChartIDs.add(p.flowchart__c);
		}
	}
	if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate)) {
		for (ProjectServiceTimePoint__c p : Trigger.new) {
			FlowChartIDs.add(p.flowchart__c);
		}
	}
	
	/* has become obsolete with version 4.6 as the timepoint__c object has been factorred out 
	*
	* if (Trigger.isAfter && (Trigger.isInsert || Trigger.isUpdate || Trigger.isDelete)) {
	*	if(Trigger.isDelete) {
	*		BDT_Utils.updateListOfMinutesInProjectServiceTimePoint(Trigger.old);
	*	} else {
	*		BDT_Utils.updateListOfMinutesInProjectServiceTimePoint(Trigger.new);
	*	}
	*}
	*/
	
	
	if (FlowChartIDs.size()>0){
		BDT_FlowchartServiceTotal.updateFlowchartServiceTotal(new Set<id>(),FlowChartIDs);
	}
	
}