trigger HR_EmployeeTerminationReasons on HR_Exit_Interview__c (after insert,after update) {
  List<HR_Employee_Details__c> Listtoupdate =new List<HR_Employee_Details__c>();
  List<HR_Employee_Details__c> EmpListtoupdate =new List<HR_Employee_Details__c>();  
  Set<ID> empset=new Set<ID>();
  Map<ID,String> primaryreasons=new Map<ID,String>();
  Map<ID,String> secondaryreasons=new Map<ID,String>();  
  Map<ID,String> Interviewmap=new Map<Id,String>();
  for(HR_Exit_Interview__c hrint:trigger.new){
     empset.add(hrint.HR_Employee_Details__c);     
    if(hrint.Interviewed__c!=null){
        Interviewmap.put(hrint.HR_Employee_Details__c,hrint.Interviewed__c);
    }
  }
  Listtoupdate=[select id,HR_ReasonCode1__c,HR_ReasonCode2__c from HR_Employee_Details__c where id in:empset];
    
    if (Trigger.isAfter) { 
                
            for(HR_Exit_Interview__c hrqint:trigger.new){
                if(hrqint.Compensation_Benefits__c==true && hrqint.CompensationBenefits_SubCategories__c!=null && hrqint.PrimarySecondary_for_CompBenefit__c=='primary reason'){               
                    primaryreasons.put(hrqint.HR_Employee_Details__c,'Compensation/Benefits');
                }
                else if(hrqint.New_Career_Opportunity__c==true && hrqint.PrimarySecondary_Newcareeropporunity__c=='primary reason' && hrqint.NewCareer_Opportunity_SubCategories__c!=null){
                    primaryreasons.put(hrqint.HR_Employee_Details__c,'New Career Opportunity');
                }
                else if(hrqint.Personal__c==true && hrqint.PrimarySecondary_Personal__c=='primary reason' && hrqint.Personal_Subcategories__c!=null){
                     primaryreasons.put(hrqint.HR_Employee_Details__c,'Personal');
                }
                else if(hrqint.Relocation__c==true && hrqint.PrimarySecondary_Relocation__c=='primary reason' && hrqint.Relocation_Subcategories__c!=null){
                     primaryreasons.put(hrqint.HR_Employee_Details__c,'Relocation');
                }
                else if(hrqint.Others__c==true && hrqint.Primary_Secondary_Other__c=='primary reason' && hrqint.Others_Subcategories__c!=null){
                     primaryreasons.put(hrqint.HR_Employee_Details__c,'Others');
                }
                else if(hrqint.Result_of_Leadership__c==true && hrqint.Primary_Secondary_Result_of_Leadership__c=='primary reason' && hrqint.Result_of_Leadership_Subcategories__c!=null){
                     primaryreasons.put(hrqint.HR_Employee_Details__c,'Result of Leadership');
                }
                if(hrqint.Compensation_Benefits__c==true && hrqint.CompensationBenefits_SubCategories__c!=null && hrqint.PrimarySecondary_for_CompBenefit__c=='secondary reason'){
                     secondaryreasons.put(hrqint.HR_Employee_Details__c,'Compensation/Benefits');
                }
                else if(hrqint.New_Career_Opportunity__c==true && hrqint.PrimarySecondary_Newcareeropporunity__c=='secondary reason' && hrqint.NewCareer_Opportunity_SubCategories__c!=null){
                    secondaryreasons.put(hrqint.HR_Employee_Details__c,'New Career Opportunity');
                }
                else if(hrqint.Personal__c==true && hrqint.PrimarySecondary_Personal__c=='secondary reason' && hrqint.Personal_Subcategories__c!=null){
                     secondaryreasons.put(hrqint.HR_Employee_Details__c,'Personal');
                }
                else if(hrqint.Relocation__c==true && hrqint.PrimarySecondary_Relocation__c=='secondary reason' && hrqint.Relocation_Subcategories__c!=null){
                     secondaryreasons.put(hrqint.HR_Employee_Details__c,'Relocation');
                }
                else if(hrqint.Others__c==true && hrqint.Primary_Secondary_Other__c=='secondary reason' && hrqint.Others_Subcategories__c!=null){
                     secondaryreasons.put(hrqint.HR_Employee_Details__c,'Others');
                }
                else if(hrqint.Result_of_Leadership__c==true && hrqint.Primary_Secondary_Result_of_Leadership__c=='secondary reason' && hrqint.Result_of_Leadership_Subcategories__c!=null){
                     secondaryreasons.put(hrqint.HR_Employee_Details__c,'Result of Leadership');
                } 
                
            }                                           
            for(HR_Employee_Details__c emplist:Listtoupdate){
                if(primaryreasons.containsKey(emplist.id)){
                    emplist.HR_ReasonCode1__c=primaryreasons.get(emplist.id);                         
                }
                if(secondaryreasons.containskey(emplist.id)){
                    emplist.HR_ReasonCode2__c=secondaryreasons.get(emplist.id);                        
                }
                if(Interviewmap.containskey(emplist.id)){
                    emplist.Interviewed__c=Interviewmap.get(emplist.id);
                }
                
                EmpListtoupdate.add(emplist);
            }
            if(EmpListtoupdate.size()>0){
               update EmpListtoupdate;
            }
    }
}