trigger RM_HandleEmployeeChange on Employee_Details__c (after update) {
	list<emplChangeClass> EmplStatus =new list<emplChangeClass>();
	
    list<emplChangeClass> EmplTermDate =new list<emplChangeClass>();
    list<emplChangeClass> EmplRoles =new list<emplChangeClass> ();
    set<string> jobs = new set<string>();
    for (string job:RM_OtherService.getClinicalJobClass())
    	jobs.add(job);
    for (Employee_Details__c empl : trigger.new){
        if (empl.Status_Desc__c != trigger.oldMap.get(empl.id).Status_Desc__c && jobs.contains(empl.Job_Class_Desc__c)){
            //status change
         	if (empl.Status_Desc__c == 'Terminated' || trigger.oldMap.get(empl.id).Status_Desc__c.IndexOf('Active') > -1)
         	   EmplStatus.add(new emplChangeClass(empl,trigger.oldMap.get(empl.id)));
         	
        }
        else if  (empl.Term_Date__c != trigger.oldMap.get(empl.id).Term_Date__c && jobs.contains(empl.Job_Class_Desc__c)){
        	EmplTermDate.add(new emplChangeClass(empl,trigger.oldMap.get(empl.id)));
        	
        }
        else if (empl.Job_Class_Desc__c != trigger.oldMap.get(empl.id).Job_Class_Desc__c){
        	if (jobs.contains(empl.Job_Class_Desc__c) || jobs.contains(trigger.oldMap.get(empl.id).Job_Class_Desc__c)){
		 		EmplRoles.add(new emplChangeClass(empl,trigger.oldMap.get(empl.id)));
		 	}
        }
    }
    // status
    //need to filter out term date and termination status with no active assignment
    EmplTermDate = CheckActiveAssignment(EmplTermDate);
    EmplStatus = CheckActiveAssignment(EmplStatus);
    list<WFM_Email_Alert__c> lstEmail = new list<WFM_Email_Alert__c>();
    if (emplStatus.size() > 0){
    	for(emplChangeClass s: emplStatus)
    	  	 lstEmail.add(insertEmail('Status', s));	
    }
    if (EmplTermDate.size() > 0){
    	for(emplChangeClass t: EmplTermDate)
    	 	lstEmail.add(insertEmail('TermDate', t));
    }
    if (EmplRoles.size() > 0){
    	for(emplChangeClass r: EmplRoles)
    	 	lstEmail.add(insertEmail('Role', r));
    	 	
    }
    if (lstEmail.size() > 0)
    	insert lstEmail;
    
    public class emplChangeClass{
    	
    	public Employee_Details__c empl{get; private set;}
    	public Employee_Details__c oldEmpl{get; private set;}
    	public emplChangeClass(Employee_Details__c emp, Employee_Details__c oEmp){
    		empl = emp;
    		oldEmpl = oEmp;
    	}    
    }
    
    private WFM_Email_Alert__c insertEmail(string strType, emplChangeClass emplChange){ //'Status', 'TermDate', 'Role'
    	string body = '';
		if (strType=='Status'){
			
			body = 'The status was updated from ' + emplChange.oldEmpl.status_desc__c + ' to ' + emplChange.empl.status_desc__c +' for resource ' +  emplChange.empl.last_name__c + ', ' + emplChange.empl.first_name__c + '.';
			
		}
		else if (strType == 'TermDate'){
			if (emplChange.empl.term_date__c != null ){ 
				if (emplChange.oldEmpl.term_date__c == null)
					body = 'The termination date was updated to ' + RM_Tools.getFormattedDate(emplChange.empl.term_date__c, 'mm/dd/yyyy') +' for resource ' +  emplChange.empl.last_name__c + ', ' + emplChange.empl.first_name__c + '.';
			
				else
					body = 'The termination date was updated from ' + RM_Tools.getFormattedDate(emplChange.oldEmpl.term_date__c, 'mm/dd/yyyy') + ' to ' + RM_Tools.getFormattedDate(emplChange.empl.term_date__c, 'mm/dd/yyyy') +' for resource ' +  emplChange.empl.last_name__c + ', ' + emplChange.empl.first_name__c + '.'; 
				}
		}
		else if (strType == 'Role')
			 body = 'The role was updated from ' + emplChange.oldEmpl.Job_Class_Desc__c + ' to ' + emplChange.empl.Job_Class_Desc__c +' for resource ' +  emplChange.empl.last_name__c + ', ' + emplChange.empl.first_name__c + '.'; 
			
		
		string subject = 'Employee Change Notification';
		
		string header  = 'Resource Manager';
		string footer = 'Please review the resource\'s record to determine if modifications are needed to the resource\'s project assignments.';
		
		string address = RM_DataAccessor.GetConstant('RMGroupEmail');
		system.debug('---------address---------'+address);
		return new WFM_Email_Alert__c(Email_Body__c=body, Email_To__c =address, Subject__c=subject, header__c = header, footer__c = footer);
    		
    }
    private list<emplChangeClass> CheckActiveAssignment(list<emplChangeClass> lstEmps){
    	Map<ID, emplChangeClass> mapEmpl= new Map<ID, emplChangeClass>();
    	Set<ID> stEmpl = new Set<ID>();
    	for (emplChangeClass e: lstEmps){
    		stEmpl.add(e.empl.id);
    		mapEmpl.put(e.empl.id, e);
    	}
    	list<emplChangeClass> lstEmplOut = new list<emplChangeClass>();
    	Set<ID> stEmplOut = new Set<ID>();
    	for (WFM_employee_Allocations__c a: RM_AssignmentsService.GetActiveAssignmentsByListEmployee(stEmpl, 0)){
    		stEmplOut.add(a.employee_id__c);
    	}   	 
    	for (ID s: stEmplOut){
			lstEmplOut.add(mapEmpl.get(s));
			
		}	
    	return lstEmplOut;
    }
}