/**
* @description Trigger Class for the Unit object.
              Delegates work to the UnitTriggerHandler class.
*/
trigger PRA_Unit_Effort on Unit_Effort__c (after insert, after delete, before insert, before update, after update) {  //after delete, after undelete, after update, before delete, before insert, before update
   PRA_Unit_Effort_TriggerHandler handler = new PRA_Unit_Effort_TriggerHandler(Trigger.isExecuting, Trigger.size);
   
   
   //Commenting it out as we dont need it for now
   /*
   if(Trigger.isInsert && Trigger.isBefore){
       //handler.OnBeforeInsert(Trigger.new);
       
   }
   else if(Trigger.isInsert && Trigger.isAfter){
        //Commented - 08/07/2012
         //handler.OnAfterInsert(Trigger.newMap); 
       //UnitTriggerHandler.OnAfterInsertAsync(Trigger.newMap.keySet());
       
   }
  
   else if(Trigger.isUpdate && Trigger.isBefore){
       //handler.OnBeforeUpdate(Trigger.oldMap,Trigger.newMap);
   }
   else
   */
   if(Trigger.isUpdate && Trigger.isAfter){
        handler.OnAfterUpdate(Trigger.oldMap, Trigger.newMap);
   }
   
   /*//Commenting it out as we dont need it for now
   else if(Trigger.isDelete && Trigger.isBefore){
       //handler.OnBeforeDelete(Trigger.old, Trigger.oldMap);
   }
   else if(Trigger.isDelete && Trigger.isAfter){
       //handler.OnAfterDelete(Trigger.old, Trigger.oldMap);
   }
   
   else if(Trigger.isUnDelete){
       //handler.OnUndelete(Trigger.new);  
   }
   */
}