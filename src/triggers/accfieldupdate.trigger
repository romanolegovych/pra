trigger accfieldupdate on Task(before insert, before update) {

    if (Trigger.isInsert){
        for (Task ist : Trigger.new) {
            System.debug('field update check for acc/inst::' +ist +' Record Type::' +ist.recordtype);
            if(ist.recordtype==[Select id, name from recordtype where name='CTA']){
                ist.Account_Institution__c='New Test Value';
                insert ist;
            }
        }
    }

}