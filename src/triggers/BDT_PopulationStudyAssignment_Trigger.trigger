trigger BDT_PopulationStudyAssignment_Trigger on PopulationStudyAssignment__c (after insert, after update, before delete) {

	// when deleting also remove studydesignpopulationassignment, a manual delete is performed to esure the
	// trigger a run on the child object (https://success.salesforce.com/ideaView?id=08730000000BqSJAA0)
	if (trigger.isBefore && trigger.isDelete) {
		Set<ID> deletedIds = new Set<Id>();
		for (PopulationStudyAssignment__c s : trigger.old) {
			deletedIds.add(s.id);
		}	 
		delete [select id from studydesignpopulationassignment__c where PopulationStudyAssignment__c in :deletedIds];
	}

}