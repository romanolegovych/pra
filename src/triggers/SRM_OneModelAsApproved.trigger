/**
@author 
@date 2014
@description this trigger for model approved
**/
trigger SRM_OneModelAsApproved on SRM_Model__c (after insert, after update) {
    Integer count = [select count() from SRM_Model__c where Status__c = 'Approved'];
    if(count > 1){
        for(SRM_Model__c model: Trigger.New){
            model.addError('Only one SRM Model can be in approved status at a time.');
        }
    }
}