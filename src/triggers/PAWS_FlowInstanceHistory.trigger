trigger PAWS_FlowInstanceHistory on STSWR1__Flow_Instance_History__c (before insert, after insert, before update, after update) 
{
	new PAWS_FlowInstanceHistoryTrigger().executeIgnoreWorkflow();
}