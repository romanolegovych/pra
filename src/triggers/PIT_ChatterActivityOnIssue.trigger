trigger PIT_ChatterActivityOnIssue on Issue__c (after insert,after update,before delete) {
    List<FeedItem> feedItems = new List<FeedItem>();    
    
    if (Trigger.isInsert){
    for (Issue__c ist : Trigger.new) {
        if(ist.description__c != null && ist.description__c != '') {
            FeedItem fitemIssue = new FeedItem();           
            //fitemIssue.type = 'LinkPost'; 
            fitemIssue.type='Textpost';          
            fitemIssue.ParentId = ist.Id;           
            //fitemIssue.LinkUrl = '/' + ist.id;            
            fitemIssue.Title = ist.name;  
            //Issue__c assignedTo = ist.Id;
            fitemIssue.Body = ist.Name +' '+'  Description :'+ist.Description__c;
            feedItems.add(fitemIssue);
            
        }
    }
    }
    if (Trigger.isUpdate){
    for (Issue__c ist : Trigger.new) {
        if(Trigger.oldMap.get(ist.id).description__c != Trigger.newMap.get(ist.id).description__c) {
            FeedItem fitemIssue = new FeedItem();           
            //fitemIssue.type = 'LinkPost'; 
            fitemIssue.type = 'Textpost';                     
            fitemIssue.ParentId = ist.Id;           
            //fitemIssue.LinkUrl = '/' + ist.id;            
            fitemIssue.Title = ist.name;  
            //Issue__c assignedTo = ist.Id;
            fitemIssue.Body = ist.Name +' '+'  Updated Description to:'+ist.Description__c;
            feedItems.add(fitemIssue);
            
        }
    }
    }
    if(trigger.isDelete){
        Id currentuserid=UserInfo.getUserId();
        for(Issue__c istd:Trigger.old){
            if(istd.createdbyid != currentuserid){
              istd.addError('Only an Admin or the creator of the issue can delete the issue. This issue cannot be deleted.');
 
            
            }
        }
    
    
    }
    //Save the FeedItems all at once.
    if (feedItems.size() > 0) {  
        Database.insert(feedItems,false);
                
    }
   
}