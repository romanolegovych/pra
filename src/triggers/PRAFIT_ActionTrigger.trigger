trigger PRAFIT_ActionTrigger on PRAFIT_Action__c (before insert, before update) {	
	for(PRAFIT_Action__c actionObj : trigger.new){
		if( trigger.isInsert || (trigger.isUpdate && actionObj.Dependency__c != trigger.oldMap.get(actionObj.id).Dependency__c) ){
			actionObj.Dependency_Reporting__c = actionObj.Dependency__c;
		}
	}
}