trigger BDT_ProjectService_Trigger on ProjectService__c (before insert, before update) {

	For (ProjectService__c ps:trigger.new) {
		
		// set unique to it's value to ensure no duplicate records can be created
		ps.unique__c = '' + ps.Client_Project__c + ps.Service__c;
		
		if (ps.BDTDeleted__c) {
			// uniqueness is not required for soft deleted records
			ps.unique__c = null;
		}
		
	}

}