trigger BDT_ServiceCategory_Trigger on ServiceCategory__c (after delete, after insert, after undelete, 
after update, before delete, before insert, before update) {
	
	if (Trigger.isBefore && Trigger.isInsert) {
		for (ServiceCategory__c sc : Trigger.new){
			sc = BDT_Utils.setServiceCategoryParent(sc);
		}
	}
	if (Trigger.isBefore && Trigger.isUpdate) {
		for (ServiceCategory__c sc : Trigger.new){
			sc = BDT_Utils.setServiceCategoryParent(sc);
		}
	}

}