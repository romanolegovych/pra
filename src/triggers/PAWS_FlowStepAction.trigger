trigger PAWS_FlowStepAction on STSWR1__Flow_Step_Action__c (after delete, after insert, after update, 
before delete, before insert, before update) {
	new PAWS_FlowStepActionTrigger().executeIgnoreWorkflow();
}