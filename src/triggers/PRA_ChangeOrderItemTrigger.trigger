trigger PRA_ChangeOrderItemTrigger on Change_Order_Item__c (before update, after update) {
	PRA_ChangeOrderItemTriggerHandler handler = new PRA_ChangeOrderItemTriggerHandler();
	
	if (Trigger.isUpdate && Trigger.isBefore){
		//handler.onBeforeUpdate(Trigger.oldMap, Trigger.newMap);
	}
	if (Trigger.isUpdate && Trigger.isAfter){
		handler.onAfterUpdate(Trigger.oldMap, Trigger.newMap);
	}
}