trigger CM_AgreementSiteRollupTrigger on Apttus__APTS_Agreement__c (after insert, after update, before delete) {
	CM_AgreementSiteRollupTriggerHandler handler = new CM_AgreementSiteRollupTriggerHandler(Trigger.isExecuting, Trigger.size);    
  	boolean bDebug = false;
  	
	if (!Trigger.isDelete) {	  
    	if (Trigger.isAfter){          
        	if (Trigger.isUpdate) {
        		if (!CM_AgreementSiteRollupTriggerHandler.bTriggerHasRun) {
            		handler.OnAfterUpdate(Trigger.oldMap,Trigger.newMap);
            		CM_AgreementSiteRollupTriggerHandler.bTriggerHasRun = true;
        		}	               
            }
            else if (Trigger.isInsert) {
                handler.onAfterInsert(Trigger.new);
            }          
        }   
    }	                 
    else if (Trigger.isDelete) {         
		if (Trigger.isBefore){  
        	handler.OnBeforeDelete(Trigger.oldMap);                           
        }
	}       
}