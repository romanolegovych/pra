trigger BDT_FlowChartServiceTotal_Trigger on FlowchartServiceTotal__c (after delete, after insert, after update) {
	
	// Start sanitization
	// Sanitize the data, this object should not be semi- or fully orphaned
	if(trigger.isAfter && (trigger.isUpdate || trigger.isInsert)) {
		List<FlowchartServiceTotal__c> orphanedList = new List<FlowchartServiceTotal__c>();
		for(FlowchartServiceTotal__c e : trigger.new) {
			if (e.Study__c == null || e.ProjectService__c == null) {
				FlowchartServiceTotal__c orphanedItem = new FlowchartServiceTotal__c(ID=e.id);
				orphanedList.add(orphanedItem);
			}
    	}
		delete orphanedList;
    }
    // End sanitization

}