trigger IPA_LinkTrigger on IPA_Links__c (before insert, before update) {
    
    //constants and vars
    static final string RECORDTYPE = 'Quick Link';
    static final Id RECORDTYPEID = Schema.SObjectType.IPA_Links__c.getRecordTypeInfosByName().get(RECORDTYPE).getRecordTypeId();
    static final integer MAXQUICKLINKS = 10;
    static final string MAXQUICKLINKSERROR = 'The limit of ' + MAXQUICKLINKS + ' ' + RECORDTYPE + 's has been reached for the selected Category.';
    List<AggregateResult> existingQuickLinksByCategoryList = [SELECT Category__c, count(id)myCount
                                                                     FROM IPA_Links__c                                          
                                                                     WHERE RecordTypeId = : RECORDTYPEID
                                                                     GROUP BY Category__c
                                                                     ORDER BY Category__c];
                                                                                                                    
    static map<string,integer> existingQuickLinksByCategoryMap = new map<string,integer>();
    for(AggregateResult arObj : existingQuickLinksByCategoryList){
        existingQuickLinksByCategoryMap.put(string.valueof(arObj.get('Category__c')),(Integer)arObj.get('myCount'));
    }
                
    //validate the Category lookup field for max amount of quicklinks reached
    public void validateMaxQuickLinks(IPA_Links__c linkObj){
        integer existingQuickLinksCount = existingQuickLinksByCategoryMap.get(linkObj.Category__c);
        system.debug(existingQuickLinksByCategoryMap.get(linkObj.Category__c));
        if(existingQuickLinksCount >= MAXQUICKLINKS){
            linkObj.adderror('');
            linkObj.Category__c.adderror(MAXQUICKLINKSERROR);
        }else{
            existingQuickLinksByCategoryMap.put(linkObj.Category__c, 1);
            system.debug(existingQuickLinksByCategoryMap.get(linkObj.Category__c));
        }        
    }
    
    List<String> TopicNameshavingMAXQUICKLINKS = new List<String>();
    public void ValidateMaxQuickLinksByTopic(IPA_Links__c linkObj)
    {
        set<Id> TopicIds = new set<Id>();
        map<string,integer> existingQuickLinksByTopicMap = new map<string,integer>();       
        /*system.debug('in ValidateMaxQuickLinksByTopic()');
        List<AggregateResult> existingQuickLinksByTopicList = [SELECT Topic__c, count(link__c)countbytopic 
                                                                    FROM IPA_Link_Topic__c 
                                                                    WHERE Link__c =: linkObj.Id 
                                                                    GROUP BY Topic__c];
        system.debug('existingQuickLinksByTopicList : ' +existingQuickLinksByTopicList);
        map<string,integer> existingQuickLinksByTopicMap = new map<string,integer>();       
        for(AggregateResult arObj : existingQuickLinksByTopicList){
            existingQuickLinksByTopicMap.put(string.valueof(arObj.get('Topic__c')),(Integer)arObj.get('countbytopic'));
            //system.debug('(Integer)arObj.get('countbytopic') : ' +(Integer)arObj.get('countbytopic'));
            if((Integer)arObj.get('countbytopic') >= 10)
            TopicNameshavingMAXQUICKLINKS.add(string.valueof(arObj.get('Topic__c')));
            }           
        */      
        List<IPA_Link_Topic__c> TopicIdshavingLink = [SELECT Topic__c
                                                        FROM IPA_Link_Topic__c 
                                                        WHERE Link__c =: linkObj.Id]; 
        
        for(IPA_Link_Topic__c loopvar :TopicIdshavingLink) TopicIds.add(loopvar.Topic__c);
            
        List<AggregateResult> existingQuickLinksByTopicList = [SELECT Topic__c, count(Link__c)linkscount 
                                                           FROM IPA_Link_Topic__c 
                                                           WHERE Topic__c =: TopicIds AND Link__r.RecordTypeId =: RECORDTYPEID
                                                           GROUP BY Topic__c];
        for(AggregateResult arObj : existingQuickLinksByTopicList)
        {
            existingQuickLinksByTopicMap.put(string.valueof(arObj.get('Topic__c')),(Integer)arObj.get('linkscount'));
            if((Integer)arObj.get('linkscount') >= 10)
                TopicNameshavingMAXQUICKLINKS.add([Select Name from IPA_Topic__c where Id =: string.valueof(arObj.get('Topic__c'))].Name);        
        }
        if(!TopicNameshavingMAXQUICKLINKS.isEmpty())
            linkObj.adderror(MAXQUICKLINKS+ ' Quick Links exists for the Topic :' +TopicNameshavingMAXQUICKLINKS);
    }
    
    //trigger loop       
    for(IPA_Links__c linkObj :Trigger.new){
        if(Trigger.IsInsert){ 
            if(linkObj.RecordTypeId == RECORDTYPEID){
                validateMaxQuickLinks(linkObj);
            }
        } 
        if(Trigger.IsUpdate){
            IPA_Links__c oldLinkObj = System.Trigger.oldMap.get(linkObj.Id);    
            if(linkObj.RecordTypeId == RECORDTYPEID && oldLinkObj.Category__c != linkObj.Category__c) 
                validateMaxQuickLinks(linkObj);
            if(linkObj.RecordTypeId == RECORDTYPEID && oldLinkObj.RecordTypeId != linkObj.RecordTypeId) {          
                system.debug('in If condition');
                validateMaxQuickLinks(linkObj);
                ValidateMaxQuickLinksByTopic(linkObj);  
            }       
        }
    }
    
}